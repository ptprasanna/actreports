
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000790')]      |
| SIG_REGION                | [('0x80002410', '0x80002590', '96 words')]      |
| COV_LABELS                | fmadd_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/FMA/work-fmadd-Zfh1/fmadd_b6-01.S/ref.S    |
| Total Number of coverpoints| 151     |
| Total Coverpoints Hit     | 151      |
| Total Signature Updates   | 92      |
| STAT1                     | 46      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 46     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmadd.h', 'rs1 : f30', 'rs2 : f29', 'rd : f31', 'rs3 : f29', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x80000128]:fmadd.h ft11, ft10, ft9, ft9, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:fsw ft11, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80002418]:0x00000002




Last Coverpoint : ['rs1 : f28', 'rs2 : f28', 'rd : f28', 'rs3 : f28', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x8000014c]:fmadd.h ft8, ft8, ft8, ft8, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:fsw ft8, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80002420]:0x00000022




Last Coverpoint : ['rs1 : f31', 'rs2 : f31', 'rd : f30', 'rs3 : f31', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x80000170]:fmadd.h ft10, ft11, ft11, ft11, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:fsw ft10, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80002428]:0x00000042




Last Coverpoint : ['rs1 : f29', 'rs2 : f27', 'rd : f27', 'rs3 : f30', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000194]:fmadd.h fs11, ft9, fs11, ft10, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:fsw fs11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80002430]:0x00000062




Last Coverpoint : ['rs1 : f26', 'rs2 : f30', 'rd : f26', 'rs3 : f26', 'rs1 == rd == rs3 != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001b8]:fmadd.h fs10, fs10, ft10, fs10, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:fsw fs10, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80002438]:0x00000082




Last Coverpoint : ['rs1 : f25', 'rs2 : f25', 'rd : f29', 'rs3 : f27', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x800001dc]:fmadd.h ft9, fs9, fs9, fs11, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:fsw ft9, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80002440]:0x00000002




Last Coverpoint : ['rs1 : f27', 'rs2 : f26', 'rd : f25', 'rs3 : f25', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000200]:fmadd.h fs9, fs11, fs10, fs9, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs9, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80002448]:0x00000022




Last Coverpoint : ['rs1 : f23', 'rs2 : f22', 'rd : f24', 'rs3 : f23', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fmadd.h fs8, fs7, fs6, fs7, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs8, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80002450]:0x00000042




Last Coverpoint : ['rs1 : f24', 'rs2 : f21', 'rd : f21', 'rs3 : f21', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x80000248]:fmadd.h fs5, fs8, fs5, fs5, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:fsw fs5, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80002458]:0x00000062




Last Coverpoint : ['rs1 : f22', 'rs2 : f24', 'rd : f23', 'rs3 : f20', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmadd.h fs7, fs6, fs8, fs4, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:fsw fs7, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80002460]:0x00000082




Last Coverpoint : ['rs1 : f20', 'rs2 : f23', 'rd : f20', 'rs3 : f24', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2']
Last Code Sequence : 
	-[0x80000290]:fmadd.h fs4, fs4, fs7, fs8, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:fsw fs4, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80002468]:0x00000000




Last Coverpoint : ['rs1 : f19', 'rs2 : f19', 'rd : f19', 'rs3 : f22', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x800002b4]:fmadd.h fs3, fs3, fs3, fs6, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:fsw fs3, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80002470]:0x00000000




Last Coverpoint : ['rs1 : f21', 'rs2 : f20', 'rd : f22', 'rs3 : f19']
Last Code Sequence : 
	-[0x800002d8]:fmadd.h fs6, fs5, fs4, fs3, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:fsw fs6, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80002478]:0x00000000




Last Coverpoint : ['rs1 : f17', 'rs2 : f16', 'rd : f18', 'rs3 : f15']
Last Code Sequence : 
	-[0x800002fc]:fmadd.h fs2, fa7, fa6, fa5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fs2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80002480]:0x00000000




Last Coverpoint : ['rs1 : f15', 'rs2 : f18', 'rd : f17', 'rs3 : f16']
Last Code Sequence : 
	-[0x80000320]:fmadd.h fa7, fa5, fs2, fa6, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:fsw fa7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80002488]:0x00000000




Last Coverpoint : ['rs1 : f18', 'rs2 : f15', 'rd : f16', 'rs3 : f17']
Last Code Sequence : 
	-[0x80000344]:fmadd.h fa6, fs2, fa5, fa7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80002490]:0x00000000




Last Coverpoint : ['rs1 : f16', 'rs2 : f17', 'rd : f15', 'rs3 : f18']
Last Code Sequence : 
	-[0x80000368]:fmadd.h fa5, fa6, fa7, fs2, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:fsw fa5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80002498]:0x00000000




Last Coverpoint : ['rs1 : f13', 'rs2 : f12', 'rd : f14', 'rs3 : f11']
Last Code Sequence : 
	-[0x8000038c]:fmadd.h fa4, fa3, fa2, fa1, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:fsw fa4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x800024a0]:0x00000000




Last Coverpoint : ['rs1 : f11', 'rs2 : f14', 'rd : f13', 'rs3 : f12']
Last Code Sequence : 
	-[0x800003b0]:fmadd.h fa3, fa1, fa4, fa2, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:fsw fa3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800024a8]:0x00000000




Last Coverpoint : ['rs1 : f14', 'rs2 : f11', 'rd : f12', 'rs3 : f13']
Last Code Sequence : 
	-[0x800003d4]:fmadd.h fa2, fa4, fa1, fa3, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsw fa2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800024b0]:0x00000000




Last Coverpoint : ['rs1 : f12', 'rs2 : f13', 'rd : f11', 'rs3 : f14']
Last Code Sequence : 
	-[0x800003f8]:fmadd.h fa1, fa2, fa3, fa4, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw fa1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800024b8]:0x00000000




Last Coverpoint : ['rs1 : f9', 'rs2 : f8', 'rd : f10', 'rs3 : f7']
Last Code Sequence : 
	-[0x8000041c]:fmadd.h fa0, fs1, fs0, ft7, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:fsw fa0, 168(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x800024c0]:0x00000000




Last Coverpoint : ['rs1 : f7', 'rs2 : f10', 'rd : f9', 'rs3 : f8']
Last Code Sequence : 
	-[0x80000440]:fmadd.h fs1, ft7, fa0, fs0, dyn
	-[0x80000444]:csrrs tp, fcsr, zero
	-[0x80000448]:fsw fs1, 176(ra)
Current Store : [0x8000044c] : sw tp, 180(ra) -- Store: [0x800024c8]:0x00000000




Last Coverpoint : ['rs1 : f10', 'rs2 : f7', 'rd : f8', 'rs3 : f9']
Last Code Sequence : 
	-[0x80000464]:fmadd.h fs0, fa0, ft7, fs1, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw fs0, 184(ra)
Current Store : [0x80000470] : sw tp, 188(ra) -- Store: [0x800024d0]:0x00000000




Last Coverpoint : ['rs1 : f8', 'rs2 : f9', 'rd : f7', 'rs3 : f10']
Last Code Sequence : 
	-[0x80000488]:fmadd.h ft7, fs0, fs1, fa0, dyn
	-[0x8000048c]:csrrs tp, fcsr, zero
	-[0x80000490]:fsw ft7, 192(ra)
Current Store : [0x80000494] : sw tp, 196(ra) -- Store: [0x800024d8]:0x00000000




Last Coverpoint : ['rs1 : f5', 'rs2 : f4', 'rd : f6', 'rs3 : f3']
Last Code Sequence : 
	-[0x800004ac]:fmadd.h ft6, ft5, ft4, ft3, dyn
	-[0x800004b0]:csrrs tp, fcsr, zero
	-[0x800004b4]:fsw ft6, 200(ra)
Current Store : [0x800004b8] : sw tp, 204(ra) -- Store: [0x800024e0]:0x00000000




Last Coverpoint : ['rs1 : f3', 'rs2 : f6', 'rd : f5', 'rs3 : f4']
Last Code Sequence : 
	-[0x800004d0]:fmadd.h ft5, ft3, ft6, ft4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsw ft5, 208(ra)
Current Store : [0x800004dc] : sw tp, 212(ra) -- Store: [0x800024e8]:0x00000000




Last Coverpoint : ['rs1 : f6', 'rs2 : f3', 'rd : f4', 'rs3 : f5']
Last Code Sequence : 
	-[0x800004f4]:fmadd.h ft4, ft6, ft3, ft5, dyn
	-[0x800004f8]:csrrs tp, fcsr, zero
	-[0x800004fc]:fsw ft4, 216(ra)
Current Store : [0x80000500] : sw tp, 220(ra) -- Store: [0x800024f0]:0x00000000




Last Coverpoint : ['rs1 : f4', 'rs2 : f5', 'rd : f3', 'rs3 : f6']
Last Code Sequence : 
	-[0x80000518]:fmadd.h ft3, ft4, ft5, ft6, dyn
	-[0x8000051c]:csrrs tp, fcsr, zero
	-[0x80000520]:fsw ft3, 224(ra)
Current Store : [0x80000524] : sw tp, 228(ra) -- Store: [0x800024f8]:0x00000000




Last Coverpoint : ['rs1 : f2']
Last Code Sequence : 
	-[0x8000053c]:fmadd.h ft11, ft2, ft10, ft9, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:fsw ft11, 232(ra)
Current Store : [0x80000548] : sw tp, 236(ra) -- Store: [0x80002500]:0x00000000




Last Coverpoint : ['rs1 : f1']
Last Code Sequence : 
	-[0x80000560]:fmadd.h ft11, ft1, ft10, ft9, dyn
	-[0x80000564]:csrrs tp, fcsr, zero
	-[0x80000568]:fsw ft11, 240(ra)
Current Store : [0x8000056c] : sw tp, 244(ra) -- Store: [0x80002508]:0x00000000




Last Coverpoint : ['rs1 : f0']
Last Code Sequence : 
	-[0x80000584]:fmadd.h ft11, ft0, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 248(ra)
Current Store : [0x80000590] : sw tp, 252(ra) -- Store: [0x80002510]:0x00000000




Last Coverpoint : ['rs2 : f2']
Last Code Sequence : 
	-[0x800005a8]:fmadd.h ft11, ft10, ft2, ft9, dyn
	-[0x800005ac]:csrrs tp, fcsr, zero
	-[0x800005b0]:fsw ft11, 256(ra)
Current Store : [0x800005b4] : sw tp, 260(ra) -- Store: [0x80002518]:0x00000000




Last Coverpoint : ['rs2 : f1']
Last Code Sequence : 
	-[0x800005cc]:fmadd.h ft11, ft10, ft1, ft9, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsw ft11, 264(ra)
Current Store : [0x800005d8] : sw tp, 268(ra) -- Store: [0x80002520]:0x00000000




Last Coverpoint : ['rs2 : f0']
Last Code Sequence : 
	-[0x800005f0]:fmadd.h ft11, ft10, ft0, ft9, dyn
	-[0x800005f4]:csrrs tp, fcsr, zero
	-[0x800005f8]:fsw ft11, 272(ra)
Current Store : [0x800005fc] : sw tp, 276(ra) -- Store: [0x80002528]:0x00000000




Last Coverpoint : ['rs3 : f2']
Last Code Sequence : 
	-[0x80000614]:fmadd.h ft11, ft10, ft9, ft2, dyn
	-[0x80000618]:csrrs tp, fcsr, zero
	-[0x8000061c]:fsw ft11, 280(ra)
Current Store : [0x80000620] : sw tp, 284(ra) -- Store: [0x80002530]:0x00000000




Last Coverpoint : ['rs3 : f1']
Last Code Sequence : 
	-[0x80000638]:fmadd.h ft11, ft10, ft9, ft1, dyn
	-[0x8000063c]:csrrs tp, fcsr, zero
	-[0x80000640]:fsw ft11, 288(ra)
Current Store : [0x80000644] : sw tp, 292(ra) -- Store: [0x80002538]:0x00000000




Last Coverpoint : ['rs3 : f0']
Last Code Sequence : 
	-[0x8000065c]:fmadd.h ft11, ft10, ft9, ft0, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:fsw ft11, 296(ra)
Current Store : [0x80000668] : sw tp, 300(ra) -- Store: [0x80002540]:0x00000000




Last Coverpoint : ['rd : f2']
Last Code Sequence : 
	-[0x80000680]:fmadd.h ft2, ft11, ft10, ft9, dyn
	-[0x80000684]:csrrs tp, fcsr, zero
	-[0x80000688]:fsw ft2, 304(ra)
Current Store : [0x8000068c] : sw tp, 308(ra) -- Store: [0x80002548]:0x00000000




Last Coverpoint : ['rd : f1']
Last Code Sequence : 
	-[0x800006a4]:fmadd.h ft1, ft11, ft10, ft9, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft1, 312(ra)
Current Store : [0x800006b0] : sw tp, 316(ra) -- Store: [0x80002550]:0x00000000




Last Coverpoint : ['rd : f0']
Last Code Sequence : 
	-[0x800006c8]:fmadd.h ft0, ft11, ft10, ft9, dyn
	-[0x800006cc]:csrrs tp, fcsr, zero
	-[0x800006d0]:fsw ft0, 320(ra)
Current Store : [0x800006d4] : sw tp, 324(ra) -- Store: [0x80002558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fmadd.h ft11, ft10, ft9, ft8, dyn
	-[0x800006f0]:csrrs tp, fcsr, zero
	-[0x800006f4]:fsw ft11, 328(ra)
Current Store : [0x800006f8] : sw tp, 332(ra) -- Store: [0x80002560]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000710]:fmadd.h ft11, ft10, ft9, ft8, dyn
	-[0x80000714]:csrrs tp, fcsr, zero
	-[0x80000718]:fsw ft11, 336(ra)
Current Store : [0x8000071c] : sw tp, 340(ra) -- Store: [0x80002568]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fmadd.h ft11, ft10, ft9, ft8, dyn
	-[0x80000738]:csrrs tp, fcsr, zero
	-[0x8000073c]:fsw ft11, 344(ra)
Current Store : [0x80000740] : sw tp, 348(ra) -- Store: [0x80002570]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000758]:fmadd.h ft11, ft10, ft9, ft8, dyn
	-[0x8000075c]:csrrs tp, fcsr, zero
	-[0x80000760]:fsw ft11, 352(ra)
Current Store : [0x80000764] : sw tp, 356(ra) -- Store: [0x80002578]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000077c]:fmadd.h ft11, ft10, ft9, ft8, dyn
	-[0x80000780]:csrrs tp, fcsr, zero
	-[0x80000784]:fsw ft11, 360(ra)
Current Store : [0x80000788] : sw tp, 364(ra) -- Store: [0x80002580]:0x00000062





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                                                                                             coverpoints                                                                                                                                                                                                             |                                                            code                                                             |
|---:|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002414]<br>0xFBB6FAB7<br> |- mnemonic : fmadd.h<br> - rs1 : f30<br> - rs2 : f29<br> - rd : f31<br> - rs3 : f29<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                                                                   |[0x80000128]:fmadd.h ft11, ft10, ft9, ft9, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:fsw ft11, 0(ra)<br>    |
|   2|[0x8000241c]<br>0xDDB7D5BF<br> |- rs1 : f28<br> - rs2 : f28<br> - rd : f28<br> - rs3 : f28<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                                                                                         |[0x8000014c]:fmadd.h ft8, ft8, ft8, ft8, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:fsw ft8, 8(ra)<br>       |
|   3|[0x80002424]<br>0xF76DF56F<br> |- rs1 : f31<br> - rs2 : f31<br> - rd : f30<br> - rs3 : f31<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000170]:fmadd.h ft10, ft11, ft11, ft11, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:fsw ft10, 16(ra)<br> |
|   4|[0x8000242c]<br>0xBB6FAB7F<br> |- rs1 : f29<br> - rs2 : f27<br> - rd : f27<br> - rs3 : f30<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                               |[0x80000194]:fmadd.h fs11, ft9, fs11, ft10, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:fsw fs11, 24(ra)<br>  |
|   5|[0x80002434]<br>0x76DF56FF<br> |- rs1 : f26<br> - rs2 : f30<br> - rd : f26<br> - rs3 : f26<br> - rs1 == rd == rs3 != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                            |[0x800001b8]:fmadd.h fs10, fs10, ft10, fs10, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:fsw fs10, 32(ra)<br> |
|   6|[0x8000243c]<br>0xEEDBEADF<br> |- rs1 : f25<br> - rs2 : f25<br> - rd : f29<br> - rs3 : f27<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                                                                                            |[0x800001dc]:fmadd.h ft9, fs9, fs9, fs11, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:fsw ft9, 40(ra)<br>     |
|   7|[0x80002444]<br>0xEDBEADFE<br> |- rs1 : f27<br> - rs2 : f26<br> - rd : f25<br> - rs3 : f25<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                               |[0x80000200]:fmadd.h fs9, fs11, fs10, fs9, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:fsw fs9, 48(ra)<br>    |
|   8|[0x8000244c]<br>0xDB7D5BFD<br> |- rs1 : f23<br> - rs2 : f22<br> - rd : f24<br> - rs3 : f23<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                               |[0x80000224]:fmadd.h fs8, fs7, fs6, fs7, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:fsw fs8, 56(ra)<br>      |
|   9|[0x80002454]<br>0xDBEADFEE<br> |- rs1 : f24<br> - rs2 : f21<br> - rd : f21<br> - rs3 : f21<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000248]:fmadd.h fs5, fs8, fs5, fs5, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:fsw fs5, 64(ra)<br>      |
|  10|[0x8000245c]<br>0xB6FAB7FB<br> |- rs1 : f22<br> - rs2 : f24<br> - rd : f23<br> - rs3 : f20<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br> |[0x8000026c]:fmadd.h fs7, fs6, fs8, fs4, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:fsw fs7, 72(ra)<br>      |
|  11|[0x80002464]<br>0xB7D5BFDD<br> |- rs1 : f20<br> - rs2 : f23<br> - rd : f20<br> - rs3 : f24<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br>                                                                                                                                                                                                                                                                                                            |[0x80000290]:fmadd.h fs4, fs4, fs7, fs8, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:fsw fs4, 80(ra)<br>      |
|  12|[0x8000246c]<br>0x6FAB7FBB<br> |- rs1 : f19<br> - rs2 : f19<br> - rd : f19<br> - rs3 : f22<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                                                                                         |[0x800002b4]:fmadd.h fs3, fs3, fs3, fs6, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:fsw fs3, 88(ra)<br>      |
|  13|[0x80002474]<br>0x6DF56FF7<br> |- rs1 : f21<br> - rs2 : f20<br> - rd : f22<br> - rs3 : f19<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x800002d8]:fmadd.h fs6, fs5, fs4, fs3, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:fsw fs6, 96(ra)<br>      |
|  14|[0x8000247c]<br>0xDF56FF76<br> |- rs1 : f17<br> - rs2 : f16<br> - rd : f18<br> - rs3 : f15<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x800002fc]:fmadd.h fs2, fa7, fa6, fa5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:fsw fs2, 104(ra)<br>     |
|  15|[0x80002484]<br>0xBEADFEED<br> |- rs1 : f15<br> - rs2 : f18<br> - rd : f17<br> - rs3 : f16<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x80000320]:fmadd.h fa7, fa5, fs2, fa6, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:fsw fa7, 112(ra)<br>     |
|  16|[0x8000248c]<br>0x7D5BFDDB<br> |- rs1 : f18<br> - rs2 : f15<br> - rd : f16<br> - rs3 : f17<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x80000344]:fmadd.h fa6, fs2, fa5, fa7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:fsw fa6, 120(ra)<br>     |
|  17|[0x80002494]<br>0xFAB7FBB6<br> |- rs1 : f16<br> - rs2 : f17<br> - rd : f15<br> - rs3 : f18<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x80000368]:fmadd.h fa5, fa6, fa7, fs2, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:fsw fa5, 128(ra)<br>     |
|  18|[0x8000249c]<br>0xF56FF76D<br> |- rs1 : f13<br> - rs2 : f12<br> - rd : f14<br> - rs3 : f11<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x8000038c]:fmadd.h fa4, fa3, fa2, fa1, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:fsw fa4, 136(ra)<br>     |
|  19|[0x800024a4]<br>0xEADFEEDB<br> |- rs1 : f11<br> - rs2 : f14<br> - rd : f13<br> - rs3 : f12<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x800003b0]:fmadd.h fa3, fa1, fa4, fa2, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:fsw fa3, 144(ra)<br>     |
|  20|[0x800024ac]<br>0xD5BFDDB7<br> |- rs1 : f14<br> - rs2 : f11<br> - rd : f12<br> - rs3 : f13<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x800003d4]:fmadd.h fa2, fa4, fa1, fa3, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:fsw fa2, 152(ra)<br>     |
|  21|[0x800024b4]<br>0xAB7FBB6F<br> |- rs1 : f12<br> - rs2 : f13<br> - rd : f11<br> - rs3 : f14<br>                                                                                                                                                                                                                                                                                                                                                                       |[0x800003f8]:fmadd.h fa1, fa2, fa3, fa4, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:fsw fa1, 160(ra)<br>     |
|  22|[0x800024bc]<br>0x00002000<br> |- rs1 : f9<br> - rs2 : f8<br> - rd : f10<br> - rs3 : f7<br>                                                                                                                                                                                                                                                                                                                                                                          |[0x8000041c]:fmadd.h fa0, fs1, fs0, ft7, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:fsw fa0, 168(ra)<br>     |
|  23|[0x800024c4]<br>0xADFEEDBE<br> |- rs1 : f7<br> - rs2 : f10<br> - rd : f9<br> - rs3 : f8<br>                                                                                                                                                                                                                                                                                                                                                                          |[0x80000440]:fmadd.h fs1, ft7, fa0, fs0, dyn<br> [0x80000444]:csrrs tp, fcsr, zero<br> [0x80000448]:fsw fs1, 176(ra)<br>     |
|  24|[0x800024cc]<br>0x5BFDDB7D<br> |- rs1 : f10<br> - rs2 : f7<br> - rd : f8<br> - rs3 : f9<br>                                                                                                                                                                                                                                                                                                                                                                          |[0x80000464]:fmadd.h fs0, fa0, ft7, fs1, dyn<br> [0x80000468]:csrrs tp, fcsr, zero<br> [0x8000046c]:fsw fs0, 184(ra)<br>     |
|  25|[0x800024d4]<br>0xB7FBB6FA<br> |- rs1 : f8<br> - rs2 : f9<br> - rd : f7<br> - rs3 : f10<br>                                                                                                                                                                                                                                                                                                                                                                          |[0x80000488]:fmadd.h ft7, fs0, fs1, fa0, dyn<br> [0x8000048c]:csrrs tp, fcsr, zero<br> [0x80000490]:fsw ft7, 192(ra)<br>     |
|  26|[0x800024dc]<br>0x80002000<br> |- rs1 : f5<br> - rs2 : f4<br> - rd : f6<br> - rs3 : f3<br>                                                                                                                                                                                                                                                                                                                                                                           |[0x800004ac]:fmadd.h ft6, ft5, ft4, ft3, dyn<br> [0x800004b0]:csrrs tp, fcsr, zero<br> [0x800004b4]:fsw ft6, 200(ra)<br>     |
|  27|[0x800024e4]<br>0x800000F8<br> |- rs1 : f3<br> - rs2 : f6<br> - rd : f5<br> - rs3 : f4<br>                                                                                                                                                                                                                                                                                                                                                                           |[0x800004d0]:fmadd.h ft5, ft3, ft6, ft4, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:fsw ft5, 208(ra)<br>     |
|  28|[0x800024ec]<br>0x00000000<br> |- rs1 : f6<br> - rs2 : f3<br> - rd : f4<br> - rs3 : f5<br>                                                                                                                                                                                                                                                                                                                                                                           |[0x800004f4]:fmadd.h ft4, ft6, ft3, ft5, dyn<br> [0x800004f8]:csrrs tp, fcsr, zero<br> [0x800004fc]:fsw ft4, 216(ra)<br>     |
|  29|[0x800024f4]<br>0x80002010<br> |- rs1 : f4<br> - rs2 : f5<br> - rd : f3<br> - rs3 : f6<br>                                                                                                                                                                                                                                                                                                                                                                           |[0x80000518]:fmadd.h ft3, ft4, ft5, ft6, dyn<br> [0x8000051c]:csrrs tp, fcsr, zero<br> [0x80000520]:fsw ft3, 224(ra)<br>     |
|  30|[0x800024fc]<br>0xFBB6FAB7<br> |- rs1 : f2<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x8000053c]:fmadd.h ft11, ft2, ft10, ft9, dyn<br> [0x80000540]:csrrs tp, fcsr, zero<br> [0x80000544]:fsw ft11, 232(ra)<br>  |
|  31|[0x80002504]<br>0xFBB6FAB7<br> |- rs1 : f1<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x80000560]:fmadd.h ft11, ft1, ft10, ft9, dyn<br> [0x80000564]:csrrs tp, fcsr, zero<br> [0x80000568]:fsw ft11, 240(ra)<br>  |
|  32|[0x8000250c]<br>0xFBB6FAB7<br> |- rs1 : f0<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x80000584]:fmadd.h ft11, ft0, ft10, ft9, dyn<br> [0x80000588]:csrrs tp, fcsr, zero<br> [0x8000058c]:fsw ft11, 248(ra)<br>  |
|  33|[0x80002514]<br>0xFBB6FAB7<br> |- rs2 : f2<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x800005a8]:fmadd.h ft11, ft10, ft2, ft9, dyn<br> [0x800005ac]:csrrs tp, fcsr, zero<br> [0x800005b0]:fsw ft11, 256(ra)<br>  |
|  34|[0x8000251c]<br>0xFBB6FAB7<br> |- rs2 : f1<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x800005cc]:fmadd.h ft11, ft10, ft1, ft9, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:fsw ft11, 264(ra)<br>  |
|  35|[0x80002524]<br>0xFBB6FAB7<br> |- rs2 : f0<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x800005f0]:fmadd.h ft11, ft10, ft0, ft9, dyn<br> [0x800005f4]:csrrs tp, fcsr, zero<br> [0x800005f8]:fsw ft11, 272(ra)<br>  |
|  36|[0x8000252c]<br>0xFBB6FAB7<br> |- rs3 : f2<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x80000614]:fmadd.h ft11, ft10, ft9, ft2, dyn<br> [0x80000618]:csrrs tp, fcsr, zero<br> [0x8000061c]:fsw ft11, 280(ra)<br>  |
|  37|[0x80002534]<br>0xFBB6FAB7<br> |- rs3 : f1<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x80000638]:fmadd.h ft11, ft10, ft9, ft1, dyn<br> [0x8000063c]:csrrs tp, fcsr, zero<br> [0x80000640]:fsw ft11, 288(ra)<br>  |
|  38|[0x8000253c]<br>0xFBB6FAB7<br> |- rs3 : f0<br>                                                                                                                                                                                                                                                                                                                                                                                                                       |[0x8000065c]:fmadd.h ft11, ft10, ft9, ft0, dyn<br> [0x80000660]:csrrs tp, fcsr, zero<br> [0x80000664]:fsw ft11, 296(ra)<br>  |
|  39|[0x80002544]<br>0x00000000<br> |- rd : f2<br>                                                                                                                                                                                                                                                                                                                                                                                                                        |[0x80000680]:fmadd.h ft2, ft11, ft10, ft9, dyn<br> [0x80000684]:csrrs tp, fcsr, zero<br> [0x80000688]:fsw ft2, 304(ra)<br>   |
|  40|[0x8000254c]<br>0x80002414<br> |- rd : f1<br>                                                                                                                                                                                                                                                                                                                                                                                                                        |[0x800006a4]:fmadd.h ft1, ft11, ft10, ft9, dyn<br> [0x800006a8]:csrrs tp, fcsr, zero<br> [0x800006ac]:fsw ft1, 312(ra)<br>   |
|  41|[0x80002554]<br>0x00000000<br> |- rd : f0<br>                                                                                                                                                                                                                                                                                                                                                                                                                        |[0x800006c8]:fmadd.h ft0, ft11, ft10, ft9, dyn<br> [0x800006cc]:csrrs tp, fcsr, zero<br> [0x800006d0]:fsw ft0, 320(ra)<br>   |
|  42|[0x8000255c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                          |[0x800006ec]:fmadd.h ft11, ft10, ft9, ft8, dyn<br> [0x800006f0]:csrrs tp, fcsr, zero<br> [0x800006f4]:fsw ft11, 328(ra)<br>  |
|  43|[0x80002564]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000710]:fmadd.h ft11, ft10, ft9, ft8, dyn<br> [0x80000714]:csrrs tp, fcsr, zero<br> [0x80000718]:fsw ft11, 336(ra)<br>  |
|  44|[0x8000256c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000734]:fmadd.h ft11, ft10, ft9, ft8, dyn<br> [0x80000738]:csrrs tp, fcsr, zero<br> [0x8000073c]:fsw ft11, 344(ra)<br>  |
|  45|[0x80002574]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                          |[0x80000758]:fmadd.h ft11, ft10, ft9, ft8, dyn<br> [0x8000075c]:csrrs tp, fcsr, zero<br> [0x80000760]:fsw ft11, 352(ra)<br>  |
|  46|[0x8000257c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and fs3 == 0 and fe3 == 0x00 and fm3 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000077c]:fmadd.h ft11, ft10, ft9, ft8, dyn<br> [0x80000780]:csrrs tp, fcsr, zero<br> [0x80000784]:fsw ft11, 360(ra)<br>  |
