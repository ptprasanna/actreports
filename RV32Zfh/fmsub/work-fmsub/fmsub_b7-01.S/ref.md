
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000a20')]      |
| SIG_REGION                | [('0x80002510', '0x80002720', '132 words')]      |
| COV_LABELS                | fmsub_b7      |
| TEST_NAME                 | /home/riscv/riscv-ctg/FMA/work-fmsub/fmsub_b7-01.S/ref.S    |
| Total Number of coverpoints| 197     |
| Total Coverpoints Hit     | 197      |
| Total Signature Updates   | 128      |
| STAT1                     | 64      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 64     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmsub.h', 'rs1 : f31', 'rs2 : f30', 'rd : f31', 'rs3 : f29', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x0d and fm2 == 0x374 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000128]:fmsub.h ft11, ft11, ft10, ft9, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:fsw ft11, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80002518]:0x00000062




Last Coverpoint : ['rs1 : f28', 'rs2 : f28', 'rd : f28', 'rs3 : f28', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x8000014c]:fmsub.h ft8, ft8, ft8, ft8, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:fsw ft8, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80002520]:0x00000067




Last Coverpoint : ['rs1 : f29', 'rs2 : f29', 'rd : f30', 'rs3 : f31', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fmsub.h ft10, ft9, ft9, ft11, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:fsw ft10, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80002528]:0x00000067




Last Coverpoint : ['rs1 : f30', 'rs2 : f27', 'rd : f27', 'rs3 : f26', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 0 and fe2 == 0x12 and fm2 == 0x05f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000194]:fmsub.h fs11, ft10, fs11, fs10, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:fsw fs11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80002530]:0x00000062




Last Coverpoint : ['rs1 : f27', 'rs2 : f25', 'rd : f29', 'rs3 : f25', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x800001b8]:fmsub.h ft9, fs11, fs9, fs9, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:fsw ft9, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80002538]:0x00000063




Last Coverpoint : ['rs1 : f24', 'rs2 : f31', 'rd : f24', 'rs3 : f24', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x800001dc]:fmsub.h fs8, fs8, ft11, fs8, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:fsw fs8, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80002540]:0x00000063




Last Coverpoint : ['rs1 : f23', 'rs2 : f23', 'rd : f26', 'rs3 : f23', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x80000200]:fmsub.h fs10, fs7, fs7, fs7, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs10, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80002548]:0x00000067




Last Coverpoint : ['rs1 : f22', 'rs2 : f26', 'rd : f25', 'rs3 : f22', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000224]:fmsub.h fs9, fs6, fs10, fs6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs9, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80002550]:0x00000063




Last Coverpoint : ['rs1 : f21', 'rs2 : f21', 'rd : f21', 'rs3 : f30', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000248]:fmsub.h fs5, fs5, fs5, ft10, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:fsw fs5, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80002558]:0x00000067




Last Coverpoint : ['rs1 : f26', 'rs2 : f24', 'rd : f20', 'rs3 : f20', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x17f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmsub.h fs4, fs10, fs8, fs4, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:fsw fs4, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80002560]:0x00000062




Last Coverpoint : ['rs1 : f25', 'rs2 : f22', 'rd : f23', 'rs3 : f27', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0a and fm2 == 0x21c and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000290]:fmsub.h fs7, fs9, fs6, fs11, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:fsw fs7, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80002568]:0x00000062




Last Coverpoint : ['rs1 : f20', 'rs2 : f19', 'rd : f19', 'rs3 : f19', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x800002b4]:fmsub.h fs3, fs4, fs3, fs3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:fsw fs3, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80002570]:0x00000063




Last Coverpoint : ['rs1 : f19', 'rs2 : f20', 'rd : f22', 'rs3 : f21', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x288 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fmsub.h fs6, fs3, fs4, fs5, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:fsw fs6, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80002578]:0x00000062




Last Coverpoint : ['rs1 : f17', 'rs2 : f16', 'rd : f18', 'rs3 : f15', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x10 and fm2 == 0x14a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmsub.h fs2, fa7, fa6, fa5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fs2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80002580]:0x00000062




Last Coverpoint : ['rs1 : f15', 'rs2 : f18', 'rd : f17', 'rs3 : f16', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x26f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000320]:fmsub.h fa7, fa5, fs2, fa6, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:fsw fa7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80002588]:0x00000062




Last Coverpoint : ['rs1 : f18', 'rs2 : f15', 'rd : f16', 'rs3 : f17', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0e and fm2 == 0x0a9 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fmsub.h fa6, fs2, fa5, fa7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80002590]:0x00000062




Last Coverpoint : ['rs1 : f16', 'rs2 : f17', 'rd : f15', 'rs3 : f18', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x356 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000368]:fmsub.h fa5, fa6, fa7, fs2, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:fsw fa5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80002598]:0x00000062




Last Coverpoint : ['rs1 : f13', 'rs2 : f12', 'rd : f14', 'rs3 : f11', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x2e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fmsub.h fa4, fa3, fa2, fa1, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:fsw fa4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x800025a0]:0x00000062




Last Coverpoint : ['rs1 : f11', 'rs2 : f14', 'rd : f13', 'rs3 : f12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x107 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003b0]:fmsub.h fa3, fa1, fa4, fa2, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:fsw fa3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800025a8]:0x00000062




Last Coverpoint : ['rs1 : f14', 'rs2 : f11', 'rd : f12', 'rs3 : f13', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x374 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmsub.h fa2, fa4, fa1, fa3, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsw fa2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800025b0]:0x00000062




Last Coverpoint : ['rs1 : f12', 'rs2 : f13', 'rd : f11', 'rs3 : f14', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x0e and fm2 == 0x14a and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003f8]:fmsub.h fa1, fa2, fa3, fa4, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw fa1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800025b8]:0x00000062




Last Coverpoint : ['rs1 : f9', 'rs2 : f8', 'rd : f10', 'rs3 : f7', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 0 and fe2 == 0x0e and fm2 == 0x30a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmsub.h fa0, fs1, fs0, ft7, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:fsw fa0, 168(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x800025c0]:0x00000062




Last Coverpoint : ['rs1 : f7', 'rs2 : f10', 'rd : f9', 'rs3 : f8', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x10 and fm2 == 0x0b4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000440]:fmsub.h fs1, ft7, fa0, fs0, dyn
	-[0x80000444]:csrrs tp, fcsr, zero
	-[0x80000448]:fsw fs1, 176(ra)
Current Store : [0x8000044c] : sw tp, 180(ra) -- Store: [0x800025c8]:0x00000062




Last Coverpoint : ['rs1 : f10', 'rs2 : f7', 'rd : f8', 'rs3 : f9', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x2aa and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000464]:fmsub.h fs0, fa0, ft7, fs1, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw fs0, 184(ra)
Current Store : [0x80000470] : sw tp, 188(ra) -- Store: [0x800025d0]:0x00000062




Last Coverpoint : ['rs1 : f8', 'rs2 : f9', 'rd : f7', 'rs3 : f10', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x250 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000488]:fmsub.h ft7, fs0, fs1, fa0, dyn
	-[0x8000048c]:csrrs tp, fcsr, zero
	-[0x80000490]:fsw ft7, 192(ra)
Current Store : [0x80000494] : sw tp, 196(ra) -- Store: [0x800025d8]:0x00000062




Last Coverpoint : ['rs1 : f5', 'rs2 : f4', 'rd : f6', 'rs3 : f3', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x3b5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fmsub.h ft6, ft5, ft4, ft3, dyn
	-[0x800004b0]:csrrs tp, fcsr, zero
	-[0x800004b4]:fsw ft6, 200(ra)
Current Store : [0x800004b8] : sw tp, 204(ra) -- Store: [0x800025e0]:0x00000062




Last Coverpoint : ['rs1 : f3', 'rs2 : f6', 'rd : f5', 'rs3 : f4', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x0f and fm2 == 0x33e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fmsub.h ft5, ft3, ft6, ft4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsw ft5, 208(ra)
Current Store : [0x800004dc] : sw tp, 212(ra) -- Store: [0x800025e8]:0x00000062




Last Coverpoint : ['rs1 : f6', 'rs2 : f3', 'rd : f4', 'rs3 : f5', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 0 and fe2 == 0x10 and fm2 == 0x26f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fmsub.h ft4, ft6, ft3, ft5, dyn
	-[0x800004f8]:csrrs tp, fcsr, zero
	-[0x800004fc]:fsw ft4, 216(ra)
Current Store : [0x80000500] : sw tp, 220(ra) -- Store: [0x800025f0]:0x00000062




Last Coverpoint : ['rs1 : f4', 'rs2 : f5', 'rd : f3', 'rs3 : f6', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x104 and fs2 == 0 and fe2 == 0x13 and fm2 == 0x042 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x157 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000518]:fmsub.h ft3, ft4, ft5, ft6, dyn
	-[0x8000051c]:csrrs tp, fcsr, zero
	-[0x80000520]:fsw ft3, 224(ra)
Current Store : [0x80000524] : sw tp, 228(ra) -- Store: [0x800025f8]:0x00000062




Last Coverpoint : ['rs1 : f2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x21d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x1fe and fs3 == 0 and fe3 == 0x1e and fm3 == 0x094 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fmsub.h ft11, ft2, ft10, ft9, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:fsw ft11, 232(ra)
Current Store : [0x80000548] : sw tp, 236(ra) -- Store: [0x80002600]:0x00000062




Last Coverpoint : ['rs1 : f1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x300 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x1bb and fs3 == 0 and fe3 == 0x1d and fm3 == 0x104 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000560]:fmsub.h ft11, ft1, ft10, ft9, dyn
	-[0x80000564]:csrrs tp, fcsr, zero
	-[0x80000568]:fsw ft11, 240(ra)
Current Store : [0x8000056c] : sw tp, 244(ra) -- Store: [0x80002608]:0x00000062




Last Coverpoint : ['rs1 : f0', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x18f and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2d6 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0c0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000584]:fmsub.h ft11, ft0, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 248(ra)
Current Store : [0x80000590] : sw tp, 252(ra) -- Store: [0x80002610]:0x00000062




Last Coverpoint : ['rs2 : f2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a3 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x24b and fs3 == 0 and fe3 == 0x1d and fm3 == 0x34b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005a8]:fmsub.h ft11, ft10, ft2, ft9, dyn
	-[0x800005ac]:csrrs tp, fcsr, zero
	-[0x800005b0]:fsw ft11, 256(ra)
Current Store : [0x800005b4] : sw tp, 260(ra) -- Store: [0x80002618]:0x00000062




Last Coverpoint : ['rs2 : f1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b7 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x07f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fmsub.h ft11, ft10, ft1, ft9, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsw ft11, 264(ra)
Current Store : [0x800005d8] : sw tp, 268(ra) -- Store: [0x80002620]:0x00000062




Last Coverpoint : ['rs2 : f0', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x394 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x19a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x14f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f0]:fmsub.h ft11, ft10, ft0, ft9, dyn
	-[0x800005f4]:csrrs tp, fcsr, zero
	-[0x800005f8]:fsw ft11, 272(ra)
Current Store : [0x800005fc] : sw tp, 276(ra) -- Store: [0x80002628]:0x00000062




Last Coverpoint : ['rs3 : f2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x12 and fm2 == 0x1c7 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x19b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fmsub.h ft11, ft10, ft9, ft2, dyn
	-[0x80000618]:csrrs tp, fcsr, zero
	-[0x8000061c]:fsw ft11, 280(ra)
Current Store : [0x80000620] : sw tp, 284(ra) -- Store: [0x80002630]:0x00000062




Last Coverpoint : ['rs3 : f1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x12c and fs2 == 0 and fe2 == 0x0f and fm2 == 0x223 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x3f0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000638]:fmsub.h ft11, ft10, ft9, ft1, dyn
	-[0x8000063c]:csrrs tp, fcsr, zero
	-[0x80000640]:fsw ft11, 288(ra)
Current Store : [0x80000644] : sw tp, 292(ra) -- Store: [0x80002638]:0x00000062




Last Coverpoint : ['rs3 : f0', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x292 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x18a and fs3 == 0 and fe3 == 0x1d and fm3 == 0x08d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmsub.h ft11, ft10, ft9, ft0, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:fsw ft11, 296(ra)
Current Store : [0x80000668] : sw tp, 300(ra) -- Store: [0x80002640]:0x00000062




Last Coverpoint : ['rd : f2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x22b and fs2 == 0 and fe2 == 0x0f and fm2 == 0x2ee and fs3 == 0 and fe3 == 0x1e and fm3 == 0x158 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000680]:fmsub.h ft2, ft11, ft10, ft9, dyn
	-[0x80000684]:csrrs tp, fcsr, zero
	-[0x80000688]:fsw ft2, 304(ra)
Current Store : [0x8000068c] : sw tp, 308(ra) -- Store: [0x80002648]:0x00000062




Last Coverpoint : ['rd : f1', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1c6 and fs2 == 0 and fe2 == 0x13 and fm2 == 0x11c and fs3 == 0 and fe3 == 0x1d and fm3 == 0x362 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fmsub.h ft1, ft11, ft10, ft9, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft1, 312(ra)
Current Store : [0x800006b0] : sw tp, 316(ra) -- Store: [0x80002650]:0x00000062




Last Coverpoint : ['rd : f0', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x160 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x193 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x37d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006c8]:fmsub.h ft0, ft11, ft10, ft9, dyn
	-[0x800006cc]:csrrs tp, fcsr, zero
	-[0x800006d0]:fsw ft0, 320(ra)
Current Store : [0x800006d4] : sw tp, 324(ra) -- Store: [0x80002658]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x38b and fs2 == 0 and fe2 == 0x12 and fm2 == 0x283 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x224 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800006f0]:csrrs tp, fcsr, zero
	-[0x800006f4]:fsw ft11, 328(ra)
Current Store : [0x800006f8] : sw tp, 332(ra) -- Store: [0x80002660]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x023 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x3b0 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x3eb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000710]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000714]:csrrs tp, fcsr, zero
	-[0x80000718]:fsw ft11, 336(ra)
Current Store : [0x8000071c] : sw tp, 340(ra) -- Store: [0x80002668]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x240 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x076 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2f7 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000738]:csrrs tp, fcsr, zero
	-[0x8000073c]:fsw ft11, 344(ra)
Current Store : [0x80000740] : sw tp, 348(ra) -- Store: [0x80002670]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x127 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x195 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x32f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000758]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000075c]:csrrs tp, fcsr, zero
	-[0x80000760]:fsw ft11, 352(ra)
Current Store : [0x80000764] : sw tp, 356(ra) -- Store: [0x80002678]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x064 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x270 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x310 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000077c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000780]:csrrs tp, fcsr, zero
	-[0x80000784]:fsw ft11, 360(ra)
Current Store : [0x80000788] : sw tp, 364(ra) -- Store: [0x80002680]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28d and fs2 == 0 and fe2 == 0x0f and fm2 == 0x039 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2e8 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007a0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007a4]:csrrs tp, fcsr, zero
	-[0x800007a8]:fsw ft11, 368(ra)
Current Store : [0x800007ac] : sw tp, 372(ra) -- Store: [0x80002688]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x073 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x1fa and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2a4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007c4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 376(ra)
Current Store : [0x800007d0] : sw tp, 380(ra) -- Store: [0x80002690]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1bc and fs2 == 0 and fe2 == 0x10 and fm2 == 0x0ef and fs3 == 0 and fe3 == 0x1e and fm3 == 0x311 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007e8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007ec]:csrrs tp, fcsr, zero
	-[0x800007f0]:fsw ft11, 384(ra)
Current Store : [0x800007f4] : sw tp, 388(ra) -- Store: [0x80002698]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a8 and fs2 == 0 and fe2 == 0x0c and fm2 == 0x10b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000810]:csrrs tp, fcsr, zero
	-[0x80000814]:fsw ft11, 392(ra)
Current Store : [0x80000818] : sw tp, 396(ra) -- Store: [0x800026a0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1a2 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x143 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x36b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000830]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000834]:csrrs tp, fcsr, zero
	-[0x80000838]:fsw ft11, 400(ra)
Current Store : [0x8000083c] : sw tp, 404(ra) -- Store: [0x800026a8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x376 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x028 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3c1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000858]:csrrs tp, fcsr, zero
	-[0x8000085c]:fsw ft11, 408(ra)
Current Store : [0x80000860] : sw tp, 412(ra) -- Store: [0x800026b0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0b5 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x322 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x033 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000878]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000087c]:csrrs tp, fcsr, zero
	-[0x80000880]:fsw ft11, 416(ra)
Current Store : [0x80000884] : sw tp, 420(ra) -- Store: [0x800026b8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x034 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x25f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2b3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000089c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008a0]:csrrs tp, fcsr, zero
	-[0x800008a4]:fsw ft11, 424(ra)
Current Store : [0x800008a8] : sw tp, 428(ra) -- Store: [0x800026c0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1e7 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x222 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x087 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008c0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008c4]:csrrs tp, fcsr, zero
	-[0x800008c8]:fsw ft11, 432(ra)
Current Store : [0x800008cc] : sw tp, 436(ra) -- Store: [0x800026c8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x39e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x1cb and fs3 == 0 and fe3 == 0x1e and fm3 == 0x185 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008e4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 440(ra)
Current Store : [0x800008f0] : sw tp, 444(ra) -- Store: [0x800026d0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x31f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000908]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000090c]:csrrs tp, fcsr, zero
	-[0x80000910]:fsw ft11, 448(ra)
Current Store : [0x80000914] : sw tp, 452(ra) -- Store: [0x800026d8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x175 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000930]:csrrs tp, fcsr, zero
	-[0x80000934]:fsw ft11, 456(ra)
Current Store : [0x80000938] : sw tp, 460(ra) -- Store: [0x800026e0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x12 and fm2 == 0x1c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000950]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000954]:csrrs tp, fcsr, zero
	-[0x80000958]:fsw ft11, 464(ra)
Current Store : [0x8000095c] : sw tp, 468(ra) -- Store: [0x800026e8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x131 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000978]:csrrs tp, fcsr, zero
	-[0x8000097c]:fsw ft11, 472(ra)
Current Store : [0x80000980] : sw tp, 476(ra) -- Store: [0x800026f0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x0f and fm2 == 0x3ca and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000998]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000099c]:csrrs tp, fcsr, zero
	-[0x800009a0]:fsw ft11, 480(ra)
Current Store : [0x800009a4] : sw tp, 484(ra) -- Store: [0x800026f8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 0 and fe2 == 0x0f and fm2 == 0x336 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009bc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800009c0]:csrrs tp, fcsr, zero
	-[0x800009c4]:fsw ft11, 488(ra)
Current Store : [0x800009c8] : sw tp, 492(ra) -- Store: [0x80002700]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x20a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009e0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800009e4]:csrrs tp, fcsr, zero
	-[0x800009e8]:fsw ft11, 496(ra)
Current Store : [0x800009ec] : sw tp, 500(ra) -- Store: [0x80002708]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 0 and fe2 == 0x0d and fm2 == 0x165 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 504(ra)
Current Store : [0x80000a10] : sw tp, 508(ra) -- Store: [0x80002710]:0x00000062





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                                                                                             coverpoints                                                                                                                                                                                                             |                                                            code                                                             |
|---:|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002514]<br>0xFBB6FAB7<br> |- mnemonic : fmsub.h<br> - rs1 : f31<br> - rs2 : f30<br> - rd : f31<br> - rs3 : f29<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x0d and fm2 == 0x374 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>      |[0x80000128]:fmsub.h ft11, ft11, ft10, ft9, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:fsw ft11, 0(ra)<br>   |
|   2|[0x8000251c]<br>0xDDB7D5BF<br> |- rs1 : f28<br> - rs2 : f28<br> - rd : f28<br> - rs3 : f28<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                                                                                         |[0x8000014c]:fmsub.h ft8, ft8, ft8, ft8, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:fsw ft8, 8(ra)<br>       |
|   3|[0x80002524]<br>0xF76DF56F<br> |- rs1 : f29<br> - rs2 : f29<br> - rd : f30<br> - rs3 : f31<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                                                                                            |[0x80000170]:fmsub.h ft10, ft9, ft9, ft11, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:fsw ft10, 16(ra)<br>   |
|   4|[0x8000252c]<br>0xBB6FAB7F<br> |- rs1 : f30<br> - rs2 : f27<br> - rd : f27<br> - rs3 : f26<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 0 and fe2 == 0x12 and fm2 == 0x05f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                               |[0x80000194]:fmsub.h fs11, ft10, fs11, fs10, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:fsw fs11, 24(ra)<br> |
|   5|[0x80002534]<br>0xEEDBEADF<br> |- rs1 : f27<br> - rs2 : f25<br> - rd : f29<br> - rs3 : f25<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                                                                                            |[0x800001b8]:fmsub.h ft9, fs11, fs9, fs9, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:fsw ft9, 32(ra)<br>     |
|   6|[0x8000253c]<br>0xDB7D5BFD<br> |- rs1 : f24<br> - rs2 : f31<br> - rd : f24<br> - rs3 : f24<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                                                                                         |[0x800001dc]:fmsub.h fs8, fs8, ft11, fs8, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:fsw fs8, 40(ra)<br>     |
|   7|[0x80002544]<br>0x76DF56FF<br> |- rs1 : f23<br> - rs2 : f23<br> - rd : f26<br> - rs3 : f23<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000200]:fmsub.h fs10, fs7, fs7, fs7, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:fsw fs10, 48(ra)<br>    |
|   8|[0x8000254c]<br>0xEDBEADFE<br> |- rs1 : f22<br> - rs2 : f26<br> - rd : f25<br> - rs3 : f22<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                                                                                            |[0x80000224]:fmsub.h fs9, fs6, fs10, fs6, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:fsw fs9, 56(ra)<br>     |
|   9|[0x80002554]<br>0xDBEADFEE<br> |- rs1 : f21<br> - rs2 : f21<br> - rd : f21<br> - rs3 : f30<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                                                                                         |[0x80000248]:fmsub.h fs5, fs5, fs5, ft10, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:fsw fs5, 64(ra)<br>     |
|  10|[0x8000255c]<br>0xB7D5BFDD<br> |- rs1 : f26<br> - rs2 : f24<br> - rd : f20<br> - rs3 : f20<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x17f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                               |[0x8000026c]:fmsub.h fs4, fs10, fs8, fs4, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:fsw fs4, 72(ra)<br>     |
|  11|[0x80002564]<br>0xB6FAB7FB<br> |- rs1 : f25<br> - rs2 : f22<br> - rd : f23<br> - rs3 : f27<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0a and fm2 == 0x21c and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br> |[0x80000290]:fmsub.h fs7, fs9, fs6, fs11, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:fsw fs7, 80(ra)<br>     |
|  12|[0x8000256c]<br>0x6FAB7FBB<br> |- rs1 : f20<br> - rs2 : f19<br> - rd : f19<br> - rs3 : f19<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                                                                                         |[0x800002b4]:fmsub.h fs3, fs4, fs3, fs3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:fsw fs3, 88(ra)<br>      |
|  13|[0x80002574]<br>0x6DF56FF7<br> |- rs1 : f19<br> - rs2 : f20<br> - rd : f22<br> - rs3 : f21<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x288 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800002d8]:fmsub.h fs6, fs3, fs4, fs5, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:fsw fs6, 96(ra)<br>      |
|  14|[0x8000257c]<br>0xDF56FF76<br> |- rs1 : f17<br> - rs2 : f16<br> - rd : f18<br> - rs3 : f15<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x10 and fm2 == 0x14a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800002fc]:fmsub.h fs2, fa7, fa6, fa5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:fsw fs2, 104(ra)<br>     |
|  15|[0x80002584]<br>0xBEADFEED<br> |- rs1 : f15<br> - rs2 : f18<br> - rd : f17<br> - rs3 : f16<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x26f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x80000320]:fmsub.h fa7, fa5, fs2, fa6, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:fsw fa7, 112(ra)<br>     |
|  16|[0x8000258c]<br>0x7D5BFDDB<br> |- rs1 : f18<br> - rs2 : f15<br> - rd : f16<br> - rs3 : f17<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0e and fm2 == 0x0a9 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x80000344]:fmsub.h fa6, fs2, fa5, fa7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:fsw fa6, 120(ra)<br>     |
|  17|[0x80002594]<br>0xFAB7FBB6<br> |- rs1 : f16<br> - rs2 : f17<br> - rd : f15<br> - rs3 : f18<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x356 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x80000368]:fmsub.h fa5, fa6, fa7, fs2, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:fsw fa5, 128(ra)<br>     |
|  18|[0x8000259c]<br>0xF56FF76D<br> |- rs1 : f13<br> - rs2 : f12<br> - rd : f14<br> - rs3 : f11<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x2e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x8000038c]:fmsub.h fa4, fa3, fa2, fa1, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:fsw fa4, 136(ra)<br>     |
|  19|[0x800025a4]<br>0xEADFEEDB<br> |- rs1 : f11<br> - rs2 : f14<br> - rd : f13<br> - rs3 : f12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x107 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800003b0]:fmsub.h fa3, fa1, fa4, fa2, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:fsw fa3, 144(ra)<br>     |
|  20|[0x800025ac]<br>0xD5BFDDB7<br> |- rs1 : f14<br> - rs2 : f11<br> - rd : f12<br> - rs3 : f13<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x374 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800003d4]:fmsub.h fa2, fa4, fa1, fa3, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:fsw fa2, 152(ra)<br>     |
|  21|[0x800025b4]<br>0xAB7FBB6F<br> |- rs1 : f12<br> - rs2 : f13<br> - rd : f11<br> - rs3 : f14<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x0e and fm2 == 0x14a and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800003f8]:fmsub.h fa1, fa2, fa3, fa4, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:fsw fa1, 160(ra)<br>     |
|  22|[0x800025bc]<br>0x00002000<br> |- rs1 : f9<br> - rs2 : f8<br> - rd : f10<br> - rs3 : f7<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 0 and fe2 == 0x0e and fm2 == 0x30a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x8000041c]:fmsub.h fa0, fs1, fs0, ft7, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:fsw fa0, 168(ra)<br>     |
|  23|[0x800025c4]<br>0xADFEEDBE<br> |- rs1 : f7<br> - rs2 : f10<br> - rd : f9<br> - rs3 : f8<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x10 and fm2 == 0x0b4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x80000440]:fmsub.h fs1, ft7, fa0, fs0, dyn<br> [0x80000444]:csrrs tp, fcsr, zero<br> [0x80000448]:fsw fs1, 176(ra)<br>     |
|  24|[0x800025cc]<br>0x5BFDDB7D<br> |- rs1 : f10<br> - rs2 : f7<br> - rd : f8<br> - rs3 : f9<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x2aa and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x80000464]:fmsub.h fs0, fa0, ft7, fs1, dyn<br> [0x80000468]:csrrs tp, fcsr, zero<br> [0x8000046c]:fsw fs0, 184(ra)<br>     |
|  25|[0x800025d4]<br>0xB7FBB6FA<br> |- rs1 : f8<br> - rs2 : f9<br> - rd : f7<br> - rs3 : f10<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x250 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x80000488]:fmsub.h ft7, fs0, fs1, fa0, dyn<br> [0x8000048c]:csrrs tp, fcsr, zero<br> [0x80000490]:fsw ft7, 192(ra)<br>     |
|  26|[0x800025dc]<br>0x80002000<br> |- rs1 : f5<br> - rs2 : f4<br> - rd : f6<br> - rs3 : f3<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x3b5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800004ac]:fmsub.h ft6, ft5, ft4, ft3, dyn<br> [0x800004b0]:csrrs tp, fcsr, zero<br> [0x800004b4]:fsw ft6, 200(ra)<br>     |
|  27|[0x800025e4]<br>0x800000F8<br> |- rs1 : f3<br> - rs2 : f6<br> - rd : f5<br> - rs3 : f4<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x0f and fm2 == 0x33e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800004d0]:fmsub.h ft5, ft3, ft6, ft4, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:fsw ft5, 208(ra)<br>     |
|  28|[0x800025ec]<br>0x00000062<br> |- rs1 : f6<br> - rs2 : f3<br> - rd : f4<br> - rs3 : f5<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 0 and fe2 == 0x10 and fm2 == 0x26f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800004f4]:fmsub.h ft4, ft6, ft3, ft5, dyn<br> [0x800004f8]:csrrs tp, fcsr, zero<br> [0x800004fc]:fsw ft4, 216(ra)<br>     |
|  29|[0x800025f4]<br>0x80002010<br> |- rs1 : f4<br> - rs2 : f5<br> - rd : f3<br> - rs3 : f6<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x104 and fs2 == 0 and fe2 == 0x13 and fm2 == 0x042 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x157 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000518]:fmsub.h ft3, ft4, ft5, ft6, dyn<br> [0x8000051c]:csrrs tp, fcsr, zero<br> [0x80000520]:fsw ft3, 224(ra)<br>     |
|  30|[0x800025fc]<br>0xFBB6FAB7<br> |- rs1 : f2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x21d and fs2 == 0 and fe2 == 0x0e and fm2 == 0x1fe and fs3 == 0 and fe3 == 0x1e and fm3 == 0x094 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x8000053c]:fmsub.h ft11, ft2, ft10, ft9, dyn<br> [0x80000540]:csrrs tp, fcsr, zero<br> [0x80000544]:fsw ft11, 232(ra)<br>  |
|  31|[0x80002604]<br>0xFBB6FAB7<br> |- rs1 : f1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x300 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x1bb and fs3 == 0 and fe3 == 0x1d and fm3 == 0x104 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000560]:fmsub.h ft11, ft1, ft10, ft9, dyn<br> [0x80000564]:csrrs tp, fcsr, zero<br> [0x80000568]:fsw ft11, 240(ra)<br>  |
|  32|[0x8000260c]<br>0xFBB6FAB7<br> |- rs1 : f0<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x18f and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2d6 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0c0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000584]:fmsub.h ft11, ft0, ft10, ft9, dyn<br> [0x80000588]:csrrs tp, fcsr, zero<br> [0x8000058c]:fsw ft11, 248(ra)<br>  |
|  33|[0x80002614]<br>0xFBB6FAB7<br> |- rs2 : f2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a3 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x24b and fs3 == 0 and fe3 == 0x1d and fm3 == 0x34b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x800005a8]:fmsub.h ft11, ft10, ft2, ft9, dyn<br> [0x800005ac]:csrrs tp, fcsr, zero<br> [0x800005b0]:fsw ft11, 256(ra)<br>  |
|  34|[0x8000261c]<br>0xFBB6FAB7<br> |- rs2 : f1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b7 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x07f and fs3 == 0 and fe3 == 0x1e and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x800005cc]:fmsub.h ft11, ft10, ft1, ft9, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:fsw ft11, 264(ra)<br>  |
|  35|[0x80002624]<br>0xFBB6FAB7<br> |- rs2 : f0<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x394 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x19a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x14f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x800005f0]:fmsub.h ft11, ft10, ft0, ft9, dyn<br> [0x800005f4]:csrrs tp, fcsr, zero<br> [0x800005f8]:fsw ft11, 272(ra)<br>  |
|  36|[0x8000262c]<br>0xFBB6FAB7<br> |- rs3 : f2<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x12 and fm2 == 0x1c7 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x19b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000614]:fmsub.h ft11, ft10, ft9, ft2, dyn<br> [0x80000618]:csrrs tp, fcsr, zero<br> [0x8000061c]:fsw ft11, 280(ra)<br>  |
|  37|[0x80002634]<br>0xFBB6FAB7<br> |- rs3 : f1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x12c and fs2 == 0 and fe2 == 0x0f and fm2 == 0x223 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x3f0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000638]:fmsub.h ft11, ft10, ft9, ft1, dyn<br> [0x8000063c]:csrrs tp, fcsr, zero<br> [0x80000640]:fsw ft11, 288(ra)<br>  |
|  38|[0x8000263c]<br>0xFBB6FAB7<br> |- rs3 : f0<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x292 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x18a and fs3 == 0 and fe3 == 0x1d and fm3 == 0x08d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x8000065c]:fmsub.h ft11, ft10, ft9, ft0, dyn<br> [0x80000660]:csrrs tp, fcsr, zero<br> [0x80000664]:fsw ft11, 296(ra)<br>  |
|  39|[0x80002644]<br>0x00000062<br> |- rd : f2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x22b and fs2 == 0 and fe2 == 0x0f and fm2 == 0x2ee and fs3 == 0 and fe3 == 0x1e and fm3 == 0x158 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                           |[0x80000680]:fmsub.h ft2, ft11, ft10, ft9, dyn<br> [0x80000684]:csrrs tp, fcsr, zero<br> [0x80000688]:fsw ft2, 304(ra)<br>   |
|  40|[0x8000264c]<br>0x80002514<br> |- rd : f1<br> - fs1 == 0 and fe1 == 0x19 and fm1 == 0x1c6 and fs2 == 0 and fe2 == 0x13 and fm2 == 0x11c and fs3 == 0 and fe3 == 0x1d and fm3 == 0x362 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                           |[0x800006a4]:fmsub.h ft1, ft11, ft10, ft9, dyn<br> [0x800006a8]:csrrs tp, fcsr, zero<br> [0x800006ac]:fsw ft1, 312(ra)<br>   |
|  41|[0x80002654]<br>0x00000000<br> |- rd : f0<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x160 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x193 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x37d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                           |[0x800006c8]:fmsub.h ft0, ft11, ft10, ft9, dyn<br> [0x800006cc]:csrrs tp, fcsr, zero<br> [0x800006d0]:fsw ft0, 320(ra)<br>   |
|  42|[0x8000265c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x38b and fs2 == 0 and fe2 == 0x12 and fm2 == 0x283 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x224 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800006ec]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800006f0]:csrrs tp, fcsr, zero<br> [0x800006f4]:fsw ft11, 328(ra)<br>  |
|  43|[0x80002664]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x023 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x3b0 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x3eb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000710]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000714]:csrrs tp, fcsr, zero<br> [0x80000718]:fsw ft11, 336(ra)<br>  |
|  44|[0x8000266c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x240 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x076 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2f7 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000734]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000738]:csrrs tp, fcsr, zero<br> [0x8000073c]:fsw ft11, 344(ra)<br>  |
|  45|[0x80002674]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x127 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x195 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x32f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000758]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000075c]:csrrs tp, fcsr, zero<br> [0x80000760]:fsw ft11, 352(ra)<br>  |
|  46|[0x8000267c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x064 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x270 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x310 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000077c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000780]:csrrs tp, fcsr, zero<br> [0x80000784]:fsw ft11, 360(ra)<br>  |
|  47|[0x80002684]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28d and fs2 == 0 and fe2 == 0x0f and fm2 == 0x039 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2e8 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800007a0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007a4]:csrrs tp, fcsr, zero<br> [0x800007a8]:fsw ft11, 368(ra)<br>  |
|  48|[0x8000268c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x073 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x1fa and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2a4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800007c4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs tp, fcsr, zero<br> [0x800007cc]:fsw ft11, 376(ra)<br>  |
|  49|[0x80002694]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1bc and fs2 == 0 and fe2 == 0x10 and fm2 == 0x0ef and fs3 == 0 and fe3 == 0x1e and fm3 == 0x311 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800007e8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007ec]:csrrs tp, fcsr, zero<br> [0x800007f0]:fsw ft11, 384(ra)<br>  |
|  50|[0x8000269c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a8 and fs2 == 0 and fe2 == 0x0c and fm2 == 0x10b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000080c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000810]:csrrs tp, fcsr, zero<br> [0x80000814]:fsw ft11, 392(ra)<br>  |
|  51|[0x800026a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1a2 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x143 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x36b and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000830]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000834]:csrrs tp, fcsr, zero<br> [0x80000838]:fsw ft11, 400(ra)<br>  |
|  52|[0x800026ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x376 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x028 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3c1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000854]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000858]:csrrs tp, fcsr, zero<br> [0x8000085c]:fsw ft11, 408(ra)<br>  |
|  53|[0x800026b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0b5 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x322 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x033 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000878]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000087c]:csrrs tp, fcsr, zero<br> [0x80000880]:fsw ft11, 416(ra)<br>  |
|  54|[0x800026bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x034 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x25f and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2b3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000089c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008a0]:csrrs tp, fcsr, zero<br> [0x800008a4]:fsw ft11, 424(ra)<br>  |
|  55|[0x800026c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1e7 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x222 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x087 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800008c0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008c4]:csrrs tp, fcsr, zero<br> [0x800008c8]:fsw ft11, 432(ra)<br>  |
|  56|[0x800026cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x39e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x1cb and fs3 == 0 and fe3 == 0x1e and fm3 == 0x185 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800008e4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs tp, fcsr, zero<br> [0x800008ec]:fsw ft11, 440(ra)<br>  |
|  57|[0x800026d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x31f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000908]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000090c]:csrrs tp, fcsr, zero<br> [0x80000910]:fsw ft11, 448(ra)<br>  |
|  58|[0x800026dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x175 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000092c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000930]:csrrs tp, fcsr, zero<br> [0x80000934]:fsw ft11, 456(ra)<br>  |
|  59|[0x800026e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x12 and fm2 == 0x1c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000950]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000954]:csrrs tp, fcsr, zero<br> [0x80000958]:fsw ft11, 464(ra)<br>  |
|  60|[0x800026ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x131 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000974]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000978]:csrrs tp, fcsr, zero<br> [0x8000097c]:fsw ft11, 472(ra)<br>  |
|  61|[0x800026f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x0f and fm2 == 0x3ca and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000998]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000099c]:csrrs tp, fcsr, zero<br> [0x800009a0]:fsw ft11, 480(ra)<br>  |
|  62|[0x800026fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 0 and fe2 == 0x0f and fm2 == 0x336 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800009bc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800009c0]:csrrs tp, fcsr, zero<br> [0x800009c4]:fsw ft11, 488(ra)<br>  |
|  63|[0x80002704]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x20a and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800009e0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800009e4]:csrrs tp, fcsr, zero<br> [0x800009e8]:fsw ft11, 496(ra)<br>  |
|  64|[0x8000270c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 0 and fe2 == 0x0d and fm2 == 0x165 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000a04]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs tp, fcsr, zero<br> [0x80000a0c]:fsw ft11, 504(ra)<br>  |
