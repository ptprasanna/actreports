
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800015f0')]      |
| SIG_REGION                | [('0x80003810', '0x80003cc0', '300 words')]      |
| COV_LABELS                | fmsub_b4      |
| TEST_NAME                 | /home/riscv/riscv-ctg/FMA/work-fmsub/fmsub_b4-01.S/ref.S    |
| Total Number of coverpoints| 281     |
| Total Coverpoints Hit     | 281      |
| Total Signature Updates   | 296      |
| STAT1                     | 148      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 148     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmsub.h', 'rs1 : f31', 'rs2 : f30', 'rd : f31', 'rs3 : f29', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000128]:fmsub.h ft11, ft11, ft10, ft9, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:fsw ft11, 0(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80003818]:0x00000003




Last Coverpoint : ['rs1 : f28', 'rs2 : f28', 'rd : f28', 'rs3 : f28', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x8000014c]:fmsub.h ft8, ft8, ft8, ft8, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:fsw ft8, 8(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80003820]:0x00000027




Last Coverpoint : ['rs1 : f29', 'rs2 : f29', 'rd : f30', 'rs3 : f31', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fmsub.h ft10, ft9, ft9, ft11, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:fsw ft10, 16(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80003828]:0x00000047




Last Coverpoint : ['rs1 : f30', 'rs2 : f27', 'rd : f27', 'rs3 : f26', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000194]:fmsub.h fs11, ft10, fs11, fs10, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:fsw fs11, 24(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80003830]:0x00000063




Last Coverpoint : ['rs1 : f27', 'rs2 : f25', 'rd : f29', 'rs3 : f25', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x800001b8]:fmsub.h ft9, fs11, fs9, fs9, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:fsw ft9, 32(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80003838]:0x00000087




Last Coverpoint : ['rs1 : f24', 'rs2 : f31', 'rd : f24', 'rs3 : f24', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x800001dc]:fmsub.h fs8, fs8, ft11, fs8, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:fsw fs8, 40(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80003840]:0x00000007




Last Coverpoint : ['rs1 : f23', 'rs2 : f23', 'rd : f26', 'rs3 : f23', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x80000200]:fmsub.h fs10, fs7, fs7, fs7, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs10, 48(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80003848]:0x00000027




Last Coverpoint : ['rs1 : f22', 'rs2 : f26', 'rd : f25', 'rs3 : f22', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000224]:fmsub.h fs9, fs6, fs10, fs6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs9, 56(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80003850]:0x00000047




Last Coverpoint : ['rs1 : f21', 'rs2 : f21', 'rd : f21', 'rs3 : f30', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000248]:fmsub.h fs5, fs5, fs5, ft10, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:fsw fs5, 64(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80003858]:0x00000067




Last Coverpoint : ['rs1 : f26', 'rs2 : f24', 'rd : f20', 'rs3 : f20', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fmsub.h fs4, fs10, fs8, fs4, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:fsw fs4, 72(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80003860]:0x00000083




Last Coverpoint : ['rs1 : f25', 'rs2 : f22', 'rd : f23', 'rs3 : f27', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000290]:fmsub.h fs7, fs9, fs6, fs11, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:fsw fs7, 80(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80003868]:0x00000003




Last Coverpoint : ['rs1 : f20', 'rs2 : f19', 'rd : f19', 'rs3 : f19', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x800002b4]:fmsub.h fs3, fs4, fs3, fs3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:fsw fs3, 88(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80003870]:0x00000027




Last Coverpoint : ['rs1 : f19', 'rs2 : f20', 'rd : f22', 'rs3 : f21', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fmsub.h fs6, fs3, fs4, fs5, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:fsw fs6, 96(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80003878]:0x00000043




Last Coverpoint : ['rs1 : f17', 'rs2 : f16', 'rd : f18', 'rs3 : f15', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmsub.h fs2, fa7, fa6, fa5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fs2, 104(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80003880]:0x00000063




Last Coverpoint : ['rs1 : f15', 'rs2 : f18', 'rd : f17', 'rs3 : f16', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000320]:fmsub.h fa7, fa5, fs2, fa6, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:fsw fa7, 112(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80003888]:0x00000083




Last Coverpoint : ['rs1 : f18', 'rs2 : f15', 'rd : f16', 'rs3 : f17', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fmsub.h fa6, fs2, fa5, fa7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa6, 120(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80003890]:0x00000003




Last Coverpoint : ['rs1 : f16', 'rs2 : f17', 'rd : f15', 'rs3 : f18', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000368]:fmsub.h fa5, fa6, fa7, fs2, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:fsw fa5, 128(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80003898]:0x00000023




Last Coverpoint : ['rs1 : f13', 'rs2 : f12', 'rd : f14', 'rs3 : f11', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fmsub.h fa4, fa3, fa2, fa1, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:fsw fa4, 136(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x800038a0]:0x00000043




Last Coverpoint : ['rs1 : f11', 'rs2 : f14', 'rd : f13', 'rs3 : f12', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003b0]:fmsub.h fa3, fa1, fa4, fa2, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:fsw fa3, 144(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800038a8]:0x00000063




Last Coverpoint : ['rs1 : f14', 'rs2 : f11', 'rd : f12', 'rs3 : f13', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fmsub.h fa2, fa4, fa1, fa3, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsw fa2, 152(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800038b0]:0x00000083




Last Coverpoint : ['rs1 : f12', 'rs2 : f13', 'rd : f11', 'rs3 : f14', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003f8]:fmsub.h fa1, fa2, fa3, fa4, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw fa1, 160(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800038b8]:0x00000003




Last Coverpoint : ['rs1 : f9', 'rs2 : f8', 'rd : f10', 'rs3 : f7', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmsub.h fa0, fs1, fs0, ft7, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:fsw fa0, 168(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x800038c0]:0x00000023




Last Coverpoint : ['rs1 : f7', 'rs2 : f10', 'rd : f9', 'rs3 : f8', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000440]:fmsub.h fs1, ft7, fa0, fs0, dyn
	-[0x80000444]:csrrs tp, fcsr, zero
	-[0x80000448]:fsw fs1, 176(ra)
Current Store : [0x8000044c] : sw tp, 180(ra) -- Store: [0x800038c8]:0x00000043




Last Coverpoint : ['rs1 : f10', 'rs2 : f7', 'rd : f8', 'rs3 : f9', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000464]:fmsub.h fs0, fa0, ft7, fs1, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw fs0, 184(ra)
Current Store : [0x80000470] : sw tp, 188(ra) -- Store: [0x800038d0]:0x00000063




Last Coverpoint : ['rs1 : f8', 'rs2 : f9', 'rd : f7', 'rs3 : f10', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000488]:fmsub.h ft7, fs0, fs1, fa0, dyn
	-[0x8000048c]:csrrs tp, fcsr, zero
	-[0x80000490]:fsw ft7, 192(ra)
Current Store : [0x80000494] : sw tp, 196(ra) -- Store: [0x800038d8]:0x00000083




Last Coverpoint : ['rs1 : f5', 'rs2 : f4', 'rd : f6', 'rs3 : f3', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fmsub.h ft6, ft5, ft4, ft3, dyn
	-[0x800004b0]:csrrs tp, fcsr, zero
	-[0x800004b4]:fsw ft6, 200(ra)
Current Store : [0x800004b8] : sw tp, 204(ra) -- Store: [0x800038e0]:0x00000003




Last Coverpoint : ['rs1 : f3', 'rs2 : f6', 'rd : f5', 'rs3 : f4', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fmsub.h ft5, ft3, ft6, ft4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsw ft5, 208(ra)
Current Store : [0x800004dc] : sw tp, 212(ra) -- Store: [0x800038e8]:0x00000023




Last Coverpoint : ['rs1 : f6', 'rs2 : f3', 'rd : f4', 'rs3 : f5', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fmsub.h ft4, ft6, ft3, ft5, dyn
	-[0x800004f8]:csrrs tp, fcsr, zero
	-[0x800004fc]:fsw ft4, 216(ra)
Current Store : [0x80000500] : sw tp, 220(ra) -- Store: [0x800038f0]:0x00000043




Last Coverpoint : ['rs1 : f4', 'rs2 : f5', 'rd : f3', 'rs3 : f6', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000518]:fmsub.h ft3, ft4, ft5, ft6, dyn
	-[0x8000051c]:csrrs tp, fcsr, zero
	-[0x80000520]:fsw ft3, 224(ra)
Current Store : [0x80000524] : sw tp, 228(ra) -- Store: [0x800038f8]:0x00000063




Last Coverpoint : ['rs1 : f2', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fmsub.h ft11, ft2, ft10, ft9, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:fsw ft11, 232(ra)
Current Store : [0x80000548] : sw tp, 236(ra) -- Store: [0x80003900]:0x00000083




Last Coverpoint : ['rs1 : f1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000560]:fmsub.h ft11, ft1, ft10, ft9, dyn
	-[0x80000564]:csrrs tp, fcsr, zero
	-[0x80000568]:fsw ft11, 240(ra)
Current Store : [0x8000056c] : sw tp, 244(ra) -- Store: [0x80003908]:0x00000003




Last Coverpoint : ['rs1 : f0', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000584]:fmsub.h ft11, ft0, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 248(ra)
Current Store : [0x80000590] : sw tp, 252(ra) -- Store: [0x80003910]:0x00000023




Last Coverpoint : ['rs2 : f2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005a8]:fmsub.h ft11, ft10, ft2, ft9, dyn
	-[0x800005ac]:csrrs tp, fcsr, zero
	-[0x800005b0]:fsw ft11, 256(ra)
Current Store : [0x800005b4] : sw tp, 260(ra) -- Store: [0x80003918]:0x00000043




Last Coverpoint : ['rs2 : f1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fmsub.h ft11, ft10, ft1, ft9, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsw ft11, 264(ra)
Current Store : [0x800005d8] : sw tp, 268(ra) -- Store: [0x80003920]:0x00000063




Last Coverpoint : ['rs2 : f0', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f0]:fmsub.h ft11, ft10, ft0, ft9, dyn
	-[0x800005f4]:csrrs tp, fcsr, zero
	-[0x800005f8]:fsw ft11, 272(ra)
Current Store : [0x800005fc] : sw tp, 276(ra) -- Store: [0x80003928]:0x00000083




Last Coverpoint : ['rs3 : f2', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fmsub.h ft11, ft10, ft9, ft2, dyn
	-[0x80000618]:csrrs tp, fcsr, zero
	-[0x8000061c]:fsw ft11, 280(ra)
Current Store : [0x80000620] : sw tp, 284(ra) -- Store: [0x80003930]:0x00000003




Last Coverpoint : ['rs3 : f1', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000638]:fmsub.h ft11, ft10, ft9, ft1, dyn
	-[0x8000063c]:csrrs tp, fcsr, zero
	-[0x80000640]:fsw ft11, 288(ra)
Current Store : [0x80000644] : sw tp, 292(ra) -- Store: [0x80003938]:0x00000023




Last Coverpoint : ['rs3 : f0', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmsub.h ft11, ft10, ft9, ft0, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:fsw ft11, 296(ra)
Current Store : [0x80000668] : sw tp, 300(ra) -- Store: [0x80003940]:0x00000043




Last Coverpoint : ['rd : f2', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000680]:fmsub.h ft2, ft11, ft10, ft9, dyn
	-[0x80000684]:csrrs tp, fcsr, zero
	-[0x80000688]:fsw ft2, 304(ra)
Current Store : [0x8000068c] : sw tp, 308(ra) -- Store: [0x80003948]:0x00000063




Last Coverpoint : ['rd : f1', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fmsub.h ft1, ft11, ft10, ft9, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft1, 312(ra)
Current Store : [0x800006b0] : sw tp, 316(ra) -- Store: [0x80003950]:0x00000083




Last Coverpoint : ['rd : f0', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006c8]:fmsub.h ft0, ft11, ft10, ft9, dyn
	-[0x800006cc]:csrrs tp, fcsr, zero
	-[0x800006d0]:fsw ft0, 320(ra)
Current Store : [0x800006d4] : sw tp, 324(ra) -- Store: [0x80003958]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800006f0]:csrrs tp, fcsr, zero
	-[0x800006f4]:fsw ft11, 328(ra)
Current Store : [0x800006f8] : sw tp, 332(ra) -- Store: [0x80003960]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000710]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000714]:csrrs tp, fcsr, zero
	-[0x80000718]:fsw ft11, 336(ra)
Current Store : [0x8000071c] : sw tp, 340(ra) -- Store: [0x80003968]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000738]:csrrs tp, fcsr, zero
	-[0x8000073c]:fsw ft11, 344(ra)
Current Store : [0x80000740] : sw tp, 348(ra) -- Store: [0x80003970]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000758]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000075c]:csrrs tp, fcsr, zero
	-[0x80000760]:fsw ft11, 352(ra)
Current Store : [0x80000764] : sw tp, 356(ra) -- Store: [0x80003978]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000077c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000780]:csrrs tp, fcsr, zero
	-[0x80000784]:fsw ft11, 360(ra)
Current Store : [0x80000788] : sw tp, 364(ra) -- Store: [0x80003980]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007a0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007a4]:csrrs tp, fcsr, zero
	-[0x800007a8]:fsw ft11, 368(ra)
Current Store : [0x800007ac] : sw tp, 372(ra) -- Store: [0x80003988]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007c4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 376(ra)
Current Store : [0x800007d0] : sw tp, 380(ra) -- Store: [0x80003990]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007e8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800007ec]:csrrs tp, fcsr, zero
	-[0x800007f0]:fsw ft11, 384(ra)
Current Store : [0x800007f4] : sw tp, 388(ra) -- Store: [0x80003998]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000810]:csrrs tp, fcsr, zero
	-[0x80000814]:fsw ft11, 392(ra)
Current Store : [0x80000818] : sw tp, 396(ra) -- Store: [0x800039a0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000830]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000834]:csrrs tp, fcsr, zero
	-[0x80000838]:fsw ft11, 400(ra)
Current Store : [0x8000083c] : sw tp, 404(ra) -- Store: [0x800039a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000858]:csrrs tp, fcsr, zero
	-[0x8000085c]:fsw ft11, 408(ra)
Current Store : [0x80000860] : sw tp, 412(ra) -- Store: [0x800039b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000878]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000087c]:csrrs tp, fcsr, zero
	-[0x80000880]:fsw ft11, 416(ra)
Current Store : [0x80000884] : sw tp, 420(ra) -- Store: [0x800039b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000089c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008a0]:csrrs tp, fcsr, zero
	-[0x800008a4]:fsw ft11, 424(ra)
Current Store : [0x800008a8] : sw tp, 428(ra) -- Store: [0x800039c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008c0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008c4]:csrrs tp, fcsr, zero
	-[0x800008c8]:fsw ft11, 432(ra)
Current Store : [0x800008cc] : sw tp, 436(ra) -- Store: [0x800039c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008e4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 440(ra)
Current Store : [0x800008f0] : sw tp, 444(ra) -- Store: [0x800039d0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000908]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000090c]:csrrs tp, fcsr, zero
	-[0x80000910]:fsw ft11, 448(ra)
Current Store : [0x80000914] : sw tp, 452(ra) -- Store: [0x800039d8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000930]:csrrs tp, fcsr, zero
	-[0x80000934]:fsw ft11, 456(ra)
Current Store : [0x80000938] : sw tp, 460(ra) -- Store: [0x800039e0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000950]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000954]:csrrs tp, fcsr, zero
	-[0x80000958]:fsw ft11, 464(ra)
Current Store : [0x8000095c] : sw tp, 468(ra) -- Store: [0x800039e8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000978]:csrrs tp, fcsr, zero
	-[0x8000097c]:fsw ft11, 472(ra)
Current Store : [0x80000980] : sw tp, 476(ra) -- Store: [0x800039f0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000998]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000099c]:csrrs tp, fcsr, zero
	-[0x800009a0]:fsw ft11, 480(ra)
Current Store : [0x800009a4] : sw tp, 484(ra) -- Store: [0x800039f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009bc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800009c0]:csrrs tp, fcsr, zero
	-[0x800009c4]:fsw ft11, 488(ra)
Current Store : [0x800009c8] : sw tp, 492(ra) -- Store: [0x80003a00]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009e0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800009e4]:csrrs tp, fcsr, zero
	-[0x800009e8]:fsw ft11, 496(ra)
Current Store : [0x800009ec] : sw tp, 500(ra) -- Store: [0x80003a08]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 504(ra)
Current Store : [0x80000a10] : sw tp, 508(ra) -- Store: [0x80003a10]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a28]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a2c]:csrrs tp, fcsr, zero
	-[0x80000a30]:fsw ft11, 512(ra)
Current Store : [0x80000a34] : sw tp, 516(ra) -- Store: [0x80003a18]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a50]:csrrs tp, fcsr, zero
	-[0x80000a54]:fsw ft11, 520(ra)
Current Store : [0x80000a58] : sw tp, 524(ra) -- Store: [0x80003a20]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a70]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a74]:csrrs tp, fcsr, zero
	-[0x80000a78]:fsw ft11, 528(ra)
Current Store : [0x80000a7c] : sw tp, 532(ra) -- Store: [0x80003a28]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000a98]:csrrs tp, fcsr, zero
	-[0x80000a9c]:fsw ft11, 536(ra)
Current Store : [0x80000aa0] : sw tp, 540(ra) -- Store: [0x80003a30]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000abc]:csrrs tp, fcsr, zero
	-[0x80000ac0]:fsw ft11, 544(ra)
Current Store : [0x80000ac4] : sw tp, 548(ra) -- Store: [0x80003a38]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000adc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000ae0]:csrrs tp, fcsr, zero
	-[0x80000ae4]:fsw ft11, 552(ra)
Current Store : [0x80000ae8] : sw tp, 556(ra) -- Store: [0x80003a40]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b00]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000b04]:csrrs tp, fcsr, zero
	-[0x80000b08]:fsw ft11, 560(ra)
Current Store : [0x80000b0c] : sw tp, 564(ra) -- Store: [0x80003a48]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b24]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000b28]:csrrs tp, fcsr, zero
	-[0x80000b2c]:fsw ft11, 568(ra)
Current Store : [0x80000b30] : sw tp, 572(ra) -- Store: [0x80003a50]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b48]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000b4c]:csrrs tp, fcsr, zero
	-[0x80000b50]:fsw ft11, 576(ra)
Current Store : [0x80000b54] : sw tp, 580(ra) -- Store: [0x80003a58]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000b70]:csrrs tp, fcsr, zero
	-[0x80000b74]:fsw ft11, 584(ra)
Current Store : [0x80000b78] : sw tp, 588(ra) -- Store: [0x80003a60]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b90]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000b94]:csrrs tp, fcsr, zero
	-[0x80000b98]:fsw ft11, 592(ra)
Current Store : [0x80000b9c] : sw tp, 596(ra) -- Store: [0x80003a68]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000bb8]:csrrs tp, fcsr, zero
	-[0x80000bbc]:fsw ft11, 600(ra)
Current Store : [0x80000bc0] : sw tp, 604(ra) -- Store: [0x80003a70]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000bdc]:csrrs tp, fcsr, zero
	-[0x80000be0]:fsw ft11, 608(ra)
Current Store : [0x80000be4] : sw tp, 612(ra) -- Store: [0x80003a78]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000c00]:csrrs tp, fcsr, zero
	-[0x80000c04]:fsw ft11, 616(ra)
Current Store : [0x80000c08] : sw tp, 620(ra) -- Store: [0x80003a80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c20]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000c24]:csrrs tp, fcsr, zero
	-[0x80000c28]:fsw ft11, 624(ra)
Current Store : [0x80000c2c] : sw tp, 628(ra) -- Store: [0x80003a88]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c44]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000c48]:csrrs tp, fcsr, zero
	-[0x80000c4c]:fsw ft11, 632(ra)
Current Store : [0x80000c50] : sw tp, 636(ra) -- Store: [0x80003a90]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c68]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000c6c]:csrrs tp, fcsr, zero
	-[0x80000c70]:fsw ft11, 640(ra)
Current Store : [0x80000c74] : sw tp, 644(ra) -- Store: [0x80003a98]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000c90]:csrrs tp, fcsr, zero
	-[0x80000c94]:fsw ft11, 648(ra)
Current Store : [0x80000c98] : sw tp, 652(ra) -- Store: [0x80003aa0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000cb4]:csrrs tp, fcsr, zero
	-[0x80000cb8]:fsw ft11, 656(ra)
Current Store : [0x80000cbc] : sw tp, 660(ra) -- Store: [0x80003aa8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000cd8]:csrrs tp, fcsr, zero
	-[0x80000cdc]:fsw ft11, 664(ra)
Current Store : [0x80000ce0] : sw tp, 668(ra) -- Store: [0x80003ab0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000cfc]:csrrs tp, fcsr, zero
	-[0x80000d00]:fsw ft11, 672(ra)
Current Store : [0x80000d04] : sw tp, 676(ra) -- Store: [0x80003ab8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d1c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000d20]:csrrs tp, fcsr, zero
	-[0x80000d24]:fsw ft11, 680(ra)
Current Store : [0x80000d28] : sw tp, 684(ra) -- Store: [0x80003ac0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d40]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000d44]:csrrs tp, fcsr, zero
	-[0x80000d48]:fsw ft11, 688(ra)
Current Store : [0x80000d4c] : sw tp, 692(ra) -- Store: [0x80003ac8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d64]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000d68]:csrrs tp, fcsr, zero
	-[0x80000d6c]:fsw ft11, 696(ra)
Current Store : [0x80000d70] : sw tp, 700(ra) -- Store: [0x80003ad0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d88]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000d8c]:csrrs tp, fcsr, zero
	-[0x80000d90]:fsw ft11, 704(ra)
Current Store : [0x80000d94] : sw tp, 708(ra) -- Store: [0x80003ad8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000db0]:csrrs tp, fcsr, zero
	-[0x80000db4]:fsw ft11, 712(ra)
Current Store : [0x80000db8] : sw tp, 716(ra) -- Store: [0x80003ae0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000dd4]:csrrs tp, fcsr, zero
	-[0x80000dd8]:fsw ft11, 720(ra)
Current Store : [0x80000ddc] : sw tp, 724(ra) -- Store: [0x80003ae8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000df8]:csrrs tp, fcsr, zero
	-[0x80000dfc]:fsw ft11, 728(ra)
Current Store : [0x80000e00] : sw tp, 732(ra) -- Store: [0x80003af0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e18]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000e1c]:csrrs tp, fcsr, zero
	-[0x80000e20]:fsw ft11, 736(ra)
Current Store : [0x80000e24] : sw tp, 740(ra) -- Store: [0x80003af8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e3c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000e40]:csrrs tp, fcsr, zero
	-[0x80000e44]:fsw ft11, 744(ra)
Current Store : [0x80000e48] : sw tp, 748(ra) -- Store: [0x80003b00]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e60]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000e64]:csrrs tp, fcsr, zero
	-[0x80000e68]:fsw ft11, 752(ra)
Current Store : [0x80000e6c] : sw tp, 756(ra) -- Store: [0x80003b08]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e84]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000e88]:csrrs tp, fcsr, zero
	-[0x80000e8c]:fsw ft11, 760(ra)
Current Store : [0x80000e90] : sw tp, 764(ra) -- Store: [0x80003b10]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000eac]:csrrs tp, fcsr, zero
	-[0x80000eb0]:fsw ft11, 768(ra)
Current Store : [0x80000eb4] : sw tp, 772(ra) -- Store: [0x80003b18]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000ed0]:csrrs tp, fcsr, zero
	-[0x80000ed4]:fsw ft11, 776(ra)
Current Store : [0x80000ed8] : sw tp, 780(ra) -- Store: [0x80003b20]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000ef4]:csrrs tp, fcsr, zero
	-[0x80000ef8]:fsw ft11, 784(ra)
Current Store : [0x80000efc] : sw tp, 788(ra) -- Store: [0x80003b28]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f14]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000f18]:csrrs tp, fcsr, zero
	-[0x80000f1c]:fsw ft11, 792(ra)
Current Store : [0x80000f20] : sw tp, 796(ra) -- Store: [0x80003b30]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f38]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000f3c]:csrrs tp, fcsr, zero
	-[0x80000f40]:fsw ft11, 800(ra)
Current Store : [0x80000f44] : sw tp, 804(ra) -- Store: [0x80003b38]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f5c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000f60]:csrrs tp, fcsr, zero
	-[0x80000f64]:fsw ft11, 808(ra)
Current Store : [0x80000f68] : sw tp, 812(ra) -- Store: [0x80003b40]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f80]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000f84]:csrrs tp, fcsr, zero
	-[0x80000f88]:fsw ft11, 816(ra)
Current Store : [0x80000f8c] : sw tp, 820(ra) -- Store: [0x80003b48]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fa4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000fa8]:csrrs tp, fcsr, zero
	-[0x80000fac]:fsw ft11, 824(ra)
Current Store : [0x80000fb0] : sw tp, 828(ra) -- Store: [0x80003b50]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fc8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000fcc]:csrrs tp, fcsr, zero
	-[0x80000fd0]:fsw ft11, 832(ra)
Current Store : [0x80000fd4] : sw tp, 836(ra) -- Store: [0x80003b58]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80000ff0]:csrrs tp, fcsr, zero
	-[0x80000ff4]:fsw ft11, 840(ra)
Current Store : [0x80000ff8] : sw tp, 844(ra) -- Store: [0x80003b60]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001010]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001014]:csrrs tp, fcsr, zero
	-[0x80001018]:fsw ft11, 848(ra)
Current Store : [0x8000101c] : sw tp, 852(ra) -- Store: [0x80003b68]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001034]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001038]:csrrs tp, fcsr, zero
	-[0x8000103c]:fsw ft11, 856(ra)
Current Store : [0x80001040] : sw tp, 860(ra) -- Store: [0x80003b70]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001058]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000105c]:csrrs tp, fcsr, zero
	-[0x80001060]:fsw ft11, 864(ra)
Current Store : [0x80001064] : sw tp, 868(ra) -- Store: [0x80003b78]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000107c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001080]:csrrs tp, fcsr, zero
	-[0x80001084]:fsw ft11, 872(ra)
Current Store : [0x80001088] : sw tp, 876(ra) -- Store: [0x80003b80]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010a0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800010a4]:csrrs tp, fcsr, zero
	-[0x800010a8]:fsw ft11, 880(ra)
Current Store : [0x800010ac] : sw tp, 884(ra) -- Store: [0x80003b88]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010c4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800010c8]:csrrs tp, fcsr, zero
	-[0x800010cc]:fsw ft11, 888(ra)
Current Store : [0x800010d0] : sw tp, 892(ra) -- Store: [0x80003b90]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010e8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800010ec]:csrrs tp, fcsr, zero
	-[0x800010f0]:fsw ft11, 896(ra)
Current Store : [0x800010f4] : sw tp, 900(ra) -- Store: [0x80003b98]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001110]:csrrs tp, fcsr, zero
	-[0x80001114]:fsw ft11, 904(ra)
Current Store : [0x80001118] : sw tp, 908(ra) -- Store: [0x80003ba0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001130]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001134]:csrrs tp, fcsr, zero
	-[0x80001138]:fsw ft11, 912(ra)
Current Store : [0x8000113c] : sw tp, 916(ra) -- Store: [0x80003ba8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001154]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001158]:csrrs tp, fcsr, zero
	-[0x8000115c]:fsw ft11, 920(ra)
Current Store : [0x80001160] : sw tp, 924(ra) -- Store: [0x80003bb0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001178]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000117c]:csrrs tp, fcsr, zero
	-[0x80001180]:fsw ft11, 928(ra)
Current Store : [0x80001184] : sw tp, 932(ra) -- Store: [0x80003bb8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800011a0]:csrrs tp, fcsr, zero
	-[0x800011a4]:fsw ft11, 936(ra)
Current Store : [0x800011a8] : sw tp, 940(ra) -- Store: [0x80003bc0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011c0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800011c4]:csrrs tp, fcsr, zero
	-[0x800011c8]:fsw ft11, 944(ra)
Current Store : [0x800011cc] : sw tp, 948(ra) -- Store: [0x80003bc8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011e4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800011e8]:csrrs tp, fcsr, zero
	-[0x800011ec]:fsw ft11, 952(ra)
Current Store : [0x800011f0] : sw tp, 956(ra) -- Store: [0x80003bd0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001208]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000120c]:csrrs tp, fcsr, zero
	-[0x80001210]:fsw ft11, 960(ra)
Current Store : [0x80001214] : sw tp, 964(ra) -- Store: [0x80003bd8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001230]:csrrs tp, fcsr, zero
	-[0x80001234]:fsw ft11, 968(ra)
Current Store : [0x80001238] : sw tp, 972(ra) -- Store: [0x80003be0]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001250]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001254]:csrrs tp, fcsr, zero
	-[0x80001258]:fsw ft11, 976(ra)
Current Store : [0x8000125c] : sw tp, 980(ra) -- Store: [0x80003be8]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001274]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001278]:csrrs tp, fcsr, zero
	-[0x8000127c]:fsw ft11, 984(ra)
Current Store : [0x80001280] : sw tp, 988(ra) -- Store: [0x80003bf0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001298]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000129c]:csrrs tp, fcsr, zero
	-[0x800012a0]:fsw ft11, 992(ra)
Current Store : [0x800012a4] : sw tp, 996(ra) -- Store: [0x80003bf8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012bc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800012c0]:csrrs tp, fcsr, zero
	-[0x800012c4]:fsw ft11, 1000(ra)
Current Store : [0x800012c8] : sw tp, 1004(ra) -- Store: [0x80003c00]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012e0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800012e4]:csrrs tp, fcsr, zero
	-[0x800012e8]:fsw ft11, 1008(ra)
Current Store : [0x800012ec] : sw tp, 1012(ra) -- Store: [0x80003c08]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001304]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001308]:csrrs tp, fcsr, zero
	-[0x8000130c]:fsw ft11, 1016(ra)
Current Store : [0x80001310] : sw tp, 1020(ra) -- Store: [0x80003c10]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001330]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001334]:csrrs tp, fcsr, zero
	-[0x80001338]:fsw ft11, 0(ra)
Current Store : [0x8000133c] : sw tp, 4(ra) -- Store: [0x80003c18]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001354]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001358]:csrrs tp, fcsr, zero
	-[0x8000135c]:fsw ft11, 8(ra)
Current Store : [0x80001360] : sw tp, 12(ra) -- Store: [0x80003c20]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001378]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000137c]:csrrs tp, fcsr, zero
	-[0x80001380]:fsw ft11, 16(ra)
Current Store : [0x80001384] : sw tp, 20(ra) -- Store: [0x80003c28]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000139c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800013a0]:csrrs tp, fcsr, zero
	-[0x800013a4]:fsw ft11, 24(ra)
Current Store : [0x800013a8] : sw tp, 28(ra) -- Store: [0x80003c30]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013c0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800013c4]:csrrs tp, fcsr, zero
	-[0x800013c8]:fsw ft11, 32(ra)
Current Store : [0x800013cc] : sw tp, 36(ra) -- Store: [0x80003c38]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013e4]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800013e8]:csrrs tp, fcsr, zero
	-[0x800013ec]:fsw ft11, 40(ra)
Current Store : [0x800013f0] : sw tp, 44(ra) -- Store: [0x80003c40]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001408]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000140c]:csrrs tp, fcsr, zero
	-[0x80001410]:fsw ft11, 48(ra)
Current Store : [0x80001414] : sw tp, 52(ra) -- Store: [0x80003c48]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001430]:csrrs tp, fcsr, zero
	-[0x80001434]:fsw ft11, 56(ra)
Current Store : [0x80001438] : sw tp, 60(ra) -- Store: [0x80003c50]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001450]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001454]:csrrs tp, fcsr, zero
	-[0x80001458]:fsw ft11, 64(ra)
Current Store : [0x8000145c] : sw tp, 68(ra) -- Store: [0x80003c58]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001474]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001478]:csrrs tp, fcsr, zero
	-[0x8000147c]:fsw ft11, 72(ra)
Current Store : [0x80001480] : sw tp, 76(ra) -- Store: [0x80003c60]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001498]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000149c]:csrrs tp, fcsr, zero
	-[0x800014a0]:fsw ft11, 80(ra)
Current Store : [0x800014a4] : sw tp, 84(ra) -- Store: [0x80003c68]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800014c0]:csrrs tp, fcsr, zero
	-[0x800014c4]:fsw ft11, 88(ra)
Current Store : [0x800014c8] : sw tp, 92(ra) -- Store: [0x80003c70]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014e0]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800014e4]:csrrs tp, fcsr, zero
	-[0x800014e8]:fsw ft11, 96(ra)
Current Store : [0x800014ec] : sw tp, 100(ra) -- Store: [0x80003c78]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001504]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001508]:csrrs tp, fcsr, zero
	-[0x8000150c]:fsw ft11, 104(ra)
Current Store : [0x80001510] : sw tp, 108(ra) -- Store: [0x80003c80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001528]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x8000152c]:csrrs tp, fcsr, zero
	-[0x80001530]:fsw ft11, 112(ra)
Current Store : [0x80001534] : sw tp, 116(ra) -- Store: [0x80003c88]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001550]:csrrs tp, fcsr, zero
	-[0x80001554]:fsw ft11, 120(ra)
Current Store : [0x80001558] : sw tp, 124(ra) -- Store: [0x80003c90]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001570]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001574]:csrrs tp, fcsr, zero
	-[0x80001578]:fsw ft11, 128(ra)
Current Store : [0x8000157c] : sw tp, 132(ra) -- Store: [0x80003c98]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001594]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x80001598]:csrrs tp, fcsr, zero
	-[0x8000159c]:fsw ft11, 136(ra)
Current Store : [0x800015a0] : sw tp, 140(ra) -- Store: [0x80003ca0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015b8]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800015bc]:csrrs tp, fcsr, zero
	-[0x800015c0]:fsw ft11, 144(ra)
Current Store : [0x800015c4] : sw tp, 148(ra) -- Store: [0x80003ca8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015dc]:fmsub.h ft11, ft10, ft9, ft8, dyn
	-[0x800015e0]:csrrs tp, fcsr, zero
	-[0x800015e4]:fsw ft11, 152(ra)
Current Store : [0x800015e8] : sw tp, 156(ra) -- Store: [0x80003cb0]:0x00000023





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                                                                                            coverpoints                                                                                                                                                                                                             |                                                            code                                                             |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003814]<br>0xFBB6FAB7<br> |- mnemonic : fmsub.h<br> - rs1 : f31<br> - rs2 : f30<br> - rd : f31<br> - rs3 : f29<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>      |[0x80000128]:fmsub.h ft11, ft11, ft10, ft9, dyn<br> [0x8000012c]:csrrs tp, fcsr, zero<br> [0x80000130]:fsw ft11, 0(ra)<br>   |
|   2|[0x8000381c]<br>0xDDB7D5BF<br> |- rs1 : f28<br> - rs2 : f28<br> - rd : f28<br> - rs3 : f28<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                                                                                        |[0x8000014c]:fmsub.h ft8, ft8, ft8, ft8, dyn<br> [0x80000150]:csrrs tp, fcsr, zero<br> [0x80000154]:fsw ft8, 8(ra)<br>       |
|   3|[0x80003824]<br>0xF76DF56F<br> |- rs1 : f29<br> - rs2 : f29<br> - rd : f30<br> - rs3 : f31<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                                                                                           |[0x80000170]:fmsub.h ft10, ft9, ft9, ft11, dyn<br> [0x80000174]:csrrs tp, fcsr, zero<br> [0x80000178]:fsw ft10, 16(ra)<br>   |
|   4|[0x8000382c]<br>0xBB6FAB7F<br> |- rs1 : f30<br> - rs2 : f27<br> - rd : f27<br> - rs3 : f26<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                              |[0x80000194]:fmsub.h fs11, ft10, fs11, fs10, dyn<br> [0x80000198]:csrrs tp, fcsr, zero<br> [0x8000019c]:fsw fs11, 24(ra)<br> |
|   5|[0x80003834]<br>0xEEDBEADF<br> |- rs1 : f27<br> - rs2 : f25<br> - rd : f29<br> - rs3 : f25<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                                                                                           |[0x800001b8]:fmsub.h ft9, fs11, fs9, fs9, dyn<br> [0x800001bc]:csrrs tp, fcsr, zero<br> [0x800001c0]:fsw ft9, 32(ra)<br>     |
|   6|[0x8000383c]<br>0xDB7D5BFD<br> |- rs1 : f24<br> - rs2 : f31<br> - rd : f24<br> - rs3 : f24<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                                                                                        |[0x800001dc]:fmsub.h fs8, fs8, ft11, fs8, dyn<br> [0x800001e0]:csrrs tp, fcsr, zero<br> [0x800001e4]:fsw fs8, 40(ra)<br>     |
|   7|[0x80003844]<br>0x76DF56FF<br> |- rs1 : f23<br> - rs2 : f23<br> - rd : f26<br> - rs3 : f23<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                                                                                        |[0x80000200]:fmsub.h fs10, fs7, fs7, fs7, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:fsw fs10, 48(ra)<br>    |
|   8|[0x8000384c]<br>0xEDBEADFE<br> |- rs1 : f22<br> - rs2 : f26<br> - rd : f25<br> - rs3 : f22<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                                                                                           |[0x80000224]:fmsub.h fs9, fs6, fs10, fs6, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:fsw fs9, 56(ra)<br>     |
|   9|[0x80003854]<br>0xDBEADFEE<br> |- rs1 : f21<br> - rs2 : f21<br> - rd : f21<br> - rs3 : f30<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                                                                                        |[0x80000248]:fmsub.h fs5, fs5, fs5, ft10, dyn<br> [0x8000024c]:csrrs tp, fcsr, zero<br> [0x80000250]:fsw fs5, 64(ra)<br>     |
|  10|[0x8000385c]<br>0xB7D5BFDD<br> |- rs1 : f26<br> - rs2 : f24<br> - rd : f20<br> - rs3 : f20<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                              |[0x8000026c]:fmsub.h fs4, fs10, fs8, fs4, dyn<br> [0x80000270]:csrrs tp, fcsr, zero<br> [0x80000274]:fsw fs4, 72(ra)<br>     |
|  11|[0x80003864]<br>0xB6FAB7FB<br> |- rs1 : f25<br> - rs2 : f22<br> - rd : f23<br> - rs3 : f27<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br> |[0x80000290]:fmsub.h fs7, fs9, fs6, fs11, dyn<br> [0x80000294]:csrrs tp, fcsr, zero<br> [0x80000298]:fsw fs7, 80(ra)<br>     |
|  12|[0x8000386c]<br>0x6FAB7FBB<br> |- rs1 : f20<br> - rs2 : f19<br> - rd : f19<br> - rs3 : f19<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                                                                                        |[0x800002b4]:fmsub.h fs3, fs4, fs3, fs3, dyn<br> [0x800002b8]:csrrs tp, fcsr, zero<br> [0x800002bc]:fsw fs3, 88(ra)<br>      |
|  13|[0x80003874]<br>0x6DF56FF7<br> |- rs1 : f19<br> - rs2 : f20<br> - rd : f22<br> - rs3 : f21<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x800002d8]:fmsub.h fs6, fs3, fs4, fs5, dyn<br> [0x800002dc]:csrrs tp, fcsr, zero<br> [0x800002e0]:fsw fs6, 96(ra)<br>      |
|  14|[0x8000387c]<br>0xDF56FF76<br> |- rs1 : f17<br> - rs2 : f16<br> - rd : f18<br> - rs3 : f15<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x800002fc]:fmsub.h fs2, fa7, fa6, fa5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:fsw fs2, 104(ra)<br>     |
|  15|[0x80003884]<br>0xBEADFEED<br> |- rs1 : f15<br> - rs2 : f18<br> - rd : f17<br> - rs3 : f16<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x80000320]:fmsub.h fa7, fa5, fs2, fa6, dyn<br> [0x80000324]:csrrs tp, fcsr, zero<br> [0x80000328]:fsw fa7, 112(ra)<br>     |
|  16|[0x8000388c]<br>0x7D5BFDDB<br> |- rs1 : f18<br> - rs2 : f15<br> - rd : f16<br> - rs3 : f17<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x80000344]:fmsub.h fa6, fs2, fa5, fa7, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:fsw fa6, 120(ra)<br>     |
|  17|[0x80003894]<br>0xFAB7FBB6<br> |- rs1 : f16<br> - rs2 : f17<br> - rd : f15<br> - rs3 : f18<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x80000368]:fmsub.h fa5, fa6, fa7, fs2, dyn<br> [0x8000036c]:csrrs tp, fcsr, zero<br> [0x80000370]:fsw fa5, 128(ra)<br>     |
|  18|[0x8000389c]<br>0xF56FF76D<br> |- rs1 : f13<br> - rs2 : f12<br> - rd : f14<br> - rs3 : f11<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x8000038c]:fmsub.h fa4, fa3, fa2, fa1, dyn<br> [0x80000390]:csrrs tp, fcsr, zero<br> [0x80000394]:fsw fa4, 136(ra)<br>     |
|  19|[0x800038a4]<br>0xEADFEEDB<br> |- rs1 : f11<br> - rs2 : f14<br> - rd : f13<br> - rs3 : f12<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x800003b0]:fmsub.h fa3, fa1, fa4, fa2, dyn<br> [0x800003b4]:csrrs tp, fcsr, zero<br> [0x800003b8]:fsw fa3, 144(ra)<br>     |
|  20|[0x800038ac]<br>0xD5BFDDB7<br> |- rs1 : f14<br> - rs2 : f11<br> - rd : f12<br> - rs3 : f13<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x36c and fs2 == 1 and fe2 == 0x12 and fm2 == 0x03e and fs3 == 0 and fe3 == 0x1e and fm3 == 0x00e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                         |[0x800003d4]:fmsub.h fa2, fa4, fa1, fa3, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:fsw fa2, 152(ra)<br>     |
|  21|[0x800038b4]<br>0xAB7FBB6F<br> |- rs1 : f12<br> - rs2 : f13<br> - rd : f11<br> - rs3 : f14<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                          |[0x800003f8]:fmsub.h fa1, fa2, fa3, fa4, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:fsw fa1, 160(ra)<br>     |
|  22|[0x800038bc]<br>0x00002000<br> |- rs1 : f9<br> - rs2 : f8<br> - rd : f10<br> - rs3 : f7<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                            |[0x8000041c]:fmsub.h fa0, fs1, fs0, ft7, dyn<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:fsw fa0, 168(ra)<br>     |
|  23|[0x800038c4]<br>0xADFEEDBE<br> |- rs1 : f7<br> - rs2 : f10<br> - rd : f9<br> - rs3 : f8<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                            |[0x80000440]:fmsub.h fs1, ft7, fa0, fs0, dyn<br> [0x80000444]:csrrs tp, fcsr, zero<br> [0x80000448]:fsw fs1, 176(ra)<br>     |
|  24|[0x800038cc]<br>0x5BFDDB7D<br> |- rs1 : f10<br> - rs2 : f7<br> - rd : f8<br> - rs3 : f9<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                            |[0x80000464]:fmsub.h fs0, fa0, ft7, fs1, dyn<br> [0x80000468]:csrrs tp, fcsr, zero<br> [0x8000046c]:fsw fs0, 184(ra)<br>     |
|  25|[0x800038d4]<br>0xB7FBB6FA<br> |- rs1 : f8<br> - rs2 : f9<br> - rd : f7<br> - rs3 : f10<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x0cc and fs2 == 0 and fe2 == 0x14 and fm2 == 0x0c7 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2ef and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                            |[0x80000488]:fmsub.h ft7, fs0, fs1, fa0, dyn<br> [0x8000048c]:csrrs tp, fcsr, zero<br> [0x80000490]:fsw ft7, 192(ra)<br>     |
|  26|[0x800038dc]<br>0x80003000<br> |- rs1 : f5<br> - rs2 : f4<br> - rd : f6<br> - rs3 : f3<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800004ac]:fmsub.h ft6, ft5, ft4, ft3, dyn<br> [0x800004b0]:csrrs tp, fcsr, zero<br> [0x800004b4]:fsw ft6, 200(ra)<br>     |
|  27|[0x800038e4]<br>0x800000F8<br> |- rs1 : f3<br> - rs2 : f6<br> - rd : f5<br> - rs3 : f4<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x800004d0]:fmsub.h ft5, ft3, ft6, ft4, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:fsw ft5, 208(ra)<br>     |
|  28|[0x800038ec]<br>0x00000043<br> |- rs1 : f6<br> - rs2 : f3<br> - rd : f4<br> - rs3 : f5<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x800004f4]:fmsub.h ft4, ft6, ft3, ft5, dyn<br> [0x800004f8]:csrrs tp, fcsr, zero<br> [0x800004fc]:fsw ft4, 216(ra)<br>     |
|  29|[0x800038f4]<br>0x80003010<br> |- rs1 : f4<br> - rs2 : f5<br> - rd : f3<br> - rs3 : f6<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                             |[0x80000518]:fmsub.h ft3, ft4, ft5, ft6, dyn<br> [0x8000051c]:csrrs tp, fcsr, zero<br> [0x80000520]:fsw ft3, 224(ra)<br>     |
|  30|[0x800038fc]<br>0xFBB6FAB7<br> |- rs1 : f2<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x078 and fs2 == 1 and fe2 == 0x12 and fm2 == 0x281 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1cd and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x8000053c]:fmsub.h ft11, ft2, ft10, ft9, dyn<br> [0x80000540]:csrrs tp, fcsr, zero<br> [0x80000544]:fsw ft11, 232(ra)<br>  |
|  31|[0x80003904]<br>0xFBB6FAB7<br> |- rs1 : f1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000560]:fmsub.h ft11, ft1, ft10, ft9, dyn<br> [0x80000564]:csrrs tp, fcsr, zero<br> [0x80000568]:fsw ft11, 240(ra)<br>  |
|  32|[0x8000390c]<br>0xFBB6FAB7<br> |- rs1 : f0<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x80000584]:fmsub.h ft11, ft0, ft10, ft9, dyn<br> [0x80000588]:csrrs tp, fcsr, zero<br> [0x8000058c]:fsw ft11, 248(ra)<br>  |
|  33|[0x80003914]<br>0xFBB6FAB7<br> |- rs2 : f2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x800005a8]:fmsub.h ft11, ft10, ft2, ft9, dyn<br> [0x800005ac]:csrrs tp, fcsr, zero<br> [0x800005b0]:fsw ft11, 256(ra)<br>  |
|  34|[0x8000391c]<br>0xFBB6FAB7<br> |- rs2 : f1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x800005cc]:fmsub.h ft11, ft10, ft1, ft9, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:fsw ft11, 264(ra)<br>  |
|  35|[0x80003924]<br>0xFBB6FAB7<br> |- rs2 : f0<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ca and fs2 == 0 and fe2 == 0x11 and fm2 == 0x04d and fs3 == 0 and fe3 == 0x1e and fm3 == 0x29c and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x800005f0]:fmsub.h ft11, ft10, ft0, ft9, dyn<br> [0x800005f4]:csrrs tp, fcsr, zero<br> [0x800005f8]:fsw ft11, 272(ra)<br>  |
|  36|[0x8000392c]<br>0xFBB6FAB7<br> |- rs3 : f2<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000614]:fmsub.h ft11, ft10, ft9, ft2, dyn<br> [0x80000618]:csrrs tp, fcsr, zero<br> [0x8000061c]:fsw ft11, 280(ra)<br>  |
|  37|[0x80003934]<br>0xFBB6FAB7<br> |- rs3 : f1<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x80000638]:fmsub.h ft11, ft10, ft9, ft1, dyn<br> [0x8000063c]:csrrs tp, fcsr, zero<br> [0x80000640]:fsw ft11, 288(ra)<br>  |
|  38|[0x8000393c]<br>0xFBB6FAB7<br> |- rs3 : f0<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                         |[0x8000065c]:fmsub.h ft11, ft10, ft9, ft0, dyn<br> [0x80000660]:csrrs tp, fcsr, zero<br> [0x80000664]:fsw ft11, 296(ra)<br>  |
|  39|[0x80003944]<br>0x00000062<br> |- rd : f2<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x80000680]:fmsub.h ft2, ft11, ft10, ft9, dyn<br> [0x80000684]:csrrs tp, fcsr, zero<br> [0x80000688]:fsw ft2, 304(ra)<br>   |
|  40|[0x8000394c]<br>0x80003814<br> |- rd : f1<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x3eb and fs2 == 1 and fe2 == 0x11 and fm2 == 0x246 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x323 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                          |[0x800006a4]:fmsub.h ft1, ft11, ft10, ft9, dyn<br> [0x800006a8]:csrrs tp, fcsr, zero<br> [0x800006ac]:fsw ft1, 312(ra)<br>   |
|  41|[0x80003954]<br>0x00000000<br> |- rd : f0<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                           |[0x800006c8]:fmsub.h ft0, ft11, ft10, ft9, dyn<br> [0x800006cc]:csrrs tp, fcsr, zero<br> [0x800006d0]:fsw ft0, 320(ra)<br>   |
|  42|[0x8000395c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800006ec]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800006f0]:csrrs tp, fcsr, zero<br> [0x800006f4]:fsw ft11, 328(ra)<br>  |
|  43|[0x80003964]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000710]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000714]:csrrs tp, fcsr, zero<br> [0x80000718]:fsw ft11, 336(ra)<br>  |
|  44|[0x8000396c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000734]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000738]:csrrs tp, fcsr, zero<br> [0x8000073c]:fsw ft11, 344(ra)<br>  |
|  45|[0x80003974]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x104 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x235 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x393 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000758]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000075c]:csrrs tp, fcsr, zero<br> [0x80000760]:fsw ft11, 352(ra)<br>  |
|  46|[0x8000397c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000077c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000780]:csrrs tp, fcsr, zero<br> [0x80000784]:fsw ft11, 360(ra)<br>  |
|  47|[0x80003984]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800007a0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007a4]:csrrs tp, fcsr, zero<br> [0x800007a8]:fsw ft11, 368(ra)<br>  |
|  48|[0x8000398c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800007c4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs tp, fcsr, zero<br> [0x800007cc]:fsw ft11, 376(ra)<br>  |
|  49|[0x80003994]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800007e8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800007ec]:csrrs tp, fcsr, zero<br> [0x800007f0]:fsw ft11, 384(ra)<br>  |
|  50|[0x8000399c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x09d and fs2 == 1 and fe2 == 0x0f and fm2 == 0x02e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x258 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000080c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000810]:csrrs tp, fcsr, zero<br> [0x80000814]:fsw ft11, 392(ra)<br>  |
|  51|[0x800039a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000830]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000834]:csrrs tp, fcsr, zero<br> [0x80000838]:fsw ft11, 400(ra)<br>  |
|  52|[0x800039ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000854]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000858]:csrrs tp, fcsr, zero<br> [0x8000085c]:fsw ft11, 408(ra)<br>  |
|  53|[0x800039b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000878]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000087c]:csrrs tp, fcsr, zero<br> [0x80000880]:fsw ft11, 416(ra)<br>  |
|  54|[0x800039bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000089c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008a0]:csrrs tp, fcsr, zero<br> [0x800008a4]:fsw ft11, 424(ra)<br>  |
|  55|[0x800039c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x0f and fm2 == 0x049 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1f6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800008c0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008c4]:csrrs tp, fcsr, zero<br> [0x800008c8]:fsw ft11, 432(ra)<br>  |
|  56|[0x800039cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800008e4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs tp, fcsr, zero<br> [0x800008ec]:fsw ft11, 440(ra)<br>  |
|  57|[0x800039d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000908]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000090c]:csrrs tp, fcsr, zero<br> [0x80000910]:fsw ft11, 448(ra)<br>  |
|  58|[0x800039dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000092c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000930]:csrrs tp, fcsr, zero<br> [0x80000934]:fsw ft11, 456(ra)<br>  |
|  59|[0x800039e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000950]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000954]:csrrs tp, fcsr, zero<br> [0x80000958]:fsw ft11, 464(ra)<br>  |
|  60|[0x800039ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2dd and fs2 == 1 and fe2 == 0x0e and fm2 == 0x29e and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0a1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000974]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000978]:csrrs tp, fcsr, zero<br> [0x8000097c]:fsw ft11, 472(ra)<br>  |
|  61|[0x800039f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000998]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000099c]:csrrs tp, fcsr, zero<br> [0x800009a0]:fsw ft11, 480(ra)<br>  |
|  62|[0x800039fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800009bc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800009c0]:csrrs tp, fcsr, zero<br> [0x800009c4]:fsw ft11, 488(ra)<br>  |
|  63|[0x80003a04]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800009e0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800009e4]:csrrs tp, fcsr, zero<br> [0x800009e8]:fsw ft11, 496(ra)<br>  |
|  64|[0x80003a0c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000a04]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs tp, fcsr, zero<br> [0x80000a0c]:fsw ft11, 504(ra)<br>  |
|  65|[0x80003a14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x09d and fs2 == 0 and fe2 == 0x11 and fm2 == 0x3bf and fs3 == 0 and fe3 == 0x1b and fm3 == 0x389 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000a28]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a2c]:csrrs tp, fcsr, zero<br> [0x80000a30]:fsw ft11, 512(ra)<br>  |
|  66|[0x80003a1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000a4c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a50]:csrrs tp, fcsr, zero<br> [0x80000a54]:fsw ft11, 520(ra)<br>  |
|  67|[0x80003a24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000a70]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a74]:csrrs tp, fcsr, zero<br> [0x80000a78]:fsw ft11, 528(ra)<br>  |
|  68|[0x80003a2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000a94]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000a98]:csrrs tp, fcsr, zero<br> [0x80000a9c]:fsw ft11, 536(ra)<br>  |
|  69|[0x80003a34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000ab8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000abc]:csrrs tp, fcsr, zero<br> [0x80000ac0]:fsw ft11, 544(ra)<br>  |
|  70|[0x80003a3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 1 and fe2 == 0x0e and fm2 == 0x0c4 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x286 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000adc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000ae0]:csrrs tp, fcsr, zero<br> [0x80000ae4]:fsw ft11, 552(ra)<br>  |
|  71|[0x80003a44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000b00]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000b04]:csrrs tp, fcsr, zero<br> [0x80000b08]:fsw ft11, 560(ra)<br>  |
|  72|[0x80003a4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000b24]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs tp, fcsr, zero<br> [0x80000b2c]:fsw ft11, 568(ra)<br>  |
|  73|[0x80003a54]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000b48]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000b4c]:csrrs tp, fcsr, zero<br> [0x80000b50]:fsw ft11, 576(ra)<br>  |
|  74|[0x80003a5c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000b6c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000b70]:csrrs tp, fcsr, zero<br> [0x80000b74]:fsw ft11, 584(ra)<br>  |
|  75|[0x80003a64]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c7 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x3d1 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0a6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000b90]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000b94]:csrrs tp, fcsr, zero<br> [0x80000b98]:fsw ft11, 592(ra)<br>  |
|  76|[0x80003a6c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000bb4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000bb8]:csrrs tp, fcsr, zero<br> [0x80000bbc]:fsw ft11, 600(ra)<br>  |
|  77|[0x80003a74]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000bd8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000bdc]:csrrs tp, fcsr, zero<br> [0x80000be0]:fsw ft11, 608(ra)<br>  |
|  78|[0x80003a7c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000bfc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000c00]:csrrs tp, fcsr, zero<br> [0x80000c04]:fsw ft11, 616(ra)<br>  |
|  79|[0x80003a84]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000c20]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000c24]:csrrs tp, fcsr, zero<br> [0x80000c28]:fsw ft11, 624(ra)<br>  |
|  80|[0x80003a8c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11b and fs2 == 0 and fe2 == 0x0d and fm2 == 0x232 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1f4 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000c44]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000c48]:csrrs tp, fcsr, zero<br> [0x80000c4c]:fsw ft11, 632(ra)<br>  |
|  81|[0x80003a94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000c68]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs tp, fcsr, zero<br> [0x80000c70]:fsw ft11, 640(ra)<br>  |
|  82|[0x80003a9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000c8c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000c90]:csrrs tp, fcsr, zero<br> [0x80000c94]:fsw ft11, 648(ra)<br>  |
|  83|[0x80003aa4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000cb0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000cb4]:csrrs tp, fcsr, zero<br> [0x80000cb8]:fsw ft11, 656(ra)<br>  |
|  84|[0x80003aac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000cd4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000cd8]:csrrs tp, fcsr, zero<br> [0x80000cdc]:fsw ft11, 664(ra)<br>  |
|  85|[0x80003ab4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x061 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x091 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x004 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000cf8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000cfc]:csrrs tp, fcsr, zero<br> [0x80000d00]:fsw ft11, 672(ra)<br>  |
|  86|[0x80003abc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000d1c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000d20]:csrrs tp, fcsr, zero<br> [0x80000d24]:fsw ft11, 680(ra)<br>  |
|  87|[0x80003ac4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000d40]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000d44]:csrrs tp, fcsr, zero<br> [0x80000d48]:fsw ft11, 688(ra)<br>  |
|  88|[0x80003acc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000d64]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000d68]:csrrs tp, fcsr, zero<br> [0x80000d6c]:fsw ft11, 696(ra)<br>  |
|  89|[0x80003ad4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000d88]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs tp, fcsr, zero<br> [0x80000d90]:fsw ft11, 704(ra)<br>  |
|  90|[0x80003adc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x1e8 and fs3 == 0 and fe3 == 0x1c and fm3 == 0x296 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000dac]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000db0]:csrrs tp, fcsr, zero<br> [0x80000db4]:fsw ft11, 712(ra)<br>  |
|  91|[0x80003ae4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000dd0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000dd4]:csrrs tp, fcsr, zero<br> [0x80000dd8]:fsw ft11, 720(ra)<br>  |
|  92|[0x80003aec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000df4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000df8]:csrrs tp, fcsr, zero<br> [0x80000dfc]:fsw ft11, 728(ra)<br>  |
|  93|[0x80003af4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000e18]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000e1c]:csrrs tp, fcsr, zero<br> [0x80000e20]:fsw ft11, 736(ra)<br>  |
|  94|[0x80003afc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000e3c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000e40]:csrrs tp, fcsr, zero<br> [0x80000e44]:fsw ft11, 744(ra)<br>  |
|  95|[0x80003b04]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x170 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x174 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x2d6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000e60]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000e64]:csrrs tp, fcsr, zero<br> [0x80000e68]:fsw ft11, 752(ra)<br>  |
|  96|[0x80003b0c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000e84]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000e88]:csrrs tp, fcsr, zero<br> [0x80000e8c]:fsw ft11, 760(ra)<br>  |
|  97|[0x80003b14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000ea8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000eac]:csrrs tp, fcsr, zero<br> [0x80000eb0]:fsw ft11, 768(ra)<br>  |
|  98|[0x80003b1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000ecc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000ed0]:csrrs tp, fcsr, zero<br> [0x80000ed4]:fsw ft11, 776(ra)<br>  |
|  99|[0x80003b24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000ef0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000ef4]:csrrs tp, fcsr, zero<br> [0x80000ef8]:fsw ft11, 784(ra)<br>  |
| 100|[0x80003b2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x106 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x05b and fs3 == 0 and fe3 == 0x1e and fm3 == 0x0ae and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000f14]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000f18]:csrrs tp, fcsr, zero<br> [0x80000f1c]:fsw ft11, 792(ra)<br>  |
| 101|[0x80003b34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000f38]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000f3c]:csrrs tp, fcsr, zero<br> [0x80000f40]:fsw ft11, 800(ra)<br>  |
| 102|[0x80003b3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000f5c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000f60]:csrrs tp, fcsr, zero<br> [0x80000f64]:fsw ft11, 808(ra)<br>  |
| 103|[0x80003b44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000f80]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000f84]:csrrs tp, fcsr, zero<br> [0x80000f88]:fsw ft11, 816(ra)<br>  |
| 104|[0x80003b4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000fa4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000fa8]:csrrs tp, fcsr, zero<br> [0x80000fac]:fsw ft11, 824(ra)<br>  |
| 105|[0x80003b54]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x33f and fs2 == 0 and fe2 == 0x10 and fm2 == 0x1bc and fs3 == 0 and fe3 == 0x1d and fm3 == 0x0cb and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80000fc8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000fcc]:csrrs tp, fcsr, zero<br> [0x80000fd0]:fsw ft11, 832(ra)<br>  |
| 106|[0x80003b5c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80000fec]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80000ff0]:csrrs tp, fcsr, zero<br> [0x80000ff4]:fsw ft11, 840(ra)<br>  |
| 107|[0x80003b64]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001010]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001014]:csrrs tp, fcsr, zero<br> [0x80001018]:fsw ft11, 848(ra)<br>  |
| 108|[0x80003b6c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001034]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001038]:csrrs tp, fcsr, zero<br> [0x8000103c]:fsw ft11, 856(ra)<br>  |
| 109|[0x80003b74]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001058]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000105c]:csrrs tp, fcsr, zero<br> [0x80001060]:fsw ft11, 864(ra)<br>  |
| 110|[0x80003b7c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x0d and fm2 == 0x210 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x196 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000107c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001080]:csrrs tp, fcsr, zero<br> [0x80001084]:fsw ft11, 872(ra)<br>  |
| 111|[0x80003b84]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800010a0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800010a4]:csrrs tp, fcsr, zero<br> [0x800010a8]:fsw ft11, 880(ra)<br>  |
| 112|[0x80003b8c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800010c4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800010c8]:csrrs tp, fcsr, zero<br> [0x800010cc]:fsw ft11, 888(ra)<br>  |
| 113|[0x80003b94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800010e8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800010ec]:csrrs tp, fcsr, zero<br> [0x800010f0]:fsw ft11, 896(ra)<br>  |
| 114|[0x80003b9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000110c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001110]:csrrs tp, fcsr, zero<br> [0x80001114]:fsw ft11, 904(ra)<br>  |
| 115|[0x80003ba4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3ce and fs2 == 0 and fe2 == 0x12 and fm2 == 0x145 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x097 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001130]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs tp, fcsr, zero<br> [0x80001138]:fsw ft11, 912(ra)<br>  |
| 116|[0x80003bac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80001154]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001158]:csrrs tp, fcsr, zero<br> [0x8000115c]:fsw ft11, 920(ra)<br>  |
| 117|[0x80003bb4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001178]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000117c]:csrrs tp, fcsr, zero<br> [0x80001180]:fsw ft11, 928(ra)<br>  |
| 118|[0x80003bbc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000119c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800011a0]:csrrs tp, fcsr, zero<br> [0x800011a4]:fsw ft11, 936(ra)<br>  |
| 119|[0x80003bc4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800011c0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800011c4]:csrrs tp, fcsr, zero<br> [0x800011c8]:fsw ft11, 944(ra)<br>  |
| 120|[0x80003bcc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x033 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x048 and fs3 == 0 and fe3 == 0x1e and fm3 == 0x2ff and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800011e4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800011e8]:csrrs tp, fcsr, zero<br> [0x800011ec]:fsw ft11, 952(ra)<br>  |
| 121|[0x80003bd4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80001208]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000120c]:csrrs tp, fcsr, zero<br> [0x80001210]:fsw ft11, 960(ra)<br>  |
| 122|[0x80003bdc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000122c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001230]:csrrs tp, fcsr, zero<br> [0x80001234]:fsw ft11, 968(ra)<br>  |
| 123|[0x80003be4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001250]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs tp, fcsr, zero<br> [0x80001258]:fsw ft11, 976(ra)<br>  |
| 124|[0x80003bec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001274]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001278]:csrrs tp, fcsr, zero<br> [0x8000127c]:fsw ft11, 984(ra)<br>  |
| 125|[0x80003bf4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1e0 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x09a and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001298]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000129c]:csrrs tp, fcsr, zero<br> [0x800012a0]:fsw ft11, 992(ra)<br>  |
| 126|[0x80003bfc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x800012bc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800012c0]:csrrs tp, fcsr, zero<br> [0x800012c4]:fsw ft11, 1000(ra)<br> |
| 127|[0x80003c04]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800012e0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800012e4]:csrrs tp, fcsr, zero<br> [0x800012e8]:fsw ft11, 1008(ra)<br> |
| 128|[0x80003c0c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001304]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001308]:csrrs tp, fcsr, zero<br> [0x8000130c]:fsw ft11, 1016(ra)<br> |
| 129|[0x80003c14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001330]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs tp, fcsr, zero<br> [0x80001338]:fsw ft11, 0(ra)<br>    |
| 130|[0x80003c1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d6 and fs2 == 1 and fe2 == 0x10 and fm2 == 0x3eb and fs3 == 0 and fe3 == 0x1b and fm3 == 0x38d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001354]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001358]:csrrs tp, fcsr, zero<br> [0x8000135c]:fsw ft11, 8(ra)<br>    |
| 131|[0x80003c24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x80001378]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000137c]:csrrs tp, fcsr, zero<br> [0x80001380]:fsw ft11, 16(ra)<br>   |
| 132|[0x80003c2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x8000139c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800013a0]:csrrs tp, fcsr, zero<br> [0x800013a4]:fsw ft11, 24(ra)<br>   |
| 133|[0x80003c34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800013c0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800013c4]:csrrs tp, fcsr, zero<br> [0x800013c8]:fsw ft11, 32(ra)<br>   |
| 134|[0x80003c3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800013e4]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800013e8]:csrrs tp, fcsr, zero<br> [0x800013ec]:fsw ft11, 40(ra)<br>   |
| 135|[0x80003c44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x13 and fm2 == 0x13c and fs3 == 0 and fe3 == 0x1e and fm3 == 0x20d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001408]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000140c]:csrrs tp, fcsr, zero<br> [0x80001410]:fsw ft11, 48(ra)<br>   |
| 136|[0x80003c4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000142c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001430]:csrrs tp, fcsr, zero<br> [0x80001434]:fsw ft11, 56(ra)<br>   |
| 137|[0x80003c54]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001450]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs tp, fcsr, zero<br> [0x80001458]:fsw ft11, 64(ra)<br>   |
| 138|[0x80003c5c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001474]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001478]:csrrs tp, fcsr, zero<br> [0x8000147c]:fsw ft11, 72(ra)<br>   |
| 139|[0x80003c64]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001498]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000149c]:csrrs tp, fcsr, zero<br> [0x800014a0]:fsw ft11, 80(ra)<br>   |
| 140|[0x80003c6c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0dc and fs2 == 1 and fe2 == 0x14 and fm2 == 0x22d and fs3 == 0 and fe3 == 0x1d and fm3 == 0x3d1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800014bc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800014c0]:csrrs tp, fcsr, zero<br> [0x800014c4]:fsw ft11, 88(ra)<br>   |
| 141|[0x80003c74]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800014e0]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800014e4]:csrrs tp, fcsr, zero<br> [0x800014e8]:fsw ft11, 96(ra)<br>   |
| 142|[0x80003c7c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001504]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001508]:csrrs tp, fcsr, zero<br> [0x8000150c]:fsw ft11, 104(ra)<br>  |
| 143|[0x80003c84]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x12d and fs2 == 0 and fe2 == 0x10 and fm2 == 0x31b and fs3 == 0 and fe3 == 0x1c and fm3 == 0x0d3 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001528]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x8000152c]:csrrs tp, fcsr, zero<br> [0x80001530]:fsw ft11, 112(ra)<br>  |
| 144|[0x80003c8c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                         |[0x8000154c]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001550]:csrrs tp, fcsr, zero<br> [0x80001554]:fsw ft11, 120(ra)<br>  |
| 145|[0x80003c94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001570]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs tp, fcsr, zero<br> [0x80001578]:fsw ft11, 128(ra)<br>  |
| 146|[0x80003c9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x80001594]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x80001598]:csrrs tp, fcsr, zero<br> [0x8000159c]:fsw ft11, 136(ra)<br>  |
| 147|[0x80003ca4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x134 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x1b3 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0a2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800015b8]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800015bc]:csrrs tp, fcsr, zero<br> [0x800015c0]:fsw ft11, 144(ra)<br>  |
| 148|[0x80003cac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x048 and fs2 == 0 and fe2 == 0x10 and fm2 == 0x118 and fs3 == 0 and fe3 == 0x1d and fm3 == 0x1d9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff and rs3_nan_prefix == 0xffff  #nosat<br>                                                                                                                                                        |[0x800015dc]:fmsub.h ft11, ft10, ft9, ft8, dyn<br> [0x800015e0]:csrrs tp, fcsr, zero<br> [0x800015e4]:fsw ft11, 152(ra)<br>  |
