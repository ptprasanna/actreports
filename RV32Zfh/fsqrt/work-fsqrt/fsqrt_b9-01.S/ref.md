
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800004b0')]      |
| SIG_REGION                | [('0x80002210', '0x80002320', '68 words')]      |
| COV_LABELS                | fsqrt_b9      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-rv32zfh-fsqrt/fsqrt_b9-01.S/ref.S    |
| Total Number of coverpoints| 89     |
| Total Coverpoints Hit     | 89      |
| Total Signature Updates   | 66      |
| STAT1                     | 33      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 33     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.h', 'rs1 : f30', 'rd : f31', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000120]:fsqrt.h ft11, ft10, dyn
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:fsw ft11, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rs1 : f29', 'rd : f29', 'rs1 == rd', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fsqrt.h ft9, ft9, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:fsw ft9, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x80002220]:0x00000001




Last Coverpoint : ['rs1 : f31', 'rd : f30', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000158]:fsqrt.h ft10, ft11, dyn
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:fsw ft10, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80002228]:0x00000001




Last Coverpoint : ['rs1 : f27', 'rd : f28', 'fs1 == 0 and fe1 == 0x0c and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000174]:fsqrt.h ft8, fs11, dyn
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:fsw ft8, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x80002230]:0x00000001




Last Coverpoint : ['rs1 : f28', 'rd : f27', 'fs1 == 0 and fe1 == 0x0b and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000190]:fsqrt.h fs11, ft8, dyn
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:fsw fs11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80002238]:0x00000001




Last Coverpoint : ['rs1 : f25', 'rd : f26', 'fs1 == 0 and fe1 == 0x0e and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fsqrt.h fs10, fs9, dyn
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:fsw fs10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x80002240]:0x00000001




Last Coverpoint : ['rs1 : f26', 'rd : f25', 'fs1 == 0 and fe1 == 0x09 and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c8]:fsqrt.h fs9, fs10, dyn
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:fsw fs9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80002248]:0x00000001




Last Coverpoint : ['rs1 : f23', 'rd : f24', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fsqrt.h fs8, fs7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:fsw fs8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x80002250]:0x00000000




Last Coverpoint : ['rs1 : f24', 'rd : f23', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000200]:fsqrt.h fs7, fs8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80002258]:0x00000001




Last Coverpoint : ['rs1 : f21', 'rd : f22', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000021c]:fsqrt.h fs6, fs5, dyn
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:fsw fs6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x80002260]:0x00000001




Last Coverpoint : ['rs1 : f22', 'rd : f21', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x1f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000238]:fsqrt.h fs5, fs6, dyn
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:fsw fs5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80002268]:0x00000001




Last Coverpoint : ['rs1 : f19', 'rd : f20', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x300 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000254]:fsqrt.h fs4, fs3, dyn
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:fsw fs4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x80002270]:0x00000001




Last Coverpoint : ['rs1 : f20', 'rd : f19', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x0f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000270]:fsqrt.h fs3, fs4, dyn
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:fsw fs3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80002278]:0x00000001




Last Coverpoint : ['rs1 : f17', 'rd : f18', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x380 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000028c]:fsqrt.h fs2, fa7, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:fsw fs2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x80002280]:0x00000001




Last Coverpoint : ['rs1 : f18', 'rd : f17', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x078 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a8]:fsqrt.h fa7, fs2, dyn
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:fsw fa7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80002288]:0x00000001




Last Coverpoint : ['rs1 : f15', 'rd : f16', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x3c0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fsqrt.h fa6, fa5, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:fsw fa6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x80002290]:0x00000001




Last Coverpoint : ['rs1 : f16', 'rd : f15', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x038 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e0]:fsqrt.h fa5, fa6, dyn
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:fsw fa5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80002298]:0x00000001




Last Coverpoint : ['rs1 : f13', 'rd : f14', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x3e0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fsqrt.h fa4, fa3, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fa4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x800022a0]:0x00000001




Last Coverpoint : ['rs1 : f14', 'rd : f13', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x018 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000318]:fsqrt.h fa3, fa4, dyn
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:fsw fa3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800022a8]:0x00000001




Last Coverpoint : ['rs1 : f11', 'rd : f12', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x3f0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000334]:fsqrt.h fa2, fa1, dyn
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:fsw fa2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800022b0]:0x00000001




Last Coverpoint : ['rs1 : f12', 'rd : f11', 'fs1 == 0 and fe1 == 0x08 and fm1 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000350]:fsqrt.h fa1, fa2, dyn
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:fsw fa1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800022b8]:0x00000001




Last Coverpoint : ['rs1 : f9', 'rd : f10', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000036c]:fsqrt.h fa0, fs1, dyn
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:fsw fa0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800022c0]:0x00000001




Last Coverpoint : ['rs1 : f10', 'rd : f9']
Last Code Sequence : 
	-[0x80000388]:fsqrt.h fs1, fa0, dyn
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:fsw fs1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800022c8]:0x00000000




Last Coverpoint : ['rs1 : f7', 'rd : f8']
Last Code Sequence : 
	-[0x800003a4]:fsqrt.h fs0, ft7, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:fsw fs0, 184(ra)
Current Store : [0x800003b0] : sw tp, 188(ra) -- Store: [0x800022d0]:0x00000000




Last Coverpoint : ['rs1 : f8', 'rd : f7']
Last Code Sequence : 
	-[0x800003c0]:fsqrt.h ft7, fs0, dyn
	-[0x800003c4]:csrrs tp, fcsr, zero
	-[0x800003c8]:fsw ft7, 192(ra)
Current Store : [0x800003cc] : sw tp, 196(ra) -- Store: [0x800022d8]:0x00000000




Last Coverpoint : ['rs1 : f5', 'rd : f6']
Last Code Sequence : 
	-[0x800003dc]:fsqrt.h ft6, ft5, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:fsw ft6, 200(ra)
Current Store : [0x800003e8] : sw tp, 204(ra) -- Store: [0x800022e0]:0x00000000




Last Coverpoint : ['rs1 : f6', 'rd : f5']
Last Code Sequence : 
	-[0x800003f8]:fsqrt.h ft5, ft6, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw ft5, 208(ra)
Current Store : [0x80000404] : sw tp, 212(ra) -- Store: [0x800022e8]:0x00000000




Last Coverpoint : ['rs1 : f3', 'rd : f4']
Last Code Sequence : 
	-[0x80000414]:fsqrt.h ft4, ft3, dyn
	-[0x80000418]:csrrs tp, fcsr, zero
	-[0x8000041c]:fsw ft4, 216(ra)
Current Store : [0x80000420] : sw tp, 220(ra) -- Store: [0x800022f0]:0x00000000




Last Coverpoint : ['rs1 : f4', 'rd : f3']
Last Code Sequence : 
	-[0x80000430]:fsqrt.h ft3, ft4, dyn
	-[0x80000434]:csrrs tp, fcsr, zero
	-[0x80000438]:fsw ft3, 224(ra)
Current Store : [0x8000043c] : sw tp, 228(ra) -- Store: [0x800022f8]:0x00000000




Last Coverpoint : ['rs1 : f1', 'rd : f2']
Last Code Sequence : 
	-[0x8000044c]:fsqrt.h ft2, ft1, dyn
	-[0x80000450]:csrrs tp, fcsr, zero
	-[0x80000454]:fsw ft2, 232(ra)
Current Store : [0x80000458] : sw tp, 236(ra) -- Store: [0x80002300]:0x00000000




Last Coverpoint : ['rs1 : f2', 'rd : f1']
Last Code Sequence : 
	-[0x80000468]:fsqrt.h ft1, ft2, dyn
	-[0x8000046c]:csrrs tp, fcsr, zero
	-[0x80000470]:fsw ft1, 240(ra)
Current Store : [0x80000474] : sw tp, 244(ra) -- Store: [0x80002308]:0x00000000




Last Coverpoint : ['rs1 : f0']
Last Code Sequence : 
	-[0x80000484]:fsqrt.h ft11, ft0, dyn
	-[0x80000488]:csrrs tp, fcsr, zero
	-[0x8000048c]:fsw ft11, 248(ra)
Current Store : [0x80000490] : sw tp, 252(ra) -- Store: [0x80002310]:0x00000000




Last Coverpoint : ['rd : f0']
Last Code Sequence : 
	-[0x800004a0]:fsqrt.h ft0, ft11, dyn
	-[0x800004a4]:csrrs tp, fcsr, zero
	-[0x800004a8]:fsw ft0, 256(ra)
Current Store : [0x800004ac] : sw tp, 260(ra) -- Store: [0x80002318]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                          coverpoints                                                                                          |                                                      code                                                       |
|---:|-------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0xFBB6FAB7<br> |- mnemonic : fsqrt.h<br> - rs1 : f30<br> - rd : f31<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br> |[0x80000120]:fsqrt.h ft11, ft10, dyn<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:fsw ft11, 0(ra)<br>  |
|   2|[0x8000221c]<br>0xEEDBEADF<br> |- rs1 : f29<br> - rd : f29<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                          |[0x8000013c]:fsqrt.h ft9, ft9, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:fsw ft9, 8(ra)<br>     |
|   3|[0x80002224]<br>0xF76DF56F<br> |- rs1 : f31<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000158]:fsqrt.h ft10, ft11, dyn<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:fsw ft10, 16(ra)<br> |
|   4|[0x8000222c]<br>0xDDB7D5BF<br> |- rs1 : f27<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x0c and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000174]:fsqrt.h ft8, fs11, dyn<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:fsw ft8, 24(ra)<br>   |
|   5|[0x80002234]<br>0xBB6FAB7F<br> |- rs1 : f28<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x0b and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000190]:fsqrt.h fs11, ft8, dyn<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:fsw fs11, 32(ra)<br>  |
|   6|[0x8000223c]<br>0x76DF56FF<br> |- rs1 : f25<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x0e and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800001ac]:fsqrt.h fs10, fs9, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:fsw fs10, 40(ra)<br>  |
|   7|[0x80002244]<br>0xEDBEADFE<br> |- rs1 : f26<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x09 and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800001c8]:fsqrt.h fs9, fs10, dyn<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:fsw fs9, 48(ra)<br>   |
|   8|[0x8000224c]<br>0xDB7D5BFD<br> |- rs1 : f23<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800001e4]:fsqrt.h fs8, fs7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:fsw fs8, 56(ra)<br>    |
|   9|[0x80002254]<br>0xB6FAB7FB<br> |- rs1 : f24<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x3f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000200]:fsqrt.h fs7, fs8, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:fsw fs7, 64(ra)<br>    |
|  10|[0x8000225c]<br>0x6DF56FF7<br> |- rs1 : f21<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x8000021c]:fsqrt.h fs6, fs5, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:fsw fs6, 72(ra)<br>    |
|  11|[0x80002264]<br>0xDBEADFEE<br> |- rs1 : f22<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x1f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000238]:fsqrt.h fs5, fs6, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:fsw fs5, 80(ra)<br>    |
|  12|[0x8000226c]<br>0xB7D5BFDD<br> |- rs1 : f19<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x300 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000254]:fsqrt.h fs4, fs3, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:fsw fs4, 88(ra)<br>    |
|  13|[0x80002274]<br>0x6FAB7FBB<br> |- rs1 : f20<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x0f8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000270]:fsqrt.h fs3, fs4, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:fsw fs3, 96(ra)<br>    |
|  14|[0x8000227c]<br>0xDF56FF76<br> |- rs1 : f17<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x380 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x8000028c]:fsqrt.h fs2, fa7, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:fsw fs2, 104(ra)<br>   |
|  15|[0x80002284]<br>0xBEADFEED<br> |- rs1 : f18<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x078 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800002a8]:fsqrt.h fa7, fs2, dyn<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:fsw fa7, 112(ra)<br>   |
|  16|[0x8000228c]<br>0x7D5BFDDB<br> |- rs1 : f15<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x3c0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800002c4]:fsqrt.h fa6, fa5, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:fsw fa6, 120(ra)<br>   |
|  17|[0x80002294]<br>0xFAB7FBB6<br> |- rs1 : f16<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x038 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800002e0]:fsqrt.h fa5, fa6, dyn<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:fsw fa5, 128(ra)<br>   |
|  18|[0x8000229c]<br>0xF56FF76D<br> |- rs1 : f13<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x3e0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x800002fc]:fsqrt.h fa4, fa3, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:fsw fa4, 136(ra)<br>   |
|  19|[0x800022a4]<br>0xEADFEEDB<br> |- rs1 : f14<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x018 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000318]:fsqrt.h fa3, fa4, dyn<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:fsw fa3, 144(ra)<br>   |
|  20|[0x800022ac]<br>0xD5BFDDB7<br> |- rs1 : f11<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x3f0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000334]:fsqrt.h fa2, fa1, dyn<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:fsw fa2, 152(ra)<br>   |
|  21|[0x800022b4]<br>0xAB7FBB6F<br> |- rs1 : f12<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x08 and fm1 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                          |[0x80000350]:fsqrt.h fa1, fa2, dyn<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:fsw fa1, 160(ra)<br>   |
|  22|[0x800022bc]<br>0x00002000<br> |- rs1 : f9<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                           |[0x8000036c]:fsqrt.h fa0, fs1, dyn<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:fsw fa0, 168(ra)<br>   |
|  23|[0x800022c4]<br>0xADFEEDBE<br> |- rs1 : f10<br> - rd : f9<br>                                                                                                                                                                  |[0x80000388]:fsqrt.h fs1, fa0, dyn<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:fsw fs1, 176(ra)<br>   |
|  24|[0x800022cc]<br>0x5BFDDB7D<br> |- rs1 : f7<br> - rd : f8<br>                                                                                                                                                                   |[0x800003a4]:fsqrt.h fs0, ft7, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:fsw fs0, 184(ra)<br>   |
|  25|[0x800022d4]<br>0xB7FBB6FA<br> |- rs1 : f8<br> - rd : f7<br>                                                                                                                                                                   |[0x800003c0]:fsqrt.h ft7, fs0, dyn<br> [0x800003c4]:csrrs tp, fcsr, zero<br> [0x800003c8]:fsw ft7, 192(ra)<br>   |
|  26|[0x800022dc]<br>0x80002000<br> |- rs1 : f5<br> - rd : f6<br>                                                                                                                                                                   |[0x800003dc]:fsqrt.h ft6, ft5, dyn<br> [0x800003e0]:csrrs tp, fcsr, zero<br> [0x800003e4]:fsw ft6, 200(ra)<br>   |
|  27|[0x800022e4]<br>0x800000F8<br> |- rs1 : f6<br> - rd : f5<br>                                                                                                                                                                   |[0x800003f8]:fsqrt.h ft5, ft6, dyn<br> [0x800003fc]:csrrs tp, fcsr, zero<br> [0x80000400]:fsw ft5, 208(ra)<br>   |
|  28|[0x800022ec]<br>0x00000000<br> |- rs1 : f3<br> - rd : f4<br>                                                                                                                                                                   |[0x80000414]:fsqrt.h ft4, ft3, dyn<br> [0x80000418]:csrrs tp, fcsr, zero<br> [0x8000041c]:fsw ft4, 216(ra)<br>   |
|  29|[0x800022f4]<br>0x80002010<br> |- rs1 : f4<br> - rd : f3<br>                                                                                                                                                                   |[0x80000430]:fsqrt.h ft3, ft4, dyn<br> [0x80000434]:csrrs tp, fcsr, zero<br> [0x80000438]:fsw ft3, 224(ra)<br>   |
|  30|[0x800022fc]<br>0x00000000<br> |- rs1 : f1<br> - rd : f2<br>                                                                                                                                                                   |[0x8000044c]:fsqrt.h ft2, ft1, dyn<br> [0x80000450]:csrrs tp, fcsr, zero<br> [0x80000454]:fsw ft2, 232(ra)<br>   |
|  31|[0x80002304]<br>0x80002214<br> |- rs1 : f2<br> - rd : f1<br>                                                                                                                                                                   |[0x80000468]:fsqrt.h ft1, ft2, dyn<br> [0x8000046c]:csrrs tp, fcsr, zero<br> [0x80000470]:fsw ft1, 240(ra)<br>   |
|  32|[0x8000230c]<br>0xFBB6FAB7<br> |- rs1 : f0<br>                                                                                                                                                                                 |[0x80000484]:fsqrt.h ft11, ft0, dyn<br> [0x80000488]:csrrs tp, fcsr, zero<br> [0x8000048c]:fsw ft11, 248(ra)<br> |
|  33|[0x80002314]<br>0x00000000<br> |- rd : f0<br>                                                                                                                                                                                  |[0x800004a0]:fsqrt.h ft0, ft11, dyn<br> [0x800004a4]:csrrs tp, fcsr, zero<br> [0x800004a8]:fsw ft0, 256(ra)<br>  |
