
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800004c0')]      |
| SIG_REGION                | [('0x80002210', '0x80002320', '68 words')]      |
| COV_LABELS                | fmv.h.x_b26      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zfh-rvopcodesdecoder/work-fmvhx/fmv.h.x_b26-01.S/ref.S    |
| Total Number of coverpoints| 97     |
| Total Coverpoints Hit     | 97      |
| Total Signature Updates   | 66      |
| STAT1                     | 33      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 33     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmv.h.x', 'rs1 : x31', 'rd : f31', 'rs1_val == 0 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000120]:fmv.h.x ft11, t6
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:fsw ft11, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rd : f30', 'rs1_val == 1 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fmv.h.x ft10, t5
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:fsw ft10, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rd : f29', 'rs1_val == 2 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000158]:fmv.h.x ft9, t4
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:fsw ft9, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80002228]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rd : f28', 'rs1_val == 7 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000174]:fmv.h.x ft8, t3
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:fsw ft8, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x80002230]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rd : f27', 'rs1_val == 15 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000190]:fmv.h.x fs11, s11
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:fsw fs11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80002238]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rd : f26', 'rs1_val == 16 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fmv.h.x fs10, s10
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:fsw fs10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x80002240]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rd : f25', 'rs1_val == 45 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001c8]:fmv.h.x fs9, s9
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:fsw fs9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80002248]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rd : f24', 'rs1_val == 123 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fmv.h.x fs8, s8
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:fsw fs8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x80002250]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rd : f23', 'rs1_val == 253 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000200]:fmv.h.x fs7, s7
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80002258]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rd : f22', 'rs1_val == 398 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000021c]:fmv.h.x fs6, s6
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:fsw fs6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x80002260]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rd : f21', 'rs1_val == 676 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000238]:fmv.h.x fs5, s5
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:fsw fs5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80002268]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rd : f20', 'rs1_val == 1094 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fmv.h.x fs4, s4
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:fsw fs4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x80002270]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rd : f19', 'rs1_val == 4055 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000270]:fmv.h.x fs3, s3
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:fsw fs3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80002278]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rd : f18', 'rs1_val == 6781 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000028c]:fmv.h.x fs2, s2
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:fsw fs2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x80002280]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rd : f17', 'rs1_val == 9438 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002a8]:fmv.h.x fa7, a7
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:fsw fa7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80002288]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rd : f16', 'rs1_val == 24575 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fmv.h.x fa6, a6
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:fsw fa6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x80002290]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rd : f15', 'rs1_val == 56436 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002e0]:fmv.h.x fa5, a5
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:fsw fa5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80002298]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : f14', 'rs1_val == 71376 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fmv.h.x fa4, a4
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fa4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x800022a0]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : f13', 'rs1_val == 241276 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000318]:fmv.h.x fa3, a3
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:fsw fa3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800022a8]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rd : f12', 'rs1_val == 334857 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000334]:fmv.h.x fa2, a2
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:fsw fa2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800022b0]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rd : f11', 'rs1_val == 896618 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000350]:fmv.h.x fa1, a1
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:fsw fa1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800022b8]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rd : f10', 'rs1_val == 1848861 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000036c]:fmv.h.x fa0, a0
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:fsw fa0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800022c0]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rd : f9', 'rs1_val == 3864061 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000388]:fmv.h.x fs1, s1
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:fsw fs1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800022c8]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rd : f8', 'rs1_val == 6573466 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fmv.h.x fs0, fp
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:fsw fs0, 184(ra)
Current Store : [0x800003b0] : sw tp, 188(ra) -- Store: [0x800022d0]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rd : f7', 'rs1_val == 12789625 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003c8]:fmv.h.x ft7, t2
	-[0x800003cc]:csrrs s1, fcsr, zero
	-[0x800003d0]:fsw ft7, 192(ra)
Current Store : [0x800003d4] : sw s1, 196(ra) -- Store: [0x800022d8]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rd : f6', 'rs1_val == 32105925 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fmv.h.x ft6, t1
	-[0x800003e8]:csrrs s1, fcsr, zero
	-[0x800003ec]:fsw ft6, 200(ra)
Current Store : [0x800003f0] : sw s1, 204(ra) -- Store: [0x800022e0]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rd : f5', 'rs1_val == 45276376 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000400]:fmv.h.x ft5, t0
	-[0x80000404]:csrrs s1, fcsr, zero
	-[0x80000408]:fsw ft5, 208(ra)
Current Store : [0x8000040c] : sw s1, 212(ra) -- Store: [0x800022e8]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rd : f4', 'rs1_val == 107790943 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000424]:fmv.h.x ft4, tp
	-[0x80000428]:csrrs s1, fcsr, zero
	-[0x8000042c]:fsw ft4, 0(t0)
Current Store : [0x80000430] : sw s1, 4(t0) -- Store: [0x800022f0]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rd : f3', 'rs1_val == 231549045 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000440]:fmv.h.x ft3, gp
	-[0x80000444]:csrrs s1, fcsr, zero
	-[0x80000448]:fsw ft3, 8(t0)
Current Store : [0x8000044c] : sw s1, 12(t0) -- Store: [0x800022f8]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rd : f2', 'rs1_val == 339827553 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fmv.h.x ft2, sp
	-[0x80000460]:csrrs s1, fcsr, zero
	-[0x80000464]:fsw ft2, 16(t0)
Current Store : [0x80000468] : sw s1, 20(t0) -- Store: [0x80002300]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rd : f1', 'rs1_val == 1027494066 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x80000478]:fmv.h.x ft1, ra
	-[0x8000047c]:csrrs s1, fcsr, zero
	-[0x80000480]:fsw ft1, 24(t0)
Current Store : [0x80000484] : sw s1, 28(t0) -- Store: [0x80002308]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rd : f0']
Last Code Sequence : 
	-[0x80000494]:fmv.h.x ft0, zero
	-[0x80000498]:csrrs s1, fcsr, zero
	-[0x8000049c]:fsw ft0, 32(t0)
Current Store : [0x800004a0] : sw s1, 36(t0) -- Store: [0x80002310]:0x00000000




Last Coverpoint : ['rs1_val == 1587807073 and  fcsr == 0  #nosat']
Last Code Sequence : 
	-[0x800004b0]:fmv.h.x ft11, t6
	-[0x800004b4]:csrrs s1, fcsr, zero
	-[0x800004b8]:fsw ft11, 40(t0)
Current Store : [0x800004bc] : sw s1, 44(t0) -- Store: [0x80002318]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                           coverpoints                                            |                                                   code                                                    |
|---:|-------------------------------|--------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000<br> |- mnemonic : fmv.h.x<br> - rs1 : x31<br> - rd : f31<br> - rs1_val == 0 and  fcsr == 0  #nosat<br> |[0x80000120]:fmv.h.x ft11, t6<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:fsw ft11, 0(ra)<br>   |
|   2|[0x8000221c]<br>0x00000001<br> |- rs1 : x30<br> - rd : f30<br> - rs1_val == 1 and  fcsr == 0  #nosat<br>                          |[0x8000013c]:fmv.h.x ft10, t5<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:fsw ft10, 8(ra)<br>   |
|   3|[0x80002224]<br>0x00000002<br> |- rs1 : x29<br> - rd : f29<br> - rs1_val == 2 and  fcsr == 0  #nosat<br>                          |[0x80000158]:fmv.h.x ft9, t4<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:fsw ft9, 16(ra)<br>    |
|   4|[0x8000222c]<br>0x00000007<br> |- rs1 : x28<br> - rd : f28<br> - rs1_val == 7 and  fcsr == 0  #nosat<br>                          |[0x80000174]:fmv.h.x ft8, t3<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:fsw ft8, 24(ra)<br>    |
|   5|[0x80002234]<br>0x0000000F<br> |- rs1 : x27<br> - rd : f27<br> - rs1_val == 15 and  fcsr == 0  #nosat<br>                         |[0x80000190]:fmv.h.x fs11, s11<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:fsw fs11, 32(ra)<br> |
|   6|[0x8000223c]<br>0x00000010<br> |- rs1 : x26<br> - rd : f26<br> - rs1_val == 16 and  fcsr == 0  #nosat<br>                         |[0x800001ac]:fmv.h.x fs10, s10<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:fsw fs10, 40(ra)<br> |
|   7|[0x80002244]<br>0x0000002D<br> |- rs1 : x25<br> - rd : f25<br> - rs1_val == 45 and  fcsr == 0  #nosat<br>                         |[0x800001c8]:fmv.h.x fs9, s9<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:fsw fs9, 48(ra)<br>    |
|   8|[0x8000224c]<br>0x0000007B<br> |- rs1 : x24<br> - rd : f24<br> - rs1_val == 123 and  fcsr == 0  #nosat<br>                        |[0x800001e4]:fmv.h.x fs8, s8<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:fsw fs8, 56(ra)<br>    |
|   9|[0x80002254]<br>0x000000FD<br> |- rs1 : x23<br> - rd : f23<br> - rs1_val == 253 and  fcsr == 0  #nosat<br>                        |[0x80000200]:fmv.h.x fs7, s7<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:fsw fs7, 64(ra)<br>    |
|  10|[0x8000225c]<br>0x0000018E<br> |- rs1 : x22<br> - rd : f22<br> - rs1_val == 398 and  fcsr == 0  #nosat<br>                        |[0x8000021c]:fmv.h.x fs6, s6<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:fsw fs6, 72(ra)<br>    |
|  11|[0x80002264]<br>0x000002A4<br> |- rs1 : x21<br> - rd : f21<br> - rs1_val == 676 and  fcsr == 0  #nosat<br>                        |[0x80000238]:fmv.h.x fs5, s5<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:fsw fs5, 80(ra)<br>    |
|  12|[0x8000226c]<br>0x00000446<br> |- rs1 : x20<br> - rd : f20<br> - rs1_val == 1094 and  fcsr == 0  #nosat<br>                       |[0x80000254]:fmv.h.x fs4, s4<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:fsw fs4, 88(ra)<br>    |
|  13|[0x80002274]<br>0x00000FD7<br> |- rs1 : x19<br> - rd : f19<br> - rs1_val == 4055 and  fcsr == 0  #nosat<br>                       |[0x80000270]:fmv.h.x fs3, s3<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:fsw fs3, 96(ra)<br>    |
|  14|[0x8000227c]<br>0x00001A7D<br> |- rs1 : x18<br> - rd : f18<br> - rs1_val == 6781 and  fcsr == 0  #nosat<br>                       |[0x8000028c]:fmv.h.x fs2, s2<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:fsw fs2, 104(ra)<br>   |
|  15|[0x80002284]<br>0x000024DE<br> |- rs1 : x17<br> - rd : f17<br> - rs1_val == 9438 and  fcsr == 0  #nosat<br>                       |[0x800002a8]:fmv.h.x fa7, a7<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:fsw fa7, 112(ra)<br>   |
|  16|[0x8000228c]<br>0x00005FFF<br> |- rs1 : x16<br> - rd : f16<br> - rs1_val == 24575 and  fcsr == 0  #nosat<br>                      |[0x800002c4]:fmv.h.x fa6, a6<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:fsw fa6, 120(ra)<br>   |
|  17|[0x80002294]<br>0x0000DC74<br> |- rs1 : x15<br> - rd : f15<br> - rs1_val == 56436 and  fcsr == 0  #nosat<br>                      |[0x800002e0]:fmv.h.x fa5, a5<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:fsw fa5, 128(ra)<br>   |
|  18|[0x8000229c]<br>0x000116D0<br> |- rs1 : x14<br> - rd : f14<br> - rs1_val == 71376 and  fcsr == 0  #nosat<br>                      |[0x800002fc]:fmv.h.x fa4, a4<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:fsw fa4, 136(ra)<br>   |
|  19|[0x800022a4]<br>0x0003AE7C<br> |- rs1 : x13<br> - rd : f13<br> - rs1_val == 241276 and  fcsr == 0  #nosat<br>                     |[0x80000318]:fmv.h.x fa3, a3<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:fsw fa3, 144(ra)<br>   |
|  20|[0x800022ac]<br>0x00051C09<br> |- rs1 : x12<br> - rd : f12<br> - rs1_val == 334857 and  fcsr == 0  #nosat<br>                     |[0x80000334]:fmv.h.x fa2, a2<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:fsw fa2, 152(ra)<br>   |
|  21|[0x800022b4]<br>0x000DAE6A<br> |- rs1 : x11<br> - rd : f11<br> - rs1_val == 896618 and  fcsr == 0  #nosat<br>                     |[0x80000350]:fmv.h.x fa1, a1<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:fsw fa1, 160(ra)<br>   |
|  22|[0x800022bc]<br>0x001C361D<br> |- rs1 : x10<br> - rd : f10<br> - rs1_val == 1848861 and  fcsr == 0  #nosat<br>                    |[0x8000036c]:fmv.h.x fa0, a0<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:fsw fa0, 168(ra)<br>   |
|  23|[0x800022c4]<br>0x003AF5FD<br> |- rs1 : x9<br> - rd : f9<br> - rs1_val == 3864061 and  fcsr == 0  #nosat<br>                      |[0x80000388]:fmv.h.x fs1, s1<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:fsw fs1, 176(ra)<br>   |
|  24|[0x800022cc]<br>0x00644D9A<br> |- rs1 : x8<br> - rd : f8<br> - rs1_val == 6573466 and  fcsr == 0  #nosat<br>                      |[0x800003a4]:fmv.h.x fs0, fp<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:fsw fs0, 184(ra)<br>   |
|  25|[0x800022d4]<br>0x00C32779<br> |- rs1 : x7<br> - rd : f7<br> - rs1_val == 12789625 and  fcsr == 0  #nosat<br>                     |[0x800003c8]:fmv.h.x ft7, t2<br> [0x800003cc]:csrrs s1, fcsr, zero<br> [0x800003d0]:fsw ft7, 192(ra)<br>   |
|  26|[0x800022dc]<br>0x01E9E5C5<br> |- rs1 : x6<br> - rd : f6<br> - rs1_val == 32105925 and  fcsr == 0  #nosat<br>                     |[0x800003e4]:fmv.h.x ft6, t1<br> [0x800003e8]:csrrs s1, fcsr, zero<br> [0x800003ec]:fsw ft6, 200(ra)<br>   |
|  27|[0x800022e4]<br>0x02B2DCD8<br> |- rs1 : x5<br> - rd : f5<br> - rs1_val == 45276376 and  fcsr == 0  #nosat<br>                     |[0x80000400]:fmv.h.x ft5, t0<br> [0x80000404]:csrrs s1, fcsr, zero<br> [0x80000408]:fsw ft5, 208(ra)<br>   |
|  28|[0x800022ec]<br>0x066CC25F<br> |- rs1 : x4<br> - rd : f4<br> - rs1_val == 107790943 and  fcsr == 0  #nosat<br>                    |[0x80000424]:fmv.h.x ft4, tp<br> [0x80000428]:csrrs s1, fcsr, zero<br> [0x8000042c]:fsw ft4, 0(t0)<br>     |
|  29|[0x800022f4]<br>0x0DCD2875<br> |- rs1 : x3<br> - rd : f3<br> - rs1_val == 231549045 and  fcsr == 0  #nosat<br>                    |[0x80000440]:fmv.h.x ft3, gp<br> [0x80000444]:csrrs s1, fcsr, zero<br> [0x80000448]:fsw ft3, 8(t0)<br>     |
|  30|[0x800022fc]<br>0x14415B61<br> |- rs1 : x2<br> - rd : f2<br> - rs1_val == 339827553 and  fcsr == 0  #nosat<br>                    |[0x8000045c]:fmv.h.x ft2, sp<br> [0x80000460]:csrrs s1, fcsr, zero<br> [0x80000464]:fsw ft2, 16(t0)<br>    |
|  31|[0x80002304]<br>0x3D3E50B2<br> |- rs1 : x1<br> - rd : f1<br> - rs1_val == 1027494066 and  fcsr == 0  #nosat<br>                   |[0x80000478]:fmv.h.x ft1, ra<br> [0x8000047c]:csrrs s1, fcsr, zero<br> [0x80000480]:fsw ft1, 24(t0)<br>    |
|  32|[0x8000230c]<br>0x00000000<br> |- rs1 : x0<br> - rd : f0<br>                                                                      |[0x80000494]:fmv.h.x ft0, zero<br> [0x80000498]:csrrs s1, fcsr, zero<br> [0x8000049c]:fsw ft0, 32(t0)<br>  |
|  33|[0x80002314]<br>0x5EA40361<br> |- rs1_val == 1587807073 and  fcsr == 0  #nosat<br>                                                |[0x800004b0]:fmv.h.x ft11, t6<br> [0x800004b4]:csrrs s1, fcsr, zero<br> [0x800004b8]:fsw ft11, 40(t0)<br>  |
