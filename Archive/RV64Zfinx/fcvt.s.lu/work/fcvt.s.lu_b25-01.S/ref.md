
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000780')]      |
| SIG_REGION                | [('0x80002310', '0x80002530', '68 dwords')]      |
| COV_LABELS                | fcvt.s.lu_b25      |
| TEST_NAME                 | /home/reg/work/zfinx/RV64Zfinx/fcvt.s.l/fcvt.s.lu/work/fcvt.s.lu_b25-01.S/ref.S    |
| Total Number of coverpoints| 69     |
| Total Coverpoints Hit     | 69      |
| Total Signature Updates   | 68      |
| STAT1                     | 32      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 34     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000748]:fcvt.s.lu t6, t5, dyn
      [0x8000074c]:csrrs a1, fcsr, zero
      [0x80000750]:sw t6, 80(t1)
 -- Signature Address: 0x80002510 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fcvt.s.lu
      - rs1 : x30
      - rd : x31
      - rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000764]:fcvt.s.lu t6, t5, dyn
      [0x80000768]:csrrs a1, fcsr, zero
      [0x8000076c]:sw t6, 96(t1)
 -- Signature Address: 0x80002520 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fcvt.s.lu
      - rs1 : x30
      - rd : x31
      - rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.s.lu', 'rs1 : x30', 'rd : x31', 'rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.s.lu t6, t5, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sw t6, 0(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002318]:0x0000000000000000




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.s.lu t5, t6, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sw t5, 16(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002328]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rd : x29', 'rs1_val == 18446744073709551615 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.s.lu t4, t3, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sw t4, 32(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002338]:0x0000000000000001




Last Coverpoint : ['rs1 : x29', 'rd : x28', 'rs1_val == 10540517427298422784 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fcvt.s.lu t3, t4, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sw t3, 48(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002348]:0x0000000000000001




Last Coverpoint : ['rs1 : x26', 'rd : x27']
Last Code Sequence : 
	-[0x80000428]:fcvt.s.lu s11, s10, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sw s11, 64(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002358]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rd : x26']
Last Code Sequence : 
	-[0x80000444]:fcvt.s.lu s10, s11, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sw s10, 80(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002368]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rd : x25']
Last Code Sequence : 
	-[0x80000460]:fcvt.s.lu s9, s8, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sw s9, 96(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002378]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rd : x24']
Last Code Sequence : 
	-[0x8000047c]:fcvt.s.lu s8, s9, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sw s8, 112(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002388]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rd : x23']
Last Code Sequence : 
	-[0x80000498]:fcvt.s.lu s7, s6, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sw s7, 128(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x80002398]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rd : x22']
Last Code Sequence : 
	-[0x800004b4]:fcvt.s.lu s6, s7, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sw s6, 144(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023a8]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rd : x21']
Last Code Sequence : 
	-[0x800004d0]:fcvt.s.lu s5, s4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sw s5, 160(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023b8]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rd : x20']
Last Code Sequence : 
	-[0x800004ec]:fcvt.s.lu s4, s5, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sw s4, 176(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023c8]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rd : x19']
Last Code Sequence : 
	-[0x80000508]:fcvt.s.lu s3, s2, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sw s3, 192(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023d8]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : x18']
Last Code Sequence : 
	-[0x80000524]:fcvt.s.lu s2, s3, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sw s2, 208(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023e8]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : x17']
Last Code Sequence : 
	-[0x80000540]:fcvt.s.lu a7, a6, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sw a7, 224(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x800023f8]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rd : x16']
Last Code Sequence : 
	-[0x8000055c]:fcvt.s.lu a6, a7, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sw a6, 240(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002408]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : x15']
Last Code Sequence : 
	-[0x80000578]:fcvt.s.lu a5, a4, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sw a5, 256(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002418]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rd : x14']
Last Code Sequence : 
	-[0x80000594]:fcvt.s.lu a4, a5, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sw a4, 272(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002428]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rd : x13']
Last Code Sequence : 
	-[0x800005b0]:fcvt.s.lu a3, a2, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sw a3, 288(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002438]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : x12']
Last Code Sequence : 
	-[0x800005cc]:fcvt.s.lu a2, a3, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sw a2, 304(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002448]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rd : x11']
Last Code Sequence : 
	-[0x800005e8]:fcvt.s.lu a1, a0, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sw a1, 320(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002458]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rd : x10']
Last Code Sequence : 
	-[0x80000604]:fcvt.s.lu a0, a1, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sw a0, 336(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002468]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rd : x9']
Last Code Sequence : 
	-[0x80000620]:fcvt.s.lu s1, fp, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sw s1, 352(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002478]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rd : x8']
Last Code Sequence : 
	-[0x80000644]:fcvt.s.lu fp, s1, dyn
	-[0x80000648]:csrrs a1, fcsr, zero
	-[0x8000064c]:sw fp, 368(ra)
Current Store : [0x80000650] : sd a1, 376(ra) -- Store: [0x80002488]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rd : x7']
Last Code Sequence : 
	-[0x80000660]:fcvt.s.lu t2, t1, dyn
	-[0x80000664]:csrrs a1, fcsr, zero
	-[0x80000668]:sw t2, 384(ra)
Current Store : [0x8000066c] : sd a1, 392(ra) -- Store: [0x80002498]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rd : x6']
Last Code Sequence : 
	-[0x8000067c]:fcvt.s.lu t1, t2, dyn
	-[0x80000680]:csrrs a1, fcsr, zero
	-[0x80000684]:sw t1, 400(ra)
Current Store : [0x80000688] : sd a1, 408(ra) -- Store: [0x800024a8]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rd : x5']
Last Code Sequence : 
	-[0x80000698]:fcvt.s.lu t0, tp, dyn
	-[0x8000069c]:csrrs a1, fcsr, zero
	-[0x800006a0]:sw t0, 416(ra)
Current Store : [0x800006a4] : sd a1, 424(ra) -- Store: [0x800024b8]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rd : x4']
Last Code Sequence : 
	-[0x800006bc]:fcvt.s.lu tp, t0, dyn
	-[0x800006c0]:csrrs a1, fcsr, zero
	-[0x800006c4]:sw tp, 0(t1)
Current Store : [0x800006c8] : sd a1, 8(t1) -- Store: [0x800024c8]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : x3']
Last Code Sequence : 
	-[0x800006d8]:fcvt.s.lu gp, sp, dyn
	-[0x800006dc]:csrrs a1, fcsr, zero
	-[0x800006e0]:sw gp, 16(t1)
Current Store : [0x800006e4] : sd a1, 24(t1) -- Store: [0x800024d8]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rd : x2']
Last Code Sequence : 
	-[0x800006f4]:fcvt.s.lu sp, gp, dyn
	-[0x800006f8]:csrrs a1, fcsr, zero
	-[0x800006fc]:sw sp, 32(t1)
Current Store : [0x80000700] : sd a1, 40(t1) -- Store: [0x800024e8]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x80000710]:fcvt.s.lu ra, zero, dyn
	-[0x80000714]:csrrs a1, fcsr, zero
	-[0x80000718]:sw ra, 48(t1)
Current Store : [0x8000071c] : sd a1, 56(t1) -- Store: [0x800024f8]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : x0']
Last Code Sequence : 
	-[0x8000072c]:fcvt.s.lu zero, ra, dyn
	-[0x80000730]:csrrs a1, fcsr, zero
	-[0x80000734]:sw zero, 64(t1)
Current Store : [0x80000738] : sd a1, 72(t1) -- Store: [0x80002508]:0x0000000000000000




Last Coverpoint : ['mnemonic : fcvt.s.lu', 'rs1 : x30', 'rd : x31', 'rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x80000748]:fcvt.s.lu t6, t5, dyn
	-[0x8000074c]:csrrs a1, fcsr, zero
	-[0x80000750]:sw t6, 80(t1)
Current Store : [0x80000754] : sd a1, 88(t1) -- Store: [0x80002518]:0x0000000000000000




Last Coverpoint : ['mnemonic : fcvt.s.lu', 'rs1 : x30', 'rd : x31', 'rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x80000764]:fcvt.s.lu t6, t5, dyn
	-[0x80000768]:csrrs a1, fcsr, zero
	-[0x8000076c]:sw t6, 96(t1)
Current Store : [0x80000770] : sd a1, 104(t1) -- Store: [0x80002528]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                    coverpoints                                                     |                                                      code                                                      |
|---:|----------------------------------|--------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x0000000000000000|- mnemonic : fcvt.s.lu<br> - rs1 : x30<br> - rd : x31<br> - rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat<br> |[0x800003b8]:fcvt.s.lu t6, t5, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sw t6, 0(ra)<br>      |
|   2|[0x80002320]<br>0x000000003F800000|- rs1 : x31<br> - rd : x30<br> - rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat<br>                            |[0x800003d4]:fcvt.s.lu t5, t6, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sw t5, 16(ra)<br>     |
|   3|[0x80002330]<br>0x000000005F800000|- rs1 : x28<br> - rd : x29<br> - rs1_val == 18446744073709551615 and  fcsr == 0 and rm_val == 7  #nosat<br>         |[0x800003f0]:fcvt.s.lu t4, t3, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sw t4, 32(ra)<br>     |
|   4|[0x80002340]<br>0x000000005F124771|- rs1 : x29<br> - rd : x28<br> - rs1_val == 10540517427298422784 and  fcsr == 0 and rm_val == 7  #nosat<br>         |[0x8000040c]:fcvt.s.lu t3, t4, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sw t3, 48(ra)<br>     |
|   5|[0x80002350]<br>0x0000000000000000|- rs1 : x26<br> - rd : x27<br>                                                                                      |[0x80000428]:fcvt.s.lu s11, s10, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sw s11, 64(ra)<br>  |
|   6|[0x80002360]<br>0x0000000000000000|- rs1 : x27<br> - rd : x26<br>                                                                                      |[0x80000444]:fcvt.s.lu s10, s11, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sw s10, 80(ra)<br>  |
|   7|[0x80002370]<br>0x0000000000000000|- rs1 : x24<br> - rd : x25<br>                                                                                      |[0x80000460]:fcvt.s.lu s9, s8, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sw s9, 96(ra)<br>     |
|   8|[0x80002380]<br>0x0000000000000000|- rs1 : x25<br> - rd : x24<br>                                                                                      |[0x8000047c]:fcvt.s.lu s8, s9, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sw s8, 112(ra)<br>    |
|   9|[0x80002390]<br>0x0000000000000000|- rs1 : x22<br> - rd : x23<br>                                                                                      |[0x80000498]:fcvt.s.lu s7, s6, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sw s7, 128(ra)<br>    |
|  10|[0x800023a0]<br>0x0000000000000000|- rs1 : x23<br> - rd : x22<br>                                                                                      |[0x800004b4]:fcvt.s.lu s6, s7, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sw s6, 144(ra)<br>    |
|  11|[0x800023b0]<br>0x0000000000000000|- rs1 : x20<br> - rd : x21<br>                                                                                      |[0x800004d0]:fcvt.s.lu s5, s4, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sw s5, 160(ra)<br>    |
|  12|[0x800023c0]<br>0x0000000000000000|- rs1 : x21<br> - rd : x20<br>                                                                                      |[0x800004ec]:fcvt.s.lu s4, s5, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sw s4, 176(ra)<br>    |
|  13|[0x800023d0]<br>0x0000000000000000|- rs1 : x18<br> - rd : x19<br>                                                                                      |[0x80000508]:fcvt.s.lu s3, s2, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sw s3, 192(ra)<br>    |
|  14|[0x800023e0]<br>0x0000000000000000|- rs1 : x19<br> - rd : x18<br>                                                                                      |[0x80000524]:fcvt.s.lu s2, s3, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sw s2, 208(ra)<br>    |
|  15|[0x800023f0]<br>0x0000000000000000|- rs1 : x16<br> - rd : x17<br>                                                                                      |[0x80000540]:fcvt.s.lu a7, a6, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sw a7, 224(ra)<br>    |
|  16|[0x80002400]<br>0x0000000000000000|- rs1 : x17<br> - rd : x16<br>                                                                                      |[0x8000055c]:fcvt.s.lu a6, a7, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sw a6, 240(ra)<br>    |
|  17|[0x80002410]<br>0x0000000000000000|- rs1 : x14<br> - rd : x15<br>                                                                                      |[0x80000578]:fcvt.s.lu a5, a4, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sw a5, 256(ra)<br>    |
|  18|[0x80002420]<br>0x0000000000000000|- rs1 : x15<br> - rd : x14<br>                                                                                      |[0x80000594]:fcvt.s.lu a4, a5, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sw a4, 272(ra)<br>    |
|  19|[0x80002430]<br>0x0000000000000000|- rs1 : x12<br> - rd : x13<br>                                                                                      |[0x800005b0]:fcvt.s.lu a3, a2, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sw a3, 288(ra)<br>    |
|  20|[0x80002440]<br>0x0000000000000000|- rs1 : x13<br> - rd : x12<br>                                                                                      |[0x800005cc]:fcvt.s.lu a2, a3, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sw a2, 304(ra)<br>    |
|  21|[0x80002450]<br>0x0000000000000000|- rs1 : x10<br> - rd : x11<br>                                                                                      |[0x800005e8]:fcvt.s.lu a1, a0, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sw a1, 320(ra)<br>    |
|  22|[0x80002460]<br>0x0000000000000000|- rs1 : x11<br> - rd : x10<br>                                                                                      |[0x80000604]:fcvt.s.lu a0, a1, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sw a0, 336(ra)<br>    |
|  23|[0x80002470]<br>0x0000000000000000|- rs1 : x8<br> - rd : x9<br>                                                                                        |[0x80000620]:fcvt.s.lu s1, fp, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sw s1, 352(ra)<br>    |
|  24|[0x80002480]<br>0x0000000000000000|- rs1 : x9<br> - rd : x8<br>                                                                                        |[0x80000644]:fcvt.s.lu fp, s1, dyn<br> [0x80000648]:csrrs a1, fcsr, zero<br> [0x8000064c]:sw fp, 368(ra)<br>    |
|  25|[0x80002490]<br>0x0000000000000000|- rs1 : x6<br> - rd : x7<br>                                                                                        |[0x80000660]:fcvt.s.lu t2, t1, dyn<br> [0x80000664]:csrrs a1, fcsr, zero<br> [0x80000668]:sw t2, 384(ra)<br>    |
|  26|[0x800024a0]<br>0x0000000000000000|- rs1 : x7<br> - rd : x6<br>                                                                                        |[0x8000067c]:fcvt.s.lu t1, t2, dyn<br> [0x80000680]:csrrs a1, fcsr, zero<br> [0x80000684]:sw t1, 400(ra)<br>    |
|  27|[0x800024b0]<br>0x0000000000000000|- rs1 : x4<br> - rd : x5<br>                                                                                        |[0x80000698]:fcvt.s.lu t0, tp, dyn<br> [0x8000069c]:csrrs a1, fcsr, zero<br> [0x800006a0]:sw t0, 416(ra)<br>    |
|  28|[0x800024c0]<br>0x0000000000000000|- rs1 : x5<br> - rd : x4<br>                                                                                        |[0x800006bc]:fcvt.s.lu tp, t0, dyn<br> [0x800006c0]:csrrs a1, fcsr, zero<br> [0x800006c4]:sw tp, 0(t1)<br>      |
|  29|[0x800024d0]<br>0x0000000000000000|- rs1 : x2<br> - rd : x3<br>                                                                                        |[0x800006d8]:fcvt.s.lu gp, sp, dyn<br> [0x800006dc]:csrrs a1, fcsr, zero<br> [0x800006e0]:sw gp, 16(t1)<br>     |
|  30|[0x800024e0]<br>0x0000000000000000|- rs1 : x3<br> - rd : x2<br>                                                                                        |[0x800006f4]:fcvt.s.lu sp, gp, dyn<br> [0x800006f8]:csrrs a1, fcsr, zero<br> [0x800006fc]:sw sp, 32(t1)<br>     |
|  31|[0x800024f0]<br>0x0000000000000000|- rs1 : x0<br> - rd : x1<br>                                                                                        |[0x80000710]:fcvt.s.lu ra, zero, dyn<br> [0x80000714]:csrrs a1, fcsr, zero<br> [0x80000718]:sw ra, 48(t1)<br>   |
|  32|[0x80002500]<br>0x0000000000000000|- rs1 : x1<br> - rd : x0<br>                                                                                        |[0x8000072c]:fcvt.s.lu zero, ra, dyn<br> [0x80000730]:csrrs a1, fcsr, zero<br> [0x80000734]:sw zero, 64(t1)<br> |
