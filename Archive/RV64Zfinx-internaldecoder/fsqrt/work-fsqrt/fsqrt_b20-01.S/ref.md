
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000b10')]      |
| SIG_REGION                | [('0x80002410', '0x80002640', '70 dwords')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fsqrt/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 132     |
| Total Coverpoints Hit     | 132      |
| Total Signature Updates   | 96      |
| STAT1                     | 48      |
| STAT2                     | 0      |
| STAT3                     | 18     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000908]:fsqrt.s t6, t5, dyn
[0x8000090c]:csrrs a0, fcsr, zero
[0x80000910]:sd t6, 352(t2)
[0x80000914]:sd a0, 360(t2)
[0x80000918]:ld t5, 200(s1)
[0x8000091c]:addi fp, zero, 0
[0x80000920]:csrrw zero, fcsr, fp
[0x80000924]:fsqrt.s t6, t5, dyn
[0x80000928]:csrrs a0, fcsr, zero

[0x80000924]:fsqrt.s t6, t5, dyn
[0x80000928]:csrrs a0, fcsr, zero
[0x8000092c]:sd t6, 368(t2)
[0x80000930]:sd a0, 376(t2)
[0x80000934]:ld t5, 208(s1)
[0x80000938]:addi fp, zero, 0
[0x8000093c]:csrrw zero, fcsr, fp
[0x80000940]:fsqrt.s t6, t5, dyn
[0x80000944]:csrrs a0, fcsr, zero

[0x80000940]:fsqrt.s t6, t5, dyn
[0x80000944]:csrrs a0, fcsr, zero
[0x80000948]:sd t6, 384(t2)
[0x8000094c]:sd a0, 392(t2)
[0x80000950]:ld t5, 216(s1)
[0x80000954]:addi fp, zero, 0
[0x80000958]:csrrw zero, fcsr, fp
[0x8000095c]:fsqrt.s t6, t5, dyn
[0x80000960]:csrrs a0, fcsr, zero

[0x8000095c]:fsqrt.s t6, t5, dyn
[0x80000960]:csrrs a0, fcsr, zero
[0x80000964]:sd t6, 400(t2)
[0x80000968]:sd a0, 408(t2)
[0x8000096c]:ld t5, 224(s1)
[0x80000970]:addi fp, zero, 0
[0x80000974]:csrrw zero, fcsr, fp
[0x80000978]:fsqrt.s t6, t5, dyn
[0x8000097c]:csrrs a0, fcsr, zero

[0x80000978]:fsqrt.s t6, t5, dyn
[0x8000097c]:csrrs a0, fcsr, zero
[0x80000980]:sd t6, 416(t2)
[0x80000984]:sd a0, 424(t2)
[0x80000988]:ld t5, 232(s1)
[0x8000098c]:addi fp, zero, 0
[0x80000990]:csrrw zero, fcsr, fp
[0x80000994]:fsqrt.s t6, t5, dyn
[0x80000998]:csrrs a0, fcsr, zero

[0x80000994]:fsqrt.s t6, t5, dyn
[0x80000998]:csrrs a0, fcsr, zero
[0x8000099c]:sd t6, 432(t2)
[0x800009a0]:sd a0, 440(t2)
[0x800009a4]:ld t5, 240(s1)
[0x800009a8]:addi fp, zero, 0
[0x800009ac]:csrrw zero, fcsr, fp
[0x800009b0]:fsqrt.s t6, t5, dyn
[0x800009b4]:csrrs a0, fcsr, zero

[0x800009b0]:fsqrt.s t6, t5, dyn
[0x800009b4]:csrrs a0, fcsr, zero
[0x800009b8]:sd t6, 448(t2)
[0x800009bc]:sd a0, 456(t2)
[0x800009c0]:ld t5, 248(s1)
[0x800009c4]:addi fp, zero, 0
[0x800009c8]:csrrw zero, fcsr, fp
[0x800009cc]:fsqrt.s t6, t5, dyn
[0x800009d0]:csrrs a0, fcsr, zero

[0x800009cc]:fsqrt.s t6, t5, dyn
[0x800009d0]:csrrs a0, fcsr, zero
[0x800009d4]:sd t6, 464(t2)
[0x800009d8]:sd a0, 472(t2)
[0x800009dc]:ld t5, 256(s1)
[0x800009e0]:addi fp, zero, 0
[0x800009e4]:csrrw zero, fcsr, fp
[0x800009e8]:fsqrt.s t6, t5, dyn
[0x800009ec]:csrrs a0, fcsr, zero

[0x800009e8]:fsqrt.s t6, t5, dyn
[0x800009ec]:csrrs a0, fcsr, zero
[0x800009f0]:sd t6, 480(t2)
[0x800009f4]:sd a0, 488(t2)
[0x800009f8]:ld t5, 264(s1)
[0x800009fc]:addi fp, zero, 0
[0x80000a00]:csrrw zero, fcsr, fp
[0x80000a04]:fsqrt.s t6, t5, dyn
[0x80000a08]:csrrs a0, fcsr, zero

[0x80000a04]:fsqrt.s t6, t5, dyn
[0x80000a08]:csrrs a0, fcsr, zero
[0x80000a0c]:sd t6, 496(t2)
[0x80000a10]:sd a0, 504(t2)
[0x80000a14]:ld t5, 272(s1)
[0x80000a18]:addi fp, zero, 0
[0x80000a1c]:csrrw zero, fcsr, fp
[0x80000a20]:fsqrt.s t6, t5, dyn
[0x80000a24]:csrrs a0, fcsr, zero

[0x80000a20]:fsqrt.s t6, t5, dyn
[0x80000a24]:csrrs a0, fcsr, zero
[0x80000a28]:sd t6, 512(t2)
[0x80000a2c]:sd a0, 520(t2)
[0x80000a30]:ld t5, 280(s1)
[0x80000a34]:addi fp, zero, 0
[0x80000a38]:csrrw zero, fcsr, fp
[0x80000a3c]:fsqrt.s t6, t5, dyn
[0x80000a40]:csrrs a0, fcsr, zero

[0x80000a3c]:fsqrt.s t6, t5, dyn
[0x80000a40]:csrrs a0, fcsr, zero
[0x80000a44]:sd t6, 528(t2)
[0x80000a48]:sd a0, 536(t2)
[0x80000a4c]:ld t5, 288(s1)
[0x80000a50]:addi fp, zero, 0
[0x80000a54]:csrrw zero, fcsr, fp
[0x80000a58]:fsqrt.s t6, t5, dyn
[0x80000a5c]:csrrs a0, fcsr, zero

[0x80000a58]:fsqrt.s t6, t5, dyn
[0x80000a5c]:csrrs a0, fcsr, zero
[0x80000a60]:sd t6, 544(t2)
[0x80000a64]:sd a0, 552(t2)
[0x80000a68]:ld t5, 296(s1)
[0x80000a6c]:addi fp, zero, 0
[0x80000a70]:csrrw zero, fcsr, fp
[0x80000a74]:fsqrt.s t6, t5, dyn
[0x80000a78]:csrrs a0, fcsr, zero

[0x80000a74]:fsqrt.s t6, t5, dyn
[0x80000a78]:csrrs a0, fcsr, zero
[0x80000a7c]:sd t6, 560(t2)
[0x80000a80]:sd a0, 568(t2)
[0x80000a84]:ld t5, 304(s1)
[0x80000a88]:addi fp, zero, 0
[0x80000a8c]:csrrw zero, fcsr, fp
[0x80000a90]:fsqrt.s t6, t5, dyn
[0x80000a94]:csrrs a0, fcsr, zero

[0x80000a90]:fsqrt.s t6, t5, dyn
[0x80000a94]:csrrs a0, fcsr, zero
[0x80000a98]:sd t6, 576(t2)
[0x80000a9c]:sd a0, 584(t2)
[0x80000aa0]:ld t5, 312(s1)
[0x80000aa4]:addi fp, zero, 0
[0x80000aa8]:csrrw zero, fcsr, fp
[0x80000aac]:fsqrt.s t6, t5, dyn
[0x80000ab0]:csrrs a0, fcsr, zero

[0x80000aac]:fsqrt.s t6, t5, dyn
[0x80000ab0]:csrrs a0, fcsr, zero
[0x80000ab4]:sd t6, 592(t2)
[0x80000ab8]:sd a0, 600(t2)
[0x80000abc]:ld t5, 320(s1)
[0x80000ac0]:addi fp, zero, 0
[0x80000ac4]:csrrw zero, fcsr, fp
[0x80000ac8]:fsqrt.s t6, t5, dyn
[0x80000acc]:csrrs a0, fcsr, zero

[0x80000ac8]:fsqrt.s t6, t5, dyn
[0x80000acc]:csrrs a0, fcsr, zero
[0x80000ad0]:sd t6, 608(t2)
[0x80000ad4]:sd a0, 616(t2)
[0x80000ad8]:ld t5, 328(s1)
[0x80000adc]:addi fp, zero, 0
[0x80000ae0]:csrrw zero, fcsr, fp
[0x80000ae4]:fsqrt.s t6, t5, dyn
[0x80000ae8]:csrrs a0, fcsr, zero

[0x80000ae4]:fsqrt.s t6, t5, dyn
[0x80000ae8]:csrrs a0, fcsr, zero
[0x80000aec]:sd t6, 624(t2)
[0x80000af0]:sd a0, 632(t2)
[0x80000af4]:ld t5, 336(s1)
[0x80000af8]:addi fp, zero, 0
[0x80000afc]:csrrw zero, fcsr, fp
[0x80000b00]:fsqrt.s t6, t5, dyn
[0x80000b04]:csrrs a0, fcsr, zero



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                             coverpoints                                                                                              |                                                                     code                                                                     |
|---:|------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002418]<br>0x000000005F7FFFFF<br> [0x80002420]<br>0x0000000000000001<br> |- mnemonic : fsqrt.s<br> - rs1 : x31<br> - rd : x31<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br> |[0x800003b8]:fsqrt.s t6, t6, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br> [0x800003c4]:sd tp, 8(ra)<br>        |
|   2|[0x80002428]<br>0x0000000000000000<br> [0x80002430]<br>0x0000000000000000<br> |- rs1 : x29<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                          |[0x800003d4]:fsqrt.s t5, t4, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br> [0x800003e0]:sd tp, 24(ra)<br>      |
|   3|[0x80002438]<br>0x000000002E200000<br> [0x80002440]<br>0x0000000000000000<br> |- rs1 : x30<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800003f0]:fsqrt.s t4, t5, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br> [0x800003fc]:sd tp, 40(ra)<br>      |
|   4|[0x80002448]<br>0x000000005E200000<br> [0x80002450]<br>0x0000000000000000<br> |- rs1 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000040c]:fsqrt.s t3, s11, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br> [0x80000418]:sd tp, 56(ra)<br>     |
|   5|[0x80002458]<br>0x000000004DB00000<br> [0x80002460]<br>0x0000000000000000<br> |- rs1 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000428]:fsqrt.s s11, t3, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br> [0x80000434]:sd tp, 72(ra)<br>    |
|   6|[0x80002468]<br>0x0000000051B00000<br> [0x80002470]<br>0x0000000000000000<br> |- rs1 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000444]:fsqrt.s s10, s9, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br> [0x80000450]:sd tp, 88(ra)<br>    |
|   7|[0x80002478]<br>0x000000005CF00000<br> [0x80002480]<br>0x0000000000000000<br> |- rs1 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000460]:fsqrt.s s9, s10, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br> [0x8000046c]:sd tp, 104(ra)<br>    |
|   8|[0x80002488]<br>0x0000000041780000<br> [0x80002490]<br>0x0000000000000000<br> |- rs1 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000047c]:fsqrt.s s8, s7, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br> [0x80000488]:sd tp, 120(ra)<br>    |
|   9|[0x80002498]<br>0x0000000045A80000<br> [0x800024a0]<br>0x0000000000000000<br> |- rs1 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000498]:fsqrt.s s7, s8, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br> [0x800004a4]:sd tp, 136(ra)<br>    |
|  10|[0x800024a8]<br>0x0000000040380000<br> [0x800024b0]<br>0x0000000000000000<br> |- rs1 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004b4]:fsqrt.s s6, s5, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br> [0x800004c0]:sd tp, 152(ra)<br>    |
|  11|[0x800024b8]<br>0x000000002E880000<br> [0x800024c0]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004d0]:fsqrt.s s5, s6, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br> [0x800004dc]:sd tp, 168(ra)<br>    |
|  12|[0x800024c8]<br>0x0000000028F40000<br> [0x800024d0]<br>0x0000000000000000<br> |- rs1 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004ec]:fsqrt.s s4, s3, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br> [0x800004f8]:sd tp, 184(ra)<br>    |
|  13|[0x800024d8]<br>0x0000000036340000<br> [0x800024e0]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000508]:fsqrt.s s3, s4, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br> [0x80000514]:sd tp, 200(ra)<br>    |
|  14|[0x800024e8]<br>0x0000000043740000<br> [0x800024f0]<br>0x0000000000000000<br> |- rs1 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000524]:fsqrt.s s2, a7, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br> [0x80000530]:sd tp, 216(ra)<br>    |
|  15|[0x800024f8]<br>0x0000000041420000<br> [0x80002500]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000540]:fsqrt.s a7, s2, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br> [0x8000054c]:sd tp, 232(ra)<br>    |
|  16|[0x80002508]<br>0x0000000041FE0000<br> [0x80002510]<br>0x0000000000000000<br> |- rs1 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000055c]:fsqrt.s a6, a5, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br> [0x80000568]:sd tp, 248(ra)<br>    |
|  17|[0x80002518]<br>0x0000000039720000<br> [0x80002520]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000578]:fsqrt.s a5, a6, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br> [0x80000584]:sd tp, 264(ra)<br>    |
|  18|[0x80002528]<br>0x000000005E350000<br> [0x80002530]<br>0x0000000000000000<br> |- rs1 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000594]:fsqrt.s a4, a3, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br> [0x800005a0]:sd tp, 280(ra)<br>    |
|  19|[0x80002538]<br>0x000000002E2D0000<br> [0x80002540]<br>0x0000000000000000<br> |- rs1 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005b0]:fsqrt.s a3, a4, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sd a3, 288(ra)<br> [0x800005bc]:sd tp, 296(ra)<br>    |
|  20|[0x80002548]<br>0x000000005BB38000<br> [0x80002550]<br>0x0000000000000000<br> |- rs1 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005cc]:fsqrt.s a2, a1, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sd a2, 304(ra)<br> [0x800005d8]:sd tp, 312(ra)<br>    |
|  21|[0x80002558]<br>0x0000000053BB8000<br> [0x80002560]<br>0x0000000000000000<br> |- rs1 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005e8]:fsqrt.s a1, a2, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br> [0x800005f4]:sd tp, 328(ra)<br>    |
|  22|[0x80002568]<br>0x000000003E9F8000<br> [0x80002570]<br>0x0000000000000000<br> |- rs1 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                           |[0x80000604]:fsqrt.s a0, s1, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br> [0x80000610]:sd tp, 344(ra)<br>    |
|  23|[0x80002578]<br>0x000000003FF68000<br> [0x80002580]<br>0x0000000000000000<br> |- rs1 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                           |[0x80000620]:fsqrt.s s1, a0, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br> [0x8000062c]:sd tp, 360(ra)<br>    |
|  24|[0x80002588]<br>0x0000000054AD4000<br> [0x80002590]<br>0x0000000000000000<br> |- rs1 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x8000063c]:fsqrt.s fp, t2, dyn<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd fp, 368(ra)<br> [0x80000648]:sd tp, 376(ra)<br>    |
|  25|[0x80002598]<br>0x000000002CBB4000<br> [0x800025a0]<br>0x0000000000000000<br> |- rs1 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x80000660]:fsqrt.s t2, fp, dyn<br> [0x80000664]:csrrs a0, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br> [0x8000066c]:sd a0, 392(ra)<br>    |
|  26|[0x800025a8]<br>0x000000003A5CA000<br> [0x800025b0]<br>0x0000000000000000<br> |- rs1 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x8000067c]:fsqrt.s t1, t0, dyn<br> [0x80000680]:csrrs a0, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br> [0x80000688]:sd a0, 408(ra)<br>    |
|  27|[0x800024e8]<br>0x000000001C544395<br> [0x800024f0]<br>0x0000000000000001<br> |- rs1 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006a0]:fsqrt.s t0, t1, dyn<br> [0x800006a4]:csrrs a0, fcsr, zero<br> [0x800006a8]:sd t0, 0(t2)<br> [0x800006ac]:sd a0, 8(t2)<br>        |
|  28|[0x800024f8]<br>0x000000004EF42000<br> [0x80002500]<br>0x0000000000000000<br> |- rs1 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006bc]:fsqrt.s tp, gp, dyn<br> [0x800006c0]:csrrs a0, fcsr, zero<br> [0x800006c4]:sd tp, 16(t2)<br> [0x800006c8]:sd a0, 24(t2)<br>      |
|  29|[0x80002508]<br>0x0000000026A79000<br> [0x80002510]<br>0x0000000000000000<br> |- rs1 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006d8]:fsqrt.s gp, tp, dyn<br> [0x800006dc]:csrrs a0, fcsr, zero<br> [0x800006e0]:sd gp, 32(t2)<br> [0x800006e4]:sd a0, 40(t2)<br>      |
|  30|[0x80002518]<br>0x000000005A665000<br> [0x80002520]<br>0x0000000000000000<br> |- rs1 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006f4]:fsqrt.s sp, ra, dyn<br> [0x800006f8]:csrrs a0, fcsr, zero<br> [0x800006fc]:sd sp, 48(t2)<br> [0x80000700]:sd a0, 56(t2)<br>      |
|  31|[0x80002528]<br>0x0000000047C09000<br> [0x80002530]<br>0x0000000000000000<br> |- rs1 : x2<br> - rd : x1<br> - fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x80000710]:fsqrt.s ra, sp, dyn<br> [0x80000714]:csrrs a0, fcsr, zero<br> [0x80000718]:sd ra, 64(t2)<br> [0x8000071c]:sd a0, 72(t2)<br>      |
|  32|[0x80002538]<br>0x0000000000000000<br> [0x80002540]<br>0x0000000000000000<br> |- rs1 : x0<br>                                                                                                                                                                                        |[0x8000072c]:fsqrt.s t6, zero, dyn<br> [0x80000730]:csrrs a0, fcsr, zero<br> [0x80000734]:sd t6, 80(t2)<br> [0x80000738]:sd a0, 88(t2)<br>    |
|  33|[0x80002548]<br>0x0000000000000000<br> [0x80002550]<br>0x0000000000000001<br> |- rd : x0<br> - fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                           |[0x80000748]:fsqrt.s zero, t6, dyn<br> [0x8000074c]:csrrs a0, fcsr, zero<br> [0x80000750]:sd zero, 96(t2)<br> [0x80000754]:sd a0, 104(t2)<br> |
|  34|[0x80002558]<br>0x000000001ACA62C2<br> [0x80002560]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000764]:fsqrt.s t6, t5, dyn<br> [0x80000768]:csrrs a0, fcsr, zero<br> [0x8000076c]:sd t6, 112(t2)<br> [0x80000770]:sd a0, 120(t2)<br>    |
|  35|[0x80002568]<br>0x000000002EC18800<br> [0x80002570]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000780]:fsqrt.s t6, t5, dyn<br> [0x80000784]:csrrs a0, fcsr, zero<br> [0x80000788]:sd t6, 128(t2)<br> [0x8000078c]:sd a0, 136(t2)<br>    |
|  36|[0x80002578]<br>0x000000004FD79800<br> [0x80002580]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000079c]:fsqrt.s t6, t5, dyn<br> [0x800007a0]:csrrs a0, fcsr, zero<br> [0x800007a4]:sd t6, 144(t2)<br> [0x800007a8]:sd a0, 152(t2)<br>    |
|  37|[0x80002588]<br>0x000000005CABB400<br> [0x80002590]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007b8]:fsqrt.s t6, t5, dyn<br> [0x800007bc]:csrrs a0, fcsr, zero<br> [0x800007c0]:sd t6, 160(t2)<br> [0x800007c4]:sd a0, 168(t2)<br>    |
|  38|[0x80002598]<br>0x000000005F7F9C00<br> [0x800025a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007d4]:fsqrt.s t6, t5, dyn<br> [0x800007d8]:csrrs a0, fcsr, zero<br> [0x800007dc]:sd t6, 176(t2)<br> [0x800007e0]:sd a0, 184(t2)<br>    |
|  39|[0x800025a8]<br>0x0000000049878400<br> [0x800025b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007f0]:fsqrt.s t6, t5, dyn<br> [0x800007f4]:csrrs a0, fcsr, zero<br> [0x800007f8]:sd t6, 192(t2)<br> [0x800007fc]:sd a0, 200(t2)<br>    |
|  40|[0x800025b8]<br>0x0000000022864400<br> [0x800025c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000080c]:fsqrt.s t6, t5, dyn<br> [0x80000810]:csrrs a0, fcsr, zero<br> [0x80000814]:sd t6, 208(t2)<br> [0x80000818]:sd a0, 216(t2)<br>    |
|  41|[0x800025c8]<br>0x00000000571D0200<br> [0x800025d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000828]:fsqrt.s t6, t5, dyn<br> [0x8000082c]:csrrs a0, fcsr, zero<br> [0x80000830]:sd t6, 224(t2)<br> [0x80000834]:sd a0, 232(t2)<br>    |
|  42|[0x800025d8]<br>0x000000001F220600<br> [0x800025e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000844]:fsqrt.s t6, t5, dyn<br> [0x80000848]:csrrs a0, fcsr, zero<br> [0x8000084c]:sd t6, 240(t2)<br> [0x80000850]:sd a0, 248(t2)<br>    |
|  43|[0x800025e8]<br>0x000000003607BB00<br> [0x800025f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000860]:fsqrt.s t6, t5, dyn<br> [0x80000864]:csrrs a0, fcsr, zero<br> [0x80000868]:sd t6, 256(t2)<br> [0x8000086c]:sd a0, 264(t2)<br>    |
|  44|[0x800025f8]<br>0x000000002E074500<br> [0x80002600]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000087c]:fsqrt.s t6, t5, dyn<br> [0x80000880]:csrrs a0, fcsr, zero<br> [0x80000884]:sd t6, 272(t2)<br> [0x80000888]:sd a0, 280(t2)<br>    |
|  45|[0x80002608]<br>0x000000001F960100<br> [0x80002610]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000898]:fsqrt.s t6, t5, dyn<br> [0x8000089c]:csrrs a0, fcsr, zero<br> [0x800008a0]:sd t6, 288(t2)<br> [0x800008a4]:sd a0, 296(t2)<br>    |
|  46|[0x80002618]<br>0x000000005D053B80<br> [0x80002620]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008b4]:fsqrt.s t6, t5, dyn<br> [0x800008b8]:csrrs a0, fcsr, zero<br> [0x800008bc]:sd t6, 304(t2)<br> [0x800008c0]:sd a0, 312(t2)<br>    |
|  47|[0x80002628]<br>0x000000002F82B480<br> [0x80002630]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008d0]:fsqrt.s t6, t5, dyn<br> [0x800008d4]:csrrs a0, fcsr, zero<br> [0x800008d8]:sd t6, 320(t2)<br> [0x800008dc]:sd a0, 328(t2)<br>    |
|  48|[0x80002638]<br>0x000000005E141C80<br> [0x80002640]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008ec]:fsqrt.s t6, t5, dyn<br> [0x800008f0]:csrrs a0, fcsr, zero<br> [0x800008f4]:sd t6, 336(t2)<br> [0x800008f8]:sd a0, 344(t2)<br>    |
