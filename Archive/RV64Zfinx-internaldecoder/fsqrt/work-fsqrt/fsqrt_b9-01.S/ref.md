
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80001930')]      |
| SIG_REGION                | [('0x80003810', '0x80003e40', '198 dwords')]      |
| COV_LABELS                | fsqrt_b9      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fsqrt/fsqrt_b9-01.S/ref.S    |
| Total Number of coverpoints| 261     |
| Total Coverpoints Hit     | 261      |
| Total Signature Updates   | 224      |
| STAT1                     | 112      |
| STAT2                     | 0      |
| STAT3                     | 83     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001008]:fsqrt.s t6, t5, dyn
[0x8000100c]:csrrs a0, fcsr, zero
[0x80001010]:sd t6, 1376(t2)
[0x80001014]:sd a0, 1384(t2)
[0x80001018]:ld t5, 712(s1)
[0x8000101c]:addi fp, zero, 0
[0x80001020]:csrrw zero, fcsr, fp
[0x80001024]:fsqrt.s t6, t5, dyn
[0x80001028]:csrrs a0, fcsr, zero

[0x80001024]:fsqrt.s t6, t5, dyn
[0x80001028]:csrrs a0, fcsr, zero
[0x8000102c]:sd t6, 1392(t2)
[0x80001030]:sd a0, 1400(t2)
[0x80001034]:ld t5, 720(s1)
[0x80001038]:addi fp, zero, 0
[0x8000103c]:csrrw zero, fcsr, fp
[0x80001040]:fsqrt.s t6, t5, dyn
[0x80001044]:csrrs a0, fcsr, zero

[0x80001040]:fsqrt.s t6, t5, dyn
[0x80001044]:csrrs a0, fcsr, zero
[0x80001048]:sd t6, 1408(t2)
[0x8000104c]:sd a0, 1416(t2)
[0x80001050]:ld t5, 728(s1)
[0x80001054]:addi fp, zero, 0
[0x80001058]:csrrw zero, fcsr, fp
[0x8000105c]:fsqrt.s t6, t5, dyn
[0x80001060]:csrrs a0, fcsr, zero

[0x8000105c]:fsqrt.s t6, t5, dyn
[0x80001060]:csrrs a0, fcsr, zero
[0x80001064]:sd t6, 1424(t2)
[0x80001068]:sd a0, 1432(t2)
[0x8000106c]:ld t5, 736(s1)
[0x80001070]:addi fp, zero, 0
[0x80001074]:csrrw zero, fcsr, fp
[0x80001078]:fsqrt.s t6, t5, dyn
[0x8000107c]:csrrs a0, fcsr, zero

[0x80001078]:fsqrt.s t6, t5, dyn
[0x8000107c]:csrrs a0, fcsr, zero
[0x80001080]:sd t6, 1440(t2)
[0x80001084]:sd a0, 1448(t2)
[0x80001088]:ld t5, 744(s1)
[0x8000108c]:addi fp, zero, 0
[0x80001090]:csrrw zero, fcsr, fp
[0x80001094]:fsqrt.s t6, t5, dyn
[0x80001098]:csrrs a0, fcsr, zero

[0x80001094]:fsqrt.s t6, t5, dyn
[0x80001098]:csrrs a0, fcsr, zero
[0x8000109c]:sd t6, 1456(t2)
[0x800010a0]:sd a0, 1464(t2)
[0x800010a4]:ld t5, 752(s1)
[0x800010a8]:addi fp, zero, 0
[0x800010ac]:csrrw zero, fcsr, fp
[0x800010b0]:fsqrt.s t6, t5, dyn
[0x800010b4]:csrrs a0, fcsr, zero

[0x800010b0]:fsqrt.s t6, t5, dyn
[0x800010b4]:csrrs a0, fcsr, zero
[0x800010b8]:sd t6, 1472(t2)
[0x800010bc]:sd a0, 1480(t2)
[0x800010c0]:ld t5, 760(s1)
[0x800010c4]:addi fp, zero, 0
[0x800010c8]:csrrw zero, fcsr, fp
[0x800010cc]:fsqrt.s t6, t5, dyn
[0x800010d0]:csrrs a0, fcsr, zero

[0x800010cc]:fsqrt.s t6, t5, dyn
[0x800010d0]:csrrs a0, fcsr, zero
[0x800010d4]:sd t6, 1488(t2)
[0x800010d8]:sd a0, 1496(t2)
[0x800010dc]:ld t5, 768(s1)
[0x800010e0]:addi fp, zero, 0
[0x800010e4]:csrrw zero, fcsr, fp
[0x800010e8]:fsqrt.s t6, t5, dyn
[0x800010ec]:csrrs a0, fcsr, zero

[0x800010e8]:fsqrt.s t6, t5, dyn
[0x800010ec]:csrrs a0, fcsr, zero
[0x800010f0]:sd t6, 1504(t2)
[0x800010f4]:sd a0, 1512(t2)
[0x800010f8]:ld t5, 776(s1)
[0x800010fc]:addi fp, zero, 0
[0x80001100]:csrrw zero, fcsr, fp
[0x80001104]:fsqrt.s t6, t5, dyn
[0x80001108]:csrrs a0, fcsr, zero

[0x80001104]:fsqrt.s t6, t5, dyn
[0x80001108]:csrrs a0, fcsr, zero
[0x8000110c]:sd t6, 1520(t2)
[0x80001110]:sd a0, 1528(t2)
[0x80001114]:ld t5, 784(s1)
[0x80001118]:addi fp, zero, 0
[0x8000111c]:csrrw zero, fcsr, fp
[0x80001120]:fsqrt.s t6, t5, dyn
[0x80001124]:csrrs a0, fcsr, zero

[0x80001120]:fsqrt.s t6, t5, dyn
[0x80001124]:csrrs a0, fcsr, zero
[0x80001128]:sd t6, 1536(t2)
[0x8000112c]:sd a0, 1544(t2)
[0x80001130]:ld t5, 792(s1)
[0x80001134]:addi fp, zero, 0
[0x80001138]:csrrw zero, fcsr, fp
[0x8000113c]:fsqrt.s t6, t5, dyn
[0x80001140]:csrrs a0, fcsr, zero

[0x8000113c]:fsqrt.s t6, t5, dyn
[0x80001140]:csrrs a0, fcsr, zero
[0x80001144]:sd t6, 1552(t2)
[0x80001148]:sd a0, 1560(t2)
[0x8000114c]:ld t5, 800(s1)
[0x80001150]:addi fp, zero, 0
[0x80001154]:csrrw zero, fcsr, fp
[0x80001158]:fsqrt.s t6, t5, dyn
[0x8000115c]:csrrs a0, fcsr, zero

[0x80001158]:fsqrt.s t6, t5, dyn
[0x8000115c]:csrrs a0, fcsr, zero
[0x80001160]:sd t6, 1568(t2)
[0x80001164]:sd a0, 1576(t2)
[0x80001168]:ld t5, 808(s1)
[0x8000116c]:addi fp, zero, 0
[0x80001170]:csrrw zero, fcsr, fp
[0x80001174]:fsqrt.s t6, t5, dyn
[0x80001178]:csrrs a0, fcsr, zero

[0x80001174]:fsqrt.s t6, t5, dyn
[0x80001178]:csrrs a0, fcsr, zero
[0x8000117c]:sd t6, 1584(t2)
[0x80001180]:sd a0, 1592(t2)
[0x80001184]:ld t5, 816(s1)
[0x80001188]:addi fp, zero, 0
[0x8000118c]:csrrw zero, fcsr, fp
[0x80001190]:fsqrt.s t6, t5, dyn
[0x80001194]:csrrs a0, fcsr, zero

[0x80001190]:fsqrt.s t6, t5, dyn
[0x80001194]:csrrs a0, fcsr, zero
[0x80001198]:sd t6, 1600(t2)
[0x8000119c]:sd a0, 1608(t2)
[0x800011a0]:ld t5, 824(s1)
[0x800011a4]:addi fp, zero, 0
[0x800011a8]:csrrw zero, fcsr, fp
[0x800011ac]:fsqrt.s t6, t5, dyn
[0x800011b0]:csrrs a0, fcsr, zero

[0x800011ac]:fsqrt.s t6, t5, dyn
[0x800011b0]:csrrs a0, fcsr, zero
[0x800011b4]:sd t6, 1616(t2)
[0x800011b8]:sd a0, 1624(t2)
[0x800011bc]:ld t5, 832(s1)
[0x800011c0]:addi fp, zero, 0
[0x800011c4]:csrrw zero, fcsr, fp
[0x800011c8]:fsqrt.s t6, t5, dyn
[0x800011cc]:csrrs a0, fcsr, zero

[0x800011c8]:fsqrt.s t6, t5, dyn
[0x800011cc]:csrrs a0, fcsr, zero
[0x800011d0]:sd t6, 1632(t2)
[0x800011d4]:sd a0, 1640(t2)
[0x800011d8]:ld t5, 840(s1)
[0x800011dc]:addi fp, zero, 0
[0x800011e0]:csrrw zero, fcsr, fp
[0x800011e4]:fsqrt.s t6, t5, dyn
[0x800011e8]:csrrs a0, fcsr, zero

[0x800011e4]:fsqrt.s t6, t5, dyn
[0x800011e8]:csrrs a0, fcsr, zero
[0x800011ec]:sd t6, 1648(t2)
[0x800011f0]:sd a0, 1656(t2)
[0x800011f4]:ld t5, 848(s1)
[0x800011f8]:addi fp, zero, 0
[0x800011fc]:csrrw zero, fcsr, fp
[0x80001200]:fsqrt.s t6, t5, dyn
[0x80001204]:csrrs a0, fcsr, zero

[0x80001200]:fsqrt.s t6, t5, dyn
[0x80001204]:csrrs a0, fcsr, zero
[0x80001208]:sd t6, 1664(t2)
[0x8000120c]:sd a0, 1672(t2)
[0x80001210]:ld t5, 856(s1)
[0x80001214]:addi fp, zero, 0
[0x80001218]:csrrw zero, fcsr, fp
[0x8000121c]:fsqrt.s t6, t5, dyn
[0x80001220]:csrrs a0, fcsr, zero

[0x8000121c]:fsqrt.s t6, t5, dyn
[0x80001220]:csrrs a0, fcsr, zero
[0x80001224]:sd t6, 1680(t2)
[0x80001228]:sd a0, 1688(t2)
[0x8000122c]:ld t5, 864(s1)
[0x80001230]:addi fp, zero, 0
[0x80001234]:csrrw zero, fcsr, fp
[0x80001238]:fsqrt.s t6, t5, dyn
[0x8000123c]:csrrs a0, fcsr, zero

[0x80001238]:fsqrt.s t6, t5, dyn
[0x8000123c]:csrrs a0, fcsr, zero
[0x80001240]:sd t6, 1696(t2)
[0x80001244]:sd a0, 1704(t2)
[0x80001248]:ld t5, 872(s1)
[0x8000124c]:addi fp, zero, 0
[0x80001250]:csrrw zero, fcsr, fp
[0x80001254]:fsqrt.s t6, t5, dyn
[0x80001258]:csrrs a0, fcsr, zero

[0x80001254]:fsqrt.s t6, t5, dyn
[0x80001258]:csrrs a0, fcsr, zero
[0x8000125c]:sd t6, 1712(t2)
[0x80001260]:sd a0, 1720(t2)
[0x80001264]:ld t5, 880(s1)
[0x80001268]:addi fp, zero, 0
[0x8000126c]:csrrw zero, fcsr, fp
[0x80001270]:fsqrt.s t6, t5, dyn
[0x80001274]:csrrs a0, fcsr, zero

[0x80001270]:fsqrt.s t6, t5, dyn
[0x80001274]:csrrs a0, fcsr, zero
[0x80001278]:sd t6, 1728(t2)
[0x8000127c]:sd a0, 1736(t2)
[0x80001280]:ld t5, 888(s1)
[0x80001284]:addi fp, zero, 0
[0x80001288]:csrrw zero, fcsr, fp
[0x8000128c]:fsqrt.s t6, t5, dyn
[0x80001290]:csrrs a0, fcsr, zero

[0x8000128c]:fsqrt.s t6, t5, dyn
[0x80001290]:csrrs a0, fcsr, zero
[0x80001294]:sd t6, 1744(t2)
[0x80001298]:sd a0, 1752(t2)
[0x8000129c]:ld t5, 896(s1)
[0x800012a0]:addi fp, zero, 0
[0x800012a4]:csrrw zero, fcsr, fp
[0x800012a8]:fsqrt.s t6, t5, dyn
[0x800012ac]:csrrs a0, fcsr, zero

[0x800012a8]:fsqrt.s t6, t5, dyn
[0x800012ac]:csrrs a0, fcsr, zero
[0x800012b0]:sd t6, 1760(t2)
[0x800012b4]:sd a0, 1768(t2)
[0x800012b8]:ld t5, 904(s1)
[0x800012bc]:addi fp, zero, 0
[0x800012c0]:csrrw zero, fcsr, fp
[0x800012c4]:fsqrt.s t6, t5, dyn
[0x800012c8]:csrrs a0, fcsr, zero

[0x800012c4]:fsqrt.s t6, t5, dyn
[0x800012c8]:csrrs a0, fcsr, zero
[0x800012cc]:sd t6, 1776(t2)
[0x800012d0]:sd a0, 1784(t2)
[0x800012d4]:ld t5, 912(s1)
[0x800012d8]:addi fp, zero, 0
[0x800012dc]:csrrw zero, fcsr, fp
[0x800012e0]:fsqrt.s t6, t5, dyn
[0x800012e4]:csrrs a0, fcsr, zero

[0x800012e0]:fsqrt.s t6, t5, dyn
[0x800012e4]:csrrs a0, fcsr, zero
[0x800012e8]:sd t6, 1792(t2)
[0x800012ec]:sd a0, 1800(t2)
[0x800012f0]:ld t5, 920(s1)
[0x800012f4]:addi fp, zero, 0
[0x800012f8]:csrrw zero, fcsr, fp
[0x800012fc]:fsqrt.s t6, t5, dyn
[0x80001300]:csrrs a0, fcsr, zero

[0x800012fc]:fsqrt.s t6, t5, dyn
[0x80001300]:csrrs a0, fcsr, zero
[0x80001304]:sd t6, 1808(t2)
[0x80001308]:sd a0, 1816(t2)
[0x8000130c]:ld t5, 928(s1)
[0x80001310]:addi fp, zero, 0
[0x80001314]:csrrw zero, fcsr, fp
[0x80001318]:fsqrt.s t6, t5, dyn
[0x8000131c]:csrrs a0, fcsr, zero

[0x80001318]:fsqrt.s t6, t5, dyn
[0x8000131c]:csrrs a0, fcsr, zero
[0x80001320]:sd t6, 1824(t2)
[0x80001324]:sd a0, 1832(t2)
[0x80001328]:ld t5, 936(s1)
[0x8000132c]:addi fp, zero, 0
[0x80001330]:csrrw zero, fcsr, fp
[0x80001334]:fsqrt.s t6, t5, dyn
[0x80001338]:csrrs a0, fcsr, zero

[0x80001334]:fsqrt.s t6, t5, dyn
[0x80001338]:csrrs a0, fcsr, zero
[0x8000133c]:sd t6, 1840(t2)
[0x80001340]:sd a0, 1848(t2)
[0x80001344]:ld t5, 944(s1)
[0x80001348]:addi fp, zero, 0
[0x8000134c]:csrrw zero, fcsr, fp
[0x80001350]:fsqrt.s t6, t5, dyn
[0x80001354]:csrrs a0, fcsr, zero

[0x80001350]:fsqrt.s t6, t5, dyn
[0x80001354]:csrrs a0, fcsr, zero
[0x80001358]:sd t6, 1856(t2)
[0x8000135c]:sd a0, 1864(t2)
[0x80001360]:ld t5, 952(s1)
[0x80001364]:addi fp, zero, 0
[0x80001368]:csrrw zero, fcsr, fp
[0x8000136c]:fsqrt.s t6, t5, dyn
[0x80001370]:csrrs a0, fcsr, zero

[0x8000136c]:fsqrt.s t6, t5, dyn
[0x80001370]:csrrs a0, fcsr, zero
[0x80001374]:sd t6, 1872(t2)
[0x80001378]:sd a0, 1880(t2)
[0x8000137c]:ld t5, 960(s1)
[0x80001380]:addi fp, zero, 0
[0x80001384]:csrrw zero, fcsr, fp
[0x80001388]:fsqrt.s t6, t5, dyn
[0x8000138c]:csrrs a0, fcsr, zero

[0x80001388]:fsqrt.s t6, t5, dyn
[0x8000138c]:csrrs a0, fcsr, zero
[0x80001390]:sd t6, 1888(t2)
[0x80001394]:sd a0, 1896(t2)
[0x80001398]:ld t5, 968(s1)
[0x8000139c]:addi fp, zero, 0
[0x800013a0]:csrrw zero, fcsr, fp
[0x800013a4]:fsqrt.s t6, t5, dyn
[0x800013a8]:csrrs a0, fcsr, zero

[0x800013a4]:fsqrt.s t6, t5, dyn
[0x800013a8]:csrrs a0, fcsr, zero
[0x800013ac]:sd t6, 1904(t2)
[0x800013b0]:sd a0, 1912(t2)
[0x800013b4]:ld t5, 976(s1)
[0x800013b8]:addi fp, zero, 0
[0x800013bc]:csrrw zero, fcsr, fp
[0x800013c0]:fsqrt.s t6, t5, dyn
[0x800013c4]:csrrs a0, fcsr, zero

[0x800013c0]:fsqrt.s t6, t5, dyn
[0x800013c4]:csrrs a0, fcsr, zero
[0x800013c8]:sd t6, 1920(t2)
[0x800013cc]:sd a0, 1928(t2)
[0x800013d0]:ld t5, 984(s1)
[0x800013d4]:addi fp, zero, 0
[0x800013d8]:csrrw zero, fcsr, fp
[0x800013dc]:fsqrt.s t6, t5, dyn
[0x800013e0]:csrrs a0, fcsr, zero

[0x800013dc]:fsqrt.s t6, t5, dyn
[0x800013e0]:csrrs a0, fcsr, zero
[0x800013e4]:sd t6, 1936(t2)
[0x800013e8]:sd a0, 1944(t2)
[0x800013ec]:ld t5, 992(s1)
[0x800013f0]:addi fp, zero, 0
[0x800013f4]:csrrw zero, fcsr, fp
[0x800013f8]:fsqrt.s t6, t5, dyn
[0x800013fc]:csrrs a0, fcsr, zero

[0x800013f8]:fsqrt.s t6, t5, dyn
[0x800013fc]:csrrs a0, fcsr, zero
[0x80001400]:sd t6, 1952(t2)
[0x80001404]:sd a0, 1960(t2)
[0x80001408]:ld t5, 1000(s1)
[0x8000140c]:addi fp, zero, 0
[0x80001410]:csrrw zero, fcsr, fp
[0x80001414]:fsqrt.s t6, t5, dyn
[0x80001418]:csrrs a0, fcsr, zero

[0x80001414]:fsqrt.s t6, t5, dyn
[0x80001418]:csrrs a0, fcsr, zero
[0x8000141c]:sd t6, 1968(t2)
[0x80001420]:sd a0, 1976(t2)
[0x80001424]:ld t5, 1008(s1)
[0x80001428]:addi fp, zero, 0
[0x8000142c]:csrrw zero, fcsr, fp
[0x80001430]:fsqrt.s t6, t5, dyn
[0x80001434]:csrrs a0, fcsr, zero

[0x80001430]:fsqrt.s t6, t5, dyn
[0x80001434]:csrrs a0, fcsr, zero
[0x80001438]:sd t6, 1984(t2)
[0x8000143c]:sd a0, 1992(t2)
[0x80001440]:ld t5, 1016(s1)
[0x80001444]:addi fp, zero, 0
[0x80001448]:csrrw zero, fcsr, fp
[0x8000144c]:fsqrt.s t6, t5, dyn
[0x80001450]:csrrs a0, fcsr, zero

[0x8000144c]:fsqrt.s t6, t5, dyn
[0x80001450]:csrrs a0, fcsr, zero
[0x80001454]:sd t6, 2000(t2)
[0x80001458]:sd a0, 2008(t2)
[0x8000145c]:ld t5, 1024(s1)
[0x80001460]:addi fp, zero, 0
[0x80001464]:csrrw zero, fcsr, fp
[0x80001468]:fsqrt.s t6, t5, dyn
[0x8000146c]:csrrs a0, fcsr, zero

[0x80001468]:fsqrt.s t6, t5, dyn
[0x8000146c]:csrrs a0, fcsr, zero
[0x80001470]:sd t6, 2016(t2)
[0x80001474]:sd a0, 2024(t2)
[0x80001478]:ld t5, 1032(s1)
[0x8000147c]:addi fp, zero, 0
[0x80001480]:csrrw zero, fcsr, fp
[0x80001484]:fsqrt.s t6, t5, dyn
[0x80001488]:csrrs a0, fcsr, zero

[0x80001484]:fsqrt.s t6, t5, dyn
[0x80001488]:csrrs a0, fcsr, zero
[0x8000148c]:sd t6, 2032(t2)
[0x80001490]:sd a0, 2040(t2)
[0x80001494]:ld t5, 1040(s1)
[0x80001498]:addi fp, zero, 0
[0x8000149c]:csrrw zero, fcsr, fp
[0x800014a0]:fsqrt.s t6, t5, dyn
[0x800014a4]:csrrs a0, fcsr, zero

[0x800014a0]:fsqrt.s t6, t5, dyn
[0x800014a4]:csrrs a0, fcsr, zero
[0x800014a8]:addi t2, t2, 2040
[0x800014ac]:sd t6, 8(t2)
[0x800014b0]:sd a0, 16(t2)
[0x800014b4]:ld t5, 1048(s1)
[0x800014b8]:addi fp, zero, 0
[0x800014bc]:csrrw zero, fcsr, fp
[0x800014c0]:fsqrt.s t6, t5, dyn
[0x800014c4]:csrrs a0, fcsr, zero

[0x800014c0]:fsqrt.s t6, t5, dyn
[0x800014c4]:csrrs a0, fcsr, zero
[0x800014c8]:sd t6, 24(t2)
[0x800014cc]:sd a0, 32(t2)
[0x800014d0]:ld t5, 1056(s1)
[0x800014d4]:addi fp, zero, 0
[0x800014d8]:csrrw zero, fcsr, fp
[0x800014dc]:fsqrt.s t6, t5, dyn
[0x800014e0]:csrrs a0, fcsr, zero

[0x800014dc]:fsqrt.s t6, t5, dyn
[0x800014e0]:csrrs a0, fcsr, zero
[0x800014e4]:sd t6, 40(t2)
[0x800014e8]:sd a0, 48(t2)
[0x800014ec]:ld t5, 1064(s1)
[0x800014f0]:addi fp, zero, 0
[0x800014f4]:csrrw zero, fcsr, fp
[0x800014f8]:fsqrt.s t6, t5, dyn
[0x800014fc]:csrrs a0, fcsr, zero

[0x800014f8]:fsqrt.s t6, t5, dyn
[0x800014fc]:csrrs a0, fcsr, zero
[0x80001500]:sd t6, 56(t2)
[0x80001504]:sd a0, 64(t2)
[0x80001508]:ld t5, 1072(s1)
[0x8000150c]:addi fp, zero, 0
[0x80001510]:csrrw zero, fcsr, fp
[0x80001514]:fsqrt.s t6, t5, dyn
[0x80001518]:csrrs a0, fcsr, zero

[0x80001514]:fsqrt.s t6, t5, dyn
[0x80001518]:csrrs a0, fcsr, zero
[0x8000151c]:sd t6, 72(t2)
[0x80001520]:sd a0, 80(t2)
[0x80001524]:ld t5, 1080(s1)
[0x80001528]:addi fp, zero, 0
[0x8000152c]:csrrw zero, fcsr, fp
[0x80001530]:fsqrt.s t6, t5, dyn
[0x80001534]:csrrs a0, fcsr, zero

[0x80001530]:fsqrt.s t6, t5, dyn
[0x80001534]:csrrs a0, fcsr, zero
[0x80001538]:sd t6, 88(t2)
[0x8000153c]:sd a0, 96(t2)
[0x80001540]:ld t5, 1088(s1)
[0x80001544]:addi fp, zero, 0
[0x80001548]:csrrw zero, fcsr, fp
[0x8000154c]:fsqrt.s t6, t5, dyn
[0x80001550]:csrrs a0, fcsr, zero

[0x8000154c]:fsqrt.s t6, t5, dyn
[0x80001550]:csrrs a0, fcsr, zero
[0x80001554]:sd t6, 104(t2)
[0x80001558]:sd a0, 112(t2)
[0x8000155c]:ld t5, 1096(s1)
[0x80001560]:addi fp, zero, 0
[0x80001564]:csrrw zero, fcsr, fp
[0x80001568]:fsqrt.s t6, t5, dyn
[0x8000156c]:csrrs a0, fcsr, zero

[0x80001568]:fsqrt.s t6, t5, dyn
[0x8000156c]:csrrs a0, fcsr, zero
[0x80001570]:sd t6, 120(t2)
[0x80001574]:sd a0, 128(t2)
[0x80001578]:ld t5, 1104(s1)
[0x8000157c]:addi fp, zero, 0
[0x80001580]:csrrw zero, fcsr, fp
[0x80001584]:fsqrt.s t6, t5, dyn
[0x80001588]:csrrs a0, fcsr, zero

[0x80001584]:fsqrt.s t6, t5, dyn
[0x80001588]:csrrs a0, fcsr, zero
[0x8000158c]:sd t6, 136(t2)
[0x80001590]:sd a0, 144(t2)
[0x80001594]:ld t5, 1112(s1)
[0x80001598]:addi fp, zero, 0
[0x8000159c]:csrrw zero, fcsr, fp
[0x800015a0]:fsqrt.s t6, t5, dyn
[0x800015a4]:csrrs a0, fcsr, zero

[0x800015a0]:fsqrt.s t6, t5, dyn
[0x800015a4]:csrrs a0, fcsr, zero
[0x800015a8]:sd t6, 152(t2)
[0x800015ac]:sd a0, 160(t2)
[0x800015b0]:ld t5, 1120(s1)
[0x800015b4]:addi fp, zero, 0
[0x800015b8]:csrrw zero, fcsr, fp
[0x800015bc]:fsqrt.s t6, t5, dyn
[0x800015c0]:csrrs a0, fcsr, zero

[0x800015bc]:fsqrt.s t6, t5, dyn
[0x800015c0]:csrrs a0, fcsr, zero
[0x800015c4]:sd t6, 168(t2)
[0x800015c8]:sd a0, 176(t2)
[0x800015cc]:ld t5, 1128(s1)
[0x800015d0]:addi fp, zero, 0
[0x800015d4]:csrrw zero, fcsr, fp
[0x800015d8]:fsqrt.s t6, t5, dyn
[0x800015dc]:csrrs a0, fcsr, zero

[0x800015d8]:fsqrt.s t6, t5, dyn
[0x800015dc]:csrrs a0, fcsr, zero
[0x800015e0]:sd t6, 184(t2)
[0x800015e4]:sd a0, 192(t2)
[0x800015e8]:ld t5, 1136(s1)
[0x800015ec]:addi fp, zero, 0
[0x800015f0]:csrrw zero, fcsr, fp
[0x800015f4]:fsqrt.s t6, t5, dyn
[0x800015f8]:csrrs a0, fcsr, zero

[0x800015f4]:fsqrt.s t6, t5, dyn
[0x800015f8]:csrrs a0, fcsr, zero
[0x800015fc]:sd t6, 200(t2)
[0x80001600]:sd a0, 208(t2)
[0x80001604]:ld t5, 1144(s1)
[0x80001608]:addi fp, zero, 0
[0x8000160c]:csrrw zero, fcsr, fp
[0x80001610]:fsqrt.s t6, t5, dyn
[0x80001614]:csrrs a0, fcsr, zero

[0x80001610]:fsqrt.s t6, t5, dyn
[0x80001614]:csrrs a0, fcsr, zero
[0x80001618]:sd t6, 216(t2)
[0x8000161c]:sd a0, 224(t2)
[0x80001620]:ld t5, 1152(s1)
[0x80001624]:addi fp, zero, 0
[0x80001628]:csrrw zero, fcsr, fp
[0x8000162c]:fsqrt.s t6, t5, dyn
[0x80001630]:csrrs a0, fcsr, zero

[0x8000162c]:fsqrt.s t6, t5, dyn
[0x80001630]:csrrs a0, fcsr, zero
[0x80001634]:sd t6, 232(t2)
[0x80001638]:sd a0, 240(t2)
[0x8000163c]:ld t5, 1160(s1)
[0x80001640]:addi fp, zero, 0
[0x80001644]:csrrw zero, fcsr, fp
[0x80001648]:fsqrt.s t6, t5, dyn
[0x8000164c]:csrrs a0, fcsr, zero

[0x80001648]:fsqrt.s t6, t5, dyn
[0x8000164c]:csrrs a0, fcsr, zero
[0x80001650]:sd t6, 248(t2)
[0x80001654]:sd a0, 256(t2)
[0x80001658]:ld t5, 1168(s1)
[0x8000165c]:addi fp, zero, 0
[0x80001660]:csrrw zero, fcsr, fp
[0x80001664]:fsqrt.s t6, t5, dyn
[0x80001668]:csrrs a0, fcsr, zero

[0x80001664]:fsqrt.s t6, t5, dyn
[0x80001668]:csrrs a0, fcsr, zero
[0x8000166c]:sd t6, 264(t2)
[0x80001670]:sd a0, 272(t2)
[0x80001674]:ld t5, 1176(s1)
[0x80001678]:addi fp, zero, 0
[0x8000167c]:csrrw zero, fcsr, fp
[0x80001680]:fsqrt.s t6, t5, dyn
[0x80001684]:csrrs a0, fcsr, zero

[0x80001680]:fsqrt.s t6, t5, dyn
[0x80001684]:csrrs a0, fcsr, zero
[0x80001688]:sd t6, 280(t2)
[0x8000168c]:sd a0, 288(t2)
[0x80001690]:ld t5, 1184(s1)
[0x80001694]:addi fp, zero, 0
[0x80001698]:csrrw zero, fcsr, fp
[0x8000169c]:fsqrt.s t6, t5, dyn
[0x800016a0]:csrrs a0, fcsr, zero

[0x8000169c]:fsqrt.s t6, t5, dyn
[0x800016a0]:csrrs a0, fcsr, zero
[0x800016a4]:sd t6, 296(t2)
[0x800016a8]:sd a0, 304(t2)
[0x800016ac]:ld t5, 1192(s1)
[0x800016b0]:addi fp, zero, 0
[0x800016b4]:csrrw zero, fcsr, fp
[0x800016b8]:fsqrt.s t6, t5, dyn
[0x800016bc]:csrrs a0, fcsr, zero

[0x800016b8]:fsqrt.s t6, t5, dyn
[0x800016bc]:csrrs a0, fcsr, zero
[0x800016c0]:sd t6, 312(t2)
[0x800016c4]:sd a0, 320(t2)
[0x800016c8]:ld t5, 1200(s1)
[0x800016cc]:addi fp, zero, 0
[0x800016d0]:csrrw zero, fcsr, fp
[0x800016d4]:fsqrt.s t6, t5, dyn
[0x800016d8]:csrrs a0, fcsr, zero

[0x800016d4]:fsqrt.s t6, t5, dyn
[0x800016d8]:csrrs a0, fcsr, zero
[0x800016dc]:sd t6, 328(t2)
[0x800016e0]:sd a0, 336(t2)
[0x800016e4]:ld t5, 1208(s1)
[0x800016e8]:addi fp, zero, 0
[0x800016ec]:csrrw zero, fcsr, fp
[0x800016f0]:fsqrt.s t6, t5, dyn
[0x800016f4]:csrrs a0, fcsr, zero

[0x800016f0]:fsqrt.s t6, t5, dyn
[0x800016f4]:csrrs a0, fcsr, zero
[0x800016f8]:sd t6, 344(t2)
[0x800016fc]:sd a0, 352(t2)
[0x80001700]:ld t5, 1216(s1)
[0x80001704]:addi fp, zero, 0
[0x80001708]:csrrw zero, fcsr, fp
[0x8000170c]:fsqrt.s t6, t5, dyn
[0x80001710]:csrrs a0, fcsr, zero

[0x8000170c]:fsqrt.s t6, t5, dyn
[0x80001710]:csrrs a0, fcsr, zero
[0x80001714]:sd t6, 360(t2)
[0x80001718]:sd a0, 368(t2)
[0x8000171c]:ld t5, 1224(s1)
[0x80001720]:addi fp, zero, 0
[0x80001724]:csrrw zero, fcsr, fp
[0x80001728]:fsqrt.s t6, t5, dyn
[0x8000172c]:csrrs a0, fcsr, zero

[0x80001728]:fsqrt.s t6, t5, dyn
[0x8000172c]:csrrs a0, fcsr, zero
[0x80001730]:sd t6, 376(t2)
[0x80001734]:sd a0, 384(t2)
[0x80001738]:ld t5, 1232(s1)
[0x8000173c]:addi fp, zero, 0
[0x80001740]:csrrw zero, fcsr, fp
[0x80001744]:fsqrt.s t6, t5, dyn
[0x80001748]:csrrs a0, fcsr, zero

[0x80001744]:fsqrt.s t6, t5, dyn
[0x80001748]:csrrs a0, fcsr, zero
[0x8000174c]:sd t6, 392(t2)
[0x80001750]:sd a0, 400(t2)
[0x80001754]:ld t5, 1240(s1)
[0x80001758]:addi fp, zero, 0
[0x8000175c]:csrrw zero, fcsr, fp
[0x80001760]:fsqrt.s t6, t5, dyn
[0x80001764]:csrrs a0, fcsr, zero

[0x80001760]:fsqrt.s t6, t5, dyn
[0x80001764]:csrrs a0, fcsr, zero
[0x80001768]:sd t6, 408(t2)
[0x8000176c]:sd a0, 416(t2)
[0x80001770]:ld t5, 1248(s1)
[0x80001774]:addi fp, zero, 0
[0x80001778]:csrrw zero, fcsr, fp
[0x8000177c]:fsqrt.s t6, t5, dyn
[0x80001780]:csrrs a0, fcsr, zero

[0x8000177c]:fsqrt.s t6, t5, dyn
[0x80001780]:csrrs a0, fcsr, zero
[0x80001784]:sd t6, 424(t2)
[0x80001788]:sd a0, 432(t2)
[0x8000178c]:ld t5, 1256(s1)
[0x80001790]:addi fp, zero, 0
[0x80001794]:csrrw zero, fcsr, fp
[0x80001798]:fsqrt.s t6, t5, dyn
[0x8000179c]:csrrs a0, fcsr, zero

[0x80001798]:fsqrt.s t6, t5, dyn
[0x8000179c]:csrrs a0, fcsr, zero
[0x800017a0]:sd t6, 440(t2)
[0x800017a4]:sd a0, 448(t2)
[0x800017a8]:ld t5, 1264(s1)
[0x800017ac]:addi fp, zero, 0
[0x800017b0]:csrrw zero, fcsr, fp
[0x800017b4]:fsqrt.s t6, t5, dyn
[0x800017b8]:csrrs a0, fcsr, zero

[0x800017b4]:fsqrt.s t6, t5, dyn
[0x800017b8]:csrrs a0, fcsr, zero
[0x800017bc]:sd t6, 456(t2)
[0x800017c0]:sd a0, 464(t2)
[0x800017c4]:ld t5, 1272(s1)
[0x800017c8]:addi fp, zero, 0
[0x800017cc]:csrrw zero, fcsr, fp
[0x800017d0]:fsqrt.s t6, t5, dyn
[0x800017d4]:csrrs a0, fcsr, zero

[0x800017d0]:fsqrt.s t6, t5, dyn
[0x800017d4]:csrrs a0, fcsr, zero
[0x800017d8]:sd t6, 472(t2)
[0x800017dc]:sd a0, 480(t2)
[0x800017e0]:ld t5, 1280(s1)
[0x800017e4]:addi fp, zero, 0
[0x800017e8]:csrrw zero, fcsr, fp
[0x800017ec]:fsqrt.s t6, t5, dyn
[0x800017f0]:csrrs a0, fcsr, zero

[0x800017ec]:fsqrt.s t6, t5, dyn
[0x800017f0]:csrrs a0, fcsr, zero
[0x800017f4]:sd t6, 488(t2)
[0x800017f8]:sd a0, 496(t2)
[0x800017fc]:ld t5, 1288(s1)
[0x80001800]:addi fp, zero, 0
[0x80001804]:csrrw zero, fcsr, fp
[0x80001808]:fsqrt.s t6, t5, dyn
[0x8000180c]:csrrs a0, fcsr, zero

[0x80001808]:fsqrt.s t6, t5, dyn
[0x8000180c]:csrrs a0, fcsr, zero
[0x80001810]:sd t6, 504(t2)
[0x80001814]:sd a0, 512(t2)
[0x80001818]:ld t5, 1296(s1)
[0x8000181c]:addi fp, zero, 0
[0x80001820]:csrrw zero, fcsr, fp
[0x80001824]:fsqrt.s t6, t5, dyn
[0x80001828]:csrrs a0, fcsr, zero

[0x80001824]:fsqrt.s t6, t5, dyn
[0x80001828]:csrrs a0, fcsr, zero
[0x8000182c]:sd t6, 520(t2)
[0x80001830]:sd a0, 528(t2)
[0x80001834]:ld t5, 1304(s1)
[0x80001838]:addi fp, zero, 0
[0x8000183c]:csrrw zero, fcsr, fp
[0x80001840]:fsqrt.s t6, t5, dyn
[0x80001844]:csrrs a0, fcsr, zero

[0x80001840]:fsqrt.s t6, t5, dyn
[0x80001844]:csrrs a0, fcsr, zero
[0x80001848]:sd t6, 536(t2)
[0x8000184c]:sd a0, 544(t2)
[0x80001850]:ld t5, 1312(s1)
[0x80001854]:addi fp, zero, 0
[0x80001858]:csrrw zero, fcsr, fp
[0x8000185c]:fsqrt.s t6, t5, dyn
[0x80001860]:csrrs a0, fcsr, zero

[0x8000185c]:fsqrt.s t6, t5, dyn
[0x80001860]:csrrs a0, fcsr, zero
[0x80001864]:sd t6, 552(t2)
[0x80001868]:sd a0, 560(t2)
[0x8000186c]:ld t5, 1320(s1)
[0x80001870]:addi fp, zero, 0
[0x80001874]:csrrw zero, fcsr, fp
[0x80001878]:fsqrt.s t6, t5, dyn
[0x8000187c]:csrrs a0, fcsr, zero

[0x80001878]:fsqrt.s t6, t5, dyn
[0x8000187c]:csrrs a0, fcsr, zero
[0x80001880]:sd t6, 568(t2)
[0x80001884]:sd a0, 576(t2)
[0x80001888]:ld t5, 1328(s1)
[0x8000188c]:addi fp, zero, 0
[0x80001890]:csrrw zero, fcsr, fp
[0x80001894]:fsqrt.s t6, t5, dyn
[0x80001898]:csrrs a0, fcsr, zero

[0x80001894]:fsqrt.s t6, t5, dyn
[0x80001898]:csrrs a0, fcsr, zero
[0x8000189c]:sd t6, 584(t2)
[0x800018a0]:sd a0, 592(t2)
[0x800018a4]:ld t5, 1336(s1)
[0x800018a8]:addi fp, zero, 0
[0x800018ac]:csrrw zero, fcsr, fp
[0x800018b0]:fsqrt.s t6, t5, dyn
[0x800018b4]:csrrs a0, fcsr, zero

[0x800018b0]:fsqrt.s t6, t5, dyn
[0x800018b4]:csrrs a0, fcsr, zero
[0x800018b8]:sd t6, 600(t2)
[0x800018bc]:sd a0, 608(t2)
[0x800018c0]:ld t5, 1344(s1)
[0x800018c4]:addi fp, zero, 0
[0x800018c8]:csrrw zero, fcsr, fp
[0x800018cc]:fsqrt.s t6, t5, dyn
[0x800018d0]:csrrs a0, fcsr, zero

[0x800018cc]:fsqrt.s t6, t5, dyn
[0x800018d0]:csrrs a0, fcsr, zero
[0x800018d4]:sd t6, 616(t2)
[0x800018d8]:sd a0, 624(t2)
[0x800018dc]:ld t5, 1352(s1)
[0x800018e0]:addi fp, zero, 0
[0x800018e4]:csrrw zero, fcsr, fp
[0x800018e8]:fsqrt.s t6, t5, dyn
[0x800018ec]:csrrs a0, fcsr, zero

[0x800018e8]:fsqrt.s t6, t5, dyn
[0x800018ec]:csrrs a0, fcsr, zero
[0x800018f0]:sd t6, 632(t2)
[0x800018f4]:sd a0, 640(t2)
[0x800018f8]:ld t5, 1360(s1)
[0x800018fc]:addi fp, zero, 0
[0x80001900]:csrrw zero, fcsr, fp
[0x80001904]:fsqrt.s t6, t5, dyn
[0x80001908]:csrrs a0, fcsr, zero

[0x80001904]:fsqrt.s t6, t5, dyn
[0x80001908]:csrrs a0, fcsr, zero
[0x8000190c]:sd t6, 648(t2)
[0x80001910]:sd a0, 656(t2)
[0x80001914]:ld t5, 1368(s1)
[0x80001918]:addi fp, zero, 0
[0x8000191c]:csrrw zero, fcsr, fp
[0x80001920]:fsqrt.s t6, t5, dyn
[0x80001924]:csrrs a0, fcsr, zero



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                             coverpoints                                                                                              |                                                                     code                                                                     |
|---:|------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003818]<br>0x0000000000000000<br> [0x80003820]<br>0x0000000000000000<br> |- mnemonic : fsqrt.s<br> - rs1 : x31<br> - rd : x31<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br> |[0x800003b8]:fsqrt.s t6, t6, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br> [0x800003c4]:sd tp, 8(ra)<br>        |
|   2|[0x80003828]<br>0x000000001FFFFFFF<br> [0x80003830]<br>0x0000000000000001<br> |- rs1 : x29<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                          |[0x800003d4]:fsqrt.s t5, t4, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br> [0x800003e0]:sd tp, 24(ra)<br>      |
|   3|[0x80003838]<br>0x000000001FB504F3<br> [0x80003840]<br>0x0000000000000001<br> |- rs1 : x30<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800003f0]:fsqrt.s t4, t5, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br> [0x800003fc]:sd tp, 40(ra)<br>      |
|   4|[0x80003848]<br>0x000000001FB504F2<br> [0x80003850]<br>0x0000000000000001<br> |- rs1 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000040c]:fsqrt.s t3, s11, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br> [0x80000418]:sd tp, 56(ra)<br>     |
|   5|[0x80003858]<br>0x000000001FDDB3D7<br> [0x80003860]<br>0x0000000000000001<br> |- rs1 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000428]:fsqrt.s s11, t3, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br> [0x80000434]:sd tp, 72(ra)<br>    |
|   6|[0x80003868]<br>0x000000001F7FFFFC<br> [0x80003870]<br>0x0000000000000001<br> |- rs1 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000444]:fsqrt.s s10, s9, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br> [0x80000450]:sd tp, 88(ra)<br>    |
|   7|[0x80003878]<br>0x000000001FEF7751<br> [0x80003880]<br>0x0000000000000001<br> |- rs1 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000460]:fsqrt.s s9, s10, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br> [0x8000046c]:sd tp, 104(ra)<br>    |
|   8|[0x80003888]<br>0x000000001F3504EE<br> [0x80003890]<br>0x0000000000000001<br> |- rs1 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000047c]:fsqrt.s s8, s7, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br> [0x80000488]:sd tp, 120(ra)<br>    |
|   9|[0x80003898]<br>0x000000001FF7DEF6<br> [0x800038a0]<br>0x0000000000000001<br> |- rs1 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000498]:fsqrt.s s7, s8, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br> [0x800004a4]:sd tp, 136(ra)<br>    |
|  10|[0x800038a8]<br>0x000000001EFFFFF0<br> [0x800038b0]<br>0x0000000000000001<br> |- rs1 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004b4]:fsqrt.s s6, s5, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br> [0x800004c0]:sd tp, 152(ra)<br>    |
|  11|[0x800038b8]<br>0x000000001FFBF7DF<br> [0x800038c0]<br>0x0000000000000001<br> |- rs1 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004d0]:fsqrt.s s5, s6, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br> [0x800004dc]:sd tp, 168(ra)<br>    |
|  12|[0x800038c8]<br>0x000000001EB504DD<br> [0x800038d0]<br>0x0000000000000001<br> |- rs1 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800004ec]:fsqrt.s s4, s3, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br> [0x800004f8]:sd tp, 184(ra)<br>    |
|  13|[0x800038d8]<br>0x000000001FFDFDFC<br> [0x800038e0]<br>0x0000000000000001<br> |- rs1 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000508]:fsqrt.s s3, s4, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br> [0x80000514]:sd tp, 200(ra)<br>    |
|  14|[0x800038e8]<br>0x000000001E7FFFC0<br> [0x800038f0]<br>0x0000000000000001<br> |- rs1 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000524]:fsqrt.s s2, a7, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br> [0x80000530]:sd tp, 216(ra)<br>    |
|  15|[0x800038f8]<br>0x000000001FFEFF7F<br> [0x80003900]<br>0x0000000000000001<br> |- rs1 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000540]:fsqrt.s a7, s2, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br> [0x8000054c]:sd tp, 232(ra)<br>    |
|  16|[0x80003908]<br>0x000000001E350499<br> [0x80003910]<br>0x0000000000000001<br> |- rs1 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x8000055c]:fsqrt.s a6, a5, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br> [0x80000568]:sd tp, 248(ra)<br>    |
|  17|[0x80003918]<br>0x000000001FFF7FE0<br> [0x80003920]<br>0x0000000000000001<br> |- rs1 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000578]:fsqrt.s a5, a6, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br> [0x80000584]:sd tp, 264(ra)<br>    |
|  18|[0x80003928]<br>0x000000001DFFFF00<br> [0x80003930]<br>0x0000000000000001<br> |- rs1 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x80000594]:fsqrt.s a4, a3, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br> [0x800005a0]:sd tp, 280(ra)<br>    |
|  19|[0x80003938]<br>0x000000001FFFBFF8<br> [0x80003940]<br>0x0000000000000001<br> |- rs1 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005b0]:fsqrt.s a3, a4, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sd a3, 288(ra)<br> [0x800005bc]:sd tp, 296(ra)<br>    |
|  20|[0x80003948]<br>0x000000001DB50389<br> [0x80003950]<br>0x0000000000000001<br> |- rs1 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005cc]:fsqrt.s a2, a1, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sd a2, 304(ra)<br> [0x800005d8]:sd tp, 312(ra)<br>    |
|  21|[0x80003958]<br>0x000000001FFFDFFE<br> [0x80003960]<br>0x0000000000000001<br> |- rs1 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                          |[0x800005e8]:fsqrt.s a1, a2, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br> [0x800005f4]:sd tp, 328(ra)<br>    |
|  22|[0x80003968]<br>0x000000001D7FFC00<br> [0x80003970]<br>0x0000000000000001<br> |- rs1 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                           |[0x80000604]:fsqrt.s a0, s1, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br> [0x80000610]:sd tp, 344(ra)<br>    |
|  23|[0x80003978]<br>0x000000001FFFEFFF<br> [0x80003980]<br>0x0000000000000001<br> |- rs1 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                           |[0x80000620]:fsqrt.s s1, a0, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br> [0x8000062c]:sd tp, 360(ra)<br>    |
|  24|[0x80003988]<br>0x000000001D34FF4B<br> [0x80003990]<br>0x0000000000000001<br> |- rs1 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x8000063c]:fsqrt.s fp, t2, dyn<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd fp, 368(ra)<br> [0x80000648]:sd tp, 376(ra)<br>    |
|  25|[0x80003998]<br>0x000000001FFFF800<br> [0x800039a0]<br>0x0000000000000001<br> |- rs1 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x80000660]:fsqrt.s t2, fp, dyn<br> [0x80000664]:csrrs a0, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br> [0x8000066c]:sd a0, 392(ra)<br>    |
|  26|[0x800039a8]<br>0x000000001CFFEFFF<br> [0x800039b0]<br>0x0000000000000001<br> |- rs1 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x8000067c]:fsqrt.s t1, t0, dyn<br> [0x80000680]:csrrs a0, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br> [0x80000688]:sd a0, 408(ra)<br>    |
|  27|[0x800038e8]<br>0x000000001FFFFC00<br> [0x800038f0]<br>0x0000000000000001<br> |- rs1 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006a0]:fsqrt.s t0, t1, dyn<br> [0x800006a4]:csrrs a0, fcsr, zero<br> [0x800006a8]:sd t0, 0(t2)<br> [0x800006ac]:sd a0, 8(t2)<br>        |
|  28|[0x800038f8]<br>0x000000001CB4EE51<br> [0x80003900]<br>0x0000000000000001<br> |- rs1 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006bc]:fsqrt.s tp, gp, dyn<br> [0x800006c0]:csrrs a0, fcsr, zero<br> [0x800006c4]:sd tp, 16(t2)<br> [0x800006c8]:sd a0, 24(t2)<br>      |
|  29|[0x80003908]<br>0x000000001FFFFE00<br> [0x80003910]<br>0x0000000000000001<br> |- rs1 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006d8]:fsqrt.s gp, tp, dyn<br> [0x800006dc]:csrrs a0, fcsr, zero<br> [0x800006e0]:sd gp, 32(t2)<br> [0x800006e4]:sd a0, 40(t2)<br>      |
|  30|[0x80003918]<br>0x000000001C7FBFF8<br> [0x80003920]<br>0x0000000000000001<br> |- rs1 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x800006f4]:fsqrt.s sp, ra, dyn<br> [0x800006f8]:csrrs a0, fcsr, zero<br> [0x800006fc]:sd sp, 48(t2)<br> [0x80000700]:sd a0, 56(t2)<br>      |
|  31|[0x80003928]<br>0x000000001FFFFF00<br> [0x80003930]<br>0x0000000000000001<br> |- rs1 : x2<br> - rd : x1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                            |[0x80000710]:fsqrt.s ra, sp, dyn<br> [0x80000714]:csrrs a0, fcsr, zero<br> [0x80000718]:sd ra, 64(t2)<br> [0x8000071c]:sd a0, 72(t2)<br>      |
|  32|[0x80003938]<br>0x0000000000000000<br> [0x80003940]<br>0x0000000000000000<br> |- rs1 : x0<br>                                                                                                                                                                                        |[0x8000072c]:fsqrt.s t6, zero, dyn<br> [0x80000730]:csrrs a0, fcsr, zero<br> [0x80000734]:sd t6, 80(t2)<br> [0x80000738]:sd a0, 88(t2)<br>    |
|  33|[0x80003948]<br>0x0000000000000000<br> [0x80003950]<br>0x0000000000000001<br> |- rd : x0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                           |[0x80000748]:fsqrt.s zero, t6, dyn<br> [0x8000074c]:csrrs a0, fcsr, zero<br> [0x80000750]:sd zero, 96(t2)<br> [0x80000754]:sd a0, 104(t2)<br> |
|  34|[0x80003958]<br>0x000000001BFEFF7F<br> [0x80003960]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000764]:fsqrt.s t6, t5, dyn<br> [0x80000768]:csrrs a0, fcsr, zero<br> [0x8000076c]:sd t6, 112(t2)<br> [0x80000770]:sd a0, 120(t2)<br>    |
|  35|[0x80003968]<br>0x000000001FFFFFC0<br> [0x80003970]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000780]:fsqrt.s t6, t5, dyn<br> [0x80000784]:csrrs a0, fcsr, zero<br> [0x80000788]:sd t6, 128(t2)<br> [0x8000078c]:sd a0, 136(t2)<br>    |
|  36|[0x80003978]<br>0x000000001BB3997C<br> [0x80003980]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000079c]:fsqrt.s t6, t5, dyn<br> [0x800007a0]:csrrs a0, fcsr, zero<br> [0x800007a4]:sd t6, 144(t2)<br> [0x800007a8]:sd a0, 152(t2)<br>    |
|  37|[0x80003988]<br>0x000000001FFFFFE0<br> [0x80003990]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007b8]:fsqrt.s t6, t5, dyn<br> [0x800007bc]:csrrs a0, fcsr, zero<br> [0x800007c0]:sd t6, 160(t2)<br> [0x800007c4]:sd a0, 168(t2)<br>    |
|  38|[0x80003998]<br>0x000000001B7BF7DF<br> [0x800039a0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007d4]:fsqrt.s t6, t5, dyn<br> [0x800007d8]:csrrs a0, fcsr, zero<br> [0x800007dc]:sd t6, 176(t2)<br> [0x800007e0]:sd a0, 184(t2)<br>    |
|  39|[0x800039a8]<br>0x000000001FFFFFF0<br> [0x800039b0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800007f0]:fsqrt.s t6, t5, dyn<br> [0x800007f4]:csrrs a0, fcsr, zero<br> [0x800007f8]:sd t6, 192(t2)<br> [0x800007fc]:sd a0, 200(t2)<br>    |
|  40|[0x800039b8]<br>0x000000001B2F456F<br> [0x800039c0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000080c]:fsqrt.s t6, t5, dyn<br> [0x80000810]:csrrs a0, fcsr, zero<br> [0x80000814]:sd t6, 208(t2)<br> [0x80000818]:sd a0, 216(t2)<br>    |
|  41|[0x800039c8]<br>0x000000001FFFFFF8<br> [0x800039d0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000828]:fsqrt.s t6, t5, dyn<br> [0x8000082c]:csrrs a0, fcsr, zero<br> [0x80000830]:sd t6, 224(t2)<br> [0x80000834]:sd a0, 232(t2)<br>    |
|  42|[0x800039d8]<br>0x000000001AEF7751<br> [0x800039e0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000844]:fsqrt.s t6, t5, dyn<br> [0x80000848]:csrrs a0, fcsr, zero<br> [0x8000084c]:sd t6, 240(t2)<br> [0x80000850]:sd a0, 248(t2)<br>    |
|  43|[0x800039e8]<br>0x000000001FFFFFFC<br> [0x800039f0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000860]:fsqrt.s t6, t5, dyn<br> [0x80000864]:csrrs a0, fcsr, zero<br> [0x80000868]:sd t6, 256(t2)<br> [0x8000086c]:sd a0, 264(t2)<br>    |
|  44|[0x800039f8]<br>0x000000001A9CC471<br> [0x80003a00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000087c]:fsqrt.s t6, t5, dyn<br> [0x80000880]:csrrs a0, fcsr, zero<br> [0x80000884]:sd t6, 272(t2)<br> [0x80000888]:sd a0, 280(t2)<br>    |
|  45|[0x80003a08]<br>0x000000001FFFFFFE<br> [0x80003a10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000898]:fsqrt.s t6, t5, dyn<br> [0x8000089c]:csrrs a0, fcsr, zero<br> [0x800008a0]:sd t6, 288(t2)<br> [0x800008a4]:sd a0, 296(t2)<br>    |
|  46|[0x80003a18]<br>0x000000001A3504F3<br> [0x80003a20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008b4]:fsqrt.s t6, t5, dyn<br> [0x800008b8]:csrrs a0, fcsr, zero<br> [0x800008bc]:sd t6, 304(t2)<br> [0x800008c0]:sd a0, 312(t2)<br>    |
|  47|[0x80003a28]<br>0x000000003F800000<br> [0x80003a30]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008d0]:fsqrt.s t6, t5, dyn<br> [0x800008d4]:csrrs a0, fcsr, zero<br> [0x800008d8]:sd t6, 320(t2)<br> [0x800008dc]:sd a0, 328(t2)<br>    |
|  48|[0x80003a38]<br>0x000000003FB504F3<br> [0x80003a40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800008ec]:fsqrt.s t6, t5, dyn<br> [0x800008f0]:csrrs a0, fcsr, zero<br> [0x800008f4]:sd t6, 336(t2)<br> [0x800008f8]:sd a0, 344(t2)<br>    |
|  49|[0x80003a48]<br>0x000000003F9CC471<br> [0x80003a50]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000908]:fsqrt.s t6, t5, dyn<br> [0x8000090c]:csrrs a0, fcsr, zero<br> [0x80000910]:sd t6, 352(t2)<br> [0x80000914]:sd a0, 360(t2)<br>    |
|  50|[0x80003a58]<br>0x000000003F9CC470<br> [0x80003a60]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000924]:fsqrt.s t6, t5, dyn<br> [0x80000928]:csrrs a0, fcsr, zero<br> [0x8000092c]:sd t6, 368(t2)<br> [0x80000930]:sd a0, 376(t2)<br>    |
|  51|[0x80003a68]<br>0x000000003FA953FD<br> [0x80003a70]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000940]:fsqrt.s t6, t5, dyn<br> [0x80000944]:csrrs a0, fcsr, zero<br> [0x80000948]:sd t6, 384(t2)<br> [0x8000094c]:sd a0, 392(t2)<br>    |
|  52|[0x80003a78]<br>0x000000003F8F1BBC<br> [0x80003a80]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x8000095c]:fsqrt.s t6, t5, dyn<br> [0x80000960]:csrrs a0, fcsr, zero<br> [0x80000964]:sd t6, 400(t2)<br> [0x80000968]:sd a0, 408(t2)<br>    |
|  53|[0x80003a88]<br>0x000000003FAF456F<br> [0x80003a90]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000978]:fsqrt.s t6, t5, dyn<br> [0x8000097c]:csrrs a0, fcsr, zero<br> [0x80000980]:sd t6, 416(t2)<br> [0x80000984]:sd a0, 424(t2)<br>    |
|  54|[0x80003a98]<br>0x000000003F87C3B6<br> [0x80003aa0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000994]:fsqrt.s t6, t5, dyn<br> [0x80000998]:csrrs a0, fcsr, zero<br> [0x8000099c]:sd t6, 432(t2)<br> [0x800009a0]:sd a0, 440(t2)<br>    |
|  55|[0x80003aa8]<br>0x000000003FB22B20<br> [0x80003ab0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800009b0]:fsqrt.s t6, t5, dyn<br> [0x800009b4]:csrrs a0, fcsr, zero<br> [0x800009b8]:sd t6, 448(t2)<br> [0x800009bc]:sd a0, 456(t2)<br>    |
|  56|[0x80003ab8]<br>0x000000003F83F07B<br> [0x80003ac0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800009cc]:fsqrt.s t6, t5, dyn<br> [0x800009d0]:csrrs a0, fcsr, zero<br> [0x800009d4]:sd t6, 464(t2)<br> [0x800009d8]:sd a0, 472(t2)<br>    |
|  57|[0x80003ac8]<br>0x000000003FB3997C<br> [0x80003ad0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x800009e8]:fsqrt.s t6, t5, dyn<br> [0x800009ec]:csrrs a0, fcsr, zero<br> [0x800009f0]:sd t6, 480(t2)<br> [0x800009f4]:sd a0, 488(t2)<br>    |
|  58|[0x80003ad8]<br>0x000000003F81FC0F<br> [0x80003ae0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a04]:fsqrt.s t6, t5, dyn<br> [0x80000a08]:csrrs a0, fcsr, zero<br> [0x80000a0c]:sd t6, 496(t2)<br> [0x80000a10]:sd a0, 504(t2)<br>    |
|  59|[0x80003ae8]<br>0x000000003FB44F93<br> [0x80003af0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a20]:fsqrt.s t6, t5, dyn<br> [0x80000a24]:csrrs a0, fcsr, zero<br> [0x80000a28]:sd t6, 512(t2)<br> [0x80000a2c]:sd a0, 520(t2)<br>    |
|  60|[0x80003af8]<br>0x000000003F80FF01<br> [0x80003b00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a3c]:fsqrt.s t6, t5, dyn<br> [0x80000a40]:csrrs a0, fcsr, zero<br> [0x80000a44]:sd t6, 528(t2)<br> [0x80000a48]:sd a0, 536(t2)<br>    |
|  61|[0x80003b08]<br>0x000000003FB4AA5A<br> [0x80003b10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a58]:fsqrt.s t6, t5, dyn<br> [0x80000a5c]:csrrs a0, fcsr, zero<br> [0x80000a60]:sd t6, 544(t2)<br> [0x80000a64]:sd a0, 552(t2)<br>    |
|  62|[0x80003b18]<br>0x000000003F807FC0<br> [0x80003b20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a74]:fsqrt.s t6, t5, dyn<br> [0x80000a78]:csrrs a0, fcsr, zero<br> [0x80000a7c]:sd t6, 560(t2)<br> [0x80000a80]:sd a0, 568(t2)<br>    |
|  63|[0x80003b28]<br>0x000000003FB4D7AC<br> [0x80003b30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000a90]:fsqrt.s t6, t5, dyn<br> [0x80000a94]:csrrs a0, fcsr, zero<br> [0x80000a98]:sd t6, 576(t2)<br> [0x80000a9c]:sd a0, 584(t2)<br>    |
|  64|[0x80003b38]<br>0x000000003F803FF0<br> [0x80003b40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000aac]:fsqrt.s t6, t5, dyn<br> [0x80000ab0]:csrrs a0, fcsr, zero<br> [0x80000ab4]:sd t6, 592(t2)<br> [0x80000ab8]:sd a0, 600(t2)<br>    |
|  65|[0x80003b48]<br>0x000000003FB4EE51<br> [0x80003b50]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ac8]:fsqrt.s t6, t5, dyn<br> [0x80000acc]:csrrs a0, fcsr, zero<br> [0x80000ad0]:sd t6, 608(t2)<br> [0x80000ad4]:sd a0, 616(t2)<br>    |
|  66|[0x80003b58]<br>0x000000003F801FFC<br> [0x80003b60]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ae4]:fsqrt.s t6, t5, dyn<br> [0x80000ae8]:csrrs a0, fcsr, zero<br> [0x80000aec]:sd t6, 624(t2)<br> [0x80000af0]:sd a0, 632(t2)<br>    |
|  67|[0x80003b68]<br>0x000000003FB4F9A3<br> [0x80003b70]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fe000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b00]:fsqrt.s t6, t5, dyn<br> [0x80000b04]:csrrs a0, fcsr, zero<br> [0x80000b08]:sd t6, 640(t2)<br> [0x80000b0c]:sd a0, 648(t2)<br>    |
|  68|[0x80003b78]<br>0x000000003F800FFF<br> [0x80003b80]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x001fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b1c]:fsqrt.s t6, t5, dyn<br> [0x80000b20]:csrrs a0, fcsr, zero<br> [0x80000b24]:sd t6, 656(t2)<br> [0x80000b28]:sd a0, 664(t2)<br>    |
|  69|[0x80003b88]<br>0x000000003FB4FF4B<br> [0x80003b90]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b38]:fsqrt.s t6, t5, dyn<br> [0x80000b3c]:csrrs a0, fcsr, zero<br> [0x80000b40]:sd t6, 672(t2)<br> [0x80000b44]:sd a0, 680(t2)<br>    |
|  70|[0x80003b98]<br>0x000000003F8007FF<br> [0x80003ba0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b54]:fsqrt.s t6, t5, dyn<br> [0x80000b58]:csrrs a0, fcsr, zero<br> [0x80000b5c]:sd t6, 688(t2)<br> [0x80000b60]:sd a0, 696(t2)<br>    |
|  71|[0x80003ba8]<br>0x000000003FB5021F<br> [0x80003bb0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ff800 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b70]:fsqrt.s t6, t5, dyn<br> [0x80000b74]:csrrs a0, fcsr, zero<br> [0x80000b78]:sd t6, 704(t2)<br> [0x80000b7c]:sd a0, 712(t2)<br>    |
|  72|[0x80003bb8]<br>0x000000003F8003FF<br> [0x80003bc0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0007ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000b8c]:fsqrt.s t6, t5, dyn<br> [0x80000b90]:csrrs a0, fcsr, zero<br> [0x80000b94]:sd t6, 720(t2)<br> [0x80000b98]:sd a0, 728(t2)<br>    |
|  73|[0x80003bc8]<br>0x000000003FB50389<br> [0x80003bd0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffc00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ba8]:fsqrt.s t6, t5, dyn<br> [0x80000bac]:csrrs a0, fcsr, zero<br> [0x80000bb0]:sd t6, 736(t2)<br> [0x80000bb4]:sd a0, 744(t2)<br>    |
|  74|[0x80003bd8]<br>0x000000003F8001FF<br> [0x80003be0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0003ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000bc4]:fsqrt.s t6, t5, dyn<br> [0x80000bc8]:csrrs a0, fcsr, zero<br> [0x80000bcc]:sd t6, 752(t2)<br> [0x80000bd0]:sd a0, 760(t2)<br>    |
|  75|[0x80003be8]<br>0x000000003FB5043E<br> [0x80003bf0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffe00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000be0]:fsqrt.s t6, t5, dyn<br> [0x80000be4]:csrrs a0, fcsr, zero<br> [0x80000be8]:sd t6, 768(t2)<br> [0x80000bec]:sd a0, 776(t2)<br>    |
|  76|[0x80003bf8]<br>0x000000003F8000FF<br> [0x80003c00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0001ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000bfc]:fsqrt.s t6, t5, dyn<br> [0x80000c00]:csrrs a0, fcsr, zero<br> [0x80000c04]:sd t6, 784(t2)<br> [0x80000c08]:sd a0, 792(t2)<br>    |
|  77|[0x80003c08]<br>0x000000003FB50499<br> [0x80003c10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff00 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000c18]:fsqrt.s t6, t5, dyn<br> [0x80000c1c]:csrrs a0, fcsr, zero<br> [0x80000c20]:sd t6, 800(t2)<br> [0x80000c24]:sd a0, 808(t2)<br>    |
|  78|[0x80003c18]<br>0x000000003F80007F<br> [0x80003c20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0000ff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000c34]:fsqrt.s t6, t5, dyn<br> [0x80000c38]:csrrs a0, fcsr, zero<br> [0x80000c3c]:sd t6, 816(t2)<br> [0x80000c40]:sd a0, 824(t2)<br>    |
|  79|[0x80003c28]<br>0x000000003FB504C6<br> [0x80003c30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fff80 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000c50]:fsqrt.s t6, t5, dyn<br> [0x80000c54]:csrrs a0, fcsr, zero<br> [0x80000c58]:sd t6, 832(t2)<br> [0x80000c5c]:sd a0, 840(t2)<br>    |
|  80|[0x80003c38]<br>0x000000003F80003F<br> [0x80003c40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00007f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000c6c]:fsqrt.s t6, t5, dyn<br> [0x80000c70]:csrrs a0, fcsr, zero<br> [0x80000c74]:sd t6, 848(t2)<br> [0x80000c78]:sd a0, 856(t2)<br>    |
|  81|[0x80003c48]<br>0x000000003FB504DD<br> [0x80003c50]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffc0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000c88]:fsqrt.s t6, t5, dyn<br> [0x80000c8c]:csrrs a0, fcsr, zero<br> [0x80000c90]:sd t6, 864(t2)<br> [0x80000c94]:sd a0, 872(t2)<br>    |
|  82|[0x80003c58]<br>0x000000003F80001F<br> [0x80003c60]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00003f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ca4]:fsqrt.s t6, t5, dyn<br> [0x80000ca8]:csrrs a0, fcsr, zero<br> [0x80000cac]:sd t6, 880(t2)<br> [0x80000cb0]:sd a0, 888(t2)<br>    |
|  83|[0x80003c68]<br>0x000000003FB504E8<br> [0x80003c70]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7fffe0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000cc0]:fsqrt.s t6, t5, dyn<br> [0x80000cc4]:csrrs a0, fcsr, zero<br> [0x80000cc8]:sd t6, 896(t2)<br> [0x80000ccc]:sd a0, 904(t2)<br>    |
|  84|[0x80003c78]<br>0x000000003F80000F<br> [0x80003c80]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00001f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000cdc]:fsqrt.s t6, t5, dyn<br> [0x80000ce0]:csrrs a0, fcsr, zero<br> [0x80000ce4]:sd t6, 912(t2)<br> [0x80000ce8]:sd a0, 920(t2)<br>    |
|  85|[0x80003c88]<br>0x000000003FB504EE<br> [0x80003c90]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000cf8]:fsqrt.s t6, t5, dyn<br> [0x80000cfc]:csrrs a0, fcsr, zero<br> [0x80000d00]:sd t6, 928(t2)<br> [0x80000d04]:sd a0, 936(t2)<br>    |
|  86|[0x80003c98]<br>0x000000003F800007<br> [0x80003ca0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x00000f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000d14]:fsqrt.s t6, t5, dyn<br> [0x80000d18]:csrrs a0, fcsr, zero<br> [0x80000d1c]:sd t6, 944(t2)<br> [0x80000d20]:sd a0, 952(t2)<br>    |
|  87|[0x80003ca8]<br>0x000000003FB504F0<br> [0x80003cb0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffff8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000d30]:fsqrt.s t6, t5, dyn<br> [0x80000d34]:csrrs a0, fcsr, zero<br> [0x80000d38]:sd t6, 960(t2)<br> [0x80000d3c]:sd a0, 968(t2)<br>    |
|  88|[0x80003cb8]<br>0x000000003F800003<br> [0x80003cc0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000007 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000d4c]:fsqrt.s t6, t5, dyn<br> [0x80000d50]:csrrs a0, fcsr, zero<br> [0x80000d54]:sd t6, 976(t2)<br> [0x80000d58]:sd a0, 984(t2)<br>    |
|  89|[0x80003cc8]<br>0x000000003FB504F2<br> [0x80003cd0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000d68]:fsqrt.s t6, t5, dyn<br> [0x80000d6c]:csrrs a0, fcsr, zero<br> [0x80000d70]:sd t6, 992(t2)<br> [0x80000d74]:sd a0, 1000(t2)<br>   |
|  90|[0x80003cd8]<br>0x000000003F800001<br> [0x80003ce0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000003 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000d84]:fsqrt.s t6, t5, dyn<br> [0x80000d88]:csrrs a0, fcsr, zero<br> [0x80000d8c]:sd t6, 1008(t2)<br> [0x80000d90]:sd a0, 1016(t2)<br>  |
|  91|[0x80003ce8]<br>0x000000003FB504F2<br> [0x80003cf0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x7ffffe and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000da0]:fsqrt.s t6, t5, dyn<br> [0x80000da4]:csrrs a0, fcsr, zero<br> [0x80000da8]:sd t6, 1024(t2)<br> [0x80000dac]:sd a0, 1032(t2)<br>  |
|  92|[0x80003cf8]<br>0x000000003F800000<br> [0x80003d00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000dbc]:fsqrt.s t6, t5, dyn<br> [0x80000dc0]:csrrs a0, fcsr, zero<br> [0x80000dc4]:sd t6, 1040(t2)<br> [0x80000dc8]:sd a0, 1048(t2)<br>  |
|  93|[0x80003d08]<br>0x0000000020000000<br> [0x80003d10]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000dd8]:fsqrt.s t6, t5, dyn<br> [0x80000ddc]:csrrs a0, fcsr, zero<br> [0x80000de0]:sd t6, 1056(t2)<br> [0x80000de4]:sd a0, 1064(t2)<br>  |
|  94|[0x80003d18]<br>0x00000000203504F3<br> [0x80003d20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000df4]:fsqrt.s t6, t5, dyn<br> [0x80000df8]:csrrs a0, fcsr, zero<br> [0x80000dfc]:sd t6, 1072(t2)<br> [0x80000e00]:sd a0, 1080(t2)<br>  |
|  95|[0x80003d28]<br>0x00000000201CC471<br> [0x80003d30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x400000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e10]:fsqrt.s t6, t5, dyn<br> [0x80000e14]:csrrs a0, fcsr, zero<br> [0x80000e18]:sd t6, 1088(t2)<br> [0x80000e1c]:sd a0, 1096(t2)<br>  |
|  96|[0x80003d38]<br>0x00000000201CC470<br> [0x80003d40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x3fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e2c]:fsqrt.s t6, t5, dyn<br> [0x80000e30]:csrrs a0, fcsr, zero<br> [0x80000e34]:sd t6, 1104(t2)<br> [0x80000e38]:sd a0, 1112(t2)<br>  |
|  97|[0x80003d48]<br>0x00000000202953FD<br> [0x80003d50]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x600000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e48]:fsqrt.s t6, t5, dyn<br> [0x80000e4c]:csrrs a0, fcsr, zero<br> [0x80000e50]:sd t6, 1120(t2)<br> [0x80000e54]:sd a0, 1128(t2)<br>  |
|  98|[0x80003d58]<br>0x00000000200F1BBC<br> [0x80003d60]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x1fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e64]:fsqrt.s t6, t5, dyn<br> [0x80000e68]:csrrs a0, fcsr, zero<br> [0x80000e6c]:sd t6, 1136(t2)<br> [0x80000e70]:sd a0, 1144(t2)<br>  |
|  99|[0x80003d68]<br>0x00000000202F456F<br> [0x80003d70]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x700000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e80]:fsqrt.s t6, t5, dyn<br> [0x80000e84]:csrrs a0, fcsr, zero<br> [0x80000e88]:sd t6, 1152(t2)<br> [0x80000e8c]:sd a0, 1160(t2)<br>  |
| 100|[0x80003d78]<br>0x000000002007C3B6<br> [0x80003d80]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000e9c]:fsqrt.s t6, t5, dyn<br> [0x80000ea0]:csrrs a0, fcsr, zero<br> [0x80000ea4]:sd t6, 1168(t2)<br> [0x80000ea8]:sd a0, 1176(t2)<br>  |
| 101|[0x80003d88]<br>0x0000000020322B20<br> [0x80003d90]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x780000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000eb8]:fsqrt.s t6, t5, dyn<br> [0x80000ebc]:csrrs a0, fcsr, zero<br> [0x80000ec0]:sd t6, 1184(t2)<br> [0x80000ec4]:sd a0, 1192(t2)<br>  |
| 102|[0x80003d98]<br>0x000000002003F07B<br> [0x80003da0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x07ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ed4]:fsqrt.s t6, t5, dyn<br> [0x80000ed8]:csrrs a0, fcsr, zero<br> [0x80000edc]:sd t6, 1200(t2)<br> [0x80000ee0]:sd a0, 1208(t2)<br>  |
| 103|[0x80003da8]<br>0x000000002033997C<br> [0x80003db0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7c0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000ef0]:fsqrt.s t6, t5, dyn<br> [0x80000ef4]:csrrs a0, fcsr, zero<br> [0x80000ef8]:sd t6, 1216(t2)<br> [0x80000efc]:sd a0, 1224(t2)<br>  |
| 104|[0x80003db8]<br>0x000000002001FC0F<br> [0x80003dc0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x03ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f0c]:fsqrt.s t6, t5, dyn<br> [0x80000f10]:csrrs a0, fcsr, zero<br> [0x80000f14]:sd t6, 1232(t2)<br> [0x80000f18]:sd a0, 1240(t2)<br>  |
| 105|[0x80003dc8]<br>0x0000000020344F93<br> [0x80003dd0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7e0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f28]:fsqrt.s t6, t5, dyn<br> [0x80000f2c]:csrrs a0, fcsr, zero<br> [0x80000f30]:sd t6, 1248(t2)<br> [0x80000f34]:sd a0, 1256(t2)<br>  |
| 106|[0x80003dd8]<br>0x000000002000FF01<br> [0x80003de0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x01ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f44]:fsqrt.s t6, t5, dyn<br> [0x80000f48]:csrrs a0, fcsr, zero<br> [0x80000f4c]:sd t6, 1264(t2)<br> [0x80000f50]:sd a0, 1272(t2)<br>  |
| 107|[0x80003de8]<br>0x000000002034AA5A<br> [0x80003df0]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f0000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f60]:fsqrt.s t6, t5, dyn<br> [0x80000f64]:csrrs a0, fcsr, zero<br> [0x80000f68]:sd t6, 1280(t2)<br> [0x80000f6c]:sd a0, 1288(t2)<br>  |
| 108|[0x80003df8]<br>0x0000000020007FC0<br> [0x80003e00]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x00ffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f7c]:fsqrt.s t6, t5, dyn<br> [0x80000f80]:csrrs a0, fcsr, zero<br> [0x80000f84]:sd t6, 1296(t2)<br> [0x80000f88]:sd a0, 1304(t2)<br>  |
| 109|[0x80003e08]<br>0x000000002034D7AC<br> [0x80003e10]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7f8000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000f98]:fsqrt.s t6, t5, dyn<br> [0x80000f9c]:csrrs a0, fcsr, zero<br> [0x80000fa0]:sd t6, 1312(t2)<br> [0x80000fa4]:sd a0, 1320(t2)<br>  |
| 110|[0x80003e18]<br>0x0000000020003FF0<br> [0x80003e20]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x007fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000fb4]:fsqrt.s t6, t5, dyn<br> [0x80000fb8]:csrrs a0, fcsr, zero<br> [0x80000fbc]:sd t6, 1328(t2)<br> [0x80000fc0]:sd a0, 1336(t2)<br>  |
| 111|[0x80003e28]<br>0x000000002034EE51<br> [0x80003e30]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x7fc000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000fd0]:fsqrt.s t6, t5, dyn<br> [0x80000fd4]:csrrs a0, fcsr, zero<br> [0x80000fd8]:sd t6, 1344(t2)<br> [0x80000fdc]:sd a0, 1352(t2)<br>  |
| 112|[0x80003e38]<br>0x0000000020001FFC<br> [0x80003e40]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x003fff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                                                         |[0x80000fec]:fsqrt.s t6, t5, dyn<br> [0x80000ff0]:csrrs a0, fcsr, zero<br> [0x80000ff4]:sd t6, 1360(t2)<br> [0x80000ff8]:sd a0, 1368(t2)<br>  |
