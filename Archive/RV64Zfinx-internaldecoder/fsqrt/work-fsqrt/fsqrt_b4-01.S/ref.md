
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000760')]      |
| SIG_REGION                | [('0x80002310', '0x80002430', '36 dwords')]      |
| COV_LABELS                | fsqrt_b4      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fsqrt/fsqrt_b4-01.S/ref.S    |
| Total Number of coverpoints| 72     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 46      |
| STAT1                     | 23      |
| STAT2                     | 0      |
| STAT3                     | 10     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero

[0x800005b0]:fsqrt.s a3, a4, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a1, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3420
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0

[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:addi zero, zero, 0
[0x8000075c]:addi zero, zero, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                             coverpoints                                                                                              |                                                                   code                                                                    |
|---:|------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x000000005F7FFFFF<br> [0x80002320]<br>0x0000000000000001<br> |- mnemonic : fsqrt.s<br> - rs1 : x31<br> - rd : x31<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br> |[0x800003b8]:fsqrt.s t6, t6, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br> [0x800003c4]:sd tp, 8(ra)<br>     |
|   2|[0x80002328]<br>0x000000005F7FFFFF<br> [0x80002330]<br>0x0000000000000021<br> |- rs1 : x29<br> - rd : x30<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                         |[0x800003d4]:fsqrt.s t5, t4, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br> [0x800003e0]:sd tp, 24(ra)<br>   |
|   3|[0x80002338]<br>0x000000005F7FFFFF<br> [0x80002340]<br>0x0000000000000041<br> |- rs1 : x30<br> - rd : x29<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                         |[0x800003f0]:fsqrt.s t4, t5, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br> [0x800003fc]:sd tp, 40(ra)<br>   |
|   4|[0x80002348]<br>0x000000005F800000<br> [0x80002350]<br>0x0000000000000061<br> |- rs1 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                         |[0x8000040c]:fsqrt.s t3, s11, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br> [0x80000418]:sd tp, 56(ra)<br>  |
|   5|[0x80002358]<br>0x000000005F7FFFFF<br> [0x80002360]<br>0x0000000000000081<br> |- rs1 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                                         |[0x80000428]:fsqrt.s s11, t3, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br> [0x80000434]:sd tp, 72(ra)<br> |
|   6|[0x80002368]<br>0x0000000000000000<br> [0x80002370]<br>0x0000000000000000<br> |- rs1 : x25<br> - rd : x26<br>                                                                                                                                                                        |[0x80000444]:fsqrt.s s10, s9, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br> [0x80000450]:sd tp, 88(ra)<br> |
|   7|[0x80002378]<br>0x0000000000000000<br> [0x80002380]<br>0x0000000000000000<br> |- rs1 : x26<br> - rd : x25<br>                                                                                                                                                                        |[0x80000460]:fsqrt.s s9, s10, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br> [0x8000046c]:sd tp, 104(ra)<br> |
|   8|[0x80002388]<br>0x0000000000000000<br> [0x80002390]<br>0x0000000000000000<br> |- rs1 : x23<br> - rd : x24<br>                                                                                                                                                                        |[0x8000047c]:fsqrt.s s8, s7, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br> [0x80000488]:sd tp, 120(ra)<br> |
|   9|[0x80002398]<br>0x0000000000000000<br> [0x800023a0]<br>0x0000000000000000<br> |- rs1 : x24<br> - rd : x23<br>                                                                                                                                                                        |[0x80000498]:fsqrt.s s7, s8, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br> [0x800004a4]:sd tp, 136(ra)<br> |
|  10|[0x800023a8]<br>0x0000000000000000<br> [0x800023b0]<br>0x0000000000000000<br> |- rs1 : x21<br> - rd : x22<br>                                                                                                                                                                        |[0x800004b4]:fsqrt.s s6, s5, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br> [0x800004c0]:sd tp, 152(ra)<br> |
|  11|[0x800023b8]<br>0x0000000000000000<br> [0x800023c0]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : x21<br>                                                                                                                                                                        |[0x800004d0]:fsqrt.s s5, s6, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br> [0x800004dc]:sd tp, 168(ra)<br> |
|  12|[0x800023c8]<br>0x0000000000000000<br> [0x800023d0]<br>0x0000000000000000<br> |- rs1 : x19<br> - rd : x20<br>                                                                                                                                                                        |[0x800004ec]:fsqrt.s s4, s3, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br> [0x800004f8]:sd tp, 184(ra)<br> |
|  13|[0x800023d8]<br>0x0000000000000000<br> [0x800023e0]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : x19<br>                                                                                                                                                                        |[0x80000508]:fsqrt.s s3, s4, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br> [0x80000514]:sd tp, 200(ra)<br> |
|  14|[0x800023e8]<br>0x0000000000000000<br> [0x800023f0]<br>0x0000000000000000<br> |- rs1 : x17<br> - rd : x18<br>                                                                                                                                                                        |[0x80000524]:fsqrt.s s2, a7, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br> [0x80000530]:sd tp, 216(ra)<br> |
|  15|[0x800023f8]<br>0x0000000000000000<br> [0x80002400]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : x17<br>                                                                                                                                                                        |[0x80000540]:fsqrt.s a7, s2, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br> [0x8000054c]:sd tp, 232(ra)<br> |
|  16|[0x80002408]<br>0x0000000000000000<br> [0x80002410]<br>0x0000000000000000<br> |- rs1 : x15<br> - rd : x16<br>                                                                                                                                                                        |[0x8000055c]:fsqrt.s a6, a5, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br> [0x80000568]:sd tp, 248(ra)<br> |
|  17|[0x80002418]<br>0x0000000000000000<br> [0x80002420]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : x15<br>                                                                                                                                                                        |[0x80000578]:fsqrt.s a5, a6, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br> [0x80000584]:sd tp, 264(ra)<br> |
|  18|[0x80002428]<br>0x0000000000000000<br> [0x80002430]<br>0x0000000000000000<br> |- rs1 : x13<br> - rd : x14<br>                                                                                                                                                                        |[0x80000594]:fsqrt.s a4, a3, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br> [0x800005a0]:sd tp, 280(ra)<br> |
|  19|[0x800023e8]<br>0x0000000000000000<br> [0x800023f0]<br>0x0000000000000000<br> |- rs1 : x6<br> - rd : x5<br>                                                                                                                                                                          |[0x800006a0]:fsqrt.s t0, t1, dyn<br> [0x800006a4]:csrrs a0, fcsr, zero<br> [0x800006a8]:sd t0, 0(t2)<br> [0x800006ac]:sd a0, 8(t2)<br>     |
|  20|[0x800023f8]<br>0x0000000000000000<br> [0x80002400]<br>0x0000000000000000<br> |- rs1 : x3<br> - rd : x4<br>                                                                                                                                                                          |[0x800006bc]:fsqrt.s tp, gp, dyn<br> [0x800006c0]:csrrs a0, fcsr, zero<br> [0x800006c4]:sd tp, 16(t2)<br> [0x800006c8]:sd a0, 24(t2)<br>   |
|  21|[0x80002408]<br>0x0000000000000000<br> [0x80002410]<br>0x0000000000000000<br> |- rs1 : x4<br> - rd : x3<br>                                                                                                                                                                          |[0x800006d8]:fsqrt.s gp, tp, dyn<br> [0x800006dc]:csrrs a0, fcsr, zero<br> [0x800006e0]:sd gp, 32(t2)<br> [0x800006e4]:sd a0, 40(t2)<br>   |
|  22|[0x80002418]<br>0x0000000000000000<br> [0x80002420]<br>0x0000000000000000<br> |- rs1 : x1<br> - rd : x2<br>                                                                                                                                                                          |[0x800006f4]:fsqrt.s sp, ra, dyn<br> [0x800006f8]:csrrs a0, fcsr, zero<br> [0x800006fc]:sd sp, 48(t2)<br> [0x80000700]:sd a0, 56(t2)<br>   |
|  23|[0x80002428]<br>0x0000000000000000<br> [0x80002430]<br>0x0000000000000000<br> |- rs1 : x2<br> - rd : x1<br>                                                                                                                                                                          |[0x80000710]:fsqrt.s ra, sp, dyn<br> [0x80000714]:csrrs a0, fcsr, zero<br> [0x80000718]:sd ra, 64(t2)<br> [0x8000071c]:sd a0, 72(t2)<br>   |
