
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80007cf0')]      |
| SIG_REGION                | [('0x8000b610', '0x8000da70', '1164 dwords')]      |
| COV_LABELS                | fsgnjn_b1      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fsgnjn/fsgnjn_b1-01.S/ref.S    |
| Total Number of coverpoints| 678     |
| Total Coverpoints Hit     | 678      |
| Total Signature Updates   | 1162      |
| STAT1                     | 579      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 581     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80007bf4]:fsgnjn.s t6, t5, t4
      [0x80007bf8]:csrrs a2, fcsr, zero
      [0x80007bfc]:sd t6, 624(fp)
 -- Signature Addresses:
      Address: 0x8000da18 Data: 0xFFFFFFFF80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsgnjn.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80007cd4]:fsgnjn.s t6, t5, t4
      [0x80007cd8]:csrrs a2, fcsr, zero
      [0x80007cdc]:sd t6, 688(fp)
 -- Signature Addresses:
      Address: 0x8000da58 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsgnjn.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsgnjn.s', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs1 == rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fsgnjn.s t6, t5, t5
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x8000b620]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x31', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800003dc]:fsgnjn.s t4, t4, t6
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t4, 16(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x8000b630]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003fc]:fsgnjn.s t3, t3, t3
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t3, 32(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x8000b640]:0x0000000000000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fsgnjn.s t5, t6, t4
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t5, 48(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x8000b650]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000043c]:fsgnjn.s s11, s10, s11
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s11, 64(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x8000b660]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fsgnjn.s s10, s11, s9
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s10, 80(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x8000b670]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fsgnjn.s s9, s8, s10
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x8000b680]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fsgnjn.s s8, s9, s7
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x8000b690]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fsgnjn.s s7, s6, s8
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x8000b6a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fsgnjn.s s6, s7, s5
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x8000b6b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fsgnjn.s s5, s4, s6
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x8000b6c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsgnjn.s s4, s5, s3
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x8000b6d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fsgnjn.s s3, s2, s4
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x8000b6e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsgnjn.s s2, s3, a7
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x8000b6f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fsgnjn.s a7, a6, s2
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x8000b700]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fsgnjn.s a6, a7, a5
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x8000b710]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsgnjn.s a5, a4, a6
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x8000b720]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fsgnjn.s a4, a5, a3
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x8000b730]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fsgnjn.s a3, a2, a4
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x8000b740]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fsgnjn.s a2, a3, a1
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x8000b750]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fsgnjn.s a1, a0, a2
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x8000b760]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fsgnjn.s a0, a1, s1
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:sd a0, 336(ra)
Current Store : [0x80000668] : sd tp, 344(ra) -- Store: [0x8000b770]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fsgnjn.s s1, fp, a0
	-[0x80000688]:csrrs a2, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
Current Store : [0x80000690] : sd a2, 360(ra) -- Store: [0x8000b780]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fsgnjn.s fp, s1, t2
	-[0x800006a8]:csrrs a2, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
Current Store : [0x800006b0] : sd a2, 376(ra) -- Store: [0x8000b790]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fsgnjn.s t2, t1, fp
	-[0x800006c8]:csrrs a2, fcsr, zero
	-[0x800006cc]:sd t2, 384(ra)
Current Store : [0x800006d0] : sd a2, 392(ra) -- Store: [0x8000b7a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fsgnjn.s t1, t2, t0
	-[0x800006f0]:csrrs a2, fcsr, zero
	-[0x800006f4]:sd t1, 0(fp)
Current Store : [0x800006f8] : sd a2, 8(fp) -- Store: [0x8000b7b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fsgnjn.s t0, tp, t1
	-[0x80000710]:csrrs a2, fcsr, zero
	-[0x80000714]:sd t0, 16(fp)
Current Store : [0x80000718] : sd a2, 24(fp) -- Store: [0x8000b7c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fsgnjn.s tp, t0, gp
	-[0x80000730]:csrrs a2, fcsr, zero
	-[0x80000734]:sd tp, 32(fp)
Current Store : [0x80000738] : sd a2, 40(fp) -- Store: [0x8000b7d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsgnjn.s gp, sp, tp
	-[0x80000750]:csrrs a2, fcsr, zero
	-[0x80000754]:sd gp, 48(fp)
Current Store : [0x80000758] : sd a2, 56(fp) -- Store: [0x8000b7e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000076c]:fsgnjn.s sp, gp, ra
	-[0x80000770]:csrrs a2, fcsr, zero
	-[0x80000774]:sd sp, 64(fp)
Current Store : [0x80000778] : sd a2, 72(fp) -- Store: [0x8000b7f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x8000078c]:fsgnjn.s ra, zero, sp
	-[0x80000790]:csrrs a2, fcsr, zero
	-[0x80000794]:sd ra, 80(fp)
Current Store : [0x80000798] : sd a2, 88(fp) -- Store: [0x8000b800]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fsgnjn.s t6, ra, t5
	-[0x800007b0]:csrrs a2, fcsr, zero
	-[0x800007b4]:sd t6, 96(fp)
Current Store : [0x800007b8] : sd a2, 104(fp) -- Store: [0x8000b810]:0x0000000000000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x800007cc]:fsgnjn.s t6, t5, zero
	-[0x800007d0]:csrrs a2, fcsr, zero
	-[0x800007d4]:sd t6, 112(fp)
Current Store : [0x800007d8] : sd a2, 120(fp) -- Store: [0x8000b820]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsgnjn.s zero, t6, t5
	-[0x800007f0]:csrrs a2, fcsr, zero
	-[0x800007f4]:sd zero, 128(fp)
Current Store : [0x800007f8] : sd a2, 136(fp) -- Store: [0x8000b830]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fsgnjn.s t6, t5, t4
	-[0x80000810]:csrrs a2, fcsr, zero
	-[0x80000814]:sd t6, 144(fp)
Current Store : [0x80000818] : sd a2, 152(fp) -- Store: [0x8000b840]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fsgnjn.s t6, t5, t4
	-[0x80000830]:csrrs a2, fcsr, zero
	-[0x80000834]:sd t6, 160(fp)
Current Store : [0x80000838] : sd a2, 168(fp) -- Store: [0x8000b850]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fsgnjn.s t6, t5, t4
	-[0x80000850]:csrrs a2, fcsr, zero
	-[0x80000854]:sd t6, 176(fp)
Current Store : [0x80000858] : sd a2, 184(fp) -- Store: [0x8000b860]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fsgnjn.s t6, t5, t4
	-[0x80000870]:csrrs a2, fcsr, zero
	-[0x80000874]:sd t6, 192(fp)
Current Store : [0x80000878] : sd a2, 200(fp) -- Store: [0x8000b870]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsgnjn.s t6, t5, t4
	-[0x80000890]:csrrs a2, fcsr, zero
	-[0x80000894]:sd t6, 208(fp)
Current Store : [0x80000898] : sd a2, 216(fp) -- Store: [0x8000b880]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fsgnjn.s t6, t5, t4
	-[0x800008b0]:csrrs a2, fcsr, zero
	-[0x800008b4]:sd t6, 224(fp)
Current Store : [0x800008b8] : sd a2, 232(fp) -- Store: [0x8000b890]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fsgnjn.s t6, t5, t4
	-[0x800008d0]:csrrs a2, fcsr, zero
	-[0x800008d4]:sd t6, 240(fp)
Current Store : [0x800008d8] : sd a2, 248(fp) -- Store: [0x8000b8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fsgnjn.s t6, t5, t4
	-[0x800008f0]:csrrs a2, fcsr, zero
	-[0x800008f4]:sd t6, 256(fp)
Current Store : [0x800008f8] : sd a2, 264(fp) -- Store: [0x8000b8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fsgnjn.s t6, t5, t4
	-[0x80000910]:csrrs a2, fcsr, zero
	-[0x80000914]:sd t6, 272(fp)
Current Store : [0x80000918] : sd a2, 280(fp) -- Store: [0x8000b8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsgnjn.s t6, t5, t4
	-[0x80000930]:csrrs a2, fcsr, zero
	-[0x80000934]:sd t6, 288(fp)
Current Store : [0x80000938] : sd a2, 296(fp) -- Store: [0x8000b8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fsgnjn.s t6, t5, t4
	-[0x80000950]:csrrs a2, fcsr, zero
	-[0x80000954]:sd t6, 304(fp)
Current Store : [0x80000958] : sd a2, 312(fp) -- Store: [0x8000b8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fsgnjn.s t6, t5, t4
	-[0x80000970]:csrrs a2, fcsr, zero
	-[0x80000974]:sd t6, 320(fp)
Current Store : [0x80000978] : sd a2, 328(fp) -- Store: [0x8000b8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fsgnjn.s t6, t5, t4
	-[0x80000990]:csrrs a2, fcsr, zero
	-[0x80000994]:sd t6, 336(fp)
Current Store : [0x80000998] : sd a2, 344(fp) -- Store: [0x8000b900]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fsgnjn.s t6, t5, t4
	-[0x800009b0]:csrrs a2, fcsr, zero
	-[0x800009b4]:sd t6, 352(fp)
Current Store : [0x800009b8] : sd a2, 360(fp) -- Store: [0x8000b910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsgnjn.s t6, t5, t4
	-[0x800009d0]:csrrs a2, fcsr, zero
	-[0x800009d4]:sd t6, 368(fp)
Current Store : [0x800009d8] : sd a2, 376(fp) -- Store: [0x8000b920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fsgnjn.s t6, t5, t4
	-[0x800009f0]:csrrs a2, fcsr, zero
	-[0x800009f4]:sd t6, 384(fp)
Current Store : [0x800009f8] : sd a2, 392(fp) -- Store: [0x8000b930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fsgnjn.s t6, t5, t4
	-[0x80000a10]:csrrs a2, fcsr, zero
	-[0x80000a14]:sd t6, 400(fp)
Current Store : [0x80000a18] : sd a2, 408(fp) -- Store: [0x8000b940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fsgnjn.s t6, t5, t4
	-[0x80000a30]:csrrs a2, fcsr, zero
	-[0x80000a34]:sd t6, 416(fp)
Current Store : [0x80000a38] : sd a2, 424(fp) -- Store: [0x8000b950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fsgnjn.s t6, t5, t4
	-[0x80000a50]:csrrs a2, fcsr, zero
	-[0x80000a54]:sd t6, 432(fp)
Current Store : [0x80000a58] : sd a2, 440(fp) -- Store: [0x8000b960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsgnjn.s t6, t5, t4
	-[0x80000a70]:csrrs a2, fcsr, zero
	-[0x80000a74]:sd t6, 448(fp)
Current Store : [0x80000a78] : sd a2, 456(fp) -- Store: [0x8000b970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fsgnjn.s t6, t5, t4
	-[0x80000a90]:csrrs a2, fcsr, zero
	-[0x80000a94]:sd t6, 464(fp)
Current Store : [0x80000a98] : sd a2, 472(fp) -- Store: [0x8000b980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fsgnjn.s t6, t5, t4
	-[0x80000ab0]:csrrs a2, fcsr, zero
	-[0x80000ab4]:sd t6, 480(fp)
Current Store : [0x80000ab8] : sd a2, 488(fp) -- Store: [0x8000b990]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fsgnjn.s t6, t5, t4
	-[0x80000ad0]:csrrs a2, fcsr, zero
	-[0x80000ad4]:sd t6, 496(fp)
Current Store : [0x80000ad8] : sd a2, 504(fp) -- Store: [0x8000b9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fsgnjn.s t6, t5, t4
	-[0x80000af0]:csrrs a2, fcsr, zero
	-[0x80000af4]:sd t6, 512(fp)
Current Store : [0x80000af8] : sd a2, 520(fp) -- Store: [0x8000b9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsgnjn.s t6, t5, t4
	-[0x80000b10]:csrrs a2, fcsr, zero
	-[0x80000b14]:sd t6, 528(fp)
Current Store : [0x80000b18] : sd a2, 536(fp) -- Store: [0x8000b9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fsgnjn.s t6, t5, t4
	-[0x80000b30]:csrrs a2, fcsr, zero
	-[0x80000b34]:sd t6, 544(fp)
Current Store : [0x80000b38] : sd a2, 552(fp) -- Store: [0x8000b9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fsgnjn.s t6, t5, t4
	-[0x80000b50]:csrrs a2, fcsr, zero
	-[0x80000b54]:sd t6, 560(fp)
Current Store : [0x80000b58] : sd a2, 568(fp) -- Store: [0x8000b9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fsgnjn.s t6, t5, t4
	-[0x80000b70]:csrrs a2, fcsr, zero
	-[0x80000b74]:sd t6, 576(fp)
Current Store : [0x80000b78] : sd a2, 584(fp) -- Store: [0x8000b9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fsgnjn.s t6, t5, t4
	-[0x80000b90]:csrrs a2, fcsr, zero
	-[0x80000b94]:sd t6, 592(fp)
Current Store : [0x80000b98] : sd a2, 600(fp) -- Store: [0x8000ba00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsgnjn.s t6, t5, t4
	-[0x80000bb0]:csrrs a2, fcsr, zero
	-[0x80000bb4]:sd t6, 608(fp)
Current Store : [0x80000bb8] : sd a2, 616(fp) -- Store: [0x8000ba10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fsgnjn.s t6, t5, t4
	-[0x80000bd0]:csrrs a2, fcsr, zero
	-[0x80000bd4]:sd t6, 624(fp)
Current Store : [0x80000bd8] : sd a2, 632(fp) -- Store: [0x8000ba20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fsgnjn.s t6, t5, t4
	-[0x80000bf0]:csrrs a2, fcsr, zero
	-[0x80000bf4]:sd t6, 640(fp)
Current Store : [0x80000bf8] : sd a2, 648(fp) -- Store: [0x8000ba30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fsgnjn.s t6, t5, t4
	-[0x80000c10]:csrrs a2, fcsr, zero
	-[0x80000c14]:sd t6, 656(fp)
Current Store : [0x80000c18] : sd a2, 664(fp) -- Store: [0x8000ba40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fsgnjn.s t6, t5, t4
	-[0x80000c30]:csrrs a2, fcsr, zero
	-[0x80000c34]:sd t6, 672(fp)
Current Store : [0x80000c38] : sd a2, 680(fp) -- Store: [0x8000ba50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsgnjn.s t6, t5, t4
	-[0x80000c50]:csrrs a2, fcsr, zero
	-[0x80000c54]:sd t6, 688(fp)
Current Store : [0x80000c58] : sd a2, 696(fp) -- Store: [0x8000ba60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fsgnjn.s t6, t5, t4
	-[0x80000c70]:csrrs a2, fcsr, zero
	-[0x80000c74]:sd t6, 704(fp)
Current Store : [0x80000c78] : sd a2, 712(fp) -- Store: [0x8000ba70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fsgnjn.s t6, t5, t4
	-[0x80000c90]:csrrs a2, fcsr, zero
	-[0x80000c94]:sd t6, 720(fp)
Current Store : [0x80000c98] : sd a2, 728(fp) -- Store: [0x8000ba80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fsgnjn.s t6, t5, t4
	-[0x80000cb0]:csrrs a2, fcsr, zero
	-[0x80000cb4]:sd t6, 736(fp)
Current Store : [0x80000cb8] : sd a2, 744(fp) -- Store: [0x8000ba90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fsgnjn.s t6, t5, t4
	-[0x80000cd0]:csrrs a2, fcsr, zero
	-[0x80000cd4]:sd t6, 752(fp)
Current Store : [0x80000cd8] : sd a2, 760(fp) -- Store: [0x8000baa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsgnjn.s t6, t5, t4
	-[0x80000cf0]:csrrs a2, fcsr, zero
	-[0x80000cf4]:sd t6, 768(fp)
Current Store : [0x80000cf8] : sd a2, 776(fp) -- Store: [0x8000bab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fsgnjn.s t6, t5, t4
	-[0x80000d10]:csrrs a2, fcsr, zero
	-[0x80000d14]:sd t6, 784(fp)
Current Store : [0x80000d18] : sd a2, 792(fp) -- Store: [0x8000bac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fsgnjn.s t6, t5, t4
	-[0x80000d30]:csrrs a2, fcsr, zero
	-[0x80000d34]:sd t6, 800(fp)
Current Store : [0x80000d38] : sd a2, 808(fp) -- Store: [0x8000bad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fsgnjn.s t6, t5, t4
	-[0x80000d50]:csrrs a2, fcsr, zero
	-[0x80000d54]:sd t6, 816(fp)
Current Store : [0x80000d58] : sd a2, 824(fp) -- Store: [0x8000bae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fsgnjn.s t6, t5, t4
	-[0x80000d70]:csrrs a2, fcsr, zero
	-[0x80000d74]:sd t6, 832(fp)
Current Store : [0x80000d78] : sd a2, 840(fp) -- Store: [0x8000baf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsgnjn.s t6, t5, t4
	-[0x80000d90]:csrrs a2, fcsr, zero
	-[0x80000d94]:sd t6, 848(fp)
Current Store : [0x80000d98] : sd a2, 856(fp) -- Store: [0x8000bb00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fsgnjn.s t6, t5, t4
	-[0x80000db0]:csrrs a2, fcsr, zero
	-[0x80000db4]:sd t6, 864(fp)
Current Store : [0x80000db8] : sd a2, 872(fp) -- Store: [0x8000bb10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dcc]:fsgnjn.s t6, t5, t4
	-[0x80000dd0]:csrrs a2, fcsr, zero
	-[0x80000dd4]:sd t6, 880(fp)
Current Store : [0x80000dd8] : sd a2, 888(fp) -- Store: [0x8000bb20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dec]:fsgnjn.s t6, t5, t4
	-[0x80000df0]:csrrs a2, fcsr, zero
	-[0x80000df4]:sd t6, 896(fp)
Current Store : [0x80000df8] : sd a2, 904(fp) -- Store: [0x8000bb30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fsgnjn.s t6, t5, t4
	-[0x80000e10]:csrrs a2, fcsr, zero
	-[0x80000e14]:sd t6, 912(fp)
Current Store : [0x80000e18] : sd a2, 920(fp) -- Store: [0x8000bb40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsgnjn.s t6, t5, t4
	-[0x80000e30]:csrrs a2, fcsr, zero
	-[0x80000e34]:sd t6, 928(fp)
Current Store : [0x80000e38] : sd a2, 936(fp) -- Store: [0x8000bb50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fsgnjn.s t6, t5, t4
	-[0x80000e50]:csrrs a2, fcsr, zero
	-[0x80000e54]:sd t6, 944(fp)
Current Store : [0x80000e58] : sd a2, 952(fp) -- Store: [0x8000bb60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fsgnjn.s t6, t5, t4
	-[0x80000e70]:csrrs a2, fcsr, zero
	-[0x80000e74]:sd t6, 960(fp)
Current Store : [0x80000e78] : sd a2, 968(fp) -- Store: [0x8000bb70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e8c]:fsgnjn.s t6, t5, t4
	-[0x80000e90]:csrrs a2, fcsr, zero
	-[0x80000e94]:sd t6, 976(fp)
Current Store : [0x80000e98] : sd a2, 984(fp) -- Store: [0x8000bb80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eac]:fsgnjn.s t6, t5, t4
	-[0x80000eb0]:csrrs a2, fcsr, zero
	-[0x80000eb4]:sd t6, 992(fp)
Current Store : [0x80000eb8] : sd a2, 1000(fp) -- Store: [0x8000bb90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsgnjn.s t6, t5, t4
	-[0x80000ed0]:csrrs a2, fcsr, zero
	-[0x80000ed4]:sd t6, 1008(fp)
Current Store : [0x80000ed8] : sd a2, 1016(fp) -- Store: [0x8000bba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eec]:fsgnjn.s t6, t5, t4
	-[0x80000ef0]:csrrs a2, fcsr, zero
	-[0x80000ef4]:sd t6, 1024(fp)
Current Store : [0x80000ef8] : sd a2, 1032(fp) -- Store: [0x8000bbb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fsgnjn.s t6, t5, t4
	-[0x80000f10]:csrrs a2, fcsr, zero
	-[0x80000f14]:sd t6, 1040(fp)
Current Store : [0x80000f18] : sd a2, 1048(fp) -- Store: [0x8000bbc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fsgnjn.s t6, t5, t4
	-[0x80000f30]:csrrs a2, fcsr, zero
	-[0x80000f34]:sd t6, 1056(fp)
Current Store : [0x80000f38] : sd a2, 1064(fp) -- Store: [0x8000bbd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fsgnjn.s t6, t5, t4
	-[0x80000f50]:csrrs a2, fcsr, zero
	-[0x80000f54]:sd t6, 1072(fp)
Current Store : [0x80000f58] : sd a2, 1080(fp) -- Store: [0x8000bbe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsgnjn.s t6, t5, t4
	-[0x80000f70]:csrrs a2, fcsr, zero
	-[0x80000f74]:sd t6, 1088(fp)
Current Store : [0x80000f78] : sd a2, 1096(fp) -- Store: [0x8000bbf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:fsgnjn.s t6, t5, t4
	-[0x80000f90]:csrrs a2, fcsr, zero
	-[0x80000f94]:sd t6, 1104(fp)
Current Store : [0x80000f98] : sd a2, 1112(fp) -- Store: [0x8000bc00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fac]:fsgnjn.s t6, t5, t4
	-[0x80000fb0]:csrrs a2, fcsr, zero
	-[0x80000fb4]:sd t6, 1120(fp)
Current Store : [0x80000fb8] : sd a2, 1128(fp) -- Store: [0x8000bc10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fcc]:fsgnjn.s t6, t5, t4
	-[0x80000fd0]:csrrs a2, fcsr, zero
	-[0x80000fd4]:sd t6, 1136(fp)
Current Store : [0x80000fd8] : sd a2, 1144(fp) -- Store: [0x8000bc20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fsgnjn.s t6, t5, t4
	-[0x80000ff0]:csrrs a2, fcsr, zero
	-[0x80000ff4]:sd t6, 1152(fp)
Current Store : [0x80000ff8] : sd a2, 1160(fp) -- Store: [0x8000bc30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsgnjn.s t6, t5, t4
	-[0x80001010]:csrrs a2, fcsr, zero
	-[0x80001014]:sd t6, 1168(fp)
Current Store : [0x80001018] : sd a2, 1176(fp) -- Store: [0x8000bc40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000102c]:fsgnjn.s t6, t5, t4
	-[0x80001030]:csrrs a2, fcsr, zero
	-[0x80001034]:sd t6, 1184(fp)
Current Store : [0x80001038] : sd a2, 1192(fp) -- Store: [0x8000bc50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000104c]:fsgnjn.s t6, t5, t4
	-[0x80001050]:csrrs a2, fcsr, zero
	-[0x80001054]:sd t6, 1200(fp)
Current Store : [0x80001058] : sd a2, 1208(fp) -- Store: [0x8000bc60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000106c]:fsgnjn.s t6, t5, t4
	-[0x80001070]:csrrs a2, fcsr, zero
	-[0x80001074]:sd t6, 1216(fp)
Current Store : [0x80001078] : sd a2, 1224(fp) -- Store: [0x8000bc70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000108c]:fsgnjn.s t6, t5, t4
	-[0x80001090]:csrrs a2, fcsr, zero
	-[0x80001094]:sd t6, 1232(fp)
Current Store : [0x80001098] : sd a2, 1240(fp) -- Store: [0x8000bc80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsgnjn.s t6, t5, t4
	-[0x800010b0]:csrrs a2, fcsr, zero
	-[0x800010b4]:sd t6, 1248(fp)
Current Store : [0x800010b8] : sd a2, 1256(fp) -- Store: [0x8000bc90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010cc]:fsgnjn.s t6, t5, t4
	-[0x800010d0]:csrrs a2, fcsr, zero
	-[0x800010d4]:sd t6, 1264(fp)
Current Store : [0x800010d8] : sd a2, 1272(fp) -- Store: [0x8000bca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010ec]:fsgnjn.s t6, t5, t4
	-[0x800010f0]:csrrs a2, fcsr, zero
	-[0x800010f4]:sd t6, 1280(fp)
Current Store : [0x800010f8] : sd a2, 1288(fp) -- Store: [0x8000bcb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fsgnjn.s t6, t5, t4
	-[0x80001110]:csrrs a2, fcsr, zero
	-[0x80001114]:sd t6, 1296(fp)
Current Store : [0x80001118] : sd a2, 1304(fp) -- Store: [0x8000bcc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000112c]:fsgnjn.s t6, t5, t4
	-[0x80001130]:csrrs a2, fcsr, zero
	-[0x80001134]:sd t6, 1312(fp)
Current Store : [0x80001138] : sd a2, 1320(fp) -- Store: [0x8000bcd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsgnjn.s t6, t5, t4
	-[0x80001150]:csrrs a2, fcsr, zero
	-[0x80001154]:sd t6, 1328(fp)
Current Store : [0x80001158] : sd a2, 1336(fp) -- Store: [0x8000bce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000116c]:fsgnjn.s t6, t5, t4
	-[0x80001170]:csrrs a2, fcsr, zero
	-[0x80001174]:sd t6, 1344(fp)
Current Store : [0x80001178] : sd a2, 1352(fp) -- Store: [0x8000bcf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000118c]:fsgnjn.s t6, t5, t4
	-[0x80001190]:csrrs a2, fcsr, zero
	-[0x80001194]:sd t6, 1360(fp)
Current Store : [0x80001198] : sd a2, 1368(fp) -- Store: [0x8000bd00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800011ac]:fsgnjn.s t6, t5, t4
	-[0x800011b0]:csrrs a2, fcsr, zero
	-[0x800011b4]:sd t6, 1376(fp)
Current Store : [0x800011b8] : sd a2, 1384(fp) -- Store: [0x8000bd10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011cc]:fsgnjn.s t6, t5, t4
	-[0x800011d0]:csrrs a2, fcsr, zero
	-[0x800011d4]:sd t6, 1392(fp)
Current Store : [0x800011d8] : sd a2, 1400(fp) -- Store: [0x8000bd20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsgnjn.s t6, t5, t4
	-[0x800011f0]:csrrs a2, fcsr, zero
	-[0x800011f4]:sd t6, 1408(fp)
Current Store : [0x800011f8] : sd a2, 1416(fp) -- Store: [0x8000bd30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000120c]:fsgnjn.s t6, t5, t4
	-[0x80001210]:csrrs a2, fcsr, zero
	-[0x80001214]:sd t6, 1424(fp)
Current Store : [0x80001218] : sd a2, 1432(fp) -- Store: [0x8000bd40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fsgnjn.s t6, t5, t4
	-[0x80001230]:csrrs a2, fcsr, zero
	-[0x80001234]:sd t6, 1440(fp)
Current Store : [0x80001238] : sd a2, 1448(fp) -- Store: [0x8000bd50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000124c]:fsgnjn.s t6, t5, t4
	-[0x80001250]:csrrs a2, fcsr, zero
	-[0x80001254]:sd t6, 1456(fp)
Current Store : [0x80001258] : sd a2, 1464(fp) -- Store: [0x8000bd60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000126c]:fsgnjn.s t6, t5, t4
	-[0x80001270]:csrrs a2, fcsr, zero
	-[0x80001274]:sd t6, 1472(fp)
Current Store : [0x80001278] : sd a2, 1480(fp) -- Store: [0x8000bd70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsgnjn.s t6, t5, t4
	-[0x80001290]:csrrs a2, fcsr, zero
	-[0x80001294]:sd t6, 1488(fp)
Current Store : [0x80001298] : sd a2, 1496(fp) -- Store: [0x8000bd80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ac]:fsgnjn.s t6, t5, t4
	-[0x800012b0]:csrrs a2, fcsr, zero
	-[0x800012b4]:sd t6, 1504(fp)
Current Store : [0x800012b8] : sd a2, 1512(fp) -- Store: [0x8000bd90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800012cc]:fsgnjn.s t6, t5, t4
	-[0x800012d0]:csrrs a2, fcsr, zero
	-[0x800012d4]:sd t6, 1520(fp)
Current Store : [0x800012d8] : sd a2, 1528(fp) -- Store: [0x8000bda0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ec]:fsgnjn.s t6, t5, t4
	-[0x800012f0]:csrrs a2, fcsr, zero
	-[0x800012f4]:sd t6, 1536(fp)
Current Store : [0x800012f8] : sd a2, 1544(fp) -- Store: [0x8000bdb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000130c]:fsgnjn.s t6, t5, t4
	-[0x80001310]:csrrs a2, fcsr, zero
	-[0x80001314]:sd t6, 1552(fp)
Current Store : [0x80001318] : sd a2, 1560(fp) -- Store: [0x8000bdc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsgnjn.s t6, t5, t4
	-[0x80001330]:csrrs a2, fcsr, zero
	-[0x80001334]:sd t6, 1568(fp)
Current Store : [0x80001338] : sd a2, 1576(fp) -- Store: [0x8000bdd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000134c]:fsgnjn.s t6, t5, t4
	-[0x80001350]:csrrs a2, fcsr, zero
	-[0x80001354]:sd t6, 1584(fp)
Current Store : [0x80001358] : sd a2, 1592(fp) -- Store: [0x8000bde0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000136c]:fsgnjn.s t6, t5, t4
	-[0x80001370]:csrrs a2, fcsr, zero
	-[0x80001374]:sd t6, 1600(fp)
Current Store : [0x80001378] : sd a2, 1608(fp) -- Store: [0x8000bdf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000138c]:fsgnjn.s t6, t5, t4
	-[0x80001390]:csrrs a2, fcsr, zero
	-[0x80001394]:sd t6, 1616(fp)
Current Store : [0x80001398] : sd a2, 1624(fp) -- Store: [0x8000be00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ac]:fsgnjn.s t6, t5, t4
	-[0x800013b0]:csrrs a2, fcsr, zero
	-[0x800013b4]:sd t6, 1632(fp)
Current Store : [0x800013b8] : sd a2, 1640(fp) -- Store: [0x8000be10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsgnjn.s t6, t5, t4
	-[0x800013d0]:csrrs a2, fcsr, zero
	-[0x800013d4]:sd t6, 1648(fp)
Current Store : [0x800013d8] : sd a2, 1656(fp) -- Store: [0x8000be20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ec]:fsgnjn.s t6, t5, t4
	-[0x800013f0]:csrrs a2, fcsr, zero
	-[0x800013f4]:sd t6, 1664(fp)
Current Store : [0x800013f8] : sd a2, 1672(fp) -- Store: [0x8000be30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000140c]:fsgnjn.s t6, t5, t4
	-[0x80001410]:csrrs a2, fcsr, zero
	-[0x80001414]:sd t6, 1680(fp)
Current Store : [0x80001418] : sd a2, 1688(fp) -- Store: [0x8000be40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fsgnjn.s t6, t5, t4
	-[0x80001430]:csrrs a2, fcsr, zero
	-[0x80001434]:sd t6, 1696(fp)
Current Store : [0x80001438] : sd a2, 1704(fp) -- Store: [0x8000be50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000144c]:fsgnjn.s t6, t5, t4
	-[0x80001450]:csrrs a2, fcsr, zero
	-[0x80001454]:sd t6, 1712(fp)
Current Store : [0x80001458] : sd a2, 1720(fp) -- Store: [0x8000be60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsgnjn.s t6, t5, t4
	-[0x80001470]:csrrs a2, fcsr, zero
	-[0x80001474]:sd t6, 1728(fp)
Current Store : [0x80001478] : sd a2, 1736(fp) -- Store: [0x8000be70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000148c]:fsgnjn.s t6, t5, t4
	-[0x80001490]:csrrs a2, fcsr, zero
	-[0x80001494]:sd t6, 1744(fp)
Current Store : [0x80001498] : sd a2, 1752(fp) -- Store: [0x8000be80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ac]:fsgnjn.s t6, t5, t4
	-[0x800014b0]:csrrs a2, fcsr, zero
	-[0x800014b4]:sd t6, 1760(fp)
Current Store : [0x800014b8] : sd a2, 1768(fp) -- Store: [0x8000be90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800014cc]:fsgnjn.s t6, t5, t4
	-[0x800014d0]:csrrs a2, fcsr, zero
	-[0x800014d4]:sd t6, 1776(fp)
Current Store : [0x800014d8] : sd a2, 1784(fp) -- Store: [0x8000bea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ec]:fsgnjn.s t6, t5, t4
	-[0x800014f0]:csrrs a2, fcsr, zero
	-[0x800014f4]:sd t6, 1792(fp)
Current Store : [0x800014f8] : sd a2, 1800(fp) -- Store: [0x8000beb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsgnjn.s t6, t5, t4
	-[0x80001510]:csrrs a2, fcsr, zero
	-[0x80001514]:sd t6, 1808(fp)
Current Store : [0x80001518] : sd a2, 1816(fp) -- Store: [0x8000bec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000152c]:fsgnjn.s t6, t5, t4
	-[0x80001530]:csrrs a2, fcsr, zero
	-[0x80001534]:sd t6, 1824(fp)
Current Store : [0x80001538] : sd a2, 1832(fp) -- Store: [0x8000bed0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fsgnjn.s t6, t5, t4
	-[0x80001550]:csrrs a2, fcsr, zero
	-[0x80001554]:sd t6, 1840(fp)
Current Store : [0x80001558] : sd a2, 1848(fp) -- Store: [0x8000bee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000156c]:fsgnjn.s t6, t5, t4
	-[0x80001570]:csrrs a2, fcsr, zero
	-[0x80001574]:sd t6, 1856(fp)
Current Store : [0x80001578] : sd a2, 1864(fp) -- Store: [0x8000bef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000158c]:fsgnjn.s t6, t5, t4
	-[0x80001590]:csrrs a2, fcsr, zero
	-[0x80001594]:sd t6, 1872(fp)
Current Store : [0x80001598] : sd a2, 1880(fp) -- Store: [0x8000bf00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsgnjn.s t6, t5, t4
	-[0x800015b0]:csrrs a2, fcsr, zero
	-[0x800015b4]:sd t6, 1888(fp)
Current Store : [0x800015b8] : sd a2, 1896(fp) -- Store: [0x8000bf10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015cc]:fsgnjn.s t6, t5, t4
	-[0x800015d0]:csrrs a2, fcsr, zero
	-[0x800015d4]:sd t6, 1904(fp)
Current Store : [0x800015d8] : sd a2, 1912(fp) -- Store: [0x8000bf20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800015ec]:fsgnjn.s t6, t5, t4
	-[0x800015f0]:csrrs a2, fcsr, zero
	-[0x800015f4]:sd t6, 1920(fp)
Current Store : [0x800015f8] : sd a2, 1928(fp) -- Store: [0x8000bf30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000160c]:fsgnjn.s t6, t5, t4
	-[0x80001610]:csrrs a2, fcsr, zero
	-[0x80001614]:sd t6, 1936(fp)
Current Store : [0x80001618] : sd a2, 1944(fp) -- Store: [0x8000bf40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000162c]:fsgnjn.s t6, t5, t4
	-[0x80001630]:csrrs a2, fcsr, zero
	-[0x80001634]:sd t6, 1952(fp)
Current Store : [0x80001638] : sd a2, 1960(fp) -- Store: [0x8000bf50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsgnjn.s t6, t5, t4
	-[0x80001650]:csrrs a2, fcsr, zero
	-[0x80001654]:sd t6, 1968(fp)
Current Store : [0x80001658] : sd a2, 1976(fp) -- Store: [0x8000bf60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000166c]:fsgnjn.s t6, t5, t4
	-[0x80001670]:csrrs a2, fcsr, zero
	-[0x80001674]:sd t6, 1984(fp)
Current Store : [0x80001678] : sd a2, 1992(fp) -- Store: [0x8000bf70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800016ac]:fsgnjn.s t6, t5, t4
	-[0x800016b0]:csrrs a2, fcsr, zero
	-[0x800016b4]:sd t6, 2000(fp)
Current Store : [0x800016b8] : sd a2, 2008(fp) -- Store: [0x8000bf80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800016ec]:fsgnjn.s t6, t5, t4
	-[0x800016f0]:csrrs a2, fcsr, zero
	-[0x800016f4]:sd t6, 2016(fp)
Current Store : [0x800016f8] : sd a2, 2024(fp) -- Store: [0x8000bf90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000172c]:fsgnjn.s t6, t5, t4
	-[0x80001730]:csrrs a2, fcsr, zero
	-[0x80001734]:sd t6, 2032(fp)
Current Store : [0x80001738] : sd a2, 2040(fp) -- Store: [0x8000bfa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001774]:fsgnjn.s t6, t5, t4
	-[0x80001778]:csrrs a2, fcsr, zero
	-[0x8000177c]:sd t6, 0(fp)
Current Store : [0x80001780] : sd a2, 8(fp) -- Store: [0x8000bfb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800017b4]:fsgnjn.s t6, t5, t4
	-[0x800017b8]:csrrs a2, fcsr, zero
	-[0x800017bc]:sd t6, 16(fp)
Current Store : [0x800017c0] : sd a2, 24(fp) -- Store: [0x8000bfc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800017f4]:fsgnjn.s t6, t5, t4
	-[0x800017f8]:csrrs a2, fcsr, zero
	-[0x800017fc]:sd t6, 32(fp)
Current Store : [0x80001800] : sd a2, 40(fp) -- Store: [0x8000bfd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001834]:fsgnjn.s t6, t5, t4
	-[0x80001838]:csrrs a2, fcsr, zero
	-[0x8000183c]:sd t6, 48(fp)
Current Store : [0x80001840] : sd a2, 56(fp) -- Store: [0x8000bfe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001874]:fsgnjn.s t6, t5, t4
	-[0x80001878]:csrrs a2, fcsr, zero
	-[0x8000187c]:sd t6, 64(fp)
Current Store : [0x80001880] : sd a2, 72(fp) -- Store: [0x8000bff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800018b4]:fsgnjn.s t6, t5, t4
	-[0x800018b8]:csrrs a2, fcsr, zero
	-[0x800018bc]:sd t6, 80(fp)
Current Store : [0x800018c0] : sd a2, 88(fp) -- Store: [0x8000c000]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800018f4]:fsgnjn.s t6, t5, t4
	-[0x800018f8]:csrrs a2, fcsr, zero
	-[0x800018fc]:sd t6, 96(fp)
Current Store : [0x80001900] : sd a2, 104(fp) -- Store: [0x8000c010]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001934]:fsgnjn.s t6, t5, t4
	-[0x80001938]:csrrs a2, fcsr, zero
	-[0x8000193c]:sd t6, 112(fp)
Current Store : [0x80001940] : sd a2, 120(fp) -- Store: [0x8000c020]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001974]:fsgnjn.s t6, t5, t4
	-[0x80001978]:csrrs a2, fcsr, zero
	-[0x8000197c]:sd t6, 128(fp)
Current Store : [0x80001980] : sd a2, 136(fp) -- Store: [0x8000c030]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800019b4]:fsgnjn.s t6, t5, t4
	-[0x800019b8]:csrrs a2, fcsr, zero
	-[0x800019bc]:sd t6, 144(fp)
Current Store : [0x800019c0] : sd a2, 152(fp) -- Store: [0x8000c040]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800019f4]:fsgnjn.s t6, t5, t4
	-[0x800019f8]:csrrs a2, fcsr, zero
	-[0x800019fc]:sd t6, 160(fp)
Current Store : [0x80001a00] : sd a2, 168(fp) -- Store: [0x8000c050]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001a34]:fsgnjn.s t6, t5, t4
	-[0x80001a38]:csrrs a2, fcsr, zero
	-[0x80001a3c]:sd t6, 176(fp)
Current Store : [0x80001a40] : sd a2, 184(fp) -- Store: [0x8000c060]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001a74]:fsgnjn.s t6, t5, t4
	-[0x80001a78]:csrrs a2, fcsr, zero
	-[0x80001a7c]:sd t6, 192(fp)
Current Store : [0x80001a80] : sd a2, 200(fp) -- Store: [0x8000c070]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ab4]:fsgnjn.s t6, t5, t4
	-[0x80001ab8]:csrrs a2, fcsr, zero
	-[0x80001abc]:sd t6, 208(fp)
Current Store : [0x80001ac0] : sd a2, 216(fp) -- Store: [0x8000c080]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001af4]:fsgnjn.s t6, t5, t4
	-[0x80001af8]:csrrs a2, fcsr, zero
	-[0x80001afc]:sd t6, 224(fp)
Current Store : [0x80001b00] : sd a2, 232(fp) -- Store: [0x8000c090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001b34]:fsgnjn.s t6, t5, t4
	-[0x80001b38]:csrrs a2, fcsr, zero
	-[0x80001b3c]:sd t6, 240(fp)
Current Store : [0x80001b40] : sd a2, 248(fp) -- Store: [0x8000c0a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001b74]:fsgnjn.s t6, t5, t4
	-[0x80001b78]:csrrs a2, fcsr, zero
	-[0x80001b7c]:sd t6, 256(fp)
Current Store : [0x80001b80] : sd a2, 264(fp) -- Store: [0x8000c0b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001bb4]:fsgnjn.s t6, t5, t4
	-[0x80001bb8]:csrrs a2, fcsr, zero
	-[0x80001bbc]:sd t6, 272(fp)
Current Store : [0x80001bc0] : sd a2, 280(fp) -- Store: [0x8000c0c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001bf4]:fsgnjn.s t6, t5, t4
	-[0x80001bf8]:csrrs a2, fcsr, zero
	-[0x80001bfc]:sd t6, 288(fp)
Current Store : [0x80001c00] : sd a2, 296(fp) -- Store: [0x8000c0d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001c34]:fsgnjn.s t6, t5, t4
	-[0x80001c38]:csrrs a2, fcsr, zero
	-[0x80001c3c]:sd t6, 304(fp)
Current Store : [0x80001c40] : sd a2, 312(fp) -- Store: [0x8000c0e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001c74]:fsgnjn.s t6, t5, t4
	-[0x80001c78]:csrrs a2, fcsr, zero
	-[0x80001c7c]:sd t6, 320(fp)
Current Store : [0x80001c80] : sd a2, 328(fp) -- Store: [0x8000c0f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001cb4]:fsgnjn.s t6, t5, t4
	-[0x80001cb8]:csrrs a2, fcsr, zero
	-[0x80001cbc]:sd t6, 336(fp)
Current Store : [0x80001cc0] : sd a2, 344(fp) -- Store: [0x8000c100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001cf4]:fsgnjn.s t6, t5, t4
	-[0x80001cf8]:csrrs a2, fcsr, zero
	-[0x80001cfc]:sd t6, 352(fp)
Current Store : [0x80001d00] : sd a2, 360(fp) -- Store: [0x8000c110]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001d34]:fsgnjn.s t6, t5, t4
	-[0x80001d38]:csrrs a2, fcsr, zero
	-[0x80001d3c]:sd t6, 368(fp)
Current Store : [0x80001d40] : sd a2, 376(fp) -- Store: [0x8000c120]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001d74]:fsgnjn.s t6, t5, t4
	-[0x80001d78]:csrrs a2, fcsr, zero
	-[0x80001d7c]:sd t6, 384(fp)
Current Store : [0x80001d80] : sd a2, 392(fp) -- Store: [0x8000c130]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001db4]:fsgnjn.s t6, t5, t4
	-[0x80001db8]:csrrs a2, fcsr, zero
	-[0x80001dbc]:sd t6, 400(fp)
Current Store : [0x80001dc0] : sd a2, 408(fp) -- Store: [0x8000c140]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001df4]:fsgnjn.s t6, t5, t4
	-[0x80001df8]:csrrs a2, fcsr, zero
	-[0x80001dfc]:sd t6, 416(fp)
Current Store : [0x80001e00] : sd a2, 424(fp) -- Store: [0x8000c150]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001e34]:fsgnjn.s t6, t5, t4
	-[0x80001e38]:csrrs a2, fcsr, zero
	-[0x80001e3c]:sd t6, 432(fp)
Current Store : [0x80001e40] : sd a2, 440(fp) -- Store: [0x8000c160]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001e74]:fsgnjn.s t6, t5, t4
	-[0x80001e78]:csrrs a2, fcsr, zero
	-[0x80001e7c]:sd t6, 448(fp)
Current Store : [0x80001e80] : sd a2, 456(fp) -- Store: [0x8000c170]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001eb4]:fsgnjn.s t6, t5, t4
	-[0x80001eb8]:csrrs a2, fcsr, zero
	-[0x80001ebc]:sd t6, 464(fp)
Current Store : [0x80001ec0] : sd a2, 472(fp) -- Store: [0x8000c180]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001ef4]:fsgnjn.s t6, t5, t4
	-[0x80001ef8]:csrrs a2, fcsr, zero
	-[0x80001efc]:sd t6, 480(fp)
Current Store : [0x80001f00] : sd a2, 488(fp) -- Store: [0x8000c190]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001f34]:fsgnjn.s t6, t5, t4
	-[0x80001f38]:csrrs a2, fcsr, zero
	-[0x80001f3c]:sd t6, 496(fp)
Current Store : [0x80001f40] : sd a2, 504(fp) -- Store: [0x8000c1a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001f74]:fsgnjn.s t6, t5, t4
	-[0x80001f78]:csrrs a2, fcsr, zero
	-[0x80001f7c]:sd t6, 512(fp)
Current Store : [0x80001f80] : sd a2, 520(fp) -- Store: [0x8000c1b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001fb4]:fsgnjn.s t6, t5, t4
	-[0x80001fb8]:csrrs a2, fcsr, zero
	-[0x80001fbc]:sd t6, 528(fp)
Current Store : [0x80001fc0] : sd a2, 536(fp) -- Store: [0x8000c1c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001ff4]:fsgnjn.s t6, t5, t4
	-[0x80001ff8]:csrrs a2, fcsr, zero
	-[0x80001ffc]:sd t6, 544(fp)
Current Store : [0x80002000] : sd a2, 552(fp) -- Store: [0x8000c1d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002034]:fsgnjn.s t6, t5, t4
	-[0x80002038]:csrrs a2, fcsr, zero
	-[0x8000203c]:sd t6, 560(fp)
Current Store : [0x80002040] : sd a2, 568(fp) -- Store: [0x8000c1e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002074]:fsgnjn.s t6, t5, t4
	-[0x80002078]:csrrs a2, fcsr, zero
	-[0x8000207c]:sd t6, 576(fp)
Current Store : [0x80002080] : sd a2, 584(fp) -- Store: [0x8000c1f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800020b4]:fsgnjn.s t6, t5, t4
	-[0x800020b8]:csrrs a2, fcsr, zero
	-[0x800020bc]:sd t6, 592(fp)
Current Store : [0x800020c0] : sd a2, 600(fp) -- Store: [0x8000c200]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800020f4]:fsgnjn.s t6, t5, t4
	-[0x800020f8]:csrrs a2, fcsr, zero
	-[0x800020fc]:sd t6, 608(fp)
Current Store : [0x80002100] : sd a2, 616(fp) -- Store: [0x8000c210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002134]:fsgnjn.s t6, t5, t4
	-[0x80002138]:csrrs a2, fcsr, zero
	-[0x8000213c]:sd t6, 624(fp)
Current Store : [0x80002140] : sd a2, 632(fp) -- Store: [0x8000c220]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002174]:fsgnjn.s t6, t5, t4
	-[0x80002178]:csrrs a2, fcsr, zero
	-[0x8000217c]:sd t6, 640(fp)
Current Store : [0x80002180] : sd a2, 648(fp) -- Store: [0x8000c230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800021b4]:fsgnjn.s t6, t5, t4
	-[0x800021b8]:csrrs a2, fcsr, zero
	-[0x800021bc]:sd t6, 656(fp)
Current Store : [0x800021c0] : sd a2, 664(fp) -- Store: [0x8000c240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800021f4]:fsgnjn.s t6, t5, t4
	-[0x800021f8]:csrrs a2, fcsr, zero
	-[0x800021fc]:sd t6, 672(fp)
Current Store : [0x80002200] : sd a2, 680(fp) -- Store: [0x8000c250]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002234]:fsgnjn.s t6, t5, t4
	-[0x80002238]:csrrs a2, fcsr, zero
	-[0x8000223c]:sd t6, 688(fp)
Current Store : [0x80002240] : sd a2, 696(fp) -- Store: [0x8000c260]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002274]:fsgnjn.s t6, t5, t4
	-[0x80002278]:csrrs a2, fcsr, zero
	-[0x8000227c]:sd t6, 704(fp)
Current Store : [0x80002280] : sd a2, 712(fp) -- Store: [0x8000c270]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800022b4]:fsgnjn.s t6, t5, t4
	-[0x800022b8]:csrrs a2, fcsr, zero
	-[0x800022bc]:sd t6, 720(fp)
Current Store : [0x800022c0] : sd a2, 728(fp) -- Store: [0x8000c280]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800022f4]:fsgnjn.s t6, t5, t4
	-[0x800022f8]:csrrs a2, fcsr, zero
	-[0x800022fc]:sd t6, 736(fp)
Current Store : [0x80002300] : sd a2, 744(fp) -- Store: [0x8000c290]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002334]:fsgnjn.s t6, t5, t4
	-[0x80002338]:csrrs a2, fcsr, zero
	-[0x8000233c]:sd t6, 752(fp)
Current Store : [0x80002340] : sd a2, 760(fp) -- Store: [0x8000c2a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002374]:fsgnjn.s t6, t5, t4
	-[0x80002378]:csrrs a2, fcsr, zero
	-[0x8000237c]:sd t6, 768(fp)
Current Store : [0x80002380] : sd a2, 776(fp) -- Store: [0x8000c2b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800023b4]:fsgnjn.s t6, t5, t4
	-[0x800023b8]:csrrs a2, fcsr, zero
	-[0x800023bc]:sd t6, 784(fp)
Current Store : [0x800023c0] : sd a2, 792(fp) -- Store: [0x8000c2c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800023f4]:fsgnjn.s t6, t5, t4
	-[0x800023f8]:csrrs a2, fcsr, zero
	-[0x800023fc]:sd t6, 800(fp)
Current Store : [0x80002400] : sd a2, 808(fp) -- Store: [0x8000c2d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002434]:fsgnjn.s t6, t5, t4
	-[0x80002438]:csrrs a2, fcsr, zero
	-[0x8000243c]:sd t6, 816(fp)
Current Store : [0x80002440] : sd a2, 824(fp) -- Store: [0x8000c2e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002474]:fsgnjn.s t6, t5, t4
	-[0x80002478]:csrrs a2, fcsr, zero
	-[0x8000247c]:sd t6, 832(fp)
Current Store : [0x80002480] : sd a2, 840(fp) -- Store: [0x8000c2f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800024b4]:fsgnjn.s t6, t5, t4
	-[0x800024b8]:csrrs a2, fcsr, zero
	-[0x800024bc]:sd t6, 848(fp)
Current Store : [0x800024c0] : sd a2, 856(fp) -- Store: [0x8000c300]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800024f4]:fsgnjn.s t6, t5, t4
	-[0x800024f8]:csrrs a2, fcsr, zero
	-[0x800024fc]:sd t6, 864(fp)
Current Store : [0x80002500] : sd a2, 872(fp) -- Store: [0x8000c310]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002534]:fsgnjn.s t6, t5, t4
	-[0x80002538]:csrrs a2, fcsr, zero
	-[0x8000253c]:sd t6, 880(fp)
Current Store : [0x80002540] : sd a2, 888(fp) -- Store: [0x8000c320]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002574]:fsgnjn.s t6, t5, t4
	-[0x80002578]:csrrs a2, fcsr, zero
	-[0x8000257c]:sd t6, 896(fp)
Current Store : [0x80002580] : sd a2, 904(fp) -- Store: [0x8000c330]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800025b4]:fsgnjn.s t6, t5, t4
	-[0x800025b8]:csrrs a2, fcsr, zero
	-[0x800025bc]:sd t6, 912(fp)
Current Store : [0x800025c0] : sd a2, 920(fp) -- Store: [0x8000c340]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800025f4]:fsgnjn.s t6, t5, t4
	-[0x800025f8]:csrrs a2, fcsr, zero
	-[0x800025fc]:sd t6, 928(fp)
Current Store : [0x80002600] : sd a2, 936(fp) -- Store: [0x8000c350]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002634]:fsgnjn.s t6, t5, t4
	-[0x80002638]:csrrs a2, fcsr, zero
	-[0x8000263c]:sd t6, 944(fp)
Current Store : [0x80002640] : sd a2, 952(fp) -- Store: [0x8000c360]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002674]:fsgnjn.s t6, t5, t4
	-[0x80002678]:csrrs a2, fcsr, zero
	-[0x8000267c]:sd t6, 960(fp)
Current Store : [0x80002680] : sd a2, 968(fp) -- Store: [0x8000c370]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800026b4]:fsgnjn.s t6, t5, t4
	-[0x800026b8]:csrrs a2, fcsr, zero
	-[0x800026bc]:sd t6, 976(fp)
Current Store : [0x800026c0] : sd a2, 984(fp) -- Store: [0x8000c380]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800026f4]:fsgnjn.s t6, t5, t4
	-[0x800026f8]:csrrs a2, fcsr, zero
	-[0x800026fc]:sd t6, 992(fp)
Current Store : [0x80002700] : sd a2, 1000(fp) -- Store: [0x8000c390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002734]:fsgnjn.s t6, t5, t4
	-[0x80002738]:csrrs a2, fcsr, zero
	-[0x8000273c]:sd t6, 1008(fp)
Current Store : [0x80002740] : sd a2, 1016(fp) -- Store: [0x8000c3a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002774]:fsgnjn.s t6, t5, t4
	-[0x80002778]:csrrs a2, fcsr, zero
	-[0x8000277c]:sd t6, 1024(fp)
Current Store : [0x80002780] : sd a2, 1032(fp) -- Store: [0x8000c3b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800027b4]:fsgnjn.s t6, t5, t4
	-[0x800027b8]:csrrs a2, fcsr, zero
	-[0x800027bc]:sd t6, 1040(fp)
Current Store : [0x800027c0] : sd a2, 1048(fp) -- Store: [0x8000c3c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800027f4]:fsgnjn.s t6, t5, t4
	-[0x800027f8]:csrrs a2, fcsr, zero
	-[0x800027fc]:sd t6, 1056(fp)
Current Store : [0x80002800] : sd a2, 1064(fp) -- Store: [0x8000c3d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002834]:fsgnjn.s t6, t5, t4
	-[0x80002838]:csrrs a2, fcsr, zero
	-[0x8000283c]:sd t6, 1072(fp)
Current Store : [0x80002840] : sd a2, 1080(fp) -- Store: [0x8000c3e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002874]:fsgnjn.s t6, t5, t4
	-[0x80002878]:csrrs a2, fcsr, zero
	-[0x8000287c]:sd t6, 1088(fp)
Current Store : [0x80002880] : sd a2, 1096(fp) -- Store: [0x8000c3f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800028b4]:fsgnjn.s t6, t5, t4
	-[0x800028b8]:csrrs a2, fcsr, zero
	-[0x800028bc]:sd t6, 1104(fp)
Current Store : [0x800028c0] : sd a2, 1112(fp) -- Store: [0x8000c400]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800028f4]:fsgnjn.s t6, t5, t4
	-[0x800028f8]:csrrs a2, fcsr, zero
	-[0x800028fc]:sd t6, 1120(fp)
Current Store : [0x80002900] : sd a2, 1128(fp) -- Store: [0x8000c410]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002934]:fsgnjn.s t6, t5, t4
	-[0x80002938]:csrrs a2, fcsr, zero
	-[0x8000293c]:sd t6, 1136(fp)
Current Store : [0x80002940] : sd a2, 1144(fp) -- Store: [0x8000c420]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002974]:fsgnjn.s t6, t5, t4
	-[0x80002978]:csrrs a2, fcsr, zero
	-[0x8000297c]:sd t6, 1152(fp)
Current Store : [0x80002980] : sd a2, 1160(fp) -- Store: [0x8000c430]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800029b4]:fsgnjn.s t6, t5, t4
	-[0x800029b8]:csrrs a2, fcsr, zero
	-[0x800029bc]:sd t6, 1168(fp)
Current Store : [0x800029c0] : sd a2, 1176(fp) -- Store: [0x8000c440]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800029f4]:fsgnjn.s t6, t5, t4
	-[0x800029f8]:csrrs a2, fcsr, zero
	-[0x800029fc]:sd t6, 1184(fp)
Current Store : [0x80002a00] : sd a2, 1192(fp) -- Store: [0x8000c450]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002a34]:fsgnjn.s t6, t5, t4
	-[0x80002a38]:csrrs a2, fcsr, zero
	-[0x80002a3c]:sd t6, 1200(fp)
Current Store : [0x80002a40] : sd a2, 1208(fp) -- Store: [0x8000c460]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002a74]:fsgnjn.s t6, t5, t4
	-[0x80002a78]:csrrs a2, fcsr, zero
	-[0x80002a7c]:sd t6, 1216(fp)
Current Store : [0x80002a80] : sd a2, 1224(fp) -- Store: [0x8000c470]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002ab4]:fsgnjn.s t6, t5, t4
	-[0x80002ab8]:csrrs a2, fcsr, zero
	-[0x80002abc]:sd t6, 1232(fp)
Current Store : [0x80002ac0] : sd a2, 1240(fp) -- Store: [0x8000c480]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002af4]:fsgnjn.s t6, t5, t4
	-[0x80002af8]:csrrs a2, fcsr, zero
	-[0x80002afc]:sd t6, 1248(fp)
Current Store : [0x80002b00] : sd a2, 1256(fp) -- Store: [0x8000c490]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002b34]:fsgnjn.s t6, t5, t4
	-[0x80002b38]:csrrs a2, fcsr, zero
	-[0x80002b3c]:sd t6, 1264(fp)
Current Store : [0x80002b40] : sd a2, 1272(fp) -- Store: [0x8000c4a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002b74]:fsgnjn.s t6, t5, t4
	-[0x80002b78]:csrrs a2, fcsr, zero
	-[0x80002b7c]:sd t6, 1280(fp)
Current Store : [0x80002b80] : sd a2, 1288(fp) -- Store: [0x8000c4b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002bb4]:fsgnjn.s t6, t5, t4
	-[0x80002bb8]:csrrs a2, fcsr, zero
	-[0x80002bbc]:sd t6, 1296(fp)
Current Store : [0x80002bc0] : sd a2, 1304(fp) -- Store: [0x8000c4c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002bf4]:fsgnjn.s t6, t5, t4
	-[0x80002bf8]:csrrs a2, fcsr, zero
	-[0x80002bfc]:sd t6, 1312(fp)
Current Store : [0x80002c00] : sd a2, 1320(fp) -- Store: [0x8000c4d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002c34]:fsgnjn.s t6, t5, t4
	-[0x80002c38]:csrrs a2, fcsr, zero
	-[0x80002c3c]:sd t6, 1328(fp)
Current Store : [0x80002c40] : sd a2, 1336(fp) -- Store: [0x8000c4e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002c74]:fsgnjn.s t6, t5, t4
	-[0x80002c78]:csrrs a2, fcsr, zero
	-[0x80002c7c]:sd t6, 1344(fp)
Current Store : [0x80002c80] : sd a2, 1352(fp) -- Store: [0x8000c4f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:fsgnjn.s t6, t5, t4
	-[0x80002cb8]:csrrs a2, fcsr, zero
	-[0x80002cbc]:sd t6, 1360(fp)
Current Store : [0x80002cc0] : sd a2, 1368(fp) -- Store: [0x8000c500]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002cf4]:fsgnjn.s t6, t5, t4
	-[0x80002cf8]:csrrs a2, fcsr, zero
	-[0x80002cfc]:sd t6, 1376(fp)
Current Store : [0x80002d00] : sd a2, 1384(fp) -- Store: [0x8000c510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002d34]:fsgnjn.s t6, t5, t4
	-[0x80002d38]:csrrs a2, fcsr, zero
	-[0x80002d3c]:sd t6, 1392(fp)
Current Store : [0x80002d40] : sd a2, 1400(fp) -- Store: [0x8000c520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002d74]:fsgnjn.s t6, t5, t4
	-[0x80002d78]:csrrs a2, fcsr, zero
	-[0x80002d7c]:sd t6, 1408(fp)
Current Store : [0x80002d80] : sd a2, 1416(fp) -- Store: [0x8000c530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002db4]:fsgnjn.s t6, t5, t4
	-[0x80002db8]:csrrs a2, fcsr, zero
	-[0x80002dbc]:sd t6, 1424(fp)
Current Store : [0x80002dc0] : sd a2, 1432(fp) -- Store: [0x8000c540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002df4]:fsgnjn.s t6, t5, t4
	-[0x80002df8]:csrrs a2, fcsr, zero
	-[0x80002dfc]:sd t6, 1440(fp)
Current Store : [0x80002e00] : sd a2, 1448(fp) -- Store: [0x8000c550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002e34]:fsgnjn.s t6, t5, t4
	-[0x80002e38]:csrrs a2, fcsr, zero
	-[0x80002e3c]:sd t6, 1456(fp)
Current Store : [0x80002e40] : sd a2, 1464(fp) -- Store: [0x8000c560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002e74]:fsgnjn.s t6, t5, t4
	-[0x80002e78]:csrrs a2, fcsr, zero
	-[0x80002e7c]:sd t6, 1472(fp)
Current Store : [0x80002e80] : sd a2, 1480(fp) -- Store: [0x8000c570]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002eb4]:fsgnjn.s t6, t5, t4
	-[0x80002eb8]:csrrs a2, fcsr, zero
	-[0x80002ebc]:sd t6, 1488(fp)
Current Store : [0x80002ec0] : sd a2, 1496(fp) -- Store: [0x8000c580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002ef4]:fsgnjn.s t6, t5, t4
	-[0x80002ef8]:csrrs a2, fcsr, zero
	-[0x80002efc]:sd t6, 1504(fp)
Current Store : [0x80002f00] : sd a2, 1512(fp) -- Store: [0x8000c590]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002f34]:fsgnjn.s t6, t5, t4
	-[0x80002f38]:csrrs a2, fcsr, zero
	-[0x80002f3c]:sd t6, 1520(fp)
Current Store : [0x80002f40] : sd a2, 1528(fp) -- Store: [0x8000c5a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002f74]:fsgnjn.s t6, t5, t4
	-[0x80002f78]:csrrs a2, fcsr, zero
	-[0x80002f7c]:sd t6, 1536(fp)
Current Store : [0x80002f80] : sd a2, 1544(fp) -- Store: [0x8000c5b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002fb4]:fsgnjn.s t6, t5, t4
	-[0x80002fb8]:csrrs a2, fcsr, zero
	-[0x80002fbc]:sd t6, 1552(fp)
Current Store : [0x80002fc0] : sd a2, 1560(fp) -- Store: [0x8000c5c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002ff4]:fsgnjn.s t6, t5, t4
	-[0x80002ff8]:csrrs a2, fcsr, zero
	-[0x80002ffc]:sd t6, 1568(fp)
Current Store : [0x80003000] : sd a2, 1576(fp) -- Store: [0x8000c5d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003034]:fsgnjn.s t6, t5, t4
	-[0x80003038]:csrrs a2, fcsr, zero
	-[0x8000303c]:sd t6, 1584(fp)
Current Store : [0x80003040] : sd a2, 1592(fp) -- Store: [0x8000c5e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003074]:fsgnjn.s t6, t5, t4
	-[0x80003078]:csrrs a2, fcsr, zero
	-[0x8000307c]:sd t6, 1600(fp)
Current Store : [0x80003080] : sd a2, 1608(fp) -- Store: [0x8000c5f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800030b4]:fsgnjn.s t6, t5, t4
	-[0x800030b8]:csrrs a2, fcsr, zero
	-[0x800030bc]:sd t6, 1616(fp)
Current Store : [0x800030c0] : sd a2, 1624(fp) -- Store: [0x8000c600]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800030f4]:fsgnjn.s t6, t5, t4
	-[0x800030f8]:csrrs a2, fcsr, zero
	-[0x800030fc]:sd t6, 1632(fp)
Current Store : [0x80003100] : sd a2, 1640(fp) -- Store: [0x8000c610]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003134]:fsgnjn.s t6, t5, t4
	-[0x80003138]:csrrs a2, fcsr, zero
	-[0x8000313c]:sd t6, 1648(fp)
Current Store : [0x80003140] : sd a2, 1656(fp) -- Store: [0x8000c620]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003174]:fsgnjn.s t6, t5, t4
	-[0x80003178]:csrrs a2, fcsr, zero
	-[0x8000317c]:sd t6, 1664(fp)
Current Store : [0x80003180] : sd a2, 1672(fp) -- Store: [0x8000c630]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800031b4]:fsgnjn.s t6, t5, t4
	-[0x800031b8]:csrrs a2, fcsr, zero
	-[0x800031bc]:sd t6, 1680(fp)
Current Store : [0x800031c0] : sd a2, 1688(fp) -- Store: [0x8000c640]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800031f4]:fsgnjn.s t6, t5, t4
	-[0x800031f8]:csrrs a2, fcsr, zero
	-[0x800031fc]:sd t6, 1696(fp)
Current Store : [0x80003200] : sd a2, 1704(fp) -- Store: [0x8000c650]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003234]:fsgnjn.s t6, t5, t4
	-[0x80003238]:csrrs a2, fcsr, zero
	-[0x8000323c]:sd t6, 1712(fp)
Current Store : [0x80003240] : sd a2, 1720(fp) -- Store: [0x8000c660]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003274]:fsgnjn.s t6, t5, t4
	-[0x80003278]:csrrs a2, fcsr, zero
	-[0x8000327c]:sd t6, 1728(fp)
Current Store : [0x80003280] : sd a2, 1736(fp) -- Store: [0x8000c670]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800032b4]:fsgnjn.s t6, t5, t4
	-[0x800032b8]:csrrs a2, fcsr, zero
	-[0x800032bc]:sd t6, 1744(fp)
Current Store : [0x800032c0] : sd a2, 1752(fp) -- Store: [0x8000c680]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800032f4]:fsgnjn.s t6, t5, t4
	-[0x800032f8]:csrrs a2, fcsr, zero
	-[0x800032fc]:sd t6, 1760(fp)
Current Store : [0x80003300] : sd a2, 1768(fp) -- Store: [0x8000c690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003334]:fsgnjn.s t6, t5, t4
	-[0x80003338]:csrrs a2, fcsr, zero
	-[0x8000333c]:sd t6, 1776(fp)
Current Store : [0x80003340] : sd a2, 1784(fp) -- Store: [0x8000c6a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003374]:fsgnjn.s t6, t5, t4
	-[0x80003378]:csrrs a2, fcsr, zero
	-[0x8000337c]:sd t6, 1792(fp)
Current Store : [0x80003380] : sd a2, 1800(fp) -- Store: [0x8000c6b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800033b4]:fsgnjn.s t6, t5, t4
	-[0x800033b8]:csrrs a2, fcsr, zero
	-[0x800033bc]:sd t6, 1808(fp)
Current Store : [0x800033c0] : sd a2, 1816(fp) -- Store: [0x8000c6c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800033f4]:fsgnjn.s t6, t5, t4
	-[0x800033f8]:csrrs a2, fcsr, zero
	-[0x800033fc]:sd t6, 1824(fp)
Current Store : [0x80003400] : sd a2, 1832(fp) -- Store: [0x8000c6d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003434]:fsgnjn.s t6, t5, t4
	-[0x80003438]:csrrs a2, fcsr, zero
	-[0x8000343c]:sd t6, 1840(fp)
Current Store : [0x80003440] : sd a2, 1848(fp) -- Store: [0x8000c6e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003474]:fsgnjn.s t6, t5, t4
	-[0x80003478]:csrrs a2, fcsr, zero
	-[0x8000347c]:sd t6, 1856(fp)
Current Store : [0x80003480] : sd a2, 1864(fp) -- Store: [0x8000c6f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800034b4]:fsgnjn.s t6, t5, t4
	-[0x800034b8]:csrrs a2, fcsr, zero
	-[0x800034bc]:sd t6, 1872(fp)
Current Store : [0x800034c0] : sd a2, 1880(fp) -- Store: [0x8000c700]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800034f4]:fsgnjn.s t6, t5, t4
	-[0x800034f8]:csrrs a2, fcsr, zero
	-[0x800034fc]:sd t6, 1888(fp)
Current Store : [0x80003500] : sd a2, 1896(fp) -- Store: [0x8000c710]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003534]:fsgnjn.s t6, t5, t4
	-[0x80003538]:csrrs a2, fcsr, zero
	-[0x8000353c]:sd t6, 1904(fp)
Current Store : [0x80003540] : sd a2, 1912(fp) -- Store: [0x8000c720]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003574]:fsgnjn.s t6, t5, t4
	-[0x80003578]:csrrs a2, fcsr, zero
	-[0x8000357c]:sd t6, 1920(fp)
Current Store : [0x80003580] : sd a2, 1928(fp) -- Store: [0x8000c730]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800035b4]:fsgnjn.s t6, t5, t4
	-[0x800035b8]:csrrs a2, fcsr, zero
	-[0x800035bc]:sd t6, 1936(fp)
Current Store : [0x800035c0] : sd a2, 1944(fp) -- Store: [0x8000c740]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800035f4]:fsgnjn.s t6, t5, t4
	-[0x800035f8]:csrrs a2, fcsr, zero
	-[0x800035fc]:sd t6, 1952(fp)
Current Store : [0x80003600] : sd a2, 1960(fp) -- Store: [0x8000c750]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003634]:fsgnjn.s t6, t5, t4
	-[0x80003638]:csrrs a2, fcsr, zero
	-[0x8000363c]:sd t6, 1968(fp)
Current Store : [0x80003640] : sd a2, 1976(fp) -- Store: [0x8000c760]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003674]:fsgnjn.s t6, t5, t4
	-[0x80003678]:csrrs a2, fcsr, zero
	-[0x8000367c]:sd t6, 1984(fp)
Current Store : [0x80003680] : sd a2, 1992(fp) -- Store: [0x8000c770]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800036ac]:fsgnjn.s t6, t5, t4
	-[0x800036b0]:csrrs a2, fcsr, zero
	-[0x800036b4]:sd t6, 2000(fp)
Current Store : [0x800036b8] : sd a2, 2008(fp) -- Store: [0x8000c780]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800036e4]:fsgnjn.s t6, t5, t4
	-[0x800036e8]:csrrs a2, fcsr, zero
	-[0x800036ec]:sd t6, 2016(fp)
Current Store : [0x800036f0] : sd a2, 2024(fp) -- Store: [0x8000c790]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000371c]:fsgnjn.s t6, t5, t4
	-[0x80003720]:csrrs a2, fcsr, zero
	-[0x80003724]:sd t6, 2032(fp)
Current Store : [0x80003728] : sd a2, 2040(fp) -- Store: [0x8000c7a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000375c]:fsgnjn.s t6, t5, t4
	-[0x80003760]:csrrs a2, fcsr, zero
	-[0x80003764]:sd t6, 0(fp)
Current Store : [0x80003768] : sd a2, 8(fp) -- Store: [0x8000c7b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003794]:fsgnjn.s t6, t5, t4
	-[0x80003798]:csrrs a2, fcsr, zero
	-[0x8000379c]:sd t6, 16(fp)
Current Store : [0x800037a0] : sd a2, 24(fp) -- Store: [0x8000c7c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800037cc]:fsgnjn.s t6, t5, t4
	-[0x800037d0]:csrrs a2, fcsr, zero
	-[0x800037d4]:sd t6, 32(fp)
Current Store : [0x800037d8] : sd a2, 40(fp) -- Store: [0x8000c7d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003804]:fsgnjn.s t6, t5, t4
	-[0x80003808]:csrrs a2, fcsr, zero
	-[0x8000380c]:sd t6, 48(fp)
Current Store : [0x80003810] : sd a2, 56(fp) -- Store: [0x8000c7e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000383c]:fsgnjn.s t6, t5, t4
	-[0x80003840]:csrrs a2, fcsr, zero
	-[0x80003844]:sd t6, 64(fp)
Current Store : [0x80003848] : sd a2, 72(fp) -- Store: [0x8000c7f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003874]:fsgnjn.s t6, t5, t4
	-[0x80003878]:csrrs a2, fcsr, zero
	-[0x8000387c]:sd t6, 80(fp)
Current Store : [0x80003880] : sd a2, 88(fp) -- Store: [0x8000c800]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800038ac]:fsgnjn.s t6, t5, t4
	-[0x800038b0]:csrrs a2, fcsr, zero
	-[0x800038b4]:sd t6, 96(fp)
Current Store : [0x800038b8] : sd a2, 104(fp) -- Store: [0x8000c810]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800038e4]:fsgnjn.s t6, t5, t4
	-[0x800038e8]:csrrs a2, fcsr, zero
	-[0x800038ec]:sd t6, 112(fp)
Current Store : [0x800038f0] : sd a2, 120(fp) -- Store: [0x8000c820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000391c]:fsgnjn.s t6, t5, t4
	-[0x80003920]:csrrs a2, fcsr, zero
	-[0x80003924]:sd t6, 128(fp)
Current Store : [0x80003928] : sd a2, 136(fp) -- Store: [0x8000c830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003954]:fsgnjn.s t6, t5, t4
	-[0x80003958]:csrrs a2, fcsr, zero
	-[0x8000395c]:sd t6, 144(fp)
Current Store : [0x80003960] : sd a2, 152(fp) -- Store: [0x8000c840]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000398c]:fsgnjn.s t6, t5, t4
	-[0x80003990]:csrrs a2, fcsr, zero
	-[0x80003994]:sd t6, 160(fp)
Current Store : [0x80003998] : sd a2, 168(fp) -- Store: [0x8000c850]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800039c4]:fsgnjn.s t6, t5, t4
	-[0x800039c8]:csrrs a2, fcsr, zero
	-[0x800039cc]:sd t6, 176(fp)
Current Store : [0x800039d0] : sd a2, 184(fp) -- Store: [0x8000c860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800039fc]:fsgnjn.s t6, t5, t4
	-[0x80003a00]:csrrs a2, fcsr, zero
	-[0x80003a04]:sd t6, 192(fp)
Current Store : [0x80003a08] : sd a2, 200(fp) -- Store: [0x8000c870]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003a34]:fsgnjn.s t6, t5, t4
	-[0x80003a38]:csrrs a2, fcsr, zero
	-[0x80003a3c]:sd t6, 208(fp)
Current Store : [0x80003a40] : sd a2, 216(fp) -- Store: [0x8000c880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003a6c]:fsgnjn.s t6, t5, t4
	-[0x80003a70]:csrrs a2, fcsr, zero
	-[0x80003a74]:sd t6, 224(fp)
Current Store : [0x80003a78] : sd a2, 232(fp) -- Store: [0x8000c890]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003aa4]:fsgnjn.s t6, t5, t4
	-[0x80003aa8]:csrrs a2, fcsr, zero
	-[0x80003aac]:sd t6, 240(fp)
Current Store : [0x80003ab0] : sd a2, 248(fp) -- Store: [0x8000c8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003adc]:fsgnjn.s t6, t5, t4
	-[0x80003ae0]:csrrs a2, fcsr, zero
	-[0x80003ae4]:sd t6, 256(fp)
Current Store : [0x80003ae8] : sd a2, 264(fp) -- Store: [0x8000c8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b14]:fsgnjn.s t6, t5, t4
	-[0x80003b18]:csrrs a2, fcsr, zero
	-[0x80003b1c]:sd t6, 272(fp)
Current Store : [0x80003b20] : sd a2, 280(fp) -- Store: [0x8000c8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003b4c]:fsgnjn.s t6, t5, t4
	-[0x80003b50]:csrrs a2, fcsr, zero
	-[0x80003b54]:sd t6, 288(fp)
Current Store : [0x80003b58] : sd a2, 296(fp) -- Store: [0x8000c8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b84]:fsgnjn.s t6, t5, t4
	-[0x80003b88]:csrrs a2, fcsr, zero
	-[0x80003b8c]:sd t6, 304(fp)
Current Store : [0x80003b90] : sd a2, 312(fp) -- Store: [0x8000c8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003bbc]:fsgnjn.s t6, t5, t4
	-[0x80003bc0]:csrrs a2, fcsr, zero
	-[0x80003bc4]:sd t6, 320(fp)
Current Store : [0x80003bc8] : sd a2, 328(fp) -- Store: [0x8000c8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003bf4]:fsgnjn.s t6, t5, t4
	-[0x80003bf8]:csrrs a2, fcsr, zero
	-[0x80003bfc]:sd t6, 336(fp)
Current Store : [0x80003c00] : sd a2, 344(fp) -- Store: [0x8000c900]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c2c]:fsgnjn.s t6, t5, t4
	-[0x80003c30]:csrrs a2, fcsr, zero
	-[0x80003c34]:sd t6, 352(fp)
Current Store : [0x80003c38] : sd a2, 360(fp) -- Store: [0x8000c910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003c64]:fsgnjn.s t6, t5, t4
	-[0x80003c68]:csrrs a2, fcsr, zero
	-[0x80003c6c]:sd t6, 368(fp)
Current Store : [0x80003c70] : sd a2, 376(fp) -- Store: [0x8000c920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c9c]:fsgnjn.s t6, t5, t4
	-[0x80003ca0]:csrrs a2, fcsr, zero
	-[0x80003ca4]:sd t6, 384(fp)
Current Store : [0x80003ca8] : sd a2, 392(fp) -- Store: [0x8000c930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003cd4]:fsgnjn.s t6, t5, t4
	-[0x80003cd8]:csrrs a2, fcsr, zero
	-[0x80003cdc]:sd t6, 400(fp)
Current Store : [0x80003ce0] : sd a2, 408(fp) -- Store: [0x8000c940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d0c]:fsgnjn.s t6, t5, t4
	-[0x80003d10]:csrrs a2, fcsr, zero
	-[0x80003d14]:sd t6, 416(fp)
Current Store : [0x80003d18] : sd a2, 424(fp) -- Store: [0x8000c950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003d44]:fsgnjn.s t6, t5, t4
	-[0x80003d48]:csrrs a2, fcsr, zero
	-[0x80003d4c]:sd t6, 432(fp)
Current Store : [0x80003d50] : sd a2, 440(fp) -- Store: [0x8000c960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d7c]:fsgnjn.s t6, t5, t4
	-[0x80003d80]:csrrs a2, fcsr, zero
	-[0x80003d84]:sd t6, 448(fp)
Current Store : [0x80003d88] : sd a2, 456(fp) -- Store: [0x8000c970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003db4]:fsgnjn.s t6, t5, t4
	-[0x80003db8]:csrrs a2, fcsr, zero
	-[0x80003dbc]:sd t6, 464(fp)
Current Store : [0x80003dc0] : sd a2, 472(fp) -- Store: [0x8000c980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003dec]:fsgnjn.s t6, t5, t4
	-[0x80003df0]:csrrs a2, fcsr, zero
	-[0x80003df4]:sd t6, 480(fp)
Current Store : [0x80003df8] : sd a2, 488(fp) -- Store: [0x8000c990]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003e24]:fsgnjn.s t6, t5, t4
	-[0x80003e28]:csrrs a2, fcsr, zero
	-[0x80003e2c]:sd t6, 496(fp)
Current Store : [0x80003e30] : sd a2, 504(fp) -- Store: [0x8000c9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003e5c]:fsgnjn.s t6, t5, t4
	-[0x80003e60]:csrrs a2, fcsr, zero
	-[0x80003e64]:sd t6, 512(fp)
Current Store : [0x80003e68] : sd a2, 520(fp) -- Store: [0x8000c9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003e94]:fsgnjn.s t6, t5, t4
	-[0x80003e98]:csrrs a2, fcsr, zero
	-[0x80003e9c]:sd t6, 528(fp)
Current Store : [0x80003ea0] : sd a2, 536(fp) -- Store: [0x8000c9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003ecc]:fsgnjn.s t6, t5, t4
	-[0x80003ed0]:csrrs a2, fcsr, zero
	-[0x80003ed4]:sd t6, 544(fp)
Current Store : [0x80003ed8] : sd a2, 552(fp) -- Store: [0x8000c9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003f04]:fsgnjn.s t6, t5, t4
	-[0x80003f08]:csrrs a2, fcsr, zero
	-[0x80003f0c]:sd t6, 560(fp)
Current Store : [0x80003f10] : sd a2, 568(fp) -- Store: [0x8000c9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003f3c]:fsgnjn.s t6, t5, t4
	-[0x80003f40]:csrrs a2, fcsr, zero
	-[0x80003f44]:sd t6, 576(fp)
Current Store : [0x80003f48] : sd a2, 584(fp) -- Store: [0x8000c9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003f74]:fsgnjn.s t6, t5, t4
	-[0x80003f78]:csrrs a2, fcsr, zero
	-[0x80003f7c]:sd t6, 592(fp)
Current Store : [0x80003f80] : sd a2, 600(fp) -- Store: [0x8000ca00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003fac]:fsgnjn.s t6, t5, t4
	-[0x80003fb0]:csrrs a2, fcsr, zero
	-[0x80003fb4]:sd t6, 608(fp)
Current Store : [0x80003fb8] : sd a2, 616(fp) -- Store: [0x8000ca10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003fe4]:fsgnjn.s t6, t5, t4
	-[0x80003fe8]:csrrs a2, fcsr, zero
	-[0x80003fec]:sd t6, 624(fp)
Current Store : [0x80003ff0] : sd a2, 632(fp) -- Store: [0x8000ca20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000401c]:fsgnjn.s t6, t5, t4
	-[0x80004020]:csrrs a2, fcsr, zero
	-[0x80004024]:sd t6, 640(fp)
Current Store : [0x80004028] : sd a2, 648(fp) -- Store: [0x8000ca30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004054]:fsgnjn.s t6, t5, t4
	-[0x80004058]:csrrs a2, fcsr, zero
	-[0x8000405c]:sd t6, 656(fp)
Current Store : [0x80004060] : sd a2, 664(fp) -- Store: [0x8000ca40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000408c]:fsgnjn.s t6, t5, t4
	-[0x80004090]:csrrs a2, fcsr, zero
	-[0x80004094]:sd t6, 672(fp)
Current Store : [0x80004098] : sd a2, 680(fp) -- Store: [0x8000ca50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800040c4]:fsgnjn.s t6, t5, t4
	-[0x800040c8]:csrrs a2, fcsr, zero
	-[0x800040cc]:sd t6, 688(fp)
Current Store : [0x800040d0] : sd a2, 696(fp) -- Store: [0x8000ca60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800040fc]:fsgnjn.s t6, t5, t4
	-[0x80004100]:csrrs a2, fcsr, zero
	-[0x80004104]:sd t6, 704(fp)
Current Store : [0x80004108] : sd a2, 712(fp) -- Store: [0x8000ca70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004134]:fsgnjn.s t6, t5, t4
	-[0x80004138]:csrrs a2, fcsr, zero
	-[0x8000413c]:sd t6, 720(fp)
Current Store : [0x80004140] : sd a2, 728(fp) -- Store: [0x8000ca80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000416c]:fsgnjn.s t6, t5, t4
	-[0x80004170]:csrrs a2, fcsr, zero
	-[0x80004174]:sd t6, 736(fp)
Current Store : [0x80004178] : sd a2, 744(fp) -- Store: [0x8000ca90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800041a4]:fsgnjn.s t6, t5, t4
	-[0x800041a8]:csrrs a2, fcsr, zero
	-[0x800041ac]:sd t6, 752(fp)
Current Store : [0x800041b0] : sd a2, 760(fp) -- Store: [0x8000caa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fsgnjn.s t6, t5, t4
	-[0x800041e0]:csrrs a2, fcsr, zero
	-[0x800041e4]:sd t6, 768(fp)
Current Store : [0x800041e8] : sd a2, 776(fp) -- Store: [0x8000cab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004214]:fsgnjn.s t6, t5, t4
	-[0x80004218]:csrrs a2, fcsr, zero
	-[0x8000421c]:sd t6, 784(fp)
Current Store : [0x80004220] : sd a2, 792(fp) -- Store: [0x8000cac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000424c]:fsgnjn.s t6, t5, t4
	-[0x80004250]:csrrs a2, fcsr, zero
	-[0x80004254]:sd t6, 800(fp)
Current Store : [0x80004258] : sd a2, 808(fp) -- Store: [0x8000cad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004284]:fsgnjn.s t6, t5, t4
	-[0x80004288]:csrrs a2, fcsr, zero
	-[0x8000428c]:sd t6, 816(fp)
Current Store : [0x80004290] : sd a2, 824(fp) -- Store: [0x8000cae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800042bc]:fsgnjn.s t6, t5, t4
	-[0x800042c0]:csrrs a2, fcsr, zero
	-[0x800042c4]:sd t6, 832(fp)
Current Store : [0x800042c8] : sd a2, 840(fp) -- Store: [0x8000caf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800042f4]:fsgnjn.s t6, t5, t4
	-[0x800042f8]:csrrs a2, fcsr, zero
	-[0x800042fc]:sd t6, 848(fp)
Current Store : [0x80004300] : sd a2, 856(fp) -- Store: [0x8000cb00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000432c]:fsgnjn.s t6, t5, t4
	-[0x80004330]:csrrs a2, fcsr, zero
	-[0x80004334]:sd t6, 864(fp)
Current Store : [0x80004338] : sd a2, 872(fp) -- Store: [0x8000cb10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004364]:fsgnjn.s t6, t5, t4
	-[0x80004368]:csrrs a2, fcsr, zero
	-[0x8000436c]:sd t6, 880(fp)
Current Store : [0x80004370] : sd a2, 888(fp) -- Store: [0x8000cb20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000439c]:fsgnjn.s t6, t5, t4
	-[0x800043a0]:csrrs a2, fcsr, zero
	-[0x800043a4]:sd t6, 896(fp)
Current Store : [0x800043a8] : sd a2, 904(fp) -- Store: [0x8000cb30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800043d4]:fsgnjn.s t6, t5, t4
	-[0x800043d8]:csrrs a2, fcsr, zero
	-[0x800043dc]:sd t6, 912(fp)
Current Store : [0x800043e0] : sd a2, 920(fp) -- Store: [0x8000cb40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000440c]:fsgnjn.s t6, t5, t4
	-[0x80004410]:csrrs a2, fcsr, zero
	-[0x80004414]:sd t6, 928(fp)
Current Store : [0x80004418] : sd a2, 936(fp) -- Store: [0x8000cb50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004444]:fsgnjn.s t6, t5, t4
	-[0x80004448]:csrrs a2, fcsr, zero
	-[0x8000444c]:sd t6, 944(fp)
Current Store : [0x80004450] : sd a2, 952(fp) -- Store: [0x8000cb60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000447c]:fsgnjn.s t6, t5, t4
	-[0x80004480]:csrrs a2, fcsr, zero
	-[0x80004484]:sd t6, 960(fp)
Current Store : [0x80004488] : sd a2, 968(fp) -- Store: [0x8000cb70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800044b4]:fsgnjn.s t6, t5, t4
	-[0x800044b8]:csrrs a2, fcsr, zero
	-[0x800044bc]:sd t6, 976(fp)
Current Store : [0x800044c0] : sd a2, 984(fp) -- Store: [0x8000cb80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800044ec]:fsgnjn.s t6, t5, t4
	-[0x800044f0]:csrrs a2, fcsr, zero
	-[0x800044f4]:sd t6, 992(fp)
Current Store : [0x800044f8] : sd a2, 1000(fp) -- Store: [0x8000cb90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004524]:fsgnjn.s t6, t5, t4
	-[0x80004528]:csrrs a2, fcsr, zero
	-[0x8000452c]:sd t6, 1008(fp)
Current Store : [0x80004530] : sd a2, 1016(fp) -- Store: [0x8000cba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000455c]:fsgnjn.s t6, t5, t4
	-[0x80004560]:csrrs a2, fcsr, zero
	-[0x80004564]:sd t6, 1024(fp)
Current Store : [0x80004568] : sd a2, 1032(fp) -- Store: [0x8000cbb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004594]:fsgnjn.s t6, t5, t4
	-[0x80004598]:csrrs a2, fcsr, zero
	-[0x8000459c]:sd t6, 1040(fp)
Current Store : [0x800045a0] : sd a2, 1048(fp) -- Store: [0x8000cbc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800045cc]:fsgnjn.s t6, t5, t4
	-[0x800045d0]:csrrs a2, fcsr, zero
	-[0x800045d4]:sd t6, 1056(fp)
Current Store : [0x800045d8] : sd a2, 1064(fp) -- Store: [0x8000cbd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004604]:fsgnjn.s t6, t5, t4
	-[0x80004608]:csrrs a2, fcsr, zero
	-[0x8000460c]:sd t6, 1072(fp)
Current Store : [0x80004610] : sd a2, 1080(fp) -- Store: [0x8000cbe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000463c]:fsgnjn.s t6, t5, t4
	-[0x80004640]:csrrs a2, fcsr, zero
	-[0x80004644]:sd t6, 1088(fp)
Current Store : [0x80004648] : sd a2, 1096(fp) -- Store: [0x8000cbf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004674]:fsgnjn.s t6, t5, t4
	-[0x80004678]:csrrs a2, fcsr, zero
	-[0x8000467c]:sd t6, 1104(fp)
Current Store : [0x80004680] : sd a2, 1112(fp) -- Store: [0x8000cc00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800046ac]:fsgnjn.s t6, t5, t4
	-[0x800046b0]:csrrs a2, fcsr, zero
	-[0x800046b4]:sd t6, 1120(fp)
Current Store : [0x800046b8] : sd a2, 1128(fp) -- Store: [0x8000cc10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800046e4]:fsgnjn.s t6, t5, t4
	-[0x800046e8]:csrrs a2, fcsr, zero
	-[0x800046ec]:sd t6, 1136(fp)
Current Store : [0x800046f0] : sd a2, 1144(fp) -- Store: [0x8000cc20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000471c]:fsgnjn.s t6, t5, t4
	-[0x80004720]:csrrs a2, fcsr, zero
	-[0x80004724]:sd t6, 1152(fp)
Current Store : [0x80004728] : sd a2, 1160(fp) -- Store: [0x8000cc30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004754]:fsgnjn.s t6, t5, t4
	-[0x80004758]:csrrs a2, fcsr, zero
	-[0x8000475c]:sd t6, 1168(fp)
Current Store : [0x80004760] : sd a2, 1176(fp) -- Store: [0x8000cc40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000478c]:fsgnjn.s t6, t5, t4
	-[0x80004790]:csrrs a2, fcsr, zero
	-[0x80004794]:sd t6, 1184(fp)
Current Store : [0x80004798] : sd a2, 1192(fp) -- Store: [0x8000cc50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800047c4]:fsgnjn.s t6, t5, t4
	-[0x800047c8]:csrrs a2, fcsr, zero
	-[0x800047cc]:sd t6, 1200(fp)
Current Store : [0x800047d0] : sd a2, 1208(fp) -- Store: [0x8000cc60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800047fc]:fsgnjn.s t6, t5, t4
	-[0x80004800]:csrrs a2, fcsr, zero
	-[0x80004804]:sd t6, 1216(fp)
Current Store : [0x80004808] : sd a2, 1224(fp) -- Store: [0x8000cc70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004834]:fsgnjn.s t6, t5, t4
	-[0x80004838]:csrrs a2, fcsr, zero
	-[0x8000483c]:sd t6, 1232(fp)
Current Store : [0x80004840] : sd a2, 1240(fp) -- Store: [0x8000cc80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000486c]:fsgnjn.s t6, t5, t4
	-[0x80004870]:csrrs a2, fcsr, zero
	-[0x80004874]:sd t6, 1248(fp)
Current Store : [0x80004878] : sd a2, 1256(fp) -- Store: [0x8000cc90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800048a4]:fsgnjn.s t6, t5, t4
	-[0x800048a8]:csrrs a2, fcsr, zero
	-[0x800048ac]:sd t6, 1264(fp)
Current Store : [0x800048b0] : sd a2, 1272(fp) -- Store: [0x8000cca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800048dc]:fsgnjn.s t6, t5, t4
	-[0x800048e0]:csrrs a2, fcsr, zero
	-[0x800048e4]:sd t6, 1280(fp)
Current Store : [0x800048e8] : sd a2, 1288(fp) -- Store: [0x8000ccb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004914]:fsgnjn.s t6, t5, t4
	-[0x80004918]:csrrs a2, fcsr, zero
	-[0x8000491c]:sd t6, 1296(fp)
Current Store : [0x80004920] : sd a2, 1304(fp) -- Store: [0x8000ccc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000494c]:fsgnjn.s t6, t5, t4
	-[0x80004950]:csrrs a2, fcsr, zero
	-[0x80004954]:sd t6, 1312(fp)
Current Store : [0x80004958] : sd a2, 1320(fp) -- Store: [0x8000ccd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004984]:fsgnjn.s t6, t5, t4
	-[0x80004988]:csrrs a2, fcsr, zero
	-[0x8000498c]:sd t6, 1328(fp)
Current Store : [0x80004990] : sd a2, 1336(fp) -- Store: [0x8000cce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800049bc]:fsgnjn.s t6, t5, t4
	-[0x800049c0]:csrrs a2, fcsr, zero
	-[0x800049c4]:sd t6, 1344(fp)
Current Store : [0x800049c8] : sd a2, 1352(fp) -- Store: [0x8000ccf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800049f4]:fsgnjn.s t6, t5, t4
	-[0x800049f8]:csrrs a2, fcsr, zero
	-[0x800049fc]:sd t6, 1360(fp)
Current Store : [0x80004a00] : sd a2, 1368(fp) -- Store: [0x8000cd00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a2c]:fsgnjn.s t6, t5, t4
	-[0x80004a30]:csrrs a2, fcsr, zero
	-[0x80004a34]:sd t6, 1376(fp)
Current Store : [0x80004a38] : sd a2, 1384(fp) -- Store: [0x8000cd10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004a64]:fsgnjn.s t6, t5, t4
	-[0x80004a68]:csrrs a2, fcsr, zero
	-[0x80004a6c]:sd t6, 1392(fp)
Current Store : [0x80004a70] : sd a2, 1400(fp) -- Store: [0x8000cd20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a9c]:fsgnjn.s t6, t5, t4
	-[0x80004aa0]:csrrs a2, fcsr, zero
	-[0x80004aa4]:sd t6, 1408(fp)
Current Store : [0x80004aa8] : sd a2, 1416(fp) -- Store: [0x8000cd30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004ad4]:fsgnjn.s t6, t5, t4
	-[0x80004ad8]:csrrs a2, fcsr, zero
	-[0x80004adc]:sd t6, 1424(fp)
Current Store : [0x80004ae0] : sd a2, 1432(fp) -- Store: [0x8000cd40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b0c]:fsgnjn.s t6, t5, t4
	-[0x80004b10]:csrrs a2, fcsr, zero
	-[0x80004b14]:sd t6, 1440(fp)
Current Store : [0x80004b18] : sd a2, 1448(fp) -- Store: [0x8000cd50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004b44]:fsgnjn.s t6, t5, t4
	-[0x80004b48]:csrrs a2, fcsr, zero
	-[0x80004b4c]:sd t6, 1456(fp)
Current Store : [0x80004b50] : sd a2, 1464(fp) -- Store: [0x8000cd60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b7c]:fsgnjn.s t6, t5, t4
	-[0x80004b80]:csrrs a2, fcsr, zero
	-[0x80004b84]:sd t6, 1472(fp)
Current Store : [0x80004b88] : sd a2, 1480(fp) -- Store: [0x8000cd70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004bb4]:fsgnjn.s t6, t5, t4
	-[0x80004bb8]:csrrs a2, fcsr, zero
	-[0x80004bbc]:sd t6, 1488(fp)
Current Store : [0x80004bc0] : sd a2, 1496(fp) -- Store: [0x8000cd80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004bec]:fsgnjn.s t6, t5, t4
	-[0x80004bf0]:csrrs a2, fcsr, zero
	-[0x80004bf4]:sd t6, 1504(fp)
Current Store : [0x80004bf8] : sd a2, 1512(fp) -- Store: [0x8000cd90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004c24]:fsgnjn.s t6, t5, t4
	-[0x80004c28]:csrrs a2, fcsr, zero
	-[0x80004c2c]:sd t6, 1520(fp)
Current Store : [0x80004c30] : sd a2, 1528(fp) -- Store: [0x8000cda0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c5c]:fsgnjn.s t6, t5, t4
	-[0x80004c60]:csrrs a2, fcsr, zero
	-[0x80004c64]:sd t6, 1536(fp)
Current Store : [0x80004c68] : sd a2, 1544(fp) -- Store: [0x8000cdb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004c94]:fsgnjn.s t6, t5, t4
	-[0x80004c98]:csrrs a2, fcsr, zero
	-[0x80004c9c]:sd t6, 1552(fp)
Current Store : [0x80004ca0] : sd a2, 1560(fp) -- Store: [0x8000cdc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ccc]:fsgnjn.s t6, t5, t4
	-[0x80004cd0]:csrrs a2, fcsr, zero
	-[0x80004cd4]:sd t6, 1568(fp)
Current Store : [0x80004cd8] : sd a2, 1576(fp) -- Store: [0x8000cdd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004d04]:fsgnjn.s t6, t5, t4
	-[0x80004d08]:csrrs a2, fcsr, zero
	-[0x80004d0c]:sd t6, 1584(fp)
Current Store : [0x80004d10] : sd a2, 1592(fp) -- Store: [0x8000cde0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004d3c]:fsgnjn.s t6, t5, t4
	-[0x80004d40]:csrrs a2, fcsr, zero
	-[0x80004d44]:sd t6, 1600(fp)
Current Store : [0x80004d48] : sd a2, 1608(fp) -- Store: [0x8000cdf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004d74]:fsgnjn.s t6, t5, t4
	-[0x80004d78]:csrrs a2, fcsr, zero
	-[0x80004d7c]:sd t6, 1616(fp)
Current Store : [0x80004d80] : sd a2, 1624(fp) -- Store: [0x8000ce00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004dac]:fsgnjn.s t6, t5, t4
	-[0x80004db0]:csrrs a2, fcsr, zero
	-[0x80004db4]:sd t6, 1632(fp)
Current Store : [0x80004db8] : sd a2, 1640(fp) -- Store: [0x8000ce10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004de4]:fsgnjn.s t6, t5, t4
	-[0x80004de8]:csrrs a2, fcsr, zero
	-[0x80004dec]:sd t6, 1648(fp)
Current Store : [0x80004df0] : sd a2, 1656(fp) -- Store: [0x8000ce20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004e1c]:fsgnjn.s t6, t5, t4
	-[0x80004e20]:csrrs a2, fcsr, zero
	-[0x80004e24]:sd t6, 1664(fp)
Current Store : [0x80004e28] : sd a2, 1672(fp) -- Store: [0x8000ce30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004e54]:fsgnjn.s t6, t5, t4
	-[0x80004e58]:csrrs a2, fcsr, zero
	-[0x80004e5c]:sd t6, 1680(fp)
Current Store : [0x80004e60] : sd a2, 1688(fp) -- Store: [0x8000ce40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004e8c]:fsgnjn.s t6, t5, t4
	-[0x80004e90]:csrrs a2, fcsr, zero
	-[0x80004e94]:sd t6, 1696(fp)
Current Store : [0x80004e98] : sd a2, 1704(fp) -- Store: [0x8000ce50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004ec4]:fsgnjn.s t6, t5, t4
	-[0x80004ec8]:csrrs a2, fcsr, zero
	-[0x80004ecc]:sd t6, 1712(fp)
Current Store : [0x80004ed0] : sd a2, 1720(fp) -- Store: [0x8000ce60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004efc]:fsgnjn.s t6, t5, t4
	-[0x80004f00]:csrrs a2, fcsr, zero
	-[0x80004f04]:sd t6, 1728(fp)
Current Store : [0x80004f08] : sd a2, 1736(fp) -- Store: [0x8000ce70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004f34]:fsgnjn.s t6, t5, t4
	-[0x80004f38]:csrrs a2, fcsr, zero
	-[0x80004f3c]:sd t6, 1744(fp)
Current Store : [0x80004f40] : sd a2, 1752(fp) -- Store: [0x8000ce80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fsgnjn.s t6, t5, t4
	-[0x80004f70]:csrrs a2, fcsr, zero
	-[0x80004f74]:sd t6, 1760(fp)
Current Store : [0x80004f78] : sd a2, 1768(fp) -- Store: [0x8000ce90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004fa4]:fsgnjn.s t6, t5, t4
	-[0x80004fa8]:csrrs a2, fcsr, zero
	-[0x80004fac]:sd t6, 1776(fp)
Current Store : [0x80004fb0] : sd a2, 1784(fp) -- Store: [0x8000cea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004fdc]:fsgnjn.s t6, t5, t4
	-[0x80004fe0]:csrrs a2, fcsr, zero
	-[0x80004fe4]:sd t6, 1792(fp)
Current Store : [0x80004fe8] : sd a2, 1800(fp) -- Store: [0x8000ceb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005014]:fsgnjn.s t6, t5, t4
	-[0x80005018]:csrrs a2, fcsr, zero
	-[0x8000501c]:sd t6, 1808(fp)
Current Store : [0x80005020] : sd a2, 1816(fp) -- Store: [0x8000cec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000504c]:fsgnjn.s t6, t5, t4
	-[0x80005050]:csrrs a2, fcsr, zero
	-[0x80005054]:sd t6, 1824(fp)
Current Store : [0x80005058] : sd a2, 1832(fp) -- Store: [0x8000ced0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005084]:fsgnjn.s t6, t5, t4
	-[0x80005088]:csrrs a2, fcsr, zero
	-[0x8000508c]:sd t6, 1840(fp)
Current Store : [0x80005090] : sd a2, 1848(fp) -- Store: [0x8000cee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800050bc]:fsgnjn.s t6, t5, t4
	-[0x800050c0]:csrrs a2, fcsr, zero
	-[0x800050c4]:sd t6, 1856(fp)
Current Store : [0x800050c8] : sd a2, 1864(fp) -- Store: [0x8000cef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800050f4]:fsgnjn.s t6, t5, t4
	-[0x800050f8]:csrrs a2, fcsr, zero
	-[0x800050fc]:sd t6, 1872(fp)
Current Store : [0x80005100] : sd a2, 1880(fp) -- Store: [0x8000cf00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000512c]:fsgnjn.s t6, t5, t4
	-[0x80005130]:csrrs a2, fcsr, zero
	-[0x80005134]:sd t6, 1888(fp)
Current Store : [0x80005138] : sd a2, 1896(fp) -- Store: [0x8000cf10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005164]:fsgnjn.s t6, t5, t4
	-[0x80005168]:csrrs a2, fcsr, zero
	-[0x8000516c]:sd t6, 1904(fp)
Current Store : [0x80005170] : sd a2, 1912(fp) -- Store: [0x8000cf20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000519c]:fsgnjn.s t6, t5, t4
	-[0x800051a0]:csrrs a2, fcsr, zero
	-[0x800051a4]:sd t6, 1920(fp)
Current Store : [0x800051a8] : sd a2, 1928(fp) -- Store: [0x8000cf30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800051d4]:fsgnjn.s t6, t5, t4
	-[0x800051d8]:csrrs a2, fcsr, zero
	-[0x800051dc]:sd t6, 1936(fp)
Current Store : [0x800051e0] : sd a2, 1944(fp) -- Store: [0x8000cf40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000520c]:fsgnjn.s t6, t5, t4
	-[0x80005210]:csrrs a2, fcsr, zero
	-[0x80005214]:sd t6, 1952(fp)
Current Store : [0x80005218] : sd a2, 1960(fp) -- Store: [0x8000cf50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005244]:fsgnjn.s t6, t5, t4
	-[0x80005248]:csrrs a2, fcsr, zero
	-[0x8000524c]:sd t6, 1968(fp)
Current Store : [0x80005250] : sd a2, 1976(fp) -- Store: [0x8000cf60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000527c]:fsgnjn.s t6, t5, t4
	-[0x80005280]:csrrs a2, fcsr, zero
	-[0x80005284]:sd t6, 1984(fp)
Current Store : [0x80005288] : sd a2, 1992(fp) -- Store: [0x8000cf70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800052bc]:fsgnjn.s t6, t5, t4
	-[0x800052c0]:csrrs a2, fcsr, zero
	-[0x800052c4]:sd t6, 2000(fp)
Current Store : [0x800052c8] : sd a2, 2008(fp) -- Store: [0x8000cf80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800052fc]:fsgnjn.s t6, t5, t4
	-[0x80005300]:csrrs a2, fcsr, zero
	-[0x80005304]:sd t6, 2016(fp)
Current Store : [0x80005308] : sd a2, 2024(fp) -- Store: [0x8000cf90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000533c]:fsgnjn.s t6, t5, t4
	-[0x80005340]:csrrs a2, fcsr, zero
	-[0x80005344]:sd t6, 2032(fp)
Current Store : [0x80005348] : sd a2, 2040(fp) -- Store: [0x8000cfa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005384]:fsgnjn.s t6, t5, t4
	-[0x80005388]:csrrs a2, fcsr, zero
	-[0x8000538c]:sd t6, 0(fp)
Current Store : [0x80005390] : sd a2, 8(fp) -- Store: [0x8000cfb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800053c4]:fsgnjn.s t6, t5, t4
	-[0x800053c8]:csrrs a2, fcsr, zero
	-[0x800053cc]:sd t6, 16(fp)
Current Store : [0x800053d0] : sd a2, 24(fp) -- Store: [0x8000cfc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005404]:fsgnjn.s t6, t5, t4
	-[0x80005408]:csrrs a2, fcsr, zero
	-[0x8000540c]:sd t6, 32(fp)
Current Store : [0x80005410] : sd a2, 40(fp) -- Store: [0x8000cfd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005444]:fsgnjn.s t6, t5, t4
	-[0x80005448]:csrrs a2, fcsr, zero
	-[0x8000544c]:sd t6, 48(fp)
Current Store : [0x80005450] : sd a2, 56(fp) -- Store: [0x8000cfe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005484]:fsgnjn.s t6, t5, t4
	-[0x80005488]:csrrs a2, fcsr, zero
	-[0x8000548c]:sd t6, 64(fp)
Current Store : [0x80005490] : sd a2, 72(fp) -- Store: [0x8000cff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800054c4]:fsgnjn.s t6, t5, t4
	-[0x800054c8]:csrrs a2, fcsr, zero
	-[0x800054cc]:sd t6, 80(fp)
Current Store : [0x800054d0] : sd a2, 88(fp) -- Store: [0x8000d000]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005504]:fsgnjn.s t6, t5, t4
	-[0x80005508]:csrrs a2, fcsr, zero
	-[0x8000550c]:sd t6, 96(fp)
Current Store : [0x80005510] : sd a2, 104(fp) -- Store: [0x8000d010]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005544]:fsgnjn.s t6, t5, t4
	-[0x80005548]:csrrs a2, fcsr, zero
	-[0x8000554c]:sd t6, 112(fp)
Current Store : [0x80005550] : sd a2, 120(fp) -- Store: [0x8000d020]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005584]:fsgnjn.s t6, t5, t4
	-[0x80005588]:csrrs a2, fcsr, zero
	-[0x8000558c]:sd t6, 128(fp)
Current Store : [0x80005590] : sd a2, 136(fp) -- Store: [0x8000d030]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800055c4]:fsgnjn.s t6, t5, t4
	-[0x800055c8]:csrrs a2, fcsr, zero
	-[0x800055cc]:sd t6, 144(fp)
Current Store : [0x800055d0] : sd a2, 152(fp) -- Store: [0x8000d040]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005604]:fsgnjn.s t6, t5, t4
	-[0x80005608]:csrrs a2, fcsr, zero
	-[0x8000560c]:sd t6, 160(fp)
Current Store : [0x80005610] : sd a2, 168(fp) -- Store: [0x8000d050]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005644]:fsgnjn.s t6, t5, t4
	-[0x80005648]:csrrs a2, fcsr, zero
	-[0x8000564c]:sd t6, 176(fp)
Current Store : [0x80005650] : sd a2, 184(fp) -- Store: [0x8000d060]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005684]:fsgnjn.s t6, t5, t4
	-[0x80005688]:csrrs a2, fcsr, zero
	-[0x8000568c]:sd t6, 192(fp)
Current Store : [0x80005690] : sd a2, 200(fp) -- Store: [0x8000d070]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800056c4]:fsgnjn.s t6, t5, t4
	-[0x800056c8]:csrrs a2, fcsr, zero
	-[0x800056cc]:sd t6, 208(fp)
Current Store : [0x800056d0] : sd a2, 216(fp) -- Store: [0x8000d080]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005704]:fsgnjn.s t6, t5, t4
	-[0x80005708]:csrrs a2, fcsr, zero
	-[0x8000570c]:sd t6, 224(fp)
Current Store : [0x80005710] : sd a2, 232(fp) -- Store: [0x8000d090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005744]:fsgnjn.s t6, t5, t4
	-[0x80005748]:csrrs a2, fcsr, zero
	-[0x8000574c]:sd t6, 240(fp)
Current Store : [0x80005750] : sd a2, 248(fp) -- Store: [0x8000d0a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005784]:fsgnjn.s t6, t5, t4
	-[0x80005788]:csrrs a2, fcsr, zero
	-[0x8000578c]:sd t6, 256(fp)
Current Store : [0x80005790] : sd a2, 264(fp) -- Store: [0x8000d0b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800057c4]:fsgnjn.s t6, t5, t4
	-[0x800057c8]:csrrs a2, fcsr, zero
	-[0x800057cc]:sd t6, 272(fp)
Current Store : [0x800057d0] : sd a2, 280(fp) -- Store: [0x8000d0c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005804]:fsgnjn.s t6, t5, t4
	-[0x80005808]:csrrs a2, fcsr, zero
	-[0x8000580c]:sd t6, 288(fp)
Current Store : [0x80005810] : sd a2, 296(fp) -- Store: [0x8000d0d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005844]:fsgnjn.s t6, t5, t4
	-[0x80005848]:csrrs a2, fcsr, zero
	-[0x8000584c]:sd t6, 304(fp)
Current Store : [0x80005850] : sd a2, 312(fp) -- Store: [0x8000d0e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005884]:fsgnjn.s t6, t5, t4
	-[0x80005888]:csrrs a2, fcsr, zero
	-[0x8000588c]:sd t6, 320(fp)
Current Store : [0x80005890] : sd a2, 328(fp) -- Store: [0x8000d0f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800058c4]:fsgnjn.s t6, t5, t4
	-[0x800058c8]:csrrs a2, fcsr, zero
	-[0x800058cc]:sd t6, 336(fp)
Current Store : [0x800058d0] : sd a2, 344(fp) -- Store: [0x8000d100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005904]:fsgnjn.s t6, t5, t4
	-[0x80005908]:csrrs a2, fcsr, zero
	-[0x8000590c]:sd t6, 352(fp)
Current Store : [0x80005910] : sd a2, 360(fp) -- Store: [0x8000d110]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005944]:fsgnjn.s t6, t5, t4
	-[0x80005948]:csrrs a2, fcsr, zero
	-[0x8000594c]:sd t6, 368(fp)
Current Store : [0x80005950] : sd a2, 376(fp) -- Store: [0x8000d120]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005984]:fsgnjn.s t6, t5, t4
	-[0x80005988]:csrrs a2, fcsr, zero
	-[0x8000598c]:sd t6, 384(fp)
Current Store : [0x80005990] : sd a2, 392(fp) -- Store: [0x8000d130]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800059c4]:fsgnjn.s t6, t5, t4
	-[0x800059c8]:csrrs a2, fcsr, zero
	-[0x800059cc]:sd t6, 400(fp)
Current Store : [0x800059d0] : sd a2, 408(fp) -- Store: [0x8000d140]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a04]:fsgnjn.s t6, t5, t4
	-[0x80005a08]:csrrs a2, fcsr, zero
	-[0x80005a0c]:sd t6, 416(fp)
Current Store : [0x80005a10] : sd a2, 424(fp) -- Store: [0x8000d150]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005a44]:fsgnjn.s t6, t5, t4
	-[0x80005a48]:csrrs a2, fcsr, zero
	-[0x80005a4c]:sd t6, 432(fp)
Current Store : [0x80005a50] : sd a2, 440(fp) -- Store: [0x8000d160]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a84]:fsgnjn.s t6, t5, t4
	-[0x80005a88]:csrrs a2, fcsr, zero
	-[0x80005a8c]:sd t6, 448(fp)
Current Store : [0x80005a90] : sd a2, 456(fp) -- Store: [0x8000d170]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005ac4]:fsgnjn.s t6, t5, t4
	-[0x80005ac8]:csrrs a2, fcsr, zero
	-[0x80005acc]:sd t6, 464(fp)
Current Store : [0x80005ad0] : sd a2, 472(fp) -- Store: [0x8000d180]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005b04]:fsgnjn.s t6, t5, t4
	-[0x80005b08]:csrrs a2, fcsr, zero
	-[0x80005b0c]:sd t6, 480(fp)
Current Store : [0x80005b10] : sd a2, 488(fp) -- Store: [0x8000d190]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005b44]:fsgnjn.s t6, t5, t4
	-[0x80005b48]:csrrs a2, fcsr, zero
	-[0x80005b4c]:sd t6, 496(fp)
Current Store : [0x80005b50] : sd a2, 504(fp) -- Store: [0x8000d1a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005b84]:fsgnjn.s t6, t5, t4
	-[0x80005b88]:csrrs a2, fcsr, zero
	-[0x80005b8c]:sd t6, 512(fp)
Current Store : [0x80005b90] : sd a2, 520(fp) -- Store: [0x8000d1b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005bc4]:fsgnjn.s t6, t5, t4
	-[0x80005bc8]:csrrs a2, fcsr, zero
	-[0x80005bcc]:sd t6, 528(fp)
Current Store : [0x80005bd0] : sd a2, 536(fp) -- Store: [0x8000d1c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005c04]:fsgnjn.s t6, t5, t4
	-[0x80005c08]:csrrs a2, fcsr, zero
	-[0x80005c0c]:sd t6, 544(fp)
Current Store : [0x80005c10] : sd a2, 552(fp) -- Store: [0x8000d1d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005c44]:fsgnjn.s t6, t5, t4
	-[0x80005c48]:csrrs a2, fcsr, zero
	-[0x80005c4c]:sd t6, 560(fp)
Current Store : [0x80005c50] : sd a2, 568(fp) -- Store: [0x8000d1e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005c84]:fsgnjn.s t6, t5, t4
	-[0x80005c88]:csrrs a2, fcsr, zero
	-[0x80005c8c]:sd t6, 576(fp)
Current Store : [0x80005c90] : sd a2, 584(fp) -- Store: [0x8000d1f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005cc4]:fsgnjn.s t6, t5, t4
	-[0x80005cc8]:csrrs a2, fcsr, zero
	-[0x80005ccc]:sd t6, 592(fp)
Current Store : [0x80005cd0] : sd a2, 600(fp) -- Store: [0x8000d200]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005d04]:fsgnjn.s t6, t5, t4
	-[0x80005d08]:csrrs a2, fcsr, zero
	-[0x80005d0c]:sd t6, 608(fp)
Current Store : [0x80005d10] : sd a2, 616(fp) -- Store: [0x8000d210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005d44]:fsgnjn.s t6, t5, t4
	-[0x80005d48]:csrrs a2, fcsr, zero
	-[0x80005d4c]:sd t6, 624(fp)
Current Store : [0x80005d50] : sd a2, 632(fp) -- Store: [0x8000d220]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005d84]:fsgnjn.s t6, t5, t4
	-[0x80005d88]:csrrs a2, fcsr, zero
	-[0x80005d8c]:sd t6, 640(fp)
Current Store : [0x80005d90] : sd a2, 648(fp) -- Store: [0x8000d230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005dc4]:fsgnjn.s t6, t5, t4
	-[0x80005dc8]:csrrs a2, fcsr, zero
	-[0x80005dcc]:sd t6, 656(fp)
Current Store : [0x80005dd0] : sd a2, 664(fp) -- Store: [0x8000d240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005e04]:fsgnjn.s t6, t5, t4
	-[0x80005e08]:csrrs a2, fcsr, zero
	-[0x80005e0c]:sd t6, 672(fp)
Current Store : [0x80005e10] : sd a2, 680(fp) -- Store: [0x8000d250]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005e44]:fsgnjn.s t6, t5, t4
	-[0x80005e48]:csrrs a2, fcsr, zero
	-[0x80005e4c]:sd t6, 688(fp)
Current Store : [0x80005e50] : sd a2, 696(fp) -- Store: [0x8000d260]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005e84]:fsgnjn.s t6, t5, t4
	-[0x80005e88]:csrrs a2, fcsr, zero
	-[0x80005e8c]:sd t6, 704(fp)
Current Store : [0x80005e90] : sd a2, 712(fp) -- Store: [0x8000d270]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005ec4]:fsgnjn.s t6, t5, t4
	-[0x80005ec8]:csrrs a2, fcsr, zero
	-[0x80005ecc]:sd t6, 720(fp)
Current Store : [0x80005ed0] : sd a2, 728(fp) -- Store: [0x8000d280]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f04]:fsgnjn.s t6, t5, t4
	-[0x80005f08]:csrrs a2, fcsr, zero
	-[0x80005f0c]:sd t6, 736(fp)
Current Store : [0x80005f10] : sd a2, 744(fp) -- Store: [0x8000d290]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005f44]:fsgnjn.s t6, t5, t4
	-[0x80005f48]:csrrs a2, fcsr, zero
	-[0x80005f4c]:sd t6, 752(fp)
Current Store : [0x80005f50] : sd a2, 760(fp) -- Store: [0x8000d2a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f84]:fsgnjn.s t6, t5, t4
	-[0x80005f88]:csrrs a2, fcsr, zero
	-[0x80005f8c]:sd t6, 768(fp)
Current Store : [0x80005f90] : sd a2, 776(fp) -- Store: [0x8000d2b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005fc4]:fsgnjn.s t6, t5, t4
	-[0x80005fc8]:csrrs a2, fcsr, zero
	-[0x80005fcc]:sd t6, 784(fp)
Current Store : [0x80005fd0] : sd a2, 792(fp) -- Store: [0x8000d2c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006004]:fsgnjn.s t6, t5, t4
	-[0x80006008]:csrrs a2, fcsr, zero
	-[0x8000600c]:sd t6, 800(fp)
Current Store : [0x80006010] : sd a2, 808(fp) -- Store: [0x8000d2d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006044]:fsgnjn.s t6, t5, t4
	-[0x80006048]:csrrs a2, fcsr, zero
	-[0x8000604c]:sd t6, 816(fp)
Current Store : [0x80006050] : sd a2, 824(fp) -- Store: [0x8000d2e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006084]:fsgnjn.s t6, t5, t4
	-[0x80006088]:csrrs a2, fcsr, zero
	-[0x8000608c]:sd t6, 832(fp)
Current Store : [0x80006090] : sd a2, 840(fp) -- Store: [0x8000d2f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800060c4]:fsgnjn.s t6, t5, t4
	-[0x800060c8]:csrrs a2, fcsr, zero
	-[0x800060cc]:sd t6, 848(fp)
Current Store : [0x800060d0] : sd a2, 856(fp) -- Store: [0x8000d300]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006104]:fsgnjn.s t6, t5, t4
	-[0x80006108]:csrrs a2, fcsr, zero
	-[0x8000610c]:sd t6, 864(fp)
Current Store : [0x80006110] : sd a2, 872(fp) -- Store: [0x8000d310]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006144]:fsgnjn.s t6, t5, t4
	-[0x80006148]:csrrs a2, fcsr, zero
	-[0x8000614c]:sd t6, 880(fp)
Current Store : [0x80006150] : sd a2, 888(fp) -- Store: [0x8000d320]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006184]:fsgnjn.s t6, t5, t4
	-[0x80006188]:csrrs a2, fcsr, zero
	-[0x8000618c]:sd t6, 896(fp)
Current Store : [0x80006190] : sd a2, 904(fp) -- Store: [0x8000d330]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800061c4]:fsgnjn.s t6, t5, t4
	-[0x800061c8]:csrrs a2, fcsr, zero
	-[0x800061cc]:sd t6, 912(fp)
Current Store : [0x800061d0] : sd a2, 920(fp) -- Store: [0x8000d340]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006204]:fsgnjn.s t6, t5, t4
	-[0x80006208]:csrrs a2, fcsr, zero
	-[0x8000620c]:sd t6, 928(fp)
Current Store : [0x80006210] : sd a2, 936(fp) -- Store: [0x8000d350]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006244]:fsgnjn.s t6, t5, t4
	-[0x80006248]:csrrs a2, fcsr, zero
	-[0x8000624c]:sd t6, 944(fp)
Current Store : [0x80006250] : sd a2, 952(fp) -- Store: [0x8000d360]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006284]:fsgnjn.s t6, t5, t4
	-[0x80006288]:csrrs a2, fcsr, zero
	-[0x8000628c]:sd t6, 960(fp)
Current Store : [0x80006290] : sd a2, 968(fp) -- Store: [0x8000d370]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800062c4]:fsgnjn.s t6, t5, t4
	-[0x800062c8]:csrrs a2, fcsr, zero
	-[0x800062cc]:sd t6, 976(fp)
Current Store : [0x800062d0] : sd a2, 984(fp) -- Store: [0x8000d380]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006304]:fsgnjn.s t6, t5, t4
	-[0x80006308]:csrrs a2, fcsr, zero
	-[0x8000630c]:sd t6, 992(fp)
Current Store : [0x80006310] : sd a2, 1000(fp) -- Store: [0x8000d390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006344]:fsgnjn.s t6, t5, t4
	-[0x80006348]:csrrs a2, fcsr, zero
	-[0x8000634c]:sd t6, 1008(fp)
Current Store : [0x80006350] : sd a2, 1016(fp) -- Store: [0x8000d3a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006384]:fsgnjn.s t6, t5, t4
	-[0x80006388]:csrrs a2, fcsr, zero
	-[0x8000638c]:sd t6, 1024(fp)
Current Store : [0x80006390] : sd a2, 1032(fp) -- Store: [0x8000d3b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800063c4]:fsgnjn.s t6, t5, t4
	-[0x800063c8]:csrrs a2, fcsr, zero
	-[0x800063cc]:sd t6, 1040(fp)
Current Store : [0x800063d0] : sd a2, 1048(fp) -- Store: [0x8000d3c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006404]:fsgnjn.s t6, t5, t4
	-[0x80006408]:csrrs a2, fcsr, zero
	-[0x8000640c]:sd t6, 1056(fp)
Current Store : [0x80006410] : sd a2, 1064(fp) -- Store: [0x8000d3d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006444]:fsgnjn.s t6, t5, t4
	-[0x80006448]:csrrs a2, fcsr, zero
	-[0x8000644c]:sd t6, 1072(fp)
Current Store : [0x80006450] : sd a2, 1080(fp) -- Store: [0x8000d3e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006484]:fsgnjn.s t6, t5, t4
	-[0x80006488]:csrrs a2, fcsr, zero
	-[0x8000648c]:sd t6, 1088(fp)
Current Store : [0x80006490] : sd a2, 1096(fp) -- Store: [0x8000d3f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800064c4]:fsgnjn.s t6, t5, t4
	-[0x800064c8]:csrrs a2, fcsr, zero
	-[0x800064cc]:sd t6, 1104(fp)
Current Store : [0x800064d0] : sd a2, 1112(fp) -- Store: [0x8000d400]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006504]:fsgnjn.s t6, t5, t4
	-[0x80006508]:csrrs a2, fcsr, zero
	-[0x8000650c]:sd t6, 1120(fp)
Current Store : [0x80006510] : sd a2, 1128(fp) -- Store: [0x8000d410]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006544]:fsgnjn.s t6, t5, t4
	-[0x80006548]:csrrs a2, fcsr, zero
	-[0x8000654c]:sd t6, 1136(fp)
Current Store : [0x80006550] : sd a2, 1144(fp) -- Store: [0x8000d420]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006584]:fsgnjn.s t6, t5, t4
	-[0x80006588]:csrrs a2, fcsr, zero
	-[0x8000658c]:sd t6, 1152(fp)
Current Store : [0x80006590] : sd a2, 1160(fp) -- Store: [0x8000d430]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800065c4]:fsgnjn.s t6, t5, t4
	-[0x800065c8]:csrrs a2, fcsr, zero
	-[0x800065cc]:sd t6, 1168(fp)
Current Store : [0x800065d0] : sd a2, 1176(fp) -- Store: [0x8000d440]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006604]:fsgnjn.s t6, t5, t4
	-[0x80006608]:csrrs a2, fcsr, zero
	-[0x8000660c]:sd t6, 1184(fp)
Current Store : [0x80006610] : sd a2, 1192(fp) -- Store: [0x8000d450]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006644]:fsgnjn.s t6, t5, t4
	-[0x80006648]:csrrs a2, fcsr, zero
	-[0x8000664c]:sd t6, 1200(fp)
Current Store : [0x80006650] : sd a2, 1208(fp) -- Store: [0x8000d460]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006684]:fsgnjn.s t6, t5, t4
	-[0x80006688]:csrrs a2, fcsr, zero
	-[0x8000668c]:sd t6, 1216(fp)
Current Store : [0x80006690] : sd a2, 1224(fp) -- Store: [0x8000d470]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800066c4]:fsgnjn.s t6, t5, t4
	-[0x800066c8]:csrrs a2, fcsr, zero
	-[0x800066cc]:sd t6, 1232(fp)
Current Store : [0x800066d0] : sd a2, 1240(fp) -- Store: [0x8000d480]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006704]:fsgnjn.s t6, t5, t4
	-[0x80006708]:csrrs a2, fcsr, zero
	-[0x8000670c]:sd t6, 1248(fp)
Current Store : [0x80006710] : sd a2, 1256(fp) -- Store: [0x8000d490]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006744]:fsgnjn.s t6, t5, t4
	-[0x80006748]:csrrs a2, fcsr, zero
	-[0x8000674c]:sd t6, 1264(fp)
Current Store : [0x80006750] : sd a2, 1272(fp) -- Store: [0x8000d4a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006784]:fsgnjn.s t6, t5, t4
	-[0x80006788]:csrrs a2, fcsr, zero
	-[0x8000678c]:sd t6, 1280(fp)
Current Store : [0x80006790] : sd a2, 1288(fp) -- Store: [0x8000d4b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800067c4]:fsgnjn.s t6, t5, t4
	-[0x800067c8]:csrrs a2, fcsr, zero
	-[0x800067cc]:sd t6, 1296(fp)
Current Store : [0x800067d0] : sd a2, 1304(fp) -- Store: [0x8000d4c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006804]:fsgnjn.s t6, t5, t4
	-[0x80006808]:csrrs a2, fcsr, zero
	-[0x8000680c]:sd t6, 1312(fp)
Current Store : [0x80006810] : sd a2, 1320(fp) -- Store: [0x8000d4d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006844]:fsgnjn.s t6, t5, t4
	-[0x80006848]:csrrs a2, fcsr, zero
	-[0x8000684c]:sd t6, 1328(fp)
Current Store : [0x80006850] : sd a2, 1336(fp) -- Store: [0x8000d4e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006884]:fsgnjn.s t6, t5, t4
	-[0x80006888]:csrrs a2, fcsr, zero
	-[0x8000688c]:sd t6, 1344(fp)
Current Store : [0x80006890] : sd a2, 1352(fp) -- Store: [0x8000d4f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800068c4]:fsgnjn.s t6, t5, t4
	-[0x800068c8]:csrrs a2, fcsr, zero
	-[0x800068cc]:sd t6, 1360(fp)
Current Store : [0x800068d0] : sd a2, 1368(fp) -- Store: [0x8000d500]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006904]:fsgnjn.s t6, t5, t4
	-[0x80006908]:csrrs a2, fcsr, zero
	-[0x8000690c]:sd t6, 1376(fp)
Current Store : [0x80006910] : sd a2, 1384(fp) -- Store: [0x8000d510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006944]:fsgnjn.s t6, t5, t4
	-[0x80006948]:csrrs a2, fcsr, zero
	-[0x8000694c]:sd t6, 1392(fp)
Current Store : [0x80006950] : sd a2, 1400(fp) -- Store: [0x8000d520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006984]:fsgnjn.s t6, t5, t4
	-[0x80006988]:csrrs a2, fcsr, zero
	-[0x8000698c]:sd t6, 1408(fp)
Current Store : [0x80006990] : sd a2, 1416(fp) -- Store: [0x8000d530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800069c4]:fsgnjn.s t6, t5, t4
	-[0x800069c8]:csrrs a2, fcsr, zero
	-[0x800069cc]:sd t6, 1424(fp)
Current Store : [0x800069d0] : sd a2, 1432(fp) -- Store: [0x8000d540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006a04]:fsgnjn.s t6, t5, t4
	-[0x80006a08]:csrrs a2, fcsr, zero
	-[0x80006a0c]:sd t6, 1440(fp)
Current Store : [0x80006a10] : sd a2, 1448(fp) -- Store: [0x8000d550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006a44]:fsgnjn.s t6, t5, t4
	-[0x80006a48]:csrrs a2, fcsr, zero
	-[0x80006a4c]:sd t6, 1456(fp)
Current Store : [0x80006a50] : sd a2, 1464(fp) -- Store: [0x8000d560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006a84]:fsgnjn.s t6, t5, t4
	-[0x80006a88]:csrrs a2, fcsr, zero
	-[0x80006a8c]:sd t6, 1472(fp)
Current Store : [0x80006a90] : sd a2, 1480(fp) -- Store: [0x8000d570]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ac4]:fsgnjn.s t6, t5, t4
	-[0x80006ac8]:csrrs a2, fcsr, zero
	-[0x80006acc]:sd t6, 1488(fp)
Current Store : [0x80006ad0] : sd a2, 1496(fp) -- Store: [0x8000d580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006b04]:fsgnjn.s t6, t5, t4
	-[0x80006b08]:csrrs a2, fcsr, zero
	-[0x80006b0c]:sd t6, 1504(fp)
Current Store : [0x80006b10] : sd a2, 1512(fp) -- Store: [0x8000d590]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006b44]:fsgnjn.s t6, t5, t4
	-[0x80006b48]:csrrs a2, fcsr, zero
	-[0x80006b4c]:sd t6, 1520(fp)
Current Store : [0x80006b50] : sd a2, 1528(fp) -- Store: [0x8000d5a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006b84]:fsgnjn.s t6, t5, t4
	-[0x80006b88]:csrrs a2, fcsr, zero
	-[0x80006b8c]:sd t6, 1536(fp)
Current Store : [0x80006b90] : sd a2, 1544(fp) -- Store: [0x8000d5b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006bc4]:fsgnjn.s t6, t5, t4
	-[0x80006bc8]:csrrs a2, fcsr, zero
	-[0x80006bcc]:sd t6, 1552(fp)
Current Store : [0x80006bd0] : sd a2, 1560(fp) -- Store: [0x8000d5c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c04]:fsgnjn.s t6, t5, t4
	-[0x80006c08]:csrrs a2, fcsr, zero
	-[0x80006c0c]:sd t6, 1568(fp)
Current Store : [0x80006c10] : sd a2, 1576(fp) -- Store: [0x8000d5d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fsgnjn.s t6, t5, t4
	-[0x80006c48]:csrrs a2, fcsr, zero
	-[0x80006c4c]:sd t6, 1584(fp)
Current Store : [0x80006c50] : sd a2, 1592(fp) -- Store: [0x8000d5e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c84]:fsgnjn.s t6, t5, t4
	-[0x80006c88]:csrrs a2, fcsr, zero
	-[0x80006c8c]:sd t6, 1600(fp)
Current Store : [0x80006c90] : sd a2, 1608(fp) -- Store: [0x8000d5f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006cc4]:fsgnjn.s t6, t5, t4
	-[0x80006cc8]:csrrs a2, fcsr, zero
	-[0x80006ccc]:sd t6, 1616(fp)
Current Store : [0x80006cd0] : sd a2, 1624(fp) -- Store: [0x8000d600]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006d04]:fsgnjn.s t6, t5, t4
	-[0x80006d08]:csrrs a2, fcsr, zero
	-[0x80006d0c]:sd t6, 1632(fp)
Current Store : [0x80006d10] : sd a2, 1640(fp) -- Store: [0x8000d610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006d44]:fsgnjn.s t6, t5, t4
	-[0x80006d48]:csrrs a2, fcsr, zero
	-[0x80006d4c]:sd t6, 1648(fp)
Current Store : [0x80006d50] : sd a2, 1656(fp) -- Store: [0x8000d620]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006d84]:fsgnjn.s t6, t5, t4
	-[0x80006d88]:csrrs a2, fcsr, zero
	-[0x80006d8c]:sd t6, 1664(fp)
Current Store : [0x80006d90] : sd a2, 1672(fp) -- Store: [0x8000d630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006dc4]:fsgnjn.s t6, t5, t4
	-[0x80006dc8]:csrrs a2, fcsr, zero
	-[0x80006dcc]:sd t6, 1680(fp)
Current Store : [0x80006dd0] : sd a2, 1688(fp) -- Store: [0x8000d640]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006e04]:fsgnjn.s t6, t5, t4
	-[0x80006e08]:csrrs a2, fcsr, zero
	-[0x80006e0c]:sd t6, 1696(fp)
Current Store : [0x80006e10] : sd a2, 1704(fp) -- Store: [0x8000d650]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006e44]:fsgnjn.s t6, t5, t4
	-[0x80006e48]:csrrs a2, fcsr, zero
	-[0x80006e4c]:sd t6, 1712(fp)
Current Store : [0x80006e50] : sd a2, 1720(fp) -- Store: [0x8000d660]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006e84]:fsgnjn.s t6, t5, t4
	-[0x80006e88]:csrrs a2, fcsr, zero
	-[0x80006e8c]:sd t6, 1728(fp)
Current Store : [0x80006e90] : sd a2, 1736(fp) -- Store: [0x8000d670]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ec4]:fsgnjn.s t6, t5, t4
	-[0x80006ec8]:csrrs a2, fcsr, zero
	-[0x80006ecc]:sd t6, 1744(fp)
Current Store : [0x80006ed0] : sd a2, 1752(fp) -- Store: [0x8000d680]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006f04]:fsgnjn.s t6, t5, t4
	-[0x80006f08]:csrrs a2, fcsr, zero
	-[0x80006f0c]:sd t6, 1760(fp)
Current Store : [0x80006f10] : sd a2, 1768(fp) -- Store: [0x8000d690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006f44]:fsgnjn.s t6, t5, t4
	-[0x80006f48]:csrrs a2, fcsr, zero
	-[0x80006f4c]:sd t6, 1776(fp)
Current Store : [0x80006f50] : sd a2, 1784(fp) -- Store: [0x8000d6a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006f84]:fsgnjn.s t6, t5, t4
	-[0x80006f88]:csrrs a2, fcsr, zero
	-[0x80006f8c]:sd t6, 1792(fp)
Current Store : [0x80006f90] : sd a2, 1800(fp) -- Store: [0x8000d6b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006fc4]:fsgnjn.s t6, t5, t4
	-[0x80006fc8]:csrrs a2, fcsr, zero
	-[0x80006fcc]:sd t6, 1808(fp)
Current Store : [0x80006fd0] : sd a2, 1816(fp) -- Store: [0x8000d6c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007004]:fsgnjn.s t6, t5, t4
	-[0x80007008]:csrrs a2, fcsr, zero
	-[0x8000700c]:sd t6, 1824(fp)
Current Store : [0x80007010] : sd a2, 1832(fp) -- Store: [0x8000d6d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007044]:fsgnjn.s t6, t5, t4
	-[0x80007048]:csrrs a2, fcsr, zero
	-[0x8000704c]:sd t6, 1840(fp)
Current Store : [0x80007050] : sd a2, 1848(fp) -- Store: [0x8000d6e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007084]:fsgnjn.s t6, t5, t4
	-[0x80007088]:csrrs a2, fcsr, zero
	-[0x8000708c]:sd t6, 1856(fp)
Current Store : [0x80007090] : sd a2, 1864(fp) -- Store: [0x8000d6f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800070c4]:fsgnjn.s t6, t5, t4
	-[0x800070c8]:csrrs a2, fcsr, zero
	-[0x800070cc]:sd t6, 1872(fp)
Current Store : [0x800070d0] : sd a2, 1880(fp) -- Store: [0x8000d700]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007104]:fsgnjn.s t6, t5, t4
	-[0x80007108]:csrrs a2, fcsr, zero
	-[0x8000710c]:sd t6, 1888(fp)
Current Store : [0x80007110] : sd a2, 1896(fp) -- Store: [0x8000d710]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007144]:fsgnjn.s t6, t5, t4
	-[0x80007148]:csrrs a2, fcsr, zero
	-[0x8000714c]:sd t6, 1904(fp)
Current Store : [0x80007150] : sd a2, 1912(fp) -- Store: [0x8000d720]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007184]:fsgnjn.s t6, t5, t4
	-[0x80007188]:csrrs a2, fcsr, zero
	-[0x8000718c]:sd t6, 1920(fp)
Current Store : [0x80007190] : sd a2, 1928(fp) -- Store: [0x8000d730]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800071c4]:fsgnjn.s t6, t5, t4
	-[0x800071c8]:csrrs a2, fcsr, zero
	-[0x800071cc]:sd t6, 1936(fp)
Current Store : [0x800071d0] : sd a2, 1944(fp) -- Store: [0x8000d740]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007204]:fsgnjn.s t6, t5, t4
	-[0x80007208]:csrrs a2, fcsr, zero
	-[0x8000720c]:sd t6, 1952(fp)
Current Store : [0x80007210] : sd a2, 1960(fp) -- Store: [0x8000d750]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007244]:fsgnjn.s t6, t5, t4
	-[0x80007248]:csrrs a2, fcsr, zero
	-[0x8000724c]:sd t6, 1968(fp)
Current Store : [0x80007250] : sd a2, 1976(fp) -- Store: [0x8000d760]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007284]:fsgnjn.s t6, t5, t4
	-[0x80007288]:csrrs a2, fcsr, zero
	-[0x8000728c]:sd t6, 1984(fp)
Current Store : [0x80007290] : sd a2, 1992(fp) -- Store: [0x8000d770]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800072bc]:fsgnjn.s t6, t5, t4
	-[0x800072c0]:csrrs a2, fcsr, zero
	-[0x800072c4]:sd t6, 2000(fp)
Current Store : [0x800072c8] : sd a2, 2008(fp) -- Store: [0x8000d780]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800072f4]:fsgnjn.s t6, t5, t4
	-[0x800072f8]:csrrs a2, fcsr, zero
	-[0x800072fc]:sd t6, 2016(fp)
Current Store : [0x80007300] : sd a2, 2024(fp) -- Store: [0x8000d790]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000732c]:fsgnjn.s t6, t5, t4
	-[0x80007330]:csrrs a2, fcsr, zero
	-[0x80007334]:sd t6, 2032(fp)
Current Store : [0x80007338] : sd a2, 2040(fp) -- Store: [0x8000d7a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000736c]:fsgnjn.s t6, t5, t4
	-[0x80007370]:csrrs a2, fcsr, zero
	-[0x80007374]:sd t6, 0(fp)
Current Store : [0x80007378] : sd a2, 8(fp) -- Store: [0x8000d7b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800073a4]:fsgnjn.s t6, t5, t4
	-[0x800073a8]:csrrs a2, fcsr, zero
	-[0x800073ac]:sd t6, 16(fp)
Current Store : [0x800073b0] : sd a2, 24(fp) -- Store: [0x8000d7c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800073dc]:fsgnjn.s t6, t5, t4
	-[0x800073e0]:csrrs a2, fcsr, zero
	-[0x800073e4]:sd t6, 32(fp)
Current Store : [0x800073e8] : sd a2, 40(fp) -- Store: [0x8000d7d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007414]:fsgnjn.s t6, t5, t4
	-[0x80007418]:csrrs a2, fcsr, zero
	-[0x8000741c]:sd t6, 48(fp)
Current Store : [0x80007420] : sd a2, 56(fp) -- Store: [0x8000d7e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000744c]:fsgnjn.s t6, t5, t4
	-[0x80007450]:csrrs a2, fcsr, zero
	-[0x80007454]:sd t6, 64(fp)
Current Store : [0x80007458] : sd a2, 72(fp) -- Store: [0x8000d7f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007484]:fsgnjn.s t6, t5, t4
	-[0x80007488]:csrrs a2, fcsr, zero
	-[0x8000748c]:sd t6, 80(fp)
Current Store : [0x80007490] : sd a2, 88(fp) -- Store: [0x8000d800]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800074bc]:fsgnjn.s t6, t5, t4
	-[0x800074c0]:csrrs a2, fcsr, zero
	-[0x800074c4]:sd t6, 96(fp)
Current Store : [0x800074c8] : sd a2, 104(fp) -- Store: [0x8000d810]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800074f4]:fsgnjn.s t6, t5, t4
	-[0x800074f8]:csrrs a2, fcsr, zero
	-[0x800074fc]:sd t6, 112(fp)
Current Store : [0x80007500] : sd a2, 120(fp) -- Store: [0x8000d820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000752c]:fsgnjn.s t6, t5, t4
	-[0x80007530]:csrrs a2, fcsr, zero
	-[0x80007534]:sd t6, 128(fp)
Current Store : [0x80007538] : sd a2, 136(fp) -- Store: [0x8000d830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007564]:fsgnjn.s t6, t5, t4
	-[0x80007568]:csrrs a2, fcsr, zero
	-[0x8000756c]:sd t6, 144(fp)
Current Store : [0x80007570] : sd a2, 152(fp) -- Store: [0x8000d840]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000759c]:fsgnjn.s t6, t5, t4
	-[0x800075a0]:csrrs a2, fcsr, zero
	-[0x800075a4]:sd t6, 160(fp)
Current Store : [0x800075a8] : sd a2, 168(fp) -- Store: [0x8000d850]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800075d4]:fsgnjn.s t6, t5, t4
	-[0x800075d8]:csrrs a2, fcsr, zero
	-[0x800075dc]:sd t6, 176(fp)
Current Store : [0x800075e0] : sd a2, 184(fp) -- Store: [0x8000d860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000760c]:fsgnjn.s t6, t5, t4
	-[0x80007610]:csrrs a2, fcsr, zero
	-[0x80007614]:sd t6, 192(fp)
Current Store : [0x80007618] : sd a2, 200(fp) -- Store: [0x8000d870]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007644]:fsgnjn.s t6, t5, t4
	-[0x80007648]:csrrs a2, fcsr, zero
	-[0x8000764c]:sd t6, 208(fp)
Current Store : [0x80007650] : sd a2, 216(fp) -- Store: [0x8000d880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000767c]:fsgnjn.s t6, t5, t4
	-[0x80007680]:csrrs a2, fcsr, zero
	-[0x80007684]:sd t6, 224(fp)
Current Store : [0x80007688] : sd a2, 232(fp) -- Store: [0x8000d890]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800076b4]:fsgnjn.s t6, t5, t4
	-[0x800076b8]:csrrs a2, fcsr, zero
	-[0x800076bc]:sd t6, 240(fp)
Current Store : [0x800076c0] : sd a2, 248(fp) -- Store: [0x8000d8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800076ec]:fsgnjn.s t6, t5, t4
	-[0x800076f0]:csrrs a2, fcsr, zero
	-[0x800076f4]:sd t6, 256(fp)
Current Store : [0x800076f8] : sd a2, 264(fp) -- Store: [0x8000d8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007724]:fsgnjn.s t6, t5, t4
	-[0x80007728]:csrrs a2, fcsr, zero
	-[0x8000772c]:sd t6, 272(fp)
Current Store : [0x80007730] : sd a2, 280(fp) -- Store: [0x8000d8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000775c]:fsgnjn.s t6, t5, t4
	-[0x80007760]:csrrs a2, fcsr, zero
	-[0x80007764]:sd t6, 288(fp)
Current Store : [0x80007768] : sd a2, 296(fp) -- Store: [0x8000d8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007794]:fsgnjn.s t6, t5, t4
	-[0x80007798]:csrrs a2, fcsr, zero
	-[0x8000779c]:sd t6, 304(fp)
Current Store : [0x800077a0] : sd a2, 312(fp) -- Store: [0x8000d8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800077cc]:fsgnjn.s t6, t5, t4
	-[0x800077d0]:csrrs a2, fcsr, zero
	-[0x800077d4]:sd t6, 320(fp)
Current Store : [0x800077d8] : sd a2, 328(fp) -- Store: [0x8000d8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007804]:fsgnjn.s t6, t5, t4
	-[0x80007808]:csrrs a2, fcsr, zero
	-[0x8000780c]:sd t6, 336(fp)
Current Store : [0x80007810] : sd a2, 344(fp) -- Store: [0x8000d900]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000783c]:fsgnjn.s t6, t5, t4
	-[0x80007840]:csrrs a2, fcsr, zero
	-[0x80007844]:sd t6, 352(fp)
Current Store : [0x80007848] : sd a2, 360(fp) -- Store: [0x8000d910]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007874]:fsgnjn.s t6, t5, t4
	-[0x80007878]:csrrs a2, fcsr, zero
	-[0x8000787c]:sd t6, 368(fp)
Current Store : [0x80007880] : sd a2, 376(fp) -- Store: [0x8000d920]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800078ac]:fsgnjn.s t6, t5, t4
	-[0x800078b0]:csrrs a2, fcsr, zero
	-[0x800078b4]:sd t6, 384(fp)
Current Store : [0x800078b8] : sd a2, 392(fp) -- Store: [0x8000d930]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800078e4]:fsgnjn.s t6, t5, t4
	-[0x800078e8]:csrrs a2, fcsr, zero
	-[0x800078ec]:sd t6, 400(fp)
Current Store : [0x800078f0] : sd a2, 408(fp) -- Store: [0x8000d940]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000791c]:fsgnjn.s t6, t5, t4
	-[0x80007920]:csrrs a2, fcsr, zero
	-[0x80007924]:sd t6, 416(fp)
Current Store : [0x80007928] : sd a2, 424(fp) -- Store: [0x8000d950]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007954]:fsgnjn.s t6, t5, t4
	-[0x80007958]:csrrs a2, fcsr, zero
	-[0x8000795c]:sd t6, 432(fp)
Current Store : [0x80007960] : sd a2, 440(fp) -- Store: [0x8000d960]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000798c]:fsgnjn.s t6, t5, t4
	-[0x80007990]:csrrs a2, fcsr, zero
	-[0x80007994]:sd t6, 448(fp)
Current Store : [0x80007998] : sd a2, 456(fp) -- Store: [0x8000d970]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800079c4]:fsgnjn.s t6, t5, t4
	-[0x800079c8]:csrrs a2, fcsr, zero
	-[0x800079cc]:sd t6, 464(fp)
Current Store : [0x800079d0] : sd a2, 472(fp) -- Store: [0x8000d980]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800079fc]:fsgnjn.s t6, t5, t4
	-[0x80007a00]:csrrs a2, fcsr, zero
	-[0x80007a04]:sd t6, 480(fp)
Current Store : [0x80007a08] : sd a2, 488(fp) -- Store: [0x8000d990]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007a34]:fsgnjn.s t6, t5, t4
	-[0x80007a38]:csrrs a2, fcsr, zero
	-[0x80007a3c]:sd t6, 496(fp)
Current Store : [0x80007a40] : sd a2, 504(fp) -- Store: [0x8000d9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007a6c]:fsgnjn.s t6, t5, t4
	-[0x80007a70]:csrrs a2, fcsr, zero
	-[0x80007a74]:sd t6, 512(fp)
Current Store : [0x80007a78] : sd a2, 520(fp) -- Store: [0x8000d9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007aa4]:fsgnjn.s t6, t5, t4
	-[0x80007aa8]:csrrs a2, fcsr, zero
	-[0x80007aac]:sd t6, 528(fp)
Current Store : [0x80007ab0] : sd a2, 536(fp) -- Store: [0x8000d9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007adc]:fsgnjn.s t6, t5, t4
	-[0x80007ae0]:csrrs a2, fcsr, zero
	-[0x80007ae4]:sd t6, 544(fp)
Current Store : [0x80007ae8] : sd a2, 552(fp) -- Store: [0x8000d9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b14]:fsgnjn.s t6, t5, t4
	-[0x80007b18]:csrrs a2, fcsr, zero
	-[0x80007b1c]:sd t6, 560(fp)
Current Store : [0x80007b20] : sd a2, 568(fp) -- Store: [0x8000d9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007b4c]:fsgnjn.s t6, t5, t4
	-[0x80007b50]:csrrs a2, fcsr, zero
	-[0x80007b54]:sd t6, 576(fp)
Current Store : [0x80007b58] : sd a2, 584(fp) -- Store: [0x8000d9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b84]:fsgnjn.s t6, t5, t4
	-[0x80007b88]:csrrs a2, fcsr, zero
	-[0x80007b8c]:sd t6, 592(fp)
Current Store : [0x80007b90] : sd a2, 600(fp) -- Store: [0x8000da00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007bbc]:fsgnjn.s t6, t5, t4
	-[0x80007bc0]:csrrs a2, fcsr, zero
	-[0x80007bc4]:sd t6, 608(fp)
Current Store : [0x80007bc8] : sd a2, 616(fp) -- Store: [0x8000da10]:0x0000000000000000




Last Coverpoint : ['mnemonic : fsgnjn.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007bf4]:fsgnjn.s t6, t5, t4
	-[0x80007bf8]:csrrs a2, fcsr, zero
	-[0x80007bfc]:sd t6, 624(fp)
Current Store : [0x80007c00] : sd a2, 632(fp) -- Store: [0x8000da20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c2c]:fsgnjn.s t6, t5, t4
	-[0x80007c30]:csrrs a2, fcsr, zero
	-[0x80007c34]:sd t6, 640(fp)
Current Store : [0x80007c38] : sd a2, 648(fp) -- Store: [0x8000da30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c64]:fsgnjn.s t6, t5, t4
	-[0x80007c68]:csrrs a2, fcsr, zero
	-[0x80007c6c]:sd t6, 656(fp)
Current Store : [0x80007c70] : sd a2, 664(fp) -- Store: [0x8000da40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c9c]:fsgnjn.s t6, t5, t4
	-[0x80007ca0]:csrrs a2, fcsr, zero
	-[0x80007ca4]:sd t6, 672(fp)
Current Store : [0x80007ca8] : sd a2, 680(fp) -- Store: [0x8000da50]:0x0000000000000000




Last Coverpoint : ['mnemonic : fsgnjn.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007cd4]:fsgnjn.s t6, t5, t4
	-[0x80007cd8]:csrrs a2, fcsr, zero
	-[0x80007cdc]:sd t6, 688(fp)
Current Store : [0x80007ce0] : sd a2, 696(fp) -- Store: [0x8000da60]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                                                                                                         coverpoints                                                                                                                                         |                                                     code                                                      |
|---:|---------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
|   1|[0x8000b618]<br>0xFFFFFFFF80000000<br> |- mnemonic : fsgnjn.s<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br> |[0x800003bc]:fsgnjn.s t6, t5, t5<br> [0x800003c0]:csrrs tp, fcsr, zero<br> [0x800003c4]:sd t6, 0(ra)<br>       |
|   2|[0x8000b628]<br>0x0000000000000000<br> |- rs1 : x29<br> - rs2 : x31<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800003dc]:fsgnjn.s t4, t4, t6<br> [0x800003e0]:csrrs tp, fcsr, zero<br> [0x800003e4]:sd t4, 16(ra)<br>      |
|   3|[0x8000b638]<br>0xFFFFFFFF80000000<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                        |[0x800003fc]:fsgnjn.s t3, t3, t3<br> [0x80000400]:csrrs tp, fcsr, zero<br> [0x80000404]:sd t3, 32(ra)<br>      |
|   4|[0x8000b648]<br>0x0000000000000000<br> |- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>    |[0x8000041c]:fsgnjn.s t5, t6, t4<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:sd t5, 48(ra)<br>      |
|   5|[0x8000b658]<br>0xFFFFFFFF80000000<br> |- rs1 : x26<br> - rs2 : x27<br> - rd : x27<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                           |[0x8000043c]:fsgnjn.s s11, s10, s11<br> [0x80000440]:csrrs tp, fcsr, zero<br> [0x80000444]:sd s11, 64(ra)<br>  |
|   6|[0x8000b668]<br>0x0000000000000000<br> |- rs1 : x27<br> - rs2 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000045c]:fsgnjn.s s10, s11, s9<br> [0x80000460]:csrrs tp, fcsr, zero<br> [0x80000464]:sd s10, 80(ra)<br>   |
|   7|[0x8000b678]<br>0xFFFFFFFF80000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000047c]:fsgnjn.s s9, s8, s10<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s9, 96(ra)<br>     |
|   8|[0x8000b688]<br>0x0000000000000000<br> |- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000049c]:fsgnjn.s s8, s9, s7<br> [0x800004a0]:csrrs tp, fcsr, zero<br> [0x800004a4]:sd s8, 112(ra)<br>     |
|   9|[0x8000b698]<br>0xFFFFFFFF80000000<br> |- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800004bc]:fsgnjn.s s7, s6, s8<br> [0x800004c0]:csrrs tp, fcsr, zero<br> [0x800004c4]:sd s7, 128(ra)<br>     |
|  10|[0x8000b6a8]<br>0x0000000000000000<br> |- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x800004dc]:fsgnjn.s s6, s7, s5<br> [0x800004e0]:csrrs tp, fcsr, zero<br> [0x800004e4]:sd s6, 144(ra)<br>     |
|  11|[0x8000b6b8]<br>0xFFFFFFFF80000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800004fc]:fsgnjn.s s5, s4, s6<br> [0x80000500]:csrrs tp, fcsr, zero<br> [0x80000504]:sd s5, 160(ra)<br>     |
|  12|[0x8000b6c8]<br>0x0000000000000000<br> |- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000051c]:fsgnjn.s s4, s5, s3<br> [0x80000520]:csrrs tp, fcsr, zero<br> [0x80000524]:sd s4, 176(ra)<br>     |
|  13|[0x8000b6d8]<br>0xFFFFFFFF80000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000053c]:fsgnjn.s s3, s2, s4<br> [0x80000540]:csrrs tp, fcsr, zero<br> [0x80000544]:sd s3, 192(ra)<br>     |
|  14|[0x8000b6e8]<br>0x0000000000000000<br> |- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000055c]:fsgnjn.s s2, s3, a7<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd s2, 208(ra)<br>     |
|  15|[0x8000b6f8]<br>0xFFFFFFFF80000000<br> |- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000057c]:fsgnjn.s a7, a6, s2<br> [0x80000580]:csrrs tp, fcsr, zero<br> [0x80000584]:sd a7, 224(ra)<br>     |
|  16|[0x8000b708]<br>0x0000000000000000<br> |- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000059c]:fsgnjn.s a6, a7, a5<br> [0x800005a0]:csrrs tp, fcsr, zero<br> [0x800005a4]:sd a6, 240(ra)<br>     |
|  17|[0x8000b718]<br>0xFFFFFFFF80000000<br> |- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800005bc]:fsgnjn.s a5, a4, a6<br> [0x800005c0]:csrrs tp, fcsr, zero<br> [0x800005c4]:sd a5, 256(ra)<br>     |
|  18|[0x8000b728]<br>0x0000000000000000<br> |- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x800005dc]:fsgnjn.s a4, a5, a3<br> [0x800005e0]:csrrs tp, fcsr, zero<br> [0x800005e4]:sd a4, 272(ra)<br>     |
|  19|[0x8000b738]<br>0xFFFFFFFF80000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800005fc]:fsgnjn.s a3, a2, a4<br> [0x80000600]:csrrs tp, fcsr, zero<br> [0x80000604]:sd a3, 288(ra)<br>     |
|  20|[0x8000b748]<br>0x0000000000000000<br> |- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000061c]:fsgnjn.s a2, a3, a1<br> [0x80000620]:csrrs tp, fcsr, zero<br> [0x80000624]:sd a2, 304(ra)<br>     |
|  21|[0x8000b758]<br>0xFFFFFFFF80000000<br> |- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000063c]:fsgnjn.s a1, a0, a2<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd a1, 320(ra)<br>     |
|  22|[0x8000b768]<br>0x0000000000000000<br> |- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                   |[0x8000065c]:fsgnjn.s a0, a1, s1<br> [0x80000660]:csrrs tp, fcsr, zero<br> [0x80000664]:sd a0, 336(ra)<br>     |
|  23|[0x8000b778]<br>0xFFFFFFFF80000000<br> |- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                    |[0x80000684]:fsgnjn.s s1, fp, a0<br> [0x80000688]:csrrs a2, fcsr, zero<br> [0x8000068c]:sd s1, 352(ra)<br>     |
|  24|[0x8000b788]<br>0x0000000000000000<br> |- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x800006a4]:fsgnjn.s fp, s1, t2<br> [0x800006a8]:csrrs a2, fcsr, zero<br> [0x800006ac]:sd fp, 368(ra)<br>     |
|  25|[0x8000b798]<br>0xFFFFFFFF80000000<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x800006c4]:fsgnjn.s t2, t1, fp<br> [0x800006c8]:csrrs a2, fcsr, zero<br> [0x800006cc]:sd t2, 384(ra)<br>     |
|  26|[0x8000b7a8]<br>0x0000000000000000<br> |- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x800006ec]:fsgnjn.s t1, t2, t0<br> [0x800006f0]:csrrs a2, fcsr, zero<br> [0x800006f4]:sd t1, 0(fp)<br>       |
|  27|[0x8000b7b8]<br>0xFFFFFFFF80000000<br> |- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x8000070c]:fsgnjn.s t0, tp, t1<br> [0x80000710]:csrrs a2, fcsr, zero<br> [0x80000714]:sd t0, 16(fp)<br>      |
|  28|[0x8000b7c8]<br>0x0000000000000000<br> |- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x8000072c]:fsgnjn.s tp, t0, gp<br> [0x80000730]:csrrs a2, fcsr, zero<br> [0x80000734]:sd tp, 32(fp)<br>      |
|  29|[0x8000b7d8]<br>0xFFFFFFFF80000000<br> |- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x8000074c]:fsgnjn.s gp, sp, tp<br> [0x80000750]:csrrs a2, fcsr, zero<br> [0x80000754]:sd gp, 48(fp)<br>      |
|  30|[0x8000b7e8]<br>0x0000000000000000<br> |- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x8000076c]:fsgnjn.s sp, gp, ra<br> [0x80000770]:csrrs a2, fcsr, zero<br> [0x80000774]:sd sp, 64(fp)<br>      |
|  31|[0x8000b7f8]<br>0xFFFFFFFF80000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                  |[0x8000078c]:fsgnjn.s ra, zero, sp<br> [0x80000790]:csrrs a2, fcsr, zero<br> [0x80000794]:sd ra, 80(fp)<br>    |
|  32|[0x8000b808]<br>0x0000000000000000<br> |- rs1 : x1<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                  |[0x800007ac]:fsgnjn.s t6, ra, t5<br> [0x800007b0]:csrrs a2, fcsr, zero<br> [0x800007b4]:sd t6, 96(fp)<br>      |
|  33|[0x8000b818]<br>0xFFFFFFFF80000000<br> |- rs2 : x0<br>                                                                                                                                                                                                                                                                               |[0x800007cc]:fsgnjn.s t6, t5, zero<br> [0x800007d0]:csrrs a2, fcsr, zero<br> [0x800007d4]:sd t6, 112(fp)<br>   |
|  34|[0x8000b828]<br>0x0000000000000000<br> |- rd : x0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                   |[0x800007ec]:fsgnjn.s zero, t6, t5<br> [0x800007f0]:csrrs a2, fcsr, zero<br> [0x800007f4]:sd zero, 128(fp)<br> |
|  35|[0x8000b838]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000080c]:fsgnjn.s t6, t5, t4<br> [0x80000810]:csrrs a2, fcsr, zero<br> [0x80000814]:sd t6, 144(fp)<br>     |
|  36|[0x8000b848]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000082c]:fsgnjn.s t6, t5, t4<br> [0x80000830]:csrrs a2, fcsr, zero<br> [0x80000834]:sd t6, 160(fp)<br>     |
|  37|[0x8000b858]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000084c]:fsgnjn.s t6, t5, t4<br> [0x80000850]:csrrs a2, fcsr, zero<br> [0x80000854]:sd t6, 176(fp)<br>     |
|  38|[0x8000b868]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000086c]:fsgnjn.s t6, t5, t4<br> [0x80000870]:csrrs a2, fcsr, zero<br> [0x80000874]:sd t6, 192(fp)<br>     |
|  39|[0x8000b878]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000088c]:fsgnjn.s t6, t5, t4<br> [0x80000890]:csrrs a2, fcsr, zero<br> [0x80000894]:sd t6, 208(fp)<br>     |
|  40|[0x8000b888]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800008ac]:fsgnjn.s t6, t5, t4<br> [0x800008b0]:csrrs a2, fcsr, zero<br> [0x800008b4]:sd t6, 224(fp)<br>     |
|  41|[0x8000b898]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800008cc]:fsgnjn.s t6, t5, t4<br> [0x800008d0]:csrrs a2, fcsr, zero<br> [0x800008d4]:sd t6, 240(fp)<br>     |
|  42|[0x8000b8a8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800008ec]:fsgnjn.s t6, t5, t4<br> [0x800008f0]:csrrs a2, fcsr, zero<br> [0x800008f4]:sd t6, 256(fp)<br>     |
|  43|[0x8000b8b8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000090c]:fsgnjn.s t6, t5, t4<br> [0x80000910]:csrrs a2, fcsr, zero<br> [0x80000914]:sd t6, 272(fp)<br>     |
|  44|[0x8000b8c8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000092c]:fsgnjn.s t6, t5, t4<br> [0x80000930]:csrrs a2, fcsr, zero<br> [0x80000934]:sd t6, 288(fp)<br>     |
|  45|[0x8000b8d8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000094c]:fsgnjn.s t6, t5, t4<br> [0x80000950]:csrrs a2, fcsr, zero<br> [0x80000954]:sd t6, 304(fp)<br>     |
|  46|[0x8000b8e8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000096c]:fsgnjn.s t6, t5, t4<br> [0x80000970]:csrrs a2, fcsr, zero<br> [0x80000974]:sd t6, 320(fp)<br>     |
|  47|[0x8000b8f8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000098c]:fsgnjn.s t6, t5, t4<br> [0x80000990]:csrrs a2, fcsr, zero<br> [0x80000994]:sd t6, 336(fp)<br>     |
|  48|[0x8000b908]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800009ac]:fsgnjn.s t6, t5, t4<br> [0x800009b0]:csrrs a2, fcsr, zero<br> [0x800009b4]:sd t6, 352(fp)<br>     |
|  49|[0x8000b918]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800009cc]:fsgnjn.s t6, t5, t4<br> [0x800009d0]:csrrs a2, fcsr, zero<br> [0x800009d4]:sd t6, 368(fp)<br>     |
|  50|[0x8000b928]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800009ec]:fsgnjn.s t6, t5, t4<br> [0x800009f0]:csrrs a2, fcsr, zero<br> [0x800009f4]:sd t6, 384(fp)<br>     |
|  51|[0x8000b938]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a0c]:fsgnjn.s t6, t5, t4<br> [0x80000a10]:csrrs a2, fcsr, zero<br> [0x80000a14]:sd t6, 400(fp)<br>     |
|  52|[0x8000b948]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000a2c]:fsgnjn.s t6, t5, t4<br> [0x80000a30]:csrrs a2, fcsr, zero<br> [0x80000a34]:sd t6, 416(fp)<br>     |
|  53|[0x8000b958]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a4c]:fsgnjn.s t6, t5, t4<br> [0x80000a50]:csrrs a2, fcsr, zero<br> [0x80000a54]:sd t6, 432(fp)<br>     |
|  54|[0x8000b968]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000a6c]:fsgnjn.s t6, t5, t4<br> [0x80000a70]:csrrs a2, fcsr, zero<br> [0x80000a74]:sd t6, 448(fp)<br>     |
|  55|[0x8000b978]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a8c]:fsgnjn.s t6, t5, t4<br> [0x80000a90]:csrrs a2, fcsr, zero<br> [0x80000a94]:sd t6, 464(fp)<br>     |
|  56|[0x8000b988]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000aac]:fsgnjn.s t6, t5, t4<br> [0x80000ab0]:csrrs a2, fcsr, zero<br> [0x80000ab4]:sd t6, 480(fp)<br>     |
|  57|[0x8000b998]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000acc]:fsgnjn.s t6, t5, t4<br> [0x80000ad0]:csrrs a2, fcsr, zero<br> [0x80000ad4]:sd t6, 496(fp)<br>     |
|  58|[0x8000b9a8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000aec]:fsgnjn.s t6, t5, t4<br> [0x80000af0]:csrrs a2, fcsr, zero<br> [0x80000af4]:sd t6, 512(fp)<br>     |
|  59|[0x8000b9b8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b0c]:fsgnjn.s t6, t5, t4<br> [0x80000b10]:csrrs a2, fcsr, zero<br> [0x80000b14]:sd t6, 528(fp)<br>     |
|  60|[0x8000b9c8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000b2c]:fsgnjn.s t6, t5, t4<br> [0x80000b30]:csrrs a2, fcsr, zero<br> [0x80000b34]:sd t6, 544(fp)<br>     |
|  61|[0x8000b9d8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b4c]:fsgnjn.s t6, t5, t4<br> [0x80000b50]:csrrs a2, fcsr, zero<br> [0x80000b54]:sd t6, 560(fp)<br>     |
|  62|[0x8000b9e8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000b6c]:fsgnjn.s t6, t5, t4<br> [0x80000b70]:csrrs a2, fcsr, zero<br> [0x80000b74]:sd t6, 576(fp)<br>     |
|  63|[0x8000b9f8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b8c]:fsgnjn.s t6, t5, t4<br> [0x80000b90]:csrrs a2, fcsr, zero<br> [0x80000b94]:sd t6, 592(fp)<br>     |
|  64|[0x8000ba08]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000bac]:fsgnjn.s t6, t5, t4<br> [0x80000bb0]:csrrs a2, fcsr, zero<br> [0x80000bb4]:sd t6, 608(fp)<br>     |
|  65|[0x8000ba18]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000bcc]:fsgnjn.s t6, t5, t4<br> [0x80000bd0]:csrrs a2, fcsr, zero<br> [0x80000bd4]:sd t6, 624(fp)<br>     |
|  66|[0x8000ba28]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000bec]:fsgnjn.s t6, t5, t4<br> [0x80000bf0]:csrrs a2, fcsr, zero<br> [0x80000bf4]:sd t6, 640(fp)<br>     |
|  67|[0x8000ba38]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c0c]:fsgnjn.s t6, t5, t4<br> [0x80000c10]:csrrs a2, fcsr, zero<br> [0x80000c14]:sd t6, 656(fp)<br>     |
|  68|[0x8000ba48]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000c2c]:fsgnjn.s t6, t5, t4<br> [0x80000c30]:csrrs a2, fcsr, zero<br> [0x80000c34]:sd t6, 672(fp)<br>     |
|  69|[0x8000ba58]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c4c]:fsgnjn.s t6, t5, t4<br> [0x80000c50]:csrrs a2, fcsr, zero<br> [0x80000c54]:sd t6, 688(fp)<br>     |
|  70|[0x8000ba68]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000c6c]:fsgnjn.s t6, t5, t4<br> [0x80000c70]:csrrs a2, fcsr, zero<br> [0x80000c74]:sd t6, 704(fp)<br>     |
|  71|[0x8000ba78]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c8c]:fsgnjn.s t6, t5, t4<br> [0x80000c90]:csrrs a2, fcsr, zero<br> [0x80000c94]:sd t6, 720(fp)<br>     |
|  72|[0x8000ba88]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000cac]:fsgnjn.s t6, t5, t4<br> [0x80000cb0]:csrrs a2, fcsr, zero<br> [0x80000cb4]:sd t6, 736(fp)<br>     |
|  73|[0x8000ba98]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000ccc]:fsgnjn.s t6, t5, t4<br> [0x80000cd0]:csrrs a2, fcsr, zero<br> [0x80000cd4]:sd t6, 752(fp)<br>     |
|  74|[0x8000baa8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000cec]:fsgnjn.s t6, t5, t4<br> [0x80000cf0]:csrrs a2, fcsr, zero<br> [0x80000cf4]:sd t6, 768(fp)<br>     |
|  75|[0x8000bab8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d0c]:fsgnjn.s t6, t5, t4<br> [0x80000d10]:csrrs a2, fcsr, zero<br> [0x80000d14]:sd t6, 784(fp)<br>     |
|  76|[0x8000bac8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000d2c]:fsgnjn.s t6, t5, t4<br> [0x80000d30]:csrrs a2, fcsr, zero<br> [0x80000d34]:sd t6, 800(fp)<br>     |
|  77|[0x8000bad8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d4c]:fsgnjn.s t6, t5, t4<br> [0x80000d50]:csrrs a2, fcsr, zero<br> [0x80000d54]:sd t6, 816(fp)<br>     |
|  78|[0x8000bae8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000d6c]:fsgnjn.s t6, t5, t4<br> [0x80000d70]:csrrs a2, fcsr, zero<br> [0x80000d74]:sd t6, 832(fp)<br>     |
|  79|[0x8000baf8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d8c]:fsgnjn.s t6, t5, t4<br> [0x80000d90]:csrrs a2, fcsr, zero<br> [0x80000d94]:sd t6, 848(fp)<br>     |
|  80|[0x8000bb08]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000dac]:fsgnjn.s t6, t5, t4<br> [0x80000db0]:csrrs a2, fcsr, zero<br> [0x80000db4]:sd t6, 864(fp)<br>     |
|  81|[0x8000bb18]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000dcc]:fsgnjn.s t6, t5, t4<br> [0x80000dd0]:csrrs a2, fcsr, zero<br> [0x80000dd4]:sd t6, 880(fp)<br>     |
|  82|[0x8000bb28]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000dec]:fsgnjn.s t6, t5, t4<br> [0x80000df0]:csrrs a2, fcsr, zero<br> [0x80000df4]:sd t6, 896(fp)<br>     |
|  83|[0x8000bb38]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e0c]:fsgnjn.s t6, t5, t4<br> [0x80000e10]:csrrs a2, fcsr, zero<br> [0x80000e14]:sd t6, 912(fp)<br>     |
|  84|[0x8000bb48]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000e2c]:fsgnjn.s t6, t5, t4<br> [0x80000e30]:csrrs a2, fcsr, zero<br> [0x80000e34]:sd t6, 928(fp)<br>     |
|  85|[0x8000bb58]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e4c]:fsgnjn.s t6, t5, t4<br> [0x80000e50]:csrrs a2, fcsr, zero<br> [0x80000e54]:sd t6, 944(fp)<br>     |
|  86|[0x8000bb68]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000e6c]:fsgnjn.s t6, t5, t4<br> [0x80000e70]:csrrs a2, fcsr, zero<br> [0x80000e74]:sd t6, 960(fp)<br>     |
|  87|[0x8000bb78]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e8c]:fsgnjn.s t6, t5, t4<br> [0x80000e90]:csrrs a2, fcsr, zero<br> [0x80000e94]:sd t6, 976(fp)<br>     |
|  88|[0x8000bb88]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000eac]:fsgnjn.s t6, t5, t4<br> [0x80000eb0]:csrrs a2, fcsr, zero<br> [0x80000eb4]:sd t6, 992(fp)<br>     |
|  89|[0x8000bb98]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000ecc]:fsgnjn.s t6, t5, t4<br> [0x80000ed0]:csrrs a2, fcsr, zero<br> [0x80000ed4]:sd t6, 1008(fp)<br>    |
|  90|[0x8000bba8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000eec]:fsgnjn.s t6, t5, t4<br> [0x80000ef0]:csrrs a2, fcsr, zero<br> [0x80000ef4]:sd t6, 1024(fp)<br>    |
|  91|[0x8000bbb8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f0c]:fsgnjn.s t6, t5, t4<br> [0x80000f10]:csrrs a2, fcsr, zero<br> [0x80000f14]:sd t6, 1040(fp)<br>    |
|  92|[0x8000bbc8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000f2c]:fsgnjn.s t6, t5, t4<br> [0x80000f30]:csrrs a2, fcsr, zero<br> [0x80000f34]:sd t6, 1056(fp)<br>    |
|  93|[0x8000bbd8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f4c]:fsgnjn.s t6, t5, t4<br> [0x80000f50]:csrrs a2, fcsr, zero<br> [0x80000f54]:sd t6, 1072(fp)<br>    |
|  94|[0x8000bbe8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000f6c]:fsgnjn.s t6, t5, t4<br> [0x80000f70]:csrrs a2, fcsr, zero<br> [0x80000f74]:sd t6, 1088(fp)<br>    |
|  95|[0x8000bbf8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f8c]:fsgnjn.s t6, t5, t4<br> [0x80000f90]:csrrs a2, fcsr, zero<br> [0x80000f94]:sd t6, 1104(fp)<br>    |
|  96|[0x8000bc08]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000fac]:fsgnjn.s t6, t5, t4<br> [0x80000fb0]:csrrs a2, fcsr, zero<br> [0x80000fb4]:sd t6, 1120(fp)<br>    |
|  97|[0x8000bc18]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000fcc]:fsgnjn.s t6, t5, t4<br> [0x80000fd0]:csrrs a2, fcsr, zero<br> [0x80000fd4]:sd t6, 1136(fp)<br>    |
|  98|[0x8000bc28]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000fec]:fsgnjn.s t6, t5, t4<br> [0x80000ff0]:csrrs a2, fcsr, zero<br> [0x80000ff4]:sd t6, 1152(fp)<br>    |
|  99|[0x8000bc38]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000100c]:fsgnjn.s t6, t5, t4<br> [0x80001010]:csrrs a2, fcsr, zero<br> [0x80001014]:sd t6, 1168(fp)<br>    |
| 100|[0x8000bc48]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000102c]:fsgnjn.s t6, t5, t4<br> [0x80001030]:csrrs a2, fcsr, zero<br> [0x80001034]:sd t6, 1184(fp)<br>    |
| 101|[0x8000bc58]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000104c]:fsgnjn.s t6, t5, t4<br> [0x80001050]:csrrs a2, fcsr, zero<br> [0x80001054]:sd t6, 1200(fp)<br>    |
| 102|[0x8000bc68]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000106c]:fsgnjn.s t6, t5, t4<br> [0x80001070]:csrrs a2, fcsr, zero<br> [0x80001074]:sd t6, 1216(fp)<br>    |
| 103|[0x8000bc78]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000108c]:fsgnjn.s t6, t5, t4<br> [0x80001090]:csrrs a2, fcsr, zero<br> [0x80001094]:sd t6, 1232(fp)<br>    |
| 104|[0x8000bc88]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800010ac]:fsgnjn.s t6, t5, t4<br> [0x800010b0]:csrrs a2, fcsr, zero<br> [0x800010b4]:sd t6, 1248(fp)<br>    |
| 105|[0x8000bc98]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800010cc]:fsgnjn.s t6, t5, t4<br> [0x800010d0]:csrrs a2, fcsr, zero<br> [0x800010d4]:sd t6, 1264(fp)<br>    |
| 106|[0x8000bca8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800010ec]:fsgnjn.s t6, t5, t4<br> [0x800010f0]:csrrs a2, fcsr, zero<br> [0x800010f4]:sd t6, 1280(fp)<br>    |
| 107|[0x8000bcb8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000110c]:fsgnjn.s t6, t5, t4<br> [0x80001110]:csrrs a2, fcsr, zero<br> [0x80001114]:sd t6, 1296(fp)<br>    |
| 108|[0x8000bcc8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000112c]:fsgnjn.s t6, t5, t4<br> [0x80001130]:csrrs a2, fcsr, zero<br> [0x80001134]:sd t6, 1312(fp)<br>    |
| 109|[0x8000bcd8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000114c]:fsgnjn.s t6, t5, t4<br> [0x80001150]:csrrs a2, fcsr, zero<br> [0x80001154]:sd t6, 1328(fp)<br>    |
| 110|[0x8000bce8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000116c]:fsgnjn.s t6, t5, t4<br> [0x80001170]:csrrs a2, fcsr, zero<br> [0x80001174]:sd t6, 1344(fp)<br>    |
| 111|[0x8000bcf8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000118c]:fsgnjn.s t6, t5, t4<br> [0x80001190]:csrrs a2, fcsr, zero<br> [0x80001194]:sd t6, 1360(fp)<br>    |
| 112|[0x8000bd08]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800011ac]:fsgnjn.s t6, t5, t4<br> [0x800011b0]:csrrs a2, fcsr, zero<br> [0x800011b4]:sd t6, 1376(fp)<br>    |
| 113|[0x8000bd18]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800011cc]:fsgnjn.s t6, t5, t4<br> [0x800011d0]:csrrs a2, fcsr, zero<br> [0x800011d4]:sd t6, 1392(fp)<br>    |
| 114|[0x8000bd28]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800011ec]:fsgnjn.s t6, t5, t4<br> [0x800011f0]:csrrs a2, fcsr, zero<br> [0x800011f4]:sd t6, 1408(fp)<br>    |
| 115|[0x8000bd38]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000120c]:fsgnjn.s t6, t5, t4<br> [0x80001210]:csrrs a2, fcsr, zero<br> [0x80001214]:sd t6, 1424(fp)<br>    |
| 116|[0x8000bd48]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000122c]:fsgnjn.s t6, t5, t4<br> [0x80001230]:csrrs a2, fcsr, zero<br> [0x80001234]:sd t6, 1440(fp)<br>    |
| 117|[0x8000bd58]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000124c]:fsgnjn.s t6, t5, t4<br> [0x80001250]:csrrs a2, fcsr, zero<br> [0x80001254]:sd t6, 1456(fp)<br>    |
| 118|[0x8000bd68]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000126c]:fsgnjn.s t6, t5, t4<br> [0x80001270]:csrrs a2, fcsr, zero<br> [0x80001274]:sd t6, 1472(fp)<br>    |
| 119|[0x8000bd78]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000128c]:fsgnjn.s t6, t5, t4<br> [0x80001290]:csrrs a2, fcsr, zero<br> [0x80001294]:sd t6, 1488(fp)<br>    |
| 120|[0x8000bd88]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800012ac]:fsgnjn.s t6, t5, t4<br> [0x800012b0]:csrrs a2, fcsr, zero<br> [0x800012b4]:sd t6, 1504(fp)<br>    |
| 121|[0x8000bd98]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800012cc]:fsgnjn.s t6, t5, t4<br> [0x800012d0]:csrrs a2, fcsr, zero<br> [0x800012d4]:sd t6, 1520(fp)<br>    |
| 122|[0x8000bda8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800012ec]:fsgnjn.s t6, t5, t4<br> [0x800012f0]:csrrs a2, fcsr, zero<br> [0x800012f4]:sd t6, 1536(fp)<br>    |
| 123|[0x8000bdb8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000130c]:fsgnjn.s t6, t5, t4<br> [0x80001310]:csrrs a2, fcsr, zero<br> [0x80001314]:sd t6, 1552(fp)<br>    |
| 124|[0x8000bdc8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000132c]:fsgnjn.s t6, t5, t4<br> [0x80001330]:csrrs a2, fcsr, zero<br> [0x80001334]:sd t6, 1568(fp)<br>    |
| 125|[0x8000bdd8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000134c]:fsgnjn.s t6, t5, t4<br> [0x80001350]:csrrs a2, fcsr, zero<br> [0x80001354]:sd t6, 1584(fp)<br>    |
| 126|[0x8000bde8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000136c]:fsgnjn.s t6, t5, t4<br> [0x80001370]:csrrs a2, fcsr, zero<br> [0x80001374]:sd t6, 1600(fp)<br>    |
| 127|[0x8000bdf8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000138c]:fsgnjn.s t6, t5, t4<br> [0x80001390]:csrrs a2, fcsr, zero<br> [0x80001394]:sd t6, 1616(fp)<br>    |
| 128|[0x8000be08]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800013ac]:fsgnjn.s t6, t5, t4<br> [0x800013b0]:csrrs a2, fcsr, zero<br> [0x800013b4]:sd t6, 1632(fp)<br>    |
| 129|[0x8000be18]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800013cc]:fsgnjn.s t6, t5, t4<br> [0x800013d0]:csrrs a2, fcsr, zero<br> [0x800013d4]:sd t6, 1648(fp)<br>    |
| 130|[0x8000be28]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800013ec]:fsgnjn.s t6, t5, t4<br> [0x800013f0]:csrrs a2, fcsr, zero<br> [0x800013f4]:sd t6, 1664(fp)<br>    |
| 131|[0x8000be38]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000140c]:fsgnjn.s t6, t5, t4<br> [0x80001410]:csrrs a2, fcsr, zero<br> [0x80001414]:sd t6, 1680(fp)<br>    |
| 132|[0x8000be48]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000142c]:fsgnjn.s t6, t5, t4<br> [0x80001430]:csrrs a2, fcsr, zero<br> [0x80001434]:sd t6, 1696(fp)<br>    |
| 133|[0x8000be58]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000144c]:fsgnjn.s t6, t5, t4<br> [0x80001450]:csrrs a2, fcsr, zero<br> [0x80001454]:sd t6, 1712(fp)<br>    |
| 134|[0x8000be68]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000146c]:fsgnjn.s t6, t5, t4<br> [0x80001470]:csrrs a2, fcsr, zero<br> [0x80001474]:sd t6, 1728(fp)<br>    |
| 135|[0x8000be78]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000148c]:fsgnjn.s t6, t5, t4<br> [0x80001490]:csrrs a2, fcsr, zero<br> [0x80001494]:sd t6, 1744(fp)<br>    |
| 136|[0x8000be88]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800014ac]:fsgnjn.s t6, t5, t4<br> [0x800014b0]:csrrs a2, fcsr, zero<br> [0x800014b4]:sd t6, 1760(fp)<br>    |
| 137|[0x8000be98]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800014cc]:fsgnjn.s t6, t5, t4<br> [0x800014d0]:csrrs a2, fcsr, zero<br> [0x800014d4]:sd t6, 1776(fp)<br>    |
| 138|[0x8000bea8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800014ec]:fsgnjn.s t6, t5, t4<br> [0x800014f0]:csrrs a2, fcsr, zero<br> [0x800014f4]:sd t6, 1792(fp)<br>    |
| 139|[0x8000beb8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000150c]:fsgnjn.s t6, t5, t4<br> [0x80001510]:csrrs a2, fcsr, zero<br> [0x80001514]:sd t6, 1808(fp)<br>    |
| 140|[0x8000bec8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000152c]:fsgnjn.s t6, t5, t4<br> [0x80001530]:csrrs a2, fcsr, zero<br> [0x80001534]:sd t6, 1824(fp)<br>    |
| 141|[0x8000bed8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000154c]:fsgnjn.s t6, t5, t4<br> [0x80001550]:csrrs a2, fcsr, zero<br> [0x80001554]:sd t6, 1840(fp)<br>    |
| 142|[0x8000bee8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000156c]:fsgnjn.s t6, t5, t4<br> [0x80001570]:csrrs a2, fcsr, zero<br> [0x80001574]:sd t6, 1856(fp)<br>    |
| 143|[0x8000bef8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000158c]:fsgnjn.s t6, t5, t4<br> [0x80001590]:csrrs a2, fcsr, zero<br> [0x80001594]:sd t6, 1872(fp)<br>    |
| 144|[0x8000bf08]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800015ac]:fsgnjn.s t6, t5, t4<br> [0x800015b0]:csrrs a2, fcsr, zero<br> [0x800015b4]:sd t6, 1888(fp)<br>    |
| 145|[0x8000bf18]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800015cc]:fsgnjn.s t6, t5, t4<br> [0x800015d0]:csrrs a2, fcsr, zero<br> [0x800015d4]:sd t6, 1904(fp)<br>    |
| 146|[0x8000bf28]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800015ec]:fsgnjn.s t6, t5, t4<br> [0x800015f0]:csrrs a2, fcsr, zero<br> [0x800015f4]:sd t6, 1920(fp)<br>    |
| 147|[0x8000bf38]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000160c]:fsgnjn.s t6, t5, t4<br> [0x80001610]:csrrs a2, fcsr, zero<br> [0x80001614]:sd t6, 1936(fp)<br>    |
| 148|[0x8000bf48]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000162c]:fsgnjn.s t6, t5, t4<br> [0x80001630]:csrrs a2, fcsr, zero<br> [0x80001634]:sd t6, 1952(fp)<br>    |
| 149|[0x8000bf58]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000164c]:fsgnjn.s t6, t5, t4<br> [0x80001650]:csrrs a2, fcsr, zero<br> [0x80001654]:sd t6, 1968(fp)<br>    |
| 150|[0x8000bf68]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000166c]:fsgnjn.s t6, t5, t4<br> [0x80001670]:csrrs a2, fcsr, zero<br> [0x80001674]:sd t6, 1984(fp)<br>    |
| 151|[0x8000bf78]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800016ac]:fsgnjn.s t6, t5, t4<br> [0x800016b0]:csrrs a2, fcsr, zero<br> [0x800016b4]:sd t6, 2000(fp)<br>    |
| 152|[0x8000bf88]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800016ec]:fsgnjn.s t6, t5, t4<br> [0x800016f0]:csrrs a2, fcsr, zero<br> [0x800016f4]:sd t6, 2016(fp)<br>    |
| 153|[0x8000bf98]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000172c]:fsgnjn.s t6, t5, t4<br> [0x80001730]:csrrs a2, fcsr, zero<br> [0x80001734]:sd t6, 2032(fp)<br>    |
| 154|[0x8000bfa8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001774]:fsgnjn.s t6, t5, t4<br> [0x80001778]:csrrs a2, fcsr, zero<br> [0x8000177c]:sd t6, 0(fp)<br>       |
| 155|[0x8000bfb8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800017b4]:fsgnjn.s t6, t5, t4<br> [0x800017b8]:csrrs a2, fcsr, zero<br> [0x800017bc]:sd t6, 16(fp)<br>      |
| 156|[0x8000bfc8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800017f4]:fsgnjn.s t6, t5, t4<br> [0x800017f8]:csrrs a2, fcsr, zero<br> [0x800017fc]:sd t6, 32(fp)<br>      |
| 157|[0x8000bfd8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001834]:fsgnjn.s t6, t5, t4<br> [0x80001838]:csrrs a2, fcsr, zero<br> [0x8000183c]:sd t6, 48(fp)<br>      |
| 158|[0x8000bfe8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001874]:fsgnjn.s t6, t5, t4<br> [0x80001878]:csrrs a2, fcsr, zero<br> [0x8000187c]:sd t6, 64(fp)<br>      |
| 159|[0x8000bff8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800018b4]:fsgnjn.s t6, t5, t4<br> [0x800018b8]:csrrs a2, fcsr, zero<br> [0x800018bc]:sd t6, 80(fp)<br>      |
| 160|[0x8000c008]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800018f4]:fsgnjn.s t6, t5, t4<br> [0x800018f8]:csrrs a2, fcsr, zero<br> [0x800018fc]:sd t6, 96(fp)<br>      |
| 161|[0x8000c018]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001934]:fsgnjn.s t6, t5, t4<br> [0x80001938]:csrrs a2, fcsr, zero<br> [0x8000193c]:sd t6, 112(fp)<br>     |
| 162|[0x8000c028]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001974]:fsgnjn.s t6, t5, t4<br> [0x80001978]:csrrs a2, fcsr, zero<br> [0x8000197c]:sd t6, 128(fp)<br>     |
| 163|[0x8000c038]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800019b4]:fsgnjn.s t6, t5, t4<br> [0x800019b8]:csrrs a2, fcsr, zero<br> [0x800019bc]:sd t6, 144(fp)<br>     |
| 164|[0x8000c048]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800019f4]:fsgnjn.s t6, t5, t4<br> [0x800019f8]:csrrs a2, fcsr, zero<br> [0x800019fc]:sd t6, 160(fp)<br>     |
| 165|[0x8000c058]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001a34]:fsgnjn.s t6, t5, t4<br> [0x80001a38]:csrrs a2, fcsr, zero<br> [0x80001a3c]:sd t6, 176(fp)<br>     |
| 166|[0x8000c068]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001a74]:fsgnjn.s t6, t5, t4<br> [0x80001a78]:csrrs a2, fcsr, zero<br> [0x80001a7c]:sd t6, 192(fp)<br>     |
| 167|[0x8000c078]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001ab4]:fsgnjn.s t6, t5, t4<br> [0x80001ab8]:csrrs a2, fcsr, zero<br> [0x80001abc]:sd t6, 208(fp)<br>     |
| 168|[0x8000c088]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001af4]:fsgnjn.s t6, t5, t4<br> [0x80001af8]:csrrs a2, fcsr, zero<br> [0x80001afc]:sd t6, 224(fp)<br>     |
| 169|[0x8000c098]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001b34]:fsgnjn.s t6, t5, t4<br> [0x80001b38]:csrrs a2, fcsr, zero<br> [0x80001b3c]:sd t6, 240(fp)<br>     |
| 170|[0x8000c0a8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001b74]:fsgnjn.s t6, t5, t4<br> [0x80001b78]:csrrs a2, fcsr, zero<br> [0x80001b7c]:sd t6, 256(fp)<br>     |
| 171|[0x8000c0b8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001bb4]:fsgnjn.s t6, t5, t4<br> [0x80001bb8]:csrrs a2, fcsr, zero<br> [0x80001bbc]:sd t6, 272(fp)<br>     |
| 172|[0x8000c0c8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001bf4]:fsgnjn.s t6, t5, t4<br> [0x80001bf8]:csrrs a2, fcsr, zero<br> [0x80001bfc]:sd t6, 288(fp)<br>     |
| 173|[0x8000c0d8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001c34]:fsgnjn.s t6, t5, t4<br> [0x80001c38]:csrrs a2, fcsr, zero<br> [0x80001c3c]:sd t6, 304(fp)<br>     |
| 174|[0x8000c0e8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001c74]:fsgnjn.s t6, t5, t4<br> [0x80001c78]:csrrs a2, fcsr, zero<br> [0x80001c7c]:sd t6, 320(fp)<br>     |
| 175|[0x8000c0f8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001cb4]:fsgnjn.s t6, t5, t4<br> [0x80001cb8]:csrrs a2, fcsr, zero<br> [0x80001cbc]:sd t6, 336(fp)<br>     |
| 176|[0x8000c108]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001cf4]:fsgnjn.s t6, t5, t4<br> [0x80001cf8]:csrrs a2, fcsr, zero<br> [0x80001cfc]:sd t6, 352(fp)<br>     |
| 177|[0x8000c118]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001d34]:fsgnjn.s t6, t5, t4<br> [0x80001d38]:csrrs a2, fcsr, zero<br> [0x80001d3c]:sd t6, 368(fp)<br>     |
| 178|[0x8000c128]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001d74]:fsgnjn.s t6, t5, t4<br> [0x80001d78]:csrrs a2, fcsr, zero<br> [0x80001d7c]:sd t6, 384(fp)<br>     |
| 179|[0x8000c138]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001db4]:fsgnjn.s t6, t5, t4<br> [0x80001db8]:csrrs a2, fcsr, zero<br> [0x80001dbc]:sd t6, 400(fp)<br>     |
| 180|[0x8000c148]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001df4]:fsgnjn.s t6, t5, t4<br> [0x80001df8]:csrrs a2, fcsr, zero<br> [0x80001dfc]:sd t6, 416(fp)<br>     |
| 181|[0x8000c158]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001e34]:fsgnjn.s t6, t5, t4<br> [0x80001e38]:csrrs a2, fcsr, zero<br> [0x80001e3c]:sd t6, 432(fp)<br>     |
| 182|[0x8000c168]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001e74]:fsgnjn.s t6, t5, t4<br> [0x80001e78]:csrrs a2, fcsr, zero<br> [0x80001e7c]:sd t6, 448(fp)<br>     |
| 183|[0x8000c178]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001eb4]:fsgnjn.s t6, t5, t4<br> [0x80001eb8]:csrrs a2, fcsr, zero<br> [0x80001ebc]:sd t6, 464(fp)<br>     |
| 184|[0x8000c188]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001ef4]:fsgnjn.s t6, t5, t4<br> [0x80001ef8]:csrrs a2, fcsr, zero<br> [0x80001efc]:sd t6, 480(fp)<br>     |
| 185|[0x8000c198]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001f34]:fsgnjn.s t6, t5, t4<br> [0x80001f38]:csrrs a2, fcsr, zero<br> [0x80001f3c]:sd t6, 496(fp)<br>     |
| 186|[0x8000c1a8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001f74]:fsgnjn.s t6, t5, t4<br> [0x80001f78]:csrrs a2, fcsr, zero<br> [0x80001f7c]:sd t6, 512(fp)<br>     |
| 187|[0x8000c1b8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001fb4]:fsgnjn.s t6, t5, t4<br> [0x80001fb8]:csrrs a2, fcsr, zero<br> [0x80001fbc]:sd t6, 528(fp)<br>     |
| 188|[0x8000c1c8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001ff4]:fsgnjn.s t6, t5, t4<br> [0x80001ff8]:csrrs a2, fcsr, zero<br> [0x80001ffc]:sd t6, 544(fp)<br>     |
| 189|[0x8000c1d8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002034]:fsgnjn.s t6, t5, t4<br> [0x80002038]:csrrs a2, fcsr, zero<br> [0x8000203c]:sd t6, 560(fp)<br>     |
| 190|[0x8000c1e8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002074]:fsgnjn.s t6, t5, t4<br> [0x80002078]:csrrs a2, fcsr, zero<br> [0x8000207c]:sd t6, 576(fp)<br>     |
| 191|[0x8000c1f8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800020b4]:fsgnjn.s t6, t5, t4<br> [0x800020b8]:csrrs a2, fcsr, zero<br> [0x800020bc]:sd t6, 592(fp)<br>     |
| 192|[0x8000c208]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800020f4]:fsgnjn.s t6, t5, t4<br> [0x800020f8]:csrrs a2, fcsr, zero<br> [0x800020fc]:sd t6, 608(fp)<br>     |
| 193|[0x8000c218]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002134]:fsgnjn.s t6, t5, t4<br> [0x80002138]:csrrs a2, fcsr, zero<br> [0x8000213c]:sd t6, 624(fp)<br>     |
| 194|[0x8000c228]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002174]:fsgnjn.s t6, t5, t4<br> [0x80002178]:csrrs a2, fcsr, zero<br> [0x8000217c]:sd t6, 640(fp)<br>     |
| 195|[0x8000c238]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800021b4]:fsgnjn.s t6, t5, t4<br> [0x800021b8]:csrrs a2, fcsr, zero<br> [0x800021bc]:sd t6, 656(fp)<br>     |
| 196|[0x8000c248]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800021f4]:fsgnjn.s t6, t5, t4<br> [0x800021f8]:csrrs a2, fcsr, zero<br> [0x800021fc]:sd t6, 672(fp)<br>     |
| 197|[0x8000c258]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002234]:fsgnjn.s t6, t5, t4<br> [0x80002238]:csrrs a2, fcsr, zero<br> [0x8000223c]:sd t6, 688(fp)<br>     |
| 198|[0x8000c268]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002274]:fsgnjn.s t6, t5, t4<br> [0x80002278]:csrrs a2, fcsr, zero<br> [0x8000227c]:sd t6, 704(fp)<br>     |
| 199|[0x8000c278]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800022b4]:fsgnjn.s t6, t5, t4<br> [0x800022b8]:csrrs a2, fcsr, zero<br> [0x800022bc]:sd t6, 720(fp)<br>     |
| 200|[0x8000c288]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800022f4]:fsgnjn.s t6, t5, t4<br> [0x800022f8]:csrrs a2, fcsr, zero<br> [0x800022fc]:sd t6, 736(fp)<br>     |
| 201|[0x8000c298]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002334]:fsgnjn.s t6, t5, t4<br> [0x80002338]:csrrs a2, fcsr, zero<br> [0x8000233c]:sd t6, 752(fp)<br>     |
| 202|[0x8000c2a8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002374]:fsgnjn.s t6, t5, t4<br> [0x80002378]:csrrs a2, fcsr, zero<br> [0x8000237c]:sd t6, 768(fp)<br>     |
| 203|[0x8000c2b8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800023b4]:fsgnjn.s t6, t5, t4<br> [0x800023b8]:csrrs a2, fcsr, zero<br> [0x800023bc]:sd t6, 784(fp)<br>     |
| 204|[0x8000c2c8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800023f4]:fsgnjn.s t6, t5, t4<br> [0x800023f8]:csrrs a2, fcsr, zero<br> [0x800023fc]:sd t6, 800(fp)<br>     |
| 205|[0x8000c2d8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002434]:fsgnjn.s t6, t5, t4<br> [0x80002438]:csrrs a2, fcsr, zero<br> [0x8000243c]:sd t6, 816(fp)<br>     |
| 206|[0x8000c2e8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002474]:fsgnjn.s t6, t5, t4<br> [0x80002478]:csrrs a2, fcsr, zero<br> [0x8000247c]:sd t6, 832(fp)<br>     |
| 207|[0x8000c2f8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800024b4]:fsgnjn.s t6, t5, t4<br> [0x800024b8]:csrrs a2, fcsr, zero<br> [0x800024bc]:sd t6, 848(fp)<br>     |
| 208|[0x8000c308]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800024f4]:fsgnjn.s t6, t5, t4<br> [0x800024f8]:csrrs a2, fcsr, zero<br> [0x800024fc]:sd t6, 864(fp)<br>     |
| 209|[0x8000c318]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002534]:fsgnjn.s t6, t5, t4<br> [0x80002538]:csrrs a2, fcsr, zero<br> [0x8000253c]:sd t6, 880(fp)<br>     |
| 210|[0x8000c328]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002574]:fsgnjn.s t6, t5, t4<br> [0x80002578]:csrrs a2, fcsr, zero<br> [0x8000257c]:sd t6, 896(fp)<br>     |
| 211|[0x8000c338]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800025b4]:fsgnjn.s t6, t5, t4<br> [0x800025b8]:csrrs a2, fcsr, zero<br> [0x800025bc]:sd t6, 912(fp)<br>     |
| 212|[0x8000c348]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800025f4]:fsgnjn.s t6, t5, t4<br> [0x800025f8]:csrrs a2, fcsr, zero<br> [0x800025fc]:sd t6, 928(fp)<br>     |
| 213|[0x8000c358]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002634]:fsgnjn.s t6, t5, t4<br> [0x80002638]:csrrs a2, fcsr, zero<br> [0x8000263c]:sd t6, 944(fp)<br>     |
| 214|[0x8000c368]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002674]:fsgnjn.s t6, t5, t4<br> [0x80002678]:csrrs a2, fcsr, zero<br> [0x8000267c]:sd t6, 960(fp)<br>     |
| 215|[0x8000c378]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800026b4]:fsgnjn.s t6, t5, t4<br> [0x800026b8]:csrrs a2, fcsr, zero<br> [0x800026bc]:sd t6, 976(fp)<br>     |
| 216|[0x8000c388]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800026f4]:fsgnjn.s t6, t5, t4<br> [0x800026f8]:csrrs a2, fcsr, zero<br> [0x800026fc]:sd t6, 992(fp)<br>     |
| 217|[0x8000c398]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002734]:fsgnjn.s t6, t5, t4<br> [0x80002738]:csrrs a2, fcsr, zero<br> [0x8000273c]:sd t6, 1008(fp)<br>    |
| 218|[0x8000c3a8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002774]:fsgnjn.s t6, t5, t4<br> [0x80002778]:csrrs a2, fcsr, zero<br> [0x8000277c]:sd t6, 1024(fp)<br>    |
| 219|[0x8000c3b8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800027b4]:fsgnjn.s t6, t5, t4<br> [0x800027b8]:csrrs a2, fcsr, zero<br> [0x800027bc]:sd t6, 1040(fp)<br>    |
| 220|[0x8000c3c8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800027f4]:fsgnjn.s t6, t5, t4<br> [0x800027f8]:csrrs a2, fcsr, zero<br> [0x800027fc]:sd t6, 1056(fp)<br>    |
| 221|[0x8000c3d8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002834]:fsgnjn.s t6, t5, t4<br> [0x80002838]:csrrs a2, fcsr, zero<br> [0x8000283c]:sd t6, 1072(fp)<br>    |
| 222|[0x8000c3e8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002874]:fsgnjn.s t6, t5, t4<br> [0x80002878]:csrrs a2, fcsr, zero<br> [0x8000287c]:sd t6, 1088(fp)<br>    |
| 223|[0x8000c3f8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800028b4]:fsgnjn.s t6, t5, t4<br> [0x800028b8]:csrrs a2, fcsr, zero<br> [0x800028bc]:sd t6, 1104(fp)<br>    |
| 224|[0x8000c408]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800028f4]:fsgnjn.s t6, t5, t4<br> [0x800028f8]:csrrs a2, fcsr, zero<br> [0x800028fc]:sd t6, 1120(fp)<br>    |
| 225|[0x8000c418]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002934]:fsgnjn.s t6, t5, t4<br> [0x80002938]:csrrs a2, fcsr, zero<br> [0x8000293c]:sd t6, 1136(fp)<br>    |
| 226|[0x8000c428]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002974]:fsgnjn.s t6, t5, t4<br> [0x80002978]:csrrs a2, fcsr, zero<br> [0x8000297c]:sd t6, 1152(fp)<br>    |
| 227|[0x8000c438]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800029b4]:fsgnjn.s t6, t5, t4<br> [0x800029b8]:csrrs a2, fcsr, zero<br> [0x800029bc]:sd t6, 1168(fp)<br>    |
| 228|[0x8000c448]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800029f4]:fsgnjn.s t6, t5, t4<br> [0x800029f8]:csrrs a2, fcsr, zero<br> [0x800029fc]:sd t6, 1184(fp)<br>    |
| 229|[0x8000c458]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002a34]:fsgnjn.s t6, t5, t4<br> [0x80002a38]:csrrs a2, fcsr, zero<br> [0x80002a3c]:sd t6, 1200(fp)<br>    |
| 230|[0x8000c468]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002a74]:fsgnjn.s t6, t5, t4<br> [0x80002a78]:csrrs a2, fcsr, zero<br> [0x80002a7c]:sd t6, 1216(fp)<br>    |
| 231|[0x8000c478]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002ab4]:fsgnjn.s t6, t5, t4<br> [0x80002ab8]:csrrs a2, fcsr, zero<br> [0x80002abc]:sd t6, 1232(fp)<br>    |
| 232|[0x8000c488]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002af4]:fsgnjn.s t6, t5, t4<br> [0x80002af8]:csrrs a2, fcsr, zero<br> [0x80002afc]:sd t6, 1248(fp)<br>    |
| 233|[0x8000c498]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002b34]:fsgnjn.s t6, t5, t4<br> [0x80002b38]:csrrs a2, fcsr, zero<br> [0x80002b3c]:sd t6, 1264(fp)<br>    |
| 234|[0x8000c4a8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002b74]:fsgnjn.s t6, t5, t4<br> [0x80002b78]:csrrs a2, fcsr, zero<br> [0x80002b7c]:sd t6, 1280(fp)<br>    |
| 235|[0x8000c4b8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002bb4]:fsgnjn.s t6, t5, t4<br> [0x80002bb8]:csrrs a2, fcsr, zero<br> [0x80002bbc]:sd t6, 1296(fp)<br>    |
| 236|[0x8000c4c8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002bf4]:fsgnjn.s t6, t5, t4<br> [0x80002bf8]:csrrs a2, fcsr, zero<br> [0x80002bfc]:sd t6, 1312(fp)<br>    |
| 237|[0x8000c4d8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002c34]:fsgnjn.s t6, t5, t4<br> [0x80002c38]:csrrs a2, fcsr, zero<br> [0x80002c3c]:sd t6, 1328(fp)<br>    |
| 238|[0x8000c4e8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002c74]:fsgnjn.s t6, t5, t4<br> [0x80002c78]:csrrs a2, fcsr, zero<br> [0x80002c7c]:sd t6, 1344(fp)<br>    |
| 239|[0x8000c4f8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002cb4]:fsgnjn.s t6, t5, t4<br> [0x80002cb8]:csrrs a2, fcsr, zero<br> [0x80002cbc]:sd t6, 1360(fp)<br>    |
| 240|[0x8000c508]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002cf4]:fsgnjn.s t6, t5, t4<br> [0x80002cf8]:csrrs a2, fcsr, zero<br> [0x80002cfc]:sd t6, 1376(fp)<br>    |
| 241|[0x8000c518]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002d34]:fsgnjn.s t6, t5, t4<br> [0x80002d38]:csrrs a2, fcsr, zero<br> [0x80002d3c]:sd t6, 1392(fp)<br>    |
| 242|[0x8000c528]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002d74]:fsgnjn.s t6, t5, t4<br> [0x80002d78]:csrrs a2, fcsr, zero<br> [0x80002d7c]:sd t6, 1408(fp)<br>    |
| 243|[0x8000c538]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002db4]:fsgnjn.s t6, t5, t4<br> [0x80002db8]:csrrs a2, fcsr, zero<br> [0x80002dbc]:sd t6, 1424(fp)<br>    |
| 244|[0x8000c548]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002df4]:fsgnjn.s t6, t5, t4<br> [0x80002df8]:csrrs a2, fcsr, zero<br> [0x80002dfc]:sd t6, 1440(fp)<br>    |
| 245|[0x8000c558]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002e34]:fsgnjn.s t6, t5, t4<br> [0x80002e38]:csrrs a2, fcsr, zero<br> [0x80002e3c]:sd t6, 1456(fp)<br>    |
| 246|[0x8000c568]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002e74]:fsgnjn.s t6, t5, t4<br> [0x80002e78]:csrrs a2, fcsr, zero<br> [0x80002e7c]:sd t6, 1472(fp)<br>    |
| 247|[0x8000c578]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002eb4]:fsgnjn.s t6, t5, t4<br> [0x80002eb8]:csrrs a2, fcsr, zero<br> [0x80002ebc]:sd t6, 1488(fp)<br>    |
| 248|[0x8000c588]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002ef4]:fsgnjn.s t6, t5, t4<br> [0x80002ef8]:csrrs a2, fcsr, zero<br> [0x80002efc]:sd t6, 1504(fp)<br>    |
| 249|[0x8000c598]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002f34]:fsgnjn.s t6, t5, t4<br> [0x80002f38]:csrrs a2, fcsr, zero<br> [0x80002f3c]:sd t6, 1520(fp)<br>    |
| 250|[0x8000c5a8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002f74]:fsgnjn.s t6, t5, t4<br> [0x80002f78]:csrrs a2, fcsr, zero<br> [0x80002f7c]:sd t6, 1536(fp)<br>    |
| 251|[0x8000c5b8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002fb4]:fsgnjn.s t6, t5, t4<br> [0x80002fb8]:csrrs a2, fcsr, zero<br> [0x80002fbc]:sd t6, 1552(fp)<br>    |
| 252|[0x8000c5c8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002ff4]:fsgnjn.s t6, t5, t4<br> [0x80002ff8]:csrrs a2, fcsr, zero<br> [0x80002ffc]:sd t6, 1568(fp)<br>    |
| 253|[0x8000c5d8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003034]:fsgnjn.s t6, t5, t4<br> [0x80003038]:csrrs a2, fcsr, zero<br> [0x8000303c]:sd t6, 1584(fp)<br>    |
| 254|[0x8000c5e8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003074]:fsgnjn.s t6, t5, t4<br> [0x80003078]:csrrs a2, fcsr, zero<br> [0x8000307c]:sd t6, 1600(fp)<br>    |
| 255|[0x8000c5f8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800030b4]:fsgnjn.s t6, t5, t4<br> [0x800030b8]:csrrs a2, fcsr, zero<br> [0x800030bc]:sd t6, 1616(fp)<br>    |
| 256|[0x8000c608]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800030f4]:fsgnjn.s t6, t5, t4<br> [0x800030f8]:csrrs a2, fcsr, zero<br> [0x800030fc]:sd t6, 1632(fp)<br>    |
| 257|[0x8000c618]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003134]:fsgnjn.s t6, t5, t4<br> [0x80003138]:csrrs a2, fcsr, zero<br> [0x8000313c]:sd t6, 1648(fp)<br>    |
| 258|[0x8000c628]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003174]:fsgnjn.s t6, t5, t4<br> [0x80003178]:csrrs a2, fcsr, zero<br> [0x8000317c]:sd t6, 1664(fp)<br>    |
| 259|[0x8000c638]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800031b4]:fsgnjn.s t6, t5, t4<br> [0x800031b8]:csrrs a2, fcsr, zero<br> [0x800031bc]:sd t6, 1680(fp)<br>    |
| 260|[0x8000c648]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800031f4]:fsgnjn.s t6, t5, t4<br> [0x800031f8]:csrrs a2, fcsr, zero<br> [0x800031fc]:sd t6, 1696(fp)<br>    |
| 261|[0x8000c658]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003234]:fsgnjn.s t6, t5, t4<br> [0x80003238]:csrrs a2, fcsr, zero<br> [0x8000323c]:sd t6, 1712(fp)<br>    |
| 262|[0x8000c668]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003274]:fsgnjn.s t6, t5, t4<br> [0x80003278]:csrrs a2, fcsr, zero<br> [0x8000327c]:sd t6, 1728(fp)<br>    |
| 263|[0x8000c678]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800032b4]:fsgnjn.s t6, t5, t4<br> [0x800032b8]:csrrs a2, fcsr, zero<br> [0x800032bc]:sd t6, 1744(fp)<br>    |
| 264|[0x8000c688]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800032f4]:fsgnjn.s t6, t5, t4<br> [0x800032f8]:csrrs a2, fcsr, zero<br> [0x800032fc]:sd t6, 1760(fp)<br>    |
| 265|[0x8000c698]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003334]:fsgnjn.s t6, t5, t4<br> [0x80003338]:csrrs a2, fcsr, zero<br> [0x8000333c]:sd t6, 1776(fp)<br>    |
| 266|[0x8000c6a8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003374]:fsgnjn.s t6, t5, t4<br> [0x80003378]:csrrs a2, fcsr, zero<br> [0x8000337c]:sd t6, 1792(fp)<br>    |
| 267|[0x8000c6b8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800033b4]:fsgnjn.s t6, t5, t4<br> [0x800033b8]:csrrs a2, fcsr, zero<br> [0x800033bc]:sd t6, 1808(fp)<br>    |
| 268|[0x8000c6c8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800033f4]:fsgnjn.s t6, t5, t4<br> [0x800033f8]:csrrs a2, fcsr, zero<br> [0x800033fc]:sd t6, 1824(fp)<br>    |
| 269|[0x8000c6d8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003434]:fsgnjn.s t6, t5, t4<br> [0x80003438]:csrrs a2, fcsr, zero<br> [0x8000343c]:sd t6, 1840(fp)<br>    |
| 270|[0x8000c6e8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003474]:fsgnjn.s t6, t5, t4<br> [0x80003478]:csrrs a2, fcsr, zero<br> [0x8000347c]:sd t6, 1856(fp)<br>    |
| 271|[0x8000c6f8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800034b4]:fsgnjn.s t6, t5, t4<br> [0x800034b8]:csrrs a2, fcsr, zero<br> [0x800034bc]:sd t6, 1872(fp)<br>    |
| 272|[0x8000c708]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800034f4]:fsgnjn.s t6, t5, t4<br> [0x800034f8]:csrrs a2, fcsr, zero<br> [0x800034fc]:sd t6, 1888(fp)<br>    |
| 273|[0x8000c718]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003534]:fsgnjn.s t6, t5, t4<br> [0x80003538]:csrrs a2, fcsr, zero<br> [0x8000353c]:sd t6, 1904(fp)<br>    |
| 274|[0x8000c728]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003574]:fsgnjn.s t6, t5, t4<br> [0x80003578]:csrrs a2, fcsr, zero<br> [0x8000357c]:sd t6, 1920(fp)<br>    |
| 275|[0x8000c738]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800035b4]:fsgnjn.s t6, t5, t4<br> [0x800035b8]:csrrs a2, fcsr, zero<br> [0x800035bc]:sd t6, 1936(fp)<br>    |
| 276|[0x8000c748]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800035f4]:fsgnjn.s t6, t5, t4<br> [0x800035f8]:csrrs a2, fcsr, zero<br> [0x800035fc]:sd t6, 1952(fp)<br>    |
| 277|[0x8000c758]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003634]:fsgnjn.s t6, t5, t4<br> [0x80003638]:csrrs a2, fcsr, zero<br> [0x8000363c]:sd t6, 1968(fp)<br>    |
| 278|[0x8000c768]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003674]:fsgnjn.s t6, t5, t4<br> [0x80003678]:csrrs a2, fcsr, zero<br> [0x8000367c]:sd t6, 1984(fp)<br>    |
| 279|[0x8000c778]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800036ac]:fsgnjn.s t6, t5, t4<br> [0x800036b0]:csrrs a2, fcsr, zero<br> [0x800036b4]:sd t6, 2000(fp)<br>    |
| 280|[0x8000c788]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800036e4]:fsgnjn.s t6, t5, t4<br> [0x800036e8]:csrrs a2, fcsr, zero<br> [0x800036ec]:sd t6, 2016(fp)<br>    |
| 281|[0x8000c798]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000371c]:fsgnjn.s t6, t5, t4<br> [0x80003720]:csrrs a2, fcsr, zero<br> [0x80003724]:sd t6, 2032(fp)<br>    |
| 282|[0x8000c7a8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000375c]:fsgnjn.s t6, t5, t4<br> [0x80003760]:csrrs a2, fcsr, zero<br> [0x80003764]:sd t6, 0(fp)<br>       |
| 283|[0x8000c7b8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003794]:fsgnjn.s t6, t5, t4<br> [0x80003798]:csrrs a2, fcsr, zero<br> [0x8000379c]:sd t6, 16(fp)<br>      |
| 284|[0x8000c7c8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800037cc]:fsgnjn.s t6, t5, t4<br> [0x800037d0]:csrrs a2, fcsr, zero<br> [0x800037d4]:sd t6, 32(fp)<br>      |
| 285|[0x8000c7d8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003804]:fsgnjn.s t6, t5, t4<br> [0x80003808]:csrrs a2, fcsr, zero<br> [0x8000380c]:sd t6, 48(fp)<br>      |
| 286|[0x8000c7e8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000383c]:fsgnjn.s t6, t5, t4<br> [0x80003840]:csrrs a2, fcsr, zero<br> [0x80003844]:sd t6, 64(fp)<br>      |
| 287|[0x8000c7f8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003874]:fsgnjn.s t6, t5, t4<br> [0x80003878]:csrrs a2, fcsr, zero<br> [0x8000387c]:sd t6, 80(fp)<br>      |
| 288|[0x8000c808]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800038ac]:fsgnjn.s t6, t5, t4<br> [0x800038b0]:csrrs a2, fcsr, zero<br> [0x800038b4]:sd t6, 96(fp)<br>      |
| 289|[0x8000c818]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800038e4]:fsgnjn.s t6, t5, t4<br> [0x800038e8]:csrrs a2, fcsr, zero<br> [0x800038ec]:sd t6, 112(fp)<br>     |
| 290|[0x8000c828]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000391c]:fsgnjn.s t6, t5, t4<br> [0x80003920]:csrrs a2, fcsr, zero<br> [0x80003924]:sd t6, 128(fp)<br>     |
| 291|[0x8000c838]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003954]:fsgnjn.s t6, t5, t4<br> [0x80003958]:csrrs a2, fcsr, zero<br> [0x8000395c]:sd t6, 144(fp)<br>     |
| 292|[0x8000c848]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000398c]:fsgnjn.s t6, t5, t4<br> [0x80003990]:csrrs a2, fcsr, zero<br> [0x80003994]:sd t6, 160(fp)<br>     |
| 293|[0x8000c858]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800039c4]:fsgnjn.s t6, t5, t4<br> [0x800039c8]:csrrs a2, fcsr, zero<br> [0x800039cc]:sd t6, 176(fp)<br>     |
| 294|[0x8000c868]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800039fc]:fsgnjn.s t6, t5, t4<br> [0x80003a00]:csrrs a2, fcsr, zero<br> [0x80003a04]:sd t6, 192(fp)<br>     |
| 295|[0x8000c878]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003a34]:fsgnjn.s t6, t5, t4<br> [0x80003a38]:csrrs a2, fcsr, zero<br> [0x80003a3c]:sd t6, 208(fp)<br>     |
| 296|[0x8000c888]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003a6c]:fsgnjn.s t6, t5, t4<br> [0x80003a70]:csrrs a2, fcsr, zero<br> [0x80003a74]:sd t6, 224(fp)<br>     |
| 297|[0x8000c898]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003aa4]:fsgnjn.s t6, t5, t4<br> [0x80003aa8]:csrrs a2, fcsr, zero<br> [0x80003aac]:sd t6, 240(fp)<br>     |
| 298|[0x8000c8a8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003adc]:fsgnjn.s t6, t5, t4<br> [0x80003ae0]:csrrs a2, fcsr, zero<br> [0x80003ae4]:sd t6, 256(fp)<br>     |
| 299|[0x8000c8b8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003b14]:fsgnjn.s t6, t5, t4<br> [0x80003b18]:csrrs a2, fcsr, zero<br> [0x80003b1c]:sd t6, 272(fp)<br>     |
| 300|[0x8000c8c8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003b4c]:fsgnjn.s t6, t5, t4<br> [0x80003b50]:csrrs a2, fcsr, zero<br> [0x80003b54]:sd t6, 288(fp)<br>     |
| 301|[0x8000c8d8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003b84]:fsgnjn.s t6, t5, t4<br> [0x80003b88]:csrrs a2, fcsr, zero<br> [0x80003b8c]:sd t6, 304(fp)<br>     |
| 302|[0x8000c8e8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003bbc]:fsgnjn.s t6, t5, t4<br> [0x80003bc0]:csrrs a2, fcsr, zero<br> [0x80003bc4]:sd t6, 320(fp)<br>     |
| 303|[0x8000c8f8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003bf4]:fsgnjn.s t6, t5, t4<br> [0x80003bf8]:csrrs a2, fcsr, zero<br> [0x80003bfc]:sd t6, 336(fp)<br>     |
| 304|[0x8000c908]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003c2c]:fsgnjn.s t6, t5, t4<br> [0x80003c30]:csrrs a2, fcsr, zero<br> [0x80003c34]:sd t6, 352(fp)<br>     |
| 305|[0x8000c918]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003c64]:fsgnjn.s t6, t5, t4<br> [0x80003c68]:csrrs a2, fcsr, zero<br> [0x80003c6c]:sd t6, 368(fp)<br>     |
| 306|[0x8000c928]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003c9c]:fsgnjn.s t6, t5, t4<br> [0x80003ca0]:csrrs a2, fcsr, zero<br> [0x80003ca4]:sd t6, 384(fp)<br>     |
| 307|[0x8000c938]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003cd4]:fsgnjn.s t6, t5, t4<br> [0x80003cd8]:csrrs a2, fcsr, zero<br> [0x80003cdc]:sd t6, 400(fp)<br>     |
| 308|[0x8000c948]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003d0c]:fsgnjn.s t6, t5, t4<br> [0x80003d10]:csrrs a2, fcsr, zero<br> [0x80003d14]:sd t6, 416(fp)<br>     |
| 309|[0x8000c958]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003d44]:fsgnjn.s t6, t5, t4<br> [0x80003d48]:csrrs a2, fcsr, zero<br> [0x80003d4c]:sd t6, 432(fp)<br>     |
| 310|[0x8000c968]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003d7c]:fsgnjn.s t6, t5, t4<br> [0x80003d80]:csrrs a2, fcsr, zero<br> [0x80003d84]:sd t6, 448(fp)<br>     |
| 311|[0x8000c978]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003db4]:fsgnjn.s t6, t5, t4<br> [0x80003db8]:csrrs a2, fcsr, zero<br> [0x80003dbc]:sd t6, 464(fp)<br>     |
| 312|[0x8000c988]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003dec]:fsgnjn.s t6, t5, t4<br> [0x80003df0]:csrrs a2, fcsr, zero<br> [0x80003df4]:sd t6, 480(fp)<br>     |
| 313|[0x8000c998]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003e24]:fsgnjn.s t6, t5, t4<br> [0x80003e28]:csrrs a2, fcsr, zero<br> [0x80003e2c]:sd t6, 496(fp)<br>     |
| 314|[0x8000c9a8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003e5c]:fsgnjn.s t6, t5, t4<br> [0x80003e60]:csrrs a2, fcsr, zero<br> [0x80003e64]:sd t6, 512(fp)<br>     |
| 315|[0x8000c9b8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003e94]:fsgnjn.s t6, t5, t4<br> [0x80003e98]:csrrs a2, fcsr, zero<br> [0x80003e9c]:sd t6, 528(fp)<br>     |
| 316|[0x8000c9c8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003ecc]:fsgnjn.s t6, t5, t4<br> [0x80003ed0]:csrrs a2, fcsr, zero<br> [0x80003ed4]:sd t6, 544(fp)<br>     |
| 317|[0x8000c9d8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003f04]:fsgnjn.s t6, t5, t4<br> [0x80003f08]:csrrs a2, fcsr, zero<br> [0x80003f0c]:sd t6, 560(fp)<br>     |
| 318|[0x8000c9e8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003f3c]:fsgnjn.s t6, t5, t4<br> [0x80003f40]:csrrs a2, fcsr, zero<br> [0x80003f44]:sd t6, 576(fp)<br>     |
| 319|[0x8000c9f8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003f74]:fsgnjn.s t6, t5, t4<br> [0x80003f78]:csrrs a2, fcsr, zero<br> [0x80003f7c]:sd t6, 592(fp)<br>     |
| 320|[0x8000ca08]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003fac]:fsgnjn.s t6, t5, t4<br> [0x80003fb0]:csrrs a2, fcsr, zero<br> [0x80003fb4]:sd t6, 608(fp)<br>     |
| 321|[0x8000ca18]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003fe4]:fsgnjn.s t6, t5, t4<br> [0x80003fe8]:csrrs a2, fcsr, zero<br> [0x80003fec]:sd t6, 624(fp)<br>     |
| 322|[0x8000ca28]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000401c]:fsgnjn.s t6, t5, t4<br> [0x80004020]:csrrs a2, fcsr, zero<br> [0x80004024]:sd t6, 640(fp)<br>     |
| 323|[0x8000ca38]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004054]:fsgnjn.s t6, t5, t4<br> [0x80004058]:csrrs a2, fcsr, zero<br> [0x8000405c]:sd t6, 656(fp)<br>     |
| 324|[0x8000ca48]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000408c]:fsgnjn.s t6, t5, t4<br> [0x80004090]:csrrs a2, fcsr, zero<br> [0x80004094]:sd t6, 672(fp)<br>     |
| 325|[0x8000ca58]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800040c4]:fsgnjn.s t6, t5, t4<br> [0x800040c8]:csrrs a2, fcsr, zero<br> [0x800040cc]:sd t6, 688(fp)<br>     |
| 326|[0x8000ca68]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800040fc]:fsgnjn.s t6, t5, t4<br> [0x80004100]:csrrs a2, fcsr, zero<br> [0x80004104]:sd t6, 704(fp)<br>     |
| 327|[0x8000ca78]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004134]:fsgnjn.s t6, t5, t4<br> [0x80004138]:csrrs a2, fcsr, zero<br> [0x8000413c]:sd t6, 720(fp)<br>     |
| 328|[0x8000ca88]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000416c]:fsgnjn.s t6, t5, t4<br> [0x80004170]:csrrs a2, fcsr, zero<br> [0x80004174]:sd t6, 736(fp)<br>     |
| 329|[0x8000ca98]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800041a4]:fsgnjn.s t6, t5, t4<br> [0x800041a8]:csrrs a2, fcsr, zero<br> [0x800041ac]:sd t6, 752(fp)<br>     |
| 330|[0x8000caa8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800041dc]:fsgnjn.s t6, t5, t4<br> [0x800041e0]:csrrs a2, fcsr, zero<br> [0x800041e4]:sd t6, 768(fp)<br>     |
| 331|[0x8000cab8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004214]:fsgnjn.s t6, t5, t4<br> [0x80004218]:csrrs a2, fcsr, zero<br> [0x8000421c]:sd t6, 784(fp)<br>     |
| 332|[0x8000cac8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000424c]:fsgnjn.s t6, t5, t4<br> [0x80004250]:csrrs a2, fcsr, zero<br> [0x80004254]:sd t6, 800(fp)<br>     |
| 333|[0x8000cad8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004284]:fsgnjn.s t6, t5, t4<br> [0x80004288]:csrrs a2, fcsr, zero<br> [0x8000428c]:sd t6, 816(fp)<br>     |
| 334|[0x8000cae8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800042bc]:fsgnjn.s t6, t5, t4<br> [0x800042c0]:csrrs a2, fcsr, zero<br> [0x800042c4]:sd t6, 832(fp)<br>     |
| 335|[0x8000caf8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800042f4]:fsgnjn.s t6, t5, t4<br> [0x800042f8]:csrrs a2, fcsr, zero<br> [0x800042fc]:sd t6, 848(fp)<br>     |
| 336|[0x8000cb08]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000432c]:fsgnjn.s t6, t5, t4<br> [0x80004330]:csrrs a2, fcsr, zero<br> [0x80004334]:sd t6, 864(fp)<br>     |
| 337|[0x8000cb18]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004364]:fsgnjn.s t6, t5, t4<br> [0x80004368]:csrrs a2, fcsr, zero<br> [0x8000436c]:sd t6, 880(fp)<br>     |
| 338|[0x8000cb28]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000439c]:fsgnjn.s t6, t5, t4<br> [0x800043a0]:csrrs a2, fcsr, zero<br> [0x800043a4]:sd t6, 896(fp)<br>     |
| 339|[0x8000cb38]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800043d4]:fsgnjn.s t6, t5, t4<br> [0x800043d8]:csrrs a2, fcsr, zero<br> [0x800043dc]:sd t6, 912(fp)<br>     |
| 340|[0x8000cb48]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000440c]:fsgnjn.s t6, t5, t4<br> [0x80004410]:csrrs a2, fcsr, zero<br> [0x80004414]:sd t6, 928(fp)<br>     |
| 341|[0x8000cb58]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004444]:fsgnjn.s t6, t5, t4<br> [0x80004448]:csrrs a2, fcsr, zero<br> [0x8000444c]:sd t6, 944(fp)<br>     |
| 342|[0x8000cb68]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000447c]:fsgnjn.s t6, t5, t4<br> [0x80004480]:csrrs a2, fcsr, zero<br> [0x80004484]:sd t6, 960(fp)<br>     |
| 343|[0x8000cb78]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800044b4]:fsgnjn.s t6, t5, t4<br> [0x800044b8]:csrrs a2, fcsr, zero<br> [0x800044bc]:sd t6, 976(fp)<br>     |
| 344|[0x8000cb88]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800044ec]:fsgnjn.s t6, t5, t4<br> [0x800044f0]:csrrs a2, fcsr, zero<br> [0x800044f4]:sd t6, 992(fp)<br>     |
| 345|[0x8000cb98]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004524]:fsgnjn.s t6, t5, t4<br> [0x80004528]:csrrs a2, fcsr, zero<br> [0x8000452c]:sd t6, 1008(fp)<br>    |
| 346|[0x8000cba8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000455c]:fsgnjn.s t6, t5, t4<br> [0x80004560]:csrrs a2, fcsr, zero<br> [0x80004564]:sd t6, 1024(fp)<br>    |
| 347|[0x8000cbb8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004594]:fsgnjn.s t6, t5, t4<br> [0x80004598]:csrrs a2, fcsr, zero<br> [0x8000459c]:sd t6, 1040(fp)<br>    |
| 348|[0x8000cbc8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800045cc]:fsgnjn.s t6, t5, t4<br> [0x800045d0]:csrrs a2, fcsr, zero<br> [0x800045d4]:sd t6, 1056(fp)<br>    |
| 349|[0x8000cbd8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004604]:fsgnjn.s t6, t5, t4<br> [0x80004608]:csrrs a2, fcsr, zero<br> [0x8000460c]:sd t6, 1072(fp)<br>    |
| 350|[0x8000cbe8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000463c]:fsgnjn.s t6, t5, t4<br> [0x80004640]:csrrs a2, fcsr, zero<br> [0x80004644]:sd t6, 1088(fp)<br>    |
| 351|[0x8000cbf8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004674]:fsgnjn.s t6, t5, t4<br> [0x80004678]:csrrs a2, fcsr, zero<br> [0x8000467c]:sd t6, 1104(fp)<br>    |
| 352|[0x8000cc08]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800046ac]:fsgnjn.s t6, t5, t4<br> [0x800046b0]:csrrs a2, fcsr, zero<br> [0x800046b4]:sd t6, 1120(fp)<br>    |
| 353|[0x8000cc18]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800046e4]:fsgnjn.s t6, t5, t4<br> [0x800046e8]:csrrs a2, fcsr, zero<br> [0x800046ec]:sd t6, 1136(fp)<br>    |
| 354|[0x8000cc28]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000471c]:fsgnjn.s t6, t5, t4<br> [0x80004720]:csrrs a2, fcsr, zero<br> [0x80004724]:sd t6, 1152(fp)<br>    |
| 355|[0x8000cc38]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004754]:fsgnjn.s t6, t5, t4<br> [0x80004758]:csrrs a2, fcsr, zero<br> [0x8000475c]:sd t6, 1168(fp)<br>    |
| 356|[0x8000cc48]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000478c]:fsgnjn.s t6, t5, t4<br> [0x80004790]:csrrs a2, fcsr, zero<br> [0x80004794]:sd t6, 1184(fp)<br>    |
| 357|[0x8000cc58]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800047c4]:fsgnjn.s t6, t5, t4<br> [0x800047c8]:csrrs a2, fcsr, zero<br> [0x800047cc]:sd t6, 1200(fp)<br>    |
| 358|[0x8000cc68]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800047fc]:fsgnjn.s t6, t5, t4<br> [0x80004800]:csrrs a2, fcsr, zero<br> [0x80004804]:sd t6, 1216(fp)<br>    |
| 359|[0x8000cc78]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004834]:fsgnjn.s t6, t5, t4<br> [0x80004838]:csrrs a2, fcsr, zero<br> [0x8000483c]:sd t6, 1232(fp)<br>    |
| 360|[0x8000cc88]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000486c]:fsgnjn.s t6, t5, t4<br> [0x80004870]:csrrs a2, fcsr, zero<br> [0x80004874]:sd t6, 1248(fp)<br>    |
| 361|[0x8000cc98]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800048a4]:fsgnjn.s t6, t5, t4<br> [0x800048a8]:csrrs a2, fcsr, zero<br> [0x800048ac]:sd t6, 1264(fp)<br>    |
| 362|[0x8000cca8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800048dc]:fsgnjn.s t6, t5, t4<br> [0x800048e0]:csrrs a2, fcsr, zero<br> [0x800048e4]:sd t6, 1280(fp)<br>    |
| 363|[0x8000ccb8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004914]:fsgnjn.s t6, t5, t4<br> [0x80004918]:csrrs a2, fcsr, zero<br> [0x8000491c]:sd t6, 1296(fp)<br>    |
| 364|[0x8000ccc8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000494c]:fsgnjn.s t6, t5, t4<br> [0x80004950]:csrrs a2, fcsr, zero<br> [0x80004954]:sd t6, 1312(fp)<br>    |
| 365|[0x8000ccd8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004984]:fsgnjn.s t6, t5, t4<br> [0x80004988]:csrrs a2, fcsr, zero<br> [0x8000498c]:sd t6, 1328(fp)<br>    |
| 366|[0x8000cce8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800049bc]:fsgnjn.s t6, t5, t4<br> [0x800049c0]:csrrs a2, fcsr, zero<br> [0x800049c4]:sd t6, 1344(fp)<br>    |
| 367|[0x8000ccf8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800049f4]:fsgnjn.s t6, t5, t4<br> [0x800049f8]:csrrs a2, fcsr, zero<br> [0x800049fc]:sd t6, 1360(fp)<br>    |
| 368|[0x8000cd08]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004a2c]:fsgnjn.s t6, t5, t4<br> [0x80004a30]:csrrs a2, fcsr, zero<br> [0x80004a34]:sd t6, 1376(fp)<br>    |
| 369|[0x8000cd18]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004a64]:fsgnjn.s t6, t5, t4<br> [0x80004a68]:csrrs a2, fcsr, zero<br> [0x80004a6c]:sd t6, 1392(fp)<br>    |
| 370|[0x8000cd28]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004a9c]:fsgnjn.s t6, t5, t4<br> [0x80004aa0]:csrrs a2, fcsr, zero<br> [0x80004aa4]:sd t6, 1408(fp)<br>    |
| 371|[0x8000cd38]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004ad4]:fsgnjn.s t6, t5, t4<br> [0x80004ad8]:csrrs a2, fcsr, zero<br> [0x80004adc]:sd t6, 1424(fp)<br>    |
| 372|[0x8000cd48]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004b0c]:fsgnjn.s t6, t5, t4<br> [0x80004b10]:csrrs a2, fcsr, zero<br> [0x80004b14]:sd t6, 1440(fp)<br>    |
| 373|[0x8000cd58]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004b44]:fsgnjn.s t6, t5, t4<br> [0x80004b48]:csrrs a2, fcsr, zero<br> [0x80004b4c]:sd t6, 1456(fp)<br>    |
| 374|[0x8000cd68]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004b7c]:fsgnjn.s t6, t5, t4<br> [0x80004b80]:csrrs a2, fcsr, zero<br> [0x80004b84]:sd t6, 1472(fp)<br>    |
| 375|[0x8000cd78]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004bb4]:fsgnjn.s t6, t5, t4<br> [0x80004bb8]:csrrs a2, fcsr, zero<br> [0x80004bbc]:sd t6, 1488(fp)<br>    |
| 376|[0x8000cd88]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004bec]:fsgnjn.s t6, t5, t4<br> [0x80004bf0]:csrrs a2, fcsr, zero<br> [0x80004bf4]:sd t6, 1504(fp)<br>    |
| 377|[0x8000cd98]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004c24]:fsgnjn.s t6, t5, t4<br> [0x80004c28]:csrrs a2, fcsr, zero<br> [0x80004c2c]:sd t6, 1520(fp)<br>    |
| 378|[0x8000cda8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004c5c]:fsgnjn.s t6, t5, t4<br> [0x80004c60]:csrrs a2, fcsr, zero<br> [0x80004c64]:sd t6, 1536(fp)<br>    |
| 379|[0x8000cdb8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004c94]:fsgnjn.s t6, t5, t4<br> [0x80004c98]:csrrs a2, fcsr, zero<br> [0x80004c9c]:sd t6, 1552(fp)<br>    |
| 380|[0x8000cdc8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004ccc]:fsgnjn.s t6, t5, t4<br> [0x80004cd0]:csrrs a2, fcsr, zero<br> [0x80004cd4]:sd t6, 1568(fp)<br>    |
| 381|[0x8000cdd8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004d04]:fsgnjn.s t6, t5, t4<br> [0x80004d08]:csrrs a2, fcsr, zero<br> [0x80004d0c]:sd t6, 1584(fp)<br>    |
| 382|[0x8000cde8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004d3c]:fsgnjn.s t6, t5, t4<br> [0x80004d40]:csrrs a2, fcsr, zero<br> [0x80004d44]:sd t6, 1600(fp)<br>    |
| 383|[0x8000cdf8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004d74]:fsgnjn.s t6, t5, t4<br> [0x80004d78]:csrrs a2, fcsr, zero<br> [0x80004d7c]:sd t6, 1616(fp)<br>    |
| 384|[0x8000ce08]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004dac]:fsgnjn.s t6, t5, t4<br> [0x80004db0]:csrrs a2, fcsr, zero<br> [0x80004db4]:sd t6, 1632(fp)<br>    |
| 385|[0x8000ce18]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004de4]:fsgnjn.s t6, t5, t4<br> [0x80004de8]:csrrs a2, fcsr, zero<br> [0x80004dec]:sd t6, 1648(fp)<br>    |
| 386|[0x8000ce28]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004e1c]:fsgnjn.s t6, t5, t4<br> [0x80004e20]:csrrs a2, fcsr, zero<br> [0x80004e24]:sd t6, 1664(fp)<br>    |
| 387|[0x8000ce38]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004e54]:fsgnjn.s t6, t5, t4<br> [0x80004e58]:csrrs a2, fcsr, zero<br> [0x80004e5c]:sd t6, 1680(fp)<br>    |
| 388|[0x8000ce48]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004e8c]:fsgnjn.s t6, t5, t4<br> [0x80004e90]:csrrs a2, fcsr, zero<br> [0x80004e94]:sd t6, 1696(fp)<br>    |
| 389|[0x8000ce58]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004ec4]:fsgnjn.s t6, t5, t4<br> [0x80004ec8]:csrrs a2, fcsr, zero<br> [0x80004ecc]:sd t6, 1712(fp)<br>    |
| 390|[0x8000ce68]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004efc]:fsgnjn.s t6, t5, t4<br> [0x80004f00]:csrrs a2, fcsr, zero<br> [0x80004f04]:sd t6, 1728(fp)<br>    |
| 391|[0x8000ce78]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004f34]:fsgnjn.s t6, t5, t4<br> [0x80004f38]:csrrs a2, fcsr, zero<br> [0x80004f3c]:sd t6, 1744(fp)<br>    |
| 392|[0x8000ce88]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004f6c]:fsgnjn.s t6, t5, t4<br> [0x80004f70]:csrrs a2, fcsr, zero<br> [0x80004f74]:sd t6, 1760(fp)<br>    |
| 393|[0x8000ce98]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004fa4]:fsgnjn.s t6, t5, t4<br> [0x80004fa8]:csrrs a2, fcsr, zero<br> [0x80004fac]:sd t6, 1776(fp)<br>    |
| 394|[0x8000cea8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004fdc]:fsgnjn.s t6, t5, t4<br> [0x80004fe0]:csrrs a2, fcsr, zero<br> [0x80004fe4]:sd t6, 1792(fp)<br>    |
| 395|[0x8000ceb8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005014]:fsgnjn.s t6, t5, t4<br> [0x80005018]:csrrs a2, fcsr, zero<br> [0x8000501c]:sd t6, 1808(fp)<br>    |
| 396|[0x8000cec8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000504c]:fsgnjn.s t6, t5, t4<br> [0x80005050]:csrrs a2, fcsr, zero<br> [0x80005054]:sd t6, 1824(fp)<br>    |
| 397|[0x8000ced8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005084]:fsgnjn.s t6, t5, t4<br> [0x80005088]:csrrs a2, fcsr, zero<br> [0x8000508c]:sd t6, 1840(fp)<br>    |
| 398|[0x8000cee8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800050bc]:fsgnjn.s t6, t5, t4<br> [0x800050c0]:csrrs a2, fcsr, zero<br> [0x800050c4]:sd t6, 1856(fp)<br>    |
| 399|[0x8000cef8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800050f4]:fsgnjn.s t6, t5, t4<br> [0x800050f8]:csrrs a2, fcsr, zero<br> [0x800050fc]:sd t6, 1872(fp)<br>    |
| 400|[0x8000cf08]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000512c]:fsgnjn.s t6, t5, t4<br> [0x80005130]:csrrs a2, fcsr, zero<br> [0x80005134]:sd t6, 1888(fp)<br>    |
| 401|[0x8000cf18]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005164]:fsgnjn.s t6, t5, t4<br> [0x80005168]:csrrs a2, fcsr, zero<br> [0x8000516c]:sd t6, 1904(fp)<br>    |
| 402|[0x8000cf28]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000519c]:fsgnjn.s t6, t5, t4<br> [0x800051a0]:csrrs a2, fcsr, zero<br> [0x800051a4]:sd t6, 1920(fp)<br>    |
| 403|[0x8000cf38]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800051d4]:fsgnjn.s t6, t5, t4<br> [0x800051d8]:csrrs a2, fcsr, zero<br> [0x800051dc]:sd t6, 1936(fp)<br>    |
| 404|[0x8000cf48]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000520c]:fsgnjn.s t6, t5, t4<br> [0x80005210]:csrrs a2, fcsr, zero<br> [0x80005214]:sd t6, 1952(fp)<br>    |
| 405|[0x8000cf58]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005244]:fsgnjn.s t6, t5, t4<br> [0x80005248]:csrrs a2, fcsr, zero<br> [0x8000524c]:sd t6, 1968(fp)<br>    |
| 406|[0x8000cf68]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000527c]:fsgnjn.s t6, t5, t4<br> [0x80005280]:csrrs a2, fcsr, zero<br> [0x80005284]:sd t6, 1984(fp)<br>    |
| 407|[0x8000cf78]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800052bc]:fsgnjn.s t6, t5, t4<br> [0x800052c0]:csrrs a2, fcsr, zero<br> [0x800052c4]:sd t6, 2000(fp)<br>    |
| 408|[0x8000cf88]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800052fc]:fsgnjn.s t6, t5, t4<br> [0x80005300]:csrrs a2, fcsr, zero<br> [0x80005304]:sd t6, 2016(fp)<br>    |
| 409|[0x8000cf98]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000533c]:fsgnjn.s t6, t5, t4<br> [0x80005340]:csrrs a2, fcsr, zero<br> [0x80005344]:sd t6, 2032(fp)<br>    |
| 410|[0x8000cfa8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005384]:fsgnjn.s t6, t5, t4<br> [0x80005388]:csrrs a2, fcsr, zero<br> [0x8000538c]:sd t6, 0(fp)<br>       |
| 411|[0x8000cfb8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800053c4]:fsgnjn.s t6, t5, t4<br> [0x800053c8]:csrrs a2, fcsr, zero<br> [0x800053cc]:sd t6, 16(fp)<br>      |
| 412|[0x8000cfc8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005404]:fsgnjn.s t6, t5, t4<br> [0x80005408]:csrrs a2, fcsr, zero<br> [0x8000540c]:sd t6, 32(fp)<br>      |
| 413|[0x8000cfd8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005444]:fsgnjn.s t6, t5, t4<br> [0x80005448]:csrrs a2, fcsr, zero<br> [0x8000544c]:sd t6, 48(fp)<br>      |
| 414|[0x8000cfe8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005484]:fsgnjn.s t6, t5, t4<br> [0x80005488]:csrrs a2, fcsr, zero<br> [0x8000548c]:sd t6, 64(fp)<br>      |
| 415|[0x8000cff8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800054c4]:fsgnjn.s t6, t5, t4<br> [0x800054c8]:csrrs a2, fcsr, zero<br> [0x800054cc]:sd t6, 80(fp)<br>      |
| 416|[0x8000d008]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005504]:fsgnjn.s t6, t5, t4<br> [0x80005508]:csrrs a2, fcsr, zero<br> [0x8000550c]:sd t6, 96(fp)<br>      |
| 417|[0x8000d018]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005544]:fsgnjn.s t6, t5, t4<br> [0x80005548]:csrrs a2, fcsr, zero<br> [0x8000554c]:sd t6, 112(fp)<br>     |
| 418|[0x8000d028]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005584]:fsgnjn.s t6, t5, t4<br> [0x80005588]:csrrs a2, fcsr, zero<br> [0x8000558c]:sd t6, 128(fp)<br>     |
| 419|[0x8000d038]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800055c4]:fsgnjn.s t6, t5, t4<br> [0x800055c8]:csrrs a2, fcsr, zero<br> [0x800055cc]:sd t6, 144(fp)<br>     |
| 420|[0x8000d048]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005604]:fsgnjn.s t6, t5, t4<br> [0x80005608]:csrrs a2, fcsr, zero<br> [0x8000560c]:sd t6, 160(fp)<br>     |
| 421|[0x8000d058]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005644]:fsgnjn.s t6, t5, t4<br> [0x80005648]:csrrs a2, fcsr, zero<br> [0x8000564c]:sd t6, 176(fp)<br>     |
| 422|[0x8000d068]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005684]:fsgnjn.s t6, t5, t4<br> [0x80005688]:csrrs a2, fcsr, zero<br> [0x8000568c]:sd t6, 192(fp)<br>     |
| 423|[0x8000d078]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800056c4]:fsgnjn.s t6, t5, t4<br> [0x800056c8]:csrrs a2, fcsr, zero<br> [0x800056cc]:sd t6, 208(fp)<br>     |
| 424|[0x8000d088]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005704]:fsgnjn.s t6, t5, t4<br> [0x80005708]:csrrs a2, fcsr, zero<br> [0x8000570c]:sd t6, 224(fp)<br>     |
| 425|[0x8000d098]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005744]:fsgnjn.s t6, t5, t4<br> [0x80005748]:csrrs a2, fcsr, zero<br> [0x8000574c]:sd t6, 240(fp)<br>     |
| 426|[0x8000d0a8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005784]:fsgnjn.s t6, t5, t4<br> [0x80005788]:csrrs a2, fcsr, zero<br> [0x8000578c]:sd t6, 256(fp)<br>     |
| 427|[0x8000d0b8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800057c4]:fsgnjn.s t6, t5, t4<br> [0x800057c8]:csrrs a2, fcsr, zero<br> [0x800057cc]:sd t6, 272(fp)<br>     |
| 428|[0x8000d0c8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005804]:fsgnjn.s t6, t5, t4<br> [0x80005808]:csrrs a2, fcsr, zero<br> [0x8000580c]:sd t6, 288(fp)<br>     |
| 429|[0x8000d0d8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005844]:fsgnjn.s t6, t5, t4<br> [0x80005848]:csrrs a2, fcsr, zero<br> [0x8000584c]:sd t6, 304(fp)<br>     |
| 430|[0x8000d0e8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005884]:fsgnjn.s t6, t5, t4<br> [0x80005888]:csrrs a2, fcsr, zero<br> [0x8000588c]:sd t6, 320(fp)<br>     |
| 431|[0x8000d0f8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800058c4]:fsgnjn.s t6, t5, t4<br> [0x800058c8]:csrrs a2, fcsr, zero<br> [0x800058cc]:sd t6, 336(fp)<br>     |
| 432|[0x8000d108]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005904]:fsgnjn.s t6, t5, t4<br> [0x80005908]:csrrs a2, fcsr, zero<br> [0x8000590c]:sd t6, 352(fp)<br>     |
| 433|[0x8000d118]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005944]:fsgnjn.s t6, t5, t4<br> [0x80005948]:csrrs a2, fcsr, zero<br> [0x8000594c]:sd t6, 368(fp)<br>     |
| 434|[0x8000d128]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005984]:fsgnjn.s t6, t5, t4<br> [0x80005988]:csrrs a2, fcsr, zero<br> [0x8000598c]:sd t6, 384(fp)<br>     |
| 435|[0x8000d138]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800059c4]:fsgnjn.s t6, t5, t4<br> [0x800059c8]:csrrs a2, fcsr, zero<br> [0x800059cc]:sd t6, 400(fp)<br>     |
| 436|[0x8000d148]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005a04]:fsgnjn.s t6, t5, t4<br> [0x80005a08]:csrrs a2, fcsr, zero<br> [0x80005a0c]:sd t6, 416(fp)<br>     |
| 437|[0x8000d158]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005a44]:fsgnjn.s t6, t5, t4<br> [0x80005a48]:csrrs a2, fcsr, zero<br> [0x80005a4c]:sd t6, 432(fp)<br>     |
| 438|[0x8000d168]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005a84]:fsgnjn.s t6, t5, t4<br> [0x80005a88]:csrrs a2, fcsr, zero<br> [0x80005a8c]:sd t6, 448(fp)<br>     |
| 439|[0x8000d178]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005ac4]:fsgnjn.s t6, t5, t4<br> [0x80005ac8]:csrrs a2, fcsr, zero<br> [0x80005acc]:sd t6, 464(fp)<br>     |
| 440|[0x8000d188]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005b04]:fsgnjn.s t6, t5, t4<br> [0x80005b08]:csrrs a2, fcsr, zero<br> [0x80005b0c]:sd t6, 480(fp)<br>     |
| 441|[0x8000d198]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005b44]:fsgnjn.s t6, t5, t4<br> [0x80005b48]:csrrs a2, fcsr, zero<br> [0x80005b4c]:sd t6, 496(fp)<br>     |
| 442|[0x8000d1a8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005b84]:fsgnjn.s t6, t5, t4<br> [0x80005b88]:csrrs a2, fcsr, zero<br> [0x80005b8c]:sd t6, 512(fp)<br>     |
| 443|[0x8000d1b8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005bc4]:fsgnjn.s t6, t5, t4<br> [0x80005bc8]:csrrs a2, fcsr, zero<br> [0x80005bcc]:sd t6, 528(fp)<br>     |
| 444|[0x8000d1c8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005c04]:fsgnjn.s t6, t5, t4<br> [0x80005c08]:csrrs a2, fcsr, zero<br> [0x80005c0c]:sd t6, 544(fp)<br>     |
| 445|[0x8000d1d8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005c44]:fsgnjn.s t6, t5, t4<br> [0x80005c48]:csrrs a2, fcsr, zero<br> [0x80005c4c]:sd t6, 560(fp)<br>     |
| 446|[0x8000d1e8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005c84]:fsgnjn.s t6, t5, t4<br> [0x80005c88]:csrrs a2, fcsr, zero<br> [0x80005c8c]:sd t6, 576(fp)<br>     |
| 447|[0x8000d1f8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005cc4]:fsgnjn.s t6, t5, t4<br> [0x80005cc8]:csrrs a2, fcsr, zero<br> [0x80005ccc]:sd t6, 592(fp)<br>     |
| 448|[0x8000d208]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005d04]:fsgnjn.s t6, t5, t4<br> [0x80005d08]:csrrs a2, fcsr, zero<br> [0x80005d0c]:sd t6, 608(fp)<br>     |
| 449|[0x8000d218]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005d44]:fsgnjn.s t6, t5, t4<br> [0x80005d48]:csrrs a2, fcsr, zero<br> [0x80005d4c]:sd t6, 624(fp)<br>     |
| 450|[0x8000d228]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005d84]:fsgnjn.s t6, t5, t4<br> [0x80005d88]:csrrs a2, fcsr, zero<br> [0x80005d8c]:sd t6, 640(fp)<br>     |
| 451|[0x8000d238]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005dc4]:fsgnjn.s t6, t5, t4<br> [0x80005dc8]:csrrs a2, fcsr, zero<br> [0x80005dcc]:sd t6, 656(fp)<br>     |
| 452|[0x8000d248]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005e04]:fsgnjn.s t6, t5, t4<br> [0x80005e08]:csrrs a2, fcsr, zero<br> [0x80005e0c]:sd t6, 672(fp)<br>     |
| 453|[0x8000d258]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005e44]:fsgnjn.s t6, t5, t4<br> [0x80005e48]:csrrs a2, fcsr, zero<br> [0x80005e4c]:sd t6, 688(fp)<br>     |
| 454|[0x8000d268]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005e84]:fsgnjn.s t6, t5, t4<br> [0x80005e88]:csrrs a2, fcsr, zero<br> [0x80005e8c]:sd t6, 704(fp)<br>     |
| 455|[0x8000d278]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005ec4]:fsgnjn.s t6, t5, t4<br> [0x80005ec8]:csrrs a2, fcsr, zero<br> [0x80005ecc]:sd t6, 720(fp)<br>     |
| 456|[0x8000d288]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005f04]:fsgnjn.s t6, t5, t4<br> [0x80005f08]:csrrs a2, fcsr, zero<br> [0x80005f0c]:sd t6, 736(fp)<br>     |
| 457|[0x8000d298]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005f44]:fsgnjn.s t6, t5, t4<br> [0x80005f48]:csrrs a2, fcsr, zero<br> [0x80005f4c]:sd t6, 752(fp)<br>     |
| 458|[0x8000d2a8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005f84]:fsgnjn.s t6, t5, t4<br> [0x80005f88]:csrrs a2, fcsr, zero<br> [0x80005f8c]:sd t6, 768(fp)<br>     |
| 459|[0x8000d2b8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005fc4]:fsgnjn.s t6, t5, t4<br> [0x80005fc8]:csrrs a2, fcsr, zero<br> [0x80005fcc]:sd t6, 784(fp)<br>     |
| 460|[0x8000d2c8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006004]:fsgnjn.s t6, t5, t4<br> [0x80006008]:csrrs a2, fcsr, zero<br> [0x8000600c]:sd t6, 800(fp)<br>     |
| 461|[0x8000d2d8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006044]:fsgnjn.s t6, t5, t4<br> [0x80006048]:csrrs a2, fcsr, zero<br> [0x8000604c]:sd t6, 816(fp)<br>     |
| 462|[0x8000d2e8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006084]:fsgnjn.s t6, t5, t4<br> [0x80006088]:csrrs a2, fcsr, zero<br> [0x8000608c]:sd t6, 832(fp)<br>     |
| 463|[0x8000d2f8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800060c4]:fsgnjn.s t6, t5, t4<br> [0x800060c8]:csrrs a2, fcsr, zero<br> [0x800060cc]:sd t6, 848(fp)<br>     |
| 464|[0x8000d308]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006104]:fsgnjn.s t6, t5, t4<br> [0x80006108]:csrrs a2, fcsr, zero<br> [0x8000610c]:sd t6, 864(fp)<br>     |
| 465|[0x8000d318]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006144]:fsgnjn.s t6, t5, t4<br> [0x80006148]:csrrs a2, fcsr, zero<br> [0x8000614c]:sd t6, 880(fp)<br>     |
| 466|[0x8000d328]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006184]:fsgnjn.s t6, t5, t4<br> [0x80006188]:csrrs a2, fcsr, zero<br> [0x8000618c]:sd t6, 896(fp)<br>     |
| 467|[0x8000d338]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800061c4]:fsgnjn.s t6, t5, t4<br> [0x800061c8]:csrrs a2, fcsr, zero<br> [0x800061cc]:sd t6, 912(fp)<br>     |
| 468|[0x8000d348]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006204]:fsgnjn.s t6, t5, t4<br> [0x80006208]:csrrs a2, fcsr, zero<br> [0x8000620c]:sd t6, 928(fp)<br>     |
| 469|[0x8000d358]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006244]:fsgnjn.s t6, t5, t4<br> [0x80006248]:csrrs a2, fcsr, zero<br> [0x8000624c]:sd t6, 944(fp)<br>     |
| 470|[0x8000d368]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006284]:fsgnjn.s t6, t5, t4<br> [0x80006288]:csrrs a2, fcsr, zero<br> [0x8000628c]:sd t6, 960(fp)<br>     |
| 471|[0x8000d378]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800062c4]:fsgnjn.s t6, t5, t4<br> [0x800062c8]:csrrs a2, fcsr, zero<br> [0x800062cc]:sd t6, 976(fp)<br>     |
| 472|[0x8000d388]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006304]:fsgnjn.s t6, t5, t4<br> [0x80006308]:csrrs a2, fcsr, zero<br> [0x8000630c]:sd t6, 992(fp)<br>     |
| 473|[0x8000d398]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006344]:fsgnjn.s t6, t5, t4<br> [0x80006348]:csrrs a2, fcsr, zero<br> [0x8000634c]:sd t6, 1008(fp)<br>    |
| 474|[0x8000d3a8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006384]:fsgnjn.s t6, t5, t4<br> [0x80006388]:csrrs a2, fcsr, zero<br> [0x8000638c]:sd t6, 1024(fp)<br>    |
| 475|[0x8000d3b8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800063c4]:fsgnjn.s t6, t5, t4<br> [0x800063c8]:csrrs a2, fcsr, zero<br> [0x800063cc]:sd t6, 1040(fp)<br>    |
| 476|[0x8000d3c8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006404]:fsgnjn.s t6, t5, t4<br> [0x80006408]:csrrs a2, fcsr, zero<br> [0x8000640c]:sd t6, 1056(fp)<br>    |
| 477|[0x8000d3d8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006444]:fsgnjn.s t6, t5, t4<br> [0x80006448]:csrrs a2, fcsr, zero<br> [0x8000644c]:sd t6, 1072(fp)<br>    |
| 478|[0x8000d3e8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006484]:fsgnjn.s t6, t5, t4<br> [0x80006488]:csrrs a2, fcsr, zero<br> [0x8000648c]:sd t6, 1088(fp)<br>    |
| 479|[0x8000d3f8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800064c4]:fsgnjn.s t6, t5, t4<br> [0x800064c8]:csrrs a2, fcsr, zero<br> [0x800064cc]:sd t6, 1104(fp)<br>    |
| 480|[0x8000d408]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006504]:fsgnjn.s t6, t5, t4<br> [0x80006508]:csrrs a2, fcsr, zero<br> [0x8000650c]:sd t6, 1120(fp)<br>    |
| 481|[0x8000d418]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006544]:fsgnjn.s t6, t5, t4<br> [0x80006548]:csrrs a2, fcsr, zero<br> [0x8000654c]:sd t6, 1136(fp)<br>    |
| 482|[0x8000d428]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006584]:fsgnjn.s t6, t5, t4<br> [0x80006588]:csrrs a2, fcsr, zero<br> [0x8000658c]:sd t6, 1152(fp)<br>    |
| 483|[0x8000d438]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800065c4]:fsgnjn.s t6, t5, t4<br> [0x800065c8]:csrrs a2, fcsr, zero<br> [0x800065cc]:sd t6, 1168(fp)<br>    |
| 484|[0x8000d448]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006604]:fsgnjn.s t6, t5, t4<br> [0x80006608]:csrrs a2, fcsr, zero<br> [0x8000660c]:sd t6, 1184(fp)<br>    |
| 485|[0x8000d458]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006644]:fsgnjn.s t6, t5, t4<br> [0x80006648]:csrrs a2, fcsr, zero<br> [0x8000664c]:sd t6, 1200(fp)<br>    |
| 486|[0x8000d468]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006684]:fsgnjn.s t6, t5, t4<br> [0x80006688]:csrrs a2, fcsr, zero<br> [0x8000668c]:sd t6, 1216(fp)<br>    |
| 487|[0x8000d478]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800066c4]:fsgnjn.s t6, t5, t4<br> [0x800066c8]:csrrs a2, fcsr, zero<br> [0x800066cc]:sd t6, 1232(fp)<br>    |
| 488|[0x8000d488]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006704]:fsgnjn.s t6, t5, t4<br> [0x80006708]:csrrs a2, fcsr, zero<br> [0x8000670c]:sd t6, 1248(fp)<br>    |
| 489|[0x8000d498]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006744]:fsgnjn.s t6, t5, t4<br> [0x80006748]:csrrs a2, fcsr, zero<br> [0x8000674c]:sd t6, 1264(fp)<br>    |
| 490|[0x8000d4a8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006784]:fsgnjn.s t6, t5, t4<br> [0x80006788]:csrrs a2, fcsr, zero<br> [0x8000678c]:sd t6, 1280(fp)<br>    |
| 491|[0x8000d4b8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800067c4]:fsgnjn.s t6, t5, t4<br> [0x800067c8]:csrrs a2, fcsr, zero<br> [0x800067cc]:sd t6, 1296(fp)<br>    |
| 492|[0x8000d4c8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006804]:fsgnjn.s t6, t5, t4<br> [0x80006808]:csrrs a2, fcsr, zero<br> [0x8000680c]:sd t6, 1312(fp)<br>    |
| 493|[0x8000d4d8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006844]:fsgnjn.s t6, t5, t4<br> [0x80006848]:csrrs a2, fcsr, zero<br> [0x8000684c]:sd t6, 1328(fp)<br>    |
| 494|[0x8000d4e8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006884]:fsgnjn.s t6, t5, t4<br> [0x80006888]:csrrs a2, fcsr, zero<br> [0x8000688c]:sd t6, 1344(fp)<br>    |
| 495|[0x8000d4f8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800068c4]:fsgnjn.s t6, t5, t4<br> [0x800068c8]:csrrs a2, fcsr, zero<br> [0x800068cc]:sd t6, 1360(fp)<br>    |
| 496|[0x8000d508]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006904]:fsgnjn.s t6, t5, t4<br> [0x80006908]:csrrs a2, fcsr, zero<br> [0x8000690c]:sd t6, 1376(fp)<br>    |
| 497|[0x8000d518]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006944]:fsgnjn.s t6, t5, t4<br> [0x80006948]:csrrs a2, fcsr, zero<br> [0x8000694c]:sd t6, 1392(fp)<br>    |
| 498|[0x8000d528]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006984]:fsgnjn.s t6, t5, t4<br> [0x80006988]:csrrs a2, fcsr, zero<br> [0x8000698c]:sd t6, 1408(fp)<br>    |
| 499|[0x8000d538]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800069c4]:fsgnjn.s t6, t5, t4<br> [0x800069c8]:csrrs a2, fcsr, zero<br> [0x800069cc]:sd t6, 1424(fp)<br>    |
| 500|[0x8000d548]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006a04]:fsgnjn.s t6, t5, t4<br> [0x80006a08]:csrrs a2, fcsr, zero<br> [0x80006a0c]:sd t6, 1440(fp)<br>    |
| 501|[0x8000d558]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006a44]:fsgnjn.s t6, t5, t4<br> [0x80006a48]:csrrs a2, fcsr, zero<br> [0x80006a4c]:sd t6, 1456(fp)<br>    |
| 502|[0x8000d568]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006a84]:fsgnjn.s t6, t5, t4<br> [0x80006a88]:csrrs a2, fcsr, zero<br> [0x80006a8c]:sd t6, 1472(fp)<br>    |
| 503|[0x8000d578]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006ac4]:fsgnjn.s t6, t5, t4<br> [0x80006ac8]:csrrs a2, fcsr, zero<br> [0x80006acc]:sd t6, 1488(fp)<br>    |
| 504|[0x8000d588]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006b04]:fsgnjn.s t6, t5, t4<br> [0x80006b08]:csrrs a2, fcsr, zero<br> [0x80006b0c]:sd t6, 1504(fp)<br>    |
| 505|[0x8000d598]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006b44]:fsgnjn.s t6, t5, t4<br> [0x80006b48]:csrrs a2, fcsr, zero<br> [0x80006b4c]:sd t6, 1520(fp)<br>    |
| 506|[0x8000d5a8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006b84]:fsgnjn.s t6, t5, t4<br> [0x80006b88]:csrrs a2, fcsr, zero<br> [0x80006b8c]:sd t6, 1536(fp)<br>    |
| 507|[0x8000d5b8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006bc4]:fsgnjn.s t6, t5, t4<br> [0x80006bc8]:csrrs a2, fcsr, zero<br> [0x80006bcc]:sd t6, 1552(fp)<br>    |
| 508|[0x8000d5c8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006c04]:fsgnjn.s t6, t5, t4<br> [0x80006c08]:csrrs a2, fcsr, zero<br> [0x80006c0c]:sd t6, 1568(fp)<br>    |
| 509|[0x8000d5d8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006c44]:fsgnjn.s t6, t5, t4<br> [0x80006c48]:csrrs a2, fcsr, zero<br> [0x80006c4c]:sd t6, 1584(fp)<br>    |
| 510|[0x8000d5e8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006c84]:fsgnjn.s t6, t5, t4<br> [0x80006c88]:csrrs a2, fcsr, zero<br> [0x80006c8c]:sd t6, 1600(fp)<br>    |
| 511|[0x8000d5f8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006cc4]:fsgnjn.s t6, t5, t4<br> [0x80006cc8]:csrrs a2, fcsr, zero<br> [0x80006ccc]:sd t6, 1616(fp)<br>    |
| 512|[0x8000d608]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006d04]:fsgnjn.s t6, t5, t4<br> [0x80006d08]:csrrs a2, fcsr, zero<br> [0x80006d0c]:sd t6, 1632(fp)<br>    |
| 513|[0x8000d618]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006d44]:fsgnjn.s t6, t5, t4<br> [0x80006d48]:csrrs a2, fcsr, zero<br> [0x80006d4c]:sd t6, 1648(fp)<br>    |
| 514|[0x8000d628]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006d84]:fsgnjn.s t6, t5, t4<br> [0x80006d88]:csrrs a2, fcsr, zero<br> [0x80006d8c]:sd t6, 1664(fp)<br>    |
| 515|[0x8000d638]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006dc4]:fsgnjn.s t6, t5, t4<br> [0x80006dc8]:csrrs a2, fcsr, zero<br> [0x80006dcc]:sd t6, 1680(fp)<br>    |
| 516|[0x8000d648]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006e04]:fsgnjn.s t6, t5, t4<br> [0x80006e08]:csrrs a2, fcsr, zero<br> [0x80006e0c]:sd t6, 1696(fp)<br>    |
| 517|[0x8000d658]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006e44]:fsgnjn.s t6, t5, t4<br> [0x80006e48]:csrrs a2, fcsr, zero<br> [0x80006e4c]:sd t6, 1712(fp)<br>    |
| 518|[0x8000d668]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006e84]:fsgnjn.s t6, t5, t4<br> [0x80006e88]:csrrs a2, fcsr, zero<br> [0x80006e8c]:sd t6, 1728(fp)<br>    |
| 519|[0x8000d678]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006ec4]:fsgnjn.s t6, t5, t4<br> [0x80006ec8]:csrrs a2, fcsr, zero<br> [0x80006ecc]:sd t6, 1744(fp)<br>    |
| 520|[0x8000d688]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006f04]:fsgnjn.s t6, t5, t4<br> [0x80006f08]:csrrs a2, fcsr, zero<br> [0x80006f0c]:sd t6, 1760(fp)<br>    |
| 521|[0x8000d698]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006f44]:fsgnjn.s t6, t5, t4<br> [0x80006f48]:csrrs a2, fcsr, zero<br> [0x80006f4c]:sd t6, 1776(fp)<br>    |
| 522|[0x8000d6a8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006f84]:fsgnjn.s t6, t5, t4<br> [0x80006f88]:csrrs a2, fcsr, zero<br> [0x80006f8c]:sd t6, 1792(fp)<br>    |
| 523|[0x8000d6b8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006fc4]:fsgnjn.s t6, t5, t4<br> [0x80006fc8]:csrrs a2, fcsr, zero<br> [0x80006fcc]:sd t6, 1808(fp)<br>    |
| 524|[0x8000d6c8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007004]:fsgnjn.s t6, t5, t4<br> [0x80007008]:csrrs a2, fcsr, zero<br> [0x8000700c]:sd t6, 1824(fp)<br>    |
| 525|[0x8000d6d8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007044]:fsgnjn.s t6, t5, t4<br> [0x80007048]:csrrs a2, fcsr, zero<br> [0x8000704c]:sd t6, 1840(fp)<br>    |
| 526|[0x8000d6e8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007084]:fsgnjn.s t6, t5, t4<br> [0x80007088]:csrrs a2, fcsr, zero<br> [0x8000708c]:sd t6, 1856(fp)<br>    |
| 527|[0x8000d6f8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800070c4]:fsgnjn.s t6, t5, t4<br> [0x800070c8]:csrrs a2, fcsr, zero<br> [0x800070cc]:sd t6, 1872(fp)<br>    |
| 528|[0x8000d708]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007104]:fsgnjn.s t6, t5, t4<br> [0x80007108]:csrrs a2, fcsr, zero<br> [0x8000710c]:sd t6, 1888(fp)<br>    |
| 529|[0x8000d718]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007144]:fsgnjn.s t6, t5, t4<br> [0x80007148]:csrrs a2, fcsr, zero<br> [0x8000714c]:sd t6, 1904(fp)<br>    |
| 530|[0x8000d728]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007184]:fsgnjn.s t6, t5, t4<br> [0x80007188]:csrrs a2, fcsr, zero<br> [0x8000718c]:sd t6, 1920(fp)<br>    |
| 531|[0x8000d738]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800071c4]:fsgnjn.s t6, t5, t4<br> [0x800071c8]:csrrs a2, fcsr, zero<br> [0x800071cc]:sd t6, 1936(fp)<br>    |
| 532|[0x8000d748]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007204]:fsgnjn.s t6, t5, t4<br> [0x80007208]:csrrs a2, fcsr, zero<br> [0x8000720c]:sd t6, 1952(fp)<br>    |
| 533|[0x8000d758]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007244]:fsgnjn.s t6, t5, t4<br> [0x80007248]:csrrs a2, fcsr, zero<br> [0x8000724c]:sd t6, 1968(fp)<br>    |
| 534|[0x8000d768]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007284]:fsgnjn.s t6, t5, t4<br> [0x80007288]:csrrs a2, fcsr, zero<br> [0x8000728c]:sd t6, 1984(fp)<br>    |
| 535|[0x8000d778]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800072bc]:fsgnjn.s t6, t5, t4<br> [0x800072c0]:csrrs a2, fcsr, zero<br> [0x800072c4]:sd t6, 2000(fp)<br>    |
| 536|[0x8000d788]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800072f4]:fsgnjn.s t6, t5, t4<br> [0x800072f8]:csrrs a2, fcsr, zero<br> [0x800072fc]:sd t6, 2016(fp)<br>    |
| 537|[0x8000d798]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000732c]:fsgnjn.s t6, t5, t4<br> [0x80007330]:csrrs a2, fcsr, zero<br> [0x80007334]:sd t6, 2032(fp)<br>    |
| 538|[0x8000d7a8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000736c]:fsgnjn.s t6, t5, t4<br> [0x80007370]:csrrs a2, fcsr, zero<br> [0x80007374]:sd t6, 0(fp)<br>       |
| 539|[0x8000d7b8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800073a4]:fsgnjn.s t6, t5, t4<br> [0x800073a8]:csrrs a2, fcsr, zero<br> [0x800073ac]:sd t6, 16(fp)<br>      |
| 540|[0x8000d7c8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800073dc]:fsgnjn.s t6, t5, t4<br> [0x800073e0]:csrrs a2, fcsr, zero<br> [0x800073e4]:sd t6, 32(fp)<br>      |
| 541|[0x8000d7d8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007414]:fsgnjn.s t6, t5, t4<br> [0x80007418]:csrrs a2, fcsr, zero<br> [0x8000741c]:sd t6, 48(fp)<br>      |
| 542|[0x8000d7e8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000744c]:fsgnjn.s t6, t5, t4<br> [0x80007450]:csrrs a2, fcsr, zero<br> [0x80007454]:sd t6, 64(fp)<br>      |
| 543|[0x8000d7f8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007484]:fsgnjn.s t6, t5, t4<br> [0x80007488]:csrrs a2, fcsr, zero<br> [0x8000748c]:sd t6, 80(fp)<br>      |
| 544|[0x8000d808]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800074bc]:fsgnjn.s t6, t5, t4<br> [0x800074c0]:csrrs a2, fcsr, zero<br> [0x800074c4]:sd t6, 96(fp)<br>      |
| 545|[0x8000d818]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800074f4]:fsgnjn.s t6, t5, t4<br> [0x800074f8]:csrrs a2, fcsr, zero<br> [0x800074fc]:sd t6, 112(fp)<br>     |
| 546|[0x8000d828]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000752c]:fsgnjn.s t6, t5, t4<br> [0x80007530]:csrrs a2, fcsr, zero<br> [0x80007534]:sd t6, 128(fp)<br>     |
| 547|[0x8000d838]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007564]:fsgnjn.s t6, t5, t4<br> [0x80007568]:csrrs a2, fcsr, zero<br> [0x8000756c]:sd t6, 144(fp)<br>     |
| 548|[0x8000d848]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000759c]:fsgnjn.s t6, t5, t4<br> [0x800075a0]:csrrs a2, fcsr, zero<br> [0x800075a4]:sd t6, 160(fp)<br>     |
| 549|[0x8000d858]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800075d4]:fsgnjn.s t6, t5, t4<br> [0x800075d8]:csrrs a2, fcsr, zero<br> [0x800075dc]:sd t6, 176(fp)<br>     |
| 550|[0x8000d868]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000760c]:fsgnjn.s t6, t5, t4<br> [0x80007610]:csrrs a2, fcsr, zero<br> [0x80007614]:sd t6, 192(fp)<br>     |
| 551|[0x8000d878]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007644]:fsgnjn.s t6, t5, t4<br> [0x80007648]:csrrs a2, fcsr, zero<br> [0x8000764c]:sd t6, 208(fp)<br>     |
| 552|[0x8000d888]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000767c]:fsgnjn.s t6, t5, t4<br> [0x80007680]:csrrs a2, fcsr, zero<br> [0x80007684]:sd t6, 224(fp)<br>     |
| 553|[0x8000d898]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800076b4]:fsgnjn.s t6, t5, t4<br> [0x800076b8]:csrrs a2, fcsr, zero<br> [0x800076bc]:sd t6, 240(fp)<br>     |
| 554|[0x8000d8a8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800076ec]:fsgnjn.s t6, t5, t4<br> [0x800076f0]:csrrs a2, fcsr, zero<br> [0x800076f4]:sd t6, 256(fp)<br>     |
| 555|[0x8000d8b8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007724]:fsgnjn.s t6, t5, t4<br> [0x80007728]:csrrs a2, fcsr, zero<br> [0x8000772c]:sd t6, 272(fp)<br>     |
| 556|[0x8000d8c8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000775c]:fsgnjn.s t6, t5, t4<br> [0x80007760]:csrrs a2, fcsr, zero<br> [0x80007764]:sd t6, 288(fp)<br>     |
| 557|[0x8000d8d8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007794]:fsgnjn.s t6, t5, t4<br> [0x80007798]:csrrs a2, fcsr, zero<br> [0x8000779c]:sd t6, 304(fp)<br>     |
| 558|[0x8000d8e8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800077cc]:fsgnjn.s t6, t5, t4<br> [0x800077d0]:csrrs a2, fcsr, zero<br> [0x800077d4]:sd t6, 320(fp)<br>     |
| 559|[0x8000d8f8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007804]:fsgnjn.s t6, t5, t4<br> [0x80007808]:csrrs a2, fcsr, zero<br> [0x8000780c]:sd t6, 336(fp)<br>     |
| 560|[0x8000d908]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000783c]:fsgnjn.s t6, t5, t4<br> [0x80007840]:csrrs a2, fcsr, zero<br> [0x80007844]:sd t6, 352(fp)<br>     |
| 561|[0x8000d918]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007874]:fsgnjn.s t6, t5, t4<br> [0x80007878]:csrrs a2, fcsr, zero<br> [0x8000787c]:sd t6, 368(fp)<br>     |
| 562|[0x8000d928]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800078ac]:fsgnjn.s t6, t5, t4<br> [0x800078b0]:csrrs a2, fcsr, zero<br> [0x800078b4]:sd t6, 384(fp)<br>     |
| 563|[0x8000d938]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800078e4]:fsgnjn.s t6, t5, t4<br> [0x800078e8]:csrrs a2, fcsr, zero<br> [0x800078ec]:sd t6, 400(fp)<br>     |
| 564|[0x8000d948]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000791c]:fsgnjn.s t6, t5, t4<br> [0x80007920]:csrrs a2, fcsr, zero<br> [0x80007924]:sd t6, 416(fp)<br>     |
| 565|[0x8000d958]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007954]:fsgnjn.s t6, t5, t4<br> [0x80007958]:csrrs a2, fcsr, zero<br> [0x8000795c]:sd t6, 432(fp)<br>     |
| 566|[0x8000d968]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000798c]:fsgnjn.s t6, t5, t4<br> [0x80007990]:csrrs a2, fcsr, zero<br> [0x80007994]:sd t6, 448(fp)<br>     |
| 567|[0x8000d978]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800079c4]:fsgnjn.s t6, t5, t4<br> [0x800079c8]:csrrs a2, fcsr, zero<br> [0x800079cc]:sd t6, 464(fp)<br>     |
| 568|[0x8000d988]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800079fc]:fsgnjn.s t6, t5, t4<br> [0x80007a00]:csrrs a2, fcsr, zero<br> [0x80007a04]:sd t6, 480(fp)<br>     |
| 569|[0x8000d998]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007a34]:fsgnjn.s t6, t5, t4<br> [0x80007a38]:csrrs a2, fcsr, zero<br> [0x80007a3c]:sd t6, 496(fp)<br>     |
| 570|[0x8000d9a8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007a6c]:fsgnjn.s t6, t5, t4<br> [0x80007a70]:csrrs a2, fcsr, zero<br> [0x80007a74]:sd t6, 512(fp)<br>     |
| 571|[0x8000d9b8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007aa4]:fsgnjn.s t6, t5, t4<br> [0x80007aa8]:csrrs a2, fcsr, zero<br> [0x80007aac]:sd t6, 528(fp)<br>     |
| 572|[0x8000d9c8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007adc]:fsgnjn.s t6, t5, t4<br> [0x80007ae0]:csrrs a2, fcsr, zero<br> [0x80007ae4]:sd t6, 544(fp)<br>     |
| 573|[0x8000d9d8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007b14]:fsgnjn.s t6, t5, t4<br> [0x80007b18]:csrrs a2, fcsr, zero<br> [0x80007b1c]:sd t6, 560(fp)<br>     |
| 574|[0x8000d9e8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007b4c]:fsgnjn.s t6, t5, t4<br> [0x80007b50]:csrrs a2, fcsr, zero<br> [0x80007b54]:sd t6, 576(fp)<br>     |
| 575|[0x8000d9f8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007b84]:fsgnjn.s t6, t5, t4<br> [0x80007b88]:csrrs a2, fcsr, zero<br> [0x80007b8c]:sd t6, 592(fp)<br>     |
| 576|[0x8000da08]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007bbc]:fsgnjn.s t6, t5, t4<br> [0x80007bc0]:csrrs a2, fcsr, zero<br> [0x80007bc4]:sd t6, 608(fp)<br>     |
| 577|[0x8000da28]<br>0xFFFFFFFF80000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007c2c]:fsgnjn.s t6, t5, t4<br> [0x80007c30]:csrrs a2, fcsr, zero<br> [0x80007c34]:sd t6, 640(fp)<br>     |
| 578|[0x8000da38]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007c64]:fsgnjn.s t6, t5, t4<br> [0x80007c68]:csrrs a2, fcsr, zero<br> [0x80007c6c]:sd t6, 656(fp)<br>     |
| 579|[0x8000da48]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007c9c]:fsgnjn.s t6, t5, t4<br> [0x80007ca0]:csrrs a2, fcsr, zero<br> [0x80007ca4]:sd t6, 672(fp)<br>     |
