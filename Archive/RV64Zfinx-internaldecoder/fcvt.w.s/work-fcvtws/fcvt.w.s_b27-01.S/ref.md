
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000740')]      |
| SIG_REGION                | [('0x80002310', '0x80002520', '66 dwords')]      |
| COV_LABELS                | fcvt.w.s_b27      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fcvtws/fcvt.w.s_b27-01.S/ref.S    |
| Total Number of coverpoints| 73     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|                                  signature                                   |                                                                                      coverpoints                                                                                      |                                                                     code                                                                     |
|---:|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x000000007FFFFFFF<br> [0x80002320]<br>0x0000000000000010<br> |- mnemonic : fcvt.w.s<br> - rs1 : x30<br> - rd : x31<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br> |[0x800003b8]:fcvt.w.s t6, t5, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br> [0x800003c4]:sd tp, 8(ra)<br>       |
|   2|[0x80002328]<br>0x000000007FFFFFFF<br> [0x80002330]<br>0x0000000000000010<br> |- rs1 : x31<br> - rd : x30<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800003d4]:fcvt.w.s t5, t6, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br> [0x800003e0]:sd tp, 24(ra)<br>     |
|   3|[0x80002338]<br>0x000000007FFFFFFF<br> [0x80002340]<br>0x0000000000000010<br> |- rs1 : x28<br> - rd : x29<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x2aaaaa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x800003f0]:fcvt.w.s t4, t3, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br> [0x800003fc]:sd tp, 40(ra)<br>     |
|   4|[0x80002348]<br>0x000000007FFFFFFF<br> [0x80002350]<br>0x0000000000000010<br> |- rs1 : x29<br> - rd : x28<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x8000040c]:fcvt.w.s t3, t4, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br> [0x80000418]:sd tp, 56(ra)<br>     |
|   5|[0x80002358]<br>0x000000007FFFFFFF<br> [0x80002360]<br>0x0000000000000010<br> |- rs1 : x26<br> - rd : x27<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000428]:fcvt.w.s s11, s10, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br> [0x80000434]:sd tp, 72(ra)<br>  |
|   6|[0x80002368]<br>0x000000007FFFFFFF<br> [0x80002370]<br>0x0000000000000010<br> |- rs1 : x27<br> - rd : x26<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x400001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x80000444]:fcvt.w.s s10, s11, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br> [0x80000450]:sd tp, 88(ra)<br>  |
|   7|[0x80002378]<br>0x000000007FFFFFFF<br> [0x80002380]<br>0x0000000000000010<br> |- rs1 : x24<br> - rd : x25<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x455555 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000460]:fcvt.w.s s9, s8, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br> [0x8000046c]:sd tp, 104(ra)<br>    |
|   8|[0x80002388]<br>0x000000007FFFFFFF<br> [0x80002390]<br>0x0000000000000010<br> |- rs1 : x25<br> - rd : x24<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x8000047c]:fcvt.w.s s8, s9, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br> [0x80000488]:sd tp, 120(ra)<br>   |
|   9|[0x80002398]<br>0x0000000000000000<br> [0x800023a0]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : x23<br>                                                                                                                                                         |[0x80000498]:fcvt.w.s s7, s6, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br> [0x800004a4]:sd tp, 136(ra)<br>   |
|  10|[0x800023a8]<br>0x0000000000000000<br> [0x800023b0]<br>0x0000000000000000<br> |- rs1 : x23<br> - rd : x22<br>                                                                                                                                                         |[0x800004b4]:fcvt.w.s s6, s7, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br> [0x800004c0]:sd tp, 152(ra)<br>   |
|  11|[0x800023b8]<br>0x0000000000000000<br> [0x800023c0]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : x21<br>                                                                                                                                                         |[0x800004d0]:fcvt.w.s s5, s4, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br> [0x800004dc]:sd tp, 168(ra)<br>   |
|  12|[0x800023c8]<br>0x0000000000000000<br> [0x800023d0]<br>0x0000000000000000<br> |- rs1 : x21<br> - rd : x20<br>                                                                                                                                                         |[0x800004ec]:fcvt.w.s s4, s5, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br> [0x800004f8]:sd tp, 184(ra)<br>   |
|  13|[0x800023d8]<br>0x0000000000000000<br> [0x800023e0]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : x19<br>                                                                                                                                                         |[0x80000508]:fcvt.w.s s3, s2, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br> [0x80000514]:sd tp, 200(ra)<br>   |
|  14|[0x800023e8]<br>0x0000000000000000<br> [0x800023f0]<br>0x0000000000000000<br> |- rs1 : x19<br> - rd : x18<br>                                                                                                                                                         |[0x80000524]:fcvt.w.s s2, s3, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br> [0x80000530]:sd tp, 216(ra)<br>   |
|  15|[0x800023f8]<br>0x0000000000000000<br> [0x80002400]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : x17<br>                                                                                                                                                         |[0x80000540]:fcvt.w.s a7, a6, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br> [0x8000054c]:sd tp, 232(ra)<br>   |
|  16|[0x80002408]<br>0x0000000000000000<br> [0x80002410]<br>0x0000000000000000<br> |- rs1 : x17<br> - rd : x16<br>                                                                                                                                                         |[0x8000055c]:fcvt.w.s a6, a7, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br> [0x80000568]:sd tp, 248(ra)<br>   |
|  17|[0x80002418]<br>0x0000000000000000<br> [0x80002420]<br>0x0000000000000000<br> |- rs1 : x14<br> - rd : x15<br>                                                                                                                                                         |[0x80000578]:fcvt.w.s a5, a4, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br> [0x80000584]:sd tp, 264(ra)<br>   |
|  18|[0x80002428]<br>0x0000000000000000<br> [0x80002430]<br>0x0000000000000000<br> |- rs1 : x15<br> - rd : x14<br>                                                                                                                                                         |[0x80000594]:fcvt.w.s a4, a5, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br> [0x800005a0]:sd tp, 280(ra)<br>   |
|  19|[0x80002438]<br>0x0000000000000000<br> [0x80002440]<br>0x0000000000000000<br> |- rs1 : x12<br> - rd : x13<br>                                                                                                                                                         |[0x800005b0]:fcvt.w.s a3, a2, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sd a3, 288(ra)<br> [0x800005bc]:sd tp, 296(ra)<br>   |
|  20|[0x80002448]<br>0x0000000000000000<br> [0x80002450]<br>0x0000000000000000<br> |- rs1 : x13<br> - rd : x12<br>                                                                                                                                                         |[0x800005cc]:fcvt.w.s a2, a3, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sd a2, 304(ra)<br> [0x800005d8]:sd tp, 312(ra)<br>   |
|  21|[0x80002458]<br>0x0000000000000000<br> [0x80002460]<br>0x0000000000000000<br> |- rs1 : x10<br> - rd : x11<br>                                                                                                                                                         |[0x800005e8]:fcvt.w.s a1, a0, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br> [0x800005f4]:sd tp, 328(ra)<br>   |
|  22|[0x80002468]<br>0x0000000000000000<br> [0x80002470]<br>0x0000000000000000<br> |- rs1 : x11<br> - rd : x10<br>                                                                                                                                                         |[0x80000604]:fcvt.w.s a0, a1, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br> [0x80000610]:sd tp, 344(ra)<br>   |
|  23|[0x80002478]<br>0x0000000000000000<br> [0x80002480]<br>0x0000000000000000<br> |- rs1 : x8<br> - rd : x9<br>                                                                                                                                                           |[0x80000620]:fcvt.w.s s1, fp, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br> [0x8000062c]:sd tp, 360(ra)<br>   |
|  24|[0x80002488]<br>0x0000000000000000<br> [0x80002490]<br>0x0000000000000000<br> |- rs1 : x9<br> - rd : x8<br>                                                                                                                                                           |[0x80000644]:fcvt.w.s fp, s1, dyn<br> [0x80000648]:csrrs a1, fcsr, zero<br> [0x8000064c]:sd fp, 368(ra)<br> [0x80000650]:sd a1, 376(ra)<br>   |
|  25|[0x80002498]<br>0x0000000000000000<br> [0x800024a0]<br>0x0000000000000000<br> |- rs1 : x6<br> - rd : x7<br>                                                                                                                                                           |[0x80000660]:fcvt.w.s t2, t1, dyn<br> [0x80000664]:csrrs a1, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br> [0x8000066c]:sd a1, 392(ra)<br>   |
|  26|[0x800024a8]<br>0x0000000000000000<br> [0x800024b0]<br>0x0000000000000000<br> |- rs1 : x7<br> - rd : x6<br>                                                                                                                                                           |[0x8000067c]:fcvt.w.s t1, t2, dyn<br> [0x80000680]:csrrs a1, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br> [0x80000688]:sd a1, 408(ra)<br>   |
|  27|[0x800024b8]<br>0x0000000000000000<br> [0x800024c0]<br>0x0000000000000000<br> |- rs1 : x4<br> - rd : x5<br>                                                                                                                                                           |[0x80000698]:fcvt.w.s t0, tp, dyn<br> [0x8000069c]:csrrs a1, fcsr, zero<br> [0x800006a0]:sd t0, 416(ra)<br> [0x800006a4]:sd a1, 424(ra)<br>   |
|  28|[0x800024c8]<br>0x0000000000000000<br> [0x800024d0]<br>0x0000000000000000<br> |- rs1 : x5<br> - rd : x4<br>                                                                                                                                                           |[0x800006bc]:fcvt.w.s tp, t0, dyn<br> [0x800006c0]:csrrs a1, fcsr, zero<br> [0x800006c4]:sd tp, 0(t1)<br> [0x800006c8]:sd a1, 8(t1)<br>       |
|  29|[0x800024d8]<br>0x0000000000000000<br> [0x800024e0]<br>0x0000000000000000<br> |- rs1 : x2<br> - rd : x3<br>                                                                                                                                                           |[0x800006d8]:fcvt.w.s gp, sp, dyn<br> [0x800006dc]:csrrs a1, fcsr, zero<br> [0x800006e0]:sd gp, 16(t1)<br> [0x800006e4]:sd a1, 24(t1)<br>     |
|  30|[0x800024e8]<br>0x0000000000000000<br> [0x800024f0]<br>0x0000000000000000<br> |- rs1 : x3<br> - rd : x2<br>                                                                                                                                                           |[0x800006f4]:fcvt.w.s sp, gp, dyn<br> [0x800006f8]:csrrs a1, fcsr, zero<br> [0x800006fc]:sd sp, 32(t1)<br> [0x80000700]:sd a1, 40(t1)<br>     |
|  31|[0x800024f8]<br>0x0000000000000000<br> [0x80002500]<br>0x0000000000000000<br> |- rs1 : x0<br> - rd : x1<br>                                                                                                                                                           |[0x80000710]:fcvt.w.s ra, zero, dyn<br> [0x80000714]:csrrs a1, fcsr, zero<br> [0x80000718]:sd ra, 48(t1)<br> [0x8000071c]:sd a1, 56(t1)<br>   |
|  32|[0x80002508]<br>0x0000000000000000<br> [0x80002510]<br>0x0000000000000000<br> |- rs1 : x1<br> - rd : x0<br>                                                                                                                                                           |[0x8000072c]:fcvt.w.s zero, ra, dyn<br> [0x80000730]:csrrs a1, fcsr, zero<br> [0x80000734]:sd zero, 64(t1)<br> [0x80000738]:sd a1, 72(t1)<br> |
