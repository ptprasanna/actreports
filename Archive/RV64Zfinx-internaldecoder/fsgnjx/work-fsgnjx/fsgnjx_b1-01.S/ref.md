
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80007d00')]      |
| SIG_REGION                | [('0x8000b610', '0x8000da70', '1164 dwords')]      |
| COV_LABELS                | fsgnjx_b1      |
| TEST_NAME                 | /home/riscv/update/riscv-ctg/RV64Zfinx/work-fsgnjx/fsgnjx_b1-01.S/ref.S    |
| Total Number of coverpoints| 678     |
| Total Coverpoints Hit     | 678      |
| Total Signature Updates   | 1162      |
| STAT1                     | 580      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 581     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80007cec]:fsgnjx.s t6, t5, t4
      [0x80007cf0]:csrrs a3, fcsr, zero
      [0x80007cf4]:sd t6, 704(s1)
 -- Signature Addresses:
      Address: 0x8000da58 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fsgnjx.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsgnjx.s', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fsgnjx.s t6, t6, t5
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x8000b620]:0x0000000000000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x29', 'rd : x29', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800003dc]:fsgnjx.s t4, t5, t4
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t4, 16(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x8000b630]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003fc]:fsgnjx.s t3, t3, t3
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t3, 32(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x8000b640]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x30', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000041c]:fsgnjx.s t5, s11, s11
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t5, 48(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x8000b650]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x31', 'rd : x27', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000043c]:fsgnjx.s s11, t4, t6
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s11, 64(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x8000b660]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fsgnjx.s s10, s9, s8
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s10, 80(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x8000b670]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fsgnjx.s s9, s8, s10
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x8000b680]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fsgnjx.s s8, s10, s9
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x8000b690]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fsgnjx.s s7, s6, s5
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x8000b6a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fsgnjx.s s6, s5, s7
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x8000b6b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fsgnjx.s s5, s7, s6
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x8000b6c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fsgnjx.s s4, s3, s2
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x8000b6d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fsgnjx.s s3, s2, s4
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x8000b6e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsgnjx.s s2, s4, s3
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x8000b6f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fsgnjx.s a7, a6, a5
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x8000b700]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fsgnjx.s a6, a5, a7
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x8000b710]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fsgnjx.s a5, a7, a6
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x8000b720]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fsgnjx.s a4, a3, a2
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x8000b730]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fsgnjx.s a3, a2, a4
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x8000b740]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fsgnjx.s a2, a4, a3
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x8000b750]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fsgnjx.s a1, a0, s1
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x8000b760]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000664]:fsgnjx.s a0, s1, a1
	-[0x80000668]:csrrs a3, fcsr, zero
	-[0x8000066c]:sd a0, 336(ra)
Current Store : [0x80000670] : sd a3, 344(ra) -- Store: [0x8000b770]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fsgnjx.s s1, a1, a0
	-[0x80000688]:csrrs a3, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
Current Store : [0x80000690] : sd a3, 360(ra) -- Store: [0x8000b780]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fsgnjx.s fp, t2, t1
	-[0x800006a8]:csrrs a3, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
Current Store : [0x800006b0] : sd a3, 376(ra) -- Store: [0x8000b790]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006cc]:fsgnjx.s t2, t1, fp
	-[0x800006d0]:csrrs a3, fcsr, zero
	-[0x800006d4]:sd t2, 0(s1)
Current Store : [0x800006d8] : sd a3, 8(s1) -- Store: [0x8000b7a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fsgnjx.s t1, fp, t2
	-[0x800006f0]:csrrs a3, fcsr, zero
	-[0x800006f4]:sd t1, 16(s1)
Current Store : [0x800006f8] : sd a3, 24(s1) -- Store: [0x8000b7b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fsgnjx.s t0, tp, gp
	-[0x80000710]:csrrs a3, fcsr, zero
	-[0x80000714]:sd t0, 32(s1)
Current Store : [0x80000718] : sd a3, 40(s1) -- Store: [0x8000b7c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fsgnjx.s tp, gp, t0
	-[0x80000730]:csrrs a3, fcsr, zero
	-[0x80000734]:sd tp, 48(s1)
Current Store : [0x80000738] : sd a3, 56(s1) -- Store: [0x8000b7d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fsgnjx.s gp, t0, tp
	-[0x80000750]:csrrs a3, fcsr, zero
	-[0x80000754]:sd gp, 64(s1)
Current Store : [0x80000758] : sd a3, 72(s1) -- Store: [0x8000b7e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2']
Last Code Sequence : 
	-[0x8000076c]:fsgnjx.s sp, ra, zero
	-[0x80000770]:csrrs a3, fcsr, zero
	-[0x80000774]:sd sp, 80(s1)
Current Store : [0x80000778] : sd a3, 88(s1) -- Store: [0x8000b7f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x8000078c]:fsgnjx.s ra, zero, sp
	-[0x80000790]:csrrs a3, fcsr, zero
	-[0x80000794]:sd ra, 96(s1)
Current Store : [0x80000798] : sd a3, 104(s1) -- Store: [0x8000b800]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fsgnjx.s zero, sp, ra
	-[0x800007b0]:csrrs a3, fcsr, zero
	-[0x800007b4]:sd zero, 112(s1)
Current Store : [0x800007b8] : sd a3, 120(s1) -- Store: [0x8000b810]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007cc]:fsgnjx.s t6, t5, t4
	-[0x800007d0]:csrrs a3, fcsr, zero
	-[0x800007d4]:sd t6, 128(s1)
Current Store : [0x800007d8] : sd a3, 136(s1) -- Store: [0x8000b820]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fsgnjx.s t6, t5, t4
	-[0x800007f0]:csrrs a3, fcsr, zero
	-[0x800007f4]:sd t6, 144(s1)
Current Store : [0x800007f8] : sd a3, 152(s1) -- Store: [0x8000b830]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fsgnjx.s t6, t5, t4
	-[0x80000810]:csrrs a3, fcsr, zero
	-[0x80000814]:sd t6, 160(s1)
Current Store : [0x80000818] : sd a3, 168(s1) -- Store: [0x8000b840]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fsgnjx.s t6, t5, t4
	-[0x80000830]:csrrs a3, fcsr, zero
	-[0x80000834]:sd t6, 176(s1)
Current Store : [0x80000838] : sd a3, 184(s1) -- Store: [0x8000b850]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fsgnjx.s t6, t5, t4
	-[0x80000850]:csrrs a3, fcsr, zero
	-[0x80000854]:sd t6, 192(s1)
Current Store : [0x80000858] : sd a3, 200(s1) -- Store: [0x8000b860]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fsgnjx.s t6, t5, t4
	-[0x80000870]:csrrs a3, fcsr, zero
	-[0x80000874]:sd t6, 208(s1)
Current Store : [0x80000878] : sd a3, 216(s1) -- Store: [0x8000b870]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fsgnjx.s t6, t5, t4
	-[0x80000890]:csrrs a3, fcsr, zero
	-[0x80000894]:sd t6, 224(s1)
Current Store : [0x80000898] : sd a3, 232(s1) -- Store: [0x8000b880]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fsgnjx.s t6, t5, t4
	-[0x800008b0]:csrrs a3, fcsr, zero
	-[0x800008b4]:sd t6, 240(s1)
Current Store : [0x800008b8] : sd a3, 248(s1) -- Store: [0x8000b890]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fsgnjx.s t6, t5, t4
	-[0x800008d0]:csrrs a3, fcsr, zero
	-[0x800008d4]:sd t6, 256(s1)
Current Store : [0x800008d8] : sd a3, 264(s1) -- Store: [0x8000b8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fsgnjx.s t6, t5, t4
	-[0x800008f0]:csrrs a3, fcsr, zero
	-[0x800008f4]:sd t6, 272(s1)
Current Store : [0x800008f8] : sd a3, 280(s1) -- Store: [0x8000b8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fsgnjx.s t6, t5, t4
	-[0x80000910]:csrrs a3, fcsr, zero
	-[0x80000914]:sd t6, 288(s1)
Current Store : [0x80000918] : sd a3, 296(s1) -- Store: [0x8000b8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fsgnjx.s t6, t5, t4
	-[0x80000930]:csrrs a3, fcsr, zero
	-[0x80000934]:sd t6, 304(s1)
Current Store : [0x80000938] : sd a3, 312(s1) -- Store: [0x8000b8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fsgnjx.s t6, t5, t4
	-[0x80000950]:csrrs a3, fcsr, zero
	-[0x80000954]:sd t6, 320(s1)
Current Store : [0x80000958] : sd a3, 328(s1) -- Store: [0x8000b8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fsgnjx.s t6, t5, t4
	-[0x80000970]:csrrs a3, fcsr, zero
	-[0x80000974]:sd t6, 336(s1)
Current Store : [0x80000978] : sd a3, 344(s1) -- Store: [0x8000b8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fsgnjx.s t6, t5, t4
	-[0x80000990]:csrrs a3, fcsr, zero
	-[0x80000994]:sd t6, 352(s1)
Current Store : [0x80000998] : sd a3, 360(s1) -- Store: [0x8000b900]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fsgnjx.s t6, t5, t4
	-[0x800009b0]:csrrs a3, fcsr, zero
	-[0x800009b4]:sd t6, 368(s1)
Current Store : [0x800009b8] : sd a3, 376(s1) -- Store: [0x8000b910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fsgnjx.s t6, t5, t4
	-[0x800009d0]:csrrs a3, fcsr, zero
	-[0x800009d4]:sd t6, 384(s1)
Current Store : [0x800009d8] : sd a3, 392(s1) -- Store: [0x8000b920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fsgnjx.s t6, t5, t4
	-[0x800009f0]:csrrs a3, fcsr, zero
	-[0x800009f4]:sd t6, 400(s1)
Current Store : [0x800009f8] : sd a3, 408(s1) -- Store: [0x8000b930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fsgnjx.s t6, t5, t4
	-[0x80000a10]:csrrs a3, fcsr, zero
	-[0x80000a14]:sd t6, 416(s1)
Current Store : [0x80000a18] : sd a3, 424(s1) -- Store: [0x8000b940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fsgnjx.s t6, t5, t4
	-[0x80000a30]:csrrs a3, fcsr, zero
	-[0x80000a34]:sd t6, 432(s1)
Current Store : [0x80000a38] : sd a3, 440(s1) -- Store: [0x8000b950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fsgnjx.s t6, t5, t4
	-[0x80000a50]:csrrs a3, fcsr, zero
	-[0x80000a54]:sd t6, 448(s1)
Current Store : [0x80000a58] : sd a3, 456(s1) -- Store: [0x8000b960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fsgnjx.s t6, t5, t4
	-[0x80000a70]:csrrs a3, fcsr, zero
	-[0x80000a74]:sd t6, 464(s1)
Current Store : [0x80000a78] : sd a3, 472(s1) -- Store: [0x8000b970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fsgnjx.s t6, t5, t4
	-[0x80000a90]:csrrs a3, fcsr, zero
	-[0x80000a94]:sd t6, 480(s1)
Current Store : [0x80000a98] : sd a3, 488(s1) -- Store: [0x8000b980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fsgnjx.s t6, t5, t4
	-[0x80000ab0]:csrrs a3, fcsr, zero
	-[0x80000ab4]:sd t6, 496(s1)
Current Store : [0x80000ab8] : sd a3, 504(s1) -- Store: [0x8000b990]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fsgnjx.s t6, t5, t4
	-[0x80000ad0]:csrrs a3, fcsr, zero
	-[0x80000ad4]:sd t6, 512(s1)
Current Store : [0x80000ad8] : sd a3, 520(s1) -- Store: [0x8000b9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fsgnjx.s t6, t5, t4
	-[0x80000af0]:csrrs a3, fcsr, zero
	-[0x80000af4]:sd t6, 528(s1)
Current Store : [0x80000af8] : sd a3, 536(s1) -- Store: [0x8000b9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fsgnjx.s t6, t5, t4
	-[0x80000b10]:csrrs a3, fcsr, zero
	-[0x80000b14]:sd t6, 544(s1)
Current Store : [0x80000b18] : sd a3, 552(s1) -- Store: [0x8000b9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fsgnjx.s t6, t5, t4
	-[0x80000b30]:csrrs a3, fcsr, zero
	-[0x80000b34]:sd t6, 560(s1)
Current Store : [0x80000b38] : sd a3, 568(s1) -- Store: [0x8000b9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fsgnjx.s t6, t5, t4
	-[0x80000b50]:csrrs a3, fcsr, zero
	-[0x80000b54]:sd t6, 576(s1)
Current Store : [0x80000b58] : sd a3, 584(s1) -- Store: [0x8000b9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fsgnjx.s t6, t5, t4
	-[0x80000b70]:csrrs a3, fcsr, zero
	-[0x80000b74]:sd t6, 592(s1)
Current Store : [0x80000b78] : sd a3, 600(s1) -- Store: [0x8000b9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fsgnjx.s t6, t5, t4
	-[0x80000b90]:csrrs a3, fcsr, zero
	-[0x80000b94]:sd t6, 608(s1)
Current Store : [0x80000b98] : sd a3, 616(s1) -- Store: [0x8000ba00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fsgnjx.s t6, t5, t4
	-[0x80000bb0]:csrrs a3, fcsr, zero
	-[0x80000bb4]:sd t6, 624(s1)
Current Store : [0x80000bb8] : sd a3, 632(s1) -- Store: [0x8000ba10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fsgnjx.s t6, t5, t4
	-[0x80000bd0]:csrrs a3, fcsr, zero
	-[0x80000bd4]:sd t6, 640(s1)
Current Store : [0x80000bd8] : sd a3, 648(s1) -- Store: [0x8000ba20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fsgnjx.s t6, t5, t4
	-[0x80000bf0]:csrrs a3, fcsr, zero
	-[0x80000bf4]:sd t6, 656(s1)
Current Store : [0x80000bf8] : sd a3, 664(s1) -- Store: [0x8000ba30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fsgnjx.s t6, t5, t4
	-[0x80000c10]:csrrs a3, fcsr, zero
	-[0x80000c14]:sd t6, 672(s1)
Current Store : [0x80000c18] : sd a3, 680(s1) -- Store: [0x8000ba40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fsgnjx.s t6, t5, t4
	-[0x80000c30]:csrrs a3, fcsr, zero
	-[0x80000c34]:sd t6, 688(s1)
Current Store : [0x80000c38] : sd a3, 696(s1) -- Store: [0x8000ba50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fsgnjx.s t6, t5, t4
	-[0x80000c50]:csrrs a3, fcsr, zero
	-[0x80000c54]:sd t6, 704(s1)
Current Store : [0x80000c58] : sd a3, 712(s1) -- Store: [0x8000ba60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fsgnjx.s t6, t5, t4
	-[0x80000c70]:csrrs a3, fcsr, zero
	-[0x80000c74]:sd t6, 720(s1)
Current Store : [0x80000c78] : sd a3, 728(s1) -- Store: [0x8000ba70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fsgnjx.s t6, t5, t4
	-[0x80000c90]:csrrs a3, fcsr, zero
	-[0x80000c94]:sd t6, 736(s1)
Current Store : [0x80000c98] : sd a3, 744(s1) -- Store: [0x8000ba80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fsgnjx.s t6, t5, t4
	-[0x80000cb0]:csrrs a3, fcsr, zero
	-[0x80000cb4]:sd t6, 752(s1)
Current Store : [0x80000cb8] : sd a3, 760(s1) -- Store: [0x8000ba90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fsgnjx.s t6, t5, t4
	-[0x80000cd0]:csrrs a3, fcsr, zero
	-[0x80000cd4]:sd t6, 768(s1)
Current Store : [0x80000cd8] : sd a3, 776(s1) -- Store: [0x8000baa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fsgnjx.s t6, t5, t4
	-[0x80000cf0]:csrrs a3, fcsr, zero
	-[0x80000cf4]:sd t6, 784(s1)
Current Store : [0x80000cf8] : sd a3, 792(s1) -- Store: [0x8000bab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fsgnjx.s t6, t5, t4
	-[0x80000d10]:csrrs a3, fcsr, zero
	-[0x80000d14]:sd t6, 800(s1)
Current Store : [0x80000d18] : sd a3, 808(s1) -- Store: [0x8000bac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fsgnjx.s t6, t5, t4
	-[0x80000d30]:csrrs a3, fcsr, zero
	-[0x80000d34]:sd t6, 816(s1)
Current Store : [0x80000d38] : sd a3, 824(s1) -- Store: [0x8000bad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fsgnjx.s t6, t5, t4
	-[0x80000d50]:csrrs a3, fcsr, zero
	-[0x80000d54]:sd t6, 832(s1)
Current Store : [0x80000d58] : sd a3, 840(s1) -- Store: [0x8000bae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fsgnjx.s t6, t5, t4
	-[0x80000d70]:csrrs a3, fcsr, zero
	-[0x80000d74]:sd t6, 848(s1)
Current Store : [0x80000d78] : sd a3, 856(s1) -- Store: [0x8000baf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fsgnjx.s t6, t5, t4
	-[0x80000d90]:csrrs a3, fcsr, zero
	-[0x80000d94]:sd t6, 864(s1)
Current Store : [0x80000d98] : sd a3, 872(s1) -- Store: [0x8000bb00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fsgnjx.s t6, t5, t4
	-[0x80000db0]:csrrs a3, fcsr, zero
	-[0x80000db4]:sd t6, 880(s1)
Current Store : [0x80000db8] : sd a3, 888(s1) -- Store: [0x8000bb10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dcc]:fsgnjx.s t6, t5, t4
	-[0x80000dd0]:csrrs a3, fcsr, zero
	-[0x80000dd4]:sd t6, 896(s1)
Current Store : [0x80000dd8] : sd a3, 904(s1) -- Store: [0x8000bb20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dec]:fsgnjx.s t6, t5, t4
	-[0x80000df0]:csrrs a3, fcsr, zero
	-[0x80000df4]:sd t6, 912(s1)
Current Store : [0x80000df8] : sd a3, 920(s1) -- Store: [0x8000bb30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fsgnjx.s t6, t5, t4
	-[0x80000e10]:csrrs a3, fcsr, zero
	-[0x80000e14]:sd t6, 928(s1)
Current Store : [0x80000e18] : sd a3, 936(s1) -- Store: [0x8000bb40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fsgnjx.s t6, t5, t4
	-[0x80000e30]:csrrs a3, fcsr, zero
	-[0x80000e34]:sd t6, 944(s1)
Current Store : [0x80000e38] : sd a3, 952(s1) -- Store: [0x8000bb50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fsgnjx.s t6, t5, t4
	-[0x80000e50]:csrrs a3, fcsr, zero
	-[0x80000e54]:sd t6, 960(s1)
Current Store : [0x80000e58] : sd a3, 968(s1) -- Store: [0x8000bb60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fsgnjx.s t6, t5, t4
	-[0x80000e70]:csrrs a3, fcsr, zero
	-[0x80000e74]:sd t6, 976(s1)
Current Store : [0x80000e78] : sd a3, 984(s1) -- Store: [0x8000bb70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e8c]:fsgnjx.s t6, t5, t4
	-[0x80000e90]:csrrs a3, fcsr, zero
	-[0x80000e94]:sd t6, 992(s1)
Current Store : [0x80000e98] : sd a3, 1000(s1) -- Store: [0x8000bb80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eac]:fsgnjx.s t6, t5, t4
	-[0x80000eb0]:csrrs a3, fcsr, zero
	-[0x80000eb4]:sd t6, 1008(s1)
Current Store : [0x80000eb8] : sd a3, 1016(s1) -- Store: [0x8000bb90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fsgnjx.s t6, t5, t4
	-[0x80000ed0]:csrrs a3, fcsr, zero
	-[0x80000ed4]:sd t6, 1024(s1)
Current Store : [0x80000ed8] : sd a3, 1032(s1) -- Store: [0x8000bba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eec]:fsgnjx.s t6, t5, t4
	-[0x80000ef0]:csrrs a3, fcsr, zero
	-[0x80000ef4]:sd t6, 1040(s1)
Current Store : [0x80000ef8] : sd a3, 1048(s1) -- Store: [0x8000bbb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fsgnjx.s t6, t5, t4
	-[0x80000f10]:csrrs a3, fcsr, zero
	-[0x80000f14]:sd t6, 1056(s1)
Current Store : [0x80000f18] : sd a3, 1064(s1) -- Store: [0x8000bbc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fsgnjx.s t6, t5, t4
	-[0x80000f30]:csrrs a3, fcsr, zero
	-[0x80000f34]:sd t6, 1072(s1)
Current Store : [0x80000f38] : sd a3, 1080(s1) -- Store: [0x8000bbd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fsgnjx.s t6, t5, t4
	-[0x80000f50]:csrrs a3, fcsr, zero
	-[0x80000f54]:sd t6, 1088(s1)
Current Store : [0x80000f58] : sd a3, 1096(s1) -- Store: [0x8000bbe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fsgnjx.s t6, t5, t4
	-[0x80000f70]:csrrs a3, fcsr, zero
	-[0x80000f74]:sd t6, 1104(s1)
Current Store : [0x80000f78] : sd a3, 1112(s1) -- Store: [0x8000bbf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:fsgnjx.s t6, t5, t4
	-[0x80000f90]:csrrs a3, fcsr, zero
	-[0x80000f94]:sd t6, 1120(s1)
Current Store : [0x80000f98] : sd a3, 1128(s1) -- Store: [0x8000bc00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fac]:fsgnjx.s t6, t5, t4
	-[0x80000fb0]:csrrs a3, fcsr, zero
	-[0x80000fb4]:sd t6, 1136(s1)
Current Store : [0x80000fb8] : sd a3, 1144(s1) -- Store: [0x8000bc10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fcc]:fsgnjx.s t6, t5, t4
	-[0x80000fd0]:csrrs a3, fcsr, zero
	-[0x80000fd4]:sd t6, 1152(s1)
Current Store : [0x80000fd8] : sd a3, 1160(s1) -- Store: [0x8000bc20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fsgnjx.s t6, t5, t4
	-[0x80000ff0]:csrrs a3, fcsr, zero
	-[0x80000ff4]:sd t6, 1168(s1)
Current Store : [0x80000ff8] : sd a3, 1176(s1) -- Store: [0x8000bc30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000100c]:fsgnjx.s t6, t5, t4
	-[0x80001010]:csrrs a3, fcsr, zero
	-[0x80001014]:sd t6, 1184(s1)
Current Store : [0x80001018] : sd a3, 1192(s1) -- Store: [0x8000bc40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000102c]:fsgnjx.s t6, t5, t4
	-[0x80001030]:csrrs a3, fcsr, zero
	-[0x80001034]:sd t6, 1200(s1)
Current Store : [0x80001038] : sd a3, 1208(s1) -- Store: [0x8000bc50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000104c]:fsgnjx.s t6, t5, t4
	-[0x80001050]:csrrs a3, fcsr, zero
	-[0x80001054]:sd t6, 1216(s1)
Current Store : [0x80001058] : sd a3, 1224(s1) -- Store: [0x8000bc60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000106c]:fsgnjx.s t6, t5, t4
	-[0x80001070]:csrrs a3, fcsr, zero
	-[0x80001074]:sd t6, 1232(s1)
Current Store : [0x80001078] : sd a3, 1240(s1) -- Store: [0x8000bc70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000108c]:fsgnjx.s t6, t5, t4
	-[0x80001090]:csrrs a3, fcsr, zero
	-[0x80001094]:sd t6, 1248(s1)
Current Store : [0x80001098] : sd a3, 1256(s1) -- Store: [0x8000bc80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010ac]:fsgnjx.s t6, t5, t4
	-[0x800010b0]:csrrs a3, fcsr, zero
	-[0x800010b4]:sd t6, 1264(s1)
Current Store : [0x800010b8] : sd a3, 1272(s1) -- Store: [0x8000bc90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010cc]:fsgnjx.s t6, t5, t4
	-[0x800010d0]:csrrs a3, fcsr, zero
	-[0x800010d4]:sd t6, 1280(s1)
Current Store : [0x800010d8] : sd a3, 1288(s1) -- Store: [0x8000bca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010ec]:fsgnjx.s t6, t5, t4
	-[0x800010f0]:csrrs a3, fcsr, zero
	-[0x800010f4]:sd t6, 1296(s1)
Current Store : [0x800010f8] : sd a3, 1304(s1) -- Store: [0x8000bcb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fsgnjx.s t6, t5, t4
	-[0x80001110]:csrrs a3, fcsr, zero
	-[0x80001114]:sd t6, 1312(s1)
Current Store : [0x80001118] : sd a3, 1320(s1) -- Store: [0x8000bcc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000112c]:fsgnjx.s t6, t5, t4
	-[0x80001130]:csrrs a3, fcsr, zero
	-[0x80001134]:sd t6, 1328(s1)
Current Store : [0x80001138] : sd a3, 1336(s1) -- Store: [0x8000bcd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000114c]:fsgnjx.s t6, t5, t4
	-[0x80001150]:csrrs a3, fcsr, zero
	-[0x80001154]:sd t6, 1344(s1)
Current Store : [0x80001158] : sd a3, 1352(s1) -- Store: [0x8000bce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000116c]:fsgnjx.s t6, t5, t4
	-[0x80001170]:csrrs a3, fcsr, zero
	-[0x80001174]:sd t6, 1360(s1)
Current Store : [0x80001178] : sd a3, 1368(s1) -- Store: [0x8000bcf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000118c]:fsgnjx.s t6, t5, t4
	-[0x80001190]:csrrs a3, fcsr, zero
	-[0x80001194]:sd t6, 1376(s1)
Current Store : [0x80001198] : sd a3, 1384(s1) -- Store: [0x8000bd00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800011ac]:fsgnjx.s t6, t5, t4
	-[0x800011b0]:csrrs a3, fcsr, zero
	-[0x800011b4]:sd t6, 1392(s1)
Current Store : [0x800011b8] : sd a3, 1400(s1) -- Store: [0x8000bd10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011cc]:fsgnjx.s t6, t5, t4
	-[0x800011d0]:csrrs a3, fcsr, zero
	-[0x800011d4]:sd t6, 1408(s1)
Current Store : [0x800011d8] : sd a3, 1416(s1) -- Store: [0x8000bd20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800011ec]:fsgnjx.s t6, t5, t4
	-[0x800011f0]:csrrs a3, fcsr, zero
	-[0x800011f4]:sd t6, 1424(s1)
Current Store : [0x800011f8] : sd a3, 1432(s1) -- Store: [0x8000bd30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000120c]:fsgnjx.s t6, t5, t4
	-[0x80001210]:csrrs a3, fcsr, zero
	-[0x80001214]:sd t6, 1440(s1)
Current Store : [0x80001218] : sd a3, 1448(s1) -- Store: [0x8000bd40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fsgnjx.s t6, t5, t4
	-[0x80001230]:csrrs a3, fcsr, zero
	-[0x80001234]:sd t6, 1456(s1)
Current Store : [0x80001238] : sd a3, 1464(s1) -- Store: [0x8000bd50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000124c]:fsgnjx.s t6, t5, t4
	-[0x80001250]:csrrs a3, fcsr, zero
	-[0x80001254]:sd t6, 1472(s1)
Current Store : [0x80001258] : sd a3, 1480(s1) -- Store: [0x8000bd60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000126c]:fsgnjx.s t6, t5, t4
	-[0x80001270]:csrrs a3, fcsr, zero
	-[0x80001274]:sd t6, 1488(s1)
Current Store : [0x80001278] : sd a3, 1496(s1) -- Store: [0x8000bd70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000128c]:fsgnjx.s t6, t5, t4
	-[0x80001290]:csrrs a3, fcsr, zero
	-[0x80001294]:sd t6, 1504(s1)
Current Store : [0x80001298] : sd a3, 1512(s1) -- Store: [0x8000bd80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ac]:fsgnjx.s t6, t5, t4
	-[0x800012b0]:csrrs a3, fcsr, zero
	-[0x800012b4]:sd t6, 1520(s1)
Current Store : [0x800012b8] : sd a3, 1528(s1) -- Store: [0x8000bd90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800012cc]:fsgnjx.s t6, t5, t4
	-[0x800012d0]:csrrs a3, fcsr, zero
	-[0x800012d4]:sd t6, 1536(s1)
Current Store : [0x800012d8] : sd a3, 1544(s1) -- Store: [0x8000bda0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ec]:fsgnjx.s t6, t5, t4
	-[0x800012f0]:csrrs a3, fcsr, zero
	-[0x800012f4]:sd t6, 1552(s1)
Current Store : [0x800012f8] : sd a3, 1560(s1) -- Store: [0x8000bdb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000130c]:fsgnjx.s t6, t5, t4
	-[0x80001310]:csrrs a3, fcsr, zero
	-[0x80001314]:sd t6, 1568(s1)
Current Store : [0x80001318] : sd a3, 1576(s1) -- Store: [0x8000bdc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000132c]:fsgnjx.s t6, t5, t4
	-[0x80001330]:csrrs a3, fcsr, zero
	-[0x80001334]:sd t6, 1584(s1)
Current Store : [0x80001338] : sd a3, 1592(s1) -- Store: [0x8000bdd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000134c]:fsgnjx.s t6, t5, t4
	-[0x80001350]:csrrs a3, fcsr, zero
	-[0x80001354]:sd t6, 1600(s1)
Current Store : [0x80001358] : sd a3, 1608(s1) -- Store: [0x8000bde0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000136c]:fsgnjx.s t6, t5, t4
	-[0x80001370]:csrrs a3, fcsr, zero
	-[0x80001374]:sd t6, 1616(s1)
Current Store : [0x80001378] : sd a3, 1624(s1) -- Store: [0x8000bdf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000138c]:fsgnjx.s t6, t5, t4
	-[0x80001390]:csrrs a3, fcsr, zero
	-[0x80001394]:sd t6, 1632(s1)
Current Store : [0x80001398] : sd a3, 1640(s1) -- Store: [0x8000be00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ac]:fsgnjx.s t6, t5, t4
	-[0x800013b0]:csrrs a3, fcsr, zero
	-[0x800013b4]:sd t6, 1648(s1)
Current Store : [0x800013b8] : sd a3, 1656(s1) -- Store: [0x8000be10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800013cc]:fsgnjx.s t6, t5, t4
	-[0x800013d0]:csrrs a3, fcsr, zero
	-[0x800013d4]:sd t6, 1664(s1)
Current Store : [0x800013d8] : sd a3, 1672(s1) -- Store: [0x8000be20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ec]:fsgnjx.s t6, t5, t4
	-[0x800013f0]:csrrs a3, fcsr, zero
	-[0x800013f4]:sd t6, 1680(s1)
Current Store : [0x800013f8] : sd a3, 1688(s1) -- Store: [0x8000be30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000140c]:fsgnjx.s t6, t5, t4
	-[0x80001410]:csrrs a3, fcsr, zero
	-[0x80001414]:sd t6, 1696(s1)
Current Store : [0x80001418] : sd a3, 1704(s1) -- Store: [0x8000be40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fsgnjx.s t6, t5, t4
	-[0x80001430]:csrrs a3, fcsr, zero
	-[0x80001434]:sd t6, 1712(s1)
Current Store : [0x80001438] : sd a3, 1720(s1) -- Store: [0x8000be50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000144c]:fsgnjx.s t6, t5, t4
	-[0x80001450]:csrrs a3, fcsr, zero
	-[0x80001454]:sd t6, 1728(s1)
Current Store : [0x80001458] : sd a3, 1736(s1) -- Store: [0x8000be60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000146c]:fsgnjx.s t6, t5, t4
	-[0x80001470]:csrrs a3, fcsr, zero
	-[0x80001474]:sd t6, 1744(s1)
Current Store : [0x80001478] : sd a3, 1752(s1) -- Store: [0x8000be70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000148c]:fsgnjx.s t6, t5, t4
	-[0x80001490]:csrrs a3, fcsr, zero
	-[0x80001494]:sd t6, 1760(s1)
Current Store : [0x80001498] : sd a3, 1768(s1) -- Store: [0x8000be80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ac]:fsgnjx.s t6, t5, t4
	-[0x800014b0]:csrrs a3, fcsr, zero
	-[0x800014b4]:sd t6, 1776(s1)
Current Store : [0x800014b8] : sd a3, 1784(s1) -- Store: [0x8000be90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800014cc]:fsgnjx.s t6, t5, t4
	-[0x800014d0]:csrrs a3, fcsr, zero
	-[0x800014d4]:sd t6, 1792(s1)
Current Store : [0x800014d8] : sd a3, 1800(s1) -- Store: [0x8000bea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ec]:fsgnjx.s t6, t5, t4
	-[0x800014f0]:csrrs a3, fcsr, zero
	-[0x800014f4]:sd t6, 1808(s1)
Current Store : [0x800014f8] : sd a3, 1816(s1) -- Store: [0x8000beb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fsgnjx.s t6, t5, t4
	-[0x80001510]:csrrs a3, fcsr, zero
	-[0x80001514]:sd t6, 1824(s1)
Current Store : [0x80001518] : sd a3, 1832(s1) -- Store: [0x8000bec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000152c]:fsgnjx.s t6, t5, t4
	-[0x80001530]:csrrs a3, fcsr, zero
	-[0x80001534]:sd t6, 1840(s1)
Current Store : [0x80001538] : sd a3, 1848(s1) -- Store: [0x8000bed0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fsgnjx.s t6, t5, t4
	-[0x80001550]:csrrs a3, fcsr, zero
	-[0x80001554]:sd t6, 1856(s1)
Current Store : [0x80001558] : sd a3, 1864(s1) -- Store: [0x8000bee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000156c]:fsgnjx.s t6, t5, t4
	-[0x80001570]:csrrs a3, fcsr, zero
	-[0x80001574]:sd t6, 1872(s1)
Current Store : [0x80001578] : sd a3, 1880(s1) -- Store: [0x8000bef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000158c]:fsgnjx.s t6, t5, t4
	-[0x80001590]:csrrs a3, fcsr, zero
	-[0x80001594]:sd t6, 1888(s1)
Current Store : [0x80001598] : sd a3, 1896(s1) -- Store: [0x8000bf00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800015ac]:fsgnjx.s t6, t5, t4
	-[0x800015b0]:csrrs a3, fcsr, zero
	-[0x800015b4]:sd t6, 1904(s1)
Current Store : [0x800015b8] : sd a3, 1912(s1) -- Store: [0x8000bf10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015cc]:fsgnjx.s t6, t5, t4
	-[0x800015d0]:csrrs a3, fcsr, zero
	-[0x800015d4]:sd t6, 1920(s1)
Current Store : [0x800015d8] : sd a3, 1928(s1) -- Store: [0x8000bf20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800015ec]:fsgnjx.s t6, t5, t4
	-[0x800015f0]:csrrs a3, fcsr, zero
	-[0x800015f4]:sd t6, 1936(s1)
Current Store : [0x800015f8] : sd a3, 1944(s1) -- Store: [0x8000bf30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000160c]:fsgnjx.s t6, t5, t4
	-[0x80001610]:csrrs a3, fcsr, zero
	-[0x80001614]:sd t6, 1952(s1)
Current Store : [0x80001618] : sd a3, 1960(s1) -- Store: [0x8000bf40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000162c]:fsgnjx.s t6, t5, t4
	-[0x80001630]:csrrs a3, fcsr, zero
	-[0x80001634]:sd t6, 1968(s1)
Current Store : [0x80001638] : sd a3, 1976(s1) -- Store: [0x8000bf50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000164c]:fsgnjx.s t6, t5, t4
	-[0x80001650]:csrrs a3, fcsr, zero
	-[0x80001654]:sd t6, 1984(s1)
Current Store : [0x80001658] : sd a3, 1992(s1) -- Store: [0x8000bf60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000168c]:fsgnjx.s t6, t5, t4
	-[0x80001690]:csrrs a3, fcsr, zero
	-[0x80001694]:sd t6, 2000(s1)
Current Store : [0x80001698] : sd a3, 2008(s1) -- Store: [0x8000bf70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800016cc]:fsgnjx.s t6, t5, t4
	-[0x800016d0]:csrrs a3, fcsr, zero
	-[0x800016d4]:sd t6, 2016(s1)
Current Store : [0x800016d8] : sd a3, 2024(s1) -- Store: [0x8000bf80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000170c]:fsgnjx.s t6, t5, t4
	-[0x80001710]:csrrs a3, fcsr, zero
	-[0x80001714]:sd t6, 2032(s1)
Current Store : [0x80001718] : sd a3, 2040(s1) -- Store: [0x8000bf90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001754]:fsgnjx.s t6, t5, t4
	-[0x80001758]:csrrs a3, fcsr, zero
	-[0x8000175c]:sd t6, 0(s1)
Current Store : [0x80001760] : sd a3, 8(s1) -- Store: [0x8000bfa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001794]:fsgnjx.s t6, t5, t4
	-[0x80001798]:csrrs a3, fcsr, zero
	-[0x8000179c]:sd t6, 16(s1)
Current Store : [0x800017a0] : sd a3, 24(s1) -- Store: [0x8000bfb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800017d4]:fsgnjx.s t6, t5, t4
	-[0x800017d8]:csrrs a3, fcsr, zero
	-[0x800017dc]:sd t6, 32(s1)
Current Store : [0x800017e0] : sd a3, 40(s1) -- Store: [0x8000bfc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001814]:fsgnjx.s t6, t5, t4
	-[0x80001818]:csrrs a3, fcsr, zero
	-[0x8000181c]:sd t6, 48(s1)
Current Store : [0x80001820] : sd a3, 56(s1) -- Store: [0x8000bfd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001854]:fsgnjx.s t6, t5, t4
	-[0x80001858]:csrrs a3, fcsr, zero
	-[0x8000185c]:sd t6, 64(s1)
Current Store : [0x80001860] : sd a3, 72(s1) -- Store: [0x8000bfe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001894]:fsgnjx.s t6, t5, t4
	-[0x80001898]:csrrs a3, fcsr, zero
	-[0x8000189c]:sd t6, 80(s1)
Current Store : [0x800018a0] : sd a3, 88(s1) -- Store: [0x8000bff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800018d4]:fsgnjx.s t6, t5, t4
	-[0x800018d8]:csrrs a3, fcsr, zero
	-[0x800018dc]:sd t6, 96(s1)
Current Store : [0x800018e0] : sd a3, 104(s1) -- Store: [0x8000c000]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001914]:fsgnjx.s t6, t5, t4
	-[0x80001918]:csrrs a3, fcsr, zero
	-[0x8000191c]:sd t6, 112(s1)
Current Store : [0x80001920] : sd a3, 120(s1) -- Store: [0x8000c010]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001954]:fsgnjx.s t6, t5, t4
	-[0x80001958]:csrrs a3, fcsr, zero
	-[0x8000195c]:sd t6, 128(s1)
Current Store : [0x80001960] : sd a3, 136(s1) -- Store: [0x8000c020]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001994]:fsgnjx.s t6, t5, t4
	-[0x80001998]:csrrs a3, fcsr, zero
	-[0x8000199c]:sd t6, 144(s1)
Current Store : [0x800019a0] : sd a3, 152(s1) -- Store: [0x8000c030]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800019d4]:fsgnjx.s t6, t5, t4
	-[0x800019d8]:csrrs a3, fcsr, zero
	-[0x800019dc]:sd t6, 160(s1)
Current Store : [0x800019e0] : sd a3, 168(s1) -- Store: [0x8000c040]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001a14]:fsgnjx.s t6, t5, t4
	-[0x80001a18]:csrrs a3, fcsr, zero
	-[0x80001a1c]:sd t6, 176(s1)
Current Store : [0x80001a20] : sd a3, 184(s1) -- Store: [0x8000c050]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001a54]:fsgnjx.s t6, t5, t4
	-[0x80001a58]:csrrs a3, fcsr, zero
	-[0x80001a5c]:sd t6, 192(s1)
Current Store : [0x80001a60] : sd a3, 200(s1) -- Store: [0x8000c060]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001a94]:fsgnjx.s t6, t5, t4
	-[0x80001a98]:csrrs a3, fcsr, zero
	-[0x80001a9c]:sd t6, 208(s1)
Current Store : [0x80001aa0] : sd a3, 216(s1) -- Store: [0x8000c070]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ad4]:fsgnjx.s t6, t5, t4
	-[0x80001ad8]:csrrs a3, fcsr, zero
	-[0x80001adc]:sd t6, 224(s1)
Current Store : [0x80001ae0] : sd a3, 232(s1) -- Store: [0x8000c080]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001b14]:fsgnjx.s t6, t5, t4
	-[0x80001b18]:csrrs a3, fcsr, zero
	-[0x80001b1c]:sd t6, 240(s1)
Current Store : [0x80001b20] : sd a3, 248(s1) -- Store: [0x8000c090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001b54]:fsgnjx.s t6, t5, t4
	-[0x80001b58]:csrrs a3, fcsr, zero
	-[0x80001b5c]:sd t6, 256(s1)
Current Store : [0x80001b60] : sd a3, 264(s1) -- Store: [0x8000c0a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001b94]:fsgnjx.s t6, t5, t4
	-[0x80001b98]:csrrs a3, fcsr, zero
	-[0x80001b9c]:sd t6, 272(s1)
Current Store : [0x80001ba0] : sd a3, 280(s1) -- Store: [0x8000c0b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001bd4]:fsgnjx.s t6, t5, t4
	-[0x80001bd8]:csrrs a3, fcsr, zero
	-[0x80001bdc]:sd t6, 288(s1)
Current Store : [0x80001be0] : sd a3, 296(s1) -- Store: [0x8000c0c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001c14]:fsgnjx.s t6, t5, t4
	-[0x80001c18]:csrrs a3, fcsr, zero
	-[0x80001c1c]:sd t6, 304(s1)
Current Store : [0x80001c20] : sd a3, 312(s1) -- Store: [0x8000c0d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001c54]:fsgnjx.s t6, t5, t4
	-[0x80001c58]:csrrs a3, fcsr, zero
	-[0x80001c5c]:sd t6, 320(s1)
Current Store : [0x80001c60] : sd a3, 328(s1) -- Store: [0x8000c0e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001c94]:fsgnjx.s t6, t5, t4
	-[0x80001c98]:csrrs a3, fcsr, zero
	-[0x80001c9c]:sd t6, 336(s1)
Current Store : [0x80001ca0] : sd a3, 344(s1) -- Store: [0x8000c0f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001cd4]:fsgnjx.s t6, t5, t4
	-[0x80001cd8]:csrrs a3, fcsr, zero
	-[0x80001cdc]:sd t6, 352(s1)
Current Store : [0x80001ce0] : sd a3, 360(s1) -- Store: [0x8000c100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001d14]:fsgnjx.s t6, t5, t4
	-[0x80001d18]:csrrs a3, fcsr, zero
	-[0x80001d1c]:sd t6, 368(s1)
Current Store : [0x80001d20] : sd a3, 376(s1) -- Store: [0x8000c110]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001d54]:fsgnjx.s t6, t5, t4
	-[0x80001d58]:csrrs a3, fcsr, zero
	-[0x80001d5c]:sd t6, 384(s1)
Current Store : [0x80001d60] : sd a3, 392(s1) -- Store: [0x8000c120]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001d94]:fsgnjx.s t6, t5, t4
	-[0x80001d98]:csrrs a3, fcsr, zero
	-[0x80001d9c]:sd t6, 400(s1)
Current Store : [0x80001da0] : sd a3, 408(s1) -- Store: [0x8000c130]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001dd4]:fsgnjx.s t6, t5, t4
	-[0x80001dd8]:csrrs a3, fcsr, zero
	-[0x80001ddc]:sd t6, 416(s1)
Current Store : [0x80001de0] : sd a3, 424(s1) -- Store: [0x8000c140]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001e14]:fsgnjx.s t6, t5, t4
	-[0x80001e18]:csrrs a3, fcsr, zero
	-[0x80001e1c]:sd t6, 432(s1)
Current Store : [0x80001e20] : sd a3, 440(s1) -- Store: [0x8000c150]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001e54]:fsgnjx.s t6, t5, t4
	-[0x80001e58]:csrrs a3, fcsr, zero
	-[0x80001e5c]:sd t6, 448(s1)
Current Store : [0x80001e60] : sd a3, 456(s1) -- Store: [0x8000c160]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001e94]:fsgnjx.s t6, t5, t4
	-[0x80001e98]:csrrs a3, fcsr, zero
	-[0x80001e9c]:sd t6, 464(s1)
Current Store : [0x80001ea0] : sd a3, 472(s1) -- Store: [0x8000c170]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ed4]:fsgnjx.s t6, t5, t4
	-[0x80001ed8]:csrrs a3, fcsr, zero
	-[0x80001edc]:sd t6, 480(s1)
Current Store : [0x80001ee0] : sd a3, 488(s1) -- Store: [0x8000c180]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001f14]:fsgnjx.s t6, t5, t4
	-[0x80001f18]:csrrs a3, fcsr, zero
	-[0x80001f1c]:sd t6, 496(s1)
Current Store : [0x80001f20] : sd a3, 504(s1) -- Store: [0x8000c190]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001f54]:fsgnjx.s t6, t5, t4
	-[0x80001f58]:csrrs a3, fcsr, zero
	-[0x80001f5c]:sd t6, 512(s1)
Current Store : [0x80001f60] : sd a3, 520(s1) -- Store: [0x8000c1a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001f94]:fsgnjx.s t6, t5, t4
	-[0x80001f98]:csrrs a3, fcsr, zero
	-[0x80001f9c]:sd t6, 528(s1)
Current Store : [0x80001fa0] : sd a3, 536(s1) -- Store: [0x8000c1b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001fd4]:fsgnjx.s t6, t5, t4
	-[0x80001fd8]:csrrs a3, fcsr, zero
	-[0x80001fdc]:sd t6, 544(s1)
Current Store : [0x80001fe0] : sd a3, 552(s1) -- Store: [0x8000c1c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002014]:fsgnjx.s t6, t5, t4
	-[0x80002018]:csrrs a3, fcsr, zero
	-[0x8000201c]:sd t6, 560(s1)
Current Store : [0x80002020] : sd a3, 568(s1) -- Store: [0x8000c1d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002054]:fsgnjx.s t6, t5, t4
	-[0x80002058]:csrrs a3, fcsr, zero
	-[0x8000205c]:sd t6, 576(s1)
Current Store : [0x80002060] : sd a3, 584(s1) -- Store: [0x8000c1e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002094]:fsgnjx.s t6, t5, t4
	-[0x80002098]:csrrs a3, fcsr, zero
	-[0x8000209c]:sd t6, 592(s1)
Current Store : [0x800020a0] : sd a3, 600(s1) -- Store: [0x8000c1f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800020d4]:fsgnjx.s t6, t5, t4
	-[0x800020d8]:csrrs a3, fcsr, zero
	-[0x800020dc]:sd t6, 608(s1)
Current Store : [0x800020e0] : sd a3, 616(s1) -- Store: [0x8000c200]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002114]:fsgnjx.s t6, t5, t4
	-[0x80002118]:csrrs a3, fcsr, zero
	-[0x8000211c]:sd t6, 624(s1)
Current Store : [0x80002120] : sd a3, 632(s1) -- Store: [0x8000c210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002154]:fsgnjx.s t6, t5, t4
	-[0x80002158]:csrrs a3, fcsr, zero
	-[0x8000215c]:sd t6, 640(s1)
Current Store : [0x80002160] : sd a3, 648(s1) -- Store: [0x8000c220]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002194]:fsgnjx.s t6, t5, t4
	-[0x80002198]:csrrs a3, fcsr, zero
	-[0x8000219c]:sd t6, 656(s1)
Current Store : [0x800021a0] : sd a3, 664(s1) -- Store: [0x8000c230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800021d4]:fsgnjx.s t6, t5, t4
	-[0x800021d8]:csrrs a3, fcsr, zero
	-[0x800021dc]:sd t6, 672(s1)
Current Store : [0x800021e0] : sd a3, 680(s1) -- Store: [0x8000c240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002214]:fsgnjx.s t6, t5, t4
	-[0x80002218]:csrrs a3, fcsr, zero
	-[0x8000221c]:sd t6, 688(s1)
Current Store : [0x80002220] : sd a3, 696(s1) -- Store: [0x8000c250]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002254]:fsgnjx.s t6, t5, t4
	-[0x80002258]:csrrs a3, fcsr, zero
	-[0x8000225c]:sd t6, 704(s1)
Current Store : [0x80002260] : sd a3, 712(s1) -- Store: [0x8000c260]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002294]:fsgnjx.s t6, t5, t4
	-[0x80002298]:csrrs a3, fcsr, zero
	-[0x8000229c]:sd t6, 720(s1)
Current Store : [0x800022a0] : sd a3, 728(s1) -- Store: [0x8000c270]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800022d4]:fsgnjx.s t6, t5, t4
	-[0x800022d8]:csrrs a3, fcsr, zero
	-[0x800022dc]:sd t6, 736(s1)
Current Store : [0x800022e0] : sd a3, 744(s1) -- Store: [0x8000c280]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002314]:fsgnjx.s t6, t5, t4
	-[0x80002318]:csrrs a3, fcsr, zero
	-[0x8000231c]:sd t6, 752(s1)
Current Store : [0x80002320] : sd a3, 760(s1) -- Store: [0x8000c290]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002354]:fsgnjx.s t6, t5, t4
	-[0x80002358]:csrrs a3, fcsr, zero
	-[0x8000235c]:sd t6, 768(s1)
Current Store : [0x80002360] : sd a3, 776(s1) -- Store: [0x8000c2a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002394]:fsgnjx.s t6, t5, t4
	-[0x80002398]:csrrs a3, fcsr, zero
	-[0x8000239c]:sd t6, 784(s1)
Current Store : [0x800023a0] : sd a3, 792(s1) -- Store: [0x8000c2b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800023d4]:fsgnjx.s t6, t5, t4
	-[0x800023d8]:csrrs a3, fcsr, zero
	-[0x800023dc]:sd t6, 800(s1)
Current Store : [0x800023e0] : sd a3, 808(s1) -- Store: [0x8000c2c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002414]:fsgnjx.s t6, t5, t4
	-[0x80002418]:csrrs a3, fcsr, zero
	-[0x8000241c]:sd t6, 816(s1)
Current Store : [0x80002420] : sd a3, 824(s1) -- Store: [0x8000c2d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002454]:fsgnjx.s t6, t5, t4
	-[0x80002458]:csrrs a3, fcsr, zero
	-[0x8000245c]:sd t6, 832(s1)
Current Store : [0x80002460] : sd a3, 840(s1) -- Store: [0x8000c2e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002494]:fsgnjx.s t6, t5, t4
	-[0x80002498]:csrrs a3, fcsr, zero
	-[0x8000249c]:sd t6, 848(s1)
Current Store : [0x800024a0] : sd a3, 856(s1) -- Store: [0x8000c2f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800024d4]:fsgnjx.s t6, t5, t4
	-[0x800024d8]:csrrs a3, fcsr, zero
	-[0x800024dc]:sd t6, 864(s1)
Current Store : [0x800024e0] : sd a3, 872(s1) -- Store: [0x8000c300]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002514]:fsgnjx.s t6, t5, t4
	-[0x80002518]:csrrs a3, fcsr, zero
	-[0x8000251c]:sd t6, 880(s1)
Current Store : [0x80002520] : sd a3, 888(s1) -- Store: [0x8000c310]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002554]:fsgnjx.s t6, t5, t4
	-[0x80002558]:csrrs a3, fcsr, zero
	-[0x8000255c]:sd t6, 896(s1)
Current Store : [0x80002560] : sd a3, 904(s1) -- Store: [0x8000c320]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002594]:fsgnjx.s t6, t5, t4
	-[0x80002598]:csrrs a3, fcsr, zero
	-[0x8000259c]:sd t6, 912(s1)
Current Store : [0x800025a0] : sd a3, 920(s1) -- Store: [0x8000c330]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800025d4]:fsgnjx.s t6, t5, t4
	-[0x800025d8]:csrrs a3, fcsr, zero
	-[0x800025dc]:sd t6, 928(s1)
Current Store : [0x800025e0] : sd a3, 936(s1) -- Store: [0x8000c340]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002614]:fsgnjx.s t6, t5, t4
	-[0x80002618]:csrrs a3, fcsr, zero
	-[0x8000261c]:sd t6, 944(s1)
Current Store : [0x80002620] : sd a3, 952(s1) -- Store: [0x8000c350]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002654]:fsgnjx.s t6, t5, t4
	-[0x80002658]:csrrs a3, fcsr, zero
	-[0x8000265c]:sd t6, 960(s1)
Current Store : [0x80002660] : sd a3, 968(s1) -- Store: [0x8000c360]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002694]:fsgnjx.s t6, t5, t4
	-[0x80002698]:csrrs a3, fcsr, zero
	-[0x8000269c]:sd t6, 976(s1)
Current Store : [0x800026a0] : sd a3, 984(s1) -- Store: [0x8000c370]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800026d4]:fsgnjx.s t6, t5, t4
	-[0x800026d8]:csrrs a3, fcsr, zero
	-[0x800026dc]:sd t6, 992(s1)
Current Store : [0x800026e0] : sd a3, 1000(s1) -- Store: [0x8000c380]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002714]:fsgnjx.s t6, t5, t4
	-[0x80002718]:csrrs a3, fcsr, zero
	-[0x8000271c]:sd t6, 1008(s1)
Current Store : [0x80002720] : sd a3, 1016(s1) -- Store: [0x8000c390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002754]:fsgnjx.s t6, t5, t4
	-[0x80002758]:csrrs a3, fcsr, zero
	-[0x8000275c]:sd t6, 1024(s1)
Current Store : [0x80002760] : sd a3, 1032(s1) -- Store: [0x8000c3a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002794]:fsgnjx.s t6, t5, t4
	-[0x80002798]:csrrs a3, fcsr, zero
	-[0x8000279c]:sd t6, 1040(s1)
Current Store : [0x800027a0] : sd a3, 1048(s1) -- Store: [0x8000c3b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800027d4]:fsgnjx.s t6, t5, t4
	-[0x800027d8]:csrrs a3, fcsr, zero
	-[0x800027dc]:sd t6, 1056(s1)
Current Store : [0x800027e0] : sd a3, 1064(s1) -- Store: [0x8000c3c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002814]:fsgnjx.s t6, t5, t4
	-[0x80002818]:csrrs a3, fcsr, zero
	-[0x8000281c]:sd t6, 1072(s1)
Current Store : [0x80002820] : sd a3, 1080(s1) -- Store: [0x8000c3d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002854]:fsgnjx.s t6, t5, t4
	-[0x80002858]:csrrs a3, fcsr, zero
	-[0x8000285c]:sd t6, 1088(s1)
Current Store : [0x80002860] : sd a3, 1096(s1) -- Store: [0x8000c3e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002894]:fsgnjx.s t6, t5, t4
	-[0x80002898]:csrrs a3, fcsr, zero
	-[0x8000289c]:sd t6, 1104(s1)
Current Store : [0x800028a0] : sd a3, 1112(s1) -- Store: [0x8000c3f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800028d4]:fsgnjx.s t6, t5, t4
	-[0x800028d8]:csrrs a3, fcsr, zero
	-[0x800028dc]:sd t6, 1120(s1)
Current Store : [0x800028e0] : sd a3, 1128(s1) -- Store: [0x8000c400]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002914]:fsgnjx.s t6, t5, t4
	-[0x80002918]:csrrs a3, fcsr, zero
	-[0x8000291c]:sd t6, 1136(s1)
Current Store : [0x80002920] : sd a3, 1144(s1) -- Store: [0x8000c410]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002954]:fsgnjx.s t6, t5, t4
	-[0x80002958]:csrrs a3, fcsr, zero
	-[0x8000295c]:sd t6, 1152(s1)
Current Store : [0x80002960] : sd a3, 1160(s1) -- Store: [0x8000c420]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002994]:fsgnjx.s t6, t5, t4
	-[0x80002998]:csrrs a3, fcsr, zero
	-[0x8000299c]:sd t6, 1168(s1)
Current Store : [0x800029a0] : sd a3, 1176(s1) -- Store: [0x8000c430]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800029d4]:fsgnjx.s t6, t5, t4
	-[0x800029d8]:csrrs a3, fcsr, zero
	-[0x800029dc]:sd t6, 1184(s1)
Current Store : [0x800029e0] : sd a3, 1192(s1) -- Store: [0x8000c440]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002a14]:fsgnjx.s t6, t5, t4
	-[0x80002a18]:csrrs a3, fcsr, zero
	-[0x80002a1c]:sd t6, 1200(s1)
Current Store : [0x80002a20] : sd a3, 1208(s1) -- Store: [0x8000c450]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002a54]:fsgnjx.s t6, t5, t4
	-[0x80002a58]:csrrs a3, fcsr, zero
	-[0x80002a5c]:sd t6, 1216(s1)
Current Store : [0x80002a60] : sd a3, 1224(s1) -- Store: [0x8000c460]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002a94]:fsgnjx.s t6, t5, t4
	-[0x80002a98]:csrrs a3, fcsr, zero
	-[0x80002a9c]:sd t6, 1232(s1)
Current Store : [0x80002aa0] : sd a3, 1240(s1) -- Store: [0x8000c470]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002ad4]:fsgnjx.s t6, t5, t4
	-[0x80002ad8]:csrrs a3, fcsr, zero
	-[0x80002adc]:sd t6, 1248(s1)
Current Store : [0x80002ae0] : sd a3, 1256(s1) -- Store: [0x8000c480]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002b14]:fsgnjx.s t6, t5, t4
	-[0x80002b18]:csrrs a3, fcsr, zero
	-[0x80002b1c]:sd t6, 1264(s1)
Current Store : [0x80002b20] : sd a3, 1272(s1) -- Store: [0x8000c490]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002b54]:fsgnjx.s t6, t5, t4
	-[0x80002b58]:csrrs a3, fcsr, zero
	-[0x80002b5c]:sd t6, 1280(s1)
Current Store : [0x80002b60] : sd a3, 1288(s1) -- Store: [0x8000c4a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002b94]:fsgnjx.s t6, t5, t4
	-[0x80002b98]:csrrs a3, fcsr, zero
	-[0x80002b9c]:sd t6, 1296(s1)
Current Store : [0x80002ba0] : sd a3, 1304(s1) -- Store: [0x8000c4b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002bd4]:fsgnjx.s t6, t5, t4
	-[0x80002bd8]:csrrs a3, fcsr, zero
	-[0x80002bdc]:sd t6, 1312(s1)
Current Store : [0x80002be0] : sd a3, 1320(s1) -- Store: [0x8000c4c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002c14]:fsgnjx.s t6, t5, t4
	-[0x80002c18]:csrrs a3, fcsr, zero
	-[0x80002c1c]:sd t6, 1328(s1)
Current Store : [0x80002c20] : sd a3, 1336(s1) -- Store: [0x8000c4d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002c54]:fsgnjx.s t6, t5, t4
	-[0x80002c58]:csrrs a3, fcsr, zero
	-[0x80002c5c]:sd t6, 1344(s1)
Current Store : [0x80002c60] : sd a3, 1352(s1) -- Store: [0x8000c4e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002c94]:fsgnjx.s t6, t5, t4
	-[0x80002c98]:csrrs a3, fcsr, zero
	-[0x80002c9c]:sd t6, 1360(s1)
Current Store : [0x80002ca0] : sd a3, 1368(s1) -- Store: [0x8000c4f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002cd4]:fsgnjx.s t6, t5, t4
	-[0x80002cd8]:csrrs a3, fcsr, zero
	-[0x80002cdc]:sd t6, 1376(s1)
Current Store : [0x80002ce0] : sd a3, 1384(s1) -- Store: [0x8000c500]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002d14]:fsgnjx.s t6, t5, t4
	-[0x80002d18]:csrrs a3, fcsr, zero
	-[0x80002d1c]:sd t6, 1392(s1)
Current Store : [0x80002d20] : sd a3, 1400(s1) -- Store: [0x8000c510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002d54]:fsgnjx.s t6, t5, t4
	-[0x80002d58]:csrrs a3, fcsr, zero
	-[0x80002d5c]:sd t6, 1408(s1)
Current Store : [0x80002d60] : sd a3, 1416(s1) -- Store: [0x8000c520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002d94]:fsgnjx.s t6, t5, t4
	-[0x80002d98]:csrrs a3, fcsr, zero
	-[0x80002d9c]:sd t6, 1424(s1)
Current Store : [0x80002da0] : sd a3, 1432(s1) -- Store: [0x8000c530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002dd4]:fsgnjx.s t6, t5, t4
	-[0x80002dd8]:csrrs a3, fcsr, zero
	-[0x80002ddc]:sd t6, 1440(s1)
Current Store : [0x80002de0] : sd a3, 1448(s1) -- Store: [0x8000c540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002e14]:fsgnjx.s t6, t5, t4
	-[0x80002e18]:csrrs a3, fcsr, zero
	-[0x80002e1c]:sd t6, 1456(s1)
Current Store : [0x80002e20] : sd a3, 1464(s1) -- Store: [0x8000c550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002e54]:fsgnjx.s t6, t5, t4
	-[0x80002e58]:csrrs a3, fcsr, zero
	-[0x80002e5c]:sd t6, 1472(s1)
Current Store : [0x80002e60] : sd a3, 1480(s1) -- Store: [0x8000c560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002e94]:fsgnjx.s t6, t5, t4
	-[0x80002e98]:csrrs a3, fcsr, zero
	-[0x80002e9c]:sd t6, 1488(s1)
Current Store : [0x80002ea0] : sd a3, 1496(s1) -- Store: [0x8000c570]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002ed4]:fsgnjx.s t6, t5, t4
	-[0x80002ed8]:csrrs a3, fcsr, zero
	-[0x80002edc]:sd t6, 1504(s1)
Current Store : [0x80002ee0] : sd a3, 1512(s1) -- Store: [0x8000c580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002f14]:fsgnjx.s t6, t5, t4
	-[0x80002f18]:csrrs a3, fcsr, zero
	-[0x80002f1c]:sd t6, 1520(s1)
Current Store : [0x80002f20] : sd a3, 1528(s1) -- Store: [0x8000c590]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002f54]:fsgnjx.s t6, t5, t4
	-[0x80002f58]:csrrs a3, fcsr, zero
	-[0x80002f5c]:sd t6, 1536(s1)
Current Store : [0x80002f60] : sd a3, 1544(s1) -- Store: [0x8000c5a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002f94]:fsgnjx.s t6, t5, t4
	-[0x80002f98]:csrrs a3, fcsr, zero
	-[0x80002f9c]:sd t6, 1552(s1)
Current Store : [0x80002fa0] : sd a3, 1560(s1) -- Store: [0x8000c5b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002fd4]:fsgnjx.s t6, t5, t4
	-[0x80002fd8]:csrrs a3, fcsr, zero
	-[0x80002fdc]:sd t6, 1568(s1)
Current Store : [0x80002fe0] : sd a3, 1576(s1) -- Store: [0x8000c5c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003014]:fsgnjx.s t6, t5, t4
	-[0x80003018]:csrrs a3, fcsr, zero
	-[0x8000301c]:sd t6, 1584(s1)
Current Store : [0x80003020] : sd a3, 1592(s1) -- Store: [0x8000c5d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003054]:fsgnjx.s t6, t5, t4
	-[0x80003058]:csrrs a3, fcsr, zero
	-[0x8000305c]:sd t6, 1600(s1)
Current Store : [0x80003060] : sd a3, 1608(s1) -- Store: [0x8000c5e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003094]:fsgnjx.s t6, t5, t4
	-[0x80003098]:csrrs a3, fcsr, zero
	-[0x8000309c]:sd t6, 1616(s1)
Current Store : [0x800030a0] : sd a3, 1624(s1) -- Store: [0x8000c5f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800030d4]:fsgnjx.s t6, t5, t4
	-[0x800030d8]:csrrs a3, fcsr, zero
	-[0x800030dc]:sd t6, 1632(s1)
Current Store : [0x800030e0] : sd a3, 1640(s1) -- Store: [0x8000c600]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003114]:fsgnjx.s t6, t5, t4
	-[0x80003118]:csrrs a3, fcsr, zero
	-[0x8000311c]:sd t6, 1648(s1)
Current Store : [0x80003120] : sd a3, 1656(s1) -- Store: [0x8000c610]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003154]:fsgnjx.s t6, t5, t4
	-[0x80003158]:csrrs a3, fcsr, zero
	-[0x8000315c]:sd t6, 1664(s1)
Current Store : [0x80003160] : sd a3, 1672(s1) -- Store: [0x8000c620]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003194]:fsgnjx.s t6, t5, t4
	-[0x80003198]:csrrs a3, fcsr, zero
	-[0x8000319c]:sd t6, 1680(s1)
Current Store : [0x800031a0] : sd a3, 1688(s1) -- Store: [0x8000c630]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800031d4]:fsgnjx.s t6, t5, t4
	-[0x800031d8]:csrrs a3, fcsr, zero
	-[0x800031dc]:sd t6, 1696(s1)
Current Store : [0x800031e0] : sd a3, 1704(s1) -- Store: [0x8000c640]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003214]:fsgnjx.s t6, t5, t4
	-[0x80003218]:csrrs a3, fcsr, zero
	-[0x8000321c]:sd t6, 1712(s1)
Current Store : [0x80003220] : sd a3, 1720(s1) -- Store: [0x8000c650]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003254]:fsgnjx.s t6, t5, t4
	-[0x80003258]:csrrs a3, fcsr, zero
	-[0x8000325c]:sd t6, 1728(s1)
Current Store : [0x80003260] : sd a3, 1736(s1) -- Store: [0x8000c660]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003294]:fsgnjx.s t6, t5, t4
	-[0x80003298]:csrrs a3, fcsr, zero
	-[0x8000329c]:sd t6, 1744(s1)
Current Store : [0x800032a0] : sd a3, 1752(s1) -- Store: [0x8000c670]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800032d4]:fsgnjx.s t6, t5, t4
	-[0x800032d8]:csrrs a3, fcsr, zero
	-[0x800032dc]:sd t6, 1760(s1)
Current Store : [0x800032e0] : sd a3, 1768(s1) -- Store: [0x8000c680]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003314]:fsgnjx.s t6, t5, t4
	-[0x80003318]:csrrs a3, fcsr, zero
	-[0x8000331c]:sd t6, 1776(s1)
Current Store : [0x80003320] : sd a3, 1784(s1) -- Store: [0x8000c690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003354]:fsgnjx.s t6, t5, t4
	-[0x80003358]:csrrs a3, fcsr, zero
	-[0x8000335c]:sd t6, 1792(s1)
Current Store : [0x80003360] : sd a3, 1800(s1) -- Store: [0x8000c6a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003394]:fsgnjx.s t6, t5, t4
	-[0x80003398]:csrrs a3, fcsr, zero
	-[0x8000339c]:sd t6, 1808(s1)
Current Store : [0x800033a0] : sd a3, 1816(s1) -- Store: [0x8000c6b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800033d4]:fsgnjx.s t6, t5, t4
	-[0x800033d8]:csrrs a3, fcsr, zero
	-[0x800033dc]:sd t6, 1824(s1)
Current Store : [0x800033e0] : sd a3, 1832(s1) -- Store: [0x8000c6c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003414]:fsgnjx.s t6, t5, t4
	-[0x80003418]:csrrs a3, fcsr, zero
	-[0x8000341c]:sd t6, 1840(s1)
Current Store : [0x80003420] : sd a3, 1848(s1) -- Store: [0x8000c6d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003454]:fsgnjx.s t6, t5, t4
	-[0x80003458]:csrrs a3, fcsr, zero
	-[0x8000345c]:sd t6, 1856(s1)
Current Store : [0x80003460] : sd a3, 1864(s1) -- Store: [0x8000c6e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003494]:fsgnjx.s t6, t5, t4
	-[0x80003498]:csrrs a3, fcsr, zero
	-[0x8000349c]:sd t6, 1872(s1)
Current Store : [0x800034a0] : sd a3, 1880(s1) -- Store: [0x8000c6f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800034d4]:fsgnjx.s t6, t5, t4
	-[0x800034d8]:csrrs a3, fcsr, zero
	-[0x800034dc]:sd t6, 1888(s1)
Current Store : [0x800034e0] : sd a3, 1896(s1) -- Store: [0x8000c700]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003514]:fsgnjx.s t6, t5, t4
	-[0x80003518]:csrrs a3, fcsr, zero
	-[0x8000351c]:sd t6, 1904(s1)
Current Store : [0x80003520] : sd a3, 1912(s1) -- Store: [0x8000c710]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003554]:fsgnjx.s t6, t5, t4
	-[0x80003558]:csrrs a3, fcsr, zero
	-[0x8000355c]:sd t6, 1920(s1)
Current Store : [0x80003560] : sd a3, 1928(s1) -- Store: [0x8000c720]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003594]:fsgnjx.s t6, t5, t4
	-[0x80003598]:csrrs a3, fcsr, zero
	-[0x8000359c]:sd t6, 1936(s1)
Current Store : [0x800035a0] : sd a3, 1944(s1) -- Store: [0x8000c730]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800035d4]:fsgnjx.s t6, t5, t4
	-[0x800035d8]:csrrs a3, fcsr, zero
	-[0x800035dc]:sd t6, 1952(s1)
Current Store : [0x800035e0] : sd a3, 1960(s1) -- Store: [0x8000c740]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003614]:fsgnjx.s t6, t5, t4
	-[0x80003618]:csrrs a3, fcsr, zero
	-[0x8000361c]:sd t6, 1968(s1)
Current Store : [0x80003620] : sd a3, 1976(s1) -- Store: [0x8000c750]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003654]:fsgnjx.s t6, t5, t4
	-[0x80003658]:csrrs a3, fcsr, zero
	-[0x8000365c]:sd t6, 1984(s1)
Current Store : [0x80003660] : sd a3, 1992(s1) -- Store: [0x8000c760]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000368c]:fsgnjx.s t6, t5, t4
	-[0x80003690]:csrrs a3, fcsr, zero
	-[0x80003694]:sd t6, 2000(s1)
Current Store : [0x80003698] : sd a3, 2008(s1) -- Store: [0x8000c770]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800036c4]:fsgnjx.s t6, t5, t4
	-[0x800036c8]:csrrs a3, fcsr, zero
	-[0x800036cc]:sd t6, 2016(s1)
Current Store : [0x800036d0] : sd a3, 2024(s1) -- Store: [0x8000c780]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800036fc]:fsgnjx.s t6, t5, t4
	-[0x80003700]:csrrs a3, fcsr, zero
	-[0x80003704]:sd t6, 2032(s1)
Current Store : [0x80003708] : sd a3, 2040(s1) -- Store: [0x8000c790]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000373c]:fsgnjx.s t6, t5, t4
	-[0x80003740]:csrrs a3, fcsr, zero
	-[0x80003744]:sd t6, 0(s1)
Current Store : [0x80003748] : sd a3, 8(s1) -- Store: [0x8000c7a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003774]:fsgnjx.s t6, t5, t4
	-[0x80003778]:csrrs a3, fcsr, zero
	-[0x8000377c]:sd t6, 16(s1)
Current Store : [0x80003780] : sd a3, 24(s1) -- Store: [0x8000c7b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800037ac]:fsgnjx.s t6, t5, t4
	-[0x800037b0]:csrrs a3, fcsr, zero
	-[0x800037b4]:sd t6, 32(s1)
Current Store : [0x800037b8] : sd a3, 40(s1) -- Store: [0x8000c7c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800037e4]:fsgnjx.s t6, t5, t4
	-[0x800037e8]:csrrs a3, fcsr, zero
	-[0x800037ec]:sd t6, 48(s1)
Current Store : [0x800037f0] : sd a3, 56(s1) -- Store: [0x8000c7d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000381c]:fsgnjx.s t6, t5, t4
	-[0x80003820]:csrrs a3, fcsr, zero
	-[0x80003824]:sd t6, 64(s1)
Current Store : [0x80003828] : sd a3, 72(s1) -- Store: [0x8000c7e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003854]:fsgnjx.s t6, t5, t4
	-[0x80003858]:csrrs a3, fcsr, zero
	-[0x8000385c]:sd t6, 80(s1)
Current Store : [0x80003860] : sd a3, 88(s1) -- Store: [0x8000c7f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000388c]:fsgnjx.s t6, t5, t4
	-[0x80003890]:csrrs a3, fcsr, zero
	-[0x80003894]:sd t6, 96(s1)
Current Store : [0x80003898] : sd a3, 104(s1) -- Store: [0x8000c800]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800038c4]:fsgnjx.s t6, t5, t4
	-[0x800038c8]:csrrs a3, fcsr, zero
	-[0x800038cc]:sd t6, 112(s1)
Current Store : [0x800038d0] : sd a3, 120(s1) -- Store: [0x8000c810]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800038fc]:fsgnjx.s t6, t5, t4
	-[0x80003900]:csrrs a3, fcsr, zero
	-[0x80003904]:sd t6, 128(s1)
Current Store : [0x80003908] : sd a3, 136(s1) -- Store: [0x8000c820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003934]:fsgnjx.s t6, t5, t4
	-[0x80003938]:csrrs a3, fcsr, zero
	-[0x8000393c]:sd t6, 144(s1)
Current Store : [0x80003940] : sd a3, 152(s1) -- Store: [0x8000c830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000396c]:fsgnjx.s t6, t5, t4
	-[0x80003970]:csrrs a3, fcsr, zero
	-[0x80003974]:sd t6, 160(s1)
Current Store : [0x80003978] : sd a3, 168(s1) -- Store: [0x8000c840]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800039a4]:fsgnjx.s t6, t5, t4
	-[0x800039a8]:csrrs a3, fcsr, zero
	-[0x800039ac]:sd t6, 176(s1)
Current Store : [0x800039b0] : sd a3, 184(s1) -- Store: [0x8000c850]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800039dc]:fsgnjx.s t6, t5, t4
	-[0x800039e0]:csrrs a3, fcsr, zero
	-[0x800039e4]:sd t6, 192(s1)
Current Store : [0x800039e8] : sd a3, 200(s1) -- Store: [0x8000c860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003a14]:fsgnjx.s t6, t5, t4
	-[0x80003a18]:csrrs a3, fcsr, zero
	-[0x80003a1c]:sd t6, 208(s1)
Current Store : [0x80003a20] : sd a3, 216(s1) -- Store: [0x8000c870]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003a4c]:fsgnjx.s t6, t5, t4
	-[0x80003a50]:csrrs a3, fcsr, zero
	-[0x80003a54]:sd t6, 224(s1)
Current Store : [0x80003a58] : sd a3, 232(s1) -- Store: [0x8000c880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003a84]:fsgnjx.s t6, t5, t4
	-[0x80003a88]:csrrs a3, fcsr, zero
	-[0x80003a8c]:sd t6, 240(s1)
Current Store : [0x80003a90] : sd a3, 248(s1) -- Store: [0x8000c890]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003abc]:fsgnjx.s t6, t5, t4
	-[0x80003ac0]:csrrs a3, fcsr, zero
	-[0x80003ac4]:sd t6, 256(s1)
Current Store : [0x80003ac8] : sd a3, 264(s1) -- Store: [0x8000c8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003af4]:fsgnjx.s t6, t5, t4
	-[0x80003af8]:csrrs a3, fcsr, zero
	-[0x80003afc]:sd t6, 272(s1)
Current Store : [0x80003b00] : sd a3, 280(s1) -- Store: [0x8000c8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b2c]:fsgnjx.s t6, t5, t4
	-[0x80003b30]:csrrs a3, fcsr, zero
	-[0x80003b34]:sd t6, 288(s1)
Current Store : [0x80003b38] : sd a3, 296(s1) -- Store: [0x8000c8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003b64]:fsgnjx.s t6, t5, t4
	-[0x80003b68]:csrrs a3, fcsr, zero
	-[0x80003b6c]:sd t6, 304(s1)
Current Store : [0x80003b70] : sd a3, 312(s1) -- Store: [0x8000c8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b9c]:fsgnjx.s t6, t5, t4
	-[0x80003ba0]:csrrs a3, fcsr, zero
	-[0x80003ba4]:sd t6, 320(s1)
Current Store : [0x80003ba8] : sd a3, 328(s1) -- Store: [0x8000c8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003bd4]:fsgnjx.s t6, t5, t4
	-[0x80003bd8]:csrrs a3, fcsr, zero
	-[0x80003bdc]:sd t6, 336(s1)
Current Store : [0x80003be0] : sd a3, 344(s1) -- Store: [0x8000c8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003c0c]:fsgnjx.s t6, t5, t4
	-[0x80003c10]:csrrs a3, fcsr, zero
	-[0x80003c14]:sd t6, 352(s1)
Current Store : [0x80003c18] : sd a3, 360(s1) -- Store: [0x8000c900]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c44]:fsgnjx.s t6, t5, t4
	-[0x80003c48]:csrrs a3, fcsr, zero
	-[0x80003c4c]:sd t6, 368(s1)
Current Store : [0x80003c50] : sd a3, 376(s1) -- Store: [0x8000c910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003c7c]:fsgnjx.s t6, t5, t4
	-[0x80003c80]:csrrs a3, fcsr, zero
	-[0x80003c84]:sd t6, 384(s1)
Current Store : [0x80003c88] : sd a3, 392(s1) -- Store: [0x8000c920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003cb4]:fsgnjx.s t6, t5, t4
	-[0x80003cb8]:csrrs a3, fcsr, zero
	-[0x80003cbc]:sd t6, 400(s1)
Current Store : [0x80003cc0] : sd a3, 408(s1) -- Store: [0x8000c930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003cec]:fsgnjx.s t6, t5, t4
	-[0x80003cf0]:csrrs a3, fcsr, zero
	-[0x80003cf4]:sd t6, 416(s1)
Current Store : [0x80003cf8] : sd a3, 424(s1) -- Store: [0x8000c940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d24]:fsgnjx.s t6, t5, t4
	-[0x80003d28]:csrrs a3, fcsr, zero
	-[0x80003d2c]:sd t6, 432(s1)
Current Store : [0x80003d30] : sd a3, 440(s1) -- Store: [0x8000c950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003d5c]:fsgnjx.s t6, t5, t4
	-[0x80003d60]:csrrs a3, fcsr, zero
	-[0x80003d64]:sd t6, 448(s1)
Current Store : [0x80003d68] : sd a3, 456(s1) -- Store: [0x8000c960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d94]:fsgnjx.s t6, t5, t4
	-[0x80003d98]:csrrs a3, fcsr, zero
	-[0x80003d9c]:sd t6, 464(s1)
Current Store : [0x80003da0] : sd a3, 472(s1) -- Store: [0x8000c970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003dcc]:fsgnjx.s t6, t5, t4
	-[0x80003dd0]:csrrs a3, fcsr, zero
	-[0x80003dd4]:sd t6, 480(s1)
Current Store : [0x80003dd8] : sd a3, 488(s1) -- Store: [0x8000c980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003e04]:fsgnjx.s t6, t5, t4
	-[0x80003e08]:csrrs a3, fcsr, zero
	-[0x80003e0c]:sd t6, 496(s1)
Current Store : [0x80003e10] : sd a3, 504(s1) -- Store: [0x8000c990]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003e3c]:fsgnjx.s t6, t5, t4
	-[0x80003e40]:csrrs a3, fcsr, zero
	-[0x80003e44]:sd t6, 512(s1)
Current Store : [0x80003e48] : sd a3, 520(s1) -- Store: [0x8000c9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003e74]:fsgnjx.s t6, t5, t4
	-[0x80003e78]:csrrs a3, fcsr, zero
	-[0x80003e7c]:sd t6, 528(s1)
Current Store : [0x80003e80] : sd a3, 536(s1) -- Store: [0x8000c9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003eac]:fsgnjx.s t6, t5, t4
	-[0x80003eb0]:csrrs a3, fcsr, zero
	-[0x80003eb4]:sd t6, 544(s1)
Current Store : [0x80003eb8] : sd a3, 552(s1) -- Store: [0x8000c9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003ee4]:fsgnjx.s t6, t5, t4
	-[0x80003ee8]:csrrs a3, fcsr, zero
	-[0x80003eec]:sd t6, 560(s1)
Current Store : [0x80003ef0] : sd a3, 568(s1) -- Store: [0x8000c9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003f1c]:fsgnjx.s t6, t5, t4
	-[0x80003f20]:csrrs a3, fcsr, zero
	-[0x80003f24]:sd t6, 576(s1)
Current Store : [0x80003f28] : sd a3, 584(s1) -- Store: [0x8000c9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003f54]:fsgnjx.s t6, t5, t4
	-[0x80003f58]:csrrs a3, fcsr, zero
	-[0x80003f5c]:sd t6, 592(s1)
Current Store : [0x80003f60] : sd a3, 600(s1) -- Store: [0x8000c9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003f8c]:fsgnjx.s t6, t5, t4
	-[0x80003f90]:csrrs a3, fcsr, zero
	-[0x80003f94]:sd t6, 608(s1)
Current Store : [0x80003f98] : sd a3, 616(s1) -- Store: [0x8000ca00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003fc4]:fsgnjx.s t6, t5, t4
	-[0x80003fc8]:csrrs a3, fcsr, zero
	-[0x80003fcc]:sd t6, 624(s1)
Current Store : [0x80003fd0] : sd a3, 632(s1) -- Store: [0x8000ca10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003ffc]:fsgnjx.s t6, t5, t4
	-[0x80004000]:csrrs a3, fcsr, zero
	-[0x80004004]:sd t6, 640(s1)
Current Store : [0x80004008] : sd a3, 648(s1) -- Store: [0x8000ca20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004034]:fsgnjx.s t6, t5, t4
	-[0x80004038]:csrrs a3, fcsr, zero
	-[0x8000403c]:sd t6, 656(s1)
Current Store : [0x80004040] : sd a3, 664(s1) -- Store: [0x8000ca30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000406c]:fsgnjx.s t6, t5, t4
	-[0x80004070]:csrrs a3, fcsr, zero
	-[0x80004074]:sd t6, 672(s1)
Current Store : [0x80004078] : sd a3, 680(s1) -- Store: [0x8000ca40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800040a4]:fsgnjx.s t6, t5, t4
	-[0x800040a8]:csrrs a3, fcsr, zero
	-[0x800040ac]:sd t6, 688(s1)
Current Store : [0x800040b0] : sd a3, 696(s1) -- Store: [0x8000ca50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800040dc]:fsgnjx.s t6, t5, t4
	-[0x800040e0]:csrrs a3, fcsr, zero
	-[0x800040e4]:sd t6, 704(s1)
Current Store : [0x800040e8] : sd a3, 712(s1) -- Store: [0x8000ca60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004114]:fsgnjx.s t6, t5, t4
	-[0x80004118]:csrrs a3, fcsr, zero
	-[0x8000411c]:sd t6, 720(s1)
Current Store : [0x80004120] : sd a3, 728(s1) -- Store: [0x8000ca70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000414c]:fsgnjx.s t6, t5, t4
	-[0x80004150]:csrrs a3, fcsr, zero
	-[0x80004154]:sd t6, 736(s1)
Current Store : [0x80004158] : sd a3, 744(s1) -- Store: [0x8000ca80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004184]:fsgnjx.s t6, t5, t4
	-[0x80004188]:csrrs a3, fcsr, zero
	-[0x8000418c]:sd t6, 752(s1)
Current Store : [0x80004190] : sd a3, 760(s1) -- Store: [0x8000ca90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800041bc]:fsgnjx.s t6, t5, t4
	-[0x800041c0]:csrrs a3, fcsr, zero
	-[0x800041c4]:sd t6, 768(s1)
Current Store : [0x800041c8] : sd a3, 776(s1) -- Store: [0x8000caa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800041f4]:fsgnjx.s t6, t5, t4
	-[0x800041f8]:csrrs a3, fcsr, zero
	-[0x800041fc]:sd t6, 784(s1)
Current Store : [0x80004200] : sd a3, 792(s1) -- Store: [0x8000cab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000422c]:fsgnjx.s t6, t5, t4
	-[0x80004230]:csrrs a3, fcsr, zero
	-[0x80004234]:sd t6, 800(s1)
Current Store : [0x80004238] : sd a3, 808(s1) -- Store: [0x8000cac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004264]:fsgnjx.s t6, t5, t4
	-[0x80004268]:csrrs a3, fcsr, zero
	-[0x8000426c]:sd t6, 816(s1)
Current Store : [0x80004270] : sd a3, 824(s1) -- Store: [0x8000cad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000429c]:fsgnjx.s t6, t5, t4
	-[0x800042a0]:csrrs a3, fcsr, zero
	-[0x800042a4]:sd t6, 832(s1)
Current Store : [0x800042a8] : sd a3, 840(s1) -- Store: [0x8000cae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800042d4]:fsgnjx.s t6, t5, t4
	-[0x800042d8]:csrrs a3, fcsr, zero
	-[0x800042dc]:sd t6, 848(s1)
Current Store : [0x800042e0] : sd a3, 856(s1) -- Store: [0x8000caf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000430c]:fsgnjx.s t6, t5, t4
	-[0x80004310]:csrrs a3, fcsr, zero
	-[0x80004314]:sd t6, 864(s1)
Current Store : [0x80004318] : sd a3, 872(s1) -- Store: [0x8000cb00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004344]:fsgnjx.s t6, t5, t4
	-[0x80004348]:csrrs a3, fcsr, zero
	-[0x8000434c]:sd t6, 880(s1)
Current Store : [0x80004350] : sd a3, 888(s1) -- Store: [0x8000cb10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000437c]:fsgnjx.s t6, t5, t4
	-[0x80004380]:csrrs a3, fcsr, zero
	-[0x80004384]:sd t6, 896(s1)
Current Store : [0x80004388] : sd a3, 904(s1) -- Store: [0x8000cb20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800043b4]:fsgnjx.s t6, t5, t4
	-[0x800043b8]:csrrs a3, fcsr, zero
	-[0x800043bc]:sd t6, 912(s1)
Current Store : [0x800043c0] : sd a3, 920(s1) -- Store: [0x8000cb30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800043ec]:fsgnjx.s t6, t5, t4
	-[0x800043f0]:csrrs a3, fcsr, zero
	-[0x800043f4]:sd t6, 928(s1)
Current Store : [0x800043f8] : sd a3, 936(s1) -- Store: [0x8000cb40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004424]:fsgnjx.s t6, t5, t4
	-[0x80004428]:csrrs a3, fcsr, zero
	-[0x8000442c]:sd t6, 944(s1)
Current Store : [0x80004430] : sd a3, 952(s1) -- Store: [0x8000cb50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000445c]:fsgnjx.s t6, t5, t4
	-[0x80004460]:csrrs a3, fcsr, zero
	-[0x80004464]:sd t6, 960(s1)
Current Store : [0x80004468] : sd a3, 968(s1) -- Store: [0x8000cb60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004494]:fsgnjx.s t6, t5, t4
	-[0x80004498]:csrrs a3, fcsr, zero
	-[0x8000449c]:sd t6, 976(s1)
Current Store : [0x800044a0] : sd a3, 984(s1) -- Store: [0x8000cb70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800044cc]:fsgnjx.s t6, t5, t4
	-[0x800044d0]:csrrs a3, fcsr, zero
	-[0x800044d4]:sd t6, 992(s1)
Current Store : [0x800044d8] : sd a3, 1000(s1) -- Store: [0x8000cb80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004504]:fsgnjx.s t6, t5, t4
	-[0x80004508]:csrrs a3, fcsr, zero
	-[0x8000450c]:sd t6, 1008(s1)
Current Store : [0x80004510] : sd a3, 1016(s1) -- Store: [0x8000cb90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000453c]:fsgnjx.s t6, t5, t4
	-[0x80004540]:csrrs a3, fcsr, zero
	-[0x80004544]:sd t6, 1024(s1)
Current Store : [0x80004548] : sd a3, 1032(s1) -- Store: [0x8000cba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004574]:fsgnjx.s t6, t5, t4
	-[0x80004578]:csrrs a3, fcsr, zero
	-[0x8000457c]:sd t6, 1040(s1)
Current Store : [0x80004580] : sd a3, 1048(s1) -- Store: [0x8000cbb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800045ac]:fsgnjx.s t6, t5, t4
	-[0x800045b0]:csrrs a3, fcsr, zero
	-[0x800045b4]:sd t6, 1056(s1)
Current Store : [0x800045b8] : sd a3, 1064(s1) -- Store: [0x8000cbc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800045e4]:fsgnjx.s t6, t5, t4
	-[0x800045e8]:csrrs a3, fcsr, zero
	-[0x800045ec]:sd t6, 1072(s1)
Current Store : [0x800045f0] : sd a3, 1080(s1) -- Store: [0x8000cbd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000461c]:fsgnjx.s t6, t5, t4
	-[0x80004620]:csrrs a3, fcsr, zero
	-[0x80004624]:sd t6, 1088(s1)
Current Store : [0x80004628] : sd a3, 1096(s1) -- Store: [0x8000cbe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004654]:fsgnjx.s t6, t5, t4
	-[0x80004658]:csrrs a3, fcsr, zero
	-[0x8000465c]:sd t6, 1104(s1)
Current Store : [0x80004660] : sd a3, 1112(s1) -- Store: [0x8000cbf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000468c]:fsgnjx.s t6, t5, t4
	-[0x80004690]:csrrs a3, fcsr, zero
	-[0x80004694]:sd t6, 1120(s1)
Current Store : [0x80004698] : sd a3, 1128(s1) -- Store: [0x8000cc00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800046c4]:fsgnjx.s t6, t5, t4
	-[0x800046c8]:csrrs a3, fcsr, zero
	-[0x800046cc]:sd t6, 1136(s1)
Current Store : [0x800046d0] : sd a3, 1144(s1) -- Store: [0x8000cc10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800046fc]:fsgnjx.s t6, t5, t4
	-[0x80004700]:csrrs a3, fcsr, zero
	-[0x80004704]:sd t6, 1152(s1)
Current Store : [0x80004708] : sd a3, 1160(s1) -- Store: [0x8000cc20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004734]:fsgnjx.s t6, t5, t4
	-[0x80004738]:csrrs a3, fcsr, zero
	-[0x8000473c]:sd t6, 1168(s1)
Current Store : [0x80004740] : sd a3, 1176(s1) -- Store: [0x8000cc30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000476c]:fsgnjx.s t6, t5, t4
	-[0x80004770]:csrrs a3, fcsr, zero
	-[0x80004774]:sd t6, 1184(s1)
Current Store : [0x80004778] : sd a3, 1192(s1) -- Store: [0x8000cc40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800047a4]:fsgnjx.s t6, t5, t4
	-[0x800047a8]:csrrs a3, fcsr, zero
	-[0x800047ac]:sd t6, 1200(s1)
Current Store : [0x800047b0] : sd a3, 1208(s1) -- Store: [0x8000cc50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800047dc]:fsgnjx.s t6, t5, t4
	-[0x800047e0]:csrrs a3, fcsr, zero
	-[0x800047e4]:sd t6, 1216(s1)
Current Store : [0x800047e8] : sd a3, 1224(s1) -- Store: [0x8000cc60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004814]:fsgnjx.s t6, t5, t4
	-[0x80004818]:csrrs a3, fcsr, zero
	-[0x8000481c]:sd t6, 1232(s1)
Current Store : [0x80004820] : sd a3, 1240(s1) -- Store: [0x8000cc70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000484c]:fsgnjx.s t6, t5, t4
	-[0x80004850]:csrrs a3, fcsr, zero
	-[0x80004854]:sd t6, 1248(s1)
Current Store : [0x80004858] : sd a3, 1256(s1) -- Store: [0x8000cc80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004884]:fsgnjx.s t6, t5, t4
	-[0x80004888]:csrrs a3, fcsr, zero
	-[0x8000488c]:sd t6, 1264(s1)
Current Store : [0x80004890] : sd a3, 1272(s1) -- Store: [0x8000cc90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800048bc]:fsgnjx.s t6, t5, t4
	-[0x800048c0]:csrrs a3, fcsr, zero
	-[0x800048c4]:sd t6, 1280(s1)
Current Store : [0x800048c8] : sd a3, 1288(s1) -- Store: [0x8000cca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800048f4]:fsgnjx.s t6, t5, t4
	-[0x800048f8]:csrrs a3, fcsr, zero
	-[0x800048fc]:sd t6, 1296(s1)
Current Store : [0x80004900] : sd a3, 1304(s1) -- Store: [0x8000ccb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000492c]:fsgnjx.s t6, t5, t4
	-[0x80004930]:csrrs a3, fcsr, zero
	-[0x80004934]:sd t6, 1312(s1)
Current Store : [0x80004938] : sd a3, 1320(s1) -- Store: [0x8000ccc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004964]:fsgnjx.s t6, t5, t4
	-[0x80004968]:csrrs a3, fcsr, zero
	-[0x8000496c]:sd t6, 1328(s1)
Current Store : [0x80004970] : sd a3, 1336(s1) -- Store: [0x8000ccd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000499c]:fsgnjx.s t6, t5, t4
	-[0x800049a0]:csrrs a3, fcsr, zero
	-[0x800049a4]:sd t6, 1344(s1)
Current Store : [0x800049a8] : sd a3, 1352(s1) -- Store: [0x8000cce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800049d4]:fsgnjx.s t6, t5, t4
	-[0x800049d8]:csrrs a3, fcsr, zero
	-[0x800049dc]:sd t6, 1360(s1)
Current Store : [0x800049e0] : sd a3, 1368(s1) -- Store: [0x8000ccf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004a0c]:fsgnjx.s t6, t5, t4
	-[0x80004a10]:csrrs a3, fcsr, zero
	-[0x80004a14]:sd t6, 1376(s1)
Current Store : [0x80004a18] : sd a3, 1384(s1) -- Store: [0x8000cd00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a44]:fsgnjx.s t6, t5, t4
	-[0x80004a48]:csrrs a3, fcsr, zero
	-[0x80004a4c]:sd t6, 1392(s1)
Current Store : [0x80004a50] : sd a3, 1400(s1) -- Store: [0x8000cd10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004a7c]:fsgnjx.s t6, t5, t4
	-[0x80004a80]:csrrs a3, fcsr, zero
	-[0x80004a84]:sd t6, 1408(s1)
Current Store : [0x80004a88] : sd a3, 1416(s1) -- Store: [0x8000cd20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ab4]:fsgnjx.s t6, t5, t4
	-[0x80004ab8]:csrrs a3, fcsr, zero
	-[0x80004abc]:sd t6, 1424(s1)
Current Store : [0x80004ac0] : sd a3, 1432(s1) -- Store: [0x8000cd30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004aec]:fsgnjx.s t6, t5, t4
	-[0x80004af0]:csrrs a3, fcsr, zero
	-[0x80004af4]:sd t6, 1440(s1)
Current Store : [0x80004af8] : sd a3, 1448(s1) -- Store: [0x8000cd40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b24]:fsgnjx.s t6, t5, t4
	-[0x80004b28]:csrrs a3, fcsr, zero
	-[0x80004b2c]:sd t6, 1456(s1)
Current Store : [0x80004b30] : sd a3, 1464(s1) -- Store: [0x8000cd50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004b5c]:fsgnjx.s t6, t5, t4
	-[0x80004b60]:csrrs a3, fcsr, zero
	-[0x80004b64]:sd t6, 1472(s1)
Current Store : [0x80004b68] : sd a3, 1480(s1) -- Store: [0x8000cd60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b94]:fsgnjx.s t6, t5, t4
	-[0x80004b98]:csrrs a3, fcsr, zero
	-[0x80004b9c]:sd t6, 1488(s1)
Current Store : [0x80004ba0] : sd a3, 1496(s1) -- Store: [0x8000cd70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004bcc]:fsgnjx.s t6, t5, t4
	-[0x80004bd0]:csrrs a3, fcsr, zero
	-[0x80004bd4]:sd t6, 1504(s1)
Current Store : [0x80004bd8] : sd a3, 1512(s1) -- Store: [0x8000cd80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c04]:fsgnjx.s t6, t5, t4
	-[0x80004c08]:csrrs a3, fcsr, zero
	-[0x80004c0c]:sd t6, 1520(s1)
Current Store : [0x80004c10] : sd a3, 1528(s1) -- Store: [0x8000cd90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004c3c]:fsgnjx.s t6, t5, t4
	-[0x80004c40]:csrrs a3, fcsr, zero
	-[0x80004c44]:sd t6, 1536(s1)
Current Store : [0x80004c48] : sd a3, 1544(s1) -- Store: [0x8000cda0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c74]:fsgnjx.s t6, t5, t4
	-[0x80004c78]:csrrs a3, fcsr, zero
	-[0x80004c7c]:sd t6, 1552(s1)
Current Store : [0x80004c80] : sd a3, 1560(s1) -- Store: [0x8000cdb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004cac]:fsgnjx.s t6, t5, t4
	-[0x80004cb0]:csrrs a3, fcsr, zero
	-[0x80004cb4]:sd t6, 1568(s1)
Current Store : [0x80004cb8] : sd a3, 1576(s1) -- Store: [0x8000cdc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ce4]:fsgnjx.s t6, t5, t4
	-[0x80004ce8]:csrrs a3, fcsr, zero
	-[0x80004cec]:sd t6, 1584(s1)
Current Store : [0x80004cf0] : sd a3, 1592(s1) -- Store: [0x8000cdd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004d1c]:fsgnjx.s t6, t5, t4
	-[0x80004d20]:csrrs a3, fcsr, zero
	-[0x80004d24]:sd t6, 1600(s1)
Current Store : [0x80004d28] : sd a3, 1608(s1) -- Store: [0x8000cde0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004d54]:fsgnjx.s t6, t5, t4
	-[0x80004d58]:csrrs a3, fcsr, zero
	-[0x80004d5c]:sd t6, 1616(s1)
Current Store : [0x80004d60] : sd a3, 1624(s1) -- Store: [0x8000cdf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004d8c]:fsgnjx.s t6, t5, t4
	-[0x80004d90]:csrrs a3, fcsr, zero
	-[0x80004d94]:sd t6, 1632(s1)
Current Store : [0x80004d98] : sd a3, 1640(s1) -- Store: [0x8000ce00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004dc4]:fsgnjx.s t6, t5, t4
	-[0x80004dc8]:csrrs a3, fcsr, zero
	-[0x80004dcc]:sd t6, 1648(s1)
Current Store : [0x80004dd0] : sd a3, 1656(s1) -- Store: [0x8000ce10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004dfc]:fsgnjx.s t6, t5, t4
	-[0x80004e00]:csrrs a3, fcsr, zero
	-[0x80004e04]:sd t6, 1664(s1)
Current Store : [0x80004e08] : sd a3, 1672(s1) -- Store: [0x8000ce20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004e34]:fsgnjx.s t6, t5, t4
	-[0x80004e38]:csrrs a3, fcsr, zero
	-[0x80004e3c]:sd t6, 1680(s1)
Current Store : [0x80004e40] : sd a3, 1688(s1) -- Store: [0x8000ce30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004e6c]:fsgnjx.s t6, t5, t4
	-[0x80004e70]:csrrs a3, fcsr, zero
	-[0x80004e74]:sd t6, 1696(s1)
Current Store : [0x80004e78] : sd a3, 1704(s1) -- Store: [0x8000ce40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ea4]:fsgnjx.s t6, t5, t4
	-[0x80004ea8]:csrrs a3, fcsr, zero
	-[0x80004eac]:sd t6, 1712(s1)
Current Store : [0x80004eb0] : sd a3, 1720(s1) -- Store: [0x8000ce50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004edc]:fsgnjx.s t6, t5, t4
	-[0x80004ee0]:csrrs a3, fcsr, zero
	-[0x80004ee4]:sd t6, 1728(s1)
Current Store : [0x80004ee8] : sd a3, 1736(s1) -- Store: [0x8000ce60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004f14]:fsgnjx.s t6, t5, t4
	-[0x80004f18]:csrrs a3, fcsr, zero
	-[0x80004f1c]:sd t6, 1744(s1)
Current Store : [0x80004f20] : sd a3, 1752(s1) -- Store: [0x8000ce70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004f4c]:fsgnjx.s t6, t5, t4
	-[0x80004f50]:csrrs a3, fcsr, zero
	-[0x80004f54]:sd t6, 1760(s1)
Current Store : [0x80004f58] : sd a3, 1768(s1) -- Store: [0x8000ce80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004f84]:fsgnjx.s t6, t5, t4
	-[0x80004f88]:csrrs a3, fcsr, zero
	-[0x80004f8c]:sd t6, 1776(s1)
Current Store : [0x80004f90] : sd a3, 1784(s1) -- Store: [0x8000ce90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004fbc]:fsgnjx.s t6, t5, t4
	-[0x80004fc0]:csrrs a3, fcsr, zero
	-[0x80004fc4]:sd t6, 1792(s1)
Current Store : [0x80004fc8] : sd a3, 1800(s1) -- Store: [0x8000cea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ff4]:fsgnjx.s t6, t5, t4
	-[0x80004ff8]:csrrs a3, fcsr, zero
	-[0x80004ffc]:sd t6, 1808(s1)
Current Store : [0x80005000] : sd a3, 1816(s1) -- Store: [0x8000ceb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000502c]:fsgnjx.s t6, t5, t4
	-[0x80005030]:csrrs a3, fcsr, zero
	-[0x80005034]:sd t6, 1824(s1)
Current Store : [0x80005038] : sd a3, 1832(s1) -- Store: [0x8000cec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005064]:fsgnjx.s t6, t5, t4
	-[0x80005068]:csrrs a3, fcsr, zero
	-[0x8000506c]:sd t6, 1840(s1)
Current Store : [0x80005070] : sd a3, 1848(s1) -- Store: [0x8000ced0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000509c]:fsgnjx.s t6, t5, t4
	-[0x800050a0]:csrrs a3, fcsr, zero
	-[0x800050a4]:sd t6, 1856(s1)
Current Store : [0x800050a8] : sd a3, 1864(s1) -- Store: [0x8000cee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800050d4]:fsgnjx.s t6, t5, t4
	-[0x800050d8]:csrrs a3, fcsr, zero
	-[0x800050dc]:sd t6, 1872(s1)
Current Store : [0x800050e0] : sd a3, 1880(s1) -- Store: [0x8000cef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000510c]:fsgnjx.s t6, t5, t4
	-[0x80005110]:csrrs a3, fcsr, zero
	-[0x80005114]:sd t6, 1888(s1)
Current Store : [0x80005118] : sd a3, 1896(s1) -- Store: [0x8000cf00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005144]:fsgnjx.s t6, t5, t4
	-[0x80005148]:csrrs a3, fcsr, zero
	-[0x8000514c]:sd t6, 1904(s1)
Current Store : [0x80005150] : sd a3, 1912(s1) -- Store: [0x8000cf10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000517c]:fsgnjx.s t6, t5, t4
	-[0x80005180]:csrrs a3, fcsr, zero
	-[0x80005184]:sd t6, 1920(s1)
Current Store : [0x80005188] : sd a3, 1928(s1) -- Store: [0x8000cf20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800051b4]:fsgnjx.s t6, t5, t4
	-[0x800051b8]:csrrs a3, fcsr, zero
	-[0x800051bc]:sd t6, 1936(s1)
Current Store : [0x800051c0] : sd a3, 1944(s1) -- Store: [0x8000cf30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800051ec]:fsgnjx.s t6, t5, t4
	-[0x800051f0]:csrrs a3, fcsr, zero
	-[0x800051f4]:sd t6, 1952(s1)
Current Store : [0x800051f8] : sd a3, 1960(s1) -- Store: [0x8000cf40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005224]:fsgnjx.s t6, t5, t4
	-[0x80005228]:csrrs a3, fcsr, zero
	-[0x8000522c]:sd t6, 1968(s1)
Current Store : [0x80005230] : sd a3, 1976(s1) -- Store: [0x8000cf50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000525c]:fsgnjx.s t6, t5, t4
	-[0x80005260]:csrrs a3, fcsr, zero
	-[0x80005264]:sd t6, 1984(s1)
Current Store : [0x80005268] : sd a3, 1992(s1) -- Store: [0x8000cf60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000529c]:fsgnjx.s t6, t5, t4
	-[0x800052a0]:csrrs a3, fcsr, zero
	-[0x800052a4]:sd t6, 2000(s1)
Current Store : [0x800052a8] : sd a3, 2008(s1) -- Store: [0x8000cf70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800052dc]:fsgnjx.s t6, t5, t4
	-[0x800052e0]:csrrs a3, fcsr, zero
	-[0x800052e4]:sd t6, 2016(s1)
Current Store : [0x800052e8] : sd a3, 2024(s1) -- Store: [0x8000cf80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000531c]:fsgnjx.s t6, t5, t4
	-[0x80005320]:csrrs a3, fcsr, zero
	-[0x80005324]:sd t6, 2032(s1)
Current Store : [0x80005328] : sd a3, 2040(s1) -- Store: [0x8000cf90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005364]:fsgnjx.s t6, t5, t4
	-[0x80005368]:csrrs a3, fcsr, zero
	-[0x8000536c]:sd t6, 0(s1)
Current Store : [0x80005370] : sd a3, 8(s1) -- Store: [0x8000cfa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800053a4]:fsgnjx.s t6, t5, t4
	-[0x800053a8]:csrrs a3, fcsr, zero
	-[0x800053ac]:sd t6, 16(s1)
Current Store : [0x800053b0] : sd a3, 24(s1) -- Store: [0x8000cfb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800053e4]:fsgnjx.s t6, t5, t4
	-[0x800053e8]:csrrs a3, fcsr, zero
	-[0x800053ec]:sd t6, 32(s1)
Current Store : [0x800053f0] : sd a3, 40(s1) -- Store: [0x8000cfc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005424]:fsgnjx.s t6, t5, t4
	-[0x80005428]:csrrs a3, fcsr, zero
	-[0x8000542c]:sd t6, 48(s1)
Current Store : [0x80005430] : sd a3, 56(s1) -- Store: [0x8000cfd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005464]:fsgnjx.s t6, t5, t4
	-[0x80005468]:csrrs a3, fcsr, zero
	-[0x8000546c]:sd t6, 64(s1)
Current Store : [0x80005470] : sd a3, 72(s1) -- Store: [0x8000cfe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800054a4]:fsgnjx.s t6, t5, t4
	-[0x800054a8]:csrrs a3, fcsr, zero
	-[0x800054ac]:sd t6, 80(s1)
Current Store : [0x800054b0] : sd a3, 88(s1) -- Store: [0x8000cff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800054e4]:fsgnjx.s t6, t5, t4
	-[0x800054e8]:csrrs a3, fcsr, zero
	-[0x800054ec]:sd t6, 96(s1)
Current Store : [0x800054f0] : sd a3, 104(s1) -- Store: [0x8000d000]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005524]:fsgnjx.s t6, t5, t4
	-[0x80005528]:csrrs a3, fcsr, zero
	-[0x8000552c]:sd t6, 112(s1)
Current Store : [0x80005530] : sd a3, 120(s1) -- Store: [0x8000d010]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005564]:fsgnjx.s t6, t5, t4
	-[0x80005568]:csrrs a3, fcsr, zero
	-[0x8000556c]:sd t6, 128(s1)
Current Store : [0x80005570] : sd a3, 136(s1) -- Store: [0x8000d020]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800055a4]:fsgnjx.s t6, t5, t4
	-[0x800055a8]:csrrs a3, fcsr, zero
	-[0x800055ac]:sd t6, 144(s1)
Current Store : [0x800055b0] : sd a3, 152(s1) -- Store: [0x8000d030]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800055e4]:fsgnjx.s t6, t5, t4
	-[0x800055e8]:csrrs a3, fcsr, zero
	-[0x800055ec]:sd t6, 160(s1)
Current Store : [0x800055f0] : sd a3, 168(s1) -- Store: [0x8000d040]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005624]:fsgnjx.s t6, t5, t4
	-[0x80005628]:csrrs a3, fcsr, zero
	-[0x8000562c]:sd t6, 176(s1)
Current Store : [0x80005630] : sd a3, 184(s1) -- Store: [0x8000d050]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005664]:fsgnjx.s t6, t5, t4
	-[0x80005668]:csrrs a3, fcsr, zero
	-[0x8000566c]:sd t6, 192(s1)
Current Store : [0x80005670] : sd a3, 200(s1) -- Store: [0x8000d060]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800056a4]:fsgnjx.s t6, t5, t4
	-[0x800056a8]:csrrs a3, fcsr, zero
	-[0x800056ac]:sd t6, 208(s1)
Current Store : [0x800056b0] : sd a3, 216(s1) -- Store: [0x8000d070]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800056e4]:fsgnjx.s t6, t5, t4
	-[0x800056e8]:csrrs a3, fcsr, zero
	-[0x800056ec]:sd t6, 224(s1)
Current Store : [0x800056f0] : sd a3, 232(s1) -- Store: [0x8000d080]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005724]:fsgnjx.s t6, t5, t4
	-[0x80005728]:csrrs a3, fcsr, zero
	-[0x8000572c]:sd t6, 240(s1)
Current Store : [0x80005730] : sd a3, 248(s1) -- Store: [0x8000d090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005764]:fsgnjx.s t6, t5, t4
	-[0x80005768]:csrrs a3, fcsr, zero
	-[0x8000576c]:sd t6, 256(s1)
Current Store : [0x80005770] : sd a3, 264(s1) -- Store: [0x8000d0a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800057a4]:fsgnjx.s t6, t5, t4
	-[0x800057a8]:csrrs a3, fcsr, zero
	-[0x800057ac]:sd t6, 272(s1)
Current Store : [0x800057b0] : sd a3, 280(s1) -- Store: [0x8000d0b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800057e4]:fsgnjx.s t6, t5, t4
	-[0x800057e8]:csrrs a3, fcsr, zero
	-[0x800057ec]:sd t6, 288(s1)
Current Store : [0x800057f0] : sd a3, 296(s1) -- Store: [0x8000d0c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005824]:fsgnjx.s t6, t5, t4
	-[0x80005828]:csrrs a3, fcsr, zero
	-[0x8000582c]:sd t6, 304(s1)
Current Store : [0x80005830] : sd a3, 312(s1) -- Store: [0x8000d0d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005864]:fsgnjx.s t6, t5, t4
	-[0x80005868]:csrrs a3, fcsr, zero
	-[0x8000586c]:sd t6, 320(s1)
Current Store : [0x80005870] : sd a3, 328(s1) -- Store: [0x8000d0e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800058a4]:fsgnjx.s t6, t5, t4
	-[0x800058a8]:csrrs a3, fcsr, zero
	-[0x800058ac]:sd t6, 336(s1)
Current Store : [0x800058b0] : sd a3, 344(s1) -- Store: [0x8000d0f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800058e4]:fsgnjx.s t6, t5, t4
	-[0x800058e8]:csrrs a3, fcsr, zero
	-[0x800058ec]:sd t6, 352(s1)
Current Store : [0x800058f0] : sd a3, 360(s1) -- Store: [0x8000d100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005924]:fsgnjx.s t6, t5, t4
	-[0x80005928]:csrrs a3, fcsr, zero
	-[0x8000592c]:sd t6, 368(s1)
Current Store : [0x80005930] : sd a3, 376(s1) -- Store: [0x8000d110]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005964]:fsgnjx.s t6, t5, t4
	-[0x80005968]:csrrs a3, fcsr, zero
	-[0x8000596c]:sd t6, 384(s1)
Current Store : [0x80005970] : sd a3, 392(s1) -- Store: [0x8000d120]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800059a4]:fsgnjx.s t6, t5, t4
	-[0x800059a8]:csrrs a3, fcsr, zero
	-[0x800059ac]:sd t6, 400(s1)
Current Store : [0x800059b0] : sd a3, 408(s1) -- Store: [0x8000d130]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800059e4]:fsgnjx.s t6, t5, t4
	-[0x800059e8]:csrrs a3, fcsr, zero
	-[0x800059ec]:sd t6, 416(s1)
Current Store : [0x800059f0] : sd a3, 424(s1) -- Store: [0x8000d140]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a24]:fsgnjx.s t6, t5, t4
	-[0x80005a28]:csrrs a3, fcsr, zero
	-[0x80005a2c]:sd t6, 432(s1)
Current Store : [0x80005a30] : sd a3, 440(s1) -- Store: [0x8000d150]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005a64]:fsgnjx.s t6, t5, t4
	-[0x80005a68]:csrrs a3, fcsr, zero
	-[0x80005a6c]:sd t6, 448(s1)
Current Store : [0x80005a70] : sd a3, 456(s1) -- Store: [0x8000d160]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005aa4]:fsgnjx.s t6, t5, t4
	-[0x80005aa8]:csrrs a3, fcsr, zero
	-[0x80005aac]:sd t6, 464(s1)
Current Store : [0x80005ab0] : sd a3, 472(s1) -- Store: [0x8000d170]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005ae4]:fsgnjx.s t6, t5, t4
	-[0x80005ae8]:csrrs a3, fcsr, zero
	-[0x80005aec]:sd t6, 480(s1)
Current Store : [0x80005af0] : sd a3, 488(s1) -- Store: [0x8000d180]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005b24]:fsgnjx.s t6, t5, t4
	-[0x80005b28]:csrrs a3, fcsr, zero
	-[0x80005b2c]:sd t6, 496(s1)
Current Store : [0x80005b30] : sd a3, 504(s1) -- Store: [0x8000d190]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005b64]:fsgnjx.s t6, t5, t4
	-[0x80005b68]:csrrs a3, fcsr, zero
	-[0x80005b6c]:sd t6, 512(s1)
Current Store : [0x80005b70] : sd a3, 520(s1) -- Store: [0x8000d1a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005ba4]:fsgnjx.s t6, t5, t4
	-[0x80005ba8]:csrrs a3, fcsr, zero
	-[0x80005bac]:sd t6, 528(s1)
Current Store : [0x80005bb0] : sd a3, 536(s1) -- Store: [0x8000d1b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005be4]:fsgnjx.s t6, t5, t4
	-[0x80005be8]:csrrs a3, fcsr, zero
	-[0x80005bec]:sd t6, 544(s1)
Current Store : [0x80005bf0] : sd a3, 552(s1) -- Store: [0x8000d1c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005c24]:fsgnjx.s t6, t5, t4
	-[0x80005c28]:csrrs a3, fcsr, zero
	-[0x80005c2c]:sd t6, 560(s1)
Current Store : [0x80005c30] : sd a3, 568(s1) -- Store: [0x8000d1d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005c64]:fsgnjx.s t6, t5, t4
	-[0x80005c68]:csrrs a3, fcsr, zero
	-[0x80005c6c]:sd t6, 576(s1)
Current Store : [0x80005c70] : sd a3, 584(s1) -- Store: [0x8000d1e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005ca4]:fsgnjx.s t6, t5, t4
	-[0x80005ca8]:csrrs a3, fcsr, zero
	-[0x80005cac]:sd t6, 592(s1)
Current Store : [0x80005cb0] : sd a3, 600(s1) -- Store: [0x8000d1f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005ce4]:fsgnjx.s t6, t5, t4
	-[0x80005ce8]:csrrs a3, fcsr, zero
	-[0x80005cec]:sd t6, 608(s1)
Current Store : [0x80005cf0] : sd a3, 616(s1) -- Store: [0x8000d200]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005d24]:fsgnjx.s t6, t5, t4
	-[0x80005d28]:csrrs a3, fcsr, zero
	-[0x80005d2c]:sd t6, 624(s1)
Current Store : [0x80005d30] : sd a3, 632(s1) -- Store: [0x8000d210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005d64]:fsgnjx.s t6, t5, t4
	-[0x80005d68]:csrrs a3, fcsr, zero
	-[0x80005d6c]:sd t6, 640(s1)
Current Store : [0x80005d70] : sd a3, 648(s1) -- Store: [0x8000d220]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005da4]:fsgnjx.s t6, t5, t4
	-[0x80005da8]:csrrs a3, fcsr, zero
	-[0x80005dac]:sd t6, 656(s1)
Current Store : [0x80005db0] : sd a3, 664(s1) -- Store: [0x8000d230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005de4]:fsgnjx.s t6, t5, t4
	-[0x80005de8]:csrrs a3, fcsr, zero
	-[0x80005dec]:sd t6, 672(s1)
Current Store : [0x80005df0] : sd a3, 680(s1) -- Store: [0x8000d240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005e24]:fsgnjx.s t6, t5, t4
	-[0x80005e28]:csrrs a3, fcsr, zero
	-[0x80005e2c]:sd t6, 688(s1)
Current Store : [0x80005e30] : sd a3, 696(s1) -- Store: [0x8000d250]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005e64]:fsgnjx.s t6, t5, t4
	-[0x80005e68]:csrrs a3, fcsr, zero
	-[0x80005e6c]:sd t6, 704(s1)
Current Store : [0x80005e70] : sd a3, 712(s1) -- Store: [0x8000d260]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005ea4]:fsgnjx.s t6, t5, t4
	-[0x80005ea8]:csrrs a3, fcsr, zero
	-[0x80005eac]:sd t6, 720(s1)
Current Store : [0x80005eb0] : sd a3, 728(s1) -- Store: [0x8000d270]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005ee4]:fsgnjx.s t6, t5, t4
	-[0x80005ee8]:csrrs a3, fcsr, zero
	-[0x80005eec]:sd t6, 736(s1)
Current Store : [0x80005ef0] : sd a3, 744(s1) -- Store: [0x8000d280]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f24]:fsgnjx.s t6, t5, t4
	-[0x80005f28]:csrrs a3, fcsr, zero
	-[0x80005f2c]:sd t6, 752(s1)
Current Store : [0x80005f30] : sd a3, 760(s1) -- Store: [0x8000d290]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005f64]:fsgnjx.s t6, t5, t4
	-[0x80005f68]:csrrs a3, fcsr, zero
	-[0x80005f6c]:sd t6, 768(s1)
Current Store : [0x80005f70] : sd a3, 776(s1) -- Store: [0x8000d2a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005fa4]:fsgnjx.s t6, t5, t4
	-[0x80005fa8]:csrrs a3, fcsr, zero
	-[0x80005fac]:sd t6, 784(s1)
Current Store : [0x80005fb0] : sd a3, 792(s1) -- Store: [0x8000d2b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005fe4]:fsgnjx.s t6, t5, t4
	-[0x80005fe8]:csrrs a3, fcsr, zero
	-[0x80005fec]:sd t6, 800(s1)
Current Store : [0x80005ff0] : sd a3, 808(s1) -- Store: [0x8000d2c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006024]:fsgnjx.s t6, t5, t4
	-[0x80006028]:csrrs a3, fcsr, zero
	-[0x8000602c]:sd t6, 816(s1)
Current Store : [0x80006030] : sd a3, 824(s1) -- Store: [0x8000d2d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006064]:fsgnjx.s t6, t5, t4
	-[0x80006068]:csrrs a3, fcsr, zero
	-[0x8000606c]:sd t6, 832(s1)
Current Store : [0x80006070] : sd a3, 840(s1) -- Store: [0x8000d2e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800060a4]:fsgnjx.s t6, t5, t4
	-[0x800060a8]:csrrs a3, fcsr, zero
	-[0x800060ac]:sd t6, 848(s1)
Current Store : [0x800060b0] : sd a3, 856(s1) -- Store: [0x8000d2f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800060e4]:fsgnjx.s t6, t5, t4
	-[0x800060e8]:csrrs a3, fcsr, zero
	-[0x800060ec]:sd t6, 864(s1)
Current Store : [0x800060f0] : sd a3, 872(s1) -- Store: [0x8000d300]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006124]:fsgnjx.s t6, t5, t4
	-[0x80006128]:csrrs a3, fcsr, zero
	-[0x8000612c]:sd t6, 880(s1)
Current Store : [0x80006130] : sd a3, 888(s1) -- Store: [0x8000d310]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006164]:fsgnjx.s t6, t5, t4
	-[0x80006168]:csrrs a3, fcsr, zero
	-[0x8000616c]:sd t6, 896(s1)
Current Store : [0x80006170] : sd a3, 904(s1) -- Store: [0x8000d320]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800061a4]:fsgnjx.s t6, t5, t4
	-[0x800061a8]:csrrs a3, fcsr, zero
	-[0x800061ac]:sd t6, 912(s1)
Current Store : [0x800061b0] : sd a3, 920(s1) -- Store: [0x8000d330]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800061e4]:fsgnjx.s t6, t5, t4
	-[0x800061e8]:csrrs a3, fcsr, zero
	-[0x800061ec]:sd t6, 928(s1)
Current Store : [0x800061f0] : sd a3, 936(s1) -- Store: [0x8000d340]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006224]:fsgnjx.s t6, t5, t4
	-[0x80006228]:csrrs a3, fcsr, zero
	-[0x8000622c]:sd t6, 944(s1)
Current Store : [0x80006230] : sd a3, 952(s1) -- Store: [0x8000d350]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006264]:fsgnjx.s t6, t5, t4
	-[0x80006268]:csrrs a3, fcsr, zero
	-[0x8000626c]:sd t6, 960(s1)
Current Store : [0x80006270] : sd a3, 968(s1) -- Store: [0x8000d360]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800062a4]:fsgnjx.s t6, t5, t4
	-[0x800062a8]:csrrs a3, fcsr, zero
	-[0x800062ac]:sd t6, 976(s1)
Current Store : [0x800062b0] : sd a3, 984(s1) -- Store: [0x8000d370]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800062e4]:fsgnjx.s t6, t5, t4
	-[0x800062e8]:csrrs a3, fcsr, zero
	-[0x800062ec]:sd t6, 992(s1)
Current Store : [0x800062f0] : sd a3, 1000(s1) -- Store: [0x8000d380]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006324]:fsgnjx.s t6, t5, t4
	-[0x80006328]:csrrs a3, fcsr, zero
	-[0x8000632c]:sd t6, 1008(s1)
Current Store : [0x80006330] : sd a3, 1016(s1) -- Store: [0x8000d390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006364]:fsgnjx.s t6, t5, t4
	-[0x80006368]:csrrs a3, fcsr, zero
	-[0x8000636c]:sd t6, 1024(s1)
Current Store : [0x80006370] : sd a3, 1032(s1) -- Store: [0x8000d3a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800063a4]:fsgnjx.s t6, t5, t4
	-[0x800063a8]:csrrs a3, fcsr, zero
	-[0x800063ac]:sd t6, 1040(s1)
Current Store : [0x800063b0] : sd a3, 1048(s1) -- Store: [0x8000d3b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800063e4]:fsgnjx.s t6, t5, t4
	-[0x800063e8]:csrrs a3, fcsr, zero
	-[0x800063ec]:sd t6, 1056(s1)
Current Store : [0x800063f0] : sd a3, 1064(s1) -- Store: [0x8000d3c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006424]:fsgnjx.s t6, t5, t4
	-[0x80006428]:csrrs a3, fcsr, zero
	-[0x8000642c]:sd t6, 1072(s1)
Current Store : [0x80006430] : sd a3, 1080(s1) -- Store: [0x8000d3d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006464]:fsgnjx.s t6, t5, t4
	-[0x80006468]:csrrs a3, fcsr, zero
	-[0x8000646c]:sd t6, 1088(s1)
Current Store : [0x80006470] : sd a3, 1096(s1) -- Store: [0x8000d3e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800064a4]:fsgnjx.s t6, t5, t4
	-[0x800064a8]:csrrs a3, fcsr, zero
	-[0x800064ac]:sd t6, 1104(s1)
Current Store : [0x800064b0] : sd a3, 1112(s1) -- Store: [0x8000d3f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800064e4]:fsgnjx.s t6, t5, t4
	-[0x800064e8]:csrrs a3, fcsr, zero
	-[0x800064ec]:sd t6, 1120(s1)
Current Store : [0x800064f0] : sd a3, 1128(s1) -- Store: [0x8000d400]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006524]:fsgnjx.s t6, t5, t4
	-[0x80006528]:csrrs a3, fcsr, zero
	-[0x8000652c]:sd t6, 1136(s1)
Current Store : [0x80006530] : sd a3, 1144(s1) -- Store: [0x8000d410]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006564]:fsgnjx.s t6, t5, t4
	-[0x80006568]:csrrs a3, fcsr, zero
	-[0x8000656c]:sd t6, 1152(s1)
Current Store : [0x80006570] : sd a3, 1160(s1) -- Store: [0x8000d420]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800065a4]:fsgnjx.s t6, t5, t4
	-[0x800065a8]:csrrs a3, fcsr, zero
	-[0x800065ac]:sd t6, 1168(s1)
Current Store : [0x800065b0] : sd a3, 1176(s1) -- Store: [0x8000d430]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800065e4]:fsgnjx.s t6, t5, t4
	-[0x800065e8]:csrrs a3, fcsr, zero
	-[0x800065ec]:sd t6, 1184(s1)
Current Store : [0x800065f0] : sd a3, 1192(s1) -- Store: [0x8000d440]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006624]:fsgnjx.s t6, t5, t4
	-[0x80006628]:csrrs a3, fcsr, zero
	-[0x8000662c]:sd t6, 1200(s1)
Current Store : [0x80006630] : sd a3, 1208(s1) -- Store: [0x8000d450]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006664]:fsgnjx.s t6, t5, t4
	-[0x80006668]:csrrs a3, fcsr, zero
	-[0x8000666c]:sd t6, 1216(s1)
Current Store : [0x80006670] : sd a3, 1224(s1) -- Store: [0x8000d460]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800066a4]:fsgnjx.s t6, t5, t4
	-[0x800066a8]:csrrs a3, fcsr, zero
	-[0x800066ac]:sd t6, 1232(s1)
Current Store : [0x800066b0] : sd a3, 1240(s1) -- Store: [0x8000d470]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800066e4]:fsgnjx.s t6, t5, t4
	-[0x800066e8]:csrrs a3, fcsr, zero
	-[0x800066ec]:sd t6, 1248(s1)
Current Store : [0x800066f0] : sd a3, 1256(s1) -- Store: [0x8000d480]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006724]:fsgnjx.s t6, t5, t4
	-[0x80006728]:csrrs a3, fcsr, zero
	-[0x8000672c]:sd t6, 1264(s1)
Current Store : [0x80006730] : sd a3, 1272(s1) -- Store: [0x8000d490]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006764]:fsgnjx.s t6, t5, t4
	-[0x80006768]:csrrs a3, fcsr, zero
	-[0x8000676c]:sd t6, 1280(s1)
Current Store : [0x80006770] : sd a3, 1288(s1) -- Store: [0x8000d4a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800067a4]:fsgnjx.s t6, t5, t4
	-[0x800067a8]:csrrs a3, fcsr, zero
	-[0x800067ac]:sd t6, 1296(s1)
Current Store : [0x800067b0] : sd a3, 1304(s1) -- Store: [0x8000d4b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800067e4]:fsgnjx.s t6, t5, t4
	-[0x800067e8]:csrrs a3, fcsr, zero
	-[0x800067ec]:sd t6, 1312(s1)
Current Store : [0x800067f0] : sd a3, 1320(s1) -- Store: [0x8000d4c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006824]:fsgnjx.s t6, t5, t4
	-[0x80006828]:csrrs a3, fcsr, zero
	-[0x8000682c]:sd t6, 1328(s1)
Current Store : [0x80006830] : sd a3, 1336(s1) -- Store: [0x8000d4d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006864]:fsgnjx.s t6, t5, t4
	-[0x80006868]:csrrs a3, fcsr, zero
	-[0x8000686c]:sd t6, 1344(s1)
Current Store : [0x80006870] : sd a3, 1352(s1) -- Store: [0x8000d4e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800068a4]:fsgnjx.s t6, t5, t4
	-[0x800068a8]:csrrs a3, fcsr, zero
	-[0x800068ac]:sd t6, 1360(s1)
Current Store : [0x800068b0] : sd a3, 1368(s1) -- Store: [0x8000d4f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800068e4]:fsgnjx.s t6, t5, t4
	-[0x800068e8]:csrrs a3, fcsr, zero
	-[0x800068ec]:sd t6, 1376(s1)
Current Store : [0x800068f0] : sd a3, 1384(s1) -- Store: [0x8000d500]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006924]:fsgnjx.s t6, t5, t4
	-[0x80006928]:csrrs a3, fcsr, zero
	-[0x8000692c]:sd t6, 1392(s1)
Current Store : [0x80006930] : sd a3, 1400(s1) -- Store: [0x8000d510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006964]:fsgnjx.s t6, t5, t4
	-[0x80006968]:csrrs a3, fcsr, zero
	-[0x8000696c]:sd t6, 1408(s1)
Current Store : [0x80006970] : sd a3, 1416(s1) -- Store: [0x8000d520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800069a4]:fsgnjx.s t6, t5, t4
	-[0x800069a8]:csrrs a3, fcsr, zero
	-[0x800069ac]:sd t6, 1424(s1)
Current Store : [0x800069b0] : sd a3, 1432(s1) -- Store: [0x8000d530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800069e4]:fsgnjx.s t6, t5, t4
	-[0x800069e8]:csrrs a3, fcsr, zero
	-[0x800069ec]:sd t6, 1440(s1)
Current Store : [0x800069f0] : sd a3, 1448(s1) -- Store: [0x8000d540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006a24]:fsgnjx.s t6, t5, t4
	-[0x80006a28]:csrrs a3, fcsr, zero
	-[0x80006a2c]:sd t6, 1456(s1)
Current Store : [0x80006a30] : sd a3, 1464(s1) -- Store: [0x8000d550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006a64]:fsgnjx.s t6, t5, t4
	-[0x80006a68]:csrrs a3, fcsr, zero
	-[0x80006a6c]:sd t6, 1472(s1)
Current Store : [0x80006a70] : sd a3, 1480(s1) -- Store: [0x8000d560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006aa4]:fsgnjx.s t6, t5, t4
	-[0x80006aa8]:csrrs a3, fcsr, zero
	-[0x80006aac]:sd t6, 1488(s1)
Current Store : [0x80006ab0] : sd a3, 1496(s1) -- Store: [0x8000d570]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ae4]:fsgnjx.s t6, t5, t4
	-[0x80006ae8]:csrrs a3, fcsr, zero
	-[0x80006aec]:sd t6, 1504(s1)
Current Store : [0x80006af0] : sd a3, 1512(s1) -- Store: [0x8000d580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006b24]:fsgnjx.s t6, t5, t4
	-[0x80006b28]:csrrs a3, fcsr, zero
	-[0x80006b2c]:sd t6, 1520(s1)
Current Store : [0x80006b30] : sd a3, 1528(s1) -- Store: [0x8000d590]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006b64]:fsgnjx.s t6, t5, t4
	-[0x80006b68]:csrrs a3, fcsr, zero
	-[0x80006b6c]:sd t6, 1536(s1)
Current Store : [0x80006b70] : sd a3, 1544(s1) -- Store: [0x8000d5a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006ba4]:fsgnjx.s t6, t5, t4
	-[0x80006ba8]:csrrs a3, fcsr, zero
	-[0x80006bac]:sd t6, 1552(s1)
Current Store : [0x80006bb0] : sd a3, 1560(s1) -- Store: [0x8000d5b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006be4]:fsgnjx.s t6, t5, t4
	-[0x80006be8]:csrrs a3, fcsr, zero
	-[0x80006bec]:sd t6, 1568(s1)
Current Store : [0x80006bf0] : sd a3, 1576(s1) -- Store: [0x8000d5c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c24]:fsgnjx.s t6, t5, t4
	-[0x80006c28]:csrrs a3, fcsr, zero
	-[0x80006c2c]:sd t6, 1584(s1)
Current Store : [0x80006c30] : sd a3, 1592(s1) -- Store: [0x8000d5d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006c64]:fsgnjx.s t6, t5, t4
	-[0x80006c68]:csrrs a3, fcsr, zero
	-[0x80006c6c]:sd t6, 1600(s1)
Current Store : [0x80006c70] : sd a3, 1608(s1) -- Store: [0x8000d5e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006ca4]:fsgnjx.s t6, t5, t4
	-[0x80006ca8]:csrrs a3, fcsr, zero
	-[0x80006cac]:sd t6, 1616(s1)
Current Store : [0x80006cb0] : sd a3, 1624(s1) -- Store: [0x8000d5f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ce4]:fsgnjx.s t6, t5, t4
	-[0x80006ce8]:csrrs a3, fcsr, zero
	-[0x80006cec]:sd t6, 1632(s1)
Current Store : [0x80006cf0] : sd a3, 1640(s1) -- Store: [0x8000d600]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006d24]:fsgnjx.s t6, t5, t4
	-[0x80006d28]:csrrs a3, fcsr, zero
	-[0x80006d2c]:sd t6, 1648(s1)
Current Store : [0x80006d30] : sd a3, 1656(s1) -- Store: [0x8000d610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006d64]:fsgnjx.s t6, t5, t4
	-[0x80006d68]:csrrs a3, fcsr, zero
	-[0x80006d6c]:sd t6, 1664(s1)
Current Store : [0x80006d70] : sd a3, 1672(s1) -- Store: [0x8000d620]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006da4]:fsgnjx.s t6, t5, t4
	-[0x80006da8]:csrrs a3, fcsr, zero
	-[0x80006dac]:sd t6, 1680(s1)
Current Store : [0x80006db0] : sd a3, 1688(s1) -- Store: [0x8000d630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006de4]:fsgnjx.s t6, t5, t4
	-[0x80006de8]:csrrs a3, fcsr, zero
	-[0x80006dec]:sd t6, 1696(s1)
Current Store : [0x80006df0] : sd a3, 1704(s1) -- Store: [0x8000d640]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006e24]:fsgnjx.s t6, t5, t4
	-[0x80006e28]:csrrs a3, fcsr, zero
	-[0x80006e2c]:sd t6, 1712(s1)
Current Store : [0x80006e30] : sd a3, 1720(s1) -- Store: [0x8000d650]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006e64]:fsgnjx.s t6, t5, t4
	-[0x80006e68]:csrrs a3, fcsr, zero
	-[0x80006e6c]:sd t6, 1728(s1)
Current Store : [0x80006e70] : sd a3, 1736(s1) -- Store: [0x8000d660]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006ea4]:fsgnjx.s t6, t5, t4
	-[0x80006ea8]:csrrs a3, fcsr, zero
	-[0x80006eac]:sd t6, 1744(s1)
Current Store : [0x80006eb0] : sd a3, 1752(s1) -- Store: [0x8000d670]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ee4]:fsgnjx.s t6, t5, t4
	-[0x80006ee8]:csrrs a3, fcsr, zero
	-[0x80006eec]:sd t6, 1760(s1)
Current Store : [0x80006ef0] : sd a3, 1768(s1) -- Store: [0x8000d680]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006f24]:fsgnjx.s t6, t5, t4
	-[0x80006f28]:csrrs a3, fcsr, zero
	-[0x80006f2c]:sd t6, 1776(s1)
Current Store : [0x80006f30] : sd a3, 1784(s1) -- Store: [0x8000d690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006f64]:fsgnjx.s t6, t5, t4
	-[0x80006f68]:csrrs a3, fcsr, zero
	-[0x80006f6c]:sd t6, 1792(s1)
Current Store : [0x80006f70] : sd a3, 1800(s1) -- Store: [0x8000d6a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006fa4]:fsgnjx.s t6, t5, t4
	-[0x80006fa8]:csrrs a3, fcsr, zero
	-[0x80006fac]:sd t6, 1808(s1)
Current Store : [0x80006fb0] : sd a3, 1816(s1) -- Store: [0x8000d6b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006fe4]:fsgnjx.s t6, t5, t4
	-[0x80006fe8]:csrrs a3, fcsr, zero
	-[0x80006fec]:sd t6, 1824(s1)
Current Store : [0x80006ff0] : sd a3, 1832(s1) -- Store: [0x8000d6c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007024]:fsgnjx.s t6, t5, t4
	-[0x80007028]:csrrs a3, fcsr, zero
	-[0x8000702c]:sd t6, 1840(s1)
Current Store : [0x80007030] : sd a3, 1848(s1) -- Store: [0x8000d6d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007064]:fsgnjx.s t6, t5, t4
	-[0x80007068]:csrrs a3, fcsr, zero
	-[0x8000706c]:sd t6, 1856(s1)
Current Store : [0x80007070] : sd a3, 1864(s1) -- Store: [0x8000d6e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800070a4]:fsgnjx.s t6, t5, t4
	-[0x800070a8]:csrrs a3, fcsr, zero
	-[0x800070ac]:sd t6, 1872(s1)
Current Store : [0x800070b0] : sd a3, 1880(s1) -- Store: [0x8000d6f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800070e4]:fsgnjx.s t6, t5, t4
	-[0x800070e8]:csrrs a3, fcsr, zero
	-[0x800070ec]:sd t6, 1888(s1)
Current Store : [0x800070f0] : sd a3, 1896(s1) -- Store: [0x8000d700]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007124]:fsgnjx.s t6, t5, t4
	-[0x80007128]:csrrs a3, fcsr, zero
	-[0x8000712c]:sd t6, 1904(s1)
Current Store : [0x80007130] : sd a3, 1912(s1) -- Store: [0x8000d710]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007164]:fsgnjx.s t6, t5, t4
	-[0x80007168]:csrrs a3, fcsr, zero
	-[0x8000716c]:sd t6, 1920(s1)
Current Store : [0x80007170] : sd a3, 1928(s1) -- Store: [0x8000d720]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800071a4]:fsgnjx.s t6, t5, t4
	-[0x800071a8]:csrrs a3, fcsr, zero
	-[0x800071ac]:sd t6, 1936(s1)
Current Store : [0x800071b0] : sd a3, 1944(s1) -- Store: [0x8000d730]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800071e4]:fsgnjx.s t6, t5, t4
	-[0x800071e8]:csrrs a3, fcsr, zero
	-[0x800071ec]:sd t6, 1952(s1)
Current Store : [0x800071f0] : sd a3, 1960(s1) -- Store: [0x8000d740]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007224]:fsgnjx.s t6, t5, t4
	-[0x80007228]:csrrs a3, fcsr, zero
	-[0x8000722c]:sd t6, 1968(s1)
Current Store : [0x80007230] : sd a3, 1976(s1) -- Store: [0x8000d750]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007264]:fsgnjx.s t6, t5, t4
	-[0x80007268]:csrrs a3, fcsr, zero
	-[0x8000726c]:sd t6, 1984(s1)
Current Store : [0x80007270] : sd a3, 1992(s1) -- Store: [0x8000d760]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000729c]:fsgnjx.s t6, t5, t4
	-[0x800072a0]:csrrs a3, fcsr, zero
	-[0x800072a4]:sd t6, 2000(s1)
Current Store : [0x800072a8] : sd a3, 2008(s1) -- Store: [0x8000d770]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800072d4]:fsgnjx.s t6, t5, t4
	-[0x800072d8]:csrrs a3, fcsr, zero
	-[0x800072dc]:sd t6, 2016(s1)
Current Store : [0x800072e0] : sd a3, 2024(s1) -- Store: [0x8000d780]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000730c]:fsgnjx.s t6, t5, t4
	-[0x80007310]:csrrs a3, fcsr, zero
	-[0x80007314]:sd t6, 2032(s1)
Current Store : [0x80007318] : sd a3, 2040(s1) -- Store: [0x8000d790]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000734c]:fsgnjx.s t6, t5, t4
	-[0x80007350]:csrrs a3, fcsr, zero
	-[0x80007354]:sd t6, 0(s1)
Current Store : [0x80007358] : sd a3, 8(s1) -- Store: [0x8000d7a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007384]:fsgnjx.s t6, t5, t4
	-[0x80007388]:csrrs a3, fcsr, zero
	-[0x8000738c]:sd t6, 16(s1)
Current Store : [0x80007390] : sd a3, 24(s1) -- Store: [0x8000d7b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800073bc]:fsgnjx.s t6, t5, t4
	-[0x800073c0]:csrrs a3, fcsr, zero
	-[0x800073c4]:sd t6, 32(s1)
Current Store : [0x800073c8] : sd a3, 40(s1) -- Store: [0x8000d7c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800073f4]:fsgnjx.s t6, t5, t4
	-[0x800073f8]:csrrs a3, fcsr, zero
	-[0x800073fc]:sd t6, 48(s1)
Current Store : [0x80007400] : sd a3, 56(s1) -- Store: [0x8000d7d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000742c]:fsgnjx.s t6, t5, t4
	-[0x80007430]:csrrs a3, fcsr, zero
	-[0x80007434]:sd t6, 64(s1)
Current Store : [0x80007438] : sd a3, 72(s1) -- Store: [0x8000d7e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007464]:fsgnjx.s t6, t5, t4
	-[0x80007468]:csrrs a3, fcsr, zero
	-[0x8000746c]:sd t6, 80(s1)
Current Store : [0x80007470] : sd a3, 88(s1) -- Store: [0x8000d7f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000749c]:fsgnjx.s t6, t5, t4
	-[0x800074a0]:csrrs a3, fcsr, zero
	-[0x800074a4]:sd t6, 96(s1)
Current Store : [0x800074a8] : sd a3, 104(s1) -- Store: [0x8000d800]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800074d4]:fsgnjx.s t6, t5, t4
	-[0x800074d8]:csrrs a3, fcsr, zero
	-[0x800074dc]:sd t6, 112(s1)
Current Store : [0x800074e0] : sd a3, 120(s1) -- Store: [0x8000d810]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000750c]:fsgnjx.s t6, t5, t4
	-[0x80007510]:csrrs a3, fcsr, zero
	-[0x80007514]:sd t6, 128(s1)
Current Store : [0x80007518] : sd a3, 136(s1) -- Store: [0x8000d820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007544]:fsgnjx.s t6, t5, t4
	-[0x80007548]:csrrs a3, fcsr, zero
	-[0x8000754c]:sd t6, 144(s1)
Current Store : [0x80007550] : sd a3, 152(s1) -- Store: [0x8000d830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000757c]:fsgnjx.s t6, t5, t4
	-[0x80007580]:csrrs a3, fcsr, zero
	-[0x80007584]:sd t6, 160(s1)
Current Store : [0x80007588] : sd a3, 168(s1) -- Store: [0x8000d840]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800075b4]:fsgnjx.s t6, t5, t4
	-[0x800075b8]:csrrs a3, fcsr, zero
	-[0x800075bc]:sd t6, 176(s1)
Current Store : [0x800075c0] : sd a3, 184(s1) -- Store: [0x8000d850]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800075ec]:fsgnjx.s t6, t5, t4
	-[0x800075f0]:csrrs a3, fcsr, zero
	-[0x800075f4]:sd t6, 192(s1)
Current Store : [0x800075f8] : sd a3, 200(s1) -- Store: [0x8000d860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007624]:fsgnjx.s t6, t5, t4
	-[0x80007628]:csrrs a3, fcsr, zero
	-[0x8000762c]:sd t6, 208(s1)
Current Store : [0x80007630] : sd a3, 216(s1) -- Store: [0x8000d870]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000765c]:fsgnjx.s t6, t5, t4
	-[0x80007660]:csrrs a3, fcsr, zero
	-[0x80007664]:sd t6, 224(s1)
Current Store : [0x80007668] : sd a3, 232(s1) -- Store: [0x8000d880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007694]:fsgnjx.s t6, t5, t4
	-[0x80007698]:csrrs a3, fcsr, zero
	-[0x8000769c]:sd t6, 240(s1)
Current Store : [0x800076a0] : sd a3, 248(s1) -- Store: [0x8000d890]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800076cc]:fsgnjx.s t6, t5, t4
	-[0x800076d0]:csrrs a3, fcsr, zero
	-[0x800076d4]:sd t6, 256(s1)
Current Store : [0x800076d8] : sd a3, 264(s1) -- Store: [0x8000d8a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007704]:fsgnjx.s t6, t5, t4
	-[0x80007708]:csrrs a3, fcsr, zero
	-[0x8000770c]:sd t6, 272(s1)
Current Store : [0x80007710] : sd a3, 280(s1) -- Store: [0x8000d8b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000773c]:fsgnjx.s t6, t5, t4
	-[0x80007740]:csrrs a3, fcsr, zero
	-[0x80007744]:sd t6, 288(s1)
Current Store : [0x80007748] : sd a3, 296(s1) -- Store: [0x8000d8c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007774]:fsgnjx.s t6, t5, t4
	-[0x80007778]:csrrs a3, fcsr, zero
	-[0x8000777c]:sd t6, 304(s1)
Current Store : [0x80007780] : sd a3, 312(s1) -- Store: [0x8000d8d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800077ac]:fsgnjx.s t6, t5, t4
	-[0x800077b0]:csrrs a3, fcsr, zero
	-[0x800077b4]:sd t6, 320(s1)
Current Store : [0x800077b8] : sd a3, 328(s1) -- Store: [0x8000d8e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800077e4]:fsgnjx.s t6, t5, t4
	-[0x800077e8]:csrrs a3, fcsr, zero
	-[0x800077ec]:sd t6, 336(s1)
Current Store : [0x800077f0] : sd a3, 344(s1) -- Store: [0x8000d8f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000781c]:fsgnjx.s t6, t5, t4
	-[0x80007820]:csrrs a3, fcsr, zero
	-[0x80007824]:sd t6, 352(s1)
Current Store : [0x80007828] : sd a3, 360(s1) -- Store: [0x8000d900]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007854]:fsgnjx.s t6, t5, t4
	-[0x80007858]:csrrs a3, fcsr, zero
	-[0x8000785c]:sd t6, 368(s1)
Current Store : [0x80007860] : sd a3, 376(s1) -- Store: [0x8000d910]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000788c]:fsgnjx.s t6, t5, t4
	-[0x80007890]:csrrs a3, fcsr, zero
	-[0x80007894]:sd t6, 384(s1)
Current Store : [0x80007898] : sd a3, 392(s1) -- Store: [0x8000d920]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800078c4]:fsgnjx.s t6, t5, t4
	-[0x800078c8]:csrrs a3, fcsr, zero
	-[0x800078cc]:sd t6, 400(s1)
Current Store : [0x800078d0] : sd a3, 408(s1) -- Store: [0x8000d930]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800078fc]:fsgnjx.s t6, t5, t4
	-[0x80007900]:csrrs a3, fcsr, zero
	-[0x80007904]:sd t6, 416(s1)
Current Store : [0x80007908] : sd a3, 424(s1) -- Store: [0x8000d940]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007934]:fsgnjx.s t6, t5, t4
	-[0x80007938]:csrrs a3, fcsr, zero
	-[0x8000793c]:sd t6, 432(s1)
Current Store : [0x80007940] : sd a3, 440(s1) -- Store: [0x8000d950]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000796c]:fsgnjx.s t6, t5, t4
	-[0x80007970]:csrrs a3, fcsr, zero
	-[0x80007974]:sd t6, 448(s1)
Current Store : [0x80007978] : sd a3, 456(s1) -- Store: [0x8000d960]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800079a4]:fsgnjx.s t6, t5, t4
	-[0x800079a8]:csrrs a3, fcsr, zero
	-[0x800079ac]:sd t6, 464(s1)
Current Store : [0x800079b0] : sd a3, 472(s1) -- Store: [0x8000d970]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800079dc]:fsgnjx.s t6, t5, t4
	-[0x800079e0]:csrrs a3, fcsr, zero
	-[0x800079e4]:sd t6, 480(s1)
Current Store : [0x800079e8] : sd a3, 488(s1) -- Store: [0x8000d980]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007a14]:fsgnjx.s t6, t5, t4
	-[0x80007a18]:csrrs a3, fcsr, zero
	-[0x80007a1c]:sd t6, 496(s1)
Current Store : [0x80007a20] : sd a3, 504(s1) -- Store: [0x8000d990]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007a4c]:fsgnjx.s t6, t5, t4
	-[0x80007a50]:csrrs a3, fcsr, zero
	-[0x80007a54]:sd t6, 512(s1)
Current Store : [0x80007a58] : sd a3, 520(s1) -- Store: [0x8000d9a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007a84]:fsgnjx.s t6, t5, t4
	-[0x80007a88]:csrrs a3, fcsr, zero
	-[0x80007a8c]:sd t6, 528(s1)
Current Store : [0x80007a90] : sd a3, 536(s1) -- Store: [0x8000d9b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007abc]:fsgnjx.s t6, t5, t4
	-[0x80007ac0]:csrrs a3, fcsr, zero
	-[0x80007ac4]:sd t6, 544(s1)
Current Store : [0x80007ac8] : sd a3, 552(s1) -- Store: [0x8000d9c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007af4]:fsgnjx.s t6, t5, t4
	-[0x80007af8]:csrrs a3, fcsr, zero
	-[0x80007afc]:sd t6, 560(s1)
Current Store : [0x80007b00] : sd a3, 568(s1) -- Store: [0x8000d9d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b2c]:fsgnjx.s t6, t5, t4
	-[0x80007b30]:csrrs a3, fcsr, zero
	-[0x80007b34]:sd t6, 576(s1)
Current Store : [0x80007b38] : sd a3, 584(s1) -- Store: [0x8000d9e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007b64]:fsgnjx.s t6, t5, t4
	-[0x80007b68]:csrrs a3, fcsr, zero
	-[0x80007b6c]:sd t6, 592(s1)
Current Store : [0x80007b70] : sd a3, 600(s1) -- Store: [0x8000d9f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b9c]:fsgnjx.s t6, t5, t4
	-[0x80007ba0]:csrrs a3, fcsr, zero
	-[0x80007ba4]:sd t6, 608(s1)
Current Store : [0x80007ba8] : sd a3, 616(s1) -- Store: [0x8000da00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007bd4]:fsgnjx.s t6, t5, t4
	-[0x80007bd8]:csrrs a3, fcsr, zero
	-[0x80007bdc]:sd t6, 624(s1)
Current Store : [0x80007be0] : sd a3, 632(s1) -- Store: [0x8000da10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c0c]:fsgnjx.s t6, t5, t4
	-[0x80007c10]:csrrs a3, fcsr, zero
	-[0x80007c14]:sd t6, 640(s1)
Current Store : [0x80007c18] : sd a3, 648(s1) -- Store: [0x8000da20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007c44]:fsgnjx.s t6, t5, t4
	-[0x80007c48]:csrrs a3, fcsr, zero
	-[0x80007c4c]:sd t6, 656(s1)
Current Store : [0x80007c50] : sd a3, 664(s1) -- Store: [0x8000da30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007c7c]:fsgnjx.s t6, t5, t4
	-[0x80007c80]:csrrs a3, fcsr, zero
	-[0x80007c84]:sd t6, 672(s1)
Current Store : [0x80007c88] : sd a3, 680(s1) -- Store: [0x8000da40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007cb4]:fsgnjx.s t6, t5, t4
	-[0x80007cb8]:csrrs a3, fcsr, zero
	-[0x80007cbc]:sd t6, 688(s1)
Current Store : [0x80007cc0] : sd a3, 696(s1) -- Store: [0x8000da50]:0x0000000000000000




Last Coverpoint : ['mnemonic : fsgnjx.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007cec]:fsgnjx.s t6, t5, t4
	-[0x80007cf0]:csrrs a3, fcsr, zero
	-[0x80007cf4]:sd t6, 704(s1)
Current Store : [0x80007cf8] : sd a3, 712(s1) -- Store: [0x8000da60]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                                                                                                         coverpoints                                                                                                                                         |                                                     code                                                      |
|---:|---------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
|   1|[0x8000b618]<br>0x0000000000000000<br> |- mnemonic : fsgnjx.s<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br> |[0x800003bc]:fsgnjx.s t6, t6, t5<br> [0x800003c0]:csrrs tp, fcsr, zero<br> [0x800003c4]:sd t6, 0(ra)<br>       |
|   2|[0x8000b628]<br>0xFFFFFFFF80000000<br> |- rs1 : x30<br> - rs2 : x29<br> - rd : x29<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800003dc]:fsgnjx.s t4, t5, t4<br> [0x800003e0]:csrrs tp, fcsr, zero<br> [0x800003e4]:sd t4, 16(ra)<br>      |
|   3|[0x8000b638]<br>0x0000000000000000<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                        |[0x800003fc]:fsgnjx.s t3, t3, t3<br> [0x80000400]:csrrs tp, fcsr, zero<br> [0x80000404]:sd t3, 32(ra)<br>      |
|   4|[0x8000b648]<br>0x0000000000000000<br> |- rs1 : x27<br> - rs2 : x27<br> - rd : x30<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                        |[0x8000041c]:fsgnjx.s t5, s11, s11<br> [0x80000420]:csrrs tp, fcsr, zero<br> [0x80000424]:sd t5, 48(ra)<br>    |
|   5|[0x8000b658]<br>0x0000000000000000<br> |- rs1 : x29<br> - rs2 : x31<br> - rd : x27<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>    |[0x8000043c]:fsgnjx.s s11, t4, t6<br> [0x80000440]:csrrs tp, fcsr, zero<br> [0x80000444]:sd s11, 64(ra)<br>    |
|   6|[0x8000b668]<br>0xFFFFFFFF80000000<br> |- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000045c]:fsgnjx.s s10, s9, s8<br> [0x80000460]:csrrs tp, fcsr, zero<br> [0x80000464]:sd s10, 80(ra)<br>    |
|   7|[0x8000b678]<br>0x0000000000000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000047c]:fsgnjx.s s9, s8, s10<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s9, 96(ra)<br>     |
|   8|[0x8000b688]<br>0xFFFFFFFF80000000<br> |- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000049c]:fsgnjx.s s8, s10, s9<br> [0x800004a0]:csrrs tp, fcsr, zero<br> [0x800004a4]:sd s8, 112(ra)<br>    |
|   9|[0x8000b698]<br>0x0000000000000000<br> |- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800004bc]:fsgnjx.s s7, s6, s5<br> [0x800004c0]:csrrs tp, fcsr, zero<br> [0x800004c4]:sd s7, 128(ra)<br>     |
|  10|[0x8000b6a8]<br>0xFFFFFFFF80000000<br> |- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x800004dc]:fsgnjx.s s6, s5, s7<br> [0x800004e0]:csrrs tp, fcsr, zero<br> [0x800004e4]:sd s6, 144(ra)<br>     |
|  11|[0x8000b6b8]<br>0x0000000000000000<br> |- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800004fc]:fsgnjx.s s5, s7, s6<br> [0x80000500]:csrrs tp, fcsr, zero<br> [0x80000504]:sd s5, 160(ra)<br>     |
|  12|[0x8000b6c8]<br>0xFFFFFFFF80000000<br> |- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000051c]:fsgnjx.s s4, s3, s2<br> [0x80000520]:csrrs tp, fcsr, zero<br> [0x80000524]:sd s4, 176(ra)<br>     |
|  13|[0x8000b6d8]<br>0x0000000000000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000053c]:fsgnjx.s s3, s2, s4<br> [0x80000540]:csrrs tp, fcsr, zero<br> [0x80000544]:sd s3, 192(ra)<br>     |
|  14|[0x8000b6e8]<br>0xFFFFFFFF80000000<br> |- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000055c]:fsgnjx.s s2, s4, s3<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd s2, 208(ra)<br>     |
|  15|[0x8000b6f8]<br>0x0000000000000000<br> |- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x8000057c]:fsgnjx.s a7, a6, a5<br> [0x80000580]:csrrs tp, fcsr, zero<br> [0x80000584]:sd a7, 224(ra)<br>     |
|  16|[0x8000b708]<br>0xFFFFFFFF80000000<br> |- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000059c]:fsgnjx.s a6, a5, a7<br> [0x800005a0]:csrrs tp, fcsr, zero<br> [0x800005a4]:sd a6, 240(ra)<br>     |
|  17|[0x8000b718]<br>0x0000000000000000<br> |- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800005bc]:fsgnjx.s a5, a7, a6<br> [0x800005c0]:csrrs tp, fcsr, zero<br> [0x800005c4]:sd a5, 256(ra)<br>     |
|  18|[0x8000b728]<br>0xFFFFFFFF80000000<br> |- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x800005dc]:fsgnjx.s a4, a3, a2<br> [0x800005e0]:csrrs tp, fcsr, zero<br> [0x800005e4]:sd a4, 272(ra)<br>     |
|  19|[0x8000b738]<br>0x0000000000000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                  |[0x800005fc]:fsgnjx.s a3, a2, a4<br> [0x80000600]:csrrs tp, fcsr, zero<br> [0x80000604]:sd a3, 288(ra)<br>     |
|  20|[0x8000b748]<br>0xFFFFFFFF80000000<br> |- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                  |[0x8000061c]:fsgnjx.s a2, a4, a3<br> [0x80000620]:csrrs tp, fcsr, zero<br> [0x80000624]:sd a2, 304(ra)<br>     |
|  21|[0x8000b758]<br>0x0000000000000000<br> |- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                   |[0x8000063c]:fsgnjx.s a1, a0, s1<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd a1, 320(ra)<br>     |
|  22|[0x8000b768]<br>0xFFFFFFFF80000000<br> |- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                   |[0x80000664]:fsgnjx.s a0, s1, a1<br> [0x80000668]:csrrs a3, fcsr, zero<br> [0x8000066c]:sd a0, 336(ra)<br>     |
|  23|[0x8000b778]<br>0x0000000000000000<br> |- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                   |[0x80000684]:fsgnjx.s s1, a1, a0<br> [0x80000688]:csrrs a3, fcsr, zero<br> [0x8000068c]:sd s1, 352(ra)<br>     |
|  24|[0x8000b788]<br>0xFFFFFFFF80000000<br> |- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x800006a4]:fsgnjx.s fp, t2, t1<br> [0x800006a8]:csrrs a3, fcsr, zero<br> [0x800006ac]:sd fp, 368(ra)<br>     |
|  25|[0x8000b798]<br>0xFFFFFFFF80000000<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x800006cc]:fsgnjx.s t2, t1, fp<br> [0x800006d0]:csrrs a3, fcsr, zero<br> [0x800006d4]:sd t2, 0(s1)<br>       |
|  26|[0x8000b7a8]<br>0x0000000000000000<br> |- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x800006ec]:fsgnjx.s t1, fp, t2<br> [0x800006f0]:csrrs a3, fcsr, zero<br> [0x800006f4]:sd t1, 16(s1)<br>      |
|  27|[0x8000b7b8]<br>0xFFFFFFFF80000000<br> |- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x8000070c]:fsgnjx.s t0, tp, gp<br> [0x80000710]:csrrs a3, fcsr, zero<br> [0x80000714]:sd t0, 32(s1)<br>      |
|  28|[0x8000b7c8]<br>0x0000000000000000<br> |- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x8000072c]:fsgnjx.s tp, gp, t0<br> [0x80000730]:csrrs a3, fcsr, zero<br> [0x80000734]:sd tp, 48(s1)<br>      |
|  29|[0x8000b7d8]<br>0xFFFFFFFF80000000<br> |- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                     |[0x8000074c]:fsgnjx.s gp, t0, tp<br> [0x80000750]:csrrs a3, fcsr, zero<br> [0x80000754]:sd gp, 64(s1)<br>      |
|  30|[0x8000b7e8]<br>0xFFFFFFFF80000000<br> |- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br>                                                                                                                                                                                                                                                  |[0x8000076c]:fsgnjx.s sp, ra, zero<br> [0x80000770]:csrrs a3, fcsr, zero<br> [0x80000774]:sd sp, 80(s1)<br>    |
|  31|[0x8000b7f8]<br>0x0000000000000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                  |[0x8000078c]:fsgnjx.s ra, zero, sp<br> [0x80000790]:csrrs a3, fcsr, zero<br> [0x80000794]:sd ra, 96(s1)<br>    |
|  32|[0x8000b808]<br>0x0000000000000000<br> |- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                     |[0x800007ac]:fsgnjx.s zero, sp, ra<br> [0x800007b0]:csrrs a3, fcsr, zero<br> [0x800007b4]:sd zero, 112(s1)<br> |
|  33|[0x8000b818]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800007cc]:fsgnjx.s t6, t5, t4<br> [0x800007d0]:csrrs a3, fcsr, zero<br> [0x800007d4]:sd t6, 128(s1)<br>     |
|  34|[0x8000b828]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800007ec]:fsgnjx.s t6, t5, t4<br> [0x800007f0]:csrrs a3, fcsr, zero<br> [0x800007f4]:sd t6, 144(s1)<br>     |
|  35|[0x8000b838]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000080c]:fsgnjx.s t6, t5, t4<br> [0x80000810]:csrrs a3, fcsr, zero<br> [0x80000814]:sd t6, 160(s1)<br>     |
|  36|[0x8000b848]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000082c]:fsgnjx.s t6, t5, t4<br> [0x80000830]:csrrs a3, fcsr, zero<br> [0x80000834]:sd t6, 176(s1)<br>     |
|  37|[0x8000b858]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000084c]:fsgnjx.s t6, t5, t4<br> [0x80000850]:csrrs a3, fcsr, zero<br> [0x80000854]:sd t6, 192(s1)<br>     |
|  38|[0x8000b868]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000086c]:fsgnjx.s t6, t5, t4<br> [0x80000870]:csrrs a3, fcsr, zero<br> [0x80000874]:sd t6, 208(s1)<br>     |
|  39|[0x8000b878]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000088c]:fsgnjx.s t6, t5, t4<br> [0x80000890]:csrrs a3, fcsr, zero<br> [0x80000894]:sd t6, 224(s1)<br>     |
|  40|[0x8000b888]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800008ac]:fsgnjx.s t6, t5, t4<br> [0x800008b0]:csrrs a3, fcsr, zero<br> [0x800008b4]:sd t6, 240(s1)<br>     |
|  41|[0x8000b898]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800008cc]:fsgnjx.s t6, t5, t4<br> [0x800008d0]:csrrs a3, fcsr, zero<br> [0x800008d4]:sd t6, 256(s1)<br>     |
|  42|[0x8000b8a8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800008ec]:fsgnjx.s t6, t5, t4<br> [0x800008f0]:csrrs a3, fcsr, zero<br> [0x800008f4]:sd t6, 272(s1)<br>     |
|  43|[0x8000b8b8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000090c]:fsgnjx.s t6, t5, t4<br> [0x80000910]:csrrs a3, fcsr, zero<br> [0x80000914]:sd t6, 288(s1)<br>     |
|  44|[0x8000b8c8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000092c]:fsgnjx.s t6, t5, t4<br> [0x80000930]:csrrs a3, fcsr, zero<br> [0x80000934]:sd t6, 304(s1)<br>     |
|  45|[0x8000b8d8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000094c]:fsgnjx.s t6, t5, t4<br> [0x80000950]:csrrs a3, fcsr, zero<br> [0x80000954]:sd t6, 320(s1)<br>     |
|  46|[0x8000b8e8]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000096c]:fsgnjx.s t6, t5, t4<br> [0x80000970]:csrrs a3, fcsr, zero<br> [0x80000974]:sd t6, 336(s1)<br>     |
|  47|[0x8000b8f8]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000098c]:fsgnjx.s t6, t5, t4<br> [0x80000990]:csrrs a3, fcsr, zero<br> [0x80000994]:sd t6, 352(s1)<br>     |
|  48|[0x8000b908]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800009ac]:fsgnjx.s t6, t5, t4<br> [0x800009b0]:csrrs a3, fcsr, zero<br> [0x800009b4]:sd t6, 368(s1)<br>     |
|  49|[0x8000b918]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800009cc]:fsgnjx.s t6, t5, t4<br> [0x800009d0]:csrrs a3, fcsr, zero<br> [0x800009d4]:sd t6, 384(s1)<br>     |
|  50|[0x8000b928]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800009ec]:fsgnjx.s t6, t5, t4<br> [0x800009f0]:csrrs a3, fcsr, zero<br> [0x800009f4]:sd t6, 400(s1)<br>     |
|  51|[0x8000b938]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a0c]:fsgnjx.s t6, t5, t4<br> [0x80000a10]:csrrs a3, fcsr, zero<br> [0x80000a14]:sd t6, 416(s1)<br>     |
|  52|[0x8000b948]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000a2c]:fsgnjx.s t6, t5, t4<br> [0x80000a30]:csrrs a3, fcsr, zero<br> [0x80000a34]:sd t6, 432(s1)<br>     |
|  53|[0x8000b958]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a4c]:fsgnjx.s t6, t5, t4<br> [0x80000a50]:csrrs a3, fcsr, zero<br> [0x80000a54]:sd t6, 448(s1)<br>     |
|  54|[0x8000b968]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000a6c]:fsgnjx.s t6, t5, t4<br> [0x80000a70]:csrrs a3, fcsr, zero<br> [0x80000a74]:sd t6, 464(s1)<br>     |
|  55|[0x8000b978]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000a8c]:fsgnjx.s t6, t5, t4<br> [0x80000a90]:csrrs a3, fcsr, zero<br> [0x80000a94]:sd t6, 480(s1)<br>     |
|  56|[0x8000b988]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000aac]:fsgnjx.s t6, t5, t4<br> [0x80000ab0]:csrrs a3, fcsr, zero<br> [0x80000ab4]:sd t6, 496(s1)<br>     |
|  57|[0x8000b998]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000acc]:fsgnjx.s t6, t5, t4<br> [0x80000ad0]:csrrs a3, fcsr, zero<br> [0x80000ad4]:sd t6, 512(s1)<br>     |
|  58|[0x8000b9a8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000aec]:fsgnjx.s t6, t5, t4<br> [0x80000af0]:csrrs a3, fcsr, zero<br> [0x80000af4]:sd t6, 528(s1)<br>     |
|  59|[0x8000b9b8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b0c]:fsgnjx.s t6, t5, t4<br> [0x80000b10]:csrrs a3, fcsr, zero<br> [0x80000b14]:sd t6, 544(s1)<br>     |
|  60|[0x8000b9c8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000b2c]:fsgnjx.s t6, t5, t4<br> [0x80000b30]:csrrs a3, fcsr, zero<br> [0x80000b34]:sd t6, 560(s1)<br>     |
|  61|[0x8000b9d8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b4c]:fsgnjx.s t6, t5, t4<br> [0x80000b50]:csrrs a3, fcsr, zero<br> [0x80000b54]:sd t6, 576(s1)<br>     |
|  62|[0x8000b9e8]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000b6c]:fsgnjx.s t6, t5, t4<br> [0x80000b70]:csrrs a3, fcsr, zero<br> [0x80000b74]:sd t6, 592(s1)<br>     |
|  63|[0x8000b9f8]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000b8c]:fsgnjx.s t6, t5, t4<br> [0x80000b90]:csrrs a3, fcsr, zero<br> [0x80000b94]:sd t6, 608(s1)<br>     |
|  64|[0x8000ba08]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000bac]:fsgnjx.s t6, t5, t4<br> [0x80000bb0]:csrrs a3, fcsr, zero<br> [0x80000bb4]:sd t6, 624(s1)<br>     |
|  65|[0x8000ba18]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000bcc]:fsgnjx.s t6, t5, t4<br> [0x80000bd0]:csrrs a3, fcsr, zero<br> [0x80000bd4]:sd t6, 640(s1)<br>     |
|  66|[0x8000ba28]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000bec]:fsgnjx.s t6, t5, t4<br> [0x80000bf0]:csrrs a3, fcsr, zero<br> [0x80000bf4]:sd t6, 656(s1)<br>     |
|  67|[0x8000ba38]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c0c]:fsgnjx.s t6, t5, t4<br> [0x80000c10]:csrrs a3, fcsr, zero<br> [0x80000c14]:sd t6, 672(s1)<br>     |
|  68|[0x8000ba48]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000c2c]:fsgnjx.s t6, t5, t4<br> [0x80000c30]:csrrs a3, fcsr, zero<br> [0x80000c34]:sd t6, 688(s1)<br>     |
|  69|[0x8000ba58]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c4c]:fsgnjx.s t6, t5, t4<br> [0x80000c50]:csrrs a3, fcsr, zero<br> [0x80000c54]:sd t6, 704(s1)<br>     |
|  70|[0x8000ba68]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000c6c]:fsgnjx.s t6, t5, t4<br> [0x80000c70]:csrrs a3, fcsr, zero<br> [0x80000c74]:sd t6, 720(s1)<br>     |
|  71|[0x8000ba78]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000c8c]:fsgnjx.s t6, t5, t4<br> [0x80000c90]:csrrs a3, fcsr, zero<br> [0x80000c94]:sd t6, 736(s1)<br>     |
|  72|[0x8000ba88]<br>0xFFFFFFFF80000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000cac]:fsgnjx.s t6, t5, t4<br> [0x80000cb0]:csrrs a3, fcsr, zero<br> [0x80000cb4]:sd t6, 752(s1)<br>     |
|  73|[0x8000ba98]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000ccc]:fsgnjx.s t6, t5, t4<br> [0x80000cd0]:csrrs a3, fcsr, zero<br> [0x80000cd4]:sd t6, 768(s1)<br>     |
|  74|[0x8000baa8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000cec]:fsgnjx.s t6, t5, t4<br> [0x80000cf0]:csrrs a3, fcsr, zero<br> [0x80000cf4]:sd t6, 784(s1)<br>     |
|  75|[0x8000bab8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d0c]:fsgnjx.s t6, t5, t4<br> [0x80000d10]:csrrs a3, fcsr, zero<br> [0x80000d14]:sd t6, 800(s1)<br>     |
|  76|[0x8000bac8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000d2c]:fsgnjx.s t6, t5, t4<br> [0x80000d30]:csrrs a3, fcsr, zero<br> [0x80000d34]:sd t6, 816(s1)<br>     |
|  77|[0x8000bad8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d4c]:fsgnjx.s t6, t5, t4<br> [0x80000d50]:csrrs a3, fcsr, zero<br> [0x80000d54]:sd t6, 832(s1)<br>     |
|  78|[0x8000bae8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000d6c]:fsgnjx.s t6, t5, t4<br> [0x80000d70]:csrrs a3, fcsr, zero<br> [0x80000d74]:sd t6, 848(s1)<br>     |
|  79|[0x8000baf8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000d8c]:fsgnjx.s t6, t5, t4<br> [0x80000d90]:csrrs a3, fcsr, zero<br> [0x80000d94]:sd t6, 864(s1)<br>     |
|  80|[0x8000bb08]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000dac]:fsgnjx.s t6, t5, t4<br> [0x80000db0]:csrrs a3, fcsr, zero<br> [0x80000db4]:sd t6, 880(s1)<br>     |
|  81|[0x8000bb18]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000dcc]:fsgnjx.s t6, t5, t4<br> [0x80000dd0]:csrrs a3, fcsr, zero<br> [0x80000dd4]:sd t6, 896(s1)<br>     |
|  82|[0x8000bb28]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000dec]:fsgnjx.s t6, t5, t4<br> [0x80000df0]:csrrs a3, fcsr, zero<br> [0x80000df4]:sd t6, 912(s1)<br>     |
|  83|[0x8000bb38]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e0c]:fsgnjx.s t6, t5, t4<br> [0x80000e10]:csrrs a3, fcsr, zero<br> [0x80000e14]:sd t6, 928(s1)<br>     |
|  84|[0x8000bb48]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000e2c]:fsgnjx.s t6, t5, t4<br> [0x80000e30]:csrrs a3, fcsr, zero<br> [0x80000e34]:sd t6, 944(s1)<br>     |
|  85|[0x8000bb58]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e4c]:fsgnjx.s t6, t5, t4<br> [0x80000e50]:csrrs a3, fcsr, zero<br> [0x80000e54]:sd t6, 960(s1)<br>     |
|  86|[0x8000bb68]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000e6c]:fsgnjx.s t6, t5, t4<br> [0x80000e70]:csrrs a3, fcsr, zero<br> [0x80000e74]:sd t6, 976(s1)<br>     |
|  87|[0x8000bb78]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000e8c]:fsgnjx.s t6, t5, t4<br> [0x80000e90]:csrrs a3, fcsr, zero<br> [0x80000e94]:sd t6, 992(s1)<br>     |
|  88|[0x8000bb88]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000eac]:fsgnjx.s t6, t5, t4<br> [0x80000eb0]:csrrs a3, fcsr, zero<br> [0x80000eb4]:sd t6, 1008(s1)<br>    |
|  89|[0x8000bb98]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000ecc]:fsgnjx.s t6, t5, t4<br> [0x80000ed0]:csrrs a3, fcsr, zero<br> [0x80000ed4]:sd t6, 1024(s1)<br>    |
|  90|[0x8000bba8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000eec]:fsgnjx.s t6, t5, t4<br> [0x80000ef0]:csrrs a3, fcsr, zero<br> [0x80000ef4]:sd t6, 1040(s1)<br>    |
|  91|[0x8000bbb8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f0c]:fsgnjx.s t6, t5, t4<br> [0x80000f10]:csrrs a3, fcsr, zero<br> [0x80000f14]:sd t6, 1056(s1)<br>    |
|  92|[0x8000bbc8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000f2c]:fsgnjx.s t6, t5, t4<br> [0x80000f30]:csrrs a3, fcsr, zero<br> [0x80000f34]:sd t6, 1072(s1)<br>    |
|  93|[0x8000bbd8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f4c]:fsgnjx.s t6, t5, t4<br> [0x80000f50]:csrrs a3, fcsr, zero<br> [0x80000f54]:sd t6, 1088(s1)<br>    |
|  94|[0x8000bbe8]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000f6c]:fsgnjx.s t6, t5, t4<br> [0x80000f70]:csrrs a3, fcsr, zero<br> [0x80000f74]:sd t6, 1104(s1)<br>    |
|  95|[0x8000bbf8]<br>0xFFFFFFFF80000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000f8c]:fsgnjx.s t6, t5, t4<br> [0x80000f90]:csrrs a3, fcsr, zero<br> [0x80000f94]:sd t6, 1120(s1)<br>    |
|  96|[0x8000bc08]<br>0x0000000000000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000fac]:fsgnjx.s t6, t5, t4<br> [0x80000fb0]:csrrs a3, fcsr, zero<br> [0x80000fb4]:sd t6, 1136(s1)<br>    |
|  97|[0x8000bc18]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80000fcc]:fsgnjx.s t6, t5, t4<br> [0x80000fd0]:csrrs a3, fcsr, zero<br> [0x80000fd4]:sd t6, 1152(s1)<br>    |
|  98|[0x8000bc28]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80000fec]:fsgnjx.s t6, t5, t4<br> [0x80000ff0]:csrrs a3, fcsr, zero<br> [0x80000ff4]:sd t6, 1168(s1)<br>    |
|  99|[0x8000bc38]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000100c]:fsgnjx.s t6, t5, t4<br> [0x80001010]:csrrs a3, fcsr, zero<br> [0x80001014]:sd t6, 1184(s1)<br>    |
| 100|[0x8000bc48]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000102c]:fsgnjx.s t6, t5, t4<br> [0x80001030]:csrrs a3, fcsr, zero<br> [0x80001034]:sd t6, 1200(s1)<br>    |
| 101|[0x8000bc58]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000104c]:fsgnjx.s t6, t5, t4<br> [0x80001050]:csrrs a3, fcsr, zero<br> [0x80001054]:sd t6, 1216(s1)<br>    |
| 102|[0x8000bc68]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000106c]:fsgnjx.s t6, t5, t4<br> [0x80001070]:csrrs a3, fcsr, zero<br> [0x80001074]:sd t6, 1232(s1)<br>    |
| 103|[0x8000bc78]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000108c]:fsgnjx.s t6, t5, t4<br> [0x80001090]:csrrs a3, fcsr, zero<br> [0x80001094]:sd t6, 1248(s1)<br>    |
| 104|[0x8000bc88]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800010ac]:fsgnjx.s t6, t5, t4<br> [0x800010b0]:csrrs a3, fcsr, zero<br> [0x800010b4]:sd t6, 1264(s1)<br>    |
| 105|[0x8000bc98]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800010cc]:fsgnjx.s t6, t5, t4<br> [0x800010d0]:csrrs a3, fcsr, zero<br> [0x800010d4]:sd t6, 1280(s1)<br>    |
| 106|[0x8000bca8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800010ec]:fsgnjx.s t6, t5, t4<br> [0x800010f0]:csrrs a3, fcsr, zero<br> [0x800010f4]:sd t6, 1296(s1)<br>    |
| 107|[0x8000bcb8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000110c]:fsgnjx.s t6, t5, t4<br> [0x80001110]:csrrs a3, fcsr, zero<br> [0x80001114]:sd t6, 1312(s1)<br>    |
| 108|[0x8000bcc8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000112c]:fsgnjx.s t6, t5, t4<br> [0x80001130]:csrrs a3, fcsr, zero<br> [0x80001134]:sd t6, 1328(s1)<br>    |
| 109|[0x8000bcd8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000114c]:fsgnjx.s t6, t5, t4<br> [0x80001150]:csrrs a3, fcsr, zero<br> [0x80001154]:sd t6, 1344(s1)<br>    |
| 110|[0x8000bce8]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000116c]:fsgnjx.s t6, t5, t4<br> [0x80001170]:csrrs a3, fcsr, zero<br> [0x80001174]:sd t6, 1360(s1)<br>    |
| 111|[0x8000bcf8]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000118c]:fsgnjx.s t6, t5, t4<br> [0x80001190]:csrrs a3, fcsr, zero<br> [0x80001194]:sd t6, 1376(s1)<br>    |
| 112|[0x8000bd08]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800011ac]:fsgnjx.s t6, t5, t4<br> [0x800011b0]:csrrs a3, fcsr, zero<br> [0x800011b4]:sd t6, 1392(s1)<br>    |
| 113|[0x8000bd18]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800011cc]:fsgnjx.s t6, t5, t4<br> [0x800011d0]:csrrs a3, fcsr, zero<br> [0x800011d4]:sd t6, 1408(s1)<br>    |
| 114|[0x8000bd28]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800011ec]:fsgnjx.s t6, t5, t4<br> [0x800011f0]:csrrs a3, fcsr, zero<br> [0x800011f4]:sd t6, 1424(s1)<br>    |
| 115|[0x8000bd38]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000120c]:fsgnjx.s t6, t5, t4<br> [0x80001210]:csrrs a3, fcsr, zero<br> [0x80001214]:sd t6, 1440(s1)<br>    |
| 116|[0x8000bd48]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000122c]:fsgnjx.s t6, t5, t4<br> [0x80001230]:csrrs a3, fcsr, zero<br> [0x80001234]:sd t6, 1456(s1)<br>    |
| 117|[0x8000bd58]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000124c]:fsgnjx.s t6, t5, t4<br> [0x80001250]:csrrs a3, fcsr, zero<br> [0x80001254]:sd t6, 1472(s1)<br>    |
| 118|[0x8000bd68]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000126c]:fsgnjx.s t6, t5, t4<br> [0x80001270]:csrrs a3, fcsr, zero<br> [0x80001274]:sd t6, 1488(s1)<br>    |
| 119|[0x8000bd78]<br>0x0000000000000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000128c]:fsgnjx.s t6, t5, t4<br> [0x80001290]:csrrs a3, fcsr, zero<br> [0x80001294]:sd t6, 1504(s1)<br>    |
| 120|[0x8000bd88]<br>0xFFFFFFFF80000002<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800012ac]:fsgnjx.s t6, t5, t4<br> [0x800012b0]:csrrs a3, fcsr, zero<br> [0x800012b4]:sd t6, 1520(s1)<br>    |
| 121|[0x8000bd98]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800012cc]:fsgnjx.s t6, t5, t4<br> [0x800012d0]:csrrs a3, fcsr, zero<br> [0x800012d4]:sd t6, 1536(s1)<br>    |
| 122|[0x8000bda8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800012ec]:fsgnjx.s t6, t5, t4<br> [0x800012f0]:csrrs a3, fcsr, zero<br> [0x800012f4]:sd t6, 1552(s1)<br>    |
| 123|[0x8000bdb8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000130c]:fsgnjx.s t6, t5, t4<br> [0x80001310]:csrrs a3, fcsr, zero<br> [0x80001314]:sd t6, 1568(s1)<br>    |
| 124|[0x8000bdc8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000132c]:fsgnjx.s t6, t5, t4<br> [0x80001330]:csrrs a3, fcsr, zero<br> [0x80001334]:sd t6, 1584(s1)<br>    |
| 125|[0x8000bdd8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000134c]:fsgnjx.s t6, t5, t4<br> [0x80001350]:csrrs a3, fcsr, zero<br> [0x80001354]:sd t6, 1600(s1)<br>    |
| 126|[0x8000bde8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000136c]:fsgnjx.s t6, t5, t4<br> [0x80001370]:csrrs a3, fcsr, zero<br> [0x80001374]:sd t6, 1616(s1)<br>    |
| 127|[0x8000bdf8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000138c]:fsgnjx.s t6, t5, t4<br> [0x80001390]:csrrs a3, fcsr, zero<br> [0x80001394]:sd t6, 1632(s1)<br>    |
| 128|[0x8000be08]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800013ac]:fsgnjx.s t6, t5, t4<br> [0x800013b0]:csrrs a3, fcsr, zero<br> [0x800013b4]:sd t6, 1648(s1)<br>    |
| 129|[0x8000be18]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800013cc]:fsgnjx.s t6, t5, t4<br> [0x800013d0]:csrrs a3, fcsr, zero<br> [0x800013d4]:sd t6, 1664(s1)<br>    |
| 130|[0x8000be28]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800013ec]:fsgnjx.s t6, t5, t4<br> [0x800013f0]:csrrs a3, fcsr, zero<br> [0x800013f4]:sd t6, 1680(s1)<br>    |
| 131|[0x8000be38]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000140c]:fsgnjx.s t6, t5, t4<br> [0x80001410]:csrrs a3, fcsr, zero<br> [0x80001414]:sd t6, 1696(s1)<br>    |
| 132|[0x8000be48]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000142c]:fsgnjx.s t6, t5, t4<br> [0x80001430]:csrrs a3, fcsr, zero<br> [0x80001434]:sd t6, 1712(s1)<br>    |
| 133|[0x8000be58]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000144c]:fsgnjx.s t6, t5, t4<br> [0x80001450]:csrrs a3, fcsr, zero<br> [0x80001454]:sd t6, 1728(s1)<br>    |
| 134|[0x8000be68]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000146c]:fsgnjx.s t6, t5, t4<br> [0x80001470]:csrrs a3, fcsr, zero<br> [0x80001474]:sd t6, 1744(s1)<br>    |
| 135|[0x8000be78]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000148c]:fsgnjx.s t6, t5, t4<br> [0x80001490]:csrrs a3, fcsr, zero<br> [0x80001494]:sd t6, 1760(s1)<br>    |
| 136|[0x8000be88]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800014ac]:fsgnjx.s t6, t5, t4<br> [0x800014b0]:csrrs a3, fcsr, zero<br> [0x800014b4]:sd t6, 1776(s1)<br>    |
| 137|[0x8000be98]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800014cc]:fsgnjx.s t6, t5, t4<br> [0x800014d0]:csrrs a3, fcsr, zero<br> [0x800014d4]:sd t6, 1792(s1)<br>    |
| 138|[0x8000bea8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800014ec]:fsgnjx.s t6, t5, t4<br> [0x800014f0]:csrrs a3, fcsr, zero<br> [0x800014f4]:sd t6, 1808(s1)<br>    |
| 139|[0x8000beb8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000150c]:fsgnjx.s t6, t5, t4<br> [0x80001510]:csrrs a3, fcsr, zero<br> [0x80001514]:sd t6, 1824(s1)<br>    |
| 140|[0x8000bec8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000152c]:fsgnjx.s t6, t5, t4<br> [0x80001530]:csrrs a3, fcsr, zero<br> [0x80001534]:sd t6, 1840(s1)<br>    |
| 141|[0x8000bed8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000154c]:fsgnjx.s t6, t5, t4<br> [0x80001550]:csrrs a3, fcsr, zero<br> [0x80001554]:sd t6, 1856(s1)<br>    |
| 142|[0x8000bee8]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000156c]:fsgnjx.s t6, t5, t4<br> [0x80001570]:csrrs a3, fcsr, zero<br> [0x80001574]:sd t6, 1872(s1)<br>    |
| 143|[0x8000bef8]<br>0xFFFFFFFF807FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000158c]:fsgnjx.s t6, t5, t4<br> [0x80001590]:csrrs a3, fcsr, zero<br> [0x80001594]:sd t6, 1888(s1)<br>    |
| 144|[0x8000bf08]<br>0x00000000007FFFFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800015ac]:fsgnjx.s t6, t5, t4<br> [0x800015b0]:csrrs a3, fcsr, zero<br> [0x800015b4]:sd t6, 1904(s1)<br>    |
| 145|[0x8000bf18]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800015cc]:fsgnjx.s t6, t5, t4<br> [0x800015d0]:csrrs a3, fcsr, zero<br> [0x800015d4]:sd t6, 1920(s1)<br>    |
| 146|[0x8000bf28]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800015ec]:fsgnjx.s t6, t5, t4<br> [0x800015f0]:csrrs a3, fcsr, zero<br> [0x800015f4]:sd t6, 1936(s1)<br>    |
| 147|[0x8000bf38]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000160c]:fsgnjx.s t6, t5, t4<br> [0x80001610]:csrrs a3, fcsr, zero<br> [0x80001614]:sd t6, 1952(s1)<br>    |
| 148|[0x8000bf48]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000162c]:fsgnjx.s t6, t5, t4<br> [0x80001630]:csrrs a3, fcsr, zero<br> [0x80001634]:sd t6, 1968(s1)<br>    |
| 149|[0x8000bf58]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000164c]:fsgnjx.s t6, t5, t4<br> [0x80001650]:csrrs a3, fcsr, zero<br> [0x80001654]:sd t6, 1984(s1)<br>    |
| 150|[0x8000bf68]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000168c]:fsgnjx.s t6, t5, t4<br> [0x80001690]:csrrs a3, fcsr, zero<br> [0x80001694]:sd t6, 2000(s1)<br>    |
| 151|[0x8000bf78]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800016cc]:fsgnjx.s t6, t5, t4<br> [0x800016d0]:csrrs a3, fcsr, zero<br> [0x800016d4]:sd t6, 2016(s1)<br>    |
| 152|[0x8000bf88]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000170c]:fsgnjx.s t6, t5, t4<br> [0x80001710]:csrrs a3, fcsr, zero<br> [0x80001714]:sd t6, 2032(s1)<br>    |
| 153|[0x8000bf98]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001754]:fsgnjx.s t6, t5, t4<br> [0x80001758]:csrrs a3, fcsr, zero<br> [0x8000175c]:sd t6, 0(s1)<br>       |
| 154|[0x8000bfa8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001794]:fsgnjx.s t6, t5, t4<br> [0x80001798]:csrrs a3, fcsr, zero<br> [0x8000179c]:sd t6, 16(s1)<br>      |
| 155|[0x8000bfb8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800017d4]:fsgnjx.s t6, t5, t4<br> [0x800017d8]:csrrs a3, fcsr, zero<br> [0x800017dc]:sd t6, 32(s1)<br>      |
| 156|[0x8000bfc8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001814]:fsgnjx.s t6, t5, t4<br> [0x80001818]:csrrs a3, fcsr, zero<br> [0x8000181c]:sd t6, 48(s1)<br>      |
| 157|[0x8000bfd8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001854]:fsgnjx.s t6, t5, t4<br> [0x80001858]:csrrs a3, fcsr, zero<br> [0x8000185c]:sd t6, 64(s1)<br>      |
| 158|[0x8000bfe8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001894]:fsgnjx.s t6, t5, t4<br> [0x80001898]:csrrs a3, fcsr, zero<br> [0x8000189c]:sd t6, 80(s1)<br>      |
| 159|[0x8000bff8]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800018d4]:fsgnjx.s t6, t5, t4<br> [0x800018d8]:csrrs a3, fcsr, zero<br> [0x800018dc]:sd t6, 96(s1)<br>      |
| 160|[0x8000c008]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001914]:fsgnjx.s t6, t5, t4<br> [0x80001918]:csrrs a3, fcsr, zero<br> [0x8000191c]:sd t6, 112(s1)<br>     |
| 161|[0x8000c018]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001954]:fsgnjx.s t6, t5, t4<br> [0x80001958]:csrrs a3, fcsr, zero<br> [0x8000195c]:sd t6, 128(s1)<br>     |
| 162|[0x8000c028]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001994]:fsgnjx.s t6, t5, t4<br> [0x80001998]:csrrs a3, fcsr, zero<br> [0x8000199c]:sd t6, 144(s1)<br>     |
| 163|[0x8000c038]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800019d4]:fsgnjx.s t6, t5, t4<br> [0x800019d8]:csrrs a3, fcsr, zero<br> [0x800019dc]:sd t6, 160(s1)<br>     |
| 164|[0x8000c048]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001a14]:fsgnjx.s t6, t5, t4<br> [0x80001a18]:csrrs a3, fcsr, zero<br> [0x80001a1c]:sd t6, 176(s1)<br>     |
| 165|[0x8000c058]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001a54]:fsgnjx.s t6, t5, t4<br> [0x80001a58]:csrrs a3, fcsr, zero<br> [0x80001a5c]:sd t6, 192(s1)<br>     |
| 166|[0x8000c068]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001a94]:fsgnjx.s t6, t5, t4<br> [0x80001a98]:csrrs a3, fcsr, zero<br> [0x80001a9c]:sd t6, 208(s1)<br>     |
| 167|[0x8000c078]<br>0x00000000007FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001ad4]:fsgnjx.s t6, t5, t4<br> [0x80001ad8]:csrrs a3, fcsr, zero<br> [0x80001adc]:sd t6, 224(s1)<br>     |
| 168|[0x8000c088]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001b14]:fsgnjx.s t6, t5, t4<br> [0x80001b18]:csrrs a3, fcsr, zero<br> [0x80001b1c]:sd t6, 240(s1)<br>     |
| 169|[0x8000c098]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001b54]:fsgnjx.s t6, t5, t4<br> [0x80001b58]:csrrs a3, fcsr, zero<br> [0x80001b5c]:sd t6, 256(s1)<br>     |
| 170|[0x8000c0a8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001b94]:fsgnjx.s t6, t5, t4<br> [0x80001b98]:csrrs a3, fcsr, zero<br> [0x80001b9c]:sd t6, 272(s1)<br>     |
| 171|[0x8000c0b8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001bd4]:fsgnjx.s t6, t5, t4<br> [0x80001bd8]:csrrs a3, fcsr, zero<br> [0x80001bdc]:sd t6, 288(s1)<br>     |
| 172|[0x8000c0c8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001c14]:fsgnjx.s t6, t5, t4<br> [0x80001c18]:csrrs a3, fcsr, zero<br> [0x80001c1c]:sd t6, 304(s1)<br>     |
| 173|[0x8000c0d8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001c54]:fsgnjx.s t6, t5, t4<br> [0x80001c58]:csrrs a3, fcsr, zero<br> [0x80001c5c]:sd t6, 320(s1)<br>     |
| 174|[0x8000c0e8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001c94]:fsgnjx.s t6, t5, t4<br> [0x80001c98]:csrrs a3, fcsr, zero<br> [0x80001c9c]:sd t6, 336(s1)<br>     |
| 175|[0x8000c0f8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001cd4]:fsgnjx.s t6, t5, t4<br> [0x80001cd8]:csrrs a3, fcsr, zero<br> [0x80001cdc]:sd t6, 352(s1)<br>     |
| 176|[0x8000c108]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001d14]:fsgnjx.s t6, t5, t4<br> [0x80001d18]:csrrs a3, fcsr, zero<br> [0x80001d1c]:sd t6, 368(s1)<br>     |
| 177|[0x8000c118]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001d54]:fsgnjx.s t6, t5, t4<br> [0x80001d58]:csrrs a3, fcsr, zero<br> [0x80001d5c]:sd t6, 384(s1)<br>     |
| 178|[0x8000c128]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001d94]:fsgnjx.s t6, t5, t4<br> [0x80001d98]:csrrs a3, fcsr, zero<br> [0x80001d9c]:sd t6, 400(s1)<br>     |
| 179|[0x8000c138]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001dd4]:fsgnjx.s t6, t5, t4<br> [0x80001dd8]:csrrs a3, fcsr, zero<br> [0x80001ddc]:sd t6, 416(s1)<br>     |
| 180|[0x8000c148]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001e14]:fsgnjx.s t6, t5, t4<br> [0x80001e18]:csrrs a3, fcsr, zero<br> [0x80001e1c]:sd t6, 432(s1)<br>     |
| 181|[0x8000c158]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001e54]:fsgnjx.s t6, t5, t4<br> [0x80001e58]:csrrs a3, fcsr, zero<br> [0x80001e5c]:sd t6, 448(s1)<br>     |
| 182|[0x8000c168]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001e94]:fsgnjx.s t6, t5, t4<br> [0x80001e98]:csrrs a3, fcsr, zero<br> [0x80001e9c]:sd t6, 464(s1)<br>     |
| 183|[0x8000c178]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001ed4]:fsgnjx.s t6, t5, t4<br> [0x80001ed8]:csrrs a3, fcsr, zero<br> [0x80001edc]:sd t6, 480(s1)<br>     |
| 184|[0x8000c188]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001f14]:fsgnjx.s t6, t5, t4<br> [0x80001f18]:csrrs a3, fcsr, zero<br> [0x80001f1c]:sd t6, 496(s1)<br>     |
| 185|[0x8000c198]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001f54]:fsgnjx.s t6, t5, t4<br> [0x80001f58]:csrrs a3, fcsr, zero<br> [0x80001f5c]:sd t6, 512(s1)<br>     |
| 186|[0x8000c1a8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80001f94]:fsgnjx.s t6, t5, t4<br> [0x80001f98]:csrrs a3, fcsr, zero<br> [0x80001f9c]:sd t6, 528(s1)<br>     |
| 187|[0x8000c1b8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80001fd4]:fsgnjx.s t6, t5, t4<br> [0x80001fd8]:csrrs a3, fcsr, zero<br> [0x80001fdc]:sd t6, 544(s1)<br>     |
| 188|[0x8000c1c8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002014]:fsgnjx.s t6, t5, t4<br> [0x80002018]:csrrs a3, fcsr, zero<br> [0x8000201c]:sd t6, 560(s1)<br>     |
| 189|[0x8000c1d8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002054]:fsgnjx.s t6, t5, t4<br> [0x80002058]:csrrs a3, fcsr, zero<br> [0x8000205c]:sd t6, 576(s1)<br>     |
| 190|[0x8000c1e8]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002094]:fsgnjx.s t6, t5, t4<br> [0x80002098]:csrrs a3, fcsr, zero<br> [0x8000209c]:sd t6, 592(s1)<br>     |
| 191|[0x8000c1f8]<br>0xFFFFFFFF807FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800020d4]:fsgnjx.s t6, t5, t4<br> [0x800020d8]:csrrs a3, fcsr, zero<br> [0x800020dc]:sd t6, 608(s1)<br>     |
| 192|[0x8000c208]<br>0x00000000007FFFFF<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002114]:fsgnjx.s t6, t5, t4<br> [0x80002118]:csrrs a3, fcsr, zero<br> [0x8000211c]:sd t6, 624(s1)<br>     |
| 193|[0x8000c218]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002154]:fsgnjx.s t6, t5, t4<br> [0x80002158]:csrrs a3, fcsr, zero<br> [0x8000215c]:sd t6, 640(s1)<br>     |
| 194|[0x8000c228]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002194]:fsgnjx.s t6, t5, t4<br> [0x80002198]:csrrs a3, fcsr, zero<br> [0x8000219c]:sd t6, 656(s1)<br>     |
| 195|[0x8000c238]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800021d4]:fsgnjx.s t6, t5, t4<br> [0x800021d8]:csrrs a3, fcsr, zero<br> [0x800021dc]:sd t6, 672(s1)<br>     |
| 196|[0x8000c248]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002214]:fsgnjx.s t6, t5, t4<br> [0x80002218]:csrrs a3, fcsr, zero<br> [0x8000221c]:sd t6, 688(s1)<br>     |
| 197|[0x8000c258]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002254]:fsgnjx.s t6, t5, t4<br> [0x80002258]:csrrs a3, fcsr, zero<br> [0x8000225c]:sd t6, 704(s1)<br>     |
| 198|[0x8000c268]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002294]:fsgnjx.s t6, t5, t4<br> [0x80002298]:csrrs a3, fcsr, zero<br> [0x8000229c]:sd t6, 720(s1)<br>     |
| 199|[0x8000c278]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800022d4]:fsgnjx.s t6, t5, t4<br> [0x800022d8]:csrrs a3, fcsr, zero<br> [0x800022dc]:sd t6, 736(s1)<br>     |
| 200|[0x8000c288]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002314]:fsgnjx.s t6, t5, t4<br> [0x80002318]:csrrs a3, fcsr, zero<br> [0x8000231c]:sd t6, 752(s1)<br>     |
| 201|[0x8000c298]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002354]:fsgnjx.s t6, t5, t4<br> [0x80002358]:csrrs a3, fcsr, zero<br> [0x8000235c]:sd t6, 768(s1)<br>     |
| 202|[0x8000c2a8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002394]:fsgnjx.s t6, t5, t4<br> [0x80002398]:csrrs a3, fcsr, zero<br> [0x8000239c]:sd t6, 784(s1)<br>     |
| 203|[0x8000c2b8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800023d4]:fsgnjx.s t6, t5, t4<br> [0x800023d8]:csrrs a3, fcsr, zero<br> [0x800023dc]:sd t6, 800(s1)<br>     |
| 204|[0x8000c2c8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002414]:fsgnjx.s t6, t5, t4<br> [0x80002418]:csrrs a3, fcsr, zero<br> [0x8000241c]:sd t6, 816(s1)<br>     |
| 205|[0x8000c2d8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002454]:fsgnjx.s t6, t5, t4<br> [0x80002458]:csrrs a3, fcsr, zero<br> [0x8000245c]:sd t6, 832(s1)<br>     |
| 206|[0x8000c2e8]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002494]:fsgnjx.s t6, t5, t4<br> [0x80002498]:csrrs a3, fcsr, zero<br> [0x8000249c]:sd t6, 848(s1)<br>     |
| 207|[0x8000c2f8]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800024d4]:fsgnjx.s t6, t5, t4<br> [0x800024d8]:csrrs a3, fcsr, zero<br> [0x800024dc]:sd t6, 864(s1)<br>     |
| 208|[0x8000c308]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002514]:fsgnjx.s t6, t5, t4<br> [0x80002518]:csrrs a3, fcsr, zero<br> [0x8000251c]:sd t6, 880(s1)<br>     |
| 209|[0x8000c318]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002554]:fsgnjx.s t6, t5, t4<br> [0x80002558]:csrrs a3, fcsr, zero<br> [0x8000255c]:sd t6, 896(s1)<br>     |
| 210|[0x8000c328]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002594]:fsgnjx.s t6, t5, t4<br> [0x80002598]:csrrs a3, fcsr, zero<br> [0x8000259c]:sd t6, 912(s1)<br>     |
| 211|[0x8000c338]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800025d4]:fsgnjx.s t6, t5, t4<br> [0x800025d8]:csrrs a3, fcsr, zero<br> [0x800025dc]:sd t6, 928(s1)<br>     |
| 212|[0x8000c348]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002614]:fsgnjx.s t6, t5, t4<br> [0x80002618]:csrrs a3, fcsr, zero<br> [0x8000261c]:sd t6, 944(s1)<br>     |
| 213|[0x8000c358]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002654]:fsgnjx.s t6, t5, t4<br> [0x80002658]:csrrs a3, fcsr, zero<br> [0x8000265c]:sd t6, 960(s1)<br>     |
| 214|[0x8000c368]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002694]:fsgnjx.s t6, t5, t4<br> [0x80002698]:csrrs a3, fcsr, zero<br> [0x8000269c]:sd t6, 976(s1)<br>     |
| 215|[0x8000c378]<br>0x0000000000800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800026d4]:fsgnjx.s t6, t5, t4<br> [0x800026d8]:csrrs a3, fcsr, zero<br> [0x800026dc]:sd t6, 992(s1)<br>     |
| 216|[0x8000c388]<br>0xFFFFFFFF80800000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002714]:fsgnjx.s t6, t5, t4<br> [0x80002718]:csrrs a3, fcsr, zero<br> [0x8000271c]:sd t6, 1008(s1)<br>    |
| 217|[0x8000c398]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002754]:fsgnjx.s t6, t5, t4<br> [0x80002758]:csrrs a3, fcsr, zero<br> [0x8000275c]:sd t6, 1024(s1)<br>    |
| 218|[0x8000c3a8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002794]:fsgnjx.s t6, t5, t4<br> [0x80002798]:csrrs a3, fcsr, zero<br> [0x8000279c]:sd t6, 1040(s1)<br>    |
| 219|[0x8000c3b8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800027d4]:fsgnjx.s t6, t5, t4<br> [0x800027d8]:csrrs a3, fcsr, zero<br> [0x800027dc]:sd t6, 1056(s1)<br>    |
| 220|[0x8000c3c8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002814]:fsgnjx.s t6, t5, t4<br> [0x80002818]:csrrs a3, fcsr, zero<br> [0x8000281c]:sd t6, 1072(s1)<br>    |
| 221|[0x8000c3d8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002854]:fsgnjx.s t6, t5, t4<br> [0x80002858]:csrrs a3, fcsr, zero<br> [0x8000285c]:sd t6, 1088(s1)<br>    |
| 222|[0x8000c3e8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002894]:fsgnjx.s t6, t5, t4<br> [0x80002898]:csrrs a3, fcsr, zero<br> [0x8000289c]:sd t6, 1104(s1)<br>    |
| 223|[0x8000c3f8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800028d4]:fsgnjx.s t6, t5, t4<br> [0x800028d8]:csrrs a3, fcsr, zero<br> [0x800028dc]:sd t6, 1120(s1)<br>    |
| 224|[0x8000c408]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002914]:fsgnjx.s t6, t5, t4<br> [0x80002918]:csrrs a3, fcsr, zero<br> [0x8000291c]:sd t6, 1136(s1)<br>    |
| 225|[0x8000c418]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002954]:fsgnjx.s t6, t5, t4<br> [0x80002958]:csrrs a3, fcsr, zero<br> [0x8000295c]:sd t6, 1152(s1)<br>    |
| 226|[0x8000c428]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002994]:fsgnjx.s t6, t5, t4<br> [0x80002998]:csrrs a3, fcsr, zero<br> [0x8000299c]:sd t6, 1168(s1)<br>    |
| 227|[0x8000c438]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800029d4]:fsgnjx.s t6, t5, t4<br> [0x800029d8]:csrrs a3, fcsr, zero<br> [0x800029dc]:sd t6, 1184(s1)<br>    |
| 228|[0x8000c448]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002a14]:fsgnjx.s t6, t5, t4<br> [0x80002a18]:csrrs a3, fcsr, zero<br> [0x80002a1c]:sd t6, 1200(s1)<br>    |
| 229|[0x8000c458]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002a54]:fsgnjx.s t6, t5, t4<br> [0x80002a58]:csrrs a3, fcsr, zero<br> [0x80002a5c]:sd t6, 1216(s1)<br>    |
| 230|[0x8000c468]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002a94]:fsgnjx.s t6, t5, t4<br> [0x80002a98]:csrrs a3, fcsr, zero<br> [0x80002a9c]:sd t6, 1232(s1)<br>    |
| 231|[0x8000c478]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002ad4]:fsgnjx.s t6, t5, t4<br> [0x80002ad8]:csrrs a3, fcsr, zero<br> [0x80002adc]:sd t6, 1248(s1)<br>    |
| 232|[0x8000c488]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002b14]:fsgnjx.s t6, t5, t4<br> [0x80002b18]:csrrs a3, fcsr, zero<br> [0x80002b1c]:sd t6, 1264(s1)<br>    |
| 233|[0x8000c498]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002b54]:fsgnjx.s t6, t5, t4<br> [0x80002b58]:csrrs a3, fcsr, zero<br> [0x80002b5c]:sd t6, 1280(s1)<br>    |
| 234|[0x8000c4a8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002b94]:fsgnjx.s t6, t5, t4<br> [0x80002b98]:csrrs a3, fcsr, zero<br> [0x80002b9c]:sd t6, 1296(s1)<br>    |
| 235|[0x8000c4b8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002bd4]:fsgnjx.s t6, t5, t4<br> [0x80002bd8]:csrrs a3, fcsr, zero<br> [0x80002bdc]:sd t6, 1312(s1)<br>    |
| 236|[0x8000c4c8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002c14]:fsgnjx.s t6, t5, t4<br> [0x80002c18]:csrrs a3, fcsr, zero<br> [0x80002c1c]:sd t6, 1328(s1)<br>    |
| 237|[0x8000c4d8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002c54]:fsgnjx.s t6, t5, t4<br> [0x80002c58]:csrrs a3, fcsr, zero<br> [0x80002c5c]:sd t6, 1344(s1)<br>    |
| 238|[0x8000c4e8]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002c94]:fsgnjx.s t6, t5, t4<br> [0x80002c98]:csrrs a3, fcsr, zero<br> [0x80002c9c]:sd t6, 1360(s1)<br>    |
| 239|[0x8000c4f8]<br>0xFFFFFFFF80800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002cd4]:fsgnjx.s t6, t5, t4<br> [0x80002cd8]:csrrs a3, fcsr, zero<br> [0x80002cdc]:sd t6, 1376(s1)<br>    |
| 240|[0x8000c508]<br>0x0000000000800000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002d14]:fsgnjx.s t6, t5, t4<br> [0x80002d18]:csrrs a3, fcsr, zero<br> [0x80002d1c]:sd t6, 1392(s1)<br>    |
| 241|[0x8000c518]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002d54]:fsgnjx.s t6, t5, t4<br> [0x80002d58]:csrrs a3, fcsr, zero<br> [0x80002d5c]:sd t6, 1408(s1)<br>    |
| 242|[0x8000c528]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002d94]:fsgnjx.s t6, t5, t4<br> [0x80002d98]:csrrs a3, fcsr, zero<br> [0x80002d9c]:sd t6, 1424(s1)<br>    |
| 243|[0x8000c538]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002dd4]:fsgnjx.s t6, t5, t4<br> [0x80002dd8]:csrrs a3, fcsr, zero<br> [0x80002ddc]:sd t6, 1440(s1)<br>    |
| 244|[0x8000c548]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002e14]:fsgnjx.s t6, t5, t4<br> [0x80002e18]:csrrs a3, fcsr, zero<br> [0x80002e1c]:sd t6, 1456(s1)<br>    |
| 245|[0x8000c558]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002e54]:fsgnjx.s t6, t5, t4<br> [0x80002e58]:csrrs a3, fcsr, zero<br> [0x80002e5c]:sd t6, 1472(s1)<br>    |
| 246|[0x8000c568]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002e94]:fsgnjx.s t6, t5, t4<br> [0x80002e98]:csrrs a3, fcsr, zero<br> [0x80002e9c]:sd t6, 1488(s1)<br>    |
| 247|[0x8000c578]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002ed4]:fsgnjx.s t6, t5, t4<br> [0x80002ed8]:csrrs a3, fcsr, zero<br> [0x80002edc]:sd t6, 1504(s1)<br>    |
| 248|[0x8000c588]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002f14]:fsgnjx.s t6, t5, t4<br> [0x80002f18]:csrrs a3, fcsr, zero<br> [0x80002f1c]:sd t6, 1520(s1)<br>    |
| 249|[0x8000c598]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002f54]:fsgnjx.s t6, t5, t4<br> [0x80002f58]:csrrs a3, fcsr, zero<br> [0x80002f5c]:sd t6, 1536(s1)<br>    |
| 250|[0x8000c5a8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80002f94]:fsgnjx.s t6, t5, t4<br> [0x80002f98]:csrrs a3, fcsr, zero<br> [0x80002f9c]:sd t6, 1552(s1)<br>    |
| 251|[0x8000c5b8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80002fd4]:fsgnjx.s t6, t5, t4<br> [0x80002fd8]:csrrs a3, fcsr, zero<br> [0x80002fdc]:sd t6, 1568(s1)<br>    |
| 252|[0x8000c5c8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003014]:fsgnjx.s t6, t5, t4<br> [0x80003018]:csrrs a3, fcsr, zero<br> [0x8000301c]:sd t6, 1584(s1)<br>    |
| 253|[0x8000c5d8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003054]:fsgnjx.s t6, t5, t4<br> [0x80003058]:csrrs a3, fcsr, zero<br> [0x8000305c]:sd t6, 1600(s1)<br>    |
| 254|[0x8000c5e8]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003094]:fsgnjx.s t6, t5, t4<br> [0x80003098]:csrrs a3, fcsr, zero<br> [0x8000309c]:sd t6, 1616(s1)<br>    |
| 255|[0x8000c5f8]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800030d4]:fsgnjx.s t6, t5, t4<br> [0x800030d8]:csrrs a3, fcsr, zero<br> [0x800030dc]:sd t6, 1632(s1)<br>    |
| 256|[0x8000c608]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003114]:fsgnjx.s t6, t5, t4<br> [0x80003118]:csrrs a3, fcsr, zero<br> [0x8000311c]:sd t6, 1648(s1)<br>    |
| 257|[0x8000c618]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003154]:fsgnjx.s t6, t5, t4<br> [0x80003158]:csrrs a3, fcsr, zero<br> [0x8000315c]:sd t6, 1664(s1)<br>    |
| 258|[0x8000c628]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003194]:fsgnjx.s t6, t5, t4<br> [0x80003198]:csrrs a3, fcsr, zero<br> [0x8000319c]:sd t6, 1680(s1)<br>    |
| 259|[0x8000c638]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800031d4]:fsgnjx.s t6, t5, t4<br> [0x800031d8]:csrrs a3, fcsr, zero<br> [0x800031dc]:sd t6, 1696(s1)<br>    |
| 260|[0x8000c648]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003214]:fsgnjx.s t6, t5, t4<br> [0x80003218]:csrrs a3, fcsr, zero<br> [0x8000321c]:sd t6, 1712(s1)<br>    |
| 261|[0x8000c658]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003254]:fsgnjx.s t6, t5, t4<br> [0x80003258]:csrrs a3, fcsr, zero<br> [0x8000325c]:sd t6, 1728(s1)<br>    |
| 262|[0x8000c668]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003294]:fsgnjx.s t6, t5, t4<br> [0x80003298]:csrrs a3, fcsr, zero<br> [0x8000329c]:sd t6, 1744(s1)<br>    |
| 263|[0x8000c678]<br>0x0000000000800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800032d4]:fsgnjx.s t6, t5, t4<br> [0x800032d8]:csrrs a3, fcsr, zero<br> [0x800032dc]:sd t6, 1760(s1)<br>    |
| 264|[0x8000c688]<br>0xFFFFFFFF80800001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003314]:fsgnjx.s t6, t5, t4<br> [0x80003318]:csrrs a3, fcsr, zero<br> [0x8000331c]:sd t6, 1776(s1)<br>    |
| 265|[0x8000c698]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003354]:fsgnjx.s t6, t5, t4<br> [0x80003358]:csrrs a3, fcsr, zero<br> [0x8000335c]:sd t6, 1792(s1)<br>    |
| 266|[0x8000c6a8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003394]:fsgnjx.s t6, t5, t4<br> [0x80003398]:csrrs a3, fcsr, zero<br> [0x8000339c]:sd t6, 1808(s1)<br>    |
| 267|[0x8000c6b8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800033d4]:fsgnjx.s t6, t5, t4<br> [0x800033d8]:csrrs a3, fcsr, zero<br> [0x800033dc]:sd t6, 1824(s1)<br>    |
| 268|[0x8000c6c8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003414]:fsgnjx.s t6, t5, t4<br> [0x80003418]:csrrs a3, fcsr, zero<br> [0x8000341c]:sd t6, 1840(s1)<br>    |
| 269|[0x8000c6d8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003454]:fsgnjx.s t6, t5, t4<br> [0x80003458]:csrrs a3, fcsr, zero<br> [0x8000345c]:sd t6, 1856(s1)<br>    |
| 270|[0x8000c6e8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003494]:fsgnjx.s t6, t5, t4<br> [0x80003498]:csrrs a3, fcsr, zero<br> [0x8000349c]:sd t6, 1872(s1)<br>    |
| 271|[0x8000c6f8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800034d4]:fsgnjx.s t6, t5, t4<br> [0x800034d8]:csrrs a3, fcsr, zero<br> [0x800034dc]:sd t6, 1888(s1)<br>    |
| 272|[0x8000c708]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003514]:fsgnjx.s t6, t5, t4<br> [0x80003518]:csrrs a3, fcsr, zero<br> [0x8000351c]:sd t6, 1904(s1)<br>    |
| 273|[0x8000c718]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003554]:fsgnjx.s t6, t5, t4<br> [0x80003558]:csrrs a3, fcsr, zero<br> [0x8000355c]:sd t6, 1920(s1)<br>    |
| 274|[0x8000c728]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003594]:fsgnjx.s t6, t5, t4<br> [0x80003598]:csrrs a3, fcsr, zero<br> [0x8000359c]:sd t6, 1936(s1)<br>    |
| 275|[0x8000c738]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800035d4]:fsgnjx.s t6, t5, t4<br> [0x800035d8]:csrrs a3, fcsr, zero<br> [0x800035dc]:sd t6, 1952(s1)<br>    |
| 276|[0x8000c748]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003614]:fsgnjx.s t6, t5, t4<br> [0x80003618]:csrrs a3, fcsr, zero<br> [0x8000361c]:sd t6, 1968(s1)<br>    |
| 277|[0x8000c758]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003654]:fsgnjx.s t6, t5, t4<br> [0x80003658]:csrrs a3, fcsr, zero<br> [0x8000365c]:sd t6, 1984(s1)<br>    |
| 278|[0x8000c768]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000368c]:fsgnjx.s t6, t5, t4<br> [0x80003690]:csrrs a3, fcsr, zero<br> [0x80003694]:sd t6, 2000(s1)<br>    |
| 279|[0x8000c778]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800036c4]:fsgnjx.s t6, t5, t4<br> [0x800036c8]:csrrs a3, fcsr, zero<br> [0x800036cc]:sd t6, 2016(s1)<br>    |
| 280|[0x8000c788]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800036fc]:fsgnjx.s t6, t5, t4<br> [0x80003700]:csrrs a3, fcsr, zero<br> [0x80003704]:sd t6, 2032(s1)<br>    |
| 281|[0x8000c798]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000373c]:fsgnjx.s t6, t5, t4<br> [0x80003740]:csrrs a3, fcsr, zero<br> [0x80003744]:sd t6, 0(s1)<br>       |
| 282|[0x8000c7a8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003774]:fsgnjx.s t6, t5, t4<br> [0x80003778]:csrrs a3, fcsr, zero<br> [0x8000377c]:sd t6, 16(s1)<br>      |
| 283|[0x8000c7b8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800037ac]:fsgnjx.s t6, t5, t4<br> [0x800037b0]:csrrs a3, fcsr, zero<br> [0x800037b4]:sd t6, 32(s1)<br>      |
| 284|[0x8000c7c8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800037e4]:fsgnjx.s t6, t5, t4<br> [0x800037e8]:csrrs a3, fcsr, zero<br> [0x800037ec]:sd t6, 48(s1)<br>      |
| 285|[0x8000c7d8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000381c]:fsgnjx.s t6, t5, t4<br> [0x80003820]:csrrs a3, fcsr, zero<br> [0x80003824]:sd t6, 64(s1)<br>      |
| 286|[0x8000c7e8]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003854]:fsgnjx.s t6, t5, t4<br> [0x80003858]:csrrs a3, fcsr, zero<br> [0x8000385c]:sd t6, 80(s1)<br>      |
| 287|[0x8000c7f8]<br>0xFFFFFFFF80855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000388c]:fsgnjx.s t6, t5, t4<br> [0x80003890]:csrrs a3, fcsr, zero<br> [0x80003894]:sd t6, 96(s1)<br>      |
| 288|[0x8000c808]<br>0x0000000000855555<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800038c4]:fsgnjx.s t6, t5, t4<br> [0x800038c8]:csrrs a3, fcsr, zero<br> [0x800038cc]:sd t6, 112(s1)<br>     |
| 289|[0x8000c818]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800038fc]:fsgnjx.s t6, t5, t4<br> [0x80003900]:csrrs a3, fcsr, zero<br> [0x80003904]:sd t6, 128(s1)<br>     |
| 290|[0x8000c828]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003934]:fsgnjx.s t6, t5, t4<br> [0x80003938]:csrrs a3, fcsr, zero<br> [0x8000393c]:sd t6, 144(s1)<br>     |
| 291|[0x8000c838]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000396c]:fsgnjx.s t6, t5, t4<br> [0x80003970]:csrrs a3, fcsr, zero<br> [0x80003974]:sd t6, 160(s1)<br>     |
| 292|[0x8000c848]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800039a4]:fsgnjx.s t6, t5, t4<br> [0x800039a8]:csrrs a3, fcsr, zero<br> [0x800039ac]:sd t6, 176(s1)<br>     |
| 293|[0x8000c858]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800039dc]:fsgnjx.s t6, t5, t4<br> [0x800039e0]:csrrs a3, fcsr, zero<br> [0x800039e4]:sd t6, 192(s1)<br>     |
| 294|[0x8000c868]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003a14]:fsgnjx.s t6, t5, t4<br> [0x80003a18]:csrrs a3, fcsr, zero<br> [0x80003a1c]:sd t6, 208(s1)<br>     |
| 295|[0x8000c878]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003a4c]:fsgnjx.s t6, t5, t4<br> [0x80003a50]:csrrs a3, fcsr, zero<br> [0x80003a54]:sd t6, 224(s1)<br>     |
| 296|[0x8000c888]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003a84]:fsgnjx.s t6, t5, t4<br> [0x80003a88]:csrrs a3, fcsr, zero<br> [0x80003a8c]:sd t6, 240(s1)<br>     |
| 297|[0x8000c898]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003abc]:fsgnjx.s t6, t5, t4<br> [0x80003ac0]:csrrs a3, fcsr, zero<br> [0x80003ac4]:sd t6, 256(s1)<br>     |
| 298|[0x8000c8a8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003af4]:fsgnjx.s t6, t5, t4<br> [0x80003af8]:csrrs a3, fcsr, zero<br> [0x80003afc]:sd t6, 272(s1)<br>     |
| 299|[0x8000c8b8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003b2c]:fsgnjx.s t6, t5, t4<br> [0x80003b30]:csrrs a3, fcsr, zero<br> [0x80003b34]:sd t6, 288(s1)<br>     |
| 300|[0x8000c8c8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003b64]:fsgnjx.s t6, t5, t4<br> [0x80003b68]:csrrs a3, fcsr, zero<br> [0x80003b6c]:sd t6, 304(s1)<br>     |
| 301|[0x8000c8d8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003b9c]:fsgnjx.s t6, t5, t4<br> [0x80003ba0]:csrrs a3, fcsr, zero<br> [0x80003ba4]:sd t6, 320(s1)<br>     |
| 302|[0x8000c8e8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003bd4]:fsgnjx.s t6, t5, t4<br> [0x80003bd8]:csrrs a3, fcsr, zero<br> [0x80003bdc]:sd t6, 336(s1)<br>     |
| 303|[0x8000c8f8]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003c0c]:fsgnjx.s t6, t5, t4<br> [0x80003c10]:csrrs a3, fcsr, zero<br> [0x80003c14]:sd t6, 352(s1)<br>     |
| 304|[0x8000c908]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003c44]:fsgnjx.s t6, t5, t4<br> [0x80003c48]:csrrs a3, fcsr, zero<br> [0x80003c4c]:sd t6, 368(s1)<br>     |
| 305|[0x8000c918]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003c7c]:fsgnjx.s t6, t5, t4<br> [0x80003c80]:csrrs a3, fcsr, zero<br> [0x80003c84]:sd t6, 384(s1)<br>     |
| 306|[0x8000c928]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003cb4]:fsgnjx.s t6, t5, t4<br> [0x80003cb8]:csrrs a3, fcsr, zero<br> [0x80003cbc]:sd t6, 400(s1)<br>     |
| 307|[0x8000c938]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003cec]:fsgnjx.s t6, t5, t4<br> [0x80003cf0]:csrrs a3, fcsr, zero<br> [0x80003cf4]:sd t6, 416(s1)<br>     |
| 308|[0x8000c948]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003d24]:fsgnjx.s t6, t5, t4<br> [0x80003d28]:csrrs a3, fcsr, zero<br> [0x80003d2c]:sd t6, 432(s1)<br>     |
| 309|[0x8000c958]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003d5c]:fsgnjx.s t6, t5, t4<br> [0x80003d60]:csrrs a3, fcsr, zero<br> [0x80003d64]:sd t6, 448(s1)<br>     |
| 310|[0x8000c968]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003d94]:fsgnjx.s t6, t5, t4<br> [0x80003d98]:csrrs a3, fcsr, zero<br> [0x80003d9c]:sd t6, 464(s1)<br>     |
| 311|[0x8000c978]<br>0x000000007F7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003dcc]:fsgnjx.s t6, t5, t4<br> [0x80003dd0]:csrrs a3, fcsr, zero<br> [0x80003dd4]:sd t6, 480(s1)<br>     |
| 312|[0x8000c988]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003e04]:fsgnjx.s t6, t5, t4<br> [0x80003e08]:csrrs a3, fcsr, zero<br> [0x80003e0c]:sd t6, 496(s1)<br>     |
| 313|[0x8000c998]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003e3c]:fsgnjx.s t6, t5, t4<br> [0x80003e40]:csrrs a3, fcsr, zero<br> [0x80003e44]:sd t6, 512(s1)<br>     |
| 314|[0x8000c9a8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003e74]:fsgnjx.s t6, t5, t4<br> [0x80003e78]:csrrs a3, fcsr, zero<br> [0x80003e7c]:sd t6, 528(s1)<br>     |
| 315|[0x8000c9b8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003eac]:fsgnjx.s t6, t5, t4<br> [0x80003eb0]:csrrs a3, fcsr, zero<br> [0x80003eb4]:sd t6, 544(s1)<br>     |
| 316|[0x8000c9c8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003ee4]:fsgnjx.s t6, t5, t4<br> [0x80003ee8]:csrrs a3, fcsr, zero<br> [0x80003eec]:sd t6, 560(s1)<br>     |
| 317|[0x8000c9d8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003f1c]:fsgnjx.s t6, t5, t4<br> [0x80003f20]:csrrs a3, fcsr, zero<br> [0x80003f24]:sd t6, 576(s1)<br>     |
| 318|[0x8000c9e8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003f54]:fsgnjx.s t6, t5, t4<br> [0x80003f58]:csrrs a3, fcsr, zero<br> [0x80003f5c]:sd t6, 592(s1)<br>     |
| 319|[0x8000c9f8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003f8c]:fsgnjx.s t6, t5, t4<br> [0x80003f90]:csrrs a3, fcsr, zero<br> [0x80003f94]:sd t6, 608(s1)<br>     |
| 320|[0x8000ca08]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80003fc4]:fsgnjx.s t6, t5, t4<br> [0x80003fc8]:csrrs a3, fcsr, zero<br> [0x80003fcc]:sd t6, 624(s1)<br>     |
| 321|[0x8000ca18]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80003ffc]:fsgnjx.s t6, t5, t4<br> [0x80004000]:csrrs a3, fcsr, zero<br> [0x80004004]:sd t6, 640(s1)<br>     |
| 322|[0x8000ca28]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004034]:fsgnjx.s t6, t5, t4<br> [0x80004038]:csrrs a3, fcsr, zero<br> [0x8000403c]:sd t6, 656(s1)<br>     |
| 323|[0x8000ca38]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000406c]:fsgnjx.s t6, t5, t4<br> [0x80004070]:csrrs a3, fcsr, zero<br> [0x80004074]:sd t6, 672(s1)<br>     |
| 324|[0x8000ca48]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800040a4]:fsgnjx.s t6, t5, t4<br> [0x800040a8]:csrrs a3, fcsr, zero<br> [0x800040ac]:sd t6, 688(s1)<br>     |
| 325|[0x8000ca58]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800040dc]:fsgnjx.s t6, t5, t4<br> [0x800040e0]:csrrs a3, fcsr, zero<br> [0x800040e4]:sd t6, 704(s1)<br>     |
| 326|[0x8000ca68]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004114]:fsgnjx.s t6, t5, t4<br> [0x80004118]:csrrs a3, fcsr, zero<br> [0x8000411c]:sd t6, 720(s1)<br>     |
| 327|[0x8000ca78]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000414c]:fsgnjx.s t6, t5, t4<br> [0x80004150]:csrrs a3, fcsr, zero<br> [0x80004154]:sd t6, 736(s1)<br>     |
| 328|[0x8000ca88]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004184]:fsgnjx.s t6, t5, t4<br> [0x80004188]:csrrs a3, fcsr, zero<br> [0x8000418c]:sd t6, 752(s1)<br>     |
| 329|[0x8000ca98]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800041bc]:fsgnjx.s t6, t5, t4<br> [0x800041c0]:csrrs a3, fcsr, zero<br> [0x800041c4]:sd t6, 768(s1)<br>     |
| 330|[0x8000caa8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800041f4]:fsgnjx.s t6, t5, t4<br> [0x800041f8]:csrrs a3, fcsr, zero<br> [0x800041fc]:sd t6, 784(s1)<br>     |
| 331|[0x8000cab8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000422c]:fsgnjx.s t6, t5, t4<br> [0x80004230]:csrrs a3, fcsr, zero<br> [0x80004234]:sd t6, 800(s1)<br>     |
| 332|[0x8000cac8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004264]:fsgnjx.s t6, t5, t4<br> [0x80004268]:csrrs a3, fcsr, zero<br> [0x8000426c]:sd t6, 816(s1)<br>     |
| 333|[0x8000cad8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000429c]:fsgnjx.s t6, t5, t4<br> [0x800042a0]:csrrs a3, fcsr, zero<br> [0x800042a4]:sd t6, 832(s1)<br>     |
| 334|[0x8000cae8]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800042d4]:fsgnjx.s t6, t5, t4<br> [0x800042d8]:csrrs a3, fcsr, zero<br> [0x800042dc]:sd t6, 848(s1)<br>     |
| 335|[0x8000caf8]<br>0xFFFFFFFFFF7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000430c]:fsgnjx.s t6, t5, t4<br> [0x80004310]:csrrs a3, fcsr, zero<br> [0x80004314]:sd t6, 864(s1)<br>     |
| 336|[0x8000cb08]<br>0x000000007F7FFFFF<br> |- fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004344]:fsgnjx.s t6, t5, t4<br> [0x80004348]:csrrs a3, fcsr, zero<br> [0x8000434c]:sd t6, 880(s1)<br>     |
| 337|[0x8000cb18]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000437c]:fsgnjx.s t6, t5, t4<br> [0x80004380]:csrrs a3, fcsr, zero<br> [0x80004384]:sd t6, 896(s1)<br>     |
| 338|[0x8000cb28]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800043b4]:fsgnjx.s t6, t5, t4<br> [0x800043b8]:csrrs a3, fcsr, zero<br> [0x800043bc]:sd t6, 912(s1)<br>     |
| 339|[0x8000cb38]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800043ec]:fsgnjx.s t6, t5, t4<br> [0x800043f0]:csrrs a3, fcsr, zero<br> [0x800043f4]:sd t6, 928(s1)<br>     |
| 340|[0x8000cb48]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004424]:fsgnjx.s t6, t5, t4<br> [0x80004428]:csrrs a3, fcsr, zero<br> [0x8000442c]:sd t6, 944(s1)<br>     |
| 341|[0x8000cb58]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000445c]:fsgnjx.s t6, t5, t4<br> [0x80004460]:csrrs a3, fcsr, zero<br> [0x80004464]:sd t6, 960(s1)<br>     |
| 342|[0x8000cb68]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004494]:fsgnjx.s t6, t5, t4<br> [0x80004498]:csrrs a3, fcsr, zero<br> [0x8000449c]:sd t6, 976(s1)<br>     |
| 343|[0x8000cb78]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800044cc]:fsgnjx.s t6, t5, t4<br> [0x800044d0]:csrrs a3, fcsr, zero<br> [0x800044d4]:sd t6, 992(s1)<br>     |
| 344|[0x8000cb88]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004504]:fsgnjx.s t6, t5, t4<br> [0x80004508]:csrrs a3, fcsr, zero<br> [0x8000450c]:sd t6, 1008(s1)<br>    |
| 345|[0x8000cb98]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000453c]:fsgnjx.s t6, t5, t4<br> [0x80004540]:csrrs a3, fcsr, zero<br> [0x80004544]:sd t6, 1024(s1)<br>    |
| 346|[0x8000cba8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004574]:fsgnjx.s t6, t5, t4<br> [0x80004578]:csrrs a3, fcsr, zero<br> [0x8000457c]:sd t6, 1040(s1)<br>    |
| 347|[0x8000cbb8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800045ac]:fsgnjx.s t6, t5, t4<br> [0x800045b0]:csrrs a3, fcsr, zero<br> [0x800045b4]:sd t6, 1056(s1)<br>    |
| 348|[0x8000cbc8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800045e4]:fsgnjx.s t6, t5, t4<br> [0x800045e8]:csrrs a3, fcsr, zero<br> [0x800045ec]:sd t6, 1072(s1)<br>    |
| 349|[0x8000cbd8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000461c]:fsgnjx.s t6, t5, t4<br> [0x80004620]:csrrs a3, fcsr, zero<br> [0x80004624]:sd t6, 1088(s1)<br>    |
| 350|[0x8000cbe8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004654]:fsgnjx.s t6, t5, t4<br> [0x80004658]:csrrs a3, fcsr, zero<br> [0x8000465c]:sd t6, 1104(s1)<br>    |
| 351|[0x8000cbf8]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000468c]:fsgnjx.s t6, t5, t4<br> [0x80004690]:csrrs a3, fcsr, zero<br> [0x80004694]:sd t6, 1120(s1)<br>    |
| 352|[0x8000cc08]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800046c4]:fsgnjx.s t6, t5, t4<br> [0x800046c8]:csrrs a3, fcsr, zero<br> [0x800046cc]:sd t6, 1136(s1)<br>    |
| 353|[0x8000cc18]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800046fc]:fsgnjx.s t6, t5, t4<br> [0x80004700]:csrrs a3, fcsr, zero<br> [0x80004704]:sd t6, 1152(s1)<br>    |
| 354|[0x8000cc28]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004734]:fsgnjx.s t6, t5, t4<br> [0x80004738]:csrrs a3, fcsr, zero<br> [0x8000473c]:sd t6, 1168(s1)<br>    |
| 355|[0x8000cc38]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000476c]:fsgnjx.s t6, t5, t4<br> [0x80004770]:csrrs a3, fcsr, zero<br> [0x80004774]:sd t6, 1184(s1)<br>    |
| 356|[0x8000cc48]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800047a4]:fsgnjx.s t6, t5, t4<br> [0x800047a8]:csrrs a3, fcsr, zero<br> [0x800047ac]:sd t6, 1200(s1)<br>    |
| 357|[0x8000cc58]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800047dc]:fsgnjx.s t6, t5, t4<br> [0x800047e0]:csrrs a3, fcsr, zero<br> [0x800047e4]:sd t6, 1216(s1)<br>    |
| 358|[0x8000cc68]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004814]:fsgnjx.s t6, t5, t4<br> [0x80004818]:csrrs a3, fcsr, zero<br> [0x8000481c]:sd t6, 1232(s1)<br>    |
| 359|[0x8000cc78]<br>0x000000007F800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000484c]:fsgnjx.s t6, t5, t4<br> [0x80004850]:csrrs a3, fcsr, zero<br> [0x80004854]:sd t6, 1248(s1)<br>    |
| 360|[0x8000cc88]<br>0xFFFFFFFFFF800000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004884]:fsgnjx.s t6, t5, t4<br> [0x80004888]:csrrs a3, fcsr, zero<br> [0x8000488c]:sd t6, 1264(s1)<br>    |
| 361|[0x8000cc98]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800048bc]:fsgnjx.s t6, t5, t4<br> [0x800048c0]:csrrs a3, fcsr, zero<br> [0x800048c4]:sd t6, 1280(s1)<br>    |
| 362|[0x8000cca8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800048f4]:fsgnjx.s t6, t5, t4<br> [0x800048f8]:csrrs a3, fcsr, zero<br> [0x800048fc]:sd t6, 1296(s1)<br>    |
| 363|[0x8000ccb8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000492c]:fsgnjx.s t6, t5, t4<br> [0x80004930]:csrrs a3, fcsr, zero<br> [0x80004934]:sd t6, 1312(s1)<br>    |
| 364|[0x8000ccc8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004964]:fsgnjx.s t6, t5, t4<br> [0x80004968]:csrrs a3, fcsr, zero<br> [0x8000496c]:sd t6, 1328(s1)<br>    |
| 365|[0x8000ccd8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000499c]:fsgnjx.s t6, t5, t4<br> [0x800049a0]:csrrs a3, fcsr, zero<br> [0x800049a4]:sd t6, 1344(s1)<br>    |
| 366|[0x8000cce8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800049d4]:fsgnjx.s t6, t5, t4<br> [0x800049d8]:csrrs a3, fcsr, zero<br> [0x800049dc]:sd t6, 1360(s1)<br>    |
| 367|[0x8000ccf8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004a0c]:fsgnjx.s t6, t5, t4<br> [0x80004a10]:csrrs a3, fcsr, zero<br> [0x80004a14]:sd t6, 1376(s1)<br>    |
| 368|[0x8000cd08]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004a44]:fsgnjx.s t6, t5, t4<br> [0x80004a48]:csrrs a3, fcsr, zero<br> [0x80004a4c]:sd t6, 1392(s1)<br>    |
| 369|[0x8000cd18]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004a7c]:fsgnjx.s t6, t5, t4<br> [0x80004a80]:csrrs a3, fcsr, zero<br> [0x80004a84]:sd t6, 1408(s1)<br>    |
| 370|[0x8000cd28]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004ab4]:fsgnjx.s t6, t5, t4<br> [0x80004ab8]:csrrs a3, fcsr, zero<br> [0x80004abc]:sd t6, 1424(s1)<br>    |
| 371|[0x8000cd38]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004aec]:fsgnjx.s t6, t5, t4<br> [0x80004af0]:csrrs a3, fcsr, zero<br> [0x80004af4]:sd t6, 1440(s1)<br>    |
| 372|[0x8000cd48]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004b24]:fsgnjx.s t6, t5, t4<br> [0x80004b28]:csrrs a3, fcsr, zero<br> [0x80004b2c]:sd t6, 1456(s1)<br>    |
| 373|[0x8000cd58]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004b5c]:fsgnjx.s t6, t5, t4<br> [0x80004b60]:csrrs a3, fcsr, zero<br> [0x80004b64]:sd t6, 1472(s1)<br>    |
| 374|[0x8000cd68]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004b94]:fsgnjx.s t6, t5, t4<br> [0x80004b98]:csrrs a3, fcsr, zero<br> [0x80004b9c]:sd t6, 1488(s1)<br>    |
| 375|[0x8000cd78]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004bcc]:fsgnjx.s t6, t5, t4<br> [0x80004bd0]:csrrs a3, fcsr, zero<br> [0x80004bd4]:sd t6, 1504(s1)<br>    |
| 376|[0x8000cd88]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004c04]:fsgnjx.s t6, t5, t4<br> [0x80004c08]:csrrs a3, fcsr, zero<br> [0x80004c0c]:sd t6, 1520(s1)<br>    |
| 377|[0x8000cd98]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004c3c]:fsgnjx.s t6, t5, t4<br> [0x80004c40]:csrrs a3, fcsr, zero<br> [0x80004c44]:sd t6, 1536(s1)<br>    |
| 378|[0x8000cda8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004c74]:fsgnjx.s t6, t5, t4<br> [0x80004c78]:csrrs a3, fcsr, zero<br> [0x80004c7c]:sd t6, 1552(s1)<br>    |
| 379|[0x8000cdb8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004cac]:fsgnjx.s t6, t5, t4<br> [0x80004cb0]:csrrs a3, fcsr, zero<br> [0x80004cb4]:sd t6, 1568(s1)<br>    |
| 380|[0x8000cdc8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004ce4]:fsgnjx.s t6, t5, t4<br> [0x80004ce8]:csrrs a3, fcsr, zero<br> [0x80004cec]:sd t6, 1584(s1)<br>    |
| 381|[0x8000cdd8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004d1c]:fsgnjx.s t6, t5, t4<br> [0x80004d20]:csrrs a3, fcsr, zero<br> [0x80004d24]:sd t6, 1600(s1)<br>    |
| 382|[0x8000cde8]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004d54]:fsgnjx.s t6, t5, t4<br> [0x80004d58]:csrrs a3, fcsr, zero<br> [0x80004d5c]:sd t6, 1616(s1)<br>    |
| 383|[0x8000cdf8]<br>0xFFFFFFFFFF800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004d8c]:fsgnjx.s t6, t5, t4<br> [0x80004d90]:csrrs a3, fcsr, zero<br> [0x80004d94]:sd t6, 1632(s1)<br>    |
| 384|[0x8000ce08]<br>0x000000007F800000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004dc4]:fsgnjx.s t6, t5, t4<br> [0x80004dc8]:csrrs a3, fcsr, zero<br> [0x80004dcc]:sd t6, 1648(s1)<br>    |
| 385|[0x8000ce18]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004dfc]:fsgnjx.s t6, t5, t4<br> [0x80004e00]:csrrs a3, fcsr, zero<br> [0x80004e04]:sd t6, 1664(s1)<br>    |
| 386|[0x8000ce28]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004e34]:fsgnjx.s t6, t5, t4<br> [0x80004e38]:csrrs a3, fcsr, zero<br> [0x80004e3c]:sd t6, 1680(s1)<br>    |
| 387|[0x8000ce38]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004e6c]:fsgnjx.s t6, t5, t4<br> [0x80004e70]:csrrs a3, fcsr, zero<br> [0x80004e74]:sd t6, 1696(s1)<br>    |
| 388|[0x8000ce48]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004ea4]:fsgnjx.s t6, t5, t4<br> [0x80004ea8]:csrrs a3, fcsr, zero<br> [0x80004eac]:sd t6, 1712(s1)<br>    |
| 389|[0x8000ce58]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004edc]:fsgnjx.s t6, t5, t4<br> [0x80004ee0]:csrrs a3, fcsr, zero<br> [0x80004ee4]:sd t6, 1728(s1)<br>    |
| 390|[0x8000ce68]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004f14]:fsgnjx.s t6, t5, t4<br> [0x80004f18]:csrrs a3, fcsr, zero<br> [0x80004f1c]:sd t6, 1744(s1)<br>    |
| 391|[0x8000ce78]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004f4c]:fsgnjx.s t6, t5, t4<br> [0x80004f50]:csrrs a3, fcsr, zero<br> [0x80004f54]:sd t6, 1760(s1)<br>    |
| 392|[0x8000ce88]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004f84]:fsgnjx.s t6, t5, t4<br> [0x80004f88]:csrrs a3, fcsr, zero<br> [0x80004f8c]:sd t6, 1776(s1)<br>    |
| 393|[0x8000ce98]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80004fbc]:fsgnjx.s t6, t5, t4<br> [0x80004fc0]:csrrs a3, fcsr, zero<br> [0x80004fc4]:sd t6, 1792(s1)<br>    |
| 394|[0x8000cea8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80004ff4]:fsgnjx.s t6, t5, t4<br> [0x80004ff8]:csrrs a3, fcsr, zero<br> [0x80004ffc]:sd t6, 1808(s1)<br>    |
| 395|[0x8000ceb8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000502c]:fsgnjx.s t6, t5, t4<br> [0x80005030]:csrrs a3, fcsr, zero<br> [0x80005034]:sd t6, 1824(s1)<br>    |
| 396|[0x8000cec8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005064]:fsgnjx.s t6, t5, t4<br> [0x80005068]:csrrs a3, fcsr, zero<br> [0x8000506c]:sd t6, 1840(s1)<br>    |
| 397|[0x8000ced8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000509c]:fsgnjx.s t6, t5, t4<br> [0x800050a0]:csrrs a3, fcsr, zero<br> [0x800050a4]:sd t6, 1856(s1)<br>    |
| 398|[0x8000cee8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800050d4]:fsgnjx.s t6, t5, t4<br> [0x800050d8]:csrrs a3, fcsr, zero<br> [0x800050dc]:sd t6, 1872(s1)<br>    |
| 399|[0x8000cef8]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000510c]:fsgnjx.s t6, t5, t4<br> [0x80005110]:csrrs a3, fcsr, zero<br> [0x80005114]:sd t6, 1888(s1)<br>    |
| 400|[0x8000cf08]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005144]:fsgnjx.s t6, t5, t4<br> [0x80005148]:csrrs a3, fcsr, zero<br> [0x8000514c]:sd t6, 1904(s1)<br>    |
| 401|[0x8000cf18]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000517c]:fsgnjx.s t6, t5, t4<br> [0x80005180]:csrrs a3, fcsr, zero<br> [0x80005184]:sd t6, 1920(s1)<br>    |
| 402|[0x8000cf28]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800051b4]:fsgnjx.s t6, t5, t4<br> [0x800051b8]:csrrs a3, fcsr, zero<br> [0x800051bc]:sd t6, 1936(s1)<br>    |
| 403|[0x8000cf38]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800051ec]:fsgnjx.s t6, t5, t4<br> [0x800051f0]:csrrs a3, fcsr, zero<br> [0x800051f4]:sd t6, 1952(s1)<br>    |
| 404|[0x8000cf48]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005224]:fsgnjx.s t6, t5, t4<br> [0x80005228]:csrrs a3, fcsr, zero<br> [0x8000522c]:sd t6, 1968(s1)<br>    |
| 405|[0x8000cf58]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000525c]:fsgnjx.s t6, t5, t4<br> [0x80005260]:csrrs a3, fcsr, zero<br> [0x80005264]:sd t6, 1984(s1)<br>    |
| 406|[0x8000cf68]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000529c]:fsgnjx.s t6, t5, t4<br> [0x800052a0]:csrrs a3, fcsr, zero<br> [0x800052a4]:sd t6, 2000(s1)<br>    |
| 407|[0x8000cf78]<br>0x000000007FC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800052dc]:fsgnjx.s t6, t5, t4<br> [0x800052e0]:csrrs a3, fcsr, zero<br> [0x800052e4]:sd t6, 2016(s1)<br>    |
| 408|[0x8000cf88]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000531c]:fsgnjx.s t6, t5, t4<br> [0x80005320]:csrrs a3, fcsr, zero<br> [0x80005324]:sd t6, 2032(s1)<br>    |
| 409|[0x8000cf98]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005364]:fsgnjx.s t6, t5, t4<br> [0x80005368]:csrrs a3, fcsr, zero<br> [0x8000536c]:sd t6, 0(s1)<br>       |
| 410|[0x8000cfa8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800053a4]:fsgnjx.s t6, t5, t4<br> [0x800053a8]:csrrs a3, fcsr, zero<br> [0x800053ac]:sd t6, 16(s1)<br>      |
| 411|[0x8000cfb8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800053e4]:fsgnjx.s t6, t5, t4<br> [0x800053e8]:csrrs a3, fcsr, zero<br> [0x800053ec]:sd t6, 32(s1)<br>      |
| 412|[0x8000cfc8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005424]:fsgnjx.s t6, t5, t4<br> [0x80005428]:csrrs a3, fcsr, zero<br> [0x8000542c]:sd t6, 48(s1)<br>      |
| 413|[0x8000cfd8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005464]:fsgnjx.s t6, t5, t4<br> [0x80005468]:csrrs a3, fcsr, zero<br> [0x8000546c]:sd t6, 64(s1)<br>      |
| 414|[0x8000cfe8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800054a4]:fsgnjx.s t6, t5, t4<br> [0x800054a8]:csrrs a3, fcsr, zero<br> [0x800054ac]:sd t6, 80(s1)<br>      |
| 415|[0x8000cff8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800054e4]:fsgnjx.s t6, t5, t4<br> [0x800054e8]:csrrs a3, fcsr, zero<br> [0x800054ec]:sd t6, 96(s1)<br>      |
| 416|[0x8000d008]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005524]:fsgnjx.s t6, t5, t4<br> [0x80005528]:csrrs a3, fcsr, zero<br> [0x8000552c]:sd t6, 112(s1)<br>     |
| 417|[0x8000d018]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005564]:fsgnjx.s t6, t5, t4<br> [0x80005568]:csrrs a3, fcsr, zero<br> [0x8000556c]:sd t6, 128(s1)<br>     |
| 418|[0x8000d028]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800055a4]:fsgnjx.s t6, t5, t4<br> [0x800055a8]:csrrs a3, fcsr, zero<br> [0x800055ac]:sd t6, 144(s1)<br>     |
| 419|[0x8000d038]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800055e4]:fsgnjx.s t6, t5, t4<br> [0x800055e8]:csrrs a3, fcsr, zero<br> [0x800055ec]:sd t6, 160(s1)<br>     |
| 420|[0x8000d048]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005624]:fsgnjx.s t6, t5, t4<br> [0x80005628]:csrrs a3, fcsr, zero<br> [0x8000562c]:sd t6, 176(s1)<br>     |
| 421|[0x8000d058]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005664]:fsgnjx.s t6, t5, t4<br> [0x80005668]:csrrs a3, fcsr, zero<br> [0x8000566c]:sd t6, 192(s1)<br>     |
| 422|[0x8000d068]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800056a4]:fsgnjx.s t6, t5, t4<br> [0x800056a8]:csrrs a3, fcsr, zero<br> [0x800056ac]:sd t6, 208(s1)<br>     |
| 423|[0x8000d078]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800056e4]:fsgnjx.s t6, t5, t4<br> [0x800056e8]:csrrs a3, fcsr, zero<br> [0x800056ec]:sd t6, 224(s1)<br>     |
| 424|[0x8000d088]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005724]:fsgnjx.s t6, t5, t4<br> [0x80005728]:csrrs a3, fcsr, zero<br> [0x8000572c]:sd t6, 240(s1)<br>     |
| 425|[0x8000d098]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005764]:fsgnjx.s t6, t5, t4<br> [0x80005768]:csrrs a3, fcsr, zero<br> [0x8000576c]:sd t6, 256(s1)<br>     |
| 426|[0x8000d0a8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800057a4]:fsgnjx.s t6, t5, t4<br> [0x800057a8]:csrrs a3, fcsr, zero<br> [0x800057ac]:sd t6, 272(s1)<br>     |
| 427|[0x8000d0b8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800057e4]:fsgnjx.s t6, t5, t4<br> [0x800057e8]:csrrs a3, fcsr, zero<br> [0x800057ec]:sd t6, 288(s1)<br>     |
| 428|[0x8000d0c8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005824]:fsgnjx.s t6, t5, t4<br> [0x80005828]:csrrs a3, fcsr, zero<br> [0x8000582c]:sd t6, 304(s1)<br>     |
| 429|[0x8000d0d8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005864]:fsgnjx.s t6, t5, t4<br> [0x80005868]:csrrs a3, fcsr, zero<br> [0x8000586c]:sd t6, 320(s1)<br>     |
| 430|[0x8000d0e8]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800058a4]:fsgnjx.s t6, t5, t4<br> [0x800058a8]:csrrs a3, fcsr, zero<br> [0x800058ac]:sd t6, 336(s1)<br>     |
| 431|[0x8000d0f8]<br>0xFFFFFFFFFFC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800058e4]:fsgnjx.s t6, t5, t4<br> [0x800058e8]:csrrs a3, fcsr, zero<br> [0x800058ec]:sd t6, 352(s1)<br>     |
| 432|[0x8000d108]<br>0x000000007FC00000<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005924]:fsgnjx.s t6, t5, t4<br> [0x80005928]:csrrs a3, fcsr, zero<br> [0x8000592c]:sd t6, 368(s1)<br>     |
| 433|[0x8000d118]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005964]:fsgnjx.s t6, t5, t4<br> [0x80005968]:csrrs a3, fcsr, zero<br> [0x8000596c]:sd t6, 384(s1)<br>     |
| 434|[0x8000d128]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800059a4]:fsgnjx.s t6, t5, t4<br> [0x800059a8]:csrrs a3, fcsr, zero<br> [0x800059ac]:sd t6, 400(s1)<br>     |
| 435|[0x8000d138]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800059e4]:fsgnjx.s t6, t5, t4<br> [0x800059e8]:csrrs a3, fcsr, zero<br> [0x800059ec]:sd t6, 416(s1)<br>     |
| 436|[0x8000d148]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005a24]:fsgnjx.s t6, t5, t4<br> [0x80005a28]:csrrs a3, fcsr, zero<br> [0x80005a2c]:sd t6, 432(s1)<br>     |
| 437|[0x8000d158]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005a64]:fsgnjx.s t6, t5, t4<br> [0x80005a68]:csrrs a3, fcsr, zero<br> [0x80005a6c]:sd t6, 448(s1)<br>     |
| 438|[0x8000d168]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005aa4]:fsgnjx.s t6, t5, t4<br> [0x80005aa8]:csrrs a3, fcsr, zero<br> [0x80005aac]:sd t6, 464(s1)<br>     |
| 439|[0x8000d178]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005ae4]:fsgnjx.s t6, t5, t4<br> [0x80005ae8]:csrrs a3, fcsr, zero<br> [0x80005aec]:sd t6, 480(s1)<br>     |
| 440|[0x8000d188]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005b24]:fsgnjx.s t6, t5, t4<br> [0x80005b28]:csrrs a3, fcsr, zero<br> [0x80005b2c]:sd t6, 496(s1)<br>     |
| 441|[0x8000d198]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005b64]:fsgnjx.s t6, t5, t4<br> [0x80005b68]:csrrs a3, fcsr, zero<br> [0x80005b6c]:sd t6, 512(s1)<br>     |
| 442|[0x8000d1a8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005ba4]:fsgnjx.s t6, t5, t4<br> [0x80005ba8]:csrrs a3, fcsr, zero<br> [0x80005bac]:sd t6, 528(s1)<br>     |
| 443|[0x8000d1b8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005be4]:fsgnjx.s t6, t5, t4<br> [0x80005be8]:csrrs a3, fcsr, zero<br> [0x80005bec]:sd t6, 544(s1)<br>     |
| 444|[0x8000d1c8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005c24]:fsgnjx.s t6, t5, t4<br> [0x80005c28]:csrrs a3, fcsr, zero<br> [0x80005c2c]:sd t6, 560(s1)<br>     |
| 445|[0x8000d1d8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005c64]:fsgnjx.s t6, t5, t4<br> [0x80005c68]:csrrs a3, fcsr, zero<br> [0x80005c6c]:sd t6, 576(s1)<br>     |
| 446|[0x8000d1e8]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005ca4]:fsgnjx.s t6, t5, t4<br> [0x80005ca8]:csrrs a3, fcsr, zero<br> [0x80005cac]:sd t6, 592(s1)<br>     |
| 447|[0x8000d1f8]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005ce4]:fsgnjx.s t6, t5, t4<br> [0x80005ce8]:csrrs a3, fcsr, zero<br> [0x80005cec]:sd t6, 608(s1)<br>     |
| 448|[0x8000d208]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005d24]:fsgnjx.s t6, t5, t4<br> [0x80005d28]:csrrs a3, fcsr, zero<br> [0x80005d2c]:sd t6, 624(s1)<br>     |
| 449|[0x8000d218]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005d64]:fsgnjx.s t6, t5, t4<br> [0x80005d68]:csrrs a3, fcsr, zero<br> [0x80005d6c]:sd t6, 640(s1)<br>     |
| 450|[0x8000d228]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005da4]:fsgnjx.s t6, t5, t4<br> [0x80005da8]:csrrs a3, fcsr, zero<br> [0x80005dac]:sd t6, 656(s1)<br>     |
| 451|[0x8000d238]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005de4]:fsgnjx.s t6, t5, t4<br> [0x80005de8]:csrrs a3, fcsr, zero<br> [0x80005dec]:sd t6, 672(s1)<br>     |
| 452|[0x8000d248]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005e24]:fsgnjx.s t6, t5, t4<br> [0x80005e28]:csrrs a3, fcsr, zero<br> [0x80005e2c]:sd t6, 688(s1)<br>     |
| 453|[0x8000d258]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005e64]:fsgnjx.s t6, t5, t4<br> [0x80005e68]:csrrs a3, fcsr, zero<br> [0x80005e6c]:sd t6, 704(s1)<br>     |
| 454|[0x8000d268]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005ea4]:fsgnjx.s t6, t5, t4<br> [0x80005ea8]:csrrs a3, fcsr, zero<br> [0x80005eac]:sd t6, 720(s1)<br>     |
| 455|[0x8000d278]<br>0x000000007FC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005ee4]:fsgnjx.s t6, t5, t4<br> [0x80005ee8]:csrrs a3, fcsr, zero<br> [0x80005eec]:sd t6, 736(s1)<br>     |
| 456|[0x8000d288]<br>0xFFFFFFFFFFC00001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005f24]:fsgnjx.s t6, t5, t4<br> [0x80005f28]:csrrs a3, fcsr, zero<br> [0x80005f2c]:sd t6, 752(s1)<br>     |
| 457|[0x8000d298]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005f64]:fsgnjx.s t6, t5, t4<br> [0x80005f68]:csrrs a3, fcsr, zero<br> [0x80005f6c]:sd t6, 768(s1)<br>     |
| 458|[0x8000d2a8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80005fa4]:fsgnjx.s t6, t5, t4<br> [0x80005fa8]:csrrs a3, fcsr, zero<br> [0x80005fac]:sd t6, 784(s1)<br>     |
| 459|[0x8000d2b8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80005fe4]:fsgnjx.s t6, t5, t4<br> [0x80005fe8]:csrrs a3, fcsr, zero<br> [0x80005fec]:sd t6, 800(s1)<br>     |
| 460|[0x8000d2c8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006024]:fsgnjx.s t6, t5, t4<br> [0x80006028]:csrrs a3, fcsr, zero<br> [0x8000602c]:sd t6, 816(s1)<br>     |
| 461|[0x8000d2d8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006064]:fsgnjx.s t6, t5, t4<br> [0x80006068]:csrrs a3, fcsr, zero<br> [0x8000606c]:sd t6, 832(s1)<br>     |
| 462|[0x8000d2e8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800060a4]:fsgnjx.s t6, t5, t4<br> [0x800060a8]:csrrs a3, fcsr, zero<br> [0x800060ac]:sd t6, 848(s1)<br>     |
| 463|[0x8000d2f8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800060e4]:fsgnjx.s t6, t5, t4<br> [0x800060e8]:csrrs a3, fcsr, zero<br> [0x800060ec]:sd t6, 864(s1)<br>     |
| 464|[0x8000d308]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006124]:fsgnjx.s t6, t5, t4<br> [0x80006128]:csrrs a3, fcsr, zero<br> [0x8000612c]:sd t6, 880(s1)<br>     |
| 465|[0x8000d318]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006164]:fsgnjx.s t6, t5, t4<br> [0x80006168]:csrrs a3, fcsr, zero<br> [0x8000616c]:sd t6, 896(s1)<br>     |
| 466|[0x8000d328]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800061a4]:fsgnjx.s t6, t5, t4<br> [0x800061a8]:csrrs a3, fcsr, zero<br> [0x800061ac]:sd t6, 912(s1)<br>     |
| 467|[0x8000d338]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800061e4]:fsgnjx.s t6, t5, t4<br> [0x800061e8]:csrrs a3, fcsr, zero<br> [0x800061ec]:sd t6, 928(s1)<br>     |
| 468|[0x8000d348]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006224]:fsgnjx.s t6, t5, t4<br> [0x80006228]:csrrs a3, fcsr, zero<br> [0x8000622c]:sd t6, 944(s1)<br>     |
| 469|[0x8000d358]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006264]:fsgnjx.s t6, t5, t4<br> [0x80006268]:csrrs a3, fcsr, zero<br> [0x8000626c]:sd t6, 960(s1)<br>     |
| 470|[0x8000d368]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800062a4]:fsgnjx.s t6, t5, t4<br> [0x800062a8]:csrrs a3, fcsr, zero<br> [0x800062ac]:sd t6, 976(s1)<br>     |
| 471|[0x8000d378]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800062e4]:fsgnjx.s t6, t5, t4<br> [0x800062e8]:csrrs a3, fcsr, zero<br> [0x800062ec]:sd t6, 992(s1)<br>     |
| 472|[0x8000d388]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006324]:fsgnjx.s t6, t5, t4<br> [0x80006328]:csrrs a3, fcsr, zero<br> [0x8000632c]:sd t6, 1008(s1)<br>    |
| 473|[0x8000d398]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006364]:fsgnjx.s t6, t5, t4<br> [0x80006368]:csrrs a3, fcsr, zero<br> [0x8000636c]:sd t6, 1024(s1)<br>    |
| 474|[0x8000d3a8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800063a4]:fsgnjx.s t6, t5, t4<br> [0x800063a8]:csrrs a3, fcsr, zero<br> [0x800063ac]:sd t6, 1040(s1)<br>    |
| 475|[0x8000d3b8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800063e4]:fsgnjx.s t6, t5, t4<br> [0x800063e8]:csrrs a3, fcsr, zero<br> [0x800063ec]:sd t6, 1056(s1)<br>    |
| 476|[0x8000d3c8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006424]:fsgnjx.s t6, t5, t4<br> [0x80006428]:csrrs a3, fcsr, zero<br> [0x8000642c]:sd t6, 1072(s1)<br>    |
| 477|[0x8000d3d8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006464]:fsgnjx.s t6, t5, t4<br> [0x80006468]:csrrs a3, fcsr, zero<br> [0x8000646c]:sd t6, 1088(s1)<br>    |
| 478|[0x8000d3e8]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800064a4]:fsgnjx.s t6, t5, t4<br> [0x800064a8]:csrrs a3, fcsr, zero<br> [0x800064ac]:sd t6, 1104(s1)<br>    |
| 479|[0x8000d3f8]<br>0xFFFFFFFFFFC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800064e4]:fsgnjx.s t6, t5, t4<br> [0x800064e8]:csrrs a3, fcsr, zero<br> [0x800064ec]:sd t6, 1120(s1)<br>    |
| 480|[0x8000d408]<br>0x000000007FC55555<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006524]:fsgnjx.s t6, t5, t4<br> [0x80006528]:csrrs a3, fcsr, zero<br> [0x8000652c]:sd t6, 1136(s1)<br>    |
| 481|[0x8000d418]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006564]:fsgnjx.s t6, t5, t4<br> [0x80006568]:csrrs a3, fcsr, zero<br> [0x8000656c]:sd t6, 1152(s1)<br>    |
| 482|[0x8000d428]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800065a4]:fsgnjx.s t6, t5, t4<br> [0x800065a8]:csrrs a3, fcsr, zero<br> [0x800065ac]:sd t6, 1168(s1)<br>    |
| 483|[0x8000d438]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800065e4]:fsgnjx.s t6, t5, t4<br> [0x800065e8]:csrrs a3, fcsr, zero<br> [0x800065ec]:sd t6, 1184(s1)<br>    |
| 484|[0x8000d448]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006624]:fsgnjx.s t6, t5, t4<br> [0x80006628]:csrrs a3, fcsr, zero<br> [0x8000662c]:sd t6, 1200(s1)<br>    |
| 485|[0x8000d458]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006664]:fsgnjx.s t6, t5, t4<br> [0x80006668]:csrrs a3, fcsr, zero<br> [0x8000666c]:sd t6, 1216(s1)<br>    |
| 486|[0x8000d468]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800066a4]:fsgnjx.s t6, t5, t4<br> [0x800066a8]:csrrs a3, fcsr, zero<br> [0x800066ac]:sd t6, 1232(s1)<br>    |
| 487|[0x8000d478]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800066e4]:fsgnjx.s t6, t5, t4<br> [0x800066e8]:csrrs a3, fcsr, zero<br> [0x800066ec]:sd t6, 1248(s1)<br>    |
| 488|[0x8000d488]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006724]:fsgnjx.s t6, t5, t4<br> [0x80006728]:csrrs a3, fcsr, zero<br> [0x8000672c]:sd t6, 1264(s1)<br>    |
| 489|[0x8000d498]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006764]:fsgnjx.s t6, t5, t4<br> [0x80006768]:csrrs a3, fcsr, zero<br> [0x8000676c]:sd t6, 1280(s1)<br>    |
| 490|[0x8000d4a8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800067a4]:fsgnjx.s t6, t5, t4<br> [0x800067a8]:csrrs a3, fcsr, zero<br> [0x800067ac]:sd t6, 1296(s1)<br>    |
| 491|[0x8000d4b8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800067e4]:fsgnjx.s t6, t5, t4<br> [0x800067e8]:csrrs a3, fcsr, zero<br> [0x800067ec]:sd t6, 1312(s1)<br>    |
| 492|[0x8000d4c8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006824]:fsgnjx.s t6, t5, t4<br> [0x80006828]:csrrs a3, fcsr, zero<br> [0x8000682c]:sd t6, 1328(s1)<br>    |
| 493|[0x8000d4d8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006864]:fsgnjx.s t6, t5, t4<br> [0x80006868]:csrrs a3, fcsr, zero<br> [0x8000686c]:sd t6, 1344(s1)<br>    |
| 494|[0x8000d4e8]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800068a4]:fsgnjx.s t6, t5, t4<br> [0x800068a8]:csrrs a3, fcsr, zero<br> [0x800068ac]:sd t6, 1360(s1)<br>    |
| 495|[0x8000d4f8]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800068e4]:fsgnjx.s t6, t5, t4<br> [0x800068e8]:csrrs a3, fcsr, zero<br> [0x800068ec]:sd t6, 1376(s1)<br>    |
| 496|[0x8000d508]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006924]:fsgnjx.s t6, t5, t4<br> [0x80006928]:csrrs a3, fcsr, zero<br> [0x8000692c]:sd t6, 1392(s1)<br>    |
| 497|[0x8000d518]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006964]:fsgnjx.s t6, t5, t4<br> [0x80006968]:csrrs a3, fcsr, zero<br> [0x8000696c]:sd t6, 1408(s1)<br>    |
| 498|[0x8000d528]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800069a4]:fsgnjx.s t6, t5, t4<br> [0x800069a8]:csrrs a3, fcsr, zero<br> [0x800069ac]:sd t6, 1424(s1)<br>    |
| 499|[0x8000d538]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800069e4]:fsgnjx.s t6, t5, t4<br> [0x800069e8]:csrrs a3, fcsr, zero<br> [0x800069ec]:sd t6, 1440(s1)<br>    |
| 500|[0x8000d548]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006a24]:fsgnjx.s t6, t5, t4<br> [0x80006a28]:csrrs a3, fcsr, zero<br> [0x80006a2c]:sd t6, 1456(s1)<br>    |
| 501|[0x8000d558]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006a64]:fsgnjx.s t6, t5, t4<br> [0x80006a68]:csrrs a3, fcsr, zero<br> [0x80006a6c]:sd t6, 1472(s1)<br>    |
| 502|[0x8000d568]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006aa4]:fsgnjx.s t6, t5, t4<br> [0x80006aa8]:csrrs a3, fcsr, zero<br> [0x80006aac]:sd t6, 1488(s1)<br>    |
| 503|[0x8000d578]<br>0x000000007F800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006ae4]:fsgnjx.s t6, t5, t4<br> [0x80006ae8]:csrrs a3, fcsr, zero<br> [0x80006aec]:sd t6, 1504(s1)<br>    |
| 504|[0x8000d588]<br>0xFFFFFFFFFF800001<br> |- fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006b24]:fsgnjx.s t6, t5, t4<br> [0x80006b28]:csrrs a3, fcsr, zero<br> [0x80006b2c]:sd t6, 1520(s1)<br>    |
| 505|[0x8000d598]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006b64]:fsgnjx.s t6, t5, t4<br> [0x80006b68]:csrrs a3, fcsr, zero<br> [0x80006b6c]:sd t6, 1536(s1)<br>    |
| 506|[0x8000d5a8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006ba4]:fsgnjx.s t6, t5, t4<br> [0x80006ba8]:csrrs a3, fcsr, zero<br> [0x80006bac]:sd t6, 1552(s1)<br>    |
| 507|[0x8000d5b8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006be4]:fsgnjx.s t6, t5, t4<br> [0x80006be8]:csrrs a3, fcsr, zero<br> [0x80006bec]:sd t6, 1568(s1)<br>    |
| 508|[0x8000d5c8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006c24]:fsgnjx.s t6, t5, t4<br> [0x80006c28]:csrrs a3, fcsr, zero<br> [0x80006c2c]:sd t6, 1584(s1)<br>    |
| 509|[0x8000d5d8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006c64]:fsgnjx.s t6, t5, t4<br> [0x80006c68]:csrrs a3, fcsr, zero<br> [0x80006c6c]:sd t6, 1600(s1)<br>    |
| 510|[0x8000d5e8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006ca4]:fsgnjx.s t6, t5, t4<br> [0x80006ca8]:csrrs a3, fcsr, zero<br> [0x80006cac]:sd t6, 1616(s1)<br>    |
| 511|[0x8000d5f8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006ce4]:fsgnjx.s t6, t5, t4<br> [0x80006ce8]:csrrs a3, fcsr, zero<br> [0x80006cec]:sd t6, 1632(s1)<br>    |
| 512|[0x8000d608]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006d24]:fsgnjx.s t6, t5, t4<br> [0x80006d28]:csrrs a3, fcsr, zero<br> [0x80006d2c]:sd t6, 1648(s1)<br>    |
| 513|[0x8000d618]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006d64]:fsgnjx.s t6, t5, t4<br> [0x80006d68]:csrrs a3, fcsr, zero<br> [0x80006d6c]:sd t6, 1664(s1)<br>    |
| 514|[0x8000d628]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006da4]:fsgnjx.s t6, t5, t4<br> [0x80006da8]:csrrs a3, fcsr, zero<br> [0x80006dac]:sd t6, 1680(s1)<br>    |
| 515|[0x8000d638]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006de4]:fsgnjx.s t6, t5, t4<br> [0x80006de8]:csrrs a3, fcsr, zero<br> [0x80006dec]:sd t6, 1696(s1)<br>    |
| 516|[0x8000d648]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006e24]:fsgnjx.s t6, t5, t4<br> [0x80006e28]:csrrs a3, fcsr, zero<br> [0x80006e2c]:sd t6, 1712(s1)<br>    |
| 517|[0x8000d658]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006e64]:fsgnjx.s t6, t5, t4<br> [0x80006e68]:csrrs a3, fcsr, zero<br> [0x80006e6c]:sd t6, 1728(s1)<br>    |
| 518|[0x8000d668]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006ea4]:fsgnjx.s t6, t5, t4<br> [0x80006ea8]:csrrs a3, fcsr, zero<br> [0x80006eac]:sd t6, 1744(s1)<br>    |
| 519|[0x8000d678]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006ee4]:fsgnjx.s t6, t5, t4<br> [0x80006ee8]:csrrs a3, fcsr, zero<br> [0x80006eec]:sd t6, 1760(s1)<br>    |
| 520|[0x8000d688]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006f24]:fsgnjx.s t6, t5, t4<br> [0x80006f28]:csrrs a3, fcsr, zero<br> [0x80006f2c]:sd t6, 1776(s1)<br>    |
| 521|[0x8000d698]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006f64]:fsgnjx.s t6, t5, t4<br> [0x80006f68]:csrrs a3, fcsr, zero<br> [0x80006f6c]:sd t6, 1792(s1)<br>    |
| 522|[0x8000d6a8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80006fa4]:fsgnjx.s t6, t5, t4<br> [0x80006fa8]:csrrs a3, fcsr, zero<br> [0x80006fac]:sd t6, 1808(s1)<br>    |
| 523|[0x8000d6b8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80006fe4]:fsgnjx.s t6, t5, t4<br> [0x80006fe8]:csrrs a3, fcsr, zero<br> [0x80006fec]:sd t6, 1824(s1)<br>    |
| 524|[0x8000d6c8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007024]:fsgnjx.s t6, t5, t4<br> [0x80007028]:csrrs a3, fcsr, zero<br> [0x8000702c]:sd t6, 1840(s1)<br>    |
| 525|[0x8000d6d8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007064]:fsgnjx.s t6, t5, t4<br> [0x80007068]:csrrs a3, fcsr, zero<br> [0x8000706c]:sd t6, 1856(s1)<br>    |
| 526|[0x8000d6e8]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800070a4]:fsgnjx.s t6, t5, t4<br> [0x800070a8]:csrrs a3, fcsr, zero<br> [0x800070ac]:sd t6, 1872(s1)<br>    |
| 527|[0x8000d6f8]<br>0xFFFFFFFFFFAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800070e4]:fsgnjx.s t6, t5, t4<br> [0x800070e8]:csrrs a3, fcsr, zero<br> [0x800070ec]:sd t6, 1888(s1)<br>    |
| 528|[0x8000d708]<br>0x000000007FAAAAAA<br> |- fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007124]:fsgnjx.s t6, t5, t4<br> [0x80007128]:csrrs a3, fcsr, zero<br> [0x8000712c]:sd t6, 1904(s1)<br>    |
| 529|[0x8000d718]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007164]:fsgnjx.s t6, t5, t4<br> [0x80007168]:csrrs a3, fcsr, zero<br> [0x8000716c]:sd t6, 1920(s1)<br>    |
| 530|[0x8000d728]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800071a4]:fsgnjx.s t6, t5, t4<br> [0x800071a8]:csrrs a3, fcsr, zero<br> [0x800071ac]:sd t6, 1936(s1)<br>    |
| 531|[0x8000d738]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800071e4]:fsgnjx.s t6, t5, t4<br> [0x800071e8]:csrrs a3, fcsr, zero<br> [0x800071ec]:sd t6, 1952(s1)<br>    |
| 532|[0x8000d748]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007224]:fsgnjx.s t6, t5, t4<br> [0x80007228]:csrrs a3, fcsr, zero<br> [0x8000722c]:sd t6, 1968(s1)<br>    |
| 533|[0x8000d758]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007264]:fsgnjx.s t6, t5, t4<br> [0x80007268]:csrrs a3, fcsr, zero<br> [0x8000726c]:sd t6, 1984(s1)<br>    |
| 534|[0x8000d768]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000729c]:fsgnjx.s t6, t5, t4<br> [0x800072a0]:csrrs a3, fcsr, zero<br> [0x800072a4]:sd t6, 2000(s1)<br>    |
| 535|[0x8000d778]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800072d4]:fsgnjx.s t6, t5, t4<br> [0x800072d8]:csrrs a3, fcsr, zero<br> [0x800072dc]:sd t6, 2016(s1)<br>    |
| 536|[0x8000d788]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x8000730c]:fsgnjx.s t6, t5, t4<br> [0x80007310]:csrrs a3, fcsr, zero<br> [0x80007314]:sd t6, 2032(s1)<br>    |
| 537|[0x8000d798]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000734c]:fsgnjx.s t6, t5, t4<br> [0x80007350]:csrrs a3, fcsr, zero<br> [0x80007354]:sd t6, 0(s1)<br>       |
| 538|[0x8000d7a8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007384]:fsgnjx.s t6, t5, t4<br> [0x80007388]:csrrs a3, fcsr, zero<br> [0x8000738c]:sd t6, 16(s1)<br>      |
| 539|[0x8000d7b8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800073bc]:fsgnjx.s t6, t5, t4<br> [0x800073c0]:csrrs a3, fcsr, zero<br> [0x800073c4]:sd t6, 32(s1)<br>      |
| 540|[0x8000d7c8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800073f4]:fsgnjx.s t6, t5, t4<br> [0x800073f8]:csrrs a3, fcsr, zero<br> [0x800073fc]:sd t6, 48(s1)<br>      |
| 541|[0x8000d7d8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000742c]:fsgnjx.s t6, t5, t4<br> [0x80007430]:csrrs a3, fcsr, zero<br> [0x80007434]:sd t6, 64(s1)<br>      |
| 542|[0x8000d7e8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007464]:fsgnjx.s t6, t5, t4<br> [0x80007468]:csrrs a3, fcsr, zero<br> [0x8000746c]:sd t6, 80(s1)<br>      |
| 543|[0x8000d7f8]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000749c]:fsgnjx.s t6, t5, t4<br> [0x800074a0]:csrrs a3, fcsr, zero<br> [0x800074a4]:sd t6, 96(s1)<br>      |
| 544|[0x8000d808]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800074d4]:fsgnjx.s t6, t5, t4<br> [0x800074d8]:csrrs a3, fcsr, zero<br> [0x800074dc]:sd t6, 112(s1)<br>     |
| 545|[0x8000d818]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000750c]:fsgnjx.s t6, t5, t4<br> [0x80007510]:csrrs a3, fcsr, zero<br> [0x80007514]:sd t6, 128(s1)<br>     |
| 546|[0x8000d828]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007544]:fsgnjx.s t6, t5, t4<br> [0x80007548]:csrrs a3, fcsr, zero<br> [0x8000754c]:sd t6, 144(s1)<br>     |
| 547|[0x8000d838]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000757c]:fsgnjx.s t6, t5, t4<br> [0x80007580]:csrrs a3, fcsr, zero<br> [0x80007584]:sd t6, 160(s1)<br>     |
| 548|[0x8000d848]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800075b4]:fsgnjx.s t6, t5, t4<br> [0x800075b8]:csrrs a3, fcsr, zero<br> [0x800075bc]:sd t6, 176(s1)<br>     |
| 549|[0x8000d858]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800075ec]:fsgnjx.s t6, t5, t4<br> [0x800075f0]:csrrs a3, fcsr, zero<br> [0x800075f4]:sd t6, 192(s1)<br>     |
| 550|[0x8000d868]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007624]:fsgnjx.s t6, t5, t4<br> [0x80007628]:csrrs a3, fcsr, zero<br> [0x8000762c]:sd t6, 208(s1)<br>     |
| 551|[0x8000d878]<br>0x000000003F800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000765c]:fsgnjx.s t6, t5, t4<br> [0x80007660]:csrrs a3, fcsr, zero<br> [0x80007664]:sd t6, 224(s1)<br>     |
| 552|[0x8000d888]<br>0xFFFFFFFFBF800000<br> |- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007694]:fsgnjx.s t6, t5, t4<br> [0x80007698]:csrrs a3, fcsr, zero<br> [0x8000769c]:sd t6, 240(s1)<br>     |
| 553|[0x8000d898]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800076cc]:fsgnjx.s t6, t5, t4<br> [0x800076d0]:csrrs a3, fcsr, zero<br> [0x800076d4]:sd t6, 256(s1)<br>     |
| 554|[0x8000d8a8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007704]:fsgnjx.s t6, t5, t4<br> [0x80007708]:csrrs a3, fcsr, zero<br> [0x8000770c]:sd t6, 272(s1)<br>     |
| 555|[0x8000d8b8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000773c]:fsgnjx.s t6, t5, t4<br> [0x80007740]:csrrs a3, fcsr, zero<br> [0x80007744]:sd t6, 288(s1)<br>     |
| 556|[0x8000d8c8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007774]:fsgnjx.s t6, t5, t4<br> [0x80007778]:csrrs a3, fcsr, zero<br> [0x8000777c]:sd t6, 304(s1)<br>     |
| 557|[0x8000d8d8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800077ac]:fsgnjx.s t6, t5, t4<br> [0x800077b0]:csrrs a3, fcsr, zero<br> [0x800077b4]:sd t6, 320(s1)<br>     |
| 558|[0x8000d8e8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800077e4]:fsgnjx.s t6, t5, t4<br> [0x800077e8]:csrrs a3, fcsr, zero<br> [0x800077ec]:sd t6, 336(s1)<br>     |
| 559|[0x8000d8f8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000781c]:fsgnjx.s t6, t5, t4<br> [0x80007820]:csrrs a3, fcsr, zero<br> [0x80007824]:sd t6, 352(s1)<br>     |
| 560|[0x8000d908]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007854]:fsgnjx.s t6, t5, t4<br> [0x80007858]:csrrs a3, fcsr, zero<br> [0x8000785c]:sd t6, 368(s1)<br>     |
| 561|[0x8000d918]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000788c]:fsgnjx.s t6, t5, t4<br> [0x80007890]:csrrs a3, fcsr, zero<br> [0x80007894]:sd t6, 384(s1)<br>     |
| 562|[0x8000d928]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800078c4]:fsgnjx.s t6, t5, t4<br> [0x800078c8]:csrrs a3, fcsr, zero<br> [0x800078cc]:sd t6, 400(s1)<br>     |
| 563|[0x8000d938]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800078fc]:fsgnjx.s t6, t5, t4<br> [0x80007900]:csrrs a3, fcsr, zero<br> [0x80007904]:sd t6, 416(s1)<br>     |
| 564|[0x8000d948]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007934]:fsgnjx.s t6, t5, t4<br> [0x80007938]:csrrs a3, fcsr, zero<br> [0x8000793c]:sd t6, 432(s1)<br>     |
| 565|[0x8000d958]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x8000796c]:fsgnjx.s t6, t5, t4<br> [0x80007970]:csrrs a3, fcsr, zero<br> [0x80007974]:sd t6, 448(s1)<br>     |
| 566|[0x8000d968]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x800079a4]:fsgnjx.s t6, t5, t4<br> [0x800079a8]:csrrs a3, fcsr, zero<br> [0x800079ac]:sd t6, 464(s1)<br>     |
| 567|[0x8000d978]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x800079dc]:fsgnjx.s t6, t5, t4<br> [0x800079e0]:csrrs a3, fcsr, zero<br> [0x800079e4]:sd t6, 480(s1)<br>     |
| 568|[0x8000d988]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007a14]:fsgnjx.s t6, t5, t4<br> [0x80007a18]:csrrs a3, fcsr, zero<br> [0x80007a1c]:sd t6, 496(s1)<br>     |
| 569|[0x8000d998]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007a4c]:fsgnjx.s t6, t5, t4<br> [0x80007a50]:csrrs a3, fcsr, zero<br> [0x80007a54]:sd t6, 512(s1)<br>     |
| 570|[0x8000d9a8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007a84]:fsgnjx.s t6, t5, t4<br> [0x80007a88]:csrrs a3, fcsr, zero<br> [0x80007a8c]:sd t6, 528(s1)<br>     |
| 571|[0x8000d9b8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x400001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007abc]:fsgnjx.s t6, t5, t4<br> [0x80007ac0]:csrrs a3, fcsr, zero<br> [0x80007ac4]:sd t6, 544(s1)<br>     |
| 572|[0x8000d9c8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x455555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007af4]:fsgnjx.s t6, t5, t4<br> [0x80007af8]:csrrs a3, fcsr, zero<br> [0x80007afc]:sd t6, 560(s1)<br>     |
| 573|[0x8000d9d8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0xff and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007b2c]:fsgnjx.s t6, t5, t4<br> [0x80007b30]:csrrs a3, fcsr, zero<br> [0x80007b34]:sd t6, 576(s1)<br>     |
| 574|[0x8000d9e8]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0xff and fm2 == 0x2aaaaa and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007b64]:fsgnjx.s t6, t5, t4<br> [0x80007b68]:csrrs a3, fcsr, zero<br> [0x80007b6c]:sd t6, 592(s1)<br>     |
| 575|[0x8000d9f8]<br>0xFFFFFFFFBF800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007b9c]:fsgnjx.s t6, t5, t4<br> [0x80007ba0]:csrrs a3, fcsr, zero<br> [0x80007ba4]:sd t6, 608(s1)<br>     |
| 576|[0x8000da08]<br>0x000000003F800000<br> |- fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007bd4]:fsgnjx.s t6, t5, t4<br> [0x80007bd8]:csrrs a3, fcsr, zero<br> [0x80007bdc]:sd t6, 624(s1)<br>     |
| 577|[0x8000da18]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007c0c]:fsgnjx.s t6, t5, t4<br> [0x80007c10]:csrrs a3, fcsr, zero<br> [0x80007c14]:sd t6, 640(s1)<br>     |
| 578|[0x8000da28]<br>0xFFFFFFFF80000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007c44]:fsgnjx.s t6, t5, t4<br> [0x80007c48]:csrrs a3, fcsr, zero<br> [0x80007c4c]:sd t6, 656(s1)<br>     |
| 579|[0x8000da38]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat<br>                                                                                                 |[0x80007c7c]:fsgnjx.s t6, t5, t4<br> [0x80007c80]:csrrs a3, fcsr, zero<br> [0x80007c84]:sd t6, 672(s1)<br>     |
| 580|[0x8000da48]<br>0xFFFFFFFF80000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat<br>                                                                                                 |[0x80007cb4]:fsgnjx.s t6, t5, t4<br> [0x80007cb8]:csrrs a3, fcsr, zero<br> [0x80007cbc]:sd t6, 688(s1)<br>     |
