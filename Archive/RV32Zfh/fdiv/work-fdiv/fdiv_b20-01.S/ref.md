
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000600')]      |
| SIG_REGION                | [('0x80002310', '0x80002450', '80 words')]      |
| COV_LABELS                | fdiv_b20      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32H/work-fdiv/fdiv_b20-01.S/ref.S    |
| Total Number of coverpoints| 139     |
| Total Coverpoints Hit     | 139      |
| Total Signature Updates   | 78      |
| STAT1                     | 39      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 39     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : f30', 'rs2 : f31', 'rd : f31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x24a and fs2 == 1 and fe2 == 0x0a and fm2 == 0x24a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fdiv.h ft11, ft10, ft11, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:fsw ft11, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002318]:0x00000005




Last Coverpoint : ['rs1 : f29', 'rs2 : f29', 'rd : f29', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000144]:fdiv.h ft9, ft9, ft9, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:fsw ft9, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80002320]:0x00000000




Last Coverpoint : ['rs1 : f28', 'rs2 : f30', 'rd : f28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x3ae and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000164]:fdiv.h ft8, ft8, ft10, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:fsw ft8, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002328]:0x00000001




Last Coverpoint : ['rs1 : f31', 'rs2 : f28', 'rd : f30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f2 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.h ft10, ft11, ft8, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:fsw ft10, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80002330]:0x00000001




Last Coverpoint : ['rs1 : f26', 'rs2 : f26', 'rd : f27', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001a4]:fdiv.h fs11, fs10, fs10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:fsw fs11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002338]:0x00000000




Last Coverpoint : ['rs1 : f27', 'rs2 : f25', 'rd : f26', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x150 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.h fs10, fs11, fs9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:fsw fs10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80002340]:0x00000001




Last Coverpoint : ['rs1 : f24', 'rs2 : f27', 'rd : f25', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.h fs9, fs8, fs11, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:fsw fs9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002348]:0x00000008




Last Coverpoint : ['rs1 : f25', 'rs2 : f23', 'rd : f24', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x30e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.h fs8, fs9, fs7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:fsw fs8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80002350]:0x00000001




Last Coverpoint : ['rs1 : f22', 'rs2 : f24', 'rd : f23', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x234 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.h fs7, fs6, fs8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002358]:0x00000008




Last Coverpoint : ['rs1 : f23', 'rs2 : f21', 'rd : f22', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1e7 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.h fs6, fs7, fs5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:fsw fs6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80002360]:0x00000001




Last Coverpoint : ['rs1 : f20', 'rs2 : f22', 'rd : f21', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x188 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.h fs5, fs4, fs6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:fsw fs5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002368]:0x00000008




Last Coverpoint : ['rs1 : f21', 'rs2 : f19', 'rd : f20', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x14e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.h fs4, fs5, fs3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:fsw fs4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80002370]:0x00000008




Last Coverpoint : ['rs1 : f18', 'rs2 : f20', 'rd : f19', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x2e7 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.h fs3, fs2, fs4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:fsw fs3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002378]:0x00000001




Last Coverpoint : ['rs1 : f19', 'rs2 : f17', 'rd : f18', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x13c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.h fs2, fs3, fa7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:fsw fs2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80002380]:0x00000001




Last Coverpoint : ['rs1 : f16', 'rs2 : f18', 'rd : f17', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b7 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.h fa7, fa6, fs2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:fsw fa7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002388]:0x00000001




Last Coverpoint : ['rs1 : f17', 'rs2 : f15', 'rd : f16', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ec and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.h fa6, fa7, fa5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:fsw fa6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80002390]:0x00000001




Last Coverpoint : ['rs1 : f14', 'rs2 : f16', 'rd : f15', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1db and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.h fa5, fa4, fa6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:fsw fa5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002398]:0x00000008




Last Coverpoint : ['rs1 : f15', 'rs2 : f13', 'rd : f14', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x381 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.h fa4, fa5, fa3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800023a0]:0x00000001




Last Coverpoint : ['rs1 : f12', 'rs2 : f14', 'rd : f13', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ef and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.h fa3, fa2, fa4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:fsw fa3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800023a8]:0x00000008




Last Coverpoint : ['rs1 : f13', 'rs2 : f11', 'rd : f12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c8 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x3c0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.h fa2, fa3, fa1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:fsw fa2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800023b0]:0x00000000




Last Coverpoint : ['rs1 : f10', 'rs2 : f12', 'rd : f11', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ea and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.h fa1, fa0, fa2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:fsw fa1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800023b8]:0x00000001




Last Coverpoint : ['rs1 : f11', 'rs2 : f9', 'rd : f10', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x09f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.h fa0, fa1, fs1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:fsw fa0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800023c0]:0x00000001




Last Coverpoint : ['rs1 : f8', 'rs2 : f10', 'rd : f9', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x12c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fdiv.h fs1, fs0, fa0, dyn
	-[0x800003e8]:csrrs tp, fcsr, zero
	-[0x800003ec]:fsw fs1, 176(ra)
Current Store : [0x800003f0] : sw tp, 180(ra) -- Store: [0x800023c8]:0x00000008




Last Coverpoint : ['rs1 : f9', 'rs2 : f7', 'rd : f8', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x164 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000404]:fdiv.h fs0, fs1, ft7, dyn
	-[0x80000408]:csrrs tp, fcsr, zero
	-[0x8000040c]:fsw fs0, 184(ra)
Current Store : [0x80000410] : sw tp, 188(ra) -- Store: [0x800023d0]:0x00000001




Last Coverpoint : ['rs1 : f6', 'rs2 : f8', 'rd : f7', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x342 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000424]:fdiv.h ft7, ft6, fs0, dyn
	-[0x80000428]:csrrs tp, fcsr, zero
	-[0x8000042c]:fsw ft7, 192(ra)
Current Store : [0x80000430] : sw tp, 196(ra) -- Store: [0x800023d8]:0x00000008




Last Coverpoint : ['rs1 : f7', 'rs2 : f5', 'rd : f6', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x24d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fdiv.h ft6, ft7, ft5, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:fsw ft6, 200(ra)
Current Store : [0x80000450] : sw tp, 204(ra) -- Store: [0x800023e0]:0x00000008




Last Coverpoint : ['rs1 : f4', 'rs2 : f6', 'rd : f5', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x261 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000464]:fdiv.h ft5, ft4, ft6, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw ft5, 208(ra)
Current Store : [0x80000470] : sw tp, 212(ra) -- Store: [0x800023e8]:0x00000005




Last Coverpoint : ['rs1 : f5', 'rs2 : f3', 'rd : f4', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x35b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000484]:fdiv.h ft4, ft5, ft3, dyn
	-[0x80000488]:csrrs tp, fcsr, zero
	-[0x8000048c]:fsw ft4, 216(ra)
Current Store : [0x80000490] : sw tp, 220(ra) -- Store: [0x800023f0]:0x00000008




Last Coverpoint : ['rs1 : f2', 'rs2 : f4', 'rd : f3', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x062 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fdiv.h ft3, ft2, ft4, dyn
	-[0x800004a8]:csrrs tp, fcsr, zero
	-[0x800004ac]:fsw ft3, 224(ra)
Current Store : [0x800004b0] : sw tp, 228(ra) -- Store: [0x800023f8]:0x00000008




Last Coverpoint : ['rs1 : f3', 'rs2 : f1', 'rd : f2', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fdiv.h ft2, ft3, ft1, dyn
	-[0x800004c8]:csrrs tp, fcsr, zero
	-[0x800004cc]:fsw ft2, 232(ra)
Current Store : [0x800004d0] : sw tp, 236(ra) -- Store: [0x80002400]:0x00000008




Last Coverpoint : ['rs1 : f0', 'rs2 : f2', 'rd : f1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x17f and fs2 == 1 and fe2 == 0x16 and fm2 == 0x266 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004e4]:fdiv.h ft1, ft0, ft2, dyn
	-[0x800004e8]:csrrs tp, fcsr, zero
	-[0x800004ec]:fsw ft1, 240(ra)
Current Store : [0x800004f0] : sw tp, 244(ra) -- Store: [0x80002408]:0x00000001




Last Coverpoint : ['rs1 : f1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000504]:fdiv.h ft11, ft1, ft10, dyn
	-[0x80000508]:csrrs tp, fcsr, zero
	-[0x8000050c]:fsw ft11, 248(ra)
Current Store : [0x80000510] : sw tp, 252(ra) -- Store: [0x80002410]:0x00000001




Last Coverpoint : ['rs2 : f0', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000524]:fdiv.h ft11, ft10, ft0, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:fsw ft11, 256(ra)
Current Store : [0x80000530] : sw tp, 260(ra) -- Store: [0x80002418]:0x00000008




Last Coverpoint : ['rd : f0', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x346 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000544]:fdiv.h ft0, ft11, ft10, dyn
	-[0x80000548]:csrrs tp, fcsr, zero
	-[0x8000054c]:fsw ft0, 264(ra)
Current Store : [0x80000550] : sw tp, 268(ra) -- Store: [0x80002420]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000564]:fdiv.h ft11, ft10, ft9, dyn
	-[0x80000568]:csrrs tp, fcsr, zero
	-[0x8000056c]:fsw ft11, 272(ra)
Current Store : [0x80000570] : sw tp, 276(ra) -- Store: [0x80002428]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0de and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000584]:fdiv.h ft11, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 280(ra)
Current Store : [0x80000590] : sw tp, 284(ra) -- Store: [0x80002430]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005a4]:fdiv.h ft11, ft10, ft9, dyn
	-[0x800005a8]:csrrs tp, fcsr, zero
	-[0x800005ac]:fsw ft11, 288(ra)
Current Store : [0x800005b0] : sw tp, 292(ra) -- Store: [0x80002438]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fdiv.h ft11, ft10, ft9, dyn
	-[0x800005c8]:csrrs tp, fcsr, zero
	-[0x800005cc]:fsw ft11, 296(ra)
Current Store : [0x800005d0] : sw tp, 300(ra) -- Store: [0x80002440]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x277 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fdiv.h ft11, ft10, ft9, dyn
	-[0x800005e8]:csrrs tp, fcsr, zero
	-[0x800005ec]:fsw ft11, 304(ra)
Current Store : [0x800005f0] : sw tp, 308(ra) -- Store: [0x80002448]:0x00000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                           |                                                         code                                                         |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0xFBB6FAB7<br> |- mnemonic : fdiv.h<br> - rs1 : f30<br> - rs2 : f31<br> - rd : f31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x24a and fs2 == 1 and fe2 == 0x0a and fm2 == 0x24a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fdiv.h ft11, ft10, ft11, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:fsw ft11, 0(ra)<br>  |
|   2|[0x8000231c]<br>0xEEDBEADF<br> |- rs1 : f29<br> - rs2 : f29<br> - rd : f29<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                           |[0x80000144]:fdiv.h ft9, ft9, ft9, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:fsw ft9, 8(ra)<br>      |
|   3|[0x80002324]<br>0xDDB7D5BF<br> |- rs1 : f28<br> - rs2 : f30<br> - rd : f28<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x3ae and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x80000164]:fdiv.h ft8, ft8, ft10, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:fsw ft8, 16(ra)<br>    |
|   4|[0x8000232c]<br>0xF76DF56F<br> |- rs1 : f31<br> - rs2 : f28<br> - rd : f30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f2 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>  |[0x80000184]:fdiv.h ft10, ft11, ft8, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:fsw ft10, 24(ra)<br>  |
|   5|[0x80002334]<br>0xBB6FAB7F<br> |- rs1 : f26<br> - rs2 : f26<br> - rd : f27<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                           |[0x800001a4]:fdiv.h fs11, fs10, fs10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:fsw fs11, 32(ra)<br> |
|   6|[0x8000233c]<br>0x76DF56FF<br> |- rs1 : f27<br> - rs2 : f25<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x150 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fdiv.h fs10, fs11, fs9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:fsw fs10, 40(ra)<br>  |
|   7|[0x80002344]<br>0xEDBEADFE<br> |- rs1 : f24<br> - rs2 : f27<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001e4]:fdiv.h fs9, fs8, fs11, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:fsw fs9, 48(ra)<br>    |
|   8|[0x8000234c]<br>0xDB7D5BFD<br> |- rs1 : f25<br> - rs2 : f23<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x30e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000204]:fdiv.h fs8, fs9, fs7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:fsw fs8, 56(ra)<br>     |
|   9|[0x80002354]<br>0xB6FAB7FB<br> |- rs1 : f22<br> - rs2 : f24<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x234 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000224]:fdiv.h fs7, fs6, fs8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:fsw fs7, 64(ra)<br>     |
|  10|[0x8000235c]<br>0x6DF56FF7<br> |- rs1 : f23<br> - rs2 : f21<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1e7 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000244]:fdiv.h fs6, fs7, fs5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:fsw fs6, 72(ra)<br>     |
|  11|[0x80002364]<br>0xDBEADFEE<br> |- rs1 : f20<br> - rs2 : f22<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x188 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fdiv.h fs5, fs4, fs6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:fsw fs5, 80(ra)<br>     |
|  12|[0x8000236c]<br>0xB7D5BFDD<br> |- rs1 : f21<br> - rs2 : f19<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x14e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000284]:fdiv.h fs4, fs5, fs3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:fsw fs4, 88(ra)<br>     |
|  13|[0x80002374]<br>0x6FAB7FBB<br> |- rs1 : f18<br> - rs2 : f20<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x2e7 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002a4]:fdiv.h fs3, fs2, fs4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:fsw fs3, 96(ra)<br>     |
|  14|[0x8000237c]<br>0xDF56FF76<br> |- rs1 : f19<br> - rs2 : f17<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x13c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002c4]:fdiv.h fs2, fs3, fa7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:fsw fs2, 104(ra)<br>    |
|  15|[0x80002384]<br>0xBEADFEED<br> |- rs1 : f16<br> - rs2 : f18<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b7 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002e4]:fdiv.h fa7, fa6, fs2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:fsw fa7, 112(ra)<br>    |
|  16|[0x8000238c]<br>0x7D5BFDDB<br> |- rs1 : f17<br> - rs2 : f15<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ec and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fdiv.h fa6, fa7, fa5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:fsw fa6, 120(ra)<br>    |
|  17|[0x80002394]<br>0xFAB7FBB6<br> |- rs1 : f14<br> - rs2 : f16<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1db and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000324]:fdiv.h fa5, fa4, fa6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:fsw fa5, 128(ra)<br>    |
|  18|[0x8000239c]<br>0xF56FF76D<br> |- rs1 : f15<br> - rs2 : f13<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x381 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000344]:fdiv.h fa4, fa5, fa3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:fsw fa4, 136(ra)<br>    |
|  19|[0x800023a4]<br>0xEADFEEDB<br> |- rs1 : f12<br> - rs2 : f14<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ef and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000364]:fdiv.h fa3, fa2, fa4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:fsw fa3, 144(ra)<br>    |
|  20|[0x800023ac]<br>0xD5BFDDB7<br> |- rs1 : f13<br> - rs2 : f11<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c8 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x3c0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000384]:fdiv.h fa2, fa3, fa1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:fsw fa2, 152(ra)<br>    |
|  21|[0x800023b4]<br>0xAB7FBB6F<br> |- rs1 : f10<br> - rs2 : f12<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ea and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fdiv.h fa1, fa0, fa2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:fsw fa1, 160(ra)<br>    |
|  22|[0x800023bc]<br>0x00002000<br> |- rs1 : f11<br> - rs2 : f9<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x09f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                 |[0x800003c4]:fdiv.h fa0, fa1, fs1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:fsw fa0, 168(ra)<br>    |
|  23|[0x800023c4]<br>0xADFEEDBE<br> |- rs1 : f8<br> - rs2 : f10<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x12c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800003e4]:fdiv.h fs1, fs0, fa0, dyn<br> [0x800003e8]:csrrs tp, fcsr, zero<br> [0x800003ec]:fsw fs1, 176(ra)<br>    |
|  24|[0x800023cc]<br>0x5BFDDB7D<br> |- rs1 : f9<br> - rs2 : f7<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x164 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000404]:fdiv.h fs0, fs1, ft7, dyn<br> [0x80000408]:csrrs tp, fcsr, zero<br> [0x8000040c]:fsw fs0, 184(ra)<br>    |
|  25|[0x800023d4]<br>0xB7FBB6FA<br> |- rs1 : f6<br> - rs2 : f8<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x342 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000424]:fdiv.h ft7, ft6, fs0, dyn<br> [0x80000428]:csrrs tp, fcsr, zero<br> [0x8000042c]:fsw ft7, 192(ra)<br>    |
|  26|[0x800023dc]<br>0x80002000<br> |- rs1 : f7<br> - rs2 : f5<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x24d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000444]:fdiv.h ft6, ft7, ft5, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:fsw ft6, 200(ra)<br>    |
|  27|[0x800023e4]<br>0x800000F8<br> |- rs1 : f4<br> - rs2 : f6<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x261 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000464]:fdiv.h ft5, ft4, ft6, dyn<br> [0x80000468]:csrrs tp, fcsr, zero<br> [0x8000046c]:fsw ft5, 208(ra)<br>    |
|  28|[0x800023ec]<br>0x00000008<br> |- rs1 : f5<br> - rs2 : f3<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x35b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000484]:fdiv.h ft4, ft5, ft3, dyn<br> [0x80000488]:csrrs tp, fcsr, zero<br> [0x8000048c]:fsw ft4, 216(ra)<br>    |
|  29|[0x800023f4]<br>0x80002010<br> |- rs1 : f2<br> - rs2 : f4<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x062 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004a4]:fdiv.h ft3, ft2, ft4, dyn<br> [0x800004a8]:csrrs tp, fcsr, zero<br> [0x800004ac]:fsw ft3, 224(ra)<br>    |
|  30|[0x800023fc]<br>0x00000000<br> |- rs1 : f3<br> - rs2 : f1<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004c4]:fdiv.h ft2, ft3, ft1, dyn<br> [0x800004c8]:csrrs tp, fcsr, zero<br> [0x800004cc]:fsw ft2, 232(ra)<br>    |
|  31|[0x80002404]<br>0x80002314<br> |- rs1 : f0<br> - rs2 : f2<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x17f and fs2 == 1 and fe2 == 0x16 and fm2 == 0x266 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004e4]:fdiv.h ft1, ft0, ft2, dyn<br> [0x800004e8]:csrrs tp, fcsr, zero<br> [0x800004ec]:fsw ft1, 240(ra)<br>    |
|  32|[0x8000240c]<br>0xFBB6FAB7<br> |- rs1 : f1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000504]:fdiv.h ft11, ft1, ft10, dyn<br> [0x80000508]:csrrs tp, fcsr, zero<br> [0x8000050c]:fsw ft11, 248(ra)<br> |
|  33|[0x80002414]<br>0xFBB6FAB7<br> |- rs2 : f0<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000524]:fdiv.h ft11, ft10, ft0, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:fsw ft11, 256(ra)<br> |
|  34|[0x8000241c]<br>0x00000000<br> |- rd : f0<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x346 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                 |[0x80000544]:fdiv.h ft0, ft11, ft10, dyn<br> [0x80000548]:csrrs tp, fcsr, zero<br> [0x8000054c]:fsw ft0, 264(ra)<br>  |
|  35|[0x80002424]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000564]:fdiv.h ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs tp, fcsr, zero<br> [0x8000056c]:fsw ft11, 272(ra)<br> |
|  36|[0x8000242c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0de and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000584]:fdiv.h ft11, ft10, ft9, dyn<br> [0x80000588]:csrrs tp, fcsr, zero<br> [0x8000058c]:fsw ft11, 280(ra)<br> |
|  37|[0x80002434]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005a4]:fdiv.h ft11, ft10, ft9, dyn<br> [0x800005a8]:csrrs tp, fcsr, zero<br> [0x800005ac]:fsw ft11, 288(ra)<br> |
|  38|[0x8000243c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005c4]:fdiv.h ft11, ft10, ft9, dyn<br> [0x800005c8]:csrrs tp, fcsr, zero<br> [0x800005cc]:fsw ft11, 296(ra)<br> |
|  39|[0x80002444]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x277 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005e4]:fdiv.h ft11, ft10, ft9, dyn<br> [0x800005e8]:csrrs tp, fcsr, zero<br> [0x800005ec]:fsw ft11, 304(ra)<br> |
