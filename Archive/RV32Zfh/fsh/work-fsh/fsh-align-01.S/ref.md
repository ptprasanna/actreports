
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005b0')]      |
| SIG_REGION                | [('0x80002310', '0x80002420', '68 words')]      |
| COV_LABELS                | fsh-align      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32H/work-fsh/fsh-align-01.S/ref.S    |
| Total Number of coverpoints| 71     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|                                                   coverpoints                                                    |                code                 |
|---:|---------|------------------------------------------------------------------------------------------------------------------|-------------------------------------|
|   1|         |- mnemonic : fsh<br> - rs1 : x31<br> - rs2 : f31<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val < 0<br> |[0x80000124]:fsh ft11, 4092(t6)<br>  |
|   2|         |- rs1 : x30<br> - rs2 : f30<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                        |[0x80000148]:fsh ft10, 4093(t5)<br>  |
|   3|         |- rs1 : x29<br> - rs2 : f29<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                        |[0x8000016c]:fsh ft9, 4090(t4)<br>   |
|   4|         |- rs1 : x28<br> - rs2 : f28<br> - ea_align == 0 and (imm_val % 4) == 3<br> - imm_val > 0<br>                      |[0x80000190]:fsh ft8, 2047(t3)<br>   |
|   5|         |- rs1 : x27<br> - rs2 : f27<br> - imm_val == 0<br>                                                                |[0x800001b4]:fsh fs11, 0(s11)<br>    |
|   6|         |- rs1 : x26<br> - rs2 : f26<br>                                                                                   |[0x800001d8]:fsh fs10, 2048(s10)<br> |
|   7|         |- rs1 : x25<br> - rs2 : f25<br>                                                                                   |[0x800001fc]:fsh fs9, 2048(s9)<br>   |
|   8|         |- rs1 : x24<br> - rs2 : f24<br>                                                                                   |[0x80000220]:fsh fs8, 2048(s8)<br>   |
|   9|         |- rs1 : x23<br> - rs2 : f23<br>                                                                                   |[0x80000244]:fsh fs7, 2048(s7)<br>   |
|  10|         |- rs1 : x22<br> - rs2 : f22<br>                                                                                   |[0x80000268]:fsh fs6, 2048(s6)<br>   |
|  11|         |- rs1 : x21<br> - rs2 : f21<br>                                                                                   |[0x8000028c]:fsh fs5, 2048(s5)<br>   |
|  12|         |- rs1 : x20<br> - rs2 : f20<br>                                                                                   |[0x800002b0]:fsh fs4, 2048(s4)<br>   |
|  13|         |- rs1 : x19<br> - rs2 : f19<br>                                                                                   |[0x800002d4]:fsh fs3, 2048(s3)<br>   |
|  14|         |- rs1 : x18<br> - rs2 : f18<br>                                                                                   |[0x800002f8]:fsh fs2, 2048(s2)<br>   |
|  15|         |- rs1 : x17<br> - rs2 : f17<br>                                                                                   |[0x8000031c]:fsh fa7, 2048(a7)<br>   |
|  16|         |- rs1 : x16<br> - rs2 : f16<br>                                                                                   |[0x80000340]:fsh fa6, 2048(a6)<br>   |
|  17|         |- rs1 : x15<br> - rs2 : f15<br>                                                                                   |[0x80000364]:fsh fa5, 2048(a5)<br>   |
|  18|         |- rs1 : x14<br> - rs2 : f14<br>                                                                                   |[0x80000388]:fsh fa4, 2048(a4)<br>   |
|  19|         |- rs1 : x13<br> - rs2 : f13<br>                                                                                   |[0x800003ac]:fsh fa3, 2048(a3)<br>   |
|  20|         |- rs1 : x12<br> - rs2 : f12<br>                                                                                   |[0x800003d0]:fsh fa2, 2048(a2)<br>   |
|  21|         |- rs1 : x11<br> - rs2 : f11<br>                                                                                   |[0x800003f4]:fsh fa1, 2048(a1)<br>   |
|  22|         |- rs1 : x10<br> - rs2 : f10<br>                                                                                   |[0x80000418]:fsh fa0, 2048(a0)<br>   |
|  23|         |- rs1 : x9<br> - rs2 : f9<br>                                                                                     |[0x8000043c]:fsh fs1, 2048(s1)<br>   |
|  24|         |- rs1 : x8<br> - rs2 : f8<br>                                                                                     |[0x80000460]:fsh fs0, 2048(fp)<br>   |
|  25|         |- rs1 : x7<br> - rs2 : f7<br>                                                                                     |[0x8000048c]:fsh ft7, 2048(t2)<br>   |
|  26|         |- rs1 : x6<br> - rs2 : f6<br>                                                                                     |[0x800004b0]:fsh ft6, 2048(t1)<br>   |
|  27|         |- rs1 : x5<br> - rs2 : f5<br>                                                                                     |[0x800004d4]:fsh ft5, 2048(t0)<br>   |
|  28|         |- rs1 : x4<br> - rs2 : f4<br>                                                                                     |[0x80000500]:fsh ft4, 2048(tp)<br>   |
|  29|         |- rs1 : x3<br> - rs2 : f3<br>                                                                                     |[0x80000524]:fsh ft3, 2048(gp)<br>   |
|  30|         |- rs1 : x2<br> - rs2 : f2<br>                                                                                     |[0x80000548]:fsh ft2, 2048(sp)<br>   |
|  31|         |- rs1 : x1<br> - rs2 : f1<br>                                                                                     |[0x8000056c]:fsh ft1, 2048(ra)<br>   |
|  32|         |- rs2 : f0<br>                                                                                                    |[0x80000590]:fsh ft0, 2048(t6)<br>   |
