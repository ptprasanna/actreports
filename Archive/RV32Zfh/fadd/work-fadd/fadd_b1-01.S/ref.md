
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80006fb0')]      |
| SIG_REGION                | [('0x80009410', '0x8000a630', '1160 words')]      |
| COV_LABELS                | fadd_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32H/work-fadd/fadd_b1-01.S/ref.S    |
| Total Number of coverpoints| 678     |
| Total Coverpoints Hit     | 678      |
| Total Signature Updates   | 1156      |
| STAT1                     | 577      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 578     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80006f5c]:fadd.h ft11, ft10, ft9, dyn
      [0x80006f60]:csrrs tp, fcsr, zero
      [0x80006f64]:fsw ft11, 512(ra)
 -- Signature Addresses:
      Address: 0x8000a614 Data: 0xFBB6FAB7
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.h
      - rs1 : f30
      - rs2 : f29
      - rd : f31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.h', 'rs1 : f31', 'rs2 : f31', 'rd : f31', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fadd.h ft11, ft11, ft11, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:fsw ft11, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80009418]:0x00000000




Last Coverpoint : ['rs1 : f29', 'rs2 : f28', 'rd : f30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fadd.h ft10, ft9, ft8, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:fsw ft10, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80009420]:0x00000000




Last Coverpoint : ['rs1 : f30', 'rs2 : f29', 'rd : f29', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000164]:fadd.h ft9, ft10, ft9, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:fsw ft9, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80009428]:0x00000000




Last Coverpoint : ['rs1 : f28', 'rs2 : f30', 'rd : f28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fadd.h ft8, ft8, ft10, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:fsw ft8, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80009430]:0x00000000




Last Coverpoint : ['rs1 : f26', 'rs2 : f26', 'rd : f27', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800001a4]:fadd.h fs11, fs10, fs10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:fsw fs11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80009438]:0x00000000




Last Coverpoint : ['rs1 : f27', 'rs2 : f25', 'rd : f26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fadd.h fs10, fs11, fs9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:fsw fs10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80009440]:0x00000000




Last Coverpoint : ['rs1 : f24', 'rs2 : f27', 'rd : f25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fadd.h fs9, fs8, fs11, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:fsw fs9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80009448]:0x00000000




Last Coverpoint : ['rs1 : f25', 'rs2 : f23', 'rd : f24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fadd.h fs8, fs9, fs7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:fsw fs8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80009450]:0x00000000




Last Coverpoint : ['rs1 : f22', 'rs2 : f24', 'rd : f23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fadd.h fs7, fs6, fs8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80009458]:0x00000000




Last Coverpoint : ['rs1 : f23', 'rs2 : f21', 'rd : f22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fadd.h fs6, fs7, fs5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:fsw fs6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80009460]:0x00000000




Last Coverpoint : ['rs1 : f20', 'rs2 : f22', 'rd : f21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fadd.h fs5, fs4, fs6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:fsw fs5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80009468]:0x00000000




Last Coverpoint : ['rs1 : f21', 'rs2 : f19', 'rd : f20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fadd.h fs4, fs5, fs3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:fsw fs4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80009470]:0x00000000




Last Coverpoint : ['rs1 : f18', 'rs2 : f20', 'rd : f19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fadd.h fs3, fs2, fs4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:fsw fs3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80009478]:0x00000000




Last Coverpoint : ['rs1 : f19', 'rs2 : f17', 'rd : f18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fadd.h fs2, fs3, fa7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:fsw fs2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80009480]:0x00000000




Last Coverpoint : ['rs1 : f16', 'rs2 : f18', 'rd : f17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fadd.h fa7, fa6, fs2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:fsw fa7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80009488]:0x00000000




Last Coverpoint : ['rs1 : f17', 'rs2 : f15', 'rd : f16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fadd.h fa6, fa7, fa5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:fsw fa6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80009490]:0x00000000




Last Coverpoint : ['rs1 : f14', 'rs2 : f16', 'rd : f15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fadd.h fa5, fa4, fa6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:fsw fa5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80009498]:0x00000000




Last Coverpoint : ['rs1 : f15', 'rs2 : f13', 'rd : f14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fadd.h fa4, fa5, fa3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800094a0]:0x00000000




Last Coverpoint : ['rs1 : f12', 'rs2 : f14', 'rd : f13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fadd.h fa3, fa2, fa4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:fsw fa3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800094a8]:0x00000000




Last Coverpoint : ['rs1 : f13', 'rs2 : f11', 'rd : f12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fadd.h fa2, fa3, fa1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:fsw fa2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800094b0]:0x00000000




Last Coverpoint : ['rs1 : f10', 'rs2 : f12', 'rd : f11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fadd.h fa1, fa0, fa2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:fsw fa1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800094b8]:0x00000010




Last Coverpoint : ['rs1 : f11', 'rs2 : f9', 'rd : f10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:fadd.h fa0, fa1, fs1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:fsw fa0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800094c0]:0x00000010




Last Coverpoint : ['rs1 : f8', 'rs2 : f10', 'rd : f9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fadd.h fs1, fs0, fa0, dyn
	-[0x800003e8]:csrrs tp, fcsr, zero
	-[0x800003ec]:fsw fs1, 176(ra)
Current Store : [0x800003f0] : sw tp, 180(ra) -- Store: [0x800094c8]:0x00000000




Last Coverpoint : ['rs1 : f9', 'rs2 : f7', 'rd : f8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000404]:fadd.h fs0, fs1, ft7, dyn
	-[0x80000408]:csrrs tp, fcsr, zero
	-[0x8000040c]:fsw fs0, 184(ra)
Current Store : [0x80000410] : sw tp, 188(ra) -- Store: [0x800094d0]:0x00000000




Last Coverpoint : ['rs1 : f6', 'rs2 : f8', 'rd : f7', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000424]:fadd.h ft7, ft6, fs0, dyn
	-[0x80000428]:csrrs tp, fcsr, zero
	-[0x8000042c]:fsw ft7, 192(ra)
Current Store : [0x80000430] : sw tp, 196(ra) -- Store: [0x800094d8]:0x00000000




Last Coverpoint : ['rs1 : f7', 'rs2 : f5', 'rd : f6', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fadd.h ft6, ft7, ft5, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:fsw ft6, 200(ra)
Current Store : [0x80000450] : sw tp, 204(ra) -- Store: [0x800094e0]:0x00000000




Last Coverpoint : ['rs1 : f4', 'rs2 : f6', 'rd : f5', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000464]:fadd.h ft5, ft4, ft6, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw ft5, 208(ra)
Current Store : [0x80000470] : sw tp, 212(ra) -- Store: [0x800094e8]:0x00000000




Last Coverpoint : ['rs1 : f5', 'rs2 : f3', 'rd : f4', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000484]:fadd.h ft4, ft5, ft3, dyn
	-[0x80000488]:csrrs tp, fcsr, zero
	-[0x8000048c]:fsw ft4, 216(ra)
Current Store : [0x80000490] : sw tp, 220(ra) -- Store: [0x800094f0]:0x00000000




Last Coverpoint : ['rs1 : f2', 'rs2 : f4', 'rd : f3', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fadd.h ft3, ft2, ft4, dyn
	-[0x800004a8]:csrrs tp, fcsr, zero
	-[0x800004ac]:fsw ft3, 224(ra)
Current Store : [0x800004b0] : sw tp, 228(ra) -- Store: [0x800094f8]:0x00000000




Last Coverpoint : ['rs1 : f3', 'rs2 : f1', 'rd : f2', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fadd.h ft2, ft3, ft1, dyn
	-[0x800004c8]:csrrs tp, fcsr, zero
	-[0x800004cc]:fsw ft2, 232(ra)
Current Store : [0x800004d0] : sw tp, 236(ra) -- Store: [0x80009500]:0x00000000




Last Coverpoint : ['rs1 : f0', 'rs2 : f2', 'rd : f1', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004e4]:fadd.h ft1, ft0, ft2, dyn
	-[0x800004e8]:csrrs tp, fcsr, zero
	-[0x800004ec]:fsw ft1, 240(ra)
Current Store : [0x800004f0] : sw tp, 244(ra) -- Store: [0x80009508]:0x00000000




Last Coverpoint : ['rs1 : f1', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000504]:fadd.h ft11, ft1, ft10, dyn
	-[0x80000508]:csrrs tp, fcsr, zero
	-[0x8000050c]:fsw ft11, 248(ra)
Current Store : [0x80000510] : sw tp, 252(ra) -- Store: [0x80009510]:0x00000000




Last Coverpoint : ['rs2 : f0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000524]:fadd.h ft11, ft10, ft0, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:fsw ft11, 256(ra)
Current Store : [0x80000530] : sw tp, 260(ra) -- Store: [0x80009518]:0x00000000




Last Coverpoint : ['rd : f0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000544]:fadd.h ft0, ft11, ft10, dyn
	-[0x80000548]:csrrs tp, fcsr, zero
	-[0x8000054c]:fsw ft0, 264(ra)
Current Store : [0x80000550] : sw tp, 268(ra) -- Store: [0x80009520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000564]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000568]:csrrs tp, fcsr, zero
	-[0x8000056c]:fsw ft11, 272(ra)
Current Store : [0x80000570] : sw tp, 276(ra) -- Store: [0x80009528]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000584]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 280(ra)
Current Store : [0x80000590] : sw tp, 284(ra) -- Store: [0x80009530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800005a8]:csrrs tp, fcsr, zero
	-[0x800005ac]:fsw ft11, 288(ra)
Current Store : [0x800005b0] : sw tp, 292(ra) -- Store: [0x80009538]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800005c8]:csrrs tp, fcsr, zero
	-[0x800005cc]:fsw ft11, 296(ra)
Current Store : [0x800005d0] : sw tp, 300(ra) -- Store: [0x80009540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800005e8]:csrrs tp, fcsr, zero
	-[0x800005ec]:fsw ft11, 304(ra)
Current Store : [0x800005f0] : sw tp, 308(ra) -- Store: [0x80009548]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000604]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:fsw ft11, 312(ra)
Current Store : [0x80000610] : sw tp, 316(ra) -- Store: [0x80009550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000624]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000628]:csrrs tp, fcsr, zero
	-[0x8000062c]:fsw ft11, 320(ra)
Current Store : [0x80000630] : sw tp, 324(ra) -- Store: [0x80009558]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000644]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000648]:csrrs tp, fcsr, zero
	-[0x8000064c]:fsw ft11, 328(ra)
Current Store : [0x80000650] : sw tp, 332(ra) -- Store: [0x80009560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000664]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000668]:csrrs tp, fcsr, zero
	-[0x8000066c]:fsw ft11, 336(ra)
Current Store : [0x80000670] : sw tp, 340(ra) -- Store: [0x80009568]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000684]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000688]:csrrs tp, fcsr, zero
	-[0x8000068c]:fsw ft11, 344(ra)
Current Store : [0x80000690] : sw tp, 348(ra) -- Store: [0x80009570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft11, 352(ra)
Current Store : [0x800006b0] : sw tp, 356(ra) -- Store: [0x80009578]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800006c8]:csrrs tp, fcsr, zero
	-[0x800006cc]:fsw ft11, 360(ra)
Current Store : [0x800006d0] : sw tp, 364(ra) -- Store: [0x80009580]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800006e8]:csrrs tp, fcsr, zero
	-[0x800006ec]:fsw ft11, 368(ra)
Current Store : [0x800006f0] : sw tp, 372(ra) -- Store: [0x80009588]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000704]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000708]:csrrs tp, fcsr, zero
	-[0x8000070c]:fsw ft11, 376(ra)
Current Store : [0x80000710] : sw tp, 380(ra) -- Store: [0x80009590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000724]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000728]:csrrs tp, fcsr, zero
	-[0x8000072c]:fsw ft11, 384(ra)
Current Store : [0x80000730] : sw tp, 388(ra) -- Store: [0x80009598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000744]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000748]:csrrs tp, fcsr, zero
	-[0x8000074c]:fsw ft11, 392(ra)
Current Store : [0x80000750] : sw tp, 396(ra) -- Store: [0x800095a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000764]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000768]:csrrs tp, fcsr, zero
	-[0x8000076c]:fsw ft11, 400(ra)
Current Store : [0x80000770] : sw tp, 404(ra) -- Store: [0x800095a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000784]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000788]:csrrs tp, fcsr, zero
	-[0x8000078c]:fsw ft11, 408(ra)
Current Store : [0x80000790] : sw tp, 412(ra) -- Store: [0x800095b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800007a8]:csrrs tp, fcsr, zero
	-[0x800007ac]:fsw ft11, 416(ra)
Current Store : [0x800007b0] : sw tp, 420(ra) -- Store: [0x800095b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 424(ra)
Current Store : [0x800007d0] : sw tp, 428(ra) -- Store: [0x800095c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800007e8]:csrrs tp, fcsr, zero
	-[0x800007ec]:fsw ft11, 432(ra)
Current Store : [0x800007f0] : sw tp, 436(ra) -- Store: [0x800095c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000804]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000808]:csrrs tp, fcsr, zero
	-[0x8000080c]:fsw ft11, 440(ra)
Current Store : [0x80000810] : sw tp, 444(ra) -- Store: [0x800095d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000824]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000828]:csrrs tp, fcsr, zero
	-[0x8000082c]:fsw ft11, 448(ra)
Current Store : [0x80000830] : sw tp, 452(ra) -- Store: [0x800095d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000844]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000848]:csrrs tp, fcsr, zero
	-[0x8000084c]:fsw ft11, 456(ra)
Current Store : [0x80000850] : sw tp, 460(ra) -- Store: [0x800095e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000864]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000868]:csrrs tp, fcsr, zero
	-[0x8000086c]:fsw ft11, 464(ra)
Current Store : [0x80000870] : sw tp, 468(ra) -- Store: [0x800095e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000884]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000888]:csrrs tp, fcsr, zero
	-[0x8000088c]:fsw ft11, 472(ra)
Current Store : [0x80000890] : sw tp, 476(ra) -- Store: [0x800095f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800008a8]:csrrs tp, fcsr, zero
	-[0x800008ac]:fsw ft11, 480(ra)
Current Store : [0x800008b0] : sw tp, 484(ra) -- Store: [0x800095f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800008c8]:csrrs tp, fcsr, zero
	-[0x800008cc]:fsw ft11, 488(ra)
Current Store : [0x800008d0] : sw tp, 492(ra) -- Store: [0x80009600]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 496(ra)
Current Store : [0x800008f0] : sw tp, 500(ra) -- Store: [0x80009608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000904]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000908]:csrrs tp, fcsr, zero
	-[0x8000090c]:fsw ft11, 504(ra)
Current Store : [0x80000910] : sw tp, 508(ra) -- Store: [0x80009610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000924]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000928]:csrrs tp, fcsr, zero
	-[0x8000092c]:fsw ft11, 512(ra)
Current Store : [0x80000930] : sw tp, 516(ra) -- Store: [0x80009618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000944]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000948]:csrrs tp, fcsr, zero
	-[0x8000094c]:fsw ft11, 520(ra)
Current Store : [0x80000950] : sw tp, 524(ra) -- Store: [0x80009620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000964]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000968]:csrrs tp, fcsr, zero
	-[0x8000096c]:fsw ft11, 528(ra)
Current Store : [0x80000970] : sw tp, 532(ra) -- Store: [0x80009628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000984]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000988]:csrrs tp, fcsr, zero
	-[0x8000098c]:fsw ft11, 536(ra)
Current Store : [0x80000990] : sw tp, 540(ra) -- Store: [0x80009630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800009a8]:csrrs tp, fcsr, zero
	-[0x800009ac]:fsw ft11, 544(ra)
Current Store : [0x800009b0] : sw tp, 548(ra) -- Store: [0x80009638]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800009c8]:csrrs tp, fcsr, zero
	-[0x800009cc]:fsw ft11, 552(ra)
Current Store : [0x800009d0] : sw tp, 556(ra) -- Store: [0x80009640]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800009e8]:csrrs tp, fcsr, zero
	-[0x800009ec]:fsw ft11, 560(ra)
Current Store : [0x800009f0] : sw tp, 564(ra) -- Store: [0x80009648]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 568(ra)
Current Store : [0x80000a10] : sw tp, 572(ra) -- Store: [0x80009650]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000a28]:csrrs tp, fcsr, zero
	-[0x80000a2c]:fsw ft11, 576(ra)
Current Store : [0x80000a30] : sw tp, 580(ra) -- Store: [0x80009658]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000a48]:csrrs tp, fcsr, zero
	-[0x80000a4c]:fsw ft11, 584(ra)
Current Store : [0x80000a50] : sw tp, 588(ra) -- Store: [0x80009660]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000a68]:csrrs tp, fcsr, zero
	-[0x80000a6c]:fsw ft11, 592(ra)
Current Store : [0x80000a70] : sw tp, 596(ra) -- Store: [0x80009668]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000a88]:csrrs tp, fcsr, zero
	-[0x80000a8c]:fsw ft11, 600(ra)
Current Store : [0x80000a90] : sw tp, 604(ra) -- Store: [0x80009670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000aa8]:csrrs tp, fcsr, zero
	-[0x80000aac]:fsw ft11, 608(ra)
Current Store : [0x80000ab0] : sw tp, 612(ra) -- Store: [0x80009678]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ac4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ac8]:csrrs tp, fcsr, zero
	-[0x80000acc]:fsw ft11, 616(ra)
Current Store : [0x80000ad0] : sw tp, 620(ra) -- Store: [0x80009680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ae4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ae8]:csrrs tp, fcsr, zero
	-[0x80000aec]:fsw ft11, 624(ra)
Current Store : [0x80000af0] : sw tp, 628(ra) -- Store: [0x80009688]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000b08]:csrrs tp, fcsr, zero
	-[0x80000b0c]:fsw ft11, 632(ra)
Current Store : [0x80000b10] : sw tp, 636(ra) -- Store: [0x80009690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000b28]:csrrs tp, fcsr, zero
	-[0x80000b2c]:fsw ft11, 640(ra)
Current Store : [0x80000b30] : sw tp, 644(ra) -- Store: [0x80009698]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000b48]:csrrs tp, fcsr, zero
	-[0x80000b4c]:fsw ft11, 648(ra)
Current Store : [0x80000b50] : sw tp, 652(ra) -- Store: [0x800096a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000b68]:csrrs tp, fcsr, zero
	-[0x80000b6c]:fsw ft11, 656(ra)
Current Store : [0x80000b70] : sw tp, 660(ra) -- Store: [0x800096a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000b88]:csrrs tp, fcsr, zero
	-[0x80000b8c]:fsw ft11, 664(ra)
Current Store : [0x80000b90] : sw tp, 668(ra) -- Store: [0x800096b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ba4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ba8]:csrrs tp, fcsr, zero
	-[0x80000bac]:fsw ft11, 672(ra)
Current Store : [0x80000bb0] : sw tp, 676(ra) -- Store: [0x800096b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000bc8]:csrrs tp, fcsr, zero
	-[0x80000bcc]:fsw ft11, 680(ra)
Current Store : [0x80000bd0] : sw tp, 684(ra) -- Store: [0x800096c0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000be4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000be8]:csrrs tp, fcsr, zero
	-[0x80000bec]:fsw ft11, 688(ra)
Current Store : [0x80000bf0] : sw tp, 692(ra) -- Store: [0x800096c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000c08]:csrrs tp, fcsr, zero
	-[0x80000c0c]:fsw ft11, 696(ra)
Current Store : [0x80000c10] : sw tp, 700(ra) -- Store: [0x800096d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000c28]:csrrs tp, fcsr, zero
	-[0x80000c2c]:fsw ft11, 704(ra)
Current Store : [0x80000c30] : sw tp, 708(ra) -- Store: [0x800096d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000c48]:csrrs tp, fcsr, zero
	-[0x80000c4c]:fsw ft11, 712(ra)
Current Store : [0x80000c50] : sw tp, 716(ra) -- Store: [0x800096e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000c68]:csrrs tp, fcsr, zero
	-[0x80000c6c]:fsw ft11, 720(ra)
Current Store : [0x80000c70] : sw tp, 724(ra) -- Store: [0x800096e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000c88]:csrrs tp, fcsr, zero
	-[0x80000c8c]:fsw ft11, 728(ra)
Current Store : [0x80000c90] : sw tp, 732(ra) -- Store: [0x800096f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ca8]:csrrs tp, fcsr, zero
	-[0x80000cac]:fsw ft11, 736(ra)
Current Store : [0x80000cb0] : sw tp, 740(ra) -- Store: [0x800096f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cc4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000cc8]:csrrs tp, fcsr, zero
	-[0x80000ccc]:fsw ft11, 744(ra)
Current Store : [0x80000cd0] : sw tp, 748(ra) -- Store: [0x80009700]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ce8]:csrrs tp, fcsr, zero
	-[0x80000cec]:fsw ft11, 752(ra)
Current Store : [0x80000cf0] : sw tp, 756(ra) -- Store: [0x80009708]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000d08]:csrrs tp, fcsr, zero
	-[0x80000d0c]:fsw ft11, 760(ra)
Current Store : [0x80000d10] : sw tp, 764(ra) -- Store: [0x80009710]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000d28]:csrrs tp, fcsr, zero
	-[0x80000d2c]:fsw ft11, 768(ra)
Current Store : [0x80000d30] : sw tp, 772(ra) -- Store: [0x80009718]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000d48]:csrrs tp, fcsr, zero
	-[0x80000d4c]:fsw ft11, 776(ra)
Current Store : [0x80000d50] : sw tp, 780(ra) -- Store: [0x80009720]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000d68]:csrrs tp, fcsr, zero
	-[0x80000d6c]:fsw ft11, 784(ra)
Current Store : [0x80000d70] : sw tp, 788(ra) -- Store: [0x80009728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000d88]:csrrs tp, fcsr, zero
	-[0x80000d8c]:fsw ft11, 792(ra)
Current Store : [0x80000d90] : sw tp, 796(ra) -- Store: [0x80009730]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000da4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000da8]:csrrs tp, fcsr, zero
	-[0x80000dac]:fsw ft11, 800(ra)
Current Store : [0x80000db0] : sw tp, 804(ra) -- Store: [0x80009738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dc4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000dc8]:csrrs tp, fcsr, zero
	-[0x80000dcc]:fsw ft11, 808(ra)
Current Store : [0x80000dd0] : sw tp, 812(ra) -- Store: [0x80009740]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000de4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000de8]:csrrs tp, fcsr, zero
	-[0x80000dec]:fsw ft11, 816(ra)
Current Store : [0x80000df0] : sw tp, 820(ra) -- Store: [0x80009748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000e08]:csrrs tp, fcsr, zero
	-[0x80000e0c]:fsw ft11, 824(ra)
Current Store : [0x80000e10] : sw tp, 828(ra) -- Store: [0x80009750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000e28]:csrrs tp, fcsr, zero
	-[0x80000e2c]:fsw ft11, 832(ra)
Current Store : [0x80000e30] : sw tp, 836(ra) -- Store: [0x80009758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000e48]:csrrs tp, fcsr, zero
	-[0x80000e4c]:fsw ft11, 840(ra)
Current Store : [0x80000e50] : sw tp, 844(ra) -- Store: [0x80009760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000e68]:csrrs tp, fcsr, zero
	-[0x80000e6c]:fsw ft11, 848(ra)
Current Store : [0x80000e70] : sw tp, 852(ra) -- Store: [0x80009768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000e88]:csrrs tp, fcsr, zero
	-[0x80000e8c]:fsw ft11, 856(ra)
Current Store : [0x80000e90] : sw tp, 860(ra) -- Store: [0x80009770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ea4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ea8]:csrrs tp, fcsr, zero
	-[0x80000eac]:fsw ft11, 864(ra)
Current Store : [0x80000eb0] : sw tp, 868(ra) -- Store: [0x80009778]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ec4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ec8]:csrrs tp, fcsr, zero
	-[0x80000ecc]:fsw ft11, 872(ra)
Current Store : [0x80000ed0] : sw tp, 876(ra) -- Store: [0x80009780]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ee4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000ee8]:csrrs tp, fcsr, zero
	-[0x80000eec]:fsw ft11, 880(ra)
Current Store : [0x80000ef0] : sw tp, 884(ra) -- Store: [0x80009788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f04]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000f08]:csrrs tp, fcsr, zero
	-[0x80000f0c]:fsw ft11, 888(ra)
Current Store : [0x80000f10] : sw tp, 892(ra) -- Store: [0x80009790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000f28]:csrrs tp, fcsr, zero
	-[0x80000f2c]:fsw ft11, 896(ra)
Current Store : [0x80000f30] : sw tp, 900(ra) -- Store: [0x80009798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000f48]:csrrs tp, fcsr, zero
	-[0x80000f4c]:fsw ft11, 904(ra)
Current Store : [0x80000f50] : sw tp, 908(ra) -- Store: [0x800097a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000f68]:csrrs tp, fcsr, zero
	-[0x80000f6c]:fsw ft11, 912(ra)
Current Store : [0x80000f70] : sw tp, 916(ra) -- Store: [0x800097a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000f88]:csrrs tp, fcsr, zero
	-[0x80000f8c]:fsw ft11, 920(ra)
Current Store : [0x80000f90] : sw tp, 924(ra) -- Store: [0x800097b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fa4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000fa8]:csrrs tp, fcsr, zero
	-[0x80000fac]:fsw ft11, 928(ra)
Current Store : [0x80000fb0] : sw tp, 932(ra) -- Store: [0x800097b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fc4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000fc8]:csrrs tp, fcsr, zero
	-[0x80000fcc]:fsw ft11, 936(ra)
Current Store : [0x80000fd0] : sw tp, 940(ra) -- Store: [0x800097c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fe4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80000fe8]:csrrs tp, fcsr, zero
	-[0x80000fec]:fsw ft11, 944(ra)
Current Store : [0x80000ff0] : sw tp, 948(ra) -- Store: [0x800097c8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001004]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001008]:csrrs tp, fcsr, zero
	-[0x8000100c]:fsw ft11, 952(ra)
Current Store : [0x80001010] : sw tp, 956(ra) -- Store: [0x800097d0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001024]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001028]:csrrs tp, fcsr, zero
	-[0x8000102c]:fsw ft11, 960(ra)
Current Store : [0x80001030] : sw tp, 964(ra) -- Store: [0x800097d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001044]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001048]:csrrs tp, fcsr, zero
	-[0x8000104c]:fsw ft11, 968(ra)
Current Store : [0x80001050] : sw tp, 972(ra) -- Store: [0x800097e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001064]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001068]:csrrs tp, fcsr, zero
	-[0x8000106c]:fsw ft11, 976(ra)
Current Store : [0x80001070] : sw tp, 980(ra) -- Store: [0x800097e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001084]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001088]:csrrs tp, fcsr, zero
	-[0x8000108c]:fsw ft11, 984(ra)
Current Store : [0x80001090] : sw tp, 988(ra) -- Store: [0x800097f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800010a8]:csrrs tp, fcsr, zero
	-[0x800010ac]:fsw ft11, 992(ra)
Current Store : [0x800010b0] : sw tp, 996(ra) -- Store: [0x800097f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800010c8]:csrrs tp, fcsr, zero
	-[0x800010cc]:fsw ft11, 1000(ra)
Current Store : [0x800010d0] : sw tp, 1004(ra) -- Store: [0x80009800]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800010e8]:csrrs tp, fcsr, zero
	-[0x800010ec]:fsw ft11, 1008(ra)
Current Store : [0x800010f0] : sw tp, 1012(ra) -- Store: [0x80009808]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001104]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001108]:csrrs tp, fcsr, zero
	-[0x8000110c]:fsw ft11, 1016(ra)
Current Store : [0x80001110] : sw tp, 1020(ra) -- Store: [0x80009810]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000112c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001130]:csrrs tp, fcsr, zero
	-[0x80001134]:fsw ft11, 0(ra)
Current Store : [0x80001138] : sw tp, 4(ra) -- Store: [0x80009818]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000114c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001150]:csrrs tp, fcsr, zero
	-[0x80001154]:fsw ft11, 8(ra)
Current Store : [0x80001158] : sw tp, 12(ra) -- Store: [0x80009820]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000116c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001170]:csrrs tp, fcsr, zero
	-[0x80001174]:fsw ft11, 16(ra)
Current Store : [0x80001178] : sw tp, 20(ra) -- Store: [0x80009828]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000118c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001190]:csrrs tp, fcsr, zero
	-[0x80001194]:fsw ft11, 24(ra)
Current Store : [0x80001198] : sw tp, 28(ra) -- Store: [0x80009830]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800011b0]:csrrs tp, fcsr, zero
	-[0x800011b4]:fsw ft11, 32(ra)
Current Store : [0x800011b8] : sw tp, 36(ra) -- Store: [0x80009838]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800011d0]:csrrs tp, fcsr, zero
	-[0x800011d4]:fsw ft11, 40(ra)
Current Store : [0x800011d8] : sw tp, 44(ra) -- Store: [0x80009840]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800011f0]:csrrs tp, fcsr, zero
	-[0x800011f4]:fsw ft11, 48(ra)
Current Store : [0x800011f8] : sw tp, 52(ra) -- Store: [0x80009848]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000120c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001210]:csrrs tp, fcsr, zero
	-[0x80001214]:fsw ft11, 56(ra)
Current Store : [0x80001218] : sw tp, 60(ra) -- Store: [0x80009850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001230]:csrrs tp, fcsr, zero
	-[0x80001234]:fsw ft11, 64(ra)
Current Store : [0x80001238] : sw tp, 68(ra) -- Store: [0x80009858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000124c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001250]:csrrs tp, fcsr, zero
	-[0x80001254]:fsw ft11, 72(ra)
Current Store : [0x80001258] : sw tp, 76(ra) -- Store: [0x80009860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000126c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001270]:csrrs tp, fcsr, zero
	-[0x80001274]:fsw ft11, 80(ra)
Current Store : [0x80001278] : sw tp, 84(ra) -- Store: [0x80009868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000128c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001290]:csrrs tp, fcsr, zero
	-[0x80001294]:fsw ft11, 88(ra)
Current Store : [0x80001298] : sw tp, 92(ra) -- Store: [0x80009870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800012b0]:csrrs tp, fcsr, zero
	-[0x800012b4]:fsw ft11, 96(ra)
Current Store : [0x800012b8] : sw tp, 100(ra) -- Store: [0x80009878]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800012d0]:csrrs tp, fcsr, zero
	-[0x800012d4]:fsw ft11, 104(ra)
Current Store : [0x800012d8] : sw tp, 108(ra) -- Store: [0x80009880]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800012f0]:csrrs tp, fcsr, zero
	-[0x800012f4]:fsw ft11, 112(ra)
Current Store : [0x800012f8] : sw tp, 116(ra) -- Store: [0x80009888]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000130c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001310]:csrrs tp, fcsr, zero
	-[0x80001314]:fsw ft11, 120(ra)
Current Store : [0x80001318] : sw tp, 124(ra) -- Store: [0x80009890]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000132c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001330]:csrrs tp, fcsr, zero
	-[0x80001334]:fsw ft11, 128(ra)
Current Store : [0x80001338] : sw tp, 132(ra) -- Store: [0x80009898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000134c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001350]:csrrs tp, fcsr, zero
	-[0x80001354]:fsw ft11, 136(ra)
Current Store : [0x80001358] : sw tp, 140(ra) -- Store: [0x800098a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000136c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001370]:csrrs tp, fcsr, zero
	-[0x80001374]:fsw ft11, 144(ra)
Current Store : [0x80001378] : sw tp, 148(ra) -- Store: [0x800098a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000138c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001390]:csrrs tp, fcsr, zero
	-[0x80001394]:fsw ft11, 152(ra)
Current Store : [0x80001398] : sw tp, 156(ra) -- Store: [0x800098b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800013b0]:csrrs tp, fcsr, zero
	-[0x800013b4]:fsw ft11, 160(ra)
Current Store : [0x800013b8] : sw tp, 164(ra) -- Store: [0x800098b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800013d0]:csrrs tp, fcsr, zero
	-[0x800013d4]:fsw ft11, 168(ra)
Current Store : [0x800013d8] : sw tp, 172(ra) -- Store: [0x800098c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800013f0]:csrrs tp, fcsr, zero
	-[0x800013f4]:fsw ft11, 176(ra)
Current Store : [0x800013f8] : sw tp, 180(ra) -- Store: [0x800098c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000140c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001410]:csrrs tp, fcsr, zero
	-[0x80001414]:fsw ft11, 184(ra)
Current Store : [0x80001418] : sw tp, 188(ra) -- Store: [0x800098d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001430]:csrrs tp, fcsr, zero
	-[0x80001434]:fsw ft11, 192(ra)
Current Store : [0x80001438] : sw tp, 196(ra) -- Store: [0x800098d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000144c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001450]:csrrs tp, fcsr, zero
	-[0x80001454]:fsw ft11, 200(ra)
Current Store : [0x80001458] : sw tp, 204(ra) -- Store: [0x800098e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000146c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001470]:csrrs tp, fcsr, zero
	-[0x80001474]:fsw ft11, 208(ra)
Current Store : [0x80001478] : sw tp, 212(ra) -- Store: [0x800098e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000148c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001490]:csrrs tp, fcsr, zero
	-[0x80001494]:fsw ft11, 216(ra)
Current Store : [0x80001498] : sw tp, 220(ra) -- Store: [0x800098f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800014b0]:csrrs tp, fcsr, zero
	-[0x800014b4]:fsw ft11, 224(ra)
Current Store : [0x800014b8] : sw tp, 228(ra) -- Store: [0x800098f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800014d0]:csrrs tp, fcsr, zero
	-[0x800014d4]:fsw ft11, 232(ra)
Current Store : [0x800014d8] : sw tp, 236(ra) -- Store: [0x80009900]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800014f0]:csrrs tp, fcsr, zero
	-[0x800014f4]:fsw ft11, 240(ra)
Current Store : [0x800014f8] : sw tp, 244(ra) -- Store: [0x80009908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001510]:csrrs tp, fcsr, zero
	-[0x80001514]:fsw ft11, 248(ra)
Current Store : [0x80001518] : sw tp, 252(ra) -- Store: [0x80009910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000152c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001530]:csrrs tp, fcsr, zero
	-[0x80001534]:fsw ft11, 256(ra)
Current Store : [0x80001538] : sw tp, 260(ra) -- Store: [0x80009918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001550]:csrrs tp, fcsr, zero
	-[0x80001554]:fsw ft11, 264(ra)
Current Store : [0x80001558] : sw tp, 268(ra) -- Store: [0x80009920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000156c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001570]:csrrs tp, fcsr, zero
	-[0x80001574]:fsw ft11, 272(ra)
Current Store : [0x80001578] : sw tp, 276(ra) -- Store: [0x80009928]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000158c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001590]:csrrs tp, fcsr, zero
	-[0x80001594]:fsw ft11, 280(ra)
Current Store : [0x80001598] : sw tp, 284(ra) -- Store: [0x80009930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800015b0]:csrrs tp, fcsr, zero
	-[0x800015b4]:fsw ft11, 288(ra)
Current Store : [0x800015b8] : sw tp, 292(ra) -- Store: [0x80009938]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800015d0]:csrrs tp, fcsr, zero
	-[0x800015d4]:fsw ft11, 296(ra)
Current Store : [0x800015d8] : sw tp, 300(ra) -- Store: [0x80009940]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800015f0]:csrrs tp, fcsr, zero
	-[0x800015f4]:fsw ft11, 304(ra)
Current Store : [0x800015f8] : sw tp, 308(ra) -- Store: [0x80009948]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000160c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001610]:csrrs tp, fcsr, zero
	-[0x80001614]:fsw ft11, 312(ra)
Current Store : [0x80001618] : sw tp, 316(ra) -- Store: [0x80009950]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000162c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001630]:csrrs tp, fcsr, zero
	-[0x80001634]:fsw ft11, 320(ra)
Current Store : [0x80001638] : sw tp, 324(ra) -- Store: [0x80009958]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000164c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001650]:csrrs tp, fcsr, zero
	-[0x80001654]:fsw ft11, 328(ra)
Current Store : [0x80001658] : sw tp, 332(ra) -- Store: [0x80009960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000166c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001670]:csrrs tp, fcsr, zero
	-[0x80001674]:fsw ft11, 336(ra)
Current Store : [0x80001678] : sw tp, 340(ra) -- Store: [0x80009968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000168c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001690]:csrrs tp, fcsr, zero
	-[0x80001694]:fsw ft11, 344(ra)
Current Store : [0x80001698] : sw tp, 348(ra) -- Store: [0x80009970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800016b0]:csrrs tp, fcsr, zero
	-[0x800016b4]:fsw ft11, 352(ra)
Current Store : [0x800016b8] : sw tp, 356(ra) -- Store: [0x80009978]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800016d0]:csrrs tp, fcsr, zero
	-[0x800016d4]:fsw ft11, 360(ra)
Current Store : [0x800016d8] : sw tp, 364(ra) -- Store: [0x80009980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800016f0]:csrrs tp, fcsr, zero
	-[0x800016f4]:fsw ft11, 368(ra)
Current Store : [0x800016f8] : sw tp, 372(ra) -- Store: [0x80009988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000170c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001710]:csrrs tp, fcsr, zero
	-[0x80001714]:fsw ft11, 376(ra)
Current Store : [0x80001718] : sw tp, 380(ra) -- Store: [0x80009990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000172c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001730]:csrrs tp, fcsr, zero
	-[0x80001734]:fsw ft11, 384(ra)
Current Store : [0x80001738] : sw tp, 388(ra) -- Store: [0x80009998]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000174c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001750]:csrrs tp, fcsr, zero
	-[0x80001754]:fsw ft11, 392(ra)
Current Store : [0x80001758] : sw tp, 396(ra) -- Store: [0x800099a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000176c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001770]:csrrs tp, fcsr, zero
	-[0x80001774]:fsw ft11, 400(ra)
Current Store : [0x80001778] : sw tp, 404(ra) -- Store: [0x800099a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000178c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001790]:csrrs tp, fcsr, zero
	-[0x80001794]:fsw ft11, 408(ra)
Current Store : [0x80001798] : sw tp, 412(ra) -- Store: [0x800099b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800017b0]:csrrs tp, fcsr, zero
	-[0x800017b4]:fsw ft11, 416(ra)
Current Store : [0x800017b8] : sw tp, 420(ra) -- Store: [0x800099b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800017d0]:csrrs tp, fcsr, zero
	-[0x800017d4]:fsw ft11, 424(ra)
Current Store : [0x800017d8] : sw tp, 428(ra) -- Store: [0x800099c0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800017f0]:csrrs tp, fcsr, zero
	-[0x800017f4]:fsw ft11, 432(ra)
Current Store : [0x800017f8] : sw tp, 436(ra) -- Store: [0x800099c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000180c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001810]:csrrs tp, fcsr, zero
	-[0x80001814]:fsw ft11, 440(ra)
Current Store : [0x80001818] : sw tp, 444(ra) -- Store: [0x800099d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000182c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001830]:csrrs tp, fcsr, zero
	-[0x80001834]:fsw ft11, 448(ra)
Current Store : [0x80001838] : sw tp, 452(ra) -- Store: [0x800099d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000184c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001850]:csrrs tp, fcsr, zero
	-[0x80001854]:fsw ft11, 456(ra)
Current Store : [0x80001858] : sw tp, 460(ra) -- Store: [0x800099e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000186c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001870]:csrrs tp, fcsr, zero
	-[0x80001874]:fsw ft11, 464(ra)
Current Store : [0x80001878] : sw tp, 468(ra) -- Store: [0x800099e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000188c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001890]:csrrs tp, fcsr, zero
	-[0x80001894]:fsw ft11, 472(ra)
Current Store : [0x80001898] : sw tp, 476(ra) -- Store: [0x800099f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800018b0]:csrrs tp, fcsr, zero
	-[0x800018b4]:fsw ft11, 480(ra)
Current Store : [0x800018b8] : sw tp, 484(ra) -- Store: [0x800099f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800018d0]:csrrs tp, fcsr, zero
	-[0x800018d4]:fsw ft11, 488(ra)
Current Store : [0x800018d8] : sw tp, 492(ra) -- Store: [0x80009a00]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800018f0]:csrrs tp, fcsr, zero
	-[0x800018f4]:fsw ft11, 496(ra)
Current Store : [0x800018f8] : sw tp, 500(ra) -- Store: [0x80009a08]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000190c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001910]:csrrs tp, fcsr, zero
	-[0x80001914]:fsw ft11, 504(ra)
Current Store : [0x80001918] : sw tp, 508(ra) -- Store: [0x80009a10]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000192c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001930]:csrrs tp, fcsr, zero
	-[0x80001934]:fsw ft11, 512(ra)
Current Store : [0x80001938] : sw tp, 516(ra) -- Store: [0x80009a18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000194c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001950]:csrrs tp, fcsr, zero
	-[0x80001954]:fsw ft11, 520(ra)
Current Store : [0x80001958] : sw tp, 524(ra) -- Store: [0x80009a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000196c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001970]:csrrs tp, fcsr, zero
	-[0x80001974]:fsw ft11, 528(ra)
Current Store : [0x80001978] : sw tp, 532(ra) -- Store: [0x80009a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000198c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001990]:csrrs tp, fcsr, zero
	-[0x80001994]:fsw ft11, 536(ra)
Current Store : [0x80001998] : sw tp, 540(ra) -- Store: [0x80009a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800019b0]:csrrs tp, fcsr, zero
	-[0x800019b4]:fsw ft11, 544(ra)
Current Store : [0x800019b8] : sw tp, 548(ra) -- Store: [0x80009a38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800019d0]:csrrs tp, fcsr, zero
	-[0x800019d4]:fsw ft11, 552(ra)
Current Store : [0x800019d8] : sw tp, 556(ra) -- Store: [0x80009a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800019f0]:csrrs tp, fcsr, zero
	-[0x800019f4]:fsw ft11, 560(ra)
Current Store : [0x800019f8] : sw tp, 564(ra) -- Store: [0x80009a48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001a10]:csrrs tp, fcsr, zero
	-[0x80001a14]:fsw ft11, 568(ra)
Current Store : [0x80001a18] : sw tp, 572(ra) -- Store: [0x80009a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001a30]:csrrs tp, fcsr, zero
	-[0x80001a34]:fsw ft11, 576(ra)
Current Store : [0x80001a38] : sw tp, 580(ra) -- Store: [0x80009a58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001a50]:csrrs tp, fcsr, zero
	-[0x80001a54]:fsw ft11, 584(ra)
Current Store : [0x80001a58] : sw tp, 588(ra) -- Store: [0x80009a60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001a70]:csrrs tp, fcsr, zero
	-[0x80001a74]:fsw ft11, 592(ra)
Current Store : [0x80001a78] : sw tp, 596(ra) -- Store: [0x80009a68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001a90]:csrrs tp, fcsr, zero
	-[0x80001a94]:fsw ft11, 600(ra)
Current Store : [0x80001a98] : sw tp, 604(ra) -- Store: [0x80009a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001aac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001ab0]:csrrs tp, fcsr, zero
	-[0x80001ab4]:fsw ft11, 608(ra)
Current Store : [0x80001ab8] : sw tp, 612(ra) -- Store: [0x80009a78]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001acc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001ad0]:csrrs tp, fcsr, zero
	-[0x80001ad4]:fsw ft11, 616(ra)
Current Store : [0x80001ad8] : sw tp, 620(ra) -- Store: [0x80009a80]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001aec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001af0]:csrrs tp, fcsr, zero
	-[0x80001af4]:fsw ft11, 624(ra)
Current Store : [0x80001af8] : sw tp, 628(ra) -- Store: [0x80009a88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001b10]:csrrs tp, fcsr, zero
	-[0x80001b14]:fsw ft11, 632(ra)
Current Store : [0x80001b18] : sw tp, 636(ra) -- Store: [0x80009a90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001b30]:csrrs tp, fcsr, zero
	-[0x80001b34]:fsw ft11, 640(ra)
Current Store : [0x80001b38] : sw tp, 644(ra) -- Store: [0x80009a98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001b50]:csrrs tp, fcsr, zero
	-[0x80001b54]:fsw ft11, 648(ra)
Current Store : [0x80001b58] : sw tp, 652(ra) -- Store: [0x80009aa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001b70]:csrrs tp, fcsr, zero
	-[0x80001b74]:fsw ft11, 656(ra)
Current Store : [0x80001b78] : sw tp, 660(ra) -- Store: [0x80009aa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001b90]:csrrs tp, fcsr, zero
	-[0x80001b94]:fsw ft11, 664(ra)
Current Store : [0x80001b98] : sw tp, 668(ra) -- Store: [0x80009ab0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001bb0]:csrrs tp, fcsr, zero
	-[0x80001bb4]:fsw ft11, 672(ra)
Current Store : [0x80001bb8] : sw tp, 676(ra) -- Store: [0x80009ab8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bcc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001bd0]:csrrs tp, fcsr, zero
	-[0x80001bd4]:fsw ft11, 680(ra)
Current Store : [0x80001bd8] : sw tp, 684(ra) -- Store: [0x80009ac0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001bf0]:csrrs tp, fcsr, zero
	-[0x80001bf4]:fsw ft11, 688(ra)
Current Store : [0x80001bf8] : sw tp, 692(ra) -- Store: [0x80009ac8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001c10]:csrrs tp, fcsr, zero
	-[0x80001c14]:fsw ft11, 696(ra)
Current Store : [0x80001c18] : sw tp, 700(ra) -- Store: [0x80009ad0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001c30]:csrrs tp, fcsr, zero
	-[0x80001c34]:fsw ft11, 704(ra)
Current Store : [0x80001c38] : sw tp, 708(ra) -- Store: [0x80009ad8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001c50]:csrrs tp, fcsr, zero
	-[0x80001c54]:fsw ft11, 712(ra)
Current Store : [0x80001c58] : sw tp, 716(ra) -- Store: [0x80009ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001c70]:csrrs tp, fcsr, zero
	-[0x80001c74]:fsw ft11, 720(ra)
Current Store : [0x80001c78] : sw tp, 724(ra) -- Store: [0x80009ae8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001c90]:csrrs tp, fcsr, zero
	-[0x80001c94]:fsw ft11, 728(ra)
Current Store : [0x80001c98] : sw tp, 732(ra) -- Store: [0x80009af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001cb0]:csrrs tp, fcsr, zero
	-[0x80001cb4]:fsw ft11, 736(ra)
Current Store : [0x80001cb8] : sw tp, 740(ra) -- Store: [0x80009af8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ccc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001cd0]:csrrs tp, fcsr, zero
	-[0x80001cd4]:fsw ft11, 744(ra)
Current Store : [0x80001cd8] : sw tp, 748(ra) -- Store: [0x80009b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001cf0]:csrrs tp, fcsr, zero
	-[0x80001cf4]:fsw ft11, 752(ra)
Current Store : [0x80001cf8] : sw tp, 756(ra) -- Store: [0x80009b08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001d10]:csrrs tp, fcsr, zero
	-[0x80001d14]:fsw ft11, 760(ra)
Current Store : [0x80001d18] : sw tp, 764(ra) -- Store: [0x80009b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001d30]:csrrs tp, fcsr, zero
	-[0x80001d34]:fsw ft11, 768(ra)
Current Store : [0x80001d38] : sw tp, 772(ra) -- Store: [0x80009b18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001d50]:csrrs tp, fcsr, zero
	-[0x80001d54]:fsw ft11, 776(ra)
Current Store : [0x80001d58] : sw tp, 780(ra) -- Store: [0x80009b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001d70]:csrrs tp, fcsr, zero
	-[0x80001d74]:fsw ft11, 784(ra)
Current Store : [0x80001d78] : sw tp, 788(ra) -- Store: [0x80009b28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001d90]:csrrs tp, fcsr, zero
	-[0x80001d94]:fsw ft11, 792(ra)
Current Store : [0x80001d98] : sw tp, 796(ra) -- Store: [0x80009b30]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001db0]:csrrs tp, fcsr, zero
	-[0x80001db4]:fsw ft11, 800(ra)
Current Store : [0x80001db8] : sw tp, 804(ra) -- Store: [0x80009b38]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dcc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001dd0]:csrrs tp, fcsr, zero
	-[0x80001dd4]:fsw ft11, 808(ra)
Current Store : [0x80001dd8] : sw tp, 812(ra) -- Store: [0x80009b40]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001df0]:csrrs tp, fcsr, zero
	-[0x80001df4]:fsw ft11, 816(ra)
Current Store : [0x80001df8] : sw tp, 820(ra) -- Store: [0x80009b48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001e10]:csrrs tp, fcsr, zero
	-[0x80001e14]:fsw ft11, 824(ra)
Current Store : [0x80001e18] : sw tp, 828(ra) -- Store: [0x80009b50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001e30]:csrrs tp, fcsr, zero
	-[0x80001e34]:fsw ft11, 832(ra)
Current Store : [0x80001e38] : sw tp, 836(ra) -- Store: [0x80009b58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001e50]:csrrs tp, fcsr, zero
	-[0x80001e54]:fsw ft11, 840(ra)
Current Store : [0x80001e58] : sw tp, 844(ra) -- Store: [0x80009b60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001e70]:csrrs tp, fcsr, zero
	-[0x80001e74]:fsw ft11, 848(ra)
Current Store : [0x80001e78] : sw tp, 852(ra) -- Store: [0x80009b68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001e90]:csrrs tp, fcsr, zero
	-[0x80001e94]:fsw ft11, 856(ra)
Current Store : [0x80001e98] : sw tp, 860(ra) -- Store: [0x80009b70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001eac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001eb0]:csrrs tp, fcsr, zero
	-[0x80001eb4]:fsw ft11, 864(ra)
Current Store : [0x80001eb8] : sw tp, 868(ra) -- Store: [0x80009b78]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ecc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001ed0]:csrrs tp, fcsr, zero
	-[0x80001ed4]:fsw ft11, 872(ra)
Current Store : [0x80001ed8] : sw tp, 876(ra) -- Store: [0x80009b80]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001eec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001ef0]:csrrs tp, fcsr, zero
	-[0x80001ef4]:fsw ft11, 880(ra)
Current Store : [0x80001ef8] : sw tp, 884(ra) -- Store: [0x80009b88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001f10]:csrrs tp, fcsr, zero
	-[0x80001f14]:fsw ft11, 888(ra)
Current Store : [0x80001f18] : sw tp, 892(ra) -- Store: [0x80009b90]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001f30]:csrrs tp, fcsr, zero
	-[0x80001f34]:fsw ft11, 896(ra)
Current Store : [0x80001f38] : sw tp, 900(ra) -- Store: [0x80009b98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001f50]:csrrs tp, fcsr, zero
	-[0x80001f54]:fsw ft11, 904(ra)
Current Store : [0x80001f58] : sw tp, 908(ra) -- Store: [0x80009ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001f70]:csrrs tp, fcsr, zero
	-[0x80001f74]:fsw ft11, 912(ra)
Current Store : [0x80001f78] : sw tp, 916(ra) -- Store: [0x80009ba8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001f90]:csrrs tp, fcsr, zero
	-[0x80001f94]:fsw ft11, 920(ra)
Current Store : [0x80001f98] : sw tp, 924(ra) -- Store: [0x80009bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fac]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001fb0]:csrrs tp, fcsr, zero
	-[0x80001fb4]:fsw ft11, 928(ra)
Current Store : [0x80001fb8] : sw tp, 932(ra) -- Store: [0x80009bb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fcc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001fd0]:csrrs tp, fcsr, zero
	-[0x80001fd4]:fsw ft11, 936(ra)
Current Store : [0x80001fd8] : sw tp, 940(ra) -- Store: [0x80009bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80001ff0]:csrrs tp, fcsr, zero
	-[0x80001ff4]:fsw ft11, 944(ra)
Current Store : [0x80001ff8] : sw tp, 948(ra) -- Store: [0x80009bc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000200c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002010]:csrrs tp, fcsr, zero
	-[0x80002014]:fsw ft11, 952(ra)
Current Store : [0x80002018] : sw tp, 956(ra) -- Store: [0x80009bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000202c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002030]:csrrs tp, fcsr, zero
	-[0x80002034]:fsw ft11, 960(ra)
Current Store : [0x80002038] : sw tp, 964(ra) -- Store: [0x80009bd8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000204c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002050]:csrrs tp, fcsr, zero
	-[0x80002054]:fsw ft11, 968(ra)
Current Store : [0x80002058] : sw tp, 972(ra) -- Store: [0x80009be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000206c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002070]:csrrs tp, fcsr, zero
	-[0x80002074]:fsw ft11, 976(ra)
Current Store : [0x80002078] : sw tp, 980(ra) -- Store: [0x80009be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000208c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002090]:csrrs tp, fcsr, zero
	-[0x80002094]:fsw ft11, 984(ra)
Current Store : [0x80002098] : sw tp, 988(ra) -- Store: [0x80009bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800020b0]:csrrs tp, fcsr, zero
	-[0x800020b4]:fsw ft11, 992(ra)
Current Store : [0x800020b8] : sw tp, 996(ra) -- Store: [0x80009bf8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800020d0]:csrrs tp, fcsr, zero
	-[0x800020d4]:fsw ft11, 1000(ra)
Current Store : [0x800020d8] : sw tp, 1004(ra) -- Store: [0x80009c00]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800020f0]:csrrs tp, fcsr, zero
	-[0x800020f4]:fsw ft11, 1008(ra)
Current Store : [0x800020f8] : sw tp, 1012(ra) -- Store: [0x80009c08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000210c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002110]:csrrs tp, fcsr, zero
	-[0x80002114]:fsw ft11, 1016(ra)
Current Store : [0x80002118] : sw tp, 1020(ra) -- Store: [0x80009c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002154]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002158]:csrrs tp, fcsr, zero
	-[0x8000215c]:fsw ft11, 0(ra)
Current Store : [0x80002160] : sw tp, 4(ra) -- Store: [0x80009c18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002194]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002198]:csrrs tp, fcsr, zero
	-[0x8000219c]:fsw ft11, 8(ra)
Current Store : [0x800021a0] : sw tp, 12(ra) -- Store: [0x80009c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800021d8]:csrrs tp, fcsr, zero
	-[0x800021dc]:fsw ft11, 16(ra)
Current Store : [0x800021e0] : sw tp, 20(ra) -- Store: [0x80009c28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002214]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002218]:csrrs tp, fcsr, zero
	-[0x8000221c]:fsw ft11, 24(ra)
Current Store : [0x80002220] : sw tp, 28(ra) -- Store: [0x80009c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002254]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002258]:csrrs tp, fcsr, zero
	-[0x8000225c]:fsw ft11, 32(ra)
Current Store : [0x80002260] : sw tp, 36(ra) -- Store: [0x80009c38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002294]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002298]:csrrs tp, fcsr, zero
	-[0x8000229c]:fsw ft11, 40(ra)
Current Store : [0x800022a0] : sw tp, 44(ra) -- Store: [0x80009c40]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800022d8]:csrrs tp, fcsr, zero
	-[0x800022dc]:fsw ft11, 48(ra)
Current Store : [0x800022e0] : sw tp, 52(ra) -- Store: [0x80009c48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002314]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002318]:csrrs tp, fcsr, zero
	-[0x8000231c]:fsw ft11, 56(ra)
Current Store : [0x80002320] : sw tp, 60(ra) -- Store: [0x80009c50]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002354]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002358]:csrrs tp, fcsr, zero
	-[0x8000235c]:fsw ft11, 64(ra)
Current Store : [0x80002360] : sw tp, 68(ra) -- Store: [0x80009c58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002394]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002398]:csrrs tp, fcsr, zero
	-[0x8000239c]:fsw ft11, 72(ra)
Current Store : [0x800023a0] : sw tp, 76(ra) -- Store: [0x80009c60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800023d8]:csrrs tp, fcsr, zero
	-[0x800023dc]:fsw ft11, 80(ra)
Current Store : [0x800023e0] : sw tp, 84(ra) -- Store: [0x80009c68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002414]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002418]:csrrs tp, fcsr, zero
	-[0x8000241c]:fsw ft11, 88(ra)
Current Store : [0x80002420] : sw tp, 92(ra) -- Store: [0x80009c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002454]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002458]:csrrs tp, fcsr, zero
	-[0x8000245c]:fsw ft11, 96(ra)
Current Store : [0x80002460] : sw tp, 100(ra) -- Store: [0x80009c78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002494]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002498]:csrrs tp, fcsr, zero
	-[0x8000249c]:fsw ft11, 104(ra)
Current Store : [0x800024a0] : sw tp, 108(ra) -- Store: [0x80009c80]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800024d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800024d8]:csrrs tp, fcsr, zero
	-[0x800024dc]:fsw ft11, 112(ra)
Current Store : [0x800024e0] : sw tp, 116(ra) -- Store: [0x80009c88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002514]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002518]:csrrs tp, fcsr, zero
	-[0x8000251c]:fsw ft11, 120(ra)
Current Store : [0x80002520] : sw tp, 124(ra) -- Store: [0x80009c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002554]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002558]:csrrs tp, fcsr, zero
	-[0x8000255c]:fsw ft11, 128(ra)
Current Store : [0x80002560] : sw tp, 132(ra) -- Store: [0x80009c98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002594]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002598]:csrrs tp, fcsr, zero
	-[0x8000259c]:fsw ft11, 136(ra)
Current Store : [0x800025a0] : sw tp, 140(ra) -- Store: [0x80009ca0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800025d8]:csrrs tp, fcsr, zero
	-[0x800025dc]:fsw ft11, 144(ra)
Current Store : [0x800025e0] : sw tp, 148(ra) -- Store: [0x80009ca8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002614]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002618]:csrrs tp, fcsr, zero
	-[0x8000261c]:fsw ft11, 152(ra)
Current Store : [0x80002620] : sw tp, 156(ra) -- Store: [0x80009cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002654]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002658]:csrrs tp, fcsr, zero
	-[0x8000265c]:fsw ft11, 160(ra)
Current Store : [0x80002660] : sw tp, 164(ra) -- Store: [0x80009cb8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002694]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002698]:csrrs tp, fcsr, zero
	-[0x8000269c]:fsw ft11, 168(ra)
Current Store : [0x800026a0] : sw tp, 172(ra) -- Store: [0x80009cc0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800026d8]:csrrs tp, fcsr, zero
	-[0x800026dc]:fsw ft11, 176(ra)
Current Store : [0x800026e0] : sw tp, 180(ra) -- Store: [0x80009cc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002714]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002718]:csrrs tp, fcsr, zero
	-[0x8000271c]:fsw ft11, 184(ra)
Current Store : [0x80002720] : sw tp, 188(ra) -- Store: [0x80009cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002754]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002758]:csrrs tp, fcsr, zero
	-[0x8000275c]:fsw ft11, 192(ra)
Current Store : [0x80002760] : sw tp, 196(ra) -- Store: [0x80009cd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002794]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002798]:csrrs tp, fcsr, zero
	-[0x8000279c]:fsw ft11, 200(ra)
Current Store : [0x800027a0] : sw tp, 204(ra) -- Store: [0x80009ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800027d8]:csrrs tp, fcsr, zero
	-[0x800027dc]:fsw ft11, 208(ra)
Current Store : [0x800027e0] : sw tp, 212(ra) -- Store: [0x80009ce8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002814]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002818]:csrrs tp, fcsr, zero
	-[0x8000281c]:fsw ft11, 216(ra)
Current Store : [0x80002820] : sw tp, 220(ra) -- Store: [0x80009cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002854]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002858]:csrrs tp, fcsr, zero
	-[0x8000285c]:fsw ft11, 224(ra)
Current Store : [0x80002860] : sw tp, 228(ra) -- Store: [0x80009cf8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002894]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002898]:csrrs tp, fcsr, zero
	-[0x8000289c]:fsw ft11, 232(ra)
Current Store : [0x800028a0] : sw tp, 236(ra) -- Store: [0x80009d00]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800028d8]:csrrs tp, fcsr, zero
	-[0x800028dc]:fsw ft11, 240(ra)
Current Store : [0x800028e0] : sw tp, 244(ra) -- Store: [0x80009d08]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002914]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002918]:csrrs tp, fcsr, zero
	-[0x8000291c]:fsw ft11, 248(ra)
Current Store : [0x80002920] : sw tp, 252(ra) -- Store: [0x80009d10]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002954]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002958]:csrrs tp, fcsr, zero
	-[0x8000295c]:fsw ft11, 256(ra)
Current Store : [0x80002960] : sw tp, 260(ra) -- Store: [0x80009d18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002994]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002998]:csrrs tp, fcsr, zero
	-[0x8000299c]:fsw ft11, 264(ra)
Current Store : [0x800029a0] : sw tp, 268(ra) -- Store: [0x80009d20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800029d8]:csrrs tp, fcsr, zero
	-[0x800029dc]:fsw ft11, 272(ra)
Current Store : [0x800029e0] : sw tp, 276(ra) -- Store: [0x80009d28]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002a18]:csrrs tp, fcsr, zero
	-[0x80002a1c]:fsw ft11, 280(ra)
Current Store : [0x80002a20] : sw tp, 284(ra) -- Store: [0x80009d30]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002a58]:csrrs tp, fcsr, zero
	-[0x80002a5c]:fsw ft11, 288(ra)
Current Store : [0x80002a60] : sw tp, 292(ra) -- Store: [0x80009d38]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002a98]:csrrs tp, fcsr, zero
	-[0x80002a9c]:fsw ft11, 296(ra)
Current Store : [0x80002aa0] : sw tp, 300(ra) -- Store: [0x80009d40]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ad4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002ad8]:csrrs tp, fcsr, zero
	-[0x80002adc]:fsw ft11, 304(ra)
Current Store : [0x80002ae0] : sw tp, 308(ra) -- Store: [0x80009d48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002b18]:csrrs tp, fcsr, zero
	-[0x80002b1c]:fsw ft11, 312(ra)
Current Store : [0x80002b20] : sw tp, 316(ra) -- Store: [0x80009d50]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002b58]:csrrs tp, fcsr, zero
	-[0x80002b5c]:fsw ft11, 320(ra)
Current Store : [0x80002b60] : sw tp, 324(ra) -- Store: [0x80009d58]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002b98]:csrrs tp, fcsr, zero
	-[0x80002b9c]:fsw ft11, 328(ra)
Current Store : [0x80002ba0] : sw tp, 332(ra) -- Store: [0x80009d60]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002bd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002bd8]:csrrs tp, fcsr, zero
	-[0x80002bdc]:fsw ft11, 336(ra)
Current Store : [0x80002be0] : sw tp, 340(ra) -- Store: [0x80009d68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002c18]:csrrs tp, fcsr, zero
	-[0x80002c1c]:fsw ft11, 344(ra)
Current Store : [0x80002c20] : sw tp, 348(ra) -- Store: [0x80009d70]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002c58]:csrrs tp, fcsr, zero
	-[0x80002c5c]:fsw ft11, 352(ra)
Current Store : [0x80002c60] : sw tp, 356(ra) -- Store: [0x80009d78]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002c98]:csrrs tp, fcsr, zero
	-[0x80002c9c]:fsw ft11, 360(ra)
Current Store : [0x80002ca0] : sw tp, 364(ra) -- Store: [0x80009d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002cd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002cd8]:csrrs tp, fcsr, zero
	-[0x80002cdc]:fsw ft11, 368(ra)
Current Store : [0x80002ce0] : sw tp, 372(ra) -- Store: [0x80009d88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002d18]:csrrs tp, fcsr, zero
	-[0x80002d1c]:fsw ft11, 376(ra)
Current Store : [0x80002d20] : sw tp, 380(ra) -- Store: [0x80009d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002d58]:csrrs tp, fcsr, zero
	-[0x80002d5c]:fsw ft11, 384(ra)
Current Store : [0x80002d60] : sw tp, 388(ra) -- Store: [0x80009d98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002d98]:csrrs tp, fcsr, zero
	-[0x80002d9c]:fsw ft11, 392(ra)
Current Store : [0x80002da0] : sw tp, 396(ra) -- Store: [0x80009da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002dd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002dd8]:csrrs tp, fcsr, zero
	-[0x80002ddc]:fsw ft11, 400(ra)
Current Store : [0x80002de0] : sw tp, 404(ra) -- Store: [0x80009da8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002e18]:csrrs tp, fcsr, zero
	-[0x80002e1c]:fsw ft11, 408(ra)
Current Store : [0x80002e20] : sw tp, 412(ra) -- Store: [0x80009db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002e58]:csrrs tp, fcsr, zero
	-[0x80002e5c]:fsw ft11, 416(ra)
Current Store : [0x80002e60] : sw tp, 420(ra) -- Store: [0x80009db8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002e98]:csrrs tp, fcsr, zero
	-[0x80002e9c]:fsw ft11, 424(ra)
Current Store : [0x80002ea0] : sw tp, 428(ra) -- Store: [0x80009dc0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ed4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002ed8]:csrrs tp, fcsr, zero
	-[0x80002edc]:fsw ft11, 432(ra)
Current Store : [0x80002ee0] : sw tp, 436(ra) -- Store: [0x80009dc8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002f18]:csrrs tp, fcsr, zero
	-[0x80002f1c]:fsw ft11, 440(ra)
Current Store : [0x80002f20] : sw tp, 444(ra) -- Store: [0x80009dd0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002f58]:csrrs tp, fcsr, zero
	-[0x80002f5c]:fsw ft11, 448(ra)
Current Store : [0x80002f60] : sw tp, 452(ra) -- Store: [0x80009dd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002f98]:csrrs tp, fcsr, zero
	-[0x80002f9c]:fsw ft11, 456(ra)
Current Store : [0x80002fa0] : sw tp, 460(ra) -- Store: [0x80009de0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80002fd8]:csrrs tp, fcsr, zero
	-[0x80002fdc]:fsw ft11, 464(ra)
Current Store : [0x80002fe0] : sw tp, 468(ra) -- Store: [0x80009de8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003014]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003018]:csrrs tp, fcsr, zero
	-[0x8000301c]:fsw ft11, 472(ra)
Current Store : [0x80003020] : sw tp, 476(ra) -- Store: [0x80009df0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003054]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003058]:csrrs tp, fcsr, zero
	-[0x8000305c]:fsw ft11, 480(ra)
Current Store : [0x80003060] : sw tp, 484(ra) -- Store: [0x80009df8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003094]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003098]:csrrs tp, fcsr, zero
	-[0x8000309c]:fsw ft11, 488(ra)
Current Store : [0x800030a0] : sw tp, 492(ra) -- Store: [0x80009e00]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800030d8]:csrrs tp, fcsr, zero
	-[0x800030dc]:fsw ft11, 496(ra)
Current Store : [0x800030e0] : sw tp, 500(ra) -- Store: [0x80009e08]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003114]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003118]:csrrs tp, fcsr, zero
	-[0x8000311c]:fsw ft11, 504(ra)
Current Store : [0x80003120] : sw tp, 508(ra) -- Store: [0x80009e10]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003154]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003158]:csrrs tp, fcsr, zero
	-[0x8000315c]:fsw ft11, 512(ra)
Current Store : [0x80003160] : sw tp, 516(ra) -- Store: [0x80009e18]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003194]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003198]:csrrs tp, fcsr, zero
	-[0x8000319c]:fsw ft11, 520(ra)
Current Store : [0x800031a0] : sw tp, 524(ra) -- Store: [0x80009e20]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800031d8]:csrrs tp, fcsr, zero
	-[0x800031dc]:fsw ft11, 528(ra)
Current Store : [0x800031e0] : sw tp, 532(ra) -- Store: [0x80009e28]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003214]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003218]:csrrs tp, fcsr, zero
	-[0x8000321c]:fsw ft11, 536(ra)
Current Store : [0x80003220] : sw tp, 540(ra) -- Store: [0x80009e30]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003254]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003258]:csrrs tp, fcsr, zero
	-[0x8000325c]:fsw ft11, 544(ra)
Current Store : [0x80003260] : sw tp, 548(ra) -- Store: [0x80009e38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003294]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003298]:csrrs tp, fcsr, zero
	-[0x8000329c]:fsw ft11, 552(ra)
Current Store : [0x800032a0] : sw tp, 556(ra) -- Store: [0x80009e40]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800032d8]:csrrs tp, fcsr, zero
	-[0x800032dc]:fsw ft11, 560(ra)
Current Store : [0x800032e0] : sw tp, 564(ra) -- Store: [0x80009e48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003314]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003318]:csrrs tp, fcsr, zero
	-[0x8000331c]:fsw ft11, 568(ra)
Current Store : [0x80003320] : sw tp, 572(ra) -- Store: [0x80009e50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003354]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003358]:csrrs tp, fcsr, zero
	-[0x8000335c]:fsw ft11, 576(ra)
Current Store : [0x80003360] : sw tp, 580(ra) -- Store: [0x80009e58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003394]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003398]:csrrs tp, fcsr, zero
	-[0x8000339c]:fsw ft11, 584(ra)
Current Store : [0x800033a0] : sw tp, 588(ra) -- Store: [0x80009e60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800033d8]:csrrs tp, fcsr, zero
	-[0x800033dc]:fsw ft11, 592(ra)
Current Store : [0x800033e0] : sw tp, 596(ra) -- Store: [0x80009e68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003414]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003418]:csrrs tp, fcsr, zero
	-[0x8000341c]:fsw ft11, 600(ra)
Current Store : [0x80003420] : sw tp, 604(ra) -- Store: [0x80009e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003454]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003458]:csrrs tp, fcsr, zero
	-[0x8000345c]:fsw ft11, 608(ra)
Current Store : [0x80003460] : sw tp, 612(ra) -- Store: [0x80009e78]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003494]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003498]:csrrs tp, fcsr, zero
	-[0x8000349c]:fsw ft11, 616(ra)
Current Store : [0x800034a0] : sw tp, 620(ra) -- Store: [0x80009e80]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800034d8]:csrrs tp, fcsr, zero
	-[0x800034dc]:fsw ft11, 624(ra)
Current Store : [0x800034e0] : sw tp, 628(ra) -- Store: [0x80009e88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003514]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003518]:csrrs tp, fcsr, zero
	-[0x8000351c]:fsw ft11, 632(ra)
Current Store : [0x80003520] : sw tp, 636(ra) -- Store: [0x80009e90]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003554]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003558]:csrrs tp, fcsr, zero
	-[0x8000355c]:fsw ft11, 640(ra)
Current Store : [0x80003560] : sw tp, 644(ra) -- Store: [0x80009e98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003594]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003598]:csrrs tp, fcsr, zero
	-[0x8000359c]:fsw ft11, 648(ra)
Current Store : [0x800035a0] : sw tp, 652(ra) -- Store: [0x80009ea0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800035d8]:csrrs tp, fcsr, zero
	-[0x800035dc]:fsw ft11, 656(ra)
Current Store : [0x800035e0] : sw tp, 660(ra) -- Store: [0x80009ea8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003614]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003618]:csrrs tp, fcsr, zero
	-[0x8000361c]:fsw ft11, 664(ra)
Current Store : [0x80003620] : sw tp, 668(ra) -- Store: [0x80009eb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003654]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003658]:csrrs tp, fcsr, zero
	-[0x8000365c]:fsw ft11, 672(ra)
Current Store : [0x80003660] : sw tp, 676(ra) -- Store: [0x80009eb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003694]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003698]:csrrs tp, fcsr, zero
	-[0x8000369c]:fsw ft11, 680(ra)
Current Store : [0x800036a0] : sw tp, 684(ra) -- Store: [0x80009ec0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800036d8]:csrrs tp, fcsr, zero
	-[0x800036dc]:fsw ft11, 688(ra)
Current Store : [0x800036e0] : sw tp, 692(ra) -- Store: [0x80009ec8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003714]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003718]:csrrs tp, fcsr, zero
	-[0x8000371c]:fsw ft11, 696(ra)
Current Store : [0x80003720] : sw tp, 700(ra) -- Store: [0x80009ed0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003754]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003758]:csrrs tp, fcsr, zero
	-[0x8000375c]:fsw ft11, 704(ra)
Current Store : [0x80003760] : sw tp, 708(ra) -- Store: [0x80009ed8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003794]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003798]:csrrs tp, fcsr, zero
	-[0x8000379c]:fsw ft11, 712(ra)
Current Store : [0x800037a0] : sw tp, 716(ra) -- Store: [0x80009ee0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800037d8]:csrrs tp, fcsr, zero
	-[0x800037dc]:fsw ft11, 720(ra)
Current Store : [0x800037e0] : sw tp, 724(ra) -- Store: [0x80009ee8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003814]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003818]:csrrs tp, fcsr, zero
	-[0x8000381c]:fsw ft11, 728(ra)
Current Store : [0x80003820] : sw tp, 732(ra) -- Store: [0x80009ef0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003854]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003858]:csrrs tp, fcsr, zero
	-[0x8000385c]:fsw ft11, 736(ra)
Current Store : [0x80003860] : sw tp, 740(ra) -- Store: [0x80009ef8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003894]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003898]:csrrs tp, fcsr, zero
	-[0x8000389c]:fsw ft11, 744(ra)
Current Store : [0x800038a0] : sw tp, 748(ra) -- Store: [0x80009f00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800038d8]:csrrs tp, fcsr, zero
	-[0x800038dc]:fsw ft11, 752(ra)
Current Store : [0x800038e0] : sw tp, 756(ra) -- Store: [0x80009f08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003914]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003918]:csrrs tp, fcsr, zero
	-[0x8000391c]:fsw ft11, 760(ra)
Current Store : [0x80003920] : sw tp, 764(ra) -- Store: [0x80009f10]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003954]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003958]:csrrs tp, fcsr, zero
	-[0x8000395c]:fsw ft11, 768(ra)
Current Store : [0x80003960] : sw tp, 772(ra) -- Store: [0x80009f18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003994]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003998]:csrrs tp, fcsr, zero
	-[0x8000399c]:fsw ft11, 776(ra)
Current Store : [0x800039a0] : sw tp, 780(ra) -- Store: [0x80009f20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800039d8]:csrrs tp, fcsr, zero
	-[0x800039dc]:fsw ft11, 784(ra)
Current Store : [0x800039e0] : sw tp, 788(ra) -- Store: [0x80009f28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003a18]:csrrs tp, fcsr, zero
	-[0x80003a1c]:fsw ft11, 792(ra)
Current Store : [0x80003a20] : sw tp, 796(ra) -- Store: [0x80009f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003a58]:csrrs tp, fcsr, zero
	-[0x80003a5c]:fsw ft11, 800(ra)
Current Store : [0x80003a60] : sw tp, 804(ra) -- Store: [0x80009f38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003a98]:csrrs tp, fcsr, zero
	-[0x80003a9c]:fsw ft11, 808(ra)
Current Store : [0x80003aa0] : sw tp, 812(ra) -- Store: [0x80009f40]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ad4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003ad8]:csrrs tp, fcsr, zero
	-[0x80003adc]:fsw ft11, 816(ra)
Current Store : [0x80003ae0] : sw tp, 820(ra) -- Store: [0x80009f48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003b18]:csrrs tp, fcsr, zero
	-[0x80003b1c]:fsw ft11, 824(ra)
Current Store : [0x80003b20] : sw tp, 828(ra) -- Store: [0x80009f50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003b58]:csrrs tp, fcsr, zero
	-[0x80003b5c]:fsw ft11, 832(ra)
Current Store : [0x80003b60] : sw tp, 836(ra) -- Store: [0x80009f58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003b98]:csrrs tp, fcsr, zero
	-[0x80003b9c]:fsw ft11, 840(ra)
Current Store : [0x80003ba0] : sw tp, 844(ra) -- Store: [0x80009f60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003bd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003bd8]:csrrs tp, fcsr, zero
	-[0x80003bdc]:fsw ft11, 848(ra)
Current Store : [0x80003be0] : sw tp, 852(ra) -- Store: [0x80009f68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003c18]:csrrs tp, fcsr, zero
	-[0x80003c1c]:fsw ft11, 856(ra)
Current Store : [0x80003c20] : sw tp, 860(ra) -- Store: [0x80009f70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003c58]:csrrs tp, fcsr, zero
	-[0x80003c5c]:fsw ft11, 864(ra)
Current Store : [0x80003c60] : sw tp, 868(ra) -- Store: [0x80009f78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003c98]:csrrs tp, fcsr, zero
	-[0x80003c9c]:fsw ft11, 872(ra)
Current Store : [0x80003ca0] : sw tp, 876(ra) -- Store: [0x80009f80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003cd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003cd8]:csrrs tp, fcsr, zero
	-[0x80003cdc]:fsw ft11, 880(ra)
Current Store : [0x80003ce0] : sw tp, 884(ra) -- Store: [0x80009f88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003d18]:csrrs tp, fcsr, zero
	-[0x80003d1c]:fsw ft11, 888(ra)
Current Store : [0x80003d20] : sw tp, 892(ra) -- Store: [0x80009f90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003d58]:csrrs tp, fcsr, zero
	-[0x80003d5c]:fsw ft11, 896(ra)
Current Store : [0x80003d60] : sw tp, 900(ra) -- Store: [0x80009f98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003d98]:csrrs tp, fcsr, zero
	-[0x80003d9c]:fsw ft11, 904(ra)
Current Store : [0x80003da0] : sw tp, 908(ra) -- Store: [0x80009fa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003dd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003dd8]:csrrs tp, fcsr, zero
	-[0x80003ddc]:fsw ft11, 912(ra)
Current Store : [0x80003de0] : sw tp, 916(ra) -- Store: [0x80009fa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003e18]:csrrs tp, fcsr, zero
	-[0x80003e1c]:fsw ft11, 920(ra)
Current Store : [0x80003e20] : sw tp, 924(ra) -- Store: [0x80009fb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003e58]:csrrs tp, fcsr, zero
	-[0x80003e5c]:fsw ft11, 928(ra)
Current Store : [0x80003e60] : sw tp, 932(ra) -- Store: [0x80009fb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003e98]:csrrs tp, fcsr, zero
	-[0x80003e9c]:fsw ft11, 936(ra)
Current Store : [0x80003ea0] : sw tp, 940(ra) -- Store: [0x80009fc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ed4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003ed8]:csrrs tp, fcsr, zero
	-[0x80003edc]:fsw ft11, 944(ra)
Current Store : [0x80003ee0] : sw tp, 948(ra) -- Store: [0x80009fc8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003f18]:csrrs tp, fcsr, zero
	-[0x80003f1c]:fsw ft11, 952(ra)
Current Store : [0x80003f20] : sw tp, 956(ra) -- Store: [0x80009fd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003f58]:csrrs tp, fcsr, zero
	-[0x80003f5c]:fsw ft11, 960(ra)
Current Store : [0x80003f60] : sw tp, 964(ra) -- Store: [0x80009fd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003f98]:csrrs tp, fcsr, zero
	-[0x80003f9c]:fsw ft11, 968(ra)
Current Store : [0x80003fa0] : sw tp, 972(ra) -- Store: [0x80009fe0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80003fd8]:csrrs tp, fcsr, zero
	-[0x80003fdc]:fsw ft11, 976(ra)
Current Store : [0x80003fe0] : sw tp, 980(ra) -- Store: [0x80009fe8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004014]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004018]:csrrs tp, fcsr, zero
	-[0x8000401c]:fsw ft11, 984(ra)
Current Store : [0x80004020] : sw tp, 988(ra) -- Store: [0x80009ff0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004054]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004058]:csrrs tp, fcsr, zero
	-[0x8000405c]:fsw ft11, 992(ra)
Current Store : [0x80004060] : sw tp, 996(ra) -- Store: [0x80009ff8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004094]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004098]:csrrs tp, fcsr, zero
	-[0x8000409c]:fsw ft11, 1000(ra)
Current Store : [0x800040a0] : sw tp, 1004(ra) -- Store: [0x8000a000]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800040d8]:csrrs tp, fcsr, zero
	-[0x800040dc]:fsw ft11, 1008(ra)
Current Store : [0x800040e0] : sw tp, 1012(ra) -- Store: [0x8000a008]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004114]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004118]:csrrs tp, fcsr, zero
	-[0x8000411c]:fsw ft11, 1016(ra)
Current Store : [0x80004120] : sw tp, 1020(ra) -- Store: [0x8000a010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000415c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004160]:csrrs tp, fcsr, zero
	-[0x80004164]:fsw ft11, 0(ra)
Current Store : [0x80004168] : sw tp, 4(ra) -- Store: [0x8000a018]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000419c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800041a0]:csrrs tp, fcsr, zero
	-[0x800041a4]:fsw ft11, 8(ra)
Current Store : [0x800041a8] : sw tp, 12(ra) -- Store: [0x8000a020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800041e0]:csrrs tp, fcsr, zero
	-[0x800041e4]:fsw ft11, 16(ra)
Current Store : [0x800041e8] : sw tp, 20(ra) -- Store: [0x8000a028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000421c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004220]:csrrs tp, fcsr, zero
	-[0x80004224]:fsw ft11, 24(ra)
Current Store : [0x80004228] : sw tp, 28(ra) -- Store: [0x8000a030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000425c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004260]:csrrs tp, fcsr, zero
	-[0x80004264]:fsw ft11, 32(ra)
Current Store : [0x80004268] : sw tp, 36(ra) -- Store: [0x8000a038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000429c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800042a0]:csrrs tp, fcsr, zero
	-[0x800042a4]:fsw ft11, 40(ra)
Current Store : [0x800042a8] : sw tp, 44(ra) -- Store: [0x8000a040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800042e0]:csrrs tp, fcsr, zero
	-[0x800042e4]:fsw ft11, 48(ra)
Current Store : [0x800042e8] : sw tp, 52(ra) -- Store: [0x8000a048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000431c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004320]:csrrs tp, fcsr, zero
	-[0x80004324]:fsw ft11, 56(ra)
Current Store : [0x80004328] : sw tp, 60(ra) -- Store: [0x8000a050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000435c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004360]:csrrs tp, fcsr, zero
	-[0x80004364]:fsw ft11, 64(ra)
Current Store : [0x80004368] : sw tp, 68(ra) -- Store: [0x8000a058]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000439c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800043a0]:csrrs tp, fcsr, zero
	-[0x800043a4]:fsw ft11, 72(ra)
Current Store : [0x800043a8] : sw tp, 76(ra) -- Store: [0x8000a060]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800043e0]:csrrs tp, fcsr, zero
	-[0x800043e4]:fsw ft11, 80(ra)
Current Store : [0x800043e8] : sw tp, 84(ra) -- Store: [0x8000a068]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000441c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004420]:csrrs tp, fcsr, zero
	-[0x80004424]:fsw ft11, 88(ra)
Current Store : [0x80004428] : sw tp, 92(ra) -- Store: [0x8000a070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000445c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004460]:csrrs tp, fcsr, zero
	-[0x80004464]:fsw ft11, 96(ra)
Current Store : [0x80004468] : sw tp, 100(ra) -- Store: [0x8000a078]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000449c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800044a0]:csrrs tp, fcsr, zero
	-[0x800044a4]:fsw ft11, 104(ra)
Current Store : [0x800044a8] : sw tp, 108(ra) -- Store: [0x8000a080]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800044e0]:csrrs tp, fcsr, zero
	-[0x800044e4]:fsw ft11, 112(ra)
Current Store : [0x800044e8] : sw tp, 116(ra) -- Store: [0x8000a088]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000451c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004520]:csrrs tp, fcsr, zero
	-[0x80004524]:fsw ft11, 120(ra)
Current Store : [0x80004528] : sw tp, 124(ra) -- Store: [0x8000a090]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000455c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004560]:csrrs tp, fcsr, zero
	-[0x80004564]:fsw ft11, 128(ra)
Current Store : [0x80004568] : sw tp, 132(ra) -- Store: [0x8000a098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000459c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800045a0]:csrrs tp, fcsr, zero
	-[0x800045a4]:fsw ft11, 136(ra)
Current Store : [0x800045a8] : sw tp, 140(ra) -- Store: [0x8000a0a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800045e0]:csrrs tp, fcsr, zero
	-[0x800045e4]:fsw ft11, 144(ra)
Current Store : [0x800045e8] : sw tp, 148(ra) -- Store: [0x8000a0a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000461c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004620]:csrrs tp, fcsr, zero
	-[0x80004624]:fsw ft11, 152(ra)
Current Store : [0x80004628] : sw tp, 156(ra) -- Store: [0x8000a0b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000465c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004660]:csrrs tp, fcsr, zero
	-[0x80004664]:fsw ft11, 160(ra)
Current Store : [0x80004668] : sw tp, 164(ra) -- Store: [0x8000a0b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000469c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800046a0]:csrrs tp, fcsr, zero
	-[0x800046a4]:fsw ft11, 168(ra)
Current Store : [0x800046a8] : sw tp, 172(ra) -- Store: [0x8000a0c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800046e0]:csrrs tp, fcsr, zero
	-[0x800046e4]:fsw ft11, 176(ra)
Current Store : [0x800046e8] : sw tp, 180(ra) -- Store: [0x8000a0c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000471c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004720]:csrrs tp, fcsr, zero
	-[0x80004724]:fsw ft11, 184(ra)
Current Store : [0x80004728] : sw tp, 188(ra) -- Store: [0x8000a0d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000475c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004760]:csrrs tp, fcsr, zero
	-[0x80004764]:fsw ft11, 192(ra)
Current Store : [0x80004768] : sw tp, 196(ra) -- Store: [0x8000a0d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000479c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800047a0]:csrrs tp, fcsr, zero
	-[0x800047a4]:fsw ft11, 200(ra)
Current Store : [0x800047a8] : sw tp, 204(ra) -- Store: [0x8000a0e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800047e0]:csrrs tp, fcsr, zero
	-[0x800047e4]:fsw ft11, 208(ra)
Current Store : [0x800047e8] : sw tp, 212(ra) -- Store: [0x8000a0e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000481c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004820]:csrrs tp, fcsr, zero
	-[0x80004824]:fsw ft11, 216(ra)
Current Store : [0x80004828] : sw tp, 220(ra) -- Store: [0x8000a0f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000485c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004860]:csrrs tp, fcsr, zero
	-[0x80004864]:fsw ft11, 224(ra)
Current Store : [0x80004868] : sw tp, 228(ra) -- Store: [0x8000a0f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000489c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800048a0]:csrrs tp, fcsr, zero
	-[0x800048a4]:fsw ft11, 232(ra)
Current Store : [0x800048a8] : sw tp, 236(ra) -- Store: [0x8000a100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800048e0]:csrrs tp, fcsr, zero
	-[0x800048e4]:fsw ft11, 240(ra)
Current Store : [0x800048e8] : sw tp, 244(ra) -- Store: [0x8000a108]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000491c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004920]:csrrs tp, fcsr, zero
	-[0x80004924]:fsw ft11, 248(ra)
Current Store : [0x80004928] : sw tp, 252(ra) -- Store: [0x8000a110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000495c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004960]:csrrs tp, fcsr, zero
	-[0x80004964]:fsw ft11, 256(ra)
Current Store : [0x80004968] : sw tp, 260(ra) -- Store: [0x8000a118]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000499c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800049a0]:csrrs tp, fcsr, zero
	-[0x800049a4]:fsw ft11, 264(ra)
Current Store : [0x800049a8] : sw tp, 268(ra) -- Store: [0x8000a120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800049e0]:csrrs tp, fcsr, zero
	-[0x800049e4]:fsw ft11, 272(ra)
Current Store : [0x800049e8] : sw tp, 276(ra) -- Store: [0x8000a128]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004a20]:csrrs tp, fcsr, zero
	-[0x80004a24]:fsw ft11, 280(ra)
Current Store : [0x80004a28] : sw tp, 284(ra) -- Store: [0x8000a130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004a60]:csrrs tp, fcsr, zero
	-[0x80004a64]:fsw ft11, 288(ra)
Current Store : [0x80004a68] : sw tp, 292(ra) -- Store: [0x8000a138]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004aa0]:csrrs tp, fcsr, zero
	-[0x80004aa4]:fsw ft11, 296(ra)
Current Store : [0x80004aa8] : sw tp, 300(ra) -- Store: [0x8000a140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004adc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ae0]:csrrs tp, fcsr, zero
	-[0x80004ae4]:fsw ft11, 304(ra)
Current Store : [0x80004ae8] : sw tp, 308(ra) -- Store: [0x8000a148]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004b20]:csrrs tp, fcsr, zero
	-[0x80004b24]:fsw ft11, 312(ra)
Current Store : [0x80004b28] : sw tp, 316(ra) -- Store: [0x8000a150]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004b60]:csrrs tp, fcsr, zero
	-[0x80004b64]:fsw ft11, 320(ra)
Current Store : [0x80004b68] : sw tp, 324(ra) -- Store: [0x8000a158]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ba0]:csrrs tp, fcsr, zero
	-[0x80004ba4]:fsw ft11, 328(ra)
Current Store : [0x80004ba8] : sw tp, 332(ra) -- Store: [0x8000a160]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004be0]:csrrs tp, fcsr, zero
	-[0x80004be4]:fsw ft11, 336(ra)
Current Store : [0x80004be8] : sw tp, 340(ra) -- Store: [0x8000a168]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004c20]:csrrs tp, fcsr, zero
	-[0x80004c24]:fsw ft11, 344(ra)
Current Store : [0x80004c28] : sw tp, 348(ra) -- Store: [0x8000a170]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004c60]:csrrs tp, fcsr, zero
	-[0x80004c64]:fsw ft11, 352(ra)
Current Store : [0x80004c68] : sw tp, 356(ra) -- Store: [0x8000a178]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ca0]:csrrs tp, fcsr, zero
	-[0x80004ca4]:fsw ft11, 360(ra)
Current Store : [0x80004ca8] : sw tp, 364(ra) -- Store: [0x8000a180]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004cdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ce0]:csrrs tp, fcsr, zero
	-[0x80004ce4]:fsw ft11, 368(ra)
Current Store : [0x80004ce8] : sw tp, 372(ra) -- Store: [0x8000a188]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004d20]:csrrs tp, fcsr, zero
	-[0x80004d24]:fsw ft11, 376(ra)
Current Store : [0x80004d28] : sw tp, 380(ra) -- Store: [0x8000a190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004d60]:csrrs tp, fcsr, zero
	-[0x80004d64]:fsw ft11, 384(ra)
Current Store : [0x80004d68] : sw tp, 388(ra) -- Store: [0x8000a198]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004da0]:csrrs tp, fcsr, zero
	-[0x80004da4]:fsw ft11, 392(ra)
Current Store : [0x80004da8] : sw tp, 396(ra) -- Store: [0x8000a1a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004ddc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004de0]:csrrs tp, fcsr, zero
	-[0x80004de4]:fsw ft11, 400(ra)
Current Store : [0x80004de8] : sw tp, 404(ra) -- Store: [0x8000a1a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004e20]:csrrs tp, fcsr, zero
	-[0x80004e24]:fsw ft11, 408(ra)
Current Store : [0x80004e28] : sw tp, 412(ra) -- Store: [0x8000a1b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004e60]:csrrs tp, fcsr, zero
	-[0x80004e64]:fsw ft11, 416(ra)
Current Store : [0x80004e68] : sw tp, 420(ra) -- Store: [0x8000a1b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ea0]:csrrs tp, fcsr, zero
	-[0x80004ea4]:fsw ft11, 424(ra)
Current Store : [0x80004ea8] : sw tp, 428(ra) -- Store: [0x8000a1c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004edc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004ee0]:csrrs tp, fcsr, zero
	-[0x80004ee4]:fsw ft11, 432(ra)
Current Store : [0x80004ee8] : sw tp, 436(ra) -- Store: [0x8000a1c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004f20]:csrrs tp, fcsr, zero
	-[0x80004f24]:fsw ft11, 440(ra)
Current Store : [0x80004f28] : sw tp, 444(ra) -- Store: [0x8000a1d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004f60]:csrrs tp, fcsr, zero
	-[0x80004f64]:fsw ft11, 448(ra)
Current Store : [0x80004f68] : sw tp, 452(ra) -- Store: [0x8000a1d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004fa0]:csrrs tp, fcsr, zero
	-[0x80004fa4]:fsw ft11, 456(ra)
Current Store : [0x80004fa8] : sw tp, 460(ra) -- Store: [0x8000a1e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80004fe0]:csrrs tp, fcsr, zero
	-[0x80004fe4]:fsw ft11, 464(ra)
Current Store : [0x80004fe8] : sw tp, 468(ra) -- Store: [0x8000a1e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000501c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005020]:csrrs tp, fcsr, zero
	-[0x80005024]:fsw ft11, 472(ra)
Current Store : [0x80005028] : sw tp, 476(ra) -- Store: [0x8000a1f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000505c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005060]:csrrs tp, fcsr, zero
	-[0x80005064]:fsw ft11, 480(ra)
Current Store : [0x80005068] : sw tp, 484(ra) -- Store: [0x8000a1f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000509c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800050a0]:csrrs tp, fcsr, zero
	-[0x800050a4]:fsw ft11, 488(ra)
Current Store : [0x800050a8] : sw tp, 492(ra) -- Store: [0x8000a200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800050e0]:csrrs tp, fcsr, zero
	-[0x800050e4]:fsw ft11, 496(ra)
Current Store : [0x800050e8] : sw tp, 500(ra) -- Store: [0x8000a208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000511c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005120]:csrrs tp, fcsr, zero
	-[0x80005124]:fsw ft11, 504(ra)
Current Store : [0x80005128] : sw tp, 508(ra) -- Store: [0x8000a210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000515c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005160]:csrrs tp, fcsr, zero
	-[0x80005164]:fsw ft11, 512(ra)
Current Store : [0x80005168] : sw tp, 516(ra) -- Store: [0x8000a218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000519c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800051a0]:csrrs tp, fcsr, zero
	-[0x800051a4]:fsw ft11, 520(ra)
Current Store : [0x800051a8] : sw tp, 524(ra) -- Store: [0x8000a220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800051e0]:csrrs tp, fcsr, zero
	-[0x800051e4]:fsw ft11, 528(ra)
Current Store : [0x800051e8] : sw tp, 532(ra) -- Store: [0x8000a228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000521c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005220]:csrrs tp, fcsr, zero
	-[0x80005224]:fsw ft11, 536(ra)
Current Store : [0x80005228] : sw tp, 540(ra) -- Store: [0x8000a230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000525c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005260]:csrrs tp, fcsr, zero
	-[0x80005264]:fsw ft11, 544(ra)
Current Store : [0x80005268] : sw tp, 548(ra) -- Store: [0x8000a238]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000529c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800052a0]:csrrs tp, fcsr, zero
	-[0x800052a4]:fsw ft11, 552(ra)
Current Store : [0x800052a8] : sw tp, 556(ra) -- Store: [0x8000a240]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800052e0]:csrrs tp, fcsr, zero
	-[0x800052e4]:fsw ft11, 560(ra)
Current Store : [0x800052e8] : sw tp, 564(ra) -- Store: [0x8000a248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000531c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005320]:csrrs tp, fcsr, zero
	-[0x80005324]:fsw ft11, 568(ra)
Current Store : [0x80005328] : sw tp, 572(ra) -- Store: [0x8000a250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000535c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005360]:csrrs tp, fcsr, zero
	-[0x80005364]:fsw ft11, 576(ra)
Current Store : [0x80005368] : sw tp, 580(ra) -- Store: [0x8000a258]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000539c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800053a0]:csrrs tp, fcsr, zero
	-[0x800053a4]:fsw ft11, 584(ra)
Current Store : [0x800053a8] : sw tp, 588(ra) -- Store: [0x8000a260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800053e0]:csrrs tp, fcsr, zero
	-[0x800053e4]:fsw ft11, 592(ra)
Current Store : [0x800053e8] : sw tp, 596(ra) -- Store: [0x8000a268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000541c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005420]:csrrs tp, fcsr, zero
	-[0x80005424]:fsw ft11, 600(ra)
Current Store : [0x80005428] : sw tp, 604(ra) -- Store: [0x8000a270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000545c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005460]:csrrs tp, fcsr, zero
	-[0x80005464]:fsw ft11, 608(ra)
Current Store : [0x80005468] : sw tp, 612(ra) -- Store: [0x8000a278]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000549c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800054a0]:csrrs tp, fcsr, zero
	-[0x800054a4]:fsw ft11, 616(ra)
Current Store : [0x800054a8] : sw tp, 620(ra) -- Store: [0x8000a280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800054e0]:csrrs tp, fcsr, zero
	-[0x800054e4]:fsw ft11, 624(ra)
Current Store : [0x800054e8] : sw tp, 628(ra) -- Store: [0x8000a288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000551c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005520]:csrrs tp, fcsr, zero
	-[0x80005524]:fsw ft11, 632(ra)
Current Store : [0x80005528] : sw tp, 636(ra) -- Store: [0x8000a290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000555c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005560]:csrrs tp, fcsr, zero
	-[0x80005564]:fsw ft11, 640(ra)
Current Store : [0x80005568] : sw tp, 644(ra) -- Store: [0x8000a298]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000559c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800055a0]:csrrs tp, fcsr, zero
	-[0x800055a4]:fsw ft11, 648(ra)
Current Store : [0x800055a8] : sw tp, 652(ra) -- Store: [0x8000a2a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800055e0]:csrrs tp, fcsr, zero
	-[0x800055e4]:fsw ft11, 656(ra)
Current Store : [0x800055e8] : sw tp, 660(ra) -- Store: [0x8000a2a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000561c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005620]:csrrs tp, fcsr, zero
	-[0x80005624]:fsw ft11, 664(ra)
Current Store : [0x80005628] : sw tp, 668(ra) -- Store: [0x8000a2b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000565c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005660]:csrrs tp, fcsr, zero
	-[0x80005664]:fsw ft11, 672(ra)
Current Store : [0x80005668] : sw tp, 676(ra) -- Store: [0x8000a2b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000569c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800056a0]:csrrs tp, fcsr, zero
	-[0x800056a4]:fsw ft11, 680(ra)
Current Store : [0x800056a8] : sw tp, 684(ra) -- Store: [0x8000a2c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800056e0]:csrrs tp, fcsr, zero
	-[0x800056e4]:fsw ft11, 688(ra)
Current Store : [0x800056e8] : sw tp, 692(ra) -- Store: [0x8000a2c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000571c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005720]:csrrs tp, fcsr, zero
	-[0x80005724]:fsw ft11, 696(ra)
Current Store : [0x80005728] : sw tp, 700(ra) -- Store: [0x8000a2d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000575c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005760]:csrrs tp, fcsr, zero
	-[0x80005764]:fsw ft11, 704(ra)
Current Store : [0x80005768] : sw tp, 708(ra) -- Store: [0x8000a2d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000579c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800057a0]:csrrs tp, fcsr, zero
	-[0x800057a4]:fsw ft11, 712(ra)
Current Store : [0x800057a8] : sw tp, 716(ra) -- Store: [0x8000a2e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800057e0]:csrrs tp, fcsr, zero
	-[0x800057e4]:fsw ft11, 720(ra)
Current Store : [0x800057e8] : sw tp, 724(ra) -- Store: [0x8000a2e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000581c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005820]:csrrs tp, fcsr, zero
	-[0x80005824]:fsw ft11, 728(ra)
Current Store : [0x80005828] : sw tp, 732(ra) -- Store: [0x8000a2f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000585c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005860]:csrrs tp, fcsr, zero
	-[0x80005864]:fsw ft11, 736(ra)
Current Store : [0x80005868] : sw tp, 740(ra) -- Store: [0x8000a2f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000589c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800058a0]:csrrs tp, fcsr, zero
	-[0x800058a4]:fsw ft11, 744(ra)
Current Store : [0x800058a8] : sw tp, 748(ra) -- Store: [0x8000a300]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800058e0]:csrrs tp, fcsr, zero
	-[0x800058e4]:fsw ft11, 752(ra)
Current Store : [0x800058e8] : sw tp, 756(ra) -- Store: [0x8000a308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000591c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005920]:csrrs tp, fcsr, zero
	-[0x80005924]:fsw ft11, 760(ra)
Current Store : [0x80005928] : sw tp, 764(ra) -- Store: [0x8000a310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000595c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005960]:csrrs tp, fcsr, zero
	-[0x80005964]:fsw ft11, 768(ra)
Current Store : [0x80005968] : sw tp, 772(ra) -- Store: [0x8000a318]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000599c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800059a0]:csrrs tp, fcsr, zero
	-[0x800059a4]:fsw ft11, 776(ra)
Current Store : [0x800059a8] : sw tp, 780(ra) -- Store: [0x8000a320]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800059e0]:csrrs tp, fcsr, zero
	-[0x800059e4]:fsw ft11, 784(ra)
Current Store : [0x800059e8] : sw tp, 788(ra) -- Store: [0x8000a328]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005a20]:csrrs tp, fcsr, zero
	-[0x80005a24]:fsw ft11, 792(ra)
Current Store : [0x80005a28] : sw tp, 796(ra) -- Store: [0x8000a330]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005a60]:csrrs tp, fcsr, zero
	-[0x80005a64]:fsw ft11, 800(ra)
Current Store : [0x80005a68] : sw tp, 804(ra) -- Store: [0x8000a338]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005aa0]:csrrs tp, fcsr, zero
	-[0x80005aa4]:fsw ft11, 808(ra)
Current Store : [0x80005aa8] : sw tp, 812(ra) -- Store: [0x8000a340]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005adc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ae0]:csrrs tp, fcsr, zero
	-[0x80005ae4]:fsw ft11, 816(ra)
Current Store : [0x80005ae8] : sw tp, 820(ra) -- Store: [0x8000a348]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005b20]:csrrs tp, fcsr, zero
	-[0x80005b24]:fsw ft11, 824(ra)
Current Store : [0x80005b28] : sw tp, 828(ra) -- Store: [0x8000a350]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005b60]:csrrs tp, fcsr, zero
	-[0x80005b64]:fsw ft11, 832(ra)
Current Store : [0x80005b68] : sw tp, 836(ra) -- Store: [0x8000a358]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ba0]:csrrs tp, fcsr, zero
	-[0x80005ba4]:fsw ft11, 840(ra)
Current Store : [0x80005ba8] : sw tp, 844(ra) -- Store: [0x8000a360]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005be0]:csrrs tp, fcsr, zero
	-[0x80005be4]:fsw ft11, 848(ra)
Current Store : [0x80005be8] : sw tp, 852(ra) -- Store: [0x8000a368]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005c20]:csrrs tp, fcsr, zero
	-[0x80005c24]:fsw ft11, 856(ra)
Current Store : [0x80005c28] : sw tp, 860(ra) -- Store: [0x8000a370]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005c60]:csrrs tp, fcsr, zero
	-[0x80005c64]:fsw ft11, 864(ra)
Current Store : [0x80005c68] : sw tp, 868(ra) -- Store: [0x8000a378]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ca0]:csrrs tp, fcsr, zero
	-[0x80005ca4]:fsw ft11, 872(ra)
Current Store : [0x80005ca8] : sw tp, 876(ra) -- Store: [0x8000a380]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005cdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ce0]:csrrs tp, fcsr, zero
	-[0x80005ce4]:fsw ft11, 880(ra)
Current Store : [0x80005ce8] : sw tp, 884(ra) -- Store: [0x8000a388]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005d20]:csrrs tp, fcsr, zero
	-[0x80005d24]:fsw ft11, 888(ra)
Current Store : [0x80005d28] : sw tp, 892(ra) -- Store: [0x8000a390]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005d60]:csrrs tp, fcsr, zero
	-[0x80005d64]:fsw ft11, 896(ra)
Current Store : [0x80005d68] : sw tp, 900(ra) -- Store: [0x8000a398]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005da0]:csrrs tp, fcsr, zero
	-[0x80005da4]:fsw ft11, 904(ra)
Current Store : [0x80005da8] : sw tp, 908(ra) -- Store: [0x8000a3a0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005ddc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005de0]:csrrs tp, fcsr, zero
	-[0x80005de4]:fsw ft11, 912(ra)
Current Store : [0x80005de8] : sw tp, 916(ra) -- Store: [0x8000a3a8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005e20]:csrrs tp, fcsr, zero
	-[0x80005e24]:fsw ft11, 920(ra)
Current Store : [0x80005e28] : sw tp, 924(ra) -- Store: [0x8000a3b0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005e60]:csrrs tp, fcsr, zero
	-[0x80005e64]:fsw ft11, 928(ra)
Current Store : [0x80005e68] : sw tp, 932(ra) -- Store: [0x8000a3b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ea0]:csrrs tp, fcsr, zero
	-[0x80005ea4]:fsw ft11, 936(ra)
Current Store : [0x80005ea8] : sw tp, 940(ra) -- Store: [0x8000a3c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005edc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005ee0]:csrrs tp, fcsr, zero
	-[0x80005ee4]:fsw ft11, 944(ra)
Current Store : [0x80005ee8] : sw tp, 948(ra) -- Store: [0x8000a3c8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005f20]:csrrs tp, fcsr, zero
	-[0x80005f24]:fsw ft11, 952(ra)
Current Store : [0x80005f28] : sw tp, 956(ra) -- Store: [0x8000a3d0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005f60]:csrrs tp, fcsr, zero
	-[0x80005f64]:fsw ft11, 960(ra)
Current Store : [0x80005f68] : sw tp, 964(ra) -- Store: [0x8000a3d8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005fa0]:csrrs tp, fcsr, zero
	-[0x80005fa4]:fsw ft11, 968(ra)
Current Store : [0x80005fa8] : sw tp, 972(ra) -- Store: [0x8000a3e0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80005fe0]:csrrs tp, fcsr, zero
	-[0x80005fe4]:fsw ft11, 976(ra)
Current Store : [0x80005fe8] : sw tp, 980(ra) -- Store: [0x8000a3e8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000601c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006020]:csrrs tp, fcsr, zero
	-[0x80006024]:fsw ft11, 984(ra)
Current Store : [0x80006028] : sw tp, 988(ra) -- Store: [0x8000a3f0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000605c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006060]:csrrs tp, fcsr, zero
	-[0x80006064]:fsw ft11, 992(ra)
Current Store : [0x80006068] : sw tp, 996(ra) -- Store: [0x8000a3f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000609c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800060a0]:csrrs tp, fcsr, zero
	-[0x800060a4]:fsw ft11, 1000(ra)
Current Store : [0x800060a8] : sw tp, 1004(ra) -- Store: [0x8000a400]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800060e0]:csrrs tp, fcsr, zero
	-[0x800060e4]:fsw ft11, 1008(ra)
Current Store : [0x800060e8] : sw tp, 1012(ra) -- Store: [0x8000a408]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000611c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006120]:csrrs tp, fcsr, zero
	-[0x80006124]:fsw ft11, 1016(ra)
Current Store : [0x80006128] : sw tp, 1020(ra) -- Store: [0x8000a410]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000615c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006160]:csrrs tp, fcsr, zero
	-[0x80006164]:fsw ft11, 0(ra)
Current Store : [0x80006168] : sw tp, 4(ra) -- Store: [0x8000a418]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006194]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006198]:csrrs tp, fcsr, zero
	-[0x8000619c]:fsw ft11, 8(ra)
Current Store : [0x800061a0] : sw tp, 12(ra) -- Store: [0x8000a420]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800061d0]:csrrs tp, fcsr, zero
	-[0x800061d4]:fsw ft11, 16(ra)
Current Store : [0x800061d8] : sw tp, 20(ra) -- Store: [0x8000a428]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006204]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006208]:csrrs tp, fcsr, zero
	-[0x8000620c]:fsw ft11, 24(ra)
Current Store : [0x80006210] : sw tp, 28(ra) -- Store: [0x8000a430]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000623c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006240]:csrrs tp, fcsr, zero
	-[0x80006244]:fsw ft11, 32(ra)
Current Store : [0x80006248] : sw tp, 36(ra) -- Store: [0x8000a438]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006274]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006278]:csrrs tp, fcsr, zero
	-[0x8000627c]:fsw ft11, 40(ra)
Current Store : [0x80006280] : sw tp, 44(ra) -- Store: [0x8000a440]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800062b0]:csrrs tp, fcsr, zero
	-[0x800062b4]:fsw ft11, 48(ra)
Current Store : [0x800062b8] : sw tp, 52(ra) -- Store: [0x8000a448]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800062e8]:csrrs tp, fcsr, zero
	-[0x800062ec]:fsw ft11, 56(ra)
Current Store : [0x800062f0] : sw tp, 60(ra) -- Store: [0x8000a450]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000631c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006320]:csrrs tp, fcsr, zero
	-[0x80006324]:fsw ft11, 64(ra)
Current Store : [0x80006328] : sw tp, 68(ra) -- Store: [0x8000a458]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006354]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006358]:csrrs tp, fcsr, zero
	-[0x8000635c]:fsw ft11, 72(ra)
Current Store : [0x80006360] : sw tp, 76(ra) -- Store: [0x8000a460]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000638c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006390]:csrrs tp, fcsr, zero
	-[0x80006394]:fsw ft11, 80(ra)
Current Store : [0x80006398] : sw tp, 84(ra) -- Store: [0x8000a468]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063c4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800063c8]:csrrs tp, fcsr, zero
	-[0x800063cc]:fsw ft11, 88(ra)
Current Store : [0x800063d0] : sw tp, 92(ra) -- Store: [0x8000a470]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063fc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006400]:csrrs tp, fcsr, zero
	-[0x80006404]:fsw ft11, 96(ra)
Current Store : [0x80006408] : sw tp, 100(ra) -- Store: [0x8000a478]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006434]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006438]:csrrs tp, fcsr, zero
	-[0x8000643c]:fsw ft11, 104(ra)
Current Store : [0x80006440] : sw tp, 108(ra) -- Store: [0x8000a480]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000646c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006470]:csrrs tp, fcsr, zero
	-[0x80006474]:fsw ft11, 112(ra)
Current Store : [0x80006478] : sw tp, 116(ra) -- Store: [0x8000a488]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064a4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800064a8]:csrrs tp, fcsr, zero
	-[0x800064ac]:fsw ft11, 120(ra)
Current Store : [0x800064b0] : sw tp, 124(ra) -- Store: [0x8000a490]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064dc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800064e0]:csrrs tp, fcsr, zero
	-[0x800064e4]:fsw ft11, 128(ra)
Current Store : [0x800064e8] : sw tp, 132(ra) -- Store: [0x8000a498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006514]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006518]:csrrs tp, fcsr, zero
	-[0x8000651c]:fsw ft11, 136(ra)
Current Store : [0x80006520] : sw tp, 140(ra) -- Store: [0x8000a4a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000654c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006550]:csrrs tp, fcsr, zero
	-[0x80006554]:fsw ft11, 144(ra)
Current Store : [0x80006558] : sw tp, 148(ra) -- Store: [0x8000a4a8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006584]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006588]:csrrs tp, fcsr, zero
	-[0x8000658c]:fsw ft11, 152(ra)
Current Store : [0x80006590] : sw tp, 156(ra) -- Store: [0x8000a4b0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065bc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800065c0]:csrrs tp, fcsr, zero
	-[0x800065c4]:fsw ft11, 160(ra)
Current Store : [0x800065c8] : sw tp, 164(ra) -- Store: [0x8000a4b8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065f4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800065f8]:csrrs tp, fcsr, zero
	-[0x800065fc]:fsw ft11, 168(ra)
Current Store : [0x80006600] : sw tp, 172(ra) -- Store: [0x8000a4c0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000662c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006630]:csrrs tp, fcsr, zero
	-[0x80006634]:fsw ft11, 176(ra)
Current Store : [0x80006638] : sw tp, 180(ra) -- Store: [0x8000a4c8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006664]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006668]:csrrs tp, fcsr, zero
	-[0x8000666c]:fsw ft11, 184(ra)
Current Store : [0x80006670] : sw tp, 188(ra) -- Store: [0x8000a4d0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000669c]:fadd.h ft11, ft10, ft9, dyn
	-[0x800066a0]:csrrs tp, fcsr, zero
	-[0x800066a4]:fsw ft11, 192(ra)
Current Store : [0x800066a8] : sw tp, 196(ra) -- Store: [0x8000a4d8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800066d4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800066d8]:csrrs tp, fcsr, zero
	-[0x800066dc]:fsw ft11, 200(ra)
Current Store : [0x800066e0] : sw tp, 204(ra) -- Store: [0x8000a4e0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000670c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006710]:csrrs tp, fcsr, zero
	-[0x80006714]:fsw ft11, 208(ra)
Current Store : [0x80006718] : sw tp, 212(ra) -- Store: [0x8000a4e8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006744]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006748]:csrrs tp, fcsr, zero
	-[0x8000674c]:fsw ft11, 216(ra)
Current Store : [0x80006750] : sw tp, 220(ra) -- Store: [0x8000a4f0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000677c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006780]:csrrs tp, fcsr, zero
	-[0x80006784]:fsw ft11, 224(ra)
Current Store : [0x80006788] : sw tp, 228(ra) -- Store: [0x8000a4f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067b4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800067b8]:csrrs tp, fcsr, zero
	-[0x800067bc]:fsw ft11, 232(ra)
Current Store : [0x800067c0] : sw tp, 236(ra) -- Store: [0x8000a500]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067ec]:fadd.h ft11, ft10, ft9, dyn
	-[0x800067f0]:csrrs tp, fcsr, zero
	-[0x800067f4]:fsw ft11, 240(ra)
Current Store : [0x800067f8] : sw tp, 244(ra) -- Store: [0x8000a508]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006824]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006828]:csrrs tp, fcsr, zero
	-[0x8000682c]:fsw ft11, 248(ra)
Current Store : [0x80006830] : sw tp, 252(ra) -- Store: [0x8000a510]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000685c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006860]:csrrs tp, fcsr, zero
	-[0x80006864]:fsw ft11, 256(ra)
Current Store : [0x80006868] : sw tp, 260(ra) -- Store: [0x8000a518]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006894]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006898]:csrrs tp, fcsr, zero
	-[0x8000689c]:fsw ft11, 264(ra)
Current Store : [0x800068a0] : sw tp, 268(ra) -- Store: [0x8000a520]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068cc]:fadd.h ft11, ft10, ft9, dyn
	-[0x800068d0]:csrrs tp, fcsr, zero
	-[0x800068d4]:fsw ft11, 272(ra)
Current Store : [0x800068d8] : sw tp, 276(ra) -- Store: [0x8000a528]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006904]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006908]:csrrs tp, fcsr, zero
	-[0x8000690c]:fsw ft11, 280(ra)
Current Store : [0x80006910] : sw tp, 284(ra) -- Store: [0x8000a530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000693c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006940]:csrrs tp, fcsr, zero
	-[0x80006944]:fsw ft11, 288(ra)
Current Store : [0x80006948] : sw tp, 292(ra) -- Store: [0x8000a538]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006974]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006978]:csrrs tp, fcsr, zero
	-[0x8000697c]:fsw ft11, 296(ra)
Current Store : [0x80006980] : sw tp, 300(ra) -- Store: [0x8000a540]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069ac]:fadd.h ft11, ft10, ft9, dyn
	-[0x800069b0]:csrrs tp, fcsr, zero
	-[0x800069b4]:fsw ft11, 304(ra)
Current Store : [0x800069b8] : sw tp, 308(ra) -- Store: [0x8000a548]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069e4]:fadd.h ft11, ft10, ft9, dyn
	-[0x800069e8]:csrrs tp, fcsr, zero
	-[0x800069ec]:fsw ft11, 312(ra)
Current Store : [0x800069f0] : sw tp, 316(ra) -- Store: [0x8000a550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a1c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006a20]:csrrs tp, fcsr, zero
	-[0x80006a24]:fsw ft11, 320(ra)
Current Store : [0x80006a28] : sw tp, 324(ra) -- Store: [0x8000a558]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a54]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006a58]:csrrs tp, fcsr, zero
	-[0x80006a5c]:fsw ft11, 328(ra)
Current Store : [0x80006a60] : sw tp, 332(ra) -- Store: [0x8000a560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a8c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006a90]:csrrs tp, fcsr, zero
	-[0x80006a94]:fsw ft11, 336(ra)
Current Store : [0x80006a98] : sw tp, 340(ra) -- Store: [0x8000a568]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ac4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006ac8]:csrrs tp, fcsr, zero
	-[0x80006acc]:fsw ft11, 344(ra)
Current Store : [0x80006ad0] : sw tp, 348(ra) -- Store: [0x8000a570]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006afc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006b00]:csrrs tp, fcsr, zero
	-[0x80006b04]:fsw ft11, 352(ra)
Current Store : [0x80006b08] : sw tp, 356(ra) -- Store: [0x8000a578]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b34]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006b38]:csrrs tp, fcsr, zero
	-[0x80006b3c]:fsw ft11, 360(ra)
Current Store : [0x80006b40] : sw tp, 364(ra) -- Store: [0x8000a580]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b6c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006b70]:csrrs tp, fcsr, zero
	-[0x80006b74]:fsw ft11, 368(ra)
Current Store : [0x80006b78] : sw tp, 372(ra) -- Store: [0x8000a588]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ba4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006ba8]:csrrs tp, fcsr, zero
	-[0x80006bac]:fsw ft11, 376(ra)
Current Store : [0x80006bb0] : sw tp, 380(ra) -- Store: [0x8000a590]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006bdc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006be0]:csrrs tp, fcsr, zero
	-[0x80006be4]:fsw ft11, 384(ra)
Current Store : [0x80006be8] : sw tp, 388(ra) -- Store: [0x8000a598]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c14]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006c18]:csrrs tp, fcsr, zero
	-[0x80006c1c]:fsw ft11, 392(ra)
Current Store : [0x80006c20] : sw tp, 396(ra) -- Store: [0x8000a5a0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c4c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006c50]:csrrs tp, fcsr, zero
	-[0x80006c54]:fsw ft11, 400(ra)
Current Store : [0x80006c58] : sw tp, 404(ra) -- Store: [0x8000a5a8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c84]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006c88]:csrrs tp, fcsr, zero
	-[0x80006c8c]:fsw ft11, 408(ra)
Current Store : [0x80006c90] : sw tp, 412(ra) -- Store: [0x8000a5b0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cbc]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006cc0]:csrrs tp, fcsr, zero
	-[0x80006cc4]:fsw ft11, 416(ra)
Current Store : [0x80006cc8] : sw tp, 420(ra) -- Store: [0x8000a5b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cf4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006cf8]:csrrs tp, fcsr, zero
	-[0x80006cfc]:fsw ft11, 424(ra)
Current Store : [0x80006d00] : sw tp, 428(ra) -- Store: [0x8000a5c0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d2c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006d30]:csrrs tp, fcsr, zero
	-[0x80006d34]:fsw ft11, 432(ra)
Current Store : [0x80006d38] : sw tp, 436(ra) -- Store: [0x8000a5c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d64]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006d68]:csrrs tp, fcsr, zero
	-[0x80006d6c]:fsw ft11, 440(ra)
Current Store : [0x80006d70] : sw tp, 444(ra) -- Store: [0x8000a5d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d9c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006da0]:csrrs tp, fcsr, zero
	-[0x80006da4]:fsw ft11, 448(ra)
Current Store : [0x80006da8] : sw tp, 452(ra) -- Store: [0x8000a5d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006dd4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006dd8]:csrrs tp, fcsr, zero
	-[0x80006ddc]:fsw ft11, 456(ra)
Current Store : [0x80006de0] : sw tp, 460(ra) -- Store: [0x8000a5e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e0c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006e10]:csrrs tp, fcsr, zero
	-[0x80006e14]:fsw ft11, 464(ra)
Current Store : [0x80006e18] : sw tp, 468(ra) -- Store: [0x8000a5e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e44]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006e48]:csrrs tp, fcsr, zero
	-[0x80006e4c]:fsw ft11, 472(ra)
Current Store : [0x80006e50] : sw tp, 476(ra) -- Store: [0x8000a5f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e7c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006e80]:csrrs tp, fcsr, zero
	-[0x80006e84]:fsw ft11, 480(ra)
Current Store : [0x80006e88] : sw tp, 484(ra) -- Store: [0x8000a5f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006eb4]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006eb8]:csrrs tp, fcsr, zero
	-[0x80006ebc]:fsw ft11, 488(ra)
Current Store : [0x80006ec0] : sw tp, 492(ra) -- Store: [0x8000a600]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006eec]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006ef0]:csrrs tp, fcsr, zero
	-[0x80006ef4]:fsw ft11, 496(ra)
Current Store : [0x80006ef8] : sw tp, 500(ra) -- Store: [0x8000a608]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f24]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006f28]:csrrs tp, fcsr, zero
	-[0x80006f2c]:fsw ft11, 504(ra)
Current Store : [0x80006f30] : sw tp, 508(ra) -- Store: [0x8000a610]:0x00000000




Last Coverpoint : ['mnemonic : fadd.h', 'rs1 : f30', 'rs2 : f29', 'rd : f31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f5c]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006f60]:csrrs tp, fcsr, zero
	-[0x80006f64]:fsw ft11, 512(ra)
Current Store : [0x80006f68] : sw tp, 516(ra) -- Store: [0x8000a618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f94]:fadd.h ft11, ft10, ft9, dyn
	-[0x80006f98]:csrrs tp, fcsr, zero
	-[0x80006f9c]:fsw ft11, 520(ra)
Current Store : [0x80006fa0] : sw tp, 524(ra) -- Store: [0x8000a620]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                           |                                                         code                                                          |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------|
|   1|[0x80009414]<br>0xFBB6FAB7<br> |- mnemonic : fadd.h<br> - rs1 : f31<br> - rs2 : f31<br> - rd : f31<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fadd.h ft11, ft11, ft11, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:fsw ft11, 0(ra)<br>   |
|   2|[0x8000941c]<br>0xF76DF56F<br> |- rs1 : f29<br> - rs2 : f28<br> - rd : f30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>  |[0x80000144]:fadd.h ft10, ft9, ft8, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:fsw ft10, 8(ra)<br>     |
|   3|[0x80009424]<br>0xEEDBEADF<br> |- rs1 : f30<br> - rs2 : f29<br> - rd : f29<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x80000164]:fadd.h ft9, ft10, ft9, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:fsw ft9, 16(ra)<br>     |
|   4|[0x8000942c]<br>0xDDB7D5BF<br> |- rs1 : f28<br> - rs2 : f30<br> - rd : f28<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x80000184]:fadd.h ft8, ft8, ft10, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:fsw ft8, 24(ra)<br>     |
|   5|[0x80009434]<br>0xBB6FAB7F<br> |- rs1 : f26<br> - rs2 : f26<br> - rd : f27<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                           |[0x800001a4]:fadd.h fs11, fs10, fs10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:fsw fs11, 32(ra)<br>  |
|   6|[0x8000943c]<br>0x76DF56FF<br> |- rs1 : f27<br> - rs2 : f25<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fadd.h fs10, fs11, fs9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:fsw fs10, 40(ra)<br>   |
|   7|[0x80009444]<br>0xEDBEADFE<br> |- rs1 : f24<br> - rs2 : f27<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001e4]:fadd.h fs9, fs8, fs11, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:fsw fs9, 48(ra)<br>     |
|   8|[0x8000944c]<br>0xDB7D5BFD<br> |- rs1 : f25<br> - rs2 : f23<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000204]:fadd.h fs8, fs9, fs7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:fsw fs8, 56(ra)<br>      |
|   9|[0x80009454]<br>0xB6FAB7FB<br> |- rs1 : f22<br> - rs2 : f24<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000224]:fadd.h fs7, fs6, fs8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:fsw fs7, 64(ra)<br>      |
|  10|[0x8000945c]<br>0x6DF56FF7<br> |- rs1 : f23<br> - rs2 : f21<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000244]:fadd.h fs6, fs7, fs5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:fsw fs6, 72(ra)<br>      |
|  11|[0x80009464]<br>0xDBEADFEE<br> |- rs1 : f20<br> - rs2 : f22<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fadd.h fs5, fs4, fs6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:fsw fs5, 80(ra)<br>      |
|  12|[0x8000946c]<br>0xB7D5BFDD<br> |- rs1 : f21<br> - rs2 : f19<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000284]:fadd.h fs4, fs5, fs3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:fsw fs4, 88(ra)<br>      |
|  13|[0x80009474]<br>0x6FAB7FBB<br> |- rs1 : f18<br> - rs2 : f20<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002a4]:fadd.h fs3, fs2, fs4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:fsw fs3, 96(ra)<br>      |
|  14|[0x8000947c]<br>0xDF56FF76<br> |- rs1 : f19<br> - rs2 : f17<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002c4]:fadd.h fs2, fs3, fa7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:fsw fs2, 104(ra)<br>     |
|  15|[0x80009484]<br>0xBEADFEED<br> |- rs1 : f16<br> - rs2 : f18<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002e4]:fadd.h fa7, fa6, fs2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:fsw fa7, 112(ra)<br>     |
|  16|[0x8000948c]<br>0x7D5BFDDB<br> |- rs1 : f17<br> - rs2 : f15<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fadd.h fa6, fa7, fa5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:fsw fa6, 120(ra)<br>     |
|  17|[0x80009494]<br>0xFAB7FBB6<br> |- rs1 : f14<br> - rs2 : f16<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000324]:fadd.h fa5, fa4, fa6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:fsw fa5, 128(ra)<br>     |
|  18|[0x8000949c]<br>0xF56FF76D<br> |- rs1 : f15<br> - rs2 : f13<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000344]:fadd.h fa4, fa5, fa3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:fsw fa4, 136(ra)<br>     |
|  19|[0x800094a4]<br>0xEADFEEDB<br> |- rs1 : f12<br> - rs2 : f14<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000364]:fadd.h fa3, fa2, fa4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:fsw fa3, 144(ra)<br>     |
|  20|[0x800094ac]<br>0xD5BFDDB7<br> |- rs1 : f13<br> - rs2 : f11<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000384]:fadd.h fa2, fa3, fa1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:fsw fa2, 152(ra)<br>     |
|  21|[0x800094b4]<br>0xAB7FBB6F<br> |- rs1 : f10<br> - rs2 : f12<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fadd.h fa1, fa0, fa2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:fsw fa1, 160(ra)<br>     |
|  22|[0x800094bc]<br>0x00002000<br> |- rs1 : f11<br> - rs2 : f9<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                 |[0x800003c4]:fadd.h fa0, fa1, fs1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:fsw fa0, 168(ra)<br>     |
|  23|[0x800094c4]<br>0xADFEEDBE<br> |- rs1 : f8<br> - rs2 : f10<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800003e4]:fadd.h fs1, fs0, fa0, dyn<br> [0x800003e8]:csrrs tp, fcsr, zero<br> [0x800003ec]:fsw fs1, 176(ra)<br>     |
|  24|[0x800094cc]<br>0x5BFDDB7D<br> |- rs1 : f9<br> - rs2 : f7<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000404]:fadd.h fs0, fs1, ft7, dyn<br> [0x80000408]:csrrs tp, fcsr, zero<br> [0x8000040c]:fsw fs0, 184(ra)<br>     |
|  25|[0x800094d4]<br>0xB7FBB6FA<br> |- rs1 : f6<br> - rs2 : f8<br> - rd : f7<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000424]:fadd.h ft7, ft6, fs0, dyn<br> [0x80000428]:csrrs tp, fcsr, zero<br> [0x8000042c]:fsw ft7, 192(ra)<br>     |
|  26|[0x800094dc]<br>0x80008000<br> |- rs1 : f7<br> - rs2 : f5<br> - rd : f6<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000444]:fadd.h ft6, ft7, ft5, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:fsw ft6, 200(ra)<br>     |
|  27|[0x800094e4]<br>0x800000F8<br> |- rs1 : f4<br> - rs2 : f6<br> - rd : f5<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000464]:fadd.h ft5, ft4, ft6, dyn<br> [0x80000468]:csrrs tp, fcsr, zero<br> [0x8000046c]:fsw ft5, 208(ra)<br>     |
|  28|[0x800094ec]<br>0x00000000<br> |- rs1 : f5<br> - rs2 : f3<br> - rd : f4<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000484]:fadd.h ft4, ft5, ft3, dyn<br> [0x80000488]:csrrs tp, fcsr, zero<br> [0x8000048c]:fsw ft4, 216(ra)<br>     |
|  29|[0x800094f4]<br>0x80008010<br> |- rs1 : f2<br> - rs2 : f4<br> - rd : f3<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004a4]:fadd.h ft3, ft2, ft4, dyn<br> [0x800004a8]:csrrs tp, fcsr, zero<br> [0x800004ac]:fsw ft3, 224(ra)<br>     |
|  30|[0x800094fc]<br>0x00000000<br> |- rs1 : f3<br> - rs2 : f1<br> - rd : f2<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004c4]:fadd.h ft2, ft3, ft1, dyn<br> [0x800004c8]:csrrs tp, fcsr, zero<br> [0x800004cc]:fsw ft2, 232(ra)<br>     |
|  31|[0x80009504]<br>0x80009414<br> |- rs1 : f0<br> - rs2 : f2<br> - rd : f1<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004e4]:fadd.h ft1, ft0, ft2, dyn<br> [0x800004e8]:csrrs tp, fcsr, zero<br> [0x800004ec]:fsw ft1, 240(ra)<br>     |
|  32|[0x8000950c]<br>0xFBB6FAB7<br> |- rs1 : f1<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000504]:fadd.h ft11, ft1, ft10, dyn<br> [0x80000508]:csrrs tp, fcsr, zero<br> [0x8000050c]:fsw ft11, 248(ra)<br>  |
|  33|[0x80009514]<br>0xFBB6FAB7<br> |- rs2 : f0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000524]:fadd.h ft11, ft10, ft0, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:fsw ft11, 256(ra)<br>  |
|  34|[0x8000951c]<br>0x00000000<br> |- rd : f0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                 |[0x80000544]:fadd.h ft0, ft11, ft10, dyn<br> [0x80000548]:csrrs tp, fcsr, zero<br> [0x8000054c]:fsw ft0, 264(ra)<br>   |
|  35|[0x80009524]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000564]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs tp, fcsr, zero<br> [0x8000056c]:fsw ft11, 272(ra)<br>  |
|  36|[0x8000952c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000584]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000588]:csrrs tp, fcsr, zero<br> [0x8000058c]:fsw ft11, 280(ra)<br>  |
|  37|[0x80009534]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800005a8]:csrrs tp, fcsr, zero<br> [0x800005ac]:fsw ft11, 288(ra)<br>  |
|  38|[0x8000953c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800005c8]:csrrs tp, fcsr, zero<br> [0x800005cc]:fsw ft11, 296(ra)<br>  |
|  39|[0x80009544]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800005e8]:csrrs tp, fcsr, zero<br> [0x800005ec]:fsw ft11, 304(ra)<br>  |
|  40|[0x8000954c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000604]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:fsw ft11, 312(ra)<br>  |
|  41|[0x80009554]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000624]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000628]:csrrs tp, fcsr, zero<br> [0x8000062c]:fsw ft11, 320(ra)<br>  |
|  42|[0x8000955c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000644]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs tp, fcsr, zero<br> [0x8000064c]:fsw ft11, 328(ra)<br>  |
|  43|[0x80009564]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000664]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000668]:csrrs tp, fcsr, zero<br> [0x8000066c]:fsw ft11, 336(ra)<br>  |
|  44|[0x8000956c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000684]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000688]:csrrs tp, fcsr, zero<br> [0x8000068c]:fsw ft11, 344(ra)<br>  |
|  45|[0x80009574]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800006a8]:csrrs tp, fcsr, zero<br> [0x800006ac]:fsw ft11, 352(ra)<br>  |
|  46|[0x8000957c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800006c8]:csrrs tp, fcsr, zero<br> [0x800006cc]:fsw ft11, 360(ra)<br>  |
|  47|[0x80009584]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800006e8]:csrrs tp, fcsr, zero<br> [0x800006ec]:fsw ft11, 368(ra)<br>  |
|  48|[0x8000958c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000704]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000708]:csrrs tp, fcsr, zero<br> [0x8000070c]:fsw ft11, 376(ra)<br>  |
|  49|[0x80009594]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000724]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs tp, fcsr, zero<br> [0x8000072c]:fsw ft11, 384(ra)<br>  |
|  50|[0x8000959c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000744]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000748]:csrrs tp, fcsr, zero<br> [0x8000074c]:fsw ft11, 392(ra)<br>  |
|  51|[0x800095a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000764]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000768]:csrrs tp, fcsr, zero<br> [0x8000076c]:fsw ft11, 400(ra)<br>  |
|  52|[0x800095ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000784]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000788]:csrrs tp, fcsr, zero<br> [0x8000078c]:fsw ft11, 408(ra)<br>  |
|  53|[0x800095b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800007a8]:csrrs tp, fcsr, zero<br> [0x800007ac]:fsw ft11, 416(ra)<br>  |
|  54|[0x800095bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800007c8]:csrrs tp, fcsr, zero<br> [0x800007cc]:fsw ft11, 424(ra)<br>  |
|  55|[0x800095c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800007e8]:csrrs tp, fcsr, zero<br> [0x800007ec]:fsw ft11, 432(ra)<br>  |
|  56|[0x800095cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000804]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs tp, fcsr, zero<br> [0x8000080c]:fsw ft11, 440(ra)<br>  |
|  57|[0x800095d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000824]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000828]:csrrs tp, fcsr, zero<br> [0x8000082c]:fsw ft11, 448(ra)<br>  |
|  58|[0x800095dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000844]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000848]:csrrs tp, fcsr, zero<br> [0x8000084c]:fsw ft11, 456(ra)<br>  |
|  59|[0x800095e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000864]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000868]:csrrs tp, fcsr, zero<br> [0x8000086c]:fsw ft11, 464(ra)<br>  |
|  60|[0x800095ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000884]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000888]:csrrs tp, fcsr, zero<br> [0x8000088c]:fsw ft11, 472(ra)<br>  |
|  61|[0x800095f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800008a8]:csrrs tp, fcsr, zero<br> [0x800008ac]:fsw ft11, 480(ra)<br>  |
|  62|[0x800095fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800008c8]:csrrs tp, fcsr, zero<br> [0x800008cc]:fsw ft11, 488(ra)<br>  |
|  63|[0x80009604]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800008e8]:csrrs tp, fcsr, zero<br> [0x800008ec]:fsw ft11, 496(ra)<br>  |
|  64|[0x8000960c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000904]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000908]:csrrs tp, fcsr, zero<br> [0x8000090c]:fsw ft11, 504(ra)<br>  |
|  65|[0x80009614]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000924]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000928]:csrrs tp, fcsr, zero<br> [0x8000092c]:fsw ft11, 512(ra)<br>  |
|  66|[0x8000961c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000944]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000948]:csrrs tp, fcsr, zero<br> [0x8000094c]:fsw ft11, 520(ra)<br>  |
|  67|[0x80009624]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000964]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000968]:csrrs tp, fcsr, zero<br> [0x8000096c]:fsw ft11, 528(ra)<br>  |
|  68|[0x8000962c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000984]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000988]:csrrs tp, fcsr, zero<br> [0x8000098c]:fsw ft11, 536(ra)<br>  |
|  69|[0x80009634]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800009a8]:csrrs tp, fcsr, zero<br> [0x800009ac]:fsw ft11, 544(ra)<br>  |
|  70|[0x8000963c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800009c8]:csrrs tp, fcsr, zero<br> [0x800009cc]:fsw ft11, 552(ra)<br>  |
|  71|[0x80009644]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800009e8]:csrrs tp, fcsr, zero<br> [0x800009ec]:fsw ft11, 560(ra)<br>  |
|  72|[0x8000964c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000a08]:csrrs tp, fcsr, zero<br> [0x80000a0c]:fsw ft11, 568(ra)<br>  |
|  73|[0x80009654]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000a28]:csrrs tp, fcsr, zero<br> [0x80000a2c]:fsw ft11, 576(ra)<br>  |
|  74|[0x8000965c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000a48]:csrrs tp, fcsr, zero<br> [0x80000a4c]:fsw ft11, 584(ra)<br>  |
|  75|[0x80009664]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000a68]:csrrs tp, fcsr, zero<br> [0x80000a6c]:fsw ft11, 592(ra)<br>  |
|  76|[0x8000966c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000a88]:csrrs tp, fcsr, zero<br> [0x80000a8c]:fsw ft11, 600(ra)<br>  |
|  77|[0x80009674]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000aa4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000aa8]:csrrs tp, fcsr, zero<br> [0x80000aac]:fsw ft11, 608(ra)<br>  |
|  78|[0x8000967c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ac4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ac8]:csrrs tp, fcsr, zero<br> [0x80000acc]:fsw ft11, 616(ra)<br>  |
|  79|[0x80009684]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ae4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ae8]:csrrs tp, fcsr, zero<br> [0x80000aec]:fsw ft11, 624(ra)<br>  |
|  80|[0x8000968c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000b08]:csrrs tp, fcsr, zero<br> [0x80000b0c]:fsw ft11, 632(ra)<br>  |
|  81|[0x80009694]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000b28]:csrrs tp, fcsr, zero<br> [0x80000b2c]:fsw ft11, 640(ra)<br>  |
|  82|[0x8000969c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000b48]:csrrs tp, fcsr, zero<br> [0x80000b4c]:fsw ft11, 648(ra)<br>  |
|  83|[0x800096a4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000b68]:csrrs tp, fcsr, zero<br> [0x80000b6c]:fsw ft11, 656(ra)<br>  |
|  84|[0x800096ac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000b88]:csrrs tp, fcsr, zero<br> [0x80000b8c]:fsw ft11, 664(ra)<br>  |
|  85|[0x800096b4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ba4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ba8]:csrrs tp, fcsr, zero<br> [0x80000bac]:fsw ft11, 672(ra)<br>  |
|  86|[0x800096bc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bc4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000bc8]:csrrs tp, fcsr, zero<br> [0x80000bcc]:fsw ft11, 680(ra)<br>  |
|  87|[0x800096c4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000be4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000be8]:csrrs tp, fcsr, zero<br> [0x80000bec]:fsw ft11, 688(ra)<br>  |
|  88|[0x800096cc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000c08]:csrrs tp, fcsr, zero<br> [0x80000c0c]:fsw ft11, 696(ra)<br>  |
|  89|[0x800096d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000c28]:csrrs tp, fcsr, zero<br> [0x80000c2c]:fsw ft11, 704(ra)<br>  |
|  90|[0x800096dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000c48]:csrrs tp, fcsr, zero<br> [0x80000c4c]:fsw ft11, 712(ra)<br>  |
|  91|[0x800096e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000c68]:csrrs tp, fcsr, zero<br> [0x80000c6c]:fsw ft11, 720(ra)<br>  |
|  92|[0x800096ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000c88]:csrrs tp, fcsr, zero<br> [0x80000c8c]:fsw ft11, 728(ra)<br>  |
|  93|[0x800096f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ca4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ca8]:csrrs tp, fcsr, zero<br> [0x80000cac]:fsw ft11, 736(ra)<br>  |
|  94|[0x800096fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000cc4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000cc8]:csrrs tp, fcsr, zero<br> [0x80000ccc]:fsw ft11, 744(ra)<br>  |
|  95|[0x80009704]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ce4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ce8]:csrrs tp, fcsr, zero<br> [0x80000cec]:fsw ft11, 752(ra)<br>  |
|  96|[0x8000970c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000d08]:csrrs tp, fcsr, zero<br> [0x80000d0c]:fsw ft11, 760(ra)<br>  |
|  97|[0x80009714]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000d28]:csrrs tp, fcsr, zero<br> [0x80000d2c]:fsw ft11, 768(ra)<br>  |
|  98|[0x8000971c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000d48]:csrrs tp, fcsr, zero<br> [0x80000d4c]:fsw ft11, 776(ra)<br>  |
|  99|[0x80009724]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000d68]:csrrs tp, fcsr, zero<br> [0x80000d6c]:fsw ft11, 784(ra)<br>  |
| 100|[0x8000972c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000d88]:csrrs tp, fcsr, zero<br> [0x80000d8c]:fsw ft11, 792(ra)<br>  |
| 101|[0x80009734]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000da4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000da8]:csrrs tp, fcsr, zero<br> [0x80000dac]:fsw ft11, 800(ra)<br>  |
| 102|[0x8000973c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000dc4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000dc8]:csrrs tp, fcsr, zero<br> [0x80000dcc]:fsw ft11, 808(ra)<br>  |
| 103|[0x80009744]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000de4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000de8]:csrrs tp, fcsr, zero<br> [0x80000dec]:fsw ft11, 816(ra)<br>  |
| 104|[0x8000974c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000e08]:csrrs tp, fcsr, zero<br> [0x80000e0c]:fsw ft11, 824(ra)<br>  |
| 105|[0x80009754]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000e28]:csrrs tp, fcsr, zero<br> [0x80000e2c]:fsw ft11, 832(ra)<br>  |
| 106|[0x8000975c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000e48]:csrrs tp, fcsr, zero<br> [0x80000e4c]:fsw ft11, 840(ra)<br>  |
| 107|[0x80009764]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000e68]:csrrs tp, fcsr, zero<br> [0x80000e6c]:fsw ft11, 848(ra)<br>  |
| 108|[0x8000976c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000e88]:csrrs tp, fcsr, zero<br> [0x80000e8c]:fsw ft11, 856(ra)<br>  |
| 109|[0x80009774]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ea4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ea8]:csrrs tp, fcsr, zero<br> [0x80000eac]:fsw ft11, 864(ra)<br>  |
| 110|[0x8000977c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ec4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ec8]:csrrs tp, fcsr, zero<br> [0x80000ecc]:fsw ft11, 872(ra)<br>  |
| 111|[0x80009784]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ee4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000ee8]:csrrs tp, fcsr, zero<br> [0x80000eec]:fsw ft11, 880(ra)<br>  |
| 112|[0x8000978c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f04]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000f08]:csrrs tp, fcsr, zero<br> [0x80000f0c]:fsw ft11, 888(ra)<br>  |
| 113|[0x80009794]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000f28]:csrrs tp, fcsr, zero<br> [0x80000f2c]:fsw ft11, 896(ra)<br>  |
| 114|[0x8000979c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000f48]:csrrs tp, fcsr, zero<br> [0x80000f4c]:fsw ft11, 904(ra)<br>  |
| 115|[0x800097a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs tp, fcsr, zero<br> [0x80000f6c]:fsw ft11, 912(ra)<br>  |
| 116|[0x800097ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000f88]:csrrs tp, fcsr, zero<br> [0x80000f8c]:fsw ft11, 920(ra)<br>  |
| 117|[0x800097b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000fa4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000fa8]:csrrs tp, fcsr, zero<br> [0x80000fac]:fsw ft11, 928(ra)<br>  |
| 118|[0x800097bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000fc4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000fc8]:csrrs tp, fcsr, zero<br> [0x80000fcc]:fsw ft11, 936(ra)<br>  |
| 119|[0x800097c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000fe4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80000fe8]:csrrs tp, fcsr, zero<br> [0x80000fec]:fsw ft11, 944(ra)<br>  |
| 120|[0x800097cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001004]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001008]:csrrs tp, fcsr, zero<br> [0x8000100c]:fsw ft11, 952(ra)<br>  |
| 121|[0x800097d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001024]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001028]:csrrs tp, fcsr, zero<br> [0x8000102c]:fsw ft11, 960(ra)<br>  |
| 122|[0x800097dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001044]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001048]:csrrs tp, fcsr, zero<br> [0x8000104c]:fsw ft11, 968(ra)<br>  |
| 123|[0x800097e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001064]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001068]:csrrs tp, fcsr, zero<br> [0x8000106c]:fsw ft11, 976(ra)<br>  |
| 124|[0x800097ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001084]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001088]:csrrs tp, fcsr, zero<br> [0x8000108c]:fsw ft11, 984(ra)<br>  |
| 125|[0x800097f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800010a8]:csrrs tp, fcsr, zero<br> [0x800010ac]:fsw ft11, 992(ra)<br>  |
| 126|[0x800097fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800010c8]:csrrs tp, fcsr, zero<br> [0x800010cc]:fsw ft11, 1000(ra)<br> |
| 127|[0x80009804]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800010e8]:csrrs tp, fcsr, zero<br> [0x800010ec]:fsw ft11, 1008(ra)<br> |
| 128|[0x8000980c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001104]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001108]:csrrs tp, fcsr, zero<br> [0x8000110c]:fsw ft11, 1016(ra)<br> |
| 129|[0x80009814]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000112c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001130]:csrrs tp, fcsr, zero<br> [0x80001134]:fsw ft11, 0(ra)<br>    |
| 130|[0x8000981c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000114c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001150]:csrrs tp, fcsr, zero<br> [0x80001154]:fsw ft11, 8(ra)<br>    |
| 131|[0x80009824]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000116c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001170]:csrrs tp, fcsr, zero<br> [0x80001174]:fsw ft11, 16(ra)<br>   |
| 132|[0x8000982c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000118c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001190]:csrrs tp, fcsr, zero<br> [0x80001194]:fsw ft11, 24(ra)<br>   |
| 133|[0x80009834]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800011b0]:csrrs tp, fcsr, zero<br> [0x800011b4]:fsw ft11, 32(ra)<br>   |
| 134|[0x8000983c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800011d0]:csrrs tp, fcsr, zero<br> [0x800011d4]:fsw ft11, 40(ra)<br>   |
| 135|[0x80009844]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800011f0]:csrrs tp, fcsr, zero<br> [0x800011f4]:fsw ft11, 48(ra)<br>   |
| 136|[0x8000984c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000120c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001210]:csrrs tp, fcsr, zero<br> [0x80001214]:fsw ft11, 56(ra)<br>   |
| 137|[0x80009854]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000122c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001230]:csrrs tp, fcsr, zero<br> [0x80001234]:fsw ft11, 64(ra)<br>   |
| 138|[0x8000985c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000124c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001250]:csrrs tp, fcsr, zero<br> [0x80001254]:fsw ft11, 72(ra)<br>   |
| 139|[0x80009864]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000126c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001270]:csrrs tp, fcsr, zero<br> [0x80001274]:fsw ft11, 80(ra)<br>   |
| 140|[0x8000986c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000128c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001290]:csrrs tp, fcsr, zero<br> [0x80001294]:fsw ft11, 88(ra)<br>   |
| 141|[0x80009874]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800012b0]:csrrs tp, fcsr, zero<br> [0x800012b4]:fsw ft11, 96(ra)<br>   |
| 142|[0x8000987c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800012d0]:csrrs tp, fcsr, zero<br> [0x800012d4]:fsw ft11, 104(ra)<br>  |
| 143|[0x80009884]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800012f0]:csrrs tp, fcsr, zero<br> [0x800012f4]:fsw ft11, 112(ra)<br>  |
| 144|[0x8000988c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000130c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001310]:csrrs tp, fcsr, zero<br> [0x80001314]:fsw ft11, 120(ra)<br>  |
| 145|[0x80009894]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000132c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001330]:csrrs tp, fcsr, zero<br> [0x80001334]:fsw ft11, 128(ra)<br>  |
| 146|[0x8000989c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000134c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001350]:csrrs tp, fcsr, zero<br> [0x80001354]:fsw ft11, 136(ra)<br>  |
| 147|[0x800098a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000136c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001370]:csrrs tp, fcsr, zero<br> [0x80001374]:fsw ft11, 144(ra)<br>  |
| 148|[0x800098ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000138c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001390]:csrrs tp, fcsr, zero<br> [0x80001394]:fsw ft11, 152(ra)<br>  |
| 149|[0x800098b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800013b0]:csrrs tp, fcsr, zero<br> [0x800013b4]:fsw ft11, 160(ra)<br>  |
| 150|[0x800098bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800013d0]:csrrs tp, fcsr, zero<br> [0x800013d4]:fsw ft11, 168(ra)<br>  |
| 151|[0x800098c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800013f0]:csrrs tp, fcsr, zero<br> [0x800013f4]:fsw ft11, 176(ra)<br>  |
| 152|[0x800098cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000140c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001410]:csrrs tp, fcsr, zero<br> [0x80001414]:fsw ft11, 184(ra)<br>  |
| 153|[0x800098d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000142c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001430]:csrrs tp, fcsr, zero<br> [0x80001434]:fsw ft11, 192(ra)<br>  |
| 154|[0x800098dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000144c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001450]:csrrs tp, fcsr, zero<br> [0x80001454]:fsw ft11, 200(ra)<br>  |
| 155|[0x800098e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000146c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001470]:csrrs tp, fcsr, zero<br> [0x80001474]:fsw ft11, 208(ra)<br>  |
| 156|[0x800098ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000148c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001490]:csrrs tp, fcsr, zero<br> [0x80001494]:fsw ft11, 216(ra)<br>  |
| 157|[0x800098f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800014b0]:csrrs tp, fcsr, zero<br> [0x800014b4]:fsw ft11, 224(ra)<br>  |
| 158|[0x800098fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800014d0]:csrrs tp, fcsr, zero<br> [0x800014d4]:fsw ft11, 232(ra)<br>  |
| 159|[0x80009904]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800014f0]:csrrs tp, fcsr, zero<br> [0x800014f4]:fsw ft11, 240(ra)<br>  |
| 160|[0x8000990c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000150c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001510]:csrrs tp, fcsr, zero<br> [0x80001514]:fsw ft11, 248(ra)<br>  |
| 161|[0x80009914]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000152c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001530]:csrrs tp, fcsr, zero<br> [0x80001534]:fsw ft11, 256(ra)<br>  |
| 162|[0x8000991c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000154c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001550]:csrrs tp, fcsr, zero<br> [0x80001554]:fsw ft11, 264(ra)<br>  |
| 163|[0x80009924]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000156c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001570]:csrrs tp, fcsr, zero<br> [0x80001574]:fsw ft11, 272(ra)<br>  |
| 164|[0x8000992c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000158c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001590]:csrrs tp, fcsr, zero<br> [0x80001594]:fsw ft11, 280(ra)<br>  |
| 165|[0x80009934]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800015b0]:csrrs tp, fcsr, zero<br> [0x800015b4]:fsw ft11, 288(ra)<br>  |
| 166|[0x8000993c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800015d0]:csrrs tp, fcsr, zero<br> [0x800015d4]:fsw ft11, 296(ra)<br>  |
| 167|[0x80009944]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800015f0]:csrrs tp, fcsr, zero<br> [0x800015f4]:fsw ft11, 304(ra)<br>  |
| 168|[0x8000994c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000160c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001610]:csrrs tp, fcsr, zero<br> [0x80001614]:fsw ft11, 312(ra)<br>  |
| 169|[0x80009954]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000162c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001630]:csrrs tp, fcsr, zero<br> [0x80001634]:fsw ft11, 320(ra)<br>  |
| 170|[0x8000995c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000164c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001650]:csrrs tp, fcsr, zero<br> [0x80001654]:fsw ft11, 328(ra)<br>  |
| 171|[0x80009964]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000166c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001670]:csrrs tp, fcsr, zero<br> [0x80001674]:fsw ft11, 336(ra)<br>  |
| 172|[0x8000996c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000168c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001690]:csrrs tp, fcsr, zero<br> [0x80001694]:fsw ft11, 344(ra)<br>  |
| 173|[0x80009974]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800016b0]:csrrs tp, fcsr, zero<br> [0x800016b4]:fsw ft11, 352(ra)<br>  |
| 174|[0x8000997c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800016d0]:csrrs tp, fcsr, zero<br> [0x800016d4]:fsw ft11, 360(ra)<br>  |
| 175|[0x80009984]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800016f0]:csrrs tp, fcsr, zero<br> [0x800016f4]:fsw ft11, 368(ra)<br>  |
| 176|[0x8000998c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000170c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001710]:csrrs tp, fcsr, zero<br> [0x80001714]:fsw ft11, 376(ra)<br>  |
| 177|[0x80009994]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000172c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001730]:csrrs tp, fcsr, zero<br> [0x80001734]:fsw ft11, 384(ra)<br>  |
| 178|[0x8000999c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000174c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001750]:csrrs tp, fcsr, zero<br> [0x80001754]:fsw ft11, 392(ra)<br>  |
| 179|[0x800099a4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000176c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001770]:csrrs tp, fcsr, zero<br> [0x80001774]:fsw ft11, 400(ra)<br>  |
| 180|[0x800099ac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000178c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001790]:csrrs tp, fcsr, zero<br> [0x80001794]:fsw ft11, 408(ra)<br>  |
| 181|[0x800099b4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800017b0]:csrrs tp, fcsr, zero<br> [0x800017b4]:fsw ft11, 416(ra)<br>  |
| 182|[0x800099bc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800017d0]:csrrs tp, fcsr, zero<br> [0x800017d4]:fsw ft11, 424(ra)<br>  |
| 183|[0x800099c4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800017f0]:csrrs tp, fcsr, zero<br> [0x800017f4]:fsw ft11, 432(ra)<br>  |
| 184|[0x800099cc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000180c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001810]:csrrs tp, fcsr, zero<br> [0x80001814]:fsw ft11, 440(ra)<br>  |
| 185|[0x800099d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000182c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001830]:csrrs tp, fcsr, zero<br> [0x80001834]:fsw ft11, 448(ra)<br>  |
| 186|[0x800099dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000184c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001850]:csrrs tp, fcsr, zero<br> [0x80001854]:fsw ft11, 456(ra)<br>  |
| 187|[0x800099e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000186c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001870]:csrrs tp, fcsr, zero<br> [0x80001874]:fsw ft11, 464(ra)<br>  |
| 188|[0x800099ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000188c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001890]:csrrs tp, fcsr, zero<br> [0x80001894]:fsw ft11, 472(ra)<br>  |
| 189|[0x800099f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800018b0]:csrrs tp, fcsr, zero<br> [0x800018b4]:fsw ft11, 480(ra)<br>  |
| 190|[0x800099fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800018d0]:csrrs tp, fcsr, zero<br> [0x800018d4]:fsw ft11, 488(ra)<br>  |
| 191|[0x80009a04]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800018f0]:csrrs tp, fcsr, zero<br> [0x800018f4]:fsw ft11, 496(ra)<br>  |
| 192|[0x80009a0c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000190c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001910]:csrrs tp, fcsr, zero<br> [0x80001914]:fsw ft11, 504(ra)<br>  |
| 193|[0x80009a14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000192c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001930]:csrrs tp, fcsr, zero<br> [0x80001934]:fsw ft11, 512(ra)<br>  |
| 194|[0x80009a1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000194c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001950]:csrrs tp, fcsr, zero<br> [0x80001954]:fsw ft11, 520(ra)<br>  |
| 195|[0x80009a24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000196c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001970]:csrrs tp, fcsr, zero<br> [0x80001974]:fsw ft11, 528(ra)<br>  |
| 196|[0x80009a2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000198c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001990]:csrrs tp, fcsr, zero<br> [0x80001994]:fsw ft11, 536(ra)<br>  |
| 197|[0x80009a34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800019b0]:csrrs tp, fcsr, zero<br> [0x800019b4]:fsw ft11, 544(ra)<br>  |
| 198|[0x80009a3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800019d0]:csrrs tp, fcsr, zero<br> [0x800019d4]:fsw ft11, 552(ra)<br>  |
| 199|[0x80009a44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800019f0]:csrrs tp, fcsr, zero<br> [0x800019f4]:fsw ft11, 560(ra)<br>  |
| 200|[0x80009a4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001a10]:csrrs tp, fcsr, zero<br> [0x80001a14]:fsw ft11, 568(ra)<br>  |
| 201|[0x80009a54]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001a30]:csrrs tp, fcsr, zero<br> [0x80001a34]:fsw ft11, 576(ra)<br>  |
| 202|[0x80009a5c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001a50]:csrrs tp, fcsr, zero<br> [0x80001a54]:fsw ft11, 584(ra)<br>  |
| 203|[0x80009a64]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001a70]:csrrs tp, fcsr, zero<br> [0x80001a74]:fsw ft11, 592(ra)<br>  |
| 204|[0x80009a6c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001a90]:csrrs tp, fcsr, zero<br> [0x80001a94]:fsw ft11, 600(ra)<br>  |
| 205|[0x80009a74]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001aac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001ab0]:csrrs tp, fcsr, zero<br> [0x80001ab4]:fsw ft11, 608(ra)<br>  |
| 206|[0x80009a7c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001acc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001ad0]:csrrs tp, fcsr, zero<br> [0x80001ad4]:fsw ft11, 616(ra)<br>  |
| 207|[0x80009a84]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001aec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001af0]:csrrs tp, fcsr, zero<br> [0x80001af4]:fsw ft11, 624(ra)<br>  |
| 208|[0x80009a8c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001b10]:csrrs tp, fcsr, zero<br> [0x80001b14]:fsw ft11, 632(ra)<br>  |
| 209|[0x80009a94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001b30]:csrrs tp, fcsr, zero<br> [0x80001b34]:fsw ft11, 640(ra)<br>  |
| 210|[0x80009a9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001b50]:csrrs tp, fcsr, zero<br> [0x80001b54]:fsw ft11, 648(ra)<br>  |
| 211|[0x80009aa4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001b70]:csrrs tp, fcsr, zero<br> [0x80001b74]:fsw ft11, 656(ra)<br>  |
| 212|[0x80009aac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001b90]:csrrs tp, fcsr, zero<br> [0x80001b94]:fsw ft11, 664(ra)<br>  |
| 213|[0x80009ab4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001bb0]:csrrs tp, fcsr, zero<br> [0x80001bb4]:fsw ft11, 672(ra)<br>  |
| 214|[0x80009abc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bcc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001bd0]:csrrs tp, fcsr, zero<br> [0x80001bd4]:fsw ft11, 680(ra)<br>  |
| 215|[0x80009ac4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001bf0]:csrrs tp, fcsr, zero<br> [0x80001bf4]:fsw ft11, 688(ra)<br>  |
| 216|[0x80009acc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001c10]:csrrs tp, fcsr, zero<br> [0x80001c14]:fsw ft11, 696(ra)<br>  |
| 217|[0x80009ad4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001c30]:csrrs tp, fcsr, zero<br> [0x80001c34]:fsw ft11, 704(ra)<br>  |
| 218|[0x80009adc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001c50]:csrrs tp, fcsr, zero<br> [0x80001c54]:fsw ft11, 712(ra)<br>  |
| 219|[0x80009ae4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001c70]:csrrs tp, fcsr, zero<br> [0x80001c74]:fsw ft11, 720(ra)<br>  |
| 220|[0x80009aec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001c90]:csrrs tp, fcsr, zero<br> [0x80001c94]:fsw ft11, 728(ra)<br>  |
| 221|[0x80009af4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001cb0]:csrrs tp, fcsr, zero<br> [0x80001cb4]:fsw ft11, 736(ra)<br>  |
| 222|[0x80009afc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001ccc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001cd0]:csrrs tp, fcsr, zero<br> [0x80001cd4]:fsw ft11, 744(ra)<br>  |
| 223|[0x80009b04]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001cf0]:csrrs tp, fcsr, zero<br> [0x80001cf4]:fsw ft11, 752(ra)<br>  |
| 224|[0x80009b0c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001d10]:csrrs tp, fcsr, zero<br> [0x80001d14]:fsw ft11, 760(ra)<br>  |
| 225|[0x80009b14]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001d30]:csrrs tp, fcsr, zero<br> [0x80001d34]:fsw ft11, 768(ra)<br>  |
| 226|[0x80009b1c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001d50]:csrrs tp, fcsr, zero<br> [0x80001d54]:fsw ft11, 776(ra)<br>  |
| 227|[0x80009b24]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001d70]:csrrs tp, fcsr, zero<br> [0x80001d74]:fsw ft11, 784(ra)<br>  |
| 228|[0x80009b2c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001d90]:csrrs tp, fcsr, zero<br> [0x80001d94]:fsw ft11, 792(ra)<br>  |
| 229|[0x80009b34]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001db0]:csrrs tp, fcsr, zero<br> [0x80001db4]:fsw ft11, 800(ra)<br>  |
| 230|[0x80009b3c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dcc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001dd0]:csrrs tp, fcsr, zero<br> [0x80001dd4]:fsw ft11, 808(ra)<br>  |
| 231|[0x80009b44]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001df0]:csrrs tp, fcsr, zero<br> [0x80001df4]:fsw ft11, 816(ra)<br>  |
| 232|[0x80009b4c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001e10]:csrrs tp, fcsr, zero<br> [0x80001e14]:fsw ft11, 824(ra)<br>  |
| 233|[0x80009b54]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001e30]:csrrs tp, fcsr, zero<br> [0x80001e34]:fsw ft11, 832(ra)<br>  |
| 234|[0x80009b5c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001e50]:csrrs tp, fcsr, zero<br> [0x80001e54]:fsw ft11, 840(ra)<br>  |
| 235|[0x80009b64]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001e70]:csrrs tp, fcsr, zero<br> [0x80001e74]:fsw ft11, 848(ra)<br>  |
| 236|[0x80009b6c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001e90]:csrrs tp, fcsr, zero<br> [0x80001e94]:fsw ft11, 856(ra)<br>  |
| 237|[0x80009b74]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001eac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001eb0]:csrrs tp, fcsr, zero<br> [0x80001eb4]:fsw ft11, 864(ra)<br>  |
| 238|[0x80009b7c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001ecc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001ed0]:csrrs tp, fcsr, zero<br> [0x80001ed4]:fsw ft11, 872(ra)<br>  |
| 239|[0x80009b84]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001eec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001ef0]:csrrs tp, fcsr, zero<br> [0x80001ef4]:fsw ft11, 880(ra)<br>  |
| 240|[0x80009b8c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001f10]:csrrs tp, fcsr, zero<br> [0x80001f14]:fsw ft11, 888(ra)<br>  |
| 241|[0x80009b94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001f30]:csrrs tp, fcsr, zero<br> [0x80001f34]:fsw ft11, 896(ra)<br>  |
| 242|[0x80009b9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001f50]:csrrs tp, fcsr, zero<br> [0x80001f54]:fsw ft11, 904(ra)<br>  |
| 243|[0x80009ba4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001f70]:csrrs tp, fcsr, zero<br> [0x80001f74]:fsw ft11, 912(ra)<br>  |
| 244|[0x80009bac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001f90]:csrrs tp, fcsr, zero<br> [0x80001f94]:fsw ft11, 920(ra)<br>  |
| 245|[0x80009bb4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fac]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001fb0]:csrrs tp, fcsr, zero<br> [0x80001fb4]:fsw ft11, 928(ra)<br>  |
| 246|[0x80009bbc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fcc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001fd0]:csrrs tp, fcsr, zero<br> [0x80001fd4]:fsw ft11, 936(ra)<br>  |
| 247|[0x80009bc4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80001ff0]:csrrs tp, fcsr, zero<br> [0x80001ff4]:fsw ft11, 944(ra)<br>  |
| 248|[0x80009bcc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000200c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002010]:csrrs tp, fcsr, zero<br> [0x80002014]:fsw ft11, 952(ra)<br>  |
| 249|[0x80009bd4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000202c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002030]:csrrs tp, fcsr, zero<br> [0x80002034]:fsw ft11, 960(ra)<br>  |
| 250|[0x80009bdc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000204c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002050]:csrrs tp, fcsr, zero<br> [0x80002054]:fsw ft11, 968(ra)<br>  |
| 251|[0x80009be4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000206c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002070]:csrrs tp, fcsr, zero<br> [0x80002074]:fsw ft11, 976(ra)<br>  |
| 252|[0x80009bec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000208c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002090]:csrrs tp, fcsr, zero<br> [0x80002094]:fsw ft11, 984(ra)<br>  |
| 253|[0x80009bf4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800020b0]:csrrs tp, fcsr, zero<br> [0x800020b4]:fsw ft11, 992(ra)<br>  |
| 254|[0x80009bfc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800020d0]:csrrs tp, fcsr, zero<br> [0x800020d4]:fsw ft11, 1000(ra)<br> |
| 255|[0x80009c04]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800020f0]:csrrs tp, fcsr, zero<br> [0x800020f4]:fsw ft11, 1008(ra)<br> |
| 256|[0x80009c0c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000210c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002110]:csrrs tp, fcsr, zero<br> [0x80002114]:fsw ft11, 1016(ra)<br> |
| 257|[0x80009c14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002154]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002158]:csrrs tp, fcsr, zero<br> [0x8000215c]:fsw ft11, 0(ra)<br>    |
| 258|[0x80009c1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002194]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002198]:csrrs tp, fcsr, zero<br> [0x8000219c]:fsw ft11, 8(ra)<br>    |
| 259|[0x80009c24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800021d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800021d8]:csrrs tp, fcsr, zero<br> [0x800021dc]:fsw ft11, 16(ra)<br>   |
| 260|[0x80009c2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002214]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002218]:csrrs tp, fcsr, zero<br> [0x8000221c]:fsw ft11, 24(ra)<br>   |
| 261|[0x80009c34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002254]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002258]:csrrs tp, fcsr, zero<br> [0x8000225c]:fsw ft11, 32(ra)<br>   |
| 262|[0x80009c3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002294]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002298]:csrrs tp, fcsr, zero<br> [0x8000229c]:fsw ft11, 40(ra)<br>   |
| 263|[0x80009c44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800022d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800022d8]:csrrs tp, fcsr, zero<br> [0x800022dc]:fsw ft11, 48(ra)<br>   |
| 264|[0x80009c4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002314]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002318]:csrrs tp, fcsr, zero<br> [0x8000231c]:fsw ft11, 56(ra)<br>   |
| 265|[0x80009c54]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002354]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002358]:csrrs tp, fcsr, zero<br> [0x8000235c]:fsw ft11, 64(ra)<br>   |
| 266|[0x80009c5c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002394]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002398]:csrrs tp, fcsr, zero<br> [0x8000239c]:fsw ft11, 72(ra)<br>   |
| 267|[0x80009c64]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800023d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800023d8]:csrrs tp, fcsr, zero<br> [0x800023dc]:fsw ft11, 80(ra)<br>   |
| 268|[0x80009c6c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002414]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002418]:csrrs tp, fcsr, zero<br> [0x8000241c]:fsw ft11, 88(ra)<br>   |
| 269|[0x80009c74]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002454]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002458]:csrrs tp, fcsr, zero<br> [0x8000245c]:fsw ft11, 96(ra)<br>   |
| 270|[0x80009c7c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002494]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002498]:csrrs tp, fcsr, zero<br> [0x8000249c]:fsw ft11, 104(ra)<br>  |
| 271|[0x80009c84]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800024d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800024d8]:csrrs tp, fcsr, zero<br> [0x800024dc]:fsw ft11, 112(ra)<br>  |
| 272|[0x80009c8c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002514]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002518]:csrrs tp, fcsr, zero<br> [0x8000251c]:fsw ft11, 120(ra)<br>  |
| 273|[0x80009c94]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002554]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002558]:csrrs tp, fcsr, zero<br> [0x8000255c]:fsw ft11, 128(ra)<br>  |
| 274|[0x80009c9c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002594]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002598]:csrrs tp, fcsr, zero<br> [0x8000259c]:fsw ft11, 136(ra)<br>  |
| 275|[0x80009ca4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800025d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800025d8]:csrrs tp, fcsr, zero<br> [0x800025dc]:fsw ft11, 144(ra)<br>  |
| 276|[0x80009cac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002614]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002618]:csrrs tp, fcsr, zero<br> [0x8000261c]:fsw ft11, 152(ra)<br>  |
| 277|[0x80009cb4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002654]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002658]:csrrs tp, fcsr, zero<br> [0x8000265c]:fsw ft11, 160(ra)<br>  |
| 278|[0x80009cbc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002694]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002698]:csrrs tp, fcsr, zero<br> [0x8000269c]:fsw ft11, 168(ra)<br>  |
| 279|[0x80009cc4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800026d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800026d8]:csrrs tp, fcsr, zero<br> [0x800026dc]:fsw ft11, 176(ra)<br>  |
| 280|[0x80009ccc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002714]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002718]:csrrs tp, fcsr, zero<br> [0x8000271c]:fsw ft11, 184(ra)<br>  |
| 281|[0x80009cd4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002754]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002758]:csrrs tp, fcsr, zero<br> [0x8000275c]:fsw ft11, 192(ra)<br>  |
| 282|[0x80009cdc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002794]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002798]:csrrs tp, fcsr, zero<br> [0x8000279c]:fsw ft11, 200(ra)<br>  |
| 283|[0x80009ce4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800027d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800027d8]:csrrs tp, fcsr, zero<br> [0x800027dc]:fsw ft11, 208(ra)<br>  |
| 284|[0x80009cec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002814]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002818]:csrrs tp, fcsr, zero<br> [0x8000281c]:fsw ft11, 216(ra)<br>  |
| 285|[0x80009cf4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002854]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002858]:csrrs tp, fcsr, zero<br> [0x8000285c]:fsw ft11, 224(ra)<br>  |
| 286|[0x80009cfc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002894]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002898]:csrrs tp, fcsr, zero<br> [0x8000289c]:fsw ft11, 232(ra)<br>  |
| 287|[0x80009d04]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800028d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800028d8]:csrrs tp, fcsr, zero<br> [0x800028dc]:fsw ft11, 240(ra)<br>  |
| 288|[0x80009d0c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002914]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002918]:csrrs tp, fcsr, zero<br> [0x8000291c]:fsw ft11, 248(ra)<br>  |
| 289|[0x80009d14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002954]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002958]:csrrs tp, fcsr, zero<br> [0x8000295c]:fsw ft11, 256(ra)<br>  |
| 290|[0x80009d1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002994]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002998]:csrrs tp, fcsr, zero<br> [0x8000299c]:fsw ft11, 264(ra)<br>  |
| 291|[0x80009d24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800029d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800029d8]:csrrs tp, fcsr, zero<br> [0x800029dc]:fsw ft11, 272(ra)<br>  |
| 292|[0x80009d2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002a14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002a18]:csrrs tp, fcsr, zero<br> [0x80002a1c]:fsw ft11, 280(ra)<br>  |
| 293|[0x80009d34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002a54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002a58]:csrrs tp, fcsr, zero<br> [0x80002a5c]:fsw ft11, 288(ra)<br>  |
| 294|[0x80009d3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002a94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002a98]:csrrs tp, fcsr, zero<br> [0x80002a9c]:fsw ft11, 296(ra)<br>  |
| 295|[0x80009d44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ad4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002ad8]:csrrs tp, fcsr, zero<br> [0x80002adc]:fsw ft11, 304(ra)<br>  |
| 296|[0x80009d4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002b18]:csrrs tp, fcsr, zero<br> [0x80002b1c]:fsw ft11, 312(ra)<br>  |
| 297|[0x80009d54]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002b58]:csrrs tp, fcsr, zero<br> [0x80002b5c]:fsw ft11, 320(ra)<br>  |
| 298|[0x80009d5c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002b98]:csrrs tp, fcsr, zero<br> [0x80002b9c]:fsw ft11, 328(ra)<br>  |
| 299|[0x80009d64]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002bd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002bd8]:csrrs tp, fcsr, zero<br> [0x80002bdc]:fsw ft11, 336(ra)<br>  |
| 300|[0x80009d6c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002c18]:csrrs tp, fcsr, zero<br> [0x80002c1c]:fsw ft11, 344(ra)<br>  |
| 301|[0x80009d74]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002c58]:csrrs tp, fcsr, zero<br> [0x80002c5c]:fsw ft11, 352(ra)<br>  |
| 302|[0x80009d7c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002c98]:csrrs tp, fcsr, zero<br> [0x80002c9c]:fsw ft11, 360(ra)<br>  |
| 303|[0x80009d84]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002cd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002cd8]:csrrs tp, fcsr, zero<br> [0x80002cdc]:fsw ft11, 368(ra)<br>  |
| 304|[0x80009d8c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002d18]:csrrs tp, fcsr, zero<br> [0x80002d1c]:fsw ft11, 376(ra)<br>  |
| 305|[0x80009d94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002d58]:csrrs tp, fcsr, zero<br> [0x80002d5c]:fsw ft11, 384(ra)<br>  |
| 306|[0x80009d9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002d98]:csrrs tp, fcsr, zero<br> [0x80002d9c]:fsw ft11, 392(ra)<br>  |
| 307|[0x80009da4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002dd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002dd8]:csrrs tp, fcsr, zero<br> [0x80002ddc]:fsw ft11, 400(ra)<br>  |
| 308|[0x80009dac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002e14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002e18]:csrrs tp, fcsr, zero<br> [0x80002e1c]:fsw ft11, 408(ra)<br>  |
| 309|[0x80009db4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002e54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002e58]:csrrs tp, fcsr, zero<br> [0x80002e5c]:fsw ft11, 416(ra)<br>  |
| 310|[0x80009dbc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002e94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002e98]:csrrs tp, fcsr, zero<br> [0x80002e9c]:fsw ft11, 424(ra)<br>  |
| 311|[0x80009dc4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ed4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002ed8]:csrrs tp, fcsr, zero<br> [0x80002edc]:fsw ft11, 432(ra)<br>  |
| 312|[0x80009dcc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002f14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002f18]:csrrs tp, fcsr, zero<br> [0x80002f1c]:fsw ft11, 440(ra)<br>  |
| 313|[0x80009dd4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002f54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002f58]:csrrs tp, fcsr, zero<br> [0x80002f5c]:fsw ft11, 448(ra)<br>  |
| 314|[0x80009ddc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002f94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002f98]:csrrs tp, fcsr, zero<br> [0x80002f9c]:fsw ft11, 456(ra)<br>  |
| 315|[0x80009de4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002fd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80002fd8]:csrrs tp, fcsr, zero<br> [0x80002fdc]:fsw ft11, 464(ra)<br>  |
| 316|[0x80009dec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003014]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003018]:csrrs tp, fcsr, zero<br> [0x8000301c]:fsw ft11, 472(ra)<br>  |
| 317|[0x80009df4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003054]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003058]:csrrs tp, fcsr, zero<br> [0x8000305c]:fsw ft11, 480(ra)<br>  |
| 318|[0x80009dfc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003094]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003098]:csrrs tp, fcsr, zero<br> [0x8000309c]:fsw ft11, 488(ra)<br>  |
| 319|[0x80009e04]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800030d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800030d8]:csrrs tp, fcsr, zero<br> [0x800030dc]:fsw ft11, 496(ra)<br>  |
| 320|[0x80009e0c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003114]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003118]:csrrs tp, fcsr, zero<br> [0x8000311c]:fsw ft11, 504(ra)<br>  |
| 321|[0x80009e14]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003154]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003158]:csrrs tp, fcsr, zero<br> [0x8000315c]:fsw ft11, 512(ra)<br>  |
| 322|[0x80009e1c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003194]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003198]:csrrs tp, fcsr, zero<br> [0x8000319c]:fsw ft11, 520(ra)<br>  |
| 323|[0x80009e24]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800031d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800031d8]:csrrs tp, fcsr, zero<br> [0x800031dc]:fsw ft11, 528(ra)<br>  |
| 324|[0x80009e2c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003214]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003218]:csrrs tp, fcsr, zero<br> [0x8000321c]:fsw ft11, 536(ra)<br>  |
| 325|[0x80009e34]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003254]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003258]:csrrs tp, fcsr, zero<br> [0x8000325c]:fsw ft11, 544(ra)<br>  |
| 326|[0x80009e3c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003294]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003298]:csrrs tp, fcsr, zero<br> [0x8000329c]:fsw ft11, 552(ra)<br>  |
| 327|[0x80009e44]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800032d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800032d8]:csrrs tp, fcsr, zero<br> [0x800032dc]:fsw ft11, 560(ra)<br>  |
| 328|[0x80009e4c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003314]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003318]:csrrs tp, fcsr, zero<br> [0x8000331c]:fsw ft11, 568(ra)<br>  |
| 329|[0x80009e54]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003354]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003358]:csrrs tp, fcsr, zero<br> [0x8000335c]:fsw ft11, 576(ra)<br>  |
| 330|[0x80009e5c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003394]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003398]:csrrs tp, fcsr, zero<br> [0x8000339c]:fsw ft11, 584(ra)<br>  |
| 331|[0x80009e64]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800033d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800033d8]:csrrs tp, fcsr, zero<br> [0x800033dc]:fsw ft11, 592(ra)<br>  |
| 332|[0x80009e6c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003414]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003418]:csrrs tp, fcsr, zero<br> [0x8000341c]:fsw ft11, 600(ra)<br>  |
| 333|[0x80009e74]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003454]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003458]:csrrs tp, fcsr, zero<br> [0x8000345c]:fsw ft11, 608(ra)<br>  |
| 334|[0x80009e7c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003494]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003498]:csrrs tp, fcsr, zero<br> [0x8000349c]:fsw ft11, 616(ra)<br>  |
| 335|[0x80009e84]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800034d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800034d8]:csrrs tp, fcsr, zero<br> [0x800034dc]:fsw ft11, 624(ra)<br>  |
| 336|[0x80009e8c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003514]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003518]:csrrs tp, fcsr, zero<br> [0x8000351c]:fsw ft11, 632(ra)<br>  |
| 337|[0x80009e94]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003554]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003558]:csrrs tp, fcsr, zero<br> [0x8000355c]:fsw ft11, 640(ra)<br>  |
| 338|[0x80009e9c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003594]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003598]:csrrs tp, fcsr, zero<br> [0x8000359c]:fsw ft11, 648(ra)<br>  |
| 339|[0x80009ea4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800035d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800035d8]:csrrs tp, fcsr, zero<br> [0x800035dc]:fsw ft11, 656(ra)<br>  |
| 340|[0x80009eac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003614]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003618]:csrrs tp, fcsr, zero<br> [0x8000361c]:fsw ft11, 664(ra)<br>  |
| 341|[0x80009eb4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003654]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003658]:csrrs tp, fcsr, zero<br> [0x8000365c]:fsw ft11, 672(ra)<br>  |
| 342|[0x80009ebc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003694]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003698]:csrrs tp, fcsr, zero<br> [0x8000369c]:fsw ft11, 680(ra)<br>  |
| 343|[0x80009ec4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800036d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800036d8]:csrrs tp, fcsr, zero<br> [0x800036dc]:fsw ft11, 688(ra)<br>  |
| 344|[0x80009ecc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003714]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003718]:csrrs tp, fcsr, zero<br> [0x8000371c]:fsw ft11, 696(ra)<br>  |
| 345|[0x80009ed4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003754]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003758]:csrrs tp, fcsr, zero<br> [0x8000375c]:fsw ft11, 704(ra)<br>  |
| 346|[0x80009edc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003794]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003798]:csrrs tp, fcsr, zero<br> [0x8000379c]:fsw ft11, 712(ra)<br>  |
| 347|[0x80009ee4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800037d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800037d8]:csrrs tp, fcsr, zero<br> [0x800037dc]:fsw ft11, 720(ra)<br>  |
| 348|[0x80009eec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003814]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003818]:csrrs tp, fcsr, zero<br> [0x8000381c]:fsw ft11, 728(ra)<br>  |
| 349|[0x80009ef4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003854]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003858]:csrrs tp, fcsr, zero<br> [0x8000385c]:fsw ft11, 736(ra)<br>  |
| 350|[0x80009efc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003894]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003898]:csrrs tp, fcsr, zero<br> [0x8000389c]:fsw ft11, 744(ra)<br>  |
| 351|[0x80009f04]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800038d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800038d8]:csrrs tp, fcsr, zero<br> [0x800038dc]:fsw ft11, 752(ra)<br>  |
| 352|[0x80009f0c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003914]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003918]:csrrs tp, fcsr, zero<br> [0x8000391c]:fsw ft11, 760(ra)<br>  |
| 353|[0x80009f14]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003954]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003958]:csrrs tp, fcsr, zero<br> [0x8000395c]:fsw ft11, 768(ra)<br>  |
| 354|[0x80009f1c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003994]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003998]:csrrs tp, fcsr, zero<br> [0x8000399c]:fsw ft11, 776(ra)<br>  |
| 355|[0x80009f24]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800039d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800039d8]:csrrs tp, fcsr, zero<br> [0x800039dc]:fsw ft11, 784(ra)<br>  |
| 356|[0x80009f2c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003a18]:csrrs tp, fcsr, zero<br> [0x80003a1c]:fsw ft11, 792(ra)<br>  |
| 357|[0x80009f34]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003a58]:csrrs tp, fcsr, zero<br> [0x80003a5c]:fsw ft11, 800(ra)<br>  |
| 358|[0x80009f3c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003a98]:csrrs tp, fcsr, zero<br> [0x80003a9c]:fsw ft11, 808(ra)<br>  |
| 359|[0x80009f44]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ad4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003ad8]:csrrs tp, fcsr, zero<br> [0x80003adc]:fsw ft11, 816(ra)<br>  |
| 360|[0x80009f4c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003b18]:csrrs tp, fcsr, zero<br> [0x80003b1c]:fsw ft11, 824(ra)<br>  |
| 361|[0x80009f54]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003b58]:csrrs tp, fcsr, zero<br> [0x80003b5c]:fsw ft11, 832(ra)<br>  |
| 362|[0x80009f5c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003b98]:csrrs tp, fcsr, zero<br> [0x80003b9c]:fsw ft11, 840(ra)<br>  |
| 363|[0x80009f64]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003bd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003bd8]:csrrs tp, fcsr, zero<br> [0x80003bdc]:fsw ft11, 848(ra)<br>  |
| 364|[0x80009f6c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003c18]:csrrs tp, fcsr, zero<br> [0x80003c1c]:fsw ft11, 856(ra)<br>  |
| 365|[0x80009f74]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003c58]:csrrs tp, fcsr, zero<br> [0x80003c5c]:fsw ft11, 864(ra)<br>  |
| 366|[0x80009f7c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003c98]:csrrs tp, fcsr, zero<br> [0x80003c9c]:fsw ft11, 872(ra)<br>  |
| 367|[0x80009f84]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003cd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003cd8]:csrrs tp, fcsr, zero<br> [0x80003cdc]:fsw ft11, 880(ra)<br>  |
| 368|[0x80009f8c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003d14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003d18]:csrrs tp, fcsr, zero<br> [0x80003d1c]:fsw ft11, 888(ra)<br>  |
| 369|[0x80009f94]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003d54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003d58]:csrrs tp, fcsr, zero<br> [0x80003d5c]:fsw ft11, 896(ra)<br>  |
| 370|[0x80009f9c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003d94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003d98]:csrrs tp, fcsr, zero<br> [0x80003d9c]:fsw ft11, 904(ra)<br>  |
| 371|[0x80009fa4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003dd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003dd8]:csrrs tp, fcsr, zero<br> [0x80003ddc]:fsw ft11, 912(ra)<br>  |
| 372|[0x80009fac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003e14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003e18]:csrrs tp, fcsr, zero<br> [0x80003e1c]:fsw ft11, 920(ra)<br>  |
| 373|[0x80009fb4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003e54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003e58]:csrrs tp, fcsr, zero<br> [0x80003e5c]:fsw ft11, 928(ra)<br>  |
| 374|[0x80009fbc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003e94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003e98]:csrrs tp, fcsr, zero<br> [0x80003e9c]:fsw ft11, 936(ra)<br>  |
| 375|[0x80009fc4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ed4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003ed8]:csrrs tp, fcsr, zero<br> [0x80003edc]:fsw ft11, 944(ra)<br>  |
| 376|[0x80009fcc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003f18]:csrrs tp, fcsr, zero<br> [0x80003f1c]:fsw ft11, 952(ra)<br>  |
| 377|[0x80009fd4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003f58]:csrrs tp, fcsr, zero<br> [0x80003f5c]:fsw ft11, 960(ra)<br>  |
| 378|[0x80009fdc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003f98]:csrrs tp, fcsr, zero<br> [0x80003f9c]:fsw ft11, 968(ra)<br>  |
| 379|[0x80009fe4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003fd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80003fd8]:csrrs tp, fcsr, zero<br> [0x80003fdc]:fsw ft11, 976(ra)<br>  |
| 380|[0x80009fec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004014]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004018]:csrrs tp, fcsr, zero<br> [0x8000401c]:fsw ft11, 984(ra)<br>  |
| 381|[0x80009ff4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004054]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004058]:csrrs tp, fcsr, zero<br> [0x8000405c]:fsw ft11, 992(ra)<br>  |
| 382|[0x80009ffc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004094]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004098]:csrrs tp, fcsr, zero<br> [0x8000409c]:fsw ft11, 1000(ra)<br> |
| 383|[0x8000a004]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800040d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800040d8]:csrrs tp, fcsr, zero<br> [0x800040dc]:fsw ft11, 1008(ra)<br> |
| 384|[0x8000a00c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004114]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004118]:csrrs tp, fcsr, zero<br> [0x8000411c]:fsw ft11, 1016(ra)<br> |
| 385|[0x8000a014]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000415c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004160]:csrrs tp, fcsr, zero<br> [0x80004164]:fsw ft11, 0(ra)<br>    |
| 386|[0x8000a01c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000419c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800041a0]:csrrs tp, fcsr, zero<br> [0x800041a4]:fsw ft11, 8(ra)<br>    |
| 387|[0x8000a024]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800041dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800041e0]:csrrs tp, fcsr, zero<br> [0x800041e4]:fsw ft11, 16(ra)<br>   |
| 388|[0x8000a02c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000421c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004220]:csrrs tp, fcsr, zero<br> [0x80004224]:fsw ft11, 24(ra)<br>   |
| 389|[0x8000a034]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000425c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004260]:csrrs tp, fcsr, zero<br> [0x80004264]:fsw ft11, 32(ra)<br>   |
| 390|[0x8000a03c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000429c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800042a0]:csrrs tp, fcsr, zero<br> [0x800042a4]:fsw ft11, 40(ra)<br>   |
| 391|[0x8000a044]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800042dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800042e0]:csrrs tp, fcsr, zero<br> [0x800042e4]:fsw ft11, 48(ra)<br>   |
| 392|[0x8000a04c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000431c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004320]:csrrs tp, fcsr, zero<br> [0x80004324]:fsw ft11, 56(ra)<br>   |
| 393|[0x8000a054]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000435c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004360]:csrrs tp, fcsr, zero<br> [0x80004364]:fsw ft11, 64(ra)<br>   |
| 394|[0x8000a05c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000439c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800043a0]:csrrs tp, fcsr, zero<br> [0x800043a4]:fsw ft11, 72(ra)<br>   |
| 395|[0x8000a064]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800043dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800043e0]:csrrs tp, fcsr, zero<br> [0x800043e4]:fsw ft11, 80(ra)<br>   |
| 396|[0x8000a06c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000441c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004420]:csrrs tp, fcsr, zero<br> [0x80004424]:fsw ft11, 88(ra)<br>   |
| 397|[0x8000a074]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000445c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004460]:csrrs tp, fcsr, zero<br> [0x80004464]:fsw ft11, 96(ra)<br>   |
| 398|[0x8000a07c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000449c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800044a0]:csrrs tp, fcsr, zero<br> [0x800044a4]:fsw ft11, 104(ra)<br>  |
| 399|[0x8000a084]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800044dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800044e0]:csrrs tp, fcsr, zero<br> [0x800044e4]:fsw ft11, 112(ra)<br>  |
| 400|[0x8000a08c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000451c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004520]:csrrs tp, fcsr, zero<br> [0x80004524]:fsw ft11, 120(ra)<br>  |
| 401|[0x8000a094]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000455c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004560]:csrrs tp, fcsr, zero<br> [0x80004564]:fsw ft11, 128(ra)<br>  |
| 402|[0x8000a09c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000459c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800045a0]:csrrs tp, fcsr, zero<br> [0x800045a4]:fsw ft11, 136(ra)<br>  |
| 403|[0x8000a0a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800045dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800045e0]:csrrs tp, fcsr, zero<br> [0x800045e4]:fsw ft11, 144(ra)<br>  |
| 404|[0x8000a0ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000461c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004620]:csrrs tp, fcsr, zero<br> [0x80004624]:fsw ft11, 152(ra)<br>  |
| 405|[0x8000a0b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000465c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004660]:csrrs tp, fcsr, zero<br> [0x80004664]:fsw ft11, 160(ra)<br>  |
| 406|[0x8000a0bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000469c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800046a0]:csrrs tp, fcsr, zero<br> [0x800046a4]:fsw ft11, 168(ra)<br>  |
| 407|[0x8000a0c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800046dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800046e0]:csrrs tp, fcsr, zero<br> [0x800046e4]:fsw ft11, 176(ra)<br>  |
| 408|[0x8000a0cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000471c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004720]:csrrs tp, fcsr, zero<br> [0x80004724]:fsw ft11, 184(ra)<br>  |
| 409|[0x8000a0d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000475c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004760]:csrrs tp, fcsr, zero<br> [0x80004764]:fsw ft11, 192(ra)<br>  |
| 410|[0x8000a0dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000479c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800047a0]:csrrs tp, fcsr, zero<br> [0x800047a4]:fsw ft11, 200(ra)<br>  |
| 411|[0x8000a0e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800047dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800047e0]:csrrs tp, fcsr, zero<br> [0x800047e4]:fsw ft11, 208(ra)<br>  |
| 412|[0x8000a0ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000481c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004820]:csrrs tp, fcsr, zero<br> [0x80004824]:fsw ft11, 216(ra)<br>  |
| 413|[0x8000a0f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000485c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004860]:csrrs tp, fcsr, zero<br> [0x80004864]:fsw ft11, 224(ra)<br>  |
| 414|[0x8000a0fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000489c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800048a0]:csrrs tp, fcsr, zero<br> [0x800048a4]:fsw ft11, 232(ra)<br>  |
| 415|[0x8000a104]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800048dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800048e0]:csrrs tp, fcsr, zero<br> [0x800048e4]:fsw ft11, 240(ra)<br>  |
| 416|[0x8000a10c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000491c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004920]:csrrs tp, fcsr, zero<br> [0x80004924]:fsw ft11, 248(ra)<br>  |
| 417|[0x8000a114]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000495c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004960]:csrrs tp, fcsr, zero<br> [0x80004964]:fsw ft11, 256(ra)<br>  |
| 418|[0x8000a11c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000499c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800049a0]:csrrs tp, fcsr, zero<br> [0x800049a4]:fsw ft11, 264(ra)<br>  |
| 419|[0x8000a124]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800049dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800049e0]:csrrs tp, fcsr, zero<br> [0x800049e4]:fsw ft11, 272(ra)<br>  |
| 420|[0x8000a12c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004a20]:csrrs tp, fcsr, zero<br> [0x80004a24]:fsw ft11, 280(ra)<br>  |
| 421|[0x8000a134]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004a60]:csrrs tp, fcsr, zero<br> [0x80004a64]:fsw ft11, 288(ra)<br>  |
| 422|[0x8000a13c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004aa0]:csrrs tp, fcsr, zero<br> [0x80004aa4]:fsw ft11, 296(ra)<br>  |
| 423|[0x8000a144]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004adc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ae0]:csrrs tp, fcsr, zero<br> [0x80004ae4]:fsw ft11, 304(ra)<br>  |
| 424|[0x8000a14c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004b20]:csrrs tp, fcsr, zero<br> [0x80004b24]:fsw ft11, 312(ra)<br>  |
| 425|[0x8000a154]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004b60]:csrrs tp, fcsr, zero<br> [0x80004b64]:fsw ft11, 320(ra)<br>  |
| 426|[0x8000a15c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ba0]:csrrs tp, fcsr, zero<br> [0x80004ba4]:fsw ft11, 328(ra)<br>  |
| 427|[0x8000a164]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004bdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004be0]:csrrs tp, fcsr, zero<br> [0x80004be4]:fsw ft11, 336(ra)<br>  |
| 428|[0x8000a16c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004c1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004c20]:csrrs tp, fcsr, zero<br> [0x80004c24]:fsw ft11, 344(ra)<br>  |
| 429|[0x8000a174]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004c5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004c60]:csrrs tp, fcsr, zero<br> [0x80004c64]:fsw ft11, 352(ra)<br>  |
| 430|[0x8000a17c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004c9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ca0]:csrrs tp, fcsr, zero<br> [0x80004ca4]:fsw ft11, 360(ra)<br>  |
| 431|[0x8000a184]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004cdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ce0]:csrrs tp, fcsr, zero<br> [0x80004ce4]:fsw ft11, 368(ra)<br>  |
| 432|[0x8000a18c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004d1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004d20]:csrrs tp, fcsr, zero<br> [0x80004d24]:fsw ft11, 376(ra)<br>  |
| 433|[0x8000a194]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004d5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004d60]:csrrs tp, fcsr, zero<br> [0x80004d64]:fsw ft11, 384(ra)<br>  |
| 434|[0x8000a19c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004d9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004da0]:csrrs tp, fcsr, zero<br> [0x80004da4]:fsw ft11, 392(ra)<br>  |
| 435|[0x8000a1a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004ddc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004de0]:csrrs tp, fcsr, zero<br> [0x80004de4]:fsw ft11, 400(ra)<br>  |
| 436|[0x8000a1ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004e20]:csrrs tp, fcsr, zero<br> [0x80004e24]:fsw ft11, 408(ra)<br>  |
| 437|[0x8000a1b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004e60]:csrrs tp, fcsr, zero<br> [0x80004e64]:fsw ft11, 416(ra)<br>  |
| 438|[0x8000a1bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ea0]:csrrs tp, fcsr, zero<br> [0x80004ea4]:fsw ft11, 424(ra)<br>  |
| 439|[0x8000a1c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004edc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004ee0]:csrrs tp, fcsr, zero<br> [0x80004ee4]:fsw ft11, 432(ra)<br>  |
| 440|[0x8000a1cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004f20]:csrrs tp, fcsr, zero<br> [0x80004f24]:fsw ft11, 440(ra)<br>  |
| 441|[0x8000a1d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004f60]:csrrs tp, fcsr, zero<br> [0x80004f64]:fsw ft11, 448(ra)<br>  |
| 442|[0x8000a1dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004fa0]:csrrs tp, fcsr, zero<br> [0x80004fa4]:fsw ft11, 456(ra)<br>  |
| 443|[0x8000a1e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004fdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80004fe0]:csrrs tp, fcsr, zero<br> [0x80004fe4]:fsw ft11, 464(ra)<br>  |
| 444|[0x8000a1ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000501c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005020]:csrrs tp, fcsr, zero<br> [0x80005024]:fsw ft11, 472(ra)<br>  |
| 445|[0x8000a1f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000505c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005060]:csrrs tp, fcsr, zero<br> [0x80005064]:fsw ft11, 480(ra)<br>  |
| 446|[0x8000a1fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000509c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800050a0]:csrrs tp, fcsr, zero<br> [0x800050a4]:fsw ft11, 488(ra)<br>  |
| 447|[0x8000a204]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800050dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800050e0]:csrrs tp, fcsr, zero<br> [0x800050e4]:fsw ft11, 496(ra)<br>  |
| 448|[0x8000a20c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000511c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005120]:csrrs tp, fcsr, zero<br> [0x80005124]:fsw ft11, 504(ra)<br>  |
| 449|[0x8000a214]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000515c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005160]:csrrs tp, fcsr, zero<br> [0x80005164]:fsw ft11, 512(ra)<br>  |
| 450|[0x8000a21c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000519c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800051a0]:csrrs tp, fcsr, zero<br> [0x800051a4]:fsw ft11, 520(ra)<br>  |
| 451|[0x8000a224]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800051dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800051e0]:csrrs tp, fcsr, zero<br> [0x800051e4]:fsw ft11, 528(ra)<br>  |
| 452|[0x8000a22c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000521c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005220]:csrrs tp, fcsr, zero<br> [0x80005224]:fsw ft11, 536(ra)<br>  |
| 453|[0x8000a234]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000525c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005260]:csrrs tp, fcsr, zero<br> [0x80005264]:fsw ft11, 544(ra)<br>  |
| 454|[0x8000a23c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000529c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800052a0]:csrrs tp, fcsr, zero<br> [0x800052a4]:fsw ft11, 552(ra)<br>  |
| 455|[0x8000a244]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800052dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800052e0]:csrrs tp, fcsr, zero<br> [0x800052e4]:fsw ft11, 560(ra)<br>  |
| 456|[0x8000a24c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000531c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005320]:csrrs tp, fcsr, zero<br> [0x80005324]:fsw ft11, 568(ra)<br>  |
| 457|[0x8000a254]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000535c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005360]:csrrs tp, fcsr, zero<br> [0x80005364]:fsw ft11, 576(ra)<br>  |
| 458|[0x8000a25c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000539c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800053a0]:csrrs tp, fcsr, zero<br> [0x800053a4]:fsw ft11, 584(ra)<br>  |
| 459|[0x8000a264]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800053dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800053e0]:csrrs tp, fcsr, zero<br> [0x800053e4]:fsw ft11, 592(ra)<br>  |
| 460|[0x8000a26c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000541c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005420]:csrrs tp, fcsr, zero<br> [0x80005424]:fsw ft11, 600(ra)<br>  |
| 461|[0x8000a274]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000545c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005460]:csrrs tp, fcsr, zero<br> [0x80005464]:fsw ft11, 608(ra)<br>  |
| 462|[0x8000a27c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000549c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800054a0]:csrrs tp, fcsr, zero<br> [0x800054a4]:fsw ft11, 616(ra)<br>  |
| 463|[0x8000a284]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800054dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800054e0]:csrrs tp, fcsr, zero<br> [0x800054e4]:fsw ft11, 624(ra)<br>  |
| 464|[0x8000a28c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000551c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005520]:csrrs tp, fcsr, zero<br> [0x80005524]:fsw ft11, 632(ra)<br>  |
| 465|[0x8000a294]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000555c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005560]:csrrs tp, fcsr, zero<br> [0x80005564]:fsw ft11, 640(ra)<br>  |
| 466|[0x8000a29c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000559c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800055a0]:csrrs tp, fcsr, zero<br> [0x800055a4]:fsw ft11, 648(ra)<br>  |
| 467|[0x8000a2a4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800055dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800055e0]:csrrs tp, fcsr, zero<br> [0x800055e4]:fsw ft11, 656(ra)<br>  |
| 468|[0x8000a2ac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000561c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005620]:csrrs tp, fcsr, zero<br> [0x80005624]:fsw ft11, 664(ra)<br>  |
| 469|[0x8000a2b4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000565c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005660]:csrrs tp, fcsr, zero<br> [0x80005664]:fsw ft11, 672(ra)<br>  |
| 470|[0x8000a2bc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000569c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800056a0]:csrrs tp, fcsr, zero<br> [0x800056a4]:fsw ft11, 680(ra)<br>  |
| 471|[0x8000a2c4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800056dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800056e0]:csrrs tp, fcsr, zero<br> [0x800056e4]:fsw ft11, 688(ra)<br>  |
| 472|[0x8000a2cc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000571c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005720]:csrrs tp, fcsr, zero<br> [0x80005724]:fsw ft11, 696(ra)<br>  |
| 473|[0x8000a2d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000575c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005760]:csrrs tp, fcsr, zero<br> [0x80005764]:fsw ft11, 704(ra)<br>  |
| 474|[0x8000a2dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000579c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800057a0]:csrrs tp, fcsr, zero<br> [0x800057a4]:fsw ft11, 712(ra)<br>  |
| 475|[0x8000a2e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800057dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800057e0]:csrrs tp, fcsr, zero<br> [0x800057e4]:fsw ft11, 720(ra)<br>  |
| 476|[0x8000a2ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000581c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005820]:csrrs tp, fcsr, zero<br> [0x80005824]:fsw ft11, 728(ra)<br>  |
| 477|[0x8000a2f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000585c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005860]:csrrs tp, fcsr, zero<br> [0x80005864]:fsw ft11, 736(ra)<br>  |
| 478|[0x8000a2fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000589c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800058a0]:csrrs tp, fcsr, zero<br> [0x800058a4]:fsw ft11, 744(ra)<br>  |
| 479|[0x8000a304]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800058dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800058e0]:csrrs tp, fcsr, zero<br> [0x800058e4]:fsw ft11, 752(ra)<br>  |
| 480|[0x8000a30c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000591c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005920]:csrrs tp, fcsr, zero<br> [0x80005924]:fsw ft11, 760(ra)<br>  |
| 481|[0x8000a314]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000595c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005960]:csrrs tp, fcsr, zero<br> [0x80005964]:fsw ft11, 768(ra)<br>  |
| 482|[0x8000a31c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000599c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800059a0]:csrrs tp, fcsr, zero<br> [0x800059a4]:fsw ft11, 776(ra)<br>  |
| 483|[0x8000a324]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800059dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800059e0]:csrrs tp, fcsr, zero<br> [0x800059e4]:fsw ft11, 784(ra)<br>  |
| 484|[0x8000a32c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005a20]:csrrs tp, fcsr, zero<br> [0x80005a24]:fsw ft11, 792(ra)<br>  |
| 485|[0x8000a334]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005a60]:csrrs tp, fcsr, zero<br> [0x80005a64]:fsw ft11, 800(ra)<br>  |
| 486|[0x8000a33c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005aa0]:csrrs tp, fcsr, zero<br> [0x80005aa4]:fsw ft11, 808(ra)<br>  |
| 487|[0x8000a344]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005adc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ae0]:csrrs tp, fcsr, zero<br> [0x80005ae4]:fsw ft11, 816(ra)<br>  |
| 488|[0x8000a34c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005b1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005b20]:csrrs tp, fcsr, zero<br> [0x80005b24]:fsw ft11, 824(ra)<br>  |
| 489|[0x8000a354]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005b5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005b60]:csrrs tp, fcsr, zero<br> [0x80005b64]:fsw ft11, 832(ra)<br>  |
| 490|[0x8000a35c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005b9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ba0]:csrrs tp, fcsr, zero<br> [0x80005ba4]:fsw ft11, 840(ra)<br>  |
| 491|[0x8000a364]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005bdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005be0]:csrrs tp, fcsr, zero<br> [0x80005be4]:fsw ft11, 848(ra)<br>  |
| 492|[0x8000a36c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005c1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005c20]:csrrs tp, fcsr, zero<br> [0x80005c24]:fsw ft11, 856(ra)<br>  |
| 493|[0x8000a374]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005c5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005c60]:csrrs tp, fcsr, zero<br> [0x80005c64]:fsw ft11, 864(ra)<br>  |
| 494|[0x8000a37c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005c9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ca0]:csrrs tp, fcsr, zero<br> [0x80005ca4]:fsw ft11, 872(ra)<br>  |
| 495|[0x8000a384]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005cdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ce0]:csrrs tp, fcsr, zero<br> [0x80005ce4]:fsw ft11, 880(ra)<br>  |
| 496|[0x8000a38c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005d20]:csrrs tp, fcsr, zero<br> [0x80005d24]:fsw ft11, 888(ra)<br>  |
| 497|[0x8000a394]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005d60]:csrrs tp, fcsr, zero<br> [0x80005d64]:fsw ft11, 896(ra)<br>  |
| 498|[0x8000a39c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005da0]:csrrs tp, fcsr, zero<br> [0x80005da4]:fsw ft11, 904(ra)<br>  |
| 499|[0x8000a3a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005ddc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005de0]:csrrs tp, fcsr, zero<br> [0x80005de4]:fsw ft11, 912(ra)<br>  |
| 500|[0x8000a3ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005e20]:csrrs tp, fcsr, zero<br> [0x80005e24]:fsw ft11, 920(ra)<br>  |
| 501|[0x8000a3b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005e60]:csrrs tp, fcsr, zero<br> [0x80005e64]:fsw ft11, 928(ra)<br>  |
| 502|[0x8000a3bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ea0]:csrrs tp, fcsr, zero<br> [0x80005ea4]:fsw ft11, 936(ra)<br>  |
| 503|[0x8000a3c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005edc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005ee0]:csrrs tp, fcsr, zero<br> [0x80005ee4]:fsw ft11, 944(ra)<br>  |
| 504|[0x8000a3cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005f20]:csrrs tp, fcsr, zero<br> [0x80005f24]:fsw ft11, 952(ra)<br>  |
| 505|[0x8000a3d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f5c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005f60]:csrrs tp, fcsr, zero<br> [0x80005f64]:fsw ft11, 960(ra)<br>  |
| 506|[0x8000a3dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005fa0]:csrrs tp, fcsr, zero<br> [0x80005fa4]:fsw ft11, 968(ra)<br>  |
| 507|[0x8000a3e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005fdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80005fe0]:csrrs tp, fcsr, zero<br> [0x80005fe4]:fsw ft11, 976(ra)<br>  |
| 508|[0x8000a3ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000601c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006020]:csrrs tp, fcsr, zero<br> [0x80006024]:fsw ft11, 984(ra)<br>  |
| 509|[0x8000a3f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000605c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006060]:csrrs tp, fcsr, zero<br> [0x80006064]:fsw ft11, 992(ra)<br>  |
| 510|[0x8000a3fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000609c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800060a0]:csrrs tp, fcsr, zero<br> [0x800060a4]:fsw ft11, 1000(ra)<br> |
| 511|[0x8000a404]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800060dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800060e0]:csrrs tp, fcsr, zero<br> [0x800060e4]:fsw ft11, 1008(ra)<br> |
| 512|[0x8000a40c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000611c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006120]:csrrs tp, fcsr, zero<br> [0x80006124]:fsw ft11, 1016(ra)<br> |
| 513|[0x8000a414]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000615c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006160]:csrrs tp, fcsr, zero<br> [0x80006164]:fsw ft11, 0(ra)<br>    |
| 514|[0x8000a41c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006194]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006198]:csrrs tp, fcsr, zero<br> [0x8000619c]:fsw ft11, 8(ra)<br>    |
| 515|[0x8000a424]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800061cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800061d0]:csrrs tp, fcsr, zero<br> [0x800061d4]:fsw ft11, 16(ra)<br>   |
| 516|[0x8000a42c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006204]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006208]:csrrs tp, fcsr, zero<br> [0x8000620c]:fsw ft11, 24(ra)<br>   |
| 517|[0x8000a434]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000623c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006240]:csrrs tp, fcsr, zero<br> [0x80006244]:fsw ft11, 32(ra)<br>   |
| 518|[0x8000a43c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006274]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006278]:csrrs tp, fcsr, zero<br> [0x8000627c]:fsw ft11, 40(ra)<br>   |
| 519|[0x8000a444]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800062ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800062b0]:csrrs tp, fcsr, zero<br> [0x800062b4]:fsw ft11, 48(ra)<br>   |
| 520|[0x8000a44c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800062e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800062e8]:csrrs tp, fcsr, zero<br> [0x800062ec]:fsw ft11, 56(ra)<br>   |
| 521|[0x8000a454]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000631c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006320]:csrrs tp, fcsr, zero<br> [0x80006324]:fsw ft11, 64(ra)<br>   |
| 522|[0x8000a45c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006354]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006358]:csrrs tp, fcsr, zero<br> [0x8000635c]:fsw ft11, 72(ra)<br>   |
| 523|[0x8000a464]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000638c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006390]:csrrs tp, fcsr, zero<br> [0x80006394]:fsw ft11, 80(ra)<br>   |
| 524|[0x8000a46c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800063c4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800063c8]:csrrs tp, fcsr, zero<br> [0x800063cc]:fsw ft11, 88(ra)<br>   |
| 525|[0x8000a474]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800063fc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006400]:csrrs tp, fcsr, zero<br> [0x80006404]:fsw ft11, 96(ra)<br>   |
| 526|[0x8000a47c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006434]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006438]:csrrs tp, fcsr, zero<br> [0x8000643c]:fsw ft11, 104(ra)<br>  |
| 527|[0x8000a484]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000646c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006470]:csrrs tp, fcsr, zero<br> [0x80006474]:fsw ft11, 112(ra)<br>  |
| 528|[0x8000a48c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800064a4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800064a8]:csrrs tp, fcsr, zero<br> [0x800064ac]:fsw ft11, 120(ra)<br>  |
| 529|[0x8000a494]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800064dc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800064e0]:csrrs tp, fcsr, zero<br> [0x800064e4]:fsw ft11, 128(ra)<br>  |
| 530|[0x8000a49c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006514]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006518]:csrrs tp, fcsr, zero<br> [0x8000651c]:fsw ft11, 136(ra)<br>  |
| 531|[0x8000a4a4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000654c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006550]:csrrs tp, fcsr, zero<br> [0x80006554]:fsw ft11, 144(ra)<br>  |
| 532|[0x8000a4ac]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006584]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006588]:csrrs tp, fcsr, zero<br> [0x8000658c]:fsw ft11, 152(ra)<br>  |
| 533|[0x8000a4b4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800065bc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800065c0]:csrrs tp, fcsr, zero<br> [0x800065c4]:fsw ft11, 160(ra)<br>  |
| 534|[0x8000a4bc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800065f4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800065f8]:csrrs tp, fcsr, zero<br> [0x800065fc]:fsw ft11, 168(ra)<br>  |
| 535|[0x8000a4c4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000662c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006630]:csrrs tp, fcsr, zero<br> [0x80006634]:fsw ft11, 176(ra)<br>  |
| 536|[0x8000a4cc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006664]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006668]:csrrs tp, fcsr, zero<br> [0x8000666c]:fsw ft11, 184(ra)<br>  |
| 537|[0x8000a4d4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000669c]:fadd.h ft11, ft10, ft9, dyn<br> [0x800066a0]:csrrs tp, fcsr, zero<br> [0x800066a4]:fsw ft11, 192(ra)<br>  |
| 538|[0x8000a4dc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800066d4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800066d8]:csrrs tp, fcsr, zero<br> [0x800066dc]:fsw ft11, 200(ra)<br>  |
| 539|[0x8000a4e4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000670c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006710]:csrrs tp, fcsr, zero<br> [0x80006714]:fsw ft11, 208(ra)<br>  |
| 540|[0x8000a4ec]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006744]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006748]:csrrs tp, fcsr, zero<br> [0x8000674c]:fsw ft11, 216(ra)<br>  |
| 541|[0x8000a4f4]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000677c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006780]:csrrs tp, fcsr, zero<br> [0x80006784]:fsw ft11, 224(ra)<br>  |
| 542|[0x8000a4fc]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800067b4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800067b8]:csrrs tp, fcsr, zero<br> [0x800067bc]:fsw ft11, 232(ra)<br>  |
| 543|[0x8000a504]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800067ec]:fadd.h ft11, ft10, ft9, dyn<br> [0x800067f0]:csrrs tp, fcsr, zero<br> [0x800067f4]:fsw ft11, 240(ra)<br>  |
| 544|[0x8000a50c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006824]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006828]:csrrs tp, fcsr, zero<br> [0x8000682c]:fsw ft11, 248(ra)<br>  |
| 545|[0x8000a514]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000685c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006860]:csrrs tp, fcsr, zero<br> [0x80006864]:fsw ft11, 256(ra)<br>  |
| 546|[0x8000a51c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006894]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006898]:csrrs tp, fcsr, zero<br> [0x8000689c]:fsw ft11, 264(ra)<br>  |
| 547|[0x8000a524]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800068cc]:fadd.h ft11, ft10, ft9, dyn<br> [0x800068d0]:csrrs tp, fcsr, zero<br> [0x800068d4]:fsw ft11, 272(ra)<br>  |
| 548|[0x8000a52c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006904]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006908]:csrrs tp, fcsr, zero<br> [0x8000690c]:fsw ft11, 280(ra)<br>  |
| 549|[0x8000a534]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000693c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006940]:csrrs tp, fcsr, zero<br> [0x80006944]:fsw ft11, 288(ra)<br>  |
| 550|[0x8000a53c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006974]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006978]:csrrs tp, fcsr, zero<br> [0x8000697c]:fsw ft11, 296(ra)<br>  |
| 551|[0x8000a544]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800069ac]:fadd.h ft11, ft10, ft9, dyn<br> [0x800069b0]:csrrs tp, fcsr, zero<br> [0x800069b4]:fsw ft11, 304(ra)<br>  |
| 552|[0x8000a54c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800069e4]:fadd.h ft11, ft10, ft9, dyn<br> [0x800069e8]:csrrs tp, fcsr, zero<br> [0x800069ec]:fsw ft11, 312(ra)<br>  |
| 553|[0x8000a554]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a1c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006a20]:csrrs tp, fcsr, zero<br> [0x80006a24]:fsw ft11, 320(ra)<br>  |
| 554|[0x8000a55c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a54]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006a58]:csrrs tp, fcsr, zero<br> [0x80006a5c]:fsw ft11, 328(ra)<br>  |
| 555|[0x8000a564]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a8c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006a90]:csrrs tp, fcsr, zero<br> [0x80006a94]:fsw ft11, 336(ra)<br>  |
| 556|[0x8000a56c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006ac4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006ac8]:csrrs tp, fcsr, zero<br> [0x80006acc]:fsw ft11, 344(ra)<br>  |
| 557|[0x8000a574]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006afc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006b00]:csrrs tp, fcsr, zero<br> [0x80006b04]:fsw ft11, 352(ra)<br>  |
| 558|[0x8000a57c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b34]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006b38]:csrrs tp, fcsr, zero<br> [0x80006b3c]:fsw ft11, 360(ra)<br>  |
| 559|[0x8000a584]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b6c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006b70]:csrrs tp, fcsr, zero<br> [0x80006b74]:fsw ft11, 368(ra)<br>  |
| 560|[0x8000a58c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006ba4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006ba8]:csrrs tp, fcsr, zero<br> [0x80006bac]:fsw ft11, 376(ra)<br>  |
| 561|[0x8000a594]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006bdc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006be0]:csrrs tp, fcsr, zero<br> [0x80006be4]:fsw ft11, 384(ra)<br>  |
| 562|[0x8000a59c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c14]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006c18]:csrrs tp, fcsr, zero<br> [0x80006c1c]:fsw ft11, 392(ra)<br>  |
| 563|[0x8000a5a4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c4c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006c50]:csrrs tp, fcsr, zero<br> [0x80006c54]:fsw ft11, 400(ra)<br>  |
| 564|[0x8000a5ac]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c84]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006c88]:csrrs tp, fcsr, zero<br> [0x80006c8c]:fsw ft11, 408(ra)<br>  |
| 565|[0x8000a5b4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006cbc]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006cc0]:csrrs tp, fcsr, zero<br> [0x80006cc4]:fsw ft11, 416(ra)<br>  |
| 566|[0x8000a5bc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006cf4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006cf8]:csrrs tp, fcsr, zero<br> [0x80006cfc]:fsw ft11, 424(ra)<br>  |
| 567|[0x8000a5c4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d2c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006d30]:csrrs tp, fcsr, zero<br> [0x80006d34]:fsw ft11, 432(ra)<br>  |
| 568|[0x8000a5cc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d64]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006d68]:csrrs tp, fcsr, zero<br> [0x80006d6c]:fsw ft11, 440(ra)<br>  |
| 569|[0x8000a5d4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d9c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006da0]:csrrs tp, fcsr, zero<br> [0x80006da4]:fsw ft11, 448(ra)<br>  |
| 570|[0x8000a5dc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006dd4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006dd8]:csrrs tp, fcsr, zero<br> [0x80006ddc]:fsw ft11, 456(ra)<br>  |
| 571|[0x8000a5e4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e0c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006e10]:csrrs tp, fcsr, zero<br> [0x80006e14]:fsw ft11, 464(ra)<br>  |
| 572|[0x8000a5ec]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e44]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006e48]:csrrs tp, fcsr, zero<br> [0x80006e4c]:fsw ft11, 472(ra)<br>  |
| 573|[0x8000a5f4]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e7c]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006e80]:csrrs tp, fcsr, zero<br> [0x80006e84]:fsw ft11, 480(ra)<br>  |
| 574|[0x8000a5fc]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006eb4]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006eb8]:csrrs tp, fcsr, zero<br> [0x80006ebc]:fsw ft11, 488(ra)<br>  |
| 575|[0x8000a604]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006eec]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006ef0]:csrrs tp, fcsr, zero<br> [0x80006ef4]:fsw ft11, 496(ra)<br>  |
| 576|[0x8000a60c]<br>0xFBB6FAB7<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f24]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006f28]:csrrs tp, fcsr, zero<br> [0x80006f2c]:fsw ft11, 504(ra)<br>  |
| 577|[0x8000a61c]<br>0xFBB6FAB7<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f94]:fadd.h ft11, ft10, ft9, dyn<br> [0x80006f98]:csrrs tp, fcsr, zero<br> [0x80006f9c]:fsw ft11, 520(ra)<br>  |
