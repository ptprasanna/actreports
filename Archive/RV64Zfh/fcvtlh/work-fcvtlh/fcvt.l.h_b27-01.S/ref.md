
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000740')]      |
| SIG_REGION                | [('0x80002210', '0x80002420', '66 dwords')]      |
| COV_LABELS                | fcvt.l.h_b27      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64H/work-fcvtlh/fcvt.l.h_b27-01.S/ref.S    |
| Total Number of coverpoints| 73     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.l.h', 'rs1 : f31', 'rd : x31', 'fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.l.h t6, ft11, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002220]:0x0000000000000010




Last Coverpoint : ['rs1 : f30', 'rd : x30', 'fs1 == 1 and fe1 == 0x1f and fm1 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.l.h t5, ft10, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002230]:0x0000000000000010




Last Coverpoint : ['rs1 : f29', 'rd : x29', 'fs1 == 0 and fe1 == 0x1f and fm1 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.l.h t4, ft9, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002240]:0x0000000000000010




Last Coverpoint : ['rs1 : f28', 'rd : x28', 'fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fcvt.l.h t3, ft8, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002250]:0x0000000000000010




Last Coverpoint : ['rs1 : f27', 'rd : x27', 'fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000428]:fcvt.l.h s11, fs11, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002260]:0x0000000000000010




Last Coverpoint : ['rs1 : f26', 'rd : x26', 'fs1 == 1 and fe1 == 0x1f and fm1 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.l.h s10, fs10, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002270]:0x0000000000000010




Last Coverpoint : ['rs1 : f25', 'rd : x25', 'fs1 == 0 and fe1 == 0x1f and fm1 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000460]:fcvt.l.h s9, fs9, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002280]:0x0000000000000010




Last Coverpoint : ['rs1 : f24', 'rd : x24', 'fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.l.h s8, fs8, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002290]:0x0000000000000010




Last Coverpoint : ['rs1 : f23', 'rd : x23']
Last Code Sequence : 
	-[0x80000498]:fcvt.l.h s7, fs7, rne
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800022a0]:0x0000000000000010




Last Coverpoint : ['rs1 : f22', 'rd : x22']
Last Code Sequence : 
	-[0x800004b4]:fcvt.l.h s6, fs6, rne
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800022b0]:0x0000000000000010




Last Coverpoint : ['rs1 : f21', 'rd : x21']
Last Code Sequence : 
	-[0x800004d0]:fcvt.l.h s5, fs5, rne
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800022c0]:0x0000000000000010




Last Coverpoint : ['rs1 : f20', 'rd : x20']
Last Code Sequence : 
	-[0x800004ec]:fcvt.l.h s4, fs4, rne
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800022d0]:0x0000000000000010




Last Coverpoint : ['rs1 : f19', 'rd : x19']
Last Code Sequence : 
	-[0x80000508]:fcvt.l.h s3, fs3, rne
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800022e0]:0x0000000000000010




Last Coverpoint : ['rs1 : f18', 'rd : x18']
Last Code Sequence : 
	-[0x80000524]:fcvt.l.h s2, fs2, rne
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800022f0]:0x0000000000000010




Last Coverpoint : ['rs1 : f17', 'rd : x17']
Last Code Sequence : 
	-[0x80000540]:fcvt.l.h a7, fa7, rne
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002300]:0x0000000000000010




Last Coverpoint : ['rs1 : f16', 'rd : x16']
Last Code Sequence : 
	-[0x8000055c]:fcvt.l.h a6, fa6, rne
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002310]:0x0000000000000010




Last Coverpoint : ['rs1 : f15', 'rd : x15']
Last Code Sequence : 
	-[0x80000578]:fcvt.l.h a5, fa5, rne
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002320]:0x0000000000000010




Last Coverpoint : ['rs1 : f14', 'rd : x14']
Last Code Sequence : 
	-[0x80000594]:fcvt.l.h a4, fa4, rne
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002330]:0x0000000000000010




Last Coverpoint : ['rs1 : f13', 'rd : x13']
Last Code Sequence : 
	-[0x800005b0]:fcvt.l.h a3, fa3, rne
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002340]:0x0000000000000010




Last Coverpoint : ['rs1 : f12', 'rd : x12']
Last Code Sequence : 
	-[0x800005cc]:fcvt.l.h a2, fa2, rne
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002350]:0x0000000000000010




Last Coverpoint : ['rs1 : f11', 'rd : x11']
Last Code Sequence : 
	-[0x800005e8]:fcvt.l.h a1, fa1, rne
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002360]:0x0000000000000010




Last Coverpoint : ['rs1 : f10', 'rd : x10']
Last Code Sequence : 
	-[0x80000604]:fcvt.l.h a0, fa0, rne
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002370]:0x0000000000000010




Last Coverpoint : ['rs1 : f9', 'rd : x9']
Last Code Sequence : 
	-[0x80000620]:fcvt.l.h s1, fs1, rne
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002380]:0x0000000000000010




Last Coverpoint : ['rs1 : f8', 'rd : x8']
Last Code Sequence : 
	-[0x8000063c]:fcvt.l.h fp, fs0, rne
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd fp, 368(ra)
Current Store : [0x80000648] : sd tp, 376(ra) -- Store: [0x80002390]:0x0000000000000010




Last Coverpoint : ['rs1 : f7', 'rd : x7']
Last Code Sequence : 
	-[0x80000660]:fcvt.l.h t2, ft7, rne
	-[0x80000664]:csrrs s1, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
Current Store : [0x8000066c] : sd s1, 392(ra) -- Store: [0x800023a0]:0x0000000000000010




Last Coverpoint : ['rs1 : f6', 'rd : x6']
Last Code Sequence : 
	-[0x8000067c]:fcvt.l.h t1, ft6, rne
	-[0x80000680]:csrrs s1, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
Current Store : [0x80000688] : sd s1, 408(ra) -- Store: [0x800023b0]:0x0000000000000010




Last Coverpoint : ['rs1 : f5', 'rd : x5']
Last Code Sequence : 
	-[0x80000698]:fcvt.l.h t0, ft5, rne
	-[0x8000069c]:csrrs s1, fcsr, zero
	-[0x800006a0]:sd t0, 416(ra)
Current Store : [0x800006a4] : sd s1, 424(ra) -- Store: [0x800023c0]:0x0000000000000010




Last Coverpoint : ['rs1 : f4', 'rd : x4']
Last Code Sequence : 
	-[0x800006bc]:fcvt.l.h tp, ft4, rne
	-[0x800006c0]:csrrs s1, fcsr, zero
	-[0x800006c4]:sd tp, 0(t0)
Current Store : [0x800006c8] : sd s1, 8(t0) -- Store: [0x800023d0]:0x0000000000000010




Last Coverpoint : ['rs1 : f3', 'rd : x3']
Last Code Sequence : 
	-[0x800006d8]:fcvt.l.h gp, ft3, rne
	-[0x800006dc]:csrrs s1, fcsr, zero
	-[0x800006e0]:sd gp, 16(t0)
Current Store : [0x800006e4] : sd s1, 24(t0) -- Store: [0x800023e0]:0x0000000000000010




Last Coverpoint : ['rs1 : f2', 'rd : x2']
Last Code Sequence : 
	-[0x800006f4]:fcvt.l.h sp, ft2, rne
	-[0x800006f8]:csrrs s1, fcsr, zero
	-[0x800006fc]:sd sp, 32(t0)
Current Store : [0x80000700] : sd s1, 40(t0) -- Store: [0x800023f0]:0x0000000000000010




Last Coverpoint : ['rs1 : f1', 'rd : x1']
Last Code Sequence : 
	-[0x80000710]:fcvt.l.h ra, ft1, rne
	-[0x80000714]:csrrs s1, fcsr, zero
	-[0x80000718]:sd ra, 48(t0)
Current Store : [0x8000071c] : sd s1, 56(t0) -- Store: [0x80002400]:0x0000000000000010




Last Coverpoint : ['rs1 : f0', 'rd : x0']
Last Code Sequence : 
	-[0x8000072c]:fcvt.l.h zero, ft0, rne
	-[0x80000730]:csrrs s1, fcsr, zero
	-[0x80000734]:sd zero, 64(t0)
Current Store : [0x80000738] : sd s1, 72(t0) -- Store: [0x80002410]:0x0000000000000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                                                  coverpoints                                                                                   |                                                      code                                                      |
|---:|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x7FFFFFFFFFFFFFFF<br> |- mnemonic : fcvt.l.h<br> - rs1 : f31<br> - rd : x31<br> - fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br> |[0x800003b8]:fcvt.l.h t6, ft11, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br>     |
|   2|[0x80002228]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f30<br> - rd : x30<br> - fs1 == 1 and fe1 == 0x1f and fm1 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800003d4]:fcvt.l.h t5, ft10, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br>    |
|   3|[0x80002238]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f29<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x1f and fm1 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800003f0]:fcvt.l.h t4, ft9, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br>     |
|   4|[0x80002248]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f28<br> - rd : x28<br> - fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x8000040c]:fcvt.l.h t3, ft8, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br>     |
|   5|[0x80002258]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f27<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000428]:fcvt.l.h s11, fs11, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br>  |
|   6|[0x80002268]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f26<br> - rd : x26<br> - fs1 == 1 and fe1 == 0x1f and fm1 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000444]:fcvt.l.h s10, fs10, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br>  |
|   7|[0x80002278]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f25<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1f and fm1 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000460]:fcvt.l.h s9, fs9, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br>     |
|   8|[0x80002288]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f24<br> - rd : x24<br> - fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x8000047c]:fcvt.l.h s8, fs8, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br>    |
|   9|[0x80002298]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f23<br> - rd : x23<br>                                                                                                                                                  |[0x80000498]:fcvt.l.h s7, fs7, rne<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br>    |
|  10|[0x800022a8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f22<br> - rd : x22<br>                                                                                                                                                  |[0x800004b4]:fcvt.l.h s6, fs6, rne<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br>    |
|  11|[0x800022b8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f21<br> - rd : x21<br>                                                                                                                                                  |[0x800004d0]:fcvt.l.h s5, fs5, rne<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br>    |
|  12|[0x800022c8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f20<br> - rd : x20<br>                                                                                                                                                  |[0x800004ec]:fcvt.l.h s4, fs4, rne<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br>    |
|  13|[0x800022d8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f19<br> - rd : x19<br>                                                                                                                                                  |[0x80000508]:fcvt.l.h s3, fs3, rne<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br>    |
|  14|[0x800022e8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f18<br> - rd : x18<br>                                                                                                                                                  |[0x80000524]:fcvt.l.h s2, fs2, rne<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br>    |
|  15|[0x800022f8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f17<br> - rd : x17<br>                                                                                                                                                  |[0x80000540]:fcvt.l.h a7, fa7, rne<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br>    |
|  16|[0x80002308]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f16<br> - rd : x16<br>                                                                                                                                                  |[0x8000055c]:fcvt.l.h a6, fa6, rne<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br>    |
|  17|[0x80002318]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f15<br> - rd : x15<br>                                                                                                                                                  |[0x80000578]:fcvt.l.h a5, fa5, rne<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br>    |
|  18|[0x80002328]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f14<br> - rd : x14<br>                                                                                                                                                  |[0x80000594]:fcvt.l.h a4, fa4, rne<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br>    |
|  19|[0x80002338]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f13<br> - rd : x13<br>                                                                                                                                                  |[0x800005b0]:fcvt.l.h a3, fa3, rne<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sd a3, 288(ra)<br>    |
|  20|[0x80002348]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f12<br> - rd : x12<br>                                                                                                                                                  |[0x800005cc]:fcvt.l.h a2, fa2, rne<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sd a2, 304(ra)<br>    |
|  21|[0x80002358]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f11<br> - rd : x11<br>                                                                                                                                                  |[0x800005e8]:fcvt.l.h a1, fa1, rne<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br>    |
|  22|[0x80002368]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f10<br> - rd : x10<br>                                                                                                                                                  |[0x80000604]:fcvt.l.h a0, fa0, rne<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br>    |
|  23|[0x80002378]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f9<br> - rd : x9<br>                                                                                                                                                    |[0x80000620]:fcvt.l.h s1, fs1, rne<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br>    |
|  24|[0x80002388]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f8<br> - rd : x8<br>                                                                                                                                                    |[0x8000063c]:fcvt.l.h fp, fs0, rne<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd fp, 368(ra)<br>    |
|  25|[0x80002398]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f7<br> - rd : x7<br>                                                                                                                                                    |[0x80000660]:fcvt.l.h t2, ft7, rne<br> [0x80000664]:csrrs s1, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br>    |
|  26|[0x800023a8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f6<br> - rd : x6<br>                                                                                                                                                    |[0x8000067c]:fcvt.l.h t1, ft6, rne<br> [0x80000680]:csrrs s1, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br>    |
|  27|[0x800023b8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f5<br> - rd : x5<br>                                                                                                                                                    |[0x80000698]:fcvt.l.h t0, ft5, rne<br> [0x8000069c]:csrrs s1, fcsr, zero<br> [0x800006a0]:sd t0, 416(ra)<br>    |
|  28|[0x800023c8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f4<br> - rd : x4<br>                                                                                                                                                    |[0x800006bc]:fcvt.l.h tp, ft4, rne<br> [0x800006c0]:csrrs s1, fcsr, zero<br> [0x800006c4]:sd tp, 0(t0)<br>      |
|  29|[0x800023d8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f3<br> - rd : x3<br>                                                                                                                                                    |[0x800006d8]:fcvt.l.h gp, ft3, rne<br> [0x800006dc]:csrrs s1, fcsr, zero<br> [0x800006e0]:sd gp, 16(t0)<br>     |
|  30|[0x800023e8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f2<br> - rd : x2<br>                                                                                                                                                    |[0x800006f4]:fcvt.l.h sp, ft2, rne<br> [0x800006f8]:csrrs s1, fcsr, zero<br> [0x800006fc]:sd sp, 32(t0)<br>     |
|  31|[0x800023f8]<br>0x7FFFFFFFFFFFFFFF<br> |- rs1 : f1<br> - rd : x1<br>                                                                                                                                                    |[0x80000710]:fcvt.l.h ra, ft1, rne<br> [0x80000714]:csrrs s1, fcsr, zero<br> [0x80000718]:sd ra, 48(t0)<br>     |
|  32|[0x80002408]<br>0x0000000000000000<br> |- rs1 : f0<br> - rd : x0<br>                                                                                                                                                    |[0x8000072c]:fcvt.l.h zero, ft0, rne<br> [0x80000730]:csrrs s1, fcsr, zero<br> [0x80000734]:sd zero, 64(t0)<br> |
