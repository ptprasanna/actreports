
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000bc0')]      |
| SIG_REGION                | [('0x80002310', '0x800027b0', '148 dwords')]      |
| COV_LABELS                | fcvt.l.h_b22      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64H/work-fcvtlh/fcvt.l.h_b22-01.S/ref.S    |
| Total Number of coverpoints| 137     |
| Total Coverpoints Hit     | 137      |
| Total Signature Updates   | 146      |
| STAT1                     | 72      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 73     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000ba8]:fcvt.l.h t6, ft11, dyn
      [0x80000bac]:csrrs s1, fcsr, zero
      [0x80000bb0]:sd t6, 720(t0)
 -- Signature Addresses:
      Address: 0x80002798 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fcvt.l.h
      - rs1 : f31
      - rd : x31
      - fs1 == 1 and fe1 == 0x0b and fm1 == 0x136 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.l.h', 'rs1 : f31', 'rd : x31', 'fs1 == 0 and fe1 == 0x0c and fm1 == 0x249 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.l.h t6, ft11, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002320]:0x0000000000000001




Last Coverpoint : ['rs1 : f30', 'rd : x30', 'fs1 == 0 and fe1 == 0x0d and fm1 == 0x1b7 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.l.h t5, ft10, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002330]:0x0000000000000001




Last Coverpoint : ['rs1 : f29', 'rd : x29', 'fs1 == 0 and fe1 == 0x0e and fm1 == 0x24f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.l.h t4, ft9, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002340]:0x0000000000000001




Last Coverpoint : ['rs1 : f28', 'rd : x28', 'fs1 == 0 and fe1 == 0x0f and fm1 == 0x0d3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fcvt.l.h t3, ft8, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002350]:0x0000000000000001




Last Coverpoint : ['rs1 : f27', 'rd : x27', 'fs1 == 0 and fe1 == 0x10 and fm1 == 0x340 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000428]:fcvt.l.h s11, fs11, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002360]:0x0000000000000001




Last Coverpoint : ['rs1 : f26', 'rd : x26', 'fs1 == 0 and fe1 == 0x11 and fm1 == 0x34b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.l.h s10, fs10, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002370]:0x0000000000000001




Last Coverpoint : ['rs1 : f25', 'rd : x25', 'fs1 == 1 and fe1 == 0x12 and fm1 == 0x29d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000460]:fcvt.l.h s9, fs9, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002380]:0x0000000000000001




Last Coverpoint : ['rs1 : f24', 'rd : x24', 'fs1 == 0 and fe1 == 0x13 and fm1 == 0x0a4 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.l.h s8, fs8, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002390]:0x0000000000000001




Last Coverpoint : ['rs1 : f23', 'rd : x23', 'fs1 == 0 and fe1 == 0x14 and fm1 == 0x215 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000498]:fcvt.l.h s7, fs7, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800023a0]:0x0000000000000001




Last Coverpoint : ['rs1 : f22', 'rd : x22', 'fs1 == 0 and fe1 == 0x15 and fm1 == 0x14f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fcvt.l.h s6, fs6, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023b0]:0x0000000000000001




Last Coverpoint : ['rs1 : f21', 'rd : x21', 'fs1 == 1 and fe1 == 0x16 and fm1 == 0x0ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fcvt.l.h s5, fs5, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023c0]:0x0000000000000001




Last Coverpoint : ['rs1 : f20', 'rd : x20', 'fs1 == 1 and fe1 == 0x17 and fm1 == 0x3cf and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.l.h s4, fs4, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023d0]:0x0000000000000001




Last Coverpoint : ['rs1 : f19', 'rd : x19', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x3fc and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000508]:fcvt.l.h s3, fs3, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rs1 : f18', 'rd : x18', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x02d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.l.h s2, fs2, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rs1 : f17', 'rd : x17', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x370 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000540]:fcvt.l.h a7, fa7, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs1 : f16', 'rd : x16', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x269 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fcvt.l.h a6, fa6, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : f15', 'rd : x15', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x186 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000578]:fcvt.l.h a5, fa5, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : f14', 'rd : x14', 'fs1 == 1 and fe1 == 0x1d and fm1 == 0x122 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.l.h a4, fa4, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002430]:0x0000000000000000




Last Coverpoint : ['rs1 : f13', 'rd : x13', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b0]:fcvt.l.h a3, fa3, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002440]:0x0000000000000000




Last Coverpoint : ['rs1 : f12', 'rd : x12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fcvt.l.h a2, fa2, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002450]:0x0000000000000000




Last Coverpoint : ['rs1 : f11', 'rd : x11', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x2be and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005e8]:fcvt.l.h a1, fa1, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002460]:0x0000000000000001




Last Coverpoint : ['rs1 : f10', 'rd : x10', 'fs1 == 1 and fe1 == 0x01 and fm1 == 0x2a5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000604]:fcvt.l.h a0, fa0, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002470]:0x0000000000000001




Last Coverpoint : ['rs1 : f9', 'rd : x9', 'fs1 == 1 and fe1 == 0x02 and fm1 == 0x088 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000620]:fcvt.l.h s1, fs1, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002480]:0x0000000000000001




Last Coverpoint : ['rs1 : f8', 'rd : x8', 'fs1 == 1 and fe1 == 0x03 and fm1 == 0x312 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fcvt.l.h fp, fs0, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd fp, 368(ra)
Current Store : [0x80000648] : sd tp, 376(ra) -- Store: [0x80002490]:0x0000000000000001




Last Coverpoint : ['rs1 : f7', 'rd : x7', 'fs1 == 1 and fe1 == 0x04 and fm1 == 0x3ed and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000660]:fcvt.l.h t2, ft7, dyn
	-[0x80000664]:csrrs s1, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
Current Store : [0x8000066c] : sd s1, 392(ra) -- Store: [0x800024a0]:0x0000000000000001




Last Coverpoint : ['rs1 : f6', 'rd : x6', 'fs1 == 1 and fe1 == 0x05 and fm1 == 0x3e0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000067c]:fcvt.l.h t1, ft6, dyn
	-[0x80000680]:csrrs s1, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
Current Store : [0x80000688] : sd s1, 408(ra) -- Store: [0x800024b0]:0x0000000000000001




Last Coverpoint : ['rs1 : f5', 'rd : x5', 'fs1 == 1 and fe1 == 0x06 and fm1 == 0x274 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000698]:fcvt.l.h t0, ft5, dyn
	-[0x8000069c]:csrrs s1, fcsr, zero
	-[0x800006a0]:sd t0, 416(ra)
Current Store : [0x800006a4] : sd s1, 424(ra) -- Store: [0x800024c0]:0x0000000000000001




Last Coverpoint : ['rs1 : f4', 'rd : x4', 'fs1 == 1 and fe1 == 0x07 and fm1 == 0x02d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fcvt.l.h tp, ft4, dyn
	-[0x800006c0]:csrrs s1, fcsr, zero
	-[0x800006c4]:sd tp, 0(t0)
Current Store : [0x800006c8] : sd s1, 8(t0) -- Store: [0x800024d0]:0x0000000000000001




Last Coverpoint : ['rs1 : f3', 'rd : x3', 'fs1 == 1 and fe1 == 0x08 and fm1 == 0x004 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d8]:fcvt.l.h gp, ft3, dyn
	-[0x800006dc]:csrrs s1, fcsr, zero
	-[0x800006e0]:sd gp, 16(t0)
Current Store : [0x800006e4] : sd s1, 24(t0) -- Store: [0x800024e0]:0x0000000000000001




Last Coverpoint : ['rs1 : f2', 'rd : x2', 'fs1 == 1 and fe1 == 0x09 and fm1 == 0x089 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fcvt.l.h sp, ft2, dyn
	-[0x800006f8]:csrrs s1, fcsr, zero
	-[0x800006fc]:sd sp, 32(t0)
Current Store : [0x80000700] : sd s1, 40(t0) -- Store: [0x800024f0]:0x0000000000000001




Last Coverpoint : ['rs1 : f1', 'rd : x1', 'fs1 == 1 and fe1 == 0x0a and fm1 == 0x3c3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000710]:fcvt.l.h ra, ft1, dyn
	-[0x80000714]:csrrs s1, fcsr, zero
	-[0x80000718]:sd ra, 48(t0)
Current Store : [0x8000071c] : sd s1, 56(t0) -- Store: [0x80002500]:0x0000000000000001




Last Coverpoint : ['rs1 : f0', 'rd : x0', 'fs1 == 1 and fe1 == 0x0b and fm1 == 0x136 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fcvt.l.h zero, ft0, dyn
	-[0x80000730]:csrrs s1, fcsr, zero
	-[0x80000734]:sd zero, 64(t0)
Current Store : [0x80000738] : sd s1, 72(t0) -- Store: [0x80002510]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0c and fm1 == 0x176 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000748]:fcvt.l.h t6, ft11, dyn
	-[0x8000074c]:csrrs s1, fcsr, zero
	-[0x80000750]:sd t6, 80(t0)
Current Store : [0x80000754] : sd s1, 88(t0) -- Store: [0x80002520]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0d and fm1 == 0x397 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000764]:fcvt.l.h t6, ft11, dyn
	-[0x80000768]:csrrs s1, fcsr, zero
	-[0x8000076c]:sd t6, 96(t0)
Current Store : [0x80000770] : sd s1, 104(t0) -- Store: [0x80002530]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0e and fm1 == 0x141 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000780]:fcvt.l.h t6, ft11, dyn
	-[0x80000784]:csrrs s1, fcsr, zero
	-[0x80000788]:sd t6, 112(t0)
Current Store : [0x8000078c] : sd s1, 120(t0) -- Store: [0x80002540]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x232 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000079c]:fcvt.l.h t6, ft11, dyn
	-[0x800007a0]:csrrs s1, fcsr, zero
	-[0x800007a4]:sd t6, 128(t0)
Current Store : [0x800007a8] : sd s1, 136(t0) -- Store: [0x80002550]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x10 and fm1 == 0x1be and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b8]:fcvt.l.h t6, ft11, dyn
	-[0x800007bc]:csrrs s1, fcsr, zero
	-[0x800007c0]:sd t6, 144(t0)
Current Store : [0x800007c4] : sd s1, 152(t0) -- Store: [0x80002560]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x11 and fm1 == 0x042 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fcvt.l.h t6, ft11, dyn
	-[0x800007d8]:csrrs s1, fcsr, zero
	-[0x800007dc]:sd t6, 160(t0)
Current Store : [0x800007e0] : sd s1, 168(t0) -- Store: [0x80002570]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x12 and fm1 == 0x256 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f0]:fcvt.l.h t6, ft11, dyn
	-[0x800007f4]:csrrs s1, fcsr, zero
	-[0x800007f8]:sd t6, 176(t0)
Current Store : [0x800007fc] : sd s1, 184(t0) -- Store: [0x80002580]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x13 and fm1 == 0x360 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fcvt.l.h t6, ft11, dyn
	-[0x80000810]:csrrs s1, fcsr, zero
	-[0x80000814]:sd t6, 192(t0)
Current Store : [0x80000818] : sd s1, 200(t0) -- Store: [0x80002590]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x14 and fm1 == 0x0a0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000828]:fcvt.l.h t6, ft11, dyn
	-[0x8000082c]:csrrs s1, fcsr, zero
	-[0x80000830]:sd t6, 208(t0)
Current Store : [0x80000834] : sd s1, 216(t0) -- Store: [0x800025a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x0e5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000844]:fcvt.l.h t6, ft11, dyn
	-[0x80000848]:csrrs s1, fcsr, zero
	-[0x8000084c]:sd t6, 224(t0)
Current Store : [0x80000850] : sd s1, 232(t0) -- Store: [0x800025b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x16 and fm1 == 0x1a6 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000860]:fcvt.l.h t6, ft11, dyn
	-[0x80000864]:csrrs s1, fcsr, zero
	-[0x80000868]:sd t6, 240(t0)
Current Store : [0x8000086c] : sd s1, 248(t0) -- Store: [0x800025c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x17 and fm1 == 0x025 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000087c]:fcvt.l.h t6, ft11, dyn
	-[0x80000880]:csrrs s1, fcsr, zero
	-[0x80000884]:sd t6, 256(t0)
Current Store : [0x80000888] : sd s1, 264(t0) -- Store: [0x800025d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x18 and fm1 == 0x1fc and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000898]:fcvt.l.h t6, ft11, dyn
	-[0x8000089c]:csrrs s1, fcsr, zero
	-[0x800008a0]:sd t6, 272(t0)
Current Store : [0x800008a4] : sd s1, 280(t0) -- Store: [0x800025e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x19 and fm1 == 0x345 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fcvt.l.h t6, ft11, dyn
	-[0x800008b8]:csrrs s1, fcsr, zero
	-[0x800008bc]:sd t6, 288(t0)
Current Store : [0x800008c0] : sd s1, 296(t0) -- Store: [0x800025f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1a and fm1 == 0x2a1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d0]:fcvt.l.h t6, ft11, dyn
	-[0x800008d4]:csrrs s1, fcsr, zero
	-[0x800008d8]:sd t6, 304(t0)
Current Store : [0x800008dc] : sd s1, 312(t0) -- Store: [0x80002600]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1b and fm1 == 0x222 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fcvt.l.h t6, ft11, dyn
	-[0x800008f0]:csrrs s1, fcsr, zero
	-[0x800008f4]:sd t6, 320(t0)
Current Store : [0x800008f8] : sd s1, 328(t0) -- Store: [0x80002610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1c and fm1 == 0x36b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000908]:fcvt.l.h t6, ft11, dyn
	-[0x8000090c]:csrrs s1, fcsr, zero
	-[0x80000910]:sd t6, 336(t0)
Current Store : [0x80000914] : sd s1, 344(t0) -- Store: [0x80002620]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1d and fm1 == 0x099 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000924]:fcvt.l.h t6, ft11, dyn
	-[0x80000928]:csrrs s1, fcsr, zero
	-[0x8000092c]:sd t6, 352(t0)
Current Store : [0x80000930] : sd s1, 360(t0) -- Store: [0x80002630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x244 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000940]:fcvt.l.h t6, ft11, dyn
	-[0x80000944]:csrrs s1, fcsr, zero
	-[0x80000948]:sd t6, 368(t0)
Current Store : [0x8000094c] : sd s1, 376(t0) -- Store: [0x80002640]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000095c]:fcvt.l.h t6, ft11, dyn
	-[0x80000960]:csrrs s1, fcsr, zero
	-[0x80000964]:sd t6, 384(t0)
Current Store : [0x80000968] : sd s1, 392(t0) -- Store: [0x80002650]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x267 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000978]:fcvt.l.h t6, ft11, dyn
	-[0x8000097c]:csrrs s1, fcsr, zero
	-[0x80000980]:sd t6, 400(t0)
Current Store : [0x80000984] : sd s1, 408(t0) -- Store: [0x80002660]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x073 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fcvt.l.h t6, ft11, dyn
	-[0x80000998]:csrrs s1, fcsr, zero
	-[0x8000099c]:sd t6, 416(t0)
Current Store : [0x800009a0] : sd s1, 424(t0) -- Store: [0x80002670]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x02 and fm1 == 0x31a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b0]:fcvt.l.h t6, ft11, dyn
	-[0x800009b4]:csrrs s1, fcsr, zero
	-[0x800009b8]:sd t6, 432(t0)
Current Store : [0x800009bc] : sd s1, 440(t0) -- Store: [0x80002680]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x03 and fm1 == 0x062 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fcvt.l.h t6, ft11, dyn
	-[0x800009d0]:csrrs s1, fcsr, zero
	-[0x800009d4]:sd t6, 448(t0)
Current Store : [0x800009d8] : sd s1, 456(t0) -- Store: [0x80002690]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x04 and fm1 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009e8]:fcvt.l.h t6, ft11, dyn
	-[0x800009ec]:csrrs s1, fcsr, zero
	-[0x800009f0]:sd t6, 464(t0)
Current Store : [0x800009f4] : sd s1, 472(t0) -- Store: [0x800026a0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x05 and fm1 == 0x33a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fcvt.l.h t6, ft11, dyn
	-[0x80000a08]:csrrs s1, fcsr, zero
	-[0x80000a0c]:sd t6, 480(t0)
Current Store : [0x80000a10] : sd s1, 488(t0) -- Store: [0x800026b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x06 and fm1 == 0x3ca and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a20]:fcvt.l.h t6, ft11, dyn
	-[0x80000a24]:csrrs s1, fcsr, zero
	-[0x80000a28]:sd t6, 496(t0)
Current Store : [0x80000a2c] : sd s1, 504(t0) -- Store: [0x800026c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x07 and fm1 == 0x1bb and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a3c]:fcvt.l.h t6, ft11, dyn
	-[0x80000a40]:csrrs s1, fcsr, zero
	-[0x80000a44]:sd t6, 512(t0)
Current Store : [0x80000a48] : sd s1, 520(t0) -- Store: [0x800026d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x08 and fm1 == 0x1a8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a58]:fcvt.l.h t6, ft11, dyn
	-[0x80000a5c]:csrrs s1, fcsr, zero
	-[0x80000a60]:sd t6, 528(t0)
Current Store : [0x80000a64] : sd s1, 536(t0) -- Store: [0x800026e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x09 and fm1 == 0x0fb and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fcvt.l.h t6, ft11, dyn
	-[0x80000a78]:csrrs s1, fcsr, zero
	-[0x80000a7c]:sd t6, 544(t0)
Current Store : [0x80000a80] : sd s1, 552(t0) -- Store: [0x800026f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x119 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a90]:fcvt.l.h t6, ft11, dyn
	-[0x80000a94]:csrrs s1, fcsr, zero
	-[0x80000a98]:sd t6, 560(t0)
Current Store : [0x80000a9c] : sd s1, 568(t0) -- Store: [0x80002700]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0b and fm1 == 0x278 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fcvt.l.h t6, ft11, dyn
	-[0x80000ab0]:csrrs s1, fcsr, zero
	-[0x80000ab4]:sd t6, 576(t0)
Current Store : [0x80000ab8] : sd s1, 584(t0) -- Store: [0x80002710]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0c and fm1 == 0x0b6 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fcvt.l.h t6, ft11, dyn
	-[0x80000acc]:csrrs s1, fcsr, zero
	-[0x80000ad0]:sd t6, 592(t0)
Current Store : [0x80000ad4] : sd s1, 600(t0) -- Store: [0x80002720]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0d and fm1 == 0x07a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ae4]:fcvt.l.h t6, ft11, dyn
	-[0x80000ae8]:csrrs s1, fcsr, zero
	-[0x80000aec]:sd t6, 608(t0)
Current Store : [0x80000af0] : sd s1, 616(t0) -- Store: [0x80002730]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0e and fm1 == 0x1d2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b00]:fcvt.l.h t6, ft11, dyn
	-[0x80000b04]:csrrs s1, fcsr, zero
	-[0x80000b08]:sd t6, 624(t0)
Current Store : [0x80000b0c] : sd s1, 632(t0) -- Store: [0x80002740]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x0a8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b1c]:fcvt.l.h t6, ft11, dyn
	-[0x80000b20]:csrrs s1, fcsr, zero
	-[0x80000b24]:sd t6, 640(t0)
Current Store : [0x80000b28] : sd s1, 648(t0) -- Store: [0x80002750]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x10 and fm1 == 0x298 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b38]:fcvt.l.h t6, ft11, dyn
	-[0x80000b3c]:csrrs s1, fcsr, zero
	-[0x80000b40]:sd t6, 656(t0)
Current Store : [0x80000b44] : sd s1, 664(t0) -- Store: [0x80002760]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x11 and fm1 == 0x348 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fcvt.l.h t6, ft11, dyn
	-[0x80000b58]:csrrs s1, fcsr, zero
	-[0x80000b5c]:sd t6, 672(t0)
Current Store : [0x80000b60] : sd s1, 680(t0) -- Store: [0x80002770]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x31c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b70]:fcvt.l.h t6, ft11, dyn
	-[0x80000b74]:csrrs s1, fcsr, zero
	-[0x80000b78]:sd t6, 688(t0)
Current Store : [0x80000b7c] : sd s1, 696(t0) -- Store: [0x80002780]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x317 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fcvt.l.h t6, ft11, dyn
	-[0x80000b90]:csrrs s1, fcsr, zero
	-[0x80000b94]:sd t6, 704(t0)
Current Store : [0x80000b98] : sd s1, 712(t0) -- Store: [0x80002790]:0x0000000000000001




Last Coverpoint : ['mnemonic : fcvt.l.h', 'rs1 : f31', 'rd : x31', 'fs1 == 1 and fe1 == 0x0b and fm1 == 0x136 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ba8]:fcvt.l.h t6, ft11, dyn
	-[0x80000bac]:csrrs s1, fcsr, zero
	-[0x80000bb0]:sd t6, 720(t0)
Current Store : [0x80000bb4] : sd s1, 728(t0) -- Store: [0x800027a0]:0x0000000000000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                                                  coverpoints                                                                                   |                                                      code                                                      |
|---:|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000000<br> |- mnemonic : fcvt.l.h<br> - rs1 : f31<br> - rd : x31<br> - fs1 == 0 and fe1 == 0x0c and fm1 == 0x249 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br> |[0x800003b8]:fcvt.l.h t6, ft11, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br>     |
|   2|[0x80002328]<br>0x0000000000000000<br> |- rs1 : f30<br> - rd : x30<br> - fs1 == 0 and fe1 == 0x0d and fm1 == 0x1b7 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800003d4]:fcvt.l.h t5, ft10, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br>    |
|   3|[0x80002338]<br>0x0000000000000001<br> |- rs1 : f29<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x0e and fm1 == 0x24f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800003f0]:fcvt.l.h t4, ft9, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br>     |
|   4|[0x80002348]<br>0x0000000000000001<br> |- rs1 : f28<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x0f and fm1 == 0x0d3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x8000040c]:fcvt.l.h t3, ft8, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br>     |
|   5|[0x80002358]<br>0x0000000000000004<br> |- rs1 : f27<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x10 and fm1 == 0x340 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000428]:fcvt.l.h s11, fs11, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br>  |
|   6|[0x80002368]<br>0x0000000000000007<br> |- rs1 : f26<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x11 and fm1 == 0x34b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000444]:fcvt.l.h s10, fs10, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br>  |
|   7|[0x80002378]<br>0xFFFFFFFFFFFFFFF3<br> |- rs1 : f25<br> - rd : x25<br> - fs1 == 1 and fe1 == 0x12 and fm1 == 0x29d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000460]:fcvt.l.h s9, fs9, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br>     |
|   8|[0x80002388]<br>0x0000000000000013<br> |- rs1 : f24<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x13 and fm1 == 0x0a4 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x8000047c]:fcvt.l.h s8, fs8, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br>    |
|   9|[0x80002398]<br>0x0000000000000031<br> |- rs1 : f23<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x14 and fm1 == 0x215 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000498]:fcvt.l.h s7, fs7, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br>    |
|  10|[0x800023a8]<br>0x0000000000000055<br> |- rs1 : f22<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x15 and fm1 == 0x14f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800004b4]:fcvt.l.h s6, fs6, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br>    |
|  11|[0x800023b8]<br>0xFFFFFFFFFFFFFF60<br> |- rs1 : f21<br> - rd : x21<br> - fs1 == 1 and fe1 == 0x16 and fm1 == 0x0ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800004d0]:fcvt.l.h s5, fs5, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br>    |
|  12|[0x800023c8]<br>0xFFFFFFFFFFFFFE0C<br> |- rs1 : f20<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x17 and fm1 == 0x3cf and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800004ec]:fcvt.l.h s4, fs4, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br>    |
|  13|[0x800023d8]<br>0x00000000000003FE<br> |- rs1 : f19<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x3fc and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000508]:fcvt.l.h s3, fs3, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br>    |
|  14|[0x800023e8]<br>0x000000000000042D<br> |- rs1 : f18<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x19 and fm1 == 0x02d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000524]:fcvt.l.h s2, fs2, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br>    |
|  15|[0x800023f8]<br>0x0000000000000EE0<br> |- rs1 : f17<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x370 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000540]:fcvt.l.h a7, fa7, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br>    |
|  16|[0x80002408]<br>0x00000000000019A4<br> |- rs1 : f16<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x269 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x8000055c]:fcvt.l.h a6, fa6, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br>    |
|  17|[0x80002418]<br>0x0000000000002C30<br> |- rs1 : f15<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x186 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000578]:fcvt.l.h a5, fa5, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br>    |
|  18|[0x80002428]<br>0xFFFFFFFFFFFFADE0<br> |- rs1 : f14<br> - rd : x14<br> - fs1 == 1 and fe1 == 0x1d and fm1 == 0x122 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000594]:fcvt.l.h a4, fa4, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:sd a4, 272(ra)<br>    |
|  19|[0x80002438]<br>0x000000000000D660<br> |- rs1 : f13<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800005b0]:fcvt.l.h a3, fa3, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:sd a3, 288(ra)<br>    |
|  20|[0x80002448]<br>0x000000000000FFE0<br> |- rs1 : f12<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800005cc]:fcvt.l.h a2, fa2, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:sd a2, 304(ra)<br>    |
|  21|[0x80002458]<br>0x0000000000000000<br> |- rs1 : f11<br> - rd : x11<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x2be and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x800005e8]:fcvt.l.h a1, fa1, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br>    |
|  22|[0x80002468]<br>0x0000000000000000<br> |- rs1 : f10<br> - rd : x10<br> - fs1 == 1 and fe1 == 0x01 and fm1 == 0x2a5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                           |[0x80000604]:fcvt.l.h a0, fa0, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br>    |
|  23|[0x80002478]<br>0x0000000000000000<br> |- rs1 : f9<br> - rd : x9<br> - fs1 == 1 and fe1 == 0x02 and fm1 == 0x088 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x80000620]:fcvt.l.h s1, fs1, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br>    |
|  24|[0x80002488]<br>0x0000000000000000<br> |- rs1 : f8<br> - rd : x8<br> - fs1 == 1 and fe1 == 0x03 and fm1 == 0x312 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x8000063c]:fcvt.l.h fp, fs0, dyn<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:sd fp, 368(ra)<br>    |
|  25|[0x80002498]<br>0x0000000000000000<br> |- rs1 : f7<br> - rd : x7<br> - fs1 == 1 and fe1 == 0x04 and fm1 == 0x3ed and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x80000660]:fcvt.l.h t2, ft7, dyn<br> [0x80000664]:csrrs s1, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br>    |
|  26|[0x800024a8]<br>0x0000000000000000<br> |- rs1 : f6<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x05 and fm1 == 0x3e0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x8000067c]:fcvt.l.h t1, ft6, dyn<br> [0x80000680]:csrrs s1, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br>    |
|  27|[0x800024b8]<br>0x0000000000000000<br> |- rs1 : f5<br> - rd : x5<br> - fs1 == 1 and fe1 == 0x06 and fm1 == 0x274 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x80000698]:fcvt.l.h t0, ft5, dyn<br> [0x8000069c]:csrrs s1, fcsr, zero<br> [0x800006a0]:sd t0, 416(ra)<br>    |
|  28|[0x800024c8]<br>0x0000000000000000<br> |- rs1 : f4<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x07 and fm1 == 0x02d and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x800006bc]:fcvt.l.h tp, ft4, dyn<br> [0x800006c0]:csrrs s1, fcsr, zero<br> [0x800006c4]:sd tp, 0(t0)<br>      |
|  29|[0x800024d8]<br>0x0000000000000000<br> |- rs1 : f3<br> - rd : x3<br> - fs1 == 1 and fe1 == 0x08 and fm1 == 0x004 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x800006d8]:fcvt.l.h gp, ft3, dyn<br> [0x800006dc]:csrrs s1, fcsr, zero<br> [0x800006e0]:sd gp, 16(t0)<br>     |
|  30|[0x800024e8]<br>0x0000000000000000<br> |- rs1 : f2<br> - rd : x2<br> - fs1 == 1 and fe1 == 0x09 and fm1 == 0x089 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x800006f4]:fcvt.l.h sp, ft2, dyn<br> [0x800006f8]:csrrs s1, fcsr, zero<br> [0x800006fc]:sd sp, 32(t0)<br>     |
|  31|[0x800024f8]<br>0x0000000000000000<br> |- rs1 : f1<br> - rd : x1<br> - fs1 == 1 and fe1 == 0x0a and fm1 == 0x3c3 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x80000710]:fcvt.l.h ra, ft1, dyn<br> [0x80000714]:csrrs s1, fcsr, zero<br> [0x80000718]:sd ra, 48(t0)<br>     |
|  32|[0x80002508]<br>0x0000000000000000<br> |- rs1 : f0<br> - rd : x0<br> - fs1 == 1 and fe1 == 0x0b and fm1 == 0x136 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                             |[0x8000072c]:fcvt.l.h zero, ft0, dyn<br> [0x80000730]:csrrs s1, fcsr, zero<br> [0x80000734]:sd zero, 64(t0)<br> |
|  33|[0x80002518]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x0c and fm1 == 0x176 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000748]:fcvt.l.h t6, ft11, dyn<br> [0x8000074c]:csrrs s1, fcsr, zero<br> [0x80000750]:sd t6, 80(t0)<br>    |
|  34|[0x80002528]<br>0x0000000000000000<br> |- fs1 == 1 and fe1 == 0x0d and fm1 == 0x397 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000764]:fcvt.l.h t6, ft11, dyn<br> [0x80000768]:csrrs s1, fcsr, zero<br> [0x8000076c]:sd t6, 96(t0)<br>    |
|  35|[0x80002538]<br>0xFFFFFFFFFFFFFFFF<br> |- fs1 == 1 and fe1 == 0x0e and fm1 == 0x141 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000780]:fcvt.l.h t6, ft11, dyn<br> [0x80000784]:csrrs s1, fcsr, zero<br> [0x80000788]:sd t6, 112(t0)<br>   |
|  36|[0x80002548]<br>0xFFFFFFFFFFFFFFFE<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x232 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x8000079c]:fcvt.l.h t6, ft11, dyn<br> [0x800007a0]:csrrs s1, fcsr, zero<br> [0x800007a4]:sd t6, 128(t0)<br>   |
|  37|[0x80002558]<br>0xFFFFFFFFFFFFFFFD<br> |- fs1 == 1 and fe1 == 0x10 and fm1 == 0x1be and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800007b8]:fcvt.l.h t6, ft11, dyn<br> [0x800007bc]:csrrs s1, fcsr, zero<br> [0x800007c0]:sd t6, 144(t0)<br>   |
|  38|[0x80002568]<br>0xFFFFFFFFFFFFFFFC<br> |- fs1 == 1 and fe1 == 0x11 and fm1 == 0x042 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800007d4]:fcvt.l.h t6, ft11, dyn<br> [0x800007d8]:csrrs s1, fcsr, zero<br> [0x800007dc]:sd t6, 160(t0)<br>   |
|  39|[0x80002578]<br>0xFFFFFFFFFFFFFFF3<br> |- fs1 == 1 and fe1 == 0x12 and fm1 == 0x256 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800007f0]:fcvt.l.h t6, ft11, dyn<br> [0x800007f4]:csrrs s1, fcsr, zero<br> [0x800007f8]:sd t6, 176(t0)<br>   |
|  40|[0x80002588]<br>0xFFFFFFFFFFFFFFE2<br> |- fs1 == 1 and fe1 == 0x13 and fm1 == 0x360 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x8000080c]:fcvt.l.h t6, ft11, dyn<br> [0x80000810]:csrrs s1, fcsr, zero<br> [0x80000814]:sd t6, 192(t0)<br>   |
|  41|[0x80002598]<br>0xFFFFFFFFFFFFFFDB<br> |- fs1 == 1 and fe1 == 0x14 and fm1 == 0x0a0 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000828]:fcvt.l.h t6, ft11, dyn<br> [0x8000082c]:csrrs s1, fcsr, zero<br> [0x80000830]:sd t6, 208(t0)<br>   |
|  42|[0x800025a8]<br>0xFFFFFFFFFFFFFFB2<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x0e5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000844]:fcvt.l.h t6, ft11, dyn<br> [0x80000848]:csrrs s1, fcsr, zero<br> [0x8000084c]:sd t6, 224(t0)<br>   |
|  43|[0x800025b8]<br>0xFFFFFFFFFFFFFF4B<br> |- fs1 == 1 and fe1 == 0x16 and fm1 == 0x1a6 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000860]:fcvt.l.h t6, ft11, dyn<br> [0x80000864]:csrrs s1, fcsr, zero<br> [0x80000868]:sd t6, 240(t0)<br>   |
|  44|[0x800025c8]<br>0xFFFFFFFFFFFFFEF7<br> |- fs1 == 1 and fe1 == 0x17 and fm1 == 0x025 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x8000087c]:fcvt.l.h t6, ft11, dyn<br> [0x80000880]:csrrs s1, fcsr, zero<br> [0x80000884]:sd t6, 256(t0)<br>   |
|  45|[0x800025d8]<br>0xFFFFFFFFFFFFFD02<br> |- fs1 == 1 and fe1 == 0x18 and fm1 == 0x1fc and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000898]:fcvt.l.h t6, ft11, dyn<br> [0x8000089c]:csrrs s1, fcsr, zero<br> [0x800008a0]:sd t6, 272(t0)<br>   |
|  46|[0x800025e8]<br>0xFFFFFFFFFFFFF8BB<br> |- fs1 == 1 and fe1 == 0x19 and fm1 == 0x345 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800008b4]:fcvt.l.h t6, ft11, dyn<br> [0x800008b8]:csrrs s1, fcsr, zero<br> [0x800008bc]:sd t6, 288(t0)<br>   |
|  47|[0x800025f8]<br>0xFFFFFFFFFFFFF2BE<br> |- fs1 == 1 and fe1 == 0x1a and fm1 == 0x2a1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800008d0]:fcvt.l.h t6, ft11, dyn<br> [0x800008d4]:csrrs s1, fcsr, zero<br> [0x800008d8]:sd t6, 304(t0)<br>   |
|  48|[0x80002608]<br>0xFFFFFFFFFFFFE778<br> |- fs1 == 1 and fe1 == 0x1b and fm1 == 0x222 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800008ec]:fcvt.l.h t6, ft11, dyn<br> [0x800008f0]:csrrs s1, fcsr, zero<br> [0x800008f4]:sd t6, 320(t0)<br>   |
|  49|[0x80002618]<br>0xFFFFFFFFFFFFC4A8<br> |- fs1 == 1 and fe1 == 0x1c and fm1 == 0x36b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000908]:fcvt.l.h t6, ft11, dyn<br> [0x8000090c]:csrrs s1, fcsr, zero<br> [0x80000910]:sd t6, 336(t0)<br>   |
|  50|[0x80002628]<br>0xFFFFFFFFFFFFB670<br> |- fs1 == 1 and fe1 == 0x1d and fm1 == 0x099 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000924]:fcvt.l.h t6, ft11, dyn<br> [0x80000928]:csrrs s1, fcsr, zero<br> [0x8000092c]:sd t6, 352(t0)<br>   |
|  51|[0x80002638]<br>0xFFFFFFFFFFFF3780<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x244 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000940]:fcvt.l.h t6, ft11, dyn<br> [0x80000944]:csrrs s1, fcsr, zero<br> [0x80000948]:sd t6, 368(t0)<br>   |
|  52|[0x80002648]<br>0xFFFFFFFFFFFF0020<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x8000095c]:fcvt.l.h t6, ft11, dyn<br> [0x80000960]:csrrs s1, fcsr, zero<br> [0x80000964]:sd t6, 384(t0)<br>   |
|  53|[0x80002658]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x267 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000978]:fcvt.l.h t6, ft11, dyn<br> [0x8000097c]:csrrs s1, fcsr, zero<br> [0x80000980]:sd t6, 400(t0)<br>   |
|  54|[0x80002668]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x073 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000994]:fcvt.l.h t6, ft11, dyn<br> [0x80000998]:csrrs s1, fcsr, zero<br> [0x8000099c]:sd t6, 416(t0)<br>   |
|  55|[0x80002678]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x02 and fm1 == 0x31a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800009b0]:fcvt.l.h t6, ft11, dyn<br> [0x800009b4]:csrrs s1, fcsr, zero<br> [0x800009b8]:sd t6, 432(t0)<br>   |
|  56|[0x80002688]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x03 and fm1 == 0x062 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800009cc]:fcvt.l.h t6, ft11, dyn<br> [0x800009d0]:csrrs s1, fcsr, zero<br> [0x800009d4]:sd t6, 448(t0)<br>   |
|  57|[0x80002698]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x04 and fm1 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x800009e8]:fcvt.l.h t6, ft11, dyn<br> [0x800009ec]:csrrs s1, fcsr, zero<br> [0x800009f0]:sd t6, 464(t0)<br>   |
|  58|[0x800026a8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x05 and fm1 == 0x33a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a04]:fcvt.l.h t6, ft11, dyn<br> [0x80000a08]:csrrs s1, fcsr, zero<br> [0x80000a0c]:sd t6, 480(t0)<br>   |
|  59|[0x800026b8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x06 and fm1 == 0x3ca and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a20]:fcvt.l.h t6, ft11, dyn<br> [0x80000a24]:csrrs s1, fcsr, zero<br> [0x80000a28]:sd t6, 496(t0)<br>   |
|  60|[0x800026c8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x07 and fm1 == 0x1bb and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a3c]:fcvt.l.h t6, ft11, dyn<br> [0x80000a40]:csrrs s1, fcsr, zero<br> [0x80000a44]:sd t6, 512(t0)<br>   |
|  61|[0x800026d8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x08 and fm1 == 0x1a8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a58]:fcvt.l.h t6, ft11, dyn<br> [0x80000a5c]:csrrs s1, fcsr, zero<br> [0x80000a60]:sd t6, 528(t0)<br>   |
|  62|[0x800026e8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x09 and fm1 == 0x0fb and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a74]:fcvt.l.h t6, ft11, dyn<br> [0x80000a78]:csrrs s1, fcsr, zero<br> [0x80000a7c]:sd t6, 544(t0)<br>   |
|  63|[0x800026f8]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x119 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000a90]:fcvt.l.h t6, ft11, dyn<br> [0x80000a94]:csrrs s1, fcsr, zero<br> [0x80000a98]:sd t6, 560(t0)<br>   |
|  64|[0x80002708]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x0b and fm1 == 0x278 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000aac]:fcvt.l.h t6, ft11, dyn<br> [0x80000ab0]:csrrs s1, fcsr, zero<br> [0x80000ab4]:sd t6, 576(t0)<br>   |
|  65|[0x80002718]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x0c and fm1 == 0x0b6 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000ac8]:fcvt.l.h t6, ft11, dyn<br> [0x80000acc]:csrrs s1, fcsr, zero<br> [0x80000ad0]:sd t6, 592(t0)<br>   |
|  66|[0x80002728]<br>0x0000000000000000<br> |- fs1 == 0 and fe1 == 0x0d and fm1 == 0x07a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000ae4]:fcvt.l.h t6, ft11, dyn<br> [0x80000ae8]:csrrs s1, fcsr, zero<br> [0x80000aec]:sd t6, 608(t0)<br>   |
|  67|[0x80002738]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x0e and fm1 == 0x1d2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b00]:fcvt.l.h t6, ft11, dyn<br> [0x80000b04]:csrrs s1, fcsr, zero<br> [0x80000b08]:sd t6, 624(t0)<br>   |
|  68|[0x80002748]<br>0x0000000000000001<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x0a8 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b1c]:fcvt.l.h t6, ft11, dyn<br> [0x80000b20]:csrrs s1, fcsr, zero<br> [0x80000b24]:sd t6, 640(t0)<br>   |
|  69|[0x80002758]<br>0x0000000000000003<br> |- fs1 == 0 and fe1 == 0x10 and fm1 == 0x298 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b38]:fcvt.l.h t6, ft11, dyn<br> [0x80000b3c]:csrrs s1, fcsr, zero<br> [0x80000b40]:sd t6, 656(t0)<br>   |
|  70|[0x80002768]<br>0x0000000000000007<br> |- fs1 == 0 and fe1 == 0x11 and fm1 == 0x348 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b54]:fcvt.l.h t6, ft11, dyn<br> [0x80000b58]:csrrs s1, fcsr, zero<br> [0x80000b5c]:sd t6, 672(t0)<br>   |
|  71|[0x80002778]<br>0x00000000000000E4<br> |- fs1 == 0 and fe1 == 0x16 and fm1 == 0x31c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b70]:fcvt.l.h t6, ft11, dyn<br> [0x80000b74]:csrrs s1, fcsr, zero<br> [0x80000b78]:sd t6, 688(t0)<br>   |
|  72|[0x80002788]<br>0x000000000000038C<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x317 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff  #nosat<br>                                                          |[0x80000b8c]:fcvt.l.h t6, ft11, dyn<br> [0x80000b90]:csrrs s1, fcsr, zero<br> [0x80000b94]:sd t6, 704(t0)<br>   |
