
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000740')]      |
| SIG_REGION                | [('0x80002310', '0x80002520', '66 dwords')]      |
| COV_LABELS                | fcvt.h.lu_b25      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64H/work-fcvthlu/fcvt.h.lu_b25-01.S/ref.S    |
| Total Number of coverpoints| 69     |
| Total Coverpoints Hit     | 67      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.h.lu', 'rs1 : x31', 'rd : f31', 'rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.h.lu ft11, t6, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:fsd ft11, 0(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002320]:0x0000000000000000




Last Coverpoint : ['rs1 : x30', 'rd : f30', 'rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.h.lu ft10, t5, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsd ft10, 16(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002330]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rd : f29']
Last Code Sequence : 
	-[0x800003f0]:fcvt.h.lu ft9, t4, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:fsd ft9, 32(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002340]:0x0000000000000005




Last Coverpoint : ['rs1 : x28', 'rd : f28']
Last Code Sequence : 
	-[0x8000040c]:fcvt.h.lu ft8, t3, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:fsd ft8, 48(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002350]:0x0000000000000005




Last Coverpoint : ['rs1 : x27', 'rd : f27']
Last Code Sequence : 
	-[0x80000428]:fcvt.h.lu fs11, s11, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:fsd fs11, 64(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002360]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rd : f26']
Last Code Sequence : 
	-[0x80000444]:fcvt.h.lu fs10, s10, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:fsd fs10, 80(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002370]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rd : f25']
Last Code Sequence : 
	-[0x80000460]:fcvt.h.lu fs9, s9, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:fsd fs9, 96(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002380]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rd : f24']
Last Code Sequence : 
	-[0x8000047c]:fcvt.h.lu fs8, s8, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:fsd fs8, 112(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002390]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rd : f23']
Last Code Sequence : 
	-[0x80000498]:fcvt.h.lu fs7, s7, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:fsd fs7, 128(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800023a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rd : f22']
Last Code Sequence : 
	-[0x800004b4]:fcvt.h.lu fs6, s6, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:fsd fs6, 144(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rd : f21']
Last Code Sequence : 
	-[0x800004d0]:fcvt.h.lu fs5, s5, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsd fs5, 160(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rd : f20']
Last Code Sequence : 
	-[0x800004ec]:fcvt.h.lu fs4, s4, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:fsd fs4, 176(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : f19']
Last Code Sequence : 
	-[0x80000508]:fcvt.h.lu fs3, s3, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:fsd fs3, 192(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rd : f18']
Last Code Sequence : 
	-[0x80000524]:fcvt.h.lu fs2, s2, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:fsd fs2, 208(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rd : f17']
Last Code Sequence : 
	-[0x80000540]:fcvt.h.lu fa7, a7, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:fsd fa7, 224(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : f16']
Last Code Sequence : 
	-[0x8000055c]:fcvt.h.lu fa6, a6, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:fsd fa6, 240(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rd : f15']
Last Code Sequence : 
	-[0x80000578]:fcvt.h.lu fa5, a5, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:fsd fa5, 256(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : f14']
Last Code Sequence : 
	-[0x80000594]:fcvt.h.lu fa4, a4, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:fsd fa4, 272(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002430]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : f13']
Last Code Sequence : 
	-[0x800005b0]:fcvt.h.lu fa3, a3, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:fsd fa3, 288(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002440]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rd : f12']
Last Code Sequence : 
	-[0x800005cc]:fcvt.h.lu fa2, a2, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsd fa2, 304(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002450]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rd : f11']
Last Code Sequence : 
	-[0x800005e8]:fcvt.h.lu fa1, a1, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:fsd fa1, 320(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002460]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rd : f10']
Last Code Sequence : 
	-[0x80000604]:fcvt.h.lu fa0, a0, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:fsd fa0, 336(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002470]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rd : f9']
Last Code Sequence : 
	-[0x80000620]:fcvt.h.lu fs1, s1, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:fsd fs1, 352(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002480]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rd : f8']
Last Code Sequence : 
	-[0x8000063c]:fcvt.h.lu fs0, fp, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:fsd fs0, 368(ra)
Current Store : [0x80000648] : sd tp, 376(ra) -- Store: [0x80002490]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rd : f7']
Last Code Sequence : 
	-[0x80000660]:fcvt.h.lu ft7, t2, dyn
	-[0x80000664]:csrrs s1, fcsr, zero
	-[0x80000668]:fsd ft7, 384(ra)
Current Store : [0x8000066c] : sd s1, 392(ra) -- Store: [0x800024a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rd : f6']
Last Code Sequence : 
	-[0x8000067c]:fcvt.h.lu ft6, t1, dyn
	-[0x80000680]:csrrs s1, fcsr, zero
	-[0x80000684]:fsd ft6, 400(ra)
Current Store : [0x80000688] : sd s1, 408(ra) -- Store: [0x800024b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rd : f5']
Last Code Sequence : 
	-[0x80000698]:fcvt.h.lu ft5, t0, dyn
	-[0x8000069c]:csrrs s1, fcsr, zero
	-[0x800006a0]:fsd ft5, 416(ra)
Current Store : [0x800006a4] : sd s1, 424(ra) -- Store: [0x800024c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rd : f4']
Last Code Sequence : 
	-[0x800006bc]:fcvt.h.lu ft4, tp, dyn
	-[0x800006c0]:csrrs s1, fcsr, zero
	-[0x800006c4]:fsd ft4, 0(t0)
Current Store : [0x800006c8] : sd s1, 8(t0) -- Store: [0x800024d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rd : f3']
Last Code Sequence : 
	-[0x800006d8]:fcvt.h.lu ft3, gp, dyn
	-[0x800006dc]:csrrs s1, fcsr, zero
	-[0x800006e0]:fsd ft3, 16(t0)
Current Store : [0x800006e4] : sd s1, 24(t0) -- Store: [0x800024e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : f2']
Last Code Sequence : 
	-[0x800006f4]:fcvt.h.lu ft2, sp, dyn
	-[0x800006f8]:csrrs s1, fcsr, zero
	-[0x800006fc]:fsd ft2, 32(t0)
Current Store : [0x80000700] : sd s1, 40(t0) -- Store: [0x800024f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : f1']
Last Code Sequence : 
	-[0x80000710]:fcvt.h.lu ft1, ra, dyn
	-[0x80000714]:csrrs s1, fcsr, zero
	-[0x80000718]:fsd ft1, 48(t0)
Current Store : [0x8000071c] : sd s1, 56(t0) -- Store: [0x80002500]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rd : f0']
Last Code Sequence : 
	-[0x8000072c]:fcvt.h.lu ft0, zero, dyn
	-[0x80000730]:csrrs s1, fcsr, zero
	-[0x80000734]:fsd ft0, 64(t0)
Current Store : [0x80000738] : sd s1, 72(t0) -- Store: [0x80002510]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                    coverpoints                                                     |                                                       code                                                       |
|---:|---------------------------------------|--------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000000<br> |- mnemonic : fcvt.h.lu<br> - rs1 : x31<br> - rd : f31<br> - rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat<br> |[0x800003b8]:fcvt.h.lu ft11, t6, dyn<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:fsd ft11, 0(ra)<br>   |
|   2|[0x80002328]<br>0x0000000000000001<br> |- rs1 : x30<br> - rd : f30<br> - rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat<br>                            |[0x800003d4]:fcvt.h.lu ft10, t5, dyn<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:fsd ft10, 16(ra)<br>  |
|   3|[0x80002338]<br>0xFFFFFFFFFFFFFFFF<br> |- rs1 : x29<br> - rd : f29<br>                                                                                      |[0x800003f0]:fcvt.h.lu ft9, t4, dyn<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:fsd ft9, 32(ra)<br>    |
|   4|[0x80002348]<br>0x924770C10AEFD000<br> |- rs1 : x28<br> - rd : f28<br>                                                                                      |[0x8000040c]:fcvt.h.lu ft8, t3, dyn<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:fsd ft8, 48(ra)<br>    |
|   5|[0x80002358]<br>0x0000000000000000<br> |- rs1 : x27<br> - rd : f27<br>                                                                                      |[0x80000428]:fcvt.h.lu fs11, s11, dyn<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:fsd fs11, 64(ra)<br> |
|   6|[0x80002368]<br>0x0000000000000000<br> |- rs1 : x26<br> - rd : f26<br>                                                                                      |[0x80000444]:fcvt.h.lu fs10, s10, dyn<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:fsd fs10, 80(ra)<br> |
|   7|[0x80002378]<br>0x0000000000000000<br> |- rs1 : x25<br> - rd : f25<br>                                                                                      |[0x80000460]:fcvt.h.lu fs9, s9, dyn<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:fsd fs9, 96(ra)<br>    |
|   8|[0x80002388]<br>0x0000000000000000<br> |- rs1 : x24<br> - rd : f24<br>                                                                                      |[0x8000047c]:fcvt.h.lu fs8, s8, dyn<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:fsd fs8, 112(ra)<br>   |
|   9|[0x80002398]<br>0x0000000000000000<br> |- rs1 : x23<br> - rd : f23<br>                                                                                      |[0x80000498]:fcvt.h.lu fs7, s7, dyn<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:fsd fs7, 128(ra)<br>   |
|  10|[0x800023a8]<br>0x0000000000000000<br> |- rs1 : x22<br> - rd : f22<br>                                                                                      |[0x800004b4]:fcvt.h.lu fs6, s6, dyn<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:fsd fs6, 144(ra)<br>   |
|  11|[0x800023b8]<br>0x0000000000000000<br> |- rs1 : x21<br> - rd : f21<br>                                                                                      |[0x800004d0]:fcvt.h.lu fs5, s5, dyn<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:fsd fs5, 160(ra)<br>   |
|  12|[0x800023c8]<br>0x0000000000000000<br> |- rs1 : x20<br> - rd : f20<br>                                                                                      |[0x800004ec]:fcvt.h.lu fs4, s4, dyn<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:fsd fs4, 176(ra)<br>   |
|  13|[0x800023d8]<br>0x0000000000000000<br> |- rs1 : x19<br> - rd : f19<br>                                                                                      |[0x80000508]:fcvt.h.lu fs3, s3, dyn<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:fsd fs3, 192(ra)<br>   |
|  14|[0x800023e8]<br>0x0000000000000000<br> |- rs1 : x18<br> - rd : f18<br>                                                                                      |[0x80000524]:fcvt.h.lu fs2, s2, dyn<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:fsd fs2, 208(ra)<br>   |
|  15|[0x800023f8]<br>0x0000000000000000<br> |- rs1 : x17<br> - rd : f17<br>                                                                                      |[0x80000540]:fcvt.h.lu fa7, a7, dyn<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:fsd fa7, 224(ra)<br>   |
|  16|[0x80002408]<br>0x0000000000000000<br> |- rs1 : x16<br> - rd : f16<br>                                                                                      |[0x8000055c]:fcvt.h.lu fa6, a6, dyn<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:fsd fa6, 240(ra)<br>   |
|  17|[0x80002418]<br>0x0000000000000000<br> |- rs1 : x15<br> - rd : f15<br>                                                                                      |[0x80000578]:fcvt.h.lu fa5, a5, dyn<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:fsd fa5, 256(ra)<br>   |
|  18|[0x80002428]<br>0x0000000000000000<br> |- rs1 : x14<br> - rd : f14<br>                                                                                      |[0x80000594]:fcvt.h.lu fa4, a4, dyn<br> [0x80000598]:csrrs tp, fcsr, zero<br> [0x8000059c]:fsd fa4, 272(ra)<br>   |
|  19|[0x80002438]<br>0x0000000000000000<br> |- rs1 : x13<br> - rd : f13<br>                                                                                      |[0x800005b0]:fcvt.h.lu fa3, a3, dyn<br> [0x800005b4]:csrrs tp, fcsr, zero<br> [0x800005b8]:fsd fa3, 288(ra)<br>   |
|  20|[0x80002448]<br>0x0000000000000000<br> |- rs1 : x12<br> - rd : f12<br>                                                                                      |[0x800005cc]:fcvt.h.lu fa2, a2, dyn<br> [0x800005d0]:csrrs tp, fcsr, zero<br> [0x800005d4]:fsd fa2, 304(ra)<br>   |
|  21|[0x80002458]<br>0x0000000000000000<br> |- rs1 : x11<br> - rd : f11<br>                                                                                      |[0x800005e8]:fcvt.h.lu fa1, a1, dyn<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:fsd fa1, 320(ra)<br>   |
|  22|[0x80002468]<br>0x0000000000000000<br> |- rs1 : x10<br> - rd : f10<br>                                                                                      |[0x80000604]:fcvt.h.lu fa0, a0, dyn<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:fsd fa0, 336(ra)<br>   |
|  23|[0x80002478]<br>0x0000000000000000<br> |- rs1 : x9<br> - rd : f9<br>                                                                                        |[0x80000620]:fcvt.h.lu fs1, s1, dyn<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:fsd fs1, 352(ra)<br>   |
|  24|[0x80002488]<br>0x0000000000000000<br> |- rs1 : x8<br> - rd : f8<br>                                                                                        |[0x8000063c]:fcvt.h.lu fs0, fp, dyn<br> [0x80000640]:csrrs tp, fcsr, zero<br> [0x80000644]:fsd fs0, 368(ra)<br>   |
|  25|[0x80002498]<br>0x0000000000000000<br> |- rs1 : x7<br> - rd : f7<br>                                                                                        |[0x80000660]:fcvt.h.lu ft7, t2, dyn<br> [0x80000664]:csrrs s1, fcsr, zero<br> [0x80000668]:fsd ft7, 384(ra)<br>   |
|  26|[0x800024a8]<br>0x0000000000000000<br> |- rs1 : x6<br> - rd : f6<br>                                                                                        |[0x8000067c]:fcvt.h.lu ft6, t1, dyn<br> [0x80000680]:csrrs s1, fcsr, zero<br> [0x80000684]:fsd ft6, 400(ra)<br>   |
|  27|[0x800024b8]<br>0x0000000000000000<br> |- rs1 : x5<br> - rd : f5<br>                                                                                        |[0x80000698]:fcvt.h.lu ft5, t0, dyn<br> [0x8000069c]:csrrs s1, fcsr, zero<br> [0x800006a0]:fsd ft5, 416(ra)<br>   |
|  28|[0x800024c8]<br>0x0000000000000000<br> |- rs1 : x4<br> - rd : f4<br>                                                                                        |[0x800006bc]:fcvt.h.lu ft4, tp, dyn<br> [0x800006c0]:csrrs s1, fcsr, zero<br> [0x800006c4]:fsd ft4, 0(t0)<br>     |
|  29|[0x800024d8]<br>0x0000000000000000<br> |- rs1 : x3<br> - rd : f3<br>                                                                                        |[0x800006d8]:fcvt.h.lu ft3, gp, dyn<br> [0x800006dc]:csrrs s1, fcsr, zero<br> [0x800006e0]:fsd ft3, 16(t0)<br>    |
|  30|[0x800024e8]<br>0x0000000000000000<br> |- rs1 : x2<br> - rd : f2<br>                                                                                        |[0x800006f4]:fcvt.h.lu ft2, sp, dyn<br> [0x800006f8]:csrrs s1, fcsr, zero<br> [0x800006fc]:fsd ft2, 32(t0)<br>    |
|  31|[0x800024f8]<br>0x0000000000000000<br> |- rs1 : x1<br> - rd : f1<br>                                                                                        |[0x80000710]:fcvt.h.lu ft1, ra, dyn<br> [0x80000714]:csrrs s1, fcsr, zero<br> [0x80000718]:fsd ft1, 48(t0)<br>    |
|  32|[0x80002508]<br>0x0000000000000000<br> |- rs1 : x0<br> - rd : f0<br>                                                                                        |[0x8000072c]:fcvt.h.lu ft0, zero, dyn<br> [0x80000730]:csrrs s1, fcsr, zero<br> [0x80000734]:fsd ft0, 64(t0)<br>  |
