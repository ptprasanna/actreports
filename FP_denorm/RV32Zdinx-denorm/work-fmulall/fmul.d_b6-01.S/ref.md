
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002e50')]      |
| SIG_REGION                | [('0x80004a10', '0x80005300', '572 words')]      |
| COV_LABELS                | fmul.d_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/FMA/work-fmulall/fmul.d_b6-01.S/ref.S    |
| Total Number of coverpoints| 191     |
| Total Coverpoints Hit     | 191      |
| Total Signature Updates   | 308      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 142     |
| STAT4                     | 231     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000014c]:fmul.d t5, t5, t3, dyn
[0x80000150]:csrrs tp, fcsr, zero
[0x80000154]:sw t5, 0(ra)
[0x80000158]:sw t6, 8(ra)
[0x8000015c]:sw t5, 16(ra)
[0x80000160]:sw tp, 24(ra)
[0x80000164]:lw s10, 16(gp)
[0x80000168]:lw s11, 20(gp)
[0x8000016c]:lw t5, 24(gp)
[0x80000170]:lw t6, 28(gp)
[0x80000174]:lui s10, 363185
[0x80000178]:addi s10, s10, 1577
[0x8000017c]:lui s11, 236108
[0x80000180]:addi s11, s11, 3425
[0x80000184]:lui t5, 1002677
[0x80000188]:addi t5, t5, 2233
[0x8000018c]:lui t6, 269707
[0x80000190]:addi t6, t6, 4054
[0x80000194]:addi sp, zero, 34
[0x80000198]:csrrw zero, fcsr, sp
[0x8000019c]:fmul.d t3, s10, t5, dyn

[0x8000019c]:fmul.d t3, s10, t5, dyn
[0x800001a0]:csrrs tp, fcsr, zero
[0x800001a4]:sw t3, 32(ra)
[0x800001a8]:sw t4, 40(ra)
[0x800001ac]:sw t3, 48(ra)
[0x800001b0]:sw tp, 56(ra)
[0x800001b4]:lw t3, 32(gp)
[0x800001b8]:lw t4, 36(gp)
[0x800001bc]:lw s10, 40(gp)
[0x800001c0]:lw s11, 44(gp)
[0x800001c4]:lui t3, 363185
[0x800001c8]:addi t3, t3, 1577
[0x800001cc]:lui t4, 236108
[0x800001d0]:addi t4, t4, 3425
[0x800001d4]:lui s10, 1002677
[0x800001d8]:addi s10, s10, 2233
[0x800001dc]:lui s11, 269707
[0x800001e0]:addi s11, s11, 4054
[0x800001e4]:addi sp, zero, 66
[0x800001e8]:csrrw zero, fcsr, sp
[0x800001ec]:fmul.d s10, t3, s10, dyn

[0x800001ec]:fmul.d s10, t3, s10, dyn
[0x800001f0]:csrrs tp, fcsr, zero
[0x800001f4]:sw s10, 64(ra)
[0x800001f8]:sw s11, 72(ra)
[0x800001fc]:sw s10, 80(ra)
[0x80000200]:sw tp, 88(ra)
[0x80000204]:lw s8, 48(gp)
[0x80000208]:lw s9, 52(gp)
[0x8000020c]:lw s8, 56(gp)
[0x80000210]:lw s9, 60(gp)
[0x80000214]:lui s8, 363185
[0x80000218]:addi s8, s8, 1577
[0x8000021c]:lui s9, 236108
[0x80000220]:addi s9, s9, 3425
[0x80000224]:lui s8, 363185
[0x80000228]:addi s8, s8, 1577
[0x8000022c]:lui s9, 236108
[0x80000230]:addi s9, s9, 3425
[0x80000234]:addi sp, zero, 98
[0x80000238]:csrrw zero, fcsr, sp
[0x8000023c]:fmul.d s8, s8, s8, dyn

[0x8000023c]:fmul.d s8, s8, s8, dyn
[0x80000240]:csrrs tp, fcsr, zero
[0x80000244]:sw s8, 96(ra)
[0x80000248]:sw s9, 104(ra)
[0x8000024c]:sw s8, 112(ra)
[0x80000250]:sw tp, 120(ra)
[0x80000254]:lw s4, 64(gp)
[0x80000258]:lw s5, 68(gp)
[0x8000025c]:lw s4, 72(gp)
[0x80000260]:lw s5, 76(gp)
[0x80000264]:lui s4, 363185
[0x80000268]:addi s4, s4, 1577
[0x8000026c]:lui s5, 236108
[0x80000270]:addi s5, s5, 3425
[0x80000274]:lui s4, 363185
[0x80000278]:addi s4, s4, 1577
[0x8000027c]:lui s5, 236108
[0x80000280]:addi s5, s5, 3425
[0x80000284]:addi sp, zero, 130
[0x80000288]:csrrw zero, fcsr, sp
[0x8000028c]:fmul.d s6, s4, s4, dyn

[0x8000028c]:fmul.d s6, s4, s4, dyn
[0x80000290]:csrrs tp, fcsr, zero
[0x80000294]:sw s6, 128(ra)
[0x80000298]:sw s7, 136(ra)
[0x8000029c]:sw s6, 144(ra)
[0x800002a0]:sw tp, 152(ra)
[0x800002a4]:lw s6, 80(gp)
[0x800002a8]:lw s7, 84(gp)
[0x800002ac]:lw s2, 88(gp)
[0x800002b0]:lw s3, 92(gp)
[0x800002b4]:lui s6, 57885
[0x800002b8]:addi s6, s6, 145
[0x800002bc]:lui s7, 236285
[0x800002c0]:addi s7, s7, 2926
[0x800002c4]:lui s2, 453803
[0x800002c8]:addi s2, s2, 2982
[0x800002cc]:lui s3, 265986
[0x800002d0]:addi s3, s3, 2676
[0x800002d4]:addi sp, zero, 2
[0x800002d8]:csrrw zero, fcsr, sp
[0x800002dc]:fmul.d s4, s6, s2, dyn

[0x800002dc]:fmul.d s4, s6, s2, dyn
[0x800002e0]:csrrs tp, fcsr, zero
[0x800002e4]:sw s4, 160(ra)
[0x800002e8]:sw s5, 168(ra)
[0x800002ec]:sw s4, 176(ra)
[0x800002f0]:sw tp, 184(ra)
[0x800002f4]:lw a6, 96(gp)
[0x800002f8]:lw a7, 100(gp)
[0x800002fc]:lw s6, 104(gp)
[0x80000300]:lw s7, 108(gp)
[0x80000304]:lui a6, 57885
[0x80000308]:addi a6, a6, 145
[0x8000030c]:lui a7, 236285
[0x80000310]:addi a7, a7, 2926
[0x80000314]:lui s6, 453803
[0x80000318]:addi s6, s6, 2982
[0x8000031c]:lui s7, 265986
[0x80000320]:addi s7, s7, 2676
[0x80000324]:addi sp, zero, 34
[0x80000328]:csrrw zero, fcsr, sp
[0x8000032c]:fmul.d s2, a6, s6, dyn

[0x8000032c]:fmul.d s2, a6, s6, dyn
[0x80000330]:csrrs tp, fcsr, zero
[0x80000334]:sw s2, 192(ra)
[0x80000338]:sw s3, 200(ra)
[0x8000033c]:sw s2, 208(ra)
[0x80000340]:sw tp, 216(ra)
[0x80000344]:lw s2, 112(gp)
[0x80000348]:lw s3, 116(gp)
[0x8000034c]:lw a4, 120(gp)
[0x80000350]:lw a5, 124(gp)
[0x80000354]:lui s2, 57885
[0x80000358]:addi s2, s2, 145
[0x8000035c]:lui s3, 236285
[0x80000360]:addi s3, s3, 2926
[0x80000364]:lui a4, 453803
[0x80000368]:addi a4, a4, 2982
[0x8000036c]:lui a5, 265986
[0x80000370]:addi a5, a5, 2676
[0x80000374]:addi sp, zero, 66
[0x80000378]:csrrw zero, fcsr, sp
[0x8000037c]:fmul.d a6, s2, a4, dyn

[0x8000037c]:fmul.d a6, s2, a4, dyn
[0x80000380]:csrrs tp, fcsr, zero
[0x80000384]:sw a6, 224(ra)
[0x80000388]:sw a7, 232(ra)
[0x8000038c]:sw a6, 240(ra)
[0x80000390]:sw tp, 248(ra)
[0x80000394]:lw a2, 128(gp)
[0x80000398]:lw a3, 132(gp)
[0x8000039c]:lw a6, 136(gp)
[0x800003a0]:lw a7, 140(gp)
[0x800003a4]:lui a2, 57885
[0x800003a8]:addi a2, a2, 145
[0x800003ac]:lui a3, 236285
[0x800003b0]:addi a3, a3, 2926
[0x800003b4]:lui a6, 453803
[0x800003b8]:addi a6, a6, 2982
[0x800003bc]:lui a7, 265986
[0x800003c0]:addi a7, a7, 2676
[0x800003c4]:addi sp, zero, 98
[0x800003c8]:csrrw zero, fcsr, sp
[0x800003cc]:fmul.d a4, a2, a6, dyn

[0x800003cc]:fmul.d a4, a2, a6, dyn
[0x800003d0]:csrrs tp, fcsr, zero
[0x800003d4]:sw a4, 256(ra)
[0x800003d8]:sw a5, 264(ra)
[0x800003dc]:sw a4, 272(ra)
[0x800003e0]:sw tp, 280(ra)
[0x800003e4]:auipc a6, 4
[0x800003e8]:addi a6, a6, 3260
[0x800003ec]:lw a4, 144(a6)
[0x800003f0]:lw a5, 148(a6)
[0x800003f4]:lw a0, 152(a6)
[0x800003f8]:lw a1, 156(a6)
[0x800003fc]:lui a4, 57885
[0x80000400]:addi a4, a4, 145
[0x80000404]:lui a5, 236285
[0x80000408]:addi a5, a5, 2926
[0x8000040c]:lui a0, 453803
[0x80000410]:addi a0, a0, 2982
[0x80000414]:lui a1, 265986
[0x80000418]:addi a1, a1, 2676
[0x8000041c]:addi sp, zero, 130
[0x80000420]:csrrw zero, fcsr, sp
[0x80000424]:fmul.d a2, a4, a0, dyn

[0x80000424]:fmul.d a2, a4, a0, dyn
[0x80000428]:csrrs a7, fcsr, zero
[0x8000042c]:sw a2, 288(ra)
[0x80000430]:sw a3, 296(ra)
[0x80000434]:sw a2, 304(ra)
[0x80000438]:sw a7, 312(ra)
[0x8000043c]:lw fp, 160(a6)
[0x80000440]:lw s1, 164(a6)
[0x80000444]:lw a2, 168(a6)
[0x80000448]:lw a3, 172(a6)
[0x8000044c]:lui fp, 29706
[0x80000450]:addi fp, fp, 1776
[0x80000454]:lui s1, 236085
[0x80000458]:addi s1, s1, 1348
[0x8000045c]:lui a2, 715137
[0x80000460]:addi a2, a2, 2352
[0x80000464]:lui a3, 264052
[0x80000468]:addi a3, a3, 2372
[0x8000046c]:addi a4, zero, 2
[0x80000470]:csrrw zero, fcsr, a4
[0x80000474]:fmul.d a0, fp, a2, dyn

[0x80000474]:fmul.d a0, fp, a2, dyn
[0x80000478]:csrrs a7, fcsr, zero
[0x8000047c]:sw a0, 320(ra)
[0x80000480]:sw a1, 328(ra)
[0x80000484]:sw a0, 336(ra)
[0x80000488]:sw a7, 344(ra)
[0x8000048c]:auipc ra, 4
[0x80000490]:addi ra, ra, 1596
[0x80000494]:lw a0, 0(a6)
[0x80000498]:lw a1, 4(a6)
[0x8000049c]:lw t1, 8(a6)
[0x800004a0]:lw t2, 12(a6)
[0x800004a4]:lui a0, 29706
[0x800004a8]:addi a0, a0, 1776
[0x800004ac]:lui a1, 236085
[0x800004b0]:addi a1, a1, 1348
[0x800004b4]:lui t1, 715137
[0x800004b8]:addi t1, t1, 2352
[0x800004bc]:lui t2, 264052
[0x800004c0]:addi t2, t2, 2372
[0x800004c4]:addi a4, zero, 34
[0x800004c8]:csrrw zero, fcsr, a4
[0x800004cc]:fmul.d fp, a0, t1, dyn

[0x800004cc]:fmul.d fp, a0, t1, dyn
[0x800004d0]:csrrs a7, fcsr, zero
[0x800004d4]:sw fp, 0(ra)
[0x800004d8]:sw s1, 8(ra)
[0x800004dc]:sw fp, 16(ra)
[0x800004e0]:sw a7, 24(ra)
[0x800004e4]:lw tp, 16(a6)
[0x800004e8]:lw t0, 20(a6)
[0x800004ec]:lw fp, 24(a6)
[0x800004f0]:lw s1, 28(a6)
[0x800004f4]:lui tp, 29706
[0x800004f8]:addi tp, tp, 1776
[0x800004fc]:lui t0, 236085
[0x80000500]:addi t0, t0, 1348
[0x80000504]:lui fp, 715137
[0x80000508]:addi fp, fp, 2352
[0x8000050c]:lui s1, 264052
[0x80000510]:addi s1, s1, 2372
[0x80000514]:addi a4, zero, 66
[0x80000518]:csrrw zero, fcsr, a4
[0x8000051c]:fmul.d t1, tp, fp, dyn

[0x8000051c]:fmul.d t1, tp, fp, dyn
[0x80000520]:csrrs a7, fcsr, zero
[0x80000524]:sw t1, 32(ra)
[0x80000528]:sw t2, 40(ra)
[0x8000052c]:sw t1, 48(ra)
[0x80000530]:sw a7, 56(ra)
[0x80000534]:lw t1, 32(a6)
[0x80000538]:lw t2, 36(a6)
[0x8000053c]:lw sp, 40(a6)
[0x80000540]:lw gp, 44(a6)
[0x80000544]:lui t1, 29706
[0x80000548]:addi t1, t1, 1776
[0x8000054c]:lui t2, 236085
[0x80000550]:addi t2, t2, 1348
[0x80000554]:lui sp, 715137
[0x80000558]:addi sp, sp, 2352
[0x8000055c]:lui gp, 264052
[0x80000560]:addi gp, gp, 2372
[0x80000564]:addi a4, zero, 98
[0x80000568]:csrrw zero, fcsr, a4
[0x8000056c]:fmul.d tp, t1, sp, dyn

[0x8000056c]:fmul.d tp, t1, sp, dyn
[0x80000570]:csrrs a7, fcsr, zero
[0x80000574]:sw tp, 64(ra)
[0x80000578]:sw t0, 72(ra)
[0x8000057c]:sw tp, 80(ra)
[0x80000580]:sw a7, 88(ra)
[0x80000584]:lw sp, 48(a6)
[0x80000588]:lw gp, 52(a6)
[0x8000058c]:lw t3, 56(a6)
[0x80000590]:lw t4, 60(a6)
[0x80000594]:lui sp, 29706
[0x80000598]:addi sp, sp, 1776
[0x8000059c]:lui gp, 236085
[0x800005a0]:addi gp, gp, 1348
[0x800005a4]:lui t3, 715137
[0x800005a8]:addi t3, t3, 2352
[0x800005ac]:lui t4, 264052
[0x800005b0]:addi t4, t4, 2372
[0x800005b4]:addi a4, zero, 130
[0x800005b8]:csrrw zero, fcsr, a4
[0x800005bc]:fmul.d t5, sp, t3, dyn

[0x800005bc]:fmul.d t5, sp, t3, dyn
[0x800005c0]:csrrs a7, fcsr, zero
[0x800005c4]:sw t5, 96(ra)
[0x800005c8]:sw t6, 104(ra)
[0x800005cc]:sw t5, 112(ra)
[0x800005d0]:sw a7, 120(ra)
[0x800005d4]:lw t3, 64(a6)
[0x800005d8]:lw t4, 68(a6)
[0x800005dc]:lw tp, 72(a6)
[0x800005e0]:lw t0, 76(a6)
[0x800005e4]:lui t3, 73751
[0x800005e8]:addi t3, t3, 938
[0x800005ec]:lui t4, 236327
[0x800005f0]:addi t4, t4, 2865
[0x800005f4]:lui tp, 253695
[0x800005f8]:addi tp, tp, 1139
[0x800005fc]:lui t0, 262333
[0x80000600]:addi t0, t0, 3133
[0x80000604]:addi a4, zero, 2
[0x80000608]:csrrw zero, fcsr, a4
[0x8000060c]:fmul.d t5, t3, tp, dyn

[0x8000060c]:fmul.d t5, t3, tp, dyn
[0x80000610]:csrrs a7, fcsr, zero
[0x80000614]:sw t5, 128(ra)
[0x80000618]:sw t6, 136(ra)
[0x8000061c]:sw t5, 144(ra)
[0x80000620]:sw a7, 152(ra)
[0x80000624]:lw t5, 80(a6)
[0x80000628]:lw t6, 84(a6)
[0x8000062c]:lw t3, 88(a6)
[0x80000630]:lw t4, 92(a6)
[0x80000634]:lui t5, 73751
[0x80000638]:addi t5, t5, 938
[0x8000063c]:lui t6, 236327
[0x80000640]:addi t6, t6, 2865
[0x80000644]:lui t3, 253695
[0x80000648]:addi t3, t3, 1139
[0x8000064c]:lui t4, 262333
[0x80000650]:addi t4, t4, 3133
[0x80000654]:addi a4, zero, 34
[0x80000658]:csrrw zero, fcsr, a4
[0x8000065c]:fmul.d sp, t5, t3, dyn

[0x8000065c]:fmul.d sp, t5, t3, dyn
[0x80000660]:csrrs a7, fcsr, zero
[0x80000664]:sw sp, 160(ra)
[0x80000668]:sw gp, 168(ra)
[0x8000066c]:sw sp, 176(ra)
[0x80000670]:sw a7, 184(ra)
[0x80000674]:lw t3, 96(a6)
[0x80000678]:lw t4, 100(a6)
[0x8000067c]:lw s10, 104(a6)
[0x80000680]:lw s11, 108(a6)
[0x80000684]:lui t3, 73751
[0x80000688]:addi t3, t3, 938
[0x8000068c]:lui t4, 236327
[0x80000690]:addi t4, t4, 2865
[0x80000694]:lui s10, 253695
[0x80000698]:addi s10, s10, 1139
[0x8000069c]:lui s11, 262333
[0x800006a0]:addi s11, s11, 3133
[0x800006a4]:addi a4, zero, 66
[0x800006a8]:csrrw zero, fcsr, a4
[0x800006ac]:fmul.d t5, t3, s10, dyn

[0x800006ac]:fmul.d t5, t3, s10, dyn
[0x800006b0]:csrrs a7, fcsr, zero
[0x800006b4]:sw t5, 192(ra)
[0x800006b8]:sw t6, 200(ra)
[0x800006bc]:sw t5, 208(ra)
[0x800006c0]:sw a7, 216(ra)
[0x800006c4]:lw t3, 112(a6)
[0x800006c8]:lw t4, 116(a6)
[0x800006cc]:lw s10, 120(a6)
[0x800006d0]:lw s11, 124(a6)
[0x800006d4]:lui t3, 73751
[0x800006d8]:addi t3, t3, 938
[0x800006dc]:lui t4, 236327
[0x800006e0]:addi t4, t4, 2865
[0x800006e4]:lui s10, 253695
[0x800006e8]:addi s10, s10, 1139
[0x800006ec]:lui s11, 262333
[0x800006f0]:addi s11, s11, 3133
[0x800006f4]:addi a4, zero, 98
[0x800006f8]:csrrw zero, fcsr, a4
[0x800006fc]:fmul.d t5, t3, s10, dyn

[0x800006fc]:fmul.d t5, t3, s10, dyn
[0x80000700]:csrrs a7, fcsr, zero
[0x80000704]:sw t5, 224(ra)
[0x80000708]:sw t6, 232(ra)
[0x8000070c]:sw t5, 240(ra)
[0x80000710]:sw a7, 248(ra)
[0x80000714]:lw t3, 128(a6)
[0x80000718]:lw t4, 132(a6)
[0x8000071c]:lw s10, 136(a6)
[0x80000720]:lw s11, 140(a6)
[0x80000724]:lui t3, 73751
[0x80000728]:addi t3, t3, 938
[0x8000072c]:lui t4, 236327
[0x80000730]:addi t4, t4, 2865
[0x80000734]:lui s10, 253695
[0x80000738]:addi s10, s10, 1139
[0x8000073c]:lui s11, 262333
[0x80000740]:addi s11, s11, 3133
[0x80000744]:addi a4, zero, 130
[0x80000748]:csrrw zero, fcsr, a4
[0x8000074c]:fmul.d t5, t3, s10, dyn

[0x8000074c]:fmul.d t5, t3, s10, dyn
[0x80000750]:csrrs a7, fcsr, zero
[0x80000754]:sw t5, 256(ra)
[0x80000758]:sw t6, 264(ra)
[0x8000075c]:sw t5, 272(ra)
[0x80000760]:sw a7, 280(ra)
[0x80000764]:lw t3, 144(a6)
[0x80000768]:lw t4, 148(a6)
[0x8000076c]:lw s10, 152(a6)
[0x80000770]:lw s11, 156(a6)
[0x80000774]:lui t3, 12783
[0x80000778]:addi t3, t3, 939
[0x8000077c]:lui t4, 235886
[0x80000780]:addi t4, t4, 3467
[0x80000784]:lui s10, 34646
[0x80000788]:addi s10, s10, 758
[0x8000078c]:lui s11, 261624
[0x80000790]:addi s11, s11, 905
[0x80000794]:addi a4, zero, 2
[0x80000798]:csrrw zero, fcsr, a4
[0x8000079c]:fmul.d t5, t3, s10, dyn

[0x8000079c]:fmul.d t5, t3, s10, dyn
[0x800007a0]:csrrs a7, fcsr, zero
[0x800007a4]:sw t5, 288(ra)
[0x800007a8]:sw t6, 296(ra)
[0x800007ac]:sw t5, 304(ra)
[0x800007b0]:sw a7, 312(ra)
[0x800007b4]:lw t3, 160(a6)
[0x800007b8]:lw t4, 164(a6)
[0x800007bc]:lw s10, 168(a6)
[0x800007c0]:lw s11, 172(a6)
[0x800007c4]:lui t3, 12783
[0x800007c8]:addi t3, t3, 939
[0x800007cc]:lui t4, 235886
[0x800007d0]:addi t4, t4, 3467
[0x800007d4]:lui s10, 34646
[0x800007d8]:addi s10, s10, 758
[0x800007dc]:lui s11, 261624
[0x800007e0]:addi s11, s11, 905
[0x800007e4]:addi a4, zero, 34
[0x800007e8]:csrrw zero, fcsr, a4
[0x800007ec]:fmul.d t5, t3, s10, dyn

[0x800007ec]:fmul.d t5, t3, s10, dyn
[0x800007f0]:csrrs a7, fcsr, zero
[0x800007f4]:sw t5, 320(ra)
[0x800007f8]:sw t6, 328(ra)
[0x800007fc]:sw t5, 336(ra)
[0x80000800]:sw a7, 344(ra)
[0x80000804]:lw t3, 176(a6)
[0x80000808]:lw t4, 180(a6)
[0x8000080c]:lw s10, 184(a6)
[0x80000810]:lw s11, 188(a6)
[0x80000814]:lui t3, 12783
[0x80000818]:addi t3, t3, 939
[0x8000081c]:lui t4, 235886
[0x80000820]:addi t4, t4, 3467
[0x80000824]:lui s10, 34646
[0x80000828]:addi s10, s10, 758
[0x8000082c]:lui s11, 261624
[0x80000830]:addi s11, s11, 905
[0x80000834]:addi a4, zero, 66
[0x80000838]:csrrw zero, fcsr, a4
[0x8000083c]:fmul.d t5, t3, s10, dyn

[0x8000083c]:fmul.d t5, t3, s10, dyn
[0x80000840]:csrrs a7, fcsr, zero
[0x80000844]:sw t5, 352(ra)
[0x80000848]:sw t6, 360(ra)
[0x8000084c]:sw t5, 368(ra)
[0x80000850]:sw a7, 376(ra)
[0x80000854]:lw t3, 192(a6)
[0x80000858]:lw t4, 196(a6)
[0x8000085c]:lw s10, 200(a6)
[0x80000860]:lw s11, 204(a6)
[0x80000864]:lui t3, 12783
[0x80000868]:addi t3, t3, 939
[0x8000086c]:lui t4, 235886
[0x80000870]:addi t4, t4, 3467
[0x80000874]:lui s10, 34646
[0x80000878]:addi s10, s10, 758
[0x8000087c]:lui s11, 261624
[0x80000880]:addi s11, s11, 905
[0x80000884]:addi a4, zero, 98
[0x80000888]:csrrw zero, fcsr, a4
[0x8000088c]:fmul.d t5, t3, s10, dyn

[0x8000088c]:fmul.d t5, t3, s10, dyn
[0x80000890]:csrrs a7, fcsr, zero
[0x80000894]:sw t5, 384(ra)
[0x80000898]:sw t6, 392(ra)
[0x8000089c]:sw t5, 400(ra)
[0x800008a0]:sw a7, 408(ra)
[0x800008a4]:lw t3, 208(a6)
[0x800008a8]:lw t4, 212(a6)
[0x800008ac]:lw s10, 216(a6)
[0x800008b0]:lw s11, 220(a6)
[0x800008b4]:lui t3, 12783
[0x800008b8]:addi t3, t3, 939
[0x800008bc]:lui t4, 235886
[0x800008c0]:addi t4, t4, 3467
[0x800008c4]:lui s10, 34646
[0x800008c8]:addi s10, s10, 758
[0x800008cc]:lui s11, 261624
[0x800008d0]:addi s11, s11, 905
[0x800008d4]:addi a4, zero, 130
[0x800008d8]:csrrw zero, fcsr, a4
[0x800008dc]:fmul.d t5, t3, s10, dyn

[0x800008dc]:fmul.d t5, t3, s10, dyn
[0x800008e0]:csrrs a7, fcsr, zero
[0x800008e4]:sw t5, 416(ra)
[0x800008e8]:sw t6, 424(ra)
[0x800008ec]:sw t5, 432(ra)
[0x800008f0]:sw a7, 440(ra)
[0x800008f4]:lw t3, 224(a6)
[0x800008f8]:lw t4, 228(a6)
[0x800008fc]:lw s10, 232(a6)
[0x80000900]:lw s11, 236(a6)
[0x80000904]:lui t3, 801335
[0x80000908]:addi t3, t3, 3892
[0x8000090c]:lui t4, 236177
[0x80000910]:addi t4, t4, 1410
[0x80000914]:lui s10, 865482
[0x80000918]:addi s10, s10, 2301
[0x8000091c]:lui s11, 260382
[0x80000920]:addi s11, s11, 1589
[0x80000924]:addi a4, zero, 2
[0x80000928]:csrrw zero, fcsr, a4
[0x8000092c]:fmul.d t5, t3, s10, dyn

[0x8000092c]:fmul.d t5, t3, s10, dyn
[0x80000930]:csrrs a7, fcsr, zero
[0x80000934]:sw t5, 448(ra)
[0x80000938]:sw t6, 456(ra)
[0x8000093c]:sw t5, 464(ra)
[0x80000940]:sw a7, 472(ra)
[0x80000944]:lw t3, 240(a6)
[0x80000948]:lw t4, 244(a6)
[0x8000094c]:lw s10, 248(a6)
[0x80000950]:lw s11, 252(a6)
[0x80000954]:lui t3, 801335
[0x80000958]:addi t3, t3, 3892
[0x8000095c]:lui t4, 236177
[0x80000960]:addi t4, t4, 1410
[0x80000964]:lui s10, 865482
[0x80000968]:addi s10, s10, 2301
[0x8000096c]:lui s11, 260382
[0x80000970]:addi s11, s11, 1589
[0x80000974]:addi a4, zero, 34
[0x80000978]:csrrw zero, fcsr, a4
[0x8000097c]:fmul.d t5, t3, s10, dyn

[0x8000097c]:fmul.d t5, t3, s10, dyn
[0x80000980]:csrrs a7, fcsr, zero
[0x80000984]:sw t5, 480(ra)
[0x80000988]:sw t6, 488(ra)
[0x8000098c]:sw t5, 496(ra)
[0x80000990]:sw a7, 504(ra)
[0x80000994]:lw t3, 256(a6)
[0x80000998]:lw t4, 260(a6)
[0x8000099c]:lw s10, 264(a6)
[0x800009a0]:lw s11, 268(a6)
[0x800009a4]:lui t3, 801335
[0x800009a8]:addi t3, t3, 3892
[0x800009ac]:lui t4, 236177
[0x800009b0]:addi t4, t4, 1410
[0x800009b4]:lui s10, 865482
[0x800009b8]:addi s10, s10, 2301
[0x800009bc]:lui s11, 260382
[0x800009c0]:addi s11, s11, 1589
[0x800009c4]:addi a4, zero, 66
[0x800009c8]:csrrw zero, fcsr, a4
[0x800009cc]:fmul.d t5, t3, s10, dyn

[0x800009cc]:fmul.d t5, t3, s10, dyn
[0x800009d0]:csrrs a7, fcsr, zero
[0x800009d4]:sw t5, 512(ra)
[0x800009d8]:sw t6, 520(ra)
[0x800009dc]:sw t5, 528(ra)
[0x800009e0]:sw a7, 536(ra)
[0x800009e4]:lw t3, 272(a6)
[0x800009e8]:lw t4, 276(a6)
[0x800009ec]:lw s10, 280(a6)
[0x800009f0]:lw s11, 284(a6)
[0x800009f4]:lui t3, 801335
[0x800009f8]:addi t3, t3, 3892
[0x800009fc]:lui t4, 236177
[0x80000a00]:addi t4, t4, 1410
[0x80000a04]:lui s10, 865482
[0x80000a08]:addi s10, s10, 2301
[0x80000a0c]:lui s11, 260382
[0x80000a10]:addi s11, s11, 1589
[0x80000a14]:addi a4, zero, 98
[0x80000a18]:csrrw zero, fcsr, a4
[0x80000a1c]:fmul.d t5, t3, s10, dyn

[0x80000a1c]:fmul.d t5, t3, s10, dyn
[0x80000a20]:csrrs a7, fcsr, zero
[0x80000a24]:sw t5, 544(ra)
[0x80000a28]:sw t6, 552(ra)
[0x80000a2c]:sw t5, 560(ra)
[0x80000a30]:sw a7, 568(ra)
[0x80000a34]:lw t3, 288(a6)
[0x80000a38]:lw t4, 292(a6)
[0x80000a3c]:lw s10, 296(a6)
[0x80000a40]:lw s11, 300(a6)
[0x80000a44]:lui t3, 801335
[0x80000a48]:addi t3, t3, 3892
[0x80000a4c]:lui t4, 236177
[0x80000a50]:addi t4, t4, 1410
[0x80000a54]:lui s10, 865482
[0x80000a58]:addi s10, s10, 2301
[0x80000a5c]:lui s11, 260382
[0x80000a60]:addi s11, s11, 1589
[0x80000a64]:addi a4, zero, 130
[0x80000a68]:csrrw zero, fcsr, a4
[0x80000a6c]:fmul.d t5, t3, s10, dyn

[0x80000a6c]:fmul.d t5, t3, s10, dyn
[0x80000a70]:csrrs a7, fcsr, zero
[0x80000a74]:sw t5, 576(ra)
[0x80000a78]:sw t6, 584(ra)
[0x80000a7c]:sw t5, 592(ra)
[0x80000a80]:sw a7, 600(ra)
[0x80000a84]:lw t3, 304(a6)
[0x80000a88]:lw t4, 308(a6)
[0x80000a8c]:lw s10, 312(a6)
[0x80000a90]:lw s11, 316(a6)
[0x80000a94]:lui t3, 201844
[0x80000a98]:addi t3, t3, 242
[0x80000a9c]:lui t4, 236327
[0x80000aa0]:addi t4, t4, 940
[0x80000aa4]:lui s10, 462235
[0x80000aa8]:addi s10, s10, 509
[0x80000aac]:lui s11, 259432
[0x80000ab0]:addi s11, s11, 3569
[0x80000ab4]:addi a4, zero, 2
[0x80000ab8]:csrrw zero, fcsr, a4
[0x80000abc]:fmul.d t5, t3, s10, dyn

[0x80000abc]:fmul.d t5, t3, s10, dyn
[0x80000ac0]:csrrs a7, fcsr, zero
[0x80000ac4]:sw t5, 608(ra)
[0x80000ac8]:sw t6, 616(ra)
[0x80000acc]:sw t5, 624(ra)
[0x80000ad0]:sw a7, 632(ra)
[0x80000ad4]:lw t3, 320(a6)
[0x80000ad8]:lw t4, 324(a6)
[0x80000adc]:lw s10, 328(a6)
[0x80000ae0]:lw s11, 332(a6)
[0x80000ae4]:lui t3, 201844
[0x80000ae8]:addi t3, t3, 242
[0x80000aec]:lui t4, 236327
[0x80000af0]:addi t4, t4, 940
[0x80000af4]:lui s10, 462235
[0x80000af8]:addi s10, s10, 509
[0x80000afc]:lui s11, 259432
[0x80000b00]:addi s11, s11, 3569
[0x80000b04]:addi a4, zero, 34
[0x80000b08]:csrrw zero, fcsr, a4
[0x80000b0c]:fmul.d t5, t3, s10, dyn

[0x80000b0c]:fmul.d t5, t3, s10, dyn
[0x80000b10]:csrrs a7, fcsr, zero
[0x80000b14]:sw t5, 640(ra)
[0x80000b18]:sw t6, 648(ra)
[0x80000b1c]:sw t5, 656(ra)
[0x80000b20]:sw a7, 664(ra)
[0x80000b24]:lw t3, 336(a6)
[0x80000b28]:lw t4, 340(a6)
[0x80000b2c]:lw s10, 344(a6)
[0x80000b30]:lw s11, 348(a6)
[0x80000b34]:lui t3, 201844
[0x80000b38]:addi t3, t3, 242
[0x80000b3c]:lui t4, 236327
[0x80000b40]:addi t4, t4, 940
[0x80000b44]:lui s10, 462235
[0x80000b48]:addi s10, s10, 509
[0x80000b4c]:lui s11, 259432
[0x80000b50]:addi s11, s11, 3569
[0x80000b54]:addi a4, zero, 66
[0x80000b58]:csrrw zero, fcsr, a4
[0x80000b5c]:fmul.d t5, t3, s10, dyn

[0x80000b5c]:fmul.d t5, t3, s10, dyn
[0x80000b60]:csrrs a7, fcsr, zero
[0x80000b64]:sw t5, 672(ra)
[0x80000b68]:sw t6, 680(ra)
[0x80000b6c]:sw t5, 688(ra)
[0x80000b70]:sw a7, 696(ra)
[0x80000b74]:lw t3, 352(a6)
[0x80000b78]:lw t4, 356(a6)
[0x80000b7c]:lw s10, 360(a6)
[0x80000b80]:lw s11, 364(a6)
[0x80000b84]:lui t3, 201844
[0x80000b88]:addi t3, t3, 242
[0x80000b8c]:lui t4, 236327
[0x80000b90]:addi t4, t4, 940
[0x80000b94]:lui s10, 462235
[0x80000b98]:addi s10, s10, 509
[0x80000b9c]:lui s11, 259432
[0x80000ba0]:addi s11, s11, 3569
[0x80000ba4]:addi a4, zero, 98
[0x80000ba8]:csrrw zero, fcsr, a4
[0x80000bac]:fmul.d t5, t3, s10, dyn

[0x80000bac]:fmul.d t5, t3, s10, dyn
[0x80000bb0]:csrrs a7, fcsr, zero
[0x80000bb4]:sw t5, 704(ra)
[0x80000bb8]:sw t6, 712(ra)
[0x80000bbc]:sw t5, 720(ra)
[0x80000bc0]:sw a7, 728(ra)
[0x80000bc4]:lw t3, 368(a6)
[0x80000bc8]:lw t4, 372(a6)
[0x80000bcc]:lw s10, 376(a6)
[0x80000bd0]:lw s11, 380(a6)
[0x80000bd4]:lui t3, 201844
[0x80000bd8]:addi t3, t3, 242
[0x80000bdc]:lui t4, 236327
[0x80000be0]:addi t4, t4, 940
[0x80000be4]:lui s10, 462235
[0x80000be8]:addi s10, s10, 509
[0x80000bec]:lui s11, 259432
[0x80000bf0]:addi s11, s11, 3569
[0x80000bf4]:addi a4, zero, 130
[0x80000bf8]:csrrw zero, fcsr, a4
[0x80000bfc]:fmul.d t5, t3, s10, dyn

[0x80000bfc]:fmul.d t5, t3, s10, dyn
[0x80000c00]:csrrs a7, fcsr, zero
[0x80000c04]:sw t5, 736(ra)
[0x80000c08]:sw t6, 744(ra)
[0x80000c0c]:sw t5, 752(ra)
[0x80000c10]:sw a7, 760(ra)
[0x80000c14]:lw t3, 384(a6)
[0x80000c18]:lw t4, 388(a6)
[0x80000c1c]:lw s10, 392(a6)
[0x80000c20]:lw s11, 396(a6)
[0x80000c24]:lui t3, 453776
[0x80000c28]:addi t3, t3, 2483
[0x80000c2c]:lui t4, 236295
[0x80000c30]:addi t4, t4, 3811
[0x80000c34]:lui s10, 681767
[0x80000c38]:addi s10, s10, 3930
[0x80000c3c]:lui s11, 269555
[0x80000c40]:addi s11, s11, 2065
[0x80000c44]:addi a4, zero, 2
[0x80000c48]:csrrw zero, fcsr, a4
[0x80000c4c]:fmul.d t5, t3, s10, dyn

[0x80000c4c]:fmul.d t5, t3, s10, dyn
[0x80000c50]:csrrs a7, fcsr, zero
[0x80000c54]:sw t5, 768(ra)
[0x80000c58]:sw t6, 776(ra)
[0x80000c5c]:sw t5, 784(ra)
[0x80000c60]:sw a7, 792(ra)
[0x80000c64]:lw t3, 400(a6)
[0x80000c68]:lw t4, 404(a6)
[0x80000c6c]:lw s10, 408(a6)
[0x80000c70]:lw s11, 412(a6)
[0x80000c74]:lui t3, 453776
[0x80000c78]:addi t3, t3, 2483
[0x80000c7c]:lui t4, 236295
[0x80000c80]:addi t4, t4, 3811
[0x80000c84]:lui s10, 681767
[0x80000c88]:addi s10, s10, 3930
[0x80000c8c]:lui s11, 269555
[0x80000c90]:addi s11, s11, 2065
[0x80000c94]:addi a4, zero, 34
[0x80000c98]:csrrw zero, fcsr, a4
[0x80000c9c]:fmul.d t5, t3, s10, dyn

[0x80000c9c]:fmul.d t5, t3, s10, dyn
[0x80000ca0]:csrrs a7, fcsr, zero
[0x80000ca4]:sw t5, 800(ra)
[0x80000ca8]:sw t6, 808(ra)
[0x80000cac]:sw t5, 816(ra)
[0x80000cb0]:sw a7, 824(ra)
[0x80000cb4]:lw t3, 416(a6)
[0x80000cb8]:lw t4, 420(a6)
[0x80000cbc]:lw s10, 424(a6)
[0x80000cc0]:lw s11, 428(a6)
[0x80000cc4]:lui t3, 453776
[0x80000cc8]:addi t3, t3, 2483
[0x80000ccc]:lui t4, 236295
[0x80000cd0]:addi t4, t4, 3811
[0x80000cd4]:lui s10, 681767
[0x80000cd8]:addi s10, s10, 3930
[0x80000cdc]:lui s11, 269555
[0x80000ce0]:addi s11, s11, 2065
[0x80000ce4]:addi a4, zero, 66
[0x80000ce8]:csrrw zero, fcsr, a4
[0x80000cec]:fmul.d t5, t3, s10, dyn

[0x80000cec]:fmul.d t5, t3, s10, dyn
[0x80000cf0]:csrrs a7, fcsr, zero
[0x80000cf4]:sw t5, 832(ra)
[0x80000cf8]:sw t6, 840(ra)
[0x80000cfc]:sw t5, 848(ra)
[0x80000d00]:sw a7, 856(ra)
[0x80000d04]:lw t3, 432(a6)
[0x80000d08]:lw t4, 436(a6)
[0x80000d0c]:lw s10, 440(a6)
[0x80000d10]:lw s11, 444(a6)
[0x80000d14]:lui t3, 453776
[0x80000d18]:addi t3, t3, 2483
[0x80000d1c]:lui t4, 236295
[0x80000d20]:addi t4, t4, 3811
[0x80000d24]:lui s10, 681767
[0x80000d28]:addi s10, s10, 3930
[0x80000d2c]:lui s11, 269555
[0x80000d30]:addi s11, s11, 2065
[0x80000d34]:addi a4, zero, 98
[0x80000d38]:csrrw zero, fcsr, a4
[0x80000d3c]:fmul.d t5, t3, s10, dyn

[0x80000d3c]:fmul.d t5, t3, s10, dyn
[0x80000d40]:csrrs a7, fcsr, zero
[0x80000d44]:sw t5, 864(ra)
[0x80000d48]:sw t6, 872(ra)
[0x80000d4c]:sw t5, 880(ra)
[0x80000d50]:sw a7, 888(ra)
[0x80000d54]:lw t3, 448(a6)
[0x80000d58]:lw t4, 452(a6)
[0x80000d5c]:lw s10, 456(a6)
[0x80000d60]:lw s11, 460(a6)
[0x80000d64]:lui t3, 453776
[0x80000d68]:addi t3, t3, 2483
[0x80000d6c]:lui t4, 236295
[0x80000d70]:addi t4, t4, 3811
[0x80000d74]:lui s10, 681767
[0x80000d78]:addi s10, s10, 3930
[0x80000d7c]:lui s11, 269555
[0x80000d80]:addi s11, s11, 2065
[0x80000d84]:addi a4, zero, 130
[0x80000d88]:csrrw zero, fcsr, a4
[0x80000d8c]:fmul.d t5, t3, s10, dyn

[0x80000d8c]:fmul.d t5, t3, s10, dyn
[0x80000d90]:csrrs a7, fcsr, zero
[0x80000d94]:sw t5, 896(ra)
[0x80000d98]:sw t6, 904(ra)
[0x80000d9c]:sw t5, 912(ra)
[0x80000da0]:sw a7, 920(ra)
[0x80000da4]:lw t3, 464(a6)
[0x80000da8]:lw t4, 468(a6)
[0x80000dac]:lw s10, 472(a6)
[0x80000db0]:lw s11, 476(a6)
[0x80000db4]:lui t3, 343782
[0x80000db8]:addi t3, t3, 2101
[0x80000dbc]:lui t4, 235923
[0x80000dc0]:addi t4, t4, 2435
[0x80000dc4]:lui s10, 502417
[0x80000dc8]:addi s10, s10, 371
[0x80000dcc]:lui s11, 266310
[0x80000dd0]:addi s11, s11, 2327
[0x80000dd4]:addi a4, zero, 2
[0x80000dd8]:csrrw zero, fcsr, a4
[0x80000ddc]:fmul.d t5, t3, s10, dyn

[0x80000ddc]:fmul.d t5, t3, s10, dyn
[0x80000de0]:csrrs a7, fcsr, zero
[0x80000de4]:sw t5, 928(ra)
[0x80000de8]:sw t6, 936(ra)
[0x80000dec]:sw t5, 944(ra)
[0x80000df0]:sw a7, 952(ra)
[0x80000df4]:lw t3, 480(a6)
[0x80000df8]:lw t4, 484(a6)
[0x80000dfc]:lw s10, 488(a6)
[0x80000e00]:lw s11, 492(a6)
[0x80000e04]:lui t3, 343782
[0x80000e08]:addi t3, t3, 2101
[0x80000e0c]:lui t4, 235923
[0x80000e10]:addi t4, t4, 2435
[0x80000e14]:lui s10, 502417
[0x80000e18]:addi s10, s10, 371
[0x80000e1c]:lui s11, 266310
[0x80000e20]:addi s11, s11, 2327
[0x80000e24]:addi a4, zero, 34
[0x80000e28]:csrrw zero, fcsr, a4
[0x80000e2c]:fmul.d t5, t3, s10, dyn

[0x80000e2c]:fmul.d t5, t3, s10, dyn
[0x80000e30]:csrrs a7, fcsr, zero
[0x80000e34]:sw t5, 960(ra)
[0x80000e38]:sw t6, 968(ra)
[0x80000e3c]:sw t5, 976(ra)
[0x80000e40]:sw a7, 984(ra)
[0x80000e44]:lw t3, 496(a6)
[0x80000e48]:lw t4, 500(a6)
[0x80000e4c]:lw s10, 504(a6)
[0x80000e50]:lw s11, 508(a6)
[0x80000e54]:lui t3, 343782
[0x80000e58]:addi t3, t3, 2101
[0x80000e5c]:lui t4, 235923
[0x80000e60]:addi t4, t4, 2435
[0x80000e64]:lui s10, 502417
[0x80000e68]:addi s10, s10, 371
[0x80000e6c]:lui s11, 266310
[0x80000e70]:addi s11, s11, 2327
[0x80000e74]:addi a4, zero, 66
[0x80000e78]:csrrw zero, fcsr, a4
[0x80000e7c]:fmul.d t5, t3, s10, dyn

[0x80000e7c]:fmul.d t5, t3, s10, dyn
[0x80000e80]:csrrs a7, fcsr, zero
[0x80000e84]:sw t5, 992(ra)
[0x80000e88]:sw t6, 1000(ra)
[0x80000e8c]:sw t5, 1008(ra)
[0x80000e90]:sw a7, 1016(ra)
[0x80000e94]:lw t3, 512(a6)
[0x80000e98]:lw t4, 516(a6)
[0x80000e9c]:lw s10, 520(a6)
[0x80000ea0]:lw s11, 524(a6)
[0x80000ea4]:lui t3, 343782
[0x80000ea8]:addi t3, t3, 2101
[0x80000eac]:lui t4, 235923
[0x80000eb0]:addi t4, t4, 2435
[0x80000eb4]:lui s10, 502417
[0x80000eb8]:addi s10, s10, 371
[0x80000ebc]:lui s11, 266310
[0x80000ec0]:addi s11, s11, 2327
[0x80000ec4]:addi a4, zero, 98
[0x80000ec8]:csrrw zero, fcsr, a4
[0x80000ecc]:fmul.d t5, t3, s10, dyn

[0x80000ecc]:fmul.d t5, t3, s10, dyn
[0x80000ed0]:csrrs a7, fcsr, zero
[0x80000ed4]:sw t5, 1024(ra)
[0x80000ed8]:sw t6, 1032(ra)
[0x80000edc]:sw t5, 1040(ra)
[0x80000ee0]:sw a7, 1048(ra)
[0x80000ee4]:lw t3, 528(a6)
[0x80000ee8]:lw t4, 532(a6)
[0x80000eec]:lw s10, 536(a6)
[0x80000ef0]:lw s11, 540(a6)
[0x80000ef4]:lui t3, 343782
[0x80000ef8]:addi t3, t3, 2101
[0x80000efc]:lui t4, 235923
[0x80000f00]:addi t4, t4, 2435
[0x80000f04]:lui s10, 502417
[0x80000f08]:addi s10, s10, 371
[0x80000f0c]:lui s11, 266310
[0x80000f10]:addi s11, s11, 2327
[0x80000f14]:addi a4, zero, 130
[0x80000f18]:csrrw zero, fcsr, a4
[0x80000f1c]:fmul.d t5, t3, s10, dyn

[0x80000f1c]:fmul.d t5, t3, s10, dyn
[0x80000f20]:csrrs a7, fcsr, zero
[0x80000f24]:sw t5, 1056(ra)
[0x80000f28]:sw t6, 1064(ra)
[0x80000f2c]:sw t5, 1072(ra)
[0x80000f30]:sw a7, 1080(ra)
[0x80000f34]:lw t3, 544(a6)
[0x80000f38]:lw t4, 548(a6)
[0x80000f3c]:lw s10, 552(a6)
[0x80000f40]:lw s11, 556(a6)
[0x80000f44]:lui t3, 16830
[0x80000f48]:addi t3, t3, 440
[0x80000f4c]:lui t4, 236324
[0x80000f50]:addi t4, t4, 2825
[0x80000f54]:lui s10, 205257
[0x80000f58]:addi s10, s10, 2457
[0x80000f5c]:lui s11, 263818
[0x80000f60]:addi s11, s11, 204
[0x80000f64]:addi a4, zero, 2
[0x80000f68]:csrrw zero, fcsr, a4
[0x80000f6c]:fmul.d t5, t3, s10, dyn

[0x80000f6c]:fmul.d t5, t3, s10, dyn
[0x80000f70]:csrrs a7, fcsr, zero
[0x80000f74]:sw t5, 1088(ra)
[0x80000f78]:sw t6, 1096(ra)
[0x80000f7c]:sw t5, 1104(ra)
[0x80000f80]:sw a7, 1112(ra)
[0x80000f84]:lw t3, 560(a6)
[0x80000f88]:lw t4, 564(a6)
[0x80000f8c]:lw s10, 568(a6)
[0x80000f90]:lw s11, 572(a6)
[0x80000f94]:lui t3, 16830
[0x80000f98]:addi t3, t3, 440
[0x80000f9c]:lui t4, 236324
[0x80000fa0]:addi t4, t4, 2825
[0x80000fa4]:lui s10, 205257
[0x80000fa8]:addi s10, s10, 2457
[0x80000fac]:lui s11, 263818
[0x80000fb0]:addi s11, s11, 204
[0x80000fb4]:addi a4, zero, 34
[0x80000fb8]:csrrw zero, fcsr, a4
[0x80000fbc]:fmul.d t5, t3, s10, dyn

[0x80000fbc]:fmul.d t5, t3, s10, dyn
[0x80000fc0]:csrrs a7, fcsr, zero
[0x80000fc4]:sw t5, 1120(ra)
[0x80000fc8]:sw t6, 1128(ra)
[0x80000fcc]:sw t5, 1136(ra)
[0x80000fd0]:sw a7, 1144(ra)
[0x80000fd4]:lw t3, 576(a6)
[0x80000fd8]:lw t4, 580(a6)
[0x80000fdc]:lw s10, 584(a6)
[0x80000fe0]:lw s11, 588(a6)
[0x80000fe4]:lui t3, 16830
[0x80000fe8]:addi t3, t3, 440
[0x80000fec]:lui t4, 236324
[0x80000ff0]:addi t4, t4, 2825
[0x80000ff4]:lui s10, 205257
[0x80000ff8]:addi s10, s10, 2457
[0x80000ffc]:lui s11, 263818
[0x80001000]:addi s11, s11, 204
[0x80001004]:addi a4, zero, 66
[0x80001008]:csrrw zero, fcsr, a4
[0x8000100c]:fmul.d t5, t3, s10, dyn

[0x8000100c]:fmul.d t5, t3, s10, dyn
[0x80001010]:csrrs a7, fcsr, zero
[0x80001014]:sw t5, 1152(ra)
[0x80001018]:sw t6, 1160(ra)
[0x8000101c]:sw t5, 1168(ra)
[0x80001020]:sw a7, 1176(ra)
[0x80001024]:lw t3, 592(a6)
[0x80001028]:lw t4, 596(a6)
[0x8000102c]:lw s10, 600(a6)
[0x80001030]:lw s11, 604(a6)
[0x80001034]:lui t3, 16830
[0x80001038]:addi t3, t3, 440
[0x8000103c]:lui t4, 236324
[0x80001040]:addi t4, t4, 2825
[0x80001044]:lui s10, 205257
[0x80001048]:addi s10, s10, 2457
[0x8000104c]:lui s11, 263818
[0x80001050]:addi s11, s11, 204
[0x80001054]:addi a4, zero, 98
[0x80001058]:csrrw zero, fcsr, a4
[0x8000105c]:fmul.d t5, t3, s10, dyn

[0x8000105c]:fmul.d t5, t3, s10, dyn
[0x80001060]:csrrs a7, fcsr, zero
[0x80001064]:sw t5, 1184(ra)
[0x80001068]:sw t6, 1192(ra)
[0x8000106c]:sw t5, 1200(ra)
[0x80001070]:sw a7, 1208(ra)
[0x80001074]:lw t3, 608(a6)
[0x80001078]:lw t4, 612(a6)
[0x8000107c]:lw s10, 616(a6)
[0x80001080]:lw s11, 620(a6)
[0x80001084]:lui t3, 16830
[0x80001088]:addi t3, t3, 440
[0x8000108c]:lui t4, 236324
[0x80001090]:addi t4, t4, 2825
[0x80001094]:lui s10, 205257
[0x80001098]:addi s10, s10, 2457
[0x8000109c]:lui s11, 263818
[0x800010a0]:addi s11, s11, 204
[0x800010a4]:addi a4, zero, 130
[0x800010a8]:csrrw zero, fcsr, a4
[0x800010ac]:fmul.d t5, t3, s10, dyn

[0x800010ac]:fmul.d t5, t3, s10, dyn
[0x800010b0]:csrrs a7, fcsr, zero
[0x800010b4]:sw t5, 1216(ra)
[0x800010b8]:sw t6, 1224(ra)
[0x800010bc]:sw t5, 1232(ra)
[0x800010c0]:sw a7, 1240(ra)
[0x800010c4]:lw t3, 624(a6)
[0x800010c8]:lw t4, 628(a6)
[0x800010cc]:lw s10, 632(a6)
[0x800010d0]:lw s11, 636(a6)
[0x800010d4]:lui t3, 661412
[0x800010d8]:addi t3, t3, 2718
[0x800010dc]:lui t4, 236082
[0x800010e0]:addi t4, t4, 1794
[0x800010e4]:lui s10, 593463
[0x800010e8]:addi s10, s10, 2373
[0x800010ec]:lui s11, 262572
[0x800010f0]:addi s11, s11, 2979
[0x800010f4]:addi a4, zero, 2
[0x800010f8]:csrrw zero, fcsr, a4
[0x800010fc]:fmul.d t5, t3, s10, dyn

[0x800010fc]:fmul.d t5, t3, s10, dyn
[0x80001100]:csrrs a7, fcsr, zero
[0x80001104]:sw t5, 1248(ra)
[0x80001108]:sw t6, 1256(ra)
[0x8000110c]:sw t5, 1264(ra)
[0x80001110]:sw a7, 1272(ra)
[0x80001114]:lw t3, 640(a6)
[0x80001118]:lw t4, 644(a6)
[0x8000111c]:lw s10, 648(a6)
[0x80001120]:lw s11, 652(a6)
[0x80001124]:lui t3, 661412
[0x80001128]:addi t3, t3, 2718
[0x8000112c]:lui t4, 236082
[0x80001130]:addi t4, t4, 1794
[0x80001134]:lui s10, 593463
[0x80001138]:addi s10, s10, 2373
[0x8000113c]:lui s11, 262572
[0x80001140]:addi s11, s11, 2979
[0x80001144]:addi a4, zero, 34
[0x80001148]:csrrw zero, fcsr, a4
[0x8000114c]:fmul.d t5, t3, s10, dyn

[0x8000114c]:fmul.d t5, t3, s10, dyn
[0x80001150]:csrrs a7, fcsr, zero
[0x80001154]:sw t5, 1280(ra)
[0x80001158]:sw t6, 1288(ra)
[0x8000115c]:sw t5, 1296(ra)
[0x80001160]:sw a7, 1304(ra)
[0x80001164]:lw t3, 656(a6)
[0x80001168]:lw t4, 660(a6)
[0x8000116c]:lw s10, 664(a6)
[0x80001170]:lw s11, 668(a6)
[0x80001174]:lui t3, 661412
[0x80001178]:addi t3, t3, 2718
[0x8000117c]:lui t4, 236082
[0x80001180]:addi t4, t4, 1794
[0x80001184]:lui s10, 593463
[0x80001188]:addi s10, s10, 2373
[0x8000118c]:lui s11, 262572
[0x80001190]:addi s11, s11, 2979
[0x80001194]:addi a4, zero, 66
[0x80001198]:csrrw zero, fcsr, a4
[0x8000119c]:fmul.d t5, t3, s10, dyn

[0x8000119c]:fmul.d t5, t3, s10, dyn
[0x800011a0]:csrrs a7, fcsr, zero
[0x800011a4]:sw t5, 1312(ra)
[0x800011a8]:sw t6, 1320(ra)
[0x800011ac]:sw t5, 1328(ra)
[0x800011b0]:sw a7, 1336(ra)
[0x800011b4]:lw t3, 672(a6)
[0x800011b8]:lw t4, 676(a6)
[0x800011bc]:lw s10, 680(a6)
[0x800011c0]:lw s11, 684(a6)
[0x800011c4]:lui t3, 661412
[0x800011c8]:addi t3, t3, 2718
[0x800011cc]:lui t4, 236082
[0x800011d0]:addi t4, t4, 1794
[0x800011d4]:lui s10, 593463
[0x800011d8]:addi s10, s10, 2373
[0x800011dc]:lui s11, 262572
[0x800011e0]:addi s11, s11, 2979
[0x800011e4]:addi a4, zero, 98
[0x800011e8]:csrrw zero, fcsr, a4
[0x800011ec]:fmul.d t5, t3, s10, dyn

[0x800011ec]:fmul.d t5, t3, s10, dyn
[0x800011f0]:csrrs a7, fcsr, zero
[0x800011f4]:sw t5, 1344(ra)
[0x800011f8]:sw t6, 1352(ra)
[0x800011fc]:sw t5, 1360(ra)
[0x80001200]:sw a7, 1368(ra)
[0x80001204]:lw t3, 688(a6)
[0x80001208]:lw t4, 692(a6)
[0x8000120c]:lw s10, 696(a6)
[0x80001210]:lw s11, 700(a6)
[0x80001214]:lui t3, 661412
[0x80001218]:addi t3, t3, 2718
[0x8000121c]:lui t4, 236082
[0x80001220]:addi t4, t4, 1794
[0x80001224]:lui s10, 593463
[0x80001228]:addi s10, s10, 2373
[0x8000122c]:lui s11, 262572
[0x80001230]:addi s11, s11, 2979
[0x80001234]:addi a4, zero, 130
[0x80001238]:csrrw zero, fcsr, a4
[0x8000123c]:fmul.d t5, t3, s10, dyn

[0x8000123c]:fmul.d t5, t3, s10, dyn
[0x80001240]:csrrs a7, fcsr, zero
[0x80001244]:sw t5, 1376(ra)
[0x80001248]:sw t6, 1384(ra)
[0x8000124c]:sw t5, 1392(ra)
[0x80001250]:sw a7, 1400(ra)
[0x80001254]:lw t3, 704(a6)
[0x80001258]:lw t4, 708(a6)
[0x8000125c]:lw s10, 712(a6)
[0x80001260]:lw s11, 716(a6)
[0x80001264]:lui t3, 752178
[0x80001268]:addi t3, t3, 612
[0x8000126c]:lui t4, 236058
[0x80001270]:addi t4, t4, 3250
[0x80001274]:lui s10, 556412
[0x80001278]:addi s10, s10, 3165
[0x8000127c]:lui s11, 261447
[0x80001280]:addi s11, s11, 1267
[0x80001284]:addi a4, zero, 2
[0x80001288]:csrrw zero, fcsr, a4
[0x8000128c]:fmul.d t5, t3, s10, dyn

[0x8000128c]:fmul.d t5, t3, s10, dyn
[0x80001290]:csrrs a7, fcsr, zero
[0x80001294]:sw t5, 1408(ra)
[0x80001298]:sw t6, 1416(ra)
[0x8000129c]:sw t5, 1424(ra)
[0x800012a0]:sw a7, 1432(ra)
[0x800012a4]:lw t3, 720(a6)
[0x800012a8]:lw t4, 724(a6)
[0x800012ac]:lw s10, 728(a6)
[0x800012b0]:lw s11, 732(a6)
[0x800012b4]:lui t3, 752178
[0x800012b8]:addi t3, t3, 612
[0x800012bc]:lui t4, 236058
[0x800012c0]:addi t4, t4, 3250
[0x800012c4]:lui s10, 556412
[0x800012c8]:addi s10, s10, 3165
[0x800012cc]:lui s11, 261447
[0x800012d0]:addi s11, s11, 1267
[0x800012d4]:addi a4, zero, 34
[0x800012d8]:csrrw zero, fcsr, a4
[0x800012dc]:fmul.d t5, t3, s10, dyn

[0x800012dc]:fmul.d t5, t3, s10, dyn
[0x800012e0]:csrrs a7, fcsr, zero
[0x800012e4]:sw t5, 1440(ra)
[0x800012e8]:sw t6, 1448(ra)
[0x800012ec]:sw t5, 1456(ra)
[0x800012f0]:sw a7, 1464(ra)
[0x800012f4]:lw t3, 736(a6)
[0x800012f8]:lw t4, 740(a6)
[0x800012fc]:lw s10, 744(a6)
[0x80001300]:lw s11, 748(a6)
[0x80001304]:lui t3, 752178
[0x80001308]:addi t3, t3, 612
[0x8000130c]:lui t4, 236058
[0x80001310]:addi t4, t4, 3250
[0x80001314]:lui s10, 556412
[0x80001318]:addi s10, s10, 3165
[0x8000131c]:lui s11, 261447
[0x80001320]:addi s11, s11, 1267
[0x80001324]:addi a4, zero, 66
[0x80001328]:csrrw zero, fcsr, a4
[0x8000132c]:fmul.d t5, t3, s10, dyn

[0x8000132c]:fmul.d t5, t3, s10, dyn
[0x80001330]:csrrs a7, fcsr, zero
[0x80001334]:sw t5, 1472(ra)
[0x80001338]:sw t6, 1480(ra)
[0x8000133c]:sw t5, 1488(ra)
[0x80001340]:sw a7, 1496(ra)
[0x80001344]:lw t3, 752(a6)
[0x80001348]:lw t4, 756(a6)
[0x8000134c]:lw s10, 760(a6)
[0x80001350]:lw s11, 764(a6)
[0x80001354]:lui t3, 752178
[0x80001358]:addi t3, t3, 612
[0x8000135c]:lui t4, 236058
[0x80001360]:addi t4, t4, 3250
[0x80001364]:lui s10, 556412
[0x80001368]:addi s10, s10, 3165
[0x8000136c]:lui s11, 261447
[0x80001370]:addi s11, s11, 1267
[0x80001374]:addi a4, zero, 98
[0x80001378]:csrrw zero, fcsr, a4
[0x8000137c]:fmul.d t5, t3, s10, dyn

[0x8000137c]:fmul.d t5, t3, s10, dyn
[0x80001380]:csrrs a7, fcsr, zero
[0x80001384]:sw t5, 1504(ra)
[0x80001388]:sw t6, 1512(ra)
[0x8000138c]:sw t5, 1520(ra)
[0x80001390]:sw a7, 1528(ra)
[0x80001394]:lw t3, 768(a6)
[0x80001398]:lw t4, 772(a6)
[0x8000139c]:lw s10, 776(a6)
[0x800013a0]:lw s11, 780(a6)
[0x800013a4]:lui t3, 752178
[0x800013a8]:addi t3, t3, 612
[0x800013ac]:lui t4, 236058
[0x800013b0]:addi t4, t4, 3250
[0x800013b4]:lui s10, 556412
[0x800013b8]:addi s10, s10, 3165
[0x800013bc]:lui s11, 261447
[0x800013c0]:addi s11, s11, 1267
[0x800013c4]:addi a4, zero, 130
[0x800013c8]:csrrw zero, fcsr, a4
[0x800013cc]:fmul.d t5, t3, s10, dyn

[0x800013cc]:fmul.d t5, t3, s10, dyn
[0x800013d0]:csrrs a7, fcsr, zero
[0x800013d4]:sw t5, 1536(ra)
[0x800013d8]:sw t6, 1544(ra)
[0x800013dc]:sw t5, 1552(ra)
[0x800013e0]:sw a7, 1560(ra)
[0x800013e4]:lw t3, 784(a6)
[0x800013e8]:lw t4, 788(a6)
[0x800013ec]:lw s10, 792(a6)
[0x800013f0]:lw s11, 796(a6)
[0x800013f4]:lui t3, 893832
[0x800013f8]:addi t3, t3, 1139
[0x800013fc]:lui t4, 236328
[0x80001400]:addi t4, t4, 1183
[0x80001404]:lui s10, 491437
[0x80001408]:addi s10, s10, 3672
[0x8000140c]:lui s11, 260228
[0x80001410]:addi s11, s11, 3821
[0x80001414]:addi a4, zero, 2
[0x80001418]:csrrw zero, fcsr, a4
[0x8000141c]:fmul.d t5, t3, s10, dyn

[0x8000141c]:fmul.d t5, t3, s10, dyn
[0x80001420]:csrrs a7, fcsr, zero
[0x80001424]:sw t5, 1568(ra)
[0x80001428]:sw t6, 1576(ra)
[0x8000142c]:sw t5, 1584(ra)
[0x80001430]:sw a7, 1592(ra)
[0x80001434]:lw t3, 800(a6)
[0x80001438]:lw t4, 804(a6)
[0x8000143c]:lw s10, 808(a6)
[0x80001440]:lw s11, 812(a6)
[0x80001444]:lui t3, 893832
[0x80001448]:addi t3, t3, 1139
[0x8000144c]:lui t4, 236328
[0x80001450]:addi t4, t4, 1183
[0x80001454]:lui s10, 491437
[0x80001458]:addi s10, s10, 3672
[0x8000145c]:lui s11, 260228
[0x80001460]:addi s11, s11, 3821
[0x80001464]:addi a4, zero, 34
[0x80001468]:csrrw zero, fcsr, a4
[0x8000146c]:fmul.d t5, t3, s10, dyn

[0x8000146c]:fmul.d t5, t3, s10, dyn
[0x80001470]:csrrs a7, fcsr, zero
[0x80001474]:sw t5, 1600(ra)
[0x80001478]:sw t6, 1608(ra)
[0x8000147c]:sw t5, 1616(ra)
[0x80001480]:sw a7, 1624(ra)
[0x80001484]:lw t3, 816(a6)
[0x80001488]:lw t4, 820(a6)
[0x8000148c]:lw s10, 824(a6)
[0x80001490]:lw s11, 828(a6)
[0x80001494]:lui t3, 893832
[0x80001498]:addi t3, t3, 1139
[0x8000149c]:lui t4, 236328
[0x800014a0]:addi t4, t4, 1183
[0x800014a4]:lui s10, 491437
[0x800014a8]:addi s10, s10, 3672
[0x800014ac]:lui s11, 260228
[0x800014b0]:addi s11, s11, 3821
[0x800014b4]:addi a4, zero, 66
[0x800014b8]:csrrw zero, fcsr, a4
[0x800014bc]:fmul.d t5, t3, s10, dyn

[0x800014bc]:fmul.d t5, t3, s10, dyn
[0x800014c0]:csrrs a7, fcsr, zero
[0x800014c4]:sw t5, 1632(ra)
[0x800014c8]:sw t6, 1640(ra)
[0x800014cc]:sw t5, 1648(ra)
[0x800014d0]:sw a7, 1656(ra)
[0x800014d4]:lw t3, 832(a6)
[0x800014d8]:lw t4, 836(a6)
[0x800014dc]:lw s10, 840(a6)
[0x800014e0]:lw s11, 844(a6)
[0x800014e4]:lui t3, 893832
[0x800014e8]:addi t3, t3, 1139
[0x800014ec]:lui t4, 236328
[0x800014f0]:addi t4, t4, 1183
[0x800014f4]:lui s10, 491437
[0x800014f8]:addi s10, s10, 3672
[0x800014fc]:lui s11, 260228
[0x80001500]:addi s11, s11, 3821
[0x80001504]:addi a4, zero, 98
[0x80001508]:csrrw zero, fcsr, a4
[0x8000150c]:fmul.d t5, t3, s10, dyn

[0x8000150c]:fmul.d t5, t3, s10, dyn
[0x80001510]:csrrs a7, fcsr, zero
[0x80001514]:sw t5, 1664(ra)
[0x80001518]:sw t6, 1672(ra)
[0x8000151c]:sw t5, 1680(ra)
[0x80001520]:sw a7, 1688(ra)
[0x80001524]:lw t3, 848(a6)
[0x80001528]:lw t4, 852(a6)
[0x8000152c]:lw s10, 856(a6)
[0x80001530]:lw s11, 860(a6)
[0x80001534]:lui t3, 893832
[0x80001538]:addi t3, t3, 1139
[0x8000153c]:lui t4, 236328
[0x80001540]:addi t4, t4, 1183
[0x80001544]:lui s10, 491437
[0x80001548]:addi s10, s10, 3672
[0x8000154c]:lui s11, 260228
[0x80001550]:addi s11, s11, 3821
[0x80001554]:addi a4, zero, 130
[0x80001558]:csrrw zero, fcsr, a4
[0x8000155c]:fmul.d t5, t3, s10, dyn

[0x8000155c]:fmul.d t5, t3, s10, dyn
[0x80001560]:csrrs a7, fcsr, zero
[0x80001564]:sw t5, 1696(ra)
[0x80001568]:sw t6, 1704(ra)
[0x8000156c]:sw t5, 1712(ra)
[0x80001570]:sw a7, 1720(ra)
[0x80001574]:lw t3, 864(a6)
[0x80001578]:lw t4, 868(a6)
[0x8000157c]:lw s10, 872(a6)
[0x80001580]:lw s11, 876(a6)
[0x80001584]:lui t3, 522730
[0x80001588]:addi t3, t3, 3159
[0x8000158c]:lui t4, 236086
[0x80001590]:addi t4, t4, 2445
[0x80001594]:lui s10, 837070
[0x80001598]:addi s10, s10, 1453
[0x8000159c]:lui s11, 259671
[0x800015a0]:addi s11, s11, 699
[0x800015a4]:addi a4, zero, 2
[0x800015a8]:csrrw zero, fcsr, a4
[0x800015ac]:fmul.d t5, t3, s10, dyn

[0x800015ac]:fmul.d t5, t3, s10, dyn
[0x800015b0]:csrrs a7, fcsr, zero
[0x800015b4]:sw t5, 1728(ra)
[0x800015b8]:sw t6, 1736(ra)
[0x800015bc]:sw t5, 1744(ra)
[0x800015c0]:sw a7, 1752(ra)
[0x800015c4]:lw t3, 880(a6)
[0x800015c8]:lw t4, 884(a6)
[0x800015cc]:lw s10, 888(a6)
[0x800015d0]:lw s11, 892(a6)
[0x800015d4]:lui t3, 522730
[0x800015d8]:addi t3, t3, 3159
[0x800015dc]:lui t4, 236086
[0x800015e0]:addi t4, t4, 2445
[0x800015e4]:lui s10, 837070
[0x800015e8]:addi s10, s10, 1453
[0x800015ec]:lui s11, 259671
[0x800015f0]:addi s11, s11, 699
[0x800015f4]:addi a4, zero, 34
[0x800015f8]:csrrw zero, fcsr, a4
[0x800015fc]:fmul.d t5, t3, s10, dyn

[0x800015fc]:fmul.d t5, t3, s10, dyn
[0x80001600]:csrrs a7, fcsr, zero
[0x80001604]:sw t5, 1760(ra)
[0x80001608]:sw t6, 1768(ra)
[0x8000160c]:sw t5, 1776(ra)
[0x80001610]:sw a7, 1784(ra)
[0x80001614]:lw t3, 896(a6)
[0x80001618]:lw t4, 900(a6)
[0x8000161c]:lw s10, 904(a6)
[0x80001620]:lw s11, 908(a6)
[0x80001624]:lui t3, 522730
[0x80001628]:addi t3, t3, 3159
[0x8000162c]:lui t4, 236086
[0x80001630]:addi t4, t4, 2445
[0x80001634]:lui s10, 837070
[0x80001638]:addi s10, s10, 1453
[0x8000163c]:lui s11, 259671
[0x80001640]:addi s11, s11, 699
[0x80001644]:addi a4, zero, 66
[0x80001648]:csrrw zero, fcsr, a4
[0x8000164c]:fmul.d t5, t3, s10, dyn

[0x8000164c]:fmul.d t5, t3, s10, dyn
[0x80001650]:csrrs a7, fcsr, zero
[0x80001654]:sw t5, 1792(ra)
[0x80001658]:sw t6, 1800(ra)
[0x8000165c]:sw t5, 1808(ra)
[0x80001660]:sw a7, 1816(ra)
[0x80001664]:lw t3, 912(a6)
[0x80001668]:lw t4, 916(a6)
[0x8000166c]:lw s10, 920(a6)
[0x80001670]:lw s11, 924(a6)
[0x80001674]:lui t3, 522730
[0x80001678]:addi t3, t3, 3159
[0x8000167c]:lui t4, 236086
[0x80001680]:addi t4, t4, 2445
[0x80001684]:lui s10, 837070
[0x80001688]:addi s10, s10, 1453
[0x8000168c]:lui s11, 259671
[0x80001690]:addi s11, s11, 699
[0x80001694]:addi a4, zero, 98
[0x80001698]:csrrw zero, fcsr, a4
[0x8000169c]:fmul.d t5, t3, s10, dyn

[0x8000169c]:fmul.d t5, t3, s10, dyn
[0x800016a0]:csrrs a7, fcsr, zero
[0x800016a4]:sw t5, 1824(ra)
[0x800016a8]:sw t6, 1832(ra)
[0x800016ac]:sw t5, 1840(ra)
[0x800016b0]:sw a7, 1848(ra)
[0x800016b4]:lw t3, 928(a6)
[0x800016b8]:lw t4, 932(a6)
[0x800016bc]:lw s10, 936(a6)
[0x800016c0]:lw s11, 940(a6)
[0x800016c4]:lui t3, 522730
[0x800016c8]:addi t3, t3, 3159
[0x800016cc]:lui t4, 236086
[0x800016d0]:addi t4, t4, 2445
[0x800016d4]:lui s10, 837070
[0x800016d8]:addi s10, s10, 1453
[0x800016dc]:lui s11, 259671
[0x800016e0]:addi s11, s11, 699
[0x800016e4]:addi a4, zero, 130
[0x800016e8]:csrrw zero, fcsr, a4
[0x800016ec]:fmul.d t5, t3, s10, dyn

[0x800016ec]:fmul.d t5, t3, s10, dyn
[0x800016f0]:csrrs a7, fcsr, zero
[0x800016f4]:sw t5, 1856(ra)
[0x800016f8]:sw t6, 1864(ra)
[0x800016fc]:sw t5, 1872(ra)
[0x80001700]:sw a7, 1880(ra)
[0x80001704]:lw t3, 944(a6)
[0x80001708]:lw t4, 948(a6)
[0x8000170c]:lw s10, 952(a6)
[0x80001710]:lw s11, 956(a6)
[0x80001714]:lui t3, 719993
[0x80001718]:addi t3, t3, 3725
[0x8000171c]:lui t4, 235858
[0x80001720]:addi t4, t4, 566
[0x80001724]:lui s10, 614912
[0x80001728]:addi s10, s10, 551
[0x8000172c]:lui s11, 269956
[0x80001730]:addi s11, s11, 2573
[0x80001734]:addi a4, zero, 2
[0x80001738]:csrrw zero, fcsr, a4
[0x8000173c]:fmul.d t5, t3, s10, dyn

[0x8000173c]:fmul.d t5, t3, s10, dyn
[0x80001740]:csrrs a7, fcsr, zero
[0x80001744]:sw t5, 1888(ra)
[0x80001748]:sw t6, 1896(ra)
[0x8000174c]:sw t5, 1904(ra)
[0x80001750]:sw a7, 1912(ra)
[0x80001754]:lw t3, 960(a6)
[0x80001758]:lw t4, 964(a6)
[0x8000175c]:lw s10, 968(a6)
[0x80001760]:lw s11, 972(a6)
[0x80001764]:lui t3, 719993
[0x80001768]:addi t3, t3, 3725
[0x8000176c]:lui t4, 235858
[0x80001770]:addi t4, t4, 566
[0x80001774]:lui s10, 614912
[0x80001778]:addi s10, s10, 551
[0x8000177c]:lui s11, 269956
[0x80001780]:addi s11, s11, 2573
[0x80001784]:addi a4, zero, 34
[0x80001788]:csrrw zero, fcsr, a4
[0x8000178c]:fmul.d t5, t3, s10, dyn

[0x8000178c]:fmul.d t5, t3, s10, dyn
[0x80001790]:csrrs a7, fcsr, zero
[0x80001794]:sw t5, 1920(ra)
[0x80001798]:sw t6, 1928(ra)
[0x8000179c]:sw t5, 1936(ra)
[0x800017a0]:sw a7, 1944(ra)
[0x800017a4]:lw t3, 976(a6)
[0x800017a8]:lw t4, 980(a6)
[0x800017ac]:lw s10, 984(a6)
[0x800017b0]:lw s11, 988(a6)
[0x800017b4]:lui t3, 719993
[0x800017b8]:addi t3, t3, 3725
[0x800017bc]:lui t4, 235858
[0x800017c0]:addi t4, t4, 566
[0x800017c4]:lui s10, 614912
[0x800017c8]:addi s10, s10, 551
[0x800017cc]:lui s11, 269956
[0x800017d0]:addi s11, s11, 2573
[0x800017d4]:addi a4, zero, 66
[0x800017d8]:csrrw zero, fcsr, a4
[0x800017dc]:fmul.d t5, t3, s10, dyn

[0x800017dc]:fmul.d t5, t3, s10, dyn
[0x800017e0]:csrrs a7, fcsr, zero
[0x800017e4]:sw t5, 1952(ra)
[0x800017e8]:sw t6, 1960(ra)
[0x800017ec]:sw t5, 1968(ra)
[0x800017f0]:sw a7, 1976(ra)
[0x800017f4]:lw t3, 992(a6)
[0x800017f8]:lw t4, 996(a6)
[0x800017fc]:lw s10, 1000(a6)
[0x80001800]:lw s11, 1004(a6)
[0x80001804]:lui t3, 719993
[0x80001808]:addi t3, t3, 3725
[0x8000180c]:lui t4, 235858
[0x80001810]:addi t4, t4, 566
[0x80001814]:lui s10, 614912
[0x80001818]:addi s10, s10, 551
[0x8000181c]:lui s11, 269956
[0x80001820]:addi s11, s11, 2573
[0x80001824]:addi a4, zero, 98
[0x80001828]:csrrw zero, fcsr, a4
[0x8000182c]:fmul.d t5, t3, s10, dyn

[0x8000182c]:fmul.d t5, t3, s10, dyn
[0x80001830]:csrrs a7, fcsr, zero
[0x80001834]:sw t5, 1984(ra)
[0x80001838]:sw t6, 1992(ra)
[0x8000183c]:sw t5, 2000(ra)
[0x80001840]:sw a7, 2008(ra)
[0x80001844]:lw t3, 1008(a6)
[0x80001848]:lw t4, 1012(a6)
[0x8000184c]:lw s10, 1016(a6)
[0x80001850]:lw s11, 1020(a6)
[0x80001854]:lui t3, 719993
[0x80001858]:addi t3, t3, 3725
[0x8000185c]:lui t4, 235858
[0x80001860]:addi t4, t4, 566
[0x80001864]:lui s10, 614912
[0x80001868]:addi s10, s10, 551
[0x8000186c]:lui s11, 269956
[0x80001870]:addi s11, s11, 2573
[0x80001874]:addi a4, zero, 130
[0x80001878]:csrrw zero, fcsr, a4
[0x8000187c]:fmul.d t5, t3, s10, dyn

[0x8000187c]:fmul.d t5, t3, s10, dyn
[0x80001880]:csrrs a7, fcsr, zero
[0x80001884]:sw t5, 2016(ra)
[0x80001888]:sw t6, 2024(ra)
[0x8000188c]:sw t5, 2032(ra)
[0x80001890]:sw a7, 2040(ra)
[0x80001894]:lw t3, 1024(a6)
[0x80001898]:lw t4, 1028(a6)
[0x8000189c]:lw s10, 1032(a6)
[0x800018a0]:lw s11, 1036(a6)
[0x800018a4]:lui t3, 998089
[0x800018a8]:addi t3, t3, 315
[0x800018ac]:lui t4, 236132
[0x800018b0]:addi t4, t4, 516
[0x800018b4]:lui s10, 955016
[0x800018b8]:addi s10, s10, 3149
[0x800018bc]:lui s11, 266096
[0x800018c0]:addi s11, s11, 201
[0x800018c4]:addi a4, zero, 2
[0x800018c8]:csrrw zero, fcsr, a4
[0x800018cc]:fmul.d t5, t3, s10, dyn

[0x800018cc]:fmul.d t5, t3, s10, dyn
[0x800018d0]:csrrs a7, fcsr, zero
[0x800018d4]:addi ra, ra, 2040
[0x800018d8]:sw t5, 8(ra)
[0x800018dc]:sw t6, 16(ra)
[0x800018e0]:sw t5, 24(ra)
[0x800018e4]:sw a7, 32(ra)
[0x800018e8]:lw t3, 1040(a6)
[0x800018ec]:lw t4, 1044(a6)
[0x800018f0]:lw s10, 1048(a6)
[0x800018f4]:lw s11, 1052(a6)
[0x800018f8]:lui t3, 998089
[0x800018fc]:addi t3, t3, 315
[0x80001900]:lui t4, 236132
[0x80001904]:addi t4, t4, 516
[0x80001908]:lui s10, 955016
[0x8000190c]:addi s10, s10, 3149
[0x80001910]:lui s11, 266096
[0x80001914]:addi s11, s11, 201
[0x80001918]:addi a4, zero, 34
[0x8000191c]:csrrw zero, fcsr, a4
[0x80001920]:fmul.d t5, t3, s10, dyn

[0x80001920]:fmul.d t5, t3, s10, dyn
[0x80001924]:csrrs a7, fcsr, zero
[0x80001928]:sw t5, 40(ra)
[0x8000192c]:sw t6, 48(ra)
[0x80001930]:sw t5, 56(ra)
[0x80001934]:sw a7, 64(ra)
[0x80001938]:lw t3, 1056(a6)
[0x8000193c]:lw t4, 1060(a6)
[0x80001940]:lw s10, 1064(a6)
[0x80001944]:lw s11, 1068(a6)
[0x80001948]:lui t3, 998089
[0x8000194c]:addi t3, t3, 315
[0x80001950]:lui t4, 236132
[0x80001954]:addi t4, t4, 516
[0x80001958]:lui s10, 955016
[0x8000195c]:addi s10, s10, 3149
[0x80001960]:lui s11, 266096
[0x80001964]:addi s11, s11, 201
[0x80001968]:addi a4, zero, 66
[0x8000196c]:csrrw zero, fcsr, a4
[0x80001970]:fmul.d t5, t3, s10, dyn

[0x80001970]:fmul.d t5, t3, s10, dyn
[0x80001974]:csrrs a7, fcsr, zero
[0x80001978]:sw t5, 72(ra)
[0x8000197c]:sw t6, 80(ra)
[0x80001980]:sw t5, 88(ra)
[0x80001984]:sw a7, 96(ra)
[0x80001988]:lw t3, 1072(a6)
[0x8000198c]:lw t4, 1076(a6)
[0x80001990]:lw s10, 1080(a6)
[0x80001994]:lw s11, 1084(a6)
[0x80001998]:lui t3, 998089
[0x8000199c]:addi t3, t3, 315
[0x800019a0]:lui t4, 236132
[0x800019a4]:addi t4, t4, 516
[0x800019a8]:lui s10, 955016
[0x800019ac]:addi s10, s10, 3149
[0x800019b0]:lui s11, 266096
[0x800019b4]:addi s11, s11, 201
[0x800019b8]:addi a4, zero, 98
[0x800019bc]:csrrw zero, fcsr, a4
[0x800019c0]:fmul.d t5, t3, s10, dyn

[0x800019c0]:fmul.d t5, t3, s10, dyn
[0x800019c4]:csrrs a7, fcsr, zero
[0x800019c8]:sw t5, 104(ra)
[0x800019cc]:sw t6, 112(ra)
[0x800019d0]:sw t5, 120(ra)
[0x800019d4]:sw a7, 128(ra)
[0x800019d8]:lw t3, 1088(a6)
[0x800019dc]:lw t4, 1092(a6)
[0x800019e0]:lw s10, 1096(a6)
[0x800019e4]:lw s11, 1100(a6)
[0x800019e8]:lui t3, 998089
[0x800019ec]:addi t3, t3, 315
[0x800019f0]:lui t4, 236132
[0x800019f4]:addi t4, t4, 516
[0x800019f8]:lui s10, 955016
[0x800019fc]:addi s10, s10, 3149
[0x80001a00]:lui s11, 266096
[0x80001a04]:addi s11, s11, 201
[0x80001a08]:addi a4, zero, 130
[0x80001a0c]:csrrw zero, fcsr, a4
[0x80001a10]:fmul.d t5, t3, s10, dyn

[0x80001a10]:fmul.d t5, t3, s10, dyn
[0x80001a14]:csrrs a7, fcsr, zero
[0x80001a18]:sw t5, 136(ra)
[0x80001a1c]:sw t6, 144(ra)
[0x80001a20]:sw t5, 152(ra)
[0x80001a24]:sw a7, 160(ra)
[0x80001a28]:lw t3, 1104(a6)
[0x80001a2c]:lw t4, 1108(a6)
[0x80001a30]:lw s10, 1112(a6)
[0x80001a34]:lw s11, 1116(a6)
[0x80001a38]:lui t3, 247182
[0x80001a3c]:addi t3, t3, 1520
[0x80001a40]:lui t4, 236243
[0x80001a44]:addi t4, t4, 471
[0x80001a48]:lui s10, 845975
[0x80001a4c]:addi s10, s10, 3501
[0x80001a50]:lui s11, 263916
[0x80001a54]:addi s11, s11, 525
[0x80001a58]:addi a4, zero, 2
[0x80001a5c]:csrrw zero, fcsr, a4
[0x80001a60]:fmul.d t5, t3, s10, dyn

[0x80001a60]:fmul.d t5, t3, s10, dyn
[0x80001a64]:csrrs a7, fcsr, zero
[0x80001a68]:sw t5, 168(ra)
[0x80001a6c]:sw t6, 176(ra)
[0x80001a70]:sw t5, 184(ra)
[0x80001a74]:sw a7, 192(ra)
[0x80001a78]:lw t3, 1120(a6)
[0x80001a7c]:lw t4, 1124(a6)
[0x80001a80]:lw s10, 1128(a6)
[0x80001a84]:lw s11, 1132(a6)
[0x80001a88]:lui t3, 247182
[0x80001a8c]:addi t3, t3, 1520
[0x80001a90]:lui t4, 236243
[0x80001a94]:addi t4, t4, 471
[0x80001a98]:lui s10, 845975
[0x80001a9c]:addi s10, s10, 3501
[0x80001aa0]:lui s11, 263916
[0x80001aa4]:addi s11, s11, 525
[0x80001aa8]:addi a4, zero, 34
[0x80001aac]:csrrw zero, fcsr, a4
[0x80001ab0]:fmul.d t5, t3, s10, dyn

[0x80001ab0]:fmul.d t5, t3, s10, dyn
[0x80001ab4]:csrrs a7, fcsr, zero
[0x80001ab8]:sw t5, 200(ra)
[0x80001abc]:sw t6, 208(ra)
[0x80001ac0]:sw t5, 216(ra)
[0x80001ac4]:sw a7, 224(ra)
[0x80001ac8]:lw t3, 1136(a6)
[0x80001acc]:lw t4, 1140(a6)
[0x80001ad0]:lw s10, 1144(a6)
[0x80001ad4]:lw s11, 1148(a6)
[0x80001ad8]:lui t3, 247182
[0x80001adc]:addi t3, t3, 1520
[0x80001ae0]:lui t4, 236243
[0x80001ae4]:addi t4, t4, 471
[0x80001ae8]:lui s10, 845975
[0x80001aec]:addi s10, s10, 3501
[0x80001af0]:lui s11, 263916
[0x80001af4]:addi s11, s11, 525
[0x80001af8]:addi a4, zero, 66
[0x80001afc]:csrrw zero, fcsr, a4
[0x80001b00]:fmul.d t5, t3, s10, dyn

[0x80001b00]:fmul.d t5, t3, s10, dyn
[0x80001b04]:csrrs a7, fcsr, zero
[0x80001b08]:sw t5, 232(ra)
[0x80001b0c]:sw t6, 240(ra)
[0x80001b10]:sw t5, 248(ra)
[0x80001b14]:sw a7, 256(ra)
[0x80001b18]:lw t3, 1152(a6)
[0x80001b1c]:lw t4, 1156(a6)
[0x80001b20]:lw s10, 1160(a6)
[0x80001b24]:lw s11, 1164(a6)
[0x80001b28]:lui t3, 247182
[0x80001b2c]:addi t3, t3, 1520
[0x80001b30]:lui t4, 236243
[0x80001b34]:addi t4, t4, 471
[0x80001b38]:lui s10, 845975
[0x80001b3c]:addi s10, s10, 3501
[0x80001b40]:lui s11, 263916
[0x80001b44]:addi s11, s11, 525
[0x80001b48]:addi a4, zero, 98
[0x80001b4c]:csrrw zero, fcsr, a4
[0x80001b50]:fmul.d t5, t3, s10, dyn

[0x80001b50]:fmul.d t5, t3, s10, dyn
[0x80001b54]:csrrs a7, fcsr, zero
[0x80001b58]:sw t5, 264(ra)
[0x80001b5c]:sw t6, 272(ra)
[0x80001b60]:sw t5, 280(ra)
[0x80001b64]:sw a7, 288(ra)
[0x80001b68]:lw t3, 1168(a6)
[0x80001b6c]:lw t4, 1172(a6)
[0x80001b70]:lw s10, 1176(a6)
[0x80001b74]:lw s11, 1180(a6)
[0x80001b78]:lui t3, 247182
[0x80001b7c]:addi t3, t3, 1520
[0x80001b80]:lui t4, 236243
[0x80001b84]:addi t4, t4, 471
[0x80001b88]:lui s10, 845975
[0x80001b8c]:addi s10, s10, 3501
[0x80001b90]:lui s11, 263916
[0x80001b94]:addi s11, s11, 525
[0x80001b98]:addi a4, zero, 130
[0x80001b9c]:csrrw zero, fcsr, a4
[0x80001ba0]:fmul.d t5, t3, s10, dyn

[0x80001ba0]:fmul.d t5, t3, s10, dyn
[0x80001ba4]:csrrs a7, fcsr, zero
[0x80001ba8]:sw t5, 296(ra)
[0x80001bac]:sw t6, 304(ra)
[0x80001bb0]:sw t5, 312(ra)
[0x80001bb4]:sw a7, 320(ra)
[0x80001bb8]:lw t3, 1184(a6)
[0x80001bbc]:lw t4, 1188(a6)
[0x80001bc0]:lw s10, 1192(a6)
[0x80001bc4]:lw s11, 1196(a6)
[0x80001bc8]:lui t3, 37547
[0x80001bcc]:addi t3, t3, 1679
[0x80001bd0]:lui t4, 236300
[0x80001bd4]:addi t4, t4, 2775
[0x80001bd8]:lui s10, 220819
[0x80001bdc]:addi s10, s10, 1993
[0x80001be0]:lui s11, 262378
[0x80001be4]:addi s11, s11, 2719
[0x80001be8]:addi a4, zero, 2
[0x80001bec]:csrrw zero, fcsr, a4
[0x80001bf0]:fmul.d t5, t3, s10, dyn

[0x80001bf0]:fmul.d t5, t3, s10, dyn
[0x80001bf4]:csrrs a7, fcsr, zero
[0x80001bf8]:sw t5, 328(ra)
[0x80001bfc]:sw t6, 336(ra)
[0x80001c00]:sw t5, 344(ra)
[0x80001c04]:sw a7, 352(ra)
[0x80001c08]:lw t3, 1200(a6)
[0x80001c0c]:lw t4, 1204(a6)
[0x80001c10]:lw s10, 1208(a6)
[0x80001c14]:lw s11, 1212(a6)
[0x80001c18]:lui t3, 37547
[0x80001c1c]:addi t3, t3, 1679
[0x80001c20]:lui t4, 236300
[0x80001c24]:addi t4, t4, 2775
[0x80001c28]:lui s10, 220819
[0x80001c2c]:addi s10, s10, 1993
[0x80001c30]:lui s11, 262378
[0x80001c34]:addi s11, s11, 2719
[0x80001c38]:addi a4, zero, 34
[0x80001c3c]:csrrw zero, fcsr, a4
[0x80001c40]:fmul.d t5, t3, s10, dyn

[0x80001c40]:fmul.d t5, t3, s10, dyn
[0x80001c44]:csrrs a7, fcsr, zero
[0x80001c48]:sw t5, 360(ra)
[0x80001c4c]:sw t6, 368(ra)
[0x80001c50]:sw t5, 376(ra)
[0x80001c54]:sw a7, 384(ra)
[0x80001c58]:lw t3, 1216(a6)
[0x80001c5c]:lw t4, 1220(a6)
[0x80001c60]:lw s10, 1224(a6)
[0x80001c64]:lw s11, 1228(a6)
[0x80001c68]:lui t3, 37547
[0x80001c6c]:addi t3, t3, 1679
[0x80001c70]:lui t4, 236300
[0x80001c74]:addi t4, t4, 2775
[0x80001c78]:lui s10, 220819
[0x80001c7c]:addi s10, s10, 1993
[0x80001c80]:lui s11, 262378
[0x80001c84]:addi s11, s11, 2719
[0x80001c88]:addi a4, zero, 66
[0x80001c8c]:csrrw zero, fcsr, a4
[0x80001c90]:fmul.d t5, t3, s10, dyn

[0x80001c90]:fmul.d t5, t3, s10, dyn
[0x80001c94]:csrrs a7, fcsr, zero
[0x80001c98]:sw t5, 392(ra)
[0x80001c9c]:sw t6, 400(ra)
[0x80001ca0]:sw t5, 408(ra)
[0x80001ca4]:sw a7, 416(ra)
[0x80001ca8]:lw t3, 1232(a6)
[0x80001cac]:lw t4, 1236(a6)
[0x80001cb0]:lw s10, 1240(a6)
[0x80001cb4]:lw s11, 1244(a6)
[0x80001cb8]:lui t3, 37547
[0x80001cbc]:addi t3, t3, 1679
[0x80001cc0]:lui t4, 236300
[0x80001cc4]:addi t4, t4, 2775
[0x80001cc8]:lui s10, 220819
[0x80001ccc]:addi s10, s10, 1993
[0x80001cd0]:lui s11, 262378
[0x80001cd4]:addi s11, s11, 2719
[0x80001cd8]:addi a4, zero, 98
[0x80001cdc]:csrrw zero, fcsr, a4
[0x80001ce0]:fmul.d t5, t3, s10, dyn

[0x80001ce0]:fmul.d t5, t3, s10, dyn
[0x80001ce4]:csrrs a7, fcsr, zero
[0x80001ce8]:sw t5, 424(ra)
[0x80001cec]:sw t6, 432(ra)
[0x80001cf0]:sw t5, 440(ra)
[0x80001cf4]:sw a7, 448(ra)
[0x80001cf8]:lw t3, 1248(a6)
[0x80001cfc]:lw t4, 1252(a6)
[0x80001d00]:lw s10, 1256(a6)
[0x80001d04]:lw s11, 1260(a6)
[0x80001d08]:lui t3, 37547
[0x80001d0c]:addi t3, t3, 1679
[0x80001d10]:lui t4, 236300
[0x80001d14]:addi t4, t4, 2775
[0x80001d18]:lui s10, 220819
[0x80001d1c]:addi s10, s10, 1993
[0x80001d20]:lui s11, 262378
[0x80001d24]:addi s11, s11, 2719
[0x80001d28]:addi a4, zero, 130
[0x80001d2c]:csrrw zero, fcsr, a4
[0x80001d30]:fmul.d t5, t3, s10, dyn

[0x80001d30]:fmul.d t5, t3, s10, dyn
[0x80001d34]:csrrs a7, fcsr, zero
[0x80001d38]:sw t5, 456(ra)
[0x80001d3c]:sw t6, 464(ra)
[0x80001d40]:sw t5, 472(ra)
[0x80001d44]:sw a7, 480(ra)
[0x80001d48]:lw t3, 1264(a6)
[0x80001d4c]:lw t4, 1268(a6)
[0x80001d50]:lw s10, 1272(a6)
[0x80001d54]:lw s11, 1276(a6)
[0x80001d58]:lui t3, 47298
[0x80001d5c]:addi t3, t3, 2324
[0x80001d60]:lui t4, 233852
[0x80001d64]:addi t4, t4, 3141
[0x80001d68]:lui s10, 540185
[0x80001d6c]:addi s10, s10, 359
[0x80001d70]:lui s11, 263654
[0x80001d74]:addi s11, s11, 3025
[0x80001d78]:addi a4, zero, 2
[0x80001d7c]:csrrw zero, fcsr, a4
[0x80001d80]:fmul.d t5, t3, s10, dyn

[0x80001d80]:fmul.d t5, t3, s10, dyn
[0x80001d84]:csrrs a7, fcsr, zero
[0x80001d88]:sw t5, 488(ra)
[0x80001d8c]:sw t6, 496(ra)
[0x80001d90]:sw t5, 504(ra)
[0x80001d94]:sw a7, 512(ra)
[0x80001d98]:lw t3, 1280(a6)
[0x80001d9c]:lw t4, 1284(a6)
[0x80001da0]:lw s10, 1288(a6)
[0x80001da4]:lw s11, 1292(a6)
[0x80001da8]:lui t3, 47298
[0x80001dac]:addi t3, t3, 2324
[0x80001db0]:lui t4, 233852
[0x80001db4]:addi t4, t4, 3141
[0x80001db8]:lui s10, 540185
[0x80001dbc]:addi s10, s10, 359
[0x80001dc0]:lui s11, 263654
[0x80001dc4]:addi s11, s11, 3025
[0x80001dc8]:addi a4, zero, 34
[0x80001dcc]:csrrw zero, fcsr, a4
[0x80001dd0]:fmul.d t5, t3, s10, dyn

[0x80001dd0]:fmul.d t5, t3, s10, dyn
[0x80001dd4]:csrrs a7, fcsr, zero
[0x80001dd8]:sw t5, 520(ra)
[0x80001ddc]:sw t6, 528(ra)
[0x80001de0]:sw t5, 536(ra)
[0x80001de4]:sw a7, 544(ra)
[0x80001de8]:lw t3, 1296(a6)
[0x80001dec]:lw t4, 1300(a6)
[0x80001df0]:lw s10, 1304(a6)
[0x80001df4]:lw s11, 1308(a6)
[0x80001df8]:lui t3, 47298
[0x80001dfc]:addi t3, t3, 2324
[0x80001e00]:lui t4, 233852
[0x80001e04]:addi t4, t4, 3141
[0x80001e08]:lui s10, 540185
[0x80001e0c]:addi s10, s10, 359
[0x80001e10]:lui s11, 263654
[0x80001e14]:addi s11, s11, 3025
[0x80001e18]:addi a4, zero, 66
[0x80001e1c]:csrrw zero, fcsr, a4
[0x80001e20]:fmul.d t5, t3, s10, dyn

[0x80001e20]:fmul.d t5, t3, s10, dyn
[0x80001e24]:csrrs a7, fcsr, zero
[0x80001e28]:sw t5, 552(ra)
[0x80001e2c]:sw t6, 560(ra)
[0x80001e30]:sw t5, 568(ra)
[0x80001e34]:sw a7, 576(ra)
[0x80001e38]:lw t3, 1312(a6)
[0x80001e3c]:lw t4, 1316(a6)
[0x80001e40]:lw s10, 1320(a6)
[0x80001e44]:lw s11, 1324(a6)
[0x80001e48]:lui t3, 47298
[0x80001e4c]:addi t3, t3, 2324
[0x80001e50]:lui t4, 233852
[0x80001e54]:addi t4, t4, 3141
[0x80001e58]:lui s10, 540185
[0x80001e5c]:addi s10, s10, 359
[0x80001e60]:lui s11, 263654
[0x80001e64]:addi s11, s11, 3025
[0x80001e68]:addi a4, zero, 98
[0x80001e6c]:csrrw zero, fcsr, a4
[0x80001e70]:fmul.d t5, t3, s10, dyn

[0x80001e70]:fmul.d t5, t3, s10, dyn
[0x80001e74]:csrrs a7, fcsr, zero
[0x80001e78]:sw t5, 584(ra)
[0x80001e7c]:sw t6, 592(ra)
[0x80001e80]:sw t5, 600(ra)
[0x80001e84]:sw a7, 608(ra)
[0x80001e88]:lw t3, 1328(a6)
[0x80001e8c]:lw t4, 1332(a6)
[0x80001e90]:lw s10, 1336(a6)
[0x80001e94]:lw s11, 1340(a6)
[0x80001e98]:lui t3, 47298
[0x80001e9c]:addi t3, t3, 2324
[0x80001ea0]:lui t4, 233852
[0x80001ea4]:addi t4, t4, 3141
[0x80001ea8]:lui s10, 540185
[0x80001eac]:addi s10, s10, 359
[0x80001eb0]:lui s11, 263654
[0x80001eb4]:addi s11, s11, 3025
[0x80001eb8]:addi a4, zero, 130
[0x80001ebc]:csrrw zero, fcsr, a4
[0x80001ec0]:fmul.d t5, t3, s10, dyn

[0x80001ec0]:fmul.d t5, t3, s10, dyn
[0x80001ec4]:csrrs a7, fcsr, zero
[0x80001ec8]:sw t5, 616(ra)
[0x80001ecc]:sw t6, 624(ra)
[0x80001ed0]:sw t5, 632(ra)
[0x80001ed4]:sw a7, 640(ra)
[0x80001ed8]:lw t3, 1344(a6)
[0x80001edc]:lw t4, 1348(a6)
[0x80001ee0]:lw s10, 1352(a6)
[0x80001ee4]:lw s11, 1356(a6)
[0x80001ee8]:lui t3, 942047
[0x80001eec]:addi t3, t3, 2682
[0x80001ef0]:lui t4, 236314
[0x80001ef4]:addi t4, t4, 2265
[0x80001ef8]:lui s10, 611059
[0x80001efc]:addi s10, s10, 669
[0x80001f00]:lui s11, 260248
[0x80001f04]:addi s11, s11, 968
[0x80001f08]:addi a4, zero, 2
[0x80001f0c]:csrrw zero, fcsr, a4
[0x80001f10]:fmul.d t5, t3, s10, dyn

[0x80001f10]:fmul.d t5, t3, s10, dyn
[0x80001f14]:csrrs a7, fcsr, zero
[0x80001f18]:sw t5, 648(ra)
[0x80001f1c]:sw t6, 656(ra)
[0x80001f20]:sw t5, 664(ra)
[0x80001f24]:sw a7, 672(ra)
[0x80001f28]:lw t3, 1360(a6)
[0x80001f2c]:lw t4, 1364(a6)
[0x80001f30]:lw s10, 1368(a6)
[0x80001f34]:lw s11, 1372(a6)
[0x80001f38]:lui t3, 942047
[0x80001f3c]:addi t3, t3, 2682
[0x80001f40]:lui t4, 236314
[0x80001f44]:addi t4, t4, 2265
[0x80001f48]:lui s10, 611059
[0x80001f4c]:addi s10, s10, 669
[0x80001f50]:lui s11, 260248
[0x80001f54]:addi s11, s11, 968
[0x80001f58]:addi a4, zero, 34
[0x80001f5c]:csrrw zero, fcsr, a4
[0x80001f60]:fmul.d t5, t3, s10, dyn

[0x80001f60]:fmul.d t5, t3, s10, dyn
[0x80001f64]:csrrs a7, fcsr, zero
[0x80001f68]:sw t5, 680(ra)
[0x80001f6c]:sw t6, 688(ra)
[0x80001f70]:sw t5, 696(ra)
[0x80001f74]:sw a7, 704(ra)
[0x80001f78]:lw t3, 1376(a6)
[0x80001f7c]:lw t4, 1380(a6)
[0x80001f80]:lw s10, 1384(a6)
[0x80001f84]:lw s11, 1388(a6)
[0x80001f88]:lui t3, 942047
[0x80001f8c]:addi t3, t3, 2682
[0x80001f90]:lui t4, 236314
[0x80001f94]:addi t4, t4, 2265
[0x80001f98]:lui s10, 611059
[0x80001f9c]:addi s10, s10, 669
[0x80001fa0]:lui s11, 260248
[0x80001fa4]:addi s11, s11, 968
[0x80001fa8]:addi a4, zero, 66
[0x80001fac]:csrrw zero, fcsr, a4
[0x80001fb0]:fmul.d t5, t3, s10, dyn

[0x80001fb0]:fmul.d t5, t3, s10, dyn
[0x80001fb4]:csrrs a7, fcsr, zero
[0x80001fb8]:sw t5, 712(ra)
[0x80001fbc]:sw t6, 720(ra)
[0x80001fc0]:sw t5, 728(ra)
[0x80001fc4]:sw a7, 736(ra)
[0x80001fc8]:lw t3, 1392(a6)
[0x80001fcc]:lw t4, 1396(a6)
[0x80001fd0]:lw s10, 1400(a6)
[0x80001fd4]:lw s11, 1404(a6)
[0x80001fd8]:lui t3, 942047
[0x80001fdc]:addi t3, t3, 2682
[0x80001fe0]:lui t4, 236314
[0x80001fe4]:addi t4, t4, 2265
[0x80001fe8]:lui s10, 611059
[0x80001fec]:addi s10, s10, 669
[0x80001ff0]:lui s11, 260248
[0x80001ff4]:addi s11, s11, 968
[0x80001ff8]:addi a4, zero, 98
[0x80001ffc]:csrrw zero, fcsr, a4
[0x80002000]:fmul.d t5, t3, s10, dyn

[0x80002000]:fmul.d t5, t3, s10, dyn
[0x80002004]:csrrs a7, fcsr, zero
[0x80002008]:sw t5, 744(ra)
[0x8000200c]:sw t6, 752(ra)
[0x80002010]:sw t5, 760(ra)
[0x80002014]:sw a7, 768(ra)
[0x80002018]:lw t3, 1408(a6)
[0x8000201c]:lw t4, 1412(a6)
[0x80002020]:lw s10, 1416(a6)
[0x80002024]:lw s11, 1420(a6)
[0x80002028]:lui t3, 942047
[0x8000202c]:addi t3, t3, 2682
[0x80002030]:lui t4, 236314
[0x80002034]:addi t4, t4, 2265
[0x80002038]:lui s10, 611059
[0x8000203c]:addi s10, s10, 669
[0x80002040]:lui s11, 260248
[0x80002044]:addi s11, s11, 968
[0x80002048]:addi a4, zero, 130
[0x8000204c]:csrrw zero, fcsr, a4
[0x80002050]:fmul.d t5, t3, s10, dyn

[0x80002050]:fmul.d t5, t3, s10, dyn
[0x80002054]:csrrs a7, fcsr, zero
[0x80002058]:sw t5, 776(ra)
[0x8000205c]:sw t6, 784(ra)
[0x80002060]:sw t5, 792(ra)
[0x80002064]:sw a7, 800(ra)
[0x80002068]:lw t3, 1424(a6)
[0x8000206c]:lw t4, 1428(a6)
[0x80002070]:lw s10, 1432(a6)
[0x80002074]:lw s11, 1436(a6)
[0x80002078]:lui t3, 1020538
[0x8000207c]:addi t3, t3, 72
[0x80002080]:lui t4, 235942
[0x80002084]:addi t4, t4, 570
[0x80002088]:lui s10, 974339
[0x8000208c]:addi s10, s10, 1117
[0x80002090]:lui s11, 259831
[0x80002094]:addi s11, s11, 1486
[0x80002098]:addi a4, zero, 2
[0x8000209c]:csrrw zero, fcsr, a4
[0x800020a0]:fmul.d t5, t3, s10, dyn

[0x800020a0]:fmul.d t5, t3, s10, dyn
[0x800020a4]:csrrs a7, fcsr, zero
[0x800020a8]:sw t5, 808(ra)
[0x800020ac]:sw t6, 816(ra)
[0x800020b0]:sw t5, 824(ra)
[0x800020b4]:sw a7, 832(ra)
[0x800020b8]:lw t3, 1440(a6)
[0x800020bc]:lw t4, 1444(a6)
[0x800020c0]:lw s10, 1448(a6)
[0x800020c4]:lw s11, 1452(a6)
[0x800020c8]:lui t3, 1020538
[0x800020cc]:addi t3, t3, 72
[0x800020d0]:lui t4, 235942
[0x800020d4]:addi t4, t4, 570
[0x800020d8]:lui s10, 974339
[0x800020dc]:addi s10, s10, 1117
[0x800020e0]:lui s11, 259831
[0x800020e4]:addi s11, s11, 1486
[0x800020e8]:addi a4, zero, 34
[0x800020ec]:csrrw zero, fcsr, a4
[0x800020f0]:fmul.d t5, t3, s10, dyn

[0x800020f0]:fmul.d t5, t3, s10, dyn
[0x800020f4]:csrrs a7, fcsr, zero
[0x800020f8]:sw t5, 840(ra)
[0x800020fc]:sw t6, 848(ra)
[0x80002100]:sw t5, 856(ra)
[0x80002104]:sw a7, 864(ra)
[0x80002108]:lw t3, 1456(a6)
[0x8000210c]:lw t4, 1460(a6)
[0x80002110]:lw s10, 1464(a6)
[0x80002114]:lw s11, 1468(a6)
[0x80002118]:lui t3, 1020538
[0x8000211c]:addi t3, t3, 72
[0x80002120]:lui t4, 235942
[0x80002124]:addi t4, t4, 570
[0x80002128]:lui s10, 974339
[0x8000212c]:addi s10, s10, 1117
[0x80002130]:lui s11, 259831
[0x80002134]:addi s11, s11, 1486
[0x80002138]:addi a4, zero, 66
[0x8000213c]:csrrw zero, fcsr, a4
[0x80002140]:fmul.d t5, t3, s10, dyn

[0x80002140]:fmul.d t5, t3, s10, dyn
[0x80002144]:csrrs a7, fcsr, zero
[0x80002148]:sw t5, 872(ra)
[0x8000214c]:sw t6, 880(ra)
[0x80002150]:sw t5, 888(ra)
[0x80002154]:sw a7, 896(ra)
[0x80002158]:lw t3, 1472(a6)
[0x8000215c]:lw t4, 1476(a6)
[0x80002160]:lw s10, 1480(a6)
[0x80002164]:lw s11, 1484(a6)
[0x80002168]:lui t3, 1020538
[0x8000216c]:addi t3, t3, 72
[0x80002170]:lui t4, 235942
[0x80002174]:addi t4, t4, 570
[0x80002178]:lui s10, 974339
[0x8000217c]:addi s10, s10, 1117
[0x80002180]:lui s11, 259831
[0x80002184]:addi s11, s11, 1486
[0x80002188]:addi a4, zero, 98
[0x8000218c]:csrrw zero, fcsr, a4
[0x80002190]:fmul.d t5, t3, s10, dyn

[0x80002190]:fmul.d t5, t3, s10, dyn
[0x80002194]:csrrs a7, fcsr, zero
[0x80002198]:sw t5, 904(ra)
[0x8000219c]:sw t6, 912(ra)
[0x800021a0]:sw t5, 920(ra)
[0x800021a4]:sw a7, 928(ra)
[0x800021a8]:lw t3, 1488(a6)
[0x800021ac]:lw t4, 1492(a6)
[0x800021b0]:lw s10, 1496(a6)
[0x800021b4]:lw s11, 1500(a6)
[0x800021b8]:lui t3, 1020538
[0x800021bc]:addi t3, t3, 72
[0x800021c0]:lui t4, 235942
[0x800021c4]:addi t4, t4, 570
[0x800021c8]:lui s10, 974339
[0x800021cc]:addi s10, s10, 1117
[0x800021d0]:lui s11, 259831
[0x800021d4]:addi s11, s11, 1486
[0x800021d8]:addi a4, zero, 130
[0x800021dc]:csrrw zero, fcsr, a4
[0x800021e0]:fmul.d t5, t3, s10, dyn

[0x800021e0]:fmul.d t5, t3, s10, dyn
[0x800021e4]:csrrs a7, fcsr, zero
[0x800021e8]:sw t5, 936(ra)
[0x800021ec]:sw t6, 944(ra)
[0x800021f0]:sw t5, 952(ra)
[0x800021f4]:sw a7, 960(ra)
[0x800021f8]:lw t3, 1504(a6)
[0x800021fc]:lw t4, 1508(a6)
[0x80002200]:lw s10, 1512(a6)
[0x80002204]:lw s11, 1516(a6)
[0x80002208]:lui t3, 856462
[0x8000220c]:addi t3, t3, 2350
[0x80002210]:lui t4, 235760
[0x80002214]:addi t4, t4, 157
[0x80002218]:lui s10, 1003929
[0x8000221c]:addi s10, s10, 1167
[0x80002220]:lui s11, 270088
[0x80002224]:addi s11, s11, 972
[0x80002228]:addi a4, zero, 2
[0x8000222c]:csrrw zero, fcsr, a4
[0x80002230]:fmul.d t5, t3, s10, dyn

[0x80002230]:fmul.d t5, t3, s10, dyn
[0x80002234]:csrrs a7, fcsr, zero
[0x80002238]:sw t5, 968(ra)
[0x8000223c]:sw t6, 976(ra)
[0x80002240]:sw t5, 984(ra)
[0x80002244]:sw a7, 992(ra)
[0x80002248]:lw t3, 1520(a6)
[0x8000224c]:lw t4, 1524(a6)
[0x80002250]:lw s10, 1528(a6)
[0x80002254]:lw s11, 1532(a6)
[0x80002258]:lui t3, 856462
[0x8000225c]:addi t3, t3, 2350
[0x80002260]:lui t4, 235760
[0x80002264]:addi t4, t4, 157
[0x80002268]:lui s10, 1003929
[0x8000226c]:addi s10, s10, 1167
[0x80002270]:lui s11, 270088
[0x80002274]:addi s11, s11, 972
[0x80002278]:addi a4, zero, 34
[0x8000227c]:csrrw zero, fcsr, a4
[0x80002280]:fmul.d t5, t3, s10, dyn

[0x80002280]:fmul.d t5, t3, s10, dyn
[0x80002284]:csrrs a7, fcsr, zero
[0x80002288]:sw t5, 1000(ra)
[0x8000228c]:sw t6, 1008(ra)
[0x80002290]:sw t5, 1016(ra)
[0x80002294]:sw a7, 1024(ra)
[0x80002298]:lw t3, 1536(a6)
[0x8000229c]:lw t4, 1540(a6)
[0x800022a0]:lw s10, 1544(a6)
[0x800022a4]:lw s11, 1548(a6)
[0x800022a8]:lui t3, 856462
[0x800022ac]:addi t3, t3, 2350
[0x800022b0]:lui t4, 235760
[0x800022b4]:addi t4, t4, 157
[0x800022b8]:lui s10, 1003929
[0x800022bc]:addi s10, s10, 1167
[0x800022c0]:lui s11, 270088
[0x800022c4]:addi s11, s11, 972
[0x800022c8]:addi a4, zero, 66
[0x800022cc]:csrrw zero, fcsr, a4
[0x800022d0]:fmul.d t5, t3, s10, dyn

[0x800022d0]:fmul.d t5, t3, s10, dyn
[0x800022d4]:csrrs a7, fcsr, zero
[0x800022d8]:sw t5, 1032(ra)
[0x800022dc]:sw t6, 1040(ra)
[0x800022e0]:sw t5, 1048(ra)
[0x800022e4]:sw a7, 1056(ra)
[0x800022e8]:lw t3, 1552(a6)
[0x800022ec]:lw t4, 1556(a6)
[0x800022f0]:lw s10, 1560(a6)
[0x800022f4]:lw s11, 1564(a6)
[0x800022f8]:lui t3, 856462
[0x800022fc]:addi t3, t3, 2350
[0x80002300]:lui t4, 235760
[0x80002304]:addi t4, t4, 157
[0x80002308]:lui s10, 1003929
[0x8000230c]:addi s10, s10, 1167
[0x80002310]:lui s11, 270088
[0x80002314]:addi s11, s11, 972
[0x80002318]:addi a4, zero, 98
[0x8000231c]:csrrw zero, fcsr, a4
[0x80002320]:fmul.d t5, t3, s10, dyn

[0x80002320]:fmul.d t5, t3, s10, dyn
[0x80002324]:csrrs a7, fcsr, zero
[0x80002328]:sw t5, 1064(ra)
[0x8000232c]:sw t6, 1072(ra)
[0x80002330]:sw t5, 1080(ra)
[0x80002334]:sw a7, 1088(ra)
[0x80002338]:lw t3, 1568(a6)
[0x8000233c]:lw t4, 1572(a6)
[0x80002340]:lw s10, 1576(a6)
[0x80002344]:lw s11, 1580(a6)
[0x80002348]:lui t3, 856462
[0x8000234c]:addi t3, t3, 2350
[0x80002350]:lui t4, 235760
[0x80002354]:addi t4, t4, 157
[0x80002358]:lui s10, 1003929
[0x8000235c]:addi s10, s10, 1167
[0x80002360]:lui s11, 270088
[0x80002364]:addi s11, s11, 972
[0x80002368]:addi a4, zero, 130
[0x8000236c]:csrrw zero, fcsr, a4
[0x80002370]:fmul.d t5, t3, s10, dyn

[0x80002370]:fmul.d t5, t3, s10, dyn
[0x80002374]:csrrs a7, fcsr, zero
[0x80002378]:sw t5, 1096(ra)
[0x8000237c]:sw t6, 1104(ra)
[0x80002380]:sw t5, 1112(ra)
[0x80002384]:sw a7, 1120(ra)
[0x80002388]:lw t3, 1584(a6)
[0x8000238c]:lw t4, 1588(a6)
[0x80002390]:lw s10, 1592(a6)
[0x80002394]:lw s11, 1596(a6)
[0x80002398]:lui t3, 933466
[0x8000239c]:addi t3, t3, 2865
[0x800023a0]:lui t4, 235830
[0x800023a4]:addi t4, t4, 3035
[0x800023a8]:lui s10, 793625
[0x800023ac]:addi s10, s10, 1745
[0x800023b0]:lui s11, 266407
[0x800023b4]:addi s11, s11, 680
[0x800023b8]:addi a4, zero, 2
[0x800023bc]:csrrw zero, fcsr, a4
[0x800023c0]:fmul.d t5, t3, s10, dyn

[0x800023c0]:fmul.d t5, t3, s10, dyn
[0x800023c4]:csrrs a7, fcsr, zero
[0x800023c8]:sw t5, 1128(ra)
[0x800023cc]:sw t6, 1136(ra)
[0x800023d0]:sw t5, 1144(ra)
[0x800023d4]:sw a7, 1152(ra)
[0x800023d8]:lw t3, 1600(a6)
[0x800023dc]:lw t4, 1604(a6)
[0x800023e0]:lw s10, 1608(a6)
[0x800023e4]:lw s11, 1612(a6)
[0x800023e8]:lui t3, 933466
[0x800023ec]:addi t3, t3, 2865
[0x800023f0]:lui t4, 235830
[0x800023f4]:addi t4, t4, 3035
[0x800023f8]:lui s10, 793625
[0x800023fc]:addi s10, s10, 1745
[0x80002400]:lui s11, 266407
[0x80002404]:addi s11, s11, 680
[0x80002408]:addi a4, zero, 34
[0x8000240c]:csrrw zero, fcsr, a4
[0x80002410]:fmul.d t5, t3, s10, dyn

[0x80002410]:fmul.d t5, t3, s10, dyn
[0x80002414]:csrrs a7, fcsr, zero
[0x80002418]:sw t5, 1160(ra)
[0x8000241c]:sw t6, 1168(ra)
[0x80002420]:sw t5, 1176(ra)
[0x80002424]:sw a7, 1184(ra)
[0x80002428]:lw t3, 1616(a6)
[0x8000242c]:lw t4, 1620(a6)
[0x80002430]:lw s10, 1624(a6)
[0x80002434]:lw s11, 1628(a6)
[0x80002438]:lui t3, 933466
[0x8000243c]:addi t3, t3, 2865
[0x80002440]:lui t4, 235830
[0x80002444]:addi t4, t4, 3035
[0x80002448]:lui s10, 793625
[0x8000244c]:addi s10, s10, 1745
[0x80002450]:lui s11, 266407
[0x80002454]:addi s11, s11, 680
[0x80002458]:addi a4, zero, 66
[0x8000245c]:csrrw zero, fcsr, a4
[0x80002460]:fmul.d t5, t3, s10, dyn

[0x80002460]:fmul.d t5, t3, s10, dyn
[0x80002464]:csrrs a7, fcsr, zero
[0x80002468]:sw t5, 1192(ra)
[0x8000246c]:sw t6, 1200(ra)
[0x80002470]:sw t5, 1208(ra)
[0x80002474]:sw a7, 1216(ra)
[0x80002478]:lw t3, 1632(a6)
[0x8000247c]:lw t4, 1636(a6)
[0x80002480]:lw s10, 1640(a6)
[0x80002484]:lw s11, 1644(a6)
[0x80002488]:lui t3, 933466
[0x8000248c]:addi t3, t3, 2865
[0x80002490]:lui t4, 235830
[0x80002494]:addi t4, t4, 3035
[0x80002498]:lui s10, 793625
[0x8000249c]:addi s10, s10, 1745
[0x800024a0]:lui s11, 266407
[0x800024a4]:addi s11, s11, 680
[0x800024a8]:addi a4, zero, 98
[0x800024ac]:csrrw zero, fcsr, a4
[0x800024b0]:fmul.d t5, t3, s10, dyn

[0x800024b0]:fmul.d t5, t3, s10, dyn
[0x800024b4]:csrrs a7, fcsr, zero
[0x800024b8]:sw t5, 1224(ra)
[0x800024bc]:sw t6, 1232(ra)
[0x800024c0]:sw t5, 1240(ra)
[0x800024c4]:sw a7, 1248(ra)
[0x800024c8]:lw t3, 1648(a6)
[0x800024cc]:lw t4, 1652(a6)
[0x800024d0]:lw s10, 1656(a6)
[0x800024d4]:lw s11, 1660(a6)
[0x800024d8]:lui t3, 933466
[0x800024dc]:addi t3, t3, 2865
[0x800024e0]:lui t4, 235830
[0x800024e4]:addi t4, t4, 3035
[0x800024e8]:lui s10, 793625
[0x800024ec]:addi s10, s10, 1745
[0x800024f0]:lui s11, 266407
[0x800024f4]:addi s11, s11, 680
[0x800024f8]:addi a4, zero, 130
[0x800024fc]:csrrw zero, fcsr, a4
[0x80002500]:fmul.d t5, t3, s10, dyn

[0x80002500]:fmul.d t5, t3, s10, dyn
[0x80002504]:csrrs a7, fcsr, zero
[0x80002508]:sw t5, 1256(ra)
[0x8000250c]:sw t6, 1264(ra)
[0x80002510]:sw t5, 1272(ra)
[0x80002514]:sw a7, 1280(ra)
[0x80002518]:lw t3, 1664(a6)
[0x8000251c]:lw t4, 1668(a6)
[0x80002520]:lw s10, 1672(a6)
[0x80002524]:lw s11, 1676(a6)
[0x80002528]:lui t3, 667111
[0x8000252c]:addi t3, t3, 3440
[0x80002530]:lui t4, 236293
[0x80002534]:addi t4, t4, 2648
[0x80002538]:lui s10, 254835
[0x8000253c]:addi s10, s10, 2947
[0x80002540]:lui s11, 263865
[0x80002544]:addi s11, s11, 4019
[0x80002548]:addi a4, zero, 2
[0x8000254c]:csrrw zero, fcsr, a4
[0x80002550]:fmul.d t5, t3, s10, dyn

[0x80002550]:fmul.d t5, t3, s10, dyn
[0x80002554]:csrrs a7, fcsr, zero
[0x80002558]:sw t5, 1288(ra)
[0x8000255c]:sw t6, 1296(ra)
[0x80002560]:sw t5, 1304(ra)
[0x80002564]:sw a7, 1312(ra)
[0x80002568]:lw t3, 1680(a6)
[0x8000256c]:lw t4, 1684(a6)
[0x80002570]:lw s10, 1688(a6)
[0x80002574]:lw s11, 1692(a6)
[0x80002578]:lui t3, 667111
[0x8000257c]:addi t3, t3, 3440
[0x80002580]:lui t4, 236293
[0x80002584]:addi t4, t4, 2648
[0x80002588]:lui s10, 254835
[0x8000258c]:addi s10, s10, 2947
[0x80002590]:lui s11, 263865
[0x80002594]:addi s11, s11, 4019
[0x80002598]:addi a4, zero, 34
[0x8000259c]:csrrw zero, fcsr, a4
[0x800025a0]:fmul.d t5, t3, s10, dyn

[0x800025a0]:fmul.d t5, t3, s10, dyn
[0x800025a4]:csrrs a7, fcsr, zero
[0x800025a8]:sw t5, 1320(ra)
[0x800025ac]:sw t6, 1328(ra)
[0x800025b0]:sw t5, 1336(ra)
[0x800025b4]:sw a7, 1344(ra)
[0x800025b8]:lw t3, 1696(a6)
[0x800025bc]:lw t4, 1700(a6)
[0x800025c0]:lw s10, 1704(a6)
[0x800025c4]:lw s11, 1708(a6)
[0x800025c8]:lui t3, 667111
[0x800025cc]:addi t3, t3, 3440
[0x800025d0]:lui t4, 236293
[0x800025d4]:addi t4, t4, 2648
[0x800025d8]:lui s10, 254835
[0x800025dc]:addi s10, s10, 2947
[0x800025e0]:lui s11, 263865
[0x800025e4]:addi s11, s11, 4019
[0x800025e8]:addi a4, zero, 66
[0x800025ec]:csrrw zero, fcsr, a4
[0x800025f0]:fmul.d t5, t3, s10, dyn

[0x800025f0]:fmul.d t5, t3, s10, dyn
[0x800025f4]:csrrs a7, fcsr, zero
[0x800025f8]:sw t5, 1352(ra)
[0x800025fc]:sw t6, 1360(ra)
[0x80002600]:sw t5, 1368(ra)
[0x80002604]:sw a7, 1376(ra)
[0x80002608]:lw t3, 1712(a6)
[0x8000260c]:lw t4, 1716(a6)
[0x80002610]:lw s10, 1720(a6)
[0x80002614]:lw s11, 1724(a6)
[0x80002618]:lui t3, 667111
[0x8000261c]:addi t3, t3, 3440
[0x80002620]:lui t4, 236293
[0x80002624]:addi t4, t4, 2648
[0x80002628]:lui s10, 254835
[0x8000262c]:addi s10, s10, 2947
[0x80002630]:lui s11, 263865
[0x80002634]:addi s11, s11, 4019
[0x80002638]:addi a4, zero, 98
[0x8000263c]:csrrw zero, fcsr, a4
[0x80002640]:fmul.d t5, t3, s10, dyn

[0x80002640]:fmul.d t5, t3, s10, dyn
[0x80002644]:csrrs a7, fcsr, zero
[0x80002648]:sw t5, 1384(ra)
[0x8000264c]:sw t6, 1392(ra)
[0x80002650]:sw t5, 1400(ra)
[0x80002654]:sw a7, 1408(ra)
[0x80002658]:lw t3, 1728(a6)
[0x8000265c]:lw t4, 1732(a6)
[0x80002660]:lw s10, 1736(a6)
[0x80002664]:lw s11, 1740(a6)
[0x80002668]:lui t3, 667111
[0x8000266c]:addi t3, t3, 3440
[0x80002670]:lui t4, 236293
[0x80002674]:addi t4, t4, 2648
[0x80002678]:lui s10, 254835
[0x8000267c]:addi s10, s10, 2947
[0x80002680]:lui s11, 263865
[0x80002684]:addi s11, s11, 4019
[0x80002688]:addi a4, zero, 130
[0x8000268c]:csrrw zero, fcsr, a4
[0x80002690]:fmul.d t5, t3, s10, dyn

[0x80002690]:fmul.d t5, t3, s10, dyn
[0x80002694]:csrrs a7, fcsr, zero
[0x80002698]:sw t5, 1416(ra)
[0x8000269c]:sw t6, 1424(ra)
[0x800026a0]:sw t5, 1432(ra)
[0x800026a4]:sw a7, 1440(ra)
[0x800026a8]:lw t3, 1744(a6)
[0x800026ac]:lw t4, 1748(a6)
[0x800026b0]:lw s10, 1752(a6)
[0x800026b4]:lw s11, 1756(a6)
[0x800026b8]:lui t3, 363459
[0x800026bc]:addi t3, t3, 1310
[0x800026c0]:lui t4, 235426
[0x800026c4]:addi t4, t4, 2861
[0x800026c8]:lui s10, 37316
[0x800026cc]:addi s10, s10, 2733
[0x800026d0]:lui s11, 263226
[0x800026d4]:addi s11, s11, 3259
[0x800026d8]:addi a4, zero, 2
[0x800026dc]:csrrw zero, fcsr, a4
[0x800026e0]:fmul.d t5, t3, s10, dyn

[0x800026e0]:fmul.d t5, t3, s10, dyn
[0x800026e4]:csrrs a7, fcsr, zero
[0x800026e8]:sw t5, 1448(ra)
[0x800026ec]:sw t6, 1456(ra)
[0x800026f0]:sw t5, 1464(ra)
[0x800026f4]:sw a7, 1472(ra)
[0x800026f8]:lw t3, 1760(a6)
[0x800026fc]:lw t4, 1764(a6)
[0x80002700]:lw s10, 1768(a6)
[0x80002704]:lw s11, 1772(a6)
[0x80002708]:lui t3, 363459
[0x8000270c]:addi t3, t3, 1310
[0x80002710]:lui t4, 235426
[0x80002714]:addi t4, t4, 2861
[0x80002718]:lui s10, 37316
[0x8000271c]:addi s10, s10, 2733
[0x80002720]:lui s11, 263226
[0x80002724]:addi s11, s11, 3259
[0x80002728]:addi a4, zero, 34
[0x8000272c]:csrrw zero, fcsr, a4
[0x80002730]:fmul.d t5, t3, s10, dyn

[0x80002730]:fmul.d t5, t3, s10, dyn
[0x80002734]:csrrs a7, fcsr, zero
[0x80002738]:sw t5, 1480(ra)
[0x8000273c]:sw t6, 1488(ra)
[0x80002740]:sw t5, 1496(ra)
[0x80002744]:sw a7, 1504(ra)
[0x80002748]:lw t3, 1776(a6)
[0x8000274c]:lw t4, 1780(a6)
[0x80002750]:lw s10, 1784(a6)
[0x80002754]:lw s11, 1788(a6)
[0x80002758]:lui t3, 363459
[0x8000275c]:addi t3, t3, 1310
[0x80002760]:lui t4, 235426
[0x80002764]:addi t4, t4, 2861
[0x80002768]:lui s10, 37316
[0x8000276c]:addi s10, s10, 2733
[0x80002770]:lui s11, 263226
[0x80002774]:addi s11, s11, 3259
[0x80002778]:addi a4, zero, 66
[0x8000277c]:csrrw zero, fcsr, a4
[0x80002780]:fmul.d t5, t3, s10, dyn

[0x80002780]:fmul.d t5, t3, s10, dyn
[0x80002784]:csrrs a7, fcsr, zero
[0x80002788]:sw t5, 1512(ra)
[0x8000278c]:sw t6, 1520(ra)
[0x80002790]:sw t5, 1528(ra)
[0x80002794]:sw a7, 1536(ra)
[0x80002798]:lw t3, 1792(a6)
[0x8000279c]:lw t4, 1796(a6)
[0x800027a0]:lw s10, 1800(a6)
[0x800027a4]:lw s11, 1804(a6)
[0x800027a8]:lui t3, 363459
[0x800027ac]:addi t3, t3, 1310
[0x800027b0]:lui t4, 235426
[0x800027b4]:addi t4, t4, 2861
[0x800027b8]:lui s10, 37316
[0x800027bc]:addi s10, s10, 2733
[0x800027c0]:lui s11, 263226
[0x800027c4]:addi s11, s11, 3259
[0x800027c8]:addi a4, zero, 98
[0x800027cc]:csrrw zero, fcsr, a4
[0x800027d0]:fmul.d t5, t3, s10, dyn

[0x800027d0]:fmul.d t5, t3, s10, dyn
[0x800027d4]:csrrs a7, fcsr, zero
[0x800027d8]:sw t5, 1544(ra)
[0x800027dc]:sw t6, 1552(ra)
[0x800027e0]:sw t5, 1560(ra)
[0x800027e4]:sw a7, 1568(ra)
[0x800027e8]:lw t3, 1808(a6)
[0x800027ec]:lw t4, 1812(a6)
[0x800027f0]:lw s10, 1816(a6)
[0x800027f4]:lw s11, 1820(a6)
[0x800027f8]:lui t3, 363459
[0x800027fc]:addi t3, t3, 1310
[0x80002800]:lui t4, 235426
[0x80002804]:addi t4, t4, 2861
[0x80002808]:lui s10, 37316
[0x8000280c]:addi s10, s10, 2733
[0x80002810]:lui s11, 263226
[0x80002814]:addi s11, s11, 3259
[0x80002818]:addi a4, zero, 130
[0x8000281c]:csrrw zero, fcsr, a4
[0x80002820]:fmul.d t5, t3, s10, dyn

[0x80002820]:fmul.d t5, t3, s10, dyn
[0x80002824]:csrrs a7, fcsr, zero
[0x80002828]:sw t5, 1576(ra)
[0x8000282c]:sw t6, 1584(ra)
[0x80002830]:sw t5, 1592(ra)
[0x80002834]:sw a7, 1600(ra)
[0x80002838]:lw t3, 1824(a6)
[0x8000283c]:lw t4, 1828(a6)
[0x80002840]:lw s10, 1832(a6)
[0x80002844]:lw s11, 1836(a6)
[0x80002848]:lui t3, 885492
[0x8000284c]:addi t3, t3, 2688
[0x80002850]:lui t4, 236106
[0x80002854]:addi t4, t4, 2753
[0x80002858]:lui s10, 1044603
[0x8000285c]:addi s10, s10, 1801
[0x80002860]:lui s11, 261400
[0x80002864]:addi s11, s11, 3169
[0x80002868]:addi a4, zero, 2
[0x8000286c]:csrrw zero, fcsr, a4
[0x80002870]:fmul.d t5, t3, s10, dyn

[0x80002870]:fmul.d t5, t3, s10, dyn
[0x80002874]:csrrs a7, fcsr, zero
[0x80002878]:sw t5, 1608(ra)
[0x8000287c]:sw t6, 1616(ra)
[0x80002880]:sw t5, 1624(ra)
[0x80002884]:sw a7, 1632(ra)
[0x80002888]:lw t3, 1840(a6)
[0x8000288c]:lw t4, 1844(a6)
[0x80002890]:lw s10, 1848(a6)
[0x80002894]:lw s11, 1852(a6)
[0x80002898]:lui t3, 885492
[0x8000289c]:addi t3, t3, 2688
[0x800028a0]:lui t4, 236106
[0x800028a4]:addi t4, t4, 2753
[0x800028a8]:lui s10, 1044603
[0x800028ac]:addi s10, s10, 1801
[0x800028b0]:lui s11, 261400
[0x800028b4]:addi s11, s11, 3169
[0x800028b8]:addi a4, zero, 34
[0x800028bc]:csrrw zero, fcsr, a4
[0x800028c0]:fmul.d t5, t3, s10, dyn

[0x800028c0]:fmul.d t5, t3, s10, dyn
[0x800028c4]:csrrs a7, fcsr, zero
[0x800028c8]:sw t5, 1640(ra)
[0x800028cc]:sw t6, 1648(ra)
[0x800028d0]:sw t5, 1656(ra)
[0x800028d4]:sw a7, 1664(ra)
[0x800028d8]:lw t3, 1856(a6)
[0x800028dc]:lw t4, 1860(a6)
[0x800028e0]:lw s10, 1864(a6)
[0x800028e4]:lw s11, 1868(a6)
[0x800028e8]:lui t3, 885492
[0x800028ec]:addi t3, t3, 2688
[0x800028f0]:lui t4, 236106
[0x800028f4]:addi t4, t4, 2753
[0x800028f8]:lui s10, 1044603
[0x800028fc]:addi s10, s10, 1801
[0x80002900]:lui s11, 261400
[0x80002904]:addi s11, s11, 3169
[0x80002908]:addi a4, zero, 66
[0x8000290c]:csrrw zero, fcsr, a4
[0x80002910]:fmul.d t5, t3, s10, dyn

[0x80002910]:fmul.d t5, t3, s10, dyn
[0x80002914]:csrrs a7, fcsr, zero
[0x80002918]:sw t5, 1672(ra)
[0x8000291c]:sw t6, 1680(ra)
[0x80002920]:sw t5, 1688(ra)
[0x80002924]:sw a7, 1696(ra)
[0x80002928]:lw t3, 1872(a6)
[0x8000292c]:lw t4, 1876(a6)
[0x80002930]:lw s10, 1880(a6)
[0x80002934]:lw s11, 1884(a6)
[0x80002938]:lui t3, 885492
[0x8000293c]:addi t3, t3, 2688
[0x80002940]:lui t4, 236106
[0x80002944]:addi t4, t4, 2753
[0x80002948]:lui s10, 1044603
[0x8000294c]:addi s10, s10, 1801
[0x80002950]:lui s11, 261400
[0x80002954]:addi s11, s11, 3169
[0x80002958]:addi a4, zero, 98
[0x8000295c]:csrrw zero, fcsr, a4
[0x80002960]:fmul.d t5, t3, s10, dyn

[0x80002960]:fmul.d t5, t3, s10, dyn
[0x80002964]:csrrs a7, fcsr, zero
[0x80002968]:sw t5, 1704(ra)
[0x8000296c]:sw t6, 1712(ra)
[0x80002970]:sw t5, 1720(ra)
[0x80002974]:sw a7, 1728(ra)
[0x80002978]:lw t3, 1888(a6)
[0x8000297c]:lw t4, 1892(a6)
[0x80002980]:lw s10, 1896(a6)
[0x80002984]:lw s11, 1900(a6)
[0x80002988]:lui t3, 885492
[0x8000298c]:addi t3, t3, 2688
[0x80002990]:lui t4, 236106
[0x80002994]:addi t4, t4, 2753
[0x80002998]:lui s10, 1044603
[0x8000299c]:addi s10, s10, 1801
[0x800029a0]:lui s11, 261400
[0x800029a4]:addi s11, s11, 3169
[0x800029a8]:addi a4, zero, 130
[0x800029ac]:csrrw zero, fcsr, a4
[0x800029b0]:fmul.d t5, t3, s10, dyn

[0x800029b0]:fmul.d t5, t3, s10, dyn
[0x800029b4]:csrrs a7, fcsr, zero
[0x800029b8]:sw t5, 1736(ra)
[0x800029bc]:sw t6, 1744(ra)
[0x800029c0]:sw t5, 1752(ra)
[0x800029c4]:sw a7, 1760(ra)
[0x800029c8]:lw t3, 1904(a6)
[0x800029cc]:lw t4, 1908(a6)
[0x800029d0]:lw s10, 1912(a6)
[0x800029d4]:lw s11, 1916(a6)
[0x800029d8]:lui t3, 981229
[0x800029dc]:addi t3, t3, 313
[0x800029e0]:lui t4, 235547
[0x800029e4]:addi t4, t4, 533
[0x800029e8]:lui s10, 67669
[0x800029ec]:addi s10, s10, 2529
[0x800029f0]:lui s11, 261014
[0x800029f4]:addi s11, s11, 3942
[0x800029f8]:addi a4, zero, 2
[0x800029fc]:csrrw zero, fcsr, a4
[0x80002a00]:fmul.d t5, t3, s10, dyn

[0x80002a00]:fmul.d t5, t3, s10, dyn
[0x80002a04]:csrrs a7, fcsr, zero
[0x80002a08]:sw t5, 1768(ra)
[0x80002a0c]:sw t6, 1776(ra)
[0x80002a10]:sw t5, 1784(ra)
[0x80002a14]:sw a7, 1792(ra)
[0x80002a18]:lw t3, 1920(a6)
[0x80002a1c]:lw t4, 1924(a6)
[0x80002a20]:lw s10, 1928(a6)
[0x80002a24]:lw s11, 1932(a6)
[0x80002a28]:lui t3, 981229
[0x80002a2c]:addi t3, t3, 313
[0x80002a30]:lui t4, 235547
[0x80002a34]:addi t4, t4, 533
[0x80002a38]:lui s10, 67669
[0x80002a3c]:addi s10, s10, 2529
[0x80002a40]:lui s11, 261014
[0x80002a44]:addi s11, s11, 3942
[0x80002a48]:addi a4, zero, 34
[0x80002a4c]:csrrw zero, fcsr, a4
[0x80002a50]:fmul.d t5, t3, s10, dyn

[0x80002a50]:fmul.d t5, t3, s10, dyn
[0x80002a54]:csrrs a7, fcsr, zero
[0x80002a58]:sw t5, 1800(ra)
[0x80002a5c]:sw t6, 1808(ra)
[0x80002a60]:sw t5, 1816(ra)
[0x80002a64]:sw a7, 1824(ra)
[0x80002a68]:lw t3, 1936(a6)
[0x80002a6c]:lw t4, 1940(a6)
[0x80002a70]:lw s10, 1944(a6)
[0x80002a74]:lw s11, 1948(a6)
[0x80002a78]:lui t3, 981229
[0x80002a7c]:addi t3, t3, 313
[0x80002a80]:lui t4, 235547
[0x80002a84]:addi t4, t4, 533
[0x80002a88]:lui s10, 67669
[0x80002a8c]:addi s10, s10, 2529
[0x80002a90]:lui s11, 261014
[0x80002a94]:addi s11, s11, 3942
[0x80002a98]:addi a4, zero, 66
[0x80002a9c]:csrrw zero, fcsr, a4
[0x80002aa0]:fmul.d t5, t3, s10, dyn

[0x80002aa0]:fmul.d t5, t3, s10, dyn
[0x80002aa4]:csrrs a7, fcsr, zero
[0x80002aa8]:sw t5, 1832(ra)
[0x80002aac]:sw t6, 1840(ra)
[0x80002ab0]:sw t5, 1848(ra)
[0x80002ab4]:sw a7, 1856(ra)
[0x80002ab8]:lw t3, 1952(a6)
[0x80002abc]:lw t4, 1956(a6)
[0x80002ac0]:lw s10, 1960(a6)
[0x80002ac4]:lw s11, 1964(a6)
[0x80002ac8]:lui t3, 981229
[0x80002acc]:addi t3, t3, 313
[0x80002ad0]:lui t4, 235547
[0x80002ad4]:addi t4, t4, 533
[0x80002ad8]:lui s10, 67669
[0x80002adc]:addi s10, s10, 2529
[0x80002ae0]:lui s11, 261014
[0x80002ae4]:addi s11, s11, 3942
[0x80002ae8]:addi a4, zero, 98
[0x80002aec]:csrrw zero, fcsr, a4
[0x80002af0]:fmul.d t5, t3, s10, dyn

[0x80002af0]:fmul.d t5, t3, s10, dyn
[0x80002af4]:csrrs a7, fcsr, zero
[0x80002af8]:sw t5, 1864(ra)
[0x80002afc]:sw t6, 1872(ra)
[0x80002b00]:sw t5, 1880(ra)
[0x80002b04]:sw a7, 1888(ra)
[0x80002b08]:lw t3, 1968(a6)
[0x80002b0c]:lw t4, 1972(a6)
[0x80002b10]:lw s10, 1976(a6)
[0x80002b14]:lw s11, 1980(a6)
[0x80002b18]:lui t3, 981229
[0x80002b1c]:addi t3, t3, 313
[0x80002b20]:lui t4, 235547
[0x80002b24]:addi t4, t4, 533
[0x80002b28]:lui s10, 67669
[0x80002b2c]:addi s10, s10, 2529
[0x80002b30]:lui s11, 261014
[0x80002b34]:addi s11, s11, 3942
[0x80002b38]:addi a4, zero, 130
[0x80002b3c]:csrrw zero, fcsr, a4
[0x80002b40]:fmul.d t5, t3, s10, dyn

[0x80002b40]:fmul.d t5, t3, s10, dyn
[0x80002b44]:csrrs a7, fcsr, zero
[0x80002b48]:sw t5, 1896(ra)
[0x80002b4c]:sw t6, 1904(ra)
[0x80002b50]:sw t5, 1912(ra)
[0x80002b54]:sw a7, 1920(ra)
[0x80002b58]:lw t3, 1984(a6)
[0x80002b5c]:lw t4, 1988(a6)
[0x80002b60]:lw s10, 1992(a6)
[0x80002b64]:lw s11, 1996(a6)
[0x80002b68]:lui t3, 433558
[0x80002b6c]:addi t3, t3, 2083
[0x80002b70]:lui t4, 236235
[0x80002b74]:addi t4, t4, 2394
[0x80002b78]:lui s10, 117992
[0x80002b7c]:addi s10, s10, 3732
[0x80002b80]:lui s11, 259535
[0x80002b84]:addi s11, s11, 1471
[0x80002b88]:addi a4, zero, 2
[0x80002b8c]:csrrw zero, fcsr, a4
[0x80002b90]:fmul.d t5, t3, s10, dyn

[0x80002b90]:fmul.d t5, t3, s10, dyn
[0x80002b94]:csrrs a7, fcsr, zero
[0x80002b98]:sw t5, 1928(ra)
[0x80002b9c]:sw t6, 1936(ra)
[0x80002ba0]:sw t5, 1944(ra)
[0x80002ba4]:sw a7, 1952(ra)
[0x80002ba8]:lw t3, 2000(a6)
[0x80002bac]:lw t4, 2004(a6)
[0x80002bb0]:lw s10, 2008(a6)
[0x80002bb4]:lw s11, 2012(a6)
[0x80002bb8]:lui t3, 433558
[0x80002bbc]:addi t3, t3, 2083
[0x80002bc0]:lui t4, 236235
[0x80002bc4]:addi t4, t4, 2394
[0x80002bc8]:lui s10, 117992
[0x80002bcc]:addi s10, s10, 3732
[0x80002bd0]:lui s11, 259535
[0x80002bd4]:addi s11, s11, 1471
[0x80002bd8]:addi a4, zero, 34
[0x80002bdc]:csrrw zero, fcsr, a4
[0x80002be0]:fmul.d t5, t3, s10, dyn

[0x80002be0]:fmul.d t5, t3, s10, dyn
[0x80002be4]:csrrs a7, fcsr, zero
[0x80002be8]:sw t5, 1960(ra)
[0x80002bec]:sw t6, 1968(ra)
[0x80002bf0]:sw t5, 1976(ra)
[0x80002bf4]:sw a7, 1984(ra)
[0x80002bf8]:lw t3, 2016(a6)
[0x80002bfc]:lw t4, 2020(a6)
[0x80002c00]:lw s10, 2024(a6)
[0x80002c04]:lw s11, 2028(a6)
[0x80002c08]:lui t3, 433558
[0x80002c0c]:addi t3, t3, 2083
[0x80002c10]:lui t4, 236235
[0x80002c14]:addi t4, t4, 2394
[0x80002c18]:lui s10, 117992
[0x80002c1c]:addi s10, s10, 3732
[0x80002c20]:lui s11, 259535
[0x80002c24]:addi s11, s11, 1471
[0x80002c28]:addi a4, zero, 66
[0x80002c2c]:csrrw zero, fcsr, a4
[0x80002c30]:fmul.d t5, t3, s10, dyn

[0x80002c30]:fmul.d t5, t3, s10, dyn
[0x80002c34]:csrrs a7, fcsr, zero
[0x80002c38]:sw t5, 1992(ra)
[0x80002c3c]:sw t6, 2000(ra)
[0x80002c40]:sw t5, 2008(ra)
[0x80002c44]:sw a7, 2016(ra)
[0x80002c48]:lw t3, 2032(a6)
[0x80002c4c]:lw t4, 2036(a6)
[0x80002c50]:lw s10, 2040(a6)
[0x80002c54]:lw s11, 2044(a6)
[0x80002c58]:lui t3, 433558
[0x80002c5c]:addi t3, t3, 2083
[0x80002c60]:lui t4, 236235
[0x80002c64]:addi t4, t4, 2394
[0x80002c68]:lui s10, 117992
[0x80002c6c]:addi s10, s10, 3732
[0x80002c70]:lui s11, 259535
[0x80002c74]:addi s11, s11, 1471
[0x80002c78]:addi a4, zero, 98
[0x80002c7c]:csrrw zero, fcsr, a4
[0x80002c80]:fmul.d t5, t3, s10, dyn

[0x80002c80]:fmul.d t5, t3, s10, dyn
[0x80002c84]:csrrs a7, fcsr, zero
[0x80002c88]:sw t5, 2024(ra)
[0x80002c8c]:sw t6, 2032(ra)
[0x80002c90]:sw t5, 2040(ra)
[0x80002c94]:addi ra, ra, 2040
[0x80002c98]:sw a7, 8(ra)
[0x80002c9c]:lui a4, 1
[0x80002ca0]:addi a4, a4, 2048
[0x80002ca4]:add a6, a6, a4
[0x80002ca8]:lw t3, 0(a6)
[0x80002cac]:sub a6, a6, a4
[0x80002cb0]:lui a4, 1
[0x80002cb4]:addi a4, a4, 2048
[0x80002cb8]:add a6, a6, a4
[0x80002cbc]:lw t4, 4(a6)
[0x80002cc0]:sub a6, a6, a4
[0x80002cc4]:lui a4, 1
[0x80002cc8]:addi a4, a4, 2048
[0x80002ccc]:add a6, a6, a4
[0x80002cd0]:lw s10, 8(a6)
[0x80002cd4]:sub a6, a6, a4
[0x80002cd8]:lui a4, 1
[0x80002cdc]:addi a4, a4, 2048
[0x80002ce0]:add a6, a6, a4
[0x80002ce4]:lw s11, 12(a6)
[0x80002ce8]:sub a6, a6, a4
[0x80002cec]:lui t3, 433558
[0x80002cf0]:addi t3, t3, 2083
[0x80002cf4]:lui t4, 236235
[0x80002cf8]:addi t4, t4, 2394
[0x80002cfc]:lui s10, 117992
[0x80002d00]:addi s10, s10, 3732
[0x80002d04]:lui s11, 259535
[0x80002d08]:addi s11, s11, 1471
[0x80002d0c]:addi a4, zero, 130
[0x80002d10]:csrrw zero, fcsr, a4
[0x80002d14]:fmul.d t5, t3, s10, dyn

[0x80002d14]:fmul.d t5, t3, s10, dyn
[0x80002d18]:csrrs a7, fcsr, zero
[0x80002d1c]:sw t5, 16(ra)
[0x80002d20]:sw t6, 24(ra)
[0x80002d24]:sw t5, 32(ra)
[0x80002d28]:sw a7, 40(ra)
[0x80002d2c]:lui a4, 1
[0x80002d30]:addi a4, a4, 2048
[0x80002d34]:add a6, a6, a4
[0x80002d38]:lw t3, 16(a6)
[0x80002d3c]:sub a6, a6, a4
[0x80002d40]:lui a4, 1
[0x80002d44]:addi a4, a4, 2048
[0x80002d48]:add a6, a6, a4
[0x80002d4c]:lw t4, 20(a6)
[0x80002d50]:sub a6, a6, a4
[0x80002d54]:lui a4, 1
[0x80002d58]:addi a4, a4, 2048
[0x80002d5c]:add a6, a6, a4
[0x80002d60]:lw s10, 24(a6)
[0x80002d64]:sub a6, a6, a4
[0x80002d68]:lui a4, 1
[0x80002d6c]:addi a4, a4, 2048
[0x80002d70]:add a6, a6, a4
[0x80002d74]:lw s11, 28(a6)
[0x80002d78]:sub a6, a6, a4
[0x80002d7c]:lui t3, 363185
[0x80002d80]:addi t3, t3, 1577
[0x80002d84]:lui t4, 236108
[0x80002d88]:addi t4, t4, 3425
[0x80002d8c]:lui s10, 1002677
[0x80002d90]:addi s10, s10, 2233
[0x80002d94]:lui s11, 269707
[0x80002d98]:addi s11, s11, 4054
[0x80002d9c]:addi a4, zero, 98
[0x80002da0]:csrrw zero, fcsr, a4
[0x80002da4]:fmul.d t5, t3, s10, dyn

[0x80002da4]:fmul.d t5, t3, s10, dyn
[0x80002da8]:csrrs a7, fcsr, zero
[0x80002dac]:sw t5, 48(ra)
[0x80002db0]:sw t6, 56(ra)
[0x80002db4]:sw t5, 64(ra)
[0x80002db8]:sw a7, 72(ra)
[0x80002dbc]:lui a4, 1
[0x80002dc0]:addi a4, a4, 2048
[0x80002dc4]:add a6, a6, a4
[0x80002dc8]:lw t3, 32(a6)
[0x80002dcc]:sub a6, a6, a4
[0x80002dd0]:lui a4, 1
[0x80002dd4]:addi a4, a4, 2048
[0x80002dd8]:add a6, a6, a4
[0x80002ddc]:lw t4, 36(a6)
[0x80002de0]:sub a6, a6, a4
[0x80002de4]:lui a4, 1
[0x80002de8]:addi a4, a4, 2048
[0x80002dec]:add a6, a6, a4
[0x80002df0]:lw s10, 40(a6)
[0x80002df4]:sub a6, a6, a4
[0x80002df8]:lui a4, 1
[0x80002dfc]:addi a4, a4, 2048
[0x80002e00]:add a6, a6, a4
[0x80002e04]:lw s11, 44(a6)
[0x80002e08]:sub a6, a6, a4
[0x80002e0c]:lui t3, 363185
[0x80002e10]:addi t3, t3, 1577
[0x80002e14]:lui t4, 236108
[0x80002e18]:addi t4, t4, 3425
[0x80002e1c]:lui s10, 1002677
[0x80002e20]:addi s10, s10, 2233
[0x80002e24]:lui s11, 269707
[0x80002e28]:addi s11, s11, 4054
[0x80002e2c]:addi a4, zero, 130
[0x80002e30]:csrrw zero, fcsr, a4
[0x80002e34]:fmul.d t5, t3, s10, dyn

[0x80002e34]:fmul.d t5, t3, s10, dyn
[0x80002e38]:csrrs a7, fcsr, zero
[0x80002e3c]:sw t5, 80(ra)
[0x80002e40]:sw t6, 88(ra)
[0x80002e44]:sw t5, 96(ra)
[0x80002e48]:sw a7, 104(ra)
[0x80002e4c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x30', 'rs2 : x28', 'rd : x30', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmul.d t5, t5, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
Current Store : [0x80000158] : sw t6, 8(ra) -- Store: [0x80004a20]:0x39A4BD61




Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x30', 'rs2 : x28', 'rd : x30', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmul.d t5, t5, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
Current Store : [0x8000015c] : sw t5, 16(ra) -- Store: [0x80004a28]:0xFFFFFFFF




Last Coverpoint : ['mnemonic : fmul.d', 'rs1 : x30', 'rs2 : x28', 'rd : x30', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000014c]:fmul.d t5, t5, t3, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:sw t5, 0(ra)
	-[0x80000158]:sw t6, 8(ra)
	-[0x8000015c]:sw t5, 16(ra)
	-[0x80000160]:sw tp, 24(ra)
Current Store : [0x80000160] : sw tp, 24(ra) -- Store: [0x80004a30]:0x00000003




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fmul.d t3, s10, t5, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
Current Store : [0x800001a8] : sw t4, 40(ra) -- Store: [0x80004a40]:0x41D8AFD6




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fmul.d t3, s10, t5, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
Current Store : [0x800001ac] : sw t3, 48(ra) -- Store: [0x80004a48]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000019c]:fmul.d t3, s10, t5, dyn
	-[0x800001a0]:csrrs tp, fcsr, zero
	-[0x800001a4]:sw t3, 32(ra)
	-[0x800001a8]:sw t4, 40(ra)
	-[0x800001ac]:sw t3, 48(ra)
	-[0x800001b0]:sw tp, 56(ra)
Current Store : [0x800001b0] : sw tp, 56(ra) -- Store: [0x80004a50]:0x00000023




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fmul.d s10, t3, s10, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
Current Store : [0x800001f8] : sw s11, 72(ra) -- Store: [0x80004a60]:0x41D8AFD6




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fmul.d s10, t3, s10, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
	-[0x800001fc]:sw s10, 80(ra)
Current Store : [0x800001fc] : sw s10, 80(ra) -- Store: [0x80004a68]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x28', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001ec]:fmul.d s10, t3, s10, dyn
	-[0x800001f0]:csrrs tp, fcsr, zero
	-[0x800001f4]:sw s10, 64(ra)
	-[0x800001f8]:sw s11, 72(ra)
	-[0x800001fc]:sw s10, 80(ra)
	-[0x80000200]:sw tp, 88(ra)
Current Store : [0x80000200] : sw tp, 88(ra) -- Store: [0x80004a70]:0x00000043




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fmul.d s8, s8, s8, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s8, 96(ra)
	-[0x80000248]:sw s9, 104(ra)
Current Store : [0x80000248] : sw s9, 104(ra) -- Store: [0x80004a80]:0x39A4BD61




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fmul.d s8, s8, s8, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s8, 96(ra)
	-[0x80000248]:sw s9, 104(ra)
	-[0x8000024c]:sw s8, 112(ra)
Current Store : [0x8000024c] : sw s8, 112(ra) -- Store: [0x80004a88]:0xEBCAC658




Last Coverpoint : ['rs1 : x24', 'rs2 : x24', 'rd : x24', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x8000023c]:fmul.d s8, s8, s8, dyn
	-[0x80000240]:csrrs tp, fcsr, zero
	-[0x80000244]:sw s8, 96(ra)
	-[0x80000248]:sw s9, 104(ra)
	-[0x8000024c]:sw s8, 112(ra)
	-[0x80000250]:sw tp, 120(ra)
Current Store : [0x80000250] : sw tp, 120(ra) -- Store: [0x80004a90]:0x00000063




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fmul.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
Current Store : [0x80000298] : sw s7, 136(ra) -- Store: [0x80004aa0]:0xB6FAB7FB




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fmul.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
Current Store : [0x8000029c] : sw s6, 144(ra) -- Store: [0x80004aa8]:0xEBCAC658




Last Coverpoint : ['rs1 : x20', 'rs2 : x20', 'rd : x22', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000028c]:fmul.d s6, s4, s4, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s6, 128(ra)
	-[0x80000298]:sw s7, 136(ra)
	-[0x8000029c]:sw s6, 144(ra)
	-[0x800002a0]:sw tp, 152(ra)
Current Store : [0x800002a0] : sw tp, 152(ra) -- Store: [0x80004ab0]:0x00000083




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fmul.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
Current Store : [0x800002e8] : sw s5, 168(ra) -- Store: [0x80004ac0]:0x39A4BD61




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fmul.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
Current Store : [0x800002ec] : sw s4, 176(ra) -- Store: [0x80004ac8]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002dc]:fmul.d s4, s6, s2, dyn
	-[0x800002e0]:csrrs tp, fcsr, zero
	-[0x800002e4]:sw s4, 160(ra)
	-[0x800002e8]:sw s5, 168(ra)
	-[0x800002ec]:sw s4, 176(ra)
	-[0x800002f0]:sw tp, 184(ra)
Current Store : [0x800002f0] : sw tp, 184(ra) -- Store: [0x80004ad0]:0x00000003




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fmul.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
Current Store : [0x80000338] : sw s3, 200(ra) -- Store: [0x80004ae0]:0x40F01A74




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fmul.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
Current Store : [0x8000033c] : sw s2, 208(ra) -- Store: [0x80004ae8]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x16', 'rs2 : x22', 'rd : x18', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000032c]:fmul.d s2, a6, s6, dyn
	-[0x80000330]:csrrs tp, fcsr, zero
	-[0x80000334]:sw s2, 192(ra)
	-[0x80000338]:sw s3, 200(ra)
	-[0x8000033c]:sw s2, 208(ra)
	-[0x80000340]:sw tp, 216(ra)
Current Store : [0x80000340] : sw tp, 216(ra) -- Store: [0x80004af0]:0x00000023




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fmul.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
Current Store : [0x80000388] : sw a7, 232(ra) -- Store: [0x80004b00]:0x39AFCB6E




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fmul.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
Current Store : [0x8000038c] : sw a6, 240(ra) -- Store: [0x80004b08]:0xFFFFFFFF




Last Coverpoint : ['rs1 : x18', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000037c]:fmul.d a6, s2, a4, dyn
	-[0x80000380]:csrrs tp, fcsr, zero
	-[0x80000384]:sw a6, 224(ra)
	-[0x80000388]:sw a7, 232(ra)
	-[0x8000038c]:sw a6, 240(ra)
	-[0x80000390]:sw tp, 248(ra)
Current Store : [0x80000390] : sw tp, 248(ra) -- Store: [0x80004b10]:0x00000043




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
Current Store : [0x800003d8] : sw a5, 264(ra) -- Store: [0x80004b20]:0x40F01A74




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
Current Store : [0x800003dc] : sw a4, 272(ra) -- Store: [0x80004b28]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x16', 'rd : x14', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.d a4, a2, a6, dyn
	-[0x800003d0]:csrrs tp, fcsr, zero
	-[0x800003d4]:sw a4, 256(ra)
	-[0x800003d8]:sw a5, 264(ra)
	-[0x800003dc]:sw a4, 272(ra)
	-[0x800003e0]:sw tp, 280(ra)
Current Store : [0x800003e0] : sw tp, 280(ra) -- Store: [0x80004b30]:0x00000063




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fmul.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
Current Store : [0x80000430] : sw a3, 296(ra) -- Store: [0x80004b40]:0x39AFCB6E




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fmul.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
Current Store : [0x80000434] : sw a2, 304(ra) -- Store: [0x80004b48]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x10', 'rd : x12', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fmul.d a2, a4, a0, dyn
	-[0x80000428]:csrrs a7, fcsr, zero
	-[0x8000042c]:sw a2, 288(ra)
	-[0x80000430]:sw a3, 296(ra)
	-[0x80000434]:sw a2, 304(ra)
	-[0x80000438]:sw a7, 312(ra)
Current Store : [0x80000438] : sw a7, 312(ra) -- Store: [0x80004b50]:0x00000083




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
Current Store : [0x80000480] : sw a1, 328(ra) -- Store: [0x80004b60]:0x40F01A74




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
Current Store : [0x80000484] : sw a0, 336(ra) -- Store: [0x80004b68]:0x8A1EDE0E




Last Coverpoint : ['rs1 : x8', 'rs2 : x12', 'rd : x10', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.d a0, fp, a2, dyn
	-[0x80000478]:csrrs a7, fcsr, zero
	-[0x8000047c]:sw a0, 320(ra)
	-[0x80000480]:sw a1, 328(ra)
	-[0x80000484]:sw a0, 336(ra)
	-[0x80000488]:sw a7, 344(ra)
Current Store : [0x80000488] : sw a7, 344(ra) -- Store: [0x80004b70]:0x00000003




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fmul.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
Current Store : [0x800004d8] : sw s1, 8(ra) -- Store: [0x80004ad0]:0x39A35544




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fmul.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
Current Store : [0x800004dc] : sw fp, 16(ra) -- Store: [0x80004ad8]:0x8A1EDE0D




Last Coverpoint : ['rs1 : x10', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004cc]:fmul.d fp, a0, t1, dyn
	-[0x800004d0]:csrrs a7, fcsr, zero
	-[0x800004d4]:sw fp, 0(ra)
	-[0x800004d8]:sw s1, 8(ra)
	-[0x800004dc]:sw fp, 16(ra)
	-[0x800004e0]:sw a7, 24(ra)
Current Store : [0x800004e0] : sw a7, 24(ra) -- Store: [0x80004ae0]:0x00000023




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
Current Store : [0x80000528] : sw t2, 40(ra) -- Store: [0x80004af0]:0x40773944




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
Current Store : [0x8000052c] : sw t1, 48(ra) -- Store: [0x80004af8]:0x8A1EDE0D




Last Coverpoint : ['rs1 : x4', 'rs2 : x8', 'rd : x6', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmul.d t1, tp, fp, dyn
	-[0x80000520]:csrrs a7, fcsr, zero
	-[0x80000524]:sw t1, 32(ra)
	-[0x80000528]:sw t2, 40(ra)
	-[0x8000052c]:sw t1, 48(ra)
	-[0x80000530]:sw a7, 56(ra)
Current Store : [0x80000530] : sw a7, 56(ra) -- Store: [0x80004b00]:0x00000043




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fmul.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
Current Store : [0x80000578] : sw t0, 72(ra) -- Store: [0x80004b10]:0x39A35544




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fmul.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
Current Store : [0x8000057c] : sw tp, 80(ra) -- Store: [0x80004b18]:0x8A1EDE0E




Last Coverpoint : ['rs1 : x6', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000056c]:fmul.d tp, t1, sp, dyn
	-[0x80000570]:csrrs a7, fcsr, zero
	-[0x80000574]:sw tp, 64(ra)
	-[0x80000578]:sw t0, 72(ra)
	-[0x8000057c]:sw tp, 80(ra)
	-[0x80000580]:sw a7, 88(ra)
Current Store : [0x80000580] : sw a7, 88(ra) -- Store: [0x80004b20]:0x00000063




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmul.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
Current Store : [0x800005c8] : sw t6, 104(ra) -- Store: [0x80004b30]:0x41D8AFD6




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmul.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
Current Store : [0x800005cc] : sw t5, 112(ra) -- Store: [0x80004b38]:0x8A1EDE0E




Last Coverpoint : ['rs1 : x2', 'fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmul.d t5, sp, t3, dyn
	-[0x800005c0]:csrrs a7, fcsr, zero
	-[0x800005c4]:sw t5, 96(ra)
	-[0x800005c8]:sw t6, 104(ra)
	-[0x800005cc]:sw t5, 112(ra)
	-[0x800005d0]:sw a7, 120(ra)
Current Store : [0x800005d0] : sw a7, 120(ra) -- Store: [0x80004b40]:0x00000083




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fmul.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
Current Store : [0x80000618] : sw t6, 136(ra) -- Store: [0x80004b50]:0x41D8AFD6




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fmul.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
Current Store : [0x8000061c] : sw t5, 144(ra) -- Store: [0x80004b58]:0x00000000




Last Coverpoint : ['rs2 : x4', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000060c]:fmul.d t5, t3, tp, dyn
	-[0x80000610]:csrrs a7, fcsr, zero
	-[0x80000614]:sw t5, 128(ra)
	-[0x80000618]:sw t6, 136(ra)
	-[0x8000061c]:sw t5, 144(ra)
	-[0x80000620]:sw a7, 152(ra)
Current Store : [0x80000620] : sw a7, 152(ra) -- Store: [0x80004b60]:0x00000003




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
Current Store : [0x80000668] : sw gp, 168(ra) -- Store: [0x80004b70]:0x39A35544




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
Current Store : [0x8000066c] : sw sp, 176(ra) -- Store: [0x80004b78]:0xFFFFFFFF




Last Coverpoint : ['rd : x2', 'fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmul.d sp, t5, t3, dyn
	-[0x80000660]:csrrs a7, fcsr, zero
	-[0x80000664]:sw sp, 160(ra)
	-[0x80000668]:sw gp, 168(ra)
	-[0x8000066c]:sw sp, 176(ra)
	-[0x80000670]:sw a7, 184(ra)
Current Store : [0x80000670] : sw a7, 184(ra) -- Store: [0x80004b80]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fmul.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
Current Store : [0x800006b8] : sw t6, 200(ra) -- Store: [0x80004b90]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fmul.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
Current Store : [0x800006bc] : sw t5, 208(ra) -- Store: [0x80004b98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ac]:fmul.d t5, t3, s10, dyn
	-[0x800006b0]:csrrs a7, fcsr, zero
	-[0x800006b4]:sw t5, 192(ra)
	-[0x800006b8]:sw t6, 200(ra)
	-[0x800006bc]:sw t5, 208(ra)
	-[0x800006c0]:sw a7, 216(ra)
Current Store : [0x800006c0] : sw a7, 216(ra) -- Store: [0x80004ba0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmul.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
Current Store : [0x80000708] : sw t6, 232(ra) -- Store: [0x80004bb0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmul.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
Current Store : [0x8000070c] : sw t5, 240(ra) -- Store: [0x80004bb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmul.d t5, t3, s10, dyn
	-[0x80000700]:csrrs a7, fcsr, zero
	-[0x80000704]:sw t5, 224(ra)
	-[0x80000708]:sw t6, 232(ra)
	-[0x8000070c]:sw t5, 240(ra)
	-[0x80000710]:sw a7, 248(ra)
Current Store : [0x80000710] : sw a7, 248(ra) -- Store: [0x80004bc0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
Current Store : [0x80000758] : sw t6, 264(ra) -- Store: [0x80004bd0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
Current Store : [0x8000075c] : sw t5, 272(ra) -- Store: [0x80004bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x26b31120173aa and fs2 == 0 and fe2 == 0x400 and fm2 == 0xbcc3d3deff473 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmul.d t5, t3, s10, dyn
	-[0x80000750]:csrrs a7, fcsr, zero
	-[0x80000754]:sw t5, 256(ra)
	-[0x80000758]:sw t6, 264(ra)
	-[0x8000075c]:sw t5, 272(ra)
	-[0x80000760]:sw a7, 280(ra)
Current Store : [0x80000760] : sw a7, 280(ra) -- Store: [0x80004be0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
Current Store : [0x800007a8] : sw t6, 296(ra) -- Store: [0x80004bf0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
Current Store : [0x800007ac] : sw t5, 304(ra) -- Store: [0x80004bf8]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fmul.d t5, t3, s10, dyn
	-[0x800007a0]:csrrs a7, fcsr, zero
	-[0x800007a4]:sw t5, 288(ra)
	-[0x800007a8]:sw t6, 296(ra)
	-[0x800007ac]:sw t5, 304(ra)
	-[0x800007b0]:sw a7, 312(ra)
Current Store : [0x800007b0] : sw a7, 312(ra) -- Store: [0x80004c00]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmul.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
Current Store : [0x800007f8] : sw t6, 328(ra) -- Store: [0x80004c10]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmul.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
Current Store : [0x800007fc] : sw t5, 336(ra) -- Store: [0x80004c18]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmul.d t5, t3, s10, dyn
	-[0x800007f0]:csrrs a7, fcsr, zero
	-[0x800007f4]:sw t5, 320(ra)
	-[0x800007f8]:sw t6, 328(ra)
	-[0x800007fc]:sw t5, 336(ra)
	-[0x80000800]:sw a7, 344(ra)
Current Store : [0x80000800] : sw a7, 344(ra) -- Store: [0x80004c20]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fmul.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
Current Store : [0x80000848] : sw t6, 360(ra) -- Store: [0x80004c30]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fmul.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
Current Store : [0x8000084c] : sw t5, 368(ra) -- Store: [0x80004c38]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000083c]:fmul.d t5, t3, s10, dyn
	-[0x80000840]:csrrs a7, fcsr, zero
	-[0x80000844]:sw t5, 352(ra)
	-[0x80000848]:sw t6, 360(ra)
	-[0x8000084c]:sw t5, 368(ra)
	-[0x80000850]:sw a7, 376(ra)
Current Store : [0x80000850] : sw a7, 376(ra) -- Store: [0x80004c40]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmul.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
Current Store : [0x80000898] : sw t6, 392(ra) -- Store: [0x80004c50]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmul.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
Current Store : [0x8000089c] : sw t5, 400(ra) -- Store: [0x80004c58]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmul.d t5, t3, s10, dyn
	-[0x80000890]:csrrs a7, fcsr, zero
	-[0x80000894]:sw t5, 384(ra)
	-[0x80000898]:sw t6, 392(ra)
	-[0x8000089c]:sw t5, 400(ra)
	-[0x800008a0]:sw a7, 408(ra)
Current Store : [0x800008a0] : sw a7, 408(ra) -- Store: [0x80004c60]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fmul.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
Current Store : [0x800008e8] : sw t6, 424(ra) -- Store: [0x80004c70]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fmul.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
Current Store : [0x800008ec] : sw t5, 432(ra) -- Store: [0x80004c78]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x6dd8b031ef3ab and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xf8389087562f6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008dc]:fmul.d t5, t3, s10, dyn
	-[0x800008e0]:csrrs a7, fcsr, zero
	-[0x800008e4]:sw t5, 416(ra)
	-[0x800008e8]:sw t6, 424(ra)
	-[0x800008ec]:sw t5, 432(ra)
	-[0x800008f0]:sw a7, 440(ra)
Current Store : [0x800008f0] : sw a7, 440(ra) -- Store: [0x80004c80]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmul.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
Current Store : [0x80000938] : sw t6, 456(ra) -- Store: [0x80004c90]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmul.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
Current Store : [0x8000093c] : sw t5, 464(ra) -- Store: [0x80004c98]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmul.d t5, t3, s10, dyn
	-[0x80000930]:csrrs a7, fcsr, zero
	-[0x80000934]:sw t5, 448(ra)
	-[0x80000938]:sw t6, 456(ra)
	-[0x8000093c]:sw t5, 464(ra)
	-[0x80000940]:sw a7, 472(ra)
Current Store : [0x80000940] : sw a7, 472(ra) -- Store: [0x80004ca0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fmul.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
Current Store : [0x80000988] : sw t6, 488(ra) -- Store: [0x80004cb0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fmul.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
Current Store : [0x8000098c] : sw t5, 496(ra) -- Store: [0x80004cb8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000097c]:fmul.d t5, t3, s10, dyn
	-[0x80000980]:csrrs a7, fcsr, zero
	-[0x80000984]:sw t5, 480(ra)
	-[0x80000988]:sw t6, 488(ra)
	-[0x8000098c]:sw t5, 496(ra)
	-[0x80000990]:sw a7, 504(ra)
Current Store : [0x80000990] : sw a7, 504(ra) -- Store: [0x80004cc0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmul.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
Current Store : [0x800009d8] : sw t6, 520(ra) -- Store: [0x80004cd0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmul.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
Current Store : [0x800009dc] : sw t5, 528(ra) -- Store: [0x80004cd8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmul.d t5, t3, s10, dyn
	-[0x800009d0]:csrrs a7, fcsr, zero
	-[0x800009d4]:sw t5, 512(ra)
	-[0x800009d8]:sw t6, 520(ra)
	-[0x800009dc]:sw t5, 528(ra)
	-[0x800009e0]:sw a7, 536(ra)
Current Store : [0x800009e0] : sw a7, 536(ra) -- Store: [0x80004ce0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
Current Store : [0x80000a28] : sw t6, 552(ra) -- Store: [0x80004cf0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
Current Store : [0x80000a2c] : sw t5, 560(ra) -- Store: [0x80004cf8]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fmul.d t5, t3, s10, dyn
	-[0x80000a20]:csrrs a7, fcsr, zero
	-[0x80000a24]:sw t5, 544(ra)
	-[0x80000a28]:sw t6, 552(ra)
	-[0x80000a2c]:sw t5, 560(ra)
	-[0x80000a30]:sw a7, 568(ra)
Current Store : [0x80000a30] : sw a7, 568(ra) -- Store: [0x80004d00]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmul.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
Current Store : [0x80000a78] : sw t6, 584(ra) -- Store: [0x80004d10]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmul.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
Current Store : [0x80000a7c] : sw t5, 592(ra) -- Store: [0x80004d18]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x91582c3a36f34 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x1e635d34c98fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmul.d t5, t3, s10, dyn
	-[0x80000a70]:csrrs a7, fcsr, zero
	-[0x80000a74]:sw t5, 576(ra)
	-[0x80000a78]:sw t6, 584(ra)
	-[0x80000a7c]:sw t5, 592(ra)
	-[0x80000a80]:sw a7, 600(ra)
Current Store : [0x80000a80] : sw a7, 600(ra) -- Store: [0x80004d20]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fmul.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
Current Store : [0x80000ac8] : sw t6, 616(ra) -- Store: [0x80004d30]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fmul.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
Current Store : [0x80000acc] : sw t5, 624(ra) -- Store: [0x80004d38]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000abc]:fmul.d t5, t3, s10, dyn
	-[0x80000ac0]:csrrs a7, fcsr, zero
	-[0x80000ac4]:sw t5, 608(ra)
	-[0x80000ac8]:sw t6, 616(ra)
	-[0x80000acc]:sw t5, 624(ra)
	-[0x80000ad0]:sw a7, 632(ra)
Current Store : [0x80000ad0] : sw a7, 632(ra) -- Store: [0x80004d40]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmul.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
Current Store : [0x80000b18] : sw t6, 648(ra) -- Store: [0x80004d50]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmul.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
Current Store : [0x80000b1c] : sw t5, 656(ra) -- Store: [0x80004d58]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmul.d t5, t3, s10, dyn
	-[0x80000b10]:csrrs a7, fcsr, zero
	-[0x80000b14]:sw t5, 640(ra)
	-[0x80000b18]:sw t6, 648(ra)
	-[0x80000b1c]:sw t5, 656(ra)
	-[0x80000b20]:sw a7, 664(ra)
Current Store : [0x80000b20] : sw a7, 664(ra) -- Store: [0x80004d60]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fmul.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
Current Store : [0x80000b68] : sw t6, 680(ra) -- Store: [0x80004d70]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fmul.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
	-[0x80000b6c]:sw t5, 688(ra)
Current Store : [0x80000b6c] : sw t5, 688(ra) -- Store: [0x80004d78]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b5c]:fmul.d t5, t3, s10, dyn
	-[0x80000b60]:csrrs a7, fcsr, zero
	-[0x80000b64]:sw t5, 672(ra)
	-[0x80000b68]:sw t6, 680(ra)
	-[0x80000b6c]:sw t5, 688(ra)
	-[0x80000b70]:sw a7, 696(ra)
Current Store : [0x80000b70] : sw a7, 696(ra) -- Store: [0x80004d80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
Current Store : [0x80000bb8] : sw t6, 712(ra) -- Store: [0x80004d90]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
	-[0x80000bbc]:sw t5, 720(ra)
Current Store : [0x80000bbc] : sw t5, 720(ra) -- Store: [0x80004d98]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmul.d t5, t3, s10, dyn
	-[0x80000bb0]:csrrs a7, fcsr, zero
	-[0x80000bb4]:sw t5, 704(ra)
	-[0x80000bb8]:sw t6, 712(ra)
	-[0x80000bbc]:sw t5, 720(ra)
	-[0x80000bc0]:sw a7, 728(ra)
Current Store : [0x80000bc0] : sw a7, 728(ra) -- Store: [0x80004da0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmul.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
Current Store : [0x80000c08] : sw t6, 744(ra) -- Store: [0x80004db0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmul.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
	-[0x80000c0c]:sw t5, 752(ra)
Current Store : [0x80000c0c] : sw t5, 752(ra) -- Store: [0x80004db8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x273ac314740f2 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0x67df170d9b1fd and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fmul.d t5, t3, s10, dyn
	-[0x80000c00]:csrrs a7, fcsr, zero
	-[0x80000c04]:sw t5, 736(ra)
	-[0x80000c08]:sw t6, 744(ra)
	-[0x80000c0c]:sw t5, 752(ra)
	-[0x80000c10]:sw a7, 760(ra)
Current Store : [0x80000c10] : sw a7, 760(ra) -- Store: [0x80004dc0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmul.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
Current Store : [0x80000c58] : sw t6, 776(ra) -- Store: [0x80004dd0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmul.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
	-[0x80000c5c]:sw t5, 784(ra)
Current Store : [0x80000c5c] : sw t5, 784(ra) -- Store: [0x80004dd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmul.d t5, t3, s10, dyn
	-[0x80000c50]:csrrs a7, fcsr, zero
	-[0x80000c54]:sw t5, 768(ra)
	-[0x80000c58]:sw t6, 776(ra)
	-[0x80000c5c]:sw t5, 784(ra)
	-[0x80000c60]:sw a7, 792(ra)
Current Store : [0x80000c60] : sw a7, 792(ra) -- Store: [0x80004de0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
Current Store : [0x80000ca8] : sw t6, 808(ra) -- Store: [0x80004df0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
	-[0x80000cac]:sw t5, 816(ra)
Current Store : [0x80000cac] : sw t5, 816(ra) -- Store: [0x80004df8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c9c]:fmul.d t5, t3, s10, dyn
	-[0x80000ca0]:csrrs a7, fcsr, zero
	-[0x80000ca4]:sw t5, 800(ra)
	-[0x80000ca8]:sw t6, 808(ra)
	-[0x80000cac]:sw t5, 816(ra)
	-[0x80000cb0]:sw a7, 824(ra)
Current Store : [0x80000cb0] : sw a7, 824(ra) -- Store: [0x80004e00]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
Current Store : [0x80000cf8] : sw t6, 840(ra) -- Store: [0x80004e10]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
	-[0x80000cfc]:sw t5, 848(ra)
Current Store : [0x80000cfc] : sw t5, 848(ra) -- Store: [0x80004e18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmul.d t5, t3, s10, dyn
	-[0x80000cf0]:csrrs a7, fcsr, zero
	-[0x80000cf4]:sw t5, 832(ra)
	-[0x80000cf8]:sw t6, 840(ra)
	-[0x80000cfc]:sw t5, 848(ra)
	-[0x80000d00]:sw a7, 856(ra)
Current Store : [0x80000d00] : sw a7, 856(ra) -- Store: [0x80004e20]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fmul.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
Current Store : [0x80000d48] : sw t6, 872(ra) -- Store: [0x80004e30]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fmul.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
	-[0x80000d4c]:sw t5, 880(ra)
Current Store : [0x80000d4c] : sw t5, 880(ra) -- Store: [0x80004e38]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d3c]:fmul.d t5, t3, s10, dyn
	-[0x80000d40]:csrrs a7, fcsr, zero
	-[0x80000d44]:sw t5, 864(ra)
	-[0x80000d48]:sw t6, 872(ra)
	-[0x80000d4c]:sw t5, 880(ra)
	-[0x80000d50]:sw a7, 888(ra)
Current Store : [0x80000d50] : sw a7, 888(ra) -- Store: [0x80004e40]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmul.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
Current Store : [0x80000d98] : sw t6, 904(ra) -- Store: [0x80004e50]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmul.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
	-[0x80000d9c]:sw t5, 912(ra)
Current Store : [0x80000d9c] : sw t5, 912(ra) -- Store: [0x80004e58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x06ee36ec8f9b3 and fs2 == 0 and fe2 == 0x41c and fm2 == 0xf2811a6726f5a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmul.d t5, t3, s10, dyn
	-[0x80000d90]:csrrs a7, fcsr, zero
	-[0x80000d94]:sw t5, 896(ra)
	-[0x80000d98]:sw t6, 904(ra)
	-[0x80000d9c]:sw t5, 912(ra)
	-[0x80000da0]:sw a7, 920(ra)
Current Store : [0x80000da0] : sw a7, 920(ra) -- Store: [0x80004e60]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fmul.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
Current Store : [0x80000de8] : sw t6, 936(ra) -- Store: [0x80004e70]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fmul.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
	-[0x80000dec]:sw t5, 944(ra)
Current Store : [0x80000dec] : sw t5, 944(ra) -- Store: [0x80004e78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ddc]:fmul.d t5, t3, s10, dyn
	-[0x80000de0]:csrrs a7, fcsr, zero
	-[0x80000de4]:sw t5, 928(ra)
	-[0x80000de8]:sw t6, 936(ra)
	-[0x80000dec]:sw t5, 944(ra)
	-[0x80000df0]:sw a7, 952(ra)
Current Store : [0x80000df0] : sw a7, 952(ra) -- Store: [0x80004e80]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
Current Store : [0x80000e38] : sw t6, 968(ra) -- Store: [0x80004e90]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
	-[0x80000e3c]:sw t5, 976(ra)
Current Store : [0x80000e3c] : sw t5, 976(ra) -- Store: [0x80004e98]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmul.d t5, t3, s10, dyn
	-[0x80000e30]:csrrs a7, fcsr, zero
	-[0x80000e34]:sw t5, 960(ra)
	-[0x80000e38]:sw t6, 968(ra)
	-[0x80000e3c]:sw t5, 976(ra)
	-[0x80000e40]:sw a7, 984(ra)
Current Store : [0x80000e40] : sw a7, 984(ra) -- Store: [0x80004ea0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fmul.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
Current Store : [0x80000e88] : sw t6, 1000(ra) -- Store: [0x80004eb0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fmul.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
	-[0x80000e8c]:sw t5, 1008(ra)
Current Store : [0x80000e8c] : sw t5, 1008(ra) -- Store: [0x80004eb8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e7c]:fmul.d t5, t3, s10, dyn
	-[0x80000e80]:csrrs a7, fcsr, zero
	-[0x80000e84]:sw t5, 992(ra)
	-[0x80000e88]:sw t6, 1000(ra)
	-[0x80000e8c]:sw t5, 1008(ra)
	-[0x80000e90]:sw a7, 1016(ra)
Current Store : [0x80000e90] : sw a7, 1016(ra) -- Store: [0x80004ec0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmul.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
Current Store : [0x80000ed8] : sw t6, 1032(ra) -- Store: [0x80004ed0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmul.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
	-[0x80000edc]:sw t5, 1040(ra)
Current Store : [0x80000edc] : sw t5, 1040(ra) -- Store: [0x80004ed8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmul.d t5, t3, s10, dyn
	-[0x80000ed0]:csrrs a7, fcsr, zero
	-[0x80000ed4]:sw t5, 1024(ra)
	-[0x80000ed8]:sw t6, 1032(ra)
	-[0x80000edc]:sw t5, 1040(ra)
	-[0x80000ee0]:sw a7, 1048(ra)
Current Store : [0x80000ee0] : sw a7, 1048(ra) -- Store: [0x80004ee0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fmul.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
Current Store : [0x80000f28] : sw t6, 1064(ra) -- Store: [0x80004ef0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fmul.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
	-[0x80000f2c]:sw t5, 1072(ra)
Current Store : [0x80000f2c] : sw t5, 1072(ra) -- Store: [0x80004ef8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x9298353ee5835 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x459177aa91173 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f1c]:fmul.d t5, t3, s10, dyn
	-[0x80000f20]:csrrs a7, fcsr, zero
	-[0x80000f24]:sw t5, 1056(ra)
	-[0x80000f28]:sw t6, 1064(ra)
	-[0x80000f2c]:sw t5, 1072(ra)
	-[0x80000f30]:sw a7, 1080(ra)
Current Store : [0x80000f30] : sw a7, 1080(ra) -- Store: [0x80004f00]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmul.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
Current Store : [0x80000f78] : sw t6, 1096(ra) -- Store: [0x80004f10]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmul.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
	-[0x80000f7c]:sw t5, 1104(ra)
Current Store : [0x80000f7c] : sw t5, 1104(ra) -- Store: [0x80004f18]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmul.d t5, t3, s10, dyn
	-[0x80000f70]:csrrs a7, fcsr, zero
	-[0x80000f74]:sw t5, 1088(ra)
	-[0x80000f78]:sw t6, 1096(ra)
	-[0x80000f7c]:sw t5, 1104(ra)
	-[0x80000f80]:sw a7, 1112(ra)
Current Store : [0x80000f80] : sw a7, 1112(ra) -- Store: [0x80004f20]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
Current Store : [0x80000fc8] : sw t6, 1128(ra) -- Store: [0x80004f30]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
	-[0x80000fcc]:sw t5, 1136(ra)
Current Store : [0x80000fcc] : sw t5, 1136(ra) -- Store: [0x80004f38]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fbc]:fmul.d t5, t3, s10, dyn
	-[0x80000fc0]:csrrs a7, fcsr, zero
	-[0x80000fc4]:sw t5, 1120(ra)
	-[0x80000fc8]:sw t6, 1128(ra)
	-[0x80000fcc]:sw t5, 1136(ra)
	-[0x80000fd0]:sw a7, 1144(ra)
Current Store : [0x80000fd0] : sw a7, 1144(ra) -- Store: [0x80004f40]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmul.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
Current Store : [0x80001018] : sw t6, 1160(ra) -- Store: [0x80004f50]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmul.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
	-[0x8000101c]:sw t5, 1168(ra)
Current Store : [0x8000101c] : sw t5, 1168(ra) -- Store: [0x80004f58]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmul.d t5, t3, s10, dyn
	-[0x80001010]:csrrs a7, fcsr, zero
	-[0x80001014]:sw t5, 1152(ra)
	-[0x80001018]:sw t6, 1160(ra)
	-[0x8000101c]:sw t5, 1168(ra)
	-[0x80001020]:sw a7, 1176(ra)
Current Store : [0x80001020] : sw a7, 1176(ra) -- Store: [0x80004f60]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fmul.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
Current Store : [0x80001068] : sw t6, 1192(ra) -- Store: [0x80004f70]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fmul.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
	-[0x8000106c]:sw t5, 1200(ra)
Current Store : [0x8000106c] : sw t5, 1200(ra) -- Store: [0x80004f78]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000105c]:fmul.d t5, t3, s10, dyn
	-[0x80001060]:csrrs a7, fcsr, zero
	-[0x80001064]:sw t5, 1184(ra)
	-[0x80001068]:sw t6, 1192(ra)
	-[0x8000106c]:sw t5, 1200(ra)
	-[0x80001070]:sw a7, 1208(ra)
Current Store : [0x80001070] : sw a7, 1208(ra) -- Store: [0x80004f80]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmul.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
Current Store : [0x800010b8] : sw t6, 1224(ra) -- Store: [0x80004f90]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmul.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
	-[0x800010bc]:sw t5, 1232(ra)
Current Store : [0x800010bc] : sw t5, 1232(ra) -- Store: [0x80004f98]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x23b09041be1b8 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x8a0cc321c8999 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmul.d t5, t3, s10, dyn
	-[0x800010b0]:csrrs a7, fcsr, zero
	-[0x800010b4]:sw t5, 1216(ra)
	-[0x800010b8]:sw t6, 1224(ra)
	-[0x800010bc]:sw t5, 1232(ra)
	-[0x800010c0]:sw a7, 1240(ra)
Current Store : [0x800010c0] : sw a7, 1240(ra) -- Store: [0x80004fa0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
Current Store : [0x80001108] : sw t6, 1256(ra) -- Store: [0x80004fb0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
	-[0x8000110c]:sw t5, 1264(ra)
Current Store : [0x8000110c] : sw t5, 1264(ra) -- Store: [0x80004fb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010fc]:fmul.d t5, t3, s10, dyn
	-[0x80001100]:csrrs a7, fcsr, zero
	-[0x80001104]:sw t5, 1248(ra)
	-[0x80001108]:sw t6, 1256(ra)
	-[0x8000110c]:sw t5, 1264(ra)
	-[0x80001110]:sw a7, 1272(ra)
Current Store : [0x80001110] : sw a7, 1272(ra) -- Store: [0x80004fc0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmul.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
Current Store : [0x80001158] : sw t6, 1288(ra) -- Store: [0x80004fd0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmul.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
	-[0x8000115c]:sw t5, 1296(ra)
Current Store : [0x8000115c] : sw t5, 1296(ra) -- Store: [0x80004fd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmul.d t5, t3, s10, dyn
	-[0x80001150]:csrrs a7, fcsr, zero
	-[0x80001154]:sw t5, 1280(ra)
	-[0x80001158]:sw t6, 1288(ra)
	-[0x8000115c]:sw t5, 1296(ra)
	-[0x80001160]:sw a7, 1304(ra)
Current Store : [0x80001160] : sw a7, 1304(ra) -- Store: [0x80004fe0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmul.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
Current Store : [0x800011a8] : sw t6, 1320(ra) -- Store: [0x80004ff0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmul.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
	-[0x800011ac]:sw t5, 1328(ra)
Current Store : [0x800011ac] : sw t5, 1328(ra) -- Store: [0x80004ff8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fmul.d t5, t3, s10, dyn
	-[0x800011a0]:csrrs a7, fcsr, zero
	-[0x800011a4]:sw t5, 1312(ra)
	-[0x800011a8]:sw t6, 1320(ra)
	-[0x800011ac]:sw t5, 1328(ra)
	-[0x800011b0]:sw a7, 1336(ra)
Current Store : [0x800011b0] : sw a7, 1336(ra) -- Store: [0x80005000]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmul.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
Current Store : [0x800011f8] : sw t6, 1352(ra) -- Store: [0x80005010]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmul.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
	-[0x800011fc]:sw t5, 1360(ra)
Current Store : [0x800011fc] : sw t5, 1360(ra) -- Store: [0x80005018]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmul.d t5, t3, s10, dyn
	-[0x800011f0]:csrrs a7, fcsr, zero
	-[0x800011f4]:sw t5, 1344(ra)
	-[0x800011f8]:sw t6, 1352(ra)
	-[0x800011fc]:sw t5, 1360(ra)
	-[0x80001200]:sw a7, 1368(ra)
Current Store : [0x80001200] : sw a7, 1368(ra) -- Store: [0x80005020]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
Current Store : [0x80001248] : sw t6, 1384(ra) -- Store: [0x80005030]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
	-[0x8000124c]:sw t5, 1392(ra)
Current Store : [0x8000124c] : sw t5, 1392(ra) -- Store: [0x80005038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x32702a17a3a9e and fs2 == 0 and fe2 == 0x401 and fm2 == 0xabba390e36945 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000123c]:fmul.d t5, t3, s10, dyn
	-[0x80001240]:csrrs a7, fcsr, zero
	-[0x80001244]:sw t5, 1376(ra)
	-[0x80001248]:sw t6, 1384(ra)
	-[0x8000124c]:sw t5, 1392(ra)
	-[0x80001250]:sw a7, 1400(ra)
Current Store : [0x80001250] : sw a7, 1400(ra) -- Store: [0x80005040]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
Current Store : [0x80001298] : sw t6, 1416(ra) -- Store: [0x80005050]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
	-[0x8000129c]:sw t5, 1424(ra)
Current Store : [0x8000129c] : sw t5, 1424(ra) -- Store: [0x80005058]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmul.d t5, t3, s10, dyn
	-[0x80001290]:csrrs a7, fcsr, zero
	-[0x80001294]:sw t5, 1408(ra)
	-[0x80001298]:sw t6, 1416(ra)
	-[0x8000129c]:sw t5, 1424(ra)
	-[0x800012a0]:sw a7, 1432(ra)
Current Store : [0x800012a0] : sw a7, 1432(ra) -- Store: [0x80005060]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fmul.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
Current Store : [0x800012e8] : sw t6, 1448(ra) -- Store: [0x80005070]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fmul.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
	-[0x800012ec]:sw t5, 1456(ra)
Current Store : [0x800012ec] : sw t5, 1456(ra) -- Store: [0x80005078]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012dc]:fmul.d t5, t3, s10, dyn
	-[0x800012e0]:csrrs a7, fcsr, zero
	-[0x800012e4]:sw t5, 1440(ra)
	-[0x800012e8]:sw t6, 1448(ra)
	-[0x800012ec]:sw t5, 1456(ra)
	-[0x800012f0]:sw a7, 1464(ra)
Current Store : [0x800012f0] : sw a7, 1464(ra) -- Store: [0x80005080]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmul.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
Current Store : [0x80001338] : sw t6, 1480(ra) -- Store: [0x80005090]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmul.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
	-[0x8000133c]:sw t5, 1488(ra)
Current Store : [0x8000133c] : sw t5, 1488(ra) -- Store: [0x80005098]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmul.d t5, t3, s10, dyn
	-[0x80001330]:csrrs a7, fcsr, zero
	-[0x80001334]:sw t5, 1472(ra)
	-[0x80001338]:sw t6, 1480(ra)
	-[0x8000133c]:sw t5, 1488(ra)
	-[0x80001340]:sw a7, 1496(ra)
Current Store : [0x80001340] : sw a7, 1496(ra) -- Store: [0x800050a0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
Current Store : [0x80001388] : sw t6, 1512(ra) -- Store: [0x800050b0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
	-[0x8000138c]:sw t5, 1520(ra)
Current Store : [0x8000138c] : sw t5, 1520(ra) -- Store: [0x800050b8]:0x86A12B9B




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000137c]:fmul.d t5, t3, s10, dyn
	-[0x80001380]:csrrs a7, fcsr, zero
	-[0x80001384]:sw t5, 1504(ra)
	-[0x80001388]:sw t6, 1512(ra)
	-[0x8000138c]:sw t5, 1520(ra)
	-[0x80001390]:sw a7, 1528(ra)
Current Store : [0x80001390] : sw a7, 1528(ra) -- Store: [0x800050c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmul.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
Current Store : [0x800013d8] : sw t6, 1544(ra) -- Store: [0x800050d0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmul.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
	-[0x800013dc]:sw t5, 1552(ra)
Current Store : [0x800013dc] : sw t5, 1552(ra) -- Store: [0x800050d8]:0x86A12B9A




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x19cb2b7a32264 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x474f387d7bc5d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmul.d t5, t3, s10, dyn
	-[0x800013d0]:csrrs a7, fcsr, zero
	-[0x800013d4]:sw t5, 1536(ra)
	-[0x800013d8]:sw t6, 1544(ra)
	-[0x800013dc]:sw t5, 1552(ra)
	-[0x800013e0]:sw a7, 1560(ra)
Current Store : [0x800013e0] : sw a7, 1560(ra) -- Store: [0x800050e0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fmul.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
Current Store : [0x80001428] : sw t6, 1576(ra) -- Store: [0x800050f0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fmul.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
	-[0x8000142c]:sw t5, 1584(ra)
Current Store : [0x8000142c] : sw t5, 1584(ra) -- Store: [0x800050f8]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000141c]:fmul.d t5, t3, s10, dyn
	-[0x80001420]:csrrs a7, fcsr, zero
	-[0x80001424]:sw t5, 1568(ra)
	-[0x80001428]:sw t6, 1576(ra)
	-[0x8000142c]:sw t5, 1584(ra)
	-[0x80001430]:sw a7, 1592(ra)
Current Store : [0x80001430] : sw a7, 1592(ra) -- Store: [0x80005100]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmul.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
Current Store : [0x80001478] : sw t6, 1608(ra) -- Store: [0x80005110]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmul.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
	-[0x8000147c]:sw t5, 1616(ra)
Current Store : [0x8000147c] : sw t5, 1616(ra) -- Store: [0x80005118]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmul.d t5, t3, s10, dyn
	-[0x80001470]:csrrs a7, fcsr, zero
	-[0x80001474]:sw t5, 1600(ra)
	-[0x80001478]:sw t6, 1608(ra)
	-[0x8000147c]:sw t5, 1616(ra)
	-[0x80001480]:sw a7, 1624(ra)
Current Store : [0x80001480] : sw a7, 1624(ra) -- Store: [0x80005120]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
Current Store : [0x800014c8] : sw t6, 1640(ra) -- Store: [0x80005130]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
	-[0x800014cc]:sw t5, 1648(ra)
Current Store : [0x800014cc] : sw t5, 1648(ra) -- Store: [0x80005138]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.d t5, t3, s10, dyn
	-[0x800014c0]:csrrs a7, fcsr, zero
	-[0x800014c4]:sw t5, 1632(ra)
	-[0x800014c8]:sw t6, 1640(ra)
	-[0x800014cc]:sw t5, 1648(ra)
	-[0x800014d0]:sw a7, 1656(ra)
Current Store : [0x800014d0] : sw a7, 1656(ra) -- Store: [0x80005140]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmul.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
Current Store : [0x80001518] : sw t6, 1672(ra) -- Store: [0x80005150]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmul.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
	-[0x8000151c]:sw t5, 1680(ra)
Current Store : [0x8000151c] : sw t5, 1680(ra) -- Store: [0x80005158]:0x8A1EDE0E




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmul.d t5, t3, s10, dyn
	-[0x80001510]:csrrs a7, fcsr, zero
	-[0x80001514]:sw t5, 1664(ra)
	-[0x80001518]:sw t6, 1672(ra)
	-[0x8000151c]:sw t5, 1680(ra)
	-[0x80001520]:sw a7, 1688(ra)
Current Store : [0x80001520] : sw a7, 1688(ra) -- Store: [0x80005160]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
Current Store : [0x80001568] : sw t6, 1704(ra) -- Store: [0x80005170]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
	-[0x8000156c]:sw t5, 1712(ra)
Current Store : [0x8000156c] : sw t5, 1712(ra) -- Store: [0x80005178]:0x8A1EDE0D




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39b and fm1 == 0x2849fda388473 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x83eed77face58 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.d t5, t3, s10, dyn
	-[0x80001560]:csrrs a7, fcsr, zero
	-[0x80001564]:sw t5, 1696(ra)
	-[0x80001568]:sw t6, 1704(ra)
	-[0x8000156c]:sw t5, 1712(ra)
	-[0x80001570]:sw a7, 1720(ra)
Current Store : [0x80001570] : sw a7, 1720(ra) -- Store: [0x80005180]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmul.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
Current Store : [0x800015b8] : sw t6, 1736(ra) -- Store: [0x80005190]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmul.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
	-[0x800015bc]:sw t5, 1744(ra)
Current Store : [0x800015bc] : sw t5, 1744(ra) -- Store: [0x80005198]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmul.d t5, t3, s10, dyn
	-[0x800015b0]:csrrs a7, fcsr, zero
	-[0x800015b4]:sw t5, 1728(ra)
	-[0x800015b8]:sw t6, 1736(ra)
	-[0x800015bc]:sw t5, 1744(ra)
	-[0x800015c0]:sw a7, 1752(ra)
Current Store : [0x800015c0] : sw a7, 1752(ra) -- Store: [0x800051a0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
Current Store : [0x80001608] : sw t6, 1768(ra) -- Store: [0x800051b0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
	-[0x8000160c]:sw t5, 1776(ra)
Current Store : [0x8000160c] : sw t5, 1776(ra) -- Store: [0x800051b8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.d t5, t3, s10, dyn
	-[0x80001600]:csrrs a7, fcsr, zero
	-[0x80001604]:sw t5, 1760(ra)
	-[0x80001608]:sw t6, 1768(ra)
	-[0x8000160c]:sw t5, 1776(ra)
	-[0x80001610]:sw a7, 1784(ra)
Current Store : [0x80001610] : sw a7, 1784(ra) -- Store: [0x800051c0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmul.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
Current Store : [0x80001658] : sw t6, 1800(ra) -- Store: [0x800051d0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmul.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
	-[0x8000165c]:sw t5, 1808(ra)
Current Store : [0x8000165c] : sw t5, 1808(ra) -- Store: [0x800051d8]:0xE5795FA5




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmul.d t5, t3, s10, dyn
	-[0x80001650]:csrrs a7, fcsr, zero
	-[0x80001654]:sw t5, 1792(ra)
	-[0x80001658]:sw t6, 1800(ra)
	-[0x8000165c]:sw t5, 1808(ra)
	-[0x80001660]:sw a7, 1816(ra)
Current Store : [0x80001660] : sw a7, 1816(ra) -- Store: [0x800051e0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
Current Store : [0x800016a8] : sw t6, 1832(ra) -- Store: [0x800051f0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
	-[0x800016ac]:sw t5, 1840(ra)
Current Store : [0x800016ac] : sw t5, 1840(ra) -- Store: [0x800051f8]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.d t5, t3, s10, dyn
	-[0x800016a0]:csrrs a7, fcsr, zero
	-[0x800016a4]:sw t5, 1824(ra)
	-[0x800016a8]:sw t6, 1832(ra)
	-[0x800016ac]:sw t5, 1840(ra)
	-[0x800016b0]:sw a7, 1848(ra)
Current Store : [0x800016b0] : sw a7, 1848(ra) -- Store: [0x80005200]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmul.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
Current Store : [0x800016f8] : sw t6, 1864(ra) -- Store: [0x80005210]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmul.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
	-[0x800016fc]:sw t5, 1872(ra)
Current Store : [0x800016fc] : sw t5, 1872(ra) -- Store: [0x80005218]:0xE5795FA6




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x3598d7f9e9c57 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0x572bbcc5ce5ad and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmul.d t5, t3, s10, dyn
	-[0x800016f0]:csrrs a7, fcsr, zero
	-[0x800016f4]:sw t5, 1856(ra)
	-[0x800016f8]:sw t6, 1864(ra)
	-[0x800016fc]:sw t5, 1872(ra)
	-[0x80001700]:sw a7, 1880(ra)
Current Store : [0x80001700] : sw a7, 1880(ra) -- Store: [0x80005220]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
Current Store : [0x80001748] : sw t6, 1896(ra) -- Store: [0x80005230]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
	-[0x8000174c]:sw t5, 1904(ra)
Current Store : [0x8000174c] : sw t5, 1904(ra) -- Store: [0x80005238]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.d t5, t3, s10, dyn
	-[0x80001740]:csrrs a7, fcsr, zero
	-[0x80001744]:sw t5, 1888(ra)
	-[0x80001748]:sw t6, 1896(ra)
	-[0x8000174c]:sw t5, 1904(ra)
	-[0x80001750]:sw a7, 1912(ra)
Current Store : [0x80001750] : sw a7, 1912(ra) -- Store: [0x80005240]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fmul.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
Current Store : [0x80001798] : sw t6, 1928(ra) -- Store: [0x80005250]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fmul.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
	-[0x8000179c]:sw t5, 1936(ra)
Current Store : [0x8000179c] : sw t5, 1936(ra) -- Store: [0x80005258]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fmul.d t5, t3, s10, dyn
	-[0x80001790]:csrrs a7, fcsr, zero
	-[0x80001794]:sw t5, 1920(ra)
	-[0x80001798]:sw t6, 1928(ra)
	-[0x8000179c]:sw t5, 1936(ra)
	-[0x800017a0]:sw a7, 1944(ra)
Current Store : [0x800017a0] : sw a7, 1944(ra) -- Store: [0x80005260]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
Current Store : [0x800017e8] : sw t6, 1960(ra) -- Store: [0x80005270]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
	-[0x800017ec]:sw t5, 1968(ra)
Current Store : [0x800017ec] : sw t5, 1968(ra) -- Store: [0x80005278]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.d t5, t3, s10, dyn
	-[0x800017e0]:csrrs a7, fcsr, zero
	-[0x800017e4]:sw t5, 1952(ra)
	-[0x800017e8]:sw t6, 1960(ra)
	-[0x800017ec]:sw t5, 1968(ra)
	-[0x800017f0]:sw a7, 1976(ra)
Current Store : [0x800017f0] : sw a7, 1976(ra) -- Store: [0x80005280]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmul.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
Current Store : [0x80001838] : sw t6, 1992(ra) -- Store: [0x80005290]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmul.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
	-[0x8000183c]:sw t5, 2000(ra)
Current Store : [0x8000183c] : sw t5, 2000(ra) -- Store: [0x80005298]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000182c]:fmul.d t5, t3, s10, dyn
	-[0x80001830]:csrrs a7, fcsr, zero
	-[0x80001834]:sw t5, 1984(ra)
	-[0x80001838]:sw t6, 1992(ra)
	-[0x8000183c]:sw t5, 2000(ra)
	-[0x80001840]:sw a7, 2008(ra)
Current Store : [0x80001840] : sw a7, 2008(ra) -- Store: [0x800052a0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
Current Store : [0x80001888] : sw t6, 2024(ra) -- Store: [0x800052b0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
	-[0x8000188c]:sw t5, 2032(ra)
Current Store : [0x8000188c] : sw t5, 2032(ra) -- Store: [0x800052b8]:0xFFFFFFFF




Last Coverpoint : ['fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.d t5, t3, s10, dyn
	-[0x80001880]:csrrs a7, fcsr, zero
	-[0x80001884]:sw t5, 2016(ra)
	-[0x80001888]:sw t6, 2024(ra)
	-[0x8000188c]:sw t5, 2032(ra)
	-[0x80001890]:sw a7, 2040(ra)
Current Store : [0x80001890] : sw a7, 2040(ra) -- Store: [0x800052c0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fmul.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
Current Store : [0x800018dc] : sw t6, 16(ra) -- Store: [0x800052d0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fmul.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
	-[0x800018e0]:sw t5, 24(ra)
Current Store : [0x800018e0] : sw t5, 24(ra) -- Store: [0x800052d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018cc]:fmul.d t5, t3, s10, dyn
	-[0x800018d0]:csrrs a7, fcsr, zero
	-[0x800018d4]:addi ra, ra, 2040
	-[0x800018d8]:sw t5, 8(ra)
	-[0x800018dc]:sw t6, 16(ra)
	-[0x800018e0]:sw t5, 24(ra)
	-[0x800018e4]:sw a7, 32(ra)
Current Store : [0x800018e4] : sw a7, 32(ra) -- Store: [0x800052e0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fmul.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
Current Store : [0x8000192c] : sw t6, 48(ra) -- Store: [0x800052f0]:0x39B26B31




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fmul.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
	-[0x80001930]:sw t5, 56(ra)
Current Store : [0x80001930] : sw t5, 56(ra) -- Store: [0x800052f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001920]:fmul.d t5, t3, s10, dyn
	-[0x80001924]:csrrs a7, fcsr, zero
	-[0x80001928]:sw t5, 40(ra)
	-[0x8000192c]:sw t6, 48(ra)
	-[0x80001930]:sw t5, 56(ra)
	-[0x80001934]:sw a7, 64(ra)
Current Store : [0x80001934] : sw a7, 64(ra) -- Store: [0x80005300]:0x00000023





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
