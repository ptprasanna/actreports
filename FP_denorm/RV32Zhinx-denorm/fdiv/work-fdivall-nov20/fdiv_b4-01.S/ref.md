
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001350')]      |
| SIG_REGION                | [('0x80003610', '0x80003aa0', '292 words')]      |
| COV_LABELS                | fdiv_b4      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx-denorm/work-fdivall-nov20/fdiv_b4-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 290      |
| STAT1                     | 144      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 145     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001334]:fdiv.h t6, t5, t4, dyn
      [0x80001338]:csrrs a2, fcsr, zero
      [0x8000133c]:sw t6, 952(fp)
 -- Signature Addresses:
      Address: 0x80003a94 Data: 0x00007BFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000124]:fdiv.h t6, t5, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003618]:0x00000002




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.h t5, t6, t4, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80003620]:0x00000023




Last Coverpoint : ['rs1 : x29', 'rs2 : x31', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000164]:fdiv.h t4, t4, t6, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t4, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003628]:0x00000043




Last Coverpoint : ['rs1 : x27', 'rs2 : x28', 'rd : x28', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.h t3, s11, t3, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80003630]:0x00000063




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x26', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001a4]:fdiv.h s10, s10, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s10, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003638]:0x00000082




Last Coverpoint : ['rs1 : x28', 'rs2 : x25', 'rd : x27', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.h s11, t3, s9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s11, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80003640]:0x00000003




Last Coverpoint : ['rs1 : x24', 'rs2 : x27', 'rd : x25', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.h s9, s8, s11, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003648]:0x00000023




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.h s8, s9, s7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80003650]:0x00000043




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.h s7, s6, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003658]:0x00000063




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.h s6, s7, s5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80003660]:0x00000083




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.h s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003668]:0x00000003




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.h s4, s5, s3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80003670]:0x00000023




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.h s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003678]:0x00000043




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.h s2, s3, a7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80003680]:0x00000063




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.h a7, a6, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003688]:0x00000083




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.h a6, a7, a5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80003690]:0x00000007




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.h a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003698]:0x00000027




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.h a4, a5, a3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800036a0]:0x00000047




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.h a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800036a8]:0x00000067




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.h a2, a3, a1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800036b0]:0x00000087




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.h a1, a0, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800036b8]:0x00000003




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.h a0, a1, s1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800036c0]:0x00000023




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.h s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800036c8]:0x00000043




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.h fp, s1, t2, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800036d0]:0x00000063




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.h t2, t1, fp, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800036d8]:0x00000083




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.h t1, t2, t0, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800036e0]:0x00000003




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.h t0, tp, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800036e8]:0x00000023




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.h tp, t0, gp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800036f0]:0x00000043




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.h gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800036f8]:0x00000063




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.h sp, gp, ra, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x80003700]:0x00000083




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.h ra, zero, sp, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80003708]:0x00000002




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000514]:fdiv.h t6, ra, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x80003710]:0x00000023




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000534]:fdiv.h t6, t5, zero, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80003718]:0x0000004A




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.h zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x80003720]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.h t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80003728]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.h t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x80003730]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.h t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80003738]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.h t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x80003740]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.h t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80003748]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.h t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x80003750]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.h t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80003758]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.h t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x80003760]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.h t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80003768]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.h t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x80003770]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.h t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80003778]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.h t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x80003780]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.h t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80003788]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.h t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x80003790]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.h t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80003798]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.h t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x800037a0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.h t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800037a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.h t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800037b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.h t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800037b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.h t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800037c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.h t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800037c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.h t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800037d0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.h t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800037d8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.h t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800037e0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.h t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800037e8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.h t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800037f0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.h t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800037f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.h t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x80003800]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.h t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80003808]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.h t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x80003810]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.h t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80003818]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.h t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x80003820]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.h t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80003828]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.h t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x80003830]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.h t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80003838]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.h t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x80003840]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.h t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80003848]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.h t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x80003850]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.h t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80003858]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.h t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x80003860]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.h t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80003868]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.h t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x80003870]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80003878]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x80003880]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.h t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80003888]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.h t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x80003890]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.h t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80003898]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.h t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x800038a0]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.h t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800038a8]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.h t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800038b0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800038b8]:0x00000082




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800038c0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800038c8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.h t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800038d0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.h t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800038d8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.h t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800038e0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.h t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800038e8]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.h t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800038f0]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800038f8]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x80003900]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80003908]:0x00000082




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.h t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x80003910]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.h t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80003918]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.h t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x80003920]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.h t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80003928]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.h t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x80003930]:0x00000082




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.h t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80003938]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x80003940]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.h t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80003948]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.h t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x80003950]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.h t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80003958]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.h t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x80003960]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.h t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80003968]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.h t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x80003970]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80003978]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x80003980]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80003988]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f14]:fdiv.h t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x80003990]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f34]:fdiv.h t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80003998]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f54]:fdiv.h t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x800039a0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f74]:fdiv.h t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x800039a8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f94]:fdiv.h t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x800039b0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x800039b8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x800039c0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x800039c8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001014]:fdiv.h t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x800039d0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001034]:fdiv.h t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x800039d8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001054]:fdiv.h t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x800039e0]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001074]:fdiv.h t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x800039e8]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001094]:fdiv.h t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x800039f0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010b4]:fdiv.h t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x800039f8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010d4]:fdiv.h t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x80003a00]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010f4]:fdiv.h t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80003a08]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001114]:fdiv.h t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x80003a10]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001134]:fdiv.h t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80003a18]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001154]:fdiv.h t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x80003a20]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001174]:fdiv.h t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80003a28]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001194]:fdiv.h t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x80003a30]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011b4]:fdiv.h t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80003a38]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011d4]:fdiv.h t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x80003a40]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011f4]:fdiv.h t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80003a48]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001214]:fdiv.h t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x80003a50]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001234]:fdiv.h t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80003a58]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001254]:fdiv.h t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x80003a60]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001274]:fdiv.h t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80003a68]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001294]:fdiv.h t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x80003a70]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012b4]:fdiv.h t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80003a78]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012d4]:fdiv.h t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x80003a80]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012f4]:fdiv.h t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80003a88]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001314]:fdiv.h t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x80003a90]:0x00000043




Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001334]:fdiv.h t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80003a98]:0x00000063





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                          |                                                      code                                                       |
|---:|-------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80003614]<br>0x00003C00<br> |- mnemonic : fdiv.h<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                  |[0x80000124]:fdiv.h t6, t5, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000361c]<br>0x00007BFE<br> |- rs1 : x31<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000144]:fdiv.h t5, t6, t4, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 8(ra)<br>      |
|   3|[0x80003624]<br>0x00007BFE<br> |- rs1 : x29<br> - rs2 : x31<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000164]:fdiv.h t4, t4, t6, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t4, 16(ra)<br>     |
|   4|[0x8000362c]<br>0x00007BFF<br> |- rs1 : x27<br> - rs2 : x28<br> - rd : x28<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000184]:fdiv.h t3, s11, t3, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>    |
|   5|[0x80003634]<br>0x00003C00<br> |- rs1 : x26<br> - rs2 : x26<br> - rd : x26<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                          |[0x800001a4]:fdiv.h s10, s10, s10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s10, 32(ra)<br> |
|   6|[0x8000363c]<br>0xFFFFFBFF<br> |- rs1 : x28<br> - rs2 : x25<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fdiv.h s11, t3, s9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s11, 40(ra)<br>   |
|   7|[0x80003644]<br>0xFFFFFBFE<br> |- rs1 : x24<br> - rs2 : x27<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800001e4]:fdiv.h s9, s8, s11, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8000364c]<br>0xFFFFFBFF<br> |- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000204]:fdiv.h s8, s9, s7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80003654]<br>0xFFFFFBFE<br> |- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000224]:fdiv.h s7, s6, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000365c]<br>0xFFFFFBFF<br> |- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1ea and fs2 == 1 and fe2 == 0x0d and fm2 == 0x1eb and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000244]:fdiv.h s6, s7, s5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80003664]<br>0x00007BFE<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fdiv.h s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000366c]<br>0x00007BFE<br> |- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000284]:fdiv.h s4, s5, s3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80003674]<br>0x00007BFE<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002a4]:fdiv.h s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000367c]<br>0x00007BFF<br> |- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002c4]:fdiv.h s2, s3, a7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80003684]<br>0x00007BFE<br> |- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x100 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x101 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002e4]:fdiv.h a7, a6, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000368c]<br>0xFFFFFC00<br> |- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fdiv.h a6, a7, a5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80003694]<br>0xFFFFFBFF<br> |- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000324]:fdiv.h a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000369c]<br>0xFFFFFC00<br> |- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000344]:fdiv.h a4, a5, a3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800036a4]<br>0xFFFFFBFF<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000364]:fdiv.h a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800036ac]<br>0xFFFFFC00<br> |- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x18 and fm1 == 0x2bf and fs2 == 1 and fe2 == 0x08 and fm2 == 0x2bf and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000384]:fdiv.h a2, a3, a1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800036b4]<br>0x00007BFE<br> |- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fdiv.h a1, a0, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800036bc]<br>0x00007BFE<br> |- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003c4]:fdiv.h a0, a1, s1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800036c4]<br>0x00007BFE<br> |- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                 |[0x800003ec]:fdiv.h s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800036cc]<br>0x00007BFF<br> |- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x8000040c]:fdiv.h fp, s1, t2, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800036d4]<br>0x00007BFE<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x025 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x026 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x8000042c]:fdiv.h t2, t1, fp, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800036dc]<br>0xFFFFFBFF<br> |- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000454]:fdiv.h t1, t2, t0, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800036e4]<br>0xFFFFFBFE<br> |- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000474]:fdiv.h t0, tp, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800036ec]<br>0xFFFFFBFF<br> |- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000494]:fdiv.h tp, t0, gp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800036f4]<br>0xFFFFFBFE<br> |- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004b4]:fdiv.h gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800036fc]<br>0xFFFFFBFF<br> |- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f6 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x3f7 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004d4]:fdiv.h sp, gp, ra, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80003704]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                    |[0x800004f4]:fdiv.h ra, zero, sp, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw ra, 40(fp)<br>   |
|  32|[0x8000370c]<br>0x00007BFE<br> |- rs1 : x1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                               |[0x80000514]:fdiv.h t6, ra, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>     |
|  33|[0x80003714]<br>0x00007C00<br> |- rs2 : x0<br>                                                                                                                                                                                                                                                                                 |[0x80000534]:fdiv.h t6, t5, zero, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>   |
|  34|[0x8000371c]<br>0x00000000<br> |- rd : x0<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000554]:fdiv.h zero, t6, t5, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw zero, 64(fp)<br> |
|  35|[0x80003724]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000574]:fdiv.h t6, t5, t4, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw t6, 72(fp)<br>     |
|  36|[0x8000372c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000594]:fdiv.h t6, t5, t4, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw t6, 80(fp)<br>     |
|  37|[0x80003734]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005b4]:fdiv.h t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x8000373c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005d4]:fdiv.h t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80003744]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005f4]:fdiv.h t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x8000374c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x11c and fs2 == 1 and fe2 == 0x0e and fm2 == 0x11c and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000614]:fdiv.h t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80003754]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000634]:fdiv.h t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x8000375c]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000654]:fdiv.h t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80003764]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000674]:fdiv.h t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x8000376c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000694]:fdiv.h t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80003774]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x113 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x114 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006b4]:fdiv.h t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x8000377c]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006d4]:fdiv.h t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80003784]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006f4]:fdiv.h t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x8000378c]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000714]:fdiv.h t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80003794]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000734]:fdiv.h t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x8000379c]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x02e and fs2 == 1 and fe2 == 0x0e and fm2 == 0x02f and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000754]:fdiv.h t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800037a4]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000774]:fdiv.h t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800037ac]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000794]:fdiv.h t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800037b4]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007b4]:fdiv.h t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800037bc]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007d4]:fdiv.h t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800037c4]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 0 and fe2 == 0x0e and fm2 == 0x15f and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007f4]:fdiv.h t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800037cc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000814]:fdiv.h t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800037d4]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000834]:fdiv.h t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800037dc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000854]:fdiv.h t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800037e4]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000874]:fdiv.h t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x800037ec]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x210 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x210 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000894]:fdiv.h t6, t5, t4, dyn<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x800037f4]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008b4]:fdiv.h t6, t5, t4, dyn<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x800037fc]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008d4]:fdiv.h t6, t5, t4, dyn<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x80003804]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008f4]:fdiv.h t6, t5, t4, dyn<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x8000380c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000914]:fdiv.h t6, t5, t4, dyn<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x80003814]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0d1 and fs2 == 0 and fe2 == 0x0d and fm2 == 0x0d2 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000934]:fdiv.h t6, t5, t4, dyn<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x8000381c]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000954]:fdiv.h t6, t5, t4, dyn<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x80003824]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000974]:fdiv.h t6, t5, t4, dyn<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x8000382c]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000994]:fdiv.h t6, t5, t4, dyn<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x80003834]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009b4]:fdiv.h t6, t5, t4, dyn<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x8000383c]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2eb and fs2 == 1 and fe2 == 0x0e and fm2 == 0x2ec and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009d4]:fdiv.h t6, t5, t4, dyn<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x80003844]<br>0x00006FFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009f4]:fdiv.h t6, t5, t4, dyn<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x8000384c]<br>0x00006FFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a14]:fdiv.h t6, t5, t4, dyn<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x80003854]<br>0x00006FFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a34]:fdiv.h t6, t5, t4, dyn<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x8000385c]<br>0x00006FFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a54]:fdiv.h t6, t5, t4, dyn<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x80003864]<br>0x00006FFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1bf and fs2 == 0 and fe2 == 0x11 and fm2 == 0x1c0 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a74]:fdiv.h t6, t5, t4, dyn<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x8000386c]<br>0xFFFFEFFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a94]:fdiv.h t6, t5, t4, dyn<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x80003874]<br>0xFFFFEFFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ab4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x8000387c]<br>0xFFFFEFFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ad4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x80003884]<br>0xFFFFEFFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000af4]:fdiv.h t6, t5, t4, dyn<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x8000388c]<br>0xFFFFEFFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b5 and fs2 == 1 and fe2 == 0x11 and fm2 == 0x1b6 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b14]:fdiv.h t6, t5, t4, dyn<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x80003894]<br>0x00007400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b34]:fdiv.h t6, t5, t4, dyn<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x8000389c]<br>0x00007400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b54]:fdiv.h t6, t5, t4, dyn<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x800038a4]<br>0x00007400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b74]:fdiv.h t6, t5, t4, dyn<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x800038ac]<br>0x00007400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b94]:fdiv.h t6, t5, t4, dyn<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x800038b4]<br>0x00007400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x251 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x251 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x800038bc]<br>0xFFFFF3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x800038c4]<br>0xFFFFF3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bf4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x800038cc]<br>0xFFFFF3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c14]:fdiv.h t6, t5, t4, dyn<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x800038d4]<br>0xFFFFF3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c34]:fdiv.h t6, t5, t4, dyn<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x800038dc]<br>0xFFFFF3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31c and fs2 == 1 and fe2 == 0x0f and fm2 == 0x31d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c54]:fdiv.h t6, t5, t4, dyn<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x800038e4]<br>0x00007800<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c74]:fdiv.h t6, t5, t4, dyn<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x800038ec]<br>0x00007800<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c94]:fdiv.h t6, t5, t4, dyn<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x800038f4]<br>0x00007800<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x800038fc]<br>0x00007800<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x80003904]<br>0x00007800<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x307 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x307 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cf4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x8000390c]<br>0xFFFFF800<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d14]:fdiv.h t6, t5, t4, dyn<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x80003914]<br>0xFFFFF800<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d34]:fdiv.h t6, t5, t4, dyn<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x8000391c]<br>0xFFFFF800<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d54]:fdiv.h t6, t5, t4, dyn<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x80003924]<br>0xFFFFF800<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d74]:fdiv.h t6, t5, t4, dyn<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x8000392c]<br>0xFFFFF800<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x059 and fs2 == 1 and fe2 == 0x0d and fm2 == 0x059 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d94]:fdiv.h t6, t5, t4, dyn<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x80003934]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000db4]:fdiv.h t6, t5, t4, dyn<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x8000393c]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000dd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x80003944]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000df4]:fdiv.h t6, t5, t4, dyn<br> [0x80000df8]:csrrs a2, fcsr, zero<br> [0x80000dfc]:sw t6, 616(fp)<br>    |
| 104|[0x8000394c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e14]:fdiv.h t6, t5, t4, dyn<br> [0x80000e18]:csrrs a2, fcsr, zero<br> [0x80000e1c]:sw t6, 624(fp)<br>    |
| 105|[0x80003954]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b8 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x3b9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e34]:fdiv.h t6, t5, t4, dyn<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 106|[0x8000395c]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e54]:fdiv.h t6, t5, t4, dyn<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 107|[0x80003964]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e74]:fdiv.h t6, t5, t4, dyn<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 108|[0x8000396c]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e94]:fdiv.h t6, t5, t4, dyn<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
| 109|[0x80003974]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000eb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a2, fcsr, zero<br> [0x80000ebc]:sw t6, 664(fp)<br>    |
| 110|[0x8000397c]<br>0xFFFFFBFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x102 and fs2 == 1 and fe2 == 0x0e and fm2 == 0x103 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ed4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a2, fcsr, zero<br> [0x80000edc]:sw t6, 672(fp)<br>    |
| 111|[0x80003984]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ef4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a2, fcsr, zero<br> [0x80000efc]:sw t6, 680(fp)<br>    |
| 112|[0x8000398c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f14]:fdiv.h t6, t5, t4, dyn<br> [0x80000f18]:csrrs a2, fcsr, zero<br> [0x80000f1c]:sw t6, 688(fp)<br>    |
| 113|[0x80003994]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f34]:fdiv.h t6, t5, t4, dyn<br> [0x80000f38]:csrrs a2, fcsr, zero<br> [0x80000f3c]:sw t6, 696(fp)<br>    |
| 114|[0x8000399c]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f54]:fdiv.h t6, t5, t4, dyn<br> [0x80000f58]:csrrs a2, fcsr, zero<br> [0x80000f5c]:sw t6, 704(fp)<br>    |
| 115|[0x800039a4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x00e and fs2 == 0 and fe2 == 0x0d and fm2 == 0x00e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f74]:fdiv.h t6, t5, t4, dyn<br> [0x80000f78]:csrrs a2, fcsr, zero<br> [0x80000f7c]:sw t6, 712(fp)<br>    |
| 116|[0x800039ac]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f94]:fdiv.h t6, t5, t4, dyn<br> [0x80000f98]:csrrs a2, fcsr, zero<br> [0x80000f9c]:sw t6, 720(fp)<br>    |
| 117|[0x800039b4]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000fb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a2, fcsr, zero<br> [0x80000fbc]:sw t6, 728(fp)<br>    |
| 118|[0x800039bc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000fd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a2, fcsr, zero<br> [0x80000fdc]:sw t6, 736(fp)<br>    |
| 119|[0x800039c4]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ff4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a2, fcsr, zero<br> [0x80000ffc]:sw t6, 744(fp)<br>    |
| 120|[0x800039cc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x19c and fs2 == 1 and fe2 == 0x0c and fm2 == 0x19d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001014]:fdiv.h t6, t5, t4, dyn<br> [0x80001018]:csrrs a2, fcsr, zero<br> [0x8000101c]:sw t6, 752(fp)<br>    |
| 121|[0x800039d4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001034]:fdiv.h t6, t5, t4, dyn<br> [0x80001038]:csrrs a2, fcsr, zero<br> [0x8000103c]:sw t6, 760(fp)<br>    |
| 122|[0x800039dc]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001054]:fdiv.h t6, t5, t4, dyn<br> [0x80001058]:csrrs a2, fcsr, zero<br> [0x8000105c]:sw t6, 768(fp)<br>    |
| 123|[0x800039e4]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001074]:fdiv.h t6, t5, t4, dyn<br> [0x80001078]:csrrs a2, fcsr, zero<br> [0x8000107c]:sw t6, 776(fp)<br>    |
| 124|[0x800039ec]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001094]:fdiv.h t6, t5, t4, dyn<br> [0x80001098]:csrrs a2, fcsr, zero<br> [0x8000109c]:sw t6, 784(fp)<br>    |
| 125|[0x800039f4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ab and fs2 == 0 and fe2 == 0x0c and fm2 == 0x0ac and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800010b4]:fdiv.h t6, t5, t4, dyn<br> [0x800010b8]:csrrs a2, fcsr, zero<br> [0x800010bc]:sw t6, 792(fp)<br>    |
| 126|[0x800039fc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010d4]:fdiv.h t6, t5, t4, dyn<br> [0x800010d8]:csrrs a2, fcsr, zero<br> [0x800010dc]:sw t6, 800(fp)<br>    |
| 127|[0x80003a04]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800010f4]:fdiv.h t6, t5, t4, dyn<br> [0x800010f8]:csrrs a2, fcsr, zero<br> [0x800010fc]:sw t6, 808(fp)<br>    |
| 128|[0x80003a0c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001114]:fdiv.h t6, t5, t4, dyn<br> [0x80001118]:csrrs a2, fcsr, zero<br> [0x8000111c]:sw t6, 816(fp)<br>    |
| 129|[0x80003a14]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001134]:fdiv.h t6, t5, t4, dyn<br> [0x80001138]:csrrs a2, fcsr, zero<br> [0x8000113c]:sw t6, 824(fp)<br>    |
| 130|[0x80003a1c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x174 and fs2 == 1 and fe2 == 0x0c and fm2 == 0x174 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001154]:fdiv.h t6, t5, t4, dyn<br> [0x80001158]:csrrs a2, fcsr, zero<br> [0x8000115c]:sw t6, 832(fp)<br>    |
| 131|[0x80003a24]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001174]:fdiv.h t6, t5, t4, dyn<br> [0x80001178]:csrrs a2, fcsr, zero<br> [0x8000117c]:sw t6, 840(fp)<br>    |
| 132|[0x80003a2c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001194]:fdiv.h t6, t5, t4, dyn<br> [0x80001198]:csrrs a2, fcsr, zero<br> [0x8000119c]:sw t6, 848(fp)<br>    |
| 133|[0x80003a34]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011b4]:fdiv.h t6, t5, t4, dyn<br> [0x800011b8]:csrrs a2, fcsr, zero<br> [0x800011bc]:sw t6, 856(fp)<br>    |
| 134|[0x80003a3c]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011d4]:fdiv.h t6, t5, t4, dyn<br> [0x800011d8]:csrrs a2, fcsr, zero<br> [0x800011dc]:sw t6, 864(fp)<br>    |
| 135|[0x80003a44]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d9 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x2d9 and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011f4]:fdiv.h t6, t5, t4, dyn<br> [0x800011f8]:csrrs a2, fcsr, zero<br> [0x800011fc]:sw t6, 872(fp)<br>    |
| 136|[0x80003a4c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001214]:fdiv.h t6, t5, t4, dyn<br> [0x80001218]:csrrs a2, fcsr, zero<br> [0x8000121c]:sw t6, 880(fp)<br>    |
| 137|[0x80003a54]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x22 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001234]:fdiv.h t6, t5, t4, dyn<br> [0x80001238]:csrrs a2, fcsr, zero<br> [0x8000123c]:sw t6, 888(fp)<br>    |
| 138|[0x80003a5c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001254]:fdiv.h t6, t5, t4, dyn<br> [0x80001258]:csrrs a2, fcsr, zero<br> [0x8000125c]:sw t6, 896(fp)<br>    |
| 139|[0x80003a64]<br>0xFFFFFBFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001274]:fdiv.h t6, t5, t4, dyn<br> [0x80001278]:csrrs a2, fcsr, zero<br> [0x8000127c]:sw t6, 904(fp)<br>    |
| 140|[0x80003a6c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15e and fs2 == 1 and fe2 == 0x0b and fm2 == 0x15e and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001294]:fdiv.h t6, t5, t4, dyn<br> [0x80001298]:csrrs a2, fcsr, zero<br> [0x8000129c]:sw t6, 912(fp)<br>    |
| 141|[0x80003a74]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012b4]:fdiv.h t6, t5, t4, dyn<br> [0x800012b8]:csrrs a2, fcsr, zero<br> [0x800012bc]:sw t6, 920(fp)<br>    |
| 142|[0x80003a7c]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x39c and fs2 == 0 and fe2 == 0x0c and fm2 == 0x39d and fcsr == 0x82 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800012d4]:fdiv.h t6, t5, t4, dyn<br> [0x800012d8]:csrrs a2, fcsr, zero<br> [0x800012dc]:sw t6, 928(fp)<br>    |
| 143|[0x80003a84]<br>0x00007BFF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x2 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012f4]:fdiv.h t6, t5, t4, dyn<br> [0x800012f8]:csrrs a2, fcsr, zero<br> [0x800012fc]:sw t6, 936(fp)<br>    |
| 144|[0x80003a8c]<br>0x00007BFE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b0 and fs2 == 0 and fe2 == 0x0e and fm2 == 0x2b1 and fcsr == 0x42 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001314]:fdiv.h t6, t5, t4, dyn<br> [0x80001318]:csrrs a2, fcsr, zero<br> [0x8000131c]:sw t6, 944(fp)<br>    |
