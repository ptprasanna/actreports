
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000df0')]      |
| SIG_REGION                | [('0x80002510', '0x80002850', '208 words')]      |
| COV_LABELS                | fmul_b7      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx-denorm/work-fmulall-nov20/fmul_b7-01.S/ref.S    |
| Total Number of coverpoints| 199     |
| Total Coverpoints Hit     | 199      |
| Total Signature Updates   | 204      |
| STAT1                     | 100      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 102     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000d94]:fmul.h t6, t5, t4, dyn
      [0x80000d98]:csrrs a3, fcsr, zero
      [0x80000d9c]:sw t6, 600(s1)
 -- Signature Addresses:
      Address: 0x8000282c Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000dd4]:fmul.h t6, t5, t4, dyn
      [0x80000dd8]:csrrs a3, fcsr, zero
      [0x80000ddc]:sw t6, 616(s1)
 -- Signature Addresses:
      Address: 0x8000283c Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x30', 'rd : x31', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000124]:fmul.h t6, t5, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002518]:0x00000067




Last Coverpoint : ['rs1 : x29', 'rs2 : x31', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.h t4, t4, t6, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80002520]:0x00000062




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000164]:fmul.h t3, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t3, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002528]:0x00000067




Last Coverpoint : ['rs1 : x31', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fmul.h s11, t6, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw s11, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80002530]:0x00000062




Last Coverpoint : ['rs1 : x27', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001a4]:fmul.h t5, s11, t4, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw t5, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002538]:0x00000062




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fmul.h s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80002540]:0x00000062




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fmul.h s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002548]:0x00000062




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fmul.h s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80002550]:0x00000062




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fmul.h s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002558]:0x00000062




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fmul.h s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80002560]:0x00000062




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fmul.h s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002568]:0x00000062




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fmul.h s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80002570]:0x00000062




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fmul.h s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002578]:0x00000062




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fmul.h s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80002580]:0x00000062




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fmul.h a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002588]:0x00000062




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.h a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80002590]:0x00000062




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fmul.h a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002598]:0x00000062




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fmul.h a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800025a0]:0x00000062




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fmul.h a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800025a8]:0x00000062




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fmul.h a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800025b0]:0x00000062




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fmul.h a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800025b8]:0x00000062




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.h a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800025c0]:0x00000062




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fmul.h s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800025c8]:0x00000062




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fmul.h fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800025d0]:0x00000062




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000434]:fmul.h t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800025d8]:0x00000062




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:fmul.h t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800025e0]:0x00000062




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.h t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800025e8]:0x00000062




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:fmul.h tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800025f0]:0x00000062




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fmul.h gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800025f8]:0x00000062




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fmul.h sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x80002600]:0x00000062




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fmul.h ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80002608]:0x00000062




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000514]:fmul.h zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x80002610]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000534]:fmul.h t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80002618]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.h t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x80002620]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:fmul.h t6, t5, t4, dyn
	-[0x80000578]:csrrs a3, fcsr, zero
	-[0x8000057c]:sw t6, 80(s1)
Current Store : [0x80000580] : sw a3, 84(s1) -- Store: [0x80002628]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fmul.h t6, t5, t4, dyn
	-[0x80000598]:csrrs a3, fcsr, zero
	-[0x8000059c]:sw t6, 88(s1)
Current Store : [0x800005a0] : sw a3, 92(s1) -- Store: [0x80002630]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fmul.h t6, t5, t4, dyn
	-[0x800005b8]:csrrs a3, fcsr, zero
	-[0x800005bc]:sw t6, 96(s1)
Current Store : [0x800005c0] : sw a3, 100(s1) -- Store: [0x80002638]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:fmul.h t6, t5, t4, dyn
	-[0x800005d8]:csrrs a3, fcsr, zero
	-[0x800005dc]:sw t6, 104(s1)
Current Store : [0x800005e0] : sw a3, 108(s1) -- Store: [0x80002640]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fmul.h t6, t5, t4, dyn
	-[0x800005f8]:csrrs a3, fcsr, zero
	-[0x800005fc]:sw t6, 112(s1)
Current Store : [0x80000600] : sw a3, 116(s1) -- Store: [0x80002648]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fmul.h t6, t5, t4, dyn
	-[0x80000618]:csrrs a3, fcsr, zero
	-[0x8000061c]:sw t6, 120(s1)
Current Store : [0x80000620] : sw a3, 124(s1) -- Store: [0x80002650]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:fmul.h t6, t5, t4, dyn
	-[0x80000638]:csrrs a3, fcsr, zero
	-[0x8000063c]:sw t6, 128(s1)
Current Store : [0x80000640] : sw a3, 132(s1) -- Store: [0x80002658]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:fmul.h t6, t5, t4, dyn
	-[0x80000658]:csrrs a3, fcsr, zero
	-[0x8000065c]:sw t6, 136(s1)
Current Store : [0x80000660] : sw a3, 140(s1) -- Store: [0x80002660]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:fmul.h t6, t5, t4, dyn
	-[0x80000678]:csrrs a3, fcsr, zero
	-[0x8000067c]:sw t6, 144(s1)
Current Store : [0x80000680] : sw a3, 148(s1) -- Store: [0x80002668]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:fmul.h t6, t5, t4, dyn
	-[0x80000698]:csrrs a3, fcsr, zero
	-[0x8000069c]:sw t6, 152(s1)
Current Store : [0x800006a0] : sw a3, 156(s1) -- Store: [0x80002670]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmul.h t6, t5, t4, dyn
	-[0x800006b8]:csrrs a3, fcsr, zero
	-[0x800006bc]:sw t6, 160(s1)
Current Store : [0x800006c0] : sw a3, 164(s1) -- Store: [0x80002678]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fmul.h t6, t5, t4, dyn
	-[0x800006d8]:csrrs a3, fcsr, zero
	-[0x800006dc]:sw t6, 168(s1)
Current Store : [0x800006e0] : sw a3, 172(s1) -- Store: [0x80002680]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fmul.h t6, t5, t4, dyn
	-[0x800006f8]:csrrs a3, fcsr, zero
	-[0x800006fc]:sw t6, 176(s1)
Current Store : [0x80000700] : sw a3, 180(s1) -- Store: [0x80002688]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:fmul.h t6, t5, t4, dyn
	-[0x80000718]:csrrs a3, fcsr, zero
	-[0x8000071c]:sw t6, 184(s1)
Current Store : [0x80000720] : sw a3, 188(s1) -- Store: [0x80002690]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fmul.h t6, t5, t4, dyn
	-[0x80000738]:csrrs a3, fcsr, zero
	-[0x8000073c]:sw t6, 192(s1)
Current Store : [0x80000740] : sw a3, 196(s1) -- Store: [0x80002698]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:fmul.h t6, t5, t4, dyn
	-[0x80000758]:csrrs a3, fcsr, zero
	-[0x8000075c]:sw t6, 200(s1)
Current Store : [0x80000760] : sw a3, 204(s1) -- Store: [0x800026a0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:fmul.h t6, t5, t4, dyn
	-[0x80000778]:csrrs a3, fcsr, zero
	-[0x8000077c]:sw t6, 208(s1)
Current Store : [0x80000780] : sw a3, 212(s1) -- Store: [0x800026a8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:fmul.h t6, t5, t4, dyn
	-[0x80000798]:csrrs a3, fcsr, zero
	-[0x8000079c]:sw t6, 216(s1)
Current Store : [0x800007a0] : sw a3, 220(s1) -- Store: [0x800026b0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:fmul.h t6, t5, t4, dyn
	-[0x800007b8]:csrrs a3, fcsr, zero
	-[0x800007bc]:sw t6, 224(s1)
Current Store : [0x800007c0] : sw a3, 228(s1) -- Store: [0x800026b8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmul.h t6, t5, t4, dyn
	-[0x800007d8]:csrrs a3, fcsr, zero
	-[0x800007dc]:sw t6, 232(s1)
Current Store : [0x800007e0] : sw a3, 236(s1) -- Store: [0x800026c0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fmul.h t6, t5, t4, dyn
	-[0x800007f8]:csrrs a3, fcsr, zero
	-[0x800007fc]:sw t6, 240(s1)
Current Store : [0x80000800] : sw a3, 244(s1) -- Store: [0x800026c8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:fmul.h t6, t5, t4, dyn
	-[0x80000818]:csrrs a3, fcsr, zero
	-[0x8000081c]:sw t6, 248(s1)
Current Store : [0x80000820] : sw a3, 252(s1) -- Store: [0x800026d0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:fmul.h t6, t5, t4, dyn
	-[0x80000838]:csrrs a3, fcsr, zero
	-[0x8000083c]:sw t6, 256(s1)
Current Store : [0x80000840] : sw a3, 260(s1) -- Store: [0x800026d8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fmul.h t6, t5, t4, dyn
	-[0x80000858]:csrrs a3, fcsr, zero
	-[0x8000085c]:sw t6, 264(s1)
Current Store : [0x80000860] : sw a3, 268(s1) -- Store: [0x800026e0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:fmul.h t6, t5, t4, dyn
	-[0x80000878]:csrrs a3, fcsr, zero
	-[0x8000087c]:sw t6, 272(s1)
Current Store : [0x80000880] : sw a3, 276(s1) -- Store: [0x800026e8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:fmul.h t6, t5, t4, dyn
	-[0x80000898]:csrrs a3, fcsr, zero
	-[0x8000089c]:sw t6, 280(s1)
Current Store : [0x800008a0] : sw a3, 284(s1) -- Store: [0x800026f0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fmul.h t6, t5, t4, dyn
	-[0x800008b8]:csrrs a3, fcsr, zero
	-[0x800008bc]:sw t6, 288(s1)
Current Store : [0x800008c0] : sw a3, 292(s1) -- Store: [0x800026f8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d4]:fmul.h t6, t5, t4, dyn
	-[0x800008d8]:csrrs a3, fcsr, zero
	-[0x800008dc]:sw t6, 296(s1)
Current Store : [0x800008e0] : sw a3, 300(s1) -- Store: [0x80002700]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmul.h t6, t5, t4, dyn
	-[0x800008f8]:csrrs a3, fcsr, zero
	-[0x800008fc]:sw t6, 304(s1)
Current Store : [0x80000900] : sw a3, 308(s1) -- Store: [0x80002708]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000914]:fmul.h t6, t5, t4, dyn
	-[0x80000918]:csrrs a3, fcsr, zero
	-[0x8000091c]:sw t6, 312(s1)
Current Store : [0x80000920] : sw a3, 316(s1) -- Store: [0x80002710]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000934]:fmul.h t6, t5, t4, dyn
	-[0x80000938]:csrrs a3, fcsr, zero
	-[0x8000093c]:sw t6, 320(s1)
Current Store : [0x80000940] : sw a3, 324(s1) -- Store: [0x80002718]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000954]:fmul.h t6, t5, t4, dyn
	-[0x80000958]:csrrs a3, fcsr, zero
	-[0x8000095c]:sw t6, 328(s1)
Current Store : [0x80000960] : sw a3, 332(s1) -- Store: [0x80002720]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fmul.h t6, t5, t4, dyn
	-[0x80000978]:csrrs a3, fcsr, zero
	-[0x8000097c]:sw t6, 336(s1)
Current Store : [0x80000980] : sw a3, 340(s1) -- Store: [0x80002728]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fmul.h t6, t5, t4, dyn
	-[0x80000998]:csrrs a3, fcsr, zero
	-[0x8000099c]:sw t6, 344(s1)
Current Store : [0x800009a0] : sw a3, 348(s1) -- Store: [0x80002730]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fmul.h t6, t5, t4, dyn
	-[0x800009b8]:csrrs a3, fcsr, zero
	-[0x800009bc]:sw t6, 352(s1)
Current Store : [0x800009c0] : sw a3, 356(s1) -- Store: [0x80002738]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fmul.h t6, t5, t4, dyn
	-[0x800009d8]:csrrs a3, fcsr, zero
	-[0x800009dc]:sw t6, 360(s1)
Current Store : [0x800009e0] : sw a3, 364(s1) -- Store: [0x80002740]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009f4]:fmul.h t6, t5, t4, dyn
	-[0x800009f8]:csrrs a3, fcsr, zero
	-[0x800009fc]:sw t6, 368(s1)
Current Store : [0x80000a00] : sw a3, 372(s1) -- Store: [0x80002748]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmul.h t6, t5, t4, dyn
	-[0x80000a18]:csrrs a3, fcsr, zero
	-[0x80000a1c]:sw t6, 376(s1)
Current Store : [0x80000a20] : sw a3, 380(s1) -- Store: [0x80002750]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fmul.h t6, t5, t4, dyn
	-[0x80000a38]:csrrs a3, fcsr, zero
	-[0x80000a3c]:sw t6, 384(s1)
Current Store : [0x80000a40] : sw a3, 388(s1) -- Store: [0x80002758]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a54]:fmul.h t6, t5, t4, dyn
	-[0x80000a58]:csrrs a3, fcsr, zero
	-[0x80000a5c]:sw t6, 392(s1)
Current Store : [0x80000a60] : sw a3, 396(s1) -- Store: [0x80002760]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fmul.h t6, t5, t4, dyn
	-[0x80000a78]:csrrs a3, fcsr, zero
	-[0x80000a7c]:sw t6, 400(s1)
Current Store : [0x80000a80] : sw a3, 404(s1) -- Store: [0x80002768]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fmul.h t6, t5, t4, dyn
	-[0x80000a98]:csrrs a3, fcsr, zero
	-[0x80000a9c]:sw t6, 408(s1)
Current Store : [0x80000aa0] : sw a3, 412(s1) -- Store: [0x80002770]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fmul.h t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a3, fcsr, zero
	-[0x80000abc]:sw t6, 416(s1)
Current Store : [0x80000ac0] : sw a3, 420(s1) -- Store: [0x80002778]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fmul.h t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a3, fcsr, zero
	-[0x80000adc]:sw t6, 424(s1)
Current Store : [0x80000ae0] : sw a3, 428(s1) -- Store: [0x80002780]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fmul.h t6, t5, t4, dyn
	-[0x80000af8]:csrrs a3, fcsr, zero
	-[0x80000afc]:sw t6, 432(s1)
Current Store : [0x80000b00] : sw a3, 436(s1) -- Store: [0x80002788]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b14]:fmul.h t6, t5, t4, dyn
	-[0x80000b18]:csrrs a3, fcsr, zero
	-[0x80000b1c]:sw t6, 440(s1)
Current Store : [0x80000b20] : sw a3, 444(s1) -- Store: [0x80002790]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmul.h t6, t5, t4, dyn
	-[0x80000b38]:csrrs a3, fcsr, zero
	-[0x80000b3c]:sw t6, 448(s1)
Current Store : [0x80000b40] : sw a3, 452(s1) -- Store: [0x80002798]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fmul.h t6, t5, t4, dyn
	-[0x80000b58]:csrrs a3, fcsr, zero
	-[0x80000b5c]:sw t6, 456(s1)
Current Store : [0x80000b60] : sw a3, 460(s1) -- Store: [0x800027a0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fmul.h t6, t5, t4, dyn
	-[0x80000b78]:csrrs a3, fcsr, zero
	-[0x80000b7c]:sw t6, 464(s1)
Current Store : [0x80000b80] : sw a3, 468(s1) -- Store: [0x800027a8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x0ba and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b94]:fmul.h t6, t5, t4, dyn
	-[0x80000b98]:csrrs a3, fcsr, zero
	-[0x80000b9c]:sw t6, 472(s1)
Current Store : [0x80000ba0] : sw a3, 476(s1) -- Store: [0x800027b0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x0c and fm2 == 0x275 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fmul.h t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a3, fcsr, zero
	-[0x80000bbc]:sw t6, 480(s1)
Current Store : [0x80000bc0] : sw a3, 484(s1) -- Store: [0x800027b8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x0e5 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fmul.h t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a3, fcsr, zero
	-[0x80000bdc]:sw t6, 488(s1)
Current Store : [0x80000be0] : sw a3, 492(s1) -- Store: [0x800027c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x187 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fmul.h t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a3, fcsr, zero
	-[0x80000bfc]:sw t6, 496(s1)
Current Store : [0x80000c00] : sw a3, 500(s1) -- Store: [0x800027c8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x103 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c14]:fmul.h t6, t5, t4, dyn
	-[0x80000c18]:csrrs a3, fcsr, zero
	-[0x80000c1c]:sw t6, 504(s1)
Current Store : [0x80000c20] : sw a3, 508(s1) -- Store: [0x800027d0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x052 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fmul.h t6, t5, t4, dyn
	-[0x80000c38]:csrrs a3, fcsr, zero
	-[0x80000c3c]:sw t6, 512(s1)
Current Store : [0x80000c40] : sw a3, 516(s1) -- Store: [0x800027d8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x3af and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmul.h t6, t5, t4, dyn
	-[0x80000c58]:csrrs a3, fcsr, zero
	-[0x80000c5c]:sw t6, 520(s1)
Current Store : [0x80000c60] : sw a3, 524(s1) -- Store: [0x800027e0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fmul.h t6, t5, t4, dyn
	-[0x80000c78]:csrrs a3, fcsr, zero
	-[0x80000c7c]:sw t6, 528(s1)
Current Store : [0x80000c80] : sw a3, 532(s1) -- Store: [0x800027e8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x025 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c94]:fmul.h t6, t5, t4, dyn
	-[0x80000c98]:csrrs a3, fcsr, zero
	-[0x80000c9c]:sw t6, 536(s1)
Current Store : [0x80000ca0] : sw a3, 540(s1) -- Store: [0x800027f0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fmul.h t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a3, fcsr, zero
	-[0x80000cbc]:sw t6, 544(s1)
Current Store : [0x80000cc0] : sw a3, 548(s1) -- Store: [0x800027f8]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 0 and fe2 == 0x00 and fm2 == 0x012 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fmul.h t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a3, fcsr, zero
	-[0x80000cdc]:sw t6, 552(s1)
Current Store : [0x80000ce0] : sw a3, 556(s1) -- Store: [0x80002800]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fmul.h t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a3, fcsr, zero
	-[0x80000cfc]:sw t6, 560(s1)
Current Store : [0x80000d00] : sw a3, 564(s1) -- Store: [0x80002808]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x00 and fm2 == 0x013 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fmul.h t6, t5, t4, dyn
	-[0x80000d18]:csrrs a3, fcsr, zero
	-[0x80000d1c]:sw t6, 568(s1)
Current Store : [0x80000d20] : sw a3, 572(s1) -- Store: [0x80002810]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x02c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fmul.h t6, t5, t4, dyn
	-[0x80000d38]:csrrs a3, fcsr, zero
	-[0x80000d3c]:sw t6, 576(s1)
Current Store : [0x80000d40] : sw a3, 580(s1) -- Store: [0x80002818]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d54]:fmul.h t6, t5, t4, dyn
	-[0x80000d58]:csrrs a3, fcsr, zero
	-[0x80000d5c]:sw t6, 584(s1)
Current Store : [0x80000d60] : sw a3, 588(s1) -- Store: [0x80002820]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmul.h t6, t5, t4, dyn
	-[0x80000d78]:csrrs a3, fcsr, zero
	-[0x80000d7c]:sw t6, 592(s1)
Current Store : [0x80000d80] : sw a3, 596(s1) -- Store: [0x80002828]:0x00000062




Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d94]:fmul.h t6, t5, t4, dyn
	-[0x80000d98]:csrrs a3, fcsr, zero
	-[0x80000d9c]:sw t6, 600(s1)
Current Store : [0x80000da0] : sw a3, 604(s1) -- Store: [0x80002830]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fmul.h t6, t5, t4, dyn
	-[0x80000db8]:csrrs a3, fcsr, zero
	-[0x80000dbc]:sw t6, 608(s1)
Current Store : [0x80000dc0] : sw a3, 612(s1) -- Store: [0x80002838]:0x00000062




Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fmul.h t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a3, fcsr, zero
	-[0x80000ddc]:sw t6, 616(s1)
Current Store : [0x80000de0] : sw a3, 620(s1) -- Store: [0x80002840]:0x00000062





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                          |                                                      code                                                       |
|---:|-------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002514]<br>0x00007C00<br> |- mnemonic : fmul.h<br> - rs1 : x30<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                  |[0x80000124]:fmul.h t6, t5, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000251c]<br>0x00000000<br> |- rs1 : x29<br> - rs2 : x31<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000144]:fmul.h t4, t4, t6, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80002524]<br>0x00007C00<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                          |[0x80000164]:fmul.h t3, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br>     |
|   4|[0x8000252c]<br>0x00000000<br> |- rs1 : x31<br> - rs2 : x27<br> - rd : x27<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000184]:fmul.h s11, t6, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw s11, 24(ra)<br>  |
|   5|[0x80002534]<br>0x00000000<br> |- rs1 : x27<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x800001a4]:fmul.h t5, s11, t4, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw t5, 32(ra)<br>    |
|   6|[0x8000253c]<br>0x00000000<br> |- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800001c4]:fmul.h s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80002544]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800001e4]:fmul.h s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8000254c]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000204]:fmul.h s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80002554]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000224]:fmul.h s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000255c]<br>0x00000000<br> |- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000244]:fmul.h s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002564]<br>0x00000000<br> |- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000264]:fmul.h s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000256c]<br>0x00000000<br> |- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000284]:fmul.h s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002574]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002a4]:fmul.h s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000257c]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002c4]:fmul.h s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002584]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002e4]:fmul.h a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000258c]<br>0x00000000<br> |- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000304]:fmul.h a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002594]<br>0x00000000<br> |- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000324]:fmul.h a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000259c]<br>0x00000000<br> |- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000344]:fmul.h a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800025a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000364]:fmul.h a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800025ac]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000384]:fmul.h a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800025b4]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fmul.h a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800025bc]<br>0x00000000<br> |- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003cc]:fmul.h a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800025c4]<br>0x00000000<br> |- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003ec]:fmul.h s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800025cc]<br>0x00000000<br> |- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x8000040c]:fmul.h fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800025d4]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000434]:fmul.h t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800025dc]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000454]:fmul.h t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800025e4]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000474]:fmul.h t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800025ec]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000494]:fmul.h tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800025f4]<br>0x00000000<br> |- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004b4]:fmul.h gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800025fc]<br>0x00000000<br> |- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004d4]:fmul.h sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80002604]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                    |[0x800004f4]:fmul.h ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x8000260c]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000514]:fmul.h zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80002614]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000534]:fmul.h t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x8000261c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000554]:fmul.h t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
|  35|[0x80002624]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000574]:fmul.h t6, t5, t4, dyn<br> [0x80000578]:csrrs a3, fcsr, zero<br> [0x8000057c]:sw t6, 80(s1)<br>     |
|  36|[0x8000262c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000594]:fmul.h t6, t5, t4, dyn<br> [0x80000598]:csrrs a3, fcsr, zero<br> [0x8000059c]:sw t6, 88(s1)<br>     |
|  37|[0x80002634]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005b4]:fmul.h t6, t5, t4, dyn<br> [0x800005b8]:csrrs a3, fcsr, zero<br> [0x800005bc]:sw t6, 96(s1)<br>     |
|  38|[0x8000263c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005d4]:fmul.h t6, t5, t4, dyn<br> [0x800005d8]:csrrs a3, fcsr, zero<br> [0x800005dc]:sw t6, 104(s1)<br>    |
|  39|[0x80002644]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005f4]:fmul.h t6, t5, t4, dyn<br> [0x800005f8]:csrrs a3, fcsr, zero<br> [0x800005fc]:sw t6, 112(s1)<br>    |
|  40|[0x8000264c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000614]:fmul.h t6, t5, t4, dyn<br> [0x80000618]:csrrs a3, fcsr, zero<br> [0x8000061c]:sw t6, 120(s1)<br>    |
|  41|[0x80002654]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000634]:fmul.h t6, t5, t4, dyn<br> [0x80000638]:csrrs a3, fcsr, zero<br> [0x8000063c]:sw t6, 128(s1)<br>    |
|  42|[0x8000265c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000654]:fmul.h t6, t5, t4, dyn<br> [0x80000658]:csrrs a3, fcsr, zero<br> [0x8000065c]:sw t6, 136(s1)<br>    |
|  43|[0x80002664]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000674]:fmul.h t6, t5, t4, dyn<br> [0x80000678]:csrrs a3, fcsr, zero<br> [0x8000067c]:sw t6, 144(s1)<br>    |
|  44|[0x8000266c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000694]:fmul.h t6, t5, t4, dyn<br> [0x80000698]:csrrs a3, fcsr, zero<br> [0x8000069c]:sw t6, 152(s1)<br>    |
|  45|[0x80002674]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006b4]:fmul.h t6, t5, t4, dyn<br> [0x800006b8]:csrrs a3, fcsr, zero<br> [0x800006bc]:sw t6, 160(s1)<br>    |
|  46|[0x8000267c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006d4]:fmul.h t6, t5, t4, dyn<br> [0x800006d8]:csrrs a3, fcsr, zero<br> [0x800006dc]:sw t6, 168(s1)<br>    |
|  47|[0x80002684]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006f4]:fmul.h t6, t5, t4, dyn<br> [0x800006f8]:csrrs a3, fcsr, zero<br> [0x800006fc]:sw t6, 176(s1)<br>    |
|  48|[0x8000268c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000714]:fmul.h t6, t5, t4, dyn<br> [0x80000718]:csrrs a3, fcsr, zero<br> [0x8000071c]:sw t6, 184(s1)<br>    |
|  49|[0x80002694]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000734]:fmul.h t6, t5, t4, dyn<br> [0x80000738]:csrrs a3, fcsr, zero<br> [0x8000073c]:sw t6, 192(s1)<br>    |
|  50|[0x8000269c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000754]:fmul.h t6, t5, t4, dyn<br> [0x80000758]:csrrs a3, fcsr, zero<br> [0x8000075c]:sw t6, 200(s1)<br>    |
|  51|[0x800026a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000774]:fmul.h t6, t5, t4, dyn<br> [0x80000778]:csrrs a3, fcsr, zero<br> [0x8000077c]:sw t6, 208(s1)<br>    |
|  52|[0x800026ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000794]:fmul.h t6, t5, t4, dyn<br> [0x80000798]:csrrs a3, fcsr, zero<br> [0x8000079c]:sw t6, 216(s1)<br>    |
|  53|[0x800026b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007b4]:fmul.h t6, t5, t4, dyn<br> [0x800007b8]:csrrs a3, fcsr, zero<br> [0x800007bc]:sw t6, 224(s1)<br>    |
|  54|[0x800026bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007d4]:fmul.h t6, t5, t4, dyn<br> [0x800007d8]:csrrs a3, fcsr, zero<br> [0x800007dc]:sw t6, 232(s1)<br>    |
|  55|[0x800026c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007f4]:fmul.h t6, t5, t4, dyn<br> [0x800007f8]:csrrs a3, fcsr, zero<br> [0x800007fc]:sw t6, 240(s1)<br>    |
|  56|[0x800026cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000814]:fmul.h t6, t5, t4, dyn<br> [0x80000818]:csrrs a3, fcsr, zero<br> [0x8000081c]:sw t6, 248(s1)<br>    |
|  57|[0x800026d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000834]:fmul.h t6, t5, t4, dyn<br> [0x80000838]:csrrs a3, fcsr, zero<br> [0x8000083c]:sw t6, 256(s1)<br>    |
|  58|[0x800026dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000854]:fmul.h t6, t5, t4, dyn<br> [0x80000858]:csrrs a3, fcsr, zero<br> [0x8000085c]:sw t6, 264(s1)<br>    |
|  59|[0x800026e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000874]:fmul.h t6, t5, t4, dyn<br> [0x80000878]:csrrs a3, fcsr, zero<br> [0x8000087c]:sw t6, 272(s1)<br>    |
|  60|[0x800026ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000894]:fmul.h t6, t5, t4, dyn<br> [0x80000898]:csrrs a3, fcsr, zero<br> [0x8000089c]:sw t6, 280(s1)<br>    |
|  61|[0x800026f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008b4]:fmul.h t6, t5, t4, dyn<br> [0x800008b8]:csrrs a3, fcsr, zero<br> [0x800008bc]:sw t6, 288(s1)<br>    |
|  62|[0x800026fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008d4]:fmul.h t6, t5, t4, dyn<br> [0x800008d8]:csrrs a3, fcsr, zero<br> [0x800008dc]:sw t6, 296(s1)<br>    |
|  63|[0x80002704]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008f4]:fmul.h t6, t5, t4, dyn<br> [0x800008f8]:csrrs a3, fcsr, zero<br> [0x800008fc]:sw t6, 304(s1)<br>    |
|  64|[0x8000270c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000914]:fmul.h t6, t5, t4, dyn<br> [0x80000918]:csrrs a3, fcsr, zero<br> [0x8000091c]:sw t6, 312(s1)<br>    |
|  65|[0x80002714]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000934]:fmul.h t6, t5, t4, dyn<br> [0x80000938]:csrrs a3, fcsr, zero<br> [0x8000093c]:sw t6, 320(s1)<br>    |
|  66|[0x8000271c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000954]:fmul.h t6, t5, t4, dyn<br> [0x80000958]:csrrs a3, fcsr, zero<br> [0x8000095c]:sw t6, 328(s1)<br>    |
|  67|[0x80002724]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000974]:fmul.h t6, t5, t4, dyn<br> [0x80000978]:csrrs a3, fcsr, zero<br> [0x8000097c]:sw t6, 336(s1)<br>    |
|  68|[0x8000272c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000994]:fmul.h t6, t5, t4, dyn<br> [0x80000998]:csrrs a3, fcsr, zero<br> [0x8000099c]:sw t6, 344(s1)<br>    |
|  69|[0x80002734]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009b4]:fmul.h t6, t5, t4, dyn<br> [0x800009b8]:csrrs a3, fcsr, zero<br> [0x800009bc]:sw t6, 352(s1)<br>    |
|  70|[0x8000273c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009d4]:fmul.h t6, t5, t4, dyn<br> [0x800009d8]:csrrs a3, fcsr, zero<br> [0x800009dc]:sw t6, 360(s1)<br>    |
|  71|[0x80002744]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009f4]:fmul.h t6, t5, t4, dyn<br> [0x800009f8]:csrrs a3, fcsr, zero<br> [0x800009fc]:sw t6, 368(s1)<br>    |
|  72|[0x8000274c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a14]:fmul.h t6, t5, t4, dyn<br> [0x80000a18]:csrrs a3, fcsr, zero<br> [0x80000a1c]:sw t6, 376(s1)<br>    |
|  73|[0x80002754]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a34]:fmul.h t6, t5, t4, dyn<br> [0x80000a38]:csrrs a3, fcsr, zero<br> [0x80000a3c]:sw t6, 384(s1)<br>    |
|  74|[0x8000275c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a54]:fmul.h t6, t5, t4, dyn<br> [0x80000a58]:csrrs a3, fcsr, zero<br> [0x80000a5c]:sw t6, 392(s1)<br>    |
|  75|[0x80002764]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a74]:fmul.h t6, t5, t4, dyn<br> [0x80000a78]:csrrs a3, fcsr, zero<br> [0x80000a7c]:sw t6, 400(s1)<br>    |
|  76|[0x8000276c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a94]:fmul.h t6, t5, t4, dyn<br> [0x80000a98]:csrrs a3, fcsr, zero<br> [0x80000a9c]:sw t6, 408(s1)<br>    |
|  77|[0x80002774]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ab4]:fmul.h t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a3, fcsr, zero<br> [0x80000abc]:sw t6, 416(s1)<br>    |
|  78|[0x8000277c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ad4]:fmul.h t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a3, fcsr, zero<br> [0x80000adc]:sw t6, 424(s1)<br>    |
|  79|[0x80002784]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000af4]:fmul.h t6, t5, t4, dyn<br> [0x80000af8]:csrrs a3, fcsr, zero<br> [0x80000afc]:sw t6, 432(s1)<br>    |
|  80|[0x8000278c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b14]:fmul.h t6, t5, t4, dyn<br> [0x80000b18]:csrrs a3, fcsr, zero<br> [0x80000b1c]:sw t6, 440(s1)<br>    |
|  81|[0x80002794]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b34]:fmul.h t6, t5, t4, dyn<br> [0x80000b38]:csrrs a3, fcsr, zero<br> [0x80000b3c]:sw t6, 448(s1)<br>    |
|  82|[0x8000279c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b54]:fmul.h t6, t5, t4, dyn<br> [0x80000b58]:csrrs a3, fcsr, zero<br> [0x80000b5c]:sw t6, 456(s1)<br>    |
|  83|[0x800027a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b74]:fmul.h t6, t5, t4, dyn<br> [0x80000b78]:csrrs a3, fcsr, zero<br> [0x80000b7c]:sw t6, 464(s1)<br>    |
|  84|[0x800027ac]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x0ba and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b94]:fmul.h t6, t5, t4, dyn<br> [0x80000b98]:csrrs a3, fcsr, zero<br> [0x80000b9c]:sw t6, 472(s1)<br>    |
|  85|[0x800027b4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x0c and fm2 == 0x275 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bb4]:fmul.h t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a3, fcsr, zero<br> [0x80000bbc]:sw t6, 480(s1)<br>    |
|  86|[0x800027bc]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x0e5 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bd4]:fmul.h t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a3, fcsr, zero<br> [0x80000bdc]:sw t6, 488(s1)<br>    |
|  87|[0x800027c4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x187 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bf4]:fmul.h t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a3, fcsr, zero<br> [0x80000bfc]:sw t6, 496(s1)<br>    |
|  88|[0x800027cc]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x103 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c14]:fmul.h t6, t5, t4, dyn<br> [0x80000c18]:csrrs a3, fcsr, zero<br> [0x80000c1c]:sw t6, 504(s1)<br>    |
|  89|[0x800027d4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x052 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c34]:fmul.h t6, t5, t4, dyn<br> [0x80000c38]:csrrs a3, fcsr, zero<br> [0x80000c3c]:sw t6, 512(s1)<br>    |
|  90|[0x800027dc]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x3af and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c54]:fmul.h t6, t5, t4, dyn<br> [0x80000c58]:csrrs a3, fcsr, zero<br> [0x80000c5c]:sw t6, 520(s1)<br>    |
|  91|[0x800027e4]<br>0x000027BE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c74]:fmul.h t6, t5, t4, dyn<br> [0x80000c78]:csrrs a3, fcsr, zero<br> [0x80000c7c]:sw t6, 528(s1)<br>    |
|  92|[0x800027ec]<br>0x000027DB<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x025 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c94]:fmul.h t6, t5, t4, dyn<br> [0x80000c98]:csrrs a3, fcsr, zero<br> [0x80000c9c]:sw t6, 536(s1)<br>    |
|  93|[0x800027f4]<br>0x000027B3<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cb4]:fmul.h t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a3, fcsr, zero<br> [0x80000cbc]:sw t6, 544(s1)<br>    |
|  94|[0x800027fc]<br>0x000027CD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 0 and fe2 == 0x00 and fm2 == 0x012 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cd4]:fmul.h t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a3, fcsr, zero<br> [0x80000cdc]:sw t6, 552(s1)<br>    |
|  95|[0x80002804]<br>0x0000278D<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00d and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cf4]:fmul.h t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a3, fcsr, zero<br> [0x80000cfc]:sw t6, 560(s1)<br>    |
|  96|[0x8000280c]<br>0x000027F0<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 0 and fe2 == 0x00 and fm2 == 0x013 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d14]:fmul.h t6, t5, t4, dyn<br> [0x80000d18]:csrrs a3, fcsr, zero<br> [0x80000d1c]:sw t6, 568(s1)<br>    |
|  97|[0x80002814]<br>0x000027F8<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x02c and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d34]:fmul.h t6, t5, t4, dyn<br> [0x80000d38]:csrrs a3, fcsr, zero<br> [0x80000d3c]:sw t6, 576(s1)<br>    |
|  98|[0x8000281c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d54]:fmul.h t6, t5, t4, dyn<br> [0x80000d58]:csrrs a3, fcsr, zero<br> [0x80000d5c]:sw t6, 584(s1)<br>    |
|  99|[0x80002824]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d74]:fmul.h t6, t5, t4, dyn<br> [0x80000d78]:csrrs a3, fcsr, zero<br> [0x80000d7c]:sw t6, 592(s1)<br>    |
| 100|[0x80002834]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and fcsr == 0x62 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000db4]:fmul.h t6, t5, t4, dyn<br> [0x80000db8]:csrrs a3, fcsr, zero<br> [0x80000dbc]:sw t6, 608(s1)<br>    |
