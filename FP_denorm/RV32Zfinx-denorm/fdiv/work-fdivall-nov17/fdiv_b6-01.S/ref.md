
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001350')]      |
| SIG_REGION                | [('0x80003610', '0x80003aa0', '292 words')]      |
| COV_LABELS                | fdiv_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zfinx-denorm/work-fdivall-nov17/fdiv_b6-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 290      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 144     |
| STAT4                     | 145     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001334]:fdiv.s t6, t5, t4, dyn
      [0x80001338]:csrrs a2, fcsr, zero
      [0x8000133c]:sw t6, 952(fp)
      [0x80001340]:sw a2, 956(fp)
      [0x80001344]:addi zero, zero, 0
      [0x80001348]:addi zero, zero, 0
      [0x8000134c]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80003a94 Data: 0x80000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x62 and rm_val == 7   #nosat






```

## Details of STAT3

```
[0x80000124]:fdiv.s t6, t6, t6, dyn
[0x80000128]:csrrs tp, fcsr, zero
[0x8000012c]:sw t6, 0(ra)
[0x80000130]:sw tp, 4(ra)
[0x80000134]:lw t4, 8(gp)
[0x80000138]:lw t5, 12(gp)
[0x8000013c]:addi sp, zero, 34
[0x80000140]:csrrw zero, fcsr, sp
[0x80000144]:fdiv.s t5, t4, t5, dyn

[0x80000144]:fdiv.s t5, t4, t5, dyn
[0x80000148]:csrrs tp, fcsr, zero
[0x8000014c]:sw t5, 8(ra)
[0x80000150]:sw tp, 12(ra)
[0x80000154]:lw t3, 16(gp)
[0x80000158]:lw t3, 20(gp)
[0x8000015c]:addi sp, zero, 66
[0x80000160]:csrrw zero, fcsr, sp
[0x80000164]:fdiv.s t4, t3, t3, dyn

[0x80000164]:fdiv.s t4, t3, t3, dyn
[0x80000168]:csrrs tp, fcsr, zero
[0x8000016c]:sw t4, 16(ra)
[0x80000170]:sw tp, 20(ra)
[0x80000174]:lw t5, 24(gp)
[0x80000178]:lw t4, 28(gp)
[0x8000017c]:addi sp, zero, 98
[0x80000180]:csrrw zero, fcsr, sp
[0x80000184]:fdiv.s t3, t5, t4, dyn

[0x80000184]:fdiv.s t3, t5, t4, dyn
[0x80000188]:csrrs tp, fcsr, zero
[0x8000018c]:sw t3, 24(ra)
[0x80000190]:sw tp, 28(ra)
[0x80000194]:lw s11, 32(gp)
[0x80000198]:lw s10, 36(gp)
[0x8000019c]:addi sp, zero, 130
[0x800001a0]:csrrw zero, fcsr, sp
[0x800001a4]:fdiv.s s11, s11, s10, dyn

[0x800001a4]:fdiv.s s11, s11, s10, dyn
[0x800001a8]:csrrs tp, fcsr, zero
[0x800001ac]:sw s11, 32(ra)
[0x800001b0]:sw tp, 36(ra)
[0x800001b4]:lw s9, 40(gp)
[0x800001b8]:lw s11, 44(gp)
[0x800001bc]:addi sp, zero, 2
[0x800001c0]:csrrw zero, fcsr, sp
[0x800001c4]:fdiv.s s10, s9, s11, dyn

[0x800001c4]:fdiv.s s10, s9, s11, dyn
[0x800001c8]:csrrs tp, fcsr, zero
[0x800001cc]:sw s10, 40(ra)
[0x800001d0]:sw tp, 44(ra)
[0x800001d4]:lw s10, 48(gp)
[0x800001d8]:lw s8, 52(gp)
[0x800001dc]:addi sp, zero, 34
[0x800001e0]:csrrw zero, fcsr, sp
[0x800001e4]:fdiv.s s9, s10, s8, dyn

[0x800001e4]:fdiv.s s9, s10, s8, dyn
[0x800001e8]:csrrs tp, fcsr, zero
[0x800001ec]:sw s9, 48(ra)
[0x800001f0]:sw tp, 52(ra)
[0x800001f4]:lw s7, 56(gp)
[0x800001f8]:lw s9, 60(gp)
[0x800001fc]:addi sp, zero, 66
[0x80000200]:csrrw zero, fcsr, sp
[0x80000204]:fdiv.s s8, s7, s9, dyn

[0x80000204]:fdiv.s s8, s7, s9, dyn
[0x80000208]:csrrs tp, fcsr, zero
[0x8000020c]:sw s8, 56(ra)
[0x80000210]:sw tp, 60(ra)
[0x80000214]:lw s8, 64(gp)
[0x80000218]:lw s6, 68(gp)
[0x8000021c]:addi sp, zero, 98
[0x80000220]:csrrw zero, fcsr, sp
[0x80000224]:fdiv.s s7, s8, s6, dyn

[0x80000224]:fdiv.s s7, s8, s6, dyn
[0x80000228]:csrrs tp, fcsr, zero
[0x8000022c]:sw s7, 64(ra)
[0x80000230]:sw tp, 68(ra)
[0x80000234]:lw s5, 72(gp)
[0x80000238]:lw s7, 76(gp)
[0x8000023c]:addi sp, zero, 130
[0x80000240]:csrrw zero, fcsr, sp
[0x80000244]:fdiv.s s6, s5, s7, dyn

[0x80000244]:fdiv.s s6, s5, s7, dyn
[0x80000248]:csrrs tp, fcsr, zero
[0x8000024c]:sw s6, 72(ra)
[0x80000250]:sw tp, 76(ra)
[0x80000254]:lw s6, 80(gp)
[0x80000258]:lw s4, 84(gp)
[0x8000025c]:addi sp, zero, 2
[0x80000260]:csrrw zero, fcsr, sp
[0x80000264]:fdiv.s s5, s6, s4, dyn

[0x80000264]:fdiv.s s5, s6, s4, dyn
[0x80000268]:csrrs tp, fcsr, zero
[0x8000026c]:sw s5, 80(ra)
[0x80000270]:sw tp, 84(ra)
[0x80000274]:lw s3, 88(gp)
[0x80000278]:lw s5, 92(gp)
[0x8000027c]:addi sp, zero, 34
[0x80000280]:csrrw zero, fcsr, sp
[0x80000284]:fdiv.s s4, s3, s5, dyn

[0x80000284]:fdiv.s s4, s3, s5, dyn
[0x80000288]:csrrs tp, fcsr, zero
[0x8000028c]:sw s4, 88(ra)
[0x80000290]:sw tp, 92(ra)
[0x80000294]:lw s4, 96(gp)
[0x80000298]:lw s2, 100(gp)
[0x8000029c]:addi sp, zero, 66
[0x800002a0]:csrrw zero, fcsr, sp
[0x800002a4]:fdiv.s s3, s4, s2, dyn

[0x800002a4]:fdiv.s s3, s4, s2, dyn
[0x800002a8]:csrrs tp, fcsr, zero
[0x800002ac]:sw s3, 96(ra)
[0x800002b0]:sw tp, 100(ra)
[0x800002b4]:lw a7, 104(gp)
[0x800002b8]:lw s3, 108(gp)
[0x800002bc]:addi sp, zero, 98
[0x800002c0]:csrrw zero, fcsr, sp
[0x800002c4]:fdiv.s s2, a7, s3, dyn

[0x800002c4]:fdiv.s s2, a7, s3, dyn
[0x800002c8]:csrrs tp, fcsr, zero
[0x800002cc]:sw s2, 104(ra)
[0x800002d0]:sw tp, 108(ra)
[0x800002d4]:lw s2, 112(gp)
[0x800002d8]:lw a6, 116(gp)
[0x800002dc]:addi sp, zero, 130
[0x800002e0]:csrrw zero, fcsr, sp
[0x800002e4]:fdiv.s a7, s2, a6, dyn

[0x800002e4]:fdiv.s a7, s2, a6, dyn
[0x800002e8]:csrrs tp, fcsr, zero
[0x800002ec]:sw a7, 112(ra)
[0x800002f0]:sw tp, 116(ra)
[0x800002f4]:lw a5, 120(gp)
[0x800002f8]:lw a7, 124(gp)
[0x800002fc]:addi sp, zero, 2
[0x80000300]:csrrw zero, fcsr, sp
[0x80000304]:fdiv.s a6, a5, a7, dyn

[0x80000304]:fdiv.s a6, a5, a7, dyn
[0x80000308]:csrrs tp, fcsr, zero
[0x8000030c]:sw a6, 120(ra)
[0x80000310]:sw tp, 124(ra)
[0x80000314]:lw a6, 128(gp)
[0x80000318]:lw a4, 132(gp)
[0x8000031c]:addi sp, zero, 34
[0x80000320]:csrrw zero, fcsr, sp
[0x80000324]:fdiv.s a5, a6, a4, dyn

[0x80000324]:fdiv.s a5, a6, a4, dyn
[0x80000328]:csrrs tp, fcsr, zero
[0x8000032c]:sw a5, 128(ra)
[0x80000330]:sw tp, 132(ra)
[0x80000334]:lw a3, 136(gp)
[0x80000338]:lw a5, 140(gp)
[0x8000033c]:addi sp, zero, 66
[0x80000340]:csrrw zero, fcsr, sp
[0x80000344]:fdiv.s a4, a3, a5, dyn

[0x80000344]:fdiv.s a4, a3, a5, dyn
[0x80000348]:csrrs tp, fcsr, zero
[0x8000034c]:sw a4, 136(ra)
[0x80000350]:sw tp, 140(ra)
[0x80000354]:lw a4, 144(gp)
[0x80000358]:lw a2, 148(gp)
[0x8000035c]:addi sp, zero, 98
[0x80000360]:csrrw zero, fcsr, sp
[0x80000364]:fdiv.s a3, a4, a2, dyn

[0x80000364]:fdiv.s a3, a4, a2, dyn
[0x80000368]:csrrs tp, fcsr, zero
[0x8000036c]:sw a3, 144(ra)
[0x80000370]:sw tp, 148(ra)
[0x80000374]:lw a1, 152(gp)
[0x80000378]:lw a3, 156(gp)
[0x8000037c]:addi sp, zero, 130
[0x80000380]:csrrw zero, fcsr, sp
[0x80000384]:fdiv.s a2, a1, a3, dyn

[0x80000384]:fdiv.s a2, a1, a3, dyn
[0x80000388]:csrrs tp, fcsr, zero
[0x8000038c]:sw a2, 152(ra)
[0x80000390]:sw tp, 156(ra)
[0x80000394]:lw a2, 160(gp)
[0x80000398]:lw a0, 164(gp)
[0x8000039c]:addi sp, zero, 2
[0x800003a0]:csrrw zero, fcsr, sp
[0x800003a4]:fdiv.s a1, a2, a0, dyn

[0x800003a4]:fdiv.s a1, a2, a0, dyn
[0x800003a8]:csrrs tp, fcsr, zero
[0x800003ac]:sw a1, 160(ra)
[0x800003b0]:sw tp, 164(ra)
[0x800003b4]:lw s1, 168(gp)
[0x800003b8]:lw a1, 172(gp)
[0x800003bc]:addi sp, zero, 34
[0x800003c0]:csrrw zero, fcsr, sp
[0x800003c4]:fdiv.s a0, s1, a1, dyn

[0x800003c4]:fdiv.s a0, s1, a1, dyn
[0x800003c8]:csrrs tp, fcsr, zero
[0x800003cc]:sw a0, 168(ra)
[0x800003d0]:sw tp, 172(ra)
[0x800003d4]:auipc a1, 3
[0x800003d8]:addi a1, a1, 3308
[0x800003dc]:lw a0, 0(a1)
[0x800003e0]:lw fp, 4(a1)
[0x800003e4]:addi sp, zero, 66
[0x800003e8]:csrrw zero, fcsr, sp
[0x800003ec]:fdiv.s s1, a0, fp, dyn

[0x800003ec]:fdiv.s s1, a0, fp, dyn
[0x800003f0]:csrrs a2, fcsr, zero
[0x800003f4]:sw s1, 176(ra)
[0x800003f8]:sw a2, 180(ra)
[0x800003fc]:lw t2, 8(a1)
[0x80000400]:lw s1, 12(a1)
[0x80000404]:addi sp, zero, 98
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fdiv.s fp, t2, s1, dyn

[0x8000040c]:fdiv.s fp, t2, s1, dyn
[0x80000410]:csrrs a2, fcsr, zero
[0x80000414]:sw fp, 184(ra)
[0x80000418]:sw a2, 188(ra)
[0x8000041c]:lw fp, 16(a1)
[0x80000420]:lw t1, 20(a1)
[0x80000424]:addi s1, zero, 130
[0x80000428]:csrrw zero, fcsr, s1
[0x8000042c]:fdiv.s t2, fp, t1, dyn

[0x8000042c]:fdiv.s t2, fp, t1, dyn
[0x80000430]:csrrs a2, fcsr, zero
[0x80000434]:sw t2, 192(ra)
[0x80000438]:sw a2, 196(ra)
[0x8000043c]:auipc fp, 3
[0x80000440]:addi fp, fp, 672
[0x80000444]:lw t0, 24(a1)
[0x80000448]:lw t2, 28(a1)
[0x8000044c]:addi s1, zero, 2
[0x80000450]:csrrw zero, fcsr, s1
[0x80000454]:fdiv.s t1, t0, t2, dyn

[0x80000454]:fdiv.s t1, t0, t2, dyn
[0x80000458]:csrrs a2, fcsr, zero
[0x8000045c]:sw t1, 0(fp)
[0x80000460]:sw a2, 4(fp)
[0x80000464]:lw t1, 32(a1)
[0x80000468]:lw tp, 36(a1)
[0x8000046c]:addi s1, zero, 34
[0x80000470]:csrrw zero, fcsr, s1
[0x80000474]:fdiv.s t0, t1, tp, dyn

[0x80000474]:fdiv.s t0, t1, tp, dyn
[0x80000478]:csrrs a2, fcsr, zero
[0x8000047c]:sw t0, 8(fp)
[0x80000480]:sw a2, 12(fp)
[0x80000484]:lw gp, 40(a1)
[0x80000488]:lw t0, 44(a1)
[0x8000048c]:addi s1, zero, 66
[0x80000490]:csrrw zero, fcsr, s1
[0x80000494]:fdiv.s tp, gp, t0, dyn

[0x80000494]:fdiv.s tp, gp, t0, dyn
[0x80000498]:csrrs a2, fcsr, zero
[0x8000049c]:sw tp, 16(fp)
[0x800004a0]:sw a2, 20(fp)
[0x800004a4]:lw tp, 48(a1)
[0x800004a8]:lw sp, 52(a1)
[0x800004ac]:addi s1, zero, 98
[0x800004b0]:csrrw zero, fcsr, s1
[0x800004b4]:fdiv.s gp, tp, sp, dyn

[0x800004b4]:fdiv.s gp, tp, sp, dyn
[0x800004b8]:csrrs a2, fcsr, zero
[0x800004bc]:sw gp, 24(fp)
[0x800004c0]:sw a2, 28(fp)
[0x800004c4]:lw ra, 56(a1)
[0x800004c8]:lw gp, 60(a1)
[0x800004cc]:addi s1, zero, 130
[0x800004d0]:csrrw zero, fcsr, s1
[0x800004d4]:fdiv.s sp, ra, gp, dyn

[0x800004d4]:fdiv.s sp, ra, gp, dyn
[0x800004d8]:csrrs a2, fcsr, zero
[0x800004dc]:sw sp, 32(fp)
[0x800004e0]:sw a2, 36(fp)
[0x800004e4]:lw sp, 64(a1)
[0x800004e8]:lw zero, 68(a1)
[0x800004ec]:addi s1, zero, 2
[0x800004f0]:csrrw zero, fcsr, s1
[0x800004f4]:fdiv.s ra, sp, zero, dyn

[0x800004f4]:fdiv.s ra, sp, zero, dyn
[0x800004f8]:csrrs a2, fcsr, zero
[0x800004fc]:sw ra, 40(fp)
[0x80000500]:sw a2, 44(fp)
[0x80000504]:lw zero, 72(a1)
[0x80000508]:lw t5, 76(a1)
[0x8000050c]:addi s1, zero, 34
[0x80000510]:csrrw zero, fcsr, s1
[0x80000514]:fdiv.s t6, zero, t5, dyn

[0x80000514]:fdiv.s t6, zero, t5, dyn
[0x80000518]:csrrs a2, fcsr, zero
[0x8000051c]:sw t6, 48(fp)
[0x80000520]:sw a2, 52(fp)
[0x80000524]:lw t5, 80(a1)
[0x80000528]:lw ra, 84(a1)
[0x8000052c]:addi s1, zero, 66
[0x80000530]:csrrw zero, fcsr, s1
[0x80000534]:fdiv.s t6, t5, ra, dyn

[0x80000534]:fdiv.s t6, t5, ra, dyn
[0x80000538]:csrrs a2, fcsr, zero
[0x8000053c]:sw t6, 56(fp)
[0x80000540]:sw a2, 60(fp)
[0x80000544]:lw t6, 88(a1)
[0x80000548]:lw t5, 92(a1)
[0x8000054c]:addi s1, zero, 98
[0x80000550]:csrrw zero, fcsr, s1
[0x80000554]:fdiv.s zero, t6, t5, dyn

[0x80000554]:fdiv.s zero, t6, t5, dyn
[0x80000558]:csrrs a2, fcsr, zero
[0x8000055c]:sw zero, 64(fp)
[0x80000560]:sw a2, 68(fp)
[0x80000564]:lw t5, 96(a1)
[0x80000568]:lw t4, 100(a1)
[0x8000056c]:addi s1, zero, 130
[0x80000570]:csrrw zero, fcsr, s1
[0x80000574]:fdiv.s t6, t5, t4, dyn

[0x80000574]:fdiv.s t6, t5, t4, dyn
[0x80000578]:csrrs a2, fcsr, zero
[0x8000057c]:sw t6, 72(fp)
[0x80000580]:sw a2, 76(fp)
[0x80000584]:lw t5, 104(a1)
[0x80000588]:lw t4, 108(a1)
[0x8000058c]:addi s1, zero, 2
[0x80000590]:csrrw zero, fcsr, s1
[0x80000594]:fdiv.s t6, t5, t4, dyn

[0x80000594]:fdiv.s t6, t5, t4, dyn
[0x80000598]:csrrs a2, fcsr, zero
[0x8000059c]:sw t6, 80(fp)
[0x800005a0]:sw a2, 84(fp)
[0x800005a4]:lw t5, 112(a1)
[0x800005a8]:lw t4, 116(a1)
[0x800005ac]:addi s1, zero, 34
[0x800005b0]:csrrw zero, fcsr, s1
[0x800005b4]:fdiv.s t6, t5, t4, dyn

[0x800005b4]:fdiv.s t6, t5, t4, dyn
[0x800005b8]:csrrs a2, fcsr, zero
[0x800005bc]:sw t6, 88(fp)
[0x800005c0]:sw a2, 92(fp)
[0x800005c4]:lw t5, 120(a1)
[0x800005c8]:lw t4, 124(a1)
[0x800005cc]:addi s1, zero, 66
[0x800005d0]:csrrw zero, fcsr, s1
[0x800005d4]:fdiv.s t6, t5, t4, dyn

[0x800005d4]:fdiv.s t6, t5, t4, dyn
[0x800005d8]:csrrs a2, fcsr, zero
[0x800005dc]:sw t6, 96(fp)
[0x800005e0]:sw a2, 100(fp)
[0x800005e4]:lw t5, 128(a1)
[0x800005e8]:lw t4, 132(a1)
[0x800005ec]:addi s1, zero, 98
[0x800005f0]:csrrw zero, fcsr, s1
[0x800005f4]:fdiv.s t6, t5, t4, dyn

[0x800005f4]:fdiv.s t6, t5, t4, dyn
[0x800005f8]:csrrs a2, fcsr, zero
[0x800005fc]:sw t6, 104(fp)
[0x80000600]:sw a2, 108(fp)
[0x80000604]:lw t5, 136(a1)
[0x80000608]:lw t4, 140(a1)
[0x8000060c]:addi s1, zero, 130
[0x80000610]:csrrw zero, fcsr, s1
[0x80000614]:fdiv.s t6, t5, t4, dyn

[0x80000614]:fdiv.s t6, t5, t4, dyn
[0x80000618]:csrrs a2, fcsr, zero
[0x8000061c]:sw t6, 112(fp)
[0x80000620]:sw a2, 116(fp)
[0x80000624]:lw t5, 144(a1)
[0x80000628]:lw t4, 148(a1)
[0x8000062c]:addi s1, zero, 2
[0x80000630]:csrrw zero, fcsr, s1
[0x80000634]:fdiv.s t6, t5, t4, dyn

[0x80000634]:fdiv.s t6, t5, t4, dyn
[0x80000638]:csrrs a2, fcsr, zero
[0x8000063c]:sw t6, 120(fp)
[0x80000640]:sw a2, 124(fp)
[0x80000644]:lw t5, 152(a1)
[0x80000648]:lw t4, 156(a1)
[0x8000064c]:addi s1, zero, 34
[0x80000650]:csrrw zero, fcsr, s1
[0x80000654]:fdiv.s t6, t5, t4, dyn

[0x80000654]:fdiv.s t6, t5, t4, dyn
[0x80000658]:csrrs a2, fcsr, zero
[0x8000065c]:sw t6, 128(fp)
[0x80000660]:sw a2, 132(fp)
[0x80000664]:lw t5, 160(a1)
[0x80000668]:lw t4, 164(a1)
[0x8000066c]:addi s1, zero, 66
[0x80000670]:csrrw zero, fcsr, s1
[0x80000674]:fdiv.s t6, t5, t4, dyn

[0x80000674]:fdiv.s t6, t5, t4, dyn
[0x80000678]:csrrs a2, fcsr, zero
[0x8000067c]:sw t6, 136(fp)
[0x80000680]:sw a2, 140(fp)
[0x80000684]:lw t5, 168(a1)
[0x80000688]:lw t4, 172(a1)
[0x8000068c]:addi s1, zero, 98
[0x80000690]:csrrw zero, fcsr, s1
[0x80000694]:fdiv.s t6, t5, t4, dyn

[0x80000694]:fdiv.s t6, t5, t4, dyn
[0x80000698]:csrrs a2, fcsr, zero
[0x8000069c]:sw t6, 144(fp)
[0x800006a0]:sw a2, 148(fp)
[0x800006a4]:lw t5, 176(a1)
[0x800006a8]:lw t4, 180(a1)
[0x800006ac]:addi s1, zero, 130
[0x800006b0]:csrrw zero, fcsr, s1
[0x800006b4]:fdiv.s t6, t5, t4, dyn

[0x800006b4]:fdiv.s t6, t5, t4, dyn
[0x800006b8]:csrrs a2, fcsr, zero
[0x800006bc]:sw t6, 152(fp)
[0x800006c0]:sw a2, 156(fp)
[0x800006c4]:lw t5, 184(a1)
[0x800006c8]:lw t4, 188(a1)
[0x800006cc]:addi s1, zero, 2
[0x800006d0]:csrrw zero, fcsr, s1
[0x800006d4]:fdiv.s t6, t5, t4, dyn

[0x800006d4]:fdiv.s t6, t5, t4, dyn
[0x800006d8]:csrrs a2, fcsr, zero
[0x800006dc]:sw t6, 160(fp)
[0x800006e0]:sw a2, 164(fp)
[0x800006e4]:lw t5, 192(a1)
[0x800006e8]:lw t4, 196(a1)
[0x800006ec]:addi s1, zero, 34
[0x800006f0]:csrrw zero, fcsr, s1
[0x800006f4]:fdiv.s t6, t5, t4, dyn

[0x800006f4]:fdiv.s t6, t5, t4, dyn
[0x800006f8]:csrrs a2, fcsr, zero
[0x800006fc]:sw t6, 168(fp)
[0x80000700]:sw a2, 172(fp)
[0x80000704]:lw t5, 200(a1)
[0x80000708]:lw t4, 204(a1)
[0x8000070c]:addi s1, zero, 66
[0x80000710]:csrrw zero, fcsr, s1
[0x80000714]:fdiv.s t6, t5, t4, dyn

[0x80000714]:fdiv.s t6, t5, t4, dyn
[0x80000718]:csrrs a2, fcsr, zero
[0x8000071c]:sw t6, 176(fp)
[0x80000720]:sw a2, 180(fp)
[0x80000724]:lw t5, 208(a1)
[0x80000728]:lw t4, 212(a1)
[0x8000072c]:addi s1, zero, 98
[0x80000730]:csrrw zero, fcsr, s1
[0x80000734]:fdiv.s t6, t5, t4, dyn

[0x80000734]:fdiv.s t6, t5, t4, dyn
[0x80000738]:csrrs a2, fcsr, zero
[0x8000073c]:sw t6, 184(fp)
[0x80000740]:sw a2, 188(fp)
[0x80000744]:lw t5, 216(a1)
[0x80000748]:lw t4, 220(a1)
[0x8000074c]:addi s1, zero, 130
[0x80000750]:csrrw zero, fcsr, s1
[0x80000754]:fdiv.s t6, t5, t4, dyn

[0x80000754]:fdiv.s t6, t5, t4, dyn
[0x80000758]:csrrs a2, fcsr, zero
[0x8000075c]:sw t6, 192(fp)
[0x80000760]:sw a2, 196(fp)
[0x80000764]:lw t5, 224(a1)
[0x80000768]:lw t4, 228(a1)
[0x8000076c]:addi s1, zero, 2
[0x80000770]:csrrw zero, fcsr, s1
[0x80000774]:fdiv.s t6, t5, t4, dyn

[0x80000774]:fdiv.s t6, t5, t4, dyn
[0x80000778]:csrrs a2, fcsr, zero
[0x8000077c]:sw t6, 200(fp)
[0x80000780]:sw a2, 204(fp)
[0x80000784]:lw t5, 232(a1)
[0x80000788]:lw t4, 236(a1)
[0x8000078c]:addi s1, zero, 34
[0x80000790]:csrrw zero, fcsr, s1
[0x80000794]:fdiv.s t6, t5, t4, dyn

[0x80000794]:fdiv.s t6, t5, t4, dyn
[0x80000798]:csrrs a2, fcsr, zero
[0x8000079c]:sw t6, 208(fp)
[0x800007a0]:sw a2, 212(fp)
[0x800007a4]:lw t5, 240(a1)
[0x800007a8]:lw t4, 244(a1)
[0x800007ac]:addi s1, zero, 66
[0x800007b0]:csrrw zero, fcsr, s1
[0x800007b4]:fdiv.s t6, t5, t4, dyn

[0x800007b4]:fdiv.s t6, t5, t4, dyn
[0x800007b8]:csrrs a2, fcsr, zero
[0x800007bc]:sw t6, 216(fp)
[0x800007c0]:sw a2, 220(fp)
[0x800007c4]:lw t5, 248(a1)
[0x800007c8]:lw t4, 252(a1)
[0x800007cc]:addi s1, zero, 98
[0x800007d0]:csrrw zero, fcsr, s1
[0x800007d4]:fdiv.s t6, t5, t4, dyn

[0x800007d4]:fdiv.s t6, t5, t4, dyn
[0x800007d8]:csrrs a2, fcsr, zero
[0x800007dc]:sw t6, 224(fp)
[0x800007e0]:sw a2, 228(fp)
[0x800007e4]:lw t5, 256(a1)
[0x800007e8]:lw t4, 260(a1)
[0x800007ec]:addi s1, zero, 130
[0x800007f0]:csrrw zero, fcsr, s1
[0x800007f4]:fdiv.s t6, t5, t4, dyn

[0x800007f4]:fdiv.s t6, t5, t4, dyn
[0x800007f8]:csrrs a2, fcsr, zero
[0x800007fc]:sw t6, 232(fp)
[0x80000800]:sw a2, 236(fp)
[0x80000804]:lw t5, 264(a1)
[0x80000808]:lw t4, 268(a1)
[0x8000080c]:addi s1, zero, 2
[0x80000810]:csrrw zero, fcsr, s1
[0x80000814]:fdiv.s t6, t5, t4, dyn

[0x80000814]:fdiv.s t6, t5, t4, dyn
[0x80000818]:csrrs a2, fcsr, zero
[0x8000081c]:sw t6, 240(fp)
[0x80000820]:sw a2, 244(fp)
[0x80000824]:lw t5, 272(a1)
[0x80000828]:lw t4, 276(a1)
[0x8000082c]:addi s1, zero, 34
[0x80000830]:csrrw zero, fcsr, s1
[0x80000834]:fdiv.s t6, t5, t4, dyn

[0x80000834]:fdiv.s t6, t5, t4, dyn
[0x80000838]:csrrs a2, fcsr, zero
[0x8000083c]:sw t6, 248(fp)
[0x80000840]:sw a2, 252(fp)
[0x80000844]:lw t5, 280(a1)
[0x80000848]:lw t4, 284(a1)
[0x8000084c]:addi s1, zero, 66
[0x80000850]:csrrw zero, fcsr, s1
[0x80000854]:fdiv.s t6, t5, t4, dyn

[0x80000854]:fdiv.s t6, t5, t4, dyn
[0x80000858]:csrrs a2, fcsr, zero
[0x8000085c]:sw t6, 256(fp)
[0x80000860]:sw a2, 260(fp)
[0x80000864]:lw t5, 288(a1)
[0x80000868]:lw t4, 292(a1)
[0x8000086c]:addi s1, zero, 98
[0x80000870]:csrrw zero, fcsr, s1
[0x80000874]:fdiv.s t6, t5, t4, dyn

[0x80000874]:fdiv.s t6, t5, t4, dyn
[0x80000878]:csrrs a2, fcsr, zero
[0x8000087c]:sw t6, 264(fp)
[0x80000880]:sw a2, 268(fp)
[0x80000884]:lw t5, 296(a1)
[0x80000888]:lw t4, 300(a1)
[0x8000088c]:addi s1, zero, 130
[0x80000890]:csrrw zero, fcsr, s1
[0x80000894]:fdiv.s t6, t5, t4, dyn

[0x80000894]:fdiv.s t6, t5, t4, dyn
[0x80000898]:csrrs a2, fcsr, zero
[0x8000089c]:sw t6, 272(fp)
[0x800008a0]:sw a2, 276(fp)
[0x800008a4]:lw t5, 304(a1)
[0x800008a8]:lw t4, 308(a1)
[0x800008ac]:addi s1, zero, 2
[0x800008b0]:csrrw zero, fcsr, s1
[0x800008b4]:fdiv.s t6, t5, t4, dyn

[0x800008b4]:fdiv.s t6, t5, t4, dyn
[0x800008b8]:csrrs a2, fcsr, zero
[0x800008bc]:sw t6, 280(fp)
[0x800008c0]:sw a2, 284(fp)
[0x800008c4]:lw t5, 312(a1)
[0x800008c8]:lw t4, 316(a1)
[0x800008cc]:addi s1, zero, 34
[0x800008d0]:csrrw zero, fcsr, s1
[0x800008d4]:fdiv.s t6, t5, t4, dyn

[0x800008d4]:fdiv.s t6, t5, t4, dyn
[0x800008d8]:csrrs a2, fcsr, zero
[0x800008dc]:sw t6, 288(fp)
[0x800008e0]:sw a2, 292(fp)
[0x800008e4]:lw t5, 320(a1)
[0x800008e8]:lw t4, 324(a1)
[0x800008ec]:addi s1, zero, 66
[0x800008f0]:csrrw zero, fcsr, s1
[0x800008f4]:fdiv.s t6, t5, t4, dyn

[0x800008f4]:fdiv.s t6, t5, t4, dyn
[0x800008f8]:csrrs a2, fcsr, zero
[0x800008fc]:sw t6, 296(fp)
[0x80000900]:sw a2, 300(fp)
[0x80000904]:lw t5, 328(a1)
[0x80000908]:lw t4, 332(a1)
[0x8000090c]:addi s1, zero, 98
[0x80000910]:csrrw zero, fcsr, s1
[0x80000914]:fdiv.s t6, t5, t4, dyn

[0x80000914]:fdiv.s t6, t5, t4, dyn
[0x80000918]:csrrs a2, fcsr, zero
[0x8000091c]:sw t6, 304(fp)
[0x80000920]:sw a2, 308(fp)
[0x80000924]:lw t5, 336(a1)
[0x80000928]:lw t4, 340(a1)
[0x8000092c]:addi s1, zero, 130
[0x80000930]:csrrw zero, fcsr, s1
[0x80000934]:fdiv.s t6, t5, t4, dyn

[0x80000934]:fdiv.s t6, t5, t4, dyn
[0x80000938]:csrrs a2, fcsr, zero
[0x8000093c]:sw t6, 312(fp)
[0x80000940]:sw a2, 316(fp)
[0x80000944]:lw t5, 344(a1)
[0x80000948]:lw t4, 348(a1)
[0x8000094c]:addi s1, zero, 2
[0x80000950]:csrrw zero, fcsr, s1
[0x80000954]:fdiv.s t6, t5, t4, dyn

[0x80000954]:fdiv.s t6, t5, t4, dyn
[0x80000958]:csrrs a2, fcsr, zero
[0x8000095c]:sw t6, 320(fp)
[0x80000960]:sw a2, 324(fp)
[0x80000964]:lw t5, 352(a1)
[0x80000968]:lw t4, 356(a1)
[0x8000096c]:addi s1, zero, 34
[0x80000970]:csrrw zero, fcsr, s1
[0x80000974]:fdiv.s t6, t5, t4, dyn

[0x80000974]:fdiv.s t6, t5, t4, dyn
[0x80000978]:csrrs a2, fcsr, zero
[0x8000097c]:sw t6, 328(fp)
[0x80000980]:sw a2, 332(fp)
[0x80000984]:lw t5, 360(a1)
[0x80000988]:lw t4, 364(a1)
[0x8000098c]:addi s1, zero, 66
[0x80000990]:csrrw zero, fcsr, s1
[0x80000994]:fdiv.s t6, t5, t4, dyn

[0x80000994]:fdiv.s t6, t5, t4, dyn
[0x80000998]:csrrs a2, fcsr, zero
[0x8000099c]:sw t6, 336(fp)
[0x800009a0]:sw a2, 340(fp)
[0x800009a4]:lw t5, 368(a1)
[0x800009a8]:lw t4, 372(a1)
[0x800009ac]:addi s1, zero, 98
[0x800009b0]:csrrw zero, fcsr, s1
[0x800009b4]:fdiv.s t6, t5, t4, dyn

[0x800009b4]:fdiv.s t6, t5, t4, dyn
[0x800009b8]:csrrs a2, fcsr, zero
[0x800009bc]:sw t6, 344(fp)
[0x800009c0]:sw a2, 348(fp)
[0x800009c4]:lw t5, 376(a1)
[0x800009c8]:lw t4, 380(a1)
[0x800009cc]:addi s1, zero, 130
[0x800009d0]:csrrw zero, fcsr, s1
[0x800009d4]:fdiv.s t6, t5, t4, dyn

[0x800009d4]:fdiv.s t6, t5, t4, dyn
[0x800009d8]:csrrs a2, fcsr, zero
[0x800009dc]:sw t6, 352(fp)
[0x800009e0]:sw a2, 356(fp)
[0x800009e4]:lw t5, 384(a1)
[0x800009e8]:lw t4, 388(a1)
[0x800009ec]:addi s1, zero, 2
[0x800009f0]:csrrw zero, fcsr, s1
[0x800009f4]:fdiv.s t6, t5, t4, dyn

[0x800009f4]:fdiv.s t6, t5, t4, dyn
[0x800009f8]:csrrs a2, fcsr, zero
[0x800009fc]:sw t6, 360(fp)
[0x80000a00]:sw a2, 364(fp)
[0x80000a04]:lw t5, 392(a1)
[0x80000a08]:lw t4, 396(a1)
[0x80000a0c]:addi s1, zero, 34
[0x80000a10]:csrrw zero, fcsr, s1
[0x80000a14]:fdiv.s t6, t5, t4, dyn

[0x80000a14]:fdiv.s t6, t5, t4, dyn
[0x80000a18]:csrrs a2, fcsr, zero
[0x80000a1c]:sw t6, 368(fp)
[0x80000a20]:sw a2, 372(fp)
[0x80000a24]:lw t5, 400(a1)
[0x80000a28]:lw t4, 404(a1)
[0x80000a2c]:addi s1, zero, 66
[0x80000a30]:csrrw zero, fcsr, s1
[0x80000a34]:fdiv.s t6, t5, t4, dyn

[0x80000a34]:fdiv.s t6, t5, t4, dyn
[0x80000a38]:csrrs a2, fcsr, zero
[0x80000a3c]:sw t6, 376(fp)
[0x80000a40]:sw a2, 380(fp)
[0x80000a44]:lw t5, 408(a1)
[0x80000a48]:lw t4, 412(a1)
[0x80000a4c]:addi s1, zero, 98
[0x80000a50]:csrrw zero, fcsr, s1
[0x80000a54]:fdiv.s t6, t5, t4, dyn

[0x80000a54]:fdiv.s t6, t5, t4, dyn
[0x80000a58]:csrrs a2, fcsr, zero
[0x80000a5c]:sw t6, 384(fp)
[0x80000a60]:sw a2, 388(fp)
[0x80000a64]:lw t5, 416(a1)
[0x80000a68]:lw t4, 420(a1)
[0x80000a6c]:addi s1, zero, 130
[0x80000a70]:csrrw zero, fcsr, s1
[0x80000a74]:fdiv.s t6, t5, t4, dyn

[0x80000a74]:fdiv.s t6, t5, t4, dyn
[0x80000a78]:csrrs a2, fcsr, zero
[0x80000a7c]:sw t6, 392(fp)
[0x80000a80]:sw a2, 396(fp)
[0x80000a84]:lw t5, 424(a1)
[0x80000a88]:lw t4, 428(a1)
[0x80000a8c]:addi s1, zero, 2
[0x80000a90]:csrrw zero, fcsr, s1
[0x80000a94]:fdiv.s t6, t5, t4, dyn

[0x80000a94]:fdiv.s t6, t5, t4, dyn
[0x80000a98]:csrrs a2, fcsr, zero
[0x80000a9c]:sw t6, 400(fp)
[0x80000aa0]:sw a2, 404(fp)
[0x80000aa4]:lw t5, 432(a1)
[0x80000aa8]:lw t4, 436(a1)
[0x80000aac]:addi s1, zero, 34
[0x80000ab0]:csrrw zero, fcsr, s1
[0x80000ab4]:fdiv.s t6, t5, t4, dyn

[0x80000ab4]:fdiv.s t6, t5, t4, dyn
[0x80000ab8]:csrrs a2, fcsr, zero
[0x80000abc]:sw t6, 408(fp)
[0x80000ac0]:sw a2, 412(fp)
[0x80000ac4]:lw t5, 440(a1)
[0x80000ac8]:lw t4, 444(a1)
[0x80000acc]:addi s1, zero, 66
[0x80000ad0]:csrrw zero, fcsr, s1
[0x80000ad4]:fdiv.s t6, t5, t4, dyn

[0x80000ad4]:fdiv.s t6, t5, t4, dyn
[0x80000ad8]:csrrs a2, fcsr, zero
[0x80000adc]:sw t6, 416(fp)
[0x80000ae0]:sw a2, 420(fp)
[0x80000ae4]:lw t5, 448(a1)
[0x80000ae8]:lw t4, 452(a1)
[0x80000aec]:addi s1, zero, 98
[0x80000af0]:csrrw zero, fcsr, s1
[0x80000af4]:fdiv.s t6, t5, t4, dyn

[0x80000af4]:fdiv.s t6, t5, t4, dyn
[0x80000af8]:csrrs a2, fcsr, zero
[0x80000afc]:sw t6, 424(fp)
[0x80000b00]:sw a2, 428(fp)
[0x80000b04]:lw t5, 456(a1)
[0x80000b08]:lw t4, 460(a1)
[0x80000b0c]:addi s1, zero, 130
[0x80000b10]:csrrw zero, fcsr, s1
[0x80000b14]:fdiv.s t6, t5, t4, dyn

[0x80000b14]:fdiv.s t6, t5, t4, dyn
[0x80000b18]:csrrs a2, fcsr, zero
[0x80000b1c]:sw t6, 432(fp)
[0x80000b20]:sw a2, 436(fp)
[0x80000b24]:lw t5, 464(a1)
[0x80000b28]:lw t4, 468(a1)
[0x80000b2c]:addi s1, zero, 2
[0x80000b30]:csrrw zero, fcsr, s1
[0x80000b34]:fdiv.s t6, t5, t4, dyn

[0x80000b34]:fdiv.s t6, t5, t4, dyn
[0x80000b38]:csrrs a2, fcsr, zero
[0x80000b3c]:sw t6, 440(fp)
[0x80000b40]:sw a2, 444(fp)
[0x80000b44]:lw t5, 472(a1)
[0x80000b48]:lw t4, 476(a1)
[0x80000b4c]:addi s1, zero, 34
[0x80000b50]:csrrw zero, fcsr, s1
[0x80000b54]:fdiv.s t6, t5, t4, dyn

[0x80000b54]:fdiv.s t6, t5, t4, dyn
[0x80000b58]:csrrs a2, fcsr, zero
[0x80000b5c]:sw t6, 448(fp)
[0x80000b60]:sw a2, 452(fp)
[0x80000b64]:lw t5, 480(a1)
[0x80000b68]:lw t4, 484(a1)
[0x80000b6c]:addi s1, zero, 66
[0x80000b70]:csrrw zero, fcsr, s1
[0x80000b74]:fdiv.s t6, t5, t4, dyn

[0x80000b74]:fdiv.s t6, t5, t4, dyn
[0x80000b78]:csrrs a2, fcsr, zero
[0x80000b7c]:sw t6, 456(fp)
[0x80000b80]:sw a2, 460(fp)
[0x80000b84]:lw t5, 488(a1)
[0x80000b88]:lw t4, 492(a1)
[0x80000b8c]:addi s1, zero, 98
[0x80000b90]:csrrw zero, fcsr, s1
[0x80000b94]:fdiv.s t6, t5, t4, dyn

[0x80000b94]:fdiv.s t6, t5, t4, dyn
[0x80000b98]:csrrs a2, fcsr, zero
[0x80000b9c]:sw t6, 464(fp)
[0x80000ba0]:sw a2, 468(fp)
[0x80000ba4]:lw t5, 496(a1)
[0x80000ba8]:lw t4, 500(a1)
[0x80000bac]:addi s1, zero, 130
[0x80000bb0]:csrrw zero, fcsr, s1
[0x80000bb4]:fdiv.s t6, t5, t4, dyn

[0x80000bb4]:fdiv.s t6, t5, t4, dyn
[0x80000bb8]:csrrs a2, fcsr, zero
[0x80000bbc]:sw t6, 472(fp)
[0x80000bc0]:sw a2, 476(fp)
[0x80000bc4]:lw t5, 504(a1)
[0x80000bc8]:lw t4, 508(a1)
[0x80000bcc]:addi s1, zero, 2
[0x80000bd0]:csrrw zero, fcsr, s1
[0x80000bd4]:fdiv.s t6, t5, t4, dyn

[0x80000bd4]:fdiv.s t6, t5, t4, dyn
[0x80000bd8]:csrrs a2, fcsr, zero
[0x80000bdc]:sw t6, 480(fp)
[0x80000be0]:sw a2, 484(fp)
[0x80000be4]:lw t5, 512(a1)
[0x80000be8]:lw t4, 516(a1)
[0x80000bec]:addi s1, zero, 34
[0x80000bf0]:csrrw zero, fcsr, s1
[0x80000bf4]:fdiv.s t6, t5, t4, dyn

[0x80000bf4]:fdiv.s t6, t5, t4, dyn
[0x80000bf8]:csrrs a2, fcsr, zero
[0x80000bfc]:sw t6, 488(fp)
[0x80000c00]:sw a2, 492(fp)
[0x80000c04]:lw t5, 520(a1)
[0x80000c08]:lw t4, 524(a1)
[0x80000c0c]:addi s1, zero, 66
[0x80000c10]:csrrw zero, fcsr, s1
[0x80000c14]:fdiv.s t6, t5, t4, dyn

[0x80000c14]:fdiv.s t6, t5, t4, dyn
[0x80000c18]:csrrs a2, fcsr, zero
[0x80000c1c]:sw t6, 496(fp)
[0x80000c20]:sw a2, 500(fp)
[0x80000c24]:lw t5, 528(a1)
[0x80000c28]:lw t4, 532(a1)
[0x80000c2c]:addi s1, zero, 98
[0x80000c30]:csrrw zero, fcsr, s1
[0x80000c34]:fdiv.s t6, t5, t4, dyn

[0x80000c34]:fdiv.s t6, t5, t4, dyn
[0x80000c38]:csrrs a2, fcsr, zero
[0x80000c3c]:sw t6, 504(fp)
[0x80000c40]:sw a2, 508(fp)
[0x80000c44]:lw t5, 536(a1)
[0x80000c48]:lw t4, 540(a1)
[0x80000c4c]:addi s1, zero, 130
[0x80000c50]:csrrw zero, fcsr, s1
[0x80000c54]:fdiv.s t6, t5, t4, dyn

[0x80000c54]:fdiv.s t6, t5, t4, dyn
[0x80000c58]:csrrs a2, fcsr, zero
[0x80000c5c]:sw t6, 512(fp)
[0x80000c60]:sw a2, 516(fp)
[0x80000c64]:lw t5, 544(a1)
[0x80000c68]:lw t4, 548(a1)
[0x80000c6c]:addi s1, zero, 2
[0x80000c70]:csrrw zero, fcsr, s1
[0x80000c74]:fdiv.s t6, t5, t4, dyn

[0x80000c74]:fdiv.s t6, t5, t4, dyn
[0x80000c78]:csrrs a2, fcsr, zero
[0x80000c7c]:sw t6, 520(fp)
[0x80000c80]:sw a2, 524(fp)
[0x80000c84]:lw t5, 552(a1)
[0x80000c88]:lw t4, 556(a1)
[0x80000c8c]:addi s1, zero, 34
[0x80000c90]:csrrw zero, fcsr, s1
[0x80000c94]:fdiv.s t6, t5, t4, dyn

[0x80000c94]:fdiv.s t6, t5, t4, dyn
[0x80000c98]:csrrs a2, fcsr, zero
[0x80000c9c]:sw t6, 528(fp)
[0x80000ca0]:sw a2, 532(fp)
[0x80000ca4]:lw t5, 560(a1)
[0x80000ca8]:lw t4, 564(a1)
[0x80000cac]:addi s1, zero, 66
[0x80000cb0]:csrrw zero, fcsr, s1
[0x80000cb4]:fdiv.s t6, t5, t4, dyn

[0x80000cb4]:fdiv.s t6, t5, t4, dyn
[0x80000cb8]:csrrs a2, fcsr, zero
[0x80000cbc]:sw t6, 536(fp)
[0x80000cc0]:sw a2, 540(fp)
[0x80000cc4]:lw t5, 568(a1)
[0x80000cc8]:lw t4, 572(a1)
[0x80000ccc]:addi s1, zero, 98
[0x80000cd0]:csrrw zero, fcsr, s1
[0x80000cd4]:fdiv.s t6, t5, t4, dyn

[0x80000cd4]:fdiv.s t6, t5, t4, dyn
[0x80000cd8]:csrrs a2, fcsr, zero
[0x80000cdc]:sw t6, 544(fp)
[0x80000ce0]:sw a2, 548(fp)
[0x80000ce4]:lw t5, 576(a1)
[0x80000ce8]:lw t4, 580(a1)
[0x80000cec]:addi s1, zero, 130
[0x80000cf0]:csrrw zero, fcsr, s1
[0x80000cf4]:fdiv.s t6, t5, t4, dyn

[0x80000cf4]:fdiv.s t6, t5, t4, dyn
[0x80000cf8]:csrrs a2, fcsr, zero
[0x80000cfc]:sw t6, 552(fp)
[0x80000d00]:sw a2, 556(fp)
[0x80000d04]:lw t5, 584(a1)
[0x80000d08]:lw t4, 588(a1)
[0x80000d0c]:addi s1, zero, 2
[0x80000d10]:csrrw zero, fcsr, s1
[0x80000d14]:fdiv.s t6, t5, t4, dyn

[0x80000d14]:fdiv.s t6, t5, t4, dyn
[0x80000d18]:csrrs a2, fcsr, zero
[0x80000d1c]:sw t6, 560(fp)
[0x80000d20]:sw a2, 564(fp)
[0x80000d24]:lw t5, 592(a1)
[0x80000d28]:lw t4, 596(a1)
[0x80000d2c]:addi s1, zero, 34
[0x80000d30]:csrrw zero, fcsr, s1
[0x80000d34]:fdiv.s t6, t5, t4, dyn

[0x80000d34]:fdiv.s t6, t5, t4, dyn
[0x80000d38]:csrrs a2, fcsr, zero
[0x80000d3c]:sw t6, 568(fp)
[0x80000d40]:sw a2, 572(fp)
[0x80000d44]:lw t5, 600(a1)
[0x80000d48]:lw t4, 604(a1)
[0x80000d4c]:addi s1, zero, 66
[0x80000d50]:csrrw zero, fcsr, s1
[0x80000d54]:fdiv.s t6, t5, t4, dyn

[0x80000d54]:fdiv.s t6, t5, t4, dyn
[0x80000d58]:csrrs a2, fcsr, zero
[0x80000d5c]:sw t6, 576(fp)
[0x80000d60]:sw a2, 580(fp)
[0x80000d64]:lw t5, 608(a1)
[0x80000d68]:lw t4, 612(a1)
[0x80000d6c]:addi s1, zero, 98
[0x80000d70]:csrrw zero, fcsr, s1
[0x80000d74]:fdiv.s t6, t5, t4, dyn

[0x80000d74]:fdiv.s t6, t5, t4, dyn
[0x80000d78]:csrrs a2, fcsr, zero
[0x80000d7c]:sw t6, 584(fp)
[0x80000d80]:sw a2, 588(fp)
[0x80000d84]:lw t5, 616(a1)
[0x80000d88]:lw t4, 620(a1)
[0x80000d8c]:addi s1, zero, 130
[0x80000d90]:csrrw zero, fcsr, s1
[0x80000d94]:fdiv.s t6, t5, t4, dyn

[0x80000d94]:fdiv.s t6, t5, t4, dyn
[0x80000d98]:csrrs a2, fcsr, zero
[0x80000d9c]:sw t6, 592(fp)
[0x80000da0]:sw a2, 596(fp)
[0x80000da4]:lw t5, 624(a1)
[0x80000da8]:lw t4, 628(a1)
[0x80000dac]:addi s1, zero, 2
[0x80000db0]:csrrw zero, fcsr, s1
[0x80000db4]:fdiv.s t6, t5, t4, dyn

[0x80000db4]:fdiv.s t6, t5, t4, dyn
[0x80000db8]:csrrs a2, fcsr, zero
[0x80000dbc]:sw t6, 600(fp)
[0x80000dc0]:sw a2, 604(fp)
[0x80000dc4]:lw t5, 632(a1)
[0x80000dc8]:lw t4, 636(a1)
[0x80000dcc]:addi s1, zero, 34
[0x80000dd0]:csrrw zero, fcsr, s1
[0x80000dd4]:fdiv.s t6, t5, t4, dyn

[0x80000dd4]:fdiv.s t6, t5, t4, dyn
[0x80000dd8]:csrrs a2, fcsr, zero
[0x80000ddc]:sw t6, 608(fp)
[0x80000de0]:sw a2, 612(fp)
[0x80000de4]:lw t5, 640(a1)
[0x80000de8]:lw t4, 644(a1)
[0x80000dec]:addi s1, zero, 66
[0x80000df0]:csrrw zero, fcsr, s1
[0x80000df4]:fdiv.s t6, t5, t4, dyn

[0x80000df4]:fdiv.s t6, t5, t4, dyn
[0x80000df8]:csrrs a2, fcsr, zero
[0x80000dfc]:sw t6, 616(fp)
[0x80000e00]:sw a2, 620(fp)
[0x80000e04]:lw t5, 648(a1)
[0x80000e08]:lw t4, 652(a1)
[0x80000e0c]:addi s1, zero, 98
[0x80000e10]:csrrw zero, fcsr, s1
[0x80000e14]:fdiv.s t6, t5, t4, dyn

[0x80000e14]:fdiv.s t6, t5, t4, dyn
[0x80000e18]:csrrs a2, fcsr, zero
[0x80000e1c]:sw t6, 624(fp)
[0x80000e20]:sw a2, 628(fp)
[0x80000e24]:lw t5, 656(a1)
[0x80000e28]:lw t4, 660(a1)
[0x80000e2c]:addi s1, zero, 130
[0x80000e30]:csrrw zero, fcsr, s1
[0x80000e34]:fdiv.s t6, t5, t4, dyn

[0x80000e34]:fdiv.s t6, t5, t4, dyn
[0x80000e38]:csrrs a2, fcsr, zero
[0x80000e3c]:sw t6, 632(fp)
[0x80000e40]:sw a2, 636(fp)
[0x80000e44]:lw t5, 664(a1)
[0x80000e48]:lw t4, 668(a1)
[0x80000e4c]:addi s1, zero, 2
[0x80000e50]:csrrw zero, fcsr, s1
[0x80000e54]:fdiv.s t6, t5, t4, dyn

[0x80000e54]:fdiv.s t6, t5, t4, dyn
[0x80000e58]:csrrs a2, fcsr, zero
[0x80000e5c]:sw t6, 640(fp)
[0x80000e60]:sw a2, 644(fp)
[0x80000e64]:lw t5, 672(a1)
[0x80000e68]:lw t4, 676(a1)
[0x80000e6c]:addi s1, zero, 34
[0x80000e70]:csrrw zero, fcsr, s1
[0x80000e74]:fdiv.s t6, t5, t4, dyn

[0x80000e74]:fdiv.s t6, t5, t4, dyn
[0x80000e78]:csrrs a2, fcsr, zero
[0x80000e7c]:sw t6, 648(fp)
[0x80000e80]:sw a2, 652(fp)
[0x80000e84]:lw t5, 680(a1)
[0x80000e88]:lw t4, 684(a1)
[0x80000e8c]:addi s1, zero, 66
[0x80000e90]:csrrw zero, fcsr, s1
[0x80000e94]:fdiv.s t6, t5, t4, dyn

[0x80000e94]:fdiv.s t6, t5, t4, dyn
[0x80000e98]:csrrs a2, fcsr, zero
[0x80000e9c]:sw t6, 656(fp)
[0x80000ea0]:sw a2, 660(fp)
[0x80000ea4]:lw t5, 688(a1)
[0x80000ea8]:lw t4, 692(a1)
[0x80000eac]:addi s1, zero, 98
[0x80000eb0]:csrrw zero, fcsr, s1
[0x80000eb4]:fdiv.s t6, t5, t4, dyn

[0x80000eb4]:fdiv.s t6, t5, t4, dyn
[0x80000eb8]:csrrs a2, fcsr, zero
[0x80000ebc]:sw t6, 664(fp)
[0x80000ec0]:sw a2, 668(fp)
[0x80000ec4]:lw t5, 696(a1)
[0x80000ec8]:lw t4, 700(a1)
[0x80000ecc]:addi s1, zero, 130
[0x80000ed0]:csrrw zero, fcsr, s1
[0x80000ed4]:fdiv.s t6, t5, t4, dyn

[0x80000ed4]:fdiv.s t6, t5, t4, dyn
[0x80000ed8]:csrrs a2, fcsr, zero
[0x80000edc]:sw t6, 672(fp)
[0x80000ee0]:sw a2, 676(fp)
[0x80000ee4]:lw t5, 704(a1)
[0x80000ee8]:lw t4, 708(a1)
[0x80000eec]:addi s1, zero, 2
[0x80000ef0]:csrrw zero, fcsr, s1
[0x80000ef4]:fdiv.s t6, t5, t4, dyn

[0x80000ef4]:fdiv.s t6, t5, t4, dyn
[0x80000ef8]:csrrs a2, fcsr, zero
[0x80000efc]:sw t6, 680(fp)
[0x80000f00]:sw a2, 684(fp)
[0x80000f04]:lw t5, 712(a1)
[0x80000f08]:lw t4, 716(a1)
[0x80000f0c]:addi s1, zero, 34
[0x80000f10]:csrrw zero, fcsr, s1
[0x80000f14]:fdiv.s t6, t5, t4, dyn

[0x80000f14]:fdiv.s t6, t5, t4, dyn
[0x80000f18]:csrrs a2, fcsr, zero
[0x80000f1c]:sw t6, 688(fp)
[0x80000f20]:sw a2, 692(fp)
[0x80000f24]:lw t5, 720(a1)
[0x80000f28]:lw t4, 724(a1)
[0x80000f2c]:addi s1, zero, 66
[0x80000f30]:csrrw zero, fcsr, s1
[0x80000f34]:fdiv.s t6, t5, t4, dyn

[0x80000f34]:fdiv.s t6, t5, t4, dyn
[0x80000f38]:csrrs a2, fcsr, zero
[0x80000f3c]:sw t6, 696(fp)
[0x80000f40]:sw a2, 700(fp)
[0x80000f44]:lw t5, 728(a1)
[0x80000f48]:lw t4, 732(a1)
[0x80000f4c]:addi s1, zero, 98
[0x80000f50]:csrrw zero, fcsr, s1
[0x80000f54]:fdiv.s t6, t5, t4, dyn

[0x80000f54]:fdiv.s t6, t5, t4, dyn
[0x80000f58]:csrrs a2, fcsr, zero
[0x80000f5c]:sw t6, 704(fp)
[0x80000f60]:sw a2, 708(fp)
[0x80000f64]:lw t5, 736(a1)
[0x80000f68]:lw t4, 740(a1)
[0x80000f6c]:addi s1, zero, 130
[0x80000f70]:csrrw zero, fcsr, s1
[0x80000f74]:fdiv.s t6, t5, t4, dyn

[0x80000f74]:fdiv.s t6, t5, t4, dyn
[0x80000f78]:csrrs a2, fcsr, zero
[0x80000f7c]:sw t6, 712(fp)
[0x80000f80]:sw a2, 716(fp)
[0x80000f84]:lw t5, 744(a1)
[0x80000f88]:lw t4, 748(a1)
[0x80000f8c]:addi s1, zero, 2
[0x80000f90]:csrrw zero, fcsr, s1
[0x80000f94]:fdiv.s t6, t5, t4, dyn

[0x80000f94]:fdiv.s t6, t5, t4, dyn
[0x80000f98]:csrrs a2, fcsr, zero
[0x80000f9c]:sw t6, 720(fp)
[0x80000fa0]:sw a2, 724(fp)
[0x80000fa4]:lw t5, 752(a1)
[0x80000fa8]:lw t4, 756(a1)
[0x80000fac]:addi s1, zero, 34
[0x80000fb0]:csrrw zero, fcsr, s1
[0x80000fb4]:fdiv.s t6, t5, t4, dyn

[0x80000fb4]:fdiv.s t6, t5, t4, dyn
[0x80000fb8]:csrrs a2, fcsr, zero
[0x80000fbc]:sw t6, 728(fp)
[0x80000fc0]:sw a2, 732(fp)
[0x80000fc4]:lw t5, 760(a1)
[0x80000fc8]:lw t4, 764(a1)
[0x80000fcc]:addi s1, zero, 66
[0x80000fd0]:csrrw zero, fcsr, s1
[0x80000fd4]:fdiv.s t6, t5, t4, dyn

[0x80000fd4]:fdiv.s t6, t5, t4, dyn
[0x80000fd8]:csrrs a2, fcsr, zero
[0x80000fdc]:sw t6, 736(fp)
[0x80000fe0]:sw a2, 740(fp)
[0x80000fe4]:lw t5, 768(a1)
[0x80000fe8]:lw t4, 772(a1)
[0x80000fec]:addi s1, zero, 98
[0x80000ff0]:csrrw zero, fcsr, s1
[0x80000ff4]:fdiv.s t6, t5, t4, dyn

[0x80000ff4]:fdiv.s t6, t5, t4, dyn
[0x80000ff8]:csrrs a2, fcsr, zero
[0x80000ffc]:sw t6, 744(fp)
[0x80001000]:sw a2, 748(fp)
[0x80001004]:lw t5, 776(a1)
[0x80001008]:lw t4, 780(a1)
[0x8000100c]:addi s1, zero, 130
[0x80001010]:csrrw zero, fcsr, s1
[0x80001014]:fdiv.s t6, t5, t4, dyn

[0x80001014]:fdiv.s t6, t5, t4, dyn
[0x80001018]:csrrs a2, fcsr, zero
[0x8000101c]:sw t6, 752(fp)
[0x80001020]:sw a2, 756(fp)
[0x80001024]:lw t5, 784(a1)
[0x80001028]:lw t4, 788(a1)
[0x8000102c]:addi s1, zero, 2
[0x80001030]:csrrw zero, fcsr, s1
[0x80001034]:fdiv.s t6, t5, t4, dyn

[0x80001034]:fdiv.s t6, t5, t4, dyn
[0x80001038]:csrrs a2, fcsr, zero
[0x8000103c]:sw t6, 760(fp)
[0x80001040]:sw a2, 764(fp)
[0x80001044]:lw t5, 792(a1)
[0x80001048]:lw t4, 796(a1)
[0x8000104c]:addi s1, zero, 34
[0x80001050]:csrrw zero, fcsr, s1
[0x80001054]:fdiv.s t6, t5, t4, dyn

[0x80001054]:fdiv.s t6, t5, t4, dyn
[0x80001058]:csrrs a2, fcsr, zero
[0x8000105c]:sw t6, 768(fp)
[0x80001060]:sw a2, 772(fp)
[0x80001064]:lw t5, 800(a1)
[0x80001068]:lw t4, 804(a1)
[0x8000106c]:addi s1, zero, 66
[0x80001070]:csrrw zero, fcsr, s1
[0x80001074]:fdiv.s t6, t5, t4, dyn

[0x80001074]:fdiv.s t6, t5, t4, dyn
[0x80001078]:csrrs a2, fcsr, zero
[0x8000107c]:sw t6, 776(fp)
[0x80001080]:sw a2, 780(fp)
[0x80001084]:lw t5, 808(a1)
[0x80001088]:lw t4, 812(a1)
[0x8000108c]:addi s1, zero, 98
[0x80001090]:csrrw zero, fcsr, s1
[0x80001094]:fdiv.s t6, t5, t4, dyn

[0x80001094]:fdiv.s t6, t5, t4, dyn
[0x80001098]:csrrs a2, fcsr, zero
[0x8000109c]:sw t6, 784(fp)
[0x800010a0]:sw a2, 788(fp)
[0x800010a4]:lw t5, 816(a1)
[0x800010a8]:lw t4, 820(a1)
[0x800010ac]:addi s1, zero, 130
[0x800010b0]:csrrw zero, fcsr, s1
[0x800010b4]:fdiv.s t6, t5, t4, dyn

[0x800010b4]:fdiv.s t6, t5, t4, dyn
[0x800010b8]:csrrs a2, fcsr, zero
[0x800010bc]:sw t6, 792(fp)
[0x800010c0]:sw a2, 796(fp)
[0x800010c4]:lw t5, 824(a1)
[0x800010c8]:lw t4, 828(a1)
[0x800010cc]:addi s1, zero, 2
[0x800010d0]:csrrw zero, fcsr, s1
[0x800010d4]:fdiv.s t6, t5, t4, dyn

[0x800010d4]:fdiv.s t6, t5, t4, dyn
[0x800010d8]:csrrs a2, fcsr, zero
[0x800010dc]:sw t6, 800(fp)
[0x800010e0]:sw a2, 804(fp)
[0x800010e4]:lw t5, 832(a1)
[0x800010e8]:lw t4, 836(a1)
[0x800010ec]:addi s1, zero, 34
[0x800010f0]:csrrw zero, fcsr, s1
[0x800010f4]:fdiv.s t6, t5, t4, dyn

[0x800010f4]:fdiv.s t6, t5, t4, dyn
[0x800010f8]:csrrs a2, fcsr, zero
[0x800010fc]:sw t6, 808(fp)
[0x80001100]:sw a2, 812(fp)
[0x80001104]:lw t5, 840(a1)
[0x80001108]:lw t4, 844(a1)
[0x8000110c]:addi s1, zero, 66
[0x80001110]:csrrw zero, fcsr, s1
[0x80001114]:fdiv.s t6, t5, t4, dyn

[0x80001114]:fdiv.s t6, t5, t4, dyn
[0x80001118]:csrrs a2, fcsr, zero
[0x8000111c]:sw t6, 816(fp)
[0x80001120]:sw a2, 820(fp)
[0x80001124]:lw t5, 848(a1)
[0x80001128]:lw t4, 852(a1)
[0x8000112c]:addi s1, zero, 98
[0x80001130]:csrrw zero, fcsr, s1
[0x80001134]:fdiv.s t6, t5, t4, dyn

[0x80001134]:fdiv.s t6, t5, t4, dyn
[0x80001138]:csrrs a2, fcsr, zero
[0x8000113c]:sw t6, 824(fp)
[0x80001140]:sw a2, 828(fp)
[0x80001144]:lw t5, 856(a1)
[0x80001148]:lw t4, 860(a1)
[0x8000114c]:addi s1, zero, 130
[0x80001150]:csrrw zero, fcsr, s1
[0x80001154]:fdiv.s t6, t5, t4, dyn

[0x80001154]:fdiv.s t6, t5, t4, dyn
[0x80001158]:csrrs a2, fcsr, zero
[0x8000115c]:sw t6, 832(fp)
[0x80001160]:sw a2, 836(fp)
[0x80001164]:lw t5, 864(a1)
[0x80001168]:lw t4, 868(a1)
[0x8000116c]:addi s1, zero, 2
[0x80001170]:csrrw zero, fcsr, s1
[0x80001174]:fdiv.s t6, t5, t4, dyn

[0x80001174]:fdiv.s t6, t5, t4, dyn
[0x80001178]:csrrs a2, fcsr, zero
[0x8000117c]:sw t6, 840(fp)
[0x80001180]:sw a2, 844(fp)
[0x80001184]:lw t5, 872(a1)
[0x80001188]:lw t4, 876(a1)
[0x8000118c]:addi s1, zero, 34
[0x80001190]:csrrw zero, fcsr, s1
[0x80001194]:fdiv.s t6, t5, t4, dyn

[0x80001194]:fdiv.s t6, t5, t4, dyn
[0x80001198]:csrrs a2, fcsr, zero
[0x8000119c]:sw t6, 848(fp)
[0x800011a0]:sw a2, 852(fp)
[0x800011a4]:lw t5, 880(a1)
[0x800011a8]:lw t4, 884(a1)
[0x800011ac]:addi s1, zero, 66
[0x800011b0]:csrrw zero, fcsr, s1
[0x800011b4]:fdiv.s t6, t5, t4, dyn

[0x800011b4]:fdiv.s t6, t5, t4, dyn
[0x800011b8]:csrrs a2, fcsr, zero
[0x800011bc]:sw t6, 856(fp)
[0x800011c0]:sw a2, 860(fp)
[0x800011c4]:lw t5, 888(a1)
[0x800011c8]:lw t4, 892(a1)
[0x800011cc]:addi s1, zero, 98
[0x800011d0]:csrrw zero, fcsr, s1
[0x800011d4]:fdiv.s t6, t5, t4, dyn

[0x800011d4]:fdiv.s t6, t5, t4, dyn
[0x800011d8]:csrrs a2, fcsr, zero
[0x800011dc]:sw t6, 864(fp)
[0x800011e0]:sw a2, 868(fp)
[0x800011e4]:lw t5, 896(a1)
[0x800011e8]:lw t4, 900(a1)
[0x800011ec]:addi s1, zero, 130
[0x800011f0]:csrrw zero, fcsr, s1
[0x800011f4]:fdiv.s t6, t5, t4, dyn

[0x800011f4]:fdiv.s t6, t5, t4, dyn
[0x800011f8]:csrrs a2, fcsr, zero
[0x800011fc]:sw t6, 872(fp)
[0x80001200]:sw a2, 876(fp)
[0x80001204]:lw t5, 904(a1)
[0x80001208]:lw t4, 908(a1)
[0x8000120c]:addi s1, zero, 2
[0x80001210]:csrrw zero, fcsr, s1
[0x80001214]:fdiv.s t6, t5, t4, dyn

[0x80001214]:fdiv.s t6, t5, t4, dyn
[0x80001218]:csrrs a2, fcsr, zero
[0x8000121c]:sw t6, 880(fp)
[0x80001220]:sw a2, 884(fp)
[0x80001224]:lw t5, 912(a1)
[0x80001228]:lw t4, 916(a1)
[0x8000122c]:addi s1, zero, 34
[0x80001230]:csrrw zero, fcsr, s1
[0x80001234]:fdiv.s t6, t5, t4, dyn

[0x80001234]:fdiv.s t6, t5, t4, dyn
[0x80001238]:csrrs a2, fcsr, zero
[0x8000123c]:sw t6, 888(fp)
[0x80001240]:sw a2, 892(fp)
[0x80001244]:lw t5, 920(a1)
[0x80001248]:lw t4, 924(a1)
[0x8000124c]:addi s1, zero, 66
[0x80001250]:csrrw zero, fcsr, s1
[0x80001254]:fdiv.s t6, t5, t4, dyn

[0x80001254]:fdiv.s t6, t5, t4, dyn
[0x80001258]:csrrs a2, fcsr, zero
[0x8000125c]:sw t6, 896(fp)
[0x80001260]:sw a2, 900(fp)
[0x80001264]:lw t5, 928(a1)
[0x80001268]:lw t4, 932(a1)
[0x8000126c]:addi s1, zero, 98
[0x80001270]:csrrw zero, fcsr, s1
[0x80001274]:fdiv.s t6, t5, t4, dyn

[0x80001274]:fdiv.s t6, t5, t4, dyn
[0x80001278]:csrrs a2, fcsr, zero
[0x8000127c]:sw t6, 904(fp)
[0x80001280]:sw a2, 908(fp)
[0x80001284]:lw t5, 936(a1)
[0x80001288]:lw t4, 940(a1)
[0x8000128c]:addi s1, zero, 130
[0x80001290]:csrrw zero, fcsr, s1
[0x80001294]:fdiv.s t6, t5, t4, dyn

[0x80001294]:fdiv.s t6, t5, t4, dyn
[0x80001298]:csrrs a2, fcsr, zero
[0x8000129c]:sw t6, 912(fp)
[0x800012a0]:sw a2, 916(fp)
[0x800012a4]:lw t5, 944(a1)
[0x800012a8]:lw t4, 948(a1)
[0x800012ac]:addi s1, zero, 2
[0x800012b0]:csrrw zero, fcsr, s1
[0x800012b4]:fdiv.s t6, t5, t4, dyn

[0x800012b4]:fdiv.s t6, t5, t4, dyn
[0x800012b8]:csrrs a2, fcsr, zero
[0x800012bc]:sw t6, 920(fp)
[0x800012c0]:sw a2, 924(fp)
[0x800012c4]:lw t5, 952(a1)
[0x800012c8]:lw t4, 956(a1)
[0x800012cc]:addi s1, zero, 66
[0x800012d0]:csrrw zero, fcsr, s1
[0x800012d4]:fdiv.s t6, t5, t4, dyn

[0x800012d4]:fdiv.s t6, t5, t4, dyn
[0x800012d8]:csrrs a2, fcsr, zero
[0x800012dc]:sw t6, 928(fp)
[0x800012e0]:sw a2, 932(fp)
[0x800012e4]:lw t5, 960(a1)
[0x800012e8]:lw t4, 964(a1)
[0x800012ec]:addi s1, zero, 2
[0x800012f0]:csrrw zero, fcsr, s1
[0x800012f4]:fdiv.s t6, t5, t4, dyn

[0x800012f4]:fdiv.s t6, t5, t4, dyn
[0x800012f8]:csrrs a2, fcsr, zero
[0x800012fc]:sw t6, 936(fp)
[0x80001300]:sw a2, 940(fp)
[0x80001304]:lw t5, 968(a1)
[0x80001308]:lw t4, 972(a1)
[0x8000130c]:addi s1, zero, 34
[0x80001310]:csrrw zero, fcsr, s1
[0x80001314]:fdiv.s t6, t5, t4, dyn

[0x80001314]:fdiv.s t6, t5, t4, dyn
[0x80001318]:csrrs a2, fcsr, zero
[0x8000131c]:sw t6, 944(fp)
[0x80001320]:sw a2, 948(fp)
[0x80001324]:lw t5, 976(a1)
[0x80001328]:lw t4, 980(a1)
[0x8000132c]:addi s1, zero, 98
[0x80001330]:csrrw zero, fcsr, s1
[0x80001334]:fdiv.s t6, t5, t4, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000124]:fdiv.s t6, t6, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
	-[0x80000130]:sw tp, 4(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003618]:0x00000002




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.s t5, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
	-[0x80000150]:sw tp, 12(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80003620]:0x00000023




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fdiv.s t4, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t4, 16(ra)
	-[0x80000170]:sw tp, 20(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003628]:0x00000042




Last Coverpoint : ['rs1 : x30', 'rs2 : x29', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.s t3, t5, t4, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
	-[0x80000190]:sw tp, 28(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80003630]:0x00000063




Last Coverpoint : ['rs1 : x27', 'rs2 : x26', 'rd : x27', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fdiv.s s11, s11, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
	-[0x800001b0]:sw tp, 36(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003638]:0x00000083




Last Coverpoint : ['rs1 : x25', 'rs2 : x27', 'rd : x26', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.s s10, s9, s11, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
	-[0x800001d0]:sw tp, 44(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80003640]:0x00000003




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x25', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.s s9, s10, s8, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
	-[0x800001f0]:sw tp, 52(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003648]:0x00000023




Last Coverpoint : ['rs1 : x23', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.s s8, s7, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
	-[0x80000210]:sw tp, 60(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80003650]:0x00000043




Last Coverpoint : ['rs1 : x24', 'rs2 : x22', 'rd : x23', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.s s7, s8, s6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
	-[0x80000230]:sw tp, 68(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003658]:0x00000063




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x537439 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x62aedc and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.s s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
	-[0x80000250]:sw tp, 76(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80003660]:0x00000083




Last Coverpoint : ['rs1 : x22', 'rs2 : x20', 'rd : x21', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.s s5, s6, s4, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
	-[0x80000270]:sw tp, 84(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003668]:0x00000003




Last Coverpoint : ['rs1 : x19', 'rs2 : x21', 'rd : x20', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.s s4, s3, s5, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
	-[0x80000290]:sw tp, 92(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80003670]:0x00000023




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x19', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.s s3, s4, s2, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
	-[0x800002b0]:sw tp, 100(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003678]:0x00000043




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.s s2, a7, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
	-[0x800002d0]:sw tp, 108(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80003680]:0x00000063




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x17', 'fs1 == 0 and fe1 == 0x17 and fm1 == 0x73ac65 and fs2 == 1 and fe2 == 0xaf and fm2 == 0x029c88 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.s a7, s2, a6, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
	-[0x800002f0]:sw tp, 116(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003688]:0x00000083




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.s a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
	-[0x80000310]:sw tp, 124(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80003690]:0x00000003




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x15', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.s a5, a6, a4, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
	-[0x80000330]:sw tp, 132(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003698]:0x00000023




Last Coverpoint : ['rs1 : x13', 'rs2 : x15', 'rd : x14', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.s a4, a3, a5, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
	-[0x80000350]:sw tp, 140(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800036a0]:0x00000043




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x13', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.s a3, a4, a2, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
	-[0x80000370]:sw tp, 148(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800036a8]:0x00000063




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x079c52 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x11609d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.s a2, a1, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
	-[0x80000390]:sw tp, 156(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800036b0]:0x00000083




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x11', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.s a1, a2, a0, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
	-[0x800003b0]:sw tp, 164(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800036b8]:0x00000003




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.s a0, s1, a1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
	-[0x800003d0]:sw tp, 172(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800036c0]:0x00000023




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x9', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.s s1, a0, fp, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
	-[0x800003f8]:sw a2, 180(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800036c8]:0x00000043




Last Coverpoint : ['rs1 : x7', 'rs2 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.s fp, t2, s1, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
	-[0x80000418]:sw a2, 188(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800036d0]:0x00000063




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x7', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x775fc0 and fs2 == 1 and fe2 == 0xb2 and fm2 == 0x049853 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.s t2, fp, t1, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
	-[0x80000438]:sw a2, 196(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800036d8]:0x00000083




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.s t1, t0, t2, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
	-[0x80000460]:sw a2, 4(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800036e0]:0x00000003




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x5', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.s t0, t1, tp, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
	-[0x80000480]:sw a2, 12(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800036e8]:0x00000023




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.s tp, gp, t0, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
	-[0x800004a0]:sw a2, 20(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800036f0]:0x00000043




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x3', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.s gp, tp, sp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
	-[0x800004c0]:sw a2, 28(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800036f8]:0x00000063




Last Coverpoint : ['rs1 : x1', 'rs2 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x108952 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x1af22d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.s sp, ra, gp, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
	-[0x800004e0]:sw a2, 36(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x80003700]:0x00000083




Last Coverpoint : ['rs1 : x2', 'rs2 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.s ra, sp, zero, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
	-[0x80000500]:sw a2, 44(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80003708]:0x0000000A




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000514]:fdiv.s t6, zero, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
	-[0x80000520]:sw a2, 52(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x80003710]:0x00000022




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fdiv.s t6, t5, ra, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
	-[0x80000540]:sw a2, 60(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80003718]:0x00000043




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.s zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
	-[0x80000560]:sw a2, 68(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x80003720]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
	-[0x80000580]:sw a2, 76(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80003728]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
	-[0x800005a0]:sw a2, 84(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x80003730]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
	-[0x800005c0]:sw a2, 92(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80003738]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
	-[0x800005e0]:sw a2, 100(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x80003740]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
	-[0x80000600]:sw a2, 108(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80003748]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1243d4 and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x6facab and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
	-[0x80000620]:sw a2, 116(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x80003750]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
	-[0x80000640]:sw a2, 124(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80003758]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
	-[0x80000660]:sw a2, 132(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x80003760]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
	-[0x80000680]:sw a2, 140(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80003768]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
	-[0x800006a0]:sw a2, 148(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x80003770]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x041f59 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x588008 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
	-[0x800006c0]:sw a2, 156(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80003778]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
	-[0x800006e0]:sw a2, 164(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x80003780]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
	-[0x80000700]:sw a2, 172(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80003788]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
	-[0x80000720]:sw a2, 180(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x80003790]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
	-[0x80000740]:sw a2, 188(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80003798]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1862ba and fs2 == 1 and fe2 == 0xb5 and fm2 == 0x79b43d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
	-[0x80000760]:sw a2, 196(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x800037a0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
	-[0x80000780]:sw a2, 204(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800037a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
	-[0x800007a0]:sw a2, 212(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800037b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
	-[0x800007c0]:sw a2, 220(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800037b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
	-[0x800007e0]:sw a2, 228(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800037c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0c94bf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x665c52 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
	-[0x80000800]:sw a2, 236(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800037c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
	-[0x80000820]:sw a2, 244(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800037d0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
	-[0x80000840]:sw a2, 252(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800037d8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
	-[0x80000860]:sw a2, 260(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800037e0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
	-[0x80000880]:sw a2, 268(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800037e8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x16c9d3 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x771633 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
	-[0x800008a0]:sw a2, 276(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800037f0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
	-[0x800008c0]:sw a2, 284(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800037f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
	-[0x800008e0]:sw a2, 292(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x80003800]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
	-[0x80000900]:sw a2, 300(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80003808]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
	-[0x80000920]:sw a2, 308(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x80003810]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0e1a47 and fs2 == 1 and fe2 == 0xb4 and fm2 == 0x68da9e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
	-[0x80000940]:sw a2, 316(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80003818]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
	-[0x80000960]:sw a2, 324(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x80003820]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
	-[0x80000980]:sw a2, 332(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80003828]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
	-[0x800009a0]:sw a2, 340(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x80003830]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
	-[0x800009c0]:sw a2, 348(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80003838]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1747cc and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x77e49e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
	-[0x800009e0]:sw a2, 356(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x80003840]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
	-[0x80000a00]:sw a2, 364(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80003848]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
	-[0x80000a20]:sw a2, 372(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x80003850]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
	-[0x80000a40]:sw a2, 380(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80003858]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
	-[0x80000a60]:sw a2, 388(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x80003860]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0dff3c and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x39ea30 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
	-[0x80000a80]:sw a2, 396(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80003868]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
	-[0x80000aa0]:sw a2, 404(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x80003870]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
	-[0x80000ac0]:sw a2, 412(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80003878]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
	-[0x80000ae0]:sw a2, 420(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x80003880]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
	-[0x80000b00]:sw a2, 428(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80003888]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3c143a and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x763fd6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
	-[0x80000b20]:sw a2, 436(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x80003890]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
	-[0x80000b40]:sw a2, 444(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80003898]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
	-[0x80000b60]:sw a2, 452(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x800038a0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
	-[0x80000b80]:sw a2, 460(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800038a8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
	-[0x80000ba0]:sw a2, 468(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800038b0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x07e7a0 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x31f037 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
	-[0x80000bc0]:sw a2, 476(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800038b8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
	-[0x80000be0]:sw a2, 484(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800038c0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
	-[0x80000c00]:sw a2, 492(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800038c8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
	-[0x80000c20]:sw a2, 500(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800038d0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
	-[0x80000c40]:sw a2, 508(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800038d8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x506fba and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x087397 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
	-[0x80000c60]:sw a2, 516(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800038e0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
	-[0x80000c80]:sw a2, 524(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800038e8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
	-[0x80000ca0]:sw a2, 532(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800038f0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
	-[0x80000cc0]:sw a2, 540(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800038f8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
	-[0x80000ce0]:sw a2, 548(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x80003900]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x210ca3 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x52dc1c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
	-[0x80000d00]:sw a2, 556(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80003908]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
	-[0x80000d20]:sw a2, 564(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x80003910]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
	-[0x80000d40]:sw a2, 572(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80003918]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
	-[0x80000d60]:sw a2, 580(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x80003920]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
	-[0x80000d80]:sw a2, 588(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80003928]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1cec94 and fs2 == 0 and fe2 == 0xb0 and fm2 == 0x4d756d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
	-[0x80000da0]:sw a2, 596(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x80003930]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
	-[0x80000dc0]:sw a2, 604(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80003938]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
	-[0x80000de0]:sw a2, 612(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x80003940]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
	-[0x80000e00]:sw a2, 620(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80003948]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
	-[0x80000e20]:sw a2, 628(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x80003950]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x6a2294 and fs2 == 0 and fe2 == 0xb3 and fm2 == 0x194664 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
	-[0x80000e40]:sw a2, 636(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80003958]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
	-[0x80000e60]:sw a2, 644(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x80003960]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
	-[0x80000e80]:sw a2, 652(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80003968]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
	-[0x80000ea0]:sw a2, 660(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x80003970]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
	-[0x80000ec0]:sw a2, 668(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80003978]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x17f238 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x7221c9 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
	-[0x80000ee0]:sw a2, 676(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x80003980]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
	-[0x80000f00]:sw a2, 684(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80003988]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fdiv.s t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
	-[0x80000f20]:sw a2, 692(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x80003990]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fdiv.s t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
	-[0x80000f40]:sw a2, 700(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80003998]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f54]:fdiv.s t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
	-[0x80000f60]:sw a2, 708(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x800039a0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x06ae55 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x569e87 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fdiv.s t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
	-[0x80000f80]:sw a2, 716(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x800039a8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f94]:fdiv.s t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
	-[0x80000fa0]:sw a2, 724(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x800039b0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
	-[0x80000fc0]:sw a2, 732(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x800039b8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
	-[0x80000fe0]:sw a2, 740(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x800039c0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
	-[0x80001000]:sw a2, 748(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x800039c8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x44e5bd and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x1ce1ba and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001014]:fdiv.s t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
	-[0x80001020]:sw a2, 756(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x800039d0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fdiv.s t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
	-[0x80001040]:sw a2, 764(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x800039d8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001054]:fdiv.s t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
	-[0x80001060]:sw a2, 772(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x800039e0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001074]:fdiv.s t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
	-[0x80001080]:sw a2, 780(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x800039e8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fdiv.s t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
	-[0x800010a0]:sw a2, 788(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x800039f0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0f30be and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x642de8 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b4]:fdiv.s t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
	-[0x800010c0]:sw a2, 796(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x800039f8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fdiv.s t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
	-[0x800010e0]:sw a2, 804(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x80003a00]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f4]:fdiv.s t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
	-[0x80001100]:sw a2, 812(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80003a08]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001114]:fdiv.s t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
	-[0x80001120]:sw a2, 820(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x80003a10]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001134]:fdiv.s t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
	-[0x80001140]:sw a2, 828(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80003a18]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x23f956 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x02a63c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fdiv.s t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
	-[0x80001160]:sw a2, 836(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x80003a20]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001174]:fdiv.s t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
	-[0x80001180]:sw a2, 844(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80003a28]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001194]:fdiv.s t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
	-[0x800011a0]:sw a2, 852(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x80003a30]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fdiv.s t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
	-[0x800011c0]:sw a2, 860(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80003a38]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d4]:fdiv.s t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
	-[0x800011e0]:sw a2, 868(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x80003a40]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x16 and fm1 == 0x3344b3 and fs2 == 0 and fe2 == 0xae and fm2 == 0x0ed5e0 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fdiv.s t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
	-[0x80001200]:sw a2, 876(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80003a48]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001214]:fdiv.s t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
	-[0x80001220]:sw a2, 884(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x80003a50]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001234]:fdiv.s t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
	-[0x80001240]:sw a2, 892(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80003a58]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fdiv.s t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
	-[0x80001260]:sw a2, 900(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x80003a60]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fdiv.s t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
	-[0x80001280]:sw a2, 908(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80003a68]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0162e0 and fs2 == 0 and fe2 == 0xb2 and fm2 == 0x4e2e8e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001294]:fdiv.s t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
	-[0x800012a0]:sw a2, 916(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x80003a70]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b4]:fdiv.s t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
	-[0x800012c0]:sw a2, 924(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80003a78]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x20c723 and fs2 == 1 and fe2 == 0xb1 and fm2 == 0x2c5b71 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fdiv.s t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
	-[0x800012e0]:sw a2, 932(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x80003a80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f4]:fdiv.s t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
	-[0x80001300]:sw a2, 940(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80003a88]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fdiv.s t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
	-[0x80001320]:sw a2, 948(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x80003a90]:0x00000023




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x1479a4 and fs2 == 1 and fe2 == 0xb0 and fm2 == 0x1f2b1f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001334]:fdiv.s t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
	-[0x80001340]:sw a2, 956(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80003a98]:0x00000063





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
