
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001d50')]      |
| SIG_REGION                | [('0x80003910', '0x80004020', '452 words')]      |
| COV_LABELS                | fdiv_b5      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zfinx-denorm/work-fdivall-nov17/fdiv_b5-01.S/ref.S    |
| Total Number of coverpoints| 322     |
| Total Coverpoints Hit     | 322      |
| Total Signature Updates   | 450      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 224     |
| STAT4                     | 225     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80001d3c]:fdiv.s t6, t5, t4, dyn
      [0x80001d40]:csrrs a2, fcsr, zero
      [0x80001d44]:sw t6, 568(fp)
      [0x80001d48]:sw a2, 572(fp)
      [0x80001d4c]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80004014 Data: 0x3F2099C1
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat






```

## Details of STAT3

```
[0x80000124]:fdiv.s t6, t6, t6, dyn
[0x80000128]:csrrs tp, fcsr, zero
[0x8000012c]:sw t6, 0(ra)
[0x80000130]:sw tp, 4(ra)
[0x80000134]:lw t4, 8(gp)
[0x80000138]:lw t5, 12(gp)
[0x8000013c]:addi sp, zero, 34
[0x80000140]:csrrw zero, fcsr, sp
[0x80000144]:fdiv.s t5, t4, t5, dyn

[0x80000144]:fdiv.s t5, t4, t5, dyn
[0x80000148]:csrrs tp, fcsr, zero
[0x8000014c]:sw t5, 8(ra)
[0x80000150]:sw tp, 12(ra)
[0x80000154]:lw t3, 16(gp)
[0x80000158]:lw t3, 20(gp)
[0x8000015c]:addi sp, zero, 66
[0x80000160]:csrrw zero, fcsr, sp
[0x80000164]:fdiv.s t4, t3, t3, dyn

[0x80000164]:fdiv.s t4, t3, t3, dyn
[0x80000168]:csrrs tp, fcsr, zero
[0x8000016c]:sw t4, 16(ra)
[0x80000170]:sw tp, 20(ra)
[0x80000174]:lw t5, 24(gp)
[0x80000178]:lw t4, 28(gp)
[0x8000017c]:addi sp, zero, 98
[0x80000180]:csrrw zero, fcsr, sp
[0x80000184]:fdiv.s t3, t5, t4, dyn

[0x80000184]:fdiv.s t3, t5, t4, dyn
[0x80000188]:csrrs tp, fcsr, zero
[0x8000018c]:sw t3, 24(ra)
[0x80000190]:sw tp, 28(ra)
[0x80000194]:lw s11, 32(gp)
[0x80000198]:lw s10, 36(gp)
[0x8000019c]:addi sp, zero, 130
[0x800001a0]:csrrw zero, fcsr, sp
[0x800001a4]:fdiv.s s11, s11, s10, dyn

[0x800001a4]:fdiv.s s11, s11, s10, dyn
[0x800001a8]:csrrs tp, fcsr, zero
[0x800001ac]:sw s11, 32(ra)
[0x800001b0]:sw tp, 36(ra)
[0x800001b4]:lw s9, 40(gp)
[0x800001b8]:lw s11, 44(gp)
[0x800001bc]:addi sp, zero, 2
[0x800001c0]:csrrw zero, fcsr, sp
[0x800001c4]:fdiv.s s10, s9, s11, dyn

[0x800001c4]:fdiv.s s10, s9, s11, dyn
[0x800001c8]:csrrs tp, fcsr, zero
[0x800001cc]:sw s10, 40(ra)
[0x800001d0]:sw tp, 44(ra)
[0x800001d4]:lw s10, 48(gp)
[0x800001d8]:lw s8, 52(gp)
[0x800001dc]:addi sp, zero, 34
[0x800001e0]:csrrw zero, fcsr, sp
[0x800001e4]:fdiv.s s9, s10, s8, dyn

[0x800001e4]:fdiv.s s9, s10, s8, dyn
[0x800001e8]:csrrs tp, fcsr, zero
[0x800001ec]:sw s9, 48(ra)
[0x800001f0]:sw tp, 52(ra)
[0x800001f4]:lw s7, 56(gp)
[0x800001f8]:lw s9, 60(gp)
[0x800001fc]:addi sp, zero, 66
[0x80000200]:csrrw zero, fcsr, sp
[0x80000204]:fdiv.s s8, s7, s9, dyn

[0x80000204]:fdiv.s s8, s7, s9, dyn
[0x80000208]:csrrs tp, fcsr, zero
[0x8000020c]:sw s8, 56(ra)
[0x80000210]:sw tp, 60(ra)
[0x80000214]:lw s8, 64(gp)
[0x80000218]:lw s6, 68(gp)
[0x8000021c]:addi sp, zero, 98
[0x80000220]:csrrw zero, fcsr, sp
[0x80000224]:fdiv.s s7, s8, s6, dyn

[0x80000224]:fdiv.s s7, s8, s6, dyn
[0x80000228]:csrrs tp, fcsr, zero
[0x8000022c]:sw s7, 64(ra)
[0x80000230]:sw tp, 68(ra)
[0x80000234]:lw s5, 72(gp)
[0x80000238]:lw s7, 76(gp)
[0x8000023c]:addi sp, zero, 130
[0x80000240]:csrrw zero, fcsr, sp
[0x80000244]:fdiv.s s6, s5, s7, dyn

[0x80000244]:fdiv.s s6, s5, s7, dyn
[0x80000248]:csrrs tp, fcsr, zero
[0x8000024c]:sw s6, 72(ra)
[0x80000250]:sw tp, 76(ra)
[0x80000254]:lw s6, 80(gp)
[0x80000258]:lw s4, 84(gp)
[0x8000025c]:addi sp, zero, 2
[0x80000260]:csrrw zero, fcsr, sp
[0x80000264]:fdiv.s s5, s6, s4, dyn

[0x80000264]:fdiv.s s5, s6, s4, dyn
[0x80000268]:csrrs tp, fcsr, zero
[0x8000026c]:sw s5, 80(ra)
[0x80000270]:sw tp, 84(ra)
[0x80000274]:lw s3, 88(gp)
[0x80000278]:lw s5, 92(gp)
[0x8000027c]:addi sp, zero, 34
[0x80000280]:csrrw zero, fcsr, sp
[0x80000284]:fdiv.s s4, s3, s5, dyn

[0x80000284]:fdiv.s s4, s3, s5, dyn
[0x80000288]:csrrs tp, fcsr, zero
[0x8000028c]:sw s4, 88(ra)
[0x80000290]:sw tp, 92(ra)
[0x80000294]:lw s4, 96(gp)
[0x80000298]:lw s2, 100(gp)
[0x8000029c]:addi sp, zero, 66
[0x800002a0]:csrrw zero, fcsr, sp
[0x800002a4]:fdiv.s s3, s4, s2, dyn

[0x800002a4]:fdiv.s s3, s4, s2, dyn
[0x800002a8]:csrrs tp, fcsr, zero
[0x800002ac]:sw s3, 96(ra)
[0x800002b0]:sw tp, 100(ra)
[0x800002b4]:lw a7, 104(gp)
[0x800002b8]:lw s3, 108(gp)
[0x800002bc]:addi sp, zero, 98
[0x800002c0]:csrrw zero, fcsr, sp
[0x800002c4]:fdiv.s s2, a7, s3, dyn

[0x800002c4]:fdiv.s s2, a7, s3, dyn
[0x800002c8]:csrrs tp, fcsr, zero
[0x800002cc]:sw s2, 104(ra)
[0x800002d0]:sw tp, 108(ra)
[0x800002d4]:lw s2, 112(gp)
[0x800002d8]:lw a6, 116(gp)
[0x800002dc]:addi sp, zero, 130
[0x800002e0]:csrrw zero, fcsr, sp
[0x800002e4]:fdiv.s a7, s2, a6, dyn

[0x800002e4]:fdiv.s a7, s2, a6, dyn
[0x800002e8]:csrrs tp, fcsr, zero
[0x800002ec]:sw a7, 112(ra)
[0x800002f0]:sw tp, 116(ra)
[0x800002f4]:lw a5, 120(gp)
[0x800002f8]:lw a7, 124(gp)
[0x800002fc]:addi sp, zero, 2
[0x80000300]:csrrw zero, fcsr, sp
[0x80000304]:fdiv.s a6, a5, a7, dyn

[0x80000304]:fdiv.s a6, a5, a7, dyn
[0x80000308]:csrrs tp, fcsr, zero
[0x8000030c]:sw a6, 120(ra)
[0x80000310]:sw tp, 124(ra)
[0x80000314]:lw a6, 128(gp)
[0x80000318]:lw a4, 132(gp)
[0x8000031c]:addi sp, zero, 34
[0x80000320]:csrrw zero, fcsr, sp
[0x80000324]:fdiv.s a5, a6, a4, dyn

[0x80000324]:fdiv.s a5, a6, a4, dyn
[0x80000328]:csrrs tp, fcsr, zero
[0x8000032c]:sw a5, 128(ra)
[0x80000330]:sw tp, 132(ra)
[0x80000334]:lw a3, 136(gp)
[0x80000338]:lw a5, 140(gp)
[0x8000033c]:addi sp, zero, 66
[0x80000340]:csrrw zero, fcsr, sp
[0x80000344]:fdiv.s a4, a3, a5, dyn

[0x80000344]:fdiv.s a4, a3, a5, dyn
[0x80000348]:csrrs tp, fcsr, zero
[0x8000034c]:sw a4, 136(ra)
[0x80000350]:sw tp, 140(ra)
[0x80000354]:lw a4, 144(gp)
[0x80000358]:lw a2, 148(gp)
[0x8000035c]:addi sp, zero, 98
[0x80000360]:csrrw zero, fcsr, sp
[0x80000364]:fdiv.s a3, a4, a2, dyn

[0x80000364]:fdiv.s a3, a4, a2, dyn
[0x80000368]:csrrs tp, fcsr, zero
[0x8000036c]:sw a3, 144(ra)
[0x80000370]:sw tp, 148(ra)
[0x80000374]:lw a1, 152(gp)
[0x80000378]:lw a3, 156(gp)
[0x8000037c]:addi sp, zero, 130
[0x80000380]:csrrw zero, fcsr, sp
[0x80000384]:fdiv.s a2, a1, a3, dyn

[0x80000384]:fdiv.s a2, a1, a3, dyn
[0x80000388]:csrrs tp, fcsr, zero
[0x8000038c]:sw a2, 152(ra)
[0x80000390]:sw tp, 156(ra)
[0x80000394]:lw a2, 160(gp)
[0x80000398]:lw a0, 164(gp)
[0x8000039c]:addi sp, zero, 2
[0x800003a0]:csrrw zero, fcsr, sp
[0x800003a4]:fdiv.s a1, a2, a0, dyn

[0x800003a4]:fdiv.s a1, a2, a0, dyn
[0x800003a8]:csrrs tp, fcsr, zero
[0x800003ac]:sw a1, 160(ra)
[0x800003b0]:sw tp, 164(ra)
[0x800003b4]:lw s1, 168(gp)
[0x800003b8]:lw a1, 172(gp)
[0x800003bc]:addi sp, zero, 34
[0x800003c0]:csrrw zero, fcsr, sp
[0x800003c4]:fdiv.s a0, s1, a1, dyn

[0x800003c4]:fdiv.s a0, s1, a1, dyn
[0x800003c8]:csrrs tp, fcsr, zero
[0x800003cc]:sw a0, 168(ra)
[0x800003d0]:sw tp, 172(ra)
[0x800003d4]:auipc a1, 3
[0x800003d8]:addi a1, a1, 3308
[0x800003dc]:lw a0, 0(a1)
[0x800003e0]:lw fp, 4(a1)
[0x800003e4]:addi sp, zero, 66
[0x800003e8]:csrrw zero, fcsr, sp
[0x800003ec]:fdiv.s s1, a0, fp, dyn

[0x800003ec]:fdiv.s s1, a0, fp, dyn
[0x800003f0]:csrrs a2, fcsr, zero
[0x800003f4]:sw s1, 176(ra)
[0x800003f8]:sw a2, 180(ra)
[0x800003fc]:lw t2, 8(a1)
[0x80000400]:lw s1, 12(a1)
[0x80000404]:addi sp, zero, 98
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fdiv.s fp, t2, s1, dyn

[0x8000040c]:fdiv.s fp, t2, s1, dyn
[0x80000410]:csrrs a2, fcsr, zero
[0x80000414]:sw fp, 184(ra)
[0x80000418]:sw a2, 188(ra)
[0x8000041c]:lw fp, 16(a1)
[0x80000420]:lw t1, 20(a1)
[0x80000424]:addi s1, zero, 130
[0x80000428]:csrrw zero, fcsr, s1
[0x8000042c]:fdiv.s t2, fp, t1, dyn

[0x8000042c]:fdiv.s t2, fp, t1, dyn
[0x80000430]:csrrs a2, fcsr, zero
[0x80000434]:sw t2, 192(ra)
[0x80000438]:sw a2, 196(ra)
[0x8000043c]:auipc fp, 3
[0x80000440]:addi fp, fp, 1440
[0x80000444]:lw t0, 24(a1)
[0x80000448]:lw t2, 28(a1)
[0x8000044c]:addi s1, zero, 2
[0x80000450]:csrrw zero, fcsr, s1
[0x80000454]:fdiv.s t1, t0, t2, dyn

[0x80000454]:fdiv.s t1, t0, t2, dyn
[0x80000458]:csrrs a2, fcsr, zero
[0x8000045c]:sw t1, 0(fp)
[0x80000460]:sw a2, 4(fp)
[0x80000464]:lw t1, 32(a1)
[0x80000468]:lw tp, 36(a1)
[0x8000046c]:addi s1, zero, 34
[0x80000470]:csrrw zero, fcsr, s1
[0x80000474]:fdiv.s t0, t1, tp, dyn

[0x80000474]:fdiv.s t0, t1, tp, dyn
[0x80000478]:csrrs a2, fcsr, zero
[0x8000047c]:sw t0, 8(fp)
[0x80000480]:sw a2, 12(fp)
[0x80000484]:lw gp, 40(a1)
[0x80000488]:lw t0, 44(a1)
[0x8000048c]:addi s1, zero, 66
[0x80000490]:csrrw zero, fcsr, s1
[0x80000494]:fdiv.s tp, gp, t0, dyn

[0x80000494]:fdiv.s tp, gp, t0, dyn
[0x80000498]:csrrs a2, fcsr, zero
[0x8000049c]:sw tp, 16(fp)
[0x800004a0]:sw a2, 20(fp)
[0x800004a4]:lw tp, 48(a1)
[0x800004a8]:lw sp, 52(a1)
[0x800004ac]:addi s1, zero, 98
[0x800004b0]:csrrw zero, fcsr, s1
[0x800004b4]:fdiv.s gp, tp, sp, dyn

[0x800004b4]:fdiv.s gp, tp, sp, dyn
[0x800004b8]:csrrs a2, fcsr, zero
[0x800004bc]:sw gp, 24(fp)
[0x800004c0]:sw a2, 28(fp)
[0x800004c4]:lw ra, 56(a1)
[0x800004c8]:lw gp, 60(a1)
[0x800004cc]:addi s1, zero, 130
[0x800004d0]:csrrw zero, fcsr, s1
[0x800004d4]:fdiv.s sp, ra, gp, dyn

[0x800004d4]:fdiv.s sp, ra, gp, dyn
[0x800004d8]:csrrs a2, fcsr, zero
[0x800004dc]:sw sp, 32(fp)
[0x800004e0]:sw a2, 36(fp)
[0x800004e4]:lw sp, 64(a1)
[0x800004e8]:lw zero, 68(a1)
[0x800004ec]:addi s1, zero, 2
[0x800004f0]:csrrw zero, fcsr, s1
[0x800004f4]:fdiv.s ra, sp, zero, dyn

[0x800004f4]:fdiv.s ra, sp, zero, dyn
[0x800004f8]:csrrs a2, fcsr, zero
[0x800004fc]:sw ra, 40(fp)
[0x80000500]:sw a2, 44(fp)
[0x80000504]:lw zero, 72(a1)
[0x80000508]:lw t5, 76(a1)
[0x8000050c]:addi s1, zero, 34
[0x80000510]:csrrw zero, fcsr, s1
[0x80000514]:fdiv.s t6, zero, t5, dyn

[0x80000514]:fdiv.s t6, zero, t5, dyn
[0x80000518]:csrrs a2, fcsr, zero
[0x8000051c]:sw t6, 48(fp)
[0x80000520]:sw a2, 52(fp)
[0x80000524]:lw t5, 80(a1)
[0x80000528]:lw ra, 84(a1)
[0x8000052c]:addi s1, zero, 66
[0x80000530]:csrrw zero, fcsr, s1
[0x80000534]:fdiv.s t6, t5, ra, dyn

[0x80000534]:fdiv.s t6, t5, ra, dyn
[0x80000538]:csrrs a2, fcsr, zero
[0x8000053c]:sw t6, 56(fp)
[0x80000540]:sw a2, 60(fp)
[0x80000544]:lw t6, 88(a1)
[0x80000548]:lw t5, 92(a1)
[0x8000054c]:addi s1, zero, 98
[0x80000550]:csrrw zero, fcsr, s1
[0x80000554]:fdiv.s zero, t6, t5, dyn

[0x80000554]:fdiv.s zero, t6, t5, dyn
[0x80000558]:csrrs a2, fcsr, zero
[0x8000055c]:sw zero, 64(fp)
[0x80000560]:sw a2, 68(fp)
[0x80000564]:lw t5, 96(a1)
[0x80000568]:lw t4, 100(a1)
[0x8000056c]:addi s1, zero, 130
[0x80000570]:csrrw zero, fcsr, s1
[0x80000574]:fdiv.s t6, t5, t4, dyn

[0x80000574]:fdiv.s t6, t5, t4, dyn
[0x80000578]:csrrs a2, fcsr, zero
[0x8000057c]:sw t6, 72(fp)
[0x80000580]:sw a2, 76(fp)
[0x80000584]:lw t5, 104(a1)
[0x80000588]:lw t4, 108(a1)
[0x8000058c]:addi s1, zero, 2
[0x80000590]:csrrw zero, fcsr, s1
[0x80000594]:fdiv.s t6, t5, t4, dyn

[0x80000594]:fdiv.s t6, t5, t4, dyn
[0x80000598]:csrrs a2, fcsr, zero
[0x8000059c]:sw t6, 80(fp)
[0x800005a0]:sw a2, 84(fp)
[0x800005a4]:lw t5, 112(a1)
[0x800005a8]:lw t4, 116(a1)
[0x800005ac]:addi s1, zero, 34
[0x800005b0]:csrrw zero, fcsr, s1
[0x800005b4]:fdiv.s t6, t5, t4, dyn

[0x800005b4]:fdiv.s t6, t5, t4, dyn
[0x800005b8]:csrrs a2, fcsr, zero
[0x800005bc]:sw t6, 88(fp)
[0x800005c0]:sw a2, 92(fp)
[0x800005c4]:lw t5, 120(a1)
[0x800005c8]:lw t4, 124(a1)
[0x800005cc]:addi s1, zero, 66
[0x800005d0]:csrrw zero, fcsr, s1
[0x800005d4]:fdiv.s t6, t5, t4, dyn

[0x800005d4]:fdiv.s t6, t5, t4, dyn
[0x800005d8]:csrrs a2, fcsr, zero
[0x800005dc]:sw t6, 96(fp)
[0x800005e0]:sw a2, 100(fp)
[0x800005e4]:lw t5, 128(a1)
[0x800005e8]:lw t4, 132(a1)
[0x800005ec]:addi s1, zero, 98
[0x800005f0]:csrrw zero, fcsr, s1
[0x800005f4]:fdiv.s t6, t5, t4, dyn

[0x800005f4]:fdiv.s t6, t5, t4, dyn
[0x800005f8]:csrrs a2, fcsr, zero
[0x800005fc]:sw t6, 104(fp)
[0x80000600]:sw a2, 108(fp)
[0x80000604]:lw t5, 136(a1)
[0x80000608]:lw t4, 140(a1)
[0x8000060c]:addi s1, zero, 130
[0x80000610]:csrrw zero, fcsr, s1
[0x80000614]:fdiv.s t6, t5, t4, dyn

[0x80000614]:fdiv.s t6, t5, t4, dyn
[0x80000618]:csrrs a2, fcsr, zero
[0x8000061c]:sw t6, 112(fp)
[0x80000620]:sw a2, 116(fp)
[0x80000624]:lw t5, 144(a1)
[0x80000628]:lw t4, 148(a1)
[0x8000062c]:addi s1, zero, 2
[0x80000630]:csrrw zero, fcsr, s1
[0x80000634]:fdiv.s t6, t5, t4, dyn

[0x80000634]:fdiv.s t6, t5, t4, dyn
[0x80000638]:csrrs a2, fcsr, zero
[0x8000063c]:sw t6, 120(fp)
[0x80000640]:sw a2, 124(fp)
[0x80000644]:lw t5, 152(a1)
[0x80000648]:lw t4, 156(a1)
[0x8000064c]:addi s1, zero, 34
[0x80000650]:csrrw zero, fcsr, s1
[0x80000654]:fdiv.s t6, t5, t4, dyn

[0x80000654]:fdiv.s t6, t5, t4, dyn
[0x80000658]:csrrs a2, fcsr, zero
[0x8000065c]:sw t6, 128(fp)
[0x80000660]:sw a2, 132(fp)
[0x80000664]:lw t5, 160(a1)
[0x80000668]:lw t4, 164(a1)
[0x8000066c]:addi s1, zero, 66
[0x80000670]:csrrw zero, fcsr, s1
[0x80000674]:fdiv.s t6, t5, t4, dyn

[0x80000674]:fdiv.s t6, t5, t4, dyn
[0x80000678]:csrrs a2, fcsr, zero
[0x8000067c]:sw t6, 136(fp)
[0x80000680]:sw a2, 140(fp)
[0x80000684]:lw t5, 168(a1)
[0x80000688]:lw t4, 172(a1)
[0x8000068c]:addi s1, zero, 98
[0x80000690]:csrrw zero, fcsr, s1
[0x80000694]:fdiv.s t6, t5, t4, dyn

[0x80000694]:fdiv.s t6, t5, t4, dyn
[0x80000698]:csrrs a2, fcsr, zero
[0x8000069c]:sw t6, 144(fp)
[0x800006a0]:sw a2, 148(fp)
[0x800006a4]:lw t5, 176(a1)
[0x800006a8]:lw t4, 180(a1)
[0x800006ac]:addi s1, zero, 130
[0x800006b0]:csrrw zero, fcsr, s1
[0x800006b4]:fdiv.s t6, t5, t4, dyn

[0x800006b4]:fdiv.s t6, t5, t4, dyn
[0x800006b8]:csrrs a2, fcsr, zero
[0x800006bc]:sw t6, 152(fp)
[0x800006c0]:sw a2, 156(fp)
[0x800006c4]:lw t5, 184(a1)
[0x800006c8]:lw t4, 188(a1)
[0x800006cc]:addi s1, zero, 2
[0x800006d0]:csrrw zero, fcsr, s1
[0x800006d4]:fdiv.s t6, t5, t4, dyn

[0x800006d4]:fdiv.s t6, t5, t4, dyn
[0x800006d8]:csrrs a2, fcsr, zero
[0x800006dc]:sw t6, 160(fp)
[0x800006e0]:sw a2, 164(fp)
[0x800006e4]:lw t5, 192(a1)
[0x800006e8]:lw t4, 196(a1)
[0x800006ec]:addi s1, zero, 34
[0x800006f0]:csrrw zero, fcsr, s1
[0x800006f4]:fdiv.s t6, t5, t4, dyn

[0x800006f4]:fdiv.s t6, t5, t4, dyn
[0x800006f8]:csrrs a2, fcsr, zero
[0x800006fc]:sw t6, 168(fp)
[0x80000700]:sw a2, 172(fp)
[0x80000704]:lw t5, 200(a1)
[0x80000708]:lw t4, 204(a1)
[0x8000070c]:addi s1, zero, 66
[0x80000710]:csrrw zero, fcsr, s1
[0x80000714]:fdiv.s t6, t5, t4, dyn

[0x80000714]:fdiv.s t6, t5, t4, dyn
[0x80000718]:csrrs a2, fcsr, zero
[0x8000071c]:sw t6, 176(fp)
[0x80000720]:sw a2, 180(fp)
[0x80000724]:lw t5, 208(a1)
[0x80000728]:lw t4, 212(a1)
[0x8000072c]:addi s1, zero, 98
[0x80000730]:csrrw zero, fcsr, s1
[0x80000734]:fdiv.s t6, t5, t4, dyn

[0x80000734]:fdiv.s t6, t5, t4, dyn
[0x80000738]:csrrs a2, fcsr, zero
[0x8000073c]:sw t6, 184(fp)
[0x80000740]:sw a2, 188(fp)
[0x80000744]:lw t5, 216(a1)
[0x80000748]:lw t4, 220(a1)
[0x8000074c]:addi s1, zero, 130
[0x80000750]:csrrw zero, fcsr, s1
[0x80000754]:fdiv.s t6, t5, t4, dyn

[0x80000754]:fdiv.s t6, t5, t4, dyn
[0x80000758]:csrrs a2, fcsr, zero
[0x8000075c]:sw t6, 192(fp)
[0x80000760]:sw a2, 196(fp)
[0x80000764]:lw t5, 224(a1)
[0x80000768]:lw t4, 228(a1)
[0x8000076c]:addi s1, zero, 2
[0x80000770]:csrrw zero, fcsr, s1
[0x80000774]:fdiv.s t6, t5, t4, dyn

[0x80000774]:fdiv.s t6, t5, t4, dyn
[0x80000778]:csrrs a2, fcsr, zero
[0x8000077c]:sw t6, 200(fp)
[0x80000780]:sw a2, 204(fp)
[0x80000784]:lw t5, 232(a1)
[0x80000788]:lw t4, 236(a1)
[0x8000078c]:addi s1, zero, 34
[0x80000790]:csrrw zero, fcsr, s1
[0x80000794]:fdiv.s t6, t5, t4, dyn

[0x80000794]:fdiv.s t6, t5, t4, dyn
[0x80000798]:csrrs a2, fcsr, zero
[0x8000079c]:sw t6, 208(fp)
[0x800007a0]:sw a2, 212(fp)
[0x800007a4]:lw t5, 240(a1)
[0x800007a8]:lw t4, 244(a1)
[0x800007ac]:addi s1, zero, 66
[0x800007b0]:csrrw zero, fcsr, s1
[0x800007b4]:fdiv.s t6, t5, t4, dyn

[0x800007b4]:fdiv.s t6, t5, t4, dyn
[0x800007b8]:csrrs a2, fcsr, zero
[0x800007bc]:sw t6, 216(fp)
[0x800007c0]:sw a2, 220(fp)
[0x800007c4]:lw t5, 248(a1)
[0x800007c8]:lw t4, 252(a1)
[0x800007cc]:addi s1, zero, 98
[0x800007d0]:csrrw zero, fcsr, s1
[0x800007d4]:fdiv.s t6, t5, t4, dyn

[0x800007d4]:fdiv.s t6, t5, t4, dyn
[0x800007d8]:csrrs a2, fcsr, zero
[0x800007dc]:sw t6, 224(fp)
[0x800007e0]:sw a2, 228(fp)
[0x800007e4]:lw t5, 256(a1)
[0x800007e8]:lw t4, 260(a1)
[0x800007ec]:addi s1, zero, 130
[0x800007f0]:csrrw zero, fcsr, s1
[0x800007f4]:fdiv.s t6, t5, t4, dyn

[0x800007f4]:fdiv.s t6, t5, t4, dyn
[0x800007f8]:csrrs a2, fcsr, zero
[0x800007fc]:sw t6, 232(fp)
[0x80000800]:sw a2, 236(fp)
[0x80000804]:lw t5, 264(a1)
[0x80000808]:lw t4, 268(a1)
[0x8000080c]:addi s1, zero, 2
[0x80000810]:csrrw zero, fcsr, s1
[0x80000814]:fdiv.s t6, t5, t4, dyn

[0x80000814]:fdiv.s t6, t5, t4, dyn
[0x80000818]:csrrs a2, fcsr, zero
[0x8000081c]:sw t6, 240(fp)
[0x80000820]:sw a2, 244(fp)
[0x80000824]:lw t5, 272(a1)
[0x80000828]:lw t4, 276(a1)
[0x8000082c]:addi s1, zero, 34
[0x80000830]:csrrw zero, fcsr, s1
[0x80000834]:fdiv.s t6, t5, t4, dyn

[0x80000834]:fdiv.s t6, t5, t4, dyn
[0x80000838]:csrrs a2, fcsr, zero
[0x8000083c]:sw t6, 248(fp)
[0x80000840]:sw a2, 252(fp)
[0x80000844]:lw t5, 280(a1)
[0x80000848]:lw t4, 284(a1)
[0x8000084c]:addi s1, zero, 66
[0x80000850]:csrrw zero, fcsr, s1
[0x80000854]:fdiv.s t6, t5, t4, dyn

[0x80000854]:fdiv.s t6, t5, t4, dyn
[0x80000858]:csrrs a2, fcsr, zero
[0x8000085c]:sw t6, 256(fp)
[0x80000860]:sw a2, 260(fp)
[0x80000864]:lw t5, 288(a1)
[0x80000868]:lw t4, 292(a1)
[0x8000086c]:addi s1, zero, 98
[0x80000870]:csrrw zero, fcsr, s1
[0x80000874]:fdiv.s t6, t5, t4, dyn

[0x80000874]:fdiv.s t6, t5, t4, dyn
[0x80000878]:csrrs a2, fcsr, zero
[0x8000087c]:sw t6, 264(fp)
[0x80000880]:sw a2, 268(fp)
[0x80000884]:lw t5, 296(a1)
[0x80000888]:lw t4, 300(a1)
[0x8000088c]:addi s1, zero, 130
[0x80000890]:csrrw zero, fcsr, s1
[0x80000894]:fdiv.s t6, t5, t4, dyn

[0x80000894]:fdiv.s t6, t5, t4, dyn
[0x80000898]:csrrs a2, fcsr, zero
[0x8000089c]:sw t6, 272(fp)
[0x800008a0]:sw a2, 276(fp)
[0x800008a4]:lw t5, 304(a1)
[0x800008a8]:lw t4, 308(a1)
[0x800008ac]:addi s1, zero, 2
[0x800008b0]:csrrw zero, fcsr, s1
[0x800008b4]:fdiv.s t6, t5, t4, dyn

[0x800008b4]:fdiv.s t6, t5, t4, dyn
[0x800008b8]:csrrs a2, fcsr, zero
[0x800008bc]:sw t6, 280(fp)
[0x800008c0]:sw a2, 284(fp)
[0x800008c4]:lw t5, 312(a1)
[0x800008c8]:lw t4, 316(a1)
[0x800008cc]:addi s1, zero, 34
[0x800008d0]:csrrw zero, fcsr, s1
[0x800008d4]:fdiv.s t6, t5, t4, dyn

[0x800008d4]:fdiv.s t6, t5, t4, dyn
[0x800008d8]:csrrs a2, fcsr, zero
[0x800008dc]:sw t6, 288(fp)
[0x800008e0]:sw a2, 292(fp)
[0x800008e4]:lw t5, 320(a1)
[0x800008e8]:lw t4, 324(a1)
[0x800008ec]:addi s1, zero, 66
[0x800008f0]:csrrw zero, fcsr, s1
[0x800008f4]:fdiv.s t6, t5, t4, dyn

[0x800008f4]:fdiv.s t6, t5, t4, dyn
[0x800008f8]:csrrs a2, fcsr, zero
[0x800008fc]:sw t6, 296(fp)
[0x80000900]:sw a2, 300(fp)
[0x80000904]:lw t5, 328(a1)
[0x80000908]:lw t4, 332(a1)
[0x8000090c]:addi s1, zero, 98
[0x80000910]:csrrw zero, fcsr, s1
[0x80000914]:fdiv.s t6, t5, t4, dyn

[0x80000914]:fdiv.s t6, t5, t4, dyn
[0x80000918]:csrrs a2, fcsr, zero
[0x8000091c]:sw t6, 304(fp)
[0x80000920]:sw a2, 308(fp)
[0x80000924]:lw t5, 336(a1)
[0x80000928]:lw t4, 340(a1)
[0x8000092c]:addi s1, zero, 130
[0x80000930]:csrrw zero, fcsr, s1
[0x80000934]:fdiv.s t6, t5, t4, dyn

[0x80000934]:fdiv.s t6, t5, t4, dyn
[0x80000938]:csrrs a2, fcsr, zero
[0x8000093c]:sw t6, 312(fp)
[0x80000940]:sw a2, 316(fp)
[0x80000944]:lw t5, 344(a1)
[0x80000948]:lw t4, 348(a1)
[0x8000094c]:addi s1, zero, 2
[0x80000950]:csrrw zero, fcsr, s1
[0x80000954]:fdiv.s t6, t5, t4, dyn

[0x80000954]:fdiv.s t6, t5, t4, dyn
[0x80000958]:csrrs a2, fcsr, zero
[0x8000095c]:sw t6, 320(fp)
[0x80000960]:sw a2, 324(fp)
[0x80000964]:lw t5, 352(a1)
[0x80000968]:lw t4, 356(a1)
[0x8000096c]:addi s1, zero, 34
[0x80000970]:csrrw zero, fcsr, s1
[0x80000974]:fdiv.s t6, t5, t4, dyn

[0x80000974]:fdiv.s t6, t5, t4, dyn
[0x80000978]:csrrs a2, fcsr, zero
[0x8000097c]:sw t6, 328(fp)
[0x80000980]:sw a2, 332(fp)
[0x80000984]:lw t5, 360(a1)
[0x80000988]:lw t4, 364(a1)
[0x8000098c]:addi s1, zero, 66
[0x80000990]:csrrw zero, fcsr, s1
[0x80000994]:fdiv.s t6, t5, t4, dyn

[0x80000994]:fdiv.s t6, t5, t4, dyn
[0x80000998]:csrrs a2, fcsr, zero
[0x8000099c]:sw t6, 336(fp)
[0x800009a0]:sw a2, 340(fp)
[0x800009a4]:lw t5, 368(a1)
[0x800009a8]:lw t4, 372(a1)
[0x800009ac]:addi s1, zero, 98
[0x800009b0]:csrrw zero, fcsr, s1
[0x800009b4]:fdiv.s t6, t5, t4, dyn

[0x800009b4]:fdiv.s t6, t5, t4, dyn
[0x800009b8]:csrrs a2, fcsr, zero
[0x800009bc]:sw t6, 344(fp)
[0x800009c0]:sw a2, 348(fp)
[0x800009c4]:lw t5, 376(a1)
[0x800009c8]:lw t4, 380(a1)
[0x800009cc]:addi s1, zero, 130
[0x800009d0]:csrrw zero, fcsr, s1
[0x800009d4]:fdiv.s t6, t5, t4, dyn

[0x800009d4]:fdiv.s t6, t5, t4, dyn
[0x800009d8]:csrrs a2, fcsr, zero
[0x800009dc]:sw t6, 352(fp)
[0x800009e0]:sw a2, 356(fp)
[0x800009e4]:lw t5, 384(a1)
[0x800009e8]:lw t4, 388(a1)
[0x800009ec]:addi s1, zero, 2
[0x800009f0]:csrrw zero, fcsr, s1
[0x800009f4]:fdiv.s t6, t5, t4, dyn

[0x800009f4]:fdiv.s t6, t5, t4, dyn
[0x800009f8]:csrrs a2, fcsr, zero
[0x800009fc]:sw t6, 360(fp)
[0x80000a00]:sw a2, 364(fp)
[0x80000a04]:lw t5, 392(a1)
[0x80000a08]:lw t4, 396(a1)
[0x80000a0c]:addi s1, zero, 34
[0x80000a10]:csrrw zero, fcsr, s1
[0x80000a14]:fdiv.s t6, t5, t4, dyn

[0x80000a14]:fdiv.s t6, t5, t4, dyn
[0x80000a18]:csrrs a2, fcsr, zero
[0x80000a1c]:sw t6, 368(fp)
[0x80000a20]:sw a2, 372(fp)
[0x80000a24]:lw t5, 400(a1)
[0x80000a28]:lw t4, 404(a1)
[0x80000a2c]:addi s1, zero, 66
[0x80000a30]:csrrw zero, fcsr, s1
[0x80000a34]:fdiv.s t6, t5, t4, dyn

[0x80000a34]:fdiv.s t6, t5, t4, dyn
[0x80000a38]:csrrs a2, fcsr, zero
[0x80000a3c]:sw t6, 376(fp)
[0x80000a40]:sw a2, 380(fp)
[0x80000a44]:lw t5, 408(a1)
[0x80000a48]:lw t4, 412(a1)
[0x80000a4c]:addi s1, zero, 98
[0x80000a50]:csrrw zero, fcsr, s1
[0x80000a54]:fdiv.s t6, t5, t4, dyn

[0x80000a54]:fdiv.s t6, t5, t4, dyn
[0x80000a58]:csrrs a2, fcsr, zero
[0x80000a5c]:sw t6, 384(fp)
[0x80000a60]:sw a2, 388(fp)
[0x80000a64]:lw t5, 416(a1)
[0x80000a68]:lw t4, 420(a1)
[0x80000a6c]:addi s1, zero, 130
[0x80000a70]:csrrw zero, fcsr, s1
[0x80000a74]:fdiv.s t6, t5, t4, dyn

[0x80000a74]:fdiv.s t6, t5, t4, dyn
[0x80000a78]:csrrs a2, fcsr, zero
[0x80000a7c]:sw t6, 392(fp)
[0x80000a80]:sw a2, 396(fp)
[0x80000a84]:lw t5, 424(a1)
[0x80000a88]:lw t4, 428(a1)
[0x80000a8c]:addi s1, zero, 2
[0x80000a90]:csrrw zero, fcsr, s1
[0x80000a94]:fdiv.s t6, t5, t4, dyn

[0x80000a94]:fdiv.s t6, t5, t4, dyn
[0x80000a98]:csrrs a2, fcsr, zero
[0x80000a9c]:sw t6, 400(fp)
[0x80000aa0]:sw a2, 404(fp)
[0x80000aa4]:lw t5, 432(a1)
[0x80000aa8]:lw t4, 436(a1)
[0x80000aac]:addi s1, zero, 34
[0x80000ab0]:csrrw zero, fcsr, s1
[0x80000ab4]:fdiv.s t6, t5, t4, dyn

[0x80000ab4]:fdiv.s t6, t5, t4, dyn
[0x80000ab8]:csrrs a2, fcsr, zero
[0x80000abc]:sw t6, 408(fp)
[0x80000ac0]:sw a2, 412(fp)
[0x80000ac4]:lw t5, 440(a1)
[0x80000ac8]:lw t4, 444(a1)
[0x80000acc]:addi s1, zero, 66
[0x80000ad0]:csrrw zero, fcsr, s1
[0x80000ad4]:fdiv.s t6, t5, t4, dyn

[0x80000ad4]:fdiv.s t6, t5, t4, dyn
[0x80000ad8]:csrrs a2, fcsr, zero
[0x80000adc]:sw t6, 416(fp)
[0x80000ae0]:sw a2, 420(fp)
[0x80000ae4]:lw t5, 448(a1)
[0x80000ae8]:lw t4, 452(a1)
[0x80000aec]:addi s1, zero, 98
[0x80000af0]:csrrw zero, fcsr, s1
[0x80000af4]:fdiv.s t6, t5, t4, dyn

[0x80000af4]:fdiv.s t6, t5, t4, dyn
[0x80000af8]:csrrs a2, fcsr, zero
[0x80000afc]:sw t6, 424(fp)
[0x80000b00]:sw a2, 428(fp)
[0x80000b04]:lw t5, 456(a1)
[0x80000b08]:lw t4, 460(a1)
[0x80000b0c]:addi s1, zero, 130
[0x80000b10]:csrrw zero, fcsr, s1
[0x80000b14]:fdiv.s t6, t5, t4, dyn

[0x80000b14]:fdiv.s t6, t5, t4, dyn
[0x80000b18]:csrrs a2, fcsr, zero
[0x80000b1c]:sw t6, 432(fp)
[0x80000b20]:sw a2, 436(fp)
[0x80000b24]:lw t5, 464(a1)
[0x80000b28]:lw t4, 468(a1)
[0x80000b2c]:addi s1, zero, 2
[0x80000b30]:csrrw zero, fcsr, s1
[0x80000b34]:fdiv.s t6, t5, t4, dyn

[0x80000b34]:fdiv.s t6, t5, t4, dyn
[0x80000b38]:csrrs a2, fcsr, zero
[0x80000b3c]:sw t6, 440(fp)
[0x80000b40]:sw a2, 444(fp)
[0x80000b44]:lw t5, 472(a1)
[0x80000b48]:lw t4, 476(a1)
[0x80000b4c]:addi s1, zero, 34
[0x80000b50]:csrrw zero, fcsr, s1
[0x80000b54]:fdiv.s t6, t5, t4, dyn

[0x80000b54]:fdiv.s t6, t5, t4, dyn
[0x80000b58]:csrrs a2, fcsr, zero
[0x80000b5c]:sw t6, 448(fp)
[0x80000b60]:sw a2, 452(fp)
[0x80000b64]:lw t5, 480(a1)
[0x80000b68]:lw t4, 484(a1)
[0x80000b6c]:addi s1, zero, 66
[0x80000b70]:csrrw zero, fcsr, s1
[0x80000b74]:fdiv.s t6, t5, t4, dyn

[0x80000b74]:fdiv.s t6, t5, t4, dyn
[0x80000b78]:csrrs a2, fcsr, zero
[0x80000b7c]:sw t6, 456(fp)
[0x80000b80]:sw a2, 460(fp)
[0x80000b84]:lw t5, 488(a1)
[0x80000b88]:lw t4, 492(a1)
[0x80000b8c]:addi s1, zero, 98
[0x80000b90]:csrrw zero, fcsr, s1
[0x80000b94]:fdiv.s t6, t5, t4, dyn

[0x80000b94]:fdiv.s t6, t5, t4, dyn
[0x80000b98]:csrrs a2, fcsr, zero
[0x80000b9c]:sw t6, 464(fp)
[0x80000ba0]:sw a2, 468(fp)
[0x80000ba4]:lw t5, 496(a1)
[0x80000ba8]:lw t4, 500(a1)
[0x80000bac]:addi s1, zero, 130
[0x80000bb0]:csrrw zero, fcsr, s1
[0x80000bb4]:fdiv.s t6, t5, t4, dyn

[0x80000bb4]:fdiv.s t6, t5, t4, dyn
[0x80000bb8]:csrrs a2, fcsr, zero
[0x80000bbc]:sw t6, 472(fp)
[0x80000bc0]:sw a2, 476(fp)
[0x80000bc4]:lw t5, 504(a1)
[0x80000bc8]:lw t4, 508(a1)
[0x80000bcc]:addi s1, zero, 2
[0x80000bd0]:csrrw zero, fcsr, s1
[0x80000bd4]:fdiv.s t6, t5, t4, dyn

[0x80000bd4]:fdiv.s t6, t5, t4, dyn
[0x80000bd8]:csrrs a2, fcsr, zero
[0x80000bdc]:sw t6, 480(fp)
[0x80000be0]:sw a2, 484(fp)
[0x80000be4]:lw t5, 512(a1)
[0x80000be8]:lw t4, 516(a1)
[0x80000bec]:addi s1, zero, 34
[0x80000bf0]:csrrw zero, fcsr, s1
[0x80000bf4]:fdiv.s t6, t5, t4, dyn

[0x80000bf4]:fdiv.s t6, t5, t4, dyn
[0x80000bf8]:csrrs a2, fcsr, zero
[0x80000bfc]:sw t6, 488(fp)
[0x80000c00]:sw a2, 492(fp)
[0x80000c04]:lw t5, 520(a1)
[0x80000c08]:lw t4, 524(a1)
[0x80000c0c]:addi s1, zero, 66
[0x80000c10]:csrrw zero, fcsr, s1
[0x80000c14]:fdiv.s t6, t5, t4, dyn

[0x80000c14]:fdiv.s t6, t5, t4, dyn
[0x80000c18]:csrrs a2, fcsr, zero
[0x80000c1c]:sw t6, 496(fp)
[0x80000c20]:sw a2, 500(fp)
[0x80000c24]:lw t5, 528(a1)
[0x80000c28]:lw t4, 532(a1)
[0x80000c2c]:addi s1, zero, 98
[0x80000c30]:csrrw zero, fcsr, s1
[0x80000c34]:fdiv.s t6, t5, t4, dyn

[0x80000c34]:fdiv.s t6, t5, t4, dyn
[0x80000c38]:csrrs a2, fcsr, zero
[0x80000c3c]:sw t6, 504(fp)
[0x80000c40]:sw a2, 508(fp)
[0x80000c44]:lw t5, 536(a1)
[0x80000c48]:lw t4, 540(a1)
[0x80000c4c]:addi s1, zero, 130
[0x80000c50]:csrrw zero, fcsr, s1
[0x80000c54]:fdiv.s t6, t5, t4, dyn

[0x80000c54]:fdiv.s t6, t5, t4, dyn
[0x80000c58]:csrrs a2, fcsr, zero
[0x80000c5c]:sw t6, 512(fp)
[0x80000c60]:sw a2, 516(fp)
[0x80000c64]:lw t5, 544(a1)
[0x80000c68]:lw t4, 548(a1)
[0x80000c6c]:addi s1, zero, 2
[0x80000c70]:csrrw zero, fcsr, s1
[0x80000c74]:fdiv.s t6, t5, t4, dyn

[0x80000c74]:fdiv.s t6, t5, t4, dyn
[0x80000c78]:csrrs a2, fcsr, zero
[0x80000c7c]:sw t6, 520(fp)
[0x80000c80]:sw a2, 524(fp)
[0x80000c84]:lw t5, 552(a1)
[0x80000c88]:lw t4, 556(a1)
[0x80000c8c]:addi s1, zero, 34
[0x80000c90]:csrrw zero, fcsr, s1
[0x80000c94]:fdiv.s t6, t5, t4, dyn

[0x80000c94]:fdiv.s t6, t5, t4, dyn
[0x80000c98]:csrrs a2, fcsr, zero
[0x80000c9c]:sw t6, 528(fp)
[0x80000ca0]:sw a2, 532(fp)
[0x80000ca4]:lw t5, 560(a1)
[0x80000ca8]:lw t4, 564(a1)
[0x80000cac]:addi s1, zero, 66
[0x80000cb0]:csrrw zero, fcsr, s1
[0x80000cb4]:fdiv.s t6, t5, t4, dyn

[0x80000cb4]:fdiv.s t6, t5, t4, dyn
[0x80000cb8]:csrrs a2, fcsr, zero
[0x80000cbc]:sw t6, 536(fp)
[0x80000cc0]:sw a2, 540(fp)
[0x80000cc4]:lw t5, 568(a1)
[0x80000cc8]:lw t4, 572(a1)
[0x80000ccc]:addi s1, zero, 98
[0x80000cd0]:csrrw zero, fcsr, s1
[0x80000cd4]:fdiv.s t6, t5, t4, dyn

[0x80000cd4]:fdiv.s t6, t5, t4, dyn
[0x80000cd8]:csrrs a2, fcsr, zero
[0x80000cdc]:sw t6, 544(fp)
[0x80000ce0]:sw a2, 548(fp)
[0x80000ce4]:lw t5, 576(a1)
[0x80000ce8]:lw t4, 580(a1)
[0x80000cec]:addi s1, zero, 130
[0x80000cf0]:csrrw zero, fcsr, s1
[0x80000cf4]:fdiv.s t6, t5, t4, dyn

[0x80000cf4]:fdiv.s t6, t5, t4, dyn
[0x80000cf8]:csrrs a2, fcsr, zero
[0x80000cfc]:sw t6, 552(fp)
[0x80000d00]:sw a2, 556(fp)
[0x80000d04]:lw t5, 584(a1)
[0x80000d08]:lw t4, 588(a1)
[0x80000d0c]:addi s1, zero, 2
[0x80000d10]:csrrw zero, fcsr, s1
[0x80000d14]:fdiv.s t6, t5, t4, dyn

[0x80000d14]:fdiv.s t6, t5, t4, dyn
[0x80000d18]:csrrs a2, fcsr, zero
[0x80000d1c]:sw t6, 560(fp)
[0x80000d20]:sw a2, 564(fp)
[0x80000d24]:lw t5, 592(a1)
[0x80000d28]:lw t4, 596(a1)
[0x80000d2c]:addi s1, zero, 34
[0x80000d30]:csrrw zero, fcsr, s1
[0x80000d34]:fdiv.s t6, t5, t4, dyn

[0x80000d34]:fdiv.s t6, t5, t4, dyn
[0x80000d38]:csrrs a2, fcsr, zero
[0x80000d3c]:sw t6, 568(fp)
[0x80000d40]:sw a2, 572(fp)
[0x80000d44]:lw t5, 600(a1)
[0x80000d48]:lw t4, 604(a1)
[0x80000d4c]:addi s1, zero, 66
[0x80000d50]:csrrw zero, fcsr, s1
[0x80000d54]:fdiv.s t6, t5, t4, dyn

[0x80000d54]:fdiv.s t6, t5, t4, dyn
[0x80000d58]:csrrs a2, fcsr, zero
[0x80000d5c]:sw t6, 576(fp)
[0x80000d60]:sw a2, 580(fp)
[0x80000d64]:lw t5, 608(a1)
[0x80000d68]:lw t4, 612(a1)
[0x80000d6c]:addi s1, zero, 98
[0x80000d70]:csrrw zero, fcsr, s1
[0x80000d74]:fdiv.s t6, t5, t4, dyn

[0x80000d74]:fdiv.s t6, t5, t4, dyn
[0x80000d78]:csrrs a2, fcsr, zero
[0x80000d7c]:sw t6, 584(fp)
[0x80000d80]:sw a2, 588(fp)
[0x80000d84]:lw t5, 616(a1)
[0x80000d88]:lw t4, 620(a1)
[0x80000d8c]:addi s1, zero, 130
[0x80000d90]:csrrw zero, fcsr, s1
[0x80000d94]:fdiv.s t6, t5, t4, dyn

[0x80000d94]:fdiv.s t6, t5, t4, dyn
[0x80000d98]:csrrs a2, fcsr, zero
[0x80000d9c]:sw t6, 592(fp)
[0x80000da0]:sw a2, 596(fp)
[0x80000da4]:lw t5, 624(a1)
[0x80000da8]:lw t4, 628(a1)
[0x80000dac]:addi s1, zero, 2
[0x80000db0]:csrrw zero, fcsr, s1
[0x80000db4]:fdiv.s t6, t5, t4, dyn

[0x80000db4]:fdiv.s t6, t5, t4, dyn
[0x80000db8]:csrrs a2, fcsr, zero
[0x80000dbc]:sw t6, 600(fp)
[0x80000dc0]:sw a2, 604(fp)
[0x80000dc4]:lw t5, 632(a1)
[0x80000dc8]:lw t4, 636(a1)
[0x80000dcc]:addi s1, zero, 34
[0x80000dd0]:csrrw zero, fcsr, s1
[0x80000dd4]:fdiv.s t6, t5, t4, dyn

[0x80000dd4]:fdiv.s t6, t5, t4, dyn
[0x80000dd8]:csrrs a2, fcsr, zero
[0x80000ddc]:sw t6, 608(fp)
[0x80000de0]:sw a2, 612(fp)
[0x80000de4]:lw t5, 640(a1)
[0x80000de8]:lw t4, 644(a1)
[0x80000dec]:addi s1, zero, 66
[0x80000df0]:csrrw zero, fcsr, s1
[0x80000df4]:fdiv.s t6, t5, t4, dyn

[0x80000df4]:fdiv.s t6, t5, t4, dyn
[0x80000df8]:csrrs a2, fcsr, zero
[0x80000dfc]:sw t6, 616(fp)
[0x80000e00]:sw a2, 620(fp)
[0x80000e04]:lw t5, 648(a1)
[0x80000e08]:lw t4, 652(a1)
[0x80000e0c]:addi s1, zero, 98
[0x80000e10]:csrrw zero, fcsr, s1
[0x80000e14]:fdiv.s t6, t5, t4, dyn

[0x80000e14]:fdiv.s t6, t5, t4, dyn
[0x80000e18]:csrrs a2, fcsr, zero
[0x80000e1c]:sw t6, 624(fp)
[0x80000e20]:sw a2, 628(fp)
[0x80000e24]:lw t5, 656(a1)
[0x80000e28]:lw t4, 660(a1)
[0x80000e2c]:addi s1, zero, 130
[0x80000e30]:csrrw zero, fcsr, s1
[0x80000e34]:fdiv.s t6, t5, t4, dyn

[0x80000e34]:fdiv.s t6, t5, t4, dyn
[0x80000e38]:csrrs a2, fcsr, zero
[0x80000e3c]:sw t6, 632(fp)
[0x80000e40]:sw a2, 636(fp)
[0x80000e44]:lw t5, 664(a1)
[0x80000e48]:lw t4, 668(a1)
[0x80000e4c]:addi s1, zero, 2
[0x80000e50]:csrrw zero, fcsr, s1
[0x80000e54]:fdiv.s t6, t5, t4, dyn

[0x80000e54]:fdiv.s t6, t5, t4, dyn
[0x80000e58]:csrrs a2, fcsr, zero
[0x80000e5c]:sw t6, 640(fp)
[0x80000e60]:sw a2, 644(fp)
[0x80000e64]:lw t5, 672(a1)
[0x80000e68]:lw t4, 676(a1)
[0x80000e6c]:addi s1, zero, 34
[0x80000e70]:csrrw zero, fcsr, s1
[0x80000e74]:fdiv.s t6, t5, t4, dyn

[0x80000e74]:fdiv.s t6, t5, t4, dyn
[0x80000e78]:csrrs a2, fcsr, zero
[0x80000e7c]:sw t6, 648(fp)
[0x80000e80]:sw a2, 652(fp)
[0x80000e84]:lw t5, 680(a1)
[0x80000e88]:lw t4, 684(a1)
[0x80000e8c]:addi s1, zero, 66
[0x80000e90]:csrrw zero, fcsr, s1
[0x80000e94]:fdiv.s t6, t5, t4, dyn

[0x80000e94]:fdiv.s t6, t5, t4, dyn
[0x80000e98]:csrrs a2, fcsr, zero
[0x80000e9c]:sw t6, 656(fp)
[0x80000ea0]:sw a2, 660(fp)
[0x80000ea4]:lw t5, 688(a1)
[0x80000ea8]:lw t4, 692(a1)
[0x80000eac]:addi s1, zero, 98
[0x80000eb0]:csrrw zero, fcsr, s1
[0x80000eb4]:fdiv.s t6, t5, t4, dyn

[0x80000eb4]:fdiv.s t6, t5, t4, dyn
[0x80000eb8]:csrrs a2, fcsr, zero
[0x80000ebc]:sw t6, 664(fp)
[0x80000ec0]:sw a2, 668(fp)
[0x80000ec4]:lw t5, 696(a1)
[0x80000ec8]:lw t4, 700(a1)
[0x80000ecc]:addi s1, zero, 130
[0x80000ed0]:csrrw zero, fcsr, s1
[0x80000ed4]:fdiv.s t6, t5, t4, dyn

[0x80000ed4]:fdiv.s t6, t5, t4, dyn
[0x80000ed8]:csrrs a2, fcsr, zero
[0x80000edc]:sw t6, 672(fp)
[0x80000ee0]:sw a2, 676(fp)
[0x80000ee4]:lw t5, 704(a1)
[0x80000ee8]:lw t4, 708(a1)
[0x80000eec]:addi s1, zero, 2
[0x80000ef0]:csrrw zero, fcsr, s1
[0x80000ef4]:fdiv.s t6, t5, t4, dyn

[0x80000ef4]:fdiv.s t6, t5, t4, dyn
[0x80000ef8]:csrrs a2, fcsr, zero
[0x80000efc]:sw t6, 680(fp)
[0x80000f00]:sw a2, 684(fp)
[0x80000f04]:lw t5, 712(a1)
[0x80000f08]:lw t4, 716(a1)
[0x80000f0c]:addi s1, zero, 34
[0x80000f10]:csrrw zero, fcsr, s1
[0x80000f14]:fdiv.s t6, t5, t4, dyn

[0x80000f14]:fdiv.s t6, t5, t4, dyn
[0x80000f18]:csrrs a2, fcsr, zero
[0x80000f1c]:sw t6, 688(fp)
[0x80000f20]:sw a2, 692(fp)
[0x80000f24]:lw t5, 720(a1)
[0x80000f28]:lw t4, 724(a1)
[0x80000f2c]:addi s1, zero, 66
[0x80000f30]:csrrw zero, fcsr, s1
[0x80000f34]:fdiv.s t6, t5, t4, dyn

[0x80000f34]:fdiv.s t6, t5, t4, dyn
[0x80000f38]:csrrs a2, fcsr, zero
[0x80000f3c]:sw t6, 696(fp)
[0x80000f40]:sw a2, 700(fp)
[0x80000f44]:lw t5, 728(a1)
[0x80000f48]:lw t4, 732(a1)
[0x80000f4c]:addi s1, zero, 98
[0x80000f50]:csrrw zero, fcsr, s1
[0x80000f54]:fdiv.s t6, t5, t4, dyn

[0x80000f54]:fdiv.s t6, t5, t4, dyn
[0x80000f58]:csrrs a2, fcsr, zero
[0x80000f5c]:sw t6, 704(fp)
[0x80000f60]:sw a2, 708(fp)
[0x80000f64]:lw t5, 736(a1)
[0x80000f68]:lw t4, 740(a1)
[0x80000f6c]:addi s1, zero, 130
[0x80000f70]:csrrw zero, fcsr, s1
[0x80000f74]:fdiv.s t6, t5, t4, dyn

[0x80000f74]:fdiv.s t6, t5, t4, dyn
[0x80000f78]:csrrs a2, fcsr, zero
[0x80000f7c]:sw t6, 712(fp)
[0x80000f80]:sw a2, 716(fp)
[0x80000f84]:lw t5, 744(a1)
[0x80000f88]:lw t4, 748(a1)
[0x80000f8c]:addi s1, zero, 2
[0x80000f90]:csrrw zero, fcsr, s1
[0x80000f94]:fdiv.s t6, t5, t4, dyn

[0x80000f94]:fdiv.s t6, t5, t4, dyn
[0x80000f98]:csrrs a2, fcsr, zero
[0x80000f9c]:sw t6, 720(fp)
[0x80000fa0]:sw a2, 724(fp)
[0x80000fa4]:lw t5, 752(a1)
[0x80000fa8]:lw t4, 756(a1)
[0x80000fac]:addi s1, zero, 34
[0x80000fb0]:csrrw zero, fcsr, s1
[0x80000fb4]:fdiv.s t6, t5, t4, dyn

[0x80000fb4]:fdiv.s t6, t5, t4, dyn
[0x80000fb8]:csrrs a2, fcsr, zero
[0x80000fbc]:sw t6, 728(fp)
[0x80000fc0]:sw a2, 732(fp)
[0x80000fc4]:lw t5, 760(a1)
[0x80000fc8]:lw t4, 764(a1)
[0x80000fcc]:addi s1, zero, 66
[0x80000fd0]:csrrw zero, fcsr, s1
[0x80000fd4]:fdiv.s t6, t5, t4, dyn

[0x80000fd4]:fdiv.s t6, t5, t4, dyn
[0x80000fd8]:csrrs a2, fcsr, zero
[0x80000fdc]:sw t6, 736(fp)
[0x80000fe0]:sw a2, 740(fp)
[0x80000fe4]:lw t5, 768(a1)
[0x80000fe8]:lw t4, 772(a1)
[0x80000fec]:addi s1, zero, 98
[0x80000ff0]:csrrw zero, fcsr, s1
[0x80000ff4]:fdiv.s t6, t5, t4, dyn

[0x80000ff4]:fdiv.s t6, t5, t4, dyn
[0x80000ff8]:csrrs a2, fcsr, zero
[0x80000ffc]:sw t6, 744(fp)
[0x80001000]:sw a2, 748(fp)
[0x80001004]:lw t5, 776(a1)
[0x80001008]:lw t4, 780(a1)
[0x8000100c]:addi s1, zero, 130
[0x80001010]:csrrw zero, fcsr, s1
[0x80001014]:fdiv.s t6, t5, t4, dyn

[0x80001014]:fdiv.s t6, t5, t4, dyn
[0x80001018]:csrrs a2, fcsr, zero
[0x8000101c]:sw t6, 752(fp)
[0x80001020]:sw a2, 756(fp)
[0x80001024]:lw t5, 784(a1)
[0x80001028]:lw t4, 788(a1)
[0x8000102c]:addi s1, zero, 2
[0x80001030]:csrrw zero, fcsr, s1
[0x80001034]:fdiv.s t6, t5, t4, dyn

[0x80001034]:fdiv.s t6, t5, t4, dyn
[0x80001038]:csrrs a2, fcsr, zero
[0x8000103c]:sw t6, 760(fp)
[0x80001040]:sw a2, 764(fp)
[0x80001044]:lw t5, 792(a1)
[0x80001048]:lw t4, 796(a1)
[0x8000104c]:addi s1, zero, 34
[0x80001050]:csrrw zero, fcsr, s1
[0x80001054]:fdiv.s t6, t5, t4, dyn

[0x80001054]:fdiv.s t6, t5, t4, dyn
[0x80001058]:csrrs a2, fcsr, zero
[0x8000105c]:sw t6, 768(fp)
[0x80001060]:sw a2, 772(fp)
[0x80001064]:lw t5, 800(a1)
[0x80001068]:lw t4, 804(a1)
[0x8000106c]:addi s1, zero, 66
[0x80001070]:csrrw zero, fcsr, s1
[0x80001074]:fdiv.s t6, t5, t4, dyn

[0x80001074]:fdiv.s t6, t5, t4, dyn
[0x80001078]:csrrs a2, fcsr, zero
[0x8000107c]:sw t6, 776(fp)
[0x80001080]:sw a2, 780(fp)
[0x80001084]:lw t5, 808(a1)
[0x80001088]:lw t4, 812(a1)
[0x8000108c]:addi s1, zero, 98
[0x80001090]:csrrw zero, fcsr, s1
[0x80001094]:fdiv.s t6, t5, t4, dyn

[0x80001094]:fdiv.s t6, t5, t4, dyn
[0x80001098]:csrrs a2, fcsr, zero
[0x8000109c]:sw t6, 784(fp)
[0x800010a0]:sw a2, 788(fp)
[0x800010a4]:lw t5, 816(a1)
[0x800010a8]:lw t4, 820(a1)
[0x800010ac]:addi s1, zero, 130
[0x800010b0]:csrrw zero, fcsr, s1
[0x800010b4]:fdiv.s t6, t5, t4, dyn

[0x800010b4]:fdiv.s t6, t5, t4, dyn
[0x800010b8]:csrrs a2, fcsr, zero
[0x800010bc]:sw t6, 792(fp)
[0x800010c0]:sw a2, 796(fp)
[0x800010c4]:lw t5, 824(a1)
[0x800010c8]:lw t4, 828(a1)
[0x800010cc]:addi s1, zero, 2
[0x800010d0]:csrrw zero, fcsr, s1
[0x800010d4]:fdiv.s t6, t5, t4, dyn

[0x800010d4]:fdiv.s t6, t5, t4, dyn
[0x800010d8]:csrrs a2, fcsr, zero
[0x800010dc]:sw t6, 800(fp)
[0x800010e0]:sw a2, 804(fp)
[0x800010e4]:lw t5, 832(a1)
[0x800010e8]:lw t4, 836(a1)
[0x800010ec]:addi s1, zero, 34
[0x800010f0]:csrrw zero, fcsr, s1
[0x800010f4]:fdiv.s t6, t5, t4, dyn

[0x800010f4]:fdiv.s t6, t5, t4, dyn
[0x800010f8]:csrrs a2, fcsr, zero
[0x800010fc]:sw t6, 808(fp)
[0x80001100]:sw a2, 812(fp)
[0x80001104]:lw t5, 840(a1)
[0x80001108]:lw t4, 844(a1)
[0x8000110c]:addi s1, zero, 66
[0x80001110]:csrrw zero, fcsr, s1
[0x80001114]:fdiv.s t6, t5, t4, dyn

[0x80001114]:fdiv.s t6, t5, t4, dyn
[0x80001118]:csrrs a2, fcsr, zero
[0x8000111c]:sw t6, 816(fp)
[0x80001120]:sw a2, 820(fp)
[0x80001124]:lw t5, 848(a1)
[0x80001128]:lw t4, 852(a1)
[0x8000112c]:addi s1, zero, 98
[0x80001130]:csrrw zero, fcsr, s1
[0x80001134]:fdiv.s t6, t5, t4, dyn

[0x80001134]:fdiv.s t6, t5, t4, dyn
[0x80001138]:csrrs a2, fcsr, zero
[0x8000113c]:sw t6, 824(fp)
[0x80001140]:sw a2, 828(fp)
[0x80001144]:lw t5, 856(a1)
[0x80001148]:lw t4, 860(a1)
[0x8000114c]:addi s1, zero, 130
[0x80001150]:csrrw zero, fcsr, s1
[0x80001154]:fdiv.s t6, t5, t4, dyn

[0x80001154]:fdiv.s t6, t5, t4, dyn
[0x80001158]:csrrs a2, fcsr, zero
[0x8000115c]:sw t6, 832(fp)
[0x80001160]:sw a2, 836(fp)
[0x80001164]:lw t5, 864(a1)
[0x80001168]:lw t4, 868(a1)
[0x8000116c]:addi s1, zero, 2
[0x80001170]:csrrw zero, fcsr, s1
[0x80001174]:fdiv.s t6, t5, t4, dyn

[0x80001174]:fdiv.s t6, t5, t4, dyn
[0x80001178]:csrrs a2, fcsr, zero
[0x8000117c]:sw t6, 840(fp)
[0x80001180]:sw a2, 844(fp)
[0x80001184]:lw t5, 872(a1)
[0x80001188]:lw t4, 876(a1)
[0x8000118c]:addi s1, zero, 34
[0x80001190]:csrrw zero, fcsr, s1
[0x80001194]:fdiv.s t6, t5, t4, dyn

[0x80001194]:fdiv.s t6, t5, t4, dyn
[0x80001198]:csrrs a2, fcsr, zero
[0x8000119c]:sw t6, 848(fp)
[0x800011a0]:sw a2, 852(fp)
[0x800011a4]:lw t5, 880(a1)
[0x800011a8]:lw t4, 884(a1)
[0x800011ac]:addi s1, zero, 66
[0x800011b0]:csrrw zero, fcsr, s1
[0x800011b4]:fdiv.s t6, t5, t4, dyn

[0x800011b4]:fdiv.s t6, t5, t4, dyn
[0x800011b8]:csrrs a2, fcsr, zero
[0x800011bc]:sw t6, 856(fp)
[0x800011c0]:sw a2, 860(fp)
[0x800011c4]:lw t5, 888(a1)
[0x800011c8]:lw t4, 892(a1)
[0x800011cc]:addi s1, zero, 98
[0x800011d0]:csrrw zero, fcsr, s1
[0x800011d4]:fdiv.s t6, t5, t4, dyn

[0x800011d4]:fdiv.s t6, t5, t4, dyn
[0x800011d8]:csrrs a2, fcsr, zero
[0x800011dc]:sw t6, 864(fp)
[0x800011e0]:sw a2, 868(fp)
[0x800011e4]:lw t5, 896(a1)
[0x800011e8]:lw t4, 900(a1)
[0x800011ec]:addi s1, zero, 130
[0x800011f0]:csrrw zero, fcsr, s1
[0x800011f4]:fdiv.s t6, t5, t4, dyn

[0x800011f4]:fdiv.s t6, t5, t4, dyn
[0x800011f8]:csrrs a2, fcsr, zero
[0x800011fc]:sw t6, 872(fp)
[0x80001200]:sw a2, 876(fp)
[0x80001204]:lw t5, 904(a1)
[0x80001208]:lw t4, 908(a1)
[0x8000120c]:addi s1, zero, 2
[0x80001210]:csrrw zero, fcsr, s1
[0x80001214]:fdiv.s t6, t5, t4, dyn

[0x80001214]:fdiv.s t6, t5, t4, dyn
[0x80001218]:csrrs a2, fcsr, zero
[0x8000121c]:sw t6, 880(fp)
[0x80001220]:sw a2, 884(fp)
[0x80001224]:lw t5, 912(a1)
[0x80001228]:lw t4, 916(a1)
[0x8000122c]:addi s1, zero, 34
[0x80001230]:csrrw zero, fcsr, s1
[0x80001234]:fdiv.s t6, t5, t4, dyn

[0x80001234]:fdiv.s t6, t5, t4, dyn
[0x80001238]:csrrs a2, fcsr, zero
[0x8000123c]:sw t6, 888(fp)
[0x80001240]:sw a2, 892(fp)
[0x80001244]:lw t5, 920(a1)
[0x80001248]:lw t4, 924(a1)
[0x8000124c]:addi s1, zero, 66
[0x80001250]:csrrw zero, fcsr, s1
[0x80001254]:fdiv.s t6, t5, t4, dyn

[0x80001254]:fdiv.s t6, t5, t4, dyn
[0x80001258]:csrrs a2, fcsr, zero
[0x8000125c]:sw t6, 896(fp)
[0x80001260]:sw a2, 900(fp)
[0x80001264]:lw t5, 928(a1)
[0x80001268]:lw t4, 932(a1)
[0x8000126c]:addi s1, zero, 98
[0x80001270]:csrrw zero, fcsr, s1
[0x80001274]:fdiv.s t6, t5, t4, dyn

[0x80001274]:fdiv.s t6, t5, t4, dyn
[0x80001278]:csrrs a2, fcsr, zero
[0x8000127c]:sw t6, 904(fp)
[0x80001280]:sw a2, 908(fp)
[0x80001284]:lw t5, 936(a1)
[0x80001288]:lw t4, 940(a1)
[0x8000128c]:addi s1, zero, 130
[0x80001290]:csrrw zero, fcsr, s1
[0x80001294]:fdiv.s t6, t5, t4, dyn

[0x80001294]:fdiv.s t6, t5, t4, dyn
[0x80001298]:csrrs a2, fcsr, zero
[0x8000129c]:sw t6, 912(fp)
[0x800012a0]:sw a2, 916(fp)
[0x800012a4]:lw t5, 944(a1)
[0x800012a8]:lw t4, 948(a1)
[0x800012ac]:addi s1, zero, 2
[0x800012b0]:csrrw zero, fcsr, s1
[0x800012b4]:fdiv.s t6, t5, t4, dyn

[0x800012b4]:fdiv.s t6, t5, t4, dyn
[0x800012b8]:csrrs a2, fcsr, zero
[0x800012bc]:sw t6, 920(fp)
[0x800012c0]:sw a2, 924(fp)
[0x800012c4]:lw t5, 952(a1)
[0x800012c8]:lw t4, 956(a1)
[0x800012cc]:addi s1, zero, 34
[0x800012d0]:csrrw zero, fcsr, s1
[0x800012d4]:fdiv.s t6, t5, t4, dyn

[0x800012d4]:fdiv.s t6, t5, t4, dyn
[0x800012d8]:csrrs a2, fcsr, zero
[0x800012dc]:sw t6, 928(fp)
[0x800012e0]:sw a2, 932(fp)
[0x800012e4]:lw t5, 960(a1)
[0x800012e8]:lw t4, 964(a1)
[0x800012ec]:addi s1, zero, 66
[0x800012f0]:csrrw zero, fcsr, s1
[0x800012f4]:fdiv.s t6, t5, t4, dyn

[0x800012f4]:fdiv.s t6, t5, t4, dyn
[0x800012f8]:csrrs a2, fcsr, zero
[0x800012fc]:sw t6, 936(fp)
[0x80001300]:sw a2, 940(fp)
[0x80001304]:lw t5, 968(a1)
[0x80001308]:lw t4, 972(a1)
[0x8000130c]:addi s1, zero, 98
[0x80001310]:csrrw zero, fcsr, s1
[0x80001314]:fdiv.s t6, t5, t4, dyn

[0x80001314]:fdiv.s t6, t5, t4, dyn
[0x80001318]:csrrs a2, fcsr, zero
[0x8000131c]:sw t6, 944(fp)
[0x80001320]:sw a2, 948(fp)
[0x80001324]:lw t5, 976(a1)
[0x80001328]:lw t4, 980(a1)
[0x8000132c]:addi s1, zero, 130
[0x80001330]:csrrw zero, fcsr, s1
[0x80001334]:fdiv.s t6, t5, t4, dyn

[0x80001334]:fdiv.s t6, t5, t4, dyn
[0x80001338]:csrrs a2, fcsr, zero
[0x8000133c]:sw t6, 952(fp)
[0x80001340]:sw a2, 956(fp)
[0x80001344]:lw t5, 984(a1)
[0x80001348]:lw t4, 988(a1)
[0x8000134c]:addi s1, zero, 2
[0x80001350]:csrrw zero, fcsr, s1
[0x80001354]:fdiv.s t6, t5, t4, dyn

[0x80001354]:fdiv.s t6, t5, t4, dyn
[0x80001358]:csrrs a2, fcsr, zero
[0x8000135c]:sw t6, 960(fp)
[0x80001360]:sw a2, 964(fp)
[0x80001364]:lw t5, 992(a1)
[0x80001368]:lw t4, 996(a1)
[0x8000136c]:addi s1, zero, 34
[0x80001370]:csrrw zero, fcsr, s1
[0x80001374]:fdiv.s t6, t5, t4, dyn

[0x80001374]:fdiv.s t6, t5, t4, dyn
[0x80001378]:csrrs a2, fcsr, zero
[0x8000137c]:sw t6, 968(fp)
[0x80001380]:sw a2, 972(fp)
[0x80001384]:lw t5, 1000(a1)
[0x80001388]:lw t4, 1004(a1)
[0x8000138c]:addi s1, zero, 66
[0x80001390]:csrrw zero, fcsr, s1
[0x80001394]:fdiv.s t6, t5, t4, dyn

[0x80001394]:fdiv.s t6, t5, t4, dyn
[0x80001398]:csrrs a2, fcsr, zero
[0x8000139c]:sw t6, 976(fp)
[0x800013a0]:sw a2, 980(fp)
[0x800013a4]:lw t5, 1008(a1)
[0x800013a8]:lw t4, 1012(a1)
[0x800013ac]:addi s1, zero, 98
[0x800013b0]:csrrw zero, fcsr, s1
[0x800013b4]:fdiv.s t6, t5, t4, dyn

[0x800013b4]:fdiv.s t6, t5, t4, dyn
[0x800013b8]:csrrs a2, fcsr, zero
[0x800013bc]:sw t6, 984(fp)
[0x800013c0]:sw a2, 988(fp)
[0x800013c4]:lw t5, 1016(a1)
[0x800013c8]:lw t4, 1020(a1)
[0x800013cc]:addi s1, zero, 130
[0x800013d0]:csrrw zero, fcsr, s1
[0x800013d4]:fdiv.s t6, t5, t4, dyn

[0x800013d4]:fdiv.s t6, t5, t4, dyn
[0x800013d8]:csrrs a2, fcsr, zero
[0x800013dc]:sw t6, 992(fp)
[0x800013e0]:sw a2, 996(fp)
[0x800013e4]:lw t5, 1024(a1)
[0x800013e8]:lw t4, 1028(a1)
[0x800013ec]:addi s1, zero, 2
[0x800013f0]:csrrw zero, fcsr, s1
[0x800013f4]:fdiv.s t6, t5, t4, dyn

[0x800013f4]:fdiv.s t6, t5, t4, dyn
[0x800013f8]:csrrs a2, fcsr, zero
[0x800013fc]:sw t6, 1000(fp)
[0x80001400]:sw a2, 1004(fp)
[0x80001404]:lw t5, 1032(a1)
[0x80001408]:lw t4, 1036(a1)
[0x8000140c]:addi s1, zero, 34
[0x80001410]:csrrw zero, fcsr, s1
[0x80001414]:fdiv.s t6, t5, t4, dyn

[0x80001414]:fdiv.s t6, t5, t4, dyn
[0x80001418]:csrrs a2, fcsr, zero
[0x8000141c]:sw t6, 1008(fp)
[0x80001420]:sw a2, 1012(fp)
[0x80001424]:lw t5, 1040(a1)
[0x80001428]:lw t4, 1044(a1)
[0x8000142c]:addi s1, zero, 66
[0x80001430]:csrrw zero, fcsr, s1
[0x80001434]:fdiv.s t6, t5, t4, dyn

[0x80001434]:fdiv.s t6, t5, t4, dyn
[0x80001438]:csrrs a2, fcsr, zero
[0x8000143c]:sw t6, 1016(fp)
[0x80001440]:sw a2, 1020(fp)
[0x80001444]:auipc fp, 3
[0x80001448]:addi fp, fp, 2456
[0x8000144c]:lw t5, 1048(a1)
[0x80001450]:lw t4, 1052(a1)
[0x80001454]:addi s1, zero, 98
[0x80001458]:csrrw zero, fcsr, s1
[0x8000145c]:fdiv.s t6, t5, t4, dyn

[0x8000145c]:fdiv.s t6, t5, t4, dyn
[0x80001460]:csrrs a2, fcsr, zero
[0x80001464]:sw t6, 0(fp)
[0x80001468]:sw a2, 4(fp)
[0x8000146c]:lw t5, 1056(a1)
[0x80001470]:lw t4, 1060(a1)
[0x80001474]:addi s1, zero, 130
[0x80001478]:csrrw zero, fcsr, s1
[0x8000147c]:fdiv.s t6, t5, t4, dyn

[0x8000147c]:fdiv.s t6, t5, t4, dyn
[0x80001480]:csrrs a2, fcsr, zero
[0x80001484]:sw t6, 8(fp)
[0x80001488]:sw a2, 12(fp)
[0x8000148c]:lw t5, 1064(a1)
[0x80001490]:lw t4, 1068(a1)
[0x80001494]:addi s1, zero, 2
[0x80001498]:csrrw zero, fcsr, s1
[0x8000149c]:fdiv.s t6, t5, t4, dyn

[0x8000149c]:fdiv.s t6, t5, t4, dyn
[0x800014a0]:csrrs a2, fcsr, zero
[0x800014a4]:sw t6, 16(fp)
[0x800014a8]:sw a2, 20(fp)
[0x800014ac]:lw t5, 1072(a1)
[0x800014b0]:lw t4, 1076(a1)
[0x800014b4]:addi s1, zero, 34
[0x800014b8]:csrrw zero, fcsr, s1
[0x800014bc]:fdiv.s t6, t5, t4, dyn

[0x800014bc]:fdiv.s t6, t5, t4, dyn
[0x800014c0]:csrrs a2, fcsr, zero
[0x800014c4]:sw t6, 24(fp)
[0x800014c8]:sw a2, 28(fp)
[0x800014cc]:lw t5, 1080(a1)
[0x800014d0]:lw t4, 1084(a1)
[0x800014d4]:addi s1, zero, 66
[0x800014d8]:csrrw zero, fcsr, s1
[0x800014dc]:fdiv.s t6, t5, t4, dyn

[0x800014dc]:fdiv.s t6, t5, t4, dyn
[0x800014e0]:csrrs a2, fcsr, zero
[0x800014e4]:sw t6, 32(fp)
[0x800014e8]:sw a2, 36(fp)
[0x800014ec]:lw t5, 1088(a1)
[0x800014f0]:lw t4, 1092(a1)
[0x800014f4]:addi s1, zero, 98
[0x800014f8]:csrrw zero, fcsr, s1
[0x800014fc]:fdiv.s t6, t5, t4, dyn

[0x800014fc]:fdiv.s t6, t5, t4, dyn
[0x80001500]:csrrs a2, fcsr, zero
[0x80001504]:sw t6, 40(fp)
[0x80001508]:sw a2, 44(fp)
[0x8000150c]:lw t5, 1096(a1)
[0x80001510]:lw t4, 1100(a1)
[0x80001514]:addi s1, zero, 130
[0x80001518]:csrrw zero, fcsr, s1
[0x8000151c]:fdiv.s t6, t5, t4, dyn

[0x8000151c]:fdiv.s t6, t5, t4, dyn
[0x80001520]:csrrs a2, fcsr, zero
[0x80001524]:sw t6, 48(fp)
[0x80001528]:sw a2, 52(fp)
[0x8000152c]:lw t5, 1104(a1)
[0x80001530]:lw t4, 1108(a1)
[0x80001534]:addi s1, zero, 2
[0x80001538]:csrrw zero, fcsr, s1
[0x8000153c]:fdiv.s t6, t5, t4, dyn

[0x8000153c]:fdiv.s t6, t5, t4, dyn
[0x80001540]:csrrs a2, fcsr, zero
[0x80001544]:sw t6, 56(fp)
[0x80001548]:sw a2, 60(fp)
[0x8000154c]:lw t5, 1112(a1)
[0x80001550]:lw t4, 1116(a1)
[0x80001554]:addi s1, zero, 34
[0x80001558]:csrrw zero, fcsr, s1
[0x8000155c]:fdiv.s t6, t5, t4, dyn

[0x8000155c]:fdiv.s t6, t5, t4, dyn
[0x80001560]:csrrs a2, fcsr, zero
[0x80001564]:sw t6, 64(fp)
[0x80001568]:sw a2, 68(fp)
[0x8000156c]:lw t5, 1120(a1)
[0x80001570]:lw t4, 1124(a1)
[0x80001574]:addi s1, zero, 66
[0x80001578]:csrrw zero, fcsr, s1
[0x8000157c]:fdiv.s t6, t5, t4, dyn

[0x8000157c]:fdiv.s t6, t5, t4, dyn
[0x80001580]:csrrs a2, fcsr, zero
[0x80001584]:sw t6, 72(fp)
[0x80001588]:sw a2, 76(fp)
[0x8000158c]:lw t5, 1128(a1)
[0x80001590]:lw t4, 1132(a1)
[0x80001594]:addi s1, zero, 98
[0x80001598]:csrrw zero, fcsr, s1
[0x8000159c]:fdiv.s t6, t5, t4, dyn

[0x8000159c]:fdiv.s t6, t5, t4, dyn
[0x800015a0]:csrrs a2, fcsr, zero
[0x800015a4]:sw t6, 80(fp)
[0x800015a8]:sw a2, 84(fp)
[0x800015ac]:lw t5, 1136(a1)
[0x800015b0]:lw t4, 1140(a1)
[0x800015b4]:addi s1, zero, 130
[0x800015b8]:csrrw zero, fcsr, s1
[0x800015bc]:fdiv.s t6, t5, t4, dyn

[0x800015bc]:fdiv.s t6, t5, t4, dyn
[0x800015c0]:csrrs a2, fcsr, zero
[0x800015c4]:sw t6, 88(fp)
[0x800015c8]:sw a2, 92(fp)
[0x800015cc]:lw t5, 1144(a1)
[0x800015d0]:lw t4, 1148(a1)
[0x800015d4]:addi s1, zero, 2
[0x800015d8]:csrrw zero, fcsr, s1
[0x800015dc]:fdiv.s t6, t5, t4, dyn

[0x800015dc]:fdiv.s t6, t5, t4, dyn
[0x800015e0]:csrrs a2, fcsr, zero
[0x800015e4]:sw t6, 96(fp)
[0x800015e8]:sw a2, 100(fp)
[0x800015ec]:lw t5, 1152(a1)
[0x800015f0]:lw t4, 1156(a1)
[0x800015f4]:addi s1, zero, 34
[0x800015f8]:csrrw zero, fcsr, s1
[0x800015fc]:fdiv.s t6, t5, t4, dyn

[0x800015fc]:fdiv.s t6, t5, t4, dyn
[0x80001600]:csrrs a2, fcsr, zero
[0x80001604]:sw t6, 104(fp)
[0x80001608]:sw a2, 108(fp)
[0x8000160c]:lw t5, 1160(a1)
[0x80001610]:lw t4, 1164(a1)
[0x80001614]:addi s1, zero, 66
[0x80001618]:csrrw zero, fcsr, s1
[0x8000161c]:fdiv.s t6, t5, t4, dyn

[0x8000161c]:fdiv.s t6, t5, t4, dyn
[0x80001620]:csrrs a2, fcsr, zero
[0x80001624]:sw t6, 112(fp)
[0x80001628]:sw a2, 116(fp)
[0x8000162c]:lw t5, 1168(a1)
[0x80001630]:lw t4, 1172(a1)
[0x80001634]:addi s1, zero, 98
[0x80001638]:csrrw zero, fcsr, s1
[0x8000163c]:fdiv.s t6, t5, t4, dyn

[0x8000163c]:fdiv.s t6, t5, t4, dyn
[0x80001640]:csrrs a2, fcsr, zero
[0x80001644]:sw t6, 120(fp)
[0x80001648]:sw a2, 124(fp)
[0x8000164c]:lw t5, 1176(a1)
[0x80001650]:lw t4, 1180(a1)
[0x80001654]:addi s1, zero, 130
[0x80001658]:csrrw zero, fcsr, s1
[0x8000165c]:fdiv.s t6, t5, t4, dyn

[0x8000165c]:fdiv.s t6, t5, t4, dyn
[0x80001660]:csrrs a2, fcsr, zero
[0x80001664]:sw t6, 128(fp)
[0x80001668]:sw a2, 132(fp)
[0x8000166c]:lw t5, 1184(a1)
[0x80001670]:lw t4, 1188(a1)
[0x80001674]:addi s1, zero, 2
[0x80001678]:csrrw zero, fcsr, s1
[0x8000167c]:fdiv.s t6, t5, t4, dyn

[0x8000167c]:fdiv.s t6, t5, t4, dyn
[0x80001680]:csrrs a2, fcsr, zero
[0x80001684]:sw t6, 136(fp)
[0x80001688]:sw a2, 140(fp)
[0x8000168c]:lw t5, 1192(a1)
[0x80001690]:lw t4, 1196(a1)
[0x80001694]:addi s1, zero, 34
[0x80001698]:csrrw zero, fcsr, s1
[0x8000169c]:fdiv.s t6, t5, t4, dyn

[0x8000169c]:fdiv.s t6, t5, t4, dyn
[0x800016a0]:csrrs a2, fcsr, zero
[0x800016a4]:sw t6, 144(fp)
[0x800016a8]:sw a2, 148(fp)
[0x800016ac]:lw t5, 1200(a1)
[0x800016b0]:lw t4, 1204(a1)
[0x800016b4]:addi s1, zero, 66
[0x800016b8]:csrrw zero, fcsr, s1
[0x800016bc]:fdiv.s t6, t5, t4, dyn

[0x800016bc]:fdiv.s t6, t5, t4, dyn
[0x800016c0]:csrrs a2, fcsr, zero
[0x800016c4]:sw t6, 152(fp)
[0x800016c8]:sw a2, 156(fp)
[0x800016cc]:lw t5, 1208(a1)
[0x800016d0]:lw t4, 1212(a1)
[0x800016d4]:addi s1, zero, 98
[0x800016d8]:csrrw zero, fcsr, s1
[0x800016dc]:fdiv.s t6, t5, t4, dyn

[0x800016dc]:fdiv.s t6, t5, t4, dyn
[0x800016e0]:csrrs a2, fcsr, zero
[0x800016e4]:sw t6, 160(fp)
[0x800016e8]:sw a2, 164(fp)
[0x800016ec]:lw t5, 1216(a1)
[0x800016f0]:lw t4, 1220(a1)
[0x800016f4]:addi s1, zero, 130
[0x800016f8]:csrrw zero, fcsr, s1
[0x800016fc]:fdiv.s t6, t5, t4, dyn

[0x800016fc]:fdiv.s t6, t5, t4, dyn
[0x80001700]:csrrs a2, fcsr, zero
[0x80001704]:sw t6, 168(fp)
[0x80001708]:sw a2, 172(fp)
[0x8000170c]:lw t5, 1224(a1)
[0x80001710]:lw t4, 1228(a1)
[0x80001714]:addi s1, zero, 2
[0x80001718]:csrrw zero, fcsr, s1
[0x8000171c]:fdiv.s t6, t5, t4, dyn

[0x8000171c]:fdiv.s t6, t5, t4, dyn
[0x80001720]:csrrs a2, fcsr, zero
[0x80001724]:sw t6, 176(fp)
[0x80001728]:sw a2, 180(fp)
[0x8000172c]:lw t5, 1232(a1)
[0x80001730]:lw t4, 1236(a1)
[0x80001734]:addi s1, zero, 34
[0x80001738]:csrrw zero, fcsr, s1
[0x8000173c]:fdiv.s t6, t5, t4, dyn

[0x8000173c]:fdiv.s t6, t5, t4, dyn
[0x80001740]:csrrs a2, fcsr, zero
[0x80001744]:sw t6, 184(fp)
[0x80001748]:sw a2, 188(fp)
[0x8000174c]:lw t5, 1240(a1)
[0x80001750]:lw t4, 1244(a1)
[0x80001754]:addi s1, zero, 66
[0x80001758]:csrrw zero, fcsr, s1
[0x8000175c]:fdiv.s t6, t5, t4, dyn

[0x8000175c]:fdiv.s t6, t5, t4, dyn
[0x80001760]:csrrs a2, fcsr, zero
[0x80001764]:sw t6, 192(fp)
[0x80001768]:sw a2, 196(fp)
[0x8000176c]:lw t5, 1248(a1)
[0x80001770]:lw t4, 1252(a1)
[0x80001774]:addi s1, zero, 98
[0x80001778]:csrrw zero, fcsr, s1
[0x8000177c]:fdiv.s t6, t5, t4, dyn

[0x8000177c]:fdiv.s t6, t5, t4, dyn
[0x80001780]:csrrs a2, fcsr, zero
[0x80001784]:sw t6, 200(fp)
[0x80001788]:sw a2, 204(fp)
[0x8000178c]:lw t5, 1256(a1)
[0x80001790]:lw t4, 1260(a1)
[0x80001794]:addi s1, zero, 130
[0x80001798]:csrrw zero, fcsr, s1
[0x8000179c]:fdiv.s t6, t5, t4, dyn

[0x8000179c]:fdiv.s t6, t5, t4, dyn
[0x800017a0]:csrrs a2, fcsr, zero
[0x800017a4]:sw t6, 208(fp)
[0x800017a8]:sw a2, 212(fp)
[0x800017ac]:lw t5, 1264(a1)
[0x800017b0]:lw t4, 1268(a1)
[0x800017b4]:addi s1, zero, 2
[0x800017b8]:csrrw zero, fcsr, s1
[0x800017bc]:fdiv.s t6, t5, t4, dyn

[0x800017bc]:fdiv.s t6, t5, t4, dyn
[0x800017c0]:csrrs a2, fcsr, zero
[0x800017c4]:sw t6, 216(fp)
[0x800017c8]:sw a2, 220(fp)
[0x800017cc]:lw t5, 1272(a1)
[0x800017d0]:lw t4, 1276(a1)
[0x800017d4]:addi s1, zero, 34
[0x800017d8]:csrrw zero, fcsr, s1
[0x800017dc]:fdiv.s t6, t5, t4, dyn

[0x800017dc]:fdiv.s t6, t5, t4, dyn
[0x800017e0]:csrrs a2, fcsr, zero
[0x800017e4]:sw t6, 224(fp)
[0x800017e8]:sw a2, 228(fp)
[0x800017ec]:lw t5, 1280(a1)
[0x800017f0]:lw t4, 1284(a1)
[0x800017f4]:addi s1, zero, 66
[0x800017f8]:csrrw zero, fcsr, s1
[0x800017fc]:fdiv.s t6, t5, t4, dyn

[0x800017fc]:fdiv.s t6, t5, t4, dyn
[0x80001800]:csrrs a2, fcsr, zero
[0x80001804]:sw t6, 232(fp)
[0x80001808]:sw a2, 236(fp)
[0x8000180c]:lw t5, 1288(a1)
[0x80001810]:lw t4, 1292(a1)
[0x80001814]:addi s1, zero, 98
[0x80001818]:csrrw zero, fcsr, s1
[0x8000181c]:fdiv.s t6, t5, t4, dyn

[0x8000181c]:fdiv.s t6, t5, t4, dyn
[0x80001820]:csrrs a2, fcsr, zero
[0x80001824]:sw t6, 240(fp)
[0x80001828]:sw a2, 244(fp)
[0x8000182c]:lw t5, 1296(a1)
[0x80001830]:lw t4, 1300(a1)
[0x80001834]:addi s1, zero, 130
[0x80001838]:csrrw zero, fcsr, s1
[0x8000183c]:fdiv.s t6, t5, t4, dyn

[0x8000183c]:fdiv.s t6, t5, t4, dyn
[0x80001840]:csrrs a2, fcsr, zero
[0x80001844]:sw t6, 248(fp)
[0x80001848]:sw a2, 252(fp)
[0x8000184c]:lw t5, 1304(a1)
[0x80001850]:lw t4, 1308(a1)
[0x80001854]:addi s1, zero, 2
[0x80001858]:csrrw zero, fcsr, s1
[0x8000185c]:fdiv.s t6, t5, t4, dyn

[0x8000185c]:fdiv.s t6, t5, t4, dyn
[0x80001860]:csrrs a2, fcsr, zero
[0x80001864]:sw t6, 256(fp)
[0x80001868]:sw a2, 260(fp)
[0x8000186c]:lw t5, 1312(a1)
[0x80001870]:lw t4, 1316(a1)
[0x80001874]:addi s1, zero, 34
[0x80001878]:csrrw zero, fcsr, s1
[0x8000187c]:fdiv.s t6, t5, t4, dyn

[0x8000187c]:fdiv.s t6, t5, t4, dyn
[0x80001880]:csrrs a2, fcsr, zero
[0x80001884]:sw t6, 264(fp)
[0x80001888]:sw a2, 268(fp)
[0x8000188c]:lw t5, 1320(a1)
[0x80001890]:lw t4, 1324(a1)
[0x80001894]:addi s1, zero, 66
[0x80001898]:csrrw zero, fcsr, s1
[0x8000189c]:fdiv.s t6, t5, t4, dyn

[0x8000189c]:fdiv.s t6, t5, t4, dyn
[0x800018a0]:csrrs a2, fcsr, zero
[0x800018a4]:sw t6, 272(fp)
[0x800018a8]:sw a2, 276(fp)
[0x800018ac]:lw t5, 1328(a1)
[0x800018b0]:lw t4, 1332(a1)
[0x800018b4]:addi s1, zero, 98
[0x800018b8]:csrrw zero, fcsr, s1
[0x800018bc]:fdiv.s t6, t5, t4, dyn

[0x800018bc]:fdiv.s t6, t5, t4, dyn
[0x800018c0]:csrrs a2, fcsr, zero
[0x800018c4]:sw t6, 280(fp)
[0x800018c8]:sw a2, 284(fp)
[0x800018cc]:lw t5, 1336(a1)
[0x800018d0]:lw t4, 1340(a1)
[0x800018d4]:addi s1, zero, 130
[0x800018d8]:csrrw zero, fcsr, s1
[0x800018dc]:fdiv.s t6, t5, t4, dyn

[0x800018dc]:fdiv.s t6, t5, t4, dyn
[0x800018e0]:csrrs a2, fcsr, zero
[0x800018e4]:sw t6, 288(fp)
[0x800018e8]:sw a2, 292(fp)
[0x800018ec]:lw t5, 1344(a1)
[0x800018f0]:lw t4, 1348(a1)
[0x800018f4]:addi s1, zero, 2
[0x800018f8]:csrrw zero, fcsr, s1
[0x800018fc]:fdiv.s t6, t5, t4, dyn

[0x800018fc]:fdiv.s t6, t5, t4, dyn
[0x80001900]:csrrs a2, fcsr, zero
[0x80001904]:sw t6, 296(fp)
[0x80001908]:sw a2, 300(fp)
[0x8000190c]:lw t5, 1352(a1)
[0x80001910]:lw t4, 1356(a1)
[0x80001914]:addi s1, zero, 34
[0x80001918]:csrrw zero, fcsr, s1
[0x8000191c]:fdiv.s t6, t5, t4, dyn

[0x8000191c]:fdiv.s t6, t5, t4, dyn
[0x80001920]:csrrs a2, fcsr, zero
[0x80001924]:sw t6, 304(fp)
[0x80001928]:sw a2, 308(fp)
[0x8000192c]:lw t5, 1360(a1)
[0x80001930]:lw t4, 1364(a1)
[0x80001934]:addi s1, zero, 66
[0x80001938]:csrrw zero, fcsr, s1
[0x8000193c]:fdiv.s t6, t5, t4, dyn

[0x8000193c]:fdiv.s t6, t5, t4, dyn
[0x80001940]:csrrs a2, fcsr, zero
[0x80001944]:sw t6, 312(fp)
[0x80001948]:sw a2, 316(fp)
[0x8000194c]:lw t5, 1368(a1)
[0x80001950]:lw t4, 1372(a1)
[0x80001954]:addi s1, zero, 98
[0x80001958]:csrrw zero, fcsr, s1
[0x8000195c]:fdiv.s t6, t5, t4, dyn

[0x8000195c]:fdiv.s t6, t5, t4, dyn
[0x80001960]:csrrs a2, fcsr, zero
[0x80001964]:sw t6, 320(fp)
[0x80001968]:sw a2, 324(fp)
[0x8000196c]:lw t5, 1376(a1)
[0x80001970]:lw t4, 1380(a1)
[0x80001974]:addi s1, zero, 130
[0x80001978]:csrrw zero, fcsr, s1
[0x8000197c]:fdiv.s t6, t5, t4, dyn

[0x8000197c]:fdiv.s t6, t5, t4, dyn
[0x80001980]:csrrs a2, fcsr, zero
[0x80001984]:sw t6, 328(fp)
[0x80001988]:sw a2, 332(fp)
[0x8000198c]:lw t5, 1384(a1)
[0x80001990]:lw t4, 1388(a1)
[0x80001994]:addi s1, zero, 2
[0x80001998]:csrrw zero, fcsr, s1
[0x8000199c]:fdiv.s t6, t5, t4, dyn

[0x8000199c]:fdiv.s t6, t5, t4, dyn
[0x800019a0]:csrrs a2, fcsr, zero
[0x800019a4]:sw t6, 336(fp)
[0x800019a8]:sw a2, 340(fp)
[0x800019ac]:lw t5, 1392(a1)
[0x800019b0]:lw t4, 1396(a1)
[0x800019b4]:addi s1, zero, 34
[0x800019b8]:csrrw zero, fcsr, s1
[0x800019bc]:fdiv.s t6, t5, t4, dyn

[0x800019bc]:fdiv.s t6, t5, t4, dyn
[0x800019c0]:csrrs a2, fcsr, zero
[0x800019c4]:sw t6, 344(fp)
[0x800019c8]:sw a2, 348(fp)
[0x800019cc]:lw t5, 1400(a1)
[0x800019d0]:lw t4, 1404(a1)
[0x800019d4]:addi s1, zero, 66
[0x800019d8]:csrrw zero, fcsr, s1
[0x800019dc]:fdiv.s t6, t5, t4, dyn

[0x800019dc]:fdiv.s t6, t5, t4, dyn
[0x800019e0]:csrrs a2, fcsr, zero
[0x800019e4]:sw t6, 352(fp)
[0x800019e8]:sw a2, 356(fp)
[0x800019ec]:lw t5, 1408(a1)
[0x800019f0]:lw t4, 1412(a1)
[0x800019f4]:addi s1, zero, 98
[0x800019f8]:csrrw zero, fcsr, s1
[0x800019fc]:fdiv.s t6, t5, t4, dyn

[0x800019fc]:fdiv.s t6, t5, t4, dyn
[0x80001a00]:csrrs a2, fcsr, zero
[0x80001a04]:sw t6, 360(fp)
[0x80001a08]:sw a2, 364(fp)
[0x80001a0c]:lw t5, 1416(a1)
[0x80001a10]:lw t4, 1420(a1)
[0x80001a14]:addi s1, zero, 130
[0x80001a18]:csrrw zero, fcsr, s1
[0x80001a1c]:fdiv.s t6, t5, t4, dyn

[0x80001a1c]:fdiv.s t6, t5, t4, dyn
[0x80001a20]:csrrs a2, fcsr, zero
[0x80001a24]:sw t6, 368(fp)
[0x80001a28]:sw a2, 372(fp)
[0x80001a2c]:lw t5, 1424(a1)
[0x80001a30]:lw t4, 1428(a1)
[0x80001a34]:addi s1, zero, 2
[0x80001a38]:csrrw zero, fcsr, s1
[0x80001a3c]:fdiv.s t6, t5, t4, dyn

[0x80001a3c]:fdiv.s t6, t5, t4, dyn
[0x80001a40]:csrrs a2, fcsr, zero
[0x80001a44]:sw t6, 376(fp)
[0x80001a48]:sw a2, 380(fp)
[0x80001a4c]:lw t5, 1432(a1)
[0x80001a50]:lw t4, 1436(a1)
[0x80001a54]:addi s1, zero, 34
[0x80001a58]:csrrw zero, fcsr, s1
[0x80001a5c]:fdiv.s t6, t5, t4, dyn

[0x80001a5c]:fdiv.s t6, t5, t4, dyn
[0x80001a60]:csrrs a2, fcsr, zero
[0x80001a64]:sw t6, 384(fp)
[0x80001a68]:sw a2, 388(fp)
[0x80001a6c]:lw t5, 1440(a1)
[0x80001a70]:lw t4, 1444(a1)
[0x80001a74]:addi s1, zero, 66
[0x80001a78]:csrrw zero, fcsr, s1
[0x80001a7c]:fdiv.s t6, t5, t4, dyn

[0x80001a7c]:fdiv.s t6, t5, t4, dyn
[0x80001a80]:csrrs a2, fcsr, zero
[0x80001a84]:sw t6, 392(fp)
[0x80001a88]:sw a2, 396(fp)
[0x80001a8c]:lw t5, 1448(a1)
[0x80001a90]:lw t4, 1452(a1)
[0x80001a94]:addi s1, zero, 98
[0x80001a98]:csrrw zero, fcsr, s1
[0x80001a9c]:fdiv.s t6, t5, t4, dyn

[0x80001a9c]:fdiv.s t6, t5, t4, dyn
[0x80001aa0]:csrrs a2, fcsr, zero
[0x80001aa4]:sw t6, 400(fp)
[0x80001aa8]:sw a2, 404(fp)
[0x80001aac]:lw t5, 1456(a1)
[0x80001ab0]:lw t4, 1460(a1)
[0x80001ab4]:addi s1, zero, 130
[0x80001ab8]:csrrw zero, fcsr, s1
[0x80001abc]:fdiv.s t6, t5, t4, dyn

[0x80001abc]:fdiv.s t6, t5, t4, dyn
[0x80001ac0]:csrrs a2, fcsr, zero
[0x80001ac4]:sw t6, 408(fp)
[0x80001ac8]:sw a2, 412(fp)
[0x80001acc]:lw t5, 1464(a1)
[0x80001ad0]:lw t4, 1468(a1)
[0x80001ad4]:addi s1, zero, 2
[0x80001ad8]:csrrw zero, fcsr, s1
[0x80001adc]:fdiv.s t6, t5, t4, dyn

[0x80001adc]:fdiv.s t6, t5, t4, dyn
[0x80001ae0]:csrrs a2, fcsr, zero
[0x80001ae4]:sw t6, 416(fp)
[0x80001ae8]:sw a2, 420(fp)
[0x80001aec]:lw t5, 1472(a1)
[0x80001af0]:lw t4, 1476(a1)
[0x80001af4]:addi s1, zero, 34
[0x80001af8]:csrrw zero, fcsr, s1
[0x80001afc]:fdiv.s t6, t5, t4, dyn

[0x80001afc]:fdiv.s t6, t5, t4, dyn
[0x80001b00]:csrrs a2, fcsr, zero
[0x80001b04]:sw t6, 424(fp)
[0x80001b08]:sw a2, 428(fp)
[0x80001b0c]:lw t5, 1480(a1)
[0x80001b10]:lw t4, 1484(a1)
[0x80001b14]:addi s1, zero, 66
[0x80001b18]:csrrw zero, fcsr, s1
[0x80001b1c]:fdiv.s t6, t5, t4, dyn

[0x80001b1c]:fdiv.s t6, t5, t4, dyn
[0x80001b20]:csrrs a2, fcsr, zero
[0x80001b24]:sw t6, 432(fp)
[0x80001b28]:sw a2, 436(fp)
[0x80001b2c]:lw t5, 1488(a1)
[0x80001b30]:lw t4, 1492(a1)
[0x80001b34]:addi s1, zero, 98
[0x80001b38]:csrrw zero, fcsr, s1
[0x80001b3c]:fdiv.s t6, t5, t4, dyn

[0x80001b3c]:fdiv.s t6, t5, t4, dyn
[0x80001b40]:csrrs a2, fcsr, zero
[0x80001b44]:sw t6, 440(fp)
[0x80001b48]:sw a2, 444(fp)
[0x80001b4c]:lw t5, 1496(a1)
[0x80001b50]:lw t4, 1500(a1)
[0x80001b54]:addi s1, zero, 130
[0x80001b58]:csrrw zero, fcsr, s1
[0x80001b5c]:fdiv.s t6, t5, t4, dyn

[0x80001b5c]:fdiv.s t6, t5, t4, dyn
[0x80001b60]:csrrs a2, fcsr, zero
[0x80001b64]:sw t6, 448(fp)
[0x80001b68]:sw a2, 452(fp)
[0x80001b6c]:lw t5, 1504(a1)
[0x80001b70]:lw t4, 1508(a1)
[0x80001b74]:addi s1, zero, 2
[0x80001b78]:csrrw zero, fcsr, s1
[0x80001b7c]:fdiv.s t6, t5, t4, dyn

[0x80001b7c]:fdiv.s t6, t5, t4, dyn
[0x80001b80]:csrrs a2, fcsr, zero
[0x80001b84]:sw t6, 456(fp)
[0x80001b88]:sw a2, 460(fp)
[0x80001b8c]:lw t5, 1512(a1)
[0x80001b90]:lw t4, 1516(a1)
[0x80001b94]:addi s1, zero, 34
[0x80001b98]:csrrw zero, fcsr, s1
[0x80001b9c]:fdiv.s t6, t5, t4, dyn

[0x80001b9c]:fdiv.s t6, t5, t4, dyn
[0x80001ba0]:csrrs a2, fcsr, zero
[0x80001ba4]:sw t6, 464(fp)
[0x80001ba8]:sw a2, 468(fp)
[0x80001bac]:lw t5, 1520(a1)
[0x80001bb0]:lw t4, 1524(a1)
[0x80001bb4]:addi s1, zero, 66
[0x80001bb8]:csrrw zero, fcsr, s1
[0x80001bbc]:fdiv.s t6, t5, t4, dyn

[0x80001bbc]:fdiv.s t6, t5, t4, dyn
[0x80001bc0]:csrrs a2, fcsr, zero
[0x80001bc4]:sw t6, 472(fp)
[0x80001bc8]:sw a2, 476(fp)
[0x80001bcc]:lw t5, 1528(a1)
[0x80001bd0]:lw t4, 1532(a1)
[0x80001bd4]:addi s1, zero, 98
[0x80001bd8]:csrrw zero, fcsr, s1
[0x80001bdc]:fdiv.s t6, t5, t4, dyn

[0x80001bdc]:fdiv.s t6, t5, t4, dyn
[0x80001be0]:csrrs a2, fcsr, zero
[0x80001be4]:sw t6, 480(fp)
[0x80001be8]:sw a2, 484(fp)
[0x80001bec]:lw t5, 1536(a1)
[0x80001bf0]:lw t4, 1540(a1)
[0x80001bf4]:addi s1, zero, 130
[0x80001bf8]:csrrw zero, fcsr, s1
[0x80001bfc]:fdiv.s t6, t5, t4, dyn

[0x80001bfc]:fdiv.s t6, t5, t4, dyn
[0x80001c00]:csrrs a2, fcsr, zero
[0x80001c04]:sw t6, 488(fp)
[0x80001c08]:sw a2, 492(fp)
[0x80001c0c]:lw t5, 1544(a1)
[0x80001c10]:lw t4, 1548(a1)
[0x80001c14]:addi s1, zero, 2
[0x80001c18]:csrrw zero, fcsr, s1
[0x80001c1c]:fdiv.s t6, t5, t4, dyn

[0x80001c1c]:fdiv.s t6, t5, t4, dyn
[0x80001c20]:csrrs a2, fcsr, zero
[0x80001c24]:sw t6, 496(fp)
[0x80001c28]:sw a2, 500(fp)
[0x80001c2c]:lw t5, 1552(a1)
[0x80001c30]:lw t4, 1556(a1)
[0x80001c34]:addi s1, zero, 34
[0x80001c38]:csrrw zero, fcsr, s1
[0x80001c3c]:fdiv.s t6, t5, t4, dyn

[0x80001c3c]:fdiv.s t6, t5, t4, dyn
[0x80001c40]:csrrs a2, fcsr, zero
[0x80001c44]:sw t6, 504(fp)
[0x80001c48]:sw a2, 508(fp)
[0x80001c4c]:lw t5, 1560(a1)
[0x80001c50]:lw t4, 1564(a1)
[0x80001c54]:addi s1, zero, 66
[0x80001c58]:csrrw zero, fcsr, s1
[0x80001c5c]:fdiv.s t6, t5, t4, dyn

[0x80001c5c]:fdiv.s t6, t5, t4, dyn
[0x80001c60]:csrrs a2, fcsr, zero
[0x80001c64]:sw t6, 512(fp)
[0x80001c68]:sw a2, 516(fp)
[0x80001c6c]:lw t5, 1568(a1)
[0x80001c70]:lw t4, 1572(a1)
[0x80001c74]:addi s1, zero, 98
[0x80001c78]:csrrw zero, fcsr, s1
[0x80001c7c]:fdiv.s t6, t5, t4, dyn

[0x80001c7c]:fdiv.s t6, t5, t4, dyn
[0x80001c80]:csrrs a2, fcsr, zero
[0x80001c84]:sw t6, 520(fp)
[0x80001c88]:sw a2, 524(fp)
[0x80001c8c]:lw t5, 1576(a1)
[0x80001c90]:lw t4, 1580(a1)
[0x80001c94]:addi s1, zero, 130
[0x80001c98]:csrrw zero, fcsr, s1
[0x80001c9c]:fdiv.s t6, t5, t4, dyn

[0x80001c9c]:fdiv.s t6, t5, t4, dyn
[0x80001ca0]:csrrs a2, fcsr, zero
[0x80001ca4]:sw t6, 528(fp)
[0x80001ca8]:sw a2, 532(fp)
[0x80001cac]:lw t5, 1584(a1)
[0x80001cb0]:lw t4, 1588(a1)
[0x80001cb4]:addi s1, zero, 2
[0x80001cb8]:csrrw zero, fcsr, s1
[0x80001cbc]:fdiv.s t6, t5, t4, dyn

[0x80001cbc]:fdiv.s t6, t5, t4, dyn
[0x80001cc0]:csrrs a2, fcsr, zero
[0x80001cc4]:sw t6, 536(fp)
[0x80001cc8]:sw a2, 540(fp)
[0x80001ccc]:lw t5, 1592(a1)
[0x80001cd0]:lw t4, 1596(a1)
[0x80001cd4]:addi s1, zero, 66
[0x80001cd8]:csrrw zero, fcsr, s1
[0x80001cdc]:fdiv.s t6, t5, t4, dyn

[0x80001cdc]:fdiv.s t6, t5, t4, dyn
[0x80001ce0]:csrrs a2, fcsr, zero
[0x80001ce4]:sw t6, 544(fp)
[0x80001ce8]:sw a2, 548(fp)
[0x80001cec]:lw t5, 1600(a1)
[0x80001cf0]:lw t4, 1604(a1)
[0x80001cf4]:addi s1, zero, 2
[0x80001cf8]:csrrw zero, fcsr, s1
[0x80001cfc]:fdiv.s t6, t5, t4, dyn

[0x80001cfc]:fdiv.s t6, t5, t4, dyn
[0x80001d00]:csrrs a2, fcsr, zero
[0x80001d04]:sw t6, 552(fp)
[0x80001d08]:sw a2, 556(fp)
[0x80001d0c]:lw t5, 1608(a1)
[0x80001d10]:lw t4, 1612(a1)
[0x80001d14]:addi s1, zero, 34
[0x80001d18]:csrrw zero, fcsr, s1
[0x80001d1c]:fdiv.s t6, t5, t4, dyn

[0x80001d1c]:fdiv.s t6, t5, t4, dyn
[0x80001d20]:csrrs a2, fcsr, zero
[0x80001d24]:sw t6, 560(fp)
[0x80001d28]:sw a2, 564(fp)
[0x80001d2c]:lw t5, 1616(a1)
[0x80001d30]:lw t4, 1620(a1)
[0x80001d34]:addi s1, zero, 98
[0x80001d38]:csrrw zero, fcsr, s1
[0x80001d3c]:fdiv.s t6, t5, t4, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000124]:fdiv.s t6, t6, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
	-[0x80000130]:sw tp, 4(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003918]:0x00000002




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.s t5, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
	-[0x80000150]:sw tp, 12(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80003920]:0x00000023




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fdiv.s t4, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t4, 16(ra)
	-[0x80000170]:sw tp, 20(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003928]:0x00000042




Last Coverpoint : ['rs1 : x30', 'rs2 : x29', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.s t3, t5, t4, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
	-[0x80000190]:sw tp, 28(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80003930]:0x00000063




Last Coverpoint : ['rs1 : x27', 'rs2 : x26', 'rd : x27', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fdiv.s s11, s11, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
	-[0x800001b0]:sw tp, 36(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003938]:0x00000083




Last Coverpoint : ['rs1 : x25', 'rs2 : x27', 'rd : x26', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.s s10, s9, s11, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
	-[0x800001d0]:sw tp, 44(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80003940]:0x00000003




Last Coverpoint : ['rs1 : x26', 'rs2 : x24', 'rd : x25', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.s s9, s10, s8, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
	-[0x800001f0]:sw tp, 52(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003948]:0x00000023




Last Coverpoint : ['rs1 : x23', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.s s8, s7, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
	-[0x80000210]:sw tp, 60(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80003950]:0x00000043




Last Coverpoint : ['rs1 : x24', 'rs2 : x22', 'rd : x23', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.s s7, s8, s6, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
	-[0x80000230]:sw tp, 68(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003958]:0x00000063




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.s s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
	-[0x80000250]:sw tp, 76(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80003960]:0x00000083




Last Coverpoint : ['rs1 : x22', 'rs2 : x20', 'rd : x21', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.s s5, s6, s4, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
	-[0x80000270]:sw tp, 84(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003968]:0x00000003




Last Coverpoint : ['rs1 : x19', 'rs2 : x21', 'rd : x20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.s s4, s3, s5, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
	-[0x80000290]:sw tp, 92(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80003970]:0x00000023




Last Coverpoint : ['rs1 : x20', 'rs2 : x18', 'rd : x19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.s s3, s4, s2, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
	-[0x800002b0]:sw tp, 100(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003978]:0x00000043




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.s s2, a7, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
	-[0x800002d0]:sw tp, 108(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80003980]:0x00000063




Last Coverpoint : ['rs1 : x18', 'rs2 : x16', 'rd : x17', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.s a7, s2, a6, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
	-[0x800002f0]:sw tp, 116(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003988]:0x00000083




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.s a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
	-[0x80000310]:sw tp, 124(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80003990]:0x00000003




Last Coverpoint : ['rs1 : x16', 'rs2 : x14', 'rd : x15', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.s a5, a6, a4, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
	-[0x80000330]:sw tp, 132(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003998]:0x00000023




Last Coverpoint : ['rs1 : x13', 'rs2 : x15', 'rd : x14', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.s a4, a3, a5, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
	-[0x80000350]:sw tp, 140(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800039a0]:0x00000043




Last Coverpoint : ['rs1 : x14', 'rs2 : x12', 'rd : x13', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.s a3, a4, a2, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
	-[0x80000370]:sw tp, 148(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800039a8]:0x00000063




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.s a2, a1, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
	-[0x80000390]:sw tp, 156(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800039b0]:0x00000083




Last Coverpoint : ['rs1 : x12', 'rs2 : x10', 'rd : x11', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.s a1, a2, a0, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
	-[0x800003b0]:sw tp, 164(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800039b8]:0x00000003




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.s a0, s1, a1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
	-[0x800003d0]:sw tp, 172(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800039c0]:0x00000023




Last Coverpoint : ['rs1 : x10', 'rs2 : x8', 'rd : x9', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.s s1, a0, fp, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
	-[0x800003f8]:sw a2, 180(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800039c8]:0x00000043




Last Coverpoint : ['rs1 : x7', 'rs2 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.s fp, t2, s1, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
	-[0x80000418]:sw a2, 188(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800039d0]:0x00000063




Last Coverpoint : ['rs1 : x8', 'rs2 : x6', 'rd : x7', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.s t2, fp, t1, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
	-[0x80000438]:sw a2, 196(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800039d8]:0x00000083




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.s t1, t0, t2, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
	-[0x80000460]:sw a2, 4(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800039e0]:0x00000003




Last Coverpoint : ['rs1 : x6', 'rs2 : x4', 'rd : x5', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.s t0, t1, tp, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
	-[0x80000480]:sw a2, 12(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800039e8]:0x00000023




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.s tp, gp, t0, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
	-[0x800004a0]:sw a2, 20(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800039f0]:0x00000043




Last Coverpoint : ['rs1 : x4', 'rs2 : x2', 'rd : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.s gp, tp, sp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
	-[0x800004c0]:sw a2, 28(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800039f8]:0x00000063




Last Coverpoint : ['rs1 : x1', 'rs2 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.s sp, ra, gp, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
	-[0x800004e0]:sw a2, 36(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x80003a00]:0x00000083




Last Coverpoint : ['rs1 : x2', 'rs2 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.s ra, sp, zero, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
	-[0x80000500]:sw a2, 44(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80003a08]:0x0000000A




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000514]:fdiv.s t6, zero, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
	-[0x80000520]:sw a2, 52(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x80003a10]:0x00000022




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000534]:fdiv.s t6, t5, ra, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
	-[0x80000540]:sw a2, 60(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80003a18]:0x00000043




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.s zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
	-[0x80000560]:sw a2, 68(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x80003a20]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.s t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
	-[0x80000580]:sw a2, 76(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80003a28]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.s t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
	-[0x800005a0]:sw a2, 84(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x80003a30]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.s t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
	-[0x800005c0]:sw a2, 92(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80003a38]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.s t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
	-[0x800005e0]:sw a2, 100(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x80003a40]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.s t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
	-[0x80000600]:sw a2, 108(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80003a48]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.s t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
	-[0x80000620]:sw a2, 116(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x80003a50]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.s t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
	-[0x80000640]:sw a2, 124(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80003a58]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.s t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
	-[0x80000660]:sw a2, 132(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x80003a60]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.s t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
	-[0x80000680]:sw a2, 140(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80003a68]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.s t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
	-[0x800006a0]:sw a2, 148(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x80003a70]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.s t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
	-[0x800006c0]:sw a2, 156(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80003a78]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.s t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
	-[0x800006e0]:sw a2, 164(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x80003a80]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.s t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
	-[0x80000700]:sw a2, 172(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80003a88]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.s t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
	-[0x80000720]:sw a2, 180(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x80003a90]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.s t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
	-[0x80000740]:sw a2, 188(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80003a98]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.s t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
	-[0x80000760]:sw a2, 196(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x80003aa0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.s t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
	-[0x80000780]:sw a2, 204(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x80003aa8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.s t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
	-[0x800007a0]:sw a2, 212(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x80003ab0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.s t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
	-[0x800007c0]:sw a2, 220(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x80003ab8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.s t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
	-[0x800007e0]:sw a2, 228(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x80003ac0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.s t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
	-[0x80000800]:sw a2, 236(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x80003ac8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.s t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
	-[0x80000820]:sw a2, 244(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x80003ad0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.s t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
	-[0x80000840]:sw a2, 252(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x80003ad8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.s t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
	-[0x80000860]:sw a2, 260(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x80003ae0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.s t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
	-[0x80000880]:sw a2, 268(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x80003ae8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.s t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
	-[0x800008a0]:sw a2, 276(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x80003af0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.s t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
	-[0x800008c0]:sw a2, 284(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x80003af8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.s t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
	-[0x800008e0]:sw a2, 292(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x80003b00]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.s t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
	-[0x80000900]:sw a2, 300(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80003b08]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.s t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
	-[0x80000920]:sw a2, 308(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x80003b10]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.s t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
	-[0x80000940]:sw a2, 316(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80003b18]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.s t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
	-[0x80000960]:sw a2, 324(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x80003b20]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.s t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
	-[0x80000980]:sw a2, 332(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80003b28]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.s t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
	-[0x800009a0]:sw a2, 340(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x80003b30]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.s t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
	-[0x800009c0]:sw a2, 348(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80003b38]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.s t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
	-[0x800009e0]:sw a2, 356(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x80003b40]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.s t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
	-[0x80000a00]:sw a2, 364(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80003b48]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.s t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
	-[0x80000a20]:sw a2, 372(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x80003b50]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.s t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
	-[0x80000a40]:sw a2, 380(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80003b58]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.s t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
	-[0x80000a60]:sw a2, 388(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x80003b60]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.s t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
	-[0x80000a80]:sw a2, 396(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80003b68]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.s t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
	-[0x80000aa0]:sw a2, 404(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x80003b70]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
	-[0x80000ac0]:sw a2, 412(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80003b78]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
	-[0x80000ae0]:sw a2, 420(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x80003b80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.s t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
	-[0x80000b00]:sw a2, 428(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80003b88]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.s t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
	-[0x80000b20]:sw a2, 436(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x80003b90]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.s t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
	-[0x80000b40]:sw a2, 444(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80003b98]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.s t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
	-[0x80000b60]:sw a2, 452(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x80003ba0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.s t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
	-[0x80000b80]:sw a2, 460(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x80003ba8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.s t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
	-[0x80000ba0]:sw a2, 468(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x80003bb0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
	-[0x80000bc0]:sw a2, 476(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x80003bb8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
	-[0x80000be0]:sw a2, 484(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x80003bc0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
	-[0x80000c00]:sw a2, 492(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x80003bc8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.s t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
	-[0x80000c20]:sw a2, 500(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x80003bd0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.s t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
	-[0x80000c40]:sw a2, 508(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x80003bd8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.s t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
	-[0x80000c60]:sw a2, 516(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x80003be0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.s t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
	-[0x80000c80]:sw a2, 524(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x80003be8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.s t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
	-[0x80000ca0]:sw a2, 532(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x80003bf0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
	-[0x80000cc0]:sw a2, 540(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x80003bf8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
	-[0x80000ce0]:sw a2, 548(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x80003c00]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.s t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
	-[0x80000d00]:sw a2, 556(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80003c08]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.s t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
	-[0x80000d20]:sw a2, 564(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x80003c10]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.s t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
	-[0x80000d40]:sw a2, 572(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80003c18]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.s t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
	-[0x80000d60]:sw a2, 580(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x80003c20]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.s t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
	-[0x80000d80]:sw a2, 588(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80003c28]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.s t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
	-[0x80000da0]:sw a2, 596(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x80003c30]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.s t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
	-[0x80000dc0]:sw a2, 604(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80003c38]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
	-[0x80000de0]:sw a2, 612(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x80003c40]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.s t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
	-[0x80000e00]:sw a2, 620(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80003c48]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.s t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
	-[0x80000e20]:sw a2, 628(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x80003c50]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.s t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
	-[0x80000e40]:sw a2, 636(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80003c58]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.s t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
	-[0x80000e60]:sw a2, 644(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x80003c60]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.s t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
	-[0x80000e80]:sw a2, 652(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80003c68]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.s t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
	-[0x80000ea0]:sw a2, 660(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x80003c70]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
	-[0x80000ec0]:sw a2, 668(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80003c78]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
	-[0x80000ee0]:sw a2, 676(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x80003c80]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
	-[0x80000f00]:sw a2, 684(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80003c88]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fdiv.s t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
	-[0x80000f20]:sw a2, 692(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x80003c90]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f34]:fdiv.s t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
	-[0x80000f40]:sw a2, 700(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80003c98]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f54]:fdiv.s t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
	-[0x80000f60]:sw a2, 708(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x80003ca0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f74]:fdiv.s t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
	-[0x80000f80]:sw a2, 716(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x80003ca8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f94]:fdiv.s t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
	-[0x80000fa0]:sw a2, 724(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x80003cb0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
	-[0x80000fc0]:sw a2, 732(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x80003cb8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fdiv.s t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
	-[0x80000fe0]:sw a2, 740(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x80003cc0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fdiv.s t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
	-[0x80001000]:sw a2, 748(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x80003cc8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001014]:fdiv.s t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
	-[0x80001020]:sw a2, 756(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x80003cd0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fdiv.s t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
	-[0x80001040]:sw a2, 764(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x80003cd8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001054]:fdiv.s t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
	-[0x80001060]:sw a2, 772(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x80003ce0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001074]:fdiv.s t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
	-[0x80001080]:sw a2, 780(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x80003ce8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001094]:fdiv.s t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
	-[0x800010a0]:sw a2, 788(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x80003cf0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010b4]:fdiv.s t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
	-[0x800010c0]:sw a2, 796(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x80003cf8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010d4]:fdiv.s t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
	-[0x800010e0]:sw a2, 804(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x80003d00]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010f4]:fdiv.s t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
	-[0x80001100]:sw a2, 812(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80003d08]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001114]:fdiv.s t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
	-[0x80001120]:sw a2, 820(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x80003d10]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001134]:fdiv.s t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
	-[0x80001140]:sw a2, 828(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80003d18]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fdiv.s t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
	-[0x80001160]:sw a2, 836(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x80003d20]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001174]:fdiv.s t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
	-[0x80001180]:sw a2, 844(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80003d28]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001194]:fdiv.s t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
	-[0x800011a0]:sw a2, 852(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x80003d30]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011b4]:fdiv.s t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
	-[0x800011c0]:sw a2, 860(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80003d38]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011d4]:fdiv.s t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
	-[0x800011e0]:sw a2, 868(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x80003d40]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011f4]:fdiv.s t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
	-[0x80001200]:sw a2, 876(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80003d48]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001214]:fdiv.s t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
	-[0x80001220]:sw a2, 884(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x80003d50]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001234]:fdiv.s t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
	-[0x80001240]:sw a2, 892(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80003d58]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001254]:fdiv.s t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
	-[0x80001260]:sw a2, 900(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x80003d60]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fdiv.s t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
	-[0x80001280]:sw a2, 908(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80003d68]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001294]:fdiv.s t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
	-[0x800012a0]:sw a2, 916(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x80003d70]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x253e0d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012b4]:fdiv.s t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
	-[0x800012c0]:sw a2, 924(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80003d78]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x253e0d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012d4]:fdiv.s t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
	-[0x800012e0]:sw a2, 932(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x80003d80]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x253e0d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012f4]:fdiv.s t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
	-[0x80001300]:sw a2, 940(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80003d88]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x253e0d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001314]:fdiv.s t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
	-[0x80001320]:sw a2, 948(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x80003d90]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x253e0d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001334]:fdiv.s t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
	-[0x80001340]:sw a2, 956(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80003d98]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2db39d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fdiv.s t6, t5, t4, dyn
	-[0x80001358]:csrrs a2, fcsr, zero
	-[0x8000135c]:sw t6, 960(fp)
	-[0x80001360]:sw a2, 964(fp)
Current Store : [0x80001360] : sw a2, 964(fp) -- Store: [0x80003da0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2db39d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001374]:fdiv.s t6, t5, t4, dyn
	-[0x80001378]:csrrs a2, fcsr, zero
	-[0x8000137c]:sw t6, 968(fp)
	-[0x80001380]:sw a2, 972(fp)
Current Store : [0x80001380] : sw a2, 972(fp) -- Store: [0x80003da8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2db39d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001394]:fdiv.s t6, t5, t4, dyn
	-[0x80001398]:csrrs a2, fcsr, zero
	-[0x8000139c]:sw t6, 976(fp)
	-[0x800013a0]:sw a2, 980(fp)
Current Store : [0x800013a0] : sw a2, 980(fp) -- Store: [0x80003db0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2db39d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013b4]:fdiv.s t6, t5, t4, dyn
	-[0x800013b8]:csrrs a2, fcsr, zero
	-[0x800013bc]:sw t6, 984(fp)
	-[0x800013c0]:sw a2, 988(fp)
Current Store : [0x800013c0] : sw a2, 988(fp) -- Store: [0x80003db8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2db39d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013d4]:fdiv.s t6, t5, t4, dyn
	-[0x800013d8]:csrrs a2, fcsr, zero
	-[0x800013dc]:sw t6, 992(fp)
	-[0x800013e0]:sw a2, 996(fp)
Current Store : [0x800013e0] : sw a2, 996(fp) -- Store: [0x80003dc0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x090d88 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013f4]:fdiv.s t6, t5, t4, dyn
	-[0x800013f8]:csrrs a2, fcsr, zero
	-[0x800013fc]:sw t6, 1000(fp)
	-[0x80001400]:sw a2, 1004(fp)
Current Store : [0x80001400] : sw a2, 1004(fp) -- Store: [0x80003dc8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x090d88 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001414]:fdiv.s t6, t5, t4, dyn
	-[0x80001418]:csrrs a2, fcsr, zero
	-[0x8000141c]:sw t6, 1008(fp)
	-[0x80001420]:sw a2, 1012(fp)
Current Store : [0x80001420] : sw a2, 1012(fp) -- Store: [0x80003dd0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x090d88 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001434]:fdiv.s t6, t5, t4, dyn
	-[0x80001438]:csrrs a2, fcsr, zero
	-[0x8000143c]:sw t6, 1016(fp)
	-[0x80001440]:sw a2, 1020(fp)
Current Store : [0x80001440] : sw a2, 1020(fp) -- Store: [0x80003dd8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x090d88 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000145c]:fdiv.s t6, t5, t4, dyn
	-[0x80001460]:csrrs a2, fcsr, zero
	-[0x80001464]:sw t6, 0(fp)
	-[0x80001468]:sw a2, 4(fp)
Current Store : [0x80001468] : sw a2, 4(fp) -- Store: [0x80003de0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x090d88 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000147c]:fdiv.s t6, t5, t4, dyn
	-[0x80001480]:csrrs a2, fcsr, zero
	-[0x80001484]:sw t6, 8(fp)
	-[0x80001488]:sw a2, 12(fp)
Current Store : [0x80001488] : sw a2, 12(fp) -- Store: [0x80003de8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fab30 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000149c]:fdiv.s t6, t5, t4, dyn
	-[0x800014a0]:csrrs a2, fcsr, zero
	-[0x800014a4]:sw t6, 16(fp)
	-[0x800014a8]:sw a2, 20(fp)
Current Store : [0x800014a8] : sw a2, 20(fp) -- Store: [0x80003df0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fab30 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fdiv.s t6, t5, t4, dyn
	-[0x800014c0]:csrrs a2, fcsr, zero
	-[0x800014c4]:sw t6, 24(fp)
	-[0x800014c8]:sw a2, 28(fp)
Current Store : [0x800014c8] : sw a2, 28(fp) -- Store: [0x80003df8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fab30 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014dc]:fdiv.s t6, t5, t4, dyn
	-[0x800014e0]:csrrs a2, fcsr, zero
	-[0x800014e4]:sw t6, 32(fp)
	-[0x800014e8]:sw a2, 36(fp)
Current Store : [0x800014e8] : sw a2, 36(fp) -- Store: [0x80003e00]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fab30 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001500]:csrrs a2, fcsr, zero
	-[0x80001504]:sw t6, 40(fp)
	-[0x80001508]:sw a2, 44(fp)
Current Store : [0x80001508] : sw a2, 44(fp) -- Store: [0x80003e08]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1fab30 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000151c]:fdiv.s t6, t5, t4, dyn
	-[0x80001520]:csrrs a2, fcsr, zero
	-[0x80001524]:sw t6, 48(fp)
	-[0x80001528]:sw a2, 52(fp)
Current Store : [0x80001528] : sw a2, 52(fp) -- Store: [0x80003e10]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ad8ea and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000153c]:fdiv.s t6, t5, t4, dyn
	-[0x80001540]:csrrs a2, fcsr, zero
	-[0x80001544]:sw t6, 56(fp)
	-[0x80001548]:sw a2, 60(fp)
Current Store : [0x80001548] : sw a2, 60(fp) -- Store: [0x80003e18]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ad8ea and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000155c]:fdiv.s t6, t5, t4, dyn
	-[0x80001560]:csrrs a2, fcsr, zero
	-[0x80001564]:sw t6, 64(fp)
	-[0x80001568]:sw a2, 68(fp)
Current Store : [0x80001568] : sw a2, 68(fp) -- Store: [0x80003e20]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ad8ea and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000157c]:fdiv.s t6, t5, t4, dyn
	-[0x80001580]:csrrs a2, fcsr, zero
	-[0x80001584]:sw t6, 72(fp)
	-[0x80001588]:sw a2, 76(fp)
Current Store : [0x80001588] : sw a2, 76(fp) -- Store: [0x80003e28]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ad8ea and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000159c]:fdiv.s t6, t5, t4, dyn
	-[0x800015a0]:csrrs a2, fcsr, zero
	-[0x800015a4]:sw t6, 80(fp)
	-[0x800015a8]:sw a2, 84(fp)
Current Store : [0x800015a8] : sw a2, 84(fp) -- Store: [0x80003e30]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5ad8ea and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015bc]:fdiv.s t6, t5, t4, dyn
	-[0x800015c0]:csrrs a2, fcsr, zero
	-[0x800015c4]:sw t6, 88(fp)
	-[0x800015c8]:sw a2, 92(fp)
Current Store : [0x800015c8] : sw a2, 92(fp) -- Store: [0x80003e38]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f4fa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015dc]:fdiv.s t6, t5, t4, dyn
	-[0x800015e0]:csrrs a2, fcsr, zero
	-[0x800015e4]:sw t6, 96(fp)
	-[0x800015e8]:sw a2, 100(fp)
Current Store : [0x800015e8] : sw a2, 100(fp) -- Store: [0x80003e40]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f4fa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001600]:csrrs a2, fcsr, zero
	-[0x80001604]:sw t6, 104(fp)
	-[0x80001608]:sw a2, 108(fp)
Current Store : [0x80001608] : sw a2, 108(fp) -- Store: [0x80003e48]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f4fa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000161c]:fdiv.s t6, t5, t4, dyn
	-[0x80001620]:csrrs a2, fcsr, zero
	-[0x80001624]:sw t6, 112(fp)
	-[0x80001628]:sw a2, 116(fp)
Current Store : [0x80001628] : sw a2, 116(fp) -- Store: [0x80003e50]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f4fa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000163c]:fdiv.s t6, t5, t4, dyn
	-[0x80001640]:csrrs a2, fcsr, zero
	-[0x80001644]:sw t6, 120(fp)
	-[0x80001648]:sw a2, 124(fp)
Current Store : [0x80001648] : sw a2, 124(fp) -- Store: [0x80003e58]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x23f4fa and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000165c]:fdiv.s t6, t5, t4, dyn
	-[0x80001660]:csrrs a2, fcsr, zero
	-[0x80001664]:sw t6, 128(fp)
	-[0x80001668]:sw a2, 132(fp)
Current Store : [0x80001668] : sw a2, 132(fp) -- Store: [0x80003e60]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x07f6ba and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000167c]:fdiv.s t6, t5, t4, dyn
	-[0x80001680]:csrrs a2, fcsr, zero
	-[0x80001684]:sw t6, 136(fp)
	-[0x80001688]:sw a2, 140(fp)
Current Store : [0x80001688] : sw a2, 140(fp) -- Store: [0x80003e68]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x07f6ba and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000169c]:fdiv.s t6, t5, t4, dyn
	-[0x800016a0]:csrrs a2, fcsr, zero
	-[0x800016a4]:sw t6, 144(fp)
	-[0x800016a8]:sw a2, 148(fp)
Current Store : [0x800016a8] : sw a2, 148(fp) -- Store: [0x80003e70]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x07f6ba and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016bc]:fdiv.s t6, t5, t4, dyn
	-[0x800016c0]:csrrs a2, fcsr, zero
	-[0x800016c4]:sw t6, 152(fp)
	-[0x800016c8]:sw a2, 156(fp)
Current Store : [0x800016c8] : sw a2, 156(fp) -- Store: [0x80003e78]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x07f6ba and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016dc]:fdiv.s t6, t5, t4, dyn
	-[0x800016e0]:csrrs a2, fcsr, zero
	-[0x800016e4]:sw t6, 160(fp)
	-[0x800016e8]:sw a2, 164(fp)
Current Store : [0x800016e8] : sw a2, 164(fp) -- Store: [0x80003e80]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x07f6ba and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001700]:csrrs a2, fcsr, zero
	-[0x80001704]:sw t6, 168(fp)
	-[0x80001708]:sw a2, 172(fp)
Current Store : [0x80001708] : sw a2, 172(fp) -- Store: [0x80003e88]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1d9e09 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000171c]:fdiv.s t6, t5, t4, dyn
	-[0x80001720]:csrrs a2, fcsr, zero
	-[0x80001724]:sw t6, 176(fp)
	-[0x80001728]:sw a2, 180(fp)
Current Store : [0x80001728] : sw a2, 180(fp) -- Store: [0x80003e90]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1d9e09 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000173c]:fdiv.s t6, t5, t4, dyn
	-[0x80001740]:csrrs a2, fcsr, zero
	-[0x80001744]:sw t6, 184(fp)
	-[0x80001748]:sw a2, 188(fp)
Current Store : [0x80001748] : sw a2, 188(fp) -- Store: [0x80003e98]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1d9e09 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000175c]:fdiv.s t6, t5, t4, dyn
	-[0x80001760]:csrrs a2, fcsr, zero
	-[0x80001764]:sw t6, 192(fp)
	-[0x80001768]:sw a2, 196(fp)
Current Store : [0x80001768] : sw a2, 196(fp) -- Store: [0x80003ea0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1d9e09 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000177c]:fdiv.s t6, t5, t4, dyn
	-[0x80001780]:csrrs a2, fcsr, zero
	-[0x80001784]:sw t6, 200(fp)
	-[0x80001788]:sw a2, 204(fp)
Current Store : [0x80001788] : sw a2, 204(fp) -- Store: [0x80003ea8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1d9e09 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000179c]:fdiv.s t6, t5, t4, dyn
	-[0x800017a0]:csrrs a2, fcsr, zero
	-[0x800017a4]:sw t6, 208(fp)
	-[0x800017a8]:sw a2, 212(fp)
Current Store : [0x800017a8] : sw a2, 212(fp) -- Store: [0x80003eb0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x61b55e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017bc]:fdiv.s t6, t5, t4, dyn
	-[0x800017c0]:csrrs a2, fcsr, zero
	-[0x800017c4]:sw t6, 216(fp)
	-[0x800017c8]:sw a2, 220(fp)
Current Store : [0x800017c8] : sw a2, 220(fp) -- Store: [0x80003eb8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x61b55e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017dc]:fdiv.s t6, t5, t4, dyn
	-[0x800017e0]:csrrs a2, fcsr, zero
	-[0x800017e4]:sw t6, 224(fp)
	-[0x800017e8]:sw a2, 228(fp)
Current Store : [0x800017e8] : sw a2, 228(fp) -- Store: [0x80003ec0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x61b55e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001800]:csrrs a2, fcsr, zero
	-[0x80001804]:sw t6, 232(fp)
	-[0x80001808]:sw a2, 236(fp)
Current Store : [0x80001808] : sw a2, 236(fp) -- Store: [0x80003ec8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x61b55e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000181c]:fdiv.s t6, t5, t4, dyn
	-[0x80001820]:csrrs a2, fcsr, zero
	-[0x80001824]:sw t6, 240(fp)
	-[0x80001828]:sw a2, 244(fp)
Current Store : [0x80001828] : sw a2, 244(fp) -- Store: [0x80003ed0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x61b55e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000183c]:fdiv.s t6, t5, t4, dyn
	-[0x80001840]:csrrs a2, fcsr, zero
	-[0x80001844]:sw t6, 248(fp)
	-[0x80001848]:sw a2, 252(fp)
Current Store : [0x80001848] : sw a2, 252(fp) -- Store: [0x80003ed8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27923e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000185c]:fdiv.s t6, t5, t4, dyn
	-[0x80001860]:csrrs a2, fcsr, zero
	-[0x80001864]:sw t6, 256(fp)
	-[0x80001868]:sw a2, 260(fp)
Current Store : [0x80001868] : sw a2, 260(fp) -- Store: [0x80003ee0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27923e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000187c]:fdiv.s t6, t5, t4, dyn
	-[0x80001880]:csrrs a2, fcsr, zero
	-[0x80001884]:sw t6, 264(fp)
	-[0x80001888]:sw a2, 268(fp)
Current Store : [0x80001888] : sw a2, 268(fp) -- Store: [0x80003ee8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27923e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000189c]:fdiv.s t6, t5, t4, dyn
	-[0x800018a0]:csrrs a2, fcsr, zero
	-[0x800018a4]:sw t6, 272(fp)
	-[0x800018a8]:sw a2, 276(fp)
Current Store : [0x800018a8] : sw a2, 276(fp) -- Store: [0x80003ef0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27923e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018bc]:fdiv.s t6, t5, t4, dyn
	-[0x800018c0]:csrrs a2, fcsr, zero
	-[0x800018c4]:sw t6, 280(fp)
	-[0x800018c8]:sw a2, 284(fp)
Current Store : [0x800018c8] : sw a2, 284(fp) -- Store: [0x80003ef8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x27923e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018dc]:fdiv.s t6, t5, t4, dyn
	-[0x800018e0]:csrrs a2, fcsr, zero
	-[0x800018e4]:sw t6, 288(fp)
	-[0x800018e8]:sw a2, 292(fp)
Current Store : [0x800018e8] : sw a2, 292(fp) -- Store: [0x80003f00]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6bfb00 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001900]:csrrs a2, fcsr, zero
	-[0x80001904]:sw t6, 296(fp)
	-[0x80001908]:sw a2, 300(fp)
Current Store : [0x80001908] : sw a2, 300(fp) -- Store: [0x80003f08]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6bfb00 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000191c]:fdiv.s t6, t5, t4, dyn
	-[0x80001920]:csrrs a2, fcsr, zero
	-[0x80001924]:sw t6, 304(fp)
	-[0x80001928]:sw a2, 308(fp)
Current Store : [0x80001928] : sw a2, 308(fp) -- Store: [0x80003f10]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6bfb00 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000193c]:fdiv.s t6, t5, t4, dyn
	-[0x80001940]:csrrs a2, fcsr, zero
	-[0x80001944]:sw t6, 312(fp)
	-[0x80001948]:sw a2, 316(fp)
Current Store : [0x80001948] : sw a2, 316(fp) -- Store: [0x80003f18]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6bfb00 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000195c]:fdiv.s t6, t5, t4, dyn
	-[0x80001960]:csrrs a2, fcsr, zero
	-[0x80001964]:sw t6, 320(fp)
	-[0x80001968]:sw a2, 324(fp)
Current Store : [0x80001968] : sw a2, 324(fp) -- Store: [0x80003f20]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6bfb00 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000197c]:fdiv.s t6, t5, t4, dyn
	-[0x80001980]:csrrs a2, fcsr, zero
	-[0x80001984]:sw t6, 328(fp)
	-[0x80001988]:sw a2, 332(fp)
Current Store : [0x80001988] : sw a2, 332(fp) -- Store: [0x80003f28]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000199c]:fdiv.s t6, t5, t4, dyn
	-[0x800019a0]:csrrs a2, fcsr, zero
	-[0x800019a4]:sw t6, 336(fp)
	-[0x800019a8]:sw a2, 340(fp)
Current Store : [0x800019a8] : sw a2, 340(fp) -- Store: [0x80003f30]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019bc]:fdiv.s t6, t5, t4, dyn
	-[0x800019c0]:csrrs a2, fcsr, zero
	-[0x800019c4]:sw t6, 344(fp)
	-[0x800019c8]:sw a2, 348(fp)
Current Store : [0x800019c8] : sw a2, 348(fp) -- Store: [0x80003f38]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019dc]:fdiv.s t6, t5, t4, dyn
	-[0x800019e0]:csrrs a2, fcsr, zero
	-[0x800019e4]:sw t6, 352(fp)
	-[0x800019e8]:sw a2, 356(fp)
Current Store : [0x800019e8] : sw a2, 356(fp) -- Store: [0x80003f40]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019fc]:fdiv.s t6, t5, t4, dyn
	-[0x80001a00]:csrrs a2, fcsr, zero
	-[0x80001a04]:sw t6, 360(fp)
	-[0x80001a08]:sw a2, 364(fp)
Current Store : [0x80001a08] : sw a2, 364(fp) -- Store: [0x80003f48]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x09f0c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a1c]:fdiv.s t6, t5, t4, dyn
	-[0x80001a20]:csrrs a2, fcsr, zero
	-[0x80001a24]:sw t6, 368(fp)
	-[0x80001a28]:sw a2, 372(fp)
Current Store : [0x80001a28] : sw a2, 372(fp) -- Store: [0x80003f50]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x292241 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a3c]:fdiv.s t6, t5, t4, dyn
	-[0x80001a40]:csrrs a2, fcsr, zero
	-[0x80001a44]:sw t6, 376(fp)
	-[0x80001a48]:sw a2, 380(fp)
Current Store : [0x80001a48] : sw a2, 380(fp) -- Store: [0x80003f58]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x292241 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a5c]:fdiv.s t6, t5, t4, dyn
	-[0x80001a60]:csrrs a2, fcsr, zero
	-[0x80001a64]:sw t6, 384(fp)
	-[0x80001a68]:sw a2, 388(fp)
Current Store : [0x80001a68] : sw a2, 388(fp) -- Store: [0x80003f60]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x292241 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a7c]:fdiv.s t6, t5, t4, dyn
	-[0x80001a80]:csrrs a2, fcsr, zero
	-[0x80001a84]:sw t6, 392(fp)
	-[0x80001a88]:sw a2, 396(fp)
Current Store : [0x80001a88] : sw a2, 396(fp) -- Store: [0x80003f68]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x292241 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a9c]:fdiv.s t6, t5, t4, dyn
	-[0x80001aa0]:csrrs a2, fcsr, zero
	-[0x80001aa4]:sw t6, 400(fp)
	-[0x80001aa8]:sw a2, 404(fp)
Current Store : [0x80001aa8] : sw a2, 404(fp) -- Store: [0x80003f70]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x292241 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001abc]:fdiv.s t6, t5, t4, dyn
	-[0x80001ac0]:csrrs a2, fcsr, zero
	-[0x80001ac4]:sw t6, 408(fp)
	-[0x80001ac8]:sw a2, 412(fp)
Current Store : [0x80001ac8] : sw a2, 412(fp) -- Store: [0x80003f78]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4214d7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001adc]:fdiv.s t6, t5, t4, dyn
	-[0x80001ae0]:csrrs a2, fcsr, zero
	-[0x80001ae4]:sw t6, 416(fp)
	-[0x80001ae8]:sw a2, 420(fp)
Current Store : [0x80001ae8] : sw a2, 420(fp) -- Store: [0x80003f80]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4214d7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001afc]:fdiv.s t6, t5, t4, dyn
	-[0x80001b00]:csrrs a2, fcsr, zero
	-[0x80001b04]:sw t6, 424(fp)
	-[0x80001b08]:sw a2, 428(fp)
Current Store : [0x80001b08] : sw a2, 428(fp) -- Store: [0x80003f88]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4214d7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fdiv.s t6, t5, t4, dyn
	-[0x80001b20]:csrrs a2, fcsr, zero
	-[0x80001b24]:sw t6, 432(fp)
	-[0x80001b28]:sw a2, 436(fp)
Current Store : [0x80001b28] : sw a2, 436(fp) -- Store: [0x80003f90]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4214d7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b3c]:fdiv.s t6, t5, t4, dyn
	-[0x80001b40]:csrrs a2, fcsr, zero
	-[0x80001b44]:sw t6, 440(fp)
	-[0x80001b48]:sw a2, 444(fp)
Current Store : [0x80001b48] : sw a2, 444(fp) -- Store: [0x80003f98]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4214d7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b5c]:fdiv.s t6, t5, t4, dyn
	-[0x80001b60]:csrrs a2, fcsr, zero
	-[0x80001b64]:sw t6, 448(fp)
	-[0x80001b68]:sw a2, 452(fp)
Current Store : [0x80001b68] : sw a2, 452(fp) -- Store: [0x80003fa0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dd39b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b7c]:fdiv.s t6, t5, t4, dyn
	-[0x80001b80]:csrrs a2, fcsr, zero
	-[0x80001b84]:sw t6, 456(fp)
	-[0x80001b88]:sw a2, 460(fp)
Current Store : [0x80001b88] : sw a2, 460(fp) -- Store: [0x80003fa8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dd39b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b9c]:fdiv.s t6, t5, t4, dyn
	-[0x80001ba0]:csrrs a2, fcsr, zero
	-[0x80001ba4]:sw t6, 464(fp)
	-[0x80001ba8]:sw a2, 468(fp)
Current Store : [0x80001ba8] : sw a2, 468(fp) -- Store: [0x80003fb0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dd39b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bbc]:fdiv.s t6, t5, t4, dyn
	-[0x80001bc0]:csrrs a2, fcsr, zero
	-[0x80001bc4]:sw t6, 472(fp)
	-[0x80001bc8]:sw a2, 476(fp)
Current Store : [0x80001bc8] : sw a2, 476(fp) -- Store: [0x80003fb8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dd39b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bdc]:fdiv.s t6, t5, t4, dyn
	-[0x80001be0]:csrrs a2, fcsr, zero
	-[0x80001be4]:sw t6, 480(fp)
	-[0x80001be8]:sw a2, 484(fp)
Current Store : [0x80001be8] : sw a2, 484(fp) -- Store: [0x80003fc0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6dd39b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bfc]:fdiv.s t6, t5, t4, dyn
	-[0x80001c00]:csrrs a2, fcsr, zero
	-[0x80001c04]:sw t6, 488(fp)
	-[0x80001c08]:sw a2, 492(fp)
Current Store : [0x80001c08] : sw a2, 492(fp) -- Store: [0x80003fc8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x664f14 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fdiv.s t6, t5, t4, dyn
	-[0x80001c20]:csrrs a2, fcsr, zero
	-[0x80001c24]:sw t6, 496(fp)
	-[0x80001c28]:sw a2, 500(fp)
Current Store : [0x80001c28] : sw a2, 500(fp) -- Store: [0x80003fd0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x664f14 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c3c]:fdiv.s t6, t5, t4, dyn
	-[0x80001c40]:csrrs a2, fcsr, zero
	-[0x80001c44]:sw t6, 504(fp)
	-[0x80001c48]:sw a2, 508(fp)
Current Store : [0x80001c48] : sw a2, 508(fp) -- Store: [0x80003fd8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x664f14 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c5c]:fdiv.s t6, t5, t4, dyn
	-[0x80001c60]:csrrs a2, fcsr, zero
	-[0x80001c64]:sw t6, 512(fp)
	-[0x80001c68]:sw a2, 516(fp)
Current Store : [0x80001c68] : sw a2, 516(fp) -- Store: [0x80003fe0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x664f14 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c7c]:fdiv.s t6, t5, t4, dyn
	-[0x80001c80]:csrrs a2, fcsr, zero
	-[0x80001c84]:sw t6, 520(fp)
	-[0x80001c88]:sw a2, 524(fp)
Current Store : [0x80001c88] : sw a2, 524(fp) -- Store: [0x80003fe8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x664f14 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fdiv.s t6, t5, t4, dyn
	-[0x80001ca0]:csrrs a2, fcsr, zero
	-[0x80001ca4]:sw t6, 528(fp)
	-[0x80001ca8]:sw a2, 532(fp)
Current Store : [0x80001ca8] : sw a2, 532(fp) -- Store: [0x80003ff0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cbc]:fdiv.s t6, t5, t4, dyn
	-[0x80001cc0]:csrrs a2, fcsr, zero
	-[0x80001cc4]:sw t6, 536(fp)
	-[0x80001cc8]:sw a2, 540(fp)
Current Store : [0x80001cc8] : sw a2, 540(fp) -- Store: [0x80003ff8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fdiv.s t6, t5, t4, dyn
	-[0x80001ce0]:csrrs a2, fcsr, zero
	-[0x80001ce4]:sw t6, 544(fp)
	-[0x80001ce8]:sw a2, 548(fp)
Current Store : [0x80001ce8] : sw a2, 548(fp) -- Store: [0x80004000]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cfc]:fdiv.s t6, t5, t4, dyn
	-[0x80001d00]:csrrs a2, fcsr, zero
	-[0x80001d04]:sw t6, 552(fp)
	-[0x80001d08]:sw a2, 556(fp)
Current Store : [0x80001d08] : sw a2, 556(fp) -- Store: [0x80004008]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d1c]:fdiv.s t6, t5, t4, dyn
	-[0x80001d20]:csrrs a2, fcsr, zero
	-[0x80001d24]:sw t6, 560(fp)
	-[0x80001d28]:sw a2, 564(fp)
Current Store : [0x80001d28] : sw a2, 564(fp) -- Store: [0x80004010]:0x00000023




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d3c]:fdiv.s t6, t5, t4, dyn
	-[0x80001d40]:csrrs a2, fcsr, zero
	-[0x80001d44]:sw t6, 568(fp)
	-[0x80001d48]:sw a2, 572(fp)
Current Store : [0x80001d48] : sw a2, 572(fp) -- Store: [0x80004018]:0x00000063





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
