
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800015f0')]      |
| SIG_REGION                | [('0x80003810', '0x80003cc0', '300 words')]      |
| COV_LABELS                | fnmadd_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV32F-fnmaddall-nov14/fnmadd_b6-01.S/ref.S    |
| Total Number of coverpoints| 281     |
| Total Coverpoints Hit     | 281      |
| Total Signature Updates   | 296      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 148     |
| STAT4                     | 148     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000128]:fnmadd.s ft11, ft10, ft9, ft10, dyn
[0x8000012c]:csrrs tp, fcsr, zero
[0x80000130]:fsw ft11, 0(ra)
[0x80000134]:sw tp, 4(ra)
[0x80000138]:flw ft11, 12(gp)
[0x8000013c]:flw ft8, 16(gp)
[0x80000140]:flw ft8, 20(gp)
[0x80000144]:addi sp, zero, 34
[0x80000148]:csrrw zero, fcsr, sp
[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn

[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn
[0x80000150]:csrrs tp, fcsr, zero
[0x80000154]:fsw ft8, 8(ra)
[0x80000158]:sw tp, 12(ra)
[0x8000015c]:flw fs11, 24(gp)
[0x80000160]:flw fs11, 28(gp)
[0x80000164]:flw ft11, 32(gp)
[0x80000168]:addi sp, zero, 66
[0x8000016c]:csrrw zero, fcsr, sp
[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn

[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn
[0x80000174]:csrrs tp, fcsr, zero
[0x80000178]:fsw fs11, 16(ra)
[0x8000017c]:sw tp, 20(ra)
[0x80000180]:flw ft9, 36(gp)
[0x80000184]:flw fs10, 40(gp)
[0x80000188]:flw fs10, 44(gp)
[0x8000018c]:addi sp, zero, 98
[0x80000190]:csrrw zero, fcsr, sp
[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn

[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn
[0x80000198]:csrrs tp, fcsr, zero
[0x8000019c]:fsw ft10, 24(ra)
[0x800001a0]:sw tp, 28(ra)
[0x800001a4]:flw fs10, 48(gp)
[0x800001a8]:flw ft11, 52(gp)
[0x800001ac]:flw ft9, 56(gp)
[0x800001b0]:addi sp, zero, 130
[0x800001b4]:csrrw zero, fcsr, sp
[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn

[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn
[0x800001bc]:csrrs tp, fcsr, zero
[0x800001c0]:fsw fs10, 32(ra)
[0x800001c4]:sw tp, 36(ra)
[0x800001c8]:flw ft8, 60(gp)
[0x800001cc]:flw ft10, 64(gp)
[0x800001d0]:flw fs9, 68(gp)
[0x800001d4]:addi sp, zero, 2
[0x800001d8]:csrrw zero, fcsr, sp
[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn

[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn
[0x800001e0]:csrrs tp, fcsr, zero
[0x800001e4]:fsw fs9, 40(ra)
[0x800001e8]:sw tp, 44(ra)
[0x800001ec]:flw fs8, 72(gp)
[0x800001f0]:flw fs8, 76(gp)
[0x800001f4]:flw fs8, 80(gp)
[0x800001f8]:addi sp, zero, 34
[0x800001fc]:csrrw zero, fcsr, sp
[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn

[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn
[0x80000204]:csrrs tp, fcsr, zero
[0x80000208]:fsw fs8, 48(ra)
[0x8000020c]:sw tp, 52(ra)
[0x80000210]:flw fs7, 84(gp)
[0x80000214]:flw fs9, 88(gp)
[0x80000218]:flw fs7, 92(gp)
[0x8000021c]:addi sp, zero, 66
[0x80000220]:csrrw zero, fcsr, sp
[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn

[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn
[0x80000228]:csrrs tp, fcsr, zero
[0x8000022c]:fsw fs7, 56(ra)
[0x80000230]:sw tp, 60(ra)
[0x80000234]:flw fs6, 96(gp)
[0x80000238]:flw fs6, 100(gp)
[0x8000023c]:flw fs11, 104(gp)
[0x80000240]:addi sp, zero, 98
[0x80000244]:csrrw zero, fcsr, sp
[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn

[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn
[0x8000024c]:csrrs tp, fcsr, zero
[0x80000250]:fsw ft9, 64(ra)
[0x80000254]:sw tp, 68(ra)
[0x80000258]:flw fs5, 108(gp)
[0x8000025c]:flw fs5, 112(gp)
[0x80000260]:flw fs5, 116(gp)
[0x80000264]:addi sp, zero, 130
[0x80000268]:csrrw zero, fcsr, sp
[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn

[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn
[0x80000270]:csrrs tp, fcsr, zero
[0x80000274]:fsw fs6, 72(ra)
[0x80000278]:sw tp, 76(ra)
[0x8000027c]:flw fs9, 120(gp)
[0x80000280]:flw fs4, 124(gp)
[0x80000284]:flw fs6, 128(gp)
[0x80000288]:addi sp, zero, 2
[0x8000028c]:csrrw zero, fcsr, sp
[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn

[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn
[0x80000294]:csrrs tp, fcsr, zero
[0x80000298]:fsw fs4, 80(ra)
[0x8000029c]:sw tp, 84(ra)
[0x800002a0]:flw fs4, 132(gp)
[0x800002a4]:flw fs7, 136(gp)
[0x800002a8]:flw fs3, 140(gp)
[0x800002ac]:addi sp, zero, 34
[0x800002b0]:csrrw zero, fcsr, sp
[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn

[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn
[0x800002b8]:csrrs tp, fcsr, zero
[0x800002bc]:fsw fs5, 88(ra)
[0x800002c0]:sw tp, 92(ra)
[0x800002c4]:flw fs2, 144(gp)
[0x800002c8]:flw fa7, 148(gp)
[0x800002cc]:flw fs4, 152(gp)
[0x800002d0]:addi sp, zero, 66
[0x800002d4]:csrrw zero, fcsr, sp
[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn

[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn
[0x800002dc]:csrrs tp, fcsr, zero
[0x800002e0]:fsw fs3, 96(ra)
[0x800002e4]:sw tp, 100(ra)
[0x800002e8]:flw fa7, 156(gp)
[0x800002ec]:flw fs3, 160(gp)
[0x800002f0]:flw fa6, 164(gp)
[0x800002f4]:addi sp, zero, 98
[0x800002f8]:csrrw zero, fcsr, sp
[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn

[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn
[0x80000300]:csrrs tp, fcsr, zero
[0x80000304]:fsw fs2, 104(ra)
[0x80000308]:sw tp, 108(ra)
[0x8000030c]:flw fs3, 168(gp)
[0x80000310]:flw fa6, 172(gp)
[0x80000314]:flw fs2, 176(gp)
[0x80000318]:addi sp, zero, 130
[0x8000031c]:csrrw zero, fcsr, sp
[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn

[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn
[0x80000324]:csrrs tp, fcsr, zero
[0x80000328]:fsw fa7, 112(ra)
[0x8000032c]:sw tp, 116(ra)
[0x80000330]:flw fa5, 180(gp)
[0x80000334]:flw fs2, 184(gp)
[0x80000338]:flw fa7, 188(gp)
[0x8000033c]:addi sp, zero, 2
[0x80000340]:csrrw zero, fcsr, sp
[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn

[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn
[0x80000348]:csrrs tp, fcsr, zero
[0x8000034c]:fsw fa6, 120(ra)
[0x80000350]:sw tp, 124(ra)
[0x80000354]:flw fa6, 192(gp)
[0x80000358]:flw fa4, 196(gp)
[0x8000035c]:flw fa3, 200(gp)
[0x80000360]:addi sp, zero, 34
[0x80000364]:csrrw zero, fcsr, sp
[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn

[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn
[0x8000036c]:csrrs tp, fcsr, zero
[0x80000370]:fsw fa5, 128(ra)
[0x80000374]:sw tp, 132(ra)
[0x80000378]:flw fa3, 204(gp)
[0x8000037c]:flw fa5, 208(gp)
[0x80000380]:flw fa2, 212(gp)
[0x80000384]:addi sp, zero, 66
[0x80000388]:csrrw zero, fcsr, sp
[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn

[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn
[0x80000390]:csrrs tp, fcsr, zero
[0x80000394]:fsw fa4, 136(ra)
[0x80000398]:sw tp, 140(ra)
[0x8000039c]:flw fa4, 216(gp)
[0x800003a0]:flw fa2, 220(gp)
[0x800003a4]:flw fa5, 224(gp)
[0x800003a8]:addi sp, zero, 98
[0x800003ac]:csrrw zero, fcsr, sp
[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn

[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn
[0x800003b4]:csrrs tp, fcsr, zero
[0x800003b8]:fsw fa3, 144(ra)
[0x800003bc]:sw tp, 148(ra)
[0x800003c0]:flw fa1, 228(gp)
[0x800003c4]:flw fa3, 232(gp)
[0x800003c8]:flw fa4, 236(gp)
[0x800003cc]:addi sp, zero, 130
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn

[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:fsw fa2, 152(ra)
[0x800003e0]:sw tp, 156(ra)
[0x800003e4]:flw fa2, 240(gp)
[0x800003e8]:flw fa0, 244(gp)
[0x800003ec]:flw fs1, 248(gp)
[0x800003f0]:addi sp, zero, 2
[0x800003f4]:csrrw zero, fcsr, sp
[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn

[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn
[0x800003fc]:csrrs tp, fcsr, zero
[0x80000400]:fsw fa1, 160(ra)
[0x80000404]:sw tp, 164(ra)
[0x80000408]:flw fs1, 252(gp)
[0x8000040c]:flw fa1, 256(gp)
[0x80000410]:flw fs0, 260(gp)
[0x80000414]:addi sp, zero, 34
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn

[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:fsw fa0, 168(ra)
[0x80000428]:sw tp, 172(ra)
[0x8000042c]:flw fa0, 264(gp)
[0x80000430]:flw fs0, 268(gp)
[0x80000434]:flw fa1, 272(gp)
[0x80000438]:addi sp, zero, 66
[0x8000043c]:csrrw zero, fcsr, sp
[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn

[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn
[0x80000444]:csrrs tp, fcsr, zero
[0x80000448]:fsw fs1, 176(ra)
[0x8000044c]:sw tp, 180(ra)
[0x80000450]:flw ft7, 276(gp)
[0x80000454]:flw fs1, 280(gp)
[0x80000458]:flw fa0, 284(gp)
[0x8000045c]:addi sp, zero, 98
[0x80000460]:csrrw zero, fcsr, sp
[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn

[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn
[0x80000468]:csrrs tp, fcsr, zero
[0x8000046c]:fsw fs0, 184(ra)
[0x80000470]:sw tp, 188(ra)
[0x80000474]:flw fs0, 288(gp)
[0x80000478]:flw ft6, 292(gp)
[0x8000047c]:flw ft5, 296(gp)
[0x80000480]:addi sp, zero, 130
[0x80000484]:csrrw zero, fcsr, sp
[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn

[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn
[0x8000048c]:csrrs tp, fcsr, zero
[0x80000490]:fsw ft7, 192(ra)
[0x80000494]:sw tp, 196(ra)
[0x80000498]:flw ft5, 300(gp)
[0x8000049c]:flw ft7, 304(gp)
[0x800004a0]:flw ft4, 308(gp)
[0x800004a4]:addi sp, zero, 2
[0x800004a8]:csrrw zero, fcsr, sp
[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn

[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn
[0x800004b0]:csrrs tp, fcsr, zero
[0x800004b4]:fsw ft6, 200(ra)
[0x800004b8]:sw tp, 204(ra)
[0x800004bc]:flw ft6, 312(gp)
[0x800004c0]:flw ft4, 316(gp)
[0x800004c4]:flw ft7, 320(gp)
[0x800004c8]:addi sp, zero, 34
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn

[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:fsw ft5, 208(ra)
[0x800004dc]:sw tp, 212(ra)
[0x800004e0]:flw ft3, 324(gp)
[0x800004e4]:flw ft5, 328(gp)
[0x800004e8]:flw ft6, 332(gp)
[0x800004ec]:addi sp, zero, 66
[0x800004f0]:csrrw zero, fcsr, sp
[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn

[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn
[0x800004f8]:csrrs tp, fcsr, zero
[0x800004fc]:fsw ft4, 216(ra)
[0x80000500]:sw tp, 220(ra)
[0x80000504]:flw ft4, 336(gp)
[0x80000508]:flw ft2, 340(gp)
[0x8000050c]:flw ft1, 344(gp)
[0x80000510]:addi sp, zero, 98
[0x80000514]:csrrw zero, fcsr, sp
[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn

[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn
[0x8000051c]:csrrs tp, fcsr, zero
[0x80000520]:fsw ft3, 224(ra)
[0x80000524]:sw tp, 228(ra)
[0x80000528]:flw ft1, 348(gp)
[0x8000052c]:flw ft3, 352(gp)
[0x80000530]:flw ft0, 356(gp)
[0x80000534]:addi sp, zero, 130
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn

[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:fsw ft2, 232(ra)
[0x80000548]:sw tp, 236(ra)
[0x8000054c]:flw ft2, 360(gp)
[0x80000550]:flw ft0, 364(gp)
[0x80000554]:flw ft3, 368(gp)
[0x80000558]:addi sp, zero, 2
[0x8000055c]:csrrw zero, fcsr, sp
[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn

[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn
[0x80000564]:csrrs tp, fcsr, zero
[0x80000568]:fsw ft1, 240(ra)
[0x8000056c]:sw tp, 244(ra)
[0x80000570]:flw ft0, 372(gp)
[0x80000574]:flw ft10, 376(gp)
[0x80000578]:flw ft9, 380(gp)
[0x8000057c]:addi sp, zero, 34
[0x80000580]:csrrw zero, fcsr, sp
[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn

[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn
[0x80000588]:csrrs tp, fcsr, zero
[0x8000058c]:fsw ft11, 248(ra)
[0x80000590]:sw tp, 252(ra)
[0x80000594]:flw ft10, 384(gp)
[0x80000598]:flw ft1, 388(gp)
[0x8000059c]:flw ft9, 392(gp)
[0x800005a0]:addi sp, zero, 66
[0x800005a4]:csrrw zero, fcsr, sp
[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn

[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn
[0x800005ac]:csrrs tp, fcsr, zero
[0x800005b0]:fsw ft11, 256(ra)
[0x800005b4]:sw tp, 260(ra)
[0x800005b8]:flw ft10, 396(gp)
[0x800005bc]:flw ft9, 400(gp)
[0x800005c0]:flw ft2, 404(gp)
[0x800005c4]:addi sp, zero, 98
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn

[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:fsw ft11, 264(ra)
[0x800005d8]:sw tp, 268(ra)
[0x800005dc]:flw ft11, 408(gp)
[0x800005e0]:flw ft10, 412(gp)
[0x800005e4]:flw ft9, 416(gp)
[0x800005e8]:addi sp, zero, 130
[0x800005ec]:csrrw zero, fcsr, sp
[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn

[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn
[0x800005f4]:csrrs tp, fcsr, zero
[0x800005f8]:fsw ft0, 272(ra)
[0x800005fc]:sw tp, 276(ra)
[0x80000600]:flw ft10, 420(gp)
[0x80000604]:flw ft9, 424(gp)
[0x80000608]:flw ft8, 428(gp)
[0x8000060c]:addi sp, zero, 2
[0x80000610]:csrrw zero, fcsr, sp
[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000618]:csrrs tp, fcsr, zero
[0x8000061c]:fsw ft11, 280(ra)
[0x80000620]:sw tp, 284(ra)
[0x80000624]:flw ft10, 432(gp)
[0x80000628]:flw ft9, 436(gp)
[0x8000062c]:flw ft8, 440(gp)
[0x80000630]:addi sp, zero, 34
[0x80000634]:csrrw zero, fcsr, sp
[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000063c]:csrrs tp, fcsr, zero
[0x80000640]:fsw ft11, 288(ra)
[0x80000644]:sw tp, 292(ra)
[0x80000648]:flw ft10, 444(gp)
[0x8000064c]:flw ft9, 448(gp)
[0x80000650]:flw ft8, 452(gp)
[0x80000654]:addi sp, zero, 66
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:fsw ft11, 296(ra)
[0x80000668]:sw tp, 300(ra)
[0x8000066c]:flw ft10, 456(gp)
[0x80000670]:flw ft9, 460(gp)
[0x80000674]:flw ft8, 464(gp)
[0x80000678]:addi sp, zero, 98
[0x8000067c]:csrrw zero, fcsr, sp
[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000684]:csrrs tp, fcsr, zero
[0x80000688]:fsw ft11, 304(ra)
[0x8000068c]:sw tp, 308(ra)
[0x80000690]:flw ft10, 468(gp)
[0x80000694]:flw ft9, 472(gp)
[0x80000698]:flw ft8, 476(gp)
[0x8000069c]:addi sp, zero, 130
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006a8]:csrrs tp, fcsr, zero
[0x800006ac]:fsw ft11, 312(ra)
[0x800006b0]:sw tp, 316(ra)
[0x800006b4]:flw ft10, 480(gp)
[0x800006b8]:flw ft9, 484(gp)
[0x800006bc]:flw ft8, 488(gp)
[0x800006c0]:addi sp, zero, 2
[0x800006c4]:csrrw zero, fcsr, sp
[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006cc]:csrrs tp, fcsr, zero
[0x800006d0]:fsw ft11, 320(ra)
[0x800006d4]:sw tp, 324(ra)
[0x800006d8]:flw ft10, 492(gp)
[0x800006dc]:flw ft9, 496(gp)
[0x800006e0]:flw ft8, 500(gp)
[0x800006e4]:addi sp, zero, 34
[0x800006e8]:csrrw zero, fcsr, sp
[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006f0]:csrrs tp, fcsr, zero
[0x800006f4]:fsw ft11, 328(ra)
[0x800006f8]:sw tp, 332(ra)
[0x800006fc]:flw ft10, 504(gp)
[0x80000700]:flw ft9, 508(gp)
[0x80000704]:flw ft8, 512(gp)
[0x80000708]:addi sp, zero, 66
[0x8000070c]:csrrw zero, fcsr, sp
[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000714]:csrrs tp, fcsr, zero
[0x80000718]:fsw ft11, 336(ra)
[0x8000071c]:sw tp, 340(ra)
[0x80000720]:flw ft10, 516(gp)
[0x80000724]:flw ft9, 520(gp)
[0x80000728]:flw ft8, 524(gp)
[0x8000072c]:addi sp, zero, 98
[0x80000730]:csrrw zero, fcsr, sp
[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000738]:csrrs tp, fcsr, zero
[0x8000073c]:fsw ft11, 344(ra)
[0x80000740]:sw tp, 348(ra)
[0x80000744]:flw ft10, 528(gp)
[0x80000748]:flw ft9, 532(gp)
[0x8000074c]:flw ft8, 536(gp)
[0x80000750]:addi sp, zero, 130
[0x80000754]:csrrw zero, fcsr, sp
[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000075c]:csrrs tp, fcsr, zero
[0x80000760]:fsw ft11, 352(ra)
[0x80000764]:sw tp, 356(ra)
[0x80000768]:flw ft10, 540(gp)
[0x8000076c]:flw ft9, 544(gp)
[0x80000770]:flw ft8, 548(gp)
[0x80000774]:addi sp, zero, 2
[0x80000778]:csrrw zero, fcsr, sp
[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000780]:csrrs tp, fcsr, zero
[0x80000784]:fsw ft11, 360(ra)
[0x80000788]:sw tp, 364(ra)
[0x8000078c]:flw ft10, 552(gp)
[0x80000790]:flw ft9, 556(gp)
[0x80000794]:flw ft8, 560(gp)
[0x80000798]:addi sp, zero, 34
[0x8000079c]:csrrw zero, fcsr, sp
[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007a4]:csrrs tp, fcsr, zero
[0x800007a8]:fsw ft11, 368(ra)
[0x800007ac]:sw tp, 372(ra)
[0x800007b0]:flw ft10, 564(gp)
[0x800007b4]:flw ft9, 568(gp)
[0x800007b8]:flw ft8, 572(gp)
[0x800007bc]:addi sp, zero, 66
[0x800007c0]:csrrw zero, fcsr, sp
[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007c8]:csrrs tp, fcsr, zero
[0x800007cc]:fsw ft11, 376(ra)
[0x800007d0]:sw tp, 380(ra)
[0x800007d4]:flw ft10, 576(gp)
[0x800007d8]:flw ft9, 580(gp)
[0x800007dc]:flw ft8, 584(gp)
[0x800007e0]:addi sp, zero, 98
[0x800007e4]:csrrw zero, fcsr, sp
[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007ec]:csrrs tp, fcsr, zero
[0x800007f0]:fsw ft11, 384(ra)
[0x800007f4]:sw tp, 388(ra)
[0x800007f8]:flw ft10, 588(gp)
[0x800007fc]:flw ft9, 592(gp)
[0x80000800]:flw ft8, 596(gp)
[0x80000804]:addi sp, zero, 130
[0x80000808]:csrrw zero, fcsr, sp
[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000810]:csrrs tp, fcsr, zero
[0x80000814]:fsw ft11, 392(ra)
[0x80000818]:sw tp, 396(ra)
[0x8000081c]:flw ft10, 600(gp)
[0x80000820]:flw ft9, 604(gp)
[0x80000824]:flw ft8, 608(gp)
[0x80000828]:addi sp, zero, 2
[0x8000082c]:csrrw zero, fcsr, sp
[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000834]:csrrs tp, fcsr, zero
[0x80000838]:fsw ft11, 400(ra)
[0x8000083c]:sw tp, 404(ra)
[0x80000840]:flw ft10, 612(gp)
[0x80000844]:flw ft9, 616(gp)
[0x80000848]:flw ft8, 620(gp)
[0x8000084c]:addi sp, zero, 34
[0x80000850]:csrrw zero, fcsr, sp
[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000858]:csrrs tp, fcsr, zero
[0x8000085c]:fsw ft11, 408(ra)
[0x80000860]:sw tp, 412(ra)
[0x80000864]:flw ft10, 624(gp)
[0x80000868]:flw ft9, 628(gp)
[0x8000086c]:flw ft8, 632(gp)
[0x80000870]:addi sp, zero, 66
[0x80000874]:csrrw zero, fcsr, sp
[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000087c]:csrrs tp, fcsr, zero
[0x80000880]:fsw ft11, 416(ra)
[0x80000884]:sw tp, 420(ra)
[0x80000888]:flw ft10, 636(gp)
[0x8000088c]:flw ft9, 640(gp)
[0x80000890]:flw ft8, 644(gp)
[0x80000894]:addi sp, zero, 98
[0x80000898]:csrrw zero, fcsr, sp
[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008a0]:csrrs tp, fcsr, zero
[0x800008a4]:fsw ft11, 424(ra)
[0x800008a8]:sw tp, 428(ra)
[0x800008ac]:flw ft10, 648(gp)
[0x800008b0]:flw ft9, 652(gp)
[0x800008b4]:flw ft8, 656(gp)
[0x800008b8]:addi sp, zero, 130
[0x800008bc]:csrrw zero, fcsr, sp
[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008c4]:csrrs tp, fcsr, zero
[0x800008c8]:fsw ft11, 432(ra)
[0x800008cc]:sw tp, 436(ra)
[0x800008d0]:flw ft10, 660(gp)
[0x800008d4]:flw ft9, 664(gp)
[0x800008d8]:flw ft8, 668(gp)
[0x800008dc]:addi sp, zero, 2
[0x800008e0]:csrrw zero, fcsr, sp
[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008e8]:csrrs tp, fcsr, zero
[0x800008ec]:fsw ft11, 440(ra)
[0x800008f0]:sw tp, 444(ra)
[0x800008f4]:flw ft10, 672(gp)
[0x800008f8]:flw ft9, 676(gp)
[0x800008fc]:flw ft8, 680(gp)
[0x80000900]:addi sp, zero, 34
[0x80000904]:csrrw zero, fcsr, sp
[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000090c]:csrrs tp, fcsr, zero
[0x80000910]:fsw ft11, 448(ra)
[0x80000914]:sw tp, 452(ra)
[0x80000918]:flw ft10, 684(gp)
[0x8000091c]:flw ft9, 688(gp)
[0x80000920]:flw ft8, 692(gp)
[0x80000924]:addi sp, zero, 66
[0x80000928]:csrrw zero, fcsr, sp
[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000930]:csrrs tp, fcsr, zero
[0x80000934]:fsw ft11, 456(ra)
[0x80000938]:sw tp, 460(ra)
[0x8000093c]:flw ft10, 696(gp)
[0x80000940]:flw ft9, 700(gp)
[0x80000944]:flw ft8, 704(gp)
[0x80000948]:addi sp, zero, 98
[0x8000094c]:csrrw zero, fcsr, sp
[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000954]:csrrs tp, fcsr, zero
[0x80000958]:fsw ft11, 464(ra)
[0x8000095c]:sw tp, 468(ra)
[0x80000960]:flw ft10, 708(gp)
[0x80000964]:flw ft9, 712(gp)
[0x80000968]:flw ft8, 716(gp)
[0x8000096c]:addi sp, zero, 130
[0x80000970]:csrrw zero, fcsr, sp
[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000978]:csrrs tp, fcsr, zero
[0x8000097c]:fsw ft11, 472(ra)
[0x80000980]:sw tp, 476(ra)
[0x80000984]:flw ft10, 720(gp)
[0x80000988]:flw ft9, 724(gp)
[0x8000098c]:flw ft8, 728(gp)
[0x80000990]:addi sp, zero, 2
[0x80000994]:csrrw zero, fcsr, sp
[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000099c]:csrrs tp, fcsr, zero
[0x800009a0]:fsw ft11, 480(ra)
[0x800009a4]:sw tp, 484(ra)
[0x800009a8]:flw ft10, 732(gp)
[0x800009ac]:flw ft9, 736(gp)
[0x800009b0]:flw ft8, 740(gp)
[0x800009b4]:addi sp, zero, 34
[0x800009b8]:csrrw zero, fcsr, sp
[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800009c0]:csrrs tp, fcsr, zero
[0x800009c4]:fsw ft11, 488(ra)
[0x800009c8]:sw tp, 492(ra)
[0x800009cc]:flw ft10, 744(gp)
[0x800009d0]:flw ft9, 748(gp)
[0x800009d4]:flw ft8, 752(gp)
[0x800009d8]:addi sp, zero, 66
[0x800009dc]:csrrw zero, fcsr, sp
[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800009e4]:csrrs tp, fcsr, zero
[0x800009e8]:fsw ft11, 496(ra)
[0x800009ec]:sw tp, 500(ra)
[0x800009f0]:flw ft10, 756(gp)
[0x800009f4]:flw ft9, 760(gp)
[0x800009f8]:flw ft8, 764(gp)
[0x800009fc]:addi sp, zero, 98
[0x80000a00]:csrrw zero, fcsr, sp
[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a08]:csrrs tp, fcsr, zero
[0x80000a0c]:fsw ft11, 504(ra)
[0x80000a10]:sw tp, 508(ra)
[0x80000a14]:flw ft10, 768(gp)
[0x80000a18]:flw ft9, 772(gp)
[0x80000a1c]:flw ft8, 776(gp)
[0x80000a20]:addi sp, zero, 130
[0x80000a24]:csrrw zero, fcsr, sp
[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a2c]:csrrs tp, fcsr, zero
[0x80000a30]:fsw ft11, 512(ra)
[0x80000a34]:sw tp, 516(ra)
[0x80000a38]:flw ft10, 780(gp)
[0x80000a3c]:flw ft9, 784(gp)
[0x80000a40]:flw ft8, 788(gp)
[0x80000a44]:addi sp, zero, 2
[0x80000a48]:csrrw zero, fcsr, sp
[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a50]:csrrs tp, fcsr, zero
[0x80000a54]:fsw ft11, 520(ra)
[0x80000a58]:sw tp, 524(ra)
[0x80000a5c]:flw ft10, 792(gp)
[0x80000a60]:flw ft9, 796(gp)
[0x80000a64]:flw ft8, 800(gp)
[0x80000a68]:addi sp, zero, 34
[0x80000a6c]:csrrw zero, fcsr, sp
[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a74]:csrrs tp, fcsr, zero
[0x80000a78]:fsw ft11, 528(ra)
[0x80000a7c]:sw tp, 532(ra)
[0x80000a80]:flw ft10, 804(gp)
[0x80000a84]:flw ft9, 808(gp)
[0x80000a88]:flw ft8, 812(gp)
[0x80000a8c]:addi sp, zero, 66
[0x80000a90]:csrrw zero, fcsr, sp
[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a98]:csrrs tp, fcsr, zero
[0x80000a9c]:fsw ft11, 536(ra)
[0x80000aa0]:sw tp, 540(ra)
[0x80000aa4]:flw ft10, 816(gp)
[0x80000aa8]:flw ft9, 820(gp)
[0x80000aac]:flw ft8, 824(gp)
[0x80000ab0]:addi sp, zero, 98
[0x80000ab4]:csrrw zero, fcsr, sp
[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000abc]:csrrs tp, fcsr, zero
[0x80000ac0]:fsw ft11, 544(ra)
[0x80000ac4]:sw tp, 548(ra)
[0x80000ac8]:flw ft10, 828(gp)
[0x80000acc]:flw ft9, 832(gp)
[0x80000ad0]:flw ft8, 836(gp)
[0x80000ad4]:addi sp, zero, 130
[0x80000ad8]:csrrw zero, fcsr, sp
[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ae0]:csrrs tp, fcsr, zero
[0x80000ae4]:fsw ft11, 552(ra)
[0x80000ae8]:sw tp, 556(ra)
[0x80000aec]:flw ft10, 840(gp)
[0x80000af0]:flw ft9, 844(gp)
[0x80000af4]:flw ft8, 848(gp)
[0x80000af8]:addi sp, zero, 2
[0x80000afc]:csrrw zero, fcsr, sp
[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b04]:csrrs tp, fcsr, zero
[0x80000b08]:fsw ft11, 560(ra)
[0x80000b0c]:sw tp, 564(ra)
[0x80000b10]:flw ft10, 852(gp)
[0x80000b14]:flw ft9, 856(gp)
[0x80000b18]:flw ft8, 860(gp)
[0x80000b1c]:addi sp, zero, 34
[0x80000b20]:csrrw zero, fcsr, sp
[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b28]:csrrs tp, fcsr, zero
[0x80000b2c]:fsw ft11, 568(ra)
[0x80000b30]:sw tp, 572(ra)
[0x80000b34]:flw ft10, 864(gp)
[0x80000b38]:flw ft9, 868(gp)
[0x80000b3c]:flw ft8, 872(gp)
[0x80000b40]:addi sp, zero, 66
[0x80000b44]:csrrw zero, fcsr, sp
[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b4c]:csrrs tp, fcsr, zero
[0x80000b50]:fsw ft11, 576(ra)
[0x80000b54]:sw tp, 580(ra)
[0x80000b58]:flw ft10, 876(gp)
[0x80000b5c]:flw ft9, 880(gp)
[0x80000b60]:flw ft8, 884(gp)
[0x80000b64]:addi sp, zero, 98
[0x80000b68]:csrrw zero, fcsr, sp
[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b70]:csrrs tp, fcsr, zero
[0x80000b74]:fsw ft11, 584(ra)
[0x80000b78]:sw tp, 588(ra)
[0x80000b7c]:flw ft10, 888(gp)
[0x80000b80]:flw ft9, 892(gp)
[0x80000b84]:flw ft8, 896(gp)
[0x80000b88]:addi sp, zero, 130
[0x80000b8c]:csrrw zero, fcsr, sp
[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b94]:csrrs tp, fcsr, zero
[0x80000b98]:fsw ft11, 592(ra)
[0x80000b9c]:sw tp, 596(ra)
[0x80000ba0]:flw ft10, 900(gp)
[0x80000ba4]:flw ft9, 904(gp)
[0x80000ba8]:flw ft8, 908(gp)
[0x80000bac]:addi sp, zero, 2
[0x80000bb0]:csrrw zero, fcsr, sp
[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000bb8]:csrrs tp, fcsr, zero
[0x80000bbc]:fsw ft11, 600(ra)
[0x80000bc0]:sw tp, 604(ra)
[0x80000bc4]:flw ft10, 912(gp)
[0x80000bc8]:flw ft9, 916(gp)
[0x80000bcc]:flw ft8, 920(gp)
[0x80000bd0]:addi sp, zero, 34
[0x80000bd4]:csrrw zero, fcsr, sp
[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000bdc]:csrrs tp, fcsr, zero
[0x80000be0]:fsw ft11, 608(ra)
[0x80000be4]:sw tp, 612(ra)
[0x80000be8]:flw ft10, 924(gp)
[0x80000bec]:flw ft9, 928(gp)
[0x80000bf0]:flw ft8, 932(gp)
[0x80000bf4]:addi sp, zero, 66
[0x80000bf8]:csrrw zero, fcsr, sp
[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c00]:csrrs tp, fcsr, zero
[0x80000c04]:fsw ft11, 616(ra)
[0x80000c08]:sw tp, 620(ra)
[0x80000c0c]:flw ft10, 936(gp)
[0x80000c10]:flw ft9, 940(gp)
[0x80000c14]:flw ft8, 944(gp)
[0x80000c18]:addi sp, zero, 98
[0x80000c1c]:csrrw zero, fcsr, sp
[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c24]:csrrs tp, fcsr, zero
[0x80000c28]:fsw ft11, 624(ra)
[0x80000c2c]:sw tp, 628(ra)
[0x80000c30]:flw ft10, 948(gp)
[0x80000c34]:flw ft9, 952(gp)
[0x80000c38]:flw ft8, 956(gp)
[0x80000c3c]:addi sp, zero, 130
[0x80000c40]:csrrw zero, fcsr, sp
[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c48]:csrrs tp, fcsr, zero
[0x80000c4c]:fsw ft11, 632(ra)
[0x80000c50]:sw tp, 636(ra)
[0x80000c54]:flw ft10, 960(gp)
[0x80000c58]:flw ft9, 964(gp)
[0x80000c5c]:flw ft8, 968(gp)
[0x80000c60]:addi sp, zero, 2
[0x80000c64]:csrrw zero, fcsr, sp
[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c6c]:csrrs tp, fcsr, zero
[0x80000c70]:fsw ft11, 640(ra)
[0x80000c74]:sw tp, 644(ra)
[0x80000c78]:flw ft10, 972(gp)
[0x80000c7c]:flw ft9, 976(gp)
[0x80000c80]:flw ft8, 980(gp)
[0x80000c84]:addi sp, zero, 34
[0x80000c88]:csrrw zero, fcsr, sp
[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c90]:csrrs tp, fcsr, zero
[0x80000c94]:fsw ft11, 648(ra)
[0x80000c98]:sw tp, 652(ra)
[0x80000c9c]:flw ft10, 984(gp)
[0x80000ca0]:flw ft9, 988(gp)
[0x80000ca4]:flw ft8, 992(gp)
[0x80000ca8]:addi sp, zero, 66
[0x80000cac]:csrrw zero, fcsr, sp
[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cb4]:csrrs tp, fcsr, zero
[0x80000cb8]:fsw ft11, 656(ra)
[0x80000cbc]:sw tp, 660(ra)
[0x80000cc0]:flw ft10, 996(gp)
[0x80000cc4]:flw ft9, 1000(gp)
[0x80000cc8]:flw ft8, 1004(gp)
[0x80000ccc]:addi sp, zero, 98
[0x80000cd0]:csrrw zero, fcsr, sp
[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cd8]:csrrs tp, fcsr, zero
[0x80000cdc]:fsw ft11, 664(ra)
[0x80000ce0]:sw tp, 668(ra)
[0x80000ce4]:flw ft10, 1008(gp)
[0x80000ce8]:flw ft9, 1012(gp)
[0x80000cec]:flw ft8, 1016(gp)
[0x80000cf0]:addi sp, zero, 130
[0x80000cf4]:csrrw zero, fcsr, sp
[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cfc]:csrrs tp, fcsr, zero
[0x80000d00]:fsw ft11, 672(ra)
[0x80000d04]:sw tp, 676(ra)
[0x80000d08]:flw ft10, 1020(gp)
[0x80000d0c]:flw ft9, 1024(gp)
[0x80000d10]:flw ft8, 1028(gp)
[0x80000d14]:addi sp, zero, 2
[0x80000d18]:csrrw zero, fcsr, sp
[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d20]:csrrs tp, fcsr, zero
[0x80000d24]:fsw ft11, 680(ra)
[0x80000d28]:sw tp, 684(ra)
[0x80000d2c]:flw ft10, 1032(gp)
[0x80000d30]:flw ft9, 1036(gp)
[0x80000d34]:flw ft8, 1040(gp)
[0x80000d38]:addi sp, zero, 34
[0x80000d3c]:csrrw zero, fcsr, sp
[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d44]:csrrs tp, fcsr, zero
[0x80000d48]:fsw ft11, 688(ra)
[0x80000d4c]:sw tp, 692(ra)
[0x80000d50]:flw ft10, 1044(gp)
[0x80000d54]:flw ft9, 1048(gp)
[0x80000d58]:flw ft8, 1052(gp)
[0x80000d5c]:addi sp, zero, 66
[0x80000d60]:csrrw zero, fcsr, sp
[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d68]:csrrs tp, fcsr, zero
[0x80000d6c]:fsw ft11, 696(ra)
[0x80000d70]:sw tp, 700(ra)
[0x80000d74]:flw ft10, 1056(gp)
[0x80000d78]:flw ft9, 1060(gp)
[0x80000d7c]:flw ft8, 1064(gp)
[0x80000d80]:addi sp, zero, 98
[0x80000d84]:csrrw zero, fcsr, sp
[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d8c]:csrrs tp, fcsr, zero
[0x80000d90]:fsw ft11, 704(ra)
[0x80000d94]:sw tp, 708(ra)
[0x80000d98]:flw ft10, 1068(gp)
[0x80000d9c]:flw ft9, 1072(gp)
[0x80000da0]:flw ft8, 1076(gp)
[0x80000da4]:addi sp, zero, 130
[0x80000da8]:csrrw zero, fcsr, sp
[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000db0]:csrrs tp, fcsr, zero
[0x80000db4]:fsw ft11, 712(ra)
[0x80000db8]:sw tp, 716(ra)
[0x80000dbc]:flw ft10, 1080(gp)
[0x80000dc0]:flw ft9, 1084(gp)
[0x80000dc4]:flw ft8, 1088(gp)
[0x80000dc8]:addi sp, zero, 2
[0x80000dcc]:csrrw zero, fcsr, sp
[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000dd4]:csrrs tp, fcsr, zero
[0x80000dd8]:fsw ft11, 720(ra)
[0x80000ddc]:sw tp, 724(ra)
[0x80000de0]:flw ft10, 1092(gp)
[0x80000de4]:flw ft9, 1096(gp)
[0x80000de8]:flw ft8, 1100(gp)
[0x80000dec]:addi sp, zero, 34
[0x80000df0]:csrrw zero, fcsr, sp
[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000df8]:csrrs tp, fcsr, zero
[0x80000dfc]:fsw ft11, 728(ra)
[0x80000e00]:sw tp, 732(ra)
[0x80000e04]:flw ft10, 1104(gp)
[0x80000e08]:flw ft9, 1108(gp)
[0x80000e0c]:flw ft8, 1112(gp)
[0x80000e10]:addi sp, zero, 66
[0x80000e14]:csrrw zero, fcsr, sp
[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e1c]:csrrs tp, fcsr, zero
[0x80000e20]:fsw ft11, 736(ra)
[0x80000e24]:sw tp, 740(ra)
[0x80000e28]:flw ft10, 1116(gp)
[0x80000e2c]:flw ft9, 1120(gp)
[0x80000e30]:flw ft8, 1124(gp)
[0x80000e34]:addi sp, zero, 98
[0x80000e38]:csrrw zero, fcsr, sp
[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e40]:csrrs tp, fcsr, zero
[0x80000e44]:fsw ft11, 744(ra)
[0x80000e48]:sw tp, 748(ra)
[0x80000e4c]:flw ft10, 1128(gp)
[0x80000e50]:flw ft9, 1132(gp)
[0x80000e54]:flw ft8, 1136(gp)
[0x80000e58]:addi sp, zero, 130
[0x80000e5c]:csrrw zero, fcsr, sp
[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e64]:csrrs tp, fcsr, zero
[0x80000e68]:fsw ft11, 752(ra)
[0x80000e6c]:sw tp, 756(ra)
[0x80000e70]:flw ft10, 1140(gp)
[0x80000e74]:flw ft9, 1144(gp)
[0x80000e78]:flw ft8, 1148(gp)
[0x80000e7c]:addi sp, zero, 2
[0x80000e80]:csrrw zero, fcsr, sp
[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e88]:csrrs tp, fcsr, zero
[0x80000e8c]:fsw ft11, 760(ra)
[0x80000e90]:sw tp, 764(ra)
[0x80000e94]:flw ft10, 1152(gp)
[0x80000e98]:flw ft9, 1156(gp)
[0x80000e9c]:flw ft8, 1160(gp)
[0x80000ea0]:addi sp, zero, 34
[0x80000ea4]:csrrw zero, fcsr, sp
[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000eac]:csrrs tp, fcsr, zero
[0x80000eb0]:fsw ft11, 768(ra)
[0x80000eb4]:sw tp, 772(ra)
[0x80000eb8]:flw ft10, 1164(gp)
[0x80000ebc]:flw ft9, 1168(gp)
[0x80000ec0]:flw ft8, 1172(gp)
[0x80000ec4]:addi sp, zero, 66
[0x80000ec8]:csrrw zero, fcsr, sp
[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ed0]:csrrs tp, fcsr, zero
[0x80000ed4]:fsw ft11, 776(ra)
[0x80000ed8]:sw tp, 780(ra)
[0x80000edc]:flw ft10, 1176(gp)
[0x80000ee0]:flw ft9, 1180(gp)
[0x80000ee4]:flw ft8, 1184(gp)
[0x80000ee8]:addi sp, zero, 98
[0x80000eec]:csrrw zero, fcsr, sp
[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ef4]:csrrs tp, fcsr, zero
[0x80000ef8]:fsw ft11, 784(ra)
[0x80000efc]:sw tp, 788(ra)
[0x80000f00]:flw ft10, 1188(gp)
[0x80000f04]:flw ft9, 1192(gp)
[0x80000f08]:flw ft8, 1196(gp)
[0x80000f0c]:addi sp, zero, 130
[0x80000f10]:csrrw zero, fcsr, sp
[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f18]:csrrs tp, fcsr, zero
[0x80000f1c]:fsw ft11, 792(ra)
[0x80000f20]:sw tp, 796(ra)
[0x80000f24]:flw ft10, 1200(gp)
[0x80000f28]:flw ft9, 1204(gp)
[0x80000f2c]:flw ft8, 1208(gp)
[0x80000f30]:addi sp, zero, 2
[0x80000f34]:csrrw zero, fcsr, sp
[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f3c]:csrrs tp, fcsr, zero
[0x80000f40]:fsw ft11, 800(ra)
[0x80000f44]:sw tp, 804(ra)
[0x80000f48]:flw ft10, 1212(gp)
[0x80000f4c]:flw ft9, 1216(gp)
[0x80000f50]:flw ft8, 1220(gp)
[0x80000f54]:addi sp, zero, 34
[0x80000f58]:csrrw zero, fcsr, sp
[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f60]:csrrs tp, fcsr, zero
[0x80000f64]:fsw ft11, 808(ra)
[0x80000f68]:sw tp, 812(ra)
[0x80000f6c]:flw ft10, 1224(gp)
[0x80000f70]:flw ft9, 1228(gp)
[0x80000f74]:flw ft8, 1232(gp)
[0x80000f78]:addi sp, zero, 66
[0x80000f7c]:csrrw zero, fcsr, sp
[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f84]:csrrs tp, fcsr, zero
[0x80000f88]:fsw ft11, 816(ra)
[0x80000f8c]:sw tp, 820(ra)
[0x80000f90]:flw ft10, 1236(gp)
[0x80000f94]:flw ft9, 1240(gp)
[0x80000f98]:flw ft8, 1244(gp)
[0x80000f9c]:addi sp, zero, 98
[0x80000fa0]:csrrw zero, fcsr, sp
[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000fa8]:csrrs tp, fcsr, zero
[0x80000fac]:fsw ft11, 824(ra)
[0x80000fb0]:sw tp, 828(ra)
[0x80000fb4]:flw ft10, 1248(gp)
[0x80000fb8]:flw ft9, 1252(gp)
[0x80000fbc]:flw ft8, 1256(gp)
[0x80000fc0]:addi sp, zero, 130
[0x80000fc4]:csrrw zero, fcsr, sp
[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs tp, fcsr, zero
[0x80000fd0]:fsw ft11, 832(ra)
[0x80000fd4]:sw tp, 836(ra)
[0x80000fd8]:flw ft10, 1260(gp)
[0x80000fdc]:flw ft9, 1264(gp)
[0x80000fe0]:flw ft8, 1268(gp)
[0x80000fe4]:addi sp, zero, 2
[0x80000fe8]:csrrw zero, fcsr, sp
[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ff0]:csrrs tp, fcsr, zero
[0x80000ff4]:fsw ft11, 840(ra)
[0x80000ff8]:sw tp, 844(ra)
[0x80000ffc]:flw ft10, 1272(gp)
[0x80001000]:flw ft9, 1276(gp)
[0x80001004]:flw ft8, 1280(gp)
[0x80001008]:addi sp, zero, 34
[0x8000100c]:csrrw zero, fcsr, sp
[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001014]:csrrs tp, fcsr, zero
[0x80001018]:fsw ft11, 848(ra)
[0x8000101c]:sw tp, 852(ra)
[0x80001020]:flw ft10, 1284(gp)
[0x80001024]:flw ft9, 1288(gp)
[0x80001028]:flw ft8, 1292(gp)
[0x8000102c]:addi sp, zero, 66
[0x80001030]:csrrw zero, fcsr, sp
[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001038]:csrrs tp, fcsr, zero
[0x8000103c]:fsw ft11, 856(ra)
[0x80001040]:sw tp, 860(ra)
[0x80001044]:flw ft10, 1296(gp)
[0x80001048]:flw ft9, 1300(gp)
[0x8000104c]:flw ft8, 1304(gp)
[0x80001050]:addi sp, zero, 98
[0x80001054]:csrrw zero, fcsr, sp
[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000105c]:csrrs tp, fcsr, zero
[0x80001060]:fsw ft11, 864(ra)
[0x80001064]:sw tp, 868(ra)
[0x80001068]:flw ft10, 1308(gp)
[0x8000106c]:flw ft9, 1312(gp)
[0x80001070]:flw ft8, 1316(gp)
[0x80001074]:addi sp, zero, 130
[0x80001078]:csrrw zero, fcsr, sp
[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001080]:csrrs tp, fcsr, zero
[0x80001084]:fsw ft11, 872(ra)
[0x80001088]:sw tp, 876(ra)
[0x8000108c]:flw ft10, 1320(gp)
[0x80001090]:flw ft9, 1324(gp)
[0x80001094]:flw ft8, 1328(gp)
[0x80001098]:addi sp, zero, 2
[0x8000109c]:csrrw zero, fcsr, sp
[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010a4]:csrrs tp, fcsr, zero
[0x800010a8]:fsw ft11, 880(ra)
[0x800010ac]:sw tp, 884(ra)
[0x800010b0]:flw ft10, 1332(gp)
[0x800010b4]:flw ft9, 1336(gp)
[0x800010b8]:flw ft8, 1340(gp)
[0x800010bc]:addi sp, zero, 34
[0x800010c0]:csrrw zero, fcsr, sp
[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010c8]:csrrs tp, fcsr, zero
[0x800010cc]:fsw ft11, 888(ra)
[0x800010d0]:sw tp, 892(ra)
[0x800010d4]:flw ft10, 1344(gp)
[0x800010d8]:flw ft9, 1348(gp)
[0x800010dc]:flw ft8, 1352(gp)
[0x800010e0]:addi sp, zero, 66
[0x800010e4]:csrrw zero, fcsr, sp
[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs tp, fcsr, zero
[0x800010f0]:fsw ft11, 896(ra)
[0x800010f4]:sw tp, 900(ra)
[0x800010f8]:flw ft10, 1356(gp)
[0x800010fc]:flw ft9, 1360(gp)
[0x80001100]:flw ft8, 1364(gp)
[0x80001104]:addi sp, zero, 98
[0x80001108]:csrrw zero, fcsr, sp
[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001110]:csrrs tp, fcsr, zero
[0x80001114]:fsw ft11, 904(ra)
[0x80001118]:sw tp, 908(ra)
[0x8000111c]:flw ft10, 1368(gp)
[0x80001120]:flw ft9, 1372(gp)
[0x80001124]:flw ft8, 1376(gp)
[0x80001128]:addi sp, zero, 130
[0x8000112c]:csrrw zero, fcsr, sp
[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001134]:csrrs tp, fcsr, zero
[0x80001138]:fsw ft11, 912(ra)
[0x8000113c]:sw tp, 916(ra)
[0x80001140]:flw ft10, 1380(gp)
[0x80001144]:flw ft9, 1384(gp)
[0x80001148]:flw ft8, 1388(gp)
[0x8000114c]:addi sp, zero, 2
[0x80001150]:csrrw zero, fcsr, sp
[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001158]:csrrs tp, fcsr, zero
[0x8000115c]:fsw ft11, 920(ra)
[0x80001160]:sw tp, 924(ra)
[0x80001164]:flw ft10, 1392(gp)
[0x80001168]:flw ft9, 1396(gp)
[0x8000116c]:flw ft8, 1400(gp)
[0x80001170]:addi sp, zero, 34
[0x80001174]:csrrw zero, fcsr, sp
[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000117c]:csrrs tp, fcsr, zero
[0x80001180]:fsw ft11, 928(ra)
[0x80001184]:sw tp, 932(ra)
[0x80001188]:flw ft10, 1404(gp)
[0x8000118c]:flw ft9, 1408(gp)
[0x80001190]:flw ft8, 1412(gp)
[0x80001194]:addi sp, zero, 66
[0x80001198]:csrrw zero, fcsr, sp
[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011a0]:csrrs tp, fcsr, zero
[0x800011a4]:fsw ft11, 936(ra)
[0x800011a8]:sw tp, 940(ra)
[0x800011ac]:flw ft10, 1416(gp)
[0x800011b0]:flw ft9, 1420(gp)
[0x800011b4]:flw ft8, 1424(gp)
[0x800011b8]:addi sp, zero, 98
[0x800011bc]:csrrw zero, fcsr, sp
[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011c4]:csrrs tp, fcsr, zero
[0x800011c8]:fsw ft11, 944(ra)
[0x800011cc]:sw tp, 948(ra)
[0x800011d0]:flw ft10, 1428(gp)
[0x800011d4]:flw ft9, 1432(gp)
[0x800011d8]:flw ft8, 1436(gp)
[0x800011dc]:addi sp, zero, 130
[0x800011e0]:csrrw zero, fcsr, sp
[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011e8]:csrrs tp, fcsr, zero
[0x800011ec]:fsw ft11, 952(ra)
[0x800011f0]:sw tp, 956(ra)
[0x800011f4]:flw ft10, 1440(gp)
[0x800011f8]:flw ft9, 1444(gp)
[0x800011fc]:flw ft8, 1448(gp)
[0x80001200]:addi sp, zero, 2
[0x80001204]:csrrw zero, fcsr, sp
[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000120c]:csrrs tp, fcsr, zero
[0x80001210]:fsw ft11, 960(ra)
[0x80001214]:sw tp, 964(ra)
[0x80001218]:flw ft10, 1452(gp)
[0x8000121c]:flw ft9, 1456(gp)
[0x80001220]:flw ft8, 1460(gp)
[0x80001224]:addi sp, zero, 34
[0x80001228]:csrrw zero, fcsr, sp
[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001230]:csrrs tp, fcsr, zero
[0x80001234]:fsw ft11, 968(ra)
[0x80001238]:sw tp, 972(ra)
[0x8000123c]:flw ft10, 1464(gp)
[0x80001240]:flw ft9, 1468(gp)
[0x80001244]:flw ft8, 1472(gp)
[0x80001248]:addi sp, zero, 66
[0x8000124c]:csrrw zero, fcsr, sp
[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001254]:csrrs tp, fcsr, zero
[0x80001258]:fsw ft11, 976(ra)
[0x8000125c]:sw tp, 980(ra)
[0x80001260]:flw ft10, 1476(gp)
[0x80001264]:flw ft9, 1480(gp)
[0x80001268]:flw ft8, 1484(gp)
[0x8000126c]:addi sp, zero, 98
[0x80001270]:csrrw zero, fcsr, sp
[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001278]:csrrs tp, fcsr, zero
[0x8000127c]:fsw ft11, 984(ra)
[0x80001280]:sw tp, 988(ra)
[0x80001284]:flw ft10, 1488(gp)
[0x80001288]:flw ft9, 1492(gp)
[0x8000128c]:flw ft8, 1496(gp)
[0x80001290]:addi sp, zero, 130
[0x80001294]:csrrw zero, fcsr, sp
[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000129c]:csrrs tp, fcsr, zero
[0x800012a0]:fsw ft11, 992(ra)
[0x800012a4]:sw tp, 996(ra)
[0x800012a8]:flw ft10, 1500(gp)
[0x800012ac]:flw ft9, 1504(gp)
[0x800012b0]:flw ft8, 1508(gp)
[0x800012b4]:addi sp, zero, 2
[0x800012b8]:csrrw zero, fcsr, sp
[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800012c0]:csrrs tp, fcsr, zero
[0x800012c4]:fsw ft11, 1000(ra)
[0x800012c8]:sw tp, 1004(ra)
[0x800012cc]:flw ft10, 1512(gp)
[0x800012d0]:flw ft9, 1516(gp)
[0x800012d4]:flw ft8, 1520(gp)
[0x800012d8]:addi sp, zero, 34
[0x800012dc]:csrrw zero, fcsr, sp
[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800012e4]:csrrs tp, fcsr, zero
[0x800012e8]:fsw ft11, 1008(ra)
[0x800012ec]:sw tp, 1012(ra)
[0x800012f0]:flw ft10, 1524(gp)
[0x800012f4]:flw ft9, 1528(gp)
[0x800012f8]:flw ft8, 1532(gp)
[0x800012fc]:addi sp, zero, 66
[0x80001300]:csrrw zero, fcsr, sp
[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001308]:csrrs tp, fcsr, zero
[0x8000130c]:fsw ft11, 1016(ra)
[0x80001310]:sw tp, 1020(ra)
[0x80001314]:auipc ra, 3
[0x80001318]:addi ra, ra, 2304
[0x8000131c]:flw ft10, 1536(gp)
[0x80001320]:flw ft9, 1540(gp)
[0x80001324]:flw ft8, 1544(gp)
[0x80001328]:addi sp, zero, 98
[0x8000132c]:csrrw zero, fcsr, sp
[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001334]:csrrs tp, fcsr, zero
[0x80001338]:fsw ft11, 0(ra)
[0x8000133c]:sw tp, 4(ra)
[0x80001340]:flw ft10, 1548(gp)
[0x80001344]:flw ft9, 1552(gp)
[0x80001348]:flw ft8, 1556(gp)
[0x8000134c]:addi sp, zero, 130
[0x80001350]:csrrw zero, fcsr, sp
[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001358]:csrrs tp, fcsr, zero
[0x8000135c]:fsw ft11, 8(ra)
[0x80001360]:sw tp, 12(ra)
[0x80001364]:flw ft10, 1560(gp)
[0x80001368]:flw ft9, 1564(gp)
[0x8000136c]:flw ft8, 1568(gp)
[0x80001370]:addi sp, zero, 2
[0x80001374]:csrrw zero, fcsr, sp
[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000137c]:csrrs tp, fcsr, zero
[0x80001380]:fsw ft11, 16(ra)
[0x80001384]:sw tp, 20(ra)
[0x80001388]:flw ft10, 1572(gp)
[0x8000138c]:flw ft9, 1576(gp)
[0x80001390]:flw ft8, 1580(gp)
[0x80001394]:addi sp, zero, 34
[0x80001398]:csrrw zero, fcsr, sp
[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013a0]:csrrs tp, fcsr, zero
[0x800013a4]:fsw ft11, 24(ra)
[0x800013a8]:sw tp, 28(ra)
[0x800013ac]:flw ft10, 1584(gp)
[0x800013b0]:flw ft9, 1588(gp)
[0x800013b4]:flw ft8, 1592(gp)
[0x800013b8]:addi sp, zero, 66
[0x800013bc]:csrrw zero, fcsr, sp
[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013c4]:csrrs tp, fcsr, zero
[0x800013c8]:fsw ft11, 32(ra)
[0x800013cc]:sw tp, 36(ra)
[0x800013d0]:flw ft10, 1596(gp)
[0x800013d4]:flw ft9, 1600(gp)
[0x800013d8]:flw ft8, 1604(gp)
[0x800013dc]:addi sp, zero, 98
[0x800013e0]:csrrw zero, fcsr, sp
[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013e8]:csrrs tp, fcsr, zero
[0x800013ec]:fsw ft11, 40(ra)
[0x800013f0]:sw tp, 44(ra)
[0x800013f4]:flw ft10, 1608(gp)
[0x800013f8]:flw ft9, 1612(gp)
[0x800013fc]:flw ft8, 1616(gp)
[0x80001400]:addi sp, zero, 130
[0x80001404]:csrrw zero, fcsr, sp
[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000140c]:csrrs tp, fcsr, zero
[0x80001410]:fsw ft11, 48(ra)
[0x80001414]:sw tp, 52(ra)
[0x80001418]:flw ft10, 1620(gp)
[0x8000141c]:flw ft9, 1624(gp)
[0x80001420]:flw ft8, 1628(gp)
[0x80001424]:addi sp, zero, 2
[0x80001428]:csrrw zero, fcsr, sp
[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001430]:csrrs tp, fcsr, zero
[0x80001434]:fsw ft11, 56(ra)
[0x80001438]:sw tp, 60(ra)
[0x8000143c]:flw ft10, 1632(gp)
[0x80001440]:flw ft9, 1636(gp)
[0x80001444]:flw ft8, 1640(gp)
[0x80001448]:addi sp, zero, 34
[0x8000144c]:csrrw zero, fcsr, sp
[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001454]:csrrs tp, fcsr, zero
[0x80001458]:fsw ft11, 64(ra)
[0x8000145c]:sw tp, 68(ra)
[0x80001460]:flw ft10, 1644(gp)
[0x80001464]:flw ft9, 1648(gp)
[0x80001468]:flw ft8, 1652(gp)
[0x8000146c]:addi sp, zero, 66
[0x80001470]:csrrw zero, fcsr, sp
[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001478]:csrrs tp, fcsr, zero
[0x8000147c]:fsw ft11, 72(ra)
[0x80001480]:sw tp, 76(ra)
[0x80001484]:flw ft10, 1656(gp)
[0x80001488]:flw ft9, 1660(gp)
[0x8000148c]:flw ft8, 1664(gp)
[0x80001490]:addi sp, zero, 98
[0x80001494]:csrrw zero, fcsr, sp
[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000149c]:csrrs tp, fcsr, zero
[0x800014a0]:fsw ft11, 80(ra)
[0x800014a4]:sw tp, 84(ra)
[0x800014a8]:flw ft10, 1668(gp)
[0x800014ac]:flw ft9, 1672(gp)
[0x800014b0]:flw ft8, 1676(gp)
[0x800014b4]:addi sp, zero, 130
[0x800014b8]:csrrw zero, fcsr, sp
[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800014c0]:csrrs tp, fcsr, zero
[0x800014c4]:fsw ft11, 88(ra)
[0x800014c8]:sw tp, 92(ra)
[0x800014cc]:flw ft10, 1680(gp)
[0x800014d0]:flw ft9, 1684(gp)
[0x800014d4]:flw ft8, 1688(gp)
[0x800014d8]:addi sp, zero, 2
[0x800014dc]:csrrw zero, fcsr, sp
[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800014e4]:csrrs tp, fcsr, zero
[0x800014e8]:fsw ft11, 96(ra)
[0x800014ec]:sw tp, 100(ra)
[0x800014f0]:flw ft10, 1692(gp)
[0x800014f4]:flw ft9, 1696(gp)
[0x800014f8]:flw ft8, 1700(gp)
[0x800014fc]:addi sp, zero, 34
[0x80001500]:csrrw zero, fcsr, sp
[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001508]:csrrs tp, fcsr, zero
[0x8000150c]:fsw ft11, 104(ra)
[0x80001510]:sw tp, 108(ra)
[0x80001514]:flw ft10, 1704(gp)
[0x80001518]:flw ft9, 1708(gp)
[0x8000151c]:flw ft8, 1712(gp)
[0x80001520]:addi sp, zero, 66
[0x80001524]:csrrw zero, fcsr, sp
[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000152c]:csrrs tp, fcsr, zero
[0x80001530]:fsw ft11, 112(ra)
[0x80001534]:sw tp, 116(ra)
[0x80001538]:flw ft10, 1716(gp)
[0x8000153c]:flw ft9, 1720(gp)
[0x80001540]:flw ft8, 1724(gp)
[0x80001544]:addi sp, zero, 98
[0x80001548]:csrrw zero, fcsr, sp
[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001550]:csrrs tp, fcsr, zero
[0x80001554]:fsw ft11, 120(ra)
[0x80001558]:sw tp, 124(ra)
[0x8000155c]:flw ft10, 1728(gp)
[0x80001560]:flw ft9, 1732(gp)
[0x80001564]:flw ft8, 1736(gp)
[0x80001568]:addi sp, zero, 34
[0x8000156c]:csrrw zero, fcsr, sp
[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001574]:csrrs tp, fcsr, zero
[0x80001578]:fsw ft11, 128(ra)
[0x8000157c]:sw tp, 132(ra)
[0x80001580]:flw ft10, 1740(gp)
[0x80001584]:flw ft9, 1744(gp)
[0x80001588]:flw ft8, 1748(gp)
[0x8000158c]:addi sp, zero, 66
[0x80001590]:csrrw zero, fcsr, sp
[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001598]:csrrs tp, fcsr, zero
[0x8000159c]:fsw ft11, 136(ra)
[0x800015a0]:sw tp, 140(ra)
[0x800015a4]:flw ft10, 1752(gp)
[0x800015a8]:flw ft9, 1756(gp)
[0x800015ac]:flw ft8, 1760(gp)
[0x800015b0]:addi sp, zero, 98
[0x800015b4]:csrrw zero, fcsr, sp
[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800015bc]:csrrs tp, fcsr, zero
[0x800015c0]:fsw ft11, 144(ra)
[0x800015c4]:sw tp, 148(ra)
[0x800015c8]:flw ft10, 1764(gp)
[0x800015cc]:flw ft9, 1768(gp)
[0x800015d0]:flw ft8, 1772(gp)
[0x800015d4]:addi sp, zero, 130
[0x800015d8]:csrrw zero, fcsr, sp
[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800015e0]:csrrs tp, fcsr, zero
[0x800015e4]:fsw ft11, 152(ra)
[0x800015e8]:sw tp, 156(ra)
[0x800015ec]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : f30', 'rs2 : f29', 'rd : f31', 'rs3 : f30', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000128]:fnmadd.s ft11, ft10, ft9, ft10, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:fsw ft11, 0(ra)
	-[0x80000134]:sw tp, 4(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80003818]:0x00000003




Last Coverpoint : ['rs1 : f31', 'rs2 : f28', 'rd : f28', 'rs3 : f28', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:fsw ft8, 8(ra)
	-[0x80000158]:sw tp, 12(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80003820]:0x00000023




Last Coverpoint : ['rs1 : f27', 'rs2 : f27', 'rd : f27', 'rs3 : f31', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:fsw fs11, 16(ra)
	-[0x8000017c]:sw tp, 20(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80003828]:0x00000043




Last Coverpoint : ['rs1 : f29', 'rs2 : f26', 'rd : f30', 'rs3 : f26', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:fsw ft10, 24(ra)
	-[0x800001a0]:sw tp, 28(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80003830]:0x00000063




Last Coverpoint : ['rs1 : f26', 'rs2 : f31', 'rd : f26', 'rs3 : f29', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b0ecf and fs2 == 0 and fe2 == 0x7b and fm2 == 0x567a4f and fs3 == 0 and fe3 == 0x17 and fm3 == 0x2a1f65 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:fsw fs10, 32(ra)
	-[0x800001c4]:sw tp, 36(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80003838]:0x00000083




Last Coverpoint : ['rs1 : f28', 'rs2 : f30', 'rd : f25', 'rs3 : f25', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0x15 and fm1 == 0x08be86 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x7e6538 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x07e31a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:fsw fs9, 40(ra)
	-[0x800001e8]:sw tp, 44(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80003840]:0x00000003




Last Coverpoint : ['rs1 : f24', 'rs2 : f24', 'rd : f24', 'rs3 : f24', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs8, 48(ra)
	-[0x8000020c]:sw tp, 52(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80003848]:0x00000023




Last Coverpoint : ['rs1 : f23', 'rs2 : f25', 'rd : f23', 'rs3 : f23', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs7, 56(ra)
	-[0x80000230]:sw tp, 60(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80003850]:0x00000043




Last Coverpoint : ['rs1 : f22', 'rs2 : f22', 'rd : f29', 'rs3 : f27', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:fsw ft9, 64(ra)
	-[0x80000254]:sw tp, 68(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80003858]:0x00000063




Last Coverpoint : ['rs1 : f21', 'rs2 : f21', 'rd : f22', 'rs3 : f21', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:fsw fs6, 72(ra)
	-[0x80000278]:sw tp, 76(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80003860]:0x00000083




Last Coverpoint : ['rs1 : f25', 'rs2 : f20', 'rd : f20', 'rs3 : f22', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x285493 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x674cf0 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x1816e8 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:fsw fs4, 80(ra)
	-[0x8000029c]:sw tp, 84(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80003868]:0x00000003




Last Coverpoint : ['rs1 : f20', 'rs2 : f23', 'rd : f21', 'rs3 : f19', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x285493 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x674cf0 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x1816e8 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:fsw fs5, 88(ra)
	-[0x800002c0]:sw tp, 92(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80003870]:0x00000023




Last Coverpoint : ['rs1 : f18', 'rs2 : f17', 'rd : f19', 'rs3 : f20', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x285493 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x674cf0 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x1816e8 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:fsw fs3, 96(ra)
	-[0x800002e4]:sw tp, 100(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80003878]:0x00000043




Last Coverpoint : ['rs1 : f17', 'rs2 : f19', 'rd : f18', 'rs3 : f16', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x285493 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x674cf0 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x1816e8 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fs2, 104(ra)
	-[0x80000308]:sw tp, 108(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80003880]:0x00000063




Last Coverpoint : ['rs1 : f19', 'rs2 : f16', 'rd : f17', 'rs3 : f18', 'fs1 == 0 and fe1 == 0x19 and fm1 == 0x285493 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x674cf0 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x1816e8 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:fsw fa7, 112(ra)
	-[0x8000032c]:sw tp, 116(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80003888]:0x00000083




Last Coverpoint : ['rs1 : f15', 'rs2 : f18', 'rd : f16', 'rs3 : f17', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x218d77 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x71d3ff and fs3 == 0 and fe3 == 0x1a and fm3 == 0x189bf6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa6, 120(ra)
	-[0x80000350]:sw tp, 124(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80003890]:0x00000003




Last Coverpoint : ['rs1 : f16', 'rs2 : f14', 'rd : f15', 'rs3 : f13', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x218d77 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x71d3ff and fs3 == 0 and fe3 == 0x1a and fm3 == 0x189bf6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:fsw fa5, 128(ra)
	-[0x80000374]:sw tp, 132(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80003898]:0x00000023




Last Coverpoint : ['rs1 : f13', 'rs2 : f15', 'rd : f14', 'rs3 : f12', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x218d77 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x71d3ff and fs3 == 0 and fe3 == 0x1a and fm3 == 0x189bf6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:fsw fa4, 136(ra)
	-[0x80000398]:sw tp, 140(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x800038a0]:0x00000043




Last Coverpoint : ['rs1 : f14', 'rs2 : f12', 'rd : f13', 'rs3 : f15', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x218d77 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x71d3ff and fs3 == 0 and fe3 == 0x1a and fm3 == 0x189bf6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:fsw fa3, 144(ra)
	-[0x800003bc]:sw tp, 148(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x800038a8]:0x00000063




Last Coverpoint : ['rs1 : f11', 'rs2 : f13', 'rd : f12', 'rs3 : f14', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x218d77 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x71d3ff and fs3 == 0 and fe3 == 0x1a and fm3 == 0x189bf6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsw fa2, 152(ra)
	-[0x800003e0]:sw tp, 156(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x800038b0]:0x00000083




Last Coverpoint : ['rs1 : f12', 'rs2 : f10', 'rd : f11', 'rs3 : f9', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x07b93f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11c9d8 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1a95e0 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw fa1, 160(ra)
	-[0x80000404]:sw tp, 164(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x800038b8]:0x00000003




Last Coverpoint : ['rs1 : f9', 'rs2 : f11', 'rd : f10', 'rs3 : f8', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x07b93f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11c9d8 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1a95e0 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:fsw fa0, 168(ra)
	-[0x80000428]:sw tp, 172(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x800038c0]:0x00000023




Last Coverpoint : ['rs1 : f10', 'rs2 : f8', 'rd : f9', 'rs3 : f11', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x07b93f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11c9d8 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1a95e0 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn
	-[0x80000444]:csrrs tp, fcsr, zero
	-[0x80000448]:fsw fs1, 176(ra)
	-[0x8000044c]:sw tp, 180(ra)
Current Store : [0x8000044c] : sw tp, 180(ra) -- Store: [0x800038c8]:0x00000043




Last Coverpoint : ['rs1 : f7', 'rs2 : f9', 'rd : f8', 'rs3 : f10', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x07b93f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11c9d8 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1a95e0 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw fs0, 184(ra)
	-[0x80000470]:sw tp, 188(ra)
Current Store : [0x80000470] : sw tp, 188(ra) -- Store: [0x800038d0]:0x00000063




Last Coverpoint : ['rs1 : f8', 'rs2 : f6', 'rd : f7', 'rs3 : f5', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x07b93f and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11c9d8 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x1a95e0 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn
	-[0x8000048c]:csrrs tp, fcsr, zero
	-[0x80000490]:fsw ft7, 192(ra)
	-[0x80000494]:sw tp, 196(ra)
Current Store : [0x80000494] : sw tp, 196(ra) -- Store: [0x800038d8]:0x00000083




Last Coverpoint : ['rs1 : f5', 'rs2 : f7', 'rd : f6', 'rs3 : f4', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4f63b4 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x715671 and fs3 == 0 and fe3 == 0x18 and fm3 == 0x4382e4 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn
	-[0x800004b0]:csrrs tp, fcsr, zero
	-[0x800004b4]:fsw ft6, 200(ra)
	-[0x800004b8]:sw tp, 204(ra)
Current Store : [0x800004b8] : sw tp, 204(ra) -- Store: [0x800038e0]:0x00000003




Last Coverpoint : ['rs1 : f6', 'rs2 : f4', 'rd : f5', 'rs3 : f7', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4f63b4 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x715671 and fs3 == 0 and fe3 == 0x18 and fm3 == 0x4382e4 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsw ft5, 208(ra)
	-[0x800004dc]:sw tp, 212(ra)
Current Store : [0x800004dc] : sw tp, 212(ra) -- Store: [0x800038e8]:0x00000023




Last Coverpoint : ['rs1 : f3', 'rs2 : f5', 'rd : f4', 'rs3 : f6', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4f63b4 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x715671 and fs3 == 0 and fe3 == 0x18 and fm3 == 0x4382e4 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn
	-[0x800004f8]:csrrs tp, fcsr, zero
	-[0x800004fc]:fsw ft4, 216(ra)
	-[0x80000500]:sw tp, 220(ra)
Current Store : [0x80000500] : sw tp, 220(ra) -- Store: [0x800038f0]:0x00000043




Last Coverpoint : ['rs1 : f4', 'rs2 : f2', 'rd : f3', 'rs3 : f1', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4f63b4 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x715671 and fs3 == 0 and fe3 == 0x18 and fm3 == 0x4382e4 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn
	-[0x8000051c]:csrrs tp, fcsr, zero
	-[0x80000520]:fsw ft3, 224(ra)
	-[0x80000524]:sw tp, 228(ra)
Current Store : [0x80000524] : sw tp, 228(ra) -- Store: [0x800038f8]:0x00000063




Last Coverpoint : ['rs1 : f1', 'rs2 : f3', 'rd : f2', 'rs3 : f0', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4f63b4 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x715671 and fs3 == 0 and fe3 == 0x18 and fm3 == 0x4382e4 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:fsw ft2, 232(ra)
	-[0x80000548]:sw tp, 236(ra)
Current Store : [0x80000548] : sw tp, 236(ra) -- Store: [0x80003900]:0x00000083




Last Coverpoint : ['rs1 : f2', 'rs2 : f0', 'rd : f1', 'rs3 : f3', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4e0628 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2f03b4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0cd930 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn
	-[0x80000564]:csrrs tp, fcsr, zero
	-[0x80000568]:fsw ft1, 240(ra)
	-[0x8000056c]:sw tp, 244(ra)
Current Store : [0x8000056c] : sw tp, 244(ra) -- Store: [0x80003908]:0x00000003




Last Coverpoint : ['rs1 : f0', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4e0628 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2f03b4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0cd930 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 248(ra)
	-[0x80000590]:sw tp, 252(ra)
Current Store : [0x80000590] : sw tp, 252(ra) -- Store: [0x80003910]:0x00000023




Last Coverpoint : ['rs2 : f1', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4e0628 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2f03b4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0cd930 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn
	-[0x800005ac]:csrrs tp, fcsr, zero
	-[0x800005b0]:fsw ft11, 256(ra)
	-[0x800005b4]:sw tp, 260(ra)
Current Store : [0x800005b4] : sw tp, 260(ra) -- Store: [0x80003918]:0x00000043




Last Coverpoint : ['rs3 : f2', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4e0628 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2f03b4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0cd930 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsw ft11, 264(ra)
	-[0x800005d8]:sw tp, 268(ra)
Current Store : [0x800005d8] : sw tp, 268(ra) -- Store: [0x80003920]:0x00000063




Last Coverpoint : ['rd : f0', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x4e0628 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2f03b4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0cd930 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn
	-[0x800005f4]:csrrs tp, fcsr, zero
	-[0x800005f8]:fsw ft0, 272(ra)
	-[0x800005fc]:sw tp, 276(ra)
Current Store : [0x800005fc] : sw tp, 276(ra) -- Store: [0x80003928]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x29c831 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x355a3f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x708ccb and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000618]:csrrs tp, fcsr, zero
	-[0x8000061c]:fsw ft11, 280(ra)
	-[0x80000620]:sw tp, 284(ra)
Current Store : [0x80000620] : sw tp, 284(ra) -- Store: [0x80003930]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x29c831 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x355a3f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x708ccb and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000063c]:csrrs tp, fcsr, zero
	-[0x80000640]:fsw ft11, 288(ra)
	-[0x80000644]:sw tp, 292(ra)
Current Store : [0x80000644] : sw tp, 292(ra) -- Store: [0x80003938]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x29c831 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x355a3f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x708ccb and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:fsw ft11, 296(ra)
	-[0x80000668]:sw tp, 300(ra)
Current Store : [0x80000668] : sw tp, 300(ra) -- Store: [0x80003940]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x29c831 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x355a3f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x708ccb and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000684]:csrrs tp, fcsr, zero
	-[0x80000688]:fsw ft11, 304(ra)
	-[0x8000068c]:sw tp, 308(ra)
Current Store : [0x8000068c] : sw tp, 308(ra) -- Store: [0x80003948]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x29c831 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x355a3f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x708ccb and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft11, 312(ra)
	-[0x800006b0]:sw tp, 316(ra)
Current Store : [0x800006b0] : sw tp, 316(ra) -- Store: [0x80003950]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59e2ae and fs2 == 0 and fe2 == 0x7b and fm2 == 0x435081 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x263c30 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006cc]:csrrs tp, fcsr, zero
	-[0x800006d0]:fsw ft11, 320(ra)
	-[0x800006d4]:sw tp, 324(ra)
Current Store : [0x800006d4] : sw tp, 324(ra) -- Store: [0x80003958]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59e2ae and fs2 == 0 and fe2 == 0x7b and fm2 == 0x435081 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x263c30 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006f0]:csrrs tp, fcsr, zero
	-[0x800006f4]:fsw ft11, 328(ra)
	-[0x800006f8]:sw tp, 332(ra)
Current Store : [0x800006f8] : sw tp, 332(ra) -- Store: [0x80003960]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59e2ae and fs2 == 0 and fe2 == 0x7b and fm2 == 0x435081 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x263c30 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000714]:csrrs tp, fcsr, zero
	-[0x80000718]:fsw ft11, 336(ra)
	-[0x8000071c]:sw tp, 340(ra)
Current Store : [0x8000071c] : sw tp, 340(ra) -- Store: [0x80003968]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59e2ae and fs2 == 0 and fe2 == 0x7b and fm2 == 0x435081 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x263c30 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000738]:csrrs tp, fcsr, zero
	-[0x8000073c]:fsw ft11, 344(ra)
	-[0x80000740]:sw tp, 348(ra)
Current Store : [0x80000740] : sw tp, 348(ra) -- Store: [0x80003970]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59e2ae and fs2 == 0 and fe2 == 0x7b and fm2 == 0x435081 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x263c30 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000075c]:csrrs tp, fcsr, zero
	-[0x80000760]:fsw ft11, 352(ra)
	-[0x80000764]:sw tp, 356(ra)
Current Store : [0x80000764] : sw tp, 356(ra) -- Store: [0x80003978]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x760f4d and fs2 == 0 and fe2 == 0x7e and fm2 == 0x47926b and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3fd2a1 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000780]:csrrs tp, fcsr, zero
	-[0x80000784]:fsw ft11, 360(ra)
	-[0x80000788]:sw tp, 364(ra)
Current Store : [0x80000788] : sw tp, 364(ra) -- Store: [0x80003980]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x760f4d and fs2 == 0 and fe2 == 0x7e and fm2 == 0x47926b and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3fd2a1 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007a4]:csrrs tp, fcsr, zero
	-[0x800007a8]:fsw ft11, 368(ra)
	-[0x800007ac]:sw tp, 372(ra)
Current Store : [0x800007ac] : sw tp, 372(ra) -- Store: [0x80003988]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x760f4d and fs2 == 0 and fe2 == 0x7e and fm2 == 0x47926b and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3fd2a1 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 376(ra)
	-[0x800007d0]:sw tp, 380(ra)
Current Store : [0x800007d0] : sw tp, 380(ra) -- Store: [0x80003990]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x760f4d and fs2 == 0 and fe2 == 0x7e and fm2 == 0x47926b and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3fd2a1 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007ec]:csrrs tp, fcsr, zero
	-[0x800007f0]:fsw ft11, 384(ra)
	-[0x800007f4]:sw tp, 388(ra)
Current Store : [0x800007f4] : sw tp, 388(ra) -- Store: [0x80003998]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x760f4d and fs2 == 0 and fe2 == 0x7e and fm2 == 0x47926b and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3fd2a1 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000810]:csrrs tp, fcsr, zero
	-[0x80000814]:fsw ft11, 392(ra)
	-[0x80000818]:sw tp, 396(ra)
Current Store : [0x80000818] : sw tp, 396(ra) -- Store: [0x800039a0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x4388ad and fs2 == 0 and fe2 == 0x7b and fm2 == 0x52d0ea and fs3 == 0 and fe3 == 0x16 and fm3 == 0x2105b0 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000834]:csrrs tp, fcsr, zero
	-[0x80000838]:fsw ft11, 400(ra)
	-[0x8000083c]:sw tp, 404(ra)
Current Store : [0x8000083c] : sw tp, 404(ra) -- Store: [0x800039a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x4388ad and fs2 == 0 and fe2 == 0x7b and fm2 == 0x52d0ea and fs3 == 0 and fe3 == 0x16 and fm3 == 0x2105b0 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000858]:csrrs tp, fcsr, zero
	-[0x8000085c]:fsw ft11, 408(ra)
	-[0x80000860]:sw tp, 412(ra)
Current Store : [0x80000860] : sw tp, 412(ra) -- Store: [0x800039b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x4388ad and fs2 == 0 and fe2 == 0x7b and fm2 == 0x52d0ea and fs3 == 0 and fe3 == 0x16 and fm3 == 0x2105b0 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000087c]:csrrs tp, fcsr, zero
	-[0x80000880]:fsw ft11, 416(ra)
	-[0x80000884]:sw tp, 420(ra)
Current Store : [0x80000884] : sw tp, 420(ra) -- Store: [0x800039b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x4388ad and fs2 == 0 and fe2 == 0x7b and fm2 == 0x52d0ea and fs3 == 0 and fe3 == 0x16 and fm3 == 0x2105b0 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008a0]:csrrs tp, fcsr, zero
	-[0x800008a4]:fsw ft11, 424(ra)
	-[0x800008a8]:sw tp, 428(ra)
Current Store : [0x800008a8] : sw tp, 428(ra) -- Store: [0x800039c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x4388ad and fs2 == 0 and fe2 == 0x7b and fm2 == 0x52d0ea and fs3 == 0 and fe3 == 0x16 and fm3 == 0x2105b0 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008c4]:csrrs tp, fcsr, zero
	-[0x800008c8]:fsw ft11, 432(ra)
	-[0x800008cc]:sw tp, 436(ra)
Current Store : [0x800008cc] : sw tp, 436(ra) -- Store: [0x800039c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0c7098 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0bd39e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x196a75 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 440(ra)
	-[0x800008f0]:sw tp, 444(ra)
Current Store : [0x800008f0] : sw tp, 444(ra) -- Store: [0x800039d0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0c7098 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0bd39e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x196a75 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000090c]:csrrs tp, fcsr, zero
	-[0x80000910]:fsw ft11, 448(ra)
	-[0x80000914]:sw tp, 452(ra)
Current Store : [0x80000914] : sw tp, 452(ra) -- Store: [0x800039d8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0c7098 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0bd39e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x196a75 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000930]:csrrs tp, fcsr, zero
	-[0x80000934]:fsw ft11, 456(ra)
	-[0x80000938]:sw tp, 460(ra)
Current Store : [0x80000938] : sw tp, 460(ra) -- Store: [0x800039e0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0c7098 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0bd39e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x196a75 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000954]:csrrs tp, fcsr, zero
	-[0x80000958]:fsw ft11, 464(ra)
	-[0x8000095c]:sw tp, 468(ra)
Current Store : [0x8000095c] : sw tp, 468(ra) -- Store: [0x800039e8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0c7098 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0bd39e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x196a75 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000978]:csrrs tp, fcsr, zero
	-[0x8000097c]:fsw ft11, 472(ra)
	-[0x80000980]:sw tp, 476(ra)
Current Store : [0x80000980] : sw tp, 476(ra) -- Store: [0x800039f0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x69458e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1c7d2f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0e9874 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000099c]:csrrs tp, fcsr, zero
	-[0x800009a0]:fsw ft11, 480(ra)
	-[0x800009a4]:sw tp, 484(ra)
Current Store : [0x800009a4] : sw tp, 484(ra) -- Store: [0x800039f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x69458e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1c7d2f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0e9874 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800009c0]:csrrs tp, fcsr, zero
	-[0x800009c4]:fsw ft11, 488(ra)
	-[0x800009c8]:sw tp, 492(ra)
Current Store : [0x800009c8] : sw tp, 492(ra) -- Store: [0x80003a00]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x69458e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1c7d2f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0e9874 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800009e4]:csrrs tp, fcsr, zero
	-[0x800009e8]:fsw ft11, 496(ra)
	-[0x800009ec]:sw tp, 500(ra)
Current Store : [0x800009ec] : sw tp, 500(ra) -- Store: [0x80003a08]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x69458e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1c7d2f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0e9874 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 504(ra)
	-[0x80000a10]:sw tp, 508(ra)
Current Store : [0x80000a10] : sw tp, 508(ra) -- Store: [0x80003a10]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x69458e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1c7d2f and fs3 == 0 and fe3 == 0x1b and fm3 == 0x0e9874 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a2c]:csrrs tp, fcsr, zero
	-[0x80000a30]:fsw ft11, 512(ra)
	-[0x80000a34]:sw tp, 516(ra)
Current Store : [0x80000a34] : sw tp, 516(ra) -- Store: [0x80003a18]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x67bf8e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2518d4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1574f1 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a50]:csrrs tp, fcsr, zero
	-[0x80000a54]:fsw ft11, 520(ra)
	-[0x80000a58]:sw tp, 524(ra)
Current Store : [0x80000a58] : sw tp, 524(ra) -- Store: [0x80003a20]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x67bf8e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2518d4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1574f1 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a74]:csrrs tp, fcsr, zero
	-[0x80000a78]:fsw ft11, 528(ra)
	-[0x80000a7c]:sw tp, 532(ra)
Current Store : [0x80000a7c] : sw tp, 532(ra) -- Store: [0x80003a28]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x67bf8e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2518d4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1574f1 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a98]:csrrs tp, fcsr, zero
	-[0x80000a9c]:fsw ft11, 536(ra)
	-[0x80000aa0]:sw tp, 540(ra)
Current Store : [0x80000aa0] : sw tp, 540(ra) -- Store: [0x80003a30]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x67bf8e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2518d4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1574f1 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000abc]:csrrs tp, fcsr, zero
	-[0x80000ac0]:fsw ft11, 544(ra)
	-[0x80000ac4]:sw tp, 548(ra)
Current Store : [0x80000ac4] : sw tp, 548(ra) -- Store: [0x80003a38]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x67bf8e and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2518d4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1574f1 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ae0]:csrrs tp, fcsr, zero
	-[0x80000ae4]:fsw ft11, 552(ra)
	-[0x80000ae8]:sw tp, 556(ra)
Current Store : [0x80000ae8] : sw tp, 556(ra) -- Store: [0x80003a40]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x002c46 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x01c78e and fs3 == 0 and fe3 == 0x1b and fm3 == 0x01f472 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b04]:csrrs tp, fcsr, zero
	-[0x80000b08]:fsw ft11, 560(ra)
	-[0x80000b0c]:sw tp, 564(ra)
Current Store : [0x80000b0c] : sw tp, 564(ra) -- Store: [0x80003a48]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x002c46 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x01c78e and fs3 == 0 and fe3 == 0x1b and fm3 == 0x01f472 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b28]:csrrs tp, fcsr, zero
	-[0x80000b2c]:fsw ft11, 568(ra)
	-[0x80000b30]:sw tp, 572(ra)
Current Store : [0x80000b30] : sw tp, 572(ra) -- Store: [0x80003a50]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x002c46 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x01c78e and fs3 == 0 and fe3 == 0x1b and fm3 == 0x01f472 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b4c]:csrrs tp, fcsr, zero
	-[0x80000b50]:fsw ft11, 576(ra)
	-[0x80000b54]:sw tp, 580(ra)
Current Store : [0x80000b54] : sw tp, 580(ra) -- Store: [0x80003a58]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x002c46 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x01c78e and fs3 == 0 and fe3 == 0x1b and fm3 == 0x01f472 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b70]:csrrs tp, fcsr, zero
	-[0x80000b74]:fsw ft11, 584(ra)
	-[0x80000b78]:sw tp, 588(ra)
Current Store : [0x80000b78] : sw tp, 588(ra) -- Store: [0x80003a60]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x002c46 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x01c78e and fs3 == 0 and fe3 == 0x1b and fm3 == 0x01f472 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b94]:csrrs tp, fcsr, zero
	-[0x80000b98]:fsw ft11, 592(ra)
	-[0x80000b9c]:sw tp, 596(ra)
Current Store : [0x80000b9c] : sw tp, 596(ra) -- Store: [0x80003a68]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x1049ac and fs2 == 0 and fe2 == 0x80 and fm2 == 0x06abc4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x17cec0 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000bb8]:csrrs tp, fcsr, zero
	-[0x80000bbc]:fsw ft11, 600(ra)
	-[0x80000bc0]:sw tp, 604(ra)
Current Store : [0x80000bc0] : sw tp, 604(ra) -- Store: [0x80003a70]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x1049ac and fs2 == 0 and fe2 == 0x80 and fm2 == 0x06abc4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x17cec0 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000bdc]:csrrs tp, fcsr, zero
	-[0x80000be0]:fsw ft11, 608(ra)
	-[0x80000be4]:sw tp, 612(ra)
Current Store : [0x80000be4] : sw tp, 612(ra) -- Store: [0x80003a78]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x1049ac and fs2 == 0 and fe2 == 0x80 and fm2 == 0x06abc4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x17cec0 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c00]:csrrs tp, fcsr, zero
	-[0x80000c04]:fsw ft11, 616(ra)
	-[0x80000c08]:sw tp, 620(ra)
Current Store : [0x80000c08] : sw tp, 620(ra) -- Store: [0x80003a80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x1049ac and fs2 == 0 and fe2 == 0x80 and fm2 == 0x06abc4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x17cec0 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c24]:csrrs tp, fcsr, zero
	-[0x80000c28]:fsw ft11, 624(ra)
	-[0x80000c2c]:sw tp, 628(ra)
Current Store : [0x80000c2c] : sw tp, 628(ra) -- Store: [0x80003a88]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x1049ac and fs2 == 0 and fe2 == 0x80 and fm2 == 0x06abc4 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x17cec0 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c48]:csrrs tp, fcsr, zero
	-[0x80000c4c]:fsw ft11, 632(ra)
	-[0x80000c50]:sw tp, 636(ra)
Current Store : [0x80000c50] : sw tp, 636(ra) -- Store: [0x80003a90]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e9ab1 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x631860 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x7d017f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c6c]:csrrs tp, fcsr, zero
	-[0x80000c70]:fsw ft11, 640(ra)
	-[0x80000c74]:sw tp, 644(ra)
Current Store : [0x80000c74] : sw tp, 644(ra) -- Store: [0x80003a98]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e9ab1 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x631860 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x7d017f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c90]:csrrs tp, fcsr, zero
	-[0x80000c94]:fsw ft11, 648(ra)
	-[0x80000c98]:sw tp, 652(ra)
Current Store : [0x80000c98] : sw tp, 652(ra) -- Store: [0x80003aa0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e9ab1 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x631860 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x7d017f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cb4]:csrrs tp, fcsr, zero
	-[0x80000cb8]:fsw ft11, 656(ra)
	-[0x80000cbc]:sw tp, 660(ra)
Current Store : [0x80000cbc] : sw tp, 660(ra) -- Store: [0x80003aa8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e9ab1 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x631860 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x7d017f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cd8]:csrrs tp, fcsr, zero
	-[0x80000cdc]:fsw ft11, 664(ra)
	-[0x80000ce0]:sw tp, 668(ra)
Current Store : [0x80000ce0] : sw tp, 668(ra) -- Store: [0x80003ab0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e9ab1 and fs2 == 0 and fe2 == 0x7b and fm2 == 0x631860 and fs3 == 0 and fe3 == 0x17 and fm3 == 0x7d017f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cfc]:csrrs tp, fcsr, zero
	-[0x80000d00]:fsw ft11, 672(ra)
	-[0x80000d04]:sw tp, 676(ra)
Current Store : [0x80000d04] : sw tp, 676(ra) -- Store: [0x80003ab8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x307f67 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4c450f and fs3 == 0 and fe3 == 0x19 and fm3 == 0x0cd523 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d20]:csrrs tp, fcsr, zero
	-[0x80000d24]:fsw ft11, 680(ra)
	-[0x80000d28]:sw tp, 684(ra)
Current Store : [0x80000d28] : sw tp, 684(ra) -- Store: [0x80003ac0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x307f67 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4c450f and fs3 == 0 and fe3 == 0x19 and fm3 == 0x0cd523 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d44]:csrrs tp, fcsr, zero
	-[0x80000d48]:fsw ft11, 688(ra)
	-[0x80000d4c]:sw tp, 692(ra)
Current Store : [0x80000d4c] : sw tp, 692(ra) -- Store: [0x80003ac8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x307f67 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4c450f and fs3 == 0 and fe3 == 0x19 and fm3 == 0x0cd523 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d68]:csrrs tp, fcsr, zero
	-[0x80000d6c]:fsw ft11, 696(ra)
	-[0x80000d70]:sw tp, 700(ra)
Current Store : [0x80000d70] : sw tp, 700(ra) -- Store: [0x80003ad0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x307f67 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4c450f and fs3 == 0 and fe3 == 0x19 and fm3 == 0x0cd523 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d8c]:csrrs tp, fcsr, zero
	-[0x80000d90]:fsw ft11, 704(ra)
	-[0x80000d94]:sw tp, 708(ra)
Current Store : [0x80000d94] : sw tp, 708(ra) -- Store: [0x80003ad8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x307f67 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4c450f and fs3 == 0 and fe3 == 0x19 and fm3 == 0x0cd523 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000db0]:csrrs tp, fcsr, zero
	-[0x80000db4]:fsw ft11, 712(ra)
	-[0x80000db8]:sw tp, 716(ra)
Current Store : [0x80000db8] : sw tp, 716(ra) -- Store: [0x80003ae0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1ca879 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x674c9e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x0d8ae9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000dd4]:csrrs tp, fcsr, zero
	-[0x80000dd8]:fsw ft11, 720(ra)
	-[0x80000ddc]:sw tp, 724(ra)
Current Store : [0x80000ddc] : sw tp, 724(ra) -- Store: [0x80003ae8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1ca879 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x674c9e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x0d8ae9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000df8]:csrrs tp, fcsr, zero
	-[0x80000dfc]:fsw ft11, 728(ra)
	-[0x80000e00]:sw tp, 732(ra)
Current Store : [0x80000e00] : sw tp, 732(ra) -- Store: [0x80003af0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1ca879 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x674c9e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x0d8ae9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e1c]:csrrs tp, fcsr, zero
	-[0x80000e20]:fsw ft11, 736(ra)
	-[0x80000e24]:sw tp, 740(ra)
Current Store : [0x80000e24] : sw tp, 740(ra) -- Store: [0x80003af8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1ca879 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x674c9e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x0d8ae9 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e40]:csrrs tp, fcsr, zero
	-[0x80000e44]:fsw ft11, 744(ra)
	-[0x80000e48]:sw tp, 748(ra)
Current Store : [0x80000e48] : sw tp, 748(ra) -- Store: [0x80003b00]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x1ca879 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x674c9e and fs3 == 0 and fe3 == 0x1a and fm3 == 0x0d8ae9 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e64]:csrrs tp, fcsr, zero
	-[0x80000e68]:fsw ft11, 752(ra)
	-[0x80000e6c]:sw tp, 756(ra)
Current Store : [0x80000e6c] : sw tp, 756(ra) -- Store: [0x80003b08]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b5be7 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x75f3b7 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x43608e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e88]:csrrs tp, fcsr, zero
	-[0x80000e8c]:fsw ft11, 760(ra)
	-[0x80000e90]:sw tp, 764(ra)
Current Store : [0x80000e90] : sw tp, 764(ra) -- Store: [0x80003b10]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b5be7 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x75f3b7 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x43608e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000eac]:csrrs tp, fcsr, zero
	-[0x80000eb0]:fsw ft11, 768(ra)
	-[0x80000eb4]:sw tp, 772(ra)
Current Store : [0x80000eb4] : sw tp, 772(ra) -- Store: [0x80003b18]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b5be7 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x75f3b7 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x43608e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ed0]:csrrs tp, fcsr, zero
	-[0x80000ed4]:fsw ft11, 776(ra)
	-[0x80000ed8]:sw tp, 780(ra)
Current Store : [0x80000ed8] : sw tp, 780(ra) -- Store: [0x80003b20]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b5be7 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x75f3b7 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x43608e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ef4]:csrrs tp, fcsr, zero
	-[0x80000ef8]:fsw ft11, 784(ra)
	-[0x80000efc]:sw tp, 788(ra)
Current Store : [0x80000efc] : sw tp, 788(ra) -- Store: [0x80003b28]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b5be7 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x75f3b7 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x43608e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f18]:csrrs tp, fcsr, zero
	-[0x80000f1c]:fsw ft11, 792(ra)
	-[0x80000f20]:sw tp, 796(ra)
Current Store : [0x80000f20] : sw tp, 796(ra) -- Store: [0x80003b30]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x249c19 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x42be13 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x7a7107 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f3c]:csrrs tp, fcsr, zero
	-[0x80000f40]:fsw ft11, 800(ra)
	-[0x80000f44]:sw tp, 804(ra)
Current Store : [0x80000f44] : sw tp, 804(ra) -- Store: [0x80003b38]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x249c19 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x42be13 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x7a7107 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f60]:csrrs tp, fcsr, zero
	-[0x80000f64]:fsw ft11, 808(ra)
	-[0x80000f68]:sw tp, 812(ra)
Current Store : [0x80000f68] : sw tp, 812(ra) -- Store: [0x80003b40]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x249c19 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x42be13 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x7a7107 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f84]:csrrs tp, fcsr, zero
	-[0x80000f88]:fsw ft11, 816(ra)
	-[0x80000f8c]:sw tp, 820(ra)
Current Store : [0x80000f8c] : sw tp, 820(ra) -- Store: [0x80003b48]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x249c19 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x42be13 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x7a7107 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000fa8]:csrrs tp, fcsr, zero
	-[0x80000fac]:fsw ft11, 824(ra)
	-[0x80000fb0]:sw tp, 828(ra)
Current Store : [0x80000fb0] : sw tp, 828(ra) -- Store: [0x80003b50]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x249c19 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x42be13 and fs3 == 0 and fe3 == 0x19 and fm3 == 0x7a7107 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000fcc]:csrrs tp, fcsr, zero
	-[0x80000fd0]:fsw ft11, 832(ra)
	-[0x80000fd4]:sw tp, 836(ra)
Current Store : [0x80000fd4] : sw tp, 836(ra) -- Store: [0x80003b58]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x63c101 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x556a2f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3dde0e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ff0]:csrrs tp, fcsr, zero
	-[0x80000ff4]:fsw ft11, 840(ra)
	-[0x80000ff8]:sw tp, 844(ra)
Current Store : [0x80000ff8] : sw tp, 844(ra) -- Store: [0x80003b60]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x63c101 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x556a2f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3dde0e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001014]:csrrs tp, fcsr, zero
	-[0x80001018]:fsw ft11, 848(ra)
	-[0x8000101c]:sw tp, 852(ra)
Current Store : [0x8000101c] : sw tp, 852(ra) -- Store: [0x80003b68]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x63c101 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x556a2f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3dde0e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001038]:csrrs tp, fcsr, zero
	-[0x8000103c]:fsw ft11, 856(ra)
	-[0x80001040]:sw tp, 860(ra)
Current Store : [0x80001040] : sw tp, 860(ra) -- Store: [0x80003b70]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x63c101 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x556a2f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3dde0e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000105c]:csrrs tp, fcsr, zero
	-[0x80001060]:fsw ft11, 864(ra)
	-[0x80001064]:sw tp, 868(ra)
Current Store : [0x80001064] : sw tp, 868(ra) -- Store: [0x80003b78]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x63c101 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x556a2f and fs3 == 0 and fe3 == 0x1a and fm3 == 0x3dde0e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001080]:csrrs tp, fcsr, zero
	-[0x80001084]:fsw ft11, 872(ra)
	-[0x80001088]:sw tp, 876(ra)
Current Store : [0x80001088] : sw tp, 876(ra) -- Store: [0x80003b80]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3d99bd and fs2 == 0 and fe2 == 0x7f and fm2 == 0x461886 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x12b712 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010a4]:csrrs tp, fcsr, zero
	-[0x800010a8]:fsw ft11, 880(ra)
	-[0x800010ac]:sw tp, 884(ra)
Current Store : [0x800010ac] : sw tp, 884(ra) -- Store: [0x80003b88]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3d99bd and fs2 == 0 and fe2 == 0x7f and fm2 == 0x461886 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x12b712 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010c8]:csrrs tp, fcsr, zero
	-[0x800010cc]:fsw ft11, 888(ra)
	-[0x800010d0]:sw tp, 892(ra)
Current Store : [0x800010d0] : sw tp, 892(ra) -- Store: [0x80003b90]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3d99bd and fs2 == 0 and fe2 == 0x7f and fm2 == 0x461886 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x12b712 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010ec]:csrrs tp, fcsr, zero
	-[0x800010f0]:fsw ft11, 896(ra)
	-[0x800010f4]:sw tp, 900(ra)
Current Store : [0x800010f4] : sw tp, 900(ra) -- Store: [0x80003b98]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3d99bd and fs2 == 0 and fe2 == 0x7f and fm2 == 0x461886 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x12b712 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001110]:csrrs tp, fcsr, zero
	-[0x80001114]:fsw ft11, 904(ra)
	-[0x80001118]:sw tp, 908(ra)
Current Store : [0x80001118] : sw tp, 908(ra) -- Store: [0x80003ba0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x3d99bd and fs2 == 0 and fe2 == 0x7f and fm2 == 0x461886 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x12b712 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001134]:csrrs tp, fcsr, zero
	-[0x80001138]:fsw ft11, 912(ra)
	-[0x8000113c]:sw tp, 916(ra)
Current Store : [0x8000113c] : sw tp, 916(ra) -- Store: [0x80003ba8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x5d50db and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2e5a25 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x16bae3 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001158]:csrrs tp, fcsr, zero
	-[0x8000115c]:fsw ft11, 920(ra)
	-[0x80001160]:sw tp, 924(ra)
Current Store : [0x80001160] : sw tp, 924(ra) -- Store: [0x80003bb0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x5d50db and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2e5a25 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x16bae3 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000117c]:csrrs tp, fcsr, zero
	-[0x80001180]:fsw ft11, 928(ra)
	-[0x80001184]:sw tp, 932(ra)
Current Store : [0x80001184] : sw tp, 932(ra) -- Store: [0x80003bb8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x5d50db and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2e5a25 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x16bae3 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011a0]:csrrs tp, fcsr, zero
	-[0x800011a4]:fsw ft11, 936(ra)
	-[0x800011a8]:sw tp, 940(ra)
Current Store : [0x800011a8] : sw tp, 940(ra) -- Store: [0x80003bc0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x5d50db and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2e5a25 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x16bae3 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011c4]:csrrs tp, fcsr, zero
	-[0x800011c8]:fsw ft11, 944(ra)
	-[0x800011cc]:sw tp, 948(ra)
Current Store : [0x800011cc] : sw tp, 948(ra) -- Store: [0x80003bc8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x5d50db and fs2 == 0 and fe2 == 0x7f and fm2 == 0x2e5a25 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x16bae3 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011e8]:csrrs tp, fcsr, zero
	-[0x800011ec]:fsw ft11, 952(ra)
	-[0x800011f0]:sw tp, 956(ra)
Current Store : [0x800011f0] : sw tp, 956(ra) -- Store: [0x80003bd0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0af57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x141db5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x20cc19 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000120c]:csrrs tp, fcsr, zero
	-[0x80001210]:fsw ft11, 960(ra)
	-[0x80001214]:sw tp, 964(ra)
Current Store : [0x80001214] : sw tp, 964(ra) -- Store: [0x80003bd8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0af57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x141db5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x20cc19 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001230]:csrrs tp, fcsr, zero
	-[0x80001234]:fsw ft11, 968(ra)
	-[0x80001238]:sw tp, 972(ra)
Current Store : [0x80001238] : sw tp, 972(ra) -- Store: [0x80003be0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0af57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x141db5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x20cc19 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001254]:csrrs tp, fcsr, zero
	-[0x80001258]:fsw ft11, 976(ra)
	-[0x8000125c]:sw tp, 980(ra)
Current Store : [0x8000125c] : sw tp, 980(ra) -- Store: [0x80003be8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0af57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x141db5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x20cc19 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001278]:csrrs tp, fcsr, zero
	-[0x8000127c]:fsw ft11, 984(ra)
	-[0x80001280]:sw tp, 988(ra)
Current Store : [0x80001280] : sw tp, 988(ra) -- Store: [0x80003bf0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0af57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x141db5 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x20cc19 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000129c]:csrrs tp, fcsr, zero
	-[0x800012a0]:fsw ft11, 992(ra)
	-[0x800012a4]:sw tp, 996(ra)
Current Store : [0x800012a4] : sw tp, 996(ra) -- Store: [0x80003bf8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59d739 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x78cd3d and fs3 == 0 and fe3 == 0x18 and fm3 == 0x53b725 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800012c0]:csrrs tp, fcsr, zero
	-[0x800012c4]:fsw ft11, 1000(ra)
	-[0x800012c8]:sw tp, 1004(ra)
Current Store : [0x800012c8] : sw tp, 1004(ra) -- Store: [0x80003c00]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59d739 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x78cd3d and fs3 == 0 and fe3 == 0x18 and fm3 == 0x53b725 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800012e4]:csrrs tp, fcsr, zero
	-[0x800012e8]:fsw ft11, 1008(ra)
	-[0x800012ec]:sw tp, 1012(ra)
Current Store : [0x800012ec] : sw tp, 1012(ra) -- Store: [0x80003c08]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59d739 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x78cd3d and fs3 == 0 and fe3 == 0x18 and fm3 == 0x53b725 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001308]:csrrs tp, fcsr, zero
	-[0x8000130c]:fsw ft11, 1016(ra)
	-[0x80001310]:sw tp, 1020(ra)
Current Store : [0x80001310] : sw tp, 1020(ra) -- Store: [0x80003c10]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59d739 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x78cd3d and fs3 == 0 and fe3 == 0x18 and fm3 == 0x53b725 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001334]:csrrs tp, fcsr, zero
	-[0x80001338]:fsw ft11, 0(ra)
	-[0x8000133c]:sw tp, 4(ra)
Current Store : [0x8000133c] : sw tp, 4(ra) -- Store: [0x80003c18]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x59d739 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x78cd3d and fs3 == 0 and fe3 == 0x18 and fm3 == 0x53b725 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001358]:csrrs tp, fcsr, zero
	-[0x8000135c]:fsw ft11, 8(ra)
	-[0x80001360]:sw tp, 12(ra)
Current Store : [0x80001360] : sw tp, 12(ra) -- Store: [0x80003c20]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0ba57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0f7788 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1c8548 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000137c]:csrrs tp, fcsr, zero
	-[0x80001380]:fsw ft11, 16(ra)
	-[0x80001384]:sw tp, 20(ra)
Current Store : [0x80001384] : sw tp, 20(ra) -- Store: [0x80003c28]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0ba57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0f7788 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1c8548 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013a0]:csrrs tp, fcsr, zero
	-[0x800013a4]:fsw ft11, 24(ra)
	-[0x800013a8]:sw tp, 28(ra)
Current Store : [0x800013a8] : sw tp, 28(ra) -- Store: [0x80003c30]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0ba57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0f7788 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1c8548 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013c4]:csrrs tp, fcsr, zero
	-[0x800013c8]:fsw ft11, 32(ra)
	-[0x800013cc]:sw tp, 36(ra)
Current Store : [0x800013cc] : sw tp, 36(ra) -- Store: [0x80003c38]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0ba57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0f7788 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1c8548 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013e8]:csrrs tp, fcsr, zero
	-[0x800013ec]:fsw ft11, 40(ra)
	-[0x800013f0]:sw tp, 44(ra)
Current Store : [0x800013f0] : sw tp, 44(ra) -- Store: [0x80003c40]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0ba57c and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0f7788 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x1c8548 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000140c]:csrrs tp, fcsr, zero
	-[0x80001410]:fsw ft11, 48(ra)
	-[0x80001414]:sw tp, 52(ra)
Current Store : [0x80001414] : sw tp, 52(ra) -- Store: [0x80003c48]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x12cb99 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x210a09 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x38af9a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001430]:csrrs tp, fcsr, zero
	-[0x80001434]:fsw ft11, 56(ra)
	-[0x80001438]:sw tp, 60(ra)
Current Store : [0x80001438] : sw tp, 60(ra) -- Store: [0x80003c50]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x12cb99 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x210a09 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x38af9a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001454]:csrrs tp, fcsr, zero
	-[0x80001458]:fsw ft11, 64(ra)
	-[0x8000145c]:sw tp, 68(ra)
Current Store : [0x8000145c] : sw tp, 68(ra) -- Store: [0x80003c58]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x12cb99 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x210a09 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x38af9a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001478]:csrrs tp, fcsr, zero
	-[0x8000147c]:fsw ft11, 72(ra)
	-[0x80001480]:sw tp, 76(ra)
Current Store : [0x80001480] : sw tp, 76(ra) -- Store: [0x80003c60]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x12cb99 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x210a09 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x38af9a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000149c]:csrrs tp, fcsr, zero
	-[0x800014a0]:fsw ft11, 80(ra)
	-[0x800014a4]:sw tp, 84(ra)
Current Store : [0x800014a4] : sw tp, 84(ra) -- Store: [0x80003c68]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x12cb99 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x210a09 and fs3 == 0 and fe3 == 0x1a and fm3 == 0x38af9a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800014c0]:csrrs tp, fcsr, zero
	-[0x800014c4]:fsw ft11, 88(ra)
	-[0x800014c8]:sw tp, 92(ra)
Current Store : [0x800014c8] : sw tp, 92(ra) -- Store: [0x80003c70]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b0ecf and fs2 == 0 and fe2 == 0x7b and fm2 == 0x567a4f and fs3 == 0 and fe3 == 0x17 and fm3 == 0x2a1f65 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800014e4]:csrrs tp, fcsr, zero
	-[0x800014e8]:fsw ft11, 96(ra)
	-[0x800014ec]:sw tp, 100(ra)
Current Store : [0x800014ec] : sw tp, 100(ra) -- Store: [0x80003c78]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b0ecf and fs2 == 0 and fe2 == 0x7b and fm2 == 0x567a4f and fs3 == 0 and fe3 == 0x17 and fm3 == 0x2a1f65 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001508]:csrrs tp, fcsr, zero
	-[0x8000150c]:fsw ft11, 104(ra)
	-[0x80001510]:sw tp, 108(ra)
Current Store : [0x80001510] : sw tp, 108(ra) -- Store: [0x80003c80]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b0ecf and fs2 == 0 and fe2 == 0x7b and fm2 == 0x567a4f and fs3 == 0 and fe3 == 0x17 and fm3 == 0x2a1f65 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000152c]:csrrs tp, fcsr, zero
	-[0x80001530]:fsw ft11, 112(ra)
	-[0x80001534]:sw tp, 116(ra)
Current Store : [0x80001534] : sw tp, 116(ra) -- Store: [0x80003c88]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x4b0ecf and fs2 == 0 and fe2 == 0x7b and fm2 == 0x567a4f and fs3 == 0 and fe3 == 0x17 and fm3 == 0x2a1f65 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001550]:csrrs tp, fcsr, zero
	-[0x80001554]:fsw ft11, 120(ra)
	-[0x80001558]:sw tp, 124(ra)
Current Store : [0x80001558] : sw tp, 124(ra) -- Store: [0x80003c90]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x08be86 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x7e6538 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x07e31a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001574]:csrrs tp, fcsr, zero
	-[0x80001578]:fsw ft11, 128(ra)
	-[0x8000157c]:sw tp, 132(ra)
Current Store : [0x8000157c] : sw tp, 132(ra) -- Store: [0x80003c98]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x08be86 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x7e6538 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x07e31a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001598]:csrrs tp, fcsr, zero
	-[0x8000159c]:fsw ft11, 136(ra)
	-[0x800015a0]:sw tp, 140(ra)
Current Store : [0x800015a0] : sw tp, 140(ra) -- Store: [0x80003ca0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x08be86 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x7e6538 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x07e31a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800015bc]:csrrs tp, fcsr, zero
	-[0x800015c0]:fsw ft11, 144(ra)
	-[0x800015c4]:sw tp, 148(ra)
Current Store : [0x800015c4] : sw tp, 148(ra) -- Store: [0x80003ca8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x08be86 and fs2 == 0 and fe2 == 0x84 and fm2 == 0x7e6538 and fs3 == 0 and fe3 == 0x1b and fm3 == 0x07e31a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800015e0]:csrrs tp, fcsr, zero
	-[0x800015e4]:fsw ft11, 152(ra)
	-[0x800015e8]:sw tp, 156(ra)
Current Store : [0x800015e8] : sw tp, 156(ra) -- Store: [0x80003cb0]:0x00000083





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
