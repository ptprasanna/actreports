
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002bf0')]      |
| SIG_REGION                | [('0x80004c10', '0x80005340', '460 words')]      |
| COV_LABELS                | fnmadd_b5      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV32F-fnmaddall-nov14/fnmadd_b5-01.S/ref.S    |
| Total Number of coverpoints| 361     |
| Total Coverpoints Hit     | 361      |
| Total Signature Updates   | 456      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 228     |
| STAT4                     | 228     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000128]:fnmadd.s ft11, ft10, ft9, ft10, dyn
[0x8000012c]:csrrs tp, fcsr, zero
[0x80000130]:fsw ft11, 0(ra)
[0x80000134]:sw tp, 4(ra)
[0x80000138]:flw ft11, 12(gp)
[0x8000013c]:flw ft8, 16(gp)
[0x80000140]:flw ft8, 20(gp)
[0x80000144]:addi sp, zero, 34
[0x80000148]:csrrw zero, fcsr, sp
[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn

[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn
[0x80000150]:csrrs tp, fcsr, zero
[0x80000154]:fsw ft8, 8(ra)
[0x80000158]:sw tp, 12(ra)
[0x8000015c]:flw fs11, 24(gp)
[0x80000160]:flw fs11, 28(gp)
[0x80000164]:flw ft11, 32(gp)
[0x80000168]:addi sp, zero, 66
[0x8000016c]:csrrw zero, fcsr, sp
[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn

[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn
[0x80000174]:csrrs tp, fcsr, zero
[0x80000178]:fsw fs11, 16(ra)
[0x8000017c]:sw tp, 20(ra)
[0x80000180]:flw ft9, 36(gp)
[0x80000184]:flw fs10, 40(gp)
[0x80000188]:flw fs10, 44(gp)
[0x8000018c]:addi sp, zero, 98
[0x80000190]:csrrw zero, fcsr, sp
[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn

[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn
[0x80000198]:csrrs tp, fcsr, zero
[0x8000019c]:fsw ft10, 24(ra)
[0x800001a0]:sw tp, 28(ra)
[0x800001a4]:flw fs10, 48(gp)
[0x800001a8]:flw ft11, 52(gp)
[0x800001ac]:flw ft9, 56(gp)
[0x800001b0]:addi sp, zero, 130
[0x800001b4]:csrrw zero, fcsr, sp
[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn

[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn
[0x800001bc]:csrrs tp, fcsr, zero
[0x800001c0]:fsw fs10, 32(ra)
[0x800001c4]:sw tp, 36(ra)
[0x800001c8]:flw ft8, 60(gp)
[0x800001cc]:flw ft10, 64(gp)
[0x800001d0]:flw fs9, 68(gp)
[0x800001d4]:addi sp, zero, 2
[0x800001d8]:csrrw zero, fcsr, sp
[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn

[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn
[0x800001e0]:csrrs tp, fcsr, zero
[0x800001e4]:fsw fs9, 40(ra)
[0x800001e8]:sw tp, 44(ra)
[0x800001ec]:flw fs8, 72(gp)
[0x800001f0]:flw fs8, 76(gp)
[0x800001f4]:flw fs8, 80(gp)
[0x800001f8]:addi sp, zero, 34
[0x800001fc]:csrrw zero, fcsr, sp
[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn

[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn
[0x80000204]:csrrs tp, fcsr, zero
[0x80000208]:fsw fs8, 48(ra)
[0x8000020c]:sw tp, 52(ra)
[0x80000210]:flw fs7, 84(gp)
[0x80000214]:flw fs9, 88(gp)
[0x80000218]:flw fs7, 92(gp)
[0x8000021c]:addi sp, zero, 66
[0x80000220]:csrrw zero, fcsr, sp
[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn

[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn
[0x80000228]:csrrs tp, fcsr, zero
[0x8000022c]:fsw fs7, 56(ra)
[0x80000230]:sw tp, 60(ra)
[0x80000234]:flw fs6, 96(gp)
[0x80000238]:flw fs6, 100(gp)
[0x8000023c]:flw fs11, 104(gp)
[0x80000240]:addi sp, zero, 98
[0x80000244]:csrrw zero, fcsr, sp
[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn

[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn
[0x8000024c]:csrrs tp, fcsr, zero
[0x80000250]:fsw ft9, 64(ra)
[0x80000254]:sw tp, 68(ra)
[0x80000258]:flw fs5, 108(gp)
[0x8000025c]:flw fs5, 112(gp)
[0x80000260]:flw fs5, 116(gp)
[0x80000264]:addi sp, zero, 130
[0x80000268]:csrrw zero, fcsr, sp
[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn

[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn
[0x80000270]:csrrs tp, fcsr, zero
[0x80000274]:fsw fs6, 72(ra)
[0x80000278]:sw tp, 76(ra)
[0x8000027c]:flw fs9, 120(gp)
[0x80000280]:flw fs4, 124(gp)
[0x80000284]:flw fs6, 128(gp)
[0x80000288]:addi sp, zero, 2
[0x8000028c]:csrrw zero, fcsr, sp
[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn

[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn
[0x80000294]:csrrs tp, fcsr, zero
[0x80000298]:fsw fs4, 80(ra)
[0x8000029c]:sw tp, 84(ra)
[0x800002a0]:flw fs4, 132(gp)
[0x800002a4]:flw fs7, 136(gp)
[0x800002a8]:flw fs3, 140(gp)
[0x800002ac]:addi sp, zero, 34
[0x800002b0]:csrrw zero, fcsr, sp
[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn

[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn
[0x800002b8]:csrrs tp, fcsr, zero
[0x800002bc]:fsw fs5, 88(ra)
[0x800002c0]:sw tp, 92(ra)
[0x800002c4]:flw fs2, 144(gp)
[0x800002c8]:flw fa7, 148(gp)
[0x800002cc]:flw fs4, 152(gp)
[0x800002d0]:addi sp, zero, 66
[0x800002d4]:csrrw zero, fcsr, sp
[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn

[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn
[0x800002dc]:csrrs tp, fcsr, zero
[0x800002e0]:fsw fs3, 96(ra)
[0x800002e4]:sw tp, 100(ra)
[0x800002e8]:flw fa7, 156(gp)
[0x800002ec]:flw fs3, 160(gp)
[0x800002f0]:flw fa6, 164(gp)
[0x800002f4]:addi sp, zero, 98
[0x800002f8]:csrrw zero, fcsr, sp
[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn

[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn
[0x80000300]:csrrs tp, fcsr, zero
[0x80000304]:fsw fs2, 104(ra)
[0x80000308]:sw tp, 108(ra)
[0x8000030c]:flw fs3, 168(gp)
[0x80000310]:flw fa6, 172(gp)
[0x80000314]:flw fs2, 176(gp)
[0x80000318]:addi sp, zero, 130
[0x8000031c]:csrrw zero, fcsr, sp
[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn

[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn
[0x80000324]:csrrs tp, fcsr, zero
[0x80000328]:fsw fa7, 112(ra)
[0x8000032c]:sw tp, 116(ra)
[0x80000330]:flw fa5, 180(gp)
[0x80000334]:flw fs2, 184(gp)
[0x80000338]:flw fa7, 188(gp)
[0x8000033c]:addi sp, zero, 2
[0x80000340]:csrrw zero, fcsr, sp
[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn

[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn
[0x80000348]:csrrs tp, fcsr, zero
[0x8000034c]:fsw fa6, 120(ra)
[0x80000350]:sw tp, 124(ra)
[0x80000354]:flw fa6, 192(gp)
[0x80000358]:flw fa4, 196(gp)
[0x8000035c]:flw fa3, 200(gp)
[0x80000360]:addi sp, zero, 34
[0x80000364]:csrrw zero, fcsr, sp
[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn

[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn
[0x8000036c]:csrrs tp, fcsr, zero
[0x80000370]:fsw fa5, 128(ra)
[0x80000374]:sw tp, 132(ra)
[0x80000378]:flw fa3, 204(gp)
[0x8000037c]:flw fa5, 208(gp)
[0x80000380]:flw fa2, 212(gp)
[0x80000384]:addi sp, zero, 66
[0x80000388]:csrrw zero, fcsr, sp
[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn

[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn
[0x80000390]:csrrs tp, fcsr, zero
[0x80000394]:fsw fa4, 136(ra)
[0x80000398]:sw tp, 140(ra)
[0x8000039c]:flw fa4, 216(gp)
[0x800003a0]:flw fa2, 220(gp)
[0x800003a4]:flw fa5, 224(gp)
[0x800003a8]:addi sp, zero, 98
[0x800003ac]:csrrw zero, fcsr, sp
[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn

[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn
[0x800003b4]:csrrs tp, fcsr, zero
[0x800003b8]:fsw fa3, 144(ra)
[0x800003bc]:sw tp, 148(ra)
[0x800003c0]:flw fa1, 228(gp)
[0x800003c4]:flw fa3, 232(gp)
[0x800003c8]:flw fa4, 236(gp)
[0x800003cc]:addi sp, zero, 130
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn

[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:fsw fa2, 152(ra)
[0x800003e0]:sw tp, 156(ra)
[0x800003e4]:flw fa2, 240(gp)
[0x800003e8]:flw fa0, 244(gp)
[0x800003ec]:flw fs1, 248(gp)
[0x800003f0]:addi sp, zero, 2
[0x800003f4]:csrrw zero, fcsr, sp
[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn

[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn
[0x800003fc]:csrrs tp, fcsr, zero
[0x80000400]:fsw fa1, 160(ra)
[0x80000404]:sw tp, 164(ra)
[0x80000408]:flw fs1, 252(gp)
[0x8000040c]:flw fa1, 256(gp)
[0x80000410]:flw fs0, 260(gp)
[0x80000414]:addi sp, zero, 34
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn

[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:fsw fa0, 168(ra)
[0x80000428]:sw tp, 172(ra)
[0x8000042c]:flw fa0, 264(gp)
[0x80000430]:flw fs0, 268(gp)
[0x80000434]:flw fa1, 272(gp)
[0x80000438]:addi sp, zero, 66
[0x8000043c]:csrrw zero, fcsr, sp
[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn

[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn
[0x80000444]:csrrs tp, fcsr, zero
[0x80000448]:fsw fs1, 176(ra)
[0x8000044c]:sw tp, 180(ra)
[0x80000450]:flw ft7, 276(gp)
[0x80000454]:flw fs1, 280(gp)
[0x80000458]:flw fa0, 284(gp)
[0x8000045c]:addi sp, zero, 98
[0x80000460]:csrrw zero, fcsr, sp
[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn

[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn
[0x80000468]:csrrs tp, fcsr, zero
[0x8000046c]:fsw fs0, 184(ra)
[0x80000470]:sw tp, 188(ra)
[0x80000474]:flw fs0, 288(gp)
[0x80000478]:flw ft6, 292(gp)
[0x8000047c]:flw ft5, 296(gp)
[0x80000480]:addi sp, zero, 130
[0x80000484]:csrrw zero, fcsr, sp
[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn

[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn
[0x8000048c]:csrrs tp, fcsr, zero
[0x80000490]:fsw ft7, 192(ra)
[0x80000494]:sw tp, 196(ra)
[0x80000498]:flw ft5, 300(gp)
[0x8000049c]:flw ft7, 304(gp)
[0x800004a0]:flw ft4, 308(gp)
[0x800004a4]:addi sp, zero, 2
[0x800004a8]:csrrw zero, fcsr, sp
[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn

[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn
[0x800004b0]:csrrs tp, fcsr, zero
[0x800004b4]:fsw ft6, 200(ra)
[0x800004b8]:sw tp, 204(ra)
[0x800004bc]:flw ft6, 312(gp)
[0x800004c0]:flw ft4, 316(gp)
[0x800004c4]:flw ft7, 320(gp)
[0x800004c8]:addi sp, zero, 34
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn

[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:fsw ft5, 208(ra)
[0x800004dc]:sw tp, 212(ra)
[0x800004e0]:flw ft3, 324(gp)
[0x800004e4]:flw ft5, 328(gp)
[0x800004e8]:flw ft6, 332(gp)
[0x800004ec]:addi sp, zero, 66
[0x800004f0]:csrrw zero, fcsr, sp
[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn

[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn
[0x800004f8]:csrrs tp, fcsr, zero
[0x800004fc]:fsw ft4, 216(ra)
[0x80000500]:sw tp, 220(ra)
[0x80000504]:flw ft4, 336(gp)
[0x80000508]:flw ft2, 340(gp)
[0x8000050c]:flw ft1, 344(gp)
[0x80000510]:addi sp, zero, 98
[0x80000514]:csrrw zero, fcsr, sp
[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn

[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn
[0x8000051c]:csrrs tp, fcsr, zero
[0x80000520]:fsw ft3, 224(ra)
[0x80000524]:sw tp, 228(ra)
[0x80000528]:flw ft1, 348(gp)
[0x8000052c]:flw ft3, 352(gp)
[0x80000530]:flw ft0, 356(gp)
[0x80000534]:addi sp, zero, 130
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn

[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:fsw ft2, 232(ra)
[0x80000548]:sw tp, 236(ra)
[0x8000054c]:flw ft2, 360(gp)
[0x80000550]:flw ft0, 364(gp)
[0x80000554]:flw ft3, 368(gp)
[0x80000558]:addi sp, zero, 2
[0x8000055c]:csrrw zero, fcsr, sp
[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn

[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn
[0x80000564]:csrrs tp, fcsr, zero
[0x80000568]:fsw ft1, 240(ra)
[0x8000056c]:sw tp, 244(ra)
[0x80000570]:flw ft0, 372(gp)
[0x80000574]:flw ft10, 376(gp)
[0x80000578]:flw ft9, 380(gp)
[0x8000057c]:addi sp, zero, 34
[0x80000580]:csrrw zero, fcsr, sp
[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn

[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn
[0x80000588]:csrrs tp, fcsr, zero
[0x8000058c]:fsw ft11, 248(ra)
[0x80000590]:sw tp, 252(ra)
[0x80000594]:flw ft10, 384(gp)
[0x80000598]:flw ft1, 388(gp)
[0x8000059c]:flw ft9, 392(gp)
[0x800005a0]:addi sp, zero, 66
[0x800005a4]:csrrw zero, fcsr, sp
[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn

[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn
[0x800005ac]:csrrs tp, fcsr, zero
[0x800005b0]:fsw ft11, 256(ra)
[0x800005b4]:sw tp, 260(ra)
[0x800005b8]:flw ft10, 396(gp)
[0x800005bc]:flw ft9, 400(gp)
[0x800005c0]:flw ft2, 404(gp)
[0x800005c4]:addi sp, zero, 98
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn

[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:fsw ft11, 264(ra)
[0x800005d8]:sw tp, 268(ra)
[0x800005dc]:flw ft11, 408(gp)
[0x800005e0]:flw ft10, 412(gp)
[0x800005e4]:flw ft9, 416(gp)
[0x800005e8]:addi sp, zero, 130
[0x800005ec]:csrrw zero, fcsr, sp
[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn

[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn
[0x800005f4]:csrrs tp, fcsr, zero
[0x800005f8]:fsw ft0, 272(ra)
[0x800005fc]:sw tp, 276(ra)
[0x80000600]:flw ft10, 420(gp)
[0x80000604]:flw ft9, 424(gp)
[0x80000608]:flw ft8, 428(gp)
[0x8000060c]:addi sp, zero, 2
[0x80000610]:csrrw zero, fcsr, sp
[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000618]:csrrs tp, fcsr, zero
[0x8000061c]:fsw ft11, 280(ra)
[0x80000620]:sw tp, 284(ra)
[0x80000624]:flw ft10, 432(gp)
[0x80000628]:flw ft9, 436(gp)
[0x8000062c]:flw ft8, 440(gp)
[0x80000630]:addi sp, zero, 34
[0x80000634]:csrrw zero, fcsr, sp
[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000063c]:csrrs tp, fcsr, zero
[0x80000640]:fsw ft11, 288(ra)
[0x80000644]:sw tp, 292(ra)
[0x80000648]:flw ft10, 444(gp)
[0x8000064c]:flw ft9, 448(gp)
[0x80000650]:flw ft8, 452(gp)
[0x80000654]:addi sp, zero, 66
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:fsw ft11, 296(ra)
[0x80000668]:sw tp, 300(ra)
[0x8000066c]:flw ft10, 456(gp)
[0x80000670]:flw ft9, 460(gp)
[0x80000674]:flw ft8, 464(gp)
[0x80000678]:addi sp, zero, 98
[0x8000067c]:csrrw zero, fcsr, sp
[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000684]:csrrs tp, fcsr, zero
[0x80000688]:fsw ft11, 304(ra)
[0x8000068c]:sw tp, 308(ra)
[0x80000690]:flw ft10, 468(gp)
[0x80000694]:flw ft9, 472(gp)
[0x80000698]:flw ft8, 476(gp)
[0x8000069c]:addi sp, zero, 130
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006a8]:csrrs tp, fcsr, zero
[0x800006ac]:fsw ft11, 312(ra)
[0x800006b0]:sw tp, 316(ra)
[0x800006b4]:flw ft10, 480(gp)
[0x800006b8]:flw ft9, 484(gp)
[0x800006bc]:flw ft8, 488(gp)
[0x800006c0]:addi sp, zero, 2
[0x800006c4]:csrrw zero, fcsr, sp
[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006cc]:csrrs tp, fcsr, zero
[0x800006d0]:fsw ft11, 320(ra)
[0x800006d4]:sw tp, 324(ra)
[0x800006d8]:flw ft10, 492(gp)
[0x800006dc]:flw ft9, 496(gp)
[0x800006e0]:flw ft8, 500(gp)
[0x800006e4]:addi sp, zero, 34
[0x800006e8]:csrrw zero, fcsr, sp
[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800006f0]:csrrs tp, fcsr, zero
[0x800006f4]:fsw ft11, 328(ra)
[0x800006f8]:sw tp, 332(ra)
[0x800006fc]:flw ft10, 504(gp)
[0x80000700]:flw ft9, 508(gp)
[0x80000704]:flw ft8, 512(gp)
[0x80000708]:addi sp, zero, 66
[0x8000070c]:csrrw zero, fcsr, sp
[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000714]:csrrs tp, fcsr, zero
[0x80000718]:fsw ft11, 336(ra)
[0x8000071c]:sw tp, 340(ra)
[0x80000720]:flw ft10, 516(gp)
[0x80000724]:flw ft9, 520(gp)
[0x80000728]:flw ft8, 524(gp)
[0x8000072c]:addi sp, zero, 98
[0x80000730]:csrrw zero, fcsr, sp
[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000738]:csrrs tp, fcsr, zero
[0x8000073c]:fsw ft11, 344(ra)
[0x80000740]:sw tp, 348(ra)
[0x80000744]:flw ft10, 528(gp)
[0x80000748]:flw ft9, 532(gp)
[0x8000074c]:flw ft8, 536(gp)
[0x80000750]:addi sp, zero, 130
[0x80000754]:csrrw zero, fcsr, sp
[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000075c]:csrrs tp, fcsr, zero
[0x80000760]:fsw ft11, 352(ra)
[0x80000764]:sw tp, 356(ra)
[0x80000768]:flw ft10, 540(gp)
[0x8000076c]:flw ft9, 544(gp)
[0x80000770]:flw ft8, 548(gp)
[0x80000774]:addi sp, zero, 2
[0x80000778]:csrrw zero, fcsr, sp
[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000780]:csrrs tp, fcsr, zero
[0x80000784]:fsw ft11, 360(ra)
[0x80000788]:sw tp, 364(ra)
[0x8000078c]:flw ft10, 552(gp)
[0x80000790]:flw ft9, 556(gp)
[0x80000794]:flw ft8, 560(gp)
[0x80000798]:addi sp, zero, 34
[0x8000079c]:csrrw zero, fcsr, sp
[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007a4]:csrrs tp, fcsr, zero
[0x800007a8]:fsw ft11, 368(ra)
[0x800007ac]:sw tp, 372(ra)
[0x800007b0]:flw ft10, 564(gp)
[0x800007b4]:flw ft9, 568(gp)
[0x800007b8]:flw ft8, 572(gp)
[0x800007bc]:addi sp, zero, 66
[0x800007c0]:csrrw zero, fcsr, sp
[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007c8]:csrrs tp, fcsr, zero
[0x800007cc]:fsw ft11, 376(ra)
[0x800007d0]:sw tp, 380(ra)
[0x800007d4]:flw ft10, 576(gp)
[0x800007d8]:flw ft9, 580(gp)
[0x800007dc]:flw ft8, 584(gp)
[0x800007e0]:addi sp, zero, 98
[0x800007e4]:csrrw zero, fcsr, sp
[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800007ec]:csrrs tp, fcsr, zero
[0x800007f0]:fsw ft11, 384(ra)
[0x800007f4]:sw tp, 388(ra)
[0x800007f8]:flw ft10, 588(gp)
[0x800007fc]:flw ft9, 592(gp)
[0x80000800]:flw ft8, 596(gp)
[0x80000804]:addi sp, zero, 130
[0x80000808]:csrrw zero, fcsr, sp
[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000810]:csrrs tp, fcsr, zero
[0x80000814]:fsw ft11, 392(ra)
[0x80000818]:sw tp, 396(ra)
[0x8000081c]:flw ft10, 600(gp)
[0x80000820]:flw ft9, 604(gp)
[0x80000824]:flw ft8, 608(gp)
[0x80000828]:addi sp, zero, 2
[0x8000082c]:csrrw zero, fcsr, sp
[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000834]:csrrs tp, fcsr, zero
[0x80000838]:fsw ft11, 400(ra)
[0x8000083c]:sw tp, 404(ra)
[0x80000840]:flw ft10, 612(gp)
[0x80000844]:flw ft9, 616(gp)
[0x80000848]:flw ft8, 620(gp)
[0x8000084c]:addi sp, zero, 34
[0x80000850]:csrrw zero, fcsr, sp
[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000858]:csrrs tp, fcsr, zero
[0x8000085c]:fsw ft11, 408(ra)
[0x80000860]:sw tp, 412(ra)
[0x80000864]:flw ft10, 624(gp)
[0x80000868]:flw ft9, 628(gp)
[0x8000086c]:flw ft8, 632(gp)
[0x80000870]:addi sp, zero, 66
[0x80000874]:csrrw zero, fcsr, sp
[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000087c]:csrrs tp, fcsr, zero
[0x80000880]:fsw ft11, 416(ra)
[0x80000884]:sw tp, 420(ra)
[0x80000888]:flw ft10, 636(gp)
[0x8000088c]:flw ft9, 640(gp)
[0x80000890]:flw ft8, 644(gp)
[0x80000894]:addi sp, zero, 98
[0x80000898]:csrrw zero, fcsr, sp
[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008a0]:csrrs tp, fcsr, zero
[0x800008a4]:fsw ft11, 424(ra)
[0x800008a8]:sw tp, 428(ra)
[0x800008ac]:flw ft10, 648(gp)
[0x800008b0]:flw ft9, 652(gp)
[0x800008b4]:flw ft8, 656(gp)
[0x800008b8]:addi sp, zero, 130
[0x800008bc]:csrrw zero, fcsr, sp
[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008c4]:csrrs tp, fcsr, zero
[0x800008c8]:fsw ft11, 432(ra)
[0x800008cc]:sw tp, 436(ra)
[0x800008d0]:flw ft10, 660(gp)
[0x800008d4]:flw ft9, 664(gp)
[0x800008d8]:flw ft8, 668(gp)
[0x800008dc]:addi sp, zero, 2
[0x800008e0]:csrrw zero, fcsr, sp
[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800008e8]:csrrs tp, fcsr, zero
[0x800008ec]:fsw ft11, 440(ra)
[0x800008f0]:sw tp, 444(ra)
[0x800008f4]:flw ft10, 672(gp)
[0x800008f8]:flw ft9, 676(gp)
[0x800008fc]:flw ft8, 680(gp)
[0x80000900]:addi sp, zero, 34
[0x80000904]:csrrw zero, fcsr, sp
[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000090c]:csrrs tp, fcsr, zero
[0x80000910]:fsw ft11, 448(ra)
[0x80000914]:sw tp, 452(ra)
[0x80000918]:flw ft10, 684(gp)
[0x8000091c]:flw ft9, 688(gp)
[0x80000920]:flw ft8, 692(gp)
[0x80000924]:addi sp, zero, 66
[0x80000928]:csrrw zero, fcsr, sp
[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000930]:csrrs tp, fcsr, zero
[0x80000934]:fsw ft11, 456(ra)
[0x80000938]:sw tp, 460(ra)
[0x8000093c]:flw ft10, 696(gp)
[0x80000940]:flw ft9, 700(gp)
[0x80000944]:flw ft8, 704(gp)
[0x80000948]:addi sp, zero, 98
[0x8000094c]:csrrw zero, fcsr, sp
[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000954]:csrrs tp, fcsr, zero
[0x80000958]:fsw ft11, 464(ra)
[0x8000095c]:sw tp, 468(ra)
[0x80000960]:flw ft10, 708(gp)
[0x80000964]:flw ft9, 712(gp)
[0x80000968]:flw ft8, 716(gp)
[0x8000096c]:addi sp, zero, 130
[0x80000970]:csrrw zero, fcsr, sp
[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000978]:csrrs tp, fcsr, zero
[0x8000097c]:fsw ft11, 472(ra)
[0x80000980]:sw tp, 476(ra)
[0x80000984]:flw ft10, 720(gp)
[0x80000988]:flw ft9, 724(gp)
[0x8000098c]:flw ft8, 728(gp)
[0x80000990]:addi sp, zero, 2
[0x80000994]:csrrw zero, fcsr, sp
[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000099c]:csrrs tp, fcsr, zero
[0x800009a0]:fsw ft11, 480(ra)
[0x800009a4]:sw tp, 484(ra)
[0x800009a8]:flw ft10, 732(gp)
[0x800009ac]:flw ft9, 736(gp)
[0x800009b0]:flw ft8, 740(gp)
[0x800009b4]:addi sp, zero, 34
[0x800009b8]:csrrw zero, fcsr, sp
[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800009c0]:csrrs tp, fcsr, zero
[0x800009c4]:fsw ft11, 488(ra)
[0x800009c8]:sw tp, 492(ra)
[0x800009cc]:flw ft10, 744(gp)
[0x800009d0]:flw ft9, 748(gp)
[0x800009d4]:flw ft8, 752(gp)
[0x800009d8]:addi sp, zero, 66
[0x800009dc]:csrrw zero, fcsr, sp
[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800009e4]:csrrs tp, fcsr, zero
[0x800009e8]:fsw ft11, 496(ra)
[0x800009ec]:sw tp, 500(ra)
[0x800009f0]:flw ft10, 756(gp)
[0x800009f4]:flw ft9, 760(gp)
[0x800009f8]:flw ft8, 764(gp)
[0x800009fc]:addi sp, zero, 98
[0x80000a00]:csrrw zero, fcsr, sp
[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a08]:csrrs tp, fcsr, zero
[0x80000a0c]:fsw ft11, 504(ra)
[0x80000a10]:sw tp, 508(ra)
[0x80000a14]:flw ft10, 768(gp)
[0x80000a18]:flw ft9, 772(gp)
[0x80000a1c]:flw ft8, 776(gp)
[0x80000a20]:addi sp, zero, 130
[0x80000a24]:csrrw zero, fcsr, sp
[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a2c]:csrrs tp, fcsr, zero
[0x80000a30]:fsw ft11, 512(ra)
[0x80000a34]:sw tp, 516(ra)
[0x80000a38]:flw ft10, 780(gp)
[0x80000a3c]:flw ft9, 784(gp)
[0x80000a40]:flw ft8, 788(gp)
[0x80000a44]:addi sp, zero, 2
[0x80000a48]:csrrw zero, fcsr, sp
[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a50]:csrrs tp, fcsr, zero
[0x80000a54]:fsw ft11, 520(ra)
[0x80000a58]:sw tp, 524(ra)
[0x80000a5c]:flw ft10, 792(gp)
[0x80000a60]:flw ft9, 796(gp)
[0x80000a64]:flw ft8, 800(gp)
[0x80000a68]:addi sp, zero, 34
[0x80000a6c]:csrrw zero, fcsr, sp
[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a74]:csrrs tp, fcsr, zero
[0x80000a78]:fsw ft11, 528(ra)
[0x80000a7c]:sw tp, 532(ra)
[0x80000a80]:flw ft10, 804(gp)
[0x80000a84]:flw ft9, 808(gp)
[0x80000a88]:flw ft8, 812(gp)
[0x80000a8c]:addi sp, zero, 66
[0x80000a90]:csrrw zero, fcsr, sp
[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000a98]:csrrs tp, fcsr, zero
[0x80000a9c]:fsw ft11, 536(ra)
[0x80000aa0]:sw tp, 540(ra)
[0x80000aa4]:flw ft10, 816(gp)
[0x80000aa8]:flw ft9, 820(gp)
[0x80000aac]:flw ft8, 824(gp)
[0x80000ab0]:addi sp, zero, 98
[0x80000ab4]:csrrw zero, fcsr, sp
[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000abc]:csrrs tp, fcsr, zero
[0x80000ac0]:fsw ft11, 544(ra)
[0x80000ac4]:sw tp, 548(ra)
[0x80000ac8]:flw ft10, 828(gp)
[0x80000acc]:flw ft9, 832(gp)
[0x80000ad0]:flw ft8, 836(gp)
[0x80000ad4]:addi sp, zero, 130
[0x80000ad8]:csrrw zero, fcsr, sp
[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ae0]:csrrs tp, fcsr, zero
[0x80000ae4]:fsw ft11, 552(ra)
[0x80000ae8]:sw tp, 556(ra)
[0x80000aec]:flw ft10, 840(gp)
[0x80000af0]:flw ft9, 844(gp)
[0x80000af4]:flw ft8, 848(gp)
[0x80000af8]:addi sp, zero, 2
[0x80000afc]:csrrw zero, fcsr, sp
[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b04]:csrrs tp, fcsr, zero
[0x80000b08]:fsw ft11, 560(ra)
[0x80000b0c]:sw tp, 564(ra)
[0x80000b10]:flw ft10, 852(gp)
[0x80000b14]:flw ft9, 856(gp)
[0x80000b18]:flw ft8, 860(gp)
[0x80000b1c]:addi sp, zero, 34
[0x80000b20]:csrrw zero, fcsr, sp
[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b28]:csrrs tp, fcsr, zero
[0x80000b2c]:fsw ft11, 568(ra)
[0x80000b30]:sw tp, 572(ra)
[0x80000b34]:flw ft10, 864(gp)
[0x80000b38]:flw ft9, 868(gp)
[0x80000b3c]:flw ft8, 872(gp)
[0x80000b40]:addi sp, zero, 66
[0x80000b44]:csrrw zero, fcsr, sp
[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b4c]:csrrs tp, fcsr, zero
[0x80000b50]:fsw ft11, 576(ra)
[0x80000b54]:sw tp, 580(ra)
[0x80000b58]:flw ft10, 876(gp)
[0x80000b5c]:flw ft9, 880(gp)
[0x80000b60]:flw ft8, 884(gp)
[0x80000b64]:addi sp, zero, 98
[0x80000b68]:csrrw zero, fcsr, sp
[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b70]:csrrs tp, fcsr, zero
[0x80000b74]:fsw ft11, 584(ra)
[0x80000b78]:sw tp, 588(ra)
[0x80000b7c]:flw ft10, 888(gp)
[0x80000b80]:flw ft9, 892(gp)
[0x80000b84]:flw ft8, 896(gp)
[0x80000b88]:addi sp, zero, 130
[0x80000b8c]:csrrw zero, fcsr, sp
[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000b94]:csrrs tp, fcsr, zero
[0x80000b98]:fsw ft11, 592(ra)
[0x80000b9c]:sw tp, 596(ra)
[0x80000ba0]:flw ft10, 900(gp)
[0x80000ba4]:flw ft9, 904(gp)
[0x80000ba8]:flw ft8, 908(gp)
[0x80000bac]:addi sp, zero, 2
[0x80000bb0]:csrrw zero, fcsr, sp
[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000bb8]:csrrs tp, fcsr, zero
[0x80000bbc]:fsw ft11, 600(ra)
[0x80000bc0]:sw tp, 604(ra)
[0x80000bc4]:flw ft10, 912(gp)
[0x80000bc8]:flw ft9, 916(gp)
[0x80000bcc]:flw ft8, 920(gp)
[0x80000bd0]:addi sp, zero, 34
[0x80000bd4]:csrrw zero, fcsr, sp
[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000bdc]:csrrs tp, fcsr, zero
[0x80000be0]:fsw ft11, 608(ra)
[0x80000be4]:sw tp, 612(ra)
[0x80000be8]:flw ft10, 924(gp)
[0x80000bec]:flw ft9, 928(gp)
[0x80000bf0]:flw ft8, 932(gp)
[0x80000bf4]:addi sp, zero, 66
[0x80000bf8]:csrrw zero, fcsr, sp
[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c00]:csrrs tp, fcsr, zero
[0x80000c04]:fsw ft11, 616(ra)
[0x80000c08]:sw tp, 620(ra)
[0x80000c0c]:flw ft10, 936(gp)
[0x80000c10]:flw ft9, 940(gp)
[0x80000c14]:flw ft8, 944(gp)
[0x80000c18]:addi sp, zero, 98
[0x80000c1c]:csrrw zero, fcsr, sp
[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c24]:csrrs tp, fcsr, zero
[0x80000c28]:fsw ft11, 624(ra)
[0x80000c2c]:sw tp, 628(ra)
[0x80000c30]:flw ft10, 948(gp)
[0x80000c34]:flw ft9, 952(gp)
[0x80000c38]:flw ft8, 956(gp)
[0x80000c3c]:addi sp, zero, 130
[0x80000c40]:csrrw zero, fcsr, sp
[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c48]:csrrs tp, fcsr, zero
[0x80000c4c]:fsw ft11, 632(ra)
[0x80000c50]:sw tp, 636(ra)
[0x80000c54]:flw ft10, 960(gp)
[0x80000c58]:flw ft9, 964(gp)
[0x80000c5c]:flw ft8, 968(gp)
[0x80000c60]:addi sp, zero, 2
[0x80000c64]:csrrw zero, fcsr, sp
[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c6c]:csrrs tp, fcsr, zero
[0x80000c70]:fsw ft11, 640(ra)
[0x80000c74]:sw tp, 644(ra)
[0x80000c78]:flw ft10, 972(gp)
[0x80000c7c]:flw ft9, 976(gp)
[0x80000c80]:flw ft8, 980(gp)
[0x80000c84]:addi sp, zero, 34
[0x80000c88]:csrrw zero, fcsr, sp
[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000c90]:csrrs tp, fcsr, zero
[0x80000c94]:fsw ft11, 648(ra)
[0x80000c98]:sw tp, 652(ra)
[0x80000c9c]:flw ft10, 984(gp)
[0x80000ca0]:flw ft9, 988(gp)
[0x80000ca4]:flw ft8, 992(gp)
[0x80000ca8]:addi sp, zero, 66
[0x80000cac]:csrrw zero, fcsr, sp
[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cb4]:csrrs tp, fcsr, zero
[0x80000cb8]:fsw ft11, 656(ra)
[0x80000cbc]:sw tp, 660(ra)
[0x80000cc0]:flw ft10, 996(gp)
[0x80000cc4]:flw ft9, 1000(gp)
[0x80000cc8]:flw ft8, 1004(gp)
[0x80000ccc]:addi sp, zero, 98
[0x80000cd0]:csrrw zero, fcsr, sp
[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cd8]:csrrs tp, fcsr, zero
[0x80000cdc]:fsw ft11, 664(ra)
[0x80000ce0]:sw tp, 668(ra)
[0x80000ce4]:flw ft10, 1008(gp)
[0x80000ce8]:flw ft9, 1012(gp)
[0x80000cec]:flw ft8, 1016(gp)
[0x80000cf0]:addi sp, zero, 130
[0x80000cf4]:csrrw zero, fcsr, sp
[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000cfc]:csrrs tp, fcsr, zero
[0x80000d00]:fsw ft11, 672(ra)
[0x80000d04]:sw tp, 676(ra)
[0x80000d08]:flw ft10, 1020(gp)
[0x80000d0c]:flw ft9, 1024(gp)
[0x80000d10]:flw ft8, 1028(gp)
[0x80000d14]:addi sp, zero, 2
[0x80000d18]:csrrw zero, fcsr, sp
[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d20]:csrrs tp, fcsr, zero
[0x80000d24]:fsw ft11, 680(ra)
[0x80000d28]:sw tp, 684(ra)
[0x80000d2c]:flw ft10, 1032(gp)
[0x80000d30]:flw ft9, 1036(gp)
[0x80000d34]:flw ft8, 1040(gp)
[0x80000d38]:addi sp, zero, 34
[0x80000d3c]:csrrw zero, fcsr, sp
[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d44]:csrrs tp, fcsr, zero
[0x80000d48]:fsw ft11, 688(ra)
[0x80000d4c]:sw tp, 692(ra)
[0x80000d50]:flw ft10, 1044(gp)
[0x80000d54]:flw ft9, 1048(gp)
[0x80000d58]:flw ft8, 1052(gp)
[0x80000d5c]:addi sp, zero, 66
[0x80000d60]:csrrw zero, fcsr, sp
[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d68]:csrrs tp, fcsr, zero
[0x80000d6c]:fsw ft11, 696(ra)
[0x80000d70]:sw tp, 700(ra)
[0x80000d74]:flw ft10, 1056(gp)
[0x80000d78]:flw ft9, 1060(gp)
[0x80000d7c]:flw ft8, 1064(gp)
[0x80000d80]:addi sp, zero, 98
[0x80000d84]:csrrw zero, fcsr, sp
[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000d8c]:csrrs tp, fcsr, zero
[0x80000d90]:fsw ft11, 704(ra)
[0x80000d94]:sw tp, 708(ra)
[0x80000d98]:flw ft10, 1068(gp)
[0x80000d9c]:flw ft9, 1072(gp)
[0x80000da0]:flw ft8, 1076(gp)
[0x80000da4]:addi sp, zero, 130
[0x80000da8]:csrrw zero, fcsr, sp
[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000db0]:csrrs tp, fcsr, zero
[0x80000db4]:fsw ft11, 712(ra)
[0x80000db8]:sw tp, 716(ra)
[0x80000dbc]:flw ft10, 1080(gp)
[0x80000dc0]:flw ft9, 1084(gp)
[0x80000dc4]:flw ft8, 1088(gp)
[0x80000dc8]:addi sp, zero, 2
[0x80000dcc]:csrrw zero, fcsr, sp
[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000dd4]:csrrs tp, fcsr, zero
[0x80000dd8]:fsw ft11, 720(ra)
[0x80000ddc]:sw tp, 724(ra)
[0x80000de0]:flw ft10, 1092(gp)
[0x80000de4]:flw ft9, 1096(gp)
[0x80000de8]:flw ft8, 1100(gp)
[0x80000dec]:addi sp, zero, 34
[0x80000df0]:csrrw zero, fcsr, sp
[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000df8]:csrrs tp, fcsr, zero
[0x80000dfc]:fsw ft11, 728(ra)
[0x80000e00]:sw tp, 732(ra)
[0x80000e04]:flw ft10, 1104(gp)
[0x80000e08]:flw ft9, 1108(gp)
[0x80000e0c]:flw ft8, 1112(gp)
[0x80000e10]:addi sp, zero, 66
[0x80000e14]:csrrw zero, fcsr, sp
[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e1c]:csrrs tp, fcsr, zero
[0x80000e20]:fsw ft11, 736(ra)
[0x80000e24]:sw tp, 740(ra)
[0x80000e28]:flw ft10, 1116(gp)
[0x80000e2c]:flw ft9, 1120(gp)
[0x80000e30]:flw ft8, 1124(gp)
[0x80000e34]:addi sp, zero, 98
[0x80000e38]:csrrw zero, fcsr, sp
[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e40]:csrrs tp, fcsr, zero
[0x80000e44]:fsw ft11, 744(ra)
[0x80000e48]:sw tp, 748(ra)
[0x80000e4c]:flw ft10, 1128(gp)
[0x80000e50]:flw ft9, 1132(gp)
[0x80000e54]:flw ft8, 1136(gp)
[0x80000e58]:addi sp, zero, 130
[0x80000e5c]:csrrw zero, fcsr, sp
[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e64]:csrrs tp, fcsr, zero
[0x80000e68]:fsw ft11, 752(ra)
[0x80000e6c]:sw tp, 756(ra)
[0x80000e70]:flw ft10, 1140(gp)
[0x80000e74]:flw ft9, 1144(gp)
[0x80000e78]:flw ft8, 1148(gp)
[0x80000e7c]:addi sp, zero, 2
[0x80000e80]:csrrw zero, fcsr, sp
[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000e88]:csrrs tp, fcsr, zero
[0x80000e8c]:fsw ft11, 760(ra)
[0x80000e90]:sw tp, 764(ra)
[0x80000e94]:flw ft10, 1152(gp)
[0x80000e98]:flw ft9, 1156(gp)
[0x80000e9c]:flw ft8, 1160(gp)
[0x80000ea0]:addi sp, zero, 34
[0x80000ea4]:csrrw zero, fcsr, sp
[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000eac]:csrrs tp, fcsr, zero
[0x80000eb0]:fsw ft11, 768(ra)
[0x80000eb4]:sw tp, 772(ra)
[0x80000eb8]:flw ft10, 1164(gp)
[0x80000ebc]:flw ft9, 1168(gp)
[0x80000ec0]:flw ft8, 1172(gp)
[0x80000ec4]:addi sp, zero, 66
[0x80000ec8]:csrrw zero, fcsr, sp
[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ed0]:csrrs tp, fcsr, zero
[0x80000ed4]:fsw ft11, 776(ra)
[0x80000ed8]:sw tp, 780(ra)
[0x80000edc]:flw ft10, 1176(gp)
[0x80000ee0]:flw ft9, 1180(gp)
[0x80000ee4]:flw ft8, 1184(gp)
[0x80000ee8]:addi sp, zero, 98
[0x80000eec]:csrrw zero, fcsr, sp
[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ef4]:csrrs tp, fcsr, zero
[0x80000ef8]:fsw ft11, 784(ra)
[0x80000efc]:sw tp, 788(ra)
[0x80000f00]:flw ft10, 1188(gp)
[0x80000f04]:flw ft9, 1192(gp)
[0x80000f08]:flw ft8, 1196(gp)
[0x80000f0c]:addi sp, zero, 130
[0x80000f10]:csrrw zero, fcsr, sp
[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f18]:csrrs tp, fcsr, zero
[0x80000f1c]:fsw ft11, 792(ra)
[0x80000f20]:sw tp, 796(ra)
[0x80000f24]:flw ft10, 1200(gp)
[0x80000f28]:flw ft9, 1204(gp)
[0x80000f2c]:flw ft8, 1208(gp)
[0x80000f30]:addi sp, zero, 2
[0x80000f34]:csrrw zero, fcsr, sp
[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f3c]:csrrs tp, fcsr, zero
[0x80000f40]:fsw ft11, 800(ra)
[0x80000f44]:sw tp, 804(ra)
[0x80000f48]:flw ft10, 1212(gp)
[0x80000f4c]:flw ft9, 1216(gp)
[0x80000f50]:flw ft8, 1220(gp)
[0x80000f54]:addi sp, zero, 34
[0x80000f58]:csrrw zero, fcsr, sp
[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f60]:csrrs tp, fcsr, zero
[0x80000f64]:fsw ft11, 808(ra)
[0x80000f68]:sw tp, 812(ra)
[0x80000f6c]:flw ft10, 1224(gp)
[0x80000f70]:flw ft9, 1228(gp)
[0x80000f74]:flw ft8, 1232(gp)
[0x80000f78]:addi sp, zero, 66
[0x80000f7c]:csrrw zero, fcsr, sp
[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000f84]:csrrs tp, fcsr, zero
[0x80000f88]:fsw ft11, 816(ra)
[0x80000f8c]:sw tp, 820(ra)
[0x80000f90]:flw ft10, 1236(gp)
[0x80000f94]:flw ft9, 1240(gp)
[0x80000f98]:flw ft8, 1244(gp)
[0x80000f9c]:addi sp, zero, 98
[0x80000fa0]:csrrw zero, fcsr, sp
[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000fa8]:csrrs tp, fcsr, zero
[0x80000fac]:fsw ft11, 824(ra)
[0x80000fb0]:sw tp, 828(ra)
[0x80000fb4]:flw ft10, 1248(gp)
[0x80000fb8]:flw ft9, 1252(gp)
[0x80000fbc]:flw ft8, 1256(gp)
[0x80000fc0]:addi sp, zero, 130
[0x80000fc4]:csrrw zero, fcsr, sp
[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs tp, fcsr, zero
[0x80000fd0]:fsw ft11, 832(ra)
[0x80000fd4]:sw tp, 836(ra)
[0x80000fd8]:flw ft10, 1260(gp)
[0x80000fdc]:flw ft9, 1264(gp)
[0x80000fe0]:flw ft8, 1268(gp)
[0x80000fe4]:addi sp, zero, 2
[0x80000fe8]:csrrw zero, fcsr, sp
[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80000ff0]:csrrs tp, fcsr, zero
[0x80000ff4]:fsw ft11, 840(ra)
[0x80000ff8]:sw tp, 844(ra)
[0x80000ffc]:flw ft10, 1272(gp)
[0x80001000]:flw ft9, 1276(gp)
[0x80001004]:flw ft8, 1280(gp)
[0x80001008]:addi sp, zero, 34
[0x8000100c]:csrrw zero, fcsr, sp
[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001014]:csrrs tp, fcsr, zero
[0x80001018]:fsw ft11, 848(ra)
[0x8000101c]:sw tp, 852(ra)
[0x80001020]:flw ft10, 1284(gp)
[0x80001024]:flw ft9, 1288(gp)
[0x80001028]:flw ft8, 1292(gp)
[0x8000102c]:addi sp, zero, 66
[0x80001030]:csrrw zero, fcsr, sp
[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001038]:csrrs tp, fcsr, zero
[0x8000103c]:fsw ft11, 856(ra)
[0x80001040]:sw tp, 860(ra)
[0x80001044]:flw ft10, 1296(gp)
[0x80001048]:flw ft9, 1300(gp)
[0x8000104c]:flw ft8, 1304(gp)
[0x80001050]:addi sp, zero, 98
[0x80001054]:csrrw zero, fcsr, sp
[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000105c]:csrrs tp, fcsr, zero
[0x80001060]:fsw ft11, 864(ra)
[0x80001064]:sw tp, 868(ra)
[0x80001068]:flw ft10, 1308(gp)
[0x8000106c]:flw ft9, 1312(gp)
[0x80001070]:flw ft8, 1316(gp)
[0x80001074]:addi sp, zero, 130
[0x80001078]:csrrw zero, fcsr, sp
[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001080]:csrrs tp, fcsr, zero
[0x80001084]:fsw ft11, 872(ra)
[0x80001088]:sw tp, 876(ra)
[0x8000108c]:flw ft10, 1320(gp)
[0x80001090]:flw ft9, 1324(gp)
[0x80001094]:flw ft8, 1328(gp)
[0x80001098]:addi sp, zero, 2
[0x8000109c]:csrrw zero, fcsr, sp
[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010a4]:csrrs tp, fcsr, zero
[0x800010a8]:fsw ft11, 880(ra)
[0x800010ac]:sw tp, 884(ra)
[0x800010b0]:flw ft10, 1332(gp)
[0x800010b4]:flw ft9, 1336(gp)
[0x800010b8]:flw ft8, 1340(gp)
[0x800010bc]:addi sp, zero, 34
[0x800010c0]:csrrw zero, fcsr, sp
[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010c8]:csrrs tp, fcsr, zero
[0x800010cc]:fsw ft11, 888(ra)
[0x800010d0]:sw tp, 892(ra)
[0x800010d4]:flw ft10, 1344(gp)
[0x800010d8]:flw ft9, 1348(gp)
[0x800010dc]:flw ft8, 1352(gp)
[0x800010e0]:addi sp, zero, 66
[0x800010e4]:csrrw zero, fcsr, sp
[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs tp, fcsr, zero
[0x800010f0]:fsw ft11, 896(ra)
[0x800010f4]:sw tp, 900(ra)
[0x800010f8]:flw ft10, 1356(gp)
[0x800010fc]:flw ft9, 1360(gp)
[0x80001100]:flw ft8, 1364(gp)
[0x80001104]:addi sp, zero, 98
[0x80001108]:csrrw zero, fcsr, sp
[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001110]:csrrs tp, fcsr, zero
[0x80001114]:fsw ft11, 904(ra)
[0x80001118]:sw tp, 908(ra)
[0x8000111c]:flw ft10, 1368(gp)
[0x80001120]:flw ft9, 1372(gp)
[0x80001124]:flw ft8, 1376(gp)
[0x80001128]:addi sp, zero, 130
[0x8000112c]:csrrw zero, fcsr, sp
[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001134]:csrrs tp, fcsr, zero
[0x80001138]:fsw ft11, 912(ra)
[0x8000113c]:sw tp, 916(ra)
[0x80001140]:flw ft10, 1380(gp)
[0x80001144]:flw ft9, 1384(gp)
[0x80001148]:flw ft8, 1388(gp)
[0x8000114c]:addi sp, zero, 2
[0x80001150]:csrrw zero, fcsr, sp
[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001158]:csrrs tp, fcsr, zero
[0x8000115c]:fsw ft11, 920(ra)
[0x80001160]:sw tp, 924(ra)
[0x80001164]:flw ft10, 1392(gp)
[0x80001168]:flw ft9, 1396(gp)
[0x8000116c]:flw ft8, 1400(gp)
[0x80001170]:addi sp, zero, 34
[0x80001174]:csrrw zero, fcsr, sp
[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000117c]:csrrs tp, fcsr, zero
[0x80001180]:fsw ft11, 928(ra)
[0x80001184]:sw tp, 932(ra)
[0x80001188]:flw ft10, 1404(gp)
[0x8000118c]:flw ft9, 1408(gp)
[0x80001190]:flw ft8, 1412(gp)
[0x80001194]:addi sp, zero, 66
[0x80001198]:csrrw zero, fcsr, sp
[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011a0]:csrrs tp, fcsr, zero
[0x800011a4]:fsw ft11, 936(ra)
[0x800011a8]:sw tp, 940(ra)
[0x800011ac]:flw ft10, 1416(gp)
[0x800011b0]:flw ft9, 1420(gp)
[0x800011b4]:flw ft8, 1424(gp)
[0x800011b8]:addi sp, zero, 98
[0x800011bc]:csrrw zero, fcsr, sp
[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011c4]:csrrs tp, fcsr, zero
[0x800011c8]:fsw ft11, 944(ra)
[0x800011cc]:sw tp, 948(ra)
[0x800011d0]:flw ft10, 1428(gp)
[0x800011d4]:flw ft9, 1432(gp)
[0x800011d8]:flw ft8, 1436(gp)
[0x800011dc]:addi sp, zero, 130
[0x800011e0]:csrrw zero, fcsr, sp
[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800011e8]:csrrs tp, fcsr, zero
[0x800011ec]:fsw ft11, 952(ra)
[0x800011f0]:sw tp, 956(ra)
[0x800011f4]:flw ft10, 1440(gp)
[0x800011f8]:flw ft9, 1444(gp)
[0x800011fc]:flw ft8, 1448(gp)
[0x80001200]:addi sp, zero, 2
[0x80001204]:csrrw zero, fcsr, sp
[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000120c]:csrrs tp, fcsr, zero
[0x80001210]:fsw ft11, 960(ra)
[0x80001214]:sw tp, 964(ra)
[0x80001218]:flw ft10, 1452(gp)
[0x8000121c]:flw ft9, 1456(gp)
[0x80001220]:flw ft8, 1460(gp)
[0x80001224]:addi sp, zero, 34
[0x80001228]:csrrw zero, fcsr, sp
[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001230]:csrrs tp, fcsr, zero
[0x80001234]:fsw ft11, 968(ra)
[0x80001238]:sw tp, 972(ra)
[0x8000123c]:flw ft10, 1464(gp)
[0x80001240]:flw ft9, 1468(gp)
[0x80001244]:flw ft8, 1472(gp)
[0x80001248]:addi sp, zero, 66
[0x8000124c]:csrrw zero, fcsr, sp
[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001254]:csrrs tp, fcsr, zero
[0x80001258]:fsw ft11, 976(ra)
[0x8000125c]:sw tp, 980(ra)
[0x80001260]:flw ft10, 1476(gp)
[0x80001264]:flw ft9, 1480(gp)
[0x80001268]:flw ft8, 1484(gp)
[0x8000126c]:addi sp, zero, 98
[0x80001270]:csrrw zero, fcsr, sp
[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001278]:csrrs tp, fcsr, zero
[0x8000127c]:fsw ft11, 984(ra)
[0x80001280]:sw tp, 988(ra)
[0x80001284]:flw ft10, 1488(gp)
[0x80001288]:flw ft9, 1492(gp)
[0x8000128c]:flw ft8, 1496(gp)
[0x80001290]:addi sp, zero, 130
[0x80001294]:csrrw zero, fcsr, sp
[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000129c]:csrrs tp, fcsr, zero
[0x800012a0]:fsw ft11, 992(ra)
[0x800012a4]:sw tp, 996(ra)
[0x800012a8]:flw ft10, 1500(gp)
[0x800012ac]:flw ft9, 1504(gp)
[0x800012b0]:flw ft8, 1508(gp)
[0x800012b4]:addi sp, zero, 2
[0x800012b8]:csrrw zero, fcsr, sp
[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800012c0]:csrrs tp, fcsr, zero
[0x800012c4]:fsw ft11, 1000(ra)
[0x800012c8]:sw tp, 1004(ra)
[0x800012cc]:flw ft10, 1512(gp)
[0x800012d0]:flw ft9, 1516(gp)
[0x800012d4]:flw ft8, 1520(gp)
[0x800012d8]:addi sp, zero, 34
[0x800012dc]:csrrw zero, fcsr, sp
[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800012e4]:csrrs tp, fcsr, zero
[0x800012e8]:fsw ft11, 1008(ra)
[0x800012ec]:sw tp, 1012(ra)
[0x800012f0]:flw ft10, 1524(gp)
[0x800012f4]:flw ft9, 1528(gp)
[0x800012f8]:flw ft8, 1532(gp)
[0x800012fc]:addi sp, zero, 66
[0x80001300]:csrrw zero, fcsr, sp
[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001308]:csrrs tp, fcsr, zero
[0x8000130c]:fsw ft11, 1016(ra)
[0x80001310]:sw tp, 1020(ra)
[0x80001314]:auipc ra, 4
[0x80001318]:addi ra, ra, 3328
[0x8000131c]:flw ft10, 1536(gp)
[0x80001320]:flw ft9, 1540(gp)
[0x80001324]:flw ft8, 1544(gp)
[0x80001328]:addi sp, zero, 98
[0x8000132c]:csrrw zero, fcsr, sp
[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001334]:csrrs tp, fcsr, zero
[0x80001338]:fsw ft11, 0(ra)
[0x8000133c]:sw tp, 4(ra)
[0x80001340]:flw ft10, 1548(gp)
[0x80001344]:flw ft9, 1552(gp)
[0x80001348]:flw ft8, 1556(gp)
[0x8000134c]:addi sp, zero, 130
[0x80001350]:csrrw zero, fcsr, sp
[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001358]:csrrs tp, fcsr, zero
[0x8000135c]:fsw ft11, 8(ra)
[0x80001360]:sw tp, 12(ra)
[0x80001364]:flw ft10, 1560(gp)
[0x80001368]:flw ft9, 1564(gp)
[0x8000136c]:flw ft8, 1568(gp)
[0x80001370]:addi sp, zero, 2
[0x80001374]:csrrw zero, fcsr, sp
[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000137c]:csrrs tp, fcsr, zero
[0x80001380]:fsw ft11, 16(ra)
[0x80001384]:sw tp, 20(ra)
[0x80001388]:flw ft10, 1572(gp)
[0x8000138c]:flw ft9, 1576(gp)
[0x80001390]:flw ft8, 1580(gp)
[0x80001394]:addi sp, zero, 34
[0x80001398]:csrrw zero, fcsr, sp
[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013a0]:csrrs tp, fcsr, zero
[0x800013a4]:fsw ft11, 24(ra)
[0x800013a8]:sw tp, 28(ra)
[0x800013ac]:flw ft10, 1584(gp)
[0x800013b0]:flw ft9, 1588(gp)
[0x800013b4]:flw ft8, 1592(gp)
[0x800013b8]:addi sp, zero, 66
[0x800013bc]:csrrw zero, fcsr, sp
[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013c4]:csrrs tp, fcsr, zero
[0x800013c8]:fsw ft11, 32(ra)
[0x800013cc]:sw tp, 36(ra)
[0x800013d0]:flw ft10, 1596(gp)
[0x800013d4]:flw ft9, 1600(gp)
[0x800013d8]:flw ft8, 1604(gp)
[0x800013dc]:addi sp, zero, 98
[0x800013e0]:csrrw zero, fcsr, sp
[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800013e8]:csrrs tp, fcsr, zero
[0x800013ec]:fsw ft11, 40(ra)
[0x800013f0]:sw tp, 44(ra)
[0x800013f4]:flw ft10, 1608(gp)
[0x800013f8]:flw ft9, 1612(gp)
[0x800013fc]:flw ft8, 1616(gp)
[0x80001400]:addi sp, zero, 130
[0x80001404]:csrrw zero, fcsr, sp
[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000140c]:csrrs tp, fcsr, zero
[0x80001410]:fsw ft11, 48(ra)
[0x80001414]:sw tp, 52(ra)
[0x80001418]:flw ft10, 1620(gp)
[0x8000141c]:flw ft9, 1624(gp)
[0x80001420]:flw ft8, 1628(gp)
[0x80001424]:addi sp, zero, 2
[0x80001428]:csrrw zero, fcsr, sp
[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001430]:csrrs tp, fcsr, zero
[0x80001434]:fsw ft11, 56(ra)
[0x80001438]:sw tp, 60(ra)
[0x8000143c]:flw ft10, 1632(gp)
[0x80001440]:flw ft9, 1636(gp)
[0x80001444]:flw ft8, 1640(gp)
[0x80001448]:addi sp, zero, 34
[0x8000144c]:csrrw zero, fcsr, sp
[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001454]:csrrs tp, fcsr, zero
[0x80001458]:fsw ft11, 64(ra)
[0x8000145c]:sw tp, 68(ra)
[0x80001460]:flw ft10, 1644(gp)
[0x80001464]:flw ft9, 1648(gp)
[0x80001468]:flw ft8, 1652(gp)
[0x8000146c]:addi sp, zero, 66
[0x80001470]:csrrw zero, fcsr, sp
[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001478]:csrrs tp, fcsr, zero
[0x8000147c]:fsw ft11, 72(ra)
[0x80001480]:sw tp, 76(ra)
[0x80001484]:flw ft10, 1656(gp)
[0x80001488]:flw ft9, 1660(gp)
[0x8000148c]:flw ft8, 1664(gp)
[0x80001490]:addi sp, zero, 98
[0x80001494]:csrrw zero, fcsr, sp
[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000149c]:csrrs tp, fcsr, zero
[0x800014a0]:fsw ft11, 80(ra)
[0x800014a4]:sw tp, 84(ra)
[0x800014a8]:flw ft10, 1668(gp)
[0x800014ac]:flw ft9, 1672(gp)
[0x800014b0]:flw ft8, 1676(gp)
[0x800014b4]:addi sp, zero, 130
[0x800014b8]:csrrw zero, fcsr, sp
[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800014c0]:csrrs tp, fcsr, zero
[0x800014c4]:fsw ft11, 88(ra)
[0x800014c8]:sw tp, 92(ra)
[0x800014cc]:flw ft10, 1680(gp)
[0x800014d0]:flw ft9, 1684(gp)
[0x800014d4]:flw ft8, 1688(gp)
[0x800014d8]:addi sp, zero, 2
[0x800014dc]:csrrw zero, fcsr, sp
[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800014e4]:csrrs tp, fcsr, zero
[0x800014e8]:fsw ft11, 96(ra)
[0x800014ec]:sw tp, 100(ra)
[0x800014f0]:flw ft10, 1692(gp)
[0x800014f4]:flw ft9, 1696(gp)
[0x800014f8]:flw ft8, 1700(gp)
[0x800014fc]:addi sp, zero, 34
[0x80001500]:csrrw zero, fcsr, sp
[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001508]:csrrs tp, fcsr, zero
[0x8000150c]:fsw ft11, 104(ra)
[0x80001510]:sw tp, 108(ra)
[0x80001514]:flw ft10, 1704(gp)
[0x80001518]:flw ft9, 1708(gp)
[0x8000151c]:flw ft8, 1712(gp)
[0x80001520]:addi sp, zero, 66
[0x80001524]:csrrw zero, fcsr, sp
[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000152c]:csrrs tp, fcsr, zero
[0x80001530]:fsw ft11, 112(ra)
[0x80001534]:sw tp, 116(ra)
[0x80001538]:flw ft10, 1716(gp)
[0x8000153c]:flw ft9, 1720(gp)
[0x80001540]:flw ft8, 1724(gp)
[0x80001544]:addi sp, zero, 98
[0x80001548]:csrrw zero, fcsr, sp
[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001550]:csrrs tp, fcsr, zero
[0x80001554]:fsw ft11, 120(ra)
[0x80001558]:sw tp, 124(ra)
[0x8000155c]:flw ft10, 1728(gp)
[0x80001560]:flw ft9, 1732(gp)
[0x80001564]:flw ft8, 1736(gp)
[0x80001568]:addi sp, zero, 130
[0x8000156c]:csrrw zero, fcsr, sp
[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001574]:csrrs tp, fcsr, zero
[0x80001578]:fsw ft11, 128(ra)
[0x8000157c]:sw tp, 132(ra)
[0x80001580]:flw ft10, 1740(gp)
[0x80001584]:flw ft9, 1744(gp)
[0x80001588]:flw ft8, 1748(gp)
[0x8000158c]:addi sp, zero, 2
[0x80001590]:csrrw zero, fcsr, sp
[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001598]:csrrs tp, fcsr, zero
[0x8000159c]:fsw ft11, 136(ra)
[0x800015a0]:sw tp, 140(ra)
[0x800015a4]:flw ft10, 1752(gp)
[0x800015a8]:flw ft9, 1756(gp)
[0x800015ac]:flw ft8, 1760(gp)
[0x800015b0]:addi sp, zero, 34
[0x800015b4]:csrrw zero, fcsr, sp
[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800015bc]:csrrs tp, fcsr, zero
[0x800015c0]:fsw ft11, 144(ra)
[0x800015c4]:sw tp, 148(ra)
[0x800015c8]:flw ft10, 1764(gp)
[0x800015cc]:flw ft9, 1768(gp)
[0x800015d0]:flw ft8, 1772(gp)
[0x800015d4]:addi sp, zero, 66
[0x800015d8]:csrrw zero, fcsr, sp
[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800015e0]:csrrs tp, fcsr, zero
[0x800015e4]:fsw ft11, 152(ra)
[0x800015e8]:sw tp, 156(ra)
[0x800015ec]:flw ft10, 1776(gp)
[0x800015f0]:flw ft9, 1780(gp)
[0x800015f4]:flw ft8, 1784(gp)
[0x800015f8]:addi sp, zero, 98
[0x800015fc]:csrrw zero, fcsr, sp
[0x80001600]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001600]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001604]:csrrs tp, fcsr, zero
[0x80001608]:fsw ft11, 160(ra)
[0x8000160c]:sw tp, 164(ra)
[0x80001610]:flw ft10, 1788(gp)
[0x80001614]:flw ft9, 1792(gp)
[0x80001618]:flw ft8, 1796(gp)
[0x8000161c]:addi sp, zero, 130
[0x80001620]:csrrw zero, fcsr, sp
[0x80001624]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001624]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001628]:csrrs tp, fcsr, zero
[0x8000162c]:fsw ft11, 168(ra)
[0x80001630]:sw tp, 172(ra)
[0x80001634]:flw ft10, 1800(gp)
[0x80001638]:flw ft9, 1804(gp)
[0x8000163c]:flw ft8, 1808(gp)
[0x80001640]:addi sp, zero, 2
[0x80001644]:csrrw zero, fcsr, sp
[0x80001648]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001648]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000164c]:csrrs tp, fcsr, zero
[0x80001650]:fsw ft11, 176(ra)
[0x80001654]:sw tp, 180(ra)
[0x80001658]:flw ft10, 1812(gp)
[0x8000165c]:flw ft9, 1816(gp)
[0x80001660]:flw ft8, 1820(gp)
[0x80001664]:addi sp, zero, 34
[0x80001668]:csrrw zero, fcsr, sp
[0x8000166c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000166c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001670]:csrrs tp, fcsr, zero
[0x80001674]:fsw ft11, 184(ra)
[0x80001678]:sw tp, 188(ra)
[0x8000167c]:flw ft10, 1824(gp)
[0x80001680]:flw ft9, 1828(gp)
[0x80001684]:flw ft8, 1832(gp)
[0x80001688]:addi sp, zero, 66
[0x8000168c]:csrrw zero, fcsr, sp
[0x80001690]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001690]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001694]:csrrs tp, fcsr, zero
[0x80001698]:fsw ft11, 192(ra)
[0x8000169c]:sw tp, 196(ra)
[0x800016a0]:flw ft10, 1836(gp)
[0x800016a4]:flw ft9, 1840(gp)
[0x800016a8]:flw ft8, 1844(gp)
[0x800016ac]:addi sp, zero, 98
[0x800016b0]:csrrw zero, fcsr, sp
[0x800016b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800016b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800016b8]:csrrs tp, fcsr, zero
[0x800016bc]:fsw ft11, 200(ra)
[0x800016c0]:sw tp, 204(ra)
[0x800016c4]:flw ft10, 1848(gp)
[0x800016c8]:flw ft9, 1852(gp)
[0x800016cc]:flw ft8, 1856(gp)
[0x800016d0]:addi sp, zero, 130
[0x800016d4]:csrrw zero, fcsr, sp
[0x800016d8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800016d8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800016dc]:csrrs tp, fcsr, zero
[0x800016e0]:fsw ft11, 208(ra)
[0x800016e4]:sw tp, 212(ra)
[0x800016e8]:flw ft10, 1860(gp)
[0x800016ec]:flw ft9, 1864(gp)
[0x800016f0]:flw ft8, 1868(gp)
[0x800016f4]:addi sp, zero, 2
[0x800016f8]:csrrw zero, fcsr, sp
[0x800016fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800016fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001700]:csrrs tp, fcsr, zero
[0x80001704]:fsw ft11, 216(ra)
[0x80001708]:sw tp, 220(ra)
[0x8000170c]:flw ft10, 1872(gp)
[0x80001710]:flw ft9, 1876(gp)
[0x80001714]:flw ft8, 1880(gp)
[0x80001718]:addi sp, zero, 34
[0x8000171c]:csrrw zero, fcsr, sp
[0x80001720]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001720]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001724]:csrrs tp, fcsr, zero
[0x80001728]:fsw ft11, 224(ra)
[0x8000172c]:sw tp, 228(ra)
[0x80001730]:flw ft10, 1884(gp)
[0x80001734]:flw ft9, 1888(gp)
[0x80001738]:flw ft8, 1892(gp)
[0x8000173c]:addi sp, zero, 66
[0x80001740]:csrrw zero, fcsr, sp
[0x80001744]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001744]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001748]:csrrs tp, fcsr, zero
[0x8000174c]:fsw ft11, 232(ra)
[0x80001750]:sw tp, 236(ra)
[0x80001754]:flw ft10, 1896(gp)
[0x80001758]:flw ft9, 1900(gp)
[0x8000175c]:flw ft8, 1904(gp)
[0x80001760]:addi sp, zero, 98
[0x80001764]:csrrw zero, fcsr, sp
[0x80001768]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001768]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000176c]:csrrs tp, fcsr, zero
[0x80001770]:fsw ft11, 240(ra)
[0x80001774]:sw tp, 244(ra)
[0x80001778]:flw ft10, 1908(gp)
[0x8000177c]:flw ft9, 1912(gp)
[0x80001780]:flw ft8, 1916(gp)
[0x80001784]:addi sp, zero, 130
[0x80001788]:csrrw zero, fcsr, sp
[0x8000178c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000178c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001790]:csrrs tp, fcsr, zero
[0x80001794]:fsw ft11, 248(ra)
[0x80001798]:sw tp, 252(ra)
[0x8000179c]:flw ft10, 1920(gp)
[0x800017a0]:flw ft9, 1924(gp)
[0x800017a4]:flw ft8, 1928(gp)
[0x800017a8]:addi sp, zero, 2
[0x800017ac]:csrrw zero, fcsr, sp
[0x800017b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800017b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800017b4]:csrrs tp, fcsr, zero
[0x800017b8]:fsw ft11, 256(ra)
[0x800017bc]:sw tp, 260(ra)
[0x800017c0]:flw ft10, 1932(gp)
[0x800017c4]:flw ft9, 1936(gp)
[0x800017c8]:flw ft8, 1940(gp)
[0x800017cc]:addi sp, zero, 34
[0x800017d0]:csrrw zero, fcsr, sp
[0x800017d4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800017d4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800017d8]:csrrs tp, fcsr, zero
[0x800017dc]:fsw ft11, 264(ra)
[0x800017e0]:sw tp, 268(ra)
[0x800017e4]:flw ft10, 1944(gp)
[0x800017e8]:flw ft9, 1948(gp)
[0x800017ec]:flw ft8, 1952(gp)
[0x800017f0]:addi sp, zero, 66
[0x800017f4]:csrrw zero, fcsr, sp
[0x800017f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800017f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800017fc]:csrrs tp, fcsr, zero
[0x80001800]:fsw ft11, 272(ra)
[0x80001804]:sw tp, 276(ra)
[0x80001808]:flw ft10, 1956(gp)
[0x8000180c]:flw ft9, 1960(gp)
[0x80001810]:flw ft8, 1964(gp)
[0x80001814]:addi sp, zero, 98
[0x80001818]:csrrw zero, fcsr, sp
[0x8000181c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000181c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001820]:csrrs tp, fcsr, zero
[0x80001824]:fsw ft11, 280(ra)
[0x80001828]:sw tp, 284(ra)
[0x8000182c]:flw ft10, 1968(gp)
[0x80001830]:flw ft9, 1972(gp)
[0x80001834]:flw ft8, 1976(gp)
[0x80001838]:addi sp, zero, 130
[0x8000183c]:csrrw zero, fcsr, sp
[0x80001840]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001840]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001844]:csrrs tp, fcsr, zero
[0x80001848]:fsw ft11, 288(ra)
[0x8000184c]:sw tp, 292(ra)
[0x80001850]:flw ft10, 1980(gp)
[0x80001854]:flw ft9, 1984(gp)
[0x80001858]:flw ft8, 1988(gp)
[0x8000185c]:addi sp, zero, 2
[0x80001860]:csrrw zero, fcsr, sp
[0x80001864]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001864]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001868]:csrrs tp, fcsr, zero
[0x8000186c]:fsw ft11, 296(ra)
[0x80001870]:sw tp, 300(ra)
[0x80001874]:flw ft10, 1992(gp)
[0x80001878]:flw ft9, 1996(gp)
[0x8000187c]:flw ft8, 2000(gp)
[0x80001880]:addi sp, zero, 34
[0x80001884]:csrrw zero, fcsr, sp
[0x80001888]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001888]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000188c]:csrrs tp, fcsr, zero
[0x80001890]:fsw ft11, 304(ra)
[0x80001894]:sw tp, 308(ra)
[0x80001898]:flw ft10, 2004(gp)
[0x8000189c]:flw ft9, 2008(gp)
[0x800018a0]:flw ft8, 2012(gp)
[0x800018a4]:addi sp, zero, 66
[0x800018a8]:csrrw zero, fcsr, sp
[0x800018ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800018ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800018b0]:csrrs tp, fcsr, zero
[0x800018b4]:fsw ft11, 312(ra)
[0x800018b8]:sw tp, 316(ra)
[0x800018bc]:flw ft10, 2016(gp)
[0x800018c0]:flw ft9, 2020(gp)
[0x800018c4]:flw ft8, 2024(gp)
[0x800018c8]:addi sp, zero, 98
[0x800018cc]:csrrw zero, fcsr, sp
[0x800018d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800018d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800018d4]:csrrs tp, fcsr, zero
[0x800018d8]:fsw ft11, 320(ra)
[0x800018dc]:sw tp, 324(ra)
[0x800018e0]:flw ft10, 2028(gp)
[0x800018e4]:flw ft9, 2032(gp)
[0x800018e8]:flw ft8, 2036(gp)
[0x800018ec]:addi sp, zero, 130
[0x800018f0]:csrrw zero, fcsr, sp
[0x800018f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800018f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800018f8]:csrrs tp, fcsr, zero
[0x800018fc]:fsw ft11, 328(ra)
[0x80001900]:sw tp, 332(ra)
[0x80001904]:flw ft10, 2040(gp)
[0x80001908]:flw ft9, 2044(gp)
[0x8000190c]:lui sp, 1
[0x80001910]:addi sp, sp, 2048
[0x80001914]:add gp, gp, sp
[0x80001918]:flw ft8, 0(gp)
[0x8000191c]:sub gp, gp, sp
[0x80001920]:addi sp, zero, 2
[0x80001924]:csrrw zero, fcsr, sp
[0x80001928]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001928]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000192c]:csrrs tp, fcsr, zero
[0x80001930]:fsw ft11, 336(ra)
[0x80001934]:sw tp, 340(ra)
[0x80001938]:lui sp, 1
[0x8000193c]:addi sp, sp, 2048
[0x80001940]:add gp, gp, sp
[0x80001944]:flw ft10, 4(gp)
[0x80001948]:sub gp, gp, sp
[0x8000194c]:lui sp, 1
[0x80001950]:addi sp, sp, 2048
[0x80001954]:add gp, gp, sp
[0x80001958]:flw ft9, 8(gp)
[0x8000195c]:sub gp, gp, sp
[0x80001960]:lui sp, 1
[0x80001964]:addi sp, sp, 2048
[0x80001968]:add gp, gp, sp
[0x8000196c]:flw ft8, 12(gp)
[0x80001970]:sub gp, gp, sp
[0x80001974]:addi sp, zero, 34
[0x80001978]:csrrw zero, fcsr, sp
[0x8000197c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000197c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001980]:csrrs tp, fcsr, zero
[0x80001984]:fsw ft11, 344(ra)
[0x80001988]:sw tp, 348(ra)
[0x8000198c]:lui sp, 1
[0x80001990]:addi sp, sp, 2048
[0x80001994]:add gp, gp, sp
[0x80001998]:flw ft10, 16(gp)
[0x8000199c]:sub gp, gp, sp
[0x800019a0]:lui sp, 1
[0x800019a4]:addi sp, sp, 2048
[0x800019a8]:add gp, gp, sp
[0x800019ac]:flw ft9, 20(gp)
[0x800019b0]:sub gp, gp, sp
[0x800019b4]:lui sp, 1
[0x800019b8]:addi sp, sp, 2048
[0x800019bc]:add gp, gp, sp
[0x800019c0]:flw ft8, 24(gp)
[0x800019c4]:sub gp, gp, sp
[0x800019c8]:addi sp, zero, 66
[0x800019cc]:csrrw zero, fcsr, sp
[0x800019d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800019d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800019d4]:csrrs tp, fcsr, zero
[0x800019d8]:fsw ft11, 352(ra)
[0x800019dc]:sw tp, 356(ra)
[0x800019e0]:lui sp, 1
[0x800019e4]:addi sp, sp, 2048
[0x800019e8]:add gp, gp, sp
[0x800019ec]:flw ft10, 28(gp)
[0x800019f0]:sub gp, gp, sp
[0x800019f4]:lui sp, 1
[0x800019f8]:addi sp, sp, 2048
[0x800019fc]:add gp, gp, sp
[0x80001a00]:flw ft9, 32(gp)
[0x80001a04]:sub gp, gp, sp
[0x80001a08]:lui sp, 1
[0x80001a0c]:addi sp, sp, 2048
[0x80001a10]:add gp, gp, sp
[0x80001a14]:flw ft8, 36(gp)
[0x80001a18]:sub gp, gp, sp
[0x80001a1c]:addi sp, zero, 98
[0x80001a20]:csrrw zero, fcsr, sp
[0x80001a24]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001a24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001a28]:csrrs tp, fcsr, zero
[0x80001a2c]:fsw ft11, 360(ra)
[0x80001a30]:sw tp, 364(ra)
[0x80001a34]:lui sp, 1
[0x80001a38]:addi sp, sp, 2048
[0x80001a3c]:add gp, gp, sp
[0x80001a40]:flw ft10, 40(gp)
[0x80001a44]:sub gp, gp, sp
[0x80001a48]:lui sp, 1
[0x80001a4c]:addi sp, sp, 2048
[0x80001a50]:add gp, gp, sp
[0x80001a54]:flw ft9, 44(gp)
[0x80001a58]:sub gp, gp, sp
[0x80001a5c]:lui sp, 1
[0x80001a60]:addi sp, sp, 2048
[0x80001a64]:add gp, gp, sp
[0x80001a68]:flw ft8, 48(gp)
[0x80001a6c]:sub gp, gp, sp
[0x80001a70]:addi sp, zero, 130
[0x80001a74]:csrrw zero, fcsr, sp
[0x80001a78]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001a78]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001a7c]:csrrs tp, fcsr, zero
[0x80001a80]:fsw ft11, 368(ra)
[0x80001a84]:sw tp, 372(ra)
[0x80001a88]:lui sp, 1
[0x80001a8c]:addi sp, sp, 2048
[0x80001a90]:add gp, gp, sp
[0x80001a94]:flw ft10, 52(gp)
[0x80001a98]:sub gp, gp, sp
[0x80001a9c]:lui sp, 1
[0x80001aa0]:addi sp, sp, 2048
[0x80001aa4]:add gp, gp, sp
[0x80001aa8]:flw ft9, 56(gp)
[0x80001aac]:sub gp, gp, sp
[0x80001ab0]:lui sp, 1
[0x80001ab4]:addi sp, sp, 2048
[0x80001ab8]:add gp, gp, sp
[0x80001abc]:flw ft8, 60(gp)
[0x80001ac0]:sub gp, gp, sp
[0x80001ac4]:addi sp, zero, 2
[0x80001ac8]:csrrw zero, fcsr, sp
[0x80001acc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001acc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001ad0]:csrrs tp, fcsr, zero
[0x80001ad4]:fsw ft11, 376(ra)
[0x80001ad8]:sw tp, 380(ra)
[0x80001adc]:lui sp, 1
[0x80001ae0]:addi sp, sp, 2048
[0x80001ae4]:add gp, gp, sp
[0x80001ae8]:flw ft10, 64(gp)
[0x80001aec]:sub gp, gp, sp
[0x80001af0]:lui sp, 1
[0x80001af4]:addi sp, sp, 2048
[0x80001af8]:add gp, gp, sp
[0x80001afc]:flw ft9, 68(gp)
[0x80001b00]:sub gp, gp, sp
[0x80001b04]:lui sp, 1
[0x80001b08]:addi sp, sp, 2048
[0x80001b0c]:add gp, gp, sp
[0x80001b10]:flw ft8, 72(gp)
[0x80001b14]:sub gp, gp, sp
[0x80001b18]:addi sp, zero, 34
[0x80001b1c]:csrrw zero, fcsr, sp
[0x80001b20]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001b20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001b24]:csrrs tp, fcsr, zero
[0x80001b28]:fsw ft11, 384(ra)
[0x80001b2c]:sw tp, 388(ra)
[0x80001b30]:lui sp, 1
[0x80001b34]:addi sp, sp, 2048
[0x80001b38]:add gp, gp, sp
[0x80001b3c]:flw ft10, 76(gp)
[0x80001b40]:sub gp, gp, sp
[0x80001b44]:lui sp, 1
[0x80001b48]:addi sp, sp, 2048
[0x80001b4c]:add gp, gp, sp
[0x80001b50]:flw ft9, 80(gp)
[0x80001b54]:sub gp, gp, sp
[0x80001b58]:lui sp, 1
[0x80001b5c]:addi sp, sp, 2048
[0x80001b60]:add gp, gp, sp
[0x80001b64]:flw ft8, 84(gp)
[0x80001b68]:sub gp, gp, sp
[0x80001b6c]:addi sp, zero, 66
[0x80001b70]:csrrw zero, fcsr, sp
[0x80001b74]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001b74]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001b78]:csrrs tp, fcsr, zero
[0x80001b7c]:fsw ft11, 392(ra)
[0x80001b80]:sw tp, 396(ra)
[0x80001b84]:lui sp, 1
[0x80001b88]:addi sp, sp, 2048
[0x80001b8c]:add gp, gp, sp
[0x80001b90]:flw ft10, 88(gp)
[0x80001b94]:sub gp, gp, sp
[0x80001b98]:lui sp, 1
[0x80001b9c]:addi sp, sp, 2048
[0x80001ba0]:add gp, gp, sp
[0x80001ba4]:flw ft9, 92(gp)
[0x80001ba8]:sub gp, gp, sp
[0x80001bac]:lui sp, 1
[0x80001bb0]:addi sp, sp, 2048
[0x80001bb4]:add gp, gp, sp
[0x80001bb8]:flw ft8, 96(gp)
[0x80001bbc]:sub gp, gp, sp
[0x80001bc0]:addi sp, zero, 98
[0x80001bc4]:csrrw zero, fcsr, sp
[0x80001bc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001bc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001bcc]:csrrs tp, fcsr, zero
[0x80001bd0]:fsw ft11, 400(ra)
[0x80001bd4]:sw tp, 404(ra)
[0x80001bd8]:lui sp, 1
[0x80001bdc]:addi sp, sp, 2048
[0x80001be0]:add gp, gp, sp
[0x80001be4]:flw ft10, 100(gp)
[0x80001be8]:sub gp, gp, sp
[0x80001bec]:lui sp, 1
[0x80001bf0]:addi sp, sp, 2048
[0x80001bf4]:add gp, gp, sp
[0x80001bf8]:flw ft9, 104(gp)
[0x80001bfc]:sub gp, gp, sp
[0x80001c00]:lui sp, 1
[0x80001c04]:addi sp, sp, 2048
[0x80001c08]:add gp, gp, sp
[0x80001c0c]:flw ft8, 108(gp)
[0x80001c10]:sub gp, gp, sp
[0x80001c14]:addi sp, zero, 130
[0x80001c18]:csrrw zero, fcsr, sp
[0x80001c1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001c1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001c20]:csrrs tp, fcsr, zero
[0x80001c24]:fsw ft11, 408(ra)
[0x80001c28]:sw tp, 412(ra)
[0x80001c2c]:lui sp, 1
[0x80001c30]:addi sp, sp, 2048
[0x80001c34]:add gp, gp, sp
[0x80001c38]:flw ft10, 112(gp)
[0x80001c3c]:sub gp, gp, sp
[0x80001c40]:lui sp, 1
[0x80001c44]:addi sp, sp, 2048
[0x80001c48]:add gp, gp, sp
[0x80001c4c]:flw ft9, 116(gp)
[0x80001c50]:sub gp, gp, sp
[0x80001c54]:lui sp, 1
[0x80001c58]:addi sp, sp, 2048
[0x80001c5c]:add gp, gp, sp
[0x80001c60]:flw ft8, 120(gp)
[0x80001c64]:sub gp, gp, sp
[0x80001c68]:addi sp, zero, 2
[0x80001c6c]:csrrw zero, fcsr, sp
[0x80001c70]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001c70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001c74]:csrrs tp, fcsr, zero
[0x80001c78]:fsw ft11, 416(ra)
[0x80001c7c]:sw tp, 420(ra)
[0x80001c80]:lui sp, 1
[0x80001c84]:addi sp, sp, 2048
[0x80001c88]:add gp, gp, sp
[0x80001c8c]:flw ft10, 124(gp)
[0x80001c90]:sub gp, gp, sp
[0x80001c94]:lui sp, 1
[0x80001c98]:addi sp, sp, 2048
[0x80001c9c]:add gp, gp, sp
[0x80001ca0]:flw ft9, 128(gp)
[0x80001ca4]:sub gp, gp, sp
[0x80001ca8]:lui sp, 1
[0x80001cac]:addi sp, sp, 2048
[0x80001cb0]:add gp, gp, sp
[0x80001cb4]:flw ft8, 132(gp)
[0x80001cb8]:sub gp, gp, sp
[0x80001cbc]:addi sp, zero, 34
[0x80001cc0]:csrrw zero, fcsr, sp
[0x80001cc4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001cc4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001cc8]:csrrs tp, fcsr, zero
[0x80001ccc]:fsw ft11, 424(ra)
[0x80001cd0]:sw tp, 428(ra)
[0x80001cd4]:lui sp, 1
[0x80001cd8]:addi sp, sp, 2048
[0x80001cdc]:add gp, gp, sp
[0x80001ce0]:flw ft10, 136(gp)
[0x80001ce4]:sub gp, gp, sp
[0x80001ce8]:lui sp, 1
[0x80001cec]:addi sp, sp, 2048
[0x80001cf0]:add gp, gp, sp
[0x80001cf4]:flw ft9, 140(gp)
[0x80001cf8]:sub gp, gp, sp
[0x80001cfc]:lui sp, 1
[0x80001d00]:addi sp, sp, 2048
[0x80001d04]:add gp, gp, sp
[0x80001d08]:flw ft8, 144(gp)
[0x80001d0c]:sub gp, gp, sp
[0x80001d10]:addi sp, zero, 66
[0x80001d14]:csrrw zero, fcsr, sp
[0x80001d18]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001d18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001d1c]:csrrs tp, fcsr, zero
[0x80001d20]:fsw ft11, 432(ra)
[0x80001d24]:sw tp, 436(ra)
[0x80001d28]:lui sp, 1
[0x80001d2c]:addi sp, sp, 2048
[0x80001d30]:add gp, gp, sp
[0x80001d34]:flw ft10, 148(gp)
[0x80001d38]:sub gp, gp, sp
[0x80001d3c]:lui sp, 1
[0x80001d40]:addi sp, sp, 2048
[0x80001d44]:add gp, gp, sp
[0x80001d48]:flw ft9, 152(gp)
[0x80001d4c]:sub gp, gp, sp
[0x80001d50]:lui sp, 1
[0x80001d54]:addi sp, sp, 2048
[0x80001d58]:add gp, gp, sp
[0x80001d5c]:flw ft8, 156(gp)
[0x80001d60]:sub gp, gp, sp
[0x80001d64]:addi sp, zero, 98
[0x80001d68]:csrrw zero, fcsr, sp
[0x80001d6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001d6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001d70]:csrrs tp, fcsr, zero
[0x80001d74]:fsw ft11, 440(ra)
[0x80001d78]:sw tp, 444(ra)
[0x80001d7c]:lui sp, 1
[0x80001d80]:addi sp, sp, 2048
[0x80001d84]:add gp, gp, sp
[0x80001d88]:flw ft10, 160(gp)
[0x80001d8c]:sub gp, gp, sp
[0x80001d90]:lui sp, 1
[0x80001d94]:addi sp, sp, 2048
[0x80001d98]:add gp, gp, sp
[0x80001d9c]:flw ft9, 164(gp)
[0x80001da0]:sub gp, gp, sp
[0x80001da4]:lui sp, 1
[0x80001da8]:addi sp, sp, 2048
[0x80001dac]:add gp, gp, sp
[0x80001db0]:flw ft8, 168(gp)
[0x80001db4]:sub gp, gp, sp
[0x80001db8]:addi sp, zero, 130
[0x80001dbc]:csrrw zero, fcsr, sp
[0x80001dc0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001dc0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001dc4]:csrrs tp, fcsr, zero
[0x80001dc8]:fsw ft11, 448(ra)
[0x80001dcc]:sw tp, 452(ra)
[0x80001dd0]:lui sp, 1
[0x80001dd4]:addi sp, sp, 2048
[0x80001dd8]:add gp, gp, sp
[0x80001ddc]:flw ft10, 172(gp)
[0x80001de0]:sub gp, gp, sp
[0x80001de4]:lui sp, 1
[0x80001de8]:addi sp, sp, 2048
[0x80001dec]:add gp, gp, sp
[0x80001df0]:flw ft9, 176(gp)
[0x80001df4]:sub gp, gp, sp
[0x80001df8]:lui sp, 1
[0x80001dfc]:addi sp, sp, 2048
[0x80001e00]:add gp, gp, sp
[0x80001e04]:flw ft8, 180(gp)
[0x80001e08]:sub gp, gp, sp
[0x80001e0c]:addi sp, zero, 2
[0x80001e10]:csrrw zero, fcsr, sp
[0x80001e14]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001e14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001e18]:csrrs tp, fcsr, zero
[0x80001e1c]:fsw ft11, 456(ra)
[0x80001e20]:sw tp, 460(ra)
[0x80001e24]:lui sp, 1
[0x80001e28]:addi sp, sp, 2048
[0x80001e2c]:add gp, gp, sp
[0x80001e30]:flw ft10, 184(gp)
[0x80001e34]:sub gp, gp, sp
[0x80001e38]:lui sp, 1
[0x80001e3c]:addi sp, sp, 2048
[0x80001e40]:add gp, gp, sp
[0x80001e44]:flw ft9, 188(gp)
[0x80001e48]:sub gp, gp, sp
[0x80001e4c]:lui sp, 1
[0x80001e50]:addi sp, sp, 2048
[0x80001e54]:add gp, gp, sp
[0x80001e58]:flw ft8, 192(gp)
[0x80001e5c]:sub gp, gp, sp
[0x80001e60]:addi sp, zero, 34
[0x80001e64]:csrrw zero, fcsr, sp
[0x80001e68]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001e68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001e6c]:csrrs tp, fcsr, zero
[0x80001e70]:fsw ft11, 464(ra)
[0x80001e74]:sw tp, 468(ra)
[0x80001e78]:lui sp, 1
[0x80001e7c]:addi sp, sp, 2048
[0x80001e80]:add gp, gp, sp
[0x80001e84]:flw ft10, 196(gp)
[0x80001e88]:sub gp, gp, sp
[0x80001e8c]:lui sp, 1
[0x80001e90]:addi sp, sp, 2048
[0x80001e94]:add gp, gp, sp
[0x80001e98]:flw ft9, 200(gp)
[0x80001e9c]:sub gp, gp, sp
[0x80001ea0]:lui sp, 1
[0x80001ea4]:addi sp, sp, 2048
[0x80001ea8]:add gp, gp, sp
[0x80001eac]:flw ft8, 204(gp)
[0x80001eb0]:sub gp, gp, sp
[0x80001eb4]:addi sp, zero, 66
[0x80001eb8]:csrrw zero, fcsr, sp
[0x80001ebc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001ebc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001ec0]:csrrs tp, fcsr, zero
[0x80001ec4]:fsw ft11, 472(ra)
[0x80001ec8]:sw tp, 476(ra)
[0x80001ecc]:lui sp, 1
[0x80001ed0]:addi sp, sp, 2048
[0x80001ed4]:add gp, gp, sp
[0x80001ed8]:flw ft10, 208(gp)
[0x80001edc]:sub gp, gp, sp
[0x80001ee0]:lui sp, 1
[0x80001ee4]:addi sp, sp, 2048
[0x80001ee8]:add gp, gp, sp
[0x80001eec]:flw ft9, 212(gp)
[0x80001ef0]:sub gp, gp, sp
[0x80001ef4]:lui sp, 1
[0x80001ef8]:addi sp, sp, 2048
[0x80001efc]:add gp, gp, sp
[0x80001f00]:flw ft8, 216(gp)
[0x80001f04]:sub gp, gp, sp
[0x80001f08]:addi sp, zero, 98
[0x80001f0c]:csrrw zero, fcsr, sp
[0x80001f10]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001f10]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001f14]:csrrs tp, fcsr, zero
[0x80001f18]:fsw ft11, 480(ra)
[0x80001f1c]:sw tp, 484(ra)
[0x80001f20]:lui sp, 1
[0x80001f24]:addi sp, sp, 2048
[0x80001f28]:add gp, gp, sp
[0x80001f2c]:flw ft10, 220(gp)
[0x80001f30]:sub gp, gp, sp
[0x80001f34]:lui sp, 1
[0x80001f38]:addi sp, sp, 2048
[0x80001f3c]:add gp, gp, sp
[0x80001f40]:flw ft9, 224(gp)
[0x80001f44]:sub gp, gp, sp
[0x80001f48]:lui sp, 1
[0x80001f4c]:addi sp, sp, 2048
[0x80001f50]:add gp, gp, sp
[0x80001f54]:flw ft8, 228(gp)
[0x80001f58]:sub gp, gp, sp
[0x80001f5c]:addi sp, zero, 130
[0x80001f60]:csrrw zero, fcsr, sp
[0x80001f64]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001f64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001f68]:csrrs tp, fcsr, zero
[0x80001f6c]:fsw ft11, 488(ra)
[0x80001f70]:sw tp, 492(ra)
[0x80001f74]:lui sp, 1
[0x80001f78]:addi sp, sp, 2048
[0x80001f7c]:add gp, gp, sp
[0x80001f80]:flw ft10, 232(gp)
[0x80001f84]:sub gp, gp, sp
[0x80001f88]:lui sp, 1
[0x80001f8c]:addi sp, sp, 2048
[0x80001f90]:add gp, gp, sp
[0x80001f94]:flw ft9, 236(gp)
[0x80001f98]:sub gp, gp, sp
[0x80001f9c]:lui sp, 1
[0x80001fa0]:addi sp, sp, 2048
[0x80001fa4]:add gp, gp, sp
[0x80001fa8]:flw ft8, 240(gp)
[0x80001fac]:sub gp, gp, sp
[0x80001fb0]:addi sp, zero, 2
[0x80001fb4]:csrrw zero, fcsr, sp
[0x80001fb8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80001fb8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80001fbc]:csrrs tp, fcsr, zero
[0x80001fc0]:fsw ft11, 496(ra)
[0x80001fc4]:sw tp, 500(ra)
[0x80001fc8]:lui sp, 1
[0x80001fcc]:addi sp, sp, 2048
[0x80001fd0]:add gp, gp, sp
[0x80001fd4]:flw ft10, 244(gp)
[0x80001fd8]:sub gp, gp, sp
[0x80001fdc]:lui sp, 1
[0x80001fe0]:addi sp, sp, 2048
[0x80001fe4]:add gp, gp, sp
[0x80001fe8]:flw ft9, 248(gp)
[0x80001fec]:sub gp, gp, sp
[0x80001ff0]:lui sp, 1
[0x80001ff4]:addi sp, sp, 2048
[0x80001ff8]:add gp, gp, sp
[0x80001ffc]:flw ft8, 252(gp)
[0x80002000]:sub gp, gp, sp
[0x80002004]:addi sp, zero, 34
[0x80002008]:csrrw zero, fcsr, sp
[0x8000200c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000200c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002010]:csrrs tp, fcsr, zero
[0x80002014]:fsw ft11, 504(ra)
[0x80002018]:sw tp, 508(ra)
[0x8000201c]:lui sp, 1
[0x80002020]:addi sp, sp, 2048
[0x80002024]:add gp, gp, sp
[0x80002028]:flw ft10, 256(gp)
[0x8000202c]:sub gp, gp, sp
[0x80002030]:lui sp, 1
[0x80002034]:addi sp, sp, 2048
[0x80002038]:add gp, gp, sp
[0x8000203c]:flw ft9, 260(gp)
[0x80002040]:sub gp, gp, sp
[0x80002044]:lui sp, 1
[0x80002048]:addi sp, sp, 2048
[0x8000204c]:add gp, gp, sp
[0x80002050]:flw ft8, 264(gp)
[0x80002054]:sub gp, gp, sp
[0x80002058]:addi sp, zero, 66
[0x8000205c]:csrrw zero, fcsr, sp
[0x80002060]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002060]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002064]:csrrs tp, fcsr, zero
[0x80002068]:fsw ft11, 512(ra)
[0x8000206c]:sw tp, 516(ra)
[0x80002070]:lui sp, 1
[0x80002074]:addi sp, sp, 2048
[0x80002078]:add gp, gp, sp
[0x8000207c]:flw ft10, 268(gp)
[0x80002080]:sub gp, gp, sp
[0x80002084]:lui sp, 1
[0x80002088]:addi sp, sp, 2048
[0x8000208c]:add gp, gp, sp
[0x80002090]:flw ft9, 272(gp)
[0x80002094]:sub gp, gp, sp
[0x80002098]:lui sp, 1
[0x8000209c]:addi sp, sp, 2048
[0x800020a0]:add gp, gp, sp
[0x800020a4]:flw ft8, 276(gp)
[0x800020a8]:sub gp, gp, sp
[0x800020ac]:addi sp, zero, 98
[0x800020b0]:csrrw zero, fcsr, sp
[0x800020b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800020b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800020b8]:csrrs tp, fcsr, zero
[0x800020bc]:fsw ft11, 520(ra)
[0x800020c0]:sw tp, 524(ra)
[0x800020c4]:lui sp, 1
[0x800020c8]:addi sp, sp, 2048
[0x800020cc]:add gp, gp, sp
[0x800020d0]:flw ft10, 280(gp)
[0x800020d4]:sub gp, gp, sp
[0x800020d8]:lui sp, 1
[0x800020dc]:addi sp, sp, 2048
[0x800020e0]:add gp, gp, sp
[0x800020e4]:flw ft9, 284(gp)
[0x800020e8]:sub gp, gp, sp
[0x800020ec]:lui sp, 1
[0x800020f0]:addi sp, sp, 2048
[0x800020f4]:add gp, gp, sp
[0x800020f8]:flw ft8, 288(gp)
[0x800020fc]:sub gp, gp, sp
[0x80002100]:addi sp, zero, 130
[0x80002104]:csrrw zero, fcsr, sp
[0x80002108]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002108]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000210c]:csrrs tp, fcsr, zero
[0x80002110]:fsw ft11, 528(ra)
[0x80002114]:sw tp, 532(ra)
[0x80002118]:lui sp, 1
[0x8000211c]:addi sp, sp, 2048
[0x80002120]:add gp, gp, sp
[0x80002124]:flw ft10, 292(gp)
[0x80002128]:sub gp, gp, sp
[0x8000212c]:lui sp, 1
[0x80002130]:addi sp, sp, 2048
[0x80002134]:add gp, gp, sp
[0x80002138]:flw ft9, 296(gp)
[0x8000213c]:sub gp, gp, sp
[0x80002140]:lui sp, 1
[0x80002144]:addi sp, sp, 2048
[0x80002148]:add gp, gp, sp
[0x8000214c]:flw ft8, 300(gp)
[0x80002150]:sub gp, gp, sp
[0x80002154]:addi sp, zero, 2
[0x80002158]:csrrw zero, fcsr, sp
[0x8000215c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000215c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002160]:csrrs tp, fcsr, zero
[0x80002164]:fsw ft11, 536(ra)
[0x80002168]:sw tp, 540(ra)
[0x8000216c]:lui sp, 1
[0x80002170]:addi sp, sp, 2048
[0x80002174]:add gp, gp, sp
[0x80002178]:flw ft10, 304(gp)
[0x8000217c]:sub gp, gp, sp
[0x80002180]:lui sp, 1
[0x80002184]:addi sp, sp, 2048
[0x80002188]:add gp, gp, sp
[0x8000218c]:flw ft9, 308(gp)
[0x80002190]:sub gp, gp, sp
[0x80002194]:lui sp, 1
[0x80002198]:addi sp, sp, 2048
[0x8000219c]:add gp, gp, sp
[0x800021a0]:flw ft8, 312(gp)
[0x800021a4]:sub gp, gp, sp
[0x800021a8]:addi sp, zero, 34
[0x800021ac]:csrrw zero, fcsr, sp
[0x800021b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800021b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800021b4]:csrrs tp, fcsr, zero
[0x800021b8]:fsw ft11, 544(ra)
[0x800021bc]:sw tp, 548(ra)
[0x800021c0]:lui sp, 1
[0x800021c4]:addi sp, sp, 2048
[0x800021c8]:add gp, gp, sp
[0x800021cc]:flw ft10, 316(gp)
[0x800021d0]:sub gp, gp, sp
[0x800021d4]:lui sp, 1
[0x800021d8]:addi sp, sp, 2048
[0x800021dc]:add gp, gp, sp
[0x800021e0]:flw ft9, 320(gp)
[0x800021e4]:sub gp, gp, sp
[0x800021e8]:lui sp, 1
[0x800021ec]:addi sp, sp, 2048
[0x800021f0]:add gp, gp, sp
[0x800021f4]:flw ft8, 324(gp)
[0x800021f8]:sub gp, gp, sp
[0x800021fc]:addi sp, zero, 66
[0x80002200]:csrrw zero, fcsr, sp
[0x80002204]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002204]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002208]:csrrs tp, fcsr, zero
[0x8000220c]:fsw ft11, 552(ra)
[0x80002210]:sw tp, 556(ra)
[0x80002214]:lui sp, 1
[0x80002218]:addi sp, sp, 2048
[0x8000221c]:add gp, gp, sp
[0x80002220]:flw ft10, 328(gp)
[0x80002224]:sub gp, gp, sp
[0x80002228]:lui sp, 1
[0x8000222c]:addi sp, sp, 2048
[0x80002230]:add gp, gp, sp
[0x80002234]:flw ft9, 332(gp)
[0x80002238]:sub gp, gp, sp
[0x8000223c]:lui sp, 1
[0x80002240]:addi sp, sp, 2048
[0x80002244]:add gp, gp, sp
[0x80002248]:flw ft8, 336(gp)
[0x8000224c]:sub gp, gp, sp
[0x80002250]:addi sp, zero, 98
[0x80002254]:csrrw zero, fcsr, sp
[0x80002258]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002258]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000225c]:csrrs tp, fcsr, zero
[0x80002260]:fsw ft11, 560(ra)
[0x80002264]:sw tp, 564(ra)
[0x80002268]:lui sp, 1
[0x8000226c]:addi sp, sp, 2048
[0x80002270]:add gp, gp, sp
[0x80002274]:flw ft10, 340(gp)
[0x80002278]:sub gp, gp, sp
[0x8000227c]:lui sp, 1
[0x80002280]:addi sp, sp, 2048
[0x80002284]:add gp, gp, sp
[0x80002288]:flw ft9, 344(gp)
[0x8000228c]:sub gp, gp, sp
[0x80002290]:lui sp, 1
[0x80002294]:addi sp, sp, 2048
[0x80002298]:add gp, gp, sp
[0x8000229c]:flw ft8, 348(gp)
[0x800022a0]:sub gp, gp, sp
[0x800022a4]:addi sp, zero, 130
[0x800022a8]:csrrw zero, fcsr, sp
[0x800022ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800022ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800022b0]:csrrs tp, fcsr, zero
[0x800022b4]:fsw ft11, 568(ra)
[0x800022b8]:sw tp, 572(ra)
[0x800022bc]:lui sp, 1
[0x800022c0]:addi sp, sp, 2048
[0x800022c4]:add gp, gp, sp
[0x800022c8]:flw ft10, 352(gp)
[0x800022cc]:sub gp, gp, sp
[0x800022d0]:lui sp, 1
[0x800022d4]:addi sp, sp, 2048
[0x800022d8]:add gp, gp, sp
[0x800022dc]:flw ft9, 356(gp)
[0x800022e0]:sub gp, gp, sp
[0x800022e4]:lui sp, 1
[0x800022e8]:addi sp, sp, 2048
[0x800022ec]:add gp, gp, sp
[0x800022f0]:flw ft8, 360(gp)
[0x800022f4]:sub gp, gp, sp
[0x800022f8]:addi sp, zero, 2
[0x800022fc]:csrrw zero, fcsr, sp
[0x80002300]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002300]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002304]:csrrs tp, fcsr, zero
[0x80002308]:fsw ft11, 576(ra)
[0x8000230c]:sw tp, 580(ra)
[0x80002310]:lui sp, 1
[0x80002314]:addi sp, sp, 2048
[0x80002318]:add gp, gp, sp
[0x8000231c]:flw ft10, 364(gp)
[0x80002320]:sub gp, gp, sp
[0x80002324]:lui sp, 1
[0x80002328]:addi sp, sp, 2048
[0x8000232c]:add gp, gp, sp
[0x80002330]:flw ft9, 368(gp)
[0x80002334]:sub gp, gp, sp
[0x80002338]:lui sp, 1
[0x8000233c]:addi sp, sp, 2048
[0x80002340]:add gp, gp, sp
[0x80002344]:flw ft8, 372(gp)
[0x80002348]:sub gp, gp, sp
[0x8000234c]:addi sp, zero, 34
[0x80002350]:csrrw zero, fcsr, sp
[0x80002354]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002358]:csrrs tp, fcsr, zero
[0x8000235c]:fsw ft11, 584(ra)
[0x80002360]:sw tp, 588(ra)
[0x80002364]:lui sp, 1
[0x80002368]:addi sp, sp, 2048
[0x8000236c]:add gp, gp, sp
[0x80002370]:flw ft10, 376(gp)
[0x80002374]:sub gp, gp, sp
[0x80002378]:lui sp, 1
[0x8000237c]:addi sp, sp, 2048
[0x80002380]:add gp, gp, sp
[0x80002384]:flw ft9, 380(gp)
[0x80002388]:sub gp, gp, sp
[0x8000238c]:lui sp, 1
[0x80002390]:addi sp, sp, 2048
[0x80002394]:add gp, gp, sp
[0x80002398]:flw ft8, 384(gp)
[0x8000239c]:sub gp, gp, sp
[0x800023a0]:addi sp, zero, 66
[0x800023a4]:csrrw zero, fcsr, sp
[0x800023a8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800023a8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800023ac]:csrrs tp, fcsr, zero
[0x800023b0]:fsw ft11, 592(ra)
[0x800023b4]:sw tp, 596(ra)
[0x800023b8]:lui sp, 1
[0x800023bc]:addi sp, sp, 2048
[0x800023c0]:add gp, gp, sp
[0x800023c4]:flw ft10, 388(gp)
[0x800023c8]:sub gp, gp, sp
[0x800023cc]:lui sp, 1
[0x800023d0]:addi sp, sp, 2048
[0x800023d4]:add gp, gp, sp
[0x800023d8]:flw ft9, 392(gp)
[0x800023dc]:sub gp, gp, sp
[0x800023e0]:lui sp, 1
[0x800023e4]:addi sp, sp, 2048
[0x800023e8]:add gp, gp, sp
[0x800023ec]:flw ft8, 396(gp)
[0x800023f0]:sub gp, gp, sp
[0x800023f4]:addi sp, zero, 98
[0x800023f8]:csrrw zero, fcsr, sp
[0x800023fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800023fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002400]:csrrs tp, fcsr, zero
[0x80002404]:fsw ft11, 600(ra)
[0x80002408]:sw tp, 604(ra)
[0x8000240c]:lui sp, 1
[0x80002410]:addi sp, sp, 2048
[0x80002414]:add gp, gp, sp
[0x80002418]:flw ft10, 400(gp)
[0x8000241c]:sub gp, gp, sp
[0x80002420]:lui sp, 1
[0x80002424]:addi sp, sp, 2048
[0x80002428]:add gp, gp, sp
[0x8000242c]:flw ft9, 404(gp)
[0x80002430]:sub gp, gp, sp
[0x80002434]:lui sp, 1
[0x80002438]:addi sp, sp, 2048
[0x8000243c]:add gp, gp, sp
[0x80002440]:flw ft8, 408(gp)
[0x80002444]:sub gp, gp, sp
[0x80002448]:addi sp, zero, 130
[0x8000244c]:csrrw zero, fcsr, sp
[0x80002450]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002454]:csrrs tp, fcsr, zero
[0x80002458]:fsw ft11, 608(ra)
[0x8000245c]:sw tp, 612(ra)
[0x80002460]:lui sp, 1
[0x80002464]:addi sp, sp, 2048
[0x80002468]:add gp, gp, sp
[0x8000246c]:flw ft10, 412(gp)
[0x80002470]:sub gp, gp, sp
[0x80002474]:lui sp, 1
[0x80002478]:addi sp, sp, 2048
[0x8000247c]:add gp, gp, sp
[0x80002480]:flw ft9, 416(gp)
[0x80002484]:sub gp, gp, sp
[0x80002488]:lui sp, 1
[0x8000248c]:addi sp, sp, 2048
[0x80002490]:add gp, gp, sp
[0x80002494]:flw ft8, 420(gp)
[0x80002498]:sub gp, gp, sp
[0x8000249c]:addi sp, zero, 2
[0x800024a0]:csrrw zero, fcsr, sp
[0x800024a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800024a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800024a8]:csrrs tp, fcsr, zero
[0x800024ac]:fsw ft11, 616(ra)
[0x800024b0]:sw tp, 620(ra)
[0x800024b4]:lui sp, 1
[0x800024b8]:addi sp, sp, 2048
[0x800024bc]:add gp, gp, sp
[0x800024c0]:flw ft10, 424(gp)
[0x800024c4]:sub gp, gp, sp
[0x800024c8]:lui sp, 1
[0x800024cc]:addi sp, sp, 2048
[0x800024d0]:add gp, gp, sp
[0x800024d4]:flw ft9, 428(gp)
[0x800024d8]:sub gp, gp, sp
[0x800024dc]:lui sp, 1
[0x800024e0]:addi sp, sp, 2048
[0x800024e4]:add gp, gp, sp
[0x800024e8]:flw ft8, 432(gp)
[0x800024ec]:sub gp, gp, sp
[0x800024f0]:addi sp, zero, 34
[0x800024f4]:csrrw zero, fcsr, sp
[0x800024f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800024f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800024fc]:csrrs tp, fcsr, zero
[0x80002500]:fsw ft11, 624(ra)
[0x80002504]:sw tp, 628(ra)
[0x80002508]:lui sp, 1
[0x8000250c]:addi sp, sp, 2048
[0x80002510]:add gp, gp, sp
[0x80002514]:flw ft10, 436(gp)
[0x80002518]:sub gp, gp, sp
[0x8000251c]:lui sp, 1
[0x80002520]:addi sp, sp, 2048
[0x80002524]:add gp, gp, sp
[0x80002528]:flw ft9, 440(gp)
[0x8000252c]:sub gp, gp, sp
[0x80002530]:lui sp, 1
[0x80002534]:addi sp, sp, 2048
[0x80002538]:add gp, gp, sp
[0x8000253c]:flw ft8, 444(gp)
[0x80002540]:sub gp, gp, sp
[0x80002544]:addi sp, zero, 66
[0x80002548]:csrrw zero, fcsr, sp
[0x8000254c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000254c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002550]:csrrs tp, fcsr, zero
[0x80002554]:fsw ft11, 632(ra)
[0x80002558]:sw tp, 636(ra)
[0x8000255c]:lui sp, 1
[0x80002560]:addi sp, sp, 2048
[0x80002564]:add gp, gp, sp
[0x80002568]:flw ft10, 448(gp)
[0x8000256c]:sub gp, gp, sp
[0x80002570]:lui sp, 1
[0x80002574]:addi sp, sp, 2048
[0x80002578]:add gp, gp, sp
[0x8000257c]:flw ft9, 452(gp)
[0x80002580]:sub gp, gp, sp
[0x80002584]:lui sp, 1
[0x80002588]:addi sp, sp, 2048
[0x8000258c]:add gp, gp, sp
[0x80002590]:flw ft8, 456(gp)
[0x80002594]:sub gp, gp, sp
[0x80002598]:addi sp, zero, 98
[0x8000259c]:csrrw zero, fcsr, sp
[0x800025a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800025a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800025a4]:csrrs tp, fcsr, zero
[0x800025a8]:fsw ft11, 640(ra)
[0x800025ac]:sw tp, 644(ra)
[0x800025b0]:lui sp, 1
[0x800025b4]:addi sp, sp, 2048
[0x800025b8]:add gp, gp, sp
[0x800025bc]:flw ft10, 460(gp)
[0x800025c0]:sub gp, gp, sp
[0x800025c4]:lui sp, 1
[0x800025c8]:addi sp, sp, 2048
[0x800025cc]:add gp, gp, sp
[0x800025d0]:flw ft9, 464(gp)
[0x800025d4]:sub gp, gp, sp
[0x800025d8]:lui sp, 1
[0x800025dc]:addi sp, sp, 2048
[0x800025e0]:add gp, gp, sp
[0x800025e4]:flw ft8, 468(gp)
[0x800025e8]:sub gp, gp, sp
[0x800025ec]:addi sp, zero, 130
[0x800025f0]:csrrw zero, fcsr, sp
[0x800025f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800025f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800025f8]:csrrs tp, fcsr, zero
[0x800025fc]:fsw ft11, 648(ra)
[0x80002600]:sw tp, 652(ra)
[0x80002604]:lui sp, 1
[0x80002608]:addi sp, sp, 2048
[0x8000260c]:add gp, gp, sp
[0x80002610]:flw ft10, 472(gp)
[0x80002614]:sub gp, gp, sp
[0x80002618]:lui sp, 1
[0x8000261c]:addi sp, sp, 2048
[0x80002620]:add gp, gp, sp
[0x80002624]:flw ft9, 476(gp)
[0x80002628]:sub gp, gp, sp
[0x8000262c]:lui sp, 1
[0x80002630]:addi sp, sp, 2048
[0x80002634]:add gp, gp, sp
[0x80002638]:flw ft8, 480(gp)
[0x8000263c]:sub gp, gp, sp
[0x80002640]:addi sp, zero, 2
[0x80002644]:csrrw zero, fcsr, sp
[0x80002648]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002648]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000264c]:csrrs tp, fcsr, zero
[0x80002650]:fsw ft11, 656(ra)
[0x80002654]:sw tp, 660(ra)
[0x80002658]:lui sp, 1
[0x8000265c]:addi sp, sp, 2048
[0x80002660]:add gp, gp, sp
[0x80002664]:flw ft10, 484(gp)
[0x80002668]:sub gp, gp, sp
[0x8000266c]:lui sp, 1
[0x80002670]:addi sp, sp, 2048
[0x80002674]:add gp, gp, sp
[0x80002678]:flw ft9, 488(gp)
[0x8000267c]:sub gp, gp, sp
[0x80002680]:lui sp, 1
[0x80002684]:addi sp, sp, 2048
[0x80002688]:add gp, gp, sp
[0x8000268c]:flw ft8, 492(gp)
[0x80002690]:sub gp, gp, sp
[0x80002694]:addi sp, zero, 34
[0x80002698]:csrrw zero, fcsr, sp
[0x8000269c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000269c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800026a0]:csrrs tp, fcsr, zero
[0x800026a4]:fsw ft11, 664(ra)
[0x800026a8]:sw tp, 668(ra)
[0x800026ac]:lui sp, 1
[0x800026b0]:addi sp, sp, 2048
[0x800026b4]:add gp, gp, sp
[0x800026b8]:flw ft10, 496(gp)
[0x800026bc]:sub gp, gp, sp
[0x800026c0]:lui sp, 1
[0x800026c4]:addi sp, sp, 2048
[0x800026c8]:add gp, gp, sp
[0x800026cc]:flw ft9, 500(gp)
[0x800026d0]:sub gp, gp, sp
[0x800026d4]:lui sp, 1
[0x800026d8]:addi sp, sp, 2048
[0x800026dc]:add gp, gp, sp
[0x800026e0]:flw ft8, 504(gp)
[0x800026e4]:sub gp, gp, sp
[0x800026e8]:addi sp, zero, 66
[0x800026ec]:csrrw zero, fcsr, sp
[0x800026f0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800026f0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800026f4]:csrrs tp, fcsr, zero
[0x800026f8]:fsw ft11, 672(ra)
[0x800026fc]:sw tp, 676(ra)
[0x80002700]:lui sp, 1
[0x80002704]:addi sp, sp, 2048
[0x80002708]:add gp, gp, sp
[0x8000270c]:flw ft10, 508(gp)
[0x80002710]:sub gp, gp, sp
[0x80002714]:lui sp, 1
[0x80002718]:addi sp, sp, 2048
[0x8000271c]:add gp, gp, sp
[0x80002720]:flw ft9, 512(gp)
[0x80002724]:sub gp, gp, sp
[0x80002728]:lui sp, 1
[0x8000272c]:addi sp, sp, 2048
[0x80002730]:add gp, gp, sp
[0x80002734]:flw ft8, 516(gp)
[0x80002738]:sub gp, gp, sp
[0x8000273c]:addi sp, zero, 98
[0x80002740]:csrrw zero, fcsr, sp
[0x80002744]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002744]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002748]:csrrs tp, fcsr, zero
[0x8000274c]:fsw ft11, 680(ra)
[0x80002750]:sw tp, 684(ra)
[0x80002754]:lui sp, 1
[0x80002758]:addi sp, sp, 2048
[0x8000275c]:add gp, gp, sp
[0x80002760]:flw ft10, 520(gp)
[0x80002764]:sub gp, gp, sp
[0x80002768]:lui sp, 1
[0x8000276c]:addi sp, sp, 2048
[0x80002770]:add gp, gp, sp
[0x80002774]:flw ft9, 524(gp)
[0x80002778]:sub gp, gp, sp
[0x8000277c]:lui sp, 1
[0x80002780]:addi sp, sp, 2048
[0x80002784]:add gp, gp, sp
[0x80002788]:flw ft8, 528(gp)
[0x8000278c]:sub gp, gp, sp
[0x80002790]:addi sp, zero, 130
[0x80002794]:csrrw zero, fcsr, sp
[0x80002798]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002798]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x8000279c]:csrrs tp, fcsr, zero
[0x800027a0]:fsw ft11, 688(ra)
[0x800027a4]:sw tp, 692(ra)
[0x800027a8]:lui sp, 1
[0x800027ac]:addi sp, sp, 2048
[0x800027b0]:add gp, gp, sp
[0x800027b4]:flw ft10, 532(gp)
[0x800027b8]:sub gp, gp, sp
[0x800027bc]:lui sp, 1
[0x800027c0]:addi sp, sp, 2048
[0x800027c4]:add gp, gp, sp
[0x800027c8]:flw ft9, 536(gp)
[0x800027cc]:sub gp, gp, sp
[0x800027d0]:lui sp, 1
[0x800027d4]:addi sp, sp, 2048
[0x800027d8]:add gp, gp, sp
[0x800027dc]:flw ft8, 540(gp)
[0x800027e0]:sub gp, gp, sp
[0x800027e4]:addi sp, zero, 2
[0x800027e8]:csrrw zero, fcsr, sp
[0x800027ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800027ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800027f0]:csrrs tp, fcsr, zero
[0x800027f4]:fsw ft11, 696(ra)
[0x800027f8]:sw tp, 700(ra)
[0x800027fc]:lui sp, 1
[0x80002800]:addi sp, sp, 2048
[0x80002804]:add gp, gp, sp
[0x80002808]:flw ft10, 544(gp)
[0x8000280c]:sub gp, gp, sp
[0x80002810]:lui sp, 1
[0x80002814]:addi sp, sp, 2048
[0x80002818]:add gp, gp, sp
[0x8000281c]:flw ft9, 548(gp)
[0x80002820]:sub gp, gp, sp
[0x80002824]:lui sp, 1
[0x80002828]:addi sp, sp, 2048
[0x8000282c]:add gp, gp, sp
[0x80002830]:flw ft8, 552(gp)
[0x80002834]:sub gp, gp, sp
[0x80002838]:addi sp, zero, 34
[0x8000283c]:csrrw zero, fcsr, sp
[0x80002840]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002840]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002844]:csrrs tp, fcsr, zero
[0x80002848]:fsw ft11, 704(ra)
[0x8000284c]:sw tp, 708(ra)
[0x80002850]:lui sp, 1
[0x80002854]:addi sp, sp, 2048
[0x80002858]:add gp, gp, sp
[0x8000285c]:flw ft10, 556(gp)
[0x80002860]:sub gp, gp, sp
[0x80002864]:lui sp, 1
[0x80002868]:addi sp, sp, 2048
[0x8000286c]:add gp, gp, sp
[0x80002870]:flw ft9, 560(gp)
[0x80002874]:sub gp, gp, sp
[0x80002878]:lui sp, 1
[0x8000287c]:addi sp, sp, 2048
[0x80002880]:add gp, gp, sp
[0x80002884]:flw ft8, 564(gp)
[0x80002888]:sub gp, gp, sp
[0x8000288c]:addi sp, zero, 66
[0x80002890]:csrrw zero, fcsr, sp
[0x80002894]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002894]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002898]:csrrs tp, fcsr, zero
[0x8000289c]:fsw ft11, 712(ra)
[0x800028a0]:sw tp, 716(ra)
[0x800028a4]:lui sp, 1
[0x800028a8]:addi sp, sp, 2048
[0x800028ac]:add gp, gp, sp
[0x800028b0]:flw ft10, 568(gp)
[0x800028b4]:sub gp, gp, sp
[0x800028b8]:lui sp, 1
[0x800028bc]:addi sp, sp, 2048
[0x800028c0]:add gp, gp, sp
[0x800028c4]:flw ft9, 572(gp)
[0x800028c8]:sub gp, gp, sp
[0x800028cc]:lui sp, 1
[0x800028d0]:addi sp, sp, 2048
[0x800028d4]:add gp, gp, sp
[0x800028d8]:flw ft8, 576(gp)
[0x800028dc]:sub gp, gp, sp
[0x800028e0]:addi sp, zero, 98
[0x800028e4]:csrrw zero, fcsr, sp
[0x800028e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800028e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800028ec]:csrrs tp, fcsr, zero
[0x800028f0]:fsw ft11, 720(ra)
[0x800028f4]:sw tp, 724(ra)
[0x800028f8]:lui sp, 1
[0x800028fc]:addi sp, sp, 2048
[0x80002900]:add gp, gp, sp
[0x80002904]:flw ft10, 580(gp)
[0x80002908]:sub gp, gp, sp
[0x8000290c]:lui sp, 1
[0x80002910]:addi sp, sp, 2048
[0x80002914]:add gp, gp, sp
[0x80002918]:flw ft9, 584(gp)
[0x8000291c]:sub gp, gp, sp
[0x80002920]:lui sp, 1
[0x80002924]:addi sp, sp, 2048
[0x80002928]:add gp, gp, sp
[0x8000292c]:flw ft8, 588(gp)
[0x80002930]:sub gp, gp, sp
[0x80002934]:addi sp, zero, 130
[0x80002938]:csrrw zero, fcsr, sp
[0x8000293c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x8000293c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002940]:csrrs tp, fcsr, zero
[0x80002944]:fsw ft11, 728(ra)
[0x80002948]:sw tp, 732(ra)
[0x8000294c]:lui sp, 1
[0x80002950]:addi sp, sp, 2048
[0x80002954]:add gp, gp, sp
[0x80002958]:flw ft10, 592(gp)
[0x8000295c]:sub gp, gp, sp
[0x80002960]:lui sp, 1
[0x80002964]:addi sp, sp, 2048
[0x80002968]:add gp, gp, sp
[0x8000296c]:flw ft9, 596(gp)
[0x80002970]:sub gp, gp, sp
[0x80002974]:lui sp, 1
[0x80002978]:addi sp, sp, 2048
[0x8000297c]:add gp, gp, sp
[0x80002980]:flw ft8, 600(gp)
[0x80002984]:sub gp, gp, sp
[0x80002988]:addi sp, zero, 2
[0x8000298c]:csrrw zero, fcsr, sp
[0x80002990]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002990]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002994]:csrrs tp, fcsr, zero
[0x80002998]:fsw ft11, 736(ra)
[0x8000299c]:sw tp, 740(ra)
[0x800029a0]:lui sp, 1
[0x800029a4]:addi sp, sp, 2048
[0x800029a8]:add gp, gp, sp
[0x800029ac]:flw ft10, 604(gp)
[0x800029b0]:sub gp, gp, sp
[0x800029b4]:lui sp, 1
[0x800029b8]:addi sp, sp, 2048
[0x800029bc]:add gp, gp, sp
[0x800029c0]:flw ft9, 608(gp)
[0x800029c4]:sub gp, gp, sp
[0x800029c8]:lui sp, 1
[0x800029cc]:addi sp, sp, 2048
[0x800029d0]:add gp, gp, sp
[0x800029d4]:flw ft8, 612(gp)
[0x800029d8]:sub gp, gp, sp
[0x800029dc]:addi sp, zero, 34
[0x800029e0]:csrrw zero, fcsr, sp
[0x800029e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x800029e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x800029e8]:csrrs tp, fcsr, zero
[0x800029ec]:fsw ft11, 744(ra)
[0x800029f0]:sw tp, 748(ra)
[0x800029f4]:lui sp, 1
[0x800029f8]:addi sp, sp, 2048
[0x800029fc]:add gp, gp, sp
[0x80002a00]:flw ft10, 616(gp)
[0x80002a04]:sub gp, gp, sp
[0x80002a08]:lui sp, 1
[0x80002a0c]:addi sp, sp, 2048
[0x80002a10]:add gp, gp, sp
[0x80002a14]:flw ft9, 620(gp)
[0x80002a18]:sub gp, gp, sp
[0x80002a1c]:lui sp, 1
[0x80002a20]:addi sp, sp, 2048
[0x80002a24]:add gp, gp, sp
[0x80002a28]:flw ft8, 624(gp)
[0x80002a2c]:sub gp, gp, sp
[0x80002a30]:addi sp, zero, 66
[0x80002a34]:csrrw zero, fcsr, sp
[0x80002a38]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002a38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002a3c]:csrrs tp, fcsr, zero
[0x80002a40]:fsw ft11, 752(ra)
[0x80002a44]:sw tp, 756(ra)
[0x80002a48]:lui sp, 1
[0x80002a4c]:addi sp, sp, 2048
[0x80002a50]:add gp, gp, sp
[0x80002a54]:flw ft10, 628(gp)
[0x80002a58]:sub gp, gp, sp
[0x80002a5c]:lui sp, 1
[0x80002a60]:addi sp, sp, 2048
[0x80002a64]:add gp, gp, sp
[0x80002a68]:flw ft9, 632(gp)
[0x80002a6c]:sub gp, gp, sp
[0x80002a70]:lui sp, 1
[0x80002a74]:addi sp, sp, 2048
[0x80002a78]:add gp, gp, sp
[0x80002a7c]:flw ft8, 636(gp)
[0x80002a80]:sub gp, gp, sp
[0x80002a84]:addi sp, zero, 98
[0x80002a88]:csrrw zero, fcsr, sp
[0x80002a8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002a8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002a90]:csrrs tp, fcsr, zero
[0x80002a94]:fsw ft11, 760(ra)
[0x80002a98]:sw tp, 764(ra)
[0x80002a9c]:lui sp, 1
[0x80002aa0]:addi sp, sp, 2048
[0x80002aa4]:add gp, gp, sp
[0x80002aa8]:flw ft10, 640(gp)
[0x80002aac]:sub gp, gp, sp
[0x80002ab0]:lui sp, 1
[0x80002ab4]:addi sp, sp, 2048
[0x80002ab8]:add gp, gp, sp
[0x80002abc]:flw ft9, 644(gp)
[0x80002ac0]:sub gp, gp, sp
[0x80002ac4]:lui sp, 1
[0x80002ac8]:addi sp, sp, 2048
[0x80002acc]:add gp, gp, sp
[0x80002ad0]:flw ft8, 648(gp)
[0x80002ad4]:sub gp, gp, sp
[0x80002ad8]:addi sp, zero, 34
[0x80002adc]:csrrw zero, fcsr, sp
[0x80002ae0]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002ae0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002ae4]:csrrs tp, fcsr, zero
[0x80002ae8]:fsw ft11, 768(ra)
[0x80002aec]:sw tp, 772(ra)
[0x80002af0]:lui sp, 1
[0x80002af4]:addi sp, sp, 2048
[0x80002af8]:add gp, gp, sp
[0x80002afc]:flw ft10, 652(gp)
[0x80002b00]:sub gp, gp, sp
[0x80002b04]:lui sp, 1
[0x80002b08]:addi sp, sp, 2048
[0x80002b0c]:add gp, gp, sp
[0x80002b10]:flw ft9, 656(gp)
[0x80002b14]:sub gp, gp, sp
[0x80002b18]:lui sp, 1
[0x80002b1c]:addi sp, sp, 2048
[0x80002b20]:add gp, gp, sp
[0x80002b24]:flw ft8, 660(gp)
[0x80002b28]:sub gp, gp, sp
[0x80002b2c]:addi sp, zero, 66
[0x80002b30]:csrrw zero, fcsr, sp
[0x80002b34]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002b34]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002b38]:csrrs tp, fcsr, zero
[0x80002b3c]:fsw ft11, 776(ra)
[0x80002b40]:sw tp, 780(ra)
[0x80002b44]:lui sp, 1
[0x80002b48]:addi sp, sp, 2048
[0x80002b4c]:add gp, gp, sp
[0x80002b50]:flw ft10, 664(gp)
[0x80002b54]:sub gp, gp, sp
[0x80002b58]:lui sp, 1
[0x80002b5c]:addi sp, sp, 2048
[0x80002b60]:add gp, gp, sp
[0x80002b64]:flw ft9, 668(gp)
[0x80002b68]:sub gp, gp, sp
[0x80002b6c]:lui sp, 1
[0x80002b70]:addi sp, sp, 2048
[0x80002b74]:add gp, gp, sp
[0x80002b78]:flw ft8, 672(gp)
[0x80002b7c]:sub gp, gp, sp
[0x80002b80]:addi sp, zero, 98
[0x80002b84]:csrrw zero, fcsr, sp
[0x80002b88]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002b88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002b8c]:csrrs tp, fcsr, zero
[0x80002b90]:fsw ft11, 784(ra)
[0x80002b94]:sw tp, 788(ra)
[0x80002b98]:lui sp, 1
[0x80002b9c]:addi sp, sp, 2048
[0x80002ba0]:add gp, gp, sp
[0x80002ba4]:flw ft10, 676(gp)
[0x80002ba8]:sub gp, gp, sp
[0x80002bac]:lui sp, 1
[0x80002bb0]:addi sp, sp, 2048
[0x80002bb4]:add gp, gp, sp
[0x80002bb8]:flw ft9, 680(gp)
[0x80002bbc]:sub gp, gp, sp
[0x80002bc0]:lui sp, 1
[0x80002bc4]:addi sp, sp, 2048
[0x80002bc8]:add gp, gp, sp
[0x80002bcc]:flw ft8, 684(gp)
[0x80002bd0]:sub gp, gp, sp
[0x80002bd4]:addi sp, zero, 130
[0x80002bd8]:csrrw zero, fcsr, sp
[0x80002bdc]:fnmadd.s ft11, ft10, ft9, ft8, dyn

[0x80002bdc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
[0x80002be0]:csrrs tp, fcsr, zero
[0x80002be4]:fsw ft11, 792(ra)
[0x80002be8]:sw tp, 796(ra)
[0x80002bec]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fnmadd.s', 'rs1 : f30', 'rs2 : f29', 'rd : f31', 'rs3 : f30', 'rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2']
Last Code Sequence : 
	-[0x80000128]:fnmadd.s ft11, ft10, ft9, ft10, dyn
	-[0x8000012c]:csrrs tp, fcsr, zero
	-[0x80000130]:fsw ft11, 0(ra)
	-[0x80000134]:sw tp, 4(ra)
Current Store : [0x80000134] : sw tp, 4(ra) -- Store: [0x80004c18]:0x00000007




Last Coverpoint : ['rs1 : f31', 'rs2 : f28', 'rd : f28', 'rs3 : f28', 'rd == rs2 == rs3 != rs1']
Last Code Sequence : 
	-[0x8000014c]:fnmadd.s ft8, ft11, ft8, ft8, dyn
	-[0x80000150]:csrrs tp, fcsr, zero
	-[0x80000154]:fsw ft8, 8(ra)
	-[0x80000158]:sw tp, 12(ra)
Current Store : [0x80000158] : sw tp, 12(ra) -- Store: [0x80004c20]:0x00000023




Last Coverpoint : ['rs1 : f27', 'rs2 : f27', 'rd : f27', 'rs3 : f31', 'rs1 == rs2 == rd != rs3']
Last Code Sequence : 
	-[0x80000170]:fnmadd.s fs11, fs11, fs11, ft11, dyn
	-[0x80000174]:csrrs tp, fcsr, zero
	-[0x80000178]:fsw fs11, 16(ra)
	-[0x8000017c]:sw tp, 20(ra)
Current Store : [0x8000017c] : sw tp, 20(ra) -- Store: [0x80004c28]:0x00000047




Last Coverpoint : ['rs1 : f29', 'rs2 : f26', 'rd : f30', 'rs3 : f26', 'rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1']
Last Code Sequence : 
	-[0x80000194]:fnmadd.s ft10, ft9, fs10, fs10, dyn
	-[0x80000198]:csrrs tp, fcsr, zero
	-[0x8000019c]:fsw ft10, 24(ra)
	-[0x800001a0]:sw tp, 28(ra)
Current Store : [0x800001a0] : sw tp, 28(ra) -- Store: [0x80004c30]:0x00000063




Last Coverpoint : ['rs1 : f26', 'rs2 : f31', 'rd : f26', 'rs3 : f29', 'rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x185240 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x049ddd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1dd0ae and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001b8]:fnmadd.s fs10, fs10, ft11, ft9, dyn
	-[0x800001bc]:csrrs tp, fcsr, zero
	-[0x800001c0]:fsw fs10, 32(ra)
	-[0x800001c4]:sw tp, 36(ra)
Current Store : [0x800001c4] : sw tp, 36(ra) -- Store: [0x80004c38]:0x00000087




Last Coverpoint : ['rs1 : f28', 'rs2 : f30', 'rd : f25', 'rs3 : f25', 'rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a40d7 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0a1548 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x48ecdf and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001dc]:fnmadd.s fs9, ft8, ft10, fs9, dyn
	-[0x800001e0]:csrrs tp, fcsr, zero
	-[0x800001e4]:fsw fs9, 40(ra)
	-[0x800001e8]:sw tp, 44(ra)
Current Store : [0x800001e8] : sw tp, 44(ra) -- Store: [0x80004c40]:0x00000003




Last Coverpoint : ['rs1 : f24', 'rs2 : f24', 'rd : f24', 'rs3 : f24', 'rs1 == rs2 == rs3 == rd']
Last Code Sequence : 
	-[0x80000200]:fnmadd.s fs8, fs8, fs8, fs8, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:fsw fs8, 48(ra)
	-[0x8000020c]:sw tp, 52(ra)
Current Store : [0x8000020c] : sw tp, 52(ra) -- Store: [0x80004c48]:0x00000027




Last Coverpoint : ['rs1 : f23', 'rs2 : f25', 'rd : f23', 'rs3 : f23', 'rs1 == rd == rs3 != rs2']
Last Code Sequence : 
	-[0x80000224]:fnmadd.s fs7, fs7, fs9, fs7, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs7, 56(ra)
	-[0x80000230]:sw tp, 60(ra)
Current Store : [0x80000230] : sw tp, 60(ra) -- Store: [0x80004c50]:0x00000043




Last Coverpoint : ['rs1 : f22', 'rs2 : f22', 'rd : f29', 'rs3 : f27', 'rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3']
Last Code Sequence : 
	-[0x80000248]:fnmadd.s ft9, fs6, fs6, fs11, dyn
	-[0x8000024c]:csrrs tp, fcsr, zero
	-[0x80000250]:fsw ft9, 64(ra)
	-[0x80000254]:sw tp, 68(ra)
Current Store : [0x80000254] : sw tp, 68(ra) -- Store: [0x80004c58]:0x00000067




Last Coverpoint : ['rs1 : f21', 'rs2 : f21', 'rd : f22', 'rs3 : f21', 'rs1 == rs2 == rs3 != rd']
Last Code Sequence : 
	-[0x8000026c]:fnmadd.s fs6, fs5, fs5, fs5, dyn
	-[0x80000270]:csrrs tp, fcsr, zero
	-[0x80000274]:fsw fs6, 72(ra)
	-[0x80000278]:sw tp, 76(ra)
Current Store : [0x80000278] : sw tp, 76(ra) -- Store: [0x80004c60]:0x00000087




Last Coverpoint : ['rs1 : f25', 'rs2 : f20', 'rd : f20', 'rs3 : f22', 'rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1', 'fs1 == 0 and fe1 == 0xf3 and fm1 == 0x319f1b and fs2 == 0 and fe2 == 0x89 and fm2 == 0x376ad3 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e85b6 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000290]:fnmadd.s fs4, fs9, fs4, fs6, dyn
	-[0x80000294]:csrrs tp, fcsr, zero
	-[0x80000298]:fsw fs4, 80(ra)
	-[0x8000029c]:sw tp, 84(ra)
Current Store : [0x8000029c] : sw tp, 84(ra) -- Store: [0x80004c68]:0x00000003




Last Coverpoint : ['rs1 : f20', 'rs2 : f23', 'rd : f21', 'rs3 : f19', 'rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd', 'fs1 == 0 and fe1 == 0xf3 and fm1 == 0x319f1b and fs2 == 0 and fe2 == 0x89 and fm2 == 0x376ad3 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e85b6 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002b4]:fnmadd.s fs5, fs4, fs7, fs3, dyn
	-[0x800002b8]:csrrs tp, fcsr, zero
	-[0x800002bc]:fsw fs5, 88(ra)
	-[0x800002c0]:sw tp, 92(ra)
Current Store : [0x800002c0] : sw tp, 92(ra) -- Store: [0x80004c70]:0x00000023




Last Coverpoint : ['rs1 : f18', 'rs2 : f17', 'rd : f19', 'rs3 : f20', 'fs1 == 0 and fe1 == 0xf3 and fm1 == 0x319f1b and fs2 == 0 and fe2 == 0x89 and fm2 == 0x376ad3 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e85b6 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002d8]:fnmadd.s fs3, fs2, fa7, fs4, dyn
	-[0x800002dc]:csrrs tp, fcsr, zero
	-[0x800002e0]:fsw fs3, 96(ra)
	-[0x800002e4]:sw tp, 100(ra)
Current Store : [0x800002e4] : sw tp, 100(ra) -- Store: [0x80004c78]:0x00000043




Last Coverpoint : ['rs1 : f17', 'rs2 : f19', 'rd : f18', 'rs3 : f16', 'fs1 == 0 and fe1 == 0xf3 and fm1 == 0x319f1b and fs2 == 0 and fe2 == 0x89 and fm2 == 0x376ad3 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e85b6 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002fc]:fnmadd.s fs2, fa7, fs3, fa6, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:fsw fs2, 104(ra)
	-[0x80000308]:sw tp, 108(ra)
Current Store : [0x80000308] : sw tp, 108(ra) -- Store: [0x80004c80]:0x00000063




Last Coverpoint : ['rs1 : f19', 'rs2 : f16', 'rd : f17', 'rs3 : f18', 'fs1 == 0 and fe1 == 0xf3 and fm1 == 0x319f1b and fs2 == 0 and fe2 == 0x89 and fm2 == 0x376ad3 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e85b6 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000320]:fnmadd.s fa7, fs3, fa6, fs2, dyn
	-[0x80000324]:csrrs tp, fcsr, zero
	-[0x80000328]:fsw fa7, 112(ra)
	-[0x8000032c]:sw tp, 116(ra)
Current Store : [0x8000032c] : sw tp, 116(ra) -- Store: [0x80004c88]:0x00000083




Last Coverpoint : ['rs1 : f15', 'rs2 : f18', 'rd : f16', 'rs3 : f17', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3493df and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4ecb22 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x11de47 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fnmadd.s fa6, fa5, fs2, fa7, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa6, 120(ra)
	-[0x80000350]:sw tp, 124(ra)
Current Store : [0x80000350] : sw tp, 124(ra) -- Store: [0x80004c90]:0x00000007




Last Coverpoint : ['rs1 : f16', 'rs2 : f14', 'rd : f15', 'rs3 : f13', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3493df and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4ecb22 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x11de47 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000368]:fnmadd.s fa5, fa6, fa4, fa3, dyn
	-[0x8000036c]:csrrs tp, fcsr, zero
	-[0x80000370]:fsw fa5, 128(ra)
	-[0x80000374]:sw tp, 132(ra)
Current Store : [0x80000374] : sw tp, 132(ra) -- Store: [0x80004c98]:0x00000027




Last Coverpoint : ['rs1 : f13', 'rs2 : f15', 'rd : f14', 'rs3 : f12', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3493df and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4ecb22 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x11de47 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000038c]:fnmadd.s fa4, fa3, fa5, fa2, dyn
	-[0x80000390]:csrrs tp, fcsr, zero
	-[0x80000394]:fsw fa4, 136(ra)
	-[0x80000398]:sw tp, 140(ra)
Current Store : [0x80000398] : sw tp, 140(ra) -- Store: [0x80004ca0]:0x00000047




Last Coverpoint : ['rs1 : f14', 'rs2 : f12', 'rd : f13', 'rs3 : f15', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3493df and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4ecb22 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x11de47 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b0]:fnmadd.s fa3, fa4, fa2, fa5, dyn
	-[0x800003b4]:csrrs tp, fcsr, zero
	-[0x800003b8]:fsw fa3, 144(ra)
	-[0x800003bc]:sw tp, 148(ra)
Current Store : [0x800003bc] : sw tp, 148(ra) -- Store: [0x80004ca8]:0x00000067




Last Coverpoint : ['rs1 : f11', 'rs2 : f13', 'rd : f12', 'rs3 : f14', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x3493df and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4ecb22 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x11de47 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fnmadd.s fa2, fa1, fa3, fa4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:fsw fa2, 152(ra)
	-[0x800003e0]:sw tp, 156(ra)
Current Store : [0x800003e0] : sw tp, 156(ra) -- Store: [0x80004cb0]:0x00000087




Last Coverpoint : ['rs1 : f12', 'rs2 : f10', 'rd : f11', 'rs3 : f9', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f4b27 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0415cd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x13dde9 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f8]:fnmadd.s fa1, fa2, fa0, fs1, dyn
	-[0x800003fc]:csrrs tp, fcsr, zero
	-[0x80000400]:fsw fa1, 160(ra)
	-[0x80000404]:sw tp, 164(ra)
Current Store : [0x80000404] : sw tp, 164(ra) -- Store: [0x80004cb8]:0x00000007




Last Coverpoint : ['rs1 : f9', 'rs2 : f11', 'rd : f10', 'rs3 : f8', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f4b27 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0415cd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x13dde9 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000041c]:fnmadd.s fa0, fs1, fa1, fs0, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:fsw fa0, 168(ra)
	-[0x80000428]:sw tp, 172(ra)
Current Store : [0x80000428] : sw tp, 172(ra) -- Store: [0x80004cc0]:0x00000027




Last Coverpoint : ['rs1 : f10', 'rs2 : f8', 'rd : f9', 'rs3 : f11', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f4b27 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0415cd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x13dde9 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000440]:fnmadd.s fs1, fa0, fs0, fa1, dyn
	-[0x80000444]:csrrs tp, fcsr, zero
	-[0x80000448]:fsw fs1, 176(ra)
	-[0x8000044c]:sw tp, 180(ra)
Current Store : [0x8000044c] : sw tp, 180(ra) -- Store: [0x80004cc8]:0x00000047




Last Coverpoint : ['rs1 : f7', 'rs2 : f9', 'rd : f8', 'rs3 : f10', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f4b27 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0415cd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x13dde9 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000464]:fnmadd.s fs0, ft7, fs1, fa0, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw fs0, 184(ra)
	-[0x80000470]:sw tp, 188(ra)
Current Store : [0x80000470] : sw tp, 188(ra) -- Store: [0x80004cd0]:0x00000067




Last Coverpoint : ['rs1 : f8', 'rs2 : f6', 'rd : f7', 'rs3 : f5', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0f4b27 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x0415cd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x13dde9 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000488]:fnmadd.s ft7, fs0, ft6, ft5, dyn
	-[0x8000048c]:csrrs tp, fcsr, zero
	-[0x80000490]:fsw ft7, 192(ra)
	-[0x80000494]:sw tp, 196(ra)
Current Store : [0x80000494] : sw tp, 196(ra) -- Store: [0x80004cd8]:0x00000087




Last Coverpoint : ['rs1 : f5', 'rs2 : f7', 'rd : f6', 'rs3 : f4', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x516e9f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c33f6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19f7bf and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004ac]:fnmadd.s ft6, ft5, ft7, ft4, dyn
	-[0x800004b0]:csrrs tp, fcsr, zero
	-[0x800004b4]:fsw ft6, 200(ra)
	-[0x800004b8]:sw tp, 204(ra)
Current Store : [0x800004b8] : sw tp, 204(ra) -- Store: [0x80004ce0]:0x00000007




Last Coverpoint : ['rs1 : f6', 'rs2 : f4', 'rd : f5', 'rs3 : f7', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x516e9f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c33f6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19f7bf and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d0]:fnmadd.s ft5, ft6, ft4, ft7, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:fsw ft5, 208(ra)
	-[0x800004dc]:sw tp, 212(ra)
Current Store : [0x800004dc] : sw tp, 212(ra) -- Store: [0x80004ce8]:0x00000027




Last Coverpoint : ['rs1 : f3', 'rs2 : f5', 'rd : f4', 'rs3 : f6', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x516e9f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c33f6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19f7bf and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004f4]:fnmadd.s ft4, ft3, ft5, ft6, dyn
	-[0x800004f8]:csrrs tp, fcsr, zero
	-[0x800004fc]:fsw ft4, 216(ra)
	-[0x80000500]:sw tp, 220(ra)
Current Store : [0x80000500] : sw tp, 220(ra) -- Store: [0x80004cf0]:0x00000047




Last Coverpoint : ['rs1 : f4', 'rs2 : f2', 'rd : f3', 'rs3 : f1', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x516e9f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c33f6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19f7bf and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000518]:fnmadd.s ft3, ft4, ft2, ft1, dyn
	-[0x8000051c]:csrrs tp, fcsr, zero
	-[0x80000520]:fsw ft3, 224(ra)
	-[0x80000524]:sw tp, 228(ra)
Current Store : [0x80000524] : sw tp, 228(ra) -- Store: [0x80004cf8]:0x00000067




Last Coverpoint : ['rs1 : f1', 'rs2 : f3', 'rd : f2', 'rs3 : f0', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x516e9f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3c33f6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19f7bf and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000053c]:fnmadd.s ft2, ft1, ft3, ft0, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:fsw ft2, 232(ra)
	-[0x80000548]:sw tp, 236(ra)
Current Store : [0x80000548] : sw tp, 236(ra) -- Store: [0x80004d00]:0x00000087




Last Coverpoint : ['rs1 : f2', 'rs2 : f0', 'rd : f1', 'rs3 : f3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c2059 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11387d and fs3 == 0 and fe3 == 0xfd and fm3 == 0x1efa7c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000560]:fnmadd.s ft1, ft2, ft0, ft3, dyn
	-[0x80000564]:csrrs tp, fcsr, zero
	-[0x80000568]:fsw ft1, 240(ra)
	-[0x8000056c]:sw tp, 244(ra)
Current Store : [0x8000056c] : sw tp, 244(ra) -- Store: [0x80004d08]:0x00000003




Last Coverpoint : ['rs1 : f0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c2059 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11387d and fs3 == 0 and fe3 == 0xfd and fm3 == 0x1efa7c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000584]:fnmadd.s ft11, ft0, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 248(ra)
	-[0x80000590]:sw tp, 252(ra)
Current Store : [0x80000590] : sw tp, 252(ra) -- Store: [0x80004d10]:0x00000023




Last Coverpoint : ['rs2 : f1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c2059 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11387d and fs3 == 0 and fe3 == 0xfd and fm3 == 0x1efa7c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005a8]:fnmadd.s ft11, ft10, ft1, ft9, dyn
	-[0x800005ac]:csrrs tp, fcsr, zero
	-[0x800005b0]:fsw ft11, 256(ra)
	-[0x800005b4]:sw tp, 260(ra)
Current Store : [0x800005b4] : sw tp, 260(ra) -- Store: [0x80004d18]:0x00000043




Last Coverpoint : ['rs3 : f2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c2059 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11387d and fs3 == 0 and fe3 == 0xfd and fm3 == 0x1efa7c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005cc]:fnmadd.s ft11, ft10, ft9, ft2, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:fsw ft11, 264(ra)
	-[0x800005d8]:sw tp, 268(ra)
Current Store : [0x800005d8] : sw tp, 268(ra) -- Store: [0x80004d20]:0x00000063




Last Coverpoint : ['rd : f0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0c2059 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x11387d and fs3 == 0 and fe3 == 0xfd and fm3 == 0x1efa7c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005f0]:fnmadd.s ft0, ft11, ft10, ft9, dyn
	-[0x800005f4]:csrrs tp, fcsr, zero
	-[0x800005f8]:fsw ft0, 272(ra)
	-[0x800005fc]:sw tp, 276(ra)
Current Store : [0x800005fc] : sw tp, 276(ra) -- Store: [0x80004d28]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4eb58d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x222e36 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x02f434 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000614]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000618]:csrrs tp, fcsr, zero
	-[0x8000061c]:fsw ft11, 280(ra)
	-[0x80000620]:sw tp, 284(ra)
Current Store : [0x80000620] : sw tp, 284(ra) -- Store: [0x80004d30]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4eb58d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x222e36 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x02f434 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000638]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000063c]:csrrs tp, fcsr, zero
	-[0x80000640]:fsw ft11, 288(ra)
	-[0x80000644]:sw tp, 292(ra)
Current Store : [0x80000644] : sw tp, 292(ra) -- Store: [0x80004d38]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4eb58d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x222e36 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x02f434 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000065c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:fsw ft11, 296(ra)
	-[0x80000668]:sw tp, 300(ra)
Current Store : [0x80000668] : sw tp, 300(ra) -- Store: [0x80004d40]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4eb58d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x222e36 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x02f434 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000680]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000684]:csrrs tp, fcsr, zero
	-[0x80000688]:fsw ft11, 304(ra)
	-[0x8000068c]:sw tp, 308(ra)
Current Store : [0x8000068c] : sw tp, 308(ra) -- Store: [0x80004d48]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x4eb58d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x222e36 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x02f434 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft11, 312(ra)
	-[0x800006b0]:sw tp, 316(ra)
Current Store : [0x800006b0] : sw tp, 316(ra) -- Store: [0x80004d50]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x14365e and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3798bb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x549695 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006c8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006cc]:csrrs tp, fcsr, zero
	-[0x800006d0]:fsw ft11, 320(ra)
	-[0x800006d4]:sw tp, 324(ra)
Current Store : [0x800006d4] : sw tp, 324(ra) -- Store: [0x80004d58]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x14365e and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3798bb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x549695 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800006f0]:csrrs tp, fcsr, zero
	-[0x800006f4]:fsw ft11, 328(ra)
	-[0x800006f8]:sw tp, 332(ra)
Current Store : [0x800006f8] : sw tp, 332(ra) -- Store: [0x80004d60]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x14365e and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3798bb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x549695 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000710]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000714]:csrrs tp, fcsr, zero
	-[0x80000718]:fsw ft11, 336(ra)
	-[0x8000071c]:sw tp, 340(ra)
Current Store : [0x8000071c] : sw tp, 340(ra) -- Store: [0x80004d68]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x14365e and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3798bb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x549695 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000734]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000738]:csrrs tp, fcsr, zero
	-[0x8000073c]:fsw ft11, 344(ra)
	-[0x80000740]:sw tp, 348(ra)
Current Store : [0x80000740] : sw tp, 348(ra) -- Store: [0x80004d70]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x14365e and fs2 == 0 and fe2 == 0x80 and fm2 == 0x3798bb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x549695 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000758]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000075c]:csrrs tp, fcsr, zero
	-[0x80000760]:fsw ft11, 352(ra)
	-[0x80000764]:sw tp, 356(ra)
Current Store : [0x80000764] : sw tp, 356(ra) -- Store: [0x80004d78]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a9574 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x28da1a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x4beb8f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000077c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000780]:csrrs tp, fcsr, zero
	-[0x80000784]:fsw ft11, 360(ra)
	-[0x80000788]:sw tp, 364(ra)
Current Store : [0x80000788] : sw tp, 364(ra) -- Store: [0x80004d80]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a9574 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x28da1a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x4beb8f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007a4]:csrrs tp, fcsr, zero
	-[0x800007a8]:fsw ft11, 368(ra)
	-[0x800007ac]:sw tp, 372(ra)
Current Store : [0x800007ac] : sw tp, 372(ra) -- Store: [0x80004d88]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a9574 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x28da1a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x4beb8f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 376(ra)
	-[0x800007d0]:sw tp, 380(ra)
Current Store : [0x800007d0] : sw tp, 380(ra) -- Store: [0x80004d90]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a9574 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x28da1a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x4beb8f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800007ec]:csrrs tp, fcsr, zero
	-[0x800007f0]:fsw ft11, 384(ra)
	-[0x800007f4]:sw tp, 388(ra)
Current Store : [0x800007f4] : sw tp, 388(ra) -- Store: [0x80004d98]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a9574 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x28da1a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x4beb8f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000080c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000810]:csrrs tp, fcsr, zero
	-[0x80000814]:fsw ft11, 392(ra)
	-[0x80000818]:sw tp, 396(ra)
Current Store : [0x80000818] : sw tp, 396(ra) -- Store: [0x80004da0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a3f02 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5a8069 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x03a707 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000830]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000834]:csrrs tp, fcsr, zero
	-[0x80000838]:fsw ft11, 400(ra)
	-[0x8000083c]:sw tp, 404(ra)
Current Store : [0x8000083c] : sw tp, 404(ra) -- Store: [0x80004da8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a3f02 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5a8069 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x03a707 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000854]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000858]:csrrs tp, fcsr, zero
	-[0x8000085c]:fsw ft11, 408(ra)
	-[0x80000860]:sw tp, 412(ra)
Current Store : [0x80000860] : sw tp, 412(ra) -- Store: [0x80004db0]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a3f02 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5a8069 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x03a707 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000878]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000087c]:csrrs tp, fcsr, zero
	-[0x80000880]:fsw ft11, 416(ra)
	-[0x80000884]:sw tp, 420(ra)
Current Store : [0x80000884] : sw tp, 420(ra) -- Store: [0x80004db8]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a3f02 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5a8069 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x03a707 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000089c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008a0]:csrrs tp, fcsr, zero
	-[0x800008a4]:fsw ft11, 424(ra)
	-[0x800008a8]:sw tp, 428(ra)
Current Store : [0x800008a8] : sw tp, 428(ra) -- Store: [0x80004dc0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a3f02 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5a8069 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x03a707 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008c4]:csrrs tp, fcsr, zero
	-[0x800008c8]:fsw ft11, 432(ra)
	-[0x800008cc]:sw tp, 436(ra)
Current Store : [0x800008cc] : sw tp, 436(ra) -- Store: [0x80004dc8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x221a29 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x4b81fb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x00dd0d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 440(ra)
	-[0x800008f0]:sw tp, 444(ra)
Current Store : [0x800008f0] : sw tp, 444(ra) -- Store: [0x80004dd0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x221a29 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x4b81fb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x00dd0d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000908]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000090c]:csrrs tp, fcsr, zero
	-[0x80000910]:fsw ft11, 448(ra)
	-[0x80000914]:sw tp, 452(ra)
Current Store : [0x80000914] : sw tp, 452(ra) -- Store: [0x80004dd8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x221a29 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x4b81fb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x00dd0d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000092c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000930]:csrrs tp, fcsr, zero
	-[0x80000934]:fsw ft11, 456(ra)
	-[0x80000938]:sw tp, 460(ra)
Current Store : [0x80000938] : sw tp, 460(ra) -- Store: [0x80004de0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x221a29 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x4b81fb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x00dd0d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000950]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000954]:csrrs tp, fcsr, zero
	-[0x80000958]:fsw ft11, 464(ra)
	-[0x8000095c]:sw tp, 468(ra)
Current Store : [0x8000095c] : sw tp, 468(ra) -- Store: [0x80004de8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x221a29 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x4b81fb and fs3 == 0 and fe3 == 0xfd and fm3 == 0x00dd0d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000974]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000978]:csrrs tp, fcsr, zero
	-[0x8000097c]:fsw ft11, 472(ra)
	-[0x80000980]:sw tp, 476(ra)
Current Store : [0x80000980] : sw tp, 476(ra) -- Store: [0x80004df0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x0f0e02 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x097bec and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19a77e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000998]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000099c]:csrrs tp, fcsr, zero
	-[0x800009a0]:fsw ft11, 480(ra)
	-[0x800009a4]:sw tp, 484(ra)
Current Store : [0x800009a4] : sw tp, 484(ra) -- Store: [0x80004df8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x0f0e02 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x097bec and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19a77e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800009c0]:csrrs tp, fcsr, zero
	-[0x800009c4]:fsw ft11, 488(ra)
	-[0x800009c8]:sw tp, 492(ra)
Current Store : [0x800009c8] : sw tp, 492(ra) -- Store: [0x80004e00]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x0f0e02 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x097bec and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19a77e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800009e4]:csrrs tp, fcsr, zero
	-[0x800009e8]:fsw ft11, 496(ra)
	-[0x800009ec]:sw tp, 500(ra)
Current Store : [0x800009ec] : sw tp, 500(ra) -- Store: [0x80004e08]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x0f0e02 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x097bec and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19a77e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a04]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 504(ra)
	-[0x80000a10]:sw tp, 508(ra)
Current Store : [0x80000a10] : sw tp, 508(ra) -- Store: [0x80004e10]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x0f0e02 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x097bec and fs3 == 0 and fe3 == 0xfe and fm3 == 0x19a77e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a28]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a2c]:csrrs tp, fcsr, zero
	-[0x80000a30]:fsw ft11, 512(ra)
	-[0x80000a34]:sw tp, 516(ra)
Current Store : [0x80000a34] : sw tp, 516(ra) -- Store: [0x80004e18]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a849e and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0d9f1b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2af628 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a50]:csrrs tp, fcsr, zero
	-[0x80000a54]:fsw ft11, 520(ra)
	-[0x80000a58]:sw tp, 524(ra)
Current Store : [0x80000a58] : sw tp, 524(ra) -- Store: [0x80004e20]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a849e and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0d9f1b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2af628 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a74]:csrrs tp, fcsr, zero
	-[0x80000a78]:fsw ft11, 528(ra)
	-[0x80000a7c]:sw tp, 532(ra)
Current Store : [0x80000a7c] : sw tp, 532(ra) -- Store: [0x80004e28]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a849e and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0d9f1b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2af628 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a94]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000a98]:csrrs tp, fcsr, zero
	-[0x80000a9c]:fsw ft11, 536(ra)
	-[0x80000aa0]:sw tp, 540(ra)
Current Store : [0x80000aa0] : sw tp, 540(ra) -- Store: [0x80004e30]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a849e and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0d9f1b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2af628 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ab8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000abc]:csrrs tp, fcsr, zero
	-[0x80000ac0]:fsw ft11, 544(ra)
	-[0x80000ac4]:sw tp, 548(ra)
Current Store : [0x80000ac4] : sw tp, 548(ra) -- Store: [0x80004e38]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a849e and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0d9f1b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2af628 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000adc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ae0]:csrrs tp, fcsr, zero
	-[0x80000ae4]:fsw ft11, 552(ra)
	-[0x80000ae8]:sw tp, 556(ra)
Current Store : [0x80000ae8] : sw tp, 556(ra) -- Store: [0x80004e40]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x100bf1 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x068d67 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x176ba2 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b00]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b04]:csrrs tp, fcsr, zero
	-[0x80000b08]:fsw ft11, 560(ra)
	-[0x80000b0c]:sw tp, 564(ra)
Current Store : [0x80000b0c] : sw tp, 564(ra) -- Store: [0x80004e48]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x100bf1 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x068d67 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x176ba2 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b28]:csrrs tp, fcsr, zero
	-[0x80000b2c]:fsw ft11, 568(ra)
	-[0x80000b30]:sw tp, 572(ra)
Current Store : [0x80000b30] : sw tp, 572(ra) -- Store: [0x80004e50]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x100bf1 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x068d67 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x176ba2 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b48]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b4c]:csrrs tp, fcsr, zero
	-[0x80000b50]:fsw ft11, 576(ra)
	-[0x80000b54]:sw tp, 580(ra)
Current Store : [0x80000b54] : sw tp, 580(ra) -- Store: [0x80004e58]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x100bf1 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x068d67 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x176ba2 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b70]:csrrs tp, fcsr, zero
	-[0x80000b74]:fsw ft11, 584(ra)
	-[0x80000b78]:sw tp, 588(ra)
Current Store : [0x80000b78] : sw tp, 588(ra) -- Store: [0x80004e60]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x100bf1 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x068d67 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x176ba2 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b90]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000b94]:csrrs tp, fcsr, zero
	-[0x80000b98]:fsw ft11, 592(ra)
	-[0x80000b9c]:sw tp, 596(ra)
Current Store : [0x80000b9c] : sw tp, 596(ra) -- Store: [0x80004e68]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73cf0f and fs2 == 0 and fe2 == 0x7d and fm2 == 0x6acfa3 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x5fa103 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000bb8]:csrrs tp, fcsr, zero
	-[0x80000bbc]:fsw ft11, 600(ra)
	-[0x80000bc0]:sw tp, 604(ra)
Current Store : [0x80000bc0] : sw tp, 604(ra) -- Store: [0x80004e70]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73cf0f and fs2 == 0 and fe2 == 0x7d and fm2 == 0x6acfa3 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x5fa103 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bd8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000bdc]:csrrs tp, fcsr, zero
	-[0x80000be0]:fsw ft11, 608(ra)
	-[0x80000be4]:sw tp, 612(ra)
Current Store : [0x80000be4] : sw tp, 612(ra) -- Store: [0x80004e78]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73cf0f and fs2 == 0 and fe2 == 0x7d and fm2 == 0x6acfa3 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x5fa103 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c00]:csrrs tp, fcsr, zero
	-[0x80000c04]:fsw ft11, 616(ra)
	-[0x80000c08]:sw tp, 620(ra)
Current Store : [0x80000c08] : sw tp, 620(ra) -- Store: [0x80004e80]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73cf0f and fs2 == 0 and fe2 == 0x7d and fm2 == 0x6acfa3 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x5fa103 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c24]:csrrs tp, fcsr, zero
	-[0x80000c28]:fsw ft11, 624(ra)
	-[0x80000c2c]:sw tp, 628(ra)
Current Store : [0x80000c2c] : sw tp, 628(ra) -- Store: [0x80004e88]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73cf0f and fs2 == 0 and fe2 == 0x7d and fm2 == 0x6acfa3 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x5fa103 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c44]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c48]:csrrs tp, fcsr, zero
	-[0x80000c4c]:fsw ft11, 632(ra)
	-[0x80000c50]:sw tp, 636(ra)
Current Store : [0x80000c50] : sw tp, 636(ra) -- Store: [0x80004e90]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36a9e2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x1e600b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x6202c3 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c6c]:csrrs tp, fcsr, zero
	-[0x80000c70]:fsw ft11, 640(ra)
	-[0x80000c74]:sw tp, 644(ra)
Current Store : [0x80000c74] : sw tp, 644(ra) -- Store: [0x80004e98]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36a9e2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x1e600b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x6202c3 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000c90]:csrrs tp, fcsr, zero
	-[0x80000c94]:fsw ft11, 648(ra)
	-[0x80000c98]:sw tp, 652(ra)
Current Store : [0x80000c98] : sw tp, 652(ra) -- Store: [0x80004ea0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36a9e2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x1e600b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x6202c3 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cb0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cb4]:csrrs tp, fcsr, zero
	-[0x80000cb8]:fsw ft11, 656(ra)
	-[0x80000cbc]:sw tp, 660(ra)
Current Store : [0x80000cbc] : sw tp, 660(ra) -- Store: [0x80004ea8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36a9e2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x1e600b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x6202c3 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cd8]:csrrs tp, fcsr, zero
	-[0x80000cdc]:fsw ft11, 664(ra)
	-[0x80000ce0]:sw tp, 668(ra)
Current Store : [0x80000ce0] : sw tp, 668(ra) -- Store: [0x80004eb0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x36a9e2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x1e600b and fs3 == 0 and fe3 == 0xfd and fm3 == 0x6202c3 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cf8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000cfc]:csrrs tp, fcsr, zero
	-[0x80000d00]:fsw ft11, 672(ra)
	-[0x80000d04]:sw tp, 676(ra)
Current Store : [0x80000d04] : sw tp, 676(ra) -- Store: [0x80004eb8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a457f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x50d100 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7bacd4 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d20]:csrrs tp, fcsr, zero
	-[0x80000d24]:fsw ft11, 680(ra)
	-[0x80000d28]:sw tp, 684(ra)
Current Store : [0x80000d28] : sw tp, 684(ra) -- Store: [0x80004ec0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a457f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x50d100 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7bacd4 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d40]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d44]:csrrs tp, fcsr, zero
	-[0x80000d48]:fsw ft11, 688(ra)
	-[0x80000d4c]:sw tp, 692(ra)
Current Store : [0x80000d4c] : sw tp, 692(ra) -- Store: [0x80004ec8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a457f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x50d100 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7bacd4 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d68]:csrrs tp, fcsr, zero
	-[0x80000d6c]:fsw ft11, 696(ra)
	-[0x80000d70]:sw tp, 700(ra)
Current Store : [0x80000d70] : sw tp, 700(ra) -- Store: [0x80004ed0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a457f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x50d100 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7bacd4 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000d8c]:csrrs tp, fcsr, zero
	-[0x80000d90]:fsw ft11, 704(ra)
	-[0x80000d94]:sw tp, 708(ra)
Current Store : [0x80000d94] : sw tp, 708(ra) -- Store: [0x80004ed8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a457f and fs2 == 0 and fe2 == 0x80 and fm2 == 0x50d100 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7bacd4 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000db0]:csrrs tp, fcsr, zero
	-[0x80000db4]:fsw ft11, 712(ra)
	-[0x80000db8]:sw tp, 716(ra)
Current Store : [0x80000db8] : sw tp, 716(ra) -- Store: [0x80004ee0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0a62c0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x25a532 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3315e5 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dd0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000dd4]:csrrs tp, fcsr, zero
	-[0x80000dd8]:fsw ft11, 720(ra)
	-[0x80000ddc]:sw tp, 724(ra)
Current Store : [0x80000ddc] : sw tp, 724(ra) -- Store: [0x80004ee8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0a62c0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x25a532 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3315e5 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000df4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000df8]:csrrs tp, fcsr, zero
	-[0x80000dfc]:fsw ft11, 728(ra)
	-[0x80000e00]:sw tp, 732(ra)
Current Store : [0x80000e00] : sw tp, 732(ra) -- Store: [0x80004ef0]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0a62c0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x25a532 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3315e5 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e1c]:csrrs tp, fcsr, zero
	-[0x80000e20]:fsw ft11, 736(ra)
	-[0x80000e24]:sw tp, 740(ra)
Current Store : [0x80000e24] : sw tp, 740(ra) -- Store: [0x80004ef8]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0a62c0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x25a532 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3315e5 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e3c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e40]:csrrs tp, fcsr, zero
	-[0x80000e44]:fsw ft11, 744(ra)
	-[0x80000e48]:sw tp, 748(ra)
Current Store : [0x80000e48] : sw tp, 748(ra) -- Store: [0x80004f00]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0a62c0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x25a532 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3315e5 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e60]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e64]:csrrs tp, fcsr, zero
	-[0x80000e68]:fsw ft11, 752(ra)
	-[0x80000e6c]:sw tp, 756(ra)
Current Store : [0x80000e6c] : sw tp, 756(ra) -- Store: [0x80004f08]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ea04a and fs2 == 0 and fe2 == 0x7e and fm2 == 0x10dbda and fs3 == 0 and fe3 == 0xfd and fm3 == 0x45a044 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e84]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000e88]:csrrs tp, fcsr, zero
	-[0x80000e8c]:fsw ft11, 760(ra)
	-[0x80000e90]:sw tp, 764(ra)
Current Store : [0x80000e90] : sw tp, 764(ra) -- Store: [0x80004f10]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ea04a and fs2 == 0 and fe2 == 0x7e and fm2 == 0x10dbda and fs3 == 0 and fe3 == 0xfd and fm3 == 0x45a044 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000eac]:csrrs tp, fcsr, zero
	-[0x80000eb0]:fsw ft11, 768(ra)
	-[0x80000eb4]:sw tp, 772(ra)
Current Store : [0x80000eb4] : sw tp, 772(ra) -- Store: [0x80004f18]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ea04a and fs2 == 0 and fe2 == 0x7e and fm2 == 0x10dbda and fs3 == 0 and fe3 == 0xfd and fm3 == 0x45a044 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ed0]:csrrs tp, fcsr, zero
	-[0x80000ed4]:fsw ft11, 776(ra)
	-[0x80000ed8]:sw tp, 780(ra)
Current Store : [0x80000ed8] : sw tp, 780(ra) -- Store: [0x80004f20]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ea04a and fs2 == 0 and fe2 == 0x7e and fm2 == 0x10dbda and fs3 == 0 and fe3 == 0xfd and fm3 == 0x45a044 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ef4]:csrrs tp, fcsr, zero
	-[0x80000ef8]:fsw ft11, 784(ra)
	-[0x80000efc]:sw tp, 788(ra)
Current Store : [0x80000efc] : sw tp, 788(ra) -- Store: [0x80004f28]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ea04a and fs2 == 0 and fe2 == 0x7e and fm2 == 0x10dbda and fs3 == 0 and fe3 == 0xfd and fm3 == 0x45a044 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f18]:csrrs tp, fcsr, zero
	-[0x80000f1c]:fsw ft11, 792(ra)
	-[0x80000f20]:sw tp, 796(ra)
Current Store : [0x80000f20] : sw tp, 796(ra) -- Store: [0x80004f30]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7783fc and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a9790 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x05ffb0 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f3c]:csrrs tp, fcsr, zero
	-[0x80000f40]:fsw ft11, 800(ra)
	-[0x80000f44]:sw tp, 804(ra)
Current Store : [0x80000f44] : sw tp, 804(ra) -- Store: [0x80004f38]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7783fc and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a9790 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x05ffb0 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f5c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f60]:csrrs tp, fcsr, zero
	-[0x80000f64]:fsw ft11, 808(ra)
	-[0x80000f68]:sw tp, 812(ra)
Current Store : [0x80000f68] : sw tp, 812(ra) -- Store: [0x80004f40]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7783fc and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a9790 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x05ffb0 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f80]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000f84]:csrrs tp, fcsr, zero
	-[0x80000f88]:fsw ft11, 816(ra)
	-[0x80000f8c]:sw tp, 820(ra)
Current Store : [0x80000f8c] : sw tp, 820(ra) -- Store: [0x80004f48]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7783fc and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a9790 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x05ffb0 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fa4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000fa8]:csrrs tp, fcsr, zero
	-[0x80000fac]:fsw ft11, 824(ra)
	-[0x80000fb0]:sw tp, 828(ra)
Current Store : [0x80000fb0] : sw tp, 828(ra) -- Store: [0x80004f50]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7783fc and fs2 == 0 and fe2 == 0x7f and fm2 == 0x0a9790 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x05ffb0 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000fcc]:csrrs tp, fcsr, zero
	-[0x80000fd0]:fsw ft11, 832(ra)
	-[0x80000fd4]:sw tp, 836(ra)
Current Store : [0x80000fd4] : sw tp, 836(ra) -- Store: [0x80004f58]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1aa82d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1b2701 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3b76c8 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80000ff0]:csrrs tp, fcsr, zero
	-[0x80000ff4]:fsw ft11, 840(ra)
	-[0x80000ff8]:sw tp, 844(ra)
Current Store : [0x80000ff8] : sw tp, 844(ra) -- Store: [0x80004f60]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1aa82d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1b2701 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3b76c8 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001010]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001014]:csrrs tp, fcsr, zero
	-[0x80001018]:fsw ft11, 848(ra)
	-[0x8000101c]:sw tp, 852(ra)
Current Store : [0x8000101c] : sw tp, 852(ra) -- Store: [0x80004f68]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1aa82d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1b2701 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3b76c8 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001034]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001038]:csrrs tp, fcsr, zero
	-[0x8000103c]:fsw ft11, 856(ra)
	-[0x80001040]:sw tp, 860(ra)
Current Store : [0x80001040] : sw tp, 860(ra) -- Store: [0x80004f70]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1aa82d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1b2701 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3b76c8 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001058]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000105c]:csrrs tp, fcsr, zero
	-[0x80001060]:fsw ft11, 864(ra)
	-[0x80001064]:sw tp, 868(ra)
Current Store : [0x80001064] : sw tp, 868(ra) -- Store: [0x80004f78]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1aa82d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x1b2701 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3b76c8 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000107c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001080]:csrrs tp, fcsr, zero
	-[0x80001084]:fsw ft11, 872(ra)
	-[0x80001088]:sw tp, 876(ra)
Current Store : [0x80001088] : sw tp, 876(ra) -- Store: [0x80004f80]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2e9fe8 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x4b6f55 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x0ac4bf and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010a4]:csrrs tp, fcsr, zero
	-[0x800010a8]:fsw ft11, 880(ra)
	-[0x800010ac]:sw tp, 884(ra)
Current Store : [0x800010ac] : sw tp, 884(ra) -- Store: [0x80004f88]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2e9fe8 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x4b6f55 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x0ac4bf and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010c4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010c8]:csrrs tp, fcsr, zero
	-[0x800010cc]:fsw ft11, 888(ra)
	-[0x800010d0]:sw tp, 892(ra)
Current Store : [0x800010d0] : sw tp, 892(ra) -- Store: [0x80004f90]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2e9fe8 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x4b6f55 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x0ac4bf and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800010ec]:csrrs tp, fcsr, zero
	-[0x800010f0]:fsw ft11, 896(ra)
	-[0x800010f4]:sw tp, 900(ra)
Current Store : [0x800010f4] : sw tp, 900(ra) -- Store: [0x80004f98]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2e9fe8 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x4b6f55 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x0ac4bf and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000110c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001110]:csrrs tp, fcsr, zero
	-[0x80001114]:fsw ft11, 904(ra)
	-[0x80001118]:sw tp, 908(ra)
Current Store : [0x80001118] : sw tp, 908(ra) -- Store: [0x80004fa0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2e9fe8 and fs2 == 0 and fe2 == 0x82 and fm2 == 0x4b6f55 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x0ac4bf and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001130]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001134]:csrrs tp, fcsr, zero
	-[0x80001138]:fsw ft11, 912(ra)
	-[0x8000113c]:sw tp, 916(ra)
Current Store : [0x8000113c] : sw tp, 916(ra) -- Store: [0x80004fa8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x671228 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0177d2 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x69b89c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001154]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001158]:csrrs tp, fcsr, zero
	-[0x8000115c]:fsw ft11, 920(ra)
	-[0x80001160]:sw tp, 924(ra)
Current Store : [0x80001160] : sw tp, 924(ra) -- Store: [0x80004fb0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x671228 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0177d2 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x69b89c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001178]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000117c]:csrrs tp, fcsr, zero
	-[0x80001180]:fsw ft11, 928(ra)
	-[0x80001184]:sw tp, 932(ra)
Current Store : [0x80001184] : sw tp, 932(ra) -- Store: [0x80004fb8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x671228 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0177d2 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x69b89c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000119c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011a0]:csrrs tp, fcsr, zero
	-[0x800011a4]:fsw ft11, 936(ra)
	-[0x800011a8]:sw tp, 940(ra)
Current Store : [0x800011a8] : sw tp, 940(ra) -- Store: [0x80004fc0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x671228 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0177d2 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x69b89c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011c4]:csrrs tp, fcsr, zero
	-[0x800011c8]:fsw ft11, 944(ra)
	-[0x800011cc]:sw tp, 948(ra)
Current Store : [0x800011cc] : sw tp, 948(ra) -- Store: [0x80004fc8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x671228 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0177d2 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x69b89c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800011e8]:csrrs tp, fcsr, zero
	-[0x800011ec]:fsw ft11, 952(ra)
	-[0x800011f0]:sw tp, 956(ra)
Current Store : [0x800011f0] : sw tp, 956(ra) -- Store: [0x80004fd0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x623c76 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x1be23d and fs3 == 0 and fe3 == 0xfb and fm3 == 0x09c28b and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001208]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000120c]:csrrs tp, fcsr, zero
	-[0x80001210]:fsw ft11, 960(ra)
	-[0x80001214]:sw tp, 964(ra)
Current Store : [0x80001214] : sw tp, 964(ra) -- Store: [0x80004fd8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x623c76 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x1be23d and fs3 == 0 and fe3 == 0xfb and fm3 == 0x09c28b and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000122c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001230]:csrrs tp, fcsr, zero
	-[0x80001234]:fsw ft11, 968(ra)
	-[0x80001238]:sw tp, 972(ra)
Current Store : [0x80001238] : sw tp, 972(ra) -- Store: [0x80004fe0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x623c76 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x1be23d and fs3 == 0 and fe3 == 0xfb and fm3 == 0x09c28b and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001250]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001254]:csrrs tp, fcsr, zero
	-[0x80001258]:fsw ft11, 976(ra)
	-[0x8000125c]:sw tp, 980(ra)
Current Store : [0x8000125c] : sw tp, 980(ra) -- Store: [0x80004fe8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x623c76 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x1be23d and fs3 == 0 and fe3 == 0xfb and fm3 == 0x09c28b and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001274]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001278]:csrrs tp, fcsr, zero
	-[0x8000127c]:fsw ft11, 984(ra)
	-[0x80001280]:sw tp, 988(ra)
Current Store : [0x80001280] : sw tp, 988(ra) -- Store: [0x80004ff0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x623c76 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x1be23d and fs3 == 0 and fe3 == 0xfb and fm3 == 0x09c28b and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001298]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000129c]:csrrs tp, fcsr, zero
	-[0x800012a0]:fsw ft11, 992(ra)
	-[0x800012a4]:sw tp, 996(ra)
Current Store : [0x800012a4] : sw tp, 996(ra) -- Store: [0x80004ff8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28f9a2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x18ca47 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x49b378 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800012c0]:csrrs tp, fcsr, zero
	-[0x800012c4]:fsw ft11, 1000(ra)
	-[0x800012c8]:sw tp, 1004(ra)
Current Store : [0x800012c8] : sw tp, 1004(ra) -- Store: [0x80005000]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28f9a2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x18ca47 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x49b378 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800012e4]:csrrs tp, fcsr, zero
	-[0x800012e8]:fsw ft11, 1008(ra)
	-[0x800012ec]:sw tp, 1012(ra)
Current Store : [0x800012ec] : sw tp, 1012(ra) -- Store: [0x80005008]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28f9a2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x18ca47 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x49b378 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001304]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001308]:csrrs tp, fcsr, zero
	-[0x8000130c]:fsw ft11, 1016(ra)
	-[0x80001310]:sw tp, 1020(ra)
Current Store : [0x80001310] : sw tp, 1020(ra) -- Store: [0x80005010]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28f9a2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x18ca47 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x49b378 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001330]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001334]:csrrs tp, fcsr, zero
	-[0x80001338]:fsw ft11, 0(ra)
	-[0x8000133c]:sw tp, 4(ra)
Current Store : [0x8000133c] : sw tp, 4(ra) -- Store: [0x80005018]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28f9a2 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x18ca47 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x49b378 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001358]:csrrs tp, fcsr, zero
	-[0x8000135c]:fsw ft11, 8(ra)
	-[0x80001360]:sw tp, 12(ra)
Current Store : [0x80001360] : sw tp, 12(ra) -- Store: [0x80005020]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x337137 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3588f6 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e7e1b and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001378]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000137c]:csrrs tp, fcsr, zero
	-[0x80001380]:fsw ft11, 16(ra)
	-[0x80001384]:sw tp, 20(ra)
Current Store : [0x80001384] : sw tp, 20(ra) -- Store: [0x80005028]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x337137 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3588f6 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e7e1b and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000139c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013a0]:csrrs tp, fcsr, zero
	-[0x800013a4]:fsw ft11, 24(ra)
	-[0x800013a8]:sw tp, 28(ra)
Current Store : [0x800013a8] : sw tp, 28(ra) -- Store: [0x80005030]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x337137 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3588f6 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e7e1b and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013c0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013c4]:csrrs tp, fcsr, zero
	-[0x800013c8]:fsw ft11, 32(ra)
	-[0x800013cc]:sw tp, 36(ra)
Current Store : [0x800013cc] : sw tp, 36(ra) -- Store: [0x80005038]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x337137 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3588f6 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e7e1b and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800013e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800013e8]:csrrs tp, fcsr, zero
	-[0x800013ec]:fsw ft11, 40(ra)
	-[0x800013f0]:sw tp, 44(ra)
Current Store : [0x800013f0] : sw tp, 44(ra) -- Store: [0x80005040]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x337137 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3588f6 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x7e7e1b and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001408]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000140c]:csrrs tp, fcsr, zero
	-[0x80001410]:fsw ft11, 48(ra)
	-[0x80001414]:sw tp, 52(ra)
Current Store : [0x80001414] : sw tp, 52(ra) -- Store: [0x80005048]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6fd4df and fs2 == 0 and fe2 == 0x7e and fm2 == 0x20dc16 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x16b33b and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000142c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001430]:csrrs tp, fcsr, zero
	-[0x80001434]:fsw ft11, 56(ra)
	-[0x80001438]:sw tp, 60(ra)
Current Store : [0x80001438] : sw tp, 60(ra) -- Store: [0x80005050]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6fd4df and fs2 == 0 and fe2 == 0x7e and fm2 == 0x20dc16 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x16b33b and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001454]:csrrs tp, fcsr, zero
	-[0x80001458]:fsw ft11, 64(ra)
	-[0x8000145c]:sw tp, 68(ra)
Current Store : [0x8000145c] : sw tp, 68(ra) -- Store: [0x80005058]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6fd4df and fs2 == 0 and fe2 == 0x7e and fm2 == 0x20dc16 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x16b33b and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001474]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001478]:csrrs tp, fcsr, zero
	-[0x8000147c]:fsw ft11, 72(ra)
	-[0x80001480]:sw tp, 76(ra)
Current Store : [0x80001480] : sw tp, 76(ra) -- Store: [0x80005060]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6fd4df and fs2 == 0 and fe2 == 0x7e and fm2 == 0x20dc16 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x16b33b and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001498]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000149c]:csrrs tp, fcsr, zero
	-[0x800014a0]:fsw ft11, 80(ra)
	-[0x800014a4]:sw tp, 84(ra)
Current Store : [0x800014a4] : sw tp, 84(ra) -- Store: [0x80005068]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6fd4df and fs2 == 0 and fe2 == 0x7e and fm2 == 0x20dc16 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x16b33b and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014bc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800014c0]:csrrs tp, fcsr, zero
	-[0x800014c4]:fsw ft11, 88(ra)
	-[0x800014c8]:sw tp, 92(ra)
Current Store : [0x800014c8] : sw tp, 92(ra) -- Store: [0x80005070]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c1fe and fs2 == 0 and fe2 == 0x7d and fm2 == 0x122c1f and fs3 == 0 and fe3 == 0xfb and fm3 == 0x54214e and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800014e0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800014e4]:csrrs tp, fcsr, zero
	-[0x800014e8]:fsw ft11, 96(ra)
	-[0x800014ec]:sw tp, 100(ra)
Current Store : [0x800014ec] : sw tp, 100(ra) -- Store: [0x80005078]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c1fe and fs2 == 0 and fe2 == 0x7d and fm2 == 0x122c1f and fs3 == 0 and fe3 == 0xfb and fm3 == 0x54214e and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001504]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001508]:csrrs tp, fcsr, zero
	-[0x8000150c]:fsw ft11, 104(ra)
	-[0x80001510]:sw tp, 108(ra)
Current Store : [0x80001510] : sw tp, 108(ra) -- Store: [0x80005080]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c1fe and fs2 == 0 and fe2 == 0x7d and fm2 == 0x122c1f and fs3 == 0 and fe3 == 0xfb and fm3 == 0x54214e and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001528]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000152c]:csrrs tp, fcsr, zero
	-[0x80001530]:fsw ft11, 112(ra)
	-[0x80001534]:sw tp, 116(ra)
Current Store : [0x80001534] : sw tp, 116(ra) -- Store: [0x80005088]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c1fe and fs2 == 0 and fe2 == 0x7d and fm2 == 0x122c1f and fs3 == 0 and fe3 == 0xfb and fm3 == 0x54214e and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000154c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001550]:csrrs tp, fcsr, zero
	-[0x80001554]:fsw ft11, 120(ra)
	-[0x80001558]:sw tp, 124(ra)
Current Store : [0x80001558] : sw tp, 124(ra) -- Store: [0x80005090]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x39c1fe and fs2 == 0 and fe2 == 0x7d and fm2 == 0x122c1f and fs3 == 0 and fe3 == 0xfb and fm3 == 0x54214e and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001570]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001574]:csrrs tp, fcsr, zero
	-[0x80001578]:fsw ft11, 128(ra)
	-[0x8000157c]:sw tp, 132(ra)
Current Store : [0x8000157c] : sw tp, 132(ra) -- Store: [0x80005098]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21311f and fs2 == 0 and fe2 == 0x81 and fm2 == 0x03f6ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x262efb and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001594]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001598]:csrrs tp, fcsr, zero
	-[0x8000159c]:fsw ft11, 136(ra)
	-[0x800015a0]:sw tp, 140(ra)
Current Store : [0x800015a0] : sw tp, 140(ra) -- Store: [0x800050a0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21311f and fs2 == 0 and fe2 == 0x81 and fm2 == 0x03f6ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x262efb and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015b8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800015bc]:csrrs tp, fcsr, zero
	-[0x800015c0]:fsw ft11, 144(ra)
	-[0x800015c4]:sw tp, 148(ra)
Current Store : [0x800015c4] : sw tp, 148(ra) -- Store: [0x800050a8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21311f and fs2 == 0 and fe2 == 0x81 and fm2 == 0x03f6ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x262efb and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800015dc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800015e0]:csrrs tp, fcsr, zero
	-[0x800015e4]:fsw ft11, 152(ra)
	-[0x800015e8]:sw tp, 156(ra)
Current Store : [0x800015e8] : sw tp, 156(ra) -- Store: [0x800050b0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21311f and fs2 == 0 and fe2 == 0x81 and fm2 == 0x03f6ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x262efb and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001600]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001604]:csrrs tp, fcsr, zero
	-[0x80001608]:fsw ft11, 160(ra)
	-[0x8000160c]:sw tp, 164(ra)
Current Store : [0x8000160c] : sw tp, 164(ra) -- Store: [0x800050b8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x21311f and fs2 == 0 and fe2 == 0x81 and fm2 == 0x03f6ba and fs3 == 0 and fe3 == 0xfe and fm3 == 0x262efb and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001624]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001628]:csrrs tp, fcsr, zero
	-[0x8000162c]:fsw ft11, 168(ra)
	-[0x80001630]:sw tp, 172(ra)
Current Store : [0x80001630] : sw tp, 172(ra) -- Store: [0x800050c0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x060f17 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x12ec96 and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x19e100 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001648]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000164c]:csrrs tp, fcsr, zero
	-[0x80001650]:fsw ft11, 176(ra)
	-[0x80001654]:sw tp, 180(ra)
Current Store : [0x80001654] : sw tp, 180(ra) -- Store: [0x800050c8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x060f17 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x12ec96 and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x19e100 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000166c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001670]:csrrs tp, fcsr, zero
	-[0x80001674]:fsw ft11, 184(ra)
	-[0x80001678]:sw tp, 188(ra)
Current Store : [0x80001678] : sw tp, 188(ra) -- Store: [0x800050d0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x060f17 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x12ec96 and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x19e100 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001690]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001694]:csrrs tp, fcsr, zero
	-[0x80001698]:fsw ft11, 192(ra)
	-[0x8000169c]:sw tp, 196(ra)
Current Store : [0x8000169c] : sw tp, 196(ra) -- Store: [0x800050d8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x060f17 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x12ec96 and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x19e100 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800016b8]:csrrs tp, fcsr, zero
	-[0x800016bc]:fsw ft11, 200(ra)
	-[0x800016c0]:sw tp, 204(ra)
Current Store : [0x800016c0] : sw tp, 204(ra) -- Store: [0x800050e0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x060f17 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x12ec96 and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x19e100 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016d8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800016dc]:csrrs tp, fcsr, zero
	-[0x800016e0]:fsw ft11, 208(ra)
	-[0x800016e4]:sw tp, 212(ra)
Current Store : [0x800016e4] : sw tp, 212(ra) -- Store: [0x800050e8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x156314 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x339b9f and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x519e2f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800016fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001700]:csrrs tp, fcsr, zero
	-[0x80001704]:fsw ft11, 216(ra)
	-[0x80001708]:sw tp, 220(ra)
Current Store : [0x80001708] : sw tp, 220(ra) -- Store: [0x800050f0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x156314 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x339b9f and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x519e2f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001720]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001724]:csrrs tp, fcsr, zero
	-[0x80001728]:fsw ft11, 224(ra)
	-[0x8000172c]:sw tp, 228(ra)
Current Store : [0x8000172c] : sw tp, 228(ra) -- Store: [0x800050f8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x156314 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x339b9f and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x519e2f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001744]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001748]:csrrs tp, fcsr, zero
	-[0x8000174c]:fsw ft11, 232(ra)
	-[0x80001750]:sw tp, 236(ra)
Current Store : [0x80001750] : sw tp, 236(ra) -- Store: [0x80005100]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x156314 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x339b9f and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x519e2f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001768]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000176c]:csrrs tp, fcsr, zero
	-[0x80001770]:fsw ft11, 240(ra)
	-[0x80001774]:sw tp, 244(ra)
Current Store : [0x80001774] : sw tp, 244(ra) -- Store: [0x80005108]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x156314 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x339b9f and fs3 == 0 and fe3 == 0xf8 and fm3 == 0x519e2f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000178c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001790]:csrrs tp, fcsr, zero
	-[0x80001794]:fsw ft11, 248(ra)
	-[0x80001798]:sw tp, 252(ra)
Current Store : [0x80001798] : sw tp, 252(ra) -- Store: [0x80005110]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7fad2c and fs2 == 0 and fe2 == 0x80 and fm2 == 0x2ddf2e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2da6ed and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800017b4]:csrrs tp, fcsr, zero
	-[0x800017b8]:fsw ft11, 256(ra)
	-[0x800017bc]:sw tp, 260(ra)
Current Store : [0x800017bc] : sw tp, 260(ra) -- Store: [0x80005118]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7fad2c and fs2 == 0 and fe2 == 0x80 and fm2 == 0x2ddf2e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2da6ed and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017d4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800017d8]:csrrs tp, fcsr, zero
	-[0x800017dc]:fsw ft11, 264(ra)
	-[0x800017e0]:sw tp, 268(ra)
Current Store : [0x800017e0] : sw tp, 268(ra) -- Store: [0x80005120]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7fad2c and fs2 == 0 and fe2 == 0x80 and fm2 == 0x2ddf2e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2da6ed and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800017f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800017fc]:csrrs tp, fcsr, zero
	-[0x80001800]:fsw ft11, 272(ra)
	-[0x80001804]:sw tp, 276(ra)
Current Store : [0x80001804] : sw tp, 276(ra) -- Store: [0x80005128]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7fad2c and fs2 == 0 and fe2 == 0x80 and fm2 == 0x2ddf2e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2da6ed and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000181c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001820]:csrrs tp, fcsr, zero
	-[0x80001824]:fsw ft11, 280(ra)
	-[0x80001828]:sw tp, 284(ra)
Current Store : [0x80001828] : sw tp, 284(ra) -- Store: [0x80005130]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7fad2c and fs2 == 0 and fe2 == 0x80 and fm2 == 0x2ddf2e and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2da6ed and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001840]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001844]:csrrs tp, fcsr, zero
	-[0x80001848]:fsw ft11, 288(ra)
	-[0x8000184c]:sw tp, 292(ra)
Current Store : [0x8000184c] : sw tp, 292(ra) -- Store: [0x80005138]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6e2e8d and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7cb106 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6b1a87 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001864]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001868]:csrrs tp, fcsr, zero
	-[0x8000186c]:fsw ft11, 296(ra)
	-[0x80001870]:sw tp, 300(ra)
Current Store : [0x80001870] : sw tp, 300(ra) -- Store: [0x80005140]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6e2e8d and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7cb106 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6b1a87 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001888]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000188c]:csrrs tp, fcsr, zero
	-[0x80001890]:fsw ft11, 304(ra)
	-[0x80001894]:sw tp, 308(ra)
Current Store : [0x80001894] : sw tp, 308(ra) -- Store: [0x80005148]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6e2e8d and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7cb106 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6b1a87 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800018b0]:csrrs tp, fcsr, zero
	-[0x800018b4]:fsw ft11, 312(ra)
	-[0x800018b8]:sw tp, 316(ra)
Current Store : [0x800018b8] : sw tp, 316(ra) -- Store: [0x80005150]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6e2e8d and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7cb106 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6b1a87 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800018d4]:csrrs tp, fcsr, zero
	-[0x800018d8]:fsw ft11, 320(ra)
	-[0x800018dc]:sw tp, 324(ra)
Current Store : [0x800018dc] : sw tp, 324(ra) -- Store: [0x80005158]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6e2e8d and fs2 == 0 and fe2 == 0x7d and fm2 == 0x7cb106 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x6b1a87 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800018f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800018f8]:csrrs tp, fcsr, zero
	-[0x800018fc]:fsw ft11, 328(ra)
	-[0x80001900]:sw tp, 332(ra)
Current Store : [0x80001900] : sw tp, 332(ra) -- Store: [0x80005160]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x10ec66 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x03fd1f and fs3 == 0 and fe3 == 0xfd and fm3 == 0x157088 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001928]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000192c]:csrrs tp, fcsr, zero
	-[0x80001930]:fsw ft11, 336(ra)
	-[0x80001934]:sw tp, 340(ra)
Current Store : [0x80001934] : sw tp, 340(ra) -- Store: [0x80005168]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x10ec66 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x03fd1f and fs3 == 0 and fe3 == 0xfd and fm3 == 0x157088 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000197c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001980]:csrrs tp, fcsr, zero
	-[0x80001984]:fsw ft11, 344(ra)
	-[0x80001988]:sw tp, 348(ra)
Current Store : [0x80001988] : sw tp, 348(ra) -- Store: [0x80005170]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x10ec66 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x03fd1f and fs3 == 0 and fe3 == 0xfd and fm3 == 0x157088 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800019d0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800019d4]:csrrs tp, fcsr, zero
	-[0x800019d8]:fsw ft11, 352(ra)
	-[0x800019dc]:sw tp, 356(ra)
Current Store : [0x800019dc] : sw tp, 356(ra) -- Store: [0x80005178]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x10ec66 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x03fd1f and fs3 == 0 and fe3 == 0xfd and fm3 == 0x157088 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a24]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001a28]:csrrs tp, fcsr, zero
	-[0x80001a2c]:fsw ft11, 360(ra)
	-[0x80001a30]:sw tp, 364(ra)
Current Store : [0x80001a30] : sw tp, 364(ra) -- Store: [0x80005180]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x10ec66 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x03fd1f and fs3 == 0 and fe3 == 0xfd and fm3 == 0x157088 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001a78]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001a7c]:csrrs tp, fcsr, zero
	-[0x80001a80]:fsw ft11, 368(ra)
	-[0x80001a84]:sw tp, 372(ra)
Current Store : [0x80001a84] : sw tp, 372(ra) -- Store: [0x80005188]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x42d171 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7c4426 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3ffa07 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001acc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001ad0]:csrrs tp, fcsr, zero
	-[0x80001ad4]:fsw ft11, 376(ra)
	-[0x80001ad8]:sw tp, 380(ra)
Current Store : [0x80001ad8] : sw tp, 380(ra) -- Store: [0x80005190]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x42d171 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7c4426 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3ffa07 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b20]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001b24]:csrrs tp, fcsr, zero
	-[0x80001b28]:fsw ft11, 384(ra)
	-[0x80001b2c]:sw tp, 388(ra)
Current Store : [0x80001b2c] : sw tp, 388(ra) -- Store: [0x80005198]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x42d171 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7c4426 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3ffa07 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001b74]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001b78]:csrrs tp, fcsr, zero
	-[0x80001b7c]:fsw ft11, 392(ra)
	-[0x80001b80]:sw tp, 396(ra)
Current Store : [0x80001b80] : sw tp, 396(ra) -- Store: [0x800051a0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x42d171 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7c4426 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3ffa07 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001bc8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001bcc]:csrrs tp, fcsr, zero
	-[0x80001bd0]:fsw ft11, 400(ra)
	-[0x80001bd4]:sw tp, 404(ra)
Current Store : [0x80001bd4] : sw tp, 404(ra) -- Store: [0x800051a8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x42d171 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7c4426 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x3ffa07 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001c20]:csrrs tp, fcsr, zero
	-[0x80001c24]:fsw ft11, 408(ra)
	-[0x80001c28]:sw tp, 412(ra)
Current Store : [0x80001c28] : sw tp, 412(ra) -- Store: [0x800051b0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x255773 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7b6dc7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2263a5 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001c70]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001c74]:csrrs tp, fcsr, zero
	-[0x80001c78]:fsw ft11, 416(ra)
	-[0x80001c7c]:sw tp, 420(ra)
Current Store : [0x80001c7c] : sw tp, 420(ra) -- Store: [0x800051b8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x255773 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7b6dc7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2263a5 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001cc4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001cc8]:csrrs tp, fcsr, zero
	-[0x80001ccc]:fsw ft11, 424(ra)
	-[0x80001cd0]:sw tp, 428(ra)
Current Store : [0x80001cd0] : sw tp, 428(ra) -- Store: [0x800051c0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x255773 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7b6dc7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2263a5 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d18]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001d1c]:csrrs tp, fcsr, zero
	-[0x80001d20]:fsw ft11, 432(ra)
	-[0x80001d24]:sw tp, 436(ra)
Current Store : [0x80001d24] : sw tp, 436(ra) -- Store: [0x800051c8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x255773 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7b6dc7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2263a5 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001d6c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001d70]:csrrs tp, fcsr, zero
	-[0x80001d74]:fsw ft11, 440(ra)
	-[0x80001d78]:sw tp, 444(ra)
Current Store : [0x80001d78] : sw tp, 444(ra) -- Store: [0x800051d0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x255773 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x7b6dc7 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x2263a5 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001dc0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001dc4]:csrrs tp, fcsr, zero
	-[0x80001dc8]:fsw ft11, 448(ra)
	-[0x80001dcc]:sw tp, 452(ra)
Current Store : [0x80001dcc] : sw tp, 452(ra) -- Store: [0x800051d8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f4ac6 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0870e2 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x18bdd1 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e14]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001e18]:csrrs tp, fcsr, zero
	-[0x80001e1c]:fsw ft11, 456(ra)
	-[0x80001e20]:sw tp, 460(ra)
Current Store : [0x80001e20] : sw tp, 460(ra) -- Store: [0x800051e0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f4ac6 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0870e2 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x18bdd1 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001e68]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001e6c]:csrrs tp, fcsr, zero
	-[0x80001e70]:fsw ft11, 464(ra)
	-[0x80001e74]:sw tp, 468(ra)
Current Store : [0x80001e74] : sw tp, 468(ra) -- Store: [0x800051e8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f4ac6 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0870e2 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x18bdd1 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001ebc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001ec0]:csrrs tp, fcsr, zero
	-[0x80001ec4]:fsw ft11, 472(ra)
	-[0x80001ec8]:sw tp, 476(ra)
Current Store : [0x80001ec8] : sw tp, 476(ra) -- Store: [0x800051f0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f4ac6 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0870e2 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x18bdd1 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f10]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001f14]:csrrs tp, fcsr, zero
	-[0x80001f18]:fsw ft11, 480(ra)
	-[0x80001f1c]:sw tp, 484(ra)
Current Store : [0x80001f1c] : sw tp, 484(ra) -- Store: [0x800051f8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0f4ac6 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0870e2 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x18bdd1 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001f64]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001f68]:csrrs tp, fcsr, zero
	-[0x80001f6c]:fsw ft11, 488(ra)
	-[0x80001f70]:sw tp, 492(ra)
Current Store : [0x80001f70] : sw tp, 492(ra) -- Store: [0x80005200]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c58cf and fs2 == 0 and fe2 == 0x7d and fm2 == 0x00090a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3c661c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001fb8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80001fbc]:csrrs tp, fcsr, zero
	-[0x80001fc0]:fsw ft11, 496(ra)
	-[0x80001fc4]:sw tp, 500(ra)
Current Store : [0x80001fc4] : sw tp, 500(ra) -- Store: [0x80005208]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c58cf and fs2 == 0 and fe2 == 0x7d and fm2 == 0x00090a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3c661c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000200c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002010]:csrrs tp, fcsr, zero
	-[0x80002014]:fsw ft11, 504(ra)
	-[0x80002018]:sw tp, 508(ra)
Current Store : [0x80002018] : sw tp, 508(ra) -- Store: [0x80005210]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c58cf and fs2 == 0 and fe2 == 0x7d and fm2 == 0x00090a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3c661c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002060]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002064]:csrrs tp, fcsr, zero
	-[0x80002068]:fsw ft11, 512(ra)
	-[0x8000206c]:sw tp, 516(ra)
Current Store : [0x8000206c] : sw tp, 516(ra) -- Store: [0x80005218]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c58cf and fs2 == 0 and fe2 == 0x7d and fm2 == 0x00090a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3c661c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800020b4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800020b8]:csrrs tp, fcsr, zero
	-[0x800020bc]:fsw ft11, 520(ra)
	-[0x800020c0]:sw tp, 524(ra)
Current Store : [0x800020c0] : sw tp, 524(ra) -- Store: [0x80005220]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c58cf and fs2 == 0 and fe2 == 0x7d and fm2 == 0x00090a and fs3 == 0 and fe3 == 0xfc and fm3 == 0x3c661c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002108]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000210c]:csrrs tp, fcsr, zero
	-[0x80002110]:fsw ft11, 528(ra)
	-[0x80002114]:sw tp, 532(ra)
Current Store : [0x80002114] : sw tp, 532(ra) -- Store: [0x80005228]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c0464 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6941a6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2b5036 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000215c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002160]:csrrs tp, fcsr, zero
	-[0x80002164]:fsw ft11, 536(ra)
	-[0x80002168]:sw tp, 540(ra)
Current Store : [0x80002168] : sw tp, 540(ra) -- Store: [0x80005230]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c0464 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6941a6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2b5036 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800021b0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800021b4]:csrrs tp, fcsr, zero
	-[0x800021b8]:fsw ft11, 544(ra)
	-[0x800021bc]:sw tp, 548(ra)
Current Store : [0x800021bc] : sw tp, 548(ra) -- Store: [0x80005238]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c0464 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6941a6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2b5036 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002204]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002208]:csrrs tp, fcsr, zero
	-[0x8000220c]:fsw ft11, 552(ra)
	-[0x80002210]:sw tp, 556(ra)
Current Store : [0x80002210] : sw tp, 556(ra) -- Store: [0x80005240]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c0464 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6941a6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2b5036 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002258]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000225c]:csrrs tp, fcsr, zero
	-[0x80002260]:fsw ft11, 560(ra)
	-[0x80002264]:sw tp, 564(ra)
Current Store : [0x80002264] : sw tp, 564(ra) -- Store: [0x80005248]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3c0464 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x6941a6 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2b5036 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800022ac]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800022b0]:csrrs tp, fcsr, zero
	-[0x800022b4]:fsw ft11, 568(ra)
	-[0x800022b8]:sw tp, 572(ra)
Current Store : [0x800022b8] : sw tp, 572(ra) -- Store: [0x80005250]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ae82 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x29e1b5 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ac951 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002300]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002304]:csrrs tp, fcsr, zero
	-[0x80002308]:fsw ft11, 576(ra)
	-[0x8000230c]:sw tp, 580(ra)
Current Store : [0x8000230c] : sw tp, 580(ra) -- Store: [0x80005258]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ae82 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x29e1b5 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ac951 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002354]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002358]:csrrs tp, fcsr, zero
	-[0x8000235c]:fsw ft11, 584(ra)
	-[0x80002360]:sw tp, 588(ra)
Current Store : [0x80002360] : sw tp, 588(ra) -- Store: [0x80005260]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ae82 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x29e1b5 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ac951 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023a8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800023ac]:csrrs tp, fcsr, zero
	-[0x800023b0]:fsw ft11, 592(ra)
	-[0x800023b4]:sw tp, 596(ra)
Current Store : [0x800023b4] : sw tp, 596(ra) -- Store: [0x80005268]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ae82 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x29e1b5 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ac951 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800023fc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002400]:csrrs tp, fcsr, zero
	-[0x80002404]:fsw ft11, 600(ra)
	-[0x80002408]:sw tp, 604(ra)
Current Store : [0x80002408] : sw tp, 604(ra) -- Store: [0x80005270]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00ae82 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x29e1b5 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x2ac951 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002450]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002454]:csrrs tp, fcsr, zero
	-[0x80002458]:fsw ft11, 608(ra)
	-[0x8000245c]:sw tp, 612(ra)
Current Store : [0x8000245c] : sw tp, 612(ra) -- Store: [0x80005278]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2bb8f9 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x6dee41 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1f9a10 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024a4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800024a8]:csrrs tp, fcsr, zero
	-[0x800024ac]:fsw ft11, 616(ra)
	-[0x800024b0]:sw tp, 620(ra)
Current Store : [0x800024b0] : sw tp, 620(ra) -- Store: [0x80005280]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2bb8f9 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x6dee41 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1f9a10 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800024f8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800024fc]:csrrs tp, fcsr, zero
	-[0x80002500]:fsw ft11, 624(ra)
	-[0x80002504]:sw tp, 628(ra)
Current Store : [0x80002504] : sw tp, 628(ra) -- Store: [0x80005288]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2bb8f9 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x6dee41 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1f9a10 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000254c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002550]:csrrs tp, fcsr, zero
	-[0x80002554]:fsw ft11, 632(ra)
	-[0x80002558]:sw tp, 636(ra)
Current Store : [0x80002558] : sw tp, 636(ra) -- Store: [0x80005290]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2bb8f9 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x6dee41 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1f9a10 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025a0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800025a4]:csrrs tp, fcsr, zero
	-[0x800025a8]:fsw ft11, 640(ra)
	-[0x800025ac]:sw tp, 644(ra)
Current Store : [0x800025ac] : sw tp, 644(ra) -- Store: [0x80005298]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2bb8f9 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x6dee41 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1f9a10 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800025f4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800025f8]:csrrs tp, fcsr, zero
	-[0x800025fc]:fsw ft11, 648(ra)
	-[0x80002600]:sw tp, 652(ra)
Current Store : [0x80002600] : sw tp, 652(ra) -- Store: [0x800052a0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18d9d9 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x262663 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x46685d and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002648]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000264c]:csrrs tp, fcsr, zero
	-[0x80002650]:fsw ft11, 656(ra)
	-[0x80002654]:sw tp, 660(ra)
Current Store : [0x80002654] : sw tp, 660(ra) -- Store: [0x800052a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18d9d9 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x262663 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x46685d and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000269c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800026a0]:csrrs tp, fcsr, zero
	-[0x800026a4]:fsw ft11, 664(ra)
	-[0x800026a8]:sw tp, 668(ra)
Current Store : [0x800026a8] : sw tp, 668(ra) -- Store: [0x800052b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18d9d9 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x262663 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x46685d and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800026f0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800026f4]:csrrs tp, fcsr, zero
	-[0x800026f8]:fsw ft11, 672(ra)
	-[0x800026fc]:sw tp, 676(ra)
Current Store : [0x800026fc] : sw tp, 676(ra) -- Store: [0x800052b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18d9d9 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x262663 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x46685d and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002744]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002748]:csrrs tp, fcsr, zero
	-[0x8000274c]:fsw ft11, 680(ra)
	-[0x80002750]:sw tp, 684(ra)
Current Store : [0x80002750] : sw tp, 684(ra) -- Store: [0x800052c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x18d9d9 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x262663 and fs3 == 0 and fe3 == 0xfd and fm3 == 0x46685d and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002798]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x8000279c]:csrrs tp, fcsr, zero
	-[0x800027a0]:fsw ft11, 688(ra)
	-[0x800027a4]:sw tp, 692(ra)
Current Store : [0x800027a4] : sw tp, 692(ra) -- Store: [0x800052c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x34d648 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3fcdc7 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x077d3c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800027ec]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800027f0]:csrrs tp, fcsr, zero
	-[0x800027f4]:fsw ft11, 696(ra)
	-[0x800027f8]:sw tp, 700(ra)
Current Store : [0x800027f8] : sw tp, 700(ra) -- Store: [0x800052d0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x34d648 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3fcdc7 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x077d3c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002840]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002844]:csrrs tp, fcsr, zero
	-[0x80002848]:fsw ft11, 704(ra)
	-[0x8000284c]:sw tp, 708(ra)
Current Store : [0x8000284c] : sw tp, 708(ra) -- Store: [0x800052d8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x34d648 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3fcdc7 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x077d3c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002894]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002898]:csrrs tp, fcsr, zero
	-[0x8000289c]:fsw ft11, 712(ra)
	-[0x800028a0]:sw tp, 716(ra)
Current Store : [0x800028a0] : sw tp, 716(ra) -- Store: [0x800052e0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x34d648 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3fcdc7 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x077d3c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800028e8]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800028ec]:csrrs tp, fcsr, zero
	-[0x800028f0]:fsw ft11, 720(ra)
	-[0x800028f4]:sw tp, 724(ra)
Current Store : [0x800028f4] : sw tp, 724(ra) -- Store: [0x800052e8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x34d648 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x3fcdc7 and fs3 == 0 and fe3 == 0xfe and fm3 == 0x077d3c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000293c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002940]:csrrs tp, fcsr, zero
	-[0x80002944]:fsw ft11, 728(ra)
	-[0x80002948]:sw tp, 732(ra)
Current Store : [0x80002948] : sw tp, 732(ra) -- Store: [0x800052f0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x185240 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x049ddd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1dd0ae and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002990]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002994]:csrrs tp, fcsr, zero
	-[0x80002998]:fsw ft11, 736(ra)
	-[0x8000299c]:sw tp, 740(ra)
Current Store : [0x8000299c] : sw tp, 740(ra) -- Store: [0x800052f8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x185240 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x049ddd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1dd0ae and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800029e4]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x800029e8]:csrrs tp, fcsr, zero
	-[0x800029ec]:fsw ft11, 744(ra)
	-[0x800029f0]:sw tp, 748(ra)
Current Store : [0x800029f0] : sw tp, 748(ra) -- Store: [0x80005300]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x185240 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x049ddd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1dd0ae and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a38]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002a3c]:csrrs tp, fcsr, zero
	-[0x80002a40]:fsw ft11, 752(ra)
	-[0x80002a44]:sw tp, 756(ra)
Current Store : [0x80002a44] : sw tp, 756(ra) -- Store: [0x80005308]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x185240 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x049ddd and fs3 == 0 and fe3 == 0xfe and fm3 == 0x1dd0ae and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002a8c]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002a90]:csrrs tp, fcsr, zero
	-[0x80002a94]:fsw ft11, 760(ra)
	-[0x80002a98]:sw tp, 764(ra)
Current Store : [0x80002a98] : sw tp, 764(ra) -- Store: [0x80005310]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a40d7 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0a1548 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x48ecdf and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002ae0]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002ae4]:csrrs tp, fcsr, zero
	-[0x80002ae8]:fsw ft11, 768(ra)
	-[0x80002aec]:sw tp, 772(ra)
Current Store : [0x80002aec] : sw tp, 772(ra) -- Store: [0x80005318]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a40d7 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0a1548 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x48ecdf and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b34]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002b38]:csrrs tp, fcsr, zero
	-[0x80002b3c]:fsw ft11, 776(ra)
	-[0x80002b40]:sw tp, 780(ra)
Current Store : [0x80002b40] : sw tp, 780(ra) -- Store: [0x80005320]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a40d7 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0a1548 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x48ecdf and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002b88]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002b8c]:csrrs tp, fcsr, zero
	-[0x80002b90]:fsw ft11, 784(ra)
	-[0x80002b94]:sw tp, 788(ra)
Current Store : [0x80002b94] : sw tp, 788(ra) -- Store: [0x80005328]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a40d7 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x0a1548 and fs3 == 0 and fe3 == 0xfc and fm3 == 0x48ecdf and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80002bdc]:fnmadd.s ft11, ft10, ft9, ft8, dyn
	-[0x80002be0]:csrrs tp, fcsr, zero
	-[0x80002be4]:fsw ft11, 792(ra)
	-[0x80002be8]:sw tp, 796(ra)
Current Store : [0x80002be8] : sw tp, 796(ra) -- Store: [0x80005330]:0x00000083





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
