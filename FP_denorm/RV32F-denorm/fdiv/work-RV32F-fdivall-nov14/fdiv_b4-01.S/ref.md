
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800012e0')]      |
| SIG_REGION                | [('0x80003610', '0x80003a90', '288 words')]      |
| COV_LABELS                | fdiv_b4      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV32F-fdivall-nov14/fdiv_b4-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 284      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 142     |
| STAT4                     | 142     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000124]:fdiv.s ft11, ft11, ft11, dyn
[0x80000128]:csrrs tp, fcsr, zero
[0x8000012c]:fsw ft11, 0(ra)
[0x80000130]:sw tp, 4(ra)
[0x80000134]:flw ft9, 8(gp)
[0x80000138]:flw ft10, 12(gp)
[0x8000013c]:addi sp, zero, 34
[0x80000140]:csrrw zero, fcsr, sp
[0x80000144]:fdiv.s ft10, ft9, ft10, dyn

[0x80000144]:fdiv.s ft10, ft9, ft10, dyn
[0x80000148]:csrrs tp, fcsr, zero
[0x8000014c]:fsw ft10, 8(ra)
[0x80000150]:sw tp, 12(ra)
[0x80000154]:flw ft8, 16(gp)
[0x80000158]:flw ft8, 20(gp)
[0x8000015c]:addi sp, zero, 66
[0x80000160]:csrrw zero, fcsr, sp
[0x80000164]:fdiv.s ft9, ft8, ft8, dyn

[0x80000164]:fdiv.s ft9, ft8, ft8, dyn
[0x80000168]:csrrs tp, fcsr, zero
[0x8000016c]:fsw ft9, 16(ra)
[0x80000170]:sw tp, 20(ra)
[0x80000174]:flw fs11, 24(gp)
[0x80000178]:flw ft9, 28(gp)
[0x8000017c]:addi sp, zero, 98
[0x80000180]:csrrw zero, fcsr, sp
[0x80000184]:fdiv.s fs11, fs11, ft9, dyn

[0x80000184]:fdiv.s fs11, fs11, ft9, dyn
[0x80000188]:csrrs tp, fcsr, zero
[0x8000018c]:fsw fs11, 24(ra)
[0x80000190]:sw tp, 28(ra)
[0x80000194]:flw ft10, 32(gp)
[0x80000198]:flw fs11, 36(gp)
[0x8000019c]:addi sp, zero, 130
[0x800001a0]:csrrw zero, fcsr, sp
[0x800001a4]:fdiv.s ft8, ft10, fs11, dyn

[0x800001a4]:fdiv.s ft8, ft10, fs11, dyn
[0x800001a8]:csrrs tp, fcsr, zero
[0x800001ac]:fsw ft8, 32(ra)
[0x800001b0]:sw tp, 36(ra)
[0x800001b4]:flw fs9, 40(gp)
[0x800001b8]:flw fs8, 44(gp)
[0x800001bc]:addi sp, zero, 2
[0x800001c0]:csrrw zero, fcsr, sp
[0x800001c4]:fdiv.s fs10, fs9, fs8, dyn

[0x800001c4]:fdiv.s fs10, fs9, fs8, dyn
[0x800001c8]:csrrs tp, fcsr, zero
[0x800001cc]:fsw fs10, 40(ra)
[0x800001d0]:sw tp, 44(ra)
[0x800001d4]:flw fs8, 48(gp)
[0x800001d8]:flw fs10, 52(gp)
[0x800001dc]:addi sp, zero, 34
[0x800001e0]:csrrw zero, fcsr, sp
[0x800001e4]:fdiv.s fs9, fs8, fs10, dyn

[0x800001e4]:fdiv.s fs9, fs8, fs10, dyn
[0x800001e8]:csrrs tp, fcsr, zero
[0x800001ec]:fsw fs9, 48(ra)
[0x800001f0]:sw tp, 52(ra)
[0x800001f4]:flw fs10, 56(gp)
[0x800001f8]:flw fs9, 60(gp)
[0x800001fc]:addi sp, zero, 66
[0x80000200]:csrrw zero, fcsr, sp
[0x80000204]:fdiv.s fs8, fs10, fs9, dyn

[0x80000204]:fdiv.s fs8, fs10, fs9, dyn
[0x80000208]:csrrs tp, fcsr, zero
[0x8000020c]:fsw fs8, 56(ra)
[0x80000210]:sw tp, 60(ra)
[0x80000214]:flw fs6, 64(gp)
[0x80000218]:flw fs5, 68(gp)
[0x8000021c]:addi sp, zero, 98
[0x80000220]:csrrw zero, fcsr, sp
[0x80000224]:fdiv.s fs7, fs6, fs5, dyn

[0x80000224]:fdiv.s fs7, fs6, fs5, dyn
[0x80000228]:csrrs tp, fcsr, zero
[0x8000022c]:fsw fs7, 64(ra)
[0x80000230]:sw tp, 68(ra)
[0x80000234]:flw fs5, 72(gp)
[0x80000238]:flw fs7, 76(gp)
[0x8000023c]:addi sp, zero, 130
[0x80000240]:csrrw zero, fcsr, sp
[0x80000244]:fdiv.s fs6, fs5, fs7, dyn

[0x80000244]:fdiv.s fs6, fs5, fs7, dyn
[0x80000248]:csrrs tp, fcsr, zero
[0x8000024c]:fsw fs6, 72(ra)
[0x80000250]:sw tp, 76(ra)
[0x80000254]:flw fs7, 80(gp)
[0x80000258]:flw fs6, 84(gp)
[0x8000025c]:addi sp, zero, 2
[0x80000260]:csrrw zero, fcsr, sp
[0x80000264]:fdiv.s fs5, fs7, fs6, dyn

[0x80000264]:fdiv.s fs5, fs7, fs6, dyn
[0x80000268]:csrrs tp, fcsr, zero
[0x8000026c]:fsw fs5, 80(ra)
[0x80000270]:sw tp, 84(ra)
[0x80000274]:flw fs3, 88(gp)
[0x80000278]:flw fs2, 92(gp)
[0x8000027c]:addi sp, zero, 34
[0x80000280]:csrrw zero, fcsr, sp
[0x80000284]:fdiv.s fs4, fs3, fs2, dyn

[0x80000284]:fdiv.s fs4, fs3, fs2, dyn
[0x80000288]:csrrs tp, fcsr, zero
[0x8000028c]:fsw fs4, 88(ra)
[0x80000290]:sw tp, 92(ra)
[0x80000294]:flw fs2, 96(gp)
[0x80000298]:flw fs4, 100(gp)
[0x8000029c]:addi sp, zero, 66
[0x800002a0]:csrrw zero, fcsr, sp
[0x800002a4]:fdiv.s fs3, fs2, fs4, dyn

[0x800002a4]:fdiv.s fs3, fs2, fs4, dyn
[0x800002a8]:csrrs tp, fcsr, zero
[0x800002ac]:fsw fs3, 96(ra)
[0x800002b0]:sw tp, 100(ra)
[0x800002b4]:flw fs4, 104(gp)
[0x800002b8]:flw fs3, 108(gp)
[0x800002bc]:addi sp, zero, 98
[0x800002c0]:csrrw zero, fcsr, sp
[0x800002c4]:fdiv.s fs2, fs4, fs3, dyn

[0x800002c4]:fdiv.s fs2, fs4, fs3, dyn
[0x800002c8]:csrrs tp, fcsr, zero
[0x800002cc]:fsw fs2, 104(ra)
[0x800002d0]:sw tp, 108(ra)
[0x800002d4]:flw fa6, 112(gp)
[0x800002d8]:flw fa5, 116(gp)
[0x800002dc]:addi sp, zero, 130
[0x800002e0]:csrrw zero, fcsr, sp
[0x800002e4]:fdiv.s fa7, fa6, fa5, dyn

[0x800002e4]:fdiv.s fa7, fa6, fa5, dyn
[0x800002e8]:csrrs tp, fcsr, zero
[0x800002ec]:fsw fa7, 112(ra)
[0x800002f0]:sw tp, 116(ra)
[0x800002f4]:flw fa5, 120(gp)
[0x800002f8]:flw fa7, 124(gp)
[0x800002fc]:addi sp, zero, 2
[0x80000300]:csrrw zero, fcsr, sp
[0x80000304]:fdiv.s fa6, fa5, fa7, dyn

[0x80000304]:fdiv.s fa6, fa5, fa7, dyn
[0x80000308]:csrrs tp, fcsr, zero
[0x8000030c]:fsw fa6, 120(ra)
[0x80000310]:sw tp, 124(ra)
[0x80000314]:flw fa7, 128(gp)
[0x80000318]:flw fa6, 132(gp)
[0x8000031c]:addi sp, zero, 34
[0x80000320]:csrrw zero, fcsr, sp
[0x80000324]:fdiv.s fa5, fa7, fa6, dyn

[0x80000324]:fdiv.s fa5, fa7, fa6, dyn
[0x80000328]:csrrs tp, fcsr, zero
[0x8000032c]:fsw fa5, 128(ra)
[0x80000330]:sw tp, 132(ra)
[0x80000334]:flw fa3, 136(gp)
[0x80000338]:flw fa2, 140(gp)
[0x8000033c]:addi sp, zero, 66
[0x80000340]:csrrw zero, fcsr, sp
[0x80000344]:fdiv.s fa4, fa3, fa2, dyn

[0x80000344]:fdiv.s fa4, fa3, fa2, dyn
[0x80000348]:csrrs tp, fcsr, zero
[0x8000034c]:fsw fa4, 136(ra)
[0x80000350]:sw tp, 140(ra)
[0x80000354]:flw fa2, 144(gp)
[0x80000358]:flw fa4, 148(gp)
[0x8000035c]:addi sp, zero, 98
[0x80000360]:csrrw zero, fcsr, sp
[0x80000364]:fdiv.s fa3, fa2, fa4, dyn

[0x80000364]:fdiv.s fa3, fa2, fa4, dyn
[0x80000368]:csrrs tp, fcsr, zero
[0x8000036c]:fsw fa3, 144(ra)
[0x80000370]:sw tp, 148(ra)
[0x80000374]:flw fa4, 152(gp)
[0x80000378]:flw fa3, 156(gp)
[0x8000037c]:addi sp, zero, 130
[0x80000380]:csrrw zero, fcsr, sp
[0x80000384]:fdiv.s fa2, fa4, fa3, dyn

[0x80000384]:fdiv.s fa2, fa4, fa3, dyn
[0x80000388]:csrrs tp, fcsr, zero
[0x8000038c]:fsw fa2, 152(ra)
[0x80000390]:sw tp, 156(ra)
[0x80000394]:flw fa0, 160(gp)
[0x80000398]:flw fs1, 164(gp)
[0x8000039c]:addi sp, zero, 2
[0x800003a0]:csrrw zero, fcsr, sp
[0x800003a4]:fdiv.s fa1, fa0, fs1, dyn

[0x800003a4]:fdiv.s fa1, fa0, fs1, dyn
[0x800003a8]:csrrs tp, fcsr, zero
[0x800003ac]:fsw fa1, 160(ra)
[0x800003b0]:sw tp, 164(ra)
[0x800003b4]:flw fs1, 168(gp)
[0x800003b8]:flw fa1, 172(gp)
[0x800003bc]:addi sp, zero, 34
[0x800003c0]:csrrw zero, fcsr, sp
[0x800003c4]:fdiv.s fa0, fs1, fa1, dyn

[0x800003c4]:fdiv.s fa0, fs1, fa1, dyn
[0x800003c8]:csrrs tp, fcsr, zero
[0x800003cc]:fsw fa0, 168(ra)
[0x800003d0]:sw tp, 172(ra)
[0x800003d4]:flw fa1, 176(gp)
[0x800003d8]:flw fa0, 180(gp)
[0x800003dc]:addi sp, zero, 66
[0x800003e0]:csrrw zero, fcsr, sp
[0x800003e4]:fdiv.s fs1, fa1, fa0, dyn

[0x800003e4]:fdiv.s fs1, fa1, fa0, dyn
[0x800003e8]:csrrs tp, fcsr, zero
[0x800003ec]:fsw fs1, 176(ra)
[0x800003f0]:sw tp, 180(ra)
[0x800003f4]:flw ft7, 184(gp)
[0x800003f8]:flw ft6, 188(gp)
[0x800003fc]:addi sp, zero, 98
[0x80000400]:csrrw zero, fcsr, sp
[0x80000404]:fdiv.s fs0, ft7, ft6, dyn

[0x80000404]:fdiv.s fs0, ft7, ft6, dyn
[0x80000408]:csrrs tp, fcsr, zero
[0x8000040c]:fsw fs0, 184(ra)
[0x80000410]:sw tp, 188(ra)
[0x80000414]:flw ft6, 192(gp)
[0x80000418]:flw fs0, 196(gp)
[0x8000041c]:addi sp, zero, 130
[0x80000420]:csrrw zero, fcsr, sp
[0x80000424]:fdiv.s ft7, ft6, fs0, dyn

[0x80000424]:fdiv.s ft7, ft6, fs0, dyn
[0x80000428]:csrrs tp, fcsr, zero
[0x8000042c]:fsw ft7, 192(ra)
[0x80000430]:sw tp, 196(ra)
[0x80000434]:flw fs0, 200(gp)
[0x80000438]:flw ft7, 204(gp)
[0x8000043c]:addi sp, zero, 2
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fdiv.s ft6, fs0, ft7, dyn

[0x80000444]:fdiv.s ft6, fs0, ft7, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:fsw ft6, 200(ra)
[0x80000450]:sw tp, 204(ra)
[0x80000454]:flw ft4, 208(gp)
[0x80000458]:flw ft3, 212(gp)
[0x8000045c]:addi sp, zero, 34
[0x80000460]:csrrw zero, fcsr, sp
[0x80000464]:fdiv.s ft5, ft4, ft3, dyn

[0x80000464]:fdiv.s ft5, ft4, ft3, dyn
[0x80000468]:csrrs tp, fcsr, zero
[0x8000046c]:fsw ft5, 208(ra)
[0x80000470]:sw tp, 212(ra)
[0x80000474]:flw ft3, 216(gp)
[0x80000478]:flw ft5, 220(gp)
[0x8000047c]:addi sp, zero, 66
[0x80000480]:csrrw zero, fcsr, sp
[0x80000484]:fdiv.s ft4, ft3, ft5, dyn

[0x80000484]:fdiv.s ft4, ft3, ft5, dyn
[0x80000488]:csrrs tp, fcsr, zero
[0x8000048c]:fsw ft4, 216(ra)
[0x80000490]:sw tp, 220(ra)
[0x80000494]:flw ft5, 224(gp)
[0x80000498]:flw ft4, 228(gp)
[0x8000049c]:addi sp, zero, 98
[0x800004a0]:csrrw zero, fcsr, sp
[0x800004a4]:fdiv.s ft3, ft5, ft4, dyn

[0x800004a4]:fdiv.s ft3, ft5, ft4, dyn
[0x800004a8]:csrrs tp, fcsr, zero
[0x800004ac]:fsw ft3, 224(ra)
[0x800004b0]:sw tp, 228(ra)
[0x800004b4]:flw ft1, 232(gp)
[0x800004b8]:flw ft0, 236(gp)
[0x800004bc]:addi sp, zero, 130
[0x800004c0]:csrrw zero, fcsr, sp
[0x800004c4]:fdiv.s ft2, ft1, ft0, dyn

[0x800004c4]:fdiv.s ft2, ft1, ft0, dyn
[0x800004c8]:csrrs tp, fcsr, zero
[0x800004cc]:fsw ft2, 232(ra)
[0x800004d0]:sw tp, 236(ra)
[0x800004d4]:flw ft0, 240(gp)
[0x800004d8]:flw ft2, 244(gp)
[0x800004dc]:addi sp, zero, 2
[0x800004e0]:csrrw zero, fcsr, sp
[0x800004e4]:fdiv.s ft1, ft0, ft2, dyn

[0x800004e4]:fdiv.s ft1, ft0, ft2, dyn
[0x800004e8]:csrrs tp, fcsr, zero
[0x800004ec]:fsw ft1, 240(ra)
[0x800004f0]:sw tp, 244(ra)
[0x800004f4]:flw ft2, 248(gp)
[0x800004f8]:flw ft1, 252(gp)
[0x800004fc]:addi sp, zero, 34
[0x80000500]:csrrw zero, fcsr, sp
[0x80000504]:fdiv.s ft0, ft2, ft1, dyn

[0x80000504]:fdiv.s ft0, ft2, ft1, dyn
[0x80000508]:csrrs tp, fcsr, zero
[0x8000050c]:fsw ft0, 248(ra)
[0x80000510]:sw tp, 252(ra)
[0x80000514]:flw ft10, 256(gp)
[0x80000518]:flw ft9, 260(gp)
[0x8000051c]:addi sp, zero, 66
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fdiv.s ft11, ft10, ft9, dyn

[0x80000524]:fdiv.s ft11, ft10, ft9, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:fsw ft11, 256(ra)
[0x80000530]:sw tp, 260(ra)
[0x80000534]:flw ft10, 264(gp)
[0x80000538]:flw ft9, 268(gp)
[0x8000053c]:addi sp, zero, 98
[0x80000540]:csrrw zero, fcsr, sp
[0x80000544]:fdiv.s ft11, ft10, ft9, dyn

[0x80000544]:fdiv.s ft11, ft10, ft9, dyn
[0x80000548]:csrrs tp, fcsr, zero
[0x8000054c]:fsw ft11, 264(ra)
[0x80000550]:sw tp, 268(ra)
[0x80000554]:flw ft10, 272(gp)
[0x80000558]:flw ft9, 276(gp)
[0x8000055c]:addi sp, zero, 130
[0x80000560]:csrrw zero, fcsr, sp
[0x80000564]:fdiv.s ft11, ft10, ft9, dyn

[0x80000564]:fdiv.s ft11, ft10, ft9, dyn
[0x80000568]:csrrs tp, fcsr, zero
[0x8000056c]:fsw ft11, 272(ra)
[0x80000570]:sw tp, 276(ra)
[0x80000574]:flw ft10, 280(gp)
[0x80000578]:flw ft9, 284(gp)
[0x8000057c]:addi sp, zero, 2
[0x80000580]:csrrw zero, fcsr, sp
[0x80000584]:fdiv.s ft11, ft10, ft9, dyn

[0x80000584]:fdiv.s ft11, ft10, ft9, dyn
[0x80000588]:csrrs tp, fcsr, zero
[0x8000058c]:fsw ft11, 280(ra)
[0x80000590]:sw tp, 284(ra)
[0x80000594]:flw ft10, 288(gp)
[0x80000598]:flw ft9, 292(gp)
[0x8000059c]:addi sp, zero, 34
[0x800005a0]:csrrw zero, fcsr, sp
[0x800005a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800005a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800005a8]:csrrs tp, fcsr, zero
[0x800005ac]:fsw ft11, 288(ra)
[0x800005b0]:sw tp, 292(ra)
[0x800005b4]:flw ft10, 296(gp)
[0x800005b8]:flw ft9, 300(gp)
[0x800005bc]:addi sp, zero, 66
[0x800005c0]:csrrw zero, fcsr, sp
[0x800005c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800005c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800005c8]:csrrs tp, fcsr, zero
[0x800005cc]:fsw ft11, 296(ra)
[0x800005d0]:sw tp, 300(ra)
[0x800005d4]:flw ft10, 304(gp)
[0x800005d8]:flw ft9, 308(gp)
[0x800005dc]:addi sp, zero, 98
[0x800005e0]:csrrw zero, fcsr, sp
[0x800005e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800005e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800005e8]:csrrs tp, fcsr, zero
[0x800005ec]:fsw ft11, 304(ra)
[0x800005f0]:sw tp, 308(ra)
[0x800005f4]:flw ft10, 312(gp)
[0x800005f8]:flw ft9, 316(gp)
[0x800005fc]:addi sp, zero, 130
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fdiv.s ft11, ft10, ft9, dyn

[0x80000604]:fdiv.s ft11, ft10, ft9, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:fsw ft11, 312(ra)
[0x80000610]:sw tp, 316(ra)
[0x80000614]:flw ft10, 320(gp)
[0x80000618]:flw ft9, 324(gp)
[0x8000061c]:addi sp, zero, 2
[0x80000620]:csrrw zero, fcsr, sp
[0x80000624]:fdiv.s ft11, ft10, ft9, dyn

[0x80000624]:fdiv.s ft11, ft10, ft9, dyn
[0x80000628]:csrrs tp, fcsr, zero
[0x8000062c]:fsw ft11, 320(ra)
[0x80000630]:sw tp, 324(ra)
[0x80000634]:flw ft10, 328(gp)
[0x80000638]:flw ft9, 332(gp)
[0x8000063c]:addi sp, zero, 34
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fdiv.s ft11, ft10, ft9, dyn

[0x80000644]:fdiv.s ft11, ft10, ft9, dyn
[0x80000648]:csrrs tp, fcsr, zero
[0x8000064c]:fsw ft11, 328(ra)
[0x80000650]:sw tp, 332(ra)
[0x80000654]:flw ft10, 336(gp)
[0x80000658]:flw ft9, 340(gp)
[0x8000065c]:addi sp, zero, 66
[0x80000660]:csrrw zero, fcsr, sp
[0x80000664]:fdiv.s ft11, ft10, ft9, dyn

[0x80000664]:fdiv.s ft11, ft10, ft9, dyn
[0x80000668]:csrrs tp, fcsr, zero
[0x8000066c]:fsw ft11, 336(ra)
[0x80000670]:sw tp, 340(ra)
[0x80000674]:flw ft10, 344(gp)
[0x80000678]:flw ft9, 348(gp)
[0x8000067c]:addi sp, zero, 98
[0x80000680]:csrrw zero, fcsr, sp
[0x80000684]:fdiv.s ft11, ft10, ft9, dyn

[0x80000684]:fdiv.s ft11, ft10, ft9, dyn
[0x80000688]:csrrs tp, fcsr, zero
[0x8000068c]:fsw ft11, 344(ra)
[0x80000690]:sw tp, 348(ra)
[0x80000694]:flw ft10, 352(gp)
[0x80000698]:flw ft9, 356(gp)
[0x8000069c]:addi sp, zero, 130
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800006a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800006a8]:csrrs tp, fcsr, zero
[0x800006ac]:fsw ft11, 352(ra)
[0x800006b0]:sw tp, 356(ra)
[0x800006b4]:flw ft10, 360(gp)
[0x800006b8]:flw ft9, 364(gp)
[0x800006bc]:addi sp, zero, 2
[0x800006c0]:csrrw zero, fcsr, sp
[0x800006c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800006c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800006c8]:csrrs tp, fcsr, zero
[0x800006cc]:fsw ft11, 360(ra)
[0x800006d0]:sw tp, 364(ra)
[0x800006d4]:flw ft10, 368(gp)
[0x800006d8]:flw ft9, 372(gp)
[0x800006dc]:addi sp, zero, 34
[0x800006e0]:csrrw zero, fcsr, sp
[0x800006e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800006e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800006e8]:csrrs tp, fcsr, zero
[0x800006ec]:fsw ft11, 368(ra)
[0x800006f0]:sw tp, 372(ra)
[0x800006f4]:flw ft10, 376(gp)
[0x800006f8]:flw ft9, 380(gp)
[0x800006fc]:addi sp, zero, 66
[0x80000700]:csrrw zero, fcsr, sp
[0x80000704]:fdiv.s ft11, ft10, ft9, dyn

[0x80000704]:fdiv.s ft11, ft10, ft9, dyn
[0x80000708]:csrrs tp, fcsr, zero
[0x8000070c]:fsw ft11, 376(ra)
[0x80000710]:sw tp, 380(ra)
[0x80000714]:flw ft10, 384(gp)
[0x80000718]:flw ft9, 388(gp)
[0x8000071c]:addi sp, zero, 98
[0x80000720]:csrrw zero, fcsr, sp
[0x80000724]:fdiv.s ft11, ft10, ft9, dyn

[0x80000724]:fdiv.s ft11, ft10, ft9, dyn
[0x80000728]:csrrs tp, fcsr, zero
[0x8000072c]:fsw ft11, 384(ra)
[0x80000730]:sw tp, 388(ra)
[0x80000734]:flw ft10, 392(gp)
[0x80000738]:flw ft9, 396(gp)
[0x8000073c]:addi sp, zero, 130
[0x80000740]:csrrw zero, fcsr, sp
[0x80000744]:fdiv.s ft11, ft10, ft9, dyn

[0x80000744]:fdiv.s ft11, ft10, ft9, dyn
[0x80000748]:csrrs tp, fcsr, zero
[0x8000074c]:fsw ft11, 392(ra)
[0x80000750]:sw tp, 396(ra)
[0x80000754]:flw ft10, 400(gp)
[0x80000758]:flw ft9, 404(gp)
[0x8000075c]:addi sp, zero, 2
[0x80000760]:csrrw zero, fcsr, sp
[0x80000764]:fdiv.s ft11, ft10, ft9, dyn

[0x80000764]:fdiv.s ft11, ft10, ft9, dyn
[0x80000768]:csrrs tp, fcsr, zero
[0x8000076c]:fsw ft11, 400(ra)
[0x80000770]:sw tp, 404(ra)
[0x80000774]:flw ft10, 408(gp)
[0x80000778]:flw ft9, 412(gp)
[0x8000077c]:addi sp, zero, 34
[0x80000780]:csrrw zero, fcsr, sp
[0x80000784]:fdiv.s ft11, ft10, ft9, dyn

[0x80000784]:fdiv.s ft11, ft10, ft9, dyn
[0x80000788]:csrrs tp, fcsr, zero
[0x8000078c]:fsw ft11, 408(ra)
[0x80000790]:sw tp, 412(ra)
[0x80000794]:flw ft10, 416(gp)
[0x80000798]:flw ft9, 420(gp)
[0x8000079c]:addi sp, zero, 66
[0x800007a0]:csrrw zero, fcsr, sp
[0x800007a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800007a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800007a8]:csrrs tp, fcsr, zero
[0x800007ac]:fsw ft11, 416(ra)
[0x800007b0]:sw tp, 420(ra)
[0x800007b4]:flw ft10, 424(gp)
[0x800007b8]:flw ft9, 428(gp)
[0x800007bc]:addi sp, zero, 98
[0x800007c0]:csrrw zero, fcsr, sp
[0x800007c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800007c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800007c8]:csrrs tp, fcsr, zero
[0x800007cc]:fsw ft11, 424(ra)
[0x800007d0]:sw tp, 428(ra)
[0x800007d4]:flw ft10, 432(gp)
[0x800007d8]:flw ft9, 436(gp)
[0x800007dc]:addi sp, zero, 130
[0x800007e0]:csrrw zero, fcsr, sp
[0x800007e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800007e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800007e8]:csrrs tp, fcsr, zero
[0x800007ec]:fsw ft11, 432(ra)
[0x800007f0]:sw tp, 436(ra)
[0x800007f4]:flw ft10, 440(gp)
[0x800007f8]:flw ft9, 444(gp)
[0x800007fc]:addi sp, zero, 2
[0x80000800]:csrrw zero, fcsr, sp
[0x80000804]:fdiv.s ft11, ft10, ft9, dyn

[0x80000804]:fdiv.s ft11, ft10, ft9, dyn
[0x80000808]:csrrs tp, fcsr, zero
[0x8000080c]:fsw ft11, 440(ra)
[0x80000810]:sw tp, 444(ra)
[0x80000814]:flw ft10, 448(gp)
[0x80000818]:flw ft9, 452(gp)
[0x8000081c]:addi sp, zero, 34
[0x80000820]:csrrw zero, fcsr, sp
[0x80000824]:fdiv.s ft11, ft10, ft9, dyn

[0x80000824]:fdiv.s ft11, ft10, ft9, dyn
[0x80000828]:csrrs tp, fcsr, zero
[0x8000082c]:fsw ft11, 448(ra)
[0x80000830]:sw tp, 452(ra)
[0x80000834]:flw ft10, 456(gp)
[0x80000838]:flw ft9, 460(gp)
[0x8000083c]:addi sp, zero, 66
[0x80000840]:csrrw zero, fcsr, sp
[0x80000844]:fdiv.s ft11, ft10, ft9, dyn

[0x80000844]:fdiv.s ft11, ft10, ft9, dyn
[0x80000848]:csrrs tp, fcsr, zero
[0x8000084c]:fsw ft11, 456(ra)
[0x80000850]:sw tp, 460(ra)
[0x80000854]:flw ft10, 464(gp)
[0x80000858]:flw ft9, 468(gp)
[0x8000085c]:addi sp, zero, 98
[0x80000860]:csrrw zero, fcsr, sp
[0x80000864]:fdiv.s ft11, ft10, ft9, dyn

[0x80000864]:fdiv.s ft11, ft10, ft9, dyn
[0x80000868]:csrrs tp, fcsr, zero
[0x8000086c]:fsw ft11, 464(ra)
[0x80000870]:sw tp, 468(ra)
[0x80000874]:flw ft10, 472(gp)
[0x80000878]:flw ft9, 476(gp)
[0x8000087c]:addi sp, zero, 130
[0x80000880]:csrrw zero, fcsr, sp
[0x80000884]:fdiv.s ft11, ft10, ft9, dyn

[0x80000884]:fdiv.s ft11, ft10, ft9, dyn
[0x80000888]:csrrs tp, fcsr, zero
[0x8000088c]:fsw ft11, 472(ra)
[0x80000890]:sw tp, 476(ra)
[0x80000894]:flw ft10, 480(gp)
[0x80000898]:flw ft9, 484(gp)
[0x8000089c]:addi sp, zero, 2
[0x800008a0]:csrrw zero, fcsr, sp
[0x800008a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800008a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800008a8]:csrrs tp, fcsr, zero
[0x800008ac]:fsw ft11, 480(ra)
[0x800008b0]:sw tp, 484(ra)
[0x800008b4]:flw ft10, 488(gp)
[0x800008b8]:flw ft9, 492(gp)
[0x800008bc]:addi sp, zero, 34
[0x800008c0]:csrrw zero, fcsr, sp
[0x800008c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800008c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800008c8]:csrrs tp, fcsr, zero
[0x800008cc]:fsw ft11, 488(ra)
[0x800008d0]:sw tp, 492(ra)
[0x800008d4]:flw ft10, 496(gp)
[0x800008d8]:flw ft9, 500(gp)
[0x800008dc]:addi sp, zero, 66
[0x800008e0]:csrrw zero, fcsr, sp
[0x800008e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800008e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800008e8]:csrrs tp, fcsr, zero
[0x800008ec]:fsw ft11, 496(ra)
[0x800008f0]:sw tp, 500(ra)
[0x800008f4]:flw ft10, 504(gp)
[0x800008f8]:flw ft9, 508(gp)
[0x800008fc]:addi sp, zero, 98
[0x80000900]:csrrw zero, fcsr, sp
[0x80000904]:fdiv.s ft11, ft10, ft9, dyn

[0x80000904]:fdiv.s ft11, ft10, ft9, dyn
[0x80000908]:csrrs tp, fcsr, zero
[0x8000090c]:fsw ft11, 504(ra)
[0x80000910]:sw tp, 508(ra)
[0x80000914]:flw ft10, 512(gp)
[0x80000918]:flw ft9, 516(gp)
[0x8000091c]:addi sp, zero, 130
[0x80000920]:csrrw zero, fcsr, sp
[0x80000924]:fdiv.s ft11, ft10, ft9, dyn

[0x80000924]:fdiv.s ft11, ft10, ft9, dyn
[0x80000928]:csrrs tp, fcsr, zero
[0x8000092c]:fsw ft11, 512(ra)
[0x80000930]:sw tp, 516(ra)
[0x80000934]:flw ft10, 520(gp)
[0x80000938]:flw ft9, 524(gp)
[0x8000093c]:addi sp, zero, 2
[0x80000940]:csrrw zero, fcsr, sp
[0x80000944]:fdiv.s ft11, ft10, ft9, dyn

[0x80000944]:fdiv.s ft11, ft10, ft9, dyn
[0x80000948]:csrrs tp, fcsr, zero
[0x8000094c]:fsw ft11, 520(ra)
[0x80000950]:sw tp, 524(ra)
[0x80000954]:flw ft10, 528(gp)
[0x80000958]:flw ft9, 532(gp)
[0x8000095c]:addi sp, zero, 34
[0x80000960]:csrrw zero, fcsr, sp
[0x80000964]:fdiv.s ft11, ft10, ft9, dyn

[0x80000964]:fdiv.s ft11, ft10, ft9, dyn
[0x80000968]:csrrs tp, fcsr, zero
[0x8000096c]:fsw ft11, 528(ra)
[0x80000970]:sw tp, 532(ra)
[0x80000974]:flw ft10, 536(gp)
[0x80000978]:flw ft9, 540(gp)
[0x8000097c]:addi sp, zero, 66
[0x80000980]:csrrw zero, fcsr, sp
[0x80000984]:fdiv.s ft11, ft10, ft9, dyn

[0x80000984]:fdiv.s ft11, ft10, ft9, dyn
[0x80000988]:csrrs tp, fcsr, zero
[0x8000098c]:fsw ft11, 536(ra)
[0x80000990]:sw tp, 540(ra)
[0x80000994]:flw ft10, 544(gp)
[0x80000998]:flw ft9, 548(gp)
[0x8000099c]:addi sp, zero, 98
[0x800009a0]:csrrw zero, fcsr, sp
[0x800009a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800009a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800009a8]:csrrs tp, fcsr, zero
[0x800009ac]:fsw ft11, 544(ra)
[0x800009b0]:sw tp, 548(ra)
[0x800009b4]:flw ft10, 552(gp)
[0x800009b8]:flw ft9, 556(gp)
[0x800009bc]:addi sp, zero, 130
[0x800009c0]:csrrw zero, fcsr, sp
[0x800009c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800009c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800009c8]:csrrs tp, fcsr, zero
[0x800009cc]:fsw ft11, 552(ra)
[0x800009d0]:sw tp, 556(ra)
[0x800009d4]:flw ft10, 560(gp)
[0x800009d8]:flw ft9, 564(gp)
[0x800009dc]:addi sp, zero, 2
[0x800009e0]:csrrw zero, fcsr, sp
[0x800009e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800009e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800009e8]:csrrs tp, fcsr, zero
[0x800009ec]:fsw ft11, 560(ra)
[0x800009f0]:sw tp, 564(ra)
[0x800009f4]:flw ft10, 568(gp)
[0x800009f8]:flw ft9, 572(gp)
[0x800009fc]:addi sp, zero, 34
[0x80000a00]:csrrw zero, fcsr, sp
[0x80000a04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000a04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000a08]:csrrs tp, fcsr, zero
[0x80000a0c]:fsw ft11, 568(ra)
[0x80000a10]:sw tp, 572(ra)
[0x80000a14]:flw ft10, 576(gp)
[0x80000a18]:flw ft9, 580(gp)
[0x80000a1c]:addi sp, zero, 66
[0x80000a20]:csrrw zero, fcsr, sp
[0x80000a24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000a24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000a28]:csrrs tp, fcsr, zero
[0x80000a2c]:fsw ft11, 576(ra)
[0x80000a30]:sw tp, 580(ra)
[0x80000a34]:flw ft10, 584(gp)
[0x80000a38]:flw ft9, 588(gp)
[0x80000a3c]:addi sp, zero, 98
[0x80000a40]:csrrw zero, fcsr, sp
[0x80000a44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000a44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000a48]:csrrs tp, fcsr, zero
[0x80000a4c]:fsw ft11, 584(ra)
[0x80000a50]:sw tp, 588(ra)
[0x80000a54]:flw ft10, 592(gp)
[0x80000a58]:flw ft9, 596(gp)
[0x80000a5c]:addi sp, zero, 130
[0x80000a60]:csrrw zero, fcsr, sp
[0x80000a64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000a64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000a68]:csrrs tp, fcsr, zero
[0x80000a6c]:fsw ft11, 592(ra)
[0x80000a70]:sw tp, 596(ra)
[0x80000a74]:flw ft10, 600(gp)
[0x80000a78]:flw ft9, 604(gp)
[0x80000a7c]:addi sp, zero, 2
[0x80000a80]:csrrw zero, fcsr, sp
[0x80000a84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000a84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000a88]:csrrs tp, fcsr, zero
[0x80000a8c]:fsw ft11, 600(ra)
[0x80000a90]:sw tp, 604(ra)
[0x80000a94]:flw ft10, 608(gp)
[0x80000a98]:flw ft9, 612(gp)
[0x80000a9c]:addi sp, zero, 34
[0x80000aa0]:csrrw zero, fcsr, sp
[0x80000aa4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000aa4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000aa8]:csrrs tp, fcsr, zero
[0x80000aac]:fsw ft11, 608(ra)
[0x80000ab0]:sw tp, 612(ra)
[0x80000ab4]:flw ft10, 616(gp)
[0x80000ab8]:flw ft9, 620(gp)
[0x80000abc]:addi sp, zero, 66
[0x80000ac0]:csrrw zero, fcsr, sp
[0x80000ac4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ac4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ac8]:csrrs tp, fcsr, zero
[0x80000acc]:fsw ft11, 616(ra)
[0x80000ad0]:sw tp, 620(ra)
[0x80000ad4]:flw ft10, 624(gp)
[0x80000ad8]:flw ft9, 628(gp)
[0x80000adc]:addi sp, zero, 98
[0x80000ae0]:csrrw zero, fcsr, sp
[0x80000ae4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ae4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ae8]:csrrs tp, fcsr, zero
[0x80000aec]:fsw ft11, 624(ra)
[0x80000af0]:sw tp, 628(ra)
[0x80000af4]:flw ft10, 632(gp)
[0x80000af8]:flw ft9, 636(gp)
[0x80000afc]:addi sp, zero, 130
[0x80000b00]:csrrw zero, fcsr, sp
[0x80000b04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000b04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000b08]:csrrs tp, fcsr, zero
[0x80000b0c]:fsw ft11, 632(ra)
[0x80000b10]:sw tp, 636(ra)
[0x80000b14]:flw ft10, 640(gp)
[0x80000b18]:flw ft9, 644(gp)
[0x80000b1c]:addi sp, zero, 2
[0x80000b20]:csrrw zero, fcsr, sp
[0x80000b24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000b24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000b28]:csrrs tp, fcsr, zero
[0x80000b2c]:fsw ft11, 640(ra)
[0x80000b30]:sw tp, 644(ra)
[0x80000b34]:flw ft10, 648(gp)
[0x80000b38]:flw ft9, 652(gp)
[0x80000b3c]:addi sp, zero, 34
[0x80000b40]:csrrw zero, fcsr, sp
[0x80000b44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000b44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000b48]:csrrs tp, fcsr, zero
[0x80000b4c]:fsw ft11, 648(ra)
[0x80000b50]:sw tp, 652(ra)
[0x80000b54]:flw ft10, 656(gp)
[0x80000b58]:flw ft9, 660(gp)
[0x80000b5c]:addi sp, zero, 66
[0x80000b60]:csrrw zero, fcsr, sp
[0x80000b64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000b64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000b68]:csrrs tp, fcsr, zero
[0x80000b6c]:fsw ft11, 656(ra)
[0x80000b70]:sw tp, 660(ra)
[0x80000b74]:flw ft10, 664(gp)
[0x80000b78]:flw ft9, 668(gp)
[0x80000b7c]:addi sp, zero, 98
[0x80000b80]:csrrw zero, fcsr, sp
[0x80000b84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000b84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000b88]:csrrs tp, fcsr, zero
[0x80000b8c]:fsw ft11, 664(ra)
[0x80000b90]:sw tp, 668(ra)
[0x80000b94]:flw ft10, 672(gp)
[0x80000b98]:flw ft9, 676(gp)
[0x80000b9c]:addi sp, zero, 130
[0x80000ba0]:csrrw zero, fcsr, sp
[0x80000ba4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ba4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ba8]:csrrs tp, fcsr, zero
[0x80000bac]:fsw ft11, 672(ra)
[0x80000bb0]:sw tp, 676(ra)
[0x80000bb4]:flw ft10, 680(gp)
[0x80000bb8]:flw ft9, 684(gp)
[0x80000bbc]:addi sp, zero, 2
[0x80000bc0]:csrrw zero, fcsr, sp
[0x80000bc4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000bc4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000bc8]:csrrs tp, fcsr, zero
[0x80000bcc]:fsw ft11, 680(ra)
[0x80000bd0]:sw tp, 684(ra)
[0x80000bd4]:flw ft10, 688(gp)
[0x80000bd8]:flw ft9, 692(gp)
[0x80000bdc]:addi sp, zero, 34
[0x80000be0]:csrrw zero, fcsr, sp
[0x80000be4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000be4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000be8]:csrrs tp, fcsr, zero
[0x80000bec]:fsw ft11, 688(ra)
[0x80000bf0]:sw tp, 692(ra)
[0x80000bf4]:flw ft10, 696(gp)
[0x80000bf8]:flw ft9, 700(gp)
[0x80000bfc]:addi sp, zero, 66
[0x80000c00]:csrrw zero, fcsr, sp
[0x80000c04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000c04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000c08]:csrrs tp, fcsr, zero
[0x80000c0c]:fsw ft11, 696(ra)
[0x80000c10]:sw tp, 700(ra)
[0x80000c14]:flw ft10, 704(gp)
[0x80000c18]:flw ft9, 708(gp)
[0x80000c1c]:addi sp, zero, 98
[0x80000c20]:csrrw zero, fcsr, sp
[0x80000c24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000c24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000c28]:csrrs tp, fcsr, zero
[0x80000c2c]:fsw ft11, 704(ra)
[0x80000c30]:sw tp, 708(ra)
[0x80000c34]:flw ft10, 712(gp)
[0x80000c38]:flw ft9, 716(gp)
[0x80000c3c]:addi sp, zero, 130
[0x80000c40]:csrrw zero, fcsr, sp
[0x80000c44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000c44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000c48]:csrrs tp, fcsr, zero
[0x80000c4c]:fsw ft11, 712(ra)
[0x80000c50]:sw tp, 716(ra)
[0x80000c54]:flw ft10, 720(gp)
[0x80000c58]:flw ft9, 724(gp)
[0x80000c5c]:addi sp, zero, 2
[0x80000c60]:csrrw zero, fcsr, sp
[0x80000c64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000c64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000c68]:csrrs tp, fcsr, zero
[0x80000c6c]:fsw ft11, 720(ra)
[0x80000c70]:sw tp, 724(ra)
[0x80000c74]:flw ft10, 728(gp)
[0x80000c78]:flw ft9, 732(gp)
[0x80000c7c]:addi sp, zero, 34
[0x80000c80]:csrrw zero, fcsr, sp
[0x80000c84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000c84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000c88]:csrrs tp, fcsr, zero
[0x80000c8c]:fsw ft11, 728(ra)
[0x80000c90]:sw tp, 732(ra)
[0x80000c94]:flw ft10, 736(gp)
[0x80000c98]:flw ft9, 740(gp)
[0x80000c9c]:addi sp, zero, 66
[0x80000ca0]:csrrw zero, fcsr, sp
[0x80000ca4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ca4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ca8]:csrrs tp, fcsr, zero
[0x80000cac]:fsw ft11, 736(ra)
[0x80000cb0]:sw tp, 740(ra)
[0x80000cb4]:flw ft10, 744(gp)
[0x80000cb8]:flw ft9, 748(gp)
[0x80000cbc]:addi sp, zero, 98
[0x80000cc0]:csrrw zero, fcsr, sp
[0x80000cc4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000cc4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000cc8]:csrrs tp, fcsr, zero
[0x80000ccc]:fsw ft11, 744(ra)
[0x80000cd0]:sw tp, 748(ra)
[0x80000cd4]:flw ft10, 752(gp)
[0x80000cd8]:flw ft9, 756(gp)
[0x80000cdc]:addi sp, zero, 130
[0x80000ce0]:csrrw zero, fcsr, sp
[0x80000ce4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ce4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ce8]:csrrs tp, fcsr, zero
[0x80000cec]:fsw ft11, 752(ra)
[0x80000cf0]:sw tp, 756(ra)
[0x80000cf4]:flw ft10, 760(gp)
[0x80000cf8]:flw ft9, 764(gp)
[0x80000cfc]:addi sp, zero, 2
[0x80000d00]:csrrw zero, fcsr, sp
[0x80000d04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000d04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000d08]:csrrs tp, fcsr, zero
[0x80000d0c]:fsw ft11, 760(ra)
[0x80000d10]:sw tp, 764(ra)
[0x80000d14]:flw ft10, 768(gp)
[0x80000d18]:flw ft9, 772(gp)
[0x80000d1c]:addi sp, zero, 34
[0x80000d20]:csrrw zero, fcsr, sp
[0x80000d24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000d24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000d28]:csrrs tp, fcsr, zero
[0x80000d2c]:fsw ft11, 768(ra)
[0x80000d30]:sw tp, 772(ra)
[0x80000d34]:flw ft10, 776(gp)
[0x80000d38]:flw ft9, 780(gp)
[0x80000d3c]:addi sp, zero, 66
[0x80000d40]:csrrw zero, fcsr, sp
[0x80000d44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000d44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000d48]:csrrs tp, fcsr, zero
[0x80000d4c]:fsw ft11, 776(ra)
[0x80000d50]:sw tp, 780(ra)
[0x80000d54]:flw ft10, 784(gp)
[0x80000d58]:flw ft9, 788(gp)
[0x80000d5c]:addi sp, zero, 98
[0x80000d60]:csrrw zero, fcsr, sp
[0x80000d64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000d64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000d68]:csrrs tp, fcsr, zero
[0x80000d6c]:fsw ft11, 784(ra)
[0x80000d70]:sw tp, 788(ra)
[0x80000d74]:flw ft10, 792(gp)
[0x80000d78]:flw ft9, 796(gp)
[0x80000d7c]:addi sp, zero, 130
[0x80000d80]:csrrw zero, fcsr, sp
[0x80000d84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000d84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000d88]:csrrs tp, fcsr, zero
[0x80000d8c]:fsw ft11, 792(ra)
[0x80000d90]:sw tp, 796(ra)
[0x80000d94]:flw ft10, 800(gp)
[0x80000d98]:flw ft9, 804(gp)
[0x80000d9c]:addi sp, zero, 2
[0x80000da0]:csrrw zero, fcsr, sp
[0x80000da4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000da4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000da8]:csrrs tp, fcsr, zero
[0x80000dac]:fsw ft11, 800(ra)
[0x80000db0]:sw tp, 804(ra)
[0x80000db4]:flw ft10, 808(gp)
[0x80000db8]:flw ft9, 812(gp)
[0x80000dbc]:addi sp, zero, 34
[0x80000dc0]:csrrw zero, fcsr, sp
[0x80000dc4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000dc4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000dc8]:csrrs tp, fcsr, zero
[0x80000dcc]:fsw ft11, 808(ra)
[0x80000dd0]:sw tp, 812(ra)
[0x80000dd4]:flw ft10, 816(gp)
[0x80000dd8]:flw ft9, 820(gp)
[0x80000ddc]:addi sp, zero, 66
[0x80000de0]:csrrw zero, fcsr, sp
[0x80000de4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000de4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000de8]:csrrs tp, fcsr, zero
[0x80000dec]:fsw ft11, 816(ra)
[0x80000df0]:sw tp, 820(ra)
[0x80000df4]:flw ft10, 824(gp)
[0x80000df8]:flw ft9, 828(gp)
[0x80000dfc]:addi sp, zero, 98
[0x80000e00]:csrrw zero, fcsr, sp
[0x80000e04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000e04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000e08]:csrrs tp, fcsr, zero
[0x80000e0c]:fsw ft11, 824(ra)
[0x80000e10]:sw tp, 828(ra)
[0x80000e14]:flw ft10, 832(gp)
[0x80000e18]:flw ft9, 836(gp)
[0x80000e1c]:addi sp, zero, 130
[0x80000e20]:csrrw zero, fcsr, sp
[0x80000e24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000e24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000e28]:csrrs tp, fcsr, zero
[0x80000e2c]:fsw ft11, 832(ra)
[0x80000e30]:sw tp, 836(ra)
[0x80000e34]:flw ft10, 840(gp)
[0x80000e38]:flw ft9, 844(gp)
[0x80000e3c]:addi sp, zero, 2
[0x80000e40]:csrrw zero, fcsr, sp
[0x80000e44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000e44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000e48]:csrrs tp, fcsr, zero
[0x80000e4c]:fsw ft11, 840(ra)
[0x80000e50]:sw tp, 844(ra)
[0x80000e54]:flw ft10, 848(gp)
[0x80000e58]:flw ft9, 852(gp)
[0x80000e5c]:addi sp, zero, 34
[0x80000e60]:csrrw zero, fcsr, sp
[0x80000e64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000e64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000e68]:csrrs tp, fcsr, zero
[0x80000e6c]:fsw ft11, 848(ra)
[0x80000e70]:sw tp, 852(ra)
[0x80000e74]:flw ft10, 856(gp)
[0x80000e78]:flw ft9, 860(gp)
[0x80000e7c]:addi sp, zero, 66
[0x80000e80]:csrrw zero, fcsr, sp
[0x80000e84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000e84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000e88]:csrrs tp, fcsr, zero
[0x80000e8c]:fsw ft11, 856(ra)
[0x80000e90]:sw tp, 860(ra)
[0x80000e94]:flw ft10, 864(gp)
[0x80000e98]:flw ft9, 868(gp)
[0x80000e9c]:addi sp, zero, 98
[0x80000ea0]:csrrw zero, fcsr, sp
[0x80000ea4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ea4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ea8]:csrrs tp, fcsr, zero
[0x80000eac]:fsw ft11, 864(ra)
[0x80000eb0]:sw tp, 868(ra)
[0x80000eb4]:flw ft10, 872(gp)
[0x80000eb8]:flw ft9, 876(gp)
[0x80000ebc]:addi sp, zero, 130
[0x80000ec0]:csrrw zero, fcsr, sp
[0x80000ec4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ec4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ec8]:csrrs tp, fcsr, zero
[0x80000ecc]:fsw ft11, 872(ra)
[0x80000ed0]:sw tp, 876(ra)
[0x80000ed4]:flw ft10, 880(gp)
[0x80000ed8]:flw ft9, 884(gp)
[0x80000edc]:addi sp, zero, 2
[0x80000ee0]:csrrw zero, fcsr, sp
[0x80000ee4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000ee4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000ee8]:csrrs tp, fcsr, zero
[0x80000eec]:fsw ft11, 880(ra)
[0x80000ef0]:sw tp, 884(ra)
[0x80000ef4]:flw ft10, 888(gp)
[0x80000ef8]:flw ft9, 892(gp)
[0x80000efc]:addi sp, zero, 34
[0x80000f00]:csrrw zero, fcsr, sp
[0x80000f04]:fdiv.s ft11, ft10, ft9, dyn

[0x80000f04]:fdiv.s ft11, ft10, ft9, dyn
[0x80000f08]:csrrs tp, fcsr, zero
[0x80000f0c]:fsw ft11, 888(ra)
[0x80000f10]:sw tp, 892(ra)
[0x80000f14]:flw ft10, 896(gp)
[0x80000f18]:flw ft9, 900(gp)
[0x80000f1c]:addi sp, zero, 66
[0x80000f20]:csrrw zero, fcsr, sp
[0x80000f24]:fdiv.s ft11, ft10, ft9, dyn

[0x80000f24]:fdiv.s ft11, ft10, ft9, dyn
[0x80000f28]:csrrs tp, fcsr, zero
[0x80000f2c]:fsw ft11, 896(ra)
[0x80000f30]:sw tp, 900(ra)
[0x80000f34]:flw ft10, 904(gp)
[0x80000f38]:flw ft9, 908(gp)
[0x80000f3c]:addi sp, zero, 98
[0x80000f40]:csrrw zero, fcsr, sp
[0x80000f44]:fdiv.s ft11, ft10, ft9, dyn

[0x80000f44]:fdiv.s ft11, ft10, ft9, dyn
[0x80000f48]:csrrs tp, fcsr, zero
[0x80000f4c]:fsw ft11, 904(ra)
[0x80000f50]:sw tp, 908(ra)
[0x80000f54]:flw ft10, 912(gp)
[0x80000f58]:flw ft9, 916(gp)
[0x80000f5c]:addi sp, zero, 130
[0x80000f60]:csrrw zero, fcsr, sp
[0x80000f64]:fdiv.s ft11, ft10, ft9, dyn

[0x80000f64]:fdiv.s ft11, ft10, ft9, dyn
[0x80000f68]:csrrs tp, fcsr, zero
[0x80000f6c]:fsw ft11, 912(ra)
[0x80000f70]:sw tp, 916(ra)
[0x80000f74]:flw ft10, 920(gp)
[0x80000f78]:flw ft9, 924(gp)
[0x80000f7c]:addi sp, zero, 2
[0x80000f80]:csrrw zero, fcsr, sp
[0x80000f84]:fdiv.s ft11, ft10, ft9, dyn

[0x80000f84]:fdiv.s ft11, ft10, ft9, dyn
[0x80000f88]:csrrs tp, fcsr, zero
[0x80000f8c]:fsw ft11, 920(ra)
[0x80000f90]:sw tp, 924(ra)
[0x80000f94]:flw ft10, 928(gp)
[0x80000f98]:flw ft9, 932(gp)
[0x80000f9c]:addi sp, zero, 34
[0x80000fa0]:csrrw zero, fcsr, sp
[0x80000fa4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000fa4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000fa8]:csrrs tp, fcsr, zero
[0x80000fac]:fsw ft11, 928(ra)
[0x80000fb0]:sw tp, 932(ra)
[0x80000fb4]:flw ft10, 936(gp)
[0x80000fb8]:flw ft9, 940(gp)
[0x80000fbc]:addi sp, zero, 66
[0x80000fc0]:csrrw zero, fcsr, sp
[0x80000fc4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000fc4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000fc8]:csrrs tp, fcsr, zero
[0x80000fcc]:fsw ft11, 936(ra)
[0x80000fd0]:sw tp, 940(ra)
[0x80000fd4]:flw ft10, 944(gp)
[0x80000fd8]:flw ft9, 948(gp)
[0x80000fdc]:addi sp, zero, 98
[0x80000fe0]:csrrw zero, fcsr, sp
[0x80000fe4]:fdiv.s ft11, ft10, ft9, dyn

[0x80000fe4]:fdiv.s ft11, ft10, ft9, dyn
[0x80000fe8]:csrrs tp, fcsr, zero
[0x80000fec]:fsw ft11, 944(ra)
[0x80000ff0]:sw tp, 948(ra)
[0x80000ff4]:flw ft10, 952(gp)
[0x80000ff8]:flw ft9, 956(gp)
[0x80000ffc]:addi sp, zero, 130
[0x80001000]:csrrw zero, fcsr, sp
[0x80001004]:fdiv.s ft11, ft10, ft9, dyn

[0x80001004]:fdiv.s ft11, ft10, ft9, dyn
[0x80001008]:csrrs tp, fcsr, zero
[0x8000100c]:fsw ft11, 952(ra)
[0x80001010]:sw tp, 956(ra)
[0x80001014]:flw ft10, 960(gp)
[0x80001018]:flw ft9, 964(gp)
[0x8000101c]:addi sp, zero, 2
[0x80001020]:csrrw zero, fcsr, sp
[0x80001024]:fdiv.s ft11, ft10, ft9, dyn

[0x80001024]:fdiv.s ft11, ft10, ft9, dyn
[0x80001028]:csrrs tp, fcsr, zero
[0x8000102c]:fsw ft11, 960(ra)
[0x80001030]:sw tp, 964(ra)
[0x80001034]:flw ft10, 968(gp)
[0x80001038]:flw ft9, 972(gp)
[0x8000103c]:addi sp, zero, 34
[0x80001040]:csrrw zero, fcsr, sp
[0x80001044]:fdiv.s ft11, ft10, ft9, dyn

[0x80001044]:fdiv.s ft11, ft10, ft9, dyn
[0x80001048]:csrrs tp, fcsr, zero
[0x8000104c]:fsw ft11, 968(ra)
[0x80001050]:sw tp, 972(ra)
[0x80001054]:flw ft10, 976(gp)
[0x80001058]:flw ft9, 980(gp)
[0x8000105c]:addi sp, zero, 66
[0x80001060]:csrrw zero, fcsr, sp
[0x80001064]:fdiv.s ft11, ft10, ft9, dyn

[0x80001064]:fdiv.s ft11, ft10, ft9, dyn
[0x80001068]:csrrs tp, fcsr, zero
[0x8000106c]:fsw ft11, 976(ra)
[0x80001070]:sw tp, 980(ra)
[0x80001074]:flw ft10, 984(gp)
[0x80001078]:flw ft9, 988(gp)
[0x8000107c]:addi sp, zero, 98
[0x80001080]:csrrw zero, fcsr, sp
[0x80001084]:fdiv.s ft11, ft10, ft9, dyn

[0x80001084]:fdiv.s ft11, ft10, ft9, dyn
[0x80001088]:csrrs tp, fcsr, zero
[0x8000108c]:fsw ft11, 984(ra)
[0x80001090]:sw tp, 988(ra)
[0x80001094]:flw ft10, 992(gp)
[0x80001098]:flw ft9, 996(gp)
[0x8000109c]:addi sp, zero, 130
[0x800010a0]:csrrw zero, fcsr, sp
[0x800010a4]:fdiv.s ft11, ft10, ft9, dyn

[0x800010a4]:fdiv.s ft11, ft10, ft9, dyn
[0x800010a8]:csrrs tp, fcsr, zero
[0x800010ac]:fsw ft11, 992(ra)
[0x800010b0]:sw tp, 996(ra)
[0x800010b4]:flw ft10, 1000(gp)
[0x800010b8]:flw ft9, 1004(gp)
[0x800010bc]:addi sp, zero, 2
[0x800010c0]:csrrw zero, fcsr, sp
[0x800010c4]:fdiv.s ft11, ft10, ft9, dyn

[0x800010c4]:fdiv.s ft11, ft10, ft9, dyn
[0x800010c8]:csrrs tp, fcsr, zero
[0x800010cc]:fsw ft11, 1000(ra)
[0x800010d0]:sw tp, 1004(ra)
[0x800010d4]:flw ft10, 1008(gp)
[0x800010d8]:flw ft9, 1012(gp)
[0x800010dc]:addi sp, zero, 34
[0x800010e0]:csrrw zero, fcsr, sp
[0x800010e4]:fdiv.s ft11, ft10, ft9, dyn

[0x800010e4]:fdiv.s ft11, ft10, ft9, dyn
[0x800010e8]:csrrs tp, fcsr, zero
[0x800010ec]:fsw ft11, 1008(ra)
[0x800010f0]:sw tp, 1012(ra)
[0x800010f4]:flw ft10, 1016(gp)
[0x800010f8]:flw ft9, 1020(gp)
[0x800010fc]:addi sp, zero, 66
[0x80001100]:csrrw zero, fcsr, sp
[0x80001104]:fdiv.s ft11, ft10, ft9, dyn

[0x80001104]:fdiv.s ft11, ft10, ft9, dyn
[0x80001108]:csrrs tp, fcsr, zero
[0x8000110c]:fsw ft11, 1016(ra)
[0x80001110]:sw tp, 1020(ra)
[0x80001114]:auipc ra, 3
[0x80001118]:addi ra, ra, 2304
[0x8000111c]:flw ft10, 1024(gp)
[0x80001120]:flw ft9, 1028(gp)
[0x80001124]:addi sp, zero, 98
[0x80001128]:csrrw zero, fcsr, sp
[0x8000112c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000112c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001130]:csrrs tp, fcsr, zero
[0x80001134]:fsw ft11, 0(ra)
[0x80001138]:sw tp, 4(ra)
[0x8000113c]:flw ft10, 1032(gp)
[0x80001140]:flw ft9, 1036(gp)
[0x80001144]:addi sp, zero, 130
[0x80001148]:csrrw zero, fcsr, sp
[0x8000114c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000114c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001150]:csrrs tp, fcsr, zero
[0x80001154]:fsw ft11, 8(ra)
[0x80001158]:sw tp, 12(ra)
[0x8000115c]:flw ft10, 1040(gp)
[0x80001160]:flw ft9, 1044(gp)
[0x80001164]:addi sp, zero, 2
[0x80001168]:csrrw zero, fcsr, sp
[0x8000116c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000116c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001170]:csrrs tp, fcsr, zero
[0x80001174]:fsw ft11, 16(ra)
[0x80001178]:sw tp, 20(ra)
[0x8000117c]:flw ft10, 1048(gp)
[0x80001180]:flw ft9, 1052(gp)
[0x80001184]:addi sp, zero, 34
[0x80001188]:csrrw zero, fcsr, sp
[0x8000118c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000118c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001190]:csrrs tp, fcsr, zero
[0x80001194]:fsw ft11, 24(ra)
[0x80001198]:sw tp, 28(ra)
[0x8000119c]:flw ft10, 1056(gp)
[0x800011a0]:flw ft9, 1060(gp)
[0x800011a4]:addi sp, zero, 66
[0x800011a8]:csrrw zero, fcsr, sp
[0x800011ac]:fdiv.s ft11, ft10, ft9, dyn

[0x800011ac]:fdiv.s ft11, ft10, ft9, dyn
[0x800011b0]:csrrs tp, fcsr, zero
[0x800011b4]:fsw ft11, 32(ra)
[0x800011b8]:sw tp, 36(ra)
[0x800011bc]:flw ft10, 1064(gp)
[0x800011c0]:flw ft9, 1068(gp)
[0x800011c4]:addi sp, zero, 98
[0x800011c8]:csrrw zero, fcsr, sp
[0x800011cc]:fdiv.s ft11, ft10, ft9, dyn

[0x800011cc]:fdiv.s ft11, ft10, ft9, dyn
[0x800011d0]:csrrs tp, fcsr, zero
[0x800011d4]:fsw ft11, 40(ra)
[0x800011d8]:sw tp, 44(ra)
[0x800011dc]:flw ft10, 1072(gp)
[0x800011e0]:flw ft9, 1076(gp)
[0x800011e4]:addi sp, zero, 130
[0x800011e8]:csrrw zero, fcsr, sp
[0x800011ec]:fdiv.s ft11, ft10, ft9, dyn

[0x800011ec]:fdiv.s ft11, ft10, ft9, dyn
[0x800011f0]:csrrs tp, fcsr, zero
[0x800011f4]:fsw ft11, 48(ra)
[0x800011f8]:sw tp, 52(ra)
[0x800011fc]:flw ft10, 1080(gp)
[0x80001200]:flw ft9, 1084(gp)
[0x80001204]:addi sp, zero, 2
[0x80001208]:csrrw zero, fcsr, sp
[0x8000120c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000120c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001210]:csrrs tp, fcsr, zero
[0x80001214]:fsw ft11, 56(ra)
[0x80001218]:sw tp, 60(ra)
[0x8000121c]:flw ft10, 1088(gp)
[0x80001220]:flw ft9, 1092(gp)
[0x80001224]:addi sp, zero, 34
[0x80001228]:csrrw zero, fcsr, sp
[0x8000122c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000122c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001230]:csrrs tp, fcsr, zero
[0x80001234]:fsw ft11, 64(ra)
[0x80001238]:sw tp, 68(ra)
[0x8000123c]:flw ft10, 1096(gp)
[0x80001240]:flw ft9, 1100(gp)
[0x80001244]:addi sp, zero, 66
[0x80001248]:csrrw zero, fcsr, sp
[0x8000124c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000124c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001250]:csrrs tp, fcsr, zero
[0x80001254]:fsw ft11, 72(ra)
[0x80001258]:sw tp, 76(ra)
[0x8000125c]:flw ft10, 1104(gp)
[0x80001260]:flw ft9, 1108(gp)
[0x80001264]:addi sp, zero, 98
[0x80001268]:csrrw zero, fcsr, sp
[0x8000126c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000126c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001270]:csrrs tp, fcsr, zero
[0x80001274]:fsw ft11, 80(ra)
[0x80001278]:sw tp, 84(ra)
[0x8000127c]:flw ft10, 1112(gp)
[0x80001280]:flw ft9, 1116(gp)
[0x80001284]:addi sp, zero, 130
[0x80001288]:csrrw zero, fcsr, sp
[0x8000128c]:fdiv.s ft11, ft10, ft9, dyn

[0x8000128c]:fdiv.s ft11, ft10, ft9, dyn
[0x80001290]:csrrs tp, fcsr, zero
[0x80001294]:fsw ft11, 88(ra)
[0x80001298]:sw tp, 92(ra)
[0x8000129c]:flw ft10, 1120(gp)
[0x800012a0]:flw ft9, 1124(gp)
[0x800012a4]:addi sp, zero, 2
[0x800012a8]:csrrw zero, fcsr, sp
[0x800012ac]:fdiv.s ft11, ft10, ft9, dyn

[0x800012ac]:fdiv.s ft11, ft10, ft9, dyn
[0x800012b0]:csrrs tp, fcsr, zero
[0x800012b4]:fsw ft11, 96(ra)
[0x800012b8]:sw tp, 100(ra)
[0x800012bc]:flw ft10, 1128(gp)
[0x800012c0]:flw ft9, 1132(gp)
[0x800012c4]:addi sp, zero, 66
[0x800012c8]:csrrw zero, fcsr, sp
[0x800012cc]:fdiv.s ft11, ft10, ft9, dyn

[0x800012cc]:fdiv.s ft11, ft10, ft9, dyn
[0x800012d0]:csrrs tp, fcsr, zero
[0x800012d4]:fsw ft11, 104(ra)
[0x800012d8]:sw tp, 108(ra)
[0x800012dc]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : f31', 'rs2 : f31', 'rd : f31', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000124]:fdiv.s ft11, ft11, ft11, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:fsw ft11, 0(ra)
	-[0x80000130]:sw tp, 4(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80003618]:0x00000002




Last Coverpoint : ['rs1 : f29', 'rs2 : f30', 'rd : f30', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0x7c and fm2 == 0x73acff and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.s ft10, ft9, ft10, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:fsw ft10, 8(ra)
	-[0x80000150]:sw tp, 12(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80003620]:0x00000023




Last Coverpoint : ['rs1 : f28', 'rs2 : f28', 'rd : f29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fdiv.s ft9, ft8, ft8, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:fsw ft9, 16(ra)
	-[0x80000170]:sw tp, 20(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80003628]:0x00000042




Last Coverpoint : ['rs1 : f27', 'rs2 : f29', 'rd : f27', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0x7c and fm2 == 0x73acff and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.s fs11, fs11, ft9, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:fsw fs11, 24(ra)
	-[0x80000190]:sw tp, 28(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80003630]:0x00000063




Last Coverpoint : ['rs1 : f30', 'rs2 : f27', 'rd : f28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0x7c and fm2 == 0x73acff and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001a4]:fdiv.s ft8, ft10, fs11, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:fsw ft8, 32(ra)
	-[0x800001b0]:sw tp, 36(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80003638]:0x00000083




Last Coverpoint : ['rs1 : f25', 'rs2 : f24', 'rd : f26', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3d6ac3 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.s fs10, fs9, fs8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:fsw fs10, 40(ra)
	-[0x800001d0]:sw tp, 44(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80003640]:0x00000003




Last Coverpoint : ['rs1 : f24', 'rs2 : f26', 'rd : f25', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3d6ac3 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.s fs9, fs8, fs10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:fsw fs9, 48(ra)
	-[0x800001f0]:sw tp, 52(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80003648]:0x00000023




Last Coverpoint : ['rs1 : f26', 'rs2 : f25', 'rd : f24', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3d6ac3 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.s fs8, fs10, fs9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:fsw fs8, 56(ra)
	-[0x80000210]:sw tp, 60(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80003650]:0x00000043




Last Coverpoint : ['rs1 : f22', 'rs2 : f21', 'rd : f23', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3d6ac3 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.s fs7, fs6, fs5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:fsw fs7, 64(ra)
	-[0x80000230]:sw tp, 68(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80003658]:0x00000063




Last Coverpoint : ['rs1 : f21', 'rs2 : f23', 'rd : f22', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x0e1012 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x3d6ac3 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.s fs6, fs5, fs7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:fsw fs6, 72(ra)
	-[0x80000250]:sw tp, 76(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80003660]:0x00000083




Last Coverpoint : ['rs1 : f23', 'rs2 : f22', 'rd : f21', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0x7e and fm2 == 0x202f34 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.s fs5, fs7, fs6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:fsw fs5, 80(ra)
	-[0x80000270]:sw tp, 84(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80003668]:0x00000002




Last Coverpoint : ['rs1 : f19', 'rs2 : f18', 'rd : f20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0x7e and fm2 == 0x202f34 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.s fs4, fs3, fs2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:fsw fs4, 88(ra)
	-[0x80000290]:sw tp, 92(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80003670]:0x00000022




Last Coverpoint : ['rs1 : f18', 'rs2 : f20', 'rd : f19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0x7e and fm2 == 0x202f34 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.s fs3, fs2, fs4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:fsw fs3, 96(ra)
	-[0x800002b0]:sw tp, 100(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80003678]:0x00000042




Last Coverpoint : ['rs1 : f20', 'rs2 : f19', 'rd : f18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0x7e and fm2 == 0x202f34 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.s fs2, fs4, fs3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:fsw fs2, 104(ra)
	-[0x800002d0]:sw tp, 108(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80003680]:0x00000062




Last Coverpoint : ['rs1 : f16', 'rs2 : f15', 'rd : f17', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7046ce and fs2 == 0 and fe2 == 0x7e and fm2 == 0x202f34 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.s fa7, fa6, fa5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:fsw fa7, 112(ra)
	-[0x800002f0]:sw tp, 116(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80003688]:0x00000082




Last Coverpoint : ['rs1 : f15', 'rs2 : f17', 'rd : f16', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0x78 and fm2 == 0x57be8f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.s fa6, fa5, fa7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:fsw fa6, 120(ra)
	-[0x80000310]:sw tp, 124(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80003690]:0x00000003




Last Coverpoint : ['rs1 : f17', 'rs2 : f16', 'rd : f15', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0x78 and fm2 == 0x57be8f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.s fa5, fa7, fa6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:fsw fa5, 128(ra)
	-[0x80000330]:sw tp, 132(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80003698]:0x00000023




Last Coverpoint : ['rs1 : f13', 'rs2 : f12', 'rd : f14', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0x78 and fm2 == 0x57be8f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.s fa4, fa3, fa2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:fsw fa4, 136(ra)
	-[0x80000350]:sw tp, 140(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800036a0]:0x00000043




Last Coverpoint : ['rs1 : f12', 'rs2 : f14', 'rd : f13', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0x78 and fm2 == 0x57be8f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.s fa3, fa2, fa4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:fsw fa3, 144(ra)
	-[0x80000370]:sw tp, 148(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800036a8]:0x00000063




Last Coverpoint : ['rs1 : f14', 'rs2 : f13', 'rd : f12', 'fs1 == 0 and fe1 == 0xf8 and fm1 == 0x21ceeb and fs2 == 0 and fe2 == 0x78 and fm2 == 0x57be8f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.s fa2, fa4, fa3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:fsw fa2, 152(ra)
	-[0x80000390]:sw tp, 156(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800036b0]:0x00000083




Last Coverpoint : ['rs1 : f10', 'rs2 : f9', 'rd : f11', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x04ca0c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.s fa1, fa0, fs1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:fsw fa1, 160(ra)
	-[0x800003b0]:sw tp, 164(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800036b8]:0x00000002




Last Coverpoint : ['rs1 : f9', 'rs2 : f11', 'rd : f10', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x04ca0c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.s fa0, fs1, fa1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:fsw fa0, 168(ra)
	-[0x800003d0]:sw tp, 172(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800036c0]:0x00000022




Last Coverpoint : ['rs1 : f11', 'rs2 : f10', 'rd : f9', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x04ca0c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003e4]:fdiv.s fs1, fa1, fa0, dyn
	-[0x800003e8]:csrrs tp, fcsr, zero
	-[0x800003ec]:fsw fs1, 176(ra)
	-[0x800003f0]:sw tp, 180(ra)
Current Store : [0x800003f0] : sw tp, 180(ra) -- Store: [0x800036c8]:0x00000042




Last Coverpoint : ['rs1 : f7', 'rs2 : f6', 'rd : f8', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x04ca0c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000404]:fdiv.s fs0, ft7, ft6, dyn
	-[0x80000408]:csrrs tp, fcsr, zero
	-[0x8000040c]:fsw fs0, 184(ra)
	-[0x80000410]:sw tp, 188(ra)
Current Store : [0x80000410] : sw tp, 188(ra) -- Store: [0x800036d0]:0x00000062




Last Coverpoint : ['rs1 : f6', 'rs2 : f8', 'rd : f7', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x472f12 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x04ca0c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000424]:fdiv.s ft7, ft6, fs0, dyn
	-[0x80000428]:csrrs tp, fcsr, zero
	-[0x8000042c]:fsw ft7, 192(ra)
	-[0x80000430]:sw tp, 196(ra)
Current Store : [0x80000430] : sw tp, 196(ra) -- Store: [0x800036d8]:0x00000082




Last Coverpoint : ['rs1 : f8', 'rs2 : f7', 'rd : f6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ee294 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000444]:fdiv.s ft6, fs0, ft7, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:fsw ft6, 200(ra)
	-[0x80000450]:sw tp, 204(ra)
Current Store : [0x80000450] : sw tp, 204(ra) -- Store: [0x800036e0]:0x00000003




Last Coverpoint : ['rs1 : f4', 'rs2 : f3', 'rd : f5', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ee294 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000464]:fdiv.s ft5, ft4, ft3, dyn
	-[0x80000468]:csrrs tp, fcsr, zero
	-[0x8000046c]:fsw ft5, 208(ra)
	-[0x80000470]:sw tp, 212(ra)
Current Store : [0x80000470] : sw tp, 212(ra) -- Store: [0x800036e8]:0x00000023




Last Coverpoint : ['rs1 : f3', 'rs2 : f5', 'rd : f4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ee294 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000484]:fdiv.s ft4, ft3, ft5, dyn
	-[0x80000488]:csrrs tp, fcsr, zero
	-[0x8000048c]:fsw ft4, 216(ra)
	-[0x80000490]:sw tp, 220(ra)
Current Store : [0x80000490] : sw tp, 220(ra) -- Store: [0x800036f0]:0x00000043




Last Coverpoint : ['rs1 : f5', 'rs2 : f4', 'rd : f3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ee294 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004a4]:fdiv.s ft3, ft5, ft4, dyn
	-[0x800004a8]:csrrs tp, fcsr, zero
	-[0x800004ac]:fsw ft3, 224(ra)
	-[0x800004b0]:sw tp, 228(ra)
Current Store : [0x800004b0] : sw tp, 228(ra) -- Store: [0x800036f8]:0x00000063




Last Coverpoint : ['rs1 : f1', 'rs2 : f0', 'rd : f2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3f29ee and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7ee294 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004c4]:fdiv.s ft2, ft1, ft0, dyn
	-[0x800004c8]:csrrs tp, fcsr, zero
	-[0x800004cc]:fsw ft2, 232(ra)
	-[0x800004d0]:sw tp, 236(ra)
Current Store : [0x800004d0] : sw tp, 236(ra) -- Store: [0x80003700]:0x00000083




Last Coverpoint : ['rs1 : f0', 'rs2 : f2', 'rd : f1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x562256 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004e4]:fdiv.s ft1, ft0, ft2, dyn
	-[0x800004e8]:csrrs tp, fcsr, zero
	-[0x800004ec]:fsw ft1, 240(ra)
	-[0x800004f0]:sw tp, 244(ra)
Current Store : [0x800004f0] : sw tp, 244(ra) -- Store: [0x80003708]:0x00000003




Last Coverpoint : ['rs1 : f2', 'rs2 : f1', 'rd : f0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x562256 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000504]:fdiv.s ft0, ft2, ft1, dyn
	-[0x80000508]:csrrs tp, fcsr, zero
	-[0x8000050c]:fsw ft0, 248(ra)
	-[0x80000510]:sw tp, 252(ra)
Current Store : [0x80000510] : sw tp, 252(ra) -- Store: [0x80003710]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x562256 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000524]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:fsw ft11, 256(ra)
	-[0x80000530]:sw tp, 260(ra)
Current Store : [0x80000530] : sw tp, 260(ra) -- Store: [0x80003718]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x562256 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000544]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000548]:csrrs tp, fcsr, zero
	-[0x8000054c]:fsw ft11, 264(ra)
	-[0x80000550]:sw tp, 268(ra)
Current Store : [0x80000550] : sw tp, 268(ra) -- Store: [0x80003720]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2099c0 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x562256 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000564]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000568]:csrrs tp, fcsr, zero
	-[0x8000056c]:fsw ft11, 272(ra)
	-[0x80000570]:sw tp, 276(ra)
Current Store : [0x80000570] : sw tp, 276(ra) -- Store: [0x80003728]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2399f8 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000584]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000588]:csrrs tp, fcsr, zero
	-[0x8000058c]:fsw ft11, 280(ra)
	-[0x80000590]:sw tp, 284(ra)
Current Store : [0x80000590] : sw tp, 284(ra) -- Store: [0x80003730]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2399f8 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800005a8]:csrrs tp, fcsr, zero
	-[0x800005ac]:fsw ft11, 288(ra)
	-[0x800005b0]:sw tp, 292(ra)
Current Store : [0x800005b0] : sw tp, 292(ra) -- Store: [0x80003738]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2399f8 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800005c8]:csrrs tp, fcsr, zero
	-[0x800005cc]:fsw ft11, 296(ra)
	-[0x800005d0]:sw tp, 300(ra)
Current Store : [0x800005d0] : sw tp, 300(ra) -- Store: [0x80003740]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2399f8 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800005e8]:csrrs tp, fcsr, zero
	-[0x800005ec]:fsw ft11, 304(ra)
	-[0x800005f0]:sw tp, 308(ra)
Current Store : [0x800005f0] : sw tp, 308(ra) -- Store: [0x80003748]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7566f3 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2399f8 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000604]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:fsw ft11, 312(ra)
	-[0x80000610]:sw tp, 316(ra)
Current Store : [0x80000610] : sw tp, 316(ra) -- Store: [0x80003750]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x22863a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000624]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000628]:csrrs tp, fcsr, zero
	-[0x8000062c]:fsw ft11, 320(ra)
	-[0x80000630]:sw tp, 324(ra)
Current Store : [0x80000630] : sw tp, 324(ra) -- Store: [0x80003758]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x22863a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000644]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000648]:csrrs tp, fcsr, zero
	-[0x8000064c]:fsw ft11, 328(ra)
	-[0x80000650]:sw tp, 332(ra)
Current Store : [0x80000650] : sw tp, 332(ra) -- Store: [0x80003760]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x22863a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000664]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000668]:csrrs tp, fcsr, zero
	-[0x8000066c]:fsw ft11, 336(ra)
	-[0x80000670]:sw tp, 340(ra)
Current Store : [0x80000670] : sw tp, 340(ra) -- Store: [0x80003768]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x22863a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000684]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000688]:csrrs tp, fcsr, zero
	-[0x8000068c]:fsw ft11, 344(ra)
	-[0x80000690]:sw tp, 348(ra)
Current Store : [0x80000690] : sw tp, 348(ra) -- Store: [0x80003770]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x73c956 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x22863a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800006a8]:csrrs tp, fcsr, zero
	-[0x800006ac]:fsw ft11, 352(ra)
	-[0x800006b0]:sw tp, 356(ra)
Current Store : [0x800006b0] : sw tp, 356(ra) -- Store: [0x80003778]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x05ef34 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800006c8]:csrrs tp, fcsr, zero
	-[0x800006cc]:fsw ft11, 360(ra)
	-[0x800006d0]:sw tp, 364(ra)
Current Store : [0x800006d0] : sw tp, 364(ra) -- Store: [0x80003780]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x05ef34 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800006e8]:csrrs tp, fcsr, zero
	-[0x800006ec]:fsw ft11, 368(ra)
	-[0x800006f0]:sw tp, 372(ra)
Current Store : [0x800006f0] : sw tp, 372(ra) -- Store: [0x80003788]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x05ef34 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000704]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000708]:csrrs tp, fcsr, zero
	-[0x8000070c]:fsw ft11, 376(ra)
	-[0x80000710]:sw tp, 380(ra)
Current Store : [0x80000710] : sw tp, 380(ra) -- Store: [0x80003790]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x05ef34 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000724]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000728]:csrrs tp, fcsr, zero
	-[0x8000072c]:fsw ft11, 384(ra)
	-[0x80000730]:sw tp, 388(ra)
Current Store : [0x80000730] : sw tp, 388(ra) -- Store: [0x80003798]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x48e6cd and fs2 == 0 and fe2 == 0x7e and fm2 == 0x05ef34 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000744]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000748]:csrrs tp, fcsr, zero
	-[0x8000074c]:fsw ft11, 392(ra)
	-[0x80000750]:sw tp, 396(ra)
Current Store : [0x80000750] : sw tp, 396(ra) -- Store: [0x800037a0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2be19f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000764]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000768]:csrrs tp, fcsr, zero
	-[0x8000076c]:fsw ft11, 400(ra)
	-[0x80000770]:sw tp, 404(ra)
Current Store : [0x80000770] : sw tp, 404(ra) -- Store: [0x800037a8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2be19f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000784]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000788]:csrrs tp, fcsr, zero
	-[0x8000078c]:fsw ft11, 408(ra)
	-[0x80000790]:sw tp, 412(ra)
Current Store : [0x80000790] : sw tp, 412(ra) -- Store: [0x800037b0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2be19f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800007a8]:csrrs tp, fcsr, zero
	-[0x800007ac]:fsw ft11, 416(ra)
	-[0x800007b0]:sw tp, 420(ra)
Current Store : [0x800007b0] : sw tp, 420(ra) -- Store: [0x800037b8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2be19f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800007c8]:csrrs tp, fcsr, zero
	-[0x800007cc]:fsw ft11, 424(ra)
	-[0x800007d0]:sw tp, 428(ra)
Current Store : [0x800007d0] : sw tp, 428(ra) -- Store: [0x800037c0]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e937 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x2be19f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800007e8]:csrrs tp, fcsr, zero
	-[0x800007ec]:fsw ft11, 432(ra)
	-[0x800007f0]:sw tp, 436(ra)
Current Store : [0x800007f0] : sw tp, 436(ra) -- Store: [0x800037c8]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x421b61 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000804]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000808]:csrrs tp, fcsr, zero
	-[0x8000080c]:fsw ft11, 440(ra)
	-[0x80000810]:sw tp, 444(ra)
Current Store : [0x80000810] : sw tp, 444(ra) -- Store: [0x800037d0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x421b61 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000824]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000828]:csrrs tp, fcsr, zero
	-[0x8000082c]:fsw ft11, 448(ra)
	-[0x80000830]:sw tp, 452(ra)
Current Store : [0x80000830] : sw tp, 452(ra) -- Store: [0x800037d8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x421b61 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000844]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000848]:csrrs tp, fcsr, zero
	-[0x8000084c]:fsw ft11, 456(ra)
	-[0x80000850]:sw tp, 460(ra)
Current Store : [0x80000850] : sw tp, 460(ra) -- Store: [0x800037e0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x421b61 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000864]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000868]:csrrs tp, fcsr, zero
	-[0x8000086c]:fsw ft11, 464(ra)
	-[0x80000870]:sw tp, 468(ra)
Current Store : [0x80000870] : sw tp, 468(ra) -- Store: [0x800037e8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x119488 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x421b61 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000884]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000888]:csrrs tp, fcsr, zero
	-[0x8000088c]:fsw ft11, 472(ra)
	-[0x80000890]:sw tp, 476(ra)
Current Store : [0x80000890] : sw tp, 476(ra) -- Store: [0x800037f0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x1a3fc1 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800008a8]:csrrs tp, fcsr, zero
	-[0x800008ac]:fsw ft11, 480(ra)
	-[0x800008b0]:sw tp, 484(ra)
Current Store : [0x800008b0] : sw tp, 484(ra) -- Store: [0x800037f8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x1a3fc1 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800008c8]:csrrs tp, fcsr, zero
	-[0x800008cc]:fsw ft11, 488(ra)
	-[0x800008d0]:sw tp, 492(ra)
Current Store : [0x800008d0] : sw tp, 492(ra) -- Store: [0x80003800]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x1a3fc1 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800008e8]:csrrs tp, fcsr, zero
	-[0x800008ec]:fsw ft11, 496(ra)
	-[0x800008f0]:sw tp, 500(ra)
Current Store : [0x800008f0] : sw tp, 500(ra) -- Store: [0x80003808]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x1a3fc1 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000904]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000908]:csrrs tp, fcsr, zero
	-[0x8000090c]:fsw ft11, 504(ra)
	-[0x80000910]:sw tp, 508(ra)
Current Store : [0x80000910] : sw tp, 508(ra) -- Store: [0x80003810]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x675fa1 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x1a3fc1 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000924]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000928]:csrrs tp, fcsr, zero
	-[0x8000092c]:fsw ft11, 512(ra)
	-[0x80000930]:sw tp, 516(ra)
Current Store : [0x80000930] : sw tp, 516(ra) -- Store: [0x80003818]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5d9331 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000944]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000948]:csrrs tp, fcsr, zero
	-[0x8000094c]:fsw ft11, 520(ra)
	-[0x80000950]:sw tp, 524(ra)
Current Store : [0x80000950] : sw tp, 524(ra) -- Store: [0x80003820]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5d9331 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000964]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000968]:csrrs tp, fcsr, zero
	-[0x8000096c]:fsw ft11, 528(ra)
	-[0x80000970]:sw tp, 532(ra)
Current Store : [0x80000970] : sw tp, 532(ra) -- Store: [0x80003828]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5d9331 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000984]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000988]:csrrs tp, fcsr, zero
	-[0x8000098c]:fsw ft11, 536(ra)
	-[0x80000990]:sw tp, 540(ra)
Current Store : [0x80000990] : sw tp, 540(ra) -- Store: [0x80003830]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5d9331 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800009a8]:csrrs tp, fcsr, zero
	-[0x800009ac]:fsw ft11, 544(ra)
	-[0x800009b0]:sw tp, 548(ra)
Current Store : [0x800009b0] : sw tp, 548(ra) -- Store: [0x80003838]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x262e64 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x5d9331 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800009c8]:csrrs tp, fcsr, zero
	-[0x800009cc]:fsw ft11, 552(ra)
	-[0x800009d0]:sw tp, 556(ra)
Current Store : [0x800009d0] : sw tp, 556(ra) -- Store: [0x80003840]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3804d8 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800009e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800009e8]:csrrs tp, fcsr, zero
	-[0x800009ec]:fsw ft11, 560(ra)
	-[0x800009f0]:sw tp, 564(ra)
Current Store : [0x800009f0] : sw tp, 564(ra) -- Store: [0x80003848]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3804d8 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000a08]:csrrs tp, fcsr, zero
	-[0x80000a0c]:fsw ft11, 568(ra)
	-[0x80000a10]:sw tp, 572(ra)
Current Store : [0x80000a10] : sw tp, 572(ra) -- Store: [0x80003850]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3804d8 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000a28]:csrrs tp, fcsr, zero
	-[0x80000a2c]:fsw ft11, 576(ra)
	-[0x80000a30]:sw tp, 580(ra)
Current Store : [0x80000a30] : sw tp, 580(ra) -- Store: [0x80003858]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3804d8 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000a48]:csrrs tp, fcsr, zero
	-[0x80000a4c]:fsw ft11, 584(ra)
	-[0x80000a50]:sw tp, 588(ra)
Current Store : [0x80000a50] : sw tp, 588(ra) -- Store: [0x80003860]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a03a1 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3804d8 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000a68]:csrrs tp, fcsr, zero
	-[0x80000a6c]:fsw ft11, 592(ra)
	-[0x80000a70]:sw tp, 596(ra)
Current Store : [0x80000a70] : sw tp, 596(ra) -- Store: [0x80003868]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x36d131 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000a84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000a88]:csrrs tp, fcsr, zero
	-[0x80000a8c]:fsw ft11, 600(ra)
	-[0x80000a90]:sw tp, 604(ra)
Current Store : [0x80000a90] : sw tp, 604(ra) -- Store: [0x80003870]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x36d131 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000aa8]:csrrs tp, fcsr, zero
	-[0x80000aac]:fsw ft11, 608(ra)
	-[0x80000ab0]:sw tp, 612(ra)
Current Store : [0x80000ab0] : sw tp, 612(ra) -- Store: [0x80003878]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x36d131 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ac4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ac8]:csrrs tp, fcsr, zero
	-[0x80000acc]:fsw ft11, 616(ra)
	-[0x80000ad0]:sw tp, 620(ra)
Current Store : [0x80000ad0] : sw tp, 620(ra) -- Store: [0x80003880]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x36d131 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ae4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ae8]:csrrs tp, fcsr, zero
	-[0x80000aec]:fsw ft11, 624(ra)
	-[0x80000af0]:sw tp, 628(ra)
Current Store : [0x80000af0] : sw tp, 628(ra) -- Store: [0x80003888]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x091ce4 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x36d131 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000b08]:csrrs tp, fcsr, zero
	-[0x80000b0c]:fsw ft11, 632(ra)
	-[0x80000b10]:sw tp, 636(ra)
Current Store : [0x80000b10] : sw tp, 636(ra) -- Store: [0x80003890]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4a38a4 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000b28]:csrrs tp, fcsr, zero
	-[0x80000b2c]:fsw ft11, 640(ra)
	-[0x80000b30]:sw tp, 644(ra)
Current Store : [0x80000b30] : sw tp, 644(ra) -- Store: [0x80003898]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4a38a4 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000b48]:csrrs tp, fcsr, zero
	-[0x80000b4c]:fsw ft11, 648(ra)
	-[0x80000b50]:sw tp, 652(ra)
Current Store : [0x80000b50] : sw tp, 652(ra) -- Store: [0x800038a0]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4a38a4 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000b68]:csrrs tp, fcsr, zero
	-[0x80000b6c]:fsw ft11, 656(ra)
	-[0x80000b70]:sw tp, 660(ra)
Current Store : [0x80000b70] : sw tp, 660(ra) -- Store: [0x800038a8]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4a38a4 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000b84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000b88]:csrrs tp, fcsr, zero
	-[0x80000b8c]:fsw ft11, 664(ra)
	-[0x80000b90]:sw tp, 668(ra)
Current Store : [0x80000b90] : sw tp, 668(ra) -- Store: [0x800038b0]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x17aa7b and fs2 == 0 and fe2 == 0x7f and fm2 == 0x4a38a4 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ba4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ba8]:csrrs tp, fcsr, zero
	-[0x80000bac]:fsw ft11, 672(ra)
	-[0x80000bb0]:sw tp, 676(ra)
Current Store : [0x80000bb0] : sw tp, 676(ra) -- Store: [0x800038b8]:0x00000082




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x63a55f and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000bc8]:csrrs tp, fcsr, zero
	-[0x80000bcc]:fsw ft11, 680(ra)
	-[0x80000bd0]:sw tp, 684(ra)
Current Store : [0x80000bd0] : sw tp, 684(ra) -- Store: [0x800038c0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x63a55f and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000be4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000be8]:csrrs tp, fcsr, zero
	-[0x80000bec]:fsw ft11, 688(ra)
	-[0x80000bf0]:sw tp, 692(ra)
Current Store : [0x80000bf0] : sw tp, 692(ra) -- Store: [0x800038c8]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x63a55f and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000c08]:csrrs tp, fcsr, zero
	-[0x80000c0c]:fsw ft11, 696(ra)
	-[0x80000c10]:sw tp, 700(ra)
Current Store : [0x80000c10] : sw tp, 700(ra) -- Store: [0x800038d0]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x63a55f and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000c28]:csrrs tp, fcsr, zero
	-[0x80000c2c]:fsw ft11, 704(ra)
	-[0x80000c30]:sw tp, 708(ra)
Current Store : [0x80000c30] : sw tp, 708(ra) -- Store: [0x800038d8]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2abc06 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x63a55f and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000c48]:csrrs tp, fcsr, zero
	-[0x80000c4c]:fsw ft11, 712(ra)
	-[0x80000c50]:sw tp, 716(ra)
Current Store : [0x80000c50] : sw tp, 716(ra) -- Store: [0x800038e0]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x60fd67 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000c68]:csrrs tp, fcsr, zero
	-[0x80000c6c]:fsw ft11, 720(ra)
	-[0x80000c70]:sw tp, 724(ra)
Current Store : [0x80000c70] : sw tp, 724(ra) -- Store: [0x800038e8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x60fd67 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000c84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000c88]:csrrs tp, fcsr, zero
	-[0x80000c8c]:fsw ft11, 728(ra)
	-[0x80000c90]:sw tp, 732(ra)
Current Store : [0x80000c90] : sw tp, 732(ra) -- Store: [0x800038f0]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x60fd67 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ca8]:csrrs tp, fcsr, zero
	-[0x80000cac]:fsw ft11, 736(ra)
	-[0x80000cb0]:sw tp, 740(ra)
Current Store : [0x80000cb0] : sw tp, 740(ra) -- Store: [0x800038f8]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x60fd67 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000cc4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000cc8]:csrrs tp, fcsr, zero
	-[0x80000ccc]:fsw ft11, 744(ra)
	-[0x80000cd0]:sw tp, 748(ra)
Current Store : [0x80000cd0] : sw tp, 748(ra) -- Store: [0x80003900]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x28be0d and fs2 == 0 and fe2 == 0x7f and fm2 == 0x60fd67 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ce4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ce8]:csrrs tp, fcsr, zero
	-[0x80000cec]:fsw ft11, 752(ra)
	-[0x80000cf0]:sw tp, 756(ra)
Current Store : [0x80000cf0] : sw tp, 756(ra) -- Store: [0x80003908]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x0b3b6c and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000d08]:csrrs tp, fcsr, zero
	-[0x80000d0c]:fsw ft11, 760(ra)
	-[0x80000d10]:sw tp, 764(ra)
Current Store : [0x80000d10] : sw tp, 764(ra) -- Store: [0x80003910]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x0b3b6c and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000d28]:csrrs tp, fcsr, zero
	-[0x80000d2c]:fsw ft11, 768(ra)
	-[0x80000d30]:sw tp, 772(ra)
Current Store : [0x80000d30] : sw tp, 772(ra) -- Store: [0x80003918]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x0b3b6c and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000d48]:csrrs tp, fcsr, zero
	-[0x80000d4c]:fsw ft11, 776(ra)
	-[0x80000d50]:sw tp, 780(ra)
Current Store : [0x80000d50] : sw tp, 780(ra) -- Store: [0x80003920]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x0b3b6c and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000d68]:csrrs tp, fcsr, zero
	-[0x80000d6c]:fsw ft11, 784(ra)
	-[0x80000d70]:sw tp, 788(ra)
Current Store : [0x80000d70] : sw tp, 788(ra) -- Store: [0x80003928]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x50d921 and fs2 == 1 and fe2 == 0x7d and fm2 == 0x0b3b6c and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000d84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000d88]:csrrs tp, fcsr, zero
	-[0x80000d8c]:fsw ft11, 792(ra)
	-[0x80000d90]:sw tp, 796(ra)
Current Store : [0x80000d90] : sw tp, 796(ra) -- Store: [0x80003930]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7729b4 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000da4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000da8]:csrrs tp, fcsr, zero
	-[0x80000dac]:fsw ft11, 800(ra)
	-[0x80000db0]:sw tp, 804(ra)
Current Store : [0x80000db0] : sw tp, 804(ra) -- Store: [0x80003938]:0x00000002




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7729b4 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000dc4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000dc8]:csrrs tp, fcsr, zero
	-[0x80000dcc]:fsw ft11, 808(ra)
	-[0x80000dd0]:sw tp, 812(ra)
Current Store : [0x80000dd0] : sw tp, 812(ra) -- Store: [0x80003940]:0x00000022




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7729b4 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000de4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000de8]:csrrs tp, fcsr, zero
	-[0x80000dec]:fsw ft11, 816(ra)
	-[0x80000df0]:sw tp, 820(ra)
Current Store : [0x80000df0] : sw tp, 820(ra) -- Store: [0x80003948]:0x00000042




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7729b4 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000e08]:csrrs tp, fcsr, zero
	-[0x80000e0c]:fsw ft11, 824(ra)
	-[0x80000e10]:sw tp, 828(ra)
Current Store : [0x80000e10] : sw tp, 828(ra) -- Store: [0x80003950]:0x00000062




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x395f47 and fs2 == 0 and fe2 == 0x7e and fm2 == 0x7729b4 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000e28]:csrrs tp, fcsr, zero
	-[0x80000e2c]:fsw ft11, 832(ra)
	-[0x80000e30]:sw tp, 836(ra)
Current Store : [0x80000e30] : sw tp, 836(ra) -- Store: [0x80003958]:0x00000082




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x206c06 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000e48]:csrrs tp, fcsr, zero
	-[0x80000e4c]:fsw ft11, 840(ra)
	-[0x80000e50]:sw tp, 844(ra)
Current Store : [0x80000e50] : sw tp, 844(ra) -- Store: [0x80003960]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x206c06 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000e68]:csrrs tp, fcsr, zero
	-[0x80000e6c]:fsw ft11, 848(ra)
	-[0x80000e70]:sw tp, 852(ra)
Current Store : [0x80000e70] : sw tp, 852(ra) -- Store: [0x80003968]:0x00000023




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x206c06 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000e84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000e88]:csrrs tp, fcsr, zero
	-[0x80000e8c]:fsw ft11, 856(ra)
	-[0x80000e90]:sw tp, 860(ra)
Current Store : [0x80000e90] : sw tp, 860(ra) -- Store: [0x80003970]:0x00000043




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x206c06 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ea4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ea8]:csrrs tp, fcsr, zero
	-[0x80000eac]:fsw ft11, 864(ra)
	-[0x80000eb0]:sw tp, 868(ra)
Current Store : [0x80000eb0] : sw tp, 868(ra) -- Store: [0x80003978]:0x00000063




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x70a207 and fs2 == 1 and fe2 == 0x7e and fm2 == 0x206c06 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ec4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ec8]:csrrs tp, fcsr, zero
	-[0x80000ecc]:fsw ft11, 872(ra)
	-[0x80000ed0]:sw tp, 876(ra)
Current Store : [0x80000ed0] : sw tp, 876(ra) -- Store: [0x80003980]:0x00000083




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x01daad and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000ee4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000ee8]:csrrs tp, fcsr, zero
	-[0x80000eec]:fsw ft11, 880(ra)
	-[0x80000ef0]:sw tp, 884(ra)
Current Store : [0x80000ef0] : sw tp, 884(ra) -- Store: [0x80003988]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x01daad and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f04]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000f08]:csrrs tp, fcsr, zero
	-[0x80000f0c]:fsw ft11, 888(ra)
	-[0x80000f10]:sw tp, 892(ra)
Current Store : [0x80000f10] : sw tp, 892(ra) -- Store: [0x80003990]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x01daad and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f24]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000f28]:csrrs tp, fcsr, zero
	-[0x80000f2c]:fsw ft11, 896(ra)
	-[0x80000f30]:sw tp, 900(ra)
Current Store : [0x80000f30] : sw tp, 900(ra) -- Store: [0x80003998]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x01daad and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f44]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000f48]:csrrs tp, fcsr, zero
	-[0x80000f4c]:fsw ft11, 904(ra)
	-[0x80000f50]:sw tp, 908(ra)
Current Store : [0x80000f50] : sw tp, 908(ra) -- Store: [0x800039a0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x42c803 and fs2 == 0 and fe2 == 0x7d and fm2 == 0x01daad and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f64]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000f68]:csrrs tp, fcsr, zero
	-[0x80000f6c]:fsw ft11, 912(ra)
	-[0x80000f70]:sw tp, 916(ra)
Current Store : [0x80000f70] : sw tp, 916(ra) -- Store: [0x800039a8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x33aa8a and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000f84]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000f88]:csrrs tp, fcsr, zero
	-[0x80000f8c]:fsw ft11, 920(ra)
	-[0x80000f90]:sw tp, 924(ra)
Current Store : [0x80000f90] : sw tp, 924(ra) -- Store: [0x800039b0]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x33aa8a and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fa4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000fa8]:csrrs tp, fcsr, zero
	-[0x80000fac]:fsw ft11, 928(ra)
	-[0x80000fb0]:sw tp, 932(ra)
Current Store : [0x80000fb0] : sw tp, 932(ra) -- Store: [0x800039b8]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x33aa8a and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fc4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000fc8]:csrrs tp, fcsr, zero
	-[0x80000fcc]:fsw ft11, 936(ra)
	-[0x80000fd0]:sw tp, 940(ra)
Current Store : [0x80000fd0] : sw tp, 940(ra) -- Store: [0x800039c0]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x33aa8a and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000fe4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80000fe8]:csrrs tp, fcsr, zero
	-[0x80000fec]:fsw ft11, 944(ra)
	-[0x80000ff0]:sw tp, 948(ra)
Current Store : [0x80000ff0] : sw tp, 948(ra) -- Store: [0x800039c8]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x06bfe7 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x33aa8a and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001004]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001008]:csrrs tp, fcsr, zero
	-[0x8000100c]:fsw ft11, 952(ra)
	-[0x80001010]:sw tp, 956(ra)
Current Store : [0x80001010] : sw tp, 956(ra) -- Store: [0x800039d0]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x159186 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001024]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001028]:csrrs tp, fcsr, zero
	-[0x8000102c]:fsw ft11, 960(ra)
	-[0x80001030]:sw tp, 964(ra)
Current Store : [0x80001030] : sw tp, 964(ra) -- Store: [0x800039d8]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x159186 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001044]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001048]:csrrs tp, fcsr, zero
	-[0x8000104c]:fsw ft11, 968(ra)
	-[0x80001050]:sw tp, 972(ra)
Current Store : [0x80001050] : sw tp, 972(ra) -- Store: [0x800039e0]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x159186 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001064]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001068]:csrrs tp, fcsr, zero
	-[0x8000106c]:fsw ft11, 976(ra)
	-[0x80001070]:sw tp, 980(ra)
Current Store : [0x80001070] : sw tp, 980(ra) -- Store: [0x800039e8]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x159186 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001084]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001088]:csrrs tp, fcsr, zero
	-[0x8000108c]:fsw ft11, 984(ra)
	-[0x80001090]:sw tp, 988(ra)
Current Store : [0x80001090] : sw tp, 988(ra) -- Store: [0x800039f0]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x605a49 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x159186 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010a4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800010a8]:csrrs tp, fcsr, zero
	-[0x800010ac]:fsw ft11, 992(ra)
	-[0x800010b0]:sw tp, 996(ra)
Current Store : [0x800010b0] : sw tp, 996(ra) -- Store: [0x800039f8]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x2e9662 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010c4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800010c8]:csrrs tp, fcsr, zero
	-[0x800010cc]:fsw ft11, 1000(ra)
	-[0x800010d0]:sw tp, 1004(ra)
Current Store : [0x800010d0] : sw tp, 1004(ra) -- Store: [0x80003a00]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x2e9662 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800010e4]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800010e8]:csrrs tp, fcsr, zero
	-[0x800010ec]:fsw ft11, 1008(ra)
	-[0x800010f0]:sw tp, 1012(ra)
Current Store : [0x800010f0] : sw tp, 1012(ra) -- Store: [0x80003a08]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x2e9662 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80001104]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001108]:csrrs tp, fcsr, zero
	-[0x8000110c]:fsw ft11, 1016(ra)
	-[0x80001110]:sw tp, 1020(ra)
Current Store : [0x80001110] : sw tp, 1020(ra) -- Store: [0x80003a10]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x2e9662 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000112c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001130]:csrrs tp, fcsr, zero
	-[0x80001134]:fsw ft11, 0(ra)
	-[0x80001138]:sw tp, 4(ra)
Current Store : [0x80001138] : sw tp, 4(ra) -- Store: [0x80003a18]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x02f0c8 and fs2 == 1 and fe2 == 0x7c and fm2 == 0x2e9662 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000114c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001150]:csrrs tp, fcsr, zero
	-[0x80001154]:fsw ft11, 8(ra)
	-[0x80001158]:sw tp, 12(ra)
Current Store : [0x80001158] : sw tp, 12(ra) -- Store: [0x80003a20]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 0 and fe2 == 0x7b and fm2 == 0x5b3d11 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000116c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001170]:csrrs tp, fcsr, zero
	-[0x80001174]:fsw ft11, 16(ra)
	-[0x80001178]:sw tp, 20(ra)
Current Store : [0x80001178] : sw tp, 20(ra) -- Store: [0x80003a28]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 0 and fe2 == 0x7b and fm2 == 0x5b3d11 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000118c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001190]:csrrs tp, fcsr, zero
	-[0x80001194]:fsw ft11, 24(ra)
	-[0x80001198]:sw tp, 28(ra)
Current Store : [0x80001198] : sw tp, 28(ra) -- Store: [0x80003a30]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 0 and fe2 == 0x7b and fm2 == 0x5b3d11 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ac]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800011b0]:csrrs tp, fcsr, zero
	-[0x800011b4]:fsw ft11, 32(ra)
	-[0x800011b8]:sw tp, 36(ra)
Current Store : [0x800011b8] : sw tp, 36(ra) -- Store: [0x80003a38]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 0 and fe2 == 0x7b and fm2 == 0x5b3d11 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011cc]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800011d0]:csrrs tp, fcsr, zero
	-[0x800011d4]:fsw ft11, 40(ra)
	-[0x800011d8]:sw tp, 44(ra)
Current Store : [0x800011d8] : sw tp, 44(ra) -- Store: [0x80003a40]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x246dcc and fs2 == 0 and fe2 == 0x7b and fm2 == 0x5b3d11 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800011ec]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800011f0]:csrrs tp, fcsr, zero
	-[0x800011f4]:fsw ft11, 48(ra)
	-[0x800011f8]:sw tp, 52(ra)
Current Store : [0x800011f8] : sw tp, 52(ra) -- Store: [0x80003a48]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0x7b and fm2 == 0x2bd895 and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000120c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001210]:csrrs tp, fcsr, zero
	-[0x80001214]:fsw ft11, 56(ra)
	-[0x80001218]:sw tp, 60(ra)
Current Store : [0x80001218] : sw tp, 60(ra) -- Store: [0x80003a50]:0x00000007




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0x7b and fm2 == 0x2bd895 and fcsr == 0x22 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000122c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001230]:csrrs tp, fcsr, zero
	-[0x80001234]:fsw ft11, 64(ra)
	-[0x80001238]:sw tp, 68(ra)
Current Store : [0x80001238] : sw tp, 68(ra) -- Store: [0x80003a58]:0x00000027




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0x7b and fm2 == 0x2bd895 and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000124c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001250]:csrrs tp, fcsr, zero
	-[0x80001254]:fsw ft11, 72(ra)
	-[0x80001258]:sw tp, 76(ra)
Current Store : [0x80001258] : sw tp, 76(ra) -- Store: [0x80003a60]:0x00000047




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0x7b and fm2 == 0x2bd895 and fcsr == 0x62 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000126c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001270]:csrrs tp, fcsr, zero
	-[0x80001274]:fsw ft11, 80(ra)
	-[0x80001278]:sw tp, 84(ra)
Current Store : [0x80001278] : sw tp, 84(ra) -- Store: [0x80003a68]:0x00000067




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x00e26f and fs2 == 1 and fe2 == 0x7b and fm2 == 0x2bd895 and fcsr == 0x82 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000128c]:fdiv.s ft11, ft10, ft9, dyn
	-[0x80001290]:csrrs tp, fcsr, zero
	-[0x80001294]:fsw ft11, 88(ra)
	-[0x80001298]:sw tp, 92(ra)
Current Store : [0x80001298] : sw tp, 92(ra) -- Store: [0x80003a70]:0x00000087




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0x7c and fm2 == 0x73acff and fcsr == 0x2 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012ac]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800012b0]:csrrs tp, fcsr, zero
	-[0x800012b4]:fsw ft11, 96(ra)
	-[0x800012b8]:sw tp, 100(ra)
Current Store : [0x800012b8] : sw tp, 100(ra) -- Store: [0x80003a78]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x36c1bf and fs2 == 0 and fe2 == 0x7c and fm2 == 0x73acff and fcsr == 0x42 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800012cc]:fdiv.s ft11, ft10, ft9, dyn
	-[0x800012d0]:csrrs tp, fcsr, zero
	-[0x800012d4]:fsw ft11, 104(ra)
	-[0x800012d8]:sw tp, 108(ra)
Current Store : [0x800012d8] : sw tp, 108(ra) -- Store: [0x80003a80]:0x00000043





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
