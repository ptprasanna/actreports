
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000320')]      |
| SIG_REGION                | [('0x80002210', '0x80002300', '60 words')]      |
| COV_LABELS                | c.fsd      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fld/c.fsd-01.S/ref.S    |
| Total Number of coverpoints| 32     |
| Total Coverpoints Hit     | 32      |
| Total Signature Updates   | 14      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 14     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000120]:c.fsd
[0x80000122]:c.nop
[0x80000124]:c.nop
[0x80000126]:csrrs
[0x8000012a]:sw
[0x8000012e]:auipc
[0x80000132]:addi
[0x80000136]:fld
[0x8000013a]:addi
[0x8000013e]:addi
[0x80000142]:sub
[0x80000146]:c.fsd

[0x80000146]:c.fsd
[0x80000148]:c.nop
[0x8000014a]:c.nop
[0x8000014c]:csrrs
[0x80000150]:sw
[0x80000154]:auipc
[0x80000158]:addi
[0x8000015c]:fld
[0x80000160]:addi
[0x80000164]:addi
[0x80000168]:sub
[0x8000016c]:c.fsd

[0x8000016c]:c.fsd
[0x8000016e]:c.nop
[0x80000170]:c.nop
[0x80000172]:csrrs
[0x80000176]:sw
[0x8000017a]:auipc
[0x8000017e]:addi
[0x80000182]:fld
[0x80000186]:addi
[0x8000018a]:addi
[0x8000018e]:sub
[0x80000192]:c.fsd

[0x80000192]:c.fsd
[0x80000194]:c.nop
[0x80000196]:c.nop
[0x80000198]:csrrs
[0x8000019c]:sw
[0x800001a0]:auipc
[0x800001a4]:addi
[0x800001a8]:fld
[0x800001ac]:addi
[0x800001b0]:addi
[0x800001b4]:sub
[0x800001b8]:c.fsd

[0x800001b8]:c.fsd
[0x800001ba]:c.nop
[0x800001bc]:c.nop
[0x800001be]:csrrs
[0x800001c2]:sw
[0x800001c6]:auipc
[0x800001ca]:addi
[0x800001ce]:fld
[0x800001d2]:addi
[0x800001d6]:addi
[0x800001da]:sub
[0x800001de]:c.fsd

[0x800001de]:c.fsd
[0x800001e0]:c.nop
[0x800001e2]:c.nop
[0x800001e4]:csrrs
[0x800001e8]:sw
[0x800001ec]:auipc
[0x800001f0]:addi
[0x800001f4]:fld
[0x800001f8]:addi
[0x800001fc]:addi
[0x80000200]:sub
[0x80000204]:c.fsd

[0x80000204]:c.fsd
[0x80000206]:c.nop
[0x80000208]:c.nop
[0x8000020a]:csrrs
[0x8000020e]:sw
[0x80000212]:auipc
[0x80000216]:addi
[0x8000021a]:fld
[0x8000021e]:addi
[0x80000222]:addi
[0x80000226]:sub
[0x8000022a]:c.fsd

[0x8000022a]:c.fsd
[0x8000022c]:c.nop
[0x8000022e]:c.nop
[0x80000230]:csrrs
[0x80000234]:sw
[0x80000238]:auipc
[0x8000023c]:addi
[0x80000240]:fld
[0x80000244]:addi
[0x80000248]:addi
[0x8000024c]:sub
[0x80000250]:c.fsd

[0x80000250]:c.fsd
[0x80000252]:c.nop
[0x80000254]:c.nop
[0x80000256]:csrrs
[0x8000025a]:sw
[0x8000025e]:auipc
[0x80000262]:addi
[0x80000266]:fld
[0x8000026a]:addi
[0x8000026e]:addi
[0x80000272]:sub
[0x80000276]:c.fsd

[0x80000276]:c.fsd
[0x80000278]:c.nop
[0x8000027a]:c.nop
[0x8000027c]:csrrs
[0x80000280]:sw
[0x80000284]:auipc
[0x80000288]:addi
[0x8000028c]:fld
[0x80000290]:addi
[0x80000294]:addi
[0x80000298]:sub
[0x8000029c]:c.fsd

[0x8000029c]:c.fsd
[0x8000029e]:c.nop
[0x800002a0]:c.nop
[0x800002a2]:csrrs
[0x800002a6]:sw
[0x800002aa]:auipc
[0x800002ae]:addi
[0x800002b2]:fld
[0x800002b6]:addi
[0x800002ba]:addi
[0x800002be]:sub
[0x800002c2]:c.fsd

[0x800002c2]:c.fsd
[0x800002c4]:c.nop
[0x800002c6]:c.nop
[0x800002c8]:csrrs
[0x800002cc]:sw
[0x800002d0]:auipc
[0x800002d4]:addi
[0x800002d8]:fld
[0x800002dc]:addi
[0x800002e0]:addi
[0x800002e4]:sub
[0x800002e8]:c.fsd

[0x800002e8]:c.fsd
[0x800002ea]:c.nop
[0x800002ec]:c.nop
[0x800002ee]:csrrs
[0x800002f2]:sw
[0x800002f6]:auipc
[0x800002fa]:addi
[0x800002fe]:fld
[0x80000302]:addi
[0x80000306]:addi
[0x8000030a]:sub
[0x8000030e]:c.fsd

[0x8000030e]:c.fsd
[0x80000310]:c.nop
[0x80000312]:c.nop
[0x80000314]:csrrs
[0x80000318]:sw
[0x8000031c]:addi



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
