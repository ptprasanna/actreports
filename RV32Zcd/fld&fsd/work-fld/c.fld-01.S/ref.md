
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800002f0')]      |
| SIG_REGION                | [('0x80002210', '0x80002300', '60 words')]      |
| COV_LABELS                | c.fld      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fld/c.fld-01.S/ref.S    |
| Total Number of coverpoints| 32     |
| Total Coverpoints Hit     | 32      |
| Total Signature Updates   | 28      |
| STAT1                     | 14      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 14     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fld', 'rs1 : x15', 'rd : f15', 'rs1 != rd', 'imm_val == 0 and fcsr == 0']
Last Code Sequence : 
	-[0x80000122]:c.fld
	-[0x80000124]:c.nop
	-[0x80000126]:c.nop
	-[0x80000128]:csrrs
	-[0x8000012c]:fsd
Current Store : [0x80000130] : None -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : f14', 'imm_val > 0  and fcsr == 0']
Last Code Sequence : 
	-[0x80000144]:c.fld
	-[0x80000146]:c.nop
	-[0x80000148]:c.nop
	-[0x8000014a]:csrrs
	-[0x8000014e]:fsd
Current Store : [0x80000152] : None -- Store: [0x80002230]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : f13', 'imm_val == 168']
Last Code Sequence : 
	-[0x80000166]:c.fld
	-[0x80000168]:c.nop
	-[0x8000016a]:c.nop
	-[0x8000016c]:csrrs
	-[0x80000170]:fsd
Current Store : [0x80000174] : None -- Store: [0x80002240]:0x0000009f




Last Coverpoint : ['rs1 : x12', 'rd : f12', 'imm_val == 80']
Last Code Sequence : 
	-[0x80000188]:c.fld
	-[0x8000018a]:c.nop
	-[0x8000018c]:c.nop
	-[0x8000018e]:csrrs
	-[0x80000192]:fsd
Current Store : [0x80000196] : None -- Store: [0x80002250]:0x0000009f




Last Coverpoint : ['rs1 : x11', 'rd : f11', 'imm_val == 8']
Last Code Sequence : 
	-[0x800001aa]:c.fld
	-[0x800001ac]:c.nop
	-[0x800001ae]:c.nop
	-[0x800001b0]:csrrs
	-[0x800001b4]:fsd
Current Store : [0x800001b8] : None -- Store: [0x80002260]:0x0000009f




Last Coverpoint : ['rs1 : x10', 'rd : f10', 'imm_val == 16']
Last Code Sequence : 
	-[0x800001cc]:c.fld
	-[0x800001ce]:c.nop
	-[0x800001d0]:c.nop
	-[0x800001d2]:csrrs
	-[0x800001d6]:fsd
Current Store : [0x800001da] : None -- Store: [0x80002270]:0x0000009f




Last Coverpoint : ['rs1 : x9', 'rd : f9', 'imm_val == 240']
Last Code Sequence : 
	-[0x800001ee]:c.fld
	-[0x800001f0]:c.nop
	-[0x800001f2]:c.nop
	-[0x800001f4]:csrrs
	-[0x800001f8]:fsd
Current Store : [0x800001fc] : None -- Store: [0x80002280]:0x0000009f




Last Coverpoint : ['rs1 : x8', 'rd : f8', 'imm_val == 232']
Last Code Sequence : 
	-[0x80000210]:c.fld
	-[0x80000212]:c.nop
	-[0x80000214]:c.nop
	-[0x80000216]:csrrs
	-[0x8000021a]:fsd
Current Store : [0x8000021e] : None -- Store: [0x80002290]:0x0000009f




Last Coverpoint : ['imm_val == 216']
Last Code Sequence : 
	-[0x80000232]:c.fld
	-[0x80000234]:c.nop
	-[0x80000236]:c.nop
	-[0x80000238]:csrrs
	-[0x8000023c]:fsd
Current Store : [0x80000240] : None -- Store: [0x800022a0]:0x0000009f




Last Coverpoint : ['imm_val == 184']
Last Code Sequence : 
	-[0x80000254]:c.fld
	-[0x80000256]:c.nop
	-[0x80000258]:c.nop
	-[0x8000025a]:csrrs
	-[0x8000025e]:fsd
Current Store : [0x80000262] : None -- Store: [0x800022b0]:0x0000009f




Last Coverpoint : ['imm_val == 120']
Last Code Sequence : 
	-[0x80000276]:c.fld
	-[0x80000278]:c.nop
	-[0x8000027a]:c.nop
	-[0x8000027c]:csrrs
	-[0x80000280]:fsd
Current Store : [0x80000284] : None -- Store: [0x800022c0]:0x0000009f




Last Coverpoint : ['imm_val == 32']
Last Code Sequence : 
	-[0x80000298]:c.fld
	-[0x8000029a]:c.nop
	-[0x8000029c]:c.nop
	-[0x8000029e]:csrrs
	-[0x800002a2]:fsd
Current Store : [0x800002a6] : None -- Store: [0x800022d0]:0x0000009f




Last Coverpoint : ['imm_val == 64']
Last Code Sequence : 
	-[0x800002ba]:c.fld
	-[0x800002bc]:c.nop
	-[0x800002be]:c.nop
	-[0x800002c0]:csrrs
	-[0x800002c4]:fsd
Current Store : [0x800002c8] : None -- Store: [0x800022e0]:0x0000009f




Last Coverpoint : ['imm_val == 128']
Last Code Sequence : 
	-[0x800002dc]:c.fld
	-[0x800002de]:c.nop
	-[0x800002e0]:c.nop
	-[0x800002e2]:csrrs
	-[0x800002e6]:fsd
Current Store : [0x800002ea] : None -- Store: [0x800022f0]:0x0000009f





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                              coverpoints                                              |                                                      code                                                       |
|---:|-------------------------------|-------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x80002000<br> |- mnemonic : c.fld<br> - rs1 : x15<br> - rd : f15<br> - rs1 != rd<br> - imm_val == 0 and fcsr == 0<br> |[0x80000122]:c.fld<br> [0x80000124]:c.nop<br> [0x80000126]:c.nop<br> [0x80000128]:csrrs<br> [0x8000012c]:fsd<br> |
|   2|[0x80002228]<br>0x80001f08<br> |- rs1 : x14<br> - rd : f14<br> - imm_val > 0  and fcsr == 0<br>                                        |[0x80000144]:c.fld<br> [0x80000146]:c.nop<br> [0x80000148]:c.nop<br> [0x8000014a]:csrrs<br> [0x8000014e]:fsd<br> |
|   3|[0x80002238]<br>0x80001f58<br> |- rs1 : x13<br> - rd : f13<br> - imm_val == 168<br>                                                    |[0x80000166]:c.fld<br> [0x80000168]:c.nop<br> [0x8000016a]:c.nop<br> [0x8000016c]:csrrs<br> [0x80000170]:fsd<br> |
|   4|[0x80002248]<br>0x80001fb0<br> |- rs1 : x12<br> - rd : f12<br> - imm_val == 80<br>                                                     |[0x80000188]:c.fld<br> [0x8000018a]:c.nop<br> [0x8000018c]:c.nop<br> [0x8000018e]:csrrs<br> [0x80000192]:fsd<br> |
|   5|[0x80002258]<br>0x80001ff8<br> |- rs1 : x11<br> - rd : f11<br> - imm_val == 8<br>                                                      |[0x800001aa]:c.fld<br> [0x800001ac]:c.nop<br> [0x800001ae]:c.nop<br> [0x800001b0]:csrrs<br> [0x800001b4]:fsd<br> |
|   6|[0x80002268]<br>0x80001ff0<br> |- rs1 : x10<br> - rd : f10<br> - imm_val == 16<br>                                                     |[0x800001cc]:c.fld<br> [0x800001ce]:c.nop<br> [0x800001d0]:c.nop<br> [0x800001d2]:csrrs<br> [0x800001d6]:fsd<br> |
|   7|[0x80002278]<br>0x80001f10<br> |- rs1 : x9<br> - rd : f9<br> - imm_val == 240<br>                                                      |[0x800001ee]:c.fld<br> [0x800001f0]:c.nop<br> [0x800001f2]:c.nop<br> [0x800001f4]:csrrs<br> [0x800001f8]:fsd<br> |
|   8|[0x80002288]<br>0x80001f18<br> |- rs1 : x8<br> - rd : f8<br> - imm_val == 232<br>                                                      |[0x80000210]:c.fld<br> [0x80000212]:c.nop<br> [0x80000214]:c.nop<br> [0x80000216]:csrrs<br> [0x8000021a]:fsd<br> |
|   9|[0x80002298]<br>0x80001f28<br> |- imm_val == 216<br>                                                                                   |[0x80000232]:c.fld<br> [0x80000234]:c.nop<br> [0x80000236]:c.nop<br> [0x80000238]:csrrs<br> [0x8000023c]:fsd<br> |
|  10|[0x800022a8]<br>0x80001f48<br> |- imm_val == 184<br>                                                                                   |[0x80000254]:c.fld<br> [0x80000256]:c.nop<br> [0x80000258]:c.nop<br> [0x8000025a]:csrrs<br> [0x8000025e]:fsd<br> |
|  11|[0x800022b8]<br>0x80001f88<br> |- imm_val == 120<br>                                                                                   |[0x80000276]:c.fld<br> [0x80000278]:c.nop<br> [0x8000027a]:c.nop<br> [0x8000027c]:csrrs<br> [0x80000280]:fsd<br> |
|  12|[0x800022c8]<br>0x80001fe0<br> |- imm_val == 32<br>                                                                                    |[0x80000298]:c.fld<br> [0x8000029a]:c.nop<br> [0x8000029c]:c.nop<br> [0x8000029e]:csrrs<br> [0x800002a2]:fsd<br> |
|  13|[0x800022d8]<br>0x80001fc0<br> |- imm_val == 64<br>                                                                                    |[0x800002ba]:c.fld<br> [0x800002bc]:c.nop<br> [0x800002be]:c.nop<br> [0x800002c0]:csrrs<br> [0x800002c4]:fsd<br> |
|  14|[0x800022e8]<br>0x80001f80<br> |- imm_val == 128<br>                                                                                   |[0x800002dc]:c.fld<br> [0x800002de]:c.nop<br> [0x800002e0]:c.nop<br> [0x800002e2]:csrrs<br> [0x800002e6]:fsd<br> |
