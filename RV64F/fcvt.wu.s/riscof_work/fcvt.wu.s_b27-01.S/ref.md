
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800006e0')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | fcvt.wu.s_b27      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fcvt.wu.s/riscof_work/fcvt.wu.s_b27-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.s', 'rd : x16', 'rs1 : f26', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.wu.s a6, fs10, dyn
	-[0x800003b8]:csrrs s5, fflags, zero
	-[0x800003bc]:sd a6, 0(s3)
Current Store : [0x800003c0] : sd s5, 8(s3) -- Store: [0x80002218]:0x0000000000000010




Last Coverpoint : ['rd : x2', 'rs1 : f7', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d8]:fcvt.wu.s sp, ft7, dyn
	-[0x800003dc]:csrrs a7, fflags, zero
	-[0x800003e0]:sd sp, 0(a5)
Current Store : [0x800003e4] : sd a7, 8(a5) -- Store: [0x80002228]:0x0000000000000010




Last Coverpoint : ['rd : x29', 'rs1 : f0', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.wu.s t4, ft0, dyn
	-[0x800003f4]:csrrs a7, fflags, zero
	-[0x800003f8]:sd t4, 16(a5)
Current Store : [0x800003fc] : sd a7, 24(a5) -- Store: [0x80002238]:0x0000000000000010




Last Coverpoint : ['rd : x27', 'rs1 : f27', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000408]:fcvt.wu.s s11, fs11, dyn
	-[0x8000040c]:csrrs a7, fflags, zero
	-[0x80000410]:sd s11, 32(a5)
Current Store : [0x80000414] : sd a7, 40(a5) -- Store: [0x80002248]:0x0000000000000010




Last Coverpoint : ['rd : x9', 'rs1 : f14', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000420]:fcvt.wu.s s1, fa4, dyn
	-[0x80000424]:csrrs a7, fflags, zero
	-[0x80000428]:sd s1, 48(a5)
Current Store : [0x8000042c] : sd a7, 56(a5) -- Store: [0x80002258]:0x0000000000000010




Last Coverpoint : ['rd : x30', 'rs1 : f5', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000438]:fcvt.wu.s t5, ft5, dyn
	-[0x8000043c]:csrrs a7, fflags, zero
	-[0x80000440]:sd t5, 64(a5)
Current Store : [0x80000444] : sd a7, 72(a5) -- Store: [0x80002268]:0x0000000000000010




Last Coverpoint : ['rd : x4', 'rs1 : f19', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000450]:fcvt.wu.s tp, fs3, dyn
	-[0x80000454]:csrrs a7, fflags, zero
	-[0x80000458]:sd tp, 80(a5)
Current Store : [0x8000045c] : sd a7, 88(a5) -- Store: [0x80002278]:0x0000000000000010




Last Coverpoint : ['rd : x6', 'rs1 : f22', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000468]:fcvt.wu.s t1, fs6, dyn
	-[0x8000046c]:csrrs a7, fflags, zero
	-[0x80000470]:sd t1, 96(a5)
Current Store : [0x80000474] : sd a7, 104(a5) -- Store: [0x80002288]:0x0000000000000010




Last Coverpoint : ['rd : x18', 'rs1 : f31']
Last Code Sequence : 
	-[0x80000480]:fcvt.wu.s s2, ft11, dyn
	-[0x80000484]:csrrs a7, fflags, zero
	-[0x80000488]:sd s2, 112(a5)
Current Store : [0x8000048c] : sd a7, 120(a5) -- Store: [0x80002298]:0x0000000000000010




Last Coverpoint : ['rd : x31', 'rs1 : f23']
Last Code Sequence : 
	-[0x80000498]:fcvt.wu.s t6, fs7, dyn
	-[0x8000049c]:csrrs a7, fflags, zero
	-[0x800004a0]:sd t6, 128(a5)
Current Store : [0x800004a4] : sd a7, 136(a5) -- Store: [0x800022a8]:0x0000000000000010




Last Coverpoint : ['rd : x11', 'rs1 : f11']
Last Code Sequence : 
	-[0x800004b0]:fcvt.wu.s a1, fa1, dyn
	-[0x800004b4]:csrrs a7, fflags, zero
	-[0x800004b8]:sd a1, 144(a5)
Current Store : [0x800004bc] : sd a7, 152(a5) -- Store: [0x800022b8]:0x0000000000000010




Last Coverpoint : ['rd : x22', 'rs1 : f15']
Last Code Sequence : 
	-[0x800004c8]:fcvt.wu.s s6, fa5, dyn
	-[0x800004cc]:csrrs a7, fflags, zero
	-[0x800004d0]:sd s6, 160(a5)
Current Store : [0x800004d4] : sd a7, 168(a5) -- Store: [0x800022c8]:0x0000000000000010




Last Coverpoint : ['rd : x1', 'rs1 : f1']
Last Code Sequence : 
	-[0x800004e0]:fcvt.wu.s ra, ft1, dyn
	-[0x800004e4]:csrrs a7, fflags, zero
	-[0x800004e8]:sd ra, 176(a5)
Current Store : [0x800004ec] : sd a7, 184(a5) -- Store: [0x800022d8]:0x0000000000000010




Last Coverpoint : ['rd : x24', 'rs1 : f20']
Last Code Sequence : 
	-[0x800004f8]:fcvt.wu.s s8, fs4, dyn
	-[0x800004fc]:csrrs a7, fflags, zero
	-[0x80000500]:sd s8, 192(a5)
Current Store : [0x80000504] : sd a7, 200(a5) -- Store: [0x800022e8]:0x0000000000000010




Last Coverpoint : ['rd : x3', 'rs1 : f3']
Last Code Sequence : 
	-[0x80000510]:fcvt.wu.s gp, ft3, dyn
	-[0x80000514]:csrrs a7, fflags, zero
	-[0x80000518]:sd gp, 208(a5)
Current Store : [0x8000051c] : sd a7, 216(a5) -- Store: [0x800022f8]:0x0000000000000010




Last Coverpoint : ['rd : x13', 'rs1 : f2']
Last Code Sequence : 
	-[0x80000528]:fcvt.wu.s a3, ft2, dyn
	-[0x8000052c]:csrrs a7, fflags, zero
	-[0x80000530]:sd a3, 224(a5)
Current Store : [0x80000534] : sd a7, 232(a5) -- Store: [0x80002308]:0x0000000000000010




Last Coverpoint : ['rd : x19', 'rs1 : f12']
Last Code Sequence : 
	-[0x80000540]:fcvt.wu.s s3, fa2, dyn
	-[0x80000544]:csrrs a7, fflags, zero
	-[0x80000548]:sd s3, 240(a5)
Current Store : [0x8000054c] : sd a7, 248(a5) -- Store: [0x80002318]:0x0000000000000010




Last Coverpoint : ['rd : x8', 'rs1 : f13']
Last Code Sequence : 
	-[0x80000558]:fcvt.wu.s fp, fa3, dyn
	-[0x8000055c]:csrrs a7, fflags, zero
	-[0x80000560]:sd fp, 256(a5)
Current Store : [0x80000564] : sd a7, 264(a5) -- Store: [0x80002328]:0x0000000000000010




Last Coverpoint : ['rd : x0', 'rs1 : f16']
Last Code Sequence : 
	-[0x80000570]:fcvt.wu.s zero, fa6, dyn
	-[0x80000574]:csrrs a7, fflags, zero
	-[0x80000578]:sd zero, 272(a5)
Current Store : [0x8000057c] : sd a7, 280(a5) -- Store: [0x80002338]:0x0000000000000010




Last Coverpoint : ['rd : x12', 'rs1 : f25']
Last Code Sequence : 
	-[0x80000588]:fcvt.wu.s a2, fs9, dyn
	-[0x8000058c]:csrrs a7, fflags, zero
	-[0x80000590]:sd a2, 288(a5)
Current Store : [0x80000594] : sd a7, 296(a5) -- Store: [0x80002348]:0x0000000000000010




Last Coverpoint : ['rd : x10', 'rs1 : f28']
Last Code Sequence : 
	-[0x800005a0]:fcvt.wu.s a0, ft8, dyn
	-[0x800005a4]:csrrs a7, fflags, zero
	-[0x800005a8]:sd a0, 304(a5)
Current Store : [0x800005ac] : sd a7, 312(a5) -- Store: [0x80002358]:0x0000000000000010




Last Coverpoint : ['rd : x20', 'rs1 : f21']
Last Code Sequence : 
	-[0x800005b8]:fcvt.wu.s s4, fs5, dyn
	-[0x800005bc]:csrrs a7, fflags, zero
	-[0x800005c0]:sd s4, 320(a5)
Current Store : [0x800005c4] : sd a7, 328(a5) -- Store: [0x80002368]:0x0000000000000010




Last Coverpoint : ['rd : x14', 'rs1 : f18']
Last Code Sequence : 
	-[0x800005d0]:fcvt.wu.s a4, fs2, dyn
	-[0x800005d4]:csrrs a7, fflags, zero
	-[0x800005d8]:sd a4, 336(a5)
Current Store : [0x800005dc] : sd a7, 344(a5) -- Store: [0x80002378]:0x0000000000000010




Last Coverpoint : ['rd : x25', 'rs1 : f24']
Last Code Sequence : 
	-[0x800005e8]:fcvt.wu.s s9, fs8, dyn
	-[0x800005ec]:csrrs a7, fflags, zero
	-[0x800005f0]:sd s9, 352(a5)
Current Store : [0x800005f4] : sd a7, 360(a5) -- Store: [0x80002388]:0x0000000000000010




Last Coverpoint : ['rd : x17', 'rs1 : f10']
Last Code Sequence : 
	-[0x8000060c]:fcvt.wu.s a7, fa0, dyn
	-[0x80000610]:csrrs s5, fflags, zero
	-[0x80000614]:sd a7, 0(s3)
Current Store : [0x80000618] : sd s5, 8(s3) -- Store: [0x80002398]:0x0000000000000010




Last Coverpoint : ['rd : x5', 'rs1 : f4']
Last Code Sequence : 
	-[0x80000630]:fcvt.wu.s t0, ft4, dyn
	-[0x80000634]:csrrs a7, fflags, zero
	-[0x80000638]:sd t0, 0(a5)
Current Store : [0x8000063c] : sd a7, 8(a5) -- Store: [0x800023a8]:0x0000000000000010




Last Coverpoint : ['rd : x23', 'rs1 : f9']
Last Code Sequence : 
	-[0x80000648]:fcvt.wu.s s7, fs1, dyn
	-[0x8000064c]:csrrs a7, fflags, zero
	-[0x80000650]:sd s7, 16(a5)
Current Store : [0x80000654] : sd a7, 24(a5) -- Store: [0x800023b8]:0x0000000000000010




Last Coverpoint : ['rd : x26', 'rs1 : f8']
Last Code Sequence : 
	-[0x80000660]:fcvt.wu.s s10, fs0, dyn
	-[0x80000664]:csrrs a7, fflags, zero
	-[0x80000668]:sd s10, 32(a5)
Current Store : [0x8000066c] : sd a7, 40(a5) -- Store: [0x800023c8]:0x0000000000000010




Last Coverpoint : ['rd : x28', 'rs1 : f6']
Last Code Sequence : 
	-[0x80000678]:fcvt.wu.s t3, ft6, dyn
	-[0x8000067c]:csrrs a7, fflags, zero
	-[0x80000680]:sd t3, 48(a5)
Current Store : [0x80000684] : sd a7, 56(a5) -- Store: [0x800023d8]:0x0000000000000010




Last Coverpoint : ['rd : x21', 'rs1 : f30']
Last Code Sequence : 
	-[0x80000690]:fcvt.wu.s s5, ft10, dyn
	-[0x80000694]:csrrs a7, fflags, zero
	-[0x80000698]:sd s5, 64(a5)
Current Store : [0x8000069c] : sd a7, 72(a5) -- Store: [0x800023e8]:0x0000000000000010




Last Coverpoint : ['rd : x7', 'rs1 : f17']
Last Code Sequence : 
	-[0x800006a8]:fcvt.wu.s t2, fa7, dyn
	-[0x800006ac]:csrrs a7, fflags, zero
	-[0x800006b0]:sd t2, 80(a5)
Current Store : [0x800006b4] : sd a7, 88(a5) -- Store: [0x800023f8]:0x0000000000000010




Last Coverpoint : ['rd : x15', 'rs1 : f29']
Last Code Sequence : 
	-[0x800006cc]:fcvt.wu.s a5, ft9, dyn
	-[0x800006d0]:csrrs s5, fflags, zero
	-[0x800006d4]:sd a5, 0(s3)
Current Store : [0x800006d8] : sd s5, 8(s3) -- Store: [0x80002408]:0x0000000000000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                            coverpoints                                                            |                                                        code                                                        |
|---:|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0xFFFFFFFFFFFFFFFF|- opcode : fcvt.wu.s<br> - rd : x16<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.wu.s a6, fs10, dyn<br> [0x800003b8]:csrrs s5, fflags, zero<br> [0x800003bc]:sd a6, 0(s3)<br>      |
|   2|[0x80002220]<br>0xFFFFFFFFFFFFFFFF|- rd : x2<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat<br>                            |[0x800003d8]:fcvt.wu.s sp, ft7, dyn<br> [0x800003dc]:csrrs a7, fflags, zero<br> [0x800003e0]:sd sp, 0(a5)<br>       |
|   3|[0x80002230]<br>0xFFFFFFFFFFFFFFFF|- rd : x29<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat<br>                           |[0x800003f0]:fcvt.wu.s t4, ft0, dyn<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:sd t4, 16(a5)<br>      |
|   4|[0x80002240]<br>0xFFFFFFFFFFFFFFFF|- rd : x27<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat<br>                          |[0x80000408]:fcvt.wu.s s11, fs11, dyn<br> [0x8000040c]:csrrs a7, fflags, zero<br> [0x80000410]:sd s11, 32(a5)<br>   |
|   5|[0x80002250]<br>0xFFFFFFFFFFFFFFFF|- rd : x9<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat<br>                           |[0x80000420]:fcvt.wu.s s1, fa4, dyn<br> [0x80000424]:csrrs a7, fflags, zero<br> [0x80000428]:sd s1, 48(a5)<br>      |
|   6|[0x80002260]<br>0xFFFFFFFFFFFFFFFF|- rd : x30<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat<br>                           |[0x80000438]:fcvt.wu.s t5, ft5, dyn<br> [0x8000043c]:csrrs a7, fflags, zero<br> [0x80000440]:sd t5, 64(a5)<br>      |
|   7|[0x80002270]<br>0xFFFFFFFFFFFFFFFF|- rd : x4<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat<br>                           |[0x80000450]:fcvt.wu.s tp, fs3, dyn<br> [0x80000454]:csrrs a7, fflags, zero<br> [0x80000458]:sd tp, 80(a5)<br>      |
|   8|[0x80002280]<br>0xFFFFFFFFFFFFFFFF|- rd : x6<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat<br>                           |[0x80000468]:fcvt.wu.s t1, fs6, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:sd t1, 96(a5)<br>      |
|   9|[0x80002290]<br>0x0000000000000000|- rd : x18<br> - rs1 : f31<br>                                                                                                     |[0x80000480]:fcvt.wu.s s2, ft11, dyn<br> [0x80000484]:csrrs a7, fflags, zero<br> [0x80000488]:sd s2, 112(a5)<br>    |
|  10|[0x800022a0]<br>0x0000000000000000|- rd : x31<br> - rs1 : f23<br>                                                                                                     |[0x80000498]:fcvt.wu.s t6, fs7, dyn<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:sd t6, 128(a5)<br>     |
|  11|[0x800022b0]<br>0x0000000000000000|- rd : x11<br> - rs1 : f11<br>                                                                                                     |[0x800004b0]:fcvt.wu.s a1, fa1, dyn<br> [0x800004b4]:csrrs a7, fflags, zero<br> [0x800004b8]:sd a1, 144(a5)<br>     |
|  12|[0x800022c0]<br>0x0000000000000000|- rd : x22<br> - rs1 : f15<br>                                                                                                     |[0x800004c8]:fcvt.wu.s s6, fa5, dyn<br> [0x800004cc]:csrrs a7, fflags, zero<br> [0x800004d0]:sd s6, 160(a5)<br>     |
|  13|[0x800022d0]<br>0x0000000000000000|- rd : x1<br> - rs1 : f1<br>                                                                                                       |[0x800004e0]:fcvt.wu.s ra, ft1, dyn<br> [0x800004e4]:csrrs a7, fflags, zero<br> [0x800004e8]:sd ra, 176(a5)<br>     |
|  14|[0x800022e0]<br>0x0000000000000000|- rd : x24<br> - rs1 : f20<br>                                                                                                     |[0x800004f8]:fcvt.wu.s s8, fs4, dyn<br> [0x800004fc]:csrrs a7, fflags, zero<br> [0x80000500]:sd s8, 192(a5)<br>     |
|  15|[0x800022f0]<br>0x0000000000000000|- rd : x3<br> - rs1 : f3<br>                                                                                                       |[0x80000510]:fcvt.wu.s gp, ft3, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:sd gp, 208(a5)<br>     |
|  16|[0x80002300]<br>0x0000000000000000|- rd : x13<br> - rs1 : f2<br>                                                                                                      |[0x80000528]:fcvt.wu.s a3, ft2, dyn<br> [0x8000052c]:csrrs a7, fflags, zero<br> [0x80000530]:sd a3, 224(a5)<br>     |
|  17|[0x80002310]<br>0x0000000000000000|- rd : x19<br> - rs1 : f12<br>                                                                                                     |[0x80000540]:fcvt.wu.s s3, fa2, dyn<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:sd s3, 240(a5)<br>     |
|  18|[0x80002320]<br>0x0000000000000000|- rd : x8<br> - rs1 : f13<br>                                                                                                      |[0x80000558]:fcvt.wu.s fp, fa3, dyn<br> [0x8000055c]:csrrs a7, fflags, zero<br> [0x80000560]:sd fp, 256(a5)<br>     |
|  19|[0x80002330]<br>0x0000000000000000|- rd : x0<br> - rs1 : f16<br>                                                                                                      |[0x80000570]:fcvt.wu.s zero, fa6, dyn<br> [0x80000574]:csrrs a7, fflags, zero<br> [0x80000578]:sd zero, 272(a5)<br> |
|  20|[0x80002340]<br>0x0000000000000000|- rd : x12<br> - rs1 : f25<br>                                                                                                     |[0x80000588]:fcvt.wu.s a2, fs9, dyn<br> [0x8000058c]:csrrs a7, fflags, zero<br> [0x80000590]:sd a2, 288(a5)<br>     |
|  21|[0x80002350]<br>0x0000000000000000|- rd : x10<br> - rs1 : f28<br>                                                                                                     |[0x800005a0]:fcvt.wu.s a0, ft8, dyn<br> [0x800005a4]:csrrs a7, fflags, zero<br> [0x800005a8]:sd a0, 304(a5)<br>     |
|  22|[0x80002360]<br>0x0000000000000000|- rd : x20<br> - rs1 : f21<br>                                                                                                     |[0x800005b8]:fcvt.wu.s s4, fs5, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:sd s4, 320(a5)<br>     |
|  23|[0x80002370]<br>0x0000000000000000|- rd : x14<br> - rs1 : f18<br>                                                                                                     |[0x800005d0]:fcvt.wu.s a4, fs2, dyn<br> [0x800005d4]:csrrs a7, fflags, zero<br> [0x800005d8]:sd a4, 336(a5)<br>     |
|  24|[0x80002380]<br>0x0000000000000000|- rd : x25<br> - rs1 : f24<br>                                                                                                     |[0x800005e8]:fcvt.wu.s s9, fs8, dyn<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:sd s9, 352(a5)<br>     |
|  25|[0x80002390]<br>0x0000000000000000|- rd : x17<br> - rs1 : f10<br>                                                                                                     |[0x8000060c]:fcvt.wu.s a7, fa0, dyn<br> [0x80000610]:csrrs s5, fflags, zero<br> [0x80000614]:sd a7, 0(s3)<br>       |
|  26|[0x800023a0]<br>0x0000000000000000|- rd : x5<br> - rs1 : f4<br>                                                                                                       |[0x80000630]:fcvt.wu.s t0, ft4, dyn<br> [0x80000634]:csrrs a7, fflags, zero<br> [0x80000638]:sd t0, 0(a5)<br>       |
|  27|[0x800023b0]<br>0x0000000000000000|- rd : x23<br> - rs1 : f9<br>                                                                                                      |[0x80000648]:fcvt.wu.s s7, fs1, dyn<br> [0x8000064c]:csrrs a7, fflags, zero<br> [0x80000650]:sd s7, 16(a5)<br>      |
|  28|[0x800023c0]<br>0x0000000000000000|- rd : x26<br> - rs1 : f8<br>                                                                                                      |[0x80000660]:fcvt.wu.s s10, fs0, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:sd s10, 32(a5)<br>    |
|  29|[0x800023d0]<br>0x0000000000000000|- rd : x28<br> - rs1 : f6<br>                                                                                                      |[0x80000678]:fcvt.wu.s t3, ft6, dyn<br> [0x8000067c]:csrrs a7, fflags, zero<br> [0x80000680]:sd t3, 48(a5)<br>      |
|  30|[0x800023e0]<br>0x0000000000000000|- rd : x21<br> - rs1 : f30<br>                                                                                                     |[0x80000690]:fcvt.wu.s s5, ft10, dyn<br> [0x80000694]:csrrs a7, fflags, zero<br> [0x80000698]:sd s5, 64(a5)<br>     |
|  31|[0x800023f0]<br>0x0000000000000000|- rd : x7<br> - rs1 : f17<br>                                                                                                      |[0x800006a8]:fcvt.wu.s t2, fa7, dyn<br> [0x800006ac]:csrrs a7, fflags, zero<br> [0x800006b0]:sd t2, 80(a5)<br>      |
|  32|[0x80002400]<br>0x0000000000000000|- rd : x15<br> - rs1 : f29<br>                                                                                                     |[0x800006cc]:fcvt.wu.s a5, ft9, dyn<br> [0x800006d0]:csrrs s5, fflags, zero<br> [0x800006d4]:sd a5, 0(s3)<br>       |
