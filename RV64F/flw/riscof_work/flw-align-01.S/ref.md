
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800007e0')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | flw-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/flw/riscof_work/flw-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                  coverpoints                                                  |                                                                                                         code                                                                                                          |
|---:|----------------------------------|---------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x0000000000000000|- opcode : flw<br> - rs1 : x22<br> - rd : f31<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x800003ac]:flw ft11, 32(s6)<br> [0x800003b0]:addi zero, zero, 0<br> [0x800003b4]:addi zero, zero, 0<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsw ft11, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>       |
|   2|[0x80002228]<br>0x0000000000000000|- rs1 : x17<br> - rd : f3<br> - ea_align == 0 and (imm_val % 4) == 1<br> - imm_val < 0<br>                     |[0x800003d4]:flw ft3, 4089(a7)<br> [0x800003d8]:addi zero, zero, 0<br> [0x800003dc]:addi zero, zero, 0<br> [0x800003e0]:csrrs s5, fflags, zero<br> [0x800003e4]:fsw ft3, 0(s3)<br> [0x800003e8]:sd s5, 8(s3)<br>       |
|   3|[0x80002238]<br>0x0000000000000000|- rs1 : x5<br> - rd : f11<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                       |[0x800003fc]:flw fa1, 2(t0)<br> [0x80000400]:addi zero, zero, 0<br> [0x80000404]:addi zero, zero, 0<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsw fa1, 0(a5)<br> [0x80000410]:sd a7, 8(a5)<br>          |
|   4|[0x80002248]<br>0x0000000000000000|- rs1 : x10<br> - rd : f6<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                       |[0x8000041c]:flw ft6, 4031(a0)<br> [0x80000420]:addi zero, zero, 0<br> [0x80000424]:addi zero, zero, 0<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsw ft6, 16(a5)<br> [0x80000430]:sd a7, 24(a5)<br>     |
|   5|[0x80002258]<br>0x0000000000000000|- rs1 : x16<br> - rd : f8<br> - imm_val == 0<br>                                                               |[0x80000444]:flw fs0, 0(a6)<br> [0x80000448]:addi zero, zero, 0<br> [0x8000044c]:addi zero, zero, 0<br> [0x80000450]:csrrs s5, fflags, zero<br> [0x80000454]:fsw fs0, 0(s3)<br> [0x80000458]:sd s5, 8(s3)<br>          |
|   6|[0x80002268]<br>0x0000000000000000|- rs1 : x19<br> - rd : f16<br>                                                                                 |[0x8000046c]:flw fa6, 2048(s3)<br> [0x80000470]:addi zero, zero, 0<br> [0x80000474]:addi zero, zero, 0<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsw fa6, 0(a5)<br> [0x80000480]:sd a7, 8(a5)<br>       |
|   7|[0x80002278]<br>0x0000000000000000|- rs1 : x31<br> - rd : f10<br>                                                                                 |[0x8000048c]:flw fa0, 2048(t6)<br> [0x80000490]:addi zero, zero, 0<br> [0x80000494]:addi zero, zero, 0<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:fsw fa0, 16(a5)<br> [0x800004a0]:sd a7, 24(a5)<br>     |
|   8|[0x80002288]<br>0x0000000000000000|- rs1 : x27<br> - rd : f19<br>                                                                                 |[0x800004ac]:flw fs3, 2048(s11)<br> [0x800004b0]:addi zero, zero, 0<br> [0x800004b4]:addi zero, zero, 0<br> [0x800004b8]:csrrs a7, fflags, zero<br> [0x800004bc]:fsw fs3, 32(a5)<br> [0x800004c0]:sd a7, 40(a5)<br>    |
|   9|[0x80002298]<br>0x0000000000000000|- rs1 : x2<br> - rd : f23<br>                                                                                  |[0x800004cc]:flw fs7, 2048(sp)<br> [0x800004d0]:addi zero, zero, 0<br> [0x800004d4]:addi zero, zero, 0<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsw fs7, 48(a5)<br> [0x800004e0]:sd a7, 56(a5)<br>     |
|  10|[0x800022a8]<br>0x0000000000000000|- rs1 : x1<br> - rd : f7<br>                                                                                   |[0x800004ec]:flw ft7, 2048(ra)<br> [0x800004f0]:addi zero, zero, 0<br> [0x800004f4]:addi zero, zero, 0<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsw ft7, 64(a5)<br> [0x80000500]:sd a7, 72(a5)<br>     |
|  11|[0x800022b8]<br>0x0000000000000000|- rs1 : x25<br> - rd : f1<br>                                                                                  |[0x8000050c]:flw ft1, 2048(s9)<br> [0x80000510]:addi zero, zero, 0<br> [0x80000514]:addi zero, zero, 0<br> [0x80000518]:csrrs a7, fflags, zero<br> [0x8000051c]:fsw ft1, 80(a5)<br> [0x80000520]:sd a7, 88(a5)<br>     |
|  12|[0x800022c8]<br>0x0000000000000000|- rs1 : x21<br> - rd : f28<br>                                                                                 |[0x8000052c]:flw ft8, 2048(s5)<br> [0x80000530]:addi zero, zero, 0<br> [0x80000534]:addi zero, zero, 0<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsw ft8, 96(a5)<br> [0x80000540]:sd a7, 104(a5)<br>    |
|  13|[0x800022d8]<br>0x0000000000000000|- rs1 : x12<br> - rd : f26<br>                                                                                 |[0x8000054c]:flw fs10, 2048(a2)<br> [0x80000550]:addi zero, zero, 0<br> [0x80000554]:addi zero, zero, 0<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:fsw fs10, 112(a5)<br> [0x80000560]:sd a7, 120(a5)<br> |
|  14|[0x800022e8]<br>0x0000000000000000|- rs1 : x14<br> - rd : f9<br>                                                                                  |[0x8000056c]:flw fs1, 2048(a4)<br> [0x80000570]:addi zero, zero, 0<br> [0x80000574]:addi zero, zero, 0<br> [0x80000578]:csrrs a7, fflags, zero<br> [0x8000057c]:fsw fs1, 128(a5)<br> [0x80000580]:sd a7, 136(a5)<br>   |
|  15|[0x800022f8]<br>0x0000000000000000|- rs1 : x8<br> - rd : f24<br>                                                                                  |[0x8000058c]:flw fs8, 2048(fp)<br> [0x80000590]:addi zero, zero, 0<br> [0x80000594]:addi zero, zero, 0<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsw fs8, 144(a5)<br> [0x800005a0]:sd a7, 152(a5)<br>   |
|  16|[0x80002308]<br>0x0000000000000000|- rs1 : x6<br> - rd : f4<br>                                                                                   |[0x800005ac]:flw ft4, 2048(t1)<br> [0x800005b0]:addi zero, zero, 0<br> [0x800005b4]:addi zero, zero, 0<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:fsw ft4, 160(a5)<br> [0x800005c0]:sd a7, 168(a5)<br>   |
|  17|[0x80002318]<br>0x0000000000000000|- rs1 : x15<br> - rd : f0<br>                                                                                  |[0x800005d4]:flw ft0, 2048(a5)<br> [0x800005d8]:addi zero, zero, 0<br> [0x800005dc]:addi zero, zero, 0<br> [0x800005e0]:csrrs s5, fflags, zero<br> [0x800005e4]:fsw ft0, 0(s3)<br> [0x800005e8]:sd s5, 8(s3)<br>       |
|  18|[0x80002328]<br>0x0000000000000000|- rs1 : x23<br> - rd : f17<br>                                                                                 |[0x800005fc]:flw fa7, 2048(s7)<br> [0x80000600]:addi zero, zero, 0<br> [0x80000604]:addi zero, zero, 0<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsw fa7, 0(a5)<br> [0x80000610]:sd a7, 8(a5)<br>       |
|  19|[0x80002338]<br>0x0000000000000000|- rs1 : x24<br> - rd : f22<br>                                                                                 |[0x8000061c]:flw fs6, 2048(s8)<br> [0x80000620]:addi zero, zero, 0<br> [0x80000624]:addi zero, zero, 0<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsw fs6, 16(a5)<br> [0x80000630]:sd a7, 24(a5)<br>     |
|  20|[0x80002348]<br>0x0000000000000000|- rs1 : x3<br> - rd : f30<br>                                                                                  |[0x8000063c]:flw ft10, 2048(gp)<br> [0x80000640]:addi zero, zero, 0<br> [0x80000644]:addi zero, zero, 0<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsw ft10, 32(a5)<br> [0x80000650]:sd a7, 40(a5)<br>   |
|  21|[0x80002358]<br>0x0000000000000000|- rs1 : x26<br> - rd : f5<br>                                                                                  |[0x8000065c]:flw ft5, 2048(s10)<br> [0x80000660]:addi zero, zero, 0<br> [0x80000664]:addi zero, zero, 0<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsw ft5, 48(a5)<br> [0x80000670]:sd a7, 56(a5)<br>    |
|  22|[0x80002368]<br>0x0000000000000000|- rs1 : x28<br> - rd : f20<br>                                                                                 |[0x8000067c]:flw fs4, 2048(t3)<br> [0x80000680]:addi zero, zero, 0<br> [0x80000684]:addi zero, zero, 0<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsw fs4, 64(a5)<br> [0x80000690]:sd a7, 72(a5)<br>     |
|  23|[0x80002378]<br>0x0000000000000000|- rs1 : x9<br> - rd : f2<br>                                                                                   |[0x8000069c]:flw ft2, 2048(s1)<br> [0x800006a0]:addi zero, zero, 0<br> [0x800006a4]:addi zero, zero, 0<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsw ft2, 80(a5)<br> [0x800006b0]:sd a7, 88(a5)<br>     |
|  24|[0x80002388]<br>0x0000000000000000|- rs1 : x29<br> - rd : f29<br>                                                                                 |[0x800006bc]:flw ft9, 2048(t4)<br> [0x800006c0]:addi zero, zero, 0<br> [0x800006c4]:addi zero, zero, 0<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsw ft9, 96(a5)<br> [0x800006d0]:sd a7, 104(a5)<br>    |
|  25|[0x80002398]<br>0x0000000000000000|- rs1 : x18<br> - rd : f18<br>                                                                                 |[0x800006dc]:flw fs2, 2048(s2)<br> [0x800006e0]:addi zero, zero, 0<br> [0x800006e4]:addi zero, zero, 0<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsw fs2, 112(a5)<br> [0x800006f0]:sd a7, 120(a5)<br>   |
|  26|[0x800023a8]<br>0x0000000000000000|- rs1 : x13<br> - rd : f21<br>                                                                                 |[0x800006fc]:flw fs5, 2048(a3)<br> [0x80000700]:addi zero, zero, 0<br> [0x80000704]:addi zero, zero, 0<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsw fs5, 128(a5)<br> [0x80000710]:sd a7, 136(a5)<br>   |
|  27|[0x800023b8]<br>0x0000000000000000|- rs1 : x30<br> - rd : f25<br>                                                                                 |[0x8000071c]:flw fs9, 2048(t5)<br> [0x80000720]:addi zero, zero, 0<br> [0x80000724]:addi zero, zero, 0<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsw fs9, 144(a5)<br> [0x80000730]:sd a7, 152(a5)<br>   |
|  28|[0x800023c8]<br>0x0000000000000000|- rs1 : x11<br> - rd : f12<br>                                                                                 |[0x8000073c]:flw fa2, 2048(a1)<br> [0x80000740]:addi zero, zero, 0<br> [0x80000744]:addi zero, zero, 0<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsw fa2, 160(a5)<br> [0x80000750]:sd a7, 168(a5)<br>   |
|  29|[0x800023d8]<br>0x0000000000000000|- rs1 : x20<br> - rd : f14<br>                                                                                 |[0x8000075c]:flw fa4, 2048(s4)<br> [0x80000760]:addi zero, zero, 0<br> [0x80000764]:addi zero, zero, 0<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsw fa4, 176(a5)<br> [0x80000770]:sd a7, 184(a5)<br>   |
|  30|[0x800023e8]<br>0x0000000000000000|- rs1 : x4<br> - rd : f27<br>                                                                                  |[0x8000077c]:flw fs11, 2048(tp)<br> [0x80000780]:addi zero, zero, 0<br> [0x80000784]:addi zero, zero, 0<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsw fs11, 192(a5)<br> [0x80000790]:sd a7, 200(a5)<br> |
|  31|[0x800023f8]<br>0x0000000000000000|- rs1 : x7<br> - rd : f13<br>                                                                                  |[0x8000079c]:flw fa3, 2048(t2)<br> [0x800007a0]:addi zero, zero, 0<br> [0x800007a4]:addi zero, zero, 0<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsw fa3, 208(a5)<br> [0x800007b0]:sd a7, 216(a5)<br>   |
|  32|[0x80002408]<br>0x0000000000000000|- rd : f15<br>                                                                                                 |[0x800007bc]:flw fa5, 2048(gp)<br> [0x800007c0]:addi zero, zero, 0<br> [0x800007c4]:addi zero, zero, 0<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsw fa5, 224(a5)<br> [0x800007d0]:sd a7, 232(a5)<br>   |
