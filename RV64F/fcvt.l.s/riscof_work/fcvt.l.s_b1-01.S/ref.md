
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000710')]      |
| SIG_REGION                | [('0x80002210', '0x80002420', '66 dwords')]      |
| COV_LABELS                | fcvt.l.s_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fcvt.l.s/riscof_work/fcvt.l.s_b1-01.S/ref.S    |
| Total Number of coverpoints| 93     |
| Total Coverpoints Hit     | 89      |
| Total Signature Updates   | 66      |
| STAT1                     | 32      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 33     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800006fc]:fcvt.l.s t6, ft11, dyn
      [0x80000700]:csrrs a7, fflags, zero
      [0x80000704]:sd t6, 0(a5)
 -- Signature Address: 0x80002410 Data: 0x7FFFFFFFFFFFFFFF
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.l.s
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.l.s', 'rd : x19', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.l.s s3, ft9, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd s3, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002218]:0x0000000000000000




Last Coverpoint : ['rd : x9', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fcvt.l.s s1, ft8, dyn
	-[0x800003d0]:csrrs a7, fflags, zero
	-[0x800003d4]:sd s1, 16(a5)
Current Store : [0x800003d8] : sd a7, 24(a5) -- Store: [0x80002228]:0x0000000000000000




Last Coverpoint : ['rd : x29', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fcvt.l.s t4, fa7, dyn
	-[0x800003e8]:csrrs a7, fflags, zero
	-[0x800003ec]:sd t4, 32(a5)
Current Store : [0x800003f0] : sd a7, 40(a5) -- Store: [0x80002238]:0x0000000000000000




Last Coverpoint : ['rd : x17', 'rs1 : f4', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000408]:fcvt.l.s a7, ft4, dyn
	-[0x8000040c]:csrrs s5, fflags, zero
	-[0x80000410]:sd a7, 0(s3)
Current Store : [0x80000414] : sd s5, 8(s3) -- Store: [0x80002248]:0x0000000000000010




Last Coverpoint : ['rd : x25', 'rs1 : f16', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.l.s s9, fa6, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd s9, 0(a5)
Current Store : [0x80000438] : sd a7, 8(a5) -- Store: [0x80002258]:0x0000000000000010




Last Coverpoint : ['rd : x12', 'rs1 : f23', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.l.s a2, fs7, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd a2, 16(a5)
Current Store : [0x80000450] : sd a7, 24(a5) -- Store: [0x80002268]:0x0000000000000010




Last Coverpoint : ['rd : x0', 'rs1 : f3', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.l.s zero, ft3, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd zero, 32(a5)
Current Store : [0x80000468] : sd a7, 40(a5) -- Store: [0x80002278]:0x0000000000000010




Last Coverpoint : ['rd : x2', 'rs1 : f18', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000474]:fcvt.l.s sp, fs2, dyn
	-[0x80000478]:csrrs a7, fflags, zero
	-[0x8000047c]:sd sp, 48(a5)
Current Store : [0x80000480] : sd a7, 56(a5) -- Store: [0x80002288]:0x0000000000000010




Last Coverpoint : ['rd : x13', 'rs1 : f10', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000048c]:fcvt.l.s a3, fa0, dyn
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd a3, 64(a5)
Current Store : [0x80000498] : sd a7, 72(a5) -- Store: [0x80002298]:0x0000000000000010




Last Coverpoint : ['rd : x18', 'rs1 : f31', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fcvt.l.s s2, ft11, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s2, 80(a5)
Current Store : [0x800004b0] : sd a7, 88(a5) -- Store: [0x800022a8]:0x0000000000000010




Last Coverpoint : ['rd : x4', 'rs1 : f25', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fcvt.l.s tp, fs9, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd tp, 96(a5)
Current Store : [0x800004c8] : sd a7, 104(a5) -- Store: [0x800022b8]:0x0000000000000010




Last Coverpoint : ['rd : x23', 'rs1 : f13', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fcvt.l.s s7, fa3, dyn
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd s7, 112(a5)
Current Store : [0x800004e0] : sd a7, 120(a5) -- Store: [0x800022c8]:0x0000000000000010




Last Coverpoint : ['rd : x10', 'rs1 : f14', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.l.s a0, fa4, dyn
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd a0, 128(a5)
Current Store : [0x800004f8] : sd a7, 136(a5) -- Store: [0x800022d8]:0x0000000000000010




Last Coverpoint : ['rd : x7', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000504]:fcvt.l.s t2, fa2, dyn
	-[0x80000508]:csrrs a7, fflags, zero
	-[0x8000050c]:sd t2, 144(a5)
Current Store : [0x80000510] : sd a7, 152(a5) -- Store: [0x800022e8]:0x0000000000000011




Last Coverpoint : ['rd : x31', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fcvt.l.s t6, fs4, dyn
	-[0x80000520]:csrrs a7, fflags, zero
	-[0x80000524]:sd t6, 160(a5)
Current Store : [0x80000528] : sd a7, 168(a5) -- Store: [0x800022f8]:0x0000000000000011




Last Coverpoint : ['rd : x8', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000534]:fcvt.l.s fp, ft2, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd fp, 176(a5)
Current Store : [0x80000540] : sd a7, 184(a5) -- Store: [0x80002308]:0x0000000000000011




Last Coverpoint : ['rd : x15', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000558]:fcvt.l.s a5, fs5, dyn
	-[0x8000055c]:csrrs s5, fflags, zero
	-[0x80000560]:sd a5, 0(s3)
Current Store : [0x80000564] : sd s5, 8(s3) -- Store: [0x80002318]:0x0000000000000011




Last Coverpoint : ['rd : x14', 'rs1 : f27', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fcvt.l.s a4, fs11, dyn
	-[0x80000580]:csrrs a7, fflags, zero
	-[0x80000584]:sd a4, 0(a5)
Current Store : [0x80000588] : sd a7, 8(a5) -- Store: [0x80002328]:0x0000000000000011




Last Coverpoint : ['rd : x24', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.l.s s8, fa1, dyn
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd s8, 16(a5)
Current Store : [0x800005a0] : sd a7, 24(a5) -- Store: [0x80002338]:0x0000000000000011




Last Coverpoint : ['rd : x1', 'rs1 : f7', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005ac]:fcvt.l.s ra, ft7, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd ra, 32(a5)
Current Store : [0x800005b8] : sd a7, 40(a5) -- Store: [0x80002348]:0x0000000000000011




Last Coverpoint : ['rd : x21', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fcvt.l.s s5, ft0, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd s5, 48(a5)
Current Store : [0x800005d0] : sd a7, 56(a5) -- Store: [0x80002358]:0x0000000000000011




Last Coverpoint : ['rd : x27', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fcvt.l.s s11, fs3, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd s11, 64(a5)
Current Store : [0x800005e8] : sd a7, 72(a5) -- Store: [0x80002368]:0x0000000000000011




Last Coverpoint : ['rd : x6', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fcvt.l.s t1, ft1, dyn
	-[0x800005f8]:csrrs a7, fflags, zero
	-[0x800005fc]:sd t1, 80(a5)
Current Store : [0x80000600] : sd a7, 88(a5) -- Store: [0x80002378]:0x0000000000000011




Last Coverpoint : ['rd : x5', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000060c]:fcvt.l.s t0, ft6, dyn
	-[0x80000610]:csrrs a7, fflags, zero
	-[0x80000614]:sd t0, 96(a5)
Current Store : [0x80000618] : sd a7, 104(a5) -- Store: [0x80002388]:0x0000000000000011




Last Coverpoint : ['rd : x28', 'rs1 : f26']
Last Code Sequence : 
	-[0x80000624]:fcvt.l.s t3, fs10, dyn
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd t3, 112(a5)
Current Store : [0x80000630] : sd a7, 120(a5) -- Store: [0x80002398]:0x0000000000000011




Last Coverpoint : ['rd : x22', 'rs1 : f30']
Last Code Sequence : 
	-[0x8000063c]:fcvt.l.s s6, ft10, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd s6, 128(a5)
Current Store : [0x80000648] : sd a7, 136(a5) -- Store: [0x800023a8]:0x0000000000000011




Last Coverpoint : ['rd : x11', 'rs1 : f22']
Last Code Sequence : 
	-[0x80000654]:fcvt.l.s a1, fs6, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd a1, 144(a5)
Current Store : [0x80000660] : sd a7, 152(a5) -- Store: [0x800023b8]:0x0000000000000011




Last Coverpoint : ['rd : x20', 'rs1 : f24']
Last Code Sequence : 
	-[0x8000066c]:fcvt.l.s s4, fs8, dyn
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd s4, 160(a5)
Current Store : [0x80000678] : sd a7, 168(a5) -- Store: [0x800023c8]:0x0000000000000011




Last Coverpoint : ['rd : x26', 'rs1 : f8']
Last Code Sequence : 
	-[0x80000684]:fcvt.l.s s10, fs0, dyn
	-[0x80000688]:csrrs a7, fflags, zero
	-[0x8000068c]:sd s10, 176(a5)
Current Store : [0x80000690] : sd a7, 184(a5) -- Store: [0x800023d8]:0x0000000000000011




Last Coverpoint : ['rd : x3', 'rs1 : f9']
Last Code Sequence : 
	-[0x8000069c]:fcvt.l.s gp, fs1, dyn
	-[0x800006a0]:csrrs a7, fflags, zero
	-[0x800006a4]:sd gp, 192(a5)
Current Store : [0x800006a8] : sd a7, 200(a5) -- Store: [0x800023e8]:0x0000000000000011




Last Coverpoint : ['rd : x30', 'rs1 : f5']
Last Code Sequence : 
	-[0x800006b4]:fcvt.l.s t5, ft5, dyn
	-[0x800006b8]:csrrs a7, fflags, zero
	-[0x800006bc]:sd t5, 208(a5)
Current Store : [0x800006c0] : sd a7, 216(a5) -- Store: [0x800023f8]:0x0000000000000011




Last Coverpoint : ['rd : x16', 'rs1 : f15']
Last Code Sequence : 
	-[0x800006d8]:fcvt.l.s a6, fa5, dyn
	-[0x800006dc]:csrrs s5, fflags, zero
	-[0x800006e0]:sd a6, 0(s3)
Current Store : [0x800006e4] : sd s5, 8(s3) -- Store: [0x80002408]:0x0000000000000011




Last Coverpoint : ['opcode : fcvt.l.s', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006fc]:fcvt.l.s t6, ft11, dyn
	-[0x80000700]:csrrs a7, fflags, zero
	-[0x80000704]:sd t6, 0(a5)
Current Store : [0x80000708] : sd a7, 8(a5) -- Store: [0x80002418]:0x0000000000000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                           coverpoints                                                            |                                                       code                                                       |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x0000000000000000|- opcode : fcvt.l.s<br> - rd : x19<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.l.s s3, ft9, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd s3, 0(a5)<br>      |
|   2|[0x80002220]<br>0xFFFFFFFFFFFFFFFF|- rd : x9<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                          |[0x800003cc]:fcvt.l.s s1, ft8, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:sd s1, 16(a5)<br>     |
|   3|[0x80002230]<br>0x0000000000000001|- rd : x29<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                         |[0x800003e4]:fcvt.l.s t4, fa7, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:sd t4, 32(a5)<br>     |
|   4|[0x80002240]<br>0x7FFFFFFFFFFFFFFF|- rd : x17<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and rm_val == 0  #nosat<br>                          |[0x80000408]:fcvt.l.s a7, ft4, dyn<br> [0x8000040c]:csrrs s5, fflags, zero<br> [0x80000410]:sd a7, 0(s3)<br>      |
|   5|[0x80002250]<br>0x7FFFFFFFFFFFFFFF|- rd : x25<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat<br>                         |[0x8000042c]:fcvt.l.s s9, fa6, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd s9, 0(a5)<br>      |
|   6|[0x80002260]<br>0x7FFFFFFFFFFFFFFF|- rd : x12<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and rm_val == 0  #nosat<br>                         |[0x80000444]:fcvt.l.s a2, fs7, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd a2, 16(a5)<br>     |
|   7|[0x80002270]<br>0x0000000000000000|- rd : x0<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat<br>                           |[0x8000045c]:fcvt.l.s zero, ft3, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd zero, 32(a5)<br> |
|   8|[0x80002280]<br>0x7FFFFFFFFFFFFFFF|- rd : x2<br> - rs1 : f18<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x400000 and rm_val == 0  #nosat<br>                          |[0x80000474]:fcvt.l.s sp, fs2, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:sd sp, 48(a5)<br>     |
|   9|[0x80002290]<br>0x7FFFFFFFFFFFFFFF|- rd : x13<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and rm_val == 0  #nosat<br>                         |[0x8000048c]:fcvt.l.s a3, fa0, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd a3, 64(a5)<br>     |
|  10|[0x800022a0]<br>0x8000000000000000|- rd : x18<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and rm_val == 0  #nosat<br>                         |[0x800004a4]:fcvt.l.s s2, ft11, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s2, 80(a5)<br>    |
|  11|[0x800022b0]<br>0x7FFFFFFFFFFFFFFF|- rd : x4<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and rm_val == 0  #nosat<br>                          |[0x800004bc]:fcvt.l.s tp, fs9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd tp, 96(a5)<br>     |
|  12|[0x800022c0]<br>0x8000000000000000|- rd : x23<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat<br>                         |[0x800004d4]:fcvt.l.s s7, fa3, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd s7, 112(a5)<br>    |
|  13|[0x800022d0]<br>0x7FFFFFFFFFFFFFFF|- rd : x10<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat<br>                         |[0x800004ec]:fcvt.l.s a0, fa4, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd a0, 128(a5)<br>    |
|  14|[0x800022e0]<br>0x0000000000000000|- rd : x7<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and rm_val == 0  #nosat<br>                          |[0x80000504]:fcvt.l.s t2, fa2, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:sd t2, 144(a5)<br>    |
|  15|[0x800022f0]<br>0x0000000000000000|- rd : x31<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and rm_val == 0  #nosat<br>                         |[0x8000051c]:fcvt.l.s t6, fs4, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:sd t6, 160(a5)<br>    |
|  16|[0x80002300]<br>0x0000000000000000|- rd : x8<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                           |[0x80000534]:fcvt.l.s fp, ft2, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd fp, 176(a5)<br>    |
|  17|[0x80002310]<br>0x0000000000000000|- rd : x15<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                         |[0x80000558]:fcvt.l.s a5, fs5, dyn<br> [0x8000055c]:csrrs s5, fflags, zero<br> [0x80000560]:sd a5, 0(s3)<br>      |
|  18|[0x80002320]<br>0x0000000000000000|- rd : x14<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and rm_val == 0  #nosat<br>                         |[0x8000057c]:fcvt.l.s a4, fs11, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:sd a4, 0(a5)<br>     |
|  19|[0x80002330]<br>0x0000000000000000|- rd : x24<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and rm_val == 0  #nosat<br>                         |[0x80000594]:fcvt.l.s s8, fa1, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd s8, 16(a5)<br>     |
|  20|[0x80002340]<br>0x0000000000000000|- rd : x1<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and rm_val == 0  #nosat<br>                           |[0x800005ac]:fcvt.l.s ra, ft7, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd ra, 32(a5)<br>     |
|  21|[0x80002350]<br>0x0000000000000000|- rd : x21<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and rm_val == 0  #nosat<br>                          |[0x800005c4]:fcvt.l.s s5, ft0, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd s5, 48(a5)<br>     |
|  22|[0x80002360]<br>0x0000000000000000|- rd : x27<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and rm_val == 0  #nosat<br>                         |[0x800005dc]:fcvt.l.s s11, fs3, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd s11, 64(a5)<br>   |
|  23|[0x80002370]<br>0x0000000000000000|- rd : x6<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and rm_val == 0  #nosat<br>                           |[0x800005f4]:fcvt.l.s t1, ft1, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:sd t1, 80(a5)<br>     |
|  24|[0x80002380]<br>0x0000000000000000|- rd : x5<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                           |[0x8000060c]:fcvt.l.s t0, ft6, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:sd t0, 96(a5)<br>     |
|  25|[0x80002390]<br>0x0000000000000000|- rd : x28<br> - rs1 : f26<br>                                                                                                    |[0x80000624]:fcvt.l.s t3, fs10, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd t3, 112(a5)<br>   |
|  26|[0x800023a0]<br>0x0000000000000000|- rd : x22<br> - rs1 : f30<br>                                                                                                    |[0x8000063c]:fcvt.l.s s6, ft10, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd s6, 128(a5)<br>   |
|  27|[0x800023b0]<br>0x0000000000000000|- rd : x11<br> - rs1 : f22<br>                                                                                                    |[0x80000654]:fcvt.l.s a1, fs6, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd a1, 144(a5)<br>    |
|  28|[0x800023c0]<br>0x0000000000000000|- rd : x20<br> - rs1 : f24<br>                                                                                                    |[0x8000066c]:fcvt.l.s s4, fs8, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd s4, 160(a5)<br>    |
|  29|[0x800023d0]<br>0x0000000000000000|- rd : x26<br> - rs1 : f8<br>                                                                                                     |[0x80000684]:fcvt.l.s s10, fs0, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:sd s10, 176(a5)<br>  |
|  30|[0x800023e0]<br>0x0000000000000000|- rd : x3<br> - rs1 : f9<br>                                                                                                      |[0x8000069c]:fcvt.l.s gp, fs1, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:sd gp, 192(a5)<br>    |
|  31|[0x800023f0]<br>0x0000000000000000|- rd : x30<br> - rs1 : f5<br>                                                                                                     |[0x800006b4]:fcvt.l.s t5, ft5, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:sd t5, 208(a5)<br>    |
|  32|[0x80002400]<br>0x0000000000000000|- rd : x16<br> - rs1 : f15<br>                                                                                                    |[0x800006d8]:fcvt.l.s a6, fa5, dyn<br> [0x800006dc]:csrrs s5, fflags, zero<br> [0x800006e0]:sd a6, 0(s3)<br>      |
