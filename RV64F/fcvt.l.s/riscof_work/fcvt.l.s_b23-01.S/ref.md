
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000850')]      |
| SIG_REGION                | [('0x80002210', '0x800024f0', '92 dwords')]      |
| COV_LABELS                | fcvt.l.s_b23      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fcvt.l.s/riscof_work/fcvt.l.s_b23-01.S/ref.S    |
| Total Number of coverpoints| 114     |
| Total Coverpoints Hit     | 110      |
| Total Signature Updates   | 92      |
| STAT1                     | 45      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 46     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000834]:fcvt.l.s t6, ft11, dyn
      [0x80000838]:csrrs a7, fflags, zero
      [0x8000083c]:sd t6, 224(a5)
 -- Signature Address: 0x800024e0 Data: 0x0000000080000200
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.l.s
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 3  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.l.s', 'rd : x25', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.l.s s9, fs6, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd s9, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002218]:0x0000000000000000




Last Coverpoint : ['rd : x23', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fcvt.l.s s7, fs11, dyn
	-[0x800003d0]:csrrs a7, fflags, zero
	-[0x800003d4]:sd s7, 16(a5)
Current Store : [0x800003d8] : sd a7, 24(a5) -- Store: [0x80002228]:0x0000000000000000




Last Coverpoint : ['rd : x20', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fcvt.l.s s4, fa6, dyn
	-[0x800003e8]:csrrs a7, fflags, zero
	-[0x800003ec]:sd s4, 32(a5)
Current Store : [0x800003f0] : sd a7, 40(a5) -- Store: [0x80002238]:0x0000000000000000




Last Coverpoint : ['rd : x26', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.l.s s10, ft8, dyn
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd s10, 48(a5)
Current Store : [0x80000408] : sd a7, 56(a5) -- Store: [0x80002248]:0x0000000000000000




Last Coverpoint : ['rd : x14', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000414]:fcvt.l.s a4, fs3, dyn
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd a4, 64(a5)
Current Store : [0x80000420] : sd a7, 72(a5) -- Store: [0x80002258]:0x0000000000000000




Last Coverpoint : ['rd : x22', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.l.s s6, ft6, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd s6, 80(a5)
Current Store : [0x80000438] : sd a7, 88(a5) -- Store: [0x80002268]:0x0000000000000000




Last Coverpoint : ['rd : x13', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.l.s a3, fa2, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd a3, 96(a5)
Current Store : [0x80000450] : sd a7, 104(a5) -- Store: [0x80002278]:0x0000000000000000




Last Coverpoint : ['rd : x9', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.l.s s1, ft4, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd s1, 112(a5)
Current Store : [0x80000468] : sd a7, 120(a5) -- Store: [0x80002288]:0x0000000000000000




Last Coverpoint : ['rd : x29', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000474]:fcvt.l.s t4, ft10, dyn
	-[0x80000478]:csrrs a7, fflags, zero
	-[0x8000047c]:sd t4, 128(a5)
Current Store : [0x80000480] : sd a7, 136(a5) -- Store: [0x80002298]:0x0000000000000000




Last Coverpoint : ['rd : x30', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000048c]:fcvt.l.s t5, fs1, dyn
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd t5, 144(a5)
Current Store : [0x80000498] : sd a7, 152(a5) -- Store: [0x800022a8]:0x0000000000000000




Last Coverpoint : ['rd : x1', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fcvt.l.s ra, fa3, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd ra, 160(a5)
Current Store : [0x800004b0] : sd a7, 168(a5) -- Store: [0x800022b8]:0x0000000000000000




Last Coverpoint : ['rd : x21', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fcvt.l.s s5, fs0, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd s5, 176(a5)
Current Store : [0x800004c8] : sd a7, 184(a5) -- Store: [0x800022c8]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fcvt.l.s zero, fa4, dyn
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd zero, 192(a5)
Current Store : [0x800004e0] : sd a7, 200(a5) -- Store: [0x800022d8]:0x0000000000000000




Last Coverpoint : ['rd : x11', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.l.s a1, ft11, dyn
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd a1, 208(a5)
Current Store : [0x800004f8] : sd a7, 216(a5) -- Store: [0x800022e8]:0x0000000000000000




Last Coverpoint : ['rd : x18', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000504]:fcvt.l.s s2, fa0, dyn
	-[0x80000508]:csrrs a7, fflags, zero
	-[0x8000050c]:sd s2, 224(a5)
Current Store : [0x80000510] : sd a7, 232(a5) -- Store: [0x800022f8]:0x0000000000000000




Last Coverpoint : ['rd : x6', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fcvt.l.s t1, ft2, dyn
	-[0x80000520]:csrrs a7, fflags, zero
	-[0x80000524]:sd t1, 240(a5)
Current Store : [0x80000528] : sd a7, 248(a5) -- Store: [0x80002308]:0x0000000000000000




Last Coverpoint : ['rd : x8', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000534]:fcvt.l.s fp, fs8, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd fp, 256(a5)
Current Store : [0x80000540] : sd a7, 264(a5) -- Store: [0x80002318]:0x0000000000000000




Last Coverpoint : ['rd : x19', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000054c]:fcvt.l.s s3, fs9, dyn
	-[0x80000550]:csrrs a7, fflags, zero
	-[0x80000554]:sd s3, 272(a5)
Current Store : [0x80000558] : sd a7, 280(a5) -- Store: [0x80002328]:0x0000000000000000




Last Coverpoint : ['rd : x5', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000564]:fcvt.l.s t0, ft1, dyn
	-[0x80000568]:csrrs a7, fflags, zero
	-[0x8000056c]:sd t0, 288(a5)
Current Store : [0x80000570] : sd a7, 296(a5) -- Store: [0x80002338]:0x0000000000000000




Last Coverpoint : ['rd : x15', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000588]:fcvt.l.s a5, fs5, dyn
	-[0x8000058c]:csrrs s5, fflags, zero
	-[0x80000590]:sd a5, 0(s3)
Current Store : [0x80000594] : sd s5, 8(s3) -- Store: [0x80002348]:0x0000000000000000




Last Coverpoint : ['rd : x24', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005ac]:fcvt.l.s s8, ft5, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd s8, 0(a5)
Current Store : [0x800005b8] : sd a7, 8(a5) -- Store: [0x80002358]:0x0000000000000000




Last Coverpoint : ['rd : x4', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fcvt.l.s tp, ft0, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd tp, 16(a5)
Current Store : [0x800005d0] : sd a7, 24(a5) -- Store: [0x80002368]:0x0000000000000000




Last Coverpoint : ['rd : x28', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fcvt.l.s t3, fs7, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd t3, 32(a5)
Current Store : [0x800005e8] : sd a7, 40(a5) -- Store: [0x80002378]:0x0000000000000000




Last Coverpoint : ['rd : x16', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000600]:fcvt.l.s a6, ft3, dyn
	-[0x80000604]:csrrs s5, fflags, zero
	-[0x80000608]:sd a6, 0(s3)
Current Store : [0x8000060c] : sd s5, 8(s3) -- Store: [0x80002388]:0x0000000000000000




Last Coverpoint : ['rd : x10', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000624]:fcvt.l.s a0, fa7, dyn
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd a0, 0(a5)
Current Store : [0x80000630] : sd a7, 8(a5) -- Store: [0x80002398]:0x0000000000000000




Last Coverpoint : ['rd : x2', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fcvt.l.s sp, fa1, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd sp, 16(a5)
Current Store : [0x80000648] : sd a7, 24(a5) -- Store: [0x800023a8]:0x0000000000000000




Last Coverpoint : ['rd : x12', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000654]:fcvt.l.s a2, ft7, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd a2, 32(a5)
Current Store : [0x80000660] : sd a7, 40(a5) -- Store: [0x800023b8]:0x0000000000000000




Last Coverpoint : ['rd : x3', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000066c]:fcvt.l.s gp, fa5, dyn
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd gp, 48(a5)
Current Store : [0x80000678] : sd a7, 56(a5) -- Store: [0x800023c8]:0x0000000000000000




Last Coverpoint : ['rd : x7', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000684]:fcvt.l.s t2, fs4, dyn
	-[0x80000688]:csrrs a7, fflags, zero
	-[0x8000068c]:sd t2, 64(a5)
Current Store : [0x80000690] : sd a7, 72(a5) -- Store: [0x800023d8]:0x0000000000000000




Last Coverpoint : ['rd : x31', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000069c]:fcvt.l.s t6, ft9, dyn
	-[0x800006a0]:csrrs a7, fflags, zero
	-[0x800006a4]:sd t6, 80(a5)
Current Store : [0x800006a8] : sd a7, 88(a5) -- Store: [0x800023e8]:0x0000000000000000




Last Coverpoint : ['rd : x17', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006c0]:fcvt.l.s a7, fs2, dyn
	-[0x800006c4]:csrrs s5, fflags, zero
	-[0x800006c8]:sd a7, 0(s3)
Current Store : [0x800006cc] : sd s5, 8(s3) -- Store: [0x800023f8]:0x0000000000000000




Last Coverpoint : ['rd : x27', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fcvt.l.s s11, fs10, dyn
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd s11, 0(a5)
Current Store : [0x800006f0] : sd a7, 8(a5) -- Store: [0x80002408]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800006fc]:fcvt.l.s t6, ft11, dyn
	-[0x80000700]:csrrs a7, fflags, zero
	-[0x80000704]:sd t6, 16(a5)
Current Store : [0x80000708] : sd a7, 24(a5) -- Store: [0x80002418]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000714]:fcvt.l.s t6, ft11, dyn
	-[0x80000718]:csrrs a7, fflags, zero
	-[0x8000071c]:sd t6, 32(a5)
Current Store : [0x80000720] : sd a7, 40(a5) -- Store: [0x80002428]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fcvt.l.s t6, ft11, dyn
	-[0x80000730]:csrrs a7, fflags, zero
	-[0x80000734]:sd t6, 48(a5)
Current Store : [0x80000738] : sd a7, 56(a5) -- Store: [0x80002438]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000744]:fcvt.l.s t6, ft11, dyn
	-[0x80000748]:csrrs a7, fflags, zero
	-[0x8000074c]:sd t6, 64(a5)
Current Store : [0x80000750] : sd a7, 72(a5) -- Store: [0x80002448]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000075c]:fcvt.l.s t6, ft11, dyn
	-[0x80000760]:csrrs a7, fflags, zero
	-[0x80000764]:sd t6, 80(a5)
Current Store : [0x80000768] : sd a7, 88(a5) -- Store: [0x80002458]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000774]:fcvt.l.s t6, ft11, dyn
	-[0x80000778]:csrrs a7, fflags, zero
	-[0x8000077c]:sd t6, 96(a5)
Current Store : [0x80000780] : sd a7, 104(a5) -- Store: [0x80002468]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000078c]:fcvt.l.s t6, ft11, dyn
	-[0x80000790]:csrrs a7, fflags, zero
	-[0x80000794]:sd t6, 112(a5)
Current Store : [0x80000798] : sd a7, 120(a5) -- Store: [0x80002478]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fcvt.l.s t6, ft11, dyn
	-[0x800007a8]:csrrs a7, fflags, zero
	-[0x800007ac]:sd t6, 128(a5)
Current Store : [0x800007b0] : sd a7, 136(a5) -- Store: [0x80002488]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007bc]:fcvt.l.s t6, ft11, dyn
	-[0x800007c0]:csrrs a7, fflags, zero
	-[0x800007c4]:sd t6, 144(a5)
Current Store : [0x800007c8] : sd a7, 152(a5) -- Store: [0x80002498]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fcvt.l.s t6, ft11, dyn
	-[0x800007d8]:csrrs a7, fflags, zero
	-[0x800007dc]:sd t6, 160(a5)
Current Store : [0x800007e0] : sd a7, 168(a5) -- Store: [0x800024a8]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fcvt.l.s t6, ft11, dyn
	-[0x800007f0]:csrrs a7, fflags, zero
	-[0x800007f4]:sd t6, 176(a5)
Current Store : [0x800007f8] : sd a7, 184(a5) -- Store: [0x800024b8]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000804]:fcvt.l.s t6, ft11, dyn
	-[0x80000808]:csrrs a7, fflags, zero
	-[0x8000080c]:sd t6, 192(a5)
Current Store : [0x80000810] : sd a7, 200(a5) -- Store: [0x800024c8]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000081c]:fcvt.l.s t6, ft11, dyn
	-[0x80000820]:csrrs a7, fflags, zero
	-[0x80000824]:sd t6, 208(a5)
Current Store : [0x80000828] : sd a7, 216(a5) -- Store: [0x800024d8]:0x0000000000000000




Last Coverpoint : ['opcode : fcvt.l.s', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000834]:fcvt.l.s t6, ft11, dyn
	-[0x80000838]:csrrs a7, fflags, zero
	-[0x8000083c]:sd t6, 224(a5)
Current Store : [0x80000840] : sd a7, 232(a5) -- Store: [0x800024e8]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                           coverpoints                                                            |                                                       code                                                        |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x000000007FFFFE00|- opcode : fcvt.l.s<br> - rd : x25<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.l.s s9, fs6, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd s9, 0(a5)<br>       |
|   2|[0x80002220]<br>0x0000000080000400|- rd : x23<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 4  #nosat<br>                         |[0x800003cc]:fcvt.l.s s7, fs11, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:sd s7, 16(a5)<br>     |
|   3|[0x80002230]<br>0x0000000080000400|- rd : x20<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 3  #nosat<br>                         |[0x800003e4]:fcvt.l.s s4, fa6, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:sd s4, 32(a5)<br>      |
|   4|[0x80002240]<br>0x0000000080000400|- rd : x26<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 2  #nosat<br>                         |[0x800003fc]:fcvt.l.s s10, ft8, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd s10, 48(a5)<br>    |
|   5|[0x80002250]<br>0x0000000080000400|- rd : x14<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 1  #nosat<br>                         |[0x80000414]:fcvt.l.s a4, fs3, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd a4, 64(a5)<br>      |
|   6|[0x80002260]<br>0x0000000080000400|- rd : x22<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000004 and rm_val == 0  #nosat<br>                          |[0x8000042c]:fcvt.l.s s6, ft6, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd s6, 80(a5)<br>      |
|   7|[0x80002270]<br>0x0000000080000300|- rd : x13<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 4  #nosat<br>                         |[0x80000444]:fcvt.l.s a3, fa2, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd a3, 96(a5)<br>      |
|   8|[0x80002280]<br>0x0000000080000300|- rd : x9<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 3  #nosat<br>                           |[0x8000045c]:fcvt.l.s s1, ft4, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd s1, 112(a5)<br>     |
|   9|[0x80002290]<br>0x0000000080000300|- rd : x29<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 2  #nosat<br>                         |[0x80000474]:fcvt.l.s t4, ft10, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:sd t4, 128(a5)<br>    |
|  10|[0x800022a0]<br>0x0000000080000300|- rd : x30<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 1  #nosat<br>                          |[0x8000048c]:fcvt.l.s t5, fs1, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd t5, 144(a5)<br>     |
|  11|[0x800022b0]<br>0x0000000080000300|- rd : x1<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000003 and rm_val == 0  #nosat<br>                          |[0x800004a4]:fcvt.l.s ra, fa3, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd ra, 160(a5)<br>     |
|  12|[0x800022c0]<br>0x0000000080000200|- rd : x21<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 4  #nosat<br>                          |[0x800004bc]:fcvt.l.s s5, fs0, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd s5, 176(a5)<br>     |
|  13|[0x800022d0]<br>0x0000000000000000|- rd : x0<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 3  #nosat<br>                          |[0x800004d4]:fcvt.l.s zero, fa4, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd zero, 192(a5)<br> |
|  14|[0x800022e0]<br>0x0000000080000200|- rd : x11<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 2  #nosat<br>                         |[0x800004ec]:fcvt.l.s a1, ft11, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd a1, 208(a5)<br>    |
|  15|[0x800022f0]<br>0x0000000080000200|- rd : x18<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 1  #nosat<br>                         |[0x80000504]:fcvt.l.s s2, fa0, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:sd s2, 224(a5)<br>     |
|  16|[0x80002300]<br>0x0000000080000200|- rd : x6<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000002 and rm_val == 0  #nosat<br>                           |[0x8000051c]:fcvt.l.s t1, ft2, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:sd t1, 240(a5)<br>     |
|  17|[0x80002310]<br>0x0000000080000100|- rd : x8<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 4  #nosat<br>                          |[0x80000534]:fcvt.l.s fp, fs8, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd fp, 256(a5)<br>     |
|  18|[0x80002320]<br>0x0000000080000100|- rd : x19<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 3  #nosat<br>                         |[0x8000054c]:fcvt.l.s s3, fs9, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:sd s3, 272(a5)<br>     |
|  19|[0x80002330]<br>0x0000000080000100|- rd : x5<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 2  #nosat<br>                           |[0x80000564]:fcvt.l.s t0, ft1, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:sd t0, 288(a5)<br>     |
|  20|[0x80002340]<br>0x0000000080000100|- rd : x15<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 1  #nosat<br>                         |[0x80000588]:fcvt.l.s a5, fs5, dyn<br> [0x8000058c]:csrrs s5, fflags, zero<br> [0x80000590]:sd a5, 0(s3)<br>       |
|  21|[0x80002350]<br>0x0000000080000100|- rd : x24<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000001 and rm_val == 0  #nosat<br>                          |[0x800005ac]:fcvt.l.s s8, ft5, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd s8, 0(a5)<br>       |
|  22|[0x80002360]<br>0x0000000080000000|- rd : x4<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 4  #nosat<br>                           |[0x800005c4]:fcvt.l.s tp, ft0, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd tp, 16(a5)<br>      |
|  23|[0x80002370]<br>0x0000000080000000|- rd : x28<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 3  #nosat<br>                         |[0x800005dc]:fcvt.l.s t3, fs7, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd t3, 32(a5)<br>      |
|  24|[0x80002380]<br>0x0000000080000000|- rd : x16<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 2  #nosat<br>                          |[0x80000600]:fcvt.l.s a6, ft3, dyn<br> [0x80000604]:csrrs s5, fflags, zero<br> [0x80000608]:sd a6, 0(s3)<br>       |
|  25|[0x80002390]<br>0x0000000080000000|- rd : x10<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 1  #nosat<br>                         |[0x80000624]:fcvt.l.s a0, fa7, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd a0, 0(a5)<br>       |
|  26|[0x800023a0]<br>0x0000000080000000|- rd : x2<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 0  #nosat<br>                          |[0x8000063c]:fcvt.l.s sp, fa1, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd sp, 16(a5)<br>      |
|  27|[0x800023b0]<br>0x000000007FFFFF80|- rd : x12<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 4  #nosat<br>                          |[0x80000654]:fcvt.l.s a2, ft7, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd a2, 32(a5)<br>      |
|  28|[0x800023c0]<br>0x000000007FFFFF80|- rd : x3<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 3  #nosat<br>                          |[0x8000066c]:fcvt.l.s gp, fa5, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd gp, 48(a5)<br>      |
|  29|[0x800023d0]<br>0x000000007FFFFF80|- rd : x7<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 2  #nosat<br>                          |[0x80000684]:fcvt.l.s t2, fs4, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:sd t2, 64(a5)<br>      |
|  30|[0x800023e0]<br>0x000000007FFFFF80|- rd : x31<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 1  #nosat<br>                         |[0x8000069c]:fcvt.l.s t6, ft9, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:sd t6, 80(a5)<br>      |
|  31|[0x800023f0]<br>0x000000007FFFFF80|- rd : x17<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7fffff and rm_val == 0  #nosat<br>                         |[0x800006c0]:fcvt.l.s a7, fs2, dyn<br> [0x800006c4]:csrrs s5, fflags, zero<br> [0x800006c8]:sd a7, 0(s3)<br>       |
|  32|[0x80002400]<br>0x000000007FFFFF00|- rd : x27<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 4  #nosat<br>                         |[0x800006e4]:fcvt.l.s s11, fs10, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd s11, 0(a5)<br>    |
|  33|[0x80002410]<br>0x000000007FFFFF00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 3  #nosat<br>                                                        |[0x800006fc]:fcvt.l.s t6, ft11, dyn<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:sd t6, 16(a5)<br>     |
|  34|[0x80002420]<br>0x000000007FFFFF00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 2  #nosat<br>                                                        |[0x80000714]:fcvt.l.s t6, ft11, dyn<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:sd t6, 32(a5)<br>     |
|  35|[0x80002430]<br>0x000000007FFFFF00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 1  #nosat<br>                                                        |[0x8000072c]:fcvt.l.s t6, ft11, dyn<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:sd t6, 48(a5)<br>     |
|  36|[0x80002440]<br>0x000000007FFFFF00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffe and rm_val == 0  #nosat<br>                                                        |[0x80000744]:fcvt.l.s t6, ft11, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:sd t6, 64(a5)<br>     |
|  37|[0x80002450]<br>0x000000007FFFFE80|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 4  #nosat<br>                                                        |[0x8000075c]:fcvt.l.s t6, ft11, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:sd t6, 80(a5)<br>     |
|  38|[0x80002460]<br>0x000000007FFFFE80|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 3  #nosat<br>                                                        |[0x80000774]:fcvt.l.s t6, ft11, dyn<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:sd t6, 96(a5)<br>     |
|  39|[0x80002470]<br>0x000000007FFFFE80|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 2  #nosat<br>                                                        |[0x8000078c]:fcvt.l.s t6, ft11, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:sd t6, 112(a5)<br>    |
|  40|[0x80002480]<br>0x000000007FFFFE80|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 1  #nosat<br>                                                        |[0x800007a4]:fcvt.l.s t6, ft11, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:sd t6, 128(a5)<br>    |
|  41|[0x80002490]<br>0x000000007FFFFE80|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffd and rm_val == 0  #nosat<br>                                                        |[0x800007bc]:fcvt.l.s t6, ft11, dyn<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:sd t6, 144(a5)<br>    |
|  42|[0x800024a0]<br>0x000000007FFFFE00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 4  #nosat<br>                                                        |[0x800007d4]:fcvt.l.s t6, ft11, dyn<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:sd t6, 160(a5)<br>    |
|  43|[0x800024b0]<br>0x000000007FFFFE00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 3  #nosat<br>                                                        |[0x800007ec]:fcvt.l.s t6, ft11, dyn<br> [0x800007f0]:csrrs a7, fflags, zero<br> [0x800007f4]:sd t6, 176(a5)<br>    |
|  44|[0x800024c0]<br>0x000000007FFFFE00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 2  #nosat<br>                                                        |[0x80000804]:fcvt.l.s t6, ft11, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:sd t6, 192(a5)<br>    |
|  45|[0x800024d0]<br>0x000000007FFFFE00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x7ffffc and rm_val == 1  #nosat<br>                                                        |[0x8000081c]:fcvt.l.s t6, ft11, dyn<br> [0x80000820]:csrrs a7, fflags, zero<br> [0x80000824]:sd t6, 208(a5)<br>    |
