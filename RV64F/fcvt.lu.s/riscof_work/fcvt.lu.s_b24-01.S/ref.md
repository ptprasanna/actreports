
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000de0')]      |
| SIG_REGION                | [('0x80002310', '0x800029b0', '212 dwords')]      |
| COV_LABELS                | fcvt.lu.s_b24      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fcvt.lu.s/riscof_work/fcvt.lu.s_b24-01.S/ref.S    |
| Total Number of coverpoints| 174     |
| Total Coverpoints Hit     | 170      |
| Total Signature Updates   | 212      |
| STAT1                     | 105      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 106     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000dc8]:fcvt.lu.s t6, ft11, dyn
      [0x80000dcc]:csrrs a7, fflags, zero
      [0x80000dd0]:sd t6, 1376(a5)
 -- Signature Address: 0x800029a0 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.lu.s
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.lu.s', 'rd : x15', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.lu.s a5, ft0, dyn
	-[0x800003b8]:csrrs s5, fflags, zero
	-[0x800003bc]:sd a5, 0(s3)
Current Store : [0x800003c0] : sd s5, 8(s3) -- Store: [0x80002318]:0x0000000000000001




Last Coverpoint : ['rd : x10', 'rs1 : f18', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003d8]:fcvt.lu.s a0, fs2, dyn
	-[0x800003dc]:csrrs a7, fflags, zero
	-[0x800003e0]:sd a0, 0(a5)
Current Store : [0x800003e4] : sd a7, 8(a5) -- Store: [0x80002328]:0x0000000000000001




Last Coverpoint : ['rd : x16', 'rs1 : f23', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.lu.s a6, fs7, dyn
	-[0x80000400]:csrrs s5, fflags, zero
	-[0x80000404]:sd a6, 0(s3)
Current Store : [0x80000408] : sd s5, 8(s3) -- Store: [0x80002338]:0x0000000000000001




Last Coverpoint : ['rd : x28', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000420]:fcvt.lu.s t3, ft10, dyn
	-[0x80000424]:csrrs a7, fflags, zero
	-[0x80000428]:sd t3, 0(a5)
Current Store : [0x8000042c] : sd a7, 8(a5) -- Store: [0x80002348]:0x0000000000000011




Last Coverpoint : ['rd : x20', 'rs1 : f3', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000438]:fcvt.lu.s s4, ft3, dyn
	-[0x8000043c]:csrrs a7, fflags, zero
	-[0x80000440]:sd s4, 16(a5)
Current Store : [0x80000444] : sd a7, 24(a5) -- Store: [0x80002358]:0x0000000000000011




Last Coverpoint : ['rd : x8', 'rs1 : f22', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000450]:fcvt.lu.s fp, fs6, dyn
	-[0x80000454]:csrrs a7, fflags, zero
	-[0x80000458]:sd fp, 32(a5)
Current Store : [0x8000045c] : sd a7, 40(a5) -- Store: [0x80002368]:0x0000000000000011




Last Coverpoint : ['rd : x7', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000468]:fcvt.lu.s t2, ft1, dyn
	-[0x8000046c]:csrrs a7, fflags, zero
	-[0x80000470]:sd t2, 48(a5)
Current Store : [0x80000474] : sd a7, 56(a5) -- Store: [0x80002378]:0x0000000000000011




Last Coverpoint : ['rd : x6', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000480]:fcvt.lu.s t1, fs4, dyn
	-[0x80000484]:csrrs a7, fflags, zero
	-[0x80000488]:sd t1, 64(a5)
Current Store : [0x8000048c] : sd a7, 72(a5) -- Store: [0x80002388]:0x0000000000000011




Last Coverpoint : ['rd : x11', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000498]:fcvt.lu.s a1, ft6, dyn
	-[0x8000049c]:csrrs a7, fflags, zero
	-[0x800004a0]:sd a1, 80(a5)
Current Store : [0x800004a4] : sd a7, 88(a5) -- Store: [0x80002398]:0x0000000000000011




Last Coverpoint : ['rd : x29', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800004b0]:fcvt.lu.s t4, fs11, dyn
	-[0x800004b4]:csrrs a7, fflags, zero
	-[0x800004b8]:sd t4, 96(a5)
Current Store : [0x800004bc] : sd a7, 104(a5) -- Store: [0x800023a8]:0x0000000000000011




Last Coverpoint : ['rd : x9', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004c8]:fcvt.lu.s s1, fa0, dyn
	-[0x800004cc]:csrrs a7, fflags, zero
	-[0x800004d0]:sd s1, 112(a5)
Current Store : [0x800004d4] : sd a7, 120(a5) -- Store: [0x800023b8]:0x0000000000000011




Last Coverpoint : ['rd : x14', 'rs1 : f14', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004e0]:fcvt.lu.s a4, fa4, dyn
	-[0x800004e4]:csrrs a7, fflags, zero
	-[0x800004e8]:sd a4, 128(a5)
Current Store : [0x800004ec] : sd a7, 136(a5) -- Store: [0x800023c8]:0x0000000000000011




Last Coverpoint : ['rd : x30', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004f8]:fcvt.lu.s t5, fs8, dyn
	-[0x800004fc]:csrrs a7, fflags, zero
	-[0x80000500]:sd t5, 144(a5)
Current Store : [0x80000504] : sd a7, 152(a5) -- Store: [0x800023d8]:0x0000000000000011




Last Coverpoint : ['rd : x22', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000510]:fcvt.lu.s s6, fa7, dyn
	-[0x80000514]:csrrs a7, fflags, zero
	-[0x80000518]:sd s6, 160(a5)
Current Store : [0x8000051c] : sd a7, 168(a5) -- Store: [0x800023e8]:0x0000000000000011




Last Coverpoint : ['rd : x3', 'rs1 : f21', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000528]:fcvt.lu.s gp, fs5, dyn
	-[0x8000052c]:csrrs a7, fflags, zero
	-[0x80000530]:sd gp, 176(a5)
Current Store : [0x80000534] : sd a7, 184(a5) -- Store: [0x800023f8]:0x0000000000000011




Last Coverpoint : ['rd : x24', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000540]:fcvt.lu.s s8, ft9, dyn
	-[0x80000544]:csrrs a7, fflags, zero
	-[0x80000548]:sd s8, 192(a5)
Current Store : [0x8000054c] : sd a7, 200(a5) -- Store: [0x80002408]:0x0000000000000011




Last Coverpoint : ['rd : x0', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000558]:fcvt.lu.s zero, ft5, dyn
	-[0x8000055c]:csrrs a7, fflags, zero
	-[0x80000560]:sd zero, 208(a5)
Current Store : [0x80000564] : sd a7, 216(a5) -- Store: [0x80002418]:0x0000000000000011




Last Coverpoint : ['rd : x25', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000570]:fcvt.lu.s s9, fa3, dyn
	-[0x80000574]:csrrs a7, fflags, zero
	-[0x80000578]:sd s9, 224(a5)
Current Store : [0x8000057c] : sd a7, 232(a5) -- Store: [0x80002428]:0x0000000000000011




Last Coverpoint : ['rd : x17', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.lu.s a7, fa5, dyn
	-[0x80000598]:csrrs s5, fflags, zero
	-[0x8000059c]:sd a7, 0(s3)
Current Store : [0x800005a0] : sd s5, 8(s3) -- Store: [0x80002438]:0x0000000000000011




Last Coverpoint : ['rd : x4', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800005b8]:fcvt.lu.s tp, fa2, dyn
	-[0x800005bc]:csrrs a7, fflags, zero
	-[0x800005c0]:sd tp, 0(a5)
Current Store : [0x800005c4] : sd a7, 8(a5) -- Store: [0x80002448]:0x0000000000000011




Last Coverpoint : ['rd : x13', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005d0]:fcvt.lu.s a3, fs0, dyn
	-[0x800005d4]:csrrs a7, fflags, zero
	-[0x800005d8]:sd a3, 16(a5)
Current Store : [0x800005dc] : sd a7, 24(a5) -- Store: [0x80002458]:0x0000000000000011




Last Coverpoint : ['rd : x31', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800005e8]:fcvt.lu.s t6, ft11, dyn
	-[0x800005ec]:csrrs a7, fflags, zero
	-[0x800005f0]:sd t6, 32(a5)
Current Store : [0x800005f4] : sd a7, 40(a5) -- Store: [0x80002468]:0x0000000000000011




Last Coverpoint : ['rd : x5', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000600]:fcvt.lu.s t0, ft8, dyn
	-[0x80000604]:csrrs a7, fflags, zero
	-[0x80000608]:sd t0, 48(a5)
Current Store : [0x8000060c] : sd a7, 56(a5) -- Store: [0x80002478]:0x0000000000000011




Last Coverpoint : ['rd : x19', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000618]:fcvt.lu.s s3, ft2, dyn
	-[0x8000061c]:csrrs a7, fflags, zero
	-[0x80000620]:sd s3, 64(a5)
Current Store : [0x80000624] : sd a7, 72(a5) -- Store: [0x80002488]:0x0000000000000011




Last Coverpoint : ['rd : x27', 'rs1 : f25', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000630]:fcvt.lu.s s11, fs9, dyn
	-[0x80000634]:csrrs a7, fflags, zero
	-[0x80000638]:sd s11, 80(a5)
Current Store : [0x8000063c] : sd a7, 88(a5) -- Store: [0x80002498]:0x0000000000000011




Last Coverpoint : ['rd : x26', 'rs1 : f7', 'fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000648]:fcvt.lu.s s10, ft7, dyn
	-[0x8000064c]:csrrs a7, fflags, zero
	-[0x80000650]:sd s10, 96(a5)
Current Store : [0x80000654] : sd a7, 104(a5) -- Store: [0x800024a8]:0x0000000000000011




Last Coverpoint : ['rd : x1', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000660]:fcvt.lu.s ra, fa1, dyn
	-[0x80000664]:csrrs a7, fflags, zero
	-[0x80000668]:sd ra, 112(a5)
Current Store : [0x8000066c] : sd a7, 120(a5) -- Store: [0x800024b8]:0x0000000000000011




Last Coverpoint : ['rd : x18', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000678]:fcvt.lu.s s2, fs10, dyn
	-[0x8000067c]:csrrs a7, fflags, zero
	-[0x80000680]:sd s2, 128(a5)
Current Store : [0x80000684] : sd a7, 136(a5) -- Store: [0x800024c8]:0x0000000000000011




Last Coverpoint : ['rd : x21', 'rs1 : f16', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000690]:fcvt.lu.s s5, fa6, dyn
	-[0x80000694]:csrrs a7, fflags, zero
	-[0x80000698]:sd s5, 144(a5)
Current Store : [0x8000069c] : sd a7, 152(a5) -- Store: [0x800024d8]:0x0000000000000011




Last Coverpoint : ['rd : x23', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800006a8]:fcvt.lu.s s7, fs3, dyn
	-[0x800006ac]:csrrs a7, fflags, zero
	-[0x800006b0]:sd s7, 160(a5)
Current Store : [0x800006b4] : sd a7, 168(a5) -- Store: [0x800024e8]:0x0000000000000011




Last Coverpoint : ['rd : x12', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006c0]:fcvt.lu.s a2, ft4, dyn
	-[0x800006c4]:csrrs a7, fflags, zero
	-[0x800006c8]:sd a2, 176(a5)
Current Store : [0x800006cc] : sd a7, 184(a5) -- Store: [0x800024f8]:0x0000000000000011




Last Coverpoint : ['rd : x2', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800006d8]:fcvt.lu.s sp, fs1, dyn
	-[0x800006dc]:csrrs a7, fflags, zero
	-[0x800006e0]:sd sp, 192(a5)
Current Store : [0x800006e4] : sd a7, 200(a5) -- Store: [0x80002508]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800006f0]:fcvt.lu.s t6, ft11, dyn
	-[0x800006f4]:csrrs a7, fflags, zero
	-[0x800006f8]:sd t6, 208(a5)
Current Store : [0x800006fc] : sd a7, 216(a5) -- Store: [0x80002518]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000708]:fcvt.lu.s t6, ft11, dyn
	-[0x8000070c]:csrrs a7, fflags, zero
	-[0x80000710]:sd t6, 224(a5)
Current Store : [0x80000714] : sd a7, 232(a5) -- Store: [0x80002528]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000720]:fcvt.lu.s t6, ft11, dyn
	-[0x80000724]:csrrs a7, fflags, zero
	-[0x80000728]:sd t6, 240(a5)
Current Store : [0x8000072c] : sd a7, 248(a5) -- Store: [0x80002538]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000738]:fcvt.lu.s t6, ft11, dyn
	-[0x8000073c]:csrrs a7, fflags, zero
	-[0x80000740]:sd t6, 256(a5)
Current Store : [0x80000744] : sd a7, 264(a5) -- Store: [0x80002548]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000750]:fcvt.lu.s t6, ft11, dyn
	-[0x80000754]:csrrs a7, fflags, zero
	-[0x80000758]:sd t6, 272(a5)
Current Store : [0x8000075c] : sd a7, 280(a5) -- Store: [0x80002558]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000768]:fcvt.lu.s t6, ft11, dyn
	-[0x8000076c]:csrrs a7, fflags, zero
	-[0x80000770]:sd t6, 288(a5)
Current Store : [0x80000774] : sd a7, 296(a5) -- Store: [0x80002568]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000780]:fcvt.lu.s t6, ft11, dyn
	-[0x80000784]:csrrs a7, fflags, zero
	-[0x80000788]:sd t6, 304(a5)
Current Store : [0x8000078c] : sd a7, 312(a5) -- Store: [0x80002578]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000798]:fcvt.lu.s t6, ft11, dyn
	-[0x8000079c]:csrrs a7, fflags, zero
	-[0x800007a0]:sd t6, 320(a5)
Current Store : [0x800007a4] : sd a7, 328(a5) -- Store: [0x80002588]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007b0]:fcvt.lu.s t6, ft11, dyn
	-[0x800007b4]:csrrs a7, fflags, zero
	-[0x800007b8]:sd t6, 336(a5)
Current Store : [0x800007bc] : sd a7, 344(a5) -- Store: [0x80002598]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800007c8]:fcvt.lu.s t6, ft11, dyn
	-[0x800007cc]:csrrs a7, fflags, zero
	-[0x800007d0]:sd t6, 352(a5)
Current Store : [0x800007d4] : sd a7, 360(a5) -- Store: [0x800025a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800007e0]:fcvt.lu.s t6, ft11, dyn
	-[0x800007e4]:csrrs a7, fflags, zero
	-[0x800007e8]:sd t6, 368(a5)
Current Store : [0x800007ec] : sd a7, 376(a5) -- Store: [0x800025b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007f8]:fcvt.lu.s t6, ft11, dyn
	-[0x800007fc]:csrrs a7, fflags, zero
	-[0x80000800]:sd t6, 384(a5)
Current Store : [0x80000804] : sd a7, 392(a5) -- Store: [0x800025c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000810]:fcvt.lu.s t6, ft11, dyn
	-[0x80000814]:csrrs a7, fflags, zero
	-[0x80000818]:sd t6, 400(a5)
Current Store : [0x8000081c] : sd a7, 408(a5) -- Store: [0x800025d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000828]:fcvt.lu.s t6, ft11, dyn
	-[0x8000082c]:csrrs a7, fflags, zero
	-[0x80000830]:sd t6, 416(a5)
Current Store : [0x80000834] : sd a7, 424(a5) -- Store: [0x800025e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000840]:fcvt.lu.s t6, ft11, dyn
	-[0x80000844]:csrrs a7, fflags, zero
	-[0x80000848]:sd t6, 432(a5)
Current Store : [0x8000084c] : sd a7, 440(a5) -- Store: [0x800025f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000858]:fcvt.lu.s t6, ft11, dyn
	-[0x8000085c]:csrrs a7, fflags, zero
	-[0x80000860]:sd t6, 448(a5)
Current Store : [0x80000864] : sd a7, 456(a5) -- Store: [0x80002608]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000870]:fcvt.lu.s t6, ft11, dyn
	-[0x80000874]:csrrs a7, fflags, zero
	-[0x80000878]:sd t6, 464(a5)
Current Store : [0x8000087c] : sd a7, 472(a5) -- Store: [0x80002618]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000888]:fcvt.lu.s t6, ft11, dyn
	-[0x8000088c]:csrrs a7, fflags, zero
	-[0x80000890]:sd t6, 480(a5)
Current Store : [0x80000894] : sd a7, 488(a5) -- Store: [0x80002628]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008a0]:fcvt.lu.s t6, ft11, dyn
	-[0x800008a4]:csrrs a7, fflags, zero
	-[0x800008a8]:sd t6, 496(a5)
Current Store : [0x800008ac] : sd a7, 504(a5) -- Store: [0x80002638]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800008b8]:fcvt.lu.s t6, ft11, dyn
	-[0x800008bc]:csrrs a7, fflags, zero
	-[0x800008c0]:sd t6, 512(a5)
Current Store : [0x800008c4] : sd a7, 520(a5) -- Store: [0x80002648]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800008d0]:fcvt.lu.s t6, ft11, dyn
	-[0x800008d4]:csrrs a7, fflags, zero
	-[0x800008d8]:sd t6, 528(a5)
Current Store : [0x800008dc] : sd a7, 536(a5) -- Store: [0x80002658]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800008e8]:fcvt.lu.s t6, ft11, dyn
	-[0x800008ec]:csrrs a7, fflags, zero
	-[0x800008f0]:sd t6, 544(a5)
Current Store : [0x800008f4] : sd a7, 552(a5) -- Store: [0x80002668]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000900]:fcvt.lu.s t6, ft11, dyn
	-[0x80000904]:csrrs a7, fflags, zero
	-[0x80000908]:sd t6, 560(a5)
Current Store : [0x8000090c] : sd a7, 568(a5) -- Store: [0x80002678]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000918]:fcvt.lu.s t6, ft11, dyn
	-[0x8000091c]:csrrs a7, fflags, zero
	-[0x80000920]:sd t6, 576(a5)
Current Store : [0x80000924] : sd a7, 584(a5) -- Store: [0x80002688]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000930]:fcvt.lu.s t6, ft11, dyn
	-[0x80000934]:csrrs a7, fflags, zero
	-[0x80000938]:sd t6, 592(a5)
Current Store : [0x8000093c] : sd a7, 600(a5) -- Store: [0x80002698]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000948]:fcvt.lu.s t6, ft11, dyn
	-[0x8000094c]:csrrs a7, fflags, zero
	-[0x80000950]:sd t6, 608(a5)
Current Store : [0x80000954] : sd a7, 616(a5) -- Store: [0x800026a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000960]:fcvt.lu.s t6, ft11, dyn
	-[0x80000964]:csrrs a7, fflags, zero
	-[0x80000968]:sd t6, 624(a5)
Current Store : [0x8000096c] : sd a7, 632(a5) -- Store: [0x800026b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000978]:fcvt.lu.s t6, ft11, dyn
	-[0x8000097c]:csrrs a7, fflags, zero
	-[0x80000980]:sd t6, 640(a5)
Current Store : [0x80000984] : sd a7, 648(a5) -- Store: [0x800026c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000990]:fcvt.lu.s t6, ft11, dyn
	-[0x80000994]:csrrs a7, fflags, zero
	-[0x80000998]:sd t6, 656(a5)
Current Store : [0x8000099c] : sd a7, 664(a5) -- Store: [0x800026d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800009a8]:fcvt.lu.s t6, ft11, dyn
	-[0x800009ac]:csrrs a7, fflags, zero
	-[0x800009b0]:sd t6, 672(a5)
Current Store : [0x800009b4] : sd a7, 680(a5) -- Store: [0x800026e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800009c0]:fcvt.lu.s t6, ft11, dyn
	-[0x800009c4]:csrrs a7, fflags, zero
	-[0x800009c8]:sd t6, 688(a5)
Current Store : [0x800009cc] : sd a7, 696(a5) -- Store: [0x800026f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800009d8]:fcvt.lu.s t6, ft11, dyn
	-[0x800009dc]:csrrs a7, fflags, zero
	-[0x800009e0]:sd t6, 704(a5)
Current Store : [0x800009e4] : sd a7, 712(a5) -- Store: [0x80002708]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800009f0]:fcvt.lu.s t6, ft11, dyn
	-[0x800009f4]:csrrs a7, fflags, zero
	-[0x800009f8]:sd t6, 720(a5)
Current Store : [0x800009fc] : sd a7, 728(a5) -- Store: [0x80002718]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a08]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a0c]:csrrs a7, fflags, zero
	-[0x80000a10]:sd t6, 736(a5)
Current Store : [0x80000a14] : sd a7, 744(a5) -- Store: [0x80002728]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000a20]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a24]:csrrs a7, fflags, zero
	-[0x80000a28]:sd t6, 752(a5)
Current Store : [0x80000a2c] : sd a7, 760(a5) -- Store: [0x80002738]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000a38]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a3c]:csrrs a7, fflags, zero
	-[0x80000a40]:sd t6, 768(a5)
Current Store : [0x80000a44] : sd a7, 776(a5) -- Store: [0x80002748]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a50]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a54]:csrrs a7, fflags, zero
	-[0x80000a58]:sd t6, 784(a5)
Current Store : [0x80000a5c] : sd a7, 792(a5) -- Store: [0x80002758]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000a68]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a6c]:csrrs a7, fflags, zero
	-[0x80000a70]:sd t6, 800(a5)
Current Store : [0x80000a74] : sd a7, 808(a5) -- Store: [0x80002768]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a80]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a84]:csrrs a7, fflags, zero
	-[0x80000a88]:sd t6, 816(a5)
Current Store : [0x80000a8c] : sd a7, 824(a5) -- Store: [0x80002778]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000a98]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a9c]:csrrs a7, fflags, zero
	-[0x80000aa0]:sd t6, 832(a5)
Current Store : [0x80000aa4] : sd a7, 840(a5) -- Store: [0x80002788]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000ab0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000ab4]:csrrs a7, fflags, zero
	-[0x80000ab8]:sd t6, 848(a5)
Current Store : [0x80000abc] : sd a7, 856(a5) -- Store: [0x80002798]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000acc]:csrrs a7, fflags, zero
	-[0x80000ad0]:sd t6, 864(a5)
Current Store : [0x80000ad4] : sd a7, 872(a5) -- Store: [0x800027a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000ae0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000ae4]:csrrs a7, fflags, zero
	-[0x80000ae8]:sd t6, 880(a5)
Current Store : [0x80000aec] : sd a7, 888(a5) -- Store: [0x800027b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000af8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000afc]:csrrs a7, fflags, zero
	-[0x80000b00]:sd t6, 896(a5)
Current Store : [0x80000b04] : sd a7, 904(a5) -- Store: [0x800027c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000b10]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b14]:csrrs a7, fflags, zero
	-[0x80000b18]:sd t6, 912(a5)
Current Store : [0x80000b1c] : sd a7, 920(a5) -- Store: [0x800027d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000b28]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b2c]:csrrs a7, fflags, zero
	-[0x80000b30]:sd t6, 928(a5)
Current Store : [0x80000b34] : sd a7, 936(a5) -- Store: [0x800027e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b40]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b44]:csrrs a7, fflags, zero
	-[0x80000b48]:sd t6, 944(a5)
Current Store : [0x80000b4c] : sd a7, 952(a5) -- Store: [0x800027f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000b58]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b5c]:csrrs a7, fflags, zero
	-[0x80000b60]:sd t6, 960(a5)
Current Store : [0x80000b64] : sd a7, 968(a5) -- Store: [0x80002808]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b70]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b74]:csrrs a7, fflags, zero
	-[0x80000b78]:sd t6, 976(a5)
Current Store : [0x80000b7c] : sd a7, 984(a5) -- Store: [0x80002818]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000b88]:fcvt.lu.s t6, ft11, dyn
	-[0x80000b8c]:csrrs a7, fflags, zero
	-[0x80000b90]:sd t6, 992(a5)
Current Store : [0x80000b94] : sd a7, 1000(a5) -- Store: [0x80002828]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000ba0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000ba4]:csrrs a7, fflags, zero
	-[0x80000ba8]:sd t6, 1008(a5)
Current Store : [0x80000bac] : sd a7, 1016(a5) -- Store: [0x80002838]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000bb8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000bbc]:csrrs a7, fflags, zero
	-[0x80000bc0]:sd t6, 1024(a5)
Current Store : [0x80000bc4] : sd a7, 1032(a5) -- Store: [0x80002848]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000bd0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000bd4]:csrrs a7, fflags, zero
	-[0x80000bd8]:sd t6, 1040(a5)
Current Store : [0x80000bdc] : sd a7, 1048(a5) -- Store: [0x80002858]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000be8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000bec]:csrrs a7, fflags, zero
	-[0x80000bf0]:sd t6, 1056(a5)
Current Store : [0x80000bf4] : sd a7, 1064(a5) -- Store: [0x80002868]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000c00]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c04]:csrrs a7, fflags, zero
	-[0x80000c08]:sd t6, 1072(a5)
Current Store : [0x80000c0c] : sd a7, 1080(a5) -- Store: [0x80002878]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000c18]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c1c]:csrrs a7, fflags, zero
	-[0x80000c20]:sd t6, 1088(a5)
Current Store : [0x80000c24] : sd a7, 1096(a5) -- Store: [0x80002888]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c30]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c34]:csrrs a7, fflags, zero
	-[0x80000c38]:sd t6, 1104(a5)
Current Store : [0x80000c3c] : sd a7, 1112(a5) -- Store: [0x80002898]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000c48]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c4c]:csrrs a7, fflags, zero
	-[0x80000c50]:sd t6, 1120(a5)
Current Store : [0x80000c54] : sd a7, 1128(a5) -- Store: [0x800028a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c60]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c64]:csrrs a7, fflags, zero
	-[0x80000c68]:sd t6, 1136(a5)
Current Store : [0x80000c6c] : sd a7, 1144(a5) -- Store: [0x800028b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000c78]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c7c]:csrrs a7, fflags, zero
	-[0x80000c80]:sd t6, 1152(a5)
Current Store : [0x80000c84] : sd a7, 1160(a5) -- Store: [0x800028c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000c90]:fcvt.lu.s t6, ft11, dyn
	-[0x80000c94]:csrrs a7, fflags, zero
	-[0x80000c98]:sd t6, 1168(a5)
Current Store : [0x80000c9c] : sd a7, 1176(a5) -- Store: [0x800028d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ca8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000cac]:csrrs a7, fflags, zero
	-[0x80000cb0]:sd t6, 1184(a5)
Current Store : [0x80000cb4] : sd a7, 1192(a5) -- Store: [0x800028e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000cc4]:csrrs a7, fflags, zero
	-[0x80000cc8]:sd t6, 1200(a5)
Current Store : [0x80000ccc] : sd a7, 1208(a5) -- Store: [0x800028f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000cd8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000cdc]:csrrs a7, fflags, zero
	-[0x80000ce0]:sd t6, 1216(a5)
Current Store : [0x80000ce4] : sd a7, 1224(a5) -- Store: [0x80002908]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000cf0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000cf4]:csrrs a7, fflags, zero
	-[0x80000cf8]:sd t6, 1232(a5)
Current Store : [0x80000cfc] : sd a7, 1240(a5) -- Store: [0x80002918]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000d08]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d0c]:csrrs a7, fflags, zero
	-[0x80000d10]:sd t6, 1248(a5)
Current Store : [0x80000d14] : sd a7, 1256(a5) -- Store: [0x80002928]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d20]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d24]:csrrs a7, fflags, zero
	-[0x80000d28]:sd t6, 1264(a5)
Current Store : [0x80000d2c] : sd a7, 1272(a5) -- Store: [0x80002938]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000d38]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d3c]:csrrs a7, fflags, zero
	-[0x80000d40]:sd t6, 1280(a5)
Current Store : [0x80000d44] : sd a7, 1288(a5) -- Store: [0x80002948]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d50]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d54]:csrrs a7, fflags, zero
	-[0x80000d58]:sd t6, 1296(a5)
Current Store : [0x80000d5c] : sd a7, 1304(a5) -- Store: [0x80002958]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000d68]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d6c]:csrrs a7, fflags, zero
	-[0x80000d70]:sd t6, 1312(a5)
Current Store : [0x80000d74] : sd a7, 1320(a5) -- Store: [0x80002968]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000d80]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d84]:csrrs a7, fflags, zero
	-[0x80000d88]:sd t6, 1328(a5)
Current Store : [0x80000d8c] : sd a7, 1336(a5) -- Store: [0x80002978]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d98]:fcvt.lu.s t6, ft11, dyn
	-[0x80000d9c]:csrrs a7, fflags, zero
	-[0x80000da0]:sd t6, 1344(a5)
Current Store : [0x80000da4] : sd a7, 1352(a5) -- Store: [0x80002988]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000db0]:fcvt.lu.s t6, ft11, dyn
	-[0x80000db4]:csrrs a7, fflags, zero
	-[0x80000db8]:sd t6, 1360(a5)
Current Store : [0x80000dbc] : sd a7, 1368(a5) -- Store: [0x80002998]:0x0000000000000011




Last Coverpoint : ['opcode : fcvt.lu.s', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000dc8]:fcvt.lu.s t6, ft11, dyn
	-[0x80000dcc]:csrrs a7, fflags, zero
	-[0x80000dd0]:sd t6, 1376(a5)
Current Store : [0x80000dd4] : sd a7, 1384(a5) -- Store: [0x800029a8]:0x0000000000000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                           coverpoints                                                            |                                                        code                                                        |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x0000000000000000|- opcode : fcvt.lu.s<br> - rd : x15<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.lu.s a5, ft0, dyn<br> [0x800003b8]:csrrs s5, fflags, zero<br> [0x800003bc]:sd a5, 0(s3)<br>       |
|   2|[0x80002320]<br>0x0000000000000000|- rd : x10<br> - rs1 : f18<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat<br>                         |[0x800003d8]:fcvt.lu.s a0, fs2, dyn<br> [0x800003dc]:csrrs a7, fflags, zero<br> [0x800003e0]:sd a0, 0(a5)<br>       |
|   3|[0x80002330]<br>0x0000000000000000|- rd : x16<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat<br>                         |[0x800003fc]:fcvt.lu.s a6, fs7, dyn<br> [0x80000400]:csrrs s5, fflags, zero<br> [0x80000404]:sd a6, 0(s3)<br>       |
|   4|[0x80002340]<br>0x0000000000000000|- rd : x28<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat<br>                         |[0x80000420]:fcvt.lu.s t3, ft10, dyn<br> [0x80000424]:csrrs a7, fflags, zero<br> [0x80000428]:sd t3, 0(a5)<br>      |
|   5|[0x80002350]<br>0x0000000000000000|- rd : x20<br> - rs1 : f3<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat<br>                          |[0x80000438]:fcvt.lu.s s4, ft3, dyn<br> [0x8000043c]:csrrs a7, fflags, zero<br> [0x80000440]:sd s4, 16(a5)<br>      |
|   6|[0x80002360]<br>0x0000000000000000|- rd : x8<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat<br>                          |[0x80000450]:fcvt.lu.s fp, fs6, dyn<br> [0x80000454]:csrrs a7, fflags, zero<br> [0x80000458]:sd fp, 32(a5)<br>      |
|   7|[0x80002370]<br>0x0000000000000001|- rd : x7<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat<br>                           |[0x80000468]:fcvt.lu.s t2, ft1, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:sd t2, 48(a5)<br>      |
|   8|[0x80002380]<br>0x0000000000000001|- rd : x6<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat<br>                          |[0x80000480]:fcvt.lu.s t1, fs4, dyn<br> [0x80000484]:csrrs a7, fflags, zero<br> [0x80000488]:sd t1, 64(a5)<br>      |
|   9|[0x80002390]<br>0x0000000000000000|- rd : x11<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat<br>                          |[0x80000498]:fcvt.lu.s a1, ft6, dyn<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:sd a1, 80(a5)<br>      |
|  10|[0x800023a0]<br>0x0000000000000000|- rd : x29<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat<br>                         |[0x800004b0]:fcvt.lu.s t4, fs11, dyn<br> [0x800004b4]:csrrs a7, fflags, zero<br> [0x800004b8]:sd t4, 96(a5)<br>     |
|  11|[0x800023b0]<br>0x0000000000000001|- rd : x9<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat<br>                          |[0x800004c8]:fcvt.lu.s s1, fa0, dyn<br> [0x800004cc]:csrrs a7, fflags, zero<br> [0x800004d0]:sd s1, 112(a5)<br>     |
|  12|[0x800023c0]<br>0x0000000000000000|- rd : x14<br> - rs1 : f14<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat<br>                         |[0x800004e0]:fcvt.lu.s a4, fa4, dyn<br> [0x800004e4]:csrrs a7, fflags, zero<br> [0x800004e8]:sd a4, 128(a5)<br>     |
|  13|[0x800023d0]<br>0x0000000000000000|- rd : x30<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat<br>                         |[0x800004f8]:fcvt.lu.s t5, fs8, dyn<br> [0x800004fc]:csrrs a7, fflags, zero<br> [0x80000500]:sd t5, 144(a5)<br>     |
|  14|[0x800023e0]<br>0x0000000000000000|- rd : x22<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat<br>                         |[0x80000510]:fcvt.lu.s s6, fa7, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:sd s6, 160(a5)<br>     |
|  15|[0x800023f0]<br>0x0000000000000000|- rd : x3<br> - rs1 : f21<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat<br>                          |[0x80000528]:fcvt.lu.s gp, fs5, dyn<br> [0x8000052c]:csrrs a7, fflags, zero<br> [0x80000530]:sd gp, 176(a5)<br>     |
|  16|[0x80002400]<br>0x0000000000000000|- rd : x24<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat<br>                         |[0x80000540]:fcvt.lu.s s8, ft9, dyn<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:sd s8, 192(a5)<br>     |
|  17|[0x80002410]<br>0x0000000000000000|- rd : x0<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat<br>                           |[0x80000558]:fcvt.lu.s zero, ft5, dyn<br> [0x8000055c]:csrrs a7, fflags, zero<br> [0x80000560]:sd zero, 208(a5)<br> |
|  18|[0x80002420]<br>0x0000000000000001|- rd : x25<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat<br>                         |[0x80000570]:fcvt.lu.s s9, fa3, dyn<br> [0x80000574]:csrrs a7, fflags, zero<br> [0x80000578]:sd s9, 224(a5)<br>     |
|  19|[0x80002430]<br>0x0000000000000000|- rd : x17<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat<br>                         |[0x80000594]:fcvt.lu.s a7, fa5, dyn<br> [0x80000598]:csrrs s5, fflags, zero<br> [0x8000059c]:sd a7, 0(s3)<br>       |
|  20|[0x80002440]<br>0x0000000000000000|- rd : x4<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat<br>                          |[0x800005b8]:fcvt.lu.s tp, fa2, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:sd tp, 0(a5)<br>       |
|  21|[0x80002450]<br>0x0000000000000000|- rd : x13<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat<br>                          |[0x800005d0]:fcvt.lu.s a3, fs0, dyn<br> [0x800005d4]:csrrs a7, fflags, zero<br> [0x800005d8]:sd a3, 16(a5)<br>      |
|  22|[0x80002460]<br>0x0000000000000000|- rd : x31<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat<br>                         |[0x800005e8]:fcvt.lu.s t6, ft11, dyn<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:sd t6, 32(a5)<br>     |
|  23|[0x80002470]<br>0x0000000000000000|- rd : x5<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat<br>                          |[0x80000600]:fcvt.lu.s t0, ft8, dyn<br> [0x80000604]:csrrs a7, fflags, zero<br> [0x80000608]:sd t0, 48(a5)<br>      |
|  24|[0x80002480]<br>0x0000000000000000|- rd : x19<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat<br>                          |[0x80000618]:fcvt.lu.s s3, ft2, dyn<br> [0x8000061c]:csrrs a7, fflags, zero<br> [0x80000620]:sd s3, 64(a5)<br>      |
|  25|[0x80002490]<br>0x0000000000000000|- rd : x27<br> - rs1 : f25<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat<br>                         |[0x80000630]:fcvt.lu.s s11, fs9, dyn<br> [0x80000634]:csrrs a7, fflags, zero<br> [0x80000638]:sd s11, 80(a5)<br>    |
|  26|[0x800024a0]<br>0x0000000000000000|- rd : x26<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                          |[0x80000648]:fcvt.lu.s s10, ft7, dyn<br> [0x8000064c]:csrrs a7, fflags, zero<br> [0x80000650]:sd s10, 96(a5)<br>    |
|  27|[0x800024b0]<br>0x0000000000000000|- rd : x1<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 4  #nosat<br>                          |[0x80000660]:fcvt.lu.s ra, fa1, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:sd ra, 112(a5)<br>     |
|  28|[0x800024c0]<br>0x0000000000000000|- rd : x18<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 3  #nosat<br>                         |[0x80000678]:fcvt.lu.s s2, fs10, dyn<br> [0x8000067c]:csrrs a7, fflags, zero<br> [0x80000680]:sd s2, 128(a5)<br>    |
|  29|[0x800024d0]<br>0x0000000000000000|- rd : x21<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 2  #nosat<br>                         |[0x80000690]:fcvt.lu.s s5, fa6, dyn<br> [0x80000694]:csrrs a7, fflags, zero<br> [0x80000698]:sd s5, 144(a5)<br>     |
|  30|[0x800024e0]<br>0x0000000000000000|- rd : x23<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 1  #nosat<br>                         |[0x800006a8]:fcvt.lu.s s7, fs3, dyn<br> [0x800006ac]:csrrs a7, fflags, zero<br> [0x800006b0]:sd s7, 160(a5)<br>     |
|  31|[0x800024f0]<br>0x0000000000000000|- rd : x12<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and rm_val == 0  #nosat<br>                          |[0x800006c0]:fcvt.lu.s a2, ft4, dyn<br> [0x800006c4]:csrrs a7, fflags, zero<br> [0x800006c8]:sd a2, 176(a5)<br>     |
|  32|[0x80002500]<br>0x0000000000000000|- rd : x2<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 4  #nosat<br>                           |[0x800006d8]:fcvt.lu.s sp, fs1, dyn<br> [0x800006dc]:csrrs a7, fflags, zero<br> [0x800006e0]:sd sp, 192(a5)<br>     |
|  33|[0x80002510]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 3  #nosat<br>                                                        |[0x800006f0]:fcvt.lu.s t6, ft11, dyn<br> [0x800006f4]:csrrs a7, fflags, zero<br> [0x800006f8]:sd t6, 208(a5)<br>    |
|  34|[0x80002520]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 2  #nosat<br>                                                        |[0x80000708]:fcvt.lu.s t6, ft11, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:sd t6, 224(a5)<br>    |
|  35|[0x80002530]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 1  #nosat<br>                                                        |[0x80000720]:fcvt.lu.s t6, ft11, dyn<br> [0x80000724]:csrrs a7, fflags, zero<br> [0x80000728]:sd t6, 240(a5)<br>    |
|  36|[0x80002540]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and rm_val == 0  #nosat<br>                                                        |[0x80000738]:fcvt.lu.s t6, ft11, dyn<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:sd t6, 256(a5)<br>    |
|  37|[0x80002550]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 4  #nosat<br>                                                        |[0x80000750]:fcvt.lu.s t6, ft11, dyn<br> [0x80000754]:csrrs a7, fflags, zero<br> [0x80000758]:sd t6, 272(a5)<br>    |
|  38|[0x80002560]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 3  #nosat<br>                                                        |[0x80000768]:fcvt.lu.s t6, ft11, dyn<br> [0x8000076c]:csrrs a7, fflags, zero<br> [0x80000770]:sd t6, 288(a5)<br>    |
|  39|[0x80002570]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 2  #nosat<br>                                                        |[0x80000780]:fcvt.lu.s t6, ft11, dyn<br> [0x80000784]:csrrs a7, fflags, zero<br> [0x80000788]:sd t6, 304(a5)<br>    |
|  40|[0x80002580]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 1  #nosat<br>                                                        |[0x80000798]:fcvt.lu.s t6, ft11, dyn<br> [0x8000079c]:csrrs a7, fflags, zero<br> [0x800007a0]:sd t6, 320(a5)<br>    |
|  41|[0x80002590]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and rm_val == 0  #nosat<br>                                                        |[0x800007b0]:fcvt.lu.s t6, ft11, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:sd t6, 336(a5)<br>    |
|  42|[0x800025a0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat<br>                                                        |[0x800007c8]:fcvt.lu.s t6, ft11, dyn<br> [0x800007cc]:csrrs a7, fflags, zero<br> [0x800007d0]:sd t6, 352(a5)<br>    |
|  43|[0x800025b0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat<br>                                                        |[0x800007e0]:fcvt.lu.s t6, ft11, dyn<br> [0x800007e4]:csrrs a7, fflags, zero<br> [0x800007e8]:sd t6, 368(a5)<br>    |
|  44|[0x800025c0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat<br>                                                        |[0x800007f8]:fcvt.lu.s t6, ft11, dyn<br> [0x800007fc]:csrrs a7, fflags, zero<br> [0x80000800]:sd t6, 384(a5)<br>    |
|  45|[0x800025d0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat<br>                                                        |[0x80000810]:fcvt.lu.s t6, ft11, dyn<br> [0x80000814]:csrrs a7, fflags, zero<br> [0x80000818]:sd t6, 400(a5)<br>    |
|  46|[0x800025e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat<br>                                                        |[0x80000828]:fcvt.lu.s t6, ft11, dyn<br> [0x8000082c]:csrrs a7, fflags, zero<br> [0x80000830]:sd t6, 416(a5)<br>    |
|  47|[0x800025f0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat<br>                                                        |[0x80000840]:fcvt.lu.s t6, ft11, dyn<br> [0x80000844]:csrrs a7, fflags, zero<br> [0x80000848]:sd t6, 432(a5)<br>    |
|  48|[0x80002600]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat<br>                                                        |[0x80000858]:fcvt.lu.s t6, ft11, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:sd t6, 448(a5)<br>    |
|  49|[0x80002610]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat<br>                                                        |[0x80000870]:fcvt.lu.s t6, ft11, dyn<br> [0x80000874]:csrrs a7, fflags, zero<br> [0x80000878]:sd t6, 464(a5)<br>    |
|  50|[0x80002620]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat<br>                                                        |[0x80000888]:fcvt.lu.s t6, ft11, dyn<br> [0x8000088c]:csrrs a7, fflags, zero<br> [0x80000890]:sd t6, 480(a5)<br>    |
|  51|[0x80002630]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat<br>                                                        |[0x800008a0]:fcvt.lu.s t6, ft11, dyn<br> [0x800008a4]:csrrs a7, fflags, zero<br> [0x800008a8]:sd t6, 496(a5)<br>    |
|  52|[0x80002640]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat<br>                                                        |[0x800008b8]:fcvt.lu.s t6, ft11, dyn<br> [0x800008bc]:csrrs a7, fflags, zero<br> [0x800008c0]:sd t6, 512(a5)<br>    |
|  53|[0x80002650]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat<br>                                                        |[0x800008d0]:fcvt.lu.s t6, ft11, dyn<br> [0x800008d4]:csrrs a7, fflags, zero<br> [0x800008d8]:sd t6, 528(a5)<br>    |
|  54|[0x80002660]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat<br>                                                        |[0x800008e8]:fcvt.lu.s t6, ft11, dyn<br> [0x800008ec]:csrrs a7, fflags, zero<br> [0x800008f0]:sd t6, 544(a5)<br>    |
|  55|[0x80002670]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat<br>                                                        |[0x80000900]:fcvt.lu.s t6, ft11, dyn<br> [0x80000904]:csrrs a7, fflags, zero<br> [0x80000908]:sd t6, 560(a5)<br>    |
|  56|[0x80002680]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 0  #nosat<br>                                                        |[0x80000918]:fcvt.lu.s t6, ft11, dyn<br> [0x8000091c]:csrrs a7, fflags, zero<br> [0x80000920]:sd t6, 576(a5)<br>    |
|  57|[0x80002690]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat<br>                                                        |[0x80000930]:fcvt.lu.s t6, ft11, dyn<br> [0x80000934]:csrrs a7, fflags, zero<br> [0x80000938]:sd t6, 592(a5)<br>    |
|  58|[0x800026a0]<br>0x0000000000000002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat<br>                                                        |[0x80000948]:fcvt.lu.s t6, ft11, dyn<br> [0x8000094c]:csrrs a7, fflags, zero<br> [0x80000950]:sd t6, 608(a5)<br>    |
|  59|[0x800026b0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat<br>                                                        |[0x80000960]:fcvt.lu.s t6, ft11, dyn<br> [0x80000964]:csrrs a7, fflags, zero<br> [0x80000968]:sd t6, 624(a5)<br>    |
|  60|[0x800026c0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat<br>                                                        |[0x80000978]:fcvt.lu.s t6, ft11, dyn<br> [0x8000097c]:csrrs a7, fflags, zero<br> [0x80000980]:sd t6, 640(a5)<br>    |
|  61|[0x800026d0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat<br>                                                        |[0x80000990]:fcvt.lu.s t6, ft11, dyn<br> [0x80000994]:csrrs a7, fflags, zero<br> [0x80000998]:sd t6, 656(a5)<br>    |
|  62|[0x800026e0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 4  #nosat<br>                                                        |[0x800009a8]:fcvt.lu.s t6, ft11, dyn<br> [0x800009ac]:csrrs a7, fflags, zero<br> [0x800009b0]:sd t6, 672(a5)<br>    |
|  63|[0x800026f0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 3  #nosat<br>                                                        |[0x800009c0]:fcvt.lu.s t6, ft11, dyn<br> [0x800009c4]:csrrs a7, fflags, zero<br> [0x800009c8]:sd t6, 688(a5)<br>    |
|  64|[0x80002700]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 2  #nosat<br>                                                        |[0x800009d8]:fcvt.lu.s t6, ft11, dyn<br> [0x800009dc]:csrrs a7, fflags, zero<br> [0x800009e0]:sd t6, 704(a5)<br>    |
|  65|[0x80002710]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 1  #nosat<br>                                                        |[0x800009f0]:fcvt.lu.s t6, ft11, dyn<br> [0x800009f4]:csrrs a7, fflags, zero<br> [0x800009f8]:sd t6, 720(a5)<br>    |
|  66|[0x80002720]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                                        |[0x80000a08]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a0c]:csrrs a7, fflags, zero<br> [0x80000a10]:sd t6, 736(a5)<br>    |
|  67|[0x80002730]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 4  #nosat<br>                                                        |[0x80000a20]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a24]:csrrs a7, fflags, zero<br> [0x80000a28]:sd t6, 752(a5)<br>    |
|  68|[0x80002740]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 3  #nosat<br>                                                        |[0x80000a38]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a3c]:csrrs a7, fflags, zero<br> [0x80000a40]:sd t6, 768(a5)<br>    |
|  69|[0x80002750]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 2  #nosat<br>                                                        |[0x80000a50]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a54]:csrrs a7, fflags, zero<br> [0x80000a58]:sd t6, 784(a5)<br>    |
|  70|[0x80002760]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 1  #nosat<br>                                                        |[0x80000a68]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a6c]:csrrs a7, fflags, zero<br> [0x80000a70]:sd t6, 800(a5)<br>    |
|  71|[0x80002770]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and rm_val == 0  #nosat<br>                                                        |[0x80000a80]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a84]:csrrs a7, fflags, zero<br> [0x80000a88]:sd t6, 816(a5)<br>    |
|  72|[0x80002780]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 4  #nosat<br>                                                        |[0x80000a98]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a9c]:csrrs a7, fflags, zero<br> [0x80000aa0]:sd t6, 832(a5)<br>    |
|  73|[0x80002790]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 3  #nosat<br>                                                        |[0x80000ab0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000ab4]:csrrs a7, fflags, zero<br> [0x80000ab8]:sd t6, 848(a5)<br>    |
|  74|[0x800027a0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 2  #nosat<br>                                                        |[0x80000ac8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000acc]:csrrs a7, fflags, zero<br> [0x80000ad0]:sd t6, 864(a5)<br>    |
|  75|[0x800027b0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 1  #nosat<br>                                                        |[0x80000ae0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000ae4]:csrrs a7, fflags, zero<br> [0x80000ae8]:sd t6, 880(a5)<br>    |
|  76|[0x800027c0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and rm_val == 0  #nosat<br>                                                        |[0x80000af8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000afc]:csrrs a7, fflags, zero<br> [0x80000b00]:sd t6, 896(a5)<br>    |
|  77|[0x800027d0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 4  #nosat<br>                                                        |[0x80000b10]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b14]:csrrs a7, fflags, zero<br> [0x80000b18]:sd t6, 912(a5)<br>    |
|  78|[0x800027e0]<br>0x0000000000000002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 3  #nosat<br>                                                        |[0x80000b28]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b2c]:csrrs a7, fflags, zero<br> [0x80000b30]:sd t6, 928(a5)<br>    |
|  79|[0x800027f0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 2  #nosat<br>                                                        |[0x80000b40]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b44]:csrrs a7, fflags, zero<br> [0x80000b48]:sd t6, 944(a5)<br>    |
|  80|[0x80002800]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 1  #nosat<br>                                                        |[0x80000b58]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b5c]:csrrs a7, fflags, zero<br> [0x80000b60]:sd t6, 960(a5)<br>    |
|  81|[0x80002810]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and rm_val == 0  #nosat<br>                                                        |[0x80000b70]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b74]:csrrs a7, fflags, zero<br> [0x80000b78]:sd t6, 976(a5)<br>    |
|  82|[0x80002820]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 4  #nosat<br>                                                        |[0x80000b88]:fcvt.lu.s t6, ft11, dyn<br> [0x80000b8c]:csrrs a7, fflags, zero<br> [0x80000b90]:sd t6, 992(a5)<br>    |
|  83|[0x80002830]<br>0x0000000000000002|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 3  #nosat<br>                                                        |[0x80000ba0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000ba4]:csrrs a7, fflags, zero<br> [0x80000ba8]:sd t6, 1008(a5)<br>   |
|  84|[0x80002840]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 2  #nosat<br>                                                        |[0x80000bb8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000bbc]:csrrs a7, fflags, zero<br> [0x80000bc0]:sd t6, 1024(a5)<br>   |
|  85|[0x80002850]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 1  #nosat<br>                                                        |[0x80000bd0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000bd4]:csrrs a7, fflags, zero<br> [0x80000bd8]:sd t6, 1040(a5)<br>   |
|  86|[0x80002860]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and rm_val == 0  #nosat<br>                                                        |[0x80000be8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:sd t6, 1056(a5)<br>   |
|  87|[0x80002870]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat<br>                                                        |[0x80000c00]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c04]:csrrs a7, fflags, zero<br> [0x80000c08]:sd t6, 1072(a5)<br>   |
|  88|[0x80002880]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat<br>                                                        |[0x80000c18]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c1c]:csrrs a7, fflags, zero<br> [0x80000c20]:sd t6, 1088(a5)<br>   |
|  89|[0x80002890]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat<br>                                                        |[0x80000c30]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c34]:csrrs a7, fflags, zero<br> [0x80000c38]:sd t6, 1104(a5)<br>   |
|  90|[0x800028a0]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat<br>                                                        |[0x80000c48]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:sd t6, 1120(a5)<br>   |
|  91|[0x800028b0]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat<br>                                                        |[0x80000c60]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c64]:csrrs a7, fflags, zero<br> [0x80000c68]:sd t6, 1136(a5)<br>   |
|  92|[0x800028c0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 4  #nosat<br>                                                        |[0x80000c78]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c7c]:csrrs a7, fflags, zero<br> [0x80000c80]:sd t6, 1152(a5)<br>   |
|  93|[0x800028d0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 3  #nosat<br>                                                        |[0x80000c90]:fcvt.lu.s t6, ft11, dyn<br> [0x80000c94]:csrrs a7, fflags, zero<br> [0x80000c98]:sd t6, 1168(a5)<br>   |
|  94|[0x800028e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 2  #nosat<br>                                                        |[0x80000ca8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:sd t6, 1184(a5)<br>   |
|  95|[0x800028f0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 1  #nosat<br>                                                        |[0x80000cc0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000cc4]:csrrs a7, fflags, zero<br> [0x80000cc8]:sd t6, 1200(a5)<br>   |
|  96|[0x80002900]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and rm_val == 0  #nosat<br>                                                        |[0x80000cd8]:fcvt.lu.s t6, ft11, dyn<br> [0x80000cdc]:csrrs a7, fflags, zero<br> [0x80000ce0]:sd t6, 1216(a5)<br>   |
|  97|[0x80002910]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 4  #nosat<br>                                                        |[0x80000cf0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000cf4]:csrrs a7, fflags, zero<br> [0x80000cf8]:sd t6, 1232(a5)<br>   |
|  98|[0x80002920]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 3  #nosat<br>                                                        |[0x80000d08]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:sd t6, 1248(a5)<br>   |
|  99|[0x80002930]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 2  #nosat<br>                                                        |[0x80000d20]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d24]:csrrs a7, fflags, zero<br> [0x80000d28]:sd t6, 1264(a5)<br>   |
| 100|[0x80002940]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 1  #nosat<br>                                                        |[0x80000d38]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d3c]:csrrs a7, fflags, zero<br> [0x80000d40]:sd t6, 1280(a5)<br>   |
| 101|[0x80002950]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and rm_val == 0  #nosat<br>                                                        |[0x80000d50]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d54]:csrrs a7, fflags, zero<br> [0x80000d58]:sd t6, 1296(a5)<br>   |
| 102|[0x80002960]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 4  #nosat<br>                                                        |[0x80000d68]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:sd t6, 1312(a5)<br>   |
| 103|[0x80002970]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 3  #nosat<br>                                                        |[0x80000d80]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d84]:csrrs a7, fflags, zero<br> [0x80000d88]:sd t6, 1328(a5)<br>   |
| 104|[0x80002980]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 2  #nosat<br>                                                        |[0x80000d98]:fcvt.lu.s t6, ft11, dyn<br> [0x80000d9c]:csrrs a7, fflags, zero<br> [0x80000da0]:sd t6, 1344(a5)<br>   |
| 105|[0x80002990]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and rm_val == 1  #nosat<br>                                                        |[0x80000db0]:fcvt.lu.s t6, ft11, dyn<br> [0x80000db4]:csrrs a7, fflags, zero<br> [0x80000db8]:sd t6, 1360(a5)<br>   |
