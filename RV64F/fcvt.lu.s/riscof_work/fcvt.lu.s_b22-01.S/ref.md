
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000ad0')]      |
| SIG_REGION                | [('0x80002310', '0x800027a0', '146 dwords')]      |
| COV_LABELS                | fcvt.lu.s_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fcvt.lu.s/riscof_work/fcvt.lu.s_b22-01.S/ref.S    |
| Total Number of coverpoints| 141     |
| Total Coverpoints Hit     | 137      |
| Total Signature Updates   | 146      |
| STAT1                     | 72      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 73     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000abc]:fcvt.lu.s t6, ft11, dyn
      [0x80000ac0]:csrrs a7, fflags, zero
      [0x80000ac4]:sd t6, 976(a5)
 -- Signature Address: 0x80002790 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.lu.s
      - rd : x31
      - rs1 : f31
      - fs1 == 1 and fe1 == 0xab and fm1 == 0x444931 and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.lu.s', 'rd : x1', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.lu.s ra, ft6, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd ra, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002318]:0x0000000000000001




Last Coverpoint : ['rd : x15', 'rs1 : f21', 'fs1 == 0 and fe1 == 0xd8 and fm1 == 0x62ecba and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d8]:fcvt.lu.s a5, fs5, dyn
	-[0x800003dc]:csrrs s5, fflags, zero
	-[0x800003e0]:sd a5, 0(s3)
Current Store : [0x800003e4] : sd s5, 8(s3) -- Store: [0x80002328]:0x0000000000000011




Last Coverpoint : ['rd : x3', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x16 and fm1 == 0x63857e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.lu.s gp, fa1, dyn
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd gp, 0(a5)
Current Store : [0x80000408] : sd a7, 8(a5) -- Store: [0x80002338]:0x0000000000000011




Last Coverpoint : ['rd : x27', 'rs1 : f5', 'fs1 == 0 and fe1 == 0xc1 and fm1 == 0x69185a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000414]:fcvt.lu.s s11, ft5, dyn
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd s11, 16(a5)
Current Store : [0x80000420] : sd a7, 24(a5) -- Store: [0x80002348]:0x0000000000000011




Last Coverpoint : ['rd : x2', 'rs1 : f8', 'fs1 == 1 and fe1 == 0xc0 and fm1 == 0x53096a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.lu.s sp, fs0, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd sp, 32(a5)
Current Store : [0x80000438] : sd a7, 40(a5) -- Store: [0x80002358]:0x0000000000000011




Last Coverpoint : ['rd : x4', 'rs1 : f2', 'fs1 == 1 and fe1 == 0xbf and fm1 == 0x151cae and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.lu.s tp, ft2, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd tp, 48(a5)
Current Store : [0x80000450] : sd a7, 56(a5) -- Store: [0x80002368]:0x0000000000000011




Last Coverpoint : ['rd : x29', 'rs1 : f13', 'fs1 == 0 and fe1 == 0xbe and fm1 == 0x3a5585 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.lu.s t4, fa3, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd t4, 64(a5)
Current Store : [0x80000468] : sd a7, 72(a5) -- Store: [0x80002378]:0x0000000000000011




Last Coverpoint : ['rd : x16', 'rs1 : f7', 'fs1 == 0 and fe1 == 0xbd and fm1 == 0x0f4b33 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000480]:fcvt.lu.s a6, ft7, dyn
	-[0x80000484]:csrrs s5, fflags, zero
	-[0x80000488]:sd a6, 0(s3)
Current Store : [0x8000048c] : sd s5, 8(s3) -- Store: [0x80002388]:0x0000000000000011




Last Coverpoint : ['rd : x18', 'rs1 : f15', 'fs1 == 1 and fe1 == 0xbc and fm1 == 0x16d7ca and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fcvt.lu.s s2, fa5, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s2, 0(a5)
Current Store : [0x800004b0] : sd a7, 8(a5) -- Store: [0x80002398]:0x0000000000000011




Last Coverpoint : ['rd : x28', 'rs1 : f17', 'fs1 == 1 and fe1 == 0xbb and fm1 == 0x4f0223 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fcvt.lu.s t3, fa7, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd t3, 16(a5)
Current Store : [0x800004c8] : sd a7, 24(a5) -- Store: [0x800023a8]:0x0000000000000011




Last Coverpoint : ['rd : x17', 'rs1 : f25', 'fs1 == 0 and fe1 == 0xba and fm1 == 0x23297b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004e0]:fcvt.lu.s a7, fs9, dyn
	-[0x800004e4]:csrrs s5, fflags, zero
	-[0x800004e8]:sd a7, 0(s3)
Current Store : [0x800004ec] : sd s5, 8(s3) -- Store: [0x800023b8]:0x0000000000000011




Last Coverpoint : ['rd : x13', 'rs1 : f1', 'fs1 == 1 and fe1 == 0xb9 and fm1 == 0x1f6bf3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000504]:fcvt.lu.s a3, ft1, dyn
	-[0x80000508]:csrrs a7, fflags, zero
	-[0x8000050c]:sd a3, 0(a5)
Current Store : [0x80000510] : sd a7, 8(a5) -- Store: [0x800023c8]:0x0000000000000011




Last Coverpoint : ['rd : x9', 'rs1 : f3', 'fs1 == 0 and fe1 == 0xb8 and fm1 == 0x350099 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fcvt.lu.s s1, ft3, dyn
	-[0x80000520]:csrrs a7, fflags, zero
	-[0x80000524]:sd s1, 16(a5)
Current Store : [0x80000528] : sd a7, 24(a5) -- Store: [0x800023d8]:0x0000000000000011




Last Coverpoint : ['rd : x19', 'rs1 : f16', 'fs1 == 1 and fe1 == 0xb7 and fm1 == 0x377421 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000534]:fcvt.lu.s s3, fa6, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd s3, 32(a5)
Current Store : [0x80000540] : sd a7, 40(a5) -- Store: [0x800023e8]:0x0000000000000011




Last Coverpoint : ['rd : x23', 'rs1 : f24', 'fs1 == 0 and fe1 == 0xb6 and fm1 == 0x794e08 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000054c]:fcvt.lu.s s7, fs8, dyn
	-[0x80000550]:csrrs a7, fflags, zero
	-[0x80000554]:sd s7, 48(a5)
Current Store : [0x80000558] : sd a7, 56(a5) -- Store: [0x800023f8]:0x0000000000000011




Last Coverpoint : ['rd : x12', 'rs1 : f20', 'fs1 == 0 and fe1 == 0xb5 and fm1 == 0x675a83 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000564]:fcvt.lu.s a2, fs4, dyn
	-[0x80000568]:csrrs a7, fflags, zero
	-[0x8000056c]:sd a2, 64(a5)
Current Store : [0x80000570] : sd a7, 72(a5) -- Store: [0x80002408]:0x0000000000000011




Last Coverpoint : ['rd : x31', 'rs1 : f4', 'fs1 == 1 and fe1 == 0xb4 and fm1 == 0x154b68 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fcvt.lu.s t6, ft4, dyn
	-[0x80000580]:csrrs a7, fflags, zero
	-[0x80000584]:sd t6, 80(a5)
Current Store : [0x80000588] : sd a7, 88(a5) -- Store: [0x80002418]:0x0000000000000011




Last Coverpoint : ['rd : x11', 'rs1 : f29', 'fs1 == 0 and fe1 == 0xb3 and fm1 == 0x0c5d94 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.lu.s a1, ft9, dyn
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd a1, 96(a5)
Current Store : [0x800005a0] : sd a7, 104(a5) -- Store: [0x80002428]:0x0000000000000011




Last Coverpoint : ['rd : x6', 'rs1 : f22', 'fs1 == 1 and fe1 == 0xb2 and fm1 == 0x634400 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005ac]:fcvt.lu.s t1, fs6, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd t1, 112(a5)
Current Store : [0x800005b8] : sd a7, 120(a5) -- Store: [0x80002438]:0x0000000000000011




Last Coverpoint : ['rd : x25', 'rs1 : f28', 'fs1 == 1 and fe1 == 0xb1 and fm1 == 0x0e7405 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fcvt.lu.s s9, ft8, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd s9, 128(a5)
Current Store : [0x800005d0] : sd a7, 136(a5) -- Store: [0x80002448]:0x0000000000000011




Last Coverpoint : ['rd : x30', 'rs1 : f30', 'fs1 == 0 and fe1 == 0xb0 and fm1 == 0x4cf78c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fcvt.lu.s t5, ft10, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd t5, 144(a5)
Current Store : [0x800005e8] : sd a7, 152(a5) -- Store: [0x80002458]:0x0000000000000011




Last Coverpoint : ['rd : x26', 'rs1 : f9', 'fs1 == 1 and fe1 == 0xaf and fm1 == 0x2fe4f5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fcvt.lu.s s10, fs1, dyn
	-[0x800005f8]:csrrs a7, fflags, zero
	-[0x800005fc]:sd s10, 160(a5)
Current Store : [0x80000600] : sd a7, 168(a5) -- Store: [0x80002468]:0x0000000000000011




Last Coverpoint : ['rd : x24', 'rs1 : f27', 'fs1 == 1 and fe1 == 0xae and fm1 == 0x489b2c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000060c]:fcvt.lu.s s8, fs11, dyn
	-[0x80000610]:csrrs a7, fflags, zero
	-[0x80000614]:sd s8, 176(a5)
Current Store : [0x80000618] : sd a7, 184(a5) -- Store: [0x80002478]:0x0000000000000011




Last Coverpoint : ['rd : x8', 'rs1 : f12', 'fs1 == 0 and fe1 == 0xad and fm1 == 0x1328fb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fcvt.lu.s fp, fa2, dyn
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd fp, 192(a5)
Current Store : [0x80000630] : sd a7, 200(a5) -- Store: [0x80002488]:0x0000000000000011




Last Coverpoint : ['rd : x5', 'rs1 : f10', 'fs1 == 0 and fe1 == 0xac and fm1 == 0x6d74c2 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fcvt.lu.s t0, fa0, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd t0, 208(a5)
Current Store : [0x80000648] : sd a7, 216(a5) -- Store: [0x80002498]:0x0000000000000011




Last Coverpoint : ['rd : x0', 'rs1 : f0', 'fs1 == 1 and fe1 == 0xab and fm1 == 0x444931 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000654]:fcvt.lu.s zero, ft0, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd zero, 224(a5)
Current Store : [0x80000660] : sd a7, 232(a5) -- Store: [0x800024a8]:0x0000000000000011




Last Coverpoint : ['rd : x14', 'rs1 : f26', 'fs1 == 0 and fe1 == 0xaa and fm1 == 0x5435c5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000066c]:fcvt.lu.s a4, fs10, dyn
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd a4, 240(a5)
Current Store : [0x80000678] : sd a7, 248(a5) -- Store: [0x800024b8]:0x0000000000000011




Last Coverpoint : ['rd : x21', 'rs1 : f31', 'fs1 == 1 and fe1 == 0xa9 and fm1 == 0x68b3a9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000684]:fcvt.lu.s s5, ft11, dyn
	-[0x80000688]:csrrs a7, fflags, zero
	-[0x8000068c]:sd s5, 256(a5)
Current Store : [0x80000690] : sd a7, 264(a5) -- Store: [0x800024c8]:0x0000000000000011




Last Coverpoint : ['rd : x10', 'rs1 : f19', 'fs1 == 0 and fe1 == 0xa8 and fm1 == 0x3f90b4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000069c]:fcvt.lu.s a0, fs3, dyn
	-[0x800006a0]:csrrs a7, fflags, zero
	-[0x800006a4]:sd a0, 272(a5)
Current Store : [0x800006a8] : sd a7, 280(a5) -- Store: [0x800024d8]:0x0000000000000011




Last Coverpoint : ['rd : x22', 'rs1 : f18', 'fs1 == 0 and fe1 == 0xa7 and fm1 == 0x04b9f4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fcvt.lu.s s6, fs2, dyn
	-[0x800006b8]:csrrs a7, fflags, zero
	-[0x800006bc]:sd s6, 288(a5)
Current Store : [0x800006c0] : sd a7, 296(a5) -- Store: [0x800024e8]:0x0000000000000011




Last Coverpoint : ['rd : x20', 'rs1 : f14', 'fs1 == 0 and fe1 == 0xa6 and fm1 == 0x34d8ee and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006cc]:fcvt.lu.s s4, fa4, dyn
	-[0x800006d0]:csrrs a7, fflags, zero
	-[0x800006d4]:sd s4, 304(a5)
Current Store : [0x800006d8] : sd a7, 312(a5) -- Store: [0x800024f8]:0x0000000000000011




Last Coverpoint : ['rd : x7', 'rs1 : f23', 'fs1 == 1 and fe1 == 0xa5 and fm1 == 0x1cb5d4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fcvt.lu.s t2, fs7, dyn
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd t2, 320(a5)
Current Store : [0x800006f0] : sd a7, 328(a5) -- Store: [0x80002508]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0xa4 and fm1 == 0x140588 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006fc]:fcvt.lu.s t6, ft11, dyn
	-[0x80000700]:csrrs a7, fflags, zero
	-[0x80000704]:sd t6, 336(a5)
Current Store : [0x80000708] : sd a7, 344(a5) -- Store: [0x80002518]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0xa3 and fm1 == 0x6c0a6a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000714]:fcvt.lu.s t6, ft11, dyn
	-[0x80000718]:csrrs a7, fflags, zero
	-[0x8000071c]:sd t6, 352(a5)
Current Store : [0x80000720] : sd a7, 360(a5) -- Store: [0x80002528]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0xa2 and fm1 == 0x4ad269 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000730]:csrrs a7, fflags, zero
	-[0x80000734]:sd t6, 368(a5)
Current Store : [0x80000738] : sd a7, 376(a5) -- Store: [0x80002538]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0xa1 and fm1 == 0x0851ba and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000744]:fcvt.lu.s t6, ft11, dyn
	-[0x80000748]:csrrs a7, fflags, zero
	-[0x8000074c]:sd t6, 384(a5)
Current Store : [0x80000750] : sd a7, 392(a5) -- Store: [0x80002548]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0xa0 and fm1 == 0x37cfdc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000075c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000760]:csrrs a7, fflags, zero
	-[0x80000764]:sd t6, 400(a5)
Current Store : [0x80000768] : sd a7, 408(a5) -- Store: [0x80002558]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9f and fm1 == 0x46450c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000774]:fcvt.lu.s t6, ft11, dyn
	-[0x80000778]:csrrs a7, fflags, zero
	-[0x8000077c]:sd t6, 416(a5)
Current Store : [0x80000780] : sd a7, 424(a5) -- Store: [0x80002568]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x9e and fm1 == 0x283d12 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000078c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000790]:csrrs a7, fflags, zero
	-[0x80000794]:sd t6, 432(a5)
Current Store : [0x80000798] : sd a7, 440(a5) -- Store: [0x80002578]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x9d and fm1 == 0x72f818 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fcvt.lu.s t6, ft11, dyn
	-[0x800007a8]:csrrs a7, fflags, zero
	-[0x800007ac]:sd t6, 448(a5)
Current Store : [0x800007b0] : sd a7, 456(a5) -- Store: [0x80002588]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x9c and fm1 == 0x2edddb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007bc]:fcvt.lu.s t6, ft11, dyn
	-[0x800007c0]:csrrs a7, fflags, zero
	-[0x800007c4]:sd t6, 464(a5)
Current Store : [0x800007c8] : sd a7, 472(a5) -- Store: [0x80002598]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x9b and fm1 == 0x26c422 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fcvt.lu.s t6, ft11, dyn
	-[0x800007d8]:csrrs a7, fflags, zero
	-[0x800007dc]:sd t6, 480(a5)
Current Store : [0x800007e0] : sd a7, 488(a5) -- Store: [0x800025a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x9a and fm1 == 0x7872c3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fcvt.lu.s t6, ft11, dyn
	-[0x800007f0]:csrrs a7, fflags, zero
	-[0x800007f4]:sd t6, 496(a5)
Current Store : [0x800007f8] : sd a7, 504(a5) -- Store: [0x800025b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x99 and fm1 == 0x112603 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000804]:fcvt.lu.s t6, ft11, dyn
	-[0x80000808]:csrrs a7, fflags, zero
	-[0x8000080c]:sd t6, 512(a5)
Current Store : [0x80000810] : sd a7, 520(a5) -- Store: [0x800025c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x98 and fm1 == 0x0084e1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000081c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000820]:csrrs a7, fflags, zero
	-[0x80000824]:sd t6, 528(a5)
Current Store : [0x80000828] : sd a7, 536(a5) -- Store: [0x800025d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x97 and fm1 == 0x05aa55 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000834]:fcvt.lu.s t6, ft11, dyn
	-[0x80000838]:csrrs a7, fflags, zero
	-[0x8000083c]:sd t6, 544(a5)
Current Store : [0x80000840] : sd a7, 552(a5) -- Store: [0x800025e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x96 and fm1 == 0x4e817e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000850]:csrrs a7, fflags, zero
	-[0x80000854]:sd t6, 560(a5)
Current Store : [0x80000858] : sd a7, 568(a5) -- Store: [0x800025f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x95 and fm1 == 0x7c14e9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000864]:fcvt.lu.s t6, ft11, dyn
	-[0x80000868]:csrrs a7, fflags, zero
	-[0x8000086c]:sd t6, 576(a5)
Current Store : [0x80000870] : sd a7, 584(a5) -- Store: [0x80002608]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x94 and fm1 == 0x7dbfb7 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000087c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000880]:csrrs a7, fflags, zero
	-[0x80000884]:sd t6, 592(a5)
Current Store : [0x80000888] : sd a7, 600(a5) -- Store: [0x80002618]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x93 and fm1 == 0x624882 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000894]:fcvt.lu.s t6, ft11, dyn
	-[0x80000898]:csrrs a7, fflags, zero
	-[0x8000089c]:sd t6, 608(a5)
Current Store : [0x800008a0] : sd a7, 616(a5) -- Store: [0x80002628]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x92 and fm1 == 0x11056d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fcvt.lu.s t6, ft11, dyn
	-[0x800008b0]:csrrs a7, fflags, zero
	-[0x800008b4]:sd t6, 624(a5)
Current Store : [0x800008b8] : sd a7, 632(a5) -- Store: [0x80002638]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x91 and fm1 == 0x54b761 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008c4]:fcvt.lu.s t6, ft11, dyn
	-[0x800008c8]:csrrs a7, fflags, zero
	-[0x800008cc]:sd t6, 640(a5)
Current Store : [0x800008d0] : sd a7, 648(a5) -- Store: [0x80002648]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x90 and fm1 == 0x57ca4f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008dc]:fcvt.lu.s t6, ft11, dyn
	-[0x800008e0]:csrrs a7, fflags, zero
	-[0x800008e4]:sd t6, 656(a5)
Current Store : [0x800008e8] : sd a7, 664(a5) -- Store: [0x80002658]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x8f and fm1 == 0x3a7971 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fcvt.lu.s t6, ft11, dyn
	-[0x800008f8]:csrrs a7, fflags, zero
	-[0x800008fc]:sd t6, 672(a5)
Current Store : [0x80000900] : sd a7, 680(a5) -- Store: [0x80002668]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000910]:csrrs a7, fflags, zero
	-[0x80000914]:sd t6, 688(a5)
Current Store : [0x80000918] : sd a7, 696(a5) -- Store: [0x80002678]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x8d and fm1 == 0x244d9a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000924]:fcvt.lu.s t6, ft11, dyn
	-[0x80000928]:csrrs a7, fflags, zero
	-[0x8000092c]:sd t6, 704(a5)
Current Store : [0x80000930] : sd a7, 712(a5) -- Store: [0x80002688]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x8c and fm1 == 0x30d877 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000093c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000940]:csrrs a7, fflags, zero
	-[0x80000944]:sd t6, 720(a5)
Current Store : [0x80000948] : sd a7, 728(a5) -- Store: [0x80002698]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x8b and fm1 == 0x4d3559 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000954]:fcvt.lu.s t6, ft11, dyn
	-[0x80000958]:csrrs a7, fflags, zero
	-[0x8000095c]:sd t6, 736(a5)
Current Store : [0x80000960] : sd a7, 744(a5) -- Store: [0x800026a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x8a and fm1 == 0x6e19c1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000970]:csrrs a7, fflags, zero
	-[0x80000974]:sd t6, 752(a5)
Current Store : [0x80000978] : sd a7, 760(a5) -- Store: [0x800026b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x89 and fm1 == 0x05b406 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000984]:fcvt.lu.s t6, ft11, dyn
	-[0x80000988]:csrrs a7, fflags, zero
	-[0x8000098c]:sd t6, 768(a5)
Current Store : [0x80000990] : sd a7, 776(a5) -- Store: [0x800026c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x88 and fm1 == 0x7f8f2d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000099c]:fcvt.lu.s t6, ft11, dyn
	-[0x800009a0]:csrrs a7, fflags, zero
	-[0x800009a4]:sd t6, 784(a5)
Current Store : [0x800009a8] : sd a7, 792(a5) -- Store: [0x800026d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x87 and fm1 == 0x79f5e7 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fcvt.lu.s t6, ft11, dyn
	-[0x800009b8]:csrrs a7, fflags, zero
	-[0x800009bc]:sd t6, 800(a5)
Current Store : [0x800009c0] : sd a7, 808(a5) -- Store: [0x800026e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x86 and fm1 == 0x1fffe4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fcvt.lu.s t6, ft11, dyn
	-[0x800009d0]:csrrs a7, fflags, zero
	-[0x800009d4]:sd t6, 816(a5)
Current Store : [0x800009d8] : sd a7, 824(a5) -- Store: [0x800026f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x85 and fm1 == 0x29f475 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009e4]:fcvt.lu.s t6, ft11, dyn
	-[0x800009e8]:csrrs a7, fflags, zero
	-[0x800009ec]:sd t6, 832(a5)
Current Store : [0x800009f0] : sd a7, 840(a5) -- Store: [0x80002708]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x84 and fm1 == 0x42a54b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009fc]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a00]:csrrs a7, fflags, zero
	-[0x80000a04]:sd t6, 848(a5)
Current Store : [0x80000a08] : sd a7, 856(a5) -- Store: [0x80002718]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x83 and fm1 == 0x148266 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a18]:csrrs a7, fflags, zero
	-[0x80000a1c]:sd t6, 864(a5)
Current Store : [0x80000a20] : sd a7, 872(a5) -- Store: [0x80002728]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x82 and fm1 == 0x53a4fc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a30]:csrrs a7, fflags, zero
	-[0x80000a34]:sd t6, 880(a5)
Current Store : [0x80000a38] : sd a7, 888(a5) -- Store: [0x80002738]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a44]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a48]:csrrs a7, fflags, zero
	-[0x80000a4c]:sd t6, 896(a5)
Current Store : [0x80000a50] : sd a7, 904(a5) -- Store: [0x80002748]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a60]:csrrs a7, fflags, zero
	-[0x80000a64]:sd t6, 912(a5)
Current Store : [0x80000a68] : sd a7, 920(a5) -- Store: [0x80002758]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a78]:csrrs a7, fflags, zero
	-[0x80000a7c]:sd t6, 928(a5)
Current Store : [0x80000a80] : sd a7, 936(a5) -- Store: [0x80002768]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fcvt.lu.s t6, ft11, dyn
	-[0x80000a90]:csrrs a7, fflags, zero
	-[0x80000a94]:sd t6, 944(a5)
Current Store : [0x80000a98] : sd a7, 952(a5) -- Store: [0x80002778]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fcvt.lu.s t6, ft11, dyn
	-[0x80000aa8]:csrrs a7, fflags, zero
	-[0x80000aac]:sd t6, 960(a5)
Current Store : [0x80000ab0] : sd a7, 968(a5) -- Store: [0x80002788]:0x0000000000000011




Last Coverpoint : ['opcode : fcvt.lu.s', 'rd : x31', 'rs1 : f31', 'fs1 == 1 and fe1 == 0xab and fm1 == 0x444931 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000abc]:fcvt.lu.s t6, ft11, dyn
	-[0x80000ac0]:csrrs a7, fflags, zero
	-[0x80000ac4]:sd t6, 976(a5)
Current Store : [0x80000ac8] : sd a7, 984(a5) -- Store: [0x80002798]:0x0000000000000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                           coverpoints                                                           |                                                        code                                                        |
|---:|----------------------------------|---------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x0000000000000000|- opcode : fcvt.lu.s<br> - rd : x1<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.lu.s ra, ft6, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd ra, 0(a5)<br>       |
|   2|[0x80002320]<br>0xFFFFFFFFFFFFFFFF|- rd : x15<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0xd8 and fm1 == 0x62ecba and rm_val == 0  #nosat<br>                        |[0x800003d8]:fcvt.lu.s a5, fs5, dyn<br> [0x800003dc]:csrrs s5, fflags, zero<br> [0x800003e0]:sd a5, 0(s3)<br>       |
|   3|[0x80002330]<br>0x0000000000000000|- rd : x3<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x16 and fm1 == 0x63857e and rm_val == 0  #nosat<br>                         |[0x800003fc]:fcvt.lu.s gp, fa1, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd gp, 0(a5)<br>       |
|   4|[0x80002340]<br>0xFFFFFFFFFFFFFFFF|- rd : x27<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0xc1 and fm1 == 0x69185a and rm_val == 0  #nosat<br>                         |[0x80000414]:fcvt.lu.s s11, ft5, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd s11, 16(a5)<br>    |
|   5|[0x80002350]<br>0x0000000000000000|- rd : x2<br> - rs1 : f8<br> - fs1 == 1 and fe1 == 0xc0 and fm1 == 0x53096a and rm_val == 0  #nosat<br>                          |[0x8000042c]:fcvt.lu.s sp, fs0, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd sp, 32(a5)<br>      |
|   6|[0x80002360]<br>0x0000000000000000|- rd : x4<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0xbf and fm1 == 0x151cae and rm_val == 0  #nosat<br>                          |[0x80000444]:fcvt.lu.s tp, ft2, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd tp, 48(a5)<br>      |
|   7|[0x80002370]<br>0xBA55850000000000|- rd : x29<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0xbe and fm1 == 0x3a5585 and rm_val == 0  #nosat<br>                        |[0x8000045c]:fcvt.lu.s t4, fa3, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd t4, 64(a5)<br>      |
|   8|[0x80002380]<br>0x47A5998000000000|- rd : x16<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0xbd and fm1 == 0x0f4b33 and rm_val == 0  #nosat<br>                         |[0x80000480]:fcvt.lu.s a6, ft7, dyn<br> [0x80000484]:csrrs s5, fflags, zero<br> [0x80000488]:sd a6, 0(s3)<br>       |
|   9|[0x80002390]<br>0x0000000000000000|- rd : x18<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0xbc and fm1 == 0x16d7ca and rm_val == 0  #nosat<br>                        |[0x800004a4]:fcvt.lu.s s2, fa5, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s2, 0(a5)<br>       |
|  10|[0x800023a0]<br>0x0000000000000000|- rd : x28<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0xbb and fm1 == 0x4f0223 and rm_val == 0  #nosat<br>                        |[0x800004bc]:fcvt.lu.s t3, fa7, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd t3, 16(a5)<br>      |
|  11|[0x800023b0]<br>0x0A3297B000000000|- rd : x17<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0xba and fm1 == 0x23297b and rm_val == 0  #nosat<br>                        |[0x800004e0]:fcvt.lu.s a7, fs9, dyn<br> [0x800004e4]:csrrs s5, fflags, zero<br> [0x800004e8]:sd a7, 0(s3)<br>       |
|  12|[0x800023c0]<br>0x0000000000000000|- rd : x13<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0xb9 and fm1 == 0x1f6bf3 and rm_val == 0  #nosat<br>                         |[0x80000504]:fcvt.lu.s a3, ft1, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:sd a3, 0(a5)<br>       |
|  13|[0x800023d0]<br>0x02D4026400000000|- rd : x9<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0xb8 and fm1 == 0x350099 and rm_val == 0  #nosat<br>                          |[0x8000051c]:fcvt.lu.s s1, ft3, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:sd s1, 16(a5)<br>      |
|  14|[0x800023e0]<br>0x0000000000000000|- rd : x19<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0xb7 and fm1 == 0x377421 and rm_val == 0  #nosat<br>                        |[0x80000534]:fcvt.lu.s s3, fa6, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd s3, 32(a5)<br>      |
|  15|[0x800023f0]<br>0x00F94E0800000000|- rd : x23<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0xb6 and fm1 == 0x794e08 and rm_val == 0  #nosat<br>                        |[0x8000054c]:fcvt.lu.s s7, fs8, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:sd s7, 48(a5)<br>      |
|  16|[0x80002400]<br>0x0073AD4180000000|- rd : x12<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0xb5 and fm1 == 0x675a83 and rm_val == 0  #nosat<br>                        |[0x80000564]:fcvt.lu.s a2, fs4, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:sd a2, 64(a5)<br>      |
|  17|[0x80002410]<br>0x0000000000000000|- rd : x31<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0xb4 and fm1 == 0x154b68 and rm_val == 0  #nosat<br>                         |[0x8000057c]:fcvt.lu.s t6, ft4, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:sd t6, 80(a5)<br>      |
|  18|[0x80002420]<br>0x00118BB280000000|- rd : x11<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0xb3 and fm1 == 0x0c5d94 and rm_val == 0  #nosat<br>                        |[0x80000594]:fcvt.lu.s a1, ft9, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd a1, 96(a5)<br>      |
|  19|[0x80002430]<br>0x0000000000000000|- rd : x6<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0xb2 and fm1 == 0x634400 and rm_val == 0  #nosat<br>                         |[0x800005ac]:fcvt.lu.s t1, fs6, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd t1, 112(a5)<br>     |
|  20|[0x80002440]<br>0x0000000000000000|- rd : x25<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0xb1 and fm1 == 0x0e7405 and rm_val == 0  #nosat<br>                        |[0x800005c4]:fcvt.lu.s s9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd s9, 128(a5)<br>     |
|  21|[0x80002450]<br>0x000333DE30000000|- rd : x30<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0xb0 and fm1 == 0x4cf78c and rm_val == 0  #nosat<br>                        |[0x800005dc]:fcvt.lu.s t5, ft10, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd t5, 144(a5)<br>    |
|  22|[0x80002460]<br>0x0000000000000000|- rd : x26<br> - rs1 : f9<br> - fs1 == 1 and fe1 == 0xaf and fm1 == 0x2fe4f5 and rm_val == 0  #nosat<br>                         |[0x800005f4]:fcvt.lu.s s10, fs1, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:sd s10, 160(a5)<br>   |
|  23|[0x80002470]<br>0x0000000000000000|- rd : x24<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0xae and fm1 == 0x489b2c and rm_val == 0  #nosat<br>                        |[0x8000060c]:fcvt.lu.s s8, fs11, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:sd s8, 176(a5)<br>    |
|  24|[0x80002480]<br>0x000049947D800000|- rd : x8<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0xad and fm1 == 0x1328fb and rm_val == 0  #nosat<br>                         |[0x80000624]:fcvt.lu.s fp, fa2, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd fp, 192(a5)<br>     |
|  25|[0x80002490]<br>0x00003B5D30800000|- rd : x5<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0xac and fm1 == 0x6d74c2 and rm_val == 0  #nosat<br>                         |[0x8000063c]:fcvt.lu.s t0, fa0, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd t0, 208(a5)<br>     |
|  26|[0x800024a0]<br>0x0000000000000000|- rd : x0<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0xab and fm1 == 0x444931 and rm_val == 0  #nosat<br>                          |[0x80000654]:fcvt.lu.s zero, ft0, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd zero, 224(a5)<br> |
|  27|[0x800024b0]<br>0x00000D435C500000|- rd : x14<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0xaa and fm1 == 0x5435c5 and rm_val == 0  #nosat<br>                        |[0x8000066c]:fcvt.lu.s a4, fs10, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd a4, 240(a5)<br>    |
|  28|[0x800024c0]<br>0x0000000000000000|- rd : x21<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0xa9 and fm1 == 0x68b3a9 and rm_val == 0  #nosat<br>                        |[0x80000684]:fcvt.lu.s s5, ft11, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:sd s5, 256(a5)<br>    |
|  29|[0x800024d0]<br>0x000002FE42D00000|- rd : x10<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0xa8 and fm1 == 0x3f90b4 and rm_val == 0  #nosat<br>                        |[0x8000069c]:fcvt.lu.s a0, fs3, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:sd a0, 272(a5)<br>     |
|  30|[0x800024e0]<br>0x0000010973E80000|- rd : x22<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0xa7 and fm1 == 0x04b9f4 and rm_val == 0  #nosat<br>                        |[0x800006b4]:fcvt.lu.s s6, fs2, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:sd s6, 288(a5)<br>     |
|  31|[0x800024f0]<br>0x000000B4D8EE0000|- rd : x20<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0xa6 and fm1 == 0x34d8ee and rm_val == 0  #nosat<br>                        |[0x800006cc]:fcvt.lu.s s4, fa4, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:sd s4, 304(a5)<br>     |
|  32|[0x80002500]<br>0x0000000000000000|- rd : x7<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0xa5 and fm1 == 0x1cb5d4 and rm_val == 0  #nosat<br>                         |[0x800006e4]:fcvt.lu.s t2, fs7, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd t2, 320(a5)<br>     |
|  33|[0x80002510]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0xa4 and fm1 == 0x140588 and rm_val == 0  #nosat<br>                                                       |[0x800006fc]:fcvt.lu.s t6, ft11, dyn<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:sd t6, 336(a5)<br>    |
|  34|[0x80002520]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0xa3 and fm1 == 0x6c0a6a and rm_val == 0  #nosat<br>                                                       |[0x80000714]:fcvt.lu.s t6, ft11, dyn<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:sd t6, 352(a5)<br>    |
|  35|[0x80002530]<br>0x0000000CAD269000|- fs1 == 0 and fe1 == 0xa2 and fm1 == 0x4ad269 and rm_val == 0  #nosat<br>                                                       |[0x8000072c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:sd t6, 368(a5)<br>    |
|  36|[0x80002540]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0xa1 and fm1 == 0x0851ba and rm_val == 0  #nosat<br>                                                       |[0x80000744]:fcvt.lu.s t6, ft11, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:sd t6, 384(a5)<br>    |
|  37|[0x80002550]<br>0x00000002DF3F7000|- fs1 == 0 and fe1 == 0xa0 and fm1 == 0x37cfdc and rm_val == 0  #nosat<br>                                                       |[0x8000075c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:sd t6, 400(a5)<br>    |
|  38|[0x80002560]<br>0x000000018C8A1800|- fs1 == 0 and fe1 == 0x9f and fm1 == 0x46450c and rm_val == 0  #nosat<br>                                                       |[0x80000774]:fcvt.lu.s t6, ft11, dyn<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:sd t6, 416(a5)<br>    |
|  39|[0x80002570]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x9e and fm1 == 0x283d12 and rm_val == 0  #nosat<br>                                                       |[0x8000078c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:sd t6, 432(a5)<br>    |
|  40|[0x80002580]<br>0x00000000797C0C00|- fs1 == 0 and fe1 == 0x9d and fm1 == 0x72f818 and rm_val == 0  #nosat<br>                                                       |[0x800007a4]:fcvt.lu.s t6, ft11, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:sd t6, 448(a5)<br>    |
|  41|[0x80002590]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x9c and fm1 == 0x2edddb and rm_val == 0  #nosat<br>                                                       |[0x800007bc]:fcvt.lu.s t6, ft11, dyn<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:sd t6, 464(a5)<br>    |
|  42|[0x800025a0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x9b and fm1 == 0x26c422 and rm_val == 0  #nosat<br>                                                       |[0x800007d4]:fcvt.lu.s t6, ft11, dyn<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:sd t6, 480(a5)<br>    |
|  43|[0x800025b0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x9a and fm1 == 0x7872c3 and rm_val == 0  #nosat<br>                                                       |[0x800007ec]:fcvt.lu.s t6, ft11, dyn<br> [0x800007f0]:csrrs a7, fflags, zero<br> [0x800007f4]:sd t6, 496(a5)<br>    |
|  44|[0x800025c0]<br>0x0000000004893018|- fs1 == 0 and fe1 == 0x99 and fm1 == 0x112603 and rm_val == 0  #nosat<br>                                                       |[0x80000804]:fcvt.lu.s t6, ft11, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:sd t6, 512(a5)<br>    |
|  45|[0x800025d0]<br>0x0000000002021384|- fs1 == 0 and fe1 == 0x98 and fm1 == 0x0084e1 and rm_val == 0  #nosat<br>                                                       |[0x8000081c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000820]:csrrs a7, fflags, zero<br> [0x80000824]:sd t6, 528(a5)<br>    |
|  46|[0x800025e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x97 and fm1 == 0x05aa55 and rm_val == 0  #nosat<br>                                                       |[0x80000834]:fcvt.lu.s t6, ft11, dyn<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:sd t6, 544(a5)<br>    |
|  47|[0x800025f0]<br>0x0000000000CE817E|- fs1 == 0 and fe1 == 0x96 and fm1 == 0x4e817e and rm_val == 0  #nosat<br>                                                       |[0x8000084c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000850]:csrrs a7, fflags, zero<br> [0x80000854]:sd t6, 560(a5)<br>    |
|  48|[0x80002600]<br>0x00000000007E0A74|- fs1 == 0 and fe1 == 0x95 and fm1 == 0x7c14e9 and rm_val == 0  #nosat<br>                                                       |[0x80000864]:fcvt.lu.s t6, ft11, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:sd t6, 576(a5)<br>    |
|  49|[0x80002610]<br>0x00000000003F6FEE|- fs1 == 0 and fe1 == 0x94 and fm1 == 0x7dbfb7 and rm_val == 0  #nosat<br>                                                       |[0x8000087c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000880]:csrrs a7, fflags, zero<br> [0x80000884]:sd t6, 592(a5)<br>    |
|  50|[0x80002620]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x93 and fm1 == 0x624882 and rm_val == 0  #nosat<br>                                                       |[0x80000894]:fcvt.lu.s t6, ft11, dyn<br> [0x80000898]:csrrs a7, fflags, zero<br> [0x8000089c]:sd t6, 608(a5)<br>    |
|  51|[0x80002630]<br>0x0000000000091057|- fs1 == 0 and fe1 == 0x92 and fm1 == 0x11056d and rm_val == 0  #nosat<br>                                                       |[0x800008ac]:fcvt.lu.s t6, ft11, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:sd t6, 624(a5)<br>    |
|  52|[0x80002640]<br>0x000000000006A5BB|- fs1 == 0 and fe1 == 0x91 and fm1 == 0x54b761 and rm_val == 0  #nosat<br>                                                       |[0x800008c4]:fcvt.lu.s t6, ft11, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:sd t6, 640(a5)<br>    |
|  53|[0x80002650]<br>0x0000000000035F29|- fs1 == 0 and fe1 == 0x90 and fm1 == 0x57ca4f and rm_val == 0  #nosat<br>                                                       |[0x800008dc]:fcvt.lu.s t6, ft11, dyn<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:sd t6, 656(a5)<br>    |
|  54|[0x80002660]<br>0x00000000000174F3|- fs1 == 0 and fe1 == 0x8f and fm1 == 0x3a7971 and rm_val == 0  #nosat<br>                                                       |[0x800008f4]:fcvt.lu.s t6, ft11, dyn<br> [0x800008f8]:csrrs a7, fflags, zero<br> [0x800008fc]:sd t6, 672(a5)<br>    |
|  55|[0x80002670]<br>0x000000000000D665|- fs1 == 0 and fe1 == 0x8e and fm1 == 0x56653f and rm_val == 0  #nosat<br>                                                       |[0x8000090c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000910]:csrrs a7, fflags, zero<br> [0x80000914]:sd t6, 688(a5)<br>    |
|  56|[0x80002680]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x8d and fm1 == 0x244d9a and rm_val == 0  #nosat<br>                                                       |[0x80000924]:fcvt.lu.s t6, ft11, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:sd t6, 704(a5)<br>    |
|  57|[0x80002690]<br>0x0000000000002C36|- fs1 == 0 and fe1 == 0x8c and fm1 == 0x30d877 and rm_val == 0  #nosat<br>                                                       |[0x8000093c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000940]:csrrs a7, fflags, zero<br> [0x80000944]:sd t6, 720(a5)<br>    |
|  58|[0x800026a0]<br>0x00000000000019A7|- fs1 == 0 and fe1 == 0x8b and fm1 == 0x4d3559 and rm_val == 0  #nosat<br>                                                       |[0x80000954]:fcvt.lu.s t6, ft11, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:sd t6, 736(a5)<br>    |
|  59|[0x800026b0]<br>0x0000000000000EE2|- fs1 == 0 and fe1 == 0x8a and fm1 == 0x6e19c1 and rm_val == 0  #nosat<br>                                                       |[0x8000096c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000970]:csrrs a7, fflags, zero<br> [0x80000974]:sd t6, 752(a5)<br>    |
|  60|[0x800026c0]<br>0x000000000000042E|- fs1 == 0 and fe1 == 0x89 and fm1 == 0x05b406 and rm_val == 0  #nosat<br>                                                       |[0x80000984]:fcvt.lu.s t6, ft11, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:sd t6, 768(a5)<br>    |
|  61|[0x800026d0]<br>0x00000000000003FE|- fs1 == 0 and fe1 == 0x88 and fm1 == 0x7f8f2d and rm_val == 0  #nosat<br>                                                       |[0x8000099c]:fcvt.lu.s t6, ft11, dyn<br> [0x800009a0]:csrrs a7, fflags, zero<br> [0x800009a4]:sd t6, 784(a5)<br>    |
|  62|[0x800026e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x87 and fm1 == 0x79f5e7 and rm_val == 0  #nosat<br>                                                       |[0x800009b4]:fcvt.lu.s t6, ft11, dyn<br> [0x800009b8]:csrrs a7, fflags, zero<br> [0x800009bc]:sd t6, 800(a5)<br>    |
|  63|[0x800026f0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x86 and fm1 == 0x1fffe4 and rm_val == 0  #nosat<br>                                                       |[0x800009cc]:fcvt.lu.s t6, ft11, dyn<br> [0x800009d0]:csrrs a7, fflags, zero<br> [0x800009d4]:sd t6, 816(a5)<br>    |
|  64|[0x80002700]<br>0x0000000000000055|- fs1 == 0 and fe1 == 0x85 and fm1 == 0x29f475 and rm_val == 0  #nosat<br>                                                       |[0x800009e4]:fcvt.lu.s t6, ft11, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:sd t6, 832(a5)<br>    |
|  65|[0x80002710]<br>0x0000000000000031|- fs1 == 0 and fe1 == 0x84 and fm1 == 0x42a54b and rm_val == 0  #nosat<br>                                                       |[0x800009fc]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:sd t6, 848(a5)<br>    |
|  66|[0x80002720]<br>0x0000000000000013|- fs1 == 0 and fe1 == 0x83 and fm1 == 0x148266 and rm_val == 0  #nosat<br>                                                       |[0x80000a14]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a18]:csrrs a7, fflags, zero<br> [0x80000a1c]:sd t6, 864(a5)<br>    |
|  67|[0x80002730]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x82 and fm1 == 0x53a4fc and rm_val == 0  #nosat<br>                                                       |[0x80000a2c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a30]:csrrs a7, fflags, zero<br> [0x80000a34]:sd t6, 880(a5)<br>    |
|  68|[0x80002740]<br>0x0000000000000007|- fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat<br>                                                       |[0x80000a44]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:sd t6, 896(a5)<br>    |
|  69|[0x80002750]<br>0x0000000000000004|- fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat<br>                                                       |[0x80000a5c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a60]:csrrs a7, fflags, zero<br> [0x80000a64]:sd t6, 912(a5)<br>    |
|  70|[0x80002760]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat<br>                                                       |[0x80000a74]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a78]:csrrs a7, fflags, zero<br> [0x80000a7c]:sd t6, 928(a5)<br>    |
|  71|[0x80002770]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat<br>                                                       |[0x80000a8c]:fcvt.lu.s t6, ft11, dyn<br> [0x80000a90]:csrrs a7, fflags, zero<br> [0x80000a94]:sd t6, 944(a5)<br>    |
|  72|[0x80002780]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat<br>                                                       |[0x80000aa4]:fcvt.lu.s t6, ft11, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:sd t6, 960(a5)<br>    |
