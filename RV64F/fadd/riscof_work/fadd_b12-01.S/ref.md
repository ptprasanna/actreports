
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000960')]      |
| SIG_REGION                | [('0x80002310', '0x80002650', '104 dwords')]      |
| COV_LABELS                | fadd_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fadd/riscof_work/fadd_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 152      |
| Total Signature Updates   | 52      |
| STAT1                     | 52      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                                                      coverpoints                                                                                                       |                                                                          code                                                                          |
|---:|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000005|- opcode : fadd.s<br> - rs1 : f5<br> - rs2 : f5<br> - rd : f26<br> - rs1 == rs2 != rd<br>                                                                                                                               |[0x800003b8]:fadd.s fs10, ft5, ft5, dyn<br> [0x800003bc]:csrrs a7, fflags, zero<br> [0x800003c0]:fsw fs10, 0(a5)<br> [0x800003c4]:sd a7, 8(a5)<br>      |
|   2|[0x80002328]<br>0x0000000000000005|- rs1 : f3<br> - rs2 : f20<br> - rd : f15<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4f07 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x530d37 and rm_val == 0  #nosat<br> |[0x800003d4]:fadd.s fa5, ft3, fs4, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsw fa5, 16(a5)<br> [0x800003e0]:sd a7, 24(a5)<br>      |
|   3|[0x80002338]<br>0x0000000000000005|- rs1 : f30<br> - rs2 : f25<br> - rd : f30<br> - rs1 == rd != rs2<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x185183 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x28224f and rm_val == 0  #nosat<br>                       |[0x800003f0]:fadd.s ft10, ft10, fs9, dyn<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:fsw ft10, 32(a5)<br> [0x800003fc]:sd a7, 40(a5)<br>   |
|   4|[0x80002348]<br>0x0000000000000005|- rs1 : f0<br> - rs2 : f0<br> - rd : f0<br> - rs1 == rs2 == rd<br>                                                                                                                                                      |[0x8000040c]:fadd.s ft0, ft0, ft0, dyn<br> [0x80000410]:csrrs a7, fflags, zero<br> [0x80000414]:fsw ft0, 48(a5)<br> [0x80000418]:sd a7, 56(a5)<br>      |
|   5|[0x80002358]<br>0x0000000000000005|- rs1 : f15<br> - rs2 : f16<br> - rd : f16<br> - rs2 == rd != rs1<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x2d0427 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x554674 and rm_val == 0  #nosat<br>                       |[0x80000428]:fadd.s fa6, fa5, fa6, dyn<br> [0x8000042c]:csrrs a7, fflags, zero<br> [0x80000430]:fsw fa6, 64(a5)<br> [0x80000434]:sd a7, 72(a5)<br>      |
|   6|[0x80002368]<br>0x0000000000000005|- rs1 : f9<br> - rs2 : f11<br> - rd : f10<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x365ad7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x80000444]:fadd.s fa0, fs1, fa1, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsw fa0, 80(a5)<br> [0x80000450]:sd a7, 88(a5)<br>      |
|   7|[0x80002378]<br>0x0000000000000005|- rs1 : f13<br> - rs2 : f1<br> - rd : f2<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2bd8f4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x80000460]:fadd.s ft2, fa3, ft1, dyn<br> [0x80000464]:csrrs a7, fflags, zero<br> [0x80000468]:fsw ft2, 96(a5)<br> [0x8000046c]:sd a7, 104(a5)<br>     |
|   8|[0x80002388]<br>0x0000000000000005|- rs1 : f25<br> - rs2 : f8<br> - rd : f24<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1bd52c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x8000047c]:fadd.s fs8, fs9, fs0, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsw fs8, 112(a5)<br> [0x80000488]:sd a7, 120(a5)<br>    |
|   9|[0x80002398]<br>0x0000000000000005|- rs1 : f10<br> - rs2 : f14<br> - rd : f31<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x076a16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c560e and rm_val == 0  #nosat<br>                                              |[0x80000498]:fadd.s ft11, fa0, fa4, dyn<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:fsw ft11, 128(a5)<br> [0x800004a4]:sd a7, 136(a5)<br>  |
|  10|[0x800023a8]<br>0x0000000000000005|- rs1 : f24<br> - rs2 : f26<br> - rd : f21<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x4f9722 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x32ec8c and rm_val == 0  #nosat<br>                                              |[0x800004b4]:fadd.s fs5, fs8, fs10, dyn<br> [0x800004b8]:csrrs a7, fflags, zero<br> [0x800004bc]:fsw fs5, 144(a5)<br> [0x800004c0]:sd a7, 152(a5)<br>   |
|  11|[0x800023b8]<br>0x0000000000000005|- rs1 : f7<br> - rs2 : f22<br> - rd : f13<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c7300 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x800004d0]:fadd.s fa3, ft7, fs6, dyn<br> [0x800004d4]:csrrs a7, fflags, zero<br> [0x800004d8]:fsw fa3, 160(a5)<br> [0x800004dc]:sd a7, 168(a5)<br>    |
|  12|[0x800023c8]<br>0x0000000000000005|- rs1 : f11<br> - rs2 : f29<br> - rd : f25<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x314a05 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                              |[0x800004ec]:fadd.s fs9, fa1, ft9, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsw fs9, 176(a5)<br> [0x800004f8]:sd a7, 184(a5)<br>    |
|  13|[0x800023d8]<br>0x0000000000000005|- rs1 : f23<br> - rs2 : f21<br> - rd : f12<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1175bf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                              |[0x80000508]:fadd.s fa2, fs7, fs5, dyn<br> [0x8000050c]:csrrs a7, fflags, zero<br> [0x80000510]:fsw fa2, 192(a5)<br> [0x80000514]:sd a7, 200(a5)<br>    |
|  14|[0x800023e8]<br>0x0000000000000005|- rs1 : f26<br> - rs2 : f31<br> - rd : f14<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x36fce6 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x597afe and rm_val == 0  #nosat<br>                                              |[0x80000524]:fadd.s fa4, fs10, ft11, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsw fa4, 208(a5)<br> [0x80000530]:sd a7, 216(a5)<br>  |
|  15|[0x800023f8]<br>0x0000000000000005|- rs1 : f14<br> - rs2 : f10<br> - rd : f5<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x6b4e0d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x73bb25 and rm_val == 0  #nosat<br>                                               |[0x80000540]:fadd.s ft5, fa4, fa0, dyn<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:fsw ft5, 224(a5)<br> [0x8000054c]:sd a7, 232(a5)<br>    |
|  16|[0x80002408]<br>0x0000000000000005|- rs1 : f31<br> - rs2 : f24<br> - rd : f17<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x1173d9 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x106e2e and rm_val == 0  #nosat<br>                                              |[0x8000055c]:fadd.s fa7, ft11, fs8, dyn<br> [0x80000560]:csrrs a7, fflags, zero<br> [0x80000564]:fsw fa7, 240(a5)<br> [0x80000568]:sd a7, 248(a5)<br>   |
|  17|[0x80002418]<br>0x0000000000000005|- rs1 : f16<br> - rs2 : f18<br> - rd : f27<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x5d0ccb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2644ac and rm_val == 0  #nosat<br>                                              |[0x80000578]:fadd.s fs11, fa6, fs2, dyn<br> [0x8000057c]:csrrs a7, fflags, zero<br> [0x80000580]:fsw fs11, 256(a5)<br> [0x80000584]:sd a7, 264(a5)<br>  |
|  18|[0x80002428]<br>0x0000000000000005|- rs1 : f12<br> - rs2 : f9<br> - rd : f18<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x64f961 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3c6359 and rm_val == 0  #nosat<br>                                               |[0x80000594]:fadd.s fs2, fa2, fs1, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsw fs2, 272(a5)<br> [0x800005a0]:sd a7, 280(a5)<br>    |
|  19|[0x80002438]<br>0x0000000000000005|- rs1 : f22<br> - rs2 : f12<br> - rd : f9<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x61a51b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4642a7 and rm_val == 0  #nosat<br>                                               |[0x800005b0]:fadd.s fs1, fs6, fa2, dyn<br> [0x800005b4]:csrrs a7, fflags, zero<br> [0x800005b8]:fsw fs1, 288(a5)<br> [0x800005bc]:sd a7, 296(a5)<br>    |
|  20|[0x80002448]<br>0x0000000000000005|- rs1 : f6<br> - rs2 : f2<br> - rd : f22<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x390e97 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                |[0x800005cc]:fadd.s fs6, ft6, ft2, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:fsw fs6, 304(a5)<br> [0x800005d8]:sd a7, 312(a5)<br>    |
|  21|[0x80002458]<br>0x0000000000000005|- rs1 : f19<br> - rs2 : f23<br> - rd : f3<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1c60ac and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x800005e8]:fadd.s ft3, fs3, fs7, dyn<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:fsw ft3, 320(a5)<br> [0x800005f4]:sd a7, 328(a5)<br>    |
|  22|[0x80002468]<br>0x0000000000000005|- rs1 : f8<br> - rs2 : f19<br> - rd : f23<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x07a8e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x70c4b8 and rm_val == 0  #nosat<br>                                               |[0x80000604]:fadd.s fs7, fs0, fs3, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsw fs7, 336(a5)<br> [0x80000610]:sd a7, 344(a5)<br>    |
|  23|[0x80002478]<br>0x0000000000000005|- rs1 : f27<br> - rs2 : f30<br> - rd : f19<br> - fs1 == 1 and fe1 == 0xfb and fm1 == 0x278349 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x7d9098 and rm_val == 0  #nosat<br>                                              |[0x80000620]:fadd.s fs3, fs11, ft10, dyn<br> [0x80000624]:csrrs a7, fflags, zero<br> [0x80000628]:fsw fs3, 352(a5)<br> [0x8000062c]:sd a7, 360(a5)<br>  |
|  24|[0x80002488]<br>0x0000000000000005|- rs1 : f20<br> - rs2 : f7<br> - rd : f29<br> - fs1 == 1 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x8000063c]:fadd.s ft9, fs4, ft7, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsw ft9, 368(a5)<br> [0x80000648]:sd a7, 376(a5)<br>    |
|  25|[0x80002498]<br>0x0000000000000005|- rs1 : f28<br> - rs2 : f13<br> - rd : f8<br> - fs1 == 1 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x19caca and rm_val == 0  #nosat<br>                                               |[0x80000658]:fadd.s fs0, ft8, fa3, dyn<br> [0x8000065c]:csrrs a7, fflags, zero<br> [0x80000660]:fsw fs0, 384(a5)<br> [0x80000664]:sd a7, 392(a5)<br>    |
|  26|[0x800024a8]<br>0x0000000000000005|- rs1 : f21<br> - rs2 : f28<br> - rd : f7<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x703879 and rm_val == 0  #nosat<br>                                               |[0x80000674]:fadd.s ft7, fs5, ft8, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:fsw ft7, 400(a5)<br> [0x80000680]:sd a7, 408(a5)<br>    |
|  27|[0x800024b8]<br>0x0000000000000005|- rs1 : f1<br> - rs2 : f3<br> - rd : f6<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6794fc and rm_val == 0  #nosat<br>                                                 |[0x80000690]:fadd.s ft6, ft1, ft3, dyn<br> [0x80000694]:csrrs a7, fflags, zero<br> [0x80000698]:fsw ft6, 416(a5)<br> [0x8000069c]:sd a7, 424(a5)<br>    |
|  28|[0x800024c8]<br>0x0000000000000005|- rs1 : f2<br> - rs2 : f6<br> - rd : f1<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x11a59d and rm_val == 0  #nosat<br>                                                 |[0x800006ac]:fadd.s ft1, ft2, ft6, dyn<br> [0x800006b0]:csrrs a7, fflags, zero<br> [0x800006b4]:fsw ft1, 432(a5)<br> [0x800006b8]:sd a7, 440(a5)<br>    |
|  29|[0x800024d8]<br>0x0000000000000005|- rs1 : f29<br> - rs2 : f17<br> - rd : f11<br> - fs1 == 1 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x6591d8 and rm_val == 0  #nosat<br>                                              |[0x800006c8]:fadd.s fa1, ft9, fa7, dyn<br> [0x800006cc]:csrrs a7, fflags, zero<br> [0x800006d0]:fsw fa1, 448(a5)<br> [0x800006d4]:sd a7, 456(a5)<br>    |
|  30|[0x800024e8]<br>0x0000000000000005|- rs1 : f17<br> - rs2 : f27<br> - rd : f4<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x55691d and rm_val == 0  #nosat<br>                                               |[0x800006e4]:fadd.s ft4, fa7, fs11, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsw ft4, 464(a5)<br> [0x800006f0]:sd a7, 472(a5)<br>   |
|  31|[0x800024f8]<br>0x0000000000000005|- rs1 : f18<br> - rs2 : f4<br> - rd : f20<br> - fs1 == 1 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3df905 and rm_val == 0  #nosat<br>                                               |[0x80000700]:fadd.s fs4, fs2, ft4, dyn<br> [0x80000704]:csrrs a7, fflags, zero<br> [0x80000708]:fsw fs4, 480(a5)<br> [0x8000070c]:sd a7, 488(a5)<br>    |
|  32|[0x80002508]<br>0x0000000000000005|- rs1 : f4<br> - rs2 : f15<br> - rd : f28<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                               |[0x8000071c]:fadd.s ft8, ft4, fa5, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:fsw ft8, 496(a5)<br> [0x80000728]:sd a7, 504(a5)<br>    |
|  33|[0x80002518]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 0 and fe2 == 0xfe and fm2 == 0x56c1e5 and rm_val == 0  #nosat<br>                                                                                             |[0x80000738]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:fsw ft11, 512(a5)<br> [0x80000744]:sd a7, 520(a5)<br> |
|  34|[0x80002528]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5817b0 and rm_val == 0  #nosat<br>                                                                                             |[0x80000754]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000758]:csrrs a7, fflags, zero<br> [0x8000075c]:fsw ft11, 528(a5)<br> [0x80000760]:sd a7, 536(a5)<br> |
|  35|[0x80002538]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4c9471 and rm_val == 0  #nosat<br>                                                                                             |[0x80000770]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000774]:csrrs a7, fflags, zero<br> [0x80000778]:fsw ft11, 544(a5)<br> [0x8000077c]:sd a7, 552(a5)<br> |
|  36|[0x80002548]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x8000078c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:fsw ft11, 560(a5)<br> [0x80000798]:sd a7, 568(a5)<br> |
|  37|[0x80002558]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800007a8]:fadd.s ft11, ft10, ft9, dyn<br> [0x800007ac]:csrrs a7, fflags, zero<br> [0x800007b0]:fsw ft11, 576(a5)<br> [0x800007b4]:sd a7, 584(a5)<br> |
|  38|[0x80002568]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x48a6ba and rm_val == 0  #nosat<br>                                                                                             |[0x800007c4]:fadd.s ft11, ft10, ft9, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsw ft11, 592(a5)<br> [0x800007d0]:sd a7, 600(a5)<br> |
|  39|[0x80002578]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5bf8d8 and rm_val == 0  #nosat<br>                                                                                             |[0x800007e0]:fadd.s ft11, ft10, ft9, dyn<br> [0x800007e4]:csrrs a7, fflags, zero<br> [0x800007e8]:fsw ft11, 608(a5)<br> [0x800007ec]:sd a7, 616(a5)<br> |
|  40|[0x80002588]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800007fc]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000800]:csrrs a7, fflags, zero<br> [0x80000804]:fsw ft11, 624(a5)<br> [0x80000808]:sd a7, 632(a5)<br> |
|  41|[0x80002598]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x47ad0f and rm_val == 0  #nosat<br>                                                                                             |[0x80000818]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000081c]:csrrs a7, fflags, zero<br> [0x80000820]:fsw ft11, 640(a5)<br> [0x80000824]:sd a7, 648(a5)<br> |
|  42|[0x800025a8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000834]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:fsw ft11, 656(a5)<br> [0x80000840]:sd a7, 664(a5)<br> |
|  43|[0x800025b8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000850]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000854]:csrrs a7, fflags, zero<br> [0x80000858]:fsw ft11, 672(a5)<br> [0x8000085c]:sd a7, 680(a5)<br> |
|  44|[0x800025c8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x26d2f5 and rm_val == 0  #nosat<br>                                                                                             |[0x8000086c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000870]:csrrs a7, fflags, zero<br> [0x80000874]:fsw ft11, 688(a5)<br> [0x80000878]:sd a7, 696(a5)<br> |
|  45|[0x800025d8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x473a2e and rm_val == 0  #nosat<br>                                                                                             |[0x80000888]:fadd.s ft11, ft10, ft9, dyn<br> [0x8000088c]:csrrs a7, fflags, zero<br> [0x80000890]:fsw ft11, 704(a5)<br> [0x80000894]:sd a7, 712(a5)<br> |
|  46|[0x800025e8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x800008a4]:fadd.s ft11, ft10, ft9, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsw ft11, 720(a5)<br> [0x800008b0]:sd a7, 728(a5)<br> |
|  47|[0x800025f8]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x4b8415 and rm_val == 0  #nosat<br>                                                                                             |[0x800008c0]:fadd.s ft11, ft10, ft9, dyn<br> [0x800008c4]:csrrs a7, fflags, zero<br> [0x800008c8]:fsw ft11, 736(a5)<br> [0x800008cc]:sd a7, 744(a5)<br> |
|  48|[0x80002608]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x50b9b1 and rm_val == 0  #nosat<br>                                                                                             |[0x800008dc]:fadd.s ft11, ft10, ft9, dyn<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:fsw ft11, 752(a5)<br> [0x800008e8]:sd a7, 760(a5)<br> |
|  49|[0x80002618]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x2fe97e and rm_val == 0  #nosat<br>                                                                                             |[0x800008f8]:fadd.s ft11, ft10, ft9, dyn<br> [0x800008fc]:csrrs a7, fflags, zero<br> [0x80000900]:fsw ft11, 768(a5)<br> [0x80000904]:sd a7, 776(a5)<br> |
|  50|[0x80002628]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfe and fm2 == 0x027635 and rm_val == 0  #nosat<br>                                                                                             |[0x80000914]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000918]:csrrs a7, fflags, zero<br> [0x8000091c]:fsw ft11, 784(a5)<br> [0x80000920]:sd a7, 792(a5)<br> |
|  51|[0x80002638]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000930]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000934]:csrrs a7, fflags, zero<br> [0x80000938]:fsw ft11, 800(a5)<br> [0x8000093c]:sd a7, 808(a5)<br> |
|  52|[0x80002648]<br>0x0000000000000005|- fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f4810 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and rm_val == 0  #nosat<br>                                                                                             |[0x8000094c]:fadd.s ft11, ft10, ft9, dyn<br> [0x80000950]:csrrs a7, fflags, zero<br> [0x80000954]:fsw ft11, 816(a5)<br> [0x80000958]:sd a7, 824(a5)<br> |
