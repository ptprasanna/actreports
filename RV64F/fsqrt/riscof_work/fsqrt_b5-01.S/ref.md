
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800006b0')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | fsqrt_b5      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fsqrt/riscof_work/fsqrt_b5-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                   coverpoints                                                                   |                                                                       code                                                                        |
|---:|----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x0000000000000000|- opcode : fsqrt.s<br> - rs1 : f28<br> - rd : f20<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br> |[0x800003b4]:fsqrt.s fs4, ft8, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsw fs4, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>       |
|   2|[0x80002228]<br>0x0000000000000000|- rs1 : f6<br> - rd : f6<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 4  #nosat<br>                          |[0x800003cc]:fsqrt.s ft6, ft6, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsw ft6, 16(a5)<br> [0x800003d8]:sd a7, 24(a5)<br>     |
|   3|[0x80002238]<br>0x0000000000000000|- rs1 : f7<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 3  #nosat<br>                                         |[0x800003e4]:fsqrt.s fs10, ft7, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsw fs10, 32(a5)<br> [0x800003f0]:sd a7, 40(a5)<br>   |
|   4|[0x80002248]<br>0x0000000000000000|- rs1 : f26<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 2  #nosat<br>                                        |[0x800003fc]:fsqrt.s fa3, fs10, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:fsw fa3, 48(a5)<br> [0x80000408]:sd a7, 56(a5)<br>    |
|   5|[0x80002258]<br>0x0000000000000000|- rs1 : f19<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 1  #nosat<br>                                         |[0x80000414]:fsqrt.s ft1, fs3, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsw ft1, 64(a5)<br> [0x80000420]:sd a7, 72(a5)<br>     |
|   6|[0x80002268]<br>0x0000000000000000|- rs1 : f27<br> - rd : f29<br>                                                                                                                   |[0x8000042c]:fsqrt.s ft9, fs11, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsw ft9, 80(a5)<br> [0x80000438]:sd a7, 88(a5)<br>    |
|   7|[0x80002278]<br>0x0000000000000000|- rs1 : f22<br> - rd : f7<br>                                                                                                                    |[0x80000444]:fsqrt.s ft7, fs6, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsw ft7, 96(a5)<br> [0x80000450]:sd a7, 104(a5)<br>    |
|   8|[0x80002288]<br>0x0000000000000000|- rs1 : f29<br> - rd : f19<br>                                                                                                                   |[0x8000045c]:fsqrt.s fs3, ft9, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:fsw fs3, 112(a5)<br> [0x80000468]:sd a7, 120(a5)<br>   |
|   9|[0x80002298]<br>0x0000000000000000|- rs1 : f17<br> - rd : f18<br>                                                                                                                   |[0x80000474]:fsqrt.s fs2, fa7, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsw fs2, 128(a5)<br> [0x80000480]:sd a7, 136(a5)<br>   |
|  10|[0x800022a8]<br>0x0000000000000000|- rs1 : f1<br> - rd : f4<br>                                                                                                                     |[0x8000048c]:fsqrt.s ft4, ft1, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsw ft4, 144(a5)<br> [0x80000498]:sd a7, 152(a5)<br>   |
|  11|[0x800022b8]<br>0x0000000000000000|- rs1 : f10<br> - rd : f25<br>                                                                                                                   |[0x800004a4]:fsqrt.s fs9, fa0, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsw fs9, 160(a5)<br> [0x800004b0]:sd a7, 168(a5)<br>   |
|  12|[0x800022c8]<br>0x0000000000000000|- rs1 : f25<br> - rd : f8<br>                                                                                                                    |[0x800004bc]:fsqrt.s fs0, fs9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsw fs0, 176(a5)<br> [0x800004c8]:sd a7, 184(a5)<br>   |
|  13|[0x800022d8]<br>0x0000000000000000|- rs1 : f23<br> - rd : f17<br>                                                                                                                   |[0x800004d4]:fsqrt.s fa7, fs7, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsw fa7, 192(a5)<br> [0x800004e0]:sd a7, 200(a5)<br>   |
|  14|[0x800022e8]<br>0x0000000000000000|- rs1 : f5<br> - rd : f3<br>                                                                                                                     |[0x800004ec]:fsqrt.s ft3, ft5, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsw ft3, 208(a5)<br> [0x800004f8]:sd a7, 216(a5)<br>   |
|  15|[0x800022f8]<br>0x0000000000000000|- rs1 : f15<br> - rd : f5<br>                                                                                                                    |[0x80000504]:fsqrt.s ft5, fa5, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsw ft5, 224(a5)<br> [0x80000510]:sd a7, 232(a5)<br>   |
|  16|[0x80002308]<br>0x0000000000000000|- rs1 : f20<br> - rd : f11<br>                                                                                                                   |[0x8000051c]:fsqrt.s fa1, fs4, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsw fa1, 240(a5)<br> [0x80000528]:sd a7, 248(a5)<br>   |
|  17|[0x80002318]<br>0x0000000000000000|- rs1 : f18<br> - rd : f15<br>                                                                                                                   |[0x80000534]:fsqrt.s fa5, fs2, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsw fa5, 256(a5)<br> [0x80000540]:sd a7, 264(a5)<br>   |
|  18|[0x80002328]<br>0x0000000000000000|- rs1 : f24<br> - rd : f27<br>                                                                                                                   |[0x8000054c]:fsqrt.s fs11, fs8, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:fsw fs11, 272(a5)<br> [0x80000558]:sd a7, 280(a5)<br> |
|  19|[0x80002338]<br>0x0000000000000000|- rs1 : f4<br> - rd : f24<br>                                                                                                                    |[0x80000564]:fsqrt.s fs8, ft4, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsw fs8, 288(a5)<br> [0x80000570]:sd a7, 296(a5)<br>   |
|  20|[0x80002348]<br>0x0000000000000000|- rs1 : f11<br> - rd : f21<br>                                                                                                                   |[0x8000057c]:fsqrt.s fs5, fa1, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:fsw fs5, 304(a5)<br> [0x80000588]:sd a7, 312(a5)<br>   |
|  21|[0x80002358]<br>0x0000000000000000|- rs1 : f31<br> - rd : f16<br>                                                                                                                   |[0x80000594]:fsqrt.s fa6, ft11, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsw fa6, 320(a5)<br> [0x800005a0]:sd a7, 328(a5)<br>  |
|  22|[0x80002368]<br>0x0000000000000000|- rs1 : f13<br> - rd : f2<br>                                                                                                                    |[0x800005ac]:fsqrt.s ft2, fa3, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:fsw ft2, 336(a5)<br> [0x800005b8]:sd a7, 344(a5)<br>   |
|  23|[0x80002378]<br>0x0000000000000000|- rs1 : f14<br> - rd : f23<br>                                                                                                                   |[0x800005c4]:fsqrt.s fs7, fa4, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsw fs7, 352(a5)<br> [0x800005d0]:sd a7, 360(a5)<br>   |
|  24|[0x80002388]<br>0x0000000000000000|- rs1 : f8<br> - rd : f30<br>                                                                                                                    |[0x800005dc]:fsqrt.s ft10, fs0, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:fsw ft10, 368(a5)<br> [0x800005e8]:sd a7, 376(a5)<br> |
|  25|[0x80002398]<br>0x0000000000000000|- rs1 : f0<br> - rd : f9<br>                                                                                                                     |[0x800005f4]:fsqrt.s fs1, ft0, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:fsw fs1, 384(a5)<br> [0x80000600]:sd a7, 392(a5)<br>   |
|  26|[0x800023a8]<br>0x0000000000000000|- rs1 : f16<br> - rd : f10<br>                                                                                                                   |[0x8000060c]:fsqrt.s fa0, fa6, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsw fa0, 400(a5)<br> [0x80000618]:sd a7, 408(a5)<br>   |
|  27|[0x800023b8]<br>0x0000000000000000|- rs1 : f3<br> - rd : f12<br>                                                                                                                    |[0x80000624]:fsqrt.s fa2, ft3, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsw fa2, 416(a5)<br> [0x80000630]:sd a7, 424(a5)<br>   |
|  28|[0x800023c8]<br>0x0000000000000000|- rs1 : f30<br> - rd : f28<br>                                                                                                                   |[0x8000063c]:fsqrt.s ft8, ft10, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsw ft8, 432(a5)<br> [0x80000648]:sd a7, 440(a5)<br>  |
|  29|[0x800023d8]<br>0x0000000000000000|- rs1 : f2<br> - rd : f22<br>                                                                                                                    |[0x80000654]:fsqrt.s fs6, ft2, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:fsw fs6, 448(a5)<br> [0x80000660]:sd a7, 456(a5)<br>   |
|  30|[0x800023e8]<br>0x0000000000000000|- rs1 : f21<br> - rd : f14<br>                                                                                                                   |[0x8000066c]:fsqrt.s fa4, fs5, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:fsw fa4, 464(a5)<br> [0x80000678]:sd a7, 472(a5)<br>   |
|  31|[0x800023f8]<br>0x0000000000000000|- rs1 : f12<br> - rd : f31<br>                                                                                                                   |[0x80000684]:fsqrt.s ft11, fa2, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsw ft11, 480(a5)<br> [0x80000690]:sd a7, 488(a5)<br> |
|  32|[0x80002408]<br>0x0000000000000000|- rs1 : f9<br> - rd : f0<br>                                                                                                                     |[0x8000069c]:fsqrt.s ft0, fs1, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:fsw ft0, 496(a5)<br> [0x800006a8]:sd a7, 504(a5)<br>   |
