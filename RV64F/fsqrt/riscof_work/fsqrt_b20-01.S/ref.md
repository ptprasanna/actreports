
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800009d0')]      |
| SIG_REGION                | [('0x80002310', '0x80002720', '130 dwords')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fsqrt/riscof_work/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 137     |
| Total Coverpoints Hit     | 132      |
| Total Signature Updates   | 65      |
| STAT1                     | 65      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                  coverpoints                                                                   |                                                                         code                                                                         |
|---:|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000001|- opcode : fsqrt.s<br> - rs1 : f9<br> - rd : f12<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and rm_val == 0  #nosat<br> |[0x800003b4]:fsqrt.s fa2, fs1, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsw fa2, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>          |
|   2|[0x80002328]<br>0x0000000000000001|- rs1 : f23<br> - rd : f23<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                       |[0x800003cc]:fsqrt.s fs7, fs7, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsw fs7, 16(a5)<br> [0x800003d8]:sd a7, 24(a5)<br>        |
|   3|[0x80002338]<br>0x0000000000000001|- rs1 : f10<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x9e and fm1 == 0x38d874 and rm_val == 0  #nosat<br>                                       |[0x800003e4]:fsqrt.s fa7, fa0, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsw fa7, 32(a5)<br> [0x800003f0]:sd a7, 40(a5)<br>        |
|   4|[0x80002348]<br>0x0000000000000001|- rs1 : f4<br> - rd : f8<br> - fs1 == 0 and fe1 == 0xd0 and fm1 == 0x010151 and rm_val == 0  #nosat<br>                                         |[0x800003fc]:fsqrt.s fs0, ft4, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:fsw fs0, 48(a5)<br> [0x80000408]:sd a7, 56(a5)<br>        |
|   5|[0x80002358]<br>0x0000000000000001|- rs1 : f3<br> - rd : f4<br> - fs1 == 0 and fe1 == 0xb7 and fm1 == 0x4bce51 and rm_val == 0  #nosat<br>                                         |[0x80000414]:fsqrt.s ft4, ft3, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsw ft4, 64(a5)<br> [0x80000420]:sd a7, 72(a5)<br>        |
|   6|[0x80002368]<br>0x0000000000000001|- rs1 : f19<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x30 and fm1 == 0x75cb89 and rm_val == 0  #nosat<br>                                       |[0x8000042c]:fsqrt.s ft9, fs3, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsw ft9, 80(a5)<br> [0x80000438]:sd a7, 88(a5)<br>        |
|   7|[0x80002378]<br>0x0000000000000001|- rs1 : f11<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x79 and fm1 == 0x785c55 and rm_val == 0  #nosat<br>                                       |[0x80000444]:fsqrt.s ft11, fa1, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsw ft11, 96(a5)<br> [0x80000450]:sd a7, 104(a5)<br>     |
|   8|[0x80002388]<br>0x0000000000000001|- rs1 : f18<br> - rd : f28<br> - fs1 == 0 and fe1 == 0xac and fm1 == 0x13884e and rm_val == 0  #nosat<br>                                       |[0x8000045c]:fsqrt.s ft8, fs2, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:fsw ft8, 112(a5)<br> [0x80000468]:sd a7, 120(a5)<br>      |
|   9|[0x80002398]<br>0x0000000000000001|- rs1 : f21<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x65 and fm1 == 0x064562 and rm_val == 0  #nosat<br>                                       |[0x80000474]:fsqrt.s fa6, fs5, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsw fa6, 128(a5)<br> [0x80000480]:sd a7, 136(a5)<br>      |
|  10|[0x800023a8]<br>0x0000000000000001|- rs1 : f6<br> - rd : f10<br> - fs1 == 0 and fe1 == 0xd3 and fm1 == 0x190acf and rm_val == 0  #nosat<br>                                        |[0x8000048c]:fsqrt.s fa0, ft6, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsw fa0, 144(a5)<br> [0x80000498]:sd a7, 152(a5)<br>      |
|  11|[0x800023b8]<br>0x0000000000000001|- rs1 : f1<br> - rd : f5<br> - fs1 == 0 and fe1 == 0xea and fm1 == 0x284ae6 and rm_val == 0  #nosat<br>                                         |[0x800004a4]:fsqrt.s ft5, ft1, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsw ft5, 160(a5)<br> [0x800004b0]:sd a7, 168(a5)<br>      |
|  12|[0x800023c8]<br>0x0000000000000001|- rs1 : f22<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x4e and fm1 == 0x454542 and rm_val == 0  #nosat<br>                                        |[0x800004bc]:fsqrt.s ft3, fs6, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsw ft3, 176(a5)<br> [0x800004c8]:sd a7, 184(a5)<br>      |
|  13|[0x800023d8]<br>0x0000000000000001|- rs1 : f20<br> - rd : f9<br> - fs1 == 0 and fe1 == 0xb6 and fm1 == 0x479816 and rm_val == 0  #nosat<br>                                        |[0x800004d4]:fsqrt.s fs1, fs4, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsw fs1, 192(a5)<br> [0x800004e0]:sd a7, 200(a5)<br>      |
|  14|[0x800023e8]<br>0x0000000000000001|- rs1 : f7<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x65 and fm1 == 0x5b1e82 and rm_val == 0  #nosat<br>                                         |[0x800004ec]:fsqrt.s ft6, ft7, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsw ft6, 208(a5)<br> [0x800004f8]:sd a7, 216(a5)<br>      |
|  15|[0x800023f8]<br>0x0000000000000001|- rs1 : f27<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7b and fm1 == 0x64e1f0 and rm_val == 0  #nosat<br>                                       |[0x80000504]:fsqrt.s fs5, fs11, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsw fs5, 224(a5)<br> [0x80000510]:sd a7, 232(a5)<br>     |
|  16|[0x80002408]<br>0x0000000000000001|- rs1 : f16<br> - rd : f2<br> - fs1 == 0 and fe1 == 0xc2 and fm1 == 0x26f9c3 and rm_val == 0  #nosat<br>                                        |[0x8000051c]:fsqrt.s ft2, fa6, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsw ft2, 240(a5)<br> [0x80000528]:sd a7, 248(a5)<br>      |
|  17|[0x80002418]<br>0x0000000000000001|- rs1 : f14<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x31 and fm1 == 0x011313 and rm_val == 0  #nosat<br>                                       |[0x80000534]:fsqrt.s fs8, fa4, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsw fs8, 256(a5)<br> [0x80000540]:sd a7, 264(a5)<br>      |
|  18|[0x80002428]<br>0x0000000000000001|- rs1 : f0<br> - rd : f14<br> - fs1 == 0 and fe1 == 0xad and fm1 == 0x75bbd8 and rm_val == 0  #nosat<br>                                        |[0x8000054c]:fsqrt.s fa4, ft0, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:fsw fa4, 272(a5)<br> [0x80000558]:sd a7, 280(a5)<br>      |
|  19|[0x80002438]<br>0x0000000000000001|- rs1 : f5<br> - rd : f22<br> - fs1 == 0 and fe1 == 0xe4 and fm1 == 0x1477dc and rm_val == 0  #nosat<br>                                        |[0x80000564]:fsqrt.s fs6, ft5, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsw fs6, 288(a5)<br> [0x80000570]:sd a7, 296(a5)<br>      |
|  20|[0x80002448]<br>0x0000000000000001|- rs1 : f29<br> - rd : f20<br> - fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and rm_val == 0  #nosat<br>                                       |[0x8000057c]:fsqrt.s fs4, ft9, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:fsw fs4, 304(a5)<br> [0x80000588]:sd a7, 312(a5)<br>      |
|  21|[0x80002458]<br>0x0000000000000001|- rs1 : f25<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and rm_val == 0  #nosat<br>                                       |[0x80000594]:fsqrt.s fs11, fs9, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsw fs11, 320(a5)<br> [0x800005a0]:sd a7, 328(a5)<br>    |
|  22|[0x80002468]<br>0x0000000000000001|- rs1 : f28<br> - rd : f11<br> - fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and rm_val == 0  #nosat<br>                                       |[0x800005ac]:fsqrt.s fa1, ft8, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:fsw fa1, 336(a5)<br> [0x800005b8]:sd a7, 344(a5)<br>      |
|  23|[0x80002478]<br>0x0000000000000001|- rs1 : f8<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and rm_val == 0  #nosat<br>                                        |[0x800005c4]:fsqrt.s fs10, fs0, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsw fs10, 352(a5)<br> [0x800005d0]:sd a7, 360(a5)<br>    |
|  24|[0x80002488]<br>0x0000000000000001|- rs1 : f15<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and rm_val == 0  #nosat<br>                                        |[0x800005dc]:fsqrt.s ft1, fa5, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:fsw ft1, 368(a5)<br> [0x800005e8]:sd a7, 376(a5)<br>      |
|  25|[0x80002498]<br>0x0000000000000001|- rs1 : f13<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and rm_val == 0  #nosat<br>                                       |[0x800005f4]:fsqrt.s fs9, fa3, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:fsw fs9, 384(a5)<br> [0x80000600]:sd a7, 392(a5)<br>      |
|  26|[0x800024a8]<br>0x0000000000000001|- rs1 : f31<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and rm_val == 0  #nosat<br>                                       |[0x8000060c]:fsqrt.s fa3, ft11, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsw fa3, 400(a5)<br> [0x80000618]:sd a7, 408(a5)<br>     |
|  27|[0x800024b8]<br>0x0000000000000001|- rs1 : f12<br> - rd : f30<br> - fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and rm_val == 0  #nosat<br>                                       |[0x80000624]:fsqrt.s ft10, fa2, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsw ft10, 416(a5)<br> [0x80000630]:sd a7, 424(a5)<br>    |
|  28|[0x800024c8]<br>0x0000000000000001|- rs1 : f30<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and rm_val == 0  #nosat<br>                                       |[0x8000063c]:fsqrt.s fa5, ft10, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsw fa5, 432(a5)<br> [0x80000648]:sd a7, 440(a5)<br>     |
|  29|[0x800024d8]<br>0x0000000000000001|- rs1 : f24<br> - rd : f18<br> - fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and rm_val == 0  #nosat<br>                                       |[0x80000654]:fsqrt.s fs2, fs8, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:fsw fs2, 448(a5)<br> [0x80000660]:sd a7, 456(a5)<br>      |
|  30|[0x800024e8]<br>0x0000000000000001|- rs1 : f17<br> - rd : f19<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and rm_val == 0  #nosat<br>                                       |[0x8000066c]:fsqrt.s fs3, fa7, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:fsw fs3, 464(a5)<br> [0x80000678]:sd a7, 472(a5)<br>      |
|  31|[0x800024f8]<br>0x0000000000000001|- rs1 : f26<br> - rd : f0<br> - fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and rm_val == 0  #nosat<br>                                        |[0x80000684]:fsqrt.s ft0, fs10, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsw ft0, 480(a5)<br> [0x80000690]:sd a7, 488(a5)<br>     |
|  32|[0x80002508]<br>0x0000000000000001|- rs1 : f2<br> - rd : f7<br> - fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and rm_val == 0  #nosat<br>                                         |[0x8000069c]:fsqrt.s ft7, ft2, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:fsw ft7, 496(a5)<br> [0x800006a8]:sd a7, 504(a5)<br>      |
|  33|[0x80002518]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and rm_val == 0  #nosat<br>                                                                      |[0x800006b4]:fsqrt.s ft11, ft10, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsw ft11, 512(a5)<br> [0x800006c0]:sd a7, 520(a5)<br>   |
|  34|[0x80002528]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and rm_val == 0  #nosat<br>                                                                      |[0x800006cc]:fsqrt.s ft11, ft10, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:fsw ft11, 528(a5)<br> [0x800006d8]:sd a7, 536(a5)<br>   |
|  35|[0x80002538]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and rm_val == 0  #nosat<br>                                                                      |[0x800006e4]:fsqrt.s ft11, ft10, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsw ft11, 544(a5)<br> [0x800006f0]:sd a7, 552(a5)<br>   |
|  36|[0x80002548]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xce and fm1 == 0x1168e1 and rm_val == 0  #nosat<br>                                                                      |[0x800006fc]:fsqrt.s ft11, ft10, dyn<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:fsw ft11, 560(a5)<br> [0x80000708]:sd a7, 568(a5)<br>   |
|  37|[0x80002558]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and rm_val == 0  #nosat<br>                                                                      |[0x80000714]:fsqrt.s ft11, ft10, dyn<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:fsw ft11, 576(a5)<br> [0x80000720]:sd a7, 584(a5)<br>   |
|  38|[0x80002568]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and rm_val == 0  #nosat<br>                                                                      |[0x8000072c]:fsqrt.s ft11, ft10, dyn<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:fsw ft11, 592(a5)<br> [0x80000738]:sd a7, 600(a5)<br>   |
|  39|[0x80002578]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and rm_val == 0  #nosat<br>                                                                      |[0x80000744]:fsqrt.s ft11, ft10, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsw ft11, 608(a5)<br> [0x80000750]:sd a7, 616(a5)<br>   |
|  40|[0x80002588]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and rm_val == 0  #nosat<br>                                                                      |[0x8000075c]:fsqrt.s ft11, ft10, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsw ft11, 624(a5)<br> [0x80000768]:sd a7, 632(a5)<br>   |
|  41|[0x80002598]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and rm_val == 0  #nosat<br>                                                                      |[0x80000774]:fsqrt.s ft11, ft10, dyn<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:fsw ft11, 640(a5)<br> [0x80000780]:sd a7, 648(a5)<br>   |
|  42|[0x800025a8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and rm_val == 0  #nosat<br>                                                                      |[0x8000078c]:fsqrt.s ft11, ft10, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:fsw ft11, 656(a5)<br> [0x80000798]:sd a7, 664(a5)<br>   |
|  43|[0x800025b8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and rm_val == 0  #nosat<br>                                                                      |[0x800007a4]:fsqrt.s ft11, ft10, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsw ft11, 672(a5)<br> [0x800007b0]:sd a7, 680(a5)<br>   |
|  44|[0x800025c8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and rm_val == 0  #nosat<br>                                                                      |[0x800007bc]:fsqrt.s ft11, ft10, dyn<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:fsw ft11, 688(a5)<br> [0x800007c8]:sd a7, 696(a5)<br>   |
|  45|[0x800025d8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and rm_val == 0  #nosat<br>                                                                      |[0x800007d4]:fsqrt.s ft11, ft10, dyn<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:fsw ft11, 704(a5)<br> [0x800007e0]:sd a7, 712(a5)<br>   |
|  46|[0x800025e8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and rm_val == 0  #nosat<br>                                                                      |[0x800007ec]:fsqrt.s ft11, ft10, dyn<br> [0x800007f0]:csrrs a7, fflags, zero<br> [0x800007f4]:fsw ft11, 720(a5)<br> [0x800007f8]:sd a7, 728(a5)<br>   |
|  47|[0x800025f8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and rm_val == 0  #nosat<br>                                                                      |[0x80000804]:fsqrt.s ft11, ft10, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsw ft11, 736(a5)<br> [0x80000810]:sd a7, 744(a5)<br>   |
|  48|[0x80002608]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and rm_val == 0  #nosat<br>                                                                      |[0x8000081c]:fsqrt.s ft11, ft10, dyn<br> [0x80000820]:csrrs a7, fflags, zero<br> [0x80000824]:fsw ft11, 752(a5)<br> [0x80000828]:sd a7, 760(a5)<br>   |
|  49|[0x80002618]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and rm_val == 0  #nosat<br>                                                                      |[0x80000834]:fsqrt.s ft11, ft10, dyn<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:fsw ft11, 768(a5)<br> [0x80000840]:sd a7, 776(a5)<br>   |
|  50|[0x80002628]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and rm_val == 0  #nosat<br>                                                                      |[0x8000084c]:fsqrt.s ft11, ft10, dyn<br> [0x80000850]:csrrs a7, fflags, zero<br> [0x80000854]:fsw ft11, 784(a5)<br> [0x80000858]:sd a7, 792(a5)<br>   |
|  51|[0x80002638]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and rm_val == 0  #nosat<br>                                                                      |[0x80000864]:fsqrt.s ft11, ft10, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsw ft11, 800(a5)<br> [0x80000870]:sd a7, 808(a5)<br>   |
|  52|[0x80002648]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and rm_val == 0  #nosat<br>                                                                      |[0x8000087c]:fsqrt.s ft11, ft10, dyn<br> [0x80000880]:csrrs a7, fflags, zero<br> [0x80000884]:fsw ft11, 816(a5)<br> [0x80000888]:sd a7, 824(a5)<br>   |
|  53|[0x80002658]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and rm_val == 0  #nosat<br>                                                                      |[0x80000894]:fsqrt.s ft11, ft10, dyn<br> [0x80000898]:csrrs a7, fflags, zero<br> [0x8000089c]:fsw ft11, 832(a5)<br> [0x800008a0]:sd a7, 840(a5)<br>   |
|  54|[0x80002668]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and rm_val == 0  #nosat<br>                                                                      |[0x800008ac]:fsqrt.s ft11, ft10, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsw ft11, 848(a5)<br> [0x800008b8]:sd a7, 856(a5)<br>   |
|  55|[0x80002678]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and rm_val == 0  #nosat<br>                                                                      |[0x800008c4]:fsqrt.s ft11, ft10, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsw ft11, 864(a5)<br> [0x800008d0]:sd a7, 872(a5)<br>   |
|  56|[0x80002688]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and rm_val == 0  #nosat<br>                                                                      |[0x800008dc]:fsqrt.s ft11, ft10, dyn<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:fsw ft11, 880(a5)<br> [0x800008e8]:sd a7, 888(a5)<br>   |
|  57|[0x80002698]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and rm_val == 0  #nosat<br>                                                                      |[0x800008f4]:fsqrt.s ft11, ft10, dyn<br> [0x800008f8]:csrrs a7, fflags, zero<br> [0x800008fc]:fsw ft11, 896(a5)<br> [0x80000900]:sd a7, 904(a5)<br>   |
|  58|[0x800026a8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and rm_val == 0  #nosat<br>                                                                      |[0x8000090c]:fsqrt.s ft11, ft10, dyn<br> [0x80000910]:csrrs a7, fflags, zero<br> [0x80000914]:fsw ft11, 912(a5)<br> [0x80000918]:sd a7, 920(a5)<br>   |
|  59|[0x800026b8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and rm_val == 0  #nosat<br>                                                                      |[0x80000924]:fsqrt.s ft11, ft10, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsw ft11, 928(a5)<br> [0x80000930]:sd a7, 936(a5)<br>   |
|  60|[0x800026c8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and rm_val == 0  #nosat<br>                                                                      |[0x8000093c]:fsqrt.s ft11, ft10, dyn<br> [0x80000940]:csrrs a7, fflags, zero<br> [0x80000944]:fsw ft11, 944(a5)<br> [0x80000948]:sd a7, 952(a5)<br>   |
|  61|[0x800026d8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and rm_val == 0  #nosat<br>                                                                      |[0x80000954]:fsqrt.s ft11, ft10, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsw ft11, 960(a5)<br> [0x80000960]:sd a7, 968(a5)<br>   |
|  62|[0x800026e8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and rm_val == 0  #nosat<br>                                                                      |[0x8000096c]:fsqrt.s ft11, ft10, dyn<br> [0x80000970]:csrrs a7, fflags, zero<br> [0x80000974]:fsw ft11, 976(a5)<br> [0x80000978]:sd a7, 984(a5)<br>   |
|  63|[0x800026f8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and rm_val == 0  #nosat<br>                                                                      |[0x80000984]:fsqrt.s ft11, ft10, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsw ft11, 992(a5)<br> [0x80000990]:sd a7, 1000(a5)<br>  |
|  64|[0x80002708]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and rm_val == 0  #nosat<br>                                                                      |[0x8000099c]:fsqrt.s ft11, ft10, dyn<br> [0x800009a0]:csrrs a7, fflags, zero<br> [0x800009a4]:fsw ft11, 1008(a5)<br> [0x800009a8]:sd a7, 1016(a5)<br> |
|  65|[0x80002718]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and rm_val == 0  #nosat<br>                                                                      |[0x800009b4]:fsqrt.s ft11, ft10, dyn<br> [0x800009b8]:csrrs a7, fflags, zero<br> [0x800009bc]:fsw ft11, 1024(a5)<br> [0x800009c0]:sd a7, 1032(a5)<br> |
