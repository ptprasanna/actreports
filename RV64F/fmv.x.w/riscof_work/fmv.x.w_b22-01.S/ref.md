
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000710')]      |
| SIG_REGION                | [('0x80002210', '0x80002420', '66 dwords')]      |
| COV_LABELS                | fmv.x.w_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fmv.x.w/riscof_work/fmv.x.w_b22-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 66      |
| STAT1                     | 32      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 33     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800006fc]:fmv.x.w t6, ft11
      [0x80000700]:csrrs a7, fflags, zero
      [0x80000704]:sd t6, 272(a5)
 -- Signature Address: 0x80002410 Data: 0x000000003E4923B8
 -- Redundant Coverpoints hit by the op
      - opcode : fmv.x.w
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fmv.x.w', 'rd : x0', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fmv.x.w zero, fs8
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd zero, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002218]:0x0000000000000000




Last Coverpoint : ['rd : x15', 'rs1 : f3', 'fs1 == 0 and fe1 == 0xc4 and fm1 == 0x046756 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d8]:fmv.x.w a5, ft3
	-[0x800003dc]:csrrs s5, fflags, zero
	-[0x800003e0]:sd a5, 0(s3)
Current Store : [0x800003e4] : sd s5, 8(s3) -- Store: [0x80002228]:0x0000000000000000




Last Coverpoint : ['rd : x13', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x67 and fm1 == 0x53a4fc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fmv.x.w a3, ft10
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd a3, 0(a5)
Current Store : [0x80000408] : sd a7, 8(a5) -- Store: [0x80002238]:0x0000000000000000




Last Coverpoint : ['rd : x18', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000414]:fmv.x.w s2, fa2
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd s2, 16(a5)
Current Store : [0x80000420] : sd a7, 24(a5) -- Store: [0x80002248]:0x0000000000000000




Last Coverpoint : ['rd : x30', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fmv.x.w t5, fs7
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd t5, 32(a5)
Current Store : [0x80000438] : sd a7, 40(a5) -- Store: [0x80002258]:0x0000000000000000




Last Coverpoint : ['rd : x1', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fmv.x.w ra, fs4
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd ra, 48(a5)
Current Store : [0x80000450] : sd a7, 56(a5) -- Store: [0x80002268]:0x0000000000000000




Last Coverpoint : ['rd : x16', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000468]:fmv.x.w a6, fa3
	-[0x8000046c]:csrrs s5, fflags, zero
	-[0x80000470]:sd a6, 0(s3)
Current Store : [0x80000474] : sd s5, 8(s3) -- Store: [0x80002278]:0x0000000000000000




Last Coverpoint : ['rd : x8', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000048c]:fmv.x.w fp, fa4
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd fp, 0(a5)
Current Store : [0x80000498] : sd a7, 8(a5) -- Store: [0x80002288]:0x0000000000000000




Last Coverpoint : ['rd : x19', 'rs1 : f15']
Last Code Sequence : 
	-[0x800004a4]:fmv.x.w s3, fa5
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s3, 16(a5)
Current Store : [0x800004b0] : sd a7, 24(a5) -- Store: [0x80002298]:0x0000000000000000




Last Coverpoint : ['rd : x28', 'rs1 : f4']
Last Code Sequence : 
	-[0x800004bc]:fmv.x.w t3, ft4
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd t3, 32(a5)
Current Store : [0x800004c8] : sd a7, 40(a5) -- Store: [0x800022a8]:0x0000000000000000




Last Coverpoint : ['rd : x10', 'rs1 : f0']
Last Code Sequence : 
	-[0x800004d4]:fmv.x.w a0, ft0
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd a0, 48(a5)
Current Store : [0x800004e0] : sd a7, 56(a5) -- Store: [0x800022b8]:0x0000000000000000




Last Coverpoint : ['rd : x25', 'rs1 : f17']
Last Code Sequence : 
	-[0x800004ec]:fmv.x.w s9, fa7
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd s9, 64(a5)
Current Store : [0x800004f8] : sd a7, 72(a5) -- Store: [0x800022c8]:0x0000000000000000




Last Coverpoint : ['rd : x23', 'rs1 : f9']
Last Code Sequence : 
	-[0x80000504]:fmv.x.w s7, fs1
	-[0x80000508]:csrrs a7, fflags, zero
	-[0x8000050c]:sd s7, 80(a5)
Current Store : [0x80000510] : sd a7, 88(a5) -- Store: [0x800022d8]:0x0000000000000000




Last Coverpoint : ['rd : x4', 'rs1 : f21']
Last Code Sequence : 
	-[0x8000051c]:fmv.x.w tp, fs5
	-[0x80000520]:csrrs a7, fflags, zero
	-[0x80000524]:sd tp, 96(a5)
Current Store : [0x80000528] : sd a7, 104(a5) -- Store: [0x800022e8]:0x0000000000000000




Last Coverpoint : ['rd : x17', 'rs1 : f27']
Last Code Sequence : 
	-[0x80000540]:fmv.x.w a7, fs11
	-[0x80000544]:csrrs s5, fflags, zero
	-[0x80000548]:sd a7, 0(s3)
Current Store : [0x8000054c] : sd s5, 8(s3) -- Store: [0x800022f8]:0x0000000000000000




Last Coverpoint : ['rd : x29', 'rs1 : f29']
Last Code Sequence : 
	-[0x80000564]:fmv.x.w t4, ft9
	-[0x80000568]:csrrs a7, fflags, zero
	-[0x8000056c]:sd t4, 0(a5)
Current Store : [0x80000570] : sd a7, 8(a5) -- Store: [0x80002308]:0x0000000000000000




Last Coverpoint : ['rd : x24', 'rs1 : f5']
Last Code Sequence : 
	-[0x8000057c]:fmv.x.w s8, ft5
	-[0x80000580]:csrrs a7, fflags, zero
	-[0x80000584]:sd s8, 16(a5)
Current Store : [0x80000588] : sd a7, 24(a5) -- Store: [0x80002318]:0x0000000000000000




Last Coverpoint : ['rd : x11', 'rs1 : f1']
Last Code Sequence : 
	-[0x80000594]:fmv.x.w a1, ft1
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd a1, 32(a5)
Current Store : [0x800005a0] : sd a7, 40(a5) -- Store: [0x80002328]:0x0000000000000000




Last Coverpoint : ['rd : x22', 'rs1 : f19']
Last Code Sequence : 
	-[0x800005ac]:fmv.x.w s6, fs3
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd s6, 48(a5)
Current Store : [0x800005b8] : sd a7, 56(a5) -- Store: [0x80002338]:0x0000000000000000




Last Coverpoint : ['rd : x31', 'rs1 : f10']
Last Code Sequence : 
	-[0x800005c4]:fmv.x.w t6, fa0
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd t6, 64(a5)
Current Store : [0x800005d0] : sd a7, 72(a5) -- Store: [0x80002348]:0x0000000000000000




Last Coverpoint : ['rd : x12', 'rs1 : f18']
Last Code Sequence : 
	-[0x800005dc]:fmv.x.w a2, fs2
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd a2, 80(a5)
Current Store : [0x800005e8] : sd a7, 88(a5) -- Store: [0x80002358]:0x0000000000000000




Last Coverpoint : ['rd : x26', 'rs1 : f26']
Last Code Sequence : 
	-[0x800005f4]:fmv.x.w s10, fs10
	-[0x800005f8]:csrrs a7, fflags, zero
	-[0x800005fc]:sd s10, 96(a5)
Current Store : [0x80000600] : sd a7, 104(a5) -- Store: [0x80002368]:0x0000000000000000




Last Coverpoint : ['rd : x27', 'rs1 : f28']
Last Code Sequence : 
	-[0x8000060c]:fmv.x.w s11, ft8
	-[0x80000610]:csrrs a7, fflags, zero
	-[0x80000614]:sd s11, 112(a5)
Current Store : [0x80000618] : sd a7, 120(a5) -- Store: [0x80002378]:0x0000000000000000




Last Coverpoint : ['rd : x14', 'rs1 : f6']
Last Code Sequence : 
	-[0x80000624]:fmv.x.w a4, ft6
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd a4, 128(a5)
Current Store : [0x80000630] : sd a7, 136(a5) -- Store: [0x80002388]:0x0000000000000000




Last Coverpoint : ['rd : x20', 'rs1 : f16']
Last Code Sequence : 
	-[0x8000063c]:fmv.x.w s4, fa6
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd s4, 144(a5)
Current Store : [0x80000648] : sd a7, 152(a5) -- Store: [0x80002398]:0x0000000000000000




Last Coverpoint : ['rd : x5', 'rs1 : f31']
Last Code Sequence : 
	-[0x80000654]:fmv.x.w t0, ft11
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd t0, 160(a5)
Current Store : [0x80000660] : sd a7, 168(a5) -- Store: [0x800023a8]:0x0000000000000000




Last Coverpoint : ['rd : x9', 'rs1 : f7']
Last Code Sequence : 
	-[0x8000066c]:fmv.x.w s1, ft7
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd s1, 176(a5)
Current Store : [0x80000678] : sd a7, 184(a5) -- Store: [0x800023b8]:0x0000000000000000




Last Coverpoint : ['rd : x7', 'rs1 : f25']
Last Code Sequence : 
	-[0x80000684]:fmv.x.w t2, fs9
	-[0x80000688]:csrrs a7, fflags, zero
	-[0x8000068c]:sd t2, 192(a5)
Current Store : [0x80000690] : sd a7, 200(a5) -- Store: [0x800023c8]:0x0000000000000000




Last Coverpoint : ['rd : x2', 'rs1 : f2']
Last Code Sequence : 
	-[0x8000069c]:fmv.x.w sp, ft2
	-[0x800006a0]:csrrs a7, fflags, zero
	-[0x800006a4]:sd sp, 208(a5)
Current Store : [0x800006a8] : sd a7, 216(a5) -- Store: [0x800023d8]:0x0000000000000000




Last Coverpoint : ['rd : x21', 'rs1 : f11']
Last Code Sequence : 
	-[0x800006b4]:fmv.x.w s5, fa1
	-[0x800006b8]:csrrs a7, fflags, zero
	-[0x800006bc]:sd s5, 224(a5)
Current Store : [0x800006c0] : sd a7, 232(a5) -- Store: [0x800023e8]:0x0000000000000000




Last Coverpoint : ['rd : x6', 'rs1 : f8']
Last Code Sequence : 
	-[0x800006cc]:fmv.x.w t1, fs0
	-[0x800006d0]:csrrs a7, fflags, zero
	-[0x800006d4]:sd t1, 240(a5)
Current Store : [0x800006d8] : sd a7, 248(a5) -- Store: [0x800023f8]:0x0000000000000000




Last Coverpoint : ['rd : x3', 'rs1 : f22']
Last Code Sequence : 
	-[0x800006e4]:fmv.x.w gp, fs6
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd gp, 256(a5)
Current Store : [0x800006f0] : sd a7, 264(a5) -- Store: [0x80002408]:0x0000000000000000




Last Coverpoint : ['opcode : fmv.x.w', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006fc]:fmv.x.w t6, ft11
	-[0x80000700]:csrrs a7, fflags, zero
	-[0x80000704]:sd t6, 272(a5)
Current Store : [0x80000708] : sd a7, 280(a5) -- Store: [0x80002418]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                          coverpoints                                                           |                                                   code                                                    |
|---:|----------------------------------|--------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x0000000000000000|- opcode : fmv.x.w<br> - rd : x0<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br> |[0x800003b4]:fmv.x.w zero, fs8<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd zero, 0(a5)<br> |
|   2|[0x80002220]<br>0x0000000062046756|- rd : x15<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0xc4 and fm1 == 0x046756 and rm_val == 0  #nosat<br>                        |[0x800003d8]:fmv.x.w a5, ft3<br> [0x800003dc]:csrrs s5, fflags, zero<br> [0x800003e0]:sd a5, 0(s3)<br>     |
|   3|[0x80002230]<br>0x0000000033D3A4FC|- rd : x13<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x67 and fm1 == 0x53a4fc and rm_val == 0  #nosat<br>                       |[0x800003fc]:fmv.x.w a3, ft10<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd a3, 0(a5)<br>    |
|   4|[0x80002240]<br>0x0000000040E96B5C|- rd : x18<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x81 and fm1 == 0x696b5c and rm_val == 0  #nosat<br>                       |[0x80000414]:fmv.x.w s2, fa2<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd s2, 16(a5)<br>    |
|   5|[0x80002250]<br>0x0000000040681AE9|- rd : x30<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x80 and fm1 == 0x681ae9 and rm_val == 0  #nosat<br>                       |[0x8000042c]:fmv.x.w t5, fs7<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd t5, 32(a5)<br>    |
|   6|[0x80002260]<br>0x000000003F9A616D|- rd : x1<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x7f and fm1 == 0x1a616d and rm_val == 0  #nosat<br>                        |[0x80000444]:fmv.x.w ra, fs4<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd ra, 48(a5)<br>    |
|   7|[0x80002270]<br>0x000000003F49FEE5|- rd : x16<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x7e and fm1 == 0x49fee5 and rm_val == 0  #nosat<br>                       |[0x80000468]:fmv.x.w a6, fa3<br> [0x8000046c]:csrrs s5, fflags, zero<br> [0x80000470]:sd a6, 0(s3)<br>     |
|   8|[0x80002280]<br>0x000000003EB6E5D6|- rd : x8<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x7d and fm1 == 0x36e5d6 and rm_val == 0  #nosat<br>                        |[0x8000048c]:fmv.x.w fp, fa4<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd fp, 0(a5)<br>     |
|   9|[0x80002290]<br>0x0000000000000000|- rd : x19<br> - rs1 : f15<br>                                                                                                  |[0x800004a4]:fmv.x.w s3, fa5<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s3, 16(a5)<br>    |
|  10|[0x800022a0]<br>0x0000000000000000|- rd : x28<br> - rs1 : f4<br>                                                                                                   |[0x800004bc]:fmv.x.w t3, ft4<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd t3, 32(a5)<br>    |
|  11|[0x800022b0]<br>0x0000000000000000|- rd : x10<br> - rs1 : f0<br>                                                                                                   |[0x800004d4]:fmv.x.w a0, ft0<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd a0, 48(a5)<br>    |
|  12|[0x800022c0]<br>0x0000000000000000|- rd : x25<br> - rs1 : f17<br>                                                                                                  |[0x800004ec]:fmv.x.w s9, fa7<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd s9, 64(a5)<br>    |
|  13|[0x800022d0]<br>0x0000000000000000|- rd : x23<br> - rs1 : f9<br>                                                                                                   |[0x80000504]:fmv.x.w s7, fs1<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:sd s7, 80(a5)<br>    |
|  14|[0x800022e0]<br>0x0000000000000000|- rd : x4<br> - rs1 : f21<br>                                                                                                   |[0x8000051c]:fmv.x.w tp, fs5<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:sd tp, 96(a5)<br>    |
|  15|[0x800022f0]<br>0x0000000000000000|- rd : x17<br> - rs1 : f27<br>                                                                                                  |[0x80000540]:fmv.x.w a7, fs11<br> [0x80000544]:csrrs s5, fflags, zero<br> [0x80000548]:sd a7, 0(s3)<br>    |
|  16|[0x80002300]<br>0x0000000000000000|- rd : x29<br> - rs1 : f29<br>                                                                                                  |[0x80000564]:fmv.x.w t4, ft9<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:sd t4, 0(a5)<br>     |
|  17|[0x80002310]<br>0x0000000000000000|- rd : x24<br> - rs1 : f5<br>                                                                                                   |[0x8000057c]:fmv.x.w s8, ft5<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:sd s8, 16(a5)<br>    |
|  18|[0x80002320]<br>0x0000000000000000|- rd : x11<br> - rs1 : f1<br>                                                                                                   |[0x80000594]:fmv.x.w a1, ft1<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd a1, 32(a5)<br>    |
|  19|[0x80002330]<br>0x0000000000000000|- rd : x22<br> - rs1 : f19<br>                                                                                                  |[0x800005ac]:fmv.x.w s6, fs3<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd s6, 48(a5)<br>    |
|  20|[0x80002340]<br>0x0000000000000000|- rd : x31<br> - rs1 : f10<br>                                                                                                  |[0x800005c4]:fmv.x.w t6, fa0<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd t6, 64(a5)<br>    |
|  21|[0x80002350]<br>0x0000000000000000|- rd : x12<br> - rs1 : f18<br>                                                                                                  |[0x800005dc]:fmv.x.w a2, fs2<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd a2, 80(a5)<br>    |
|  22|[0x80002360]<br>0x0000000000000000|- rd : x26<br> - rs1 : f26<br>                                                                                                  |[0x800005f4]:fmv.x.w s10, fs10<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:sd s10, 96(a5)<br> |
|  23|[0x80002370]<br>0x0000000000000000|- rd : x27<br> - rs1 : f28<br>                                                                                                  |[0x8000060c]:fmv.x.w s11, ft8<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:sd s11, 112(a5)<br> |
|  24|[0x80002380]<br>0x0000000000000000|- rd : x14<br> - rs1 : f6<br>                                                                                                   |[0x80000624]:fmv.x.w a4, ft6<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd a4, 128(a5)<br>   |
|  25|[0x80002390]<br>0x0000000000000000|- rd : x20<br> - rs1 : f16<br>                                                                                                  |[0x8000063c]:fmv.x.w s4, fa6<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd s4, 144(a5)<br>   |
|  26|[0x800023a0]<br>0x0000000000000000|- rd : x5<br> - rs1 : f31<br>                                                                                                   |[0x80000654]:fmv.x.w t0, ft11<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd t0, 160(a5)<br>  |
|  27|[0x800023b0]<br>0x0000000000000000|- rd : x9<br> - rs1 : f7<br>                                                                                                    |[0x8000066c]:fmv.x.w s1, ft7<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd s1, 176(a5)<br>   |
|  28|[0x800023c0]<br>0x0000000000000000|- rd : x7<br> - rs1 : f25<br>                                                                                                   |[0x80000684]:fmv.x.w t2, fs9<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:sd t2, 192(a5)<br>   |
|  29|[0x800023d0]<br>0x0000000000000000|- rd : x2<br> - rs1 : f2<br>                                                                                                    |[0x8000069c]:fmv.x.w sp, ft2<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:sd sp, 208(a5)<br>   |
|  30|[0x800023e0]<br>0x0000000000000000|- rd : x21<br> - rs1 : f11<br>                                                                                                  |[0x800006b4]:fmv.x.w s5, fa1<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:sd s5, 224(a5)<br>   |
|  31|[0x800023f0]<br>0x0000000000000000|- rd : x6<br> - rs1 : f8<br>                                                                                                    |[0x800006cc]:fmv.x.w t1, fs0<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:sd t1, 240(a5)<br>   |
|  32|[0x80002400]<br>0x0000000000000000|- rd : x3<br> - rs1 : f22<br>                                                                                                   |[0x800006e4]:fmv.x.w gp, fs6<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd gp, 256(a5)<br>   |
