
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800008f0')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | fsw-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch11/fsw/riscof_work/fsw-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                 coverpoints                                                  |                                                                                        code                                                                                        |
|---:|----------------------------------|--------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x0000000000000000|- opcode : fsw<br> - rs1 : x6<br> - rs2 : f4<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x800003c0]:fsw ft4, 8(t1)<br> [0x800003c4]:addi zero, zero, 0<br> [0x800003c8]:addi zero, zero, 0<br> [0x800003cc]:csrrs a7, fflags, zero<br> [0x800003d0]:sd a7, 0(a5)<br>       |
|   2|[0x80002220]<br>0x0000000000000000|- rs1 : x14<br> - rs2 : f16<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                    |[0x800003e8]:fsw fa6, 5(a4)<br> [0x800003ec]:addi zero, zero, 0<br> [0x800003f0]:addi zero, zero, 0<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:sd a7, 16(a5)<br>      |
|   3|[0x80002230]<br>0x0000000000000000|- rs1 : x5<br> - rs2 : f9<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                      |[0x80000410]:fsw fs1, 6(t0)<br> [0x80000414]:addi zero, zero, 0<br> [0x80000418]:addi zero, zero, 0<br> [0x8000041c]:csrrs a7, fflags, zero<br> [0x80000420]:sd a7, 32(a5)<br>      |
|   4|[0x80002240]<br>0x0000000000000000|- rs1 : x12<br> - rs2 : f26<br> - ea_align == 0 and (imm_val % 4) == 3<br> - imm_val < 0<br>                  |[0x80000438]:fsw fs10, 3839(a2)<br> [0x8000043c]:addi zero, zero, 0<br> [0x80000440]:addi zero, zero, 0<br> [0x80000444]:csrrs a7, fflags, zero<br> [0x80000448]:sd a7, 48(a5)<br>  |
|   5|[0x80002250]<br>0x0000000000000000|- rs1 : x16<br> - rs2 : f12<br> - imm_val == 0<br>                                                            |[0x80000470]:fsw fa2, 0(a6)<br> [0x80000474]:addi zero, zero, 0<br> [0x80000478]:addi zero, zero, 0<br> [0x8000047c]:csrrs s5, fflags, zero<br> [0x80000480]:sd s5, 0(s3)<br>       |
|   6|[0x80002260]<br>0x0000000000000000|- rs1 : x20<br> - rs2 : f30<br>                                                                               |[0x800004a0]:fsw ft10, 2048(s4)<br> [0x800004a4]:addi zero, zero, 0<br> [0x800004a8]:addi zero, zero, 0<br> [0x800004ac]:csrrs a7, fflags, zero<br> [0x800004b0]:sd a7, 0(a5)<br>   |
|   7|[0x80002270]<br>0x0000000000000000|- rs1 : x22<br> - rs2 : f1<br>                                                                                |[0x800004c8]:fsw ft1, 2048(s6)<br> [0x800004cc]:addi zero, zero, 0<br> [0x800004d0]:addi zero, zero, 0<br> [0x800004d4]:csrrs a7, fflags, zero<br> [0x800004d8]:sd a7, 16(a5)<br>   |
|   8|[0x80002280]<br>0x0000000000000000|- rs1 : x27<br> - rs2 : f13<br>                                                                               |[0x800004f0]:fsw fa3, 2048(s11)<br> [0x800004f4]:addi zero, zero, 0<br> [0x800004f8]:addi zero, zero, 0<br> [0x800004fc]:csrrs a7, fflags, zero<br> [0x80000500]:sd a7, 32(a5)<br>  |
|   9|[0x80002290]<br>0x0000000000000000|- rs1 : x2<br> - rs2 : f29<br>                                                                                |[0x80000518]:fsw ft9, 2048(sp)<br> [0x8000051c]:addi zero, zero, 0<br> [0x80000520]:addi zero, zero, 0<br> [0x80000524]:csrrs a7, fflags, zero<br> [0x80000528]:sd a7, 48(a5)<br>   |
|  10|[0x800022a0]<br>0x0000000000000000|- rs1 : x1<br> - rs2 : f24<br>                                                                                |[0x80000540]:fsw fs8, 2048(ra)<br> [0x80000544]:addi zero, zero, 0<br> [0x80000548]:addi zero, zero, 0<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:sd a7, 64(a5)<br>   |
|  11|[0x800022b0]<br>0x0000000000000000|- rs1 : x13<br> - rs2 : f17<br>                                                                               |[0x80000568]:fsw fa7, 2048(a3)<br> [0x8000056c]:addi zero, zero, 0<br> [0x80000570]:addi zero, zero, 0<br> [0x80000574]:csrrs a7, fflags, zero<br> [0x80000578]:sd a7, 80(a5)<br>   |
|  12|[0x800022c0]<br>0x0000000000000000|- rs1 : x28<br> - rs2 : f6<br>                                                                                |[0x80000590]:fsw ft6, 2048(t3)<br> [0x80000594]:addi zero, zero, 0<br> [0x80000598]:addi zero, zero, 0<br> [0x8000059c]:csrrs a7, fflags, zero<br> [0x800005a0]:sd a7, 96(a5)<br>   |
|  13|[0x800022d0]<br>0x0000000000000000|- rs1 : x9<br> - rs2 : f0<br>                                                                                 |[0x800005b8]:fsw ft0, 2048(s1)<br> [0x800005bc]:addi zero, zero, 0<br> [0x800005c0]:addi zero, zero, 0<br> [0x800005c4]:csrrs a7, fflags, zero<br> [0x800005c8]:sd a7, 112(a5)<br>  |
|  14|[0x800022e0]<br>0x0000000000000000|- rs1 : x30<br> - rs2 : f15<br>                                                                               |[0x800005e0]:fsw fa5, 2048(t5)<br> [0x800005e4]:addi zero, zero, 0<br> [0x800005e8]:addi zero, zero, 0<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:sd a7, 128(a5)<br>  |
|  15|[0x800022f0]<br>0x0000000000000000|- rs1 : x8<br> - rs2 : f5<br>                                                                                 |[0x80000608]:fsw ft5, 2048(fp)<br> [0x8000060c]:addi zero, zero, 0<br> [0x80000610]:addi zero, zero, 0<br> [0x80000614]:csrrs a7, fflags, zero<br> [0x80000618]:sd a7, 144(a5)<br>  |
|  16|[0x80002300]<br>0x0000000000000000|- rs1 : x26<br> - rs2 : f25<br>                                                                               |[0x80000630]:fsw fs9, 2048(s10)<br> [0x80000634]:addi zero, zero, 0<br> [0x80000638]:addi zero, zero, 0<br> [0x8000063c]:csrrs a7, fflags, zero<br> [0x80000640]:sd a7, 160(a5)<br> |
|  17|[0x80002310]<br>0x0000000000000000|- rs1 : x18<br> - rs2 : f21<br>                                                                               |[0x80000658]:fsw fs5, 2048(s2)<br> [0x8000065c]:addi zero, zero, 0<br> [0x80000660]:addi zero, zero, 0<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:sd a7, 176(a5)<br>  |
|  18|[0x80002320]<br>0x0000000000000000|- rs1 : x31<br> - rs2 : f8<br>                                                                                |[0x80000680]:fsw fs0, 2048(t6)<br> [0x80000684]:addi zero, zero, 0<br> [0x80000688]:addi zero, zero, 0<br> [0x8000068c]:csrrs a7, fflags, zero<br> [0x80000690]:sd a7, 192(a5)<br>  |
|  19|[0x80002330]<br>0x0000000000000000|- rs1 : x11<br> - rs2 : f28<br>                                                                               |[0x800006a8]:fsw ft8, 2048(a1)<br> [0x800006ac]:addi zero, zero, 0<br> [0x800006b0]:addi zero, zero, 0<br> [0x800006b4]:csrrs a7, fflags, zero<br> [0x800006b8]:sd a7, 208(a5)<br>  |
|  20|[0x80002340]<br>0x0000000000000000|- rs1 : x17<br> - rs2 : f7<br>                                                                                |[0x800006d8]:fsw ft7, 2048(a7)<br> [0x800006dc]:addi zero, zero, 0<br> [0x800006e0]:addi zero, zero, 0<br> [0x800006e4]:csrrs s5, fflags, zero<br> [0x800006e8]:sd s5, 0(s3)<br>    |
|  21|[0x80002350]<br>0x0000000000000000|- rs1 : x7<br> - rs2 : f3<br>                                                                                 |[0x80000708]:fsw ft3, 2048(t2)<br> [0x8000070c]:addi zero, zero, 0<br> [0x80000710]:addi zero, zero, 0<br> [0x80000714]:csrrs a7, fflags, zero<br> [0x80000718]:sd a7, 0(a5)<br>    |
|  22|[0x80002360]<br>0x0000000000000000|- rs1 : x25<br> - rs2 : f18<br>                                                                               |[0x80000730]:fsw fs2, 2048(s9)<br> [0x80000734]:addi zero, zero, 0<br> [0x80000738]:addi zero, zero, 0<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:sd a7, 16(a5)<br>   |
|  23|[0x80002370]<br>0x0000000000000000|- rs1 : x21<br> - rs2 : f31<br>                                                                               |[0x80000758]:fsw ft11, 2048(s5)<br> [0x8000075c]:addi zero, zero, 0<br> [0x80000760]:addi zero, zero, 0<br> [0x80000764]:csrrs a7, fflags, zero<br> [0x80000768]:sd a7, 32(a5)<br>  |
|  24|[0x80002380]<br>0x0000000000000000|- rs1 : x29<br> - rs2 : f22<br>                                                                               |[0x80000780]:fsw fs6, 2048(t4)<br> [0x80000784]:addi zero, zero, 0<br> [0x80000788]:addi zero, zero, 0<br> [0x8000078c]:csrrs a7, fflags, zero<br> [0x80000790]:sd a7, 48(a5)<br>   |
|  25|[0x80002390]<br>0x0000000000000000|- rs1 : x4<br> - rs2 : f23<br>                                                                                |[0x800007a8]:fsw fs7, 2048(tp)<br> [0x800007ac]:addi zero, zero, 0<br> [0x800007b0]:addi zero, zero, 0<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:sd a7, 64(a5)<br>   |
|  26|[0x800023a0]<br>0x0000000000000000|- rs1 : x3<br> - rs2 : f11<br>                                                                                |[0x800007d0]:fsw fa1, 2048(gp)<br> [0x800007d4]:addi zero, zero, 0<br> [0x800007d8]:addi zero, zero, 0<br> [0x800007dc]:csrrs a7, fflags, zero<br> [0x800007e0]:sd a7, 80(a5)<br>   |
|  27|[0x800023b0]<br>0x0000000000000000|- rs1 : x15<br> - rs2 : f20<br>                                                                               |[0x80000800]:fsw fs4, 2048(a5)<br> [0x80000804]:addi zero, zero, 0<br> [0x80000808]:addi zero, zero, 0<br> [0x8000080c]:csrrs s5, fflags, zero<br> [0x80000810]:sd s5, 0(s3)<br>    |
|  28|[0x800023c0]<br>0x0000000000000000|- rs1 : x10<br> - rs2 : f10<br>                                                                               |[0x80000830]:fsw fa0, 2048(a0)<br> [0x80000834]:addi zero, zero, 0<br> [0x80000838]:addi zero, zero, 0<br> [0x8000083c]:csrrs a7, fflags, zero<br> [0x80000840]:sd a7, 0(a5)<br>    |
|  29|[0x800023d0]<br>0x0000000000000000|- rs1 : x19<br> - rs2 : f19<br>                                                                               |[0x80000858]:fsw fs3, 2048(s3)<br> [0x8000085c]:addi zero, zero, 0<br> [0x80000860]:addi zero, zero, 0<br> [0x80000864]:csrrs a7, fflags, zero<br> [0x80000868]:sd a7, 16(a5)<br>   |
|  30|[0x800023e0]<br>0x0000000000000000|- rs1 : x24<br> - rs2 : f27<br>                                                                               |[0x80000880]:fsw fs11, 2048(s8)<br> [0x80000884]:addi zero, zero, 0<br> [0x80000888]:addi zero, zero, 0<br> [0x8000088c]:csrrs a7, fflags, zero<br> [0x80000890]:sd a7, 32(a5)<br>  |
|  31|[0x800023f0]<br>0x0000000000000000|- rs1 : x23<br> - rs2 : f14<br>                                                                               |[0x800008a8]:fsw fa4, 2048(s7)<br> [0x800008ac]:addi zero, zero, 0<br> [0x800008b0]:addi zero, zero, 0<br> [0x800008b4]:csrrs a7, fflags, zero<br> [0x800008b8]:sd a7, 48(a5)<br>   |
|  32|[0x80002400]<br>0x0000000000000000|- rs2 : f2<br>                                                                                                |[0x800008d0]:fsw ft2, 2048(a1)<br> [0x800008d4]:addi zero, zero, 0<br> [0x800008d8]:addi zero, zero, 0<br> [0x800008dc]:csrrs a7, fflags, zero<br> [0x800008e0]:sd a7, 64(a5)<br>   |
