
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005d0')]      |
| SIG_REGION                | [('0x80002210', '0x800022a0', '36 words')]      |
| COV_LABELS                | c.fswsp      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-fswsp5/c.fswsp-01.S/ref.S    |
| Total Number of coverpoints| 49     |
| Total Coverpoints Hit     | 49      |
| Total Signature Updates   | 32      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 32     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000120]:c.fswsp
[0x80000122]:c.nop
[0x80000124]:c.nop
[0x80000126]:csrrs
[0x8000012a]:sw
[0x8000012e]:auipc
[0x80000132]:addi
[0x80000136]:flw
[0x8000013a]:addi
[0x8000013e]:addi
[0x80000142]:sub
[0x80000146]:c.fswsp

[0x80000146]:c.fswsp
[0x80000148]:c.nop
[0x8000014a]:c.nop
[0x8000014c]:csrrs
[0x80000150]:sw
[0x80000154]:auipc
[0x80000158]:addi
[0x8000015c]:flw
[0x80000160]:addi
[0x80000164]:addi
[0x80000168]:sub
[0x8000016c]:c.fswsp

[0x8000016c]:c.fswsp
[0x8000016e]:c.nop
[0x80000170]:c.nop
[0x80000172]:csrrs
[0x80000176]:sw
[0x8000017a]:auipc
[0x8000017e]:addi
[0x80000182]:flw
[0x80000186]:addi
[0x8000018a]:addi
[0x8000018e]:sub
[0x80000192]:c.fswsp

[0x80000192]:c.fswsp
[0x80000194]:c.nop
[0x80000196]:c.nop
[0x80000198]:csrrs
[0x8000019c]:sw
[0x800001a0]:auipc
[0x800001a4]:addi
[0x800001a8]:flw
[0x800001ac]:addi
[0x800001b0]:addi
[0x800001b4]:sub
[0x800001b8]:c.fswsp

[0x800001b8]:c.fswsp
[0x800001ba]:c.nop
[0x800001bc]:c.nop
[0x800001be]:csrrs
[0x800001c2]:sw
[0x800001c6]:auipc
[0x800001ca]:addi
[0x800001ce]:flw
[0x800001d2]:addi
[0x800001d6]:addi
[0x800001da]:sub
[0x800001de]:c.fswsp

[0x800001de]:c.fswsp
[0x800001e0]:c.nop
[0x800001e2]:c.nop
[0x800001e4]:csrrs
[0x800001e8]:sw
[0x800001ec]:auipc
[0x800001f0]:addi
[0x800001f4]:flw
[0x800001f8]:addi
[0x800001fc]:addi
[0x80000200]:sub
[0x80000204]:c.fswsp

[0x80000204]:c.fswsp
[0x80000206]:c.nop
[0x80000208]:c.nop
[0x8000020a]:csrrs
[0x8000020e]:sw
[0x80000212]:auipc
[0x80000216]:addi
[0x8000021a]:flw
[0x8000021e]:addi
[0x80000222]:addi
[0x80000226]:sub
[0x8000022a]:c.fswsp

[0x8000022a]:c.fswsp
[0x8000022c]:c.nop
[0x8000022e]:c.nop
[0x80000230]:csrrs
[0x80000234]:sw
[0x80000238]:auipc
[0x8000023c]:addi
[0x80000240]:flw
[0x80000244]:addi
[0x80000248]:addi
[0x8000024c]:sub
[0x80000250]:c.fswsp

[0x80000250]:c.fswsp
[0x80000252]:c.nop
[0x80000254]:c.nop
[0x80000256]:csrrs
[0x8000025a]:sw
[0x8000025e]:auipc
[0x80000262]:addi
[0x80000266]:flw
[0x8000026a]:addi
[0x8000026e]:addi
[0x80000272]:sub
[0x80000276]:c.fswsp

[0x80000276]:c.fswsp
[0x80000278]:c.nop
[0x8000027a]:c.nop
[0x8000027c]:csrrs
[0x80000280]:sw
[0x80000284]:auipc
[0x80000288]:addi
[0x8000028c]:flw
[0x80000290]:addi
[0x80000294]:addi
[0x80000298]:sub
[0x8000029c]:c.fswsp

[0x8000029c]:c.fswsp
[0x8000029e]:c.nop
[0x800002a0]:c.nop
[0x800002a2]:csrrs
[0x800002a6]:sw
[0x800002aa]:auipc
[0x800002ae]:addi
[0x800002b2]:flw
[0x800002b6]:addi
[0x800002ba]:addi
[0x800002be]:sub
[0x800002c2]:c.fswsp

[0x800002c2]:c.fswsp
[0x800002c4]:c.nop
[0x800002c6]:c.nop
[0x800002c8]:csrrs
[0x800002cc]:sw
[0x800002d0]:auipc
[0x800002d4]:addi
[0x800002d8]:flw
[0x800002dc]:addi
[0x800002e0]:addi
[0x800002e4]:sub
[0x800002e8]:c.fswsp

[0x800002e8]:c.fswsp
[0x800002ea]:c.nop
[0x800002ec]:c.nop
[0x800002ee]:csrrs
[0x800002f2]:sw
[0x800002f6]:auipc
[0x800002fa]:addi
[0x800002fe]:flw
[0x80000302]:addi
[0x80000306]:addi
[0x8000030a]:sub
[0x8000030e]:c.fswsp

[0x8000030e]:c.fswsp
[0x80000310]:c.nop
[0x80000312]:c.nop
[0x80000314]:csrrs
[0x80000318]:sw
[0x8000031c]:auipc
[0x80000320]:addi
[0x80000324]:flw
[0x80000328]:addi
[0x8000032c]:addi
[0x80000330]:sub
[0x80000334]:c.fswsp

[0x80000334]:c.fswsp
[0x80000336]:c.nop
[0x80000338]:c.nop
[0x8000033a]:csrrs
[0x8000033e]:sw
[0x80000342]:auipc
[0x80000346]:addi
[0x8000034a]:flw
[0x8000034e]:addi
[0x80000352]:addi
[0x80000356]:sub
[0x8000035a]:c.fswsp

[0x8000035a]:c.fswsp
[0x8000035c]:c.nop
[0x8000035e]:c.nop
[0x80000360]:csrrs
[0x80000364]:sw
[0x80000368]:auipc
[0x8000036c]:addi
[0x80000370]:flw
[0x80000374]:addi
[0x80000378]:addi
[0x8000037c]:sub
[0x80000380]:c.fswsp

[0x80000380]:c.fswsp
[0x80000382]:c.nop
[0x80000384]:c.nop
[0x80000386]:csrrs
[0x8000038a]:sw
[0x8000038e]:auipc
[0x80000392]:addi
[0x80000396]:flw
[0x8000039a]:addi
[0x8000039e]:addi
[0x800003a2]:sub
[0x800003a6]:c.fswsp

[0x800003a6]:c.fswsp
[0x800003a8]:c.nop
[0x800003aa]:c.nop
[0x800003ac]:csrrs
[0x800003b0]:sw
[0x800003b4]:auipc
[0x800003b8]:addi
[0x800003bc]:flw
[0x800003c0]:addi
[0x800003c4]:addi
[0x800003c8]:sub
[0x800003cc]:c.fswsp

[0x800003cc]:c.fswsp
[0x800003ce]:c.nop
[0x800003d0]:c.nop
[0x800003d2]:csrrs
[0x800003d6]:sw
[0x800003da]:auipc
[0x800003de]:addi
[0x800003e2]:flw
[0x800003e6]:addi
[0x800003ea]:addi
[0x800003ee]:sub
[0x800003f2]:c.fswsp

[0x800003f2]:c.fswsp
[0x800003f4]:c.nop
[0x800003f6]:c.nop
[0x800003f8]:csrrs
[0x800003fc]:sw
[0x80000400]:auipc
[0x80000404]:addi
[0x80000408]:flw
[0x8000040c]:addi
[0x80000410]:addi
[0x80000414]:sub
[0x80000418]:c.fswsp

[0x80000418]:c.fswsp
[0x8000041a]:c.nop
[0x8000041c]:c.nop
[0x8000041e]:csrrs
[0x80000422]:sw
[0x80000426]:auipc
[0x8000042a]:addi
[0x8000042e]:flw
[0x80000432]:addi
[0x80000436]:addi
[0x8000043a]:sub
[0x8000043e]:c.fswsp

[0x8000043e]:c.fswsp
[0x80000440]:c.nop
[0x80000442]:c.nop
[0x80000444]:csrrs
[0x80000448]:sw
[0x8000044c]:auipc
[0x80000450]:addi
[0x80000454]:flw
[0x80000458]:addi
[0x8000045c]:addi
[0x80000460]:sub
[0x80000464]:c.fswsp

[0x80000464]:c.fswsp
[0x80000466]:c.nop
[0x80000468]:c.nop
[0x8000046a]:csrrs
[0x8000046e]:sw
[0x80000472]:auipc
[0x80000476]:addi
[0x8000047a]:flw
[0x8000047e]:addi
[0x80000482]:addi
[0x80000486]:sub
[0x8000048a]:c.fswsp

[0x8000048a]:c.fswsp
[0x8000048c]:c.nop
[0x8000048e]:c.nop
[0x80000490]:csrrs
[0x80000494]:sw
[0x80000498]:auipc
[0x8000049c]:addi
[0x800004a0]:flw
[0x800004a4]:addi
[0x800004a8]:addi
[0x800004ac]:sub
[0x800004b0]:c.fswsp

[0x800004b0]:c.fswsp
[0x800004b2]:c.nop
[0x800004b4]:c.nop
[0x800004b6]:csrrs
[0x800004ba]:sw
[0x800004be]:auipc
[0x800004c2]:addi
[0x800004c6]:flw
[0x800004ca]:addi
[0x800004ce]:addi
[0x800004d2]:sub
[0x800004d6]:c.fswsp

[0x800004d6]:c.fswsp
[0x800004d8]:c.nop
[0x800004da]:c.nop
[0x800004dc]:csrrs
[0x800004e0]:sw
[0x800004e4]:auipc
[0x800004e8]:addi
[0x800004ec]:flw
[0x800004f0]:addi
[0x800004f4]:addi
[0x800004f8]:sub
[0x800004fc]:c.fswsp

[0x800004fc]:c.fswsp
[0x800004fe]:c.nop
[0x80000500]:c.nop
[0x80000502]:csrrs
[0x80000506]:sw
[0x8000050a]:auipc
[0x8000050e]:addi
[0x80000512]:flw
[0x80000516]:addi
[0x8000051a]:addi
[0x8000051e]:sub
[0x80000522]:c.fswsp

[0x80000522]:c.fswsp
[0x80000524]:c.nop
[0x80000526]:c.nop
[0x80000528]:csrrs
[0x8000052c]:sw
[0x80000530]:auipc
[0x80000534]:addi
[0x80000538]:flw
[0x8000053c]:addi
[0x80000540]:addi
[0x80000544]:sub
[0x80000548]:c.fswsp

[0x80000548]:c.fswsp
[0x8000054a]:c.nop
[0x8000054c]:c.nop
[0x8000054e]:csrrs
[0x80000552]:sw
[0x80000556]:auipc
[0x8000055a]:addi
[0x8000055e]:flw
[0x80000562]:addi
[0x80000566]:addi
[0x8000056a]:sub
[0x8000056e]:c.fswsp

[0x8000056e]:c.fswsp
[0x80000570]:c.nop
[0x80000572]:c.nop
[0x80000574]:csrrs
[0x80000578]:sw
[0x8000057c]:auipc
[0x80000580]:addi
[0x80000584]:flw
[0x80000588]:addi
[0x8000058c]:addi
[0x80000590]:sub
[0x80000594]:c.fswsp

[0x80000594]:c.fswsp
[0x80000596]:c.nop
[0x80000598]:c.nop
[0x8000059a]:csrrs
[0x8000059e]:sw
[0x800005a2]:auipc
[0x800005a6]:addi
[0x800005aa]:flw
[0x800005ae]:addi
[0x800005b2]:addi
[0x800005b6]:sub
[0x800005ba]:c.fswsp

[0x800005ba]:c.fswsp
[0x800005bc]:c.nop
[0x800005be]:c.nop
[0x800005c0]:csrrs
[0x800005c4]:sw
[0x800005c8]:addi
[0x800005cc]:addi



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
