
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f4', '0x800002f0')]      |
| SIG_REGION                | [('0x80002210', '0x80002290', '32 words')]      |
| COV_LABELS                | c.flw      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-FLW/c.flw-01.S/ref.S    |
| Total Number of coverpoints| 31     |
| Total Coverpoints Hit     | 31      |
| Total Signature Updates   | 28      |
| STAT1                     | 14      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 14     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.flw', 'rs1 : x15', 'rd : f15', 'imm_val == 0 and fcsr == 0']
Last Code Sequence : 
	-[0x8000011c]:c.flw
	-[0x8000011e]:c.nop
	-[0x80000120]:c.nop
	-[0x80000122]:csrrs
	-[0x80000126]:fsw
Current Store : [0x8000012a] : None -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : f14', 'imm_val > 0  and fcsr == 0']
Last Code Sequence : 
	-[0x8000013c]:c.flw
	-[0x8000013e]:c.nop
	-[0x80000140]:c.nop
	-[0x80000142]:csrrs
	-[0x80000146]:fsw
Current Store : [0x8000014a] : None -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : f13', 'imm_val == 84']
Last Code Sequence : 
	-[0x8000015e]:c.flw
	-[0x80000160]:c.nop
	-[0x80000162]:c.nop
	-[0x80000164]:csrrs
	-[0x80000168]:fsw
Current Store : [0x8000016c] : None -- Store: [0x80002228]:0x0000009f




Last Coverpoint : ['rs1 : x12', 'rd : f12', 'imm_val == 40']
Last Code Sequence : 
	-[0x80000180]:c.flw
	-[0x80000182]:c.nop
	-[0x80000184]:c.nop
	-[0x80000186]:csrrs
	-[0x8000018a]:fsw
Current Store : [0x8000018e] : None -- Store: [0x80002230]:0x0000009f




Last Coverpoint : ['rs1 : x11', 'rd : f11', 'imm_val == 4']
Last Code Sequence : 
	-[0x800001a2]:c.flw
	-[0x800001a4]:c.nop
	-[0x800001a6]:c.nop
	-[0x800001a8]:csrrs
	-[0x800001ac]:fsw
Current Store : [0x800001b0] : None -- Store: [0x80002238]:0x0000009f




Last Coverpoint : ['rs1 : x10', 'rd : f10', 'imm_val == 120']
Last Code Sequence : 
	-[0x800001c4]:c.flw
	-[0x800001c6]:c.nop
	-[0x800001c8]:c.nop
	-[0x800001ca]:csrrs
	-[0x800001ce]:fsw
Current Store : [0x800001d2] : None -- Store: [0x80002240]:0x0000009f




Last Coverpoint : ['rs1 : x9', 'rd : f9', 'imm_val == 116']
Last Code Sequence : 
	-[0x800001e6]:c.flw
	-[0x800001e8]:c.nop
	-[0x800001ea]:c.nop
	-[0x800001ec]:csrrs
	-[0x800001f0]:fsw
Current Store : [0x800001f4] : None -- Store: [0x80002248]:0x0000009f




Last Coverpoint : ['rs1 : x8', 'rd : f8', 'imm_val == 108']
Last Code Sequence : 
	-[0x80000208]:c.flw
	-[0x8000020a]:c.nop
	-[0x8000020c]:c.nop
	-[0x8000020e]:csrrs
	-[0x80000212]:fsw
Current Store : [0x80000216] : None -- Store: [0x80002250]:0x0000009f




Last Coverpoint : ['imm_val == 92']
Last Code Sequence : 
	-[0x8000022a]:c.flw
	-[0x8000022c]:c.nop
	-[0x8000022e]:c.nop
	-[0x80000230]:csrrs
	-[0x80000234]:fsw
Current Store : [0x80000238] : None -- Store: [0x80002258]:0x0000009f




Last Coverpoint : ['imm_val == 60']
Last Code Sequence : 
	-[0x8000024c]:c.flw
	-[0x8000024e]:c.nop
	-[0x80000250]:c.nop
	-[0x80000252]:csrrs
	-[0x80000256]:fsw
Current Store : [0x8000025a] : None -- Store: [0x80002260]:0x0000009f




Last Coverpoint : ['imm_val == 8']
Last Code Sequence : 
	-[0x8000026e]:c.flw
	-[0x80000270]:c.nop
	-[0x80000272]:c.nop
	-[0x80000274]:csrrs
	-[0x80000278]:fsw
Current Store : [0x8000027c] : None -- Store: [0x80002268]:0x0000009f




Last Coverpoint : ['imm_val == 16']
Last Code Sequence : 
	-[0x80000290]:c.flw
	-[0x80000292]:c.nop
	-[0x80000294]:c.nop
	-[0x80000296]:csrrs
	-[0x8000029a]:fsw
Current Store : [0x8000029e] : None -- Store: [0x80002270]:0x0000009f




Last Coverpoint : ['imm_val == 32']
Last Code Sequence : 
	-[0x800002b2]:c.flw
	-[0x800002b4]:c.nop
	-[0x800002b6]:c.nop
	-[0x800002b8]:csrrs
	-[0x800002bc]:fsw
Current Store : [0x800002c0] : None -- Store: [0x80002278]:0x0000009f




Last Coverpoint : ['imm_val == 64']
Last Code Sequence : 
	-[0x800002d4]:c.flw
	-[0x800002d6]:c.nop
	-[0x800002d8]:c.nop
	-[0x800002da]:csrrs
	-[0x800002de]:fsw
Current Store : [0x800002e2] : None -- Store: [0x80002280]:0x0000009f





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                      coverpoints                                      |                                                      code                                                       |
|---:|-------------------------------|---------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x80002000<br> |- mnemonic : c.flw<br> - rs1 : x15<br> - rd : f15<br> - imm_val == 0 and fcsr == 0<br> |[0x8000011c]:c.flw<br> [0x8000011e]:c.nop<br> [0x80000120]:c.nop<br> [0x80000122]:csrrs<br> [0x80000126]:fsw<br> |
|   2|[0x8000221c]<br>0x80001f84<br> |- rs1 : x14<br> - rd : f14<br> - imm_val > 0  and fcsr == 0<br>                        |[0x8000013c]:c.flw<br> [0x8000013e]:c.nop<br> [0x80000140]:c.nop<br> [0x80000142]:csrrs<br> [0x80000146]:fsw<br> |
|   3|[0x80002224]<br>0x80001fac<br> |- rs1 : x13<br> - rd : f13<br> - imm_val == 84<br>                                     |[0x8000015e]:c.flw<br> [0x80000160]:c.nop<br> [0x80000162]:c.nop<br> [0x80000164]:csrrs<br> [0x80000168]:fsw<br> |
|   4|[0x8000222c]<br>0x80001fd8<br> |- rs1 : x12<br> - rd : f12<br> - imm_val == 40<br>                                     |[0x80000180]:c.flw<br> [0x80000182]:c.nop<br> [0x80000184]:c.nop<br> [0x80000186]:csrrs<br> [0x8000018a]:fsw<br> |
|   5|[0x80002234]<br>0x80001ffc<br> |- rs1 : x11<br> - rd : f11<br> - imm_val == 4<br>                                      |[0x800001a2]:c.flw<br> [0x800001a4]:c.nop<br> [0x800001a6]:c.nop<br> [0x800001a8]:csrrs<br> [0x800001ac]:fsw<br> |
|   6|[0x8000223c]<br>0x80001f88<br> |- rs1 : x10<br> - rd : f10<br> - imm_val == 120<br>                                    |[0x800001c4]:c.flw<br> [0x800001c6]:c.nop<br> [0x800001c8]:c.nop<br> [0x800001ca]:csrrs<br> [0x800001ce]:fsw<br> |
|   7|[0x80002244]<br>0x80001f8c<br> |- rs1 : x9<br> - rd : f9<br> - imm_val == 116<br>                                      |[0x800001e6]:c.flw<br> [0x800001e8]:c.nop<br> [0x800001ea]:c.nop<br> [0x800001ec]:csrrs<br> [0x800001f0]:fsw<br> |
|   8|[0x8000224c]<br>0x80001f94<br> |- rs1 : x8<br> - rd : f8<br> - imm_val == 108<br>                                      |[0x80000208]:c.flw<br> [0x8000020a]:c.nop<br> [0x8000020c]:c.nop<br> [0x8000020e]:csrrs<br> [0x80000212]:fsw<br> |
|   9|[0x80002254]<br>0x80001fa4<br> |- imm_val == 92<br>                                                                    |[0x8000022a]:c.flw<br> [0x8000022c]:c.nop<br> [0x8000022e]:c.nop<br> [0x80000230]:csrrs<br> [0x80000234]:fsw<br> |
|  10|[0x8000225c]<br>0x80001fc4<br> |- imm_val == 60<br>                                                                    |[0x8000024c]:c.flw<br> [0x8000024e]:c.nop<br> [0x80000250]:c.nop<br> [0x80000252]:csrrs<br> [0x80000256]:fsw<br> |
|  11|[0x80002264]<br>0x80001ff8<br> |- imm_val == 8<br>                                                                     |[0x8000026e]:c.flw<br> [0x80000270]:c.nop<br> [0x80000272]:c.nop<br> [0x80000274]:csrrs<br> [0x80000278]:fsw<br> |
|  12|[0x8000226c]<br>0x80001ff0<br> |- imm_val == 16<br>                                                                    |[0x80000290]:c.flw<br> [0x80000292]:c.nop<br> [0x80000294]:c.nop<br> [0x80000296]:csrrs<br> [0x8000029a]:fsw<br> |
|  13|[0x80002274]<br>0x80001fe0<br> |- imm_val == 32<br>                                                                    |[0x800002b2]:c.flw<br> [0x800002b4]:c.nop<br> [0x800002b6]:c.nop<br> [0x800002b8]:csrrs<br> [0x800002bc]:fsw<br> |
|  14|[0x8000227c]<br>0x80001fc0<br> |- imm_val == 64<br>                                                                    |[0x800002d4]:c.flw<br> [0x800002d6]:c.nop<br> [0x800002d8]:c.nop<br> [0x800002da]:csrrs<br> [0x800002de]:fsw<br> |
