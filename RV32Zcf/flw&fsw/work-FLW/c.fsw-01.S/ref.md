
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f4', '0x80000310')]      |
| SIG_REGION                | [('0x80002210', '0x80002250', '16 words')]      |
| COV_LABELS                | c.fsw      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-FLW/c.fsw-01.S/ref.S    |
| Total Number of coverpoints| 32     |
| Total Coverpoints Hit     | 32      |
| Total Signature Updates   | 28      |
| STAT1                     | 14      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 14     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fsw', 'rs1 : x15', 'rs2 : f15', 'rs1 != rs2', 'imm_val == 0']
Last Code Sequence : 
	-[0x8000011a]:c.fsw
Current Store : [0x80000124] : None -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : f14', 'imm_val > 0']
Last Code Sequence : 
	-[0x80000140]:c.fsw
Current Store : [0x8000014a] : None -- Store: [0x8000221c]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : f13', 'imm_val == 84']
Last Code Sequence : 
	-[0x80000166]:c.fsw
Current Store : [0x80000170] : None -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : f12', 'imm_val == 40']
Last Code Sequence : 
	-[0x8000018c]:c.fsw
Current Store : [0x80000196] : None -- Store: [0x80002224]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : f11', 'imm_val == 4']
Last Code Sequence : 
	-[0x800001b0]:c.fsw
Current Store : [0x800001ba] : None -- Store: [0x80002228]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : f10', 'imm_val == 8']
Last Code Sequence : 
	-[0x800001d4]:c.fsw
Current Store : [0x800001de] : None -- Store: [0x8000222c]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : f9', 'imm_val == 120']
Last Code Sequence : 
	-[0x800001fa]:c.fsw
Current Store : [0x80000204] : None -- Store: [0x80002230]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : f8', 'imm_val == 116']
Last Code Sequence : 
	-[0x80000220]:c.fsw
Current Store : [0x8000022a] : None -- Store: [0x80002234]:0x00000000




Last Coverpoint : ['imm_val == 108']
Last Code Sequence : 
	-[0x80000246]:c.fsw
Current Store : [0x80000250] : None -- Store: [0x80002238]:0x00000000




Last Coverpoint : ['imm_val == 92']
Last Code Sequence : 
	-[0x8000026c]:c.fsw
Current Store : [0x80000276] : None -- Store: [0x8000223c]:0x00000000




Last Coverpoint : ['imm_val == 60']
Last Code Sequence : 
	-[0x80000292]:c.fsw
Current Store : [0x8000029c] : None -- Store: [0x80002240]:0x00000000




Last Coverpoint : ['imm_val == 16']
Last Code Sequence : 
	-[0x800002b6]:c.fsw
Current Store : [0x800002c0] : None -- Store: [0x80002244]:0x00000000




Last Coverpoint : ['imm_val == 32']
Last Code Sequence : 
	-[0x800002dc]:c.fsw
Current Store : [0x800002e6] : None -- Store: [0x80002248]:0x00000000




Last Coverpoint : ['imm_val == 64']
Last Code Sequence : 
	-[0x80000302]:c.fsw
Current Store : [0x8000030c] : None -- Store: [0x8000224c]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                        coverpoints                                        |         code          |
|---:|-------------------------------|-------------------------------------------------------------------------------------------|-----------------------|
|   1|[0x80002214]<br>0x80002214<br> |- mnemonic : c.fsw<br> - rs1 : x15<br> - rs2 : f15<br> - rs1 != rs2<br> - imm_val == 0<br> |[0x8000011a]:c.fsw<br> |
|   2|[0x80002218]<br>0x8000219c<br> |- rs1 : x14<br> - rs2 : f14<br> - imm_val > 0<br>                                          |[0x80000140]:c.fsw<br> |
|   3|[0x8000221c]<br>0x800021c8<br> |- rs1 : x13<br> - rs2 : f13<br> - imm_val == 84<br>                                        |[0x80000166]:c.fsw<br> |
|   4|[0x80002220]<br>0x800021f8<br> |- rs1 : x12<br> - rs2 : f12<br> - imm_val == 40<br>                                        |[0x8000018c]:c.fsw<br> |
|   5|[0x80002224]<br>0x80002220<br> |- rs1 : x11<br> - rs2 : f11<br> - imm_val == 4<br>                                         |[0x800001b0]:c.fsw<br> |
|   6|[0x80002228]<br>0x80002220<br> |- rs1 : x10<br> - rs2 : f10<br> - imm_val == 8<br>                                         |[0x800001d4]:c.fsw<br> |
|   7|[0x8000222c]<br>0x800021b4<br> |- rs1 : x9<br> - rs2 : f9<br> - imm_val == 120<br>                                         |[0x800001fa]:c.fsw<br> |
|   8|[0x80002230]<br>0x800021bc<br> |- rs1 : x8<br> - rs2 : f8<br> - imm_val == 116<br>                                         |[0x80000220]:c.fsw<br> |
|   9|[0x80002234]<br>0x800021c8<br> |- imm_val == 108<br>                                                                       |[0x80000246]:c.fsw<br> |
|  10|[0x80002238]<br>0x800021dc<br> |- imm_val == 92<br>                                                                        |[0x8000026c]:c.fsw<br> |
|  11|[0x8000223c]<br>0x80002200<br> |- imm_val == 60<br>                                                                        |[0x80000292]:c.fsw<br> |
|  12|[0x80002240]<br>0x80002230<br> |- imm_val == 16<br>                                                                        |[0x800002b6]:c.fsw<br> |
|  13|[0x80002244]<br>0x80002224<br> |- imm_val == 32<br>                                                                        |[0x800002dc]:c.fsw<br> |
|  14|[0x80002248]<br>0x80002208<br> |- imm_val == 64<br>                                                                        |[0x80000302]:c.fsw<br> |
