
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f4', '0x80000530')]      |
| SIG_REGION                | [('0x80002210', '0x80002320', '68 words')]      |
| COV_LABELS                | c.flwsp      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-flwsp-new5/c.flwsp-01.S/ref.S    |
| Total Number of coverpoints| 49     |
| Total Coverpoints Hit     | 49      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.flwsp', 'rd : f31', 'imm_val == 0 and fcsr == 0']
Last Code Sequence : 
	-[0x8000011c]:c.flwsp
	-[0x8000011e]:c.nop
	-[0x80000120]:c.nop
	-[0x80000122]:csrrs
	-[0x80000126]:fsw
Current Store : [0x8000012a] : None -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rd : f30', 'imm_val > 0  and fcsr == 0']
Last Code Sequence : 
	-[0x8000013c]:c.flwsp
	-[0x8000013e]:c.nop
	-[0x80000140]:c.nop
	-[0x80000142]:csrrs
	-[0x80000146]:fsw
Current Store : [0x8000014a] : None -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rd : f29', 'imm_val == 84']
Last Code Sequence : 
	-[0x8000015e]:c.flwsp
	-[0x80000160]:c.nop
	-[0x80000162]:c.nop
	-[0x80000164]:csrrs
	-[0x80000168]:fsw
Current Store : [0x8000016c] : None -- Store: [0x80002228]:0x0000009f




Last Coverpoint : ['rd : f28', 'imm_val == 168']
Last Code Sequence : 
	-[0x80000180]:c.flwsp
	-[0x80000182]:c.nop
	-[0x80000184]:c.nop
	-[0x80000186]:csrrs
	-[0x8000018a]:fsw
Current Store : [0x8000018e] : None -- Store: [0x80002230]:0x0000009f




Last Coverpoint : ['rd : f27', 'imm_val == 248']
Last Code Sequence : 
	-[0x800001a2]:c.flwsp
	-[0x800001a4]:c.nop
	-[0x800001a6]:c.nop
	-[0x800001a8]:csrrs
	-[0x800001ac]:fsw
Current Store : [0x800001b0] : None -- Store: [0x80002238]:0x0000009f




Last Coverpoint : ['rd : f26', 'imm_val == 244']
Last Code Sequence : 
	-[0x800001c4]:c.flwsp
	-[0x800001c6]:c.nop
	-[0x800001c8]:c.nop
	-[0x800001ca]:csrrs
	-[0x800001ce]:fsw
Current Store : [0x800001d2] : None -- Store: [0x80002240]:0x0000009f




Last Coverpoint : ['rd : f25', 'imm_val == 236']
Last Code Sequence : 
	-[0x800001e6]:c.flwsp
	-[0x800001e8]:c.nop
	-[0x800001ea]:c.nop
	-[0x800001ec]:csrrs
	-[0x800001f0]:fsw
Current Store : [0x800001f4] : None -- Store: [0x80002248]:0x0000009f




Last Coverpoint : ['rd : f24', 'imm_val == 220']
Last Code Sequence : 
	-[0x80000208]:c.flwsp
	-[0x8000020a]:c.nop
	-[0x8000020c]:c.nop
	-[0x8000020e]:csrrs
	-[0x80000212]:fsw
Current Store : [0x80000216] : None -- Store: [0x80002250]:0x0000009f




Last Coverpoint : ['rd : f23', 'imm_val == 188']
Last Code Sequence : 
	-[0x8000022a]:c.flwsp
	-[0x8000022c]:c.nop
	-[0x8000022e]:c.nop
	-[0x80000230]:csrrs
	-[0x80000234]:fsw
Current Store : [0x80000238] : None -- Store: [0x80002258]:0x0000009f




Last Coverpoint : ['rd : f22', 'imm_val == 124']
Last Code Sequence : 
	-[0x8000024c]:c.flwsp
	-[0x8000024e]:c.nop
	-[0x80000250]:c.nop
	-[0x80000252]:csrrs
	-[0x80000256]:fsw
Current Store : [0x8000025a] : None -- Store: [0x80002260]:0x0000009f




Last Coverpoint : ['rd : f21', 'imm_val == 4']
Last Code Sequence : 
	-[0x8000026e]:c.flwsp
	-[0x80000270]:c.nop
	-[0x80000272]:c.nop
	-[0x80000274]:csrrs
	-[0x80000278]:fsw
Current Store : [0x8000027c] : None -- Store: [0x80002268]:0x0000009f




Last Coverpoint : ['rd : f20', 'imm_val == 8']
Last Code Sequence : 
	-[0x80000290]:c.flwsp
	-[0x80000292]:c.nop
	-[0x80000294]:c.nop
	-[0x80000296]:csrrs
	-[0x8000029a]:fsw
Current Store : [0x8000029e] : None -- Store: [0x80002270]:0x0000009f




Last Coverpoint : ['rd : f19', 'imm_val == 16']
Last Code Sequence : 
	-[0x800002b2]:c.flwsp
	-[0x800002b4]:c.nop
	-[0x800002b6]:c.nop
	-[0x800002b8]:csrrs
	-[0x800002bc]:fsw
Current Store : [0x800002c0] : None -- Store: [0x80002278]:0x0000009f




Last Coverpoint : ['rd : f18', 'imm_val == 32']
Last Code Sequence : 
	-[0x800002d4]:c.flwsp
	-[0x800002d6]:c.nop
	-[0x800002d8]:c.nop
	-[0x800002da]:csrrs
	-[0x800002de]:fsw
Current Store : [0x800002e2] : None -- Store: [0x80002280]:0x0000009f




Last Coverpoint : ['rd : f17', 'imm_val == 64']
Last Code Sequence : 
	-[0x800002f6]:c.flwsp
	-[0x800002f8]:c.nop
	-[0x800002fa]:c.nop
	-[0x800002fc]:csrrs
	-[0x80000300]:fsw
Current Store : [0x80000304] : None -- Store: [0x80002288]:0x0000009f




Last Coverpoint : ['rd : f16', 'imm_val == 128']
Last Code Sequence : 
	-[0x80000318]:c.flwsp
	-[0x8000031a]:c.nop
	-[0x8000031c]:c.nop
	-[0x8000031e]:csrrs
	-[0x80000322]:fsw
Current Store : [0x80000326] : None -- Store: [0x80002290]:0x0000009f




Last Coverpoint : ['rd : f15']
Last Code Sequence : 
	-[0x80000338]:c.flwsp
	-[0x8000033a]:c.nop
	-[0x8000033c]:c.nop
	-[0x8000033e]:csrrs
	-[0x80000342]:fsw
Current Store : [0x80000346] : None -- Store: [0x80002298]:0x00000000




Last Coverpoint : ['rd : f14']
Last Code Sequence : 
	-[0x80000358]:c.flwsp
	-[0x8000035a]:c.nop
	-[0x8000035c]:c.nop
	-[0x8000035e]:csrrs
	-[0x80000362]:fsw
Current Store : [0x80000366] : None -- Store: [0x800022a0]:0x00000000




Last Coverpoint : ['rd : f13']
Last Code Sequence : 
	-[0x80000378]:c.flwsp
	-[0x8000037a]:c.nop
	-[0x8000037c]:c.nop
	-[0x8000037e]:csrrs
	-[0x80000382]:fsw
Current Store : [0x80000386] : None -- Store: [0x800022a8]:0x00000000




Last Coverpoint : ['rd : f12']
Last Code Sequence : 
	-[0x80000398]:c.flwsp
	-[0x8000039a]:c.nop
	-[0x8000039c]:c.nop
	-[0x8000039e]:csrrs
	-[0x800003a2]:fsw
Current Store : [0x800003a6] : None -- Store: [0x800022b0]:0x00000000




Last Coverpoint : ['rd : f11']
Last Code Sequence : 
	-[0x800003b8]:c.flwsp
	-[0x800003ba]:c.nop
	-[0x800003bc]:c.nop
	-[0x800003be]:csrrs
	-[0x800003c2]:fsw
Current Store : [0x800003c6] : None -- Store: [0x800022b8]:0x00000000




Last Coverpoint : ['rd : f10']
Last Code Sequence : 
	-[0x800003d8]:c.flwsp
	-[0x800003da]:c.nop
	-[0x800003dc]:c.nop
	-[0x800003de]:csrrs
	-[0x800003e2]:fsw
Current Store : [0x800003e6] : None -- Store: [0x800022c0]:0x00000000




Last Coverpoint : ['rd : f9']
Last Code Sequence : 
	-[0x800003f8]:c.flwsp
	-[0x800003fa]:c.nop
	-[0x800003fc]:c.nop
	-[0x800003fe]:csrrs
	-[0x80000402]:fsw
Current Store : [0x80000406] : None -- Store: [0x800022c8]:0x00000000




Last Coverpoint : ['rd : f8']
Last Code Sequence : 
	-[0x80000418]:c.flwsp
	-[0x8000041a]:c.nop
	-[0x8000041c]:c.nop
	-[0x8000041e]:csrrs
	-[0x80000422]:fsw
Current Store : [0x80000426] : None -- Store: [0x800022d0]:0x00000000




Last Coverpoint : ['rd : f7']
Last Code Sequence : 
	-[0x80000438]:c.flwsp
	-[0x8000043a]:c.nop
	-[0x8000043c]:c.nop
	-[0x8000043e]:csrrs
	-[0x80000442]:fsw
Current Store : [0x80000446] : None -- Store: [0x800022d8]:0x00000000




Last Coverpoint : ['rd : f6']
Last Code Sequence : 
	-[0x80000458]:c.flwsp
	-[0x8000045a]:c.nop
	-[0x8000045c]:c.nop
	-[0x8000045e]:csrrs
	-[0x80000462]:fsw
Current Store : [0x80000466] : None -- Store: [0x800022e0]:0x00000000




Last Coverpoint : ['rd : f5']
Last Code Sequence : 
	-[0x80000478]:c.flwsp
	-[0x8000047a]:c.nop
	-[0x8000047c]:c.nop
	-[0x8000047e]:csrrs
	-[0x80000482]:fsw
Current Store : [0x80000486] : None -- Store: [0x800022e8]:0x00000000




Last Coverpoint : ['rd : f4']
Last Code Sequence : 
	-[0x80000498]:c.flwsp
	-[0x8000049a]:c.nop
	-[0x8000049c]:c.nop
	-[0x8000049e]:csrrs
	-[0x800004a2]:fsw
Current Store : [0x800004a6] : None -- Store: [0x800022f0]:0x00000000




Last Coverpoint : ['rd : f3']
Last Code Sequence : 
	-[0x800004b8]:c.flwsp
	-[0x800004ba]:c.nop
	-[0x800004bc]:c.nop
	-[0x800004be]:csrrs
	-[0x800004c2]:fsw
Current Store : [0x800004c6] : None -- Store: [0x800022f8]:0x00000000




Last Coverpoint : ['rd : f2']
Last Code Sequence : 
	-[0x800004d8]:c.flwsp
	-[0x800004da]:c.nop
	-[0x800004dc]:c.nop
	-[0x800004de]:csrrs
	-[0x800004e2]:fsw
Current Store : [0x800004e6] : None -- Store: [0x80002300]:0x00000000




Last Coverpoint : ['rd : f1']
Last Code Sequence : 
	-[0x800004f8]:c.flwsp
	-[0x800004fa]:c.nop
	-[0x800004fc]:c.nop
	-[0x800004fe]:csrrs
	-[0x80000502]:fsw
Current Store : [0x80000506] : None -- Store: [0x80002308]:0x00000000




Last Coverpoint : ['rd : f0']
Last Code Sequence : 
	-[0x80000518]:c.flwsp
	-[0x8000051a]:c.nop
	-[0x8000051c]:c.nop
	-[0x8000051e]:csrrs
	-[0x80000522]:fsw
Current Store : [0x80000526] : None -- Store: [0x80002310]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                               coverpoints                               |                                                       code                                                        |
|---:|-------------------------------|-------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0xfbb6fab7<br> |- mnemonic : c.flwsp<br> - rd : f31<br> - imm_val == 0 and fcsr == 0<br> |[0x8000011c]:c.flwsp<br> [0x8000011e]:c.nop<br> [0x80000120]:c.nop<br> [0x80000122]:csrrs<br> [0x80000126]:fsw<br> |
|   2|[0x8000221c]<br>0xf76df56f<br> |- rd : f30<br> - imm_val > 0  and fcsr == 0<br>                          |[0x8000013c]:c.flwsp<br> [0x8000013e]:c.nop<br> [0x80000140]:c.nop<br> [0x80000142]:csrrs<br> [0x80000146]:fsw<br> |
|   3|[0x80002224]<br>0xeedbeadf<br> |- rd : f29<br> - imm_val == 84<br>                                       |[0x8000015e]:c.flwsp<br> [0x80000160]:c.nop<br> [0x80000162]:c.nop<br> [0x80000164]:csrrs<br> [0x80000168]:fsw<br> |
|   4|[0x8000222c]<br>0xddb7d5bf<br> |- rd : f28<br> - imm_val == 168<br>                                      |[0x80000180]:c.flwsp<br> [0x80000182]:c.nop<br> [0x80000184]:c.nop<br> [0x80000186]:csrrs<br> [0x8000018a]:fsw<br> |
|   5|[0x80002234]<br>0xbb6fab7f<br> |- rd : f27<br> - imm_val == 248<br>                                      |[0x800001a2]:c.flwsp<br> [0x800001a4]:c.nop<br> [0x800001a6]:c.nop<br> [0x800001a8]:csrrs<br> [0x800001ac]:fsw<br> |
|   6|[0x8000223c]<br>0x76df56ff<br> |- rd : f26<br> - imm_val == 244<br>                                      |[0x800001c4]:c.flwsp<br> [0x800001c6]:c.nop<br> [0x800001c8]:c.nop<br> [0x800001ca]:csrrs<br> [0x800001ce]:fsw<br> |
|   7|[0x80002244]<br>0xedbeadfe<br> |- rd : f25<br> - imm_val == 236<br>                                      |[0x800001e6]:c.flwsp<br> [0x800001e8]:c.nop<br> [0x800001ea]:c.nop<br> [0x800001ec]:csrrs<br> [0x800001f0]:fsw<br> |
|   8|[0x8000224c]<br>0xdb7d5bfd<br> |- rd : f24<br> - imm_val == 220<br>                                      |[0x80000208]:c.flwsp<br> [0x8000020a]:c.nop<br> [0x8000020c]:c.nop<br> [0x8000020e]:csrrs<br> [0x80000212]:fsw<br> |
|   9|[0x80002254]<br>0xb6fab7fb<br> |- rd : f23<br> - imm_val == 188<br>                                      |[0x8000022a]:c.flwsp<br> [0x8000022c]:c.nop<br> [0x8000022e]:c.nop<br> [0x80000230]:csrrs<br> [0x80000234]:fsw<br> |
|  10|[0x8000225c]<br>0x6df56ff7<br> |- rd : f22<br> - imm_val == 124<br>                                      |[0x8000024c]:c.flwsp<br> [0x8000024e]:c.nop<br> [0x80000250]:c.nop<br> [0x80000252]:csrrs<br> [0x80000256]:fsw<br> |
|  11|[0x80002264]<br>0xdbeadfee<br> |- rd : f21<br> - imm_val == 4<br>                                        |[0x8000026e]:c.flwsp<br> [0x80000270]:c.nop<br> [0x80000272]:c.nop<br> [0x80000274]:csrrs<br> [0x80000278]:fsw<br> |
|  12|[0x8000226c]<br>0xb7d5bfdd<br> |- rd : f20<br> - imm_val == 8<br>                                        |[0x80000290]:c.flwsp<br> [0x80000292]:c.nop<br> [0x80000294]:c.nop<br> [0x80000296]:csrrs<br> [0x8000029a]:fsw<br> |
|  13|[0x80002274]<br>0x6fab7fbb<br> |- rd : f19<br> - imm_val == 16<br>                                       |[0x800002b2]:c.flwsp<br> [0x800002b4]:c.nop<br> [0x800002b6]:c.nop<br> [0x800002b8]:csrrs<br> [0x800002bc]:fsw<br> |
|  14|[0x8000227c]<br>0xdf56ff76<br> |- rd : f18<br> - imm_val == 32<br>                                       |[0x800002d4]:c.flwsp<br> [0x800002d6]:c.nop<br> [0x800002d8]:c.nop<br> [0x800002da]:csrrs<br> [0x800002de]:fsw<br> |
|  15|[0x80002284]<br>0xbeadfeed<br> |- rd : f17<br> - imm_val == 64<br>                                       |[0x800002f6]:c.flwsp<br> [0x800002f8]:c.nop<br> [0x800002fa]:c.nop<br> [0x800002fc]:csrrs<br> [0x80000300]:fsw<br> |
|  16|[0x8000228c]<br>0x7d5bfddb<br> |- rd : f16<br> - imm_val == 128<br>                                      |[0x80000318]:c.flwsp<br> [0x8000031a]:c.nop<br> [0x8000031c]:c.nop<br> [0x8000031e]:csrrs<br> [0x80000322]:fsw<br> |
|  17|[0x80002294]<br>0xfab7fbb6<br> |- rd : f15<br>                                                           |[0x80000338]:c.flwsp<br> [0x8000033a]:c.nop<br> [0x8000033c]:c.nop<br> [0x8000033e]:csrrs<br> [0x80000342]:fsw<br> |
|  18|[0x8000229c]<br>0xf56ff76d<br> |- rd : f14<br>                                                           |[0x80000358]:c.flwsp<br> [0x8000035a]:c.nop<br> [0x8000035c]:c.nop<br> [0x8000035e]:csrrs<br> [0x80000362]:fsw<br> |
|  19|[0x800022a4]<br>0xeadfeedb<br> |- rd : f13<br>                                                           |[0x80000378]:c.flwsp<br> [0x8000037a]:c.nop<br> [0x8000037c]:c.nop<br> [0x8000037e]:csrrs<br> [0x80000382]:fsw<br> |
|  20|[0x800022ac]<br>0xd5bfddb7<br> |- rd : f12<br>                                                           |[0x80000398]:c.flwsp<br> [0x8000039a]:c.nop<br> [0x8000039c]:c.nop<br> [0x8000039e]:csrrs<br> [0x800003a2]:fsw<br> |
|  21|[0x800022b4]<br>0xab7fbb6f<br> |- rd : f11<br>                                                           |[0x800003b8]:c.flwsp<br> [0x800003ba]:c.nop<br> [0x800003bc]:c.nop<br> [0x800003be]:csrrs<br> [0x800003c2]:fsw<br> |
|  22|[0x800022bc]<br>0x56ff76df<br> |- rd : f10<br>                                                           |[0x800003d8]:c.flwsp<br> [0x800003da]:c.nop<br> [0x800003dc]:c.nop<br> [0x800003de]:csrrs<br> [0x800003e2]:fsw<br> |
|  23|[0x800022c4]<br>0xadfeedbe<br> |- rd : f9<br>                                                            |[0x800003f8]:c.flwsp<br> [0x800003fa]:c.nop<br> [0x800003fc]:c.nop<br> [0x800003fe]:csrrs<br> [0x80000402]:fsw<br> |
|  24|[0x800022cc]<br>0x5bfddb7d<br> |- rd : f8<br>                                                            |[0x80000418]:c.flwsp<br> [0x8000041a]:c.nop<br> [0x8000041c]:c.nop<br> [0x8000041e]:csrrs<br> [0x80000422]:fsw<br> |
|  25|[0x800022d4]<br>0xb7fbb6fa<br> |- rd : f7<br>                                                            |[0x80000438]:c.flwsp<br> [0x8000043a]:c.nop<br> [0x8000043c]:c.nop<br> [0x8000043e]:csrrs<br> [0x80000442]:fsw<br> |
|  26|[0x800022dc]<br>0x80002000<br> |- rd : f6<br>                                                            |[0x80000458]:c.flwsp<br> [0x8000045a]:c.nop<br> [0x8000045c]:c.nop<br> [0x8000045e]:csrrs<br> [0x80000462]:fsw<br> |
|  27|[0x800022e4]<br>0x800000f4<br> |- rd : f5<br>                                                            |[0x80000478]:c.flwsp<br> [0x8000047a]:c.nop<br> [0x8000047c]:c.nop<br> [0x8000047e]:csrrs<br> [0x80000482]:fsw<br> |
|  28|[0x800022ec]<br>0x00000000<br> |- rd : f4<br>                                                            |[0x80000498]:c.flwsp<br> [0x8000049a]:c.nop<br> [0x8000049c]:c.nop<br> [0x8000049e]:csrrs<br> [0x800004a2]:fsw<br> |
|  29|[0x800022f4]<br>0x0000009f<br> |- rd : f3<br>                                                            |[0x800004b8]:c.flwsp<br> [0x800004ba]:c.nop<br> [0x800004bc]:c.nop<br> [0x800004be]:csrrs<br> [0x800004c2]:fsw<br> |
|  30|[0x800022fc]<br>0x80002000<br> |- rd : f2<br>                                                            |[0x800004d8]:c.flwsp<br> [0x800004da]:c.nop<br> [0x800004dc]:c.nop<br> [0x800004de]:csrrs<br> [0x800004e2]:fsw<br> |
|  31|[0x80002304]<br>0x80002214<br> |- rd : f1<br>                                                            |[0x800004f8]:c.flwsp<br> [0x800004fa]:c.nop<br> [0x800004fc]:c.nop<br> [0x800004fe]:csrrs<br> [0x80000502]:fsw<br> |
|  32|[0x8000230c]<br>0x00000000<br> |- rd : f0<br>                                                            |[0x80000518]:c.flwsp<br> [0x8000051a]:c.nop<br> [0x8000051c]:c.nop<br> [0x8000051e]:csrrs<br> [0x80000522]:fsw<br> |
