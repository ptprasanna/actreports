
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800037c0')]      |
| SIG_REGION                | [('0x80007610', '0x80008820', '1156 words')]      |
| COV_LABELS                | fle_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fle/riscof_work/fle_b1-01.S/ref.S    |
| Total Number of coverpoints| 681     |
| Total Coverpoints Hit     | 615      |
| Total Signature Updates   | 1031      |
| STAT1                     | 516      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 515     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80002ee8]:fle.d t6, ft11, ft10
[0x80002eec]:csrrs a7, fflags, zero
[0x80002ef0]:sw t6, 1536(a5)
[0x80002ef4]:sw a7, 1540(a5)
[0x80002ef8]:fld ft11, 1664(a6)
[0x80002efc]:fld ft10, 1672(a6)

[0x80002f00]:fle.d t6, ft11, ft10
[0x80002f04]:csrrs a7, fflags, zero
[0x80002f08]:sw t6, 1552(a5)
[0x80002f0c]:sw a7, 1556(a5)
[0x80002f10]:fld ft11, 1680(a6)
[0x80002f14]:fld ft10, 1688(a6)

[0x80002f18]:fle.d t6, ft11, ft10
[0x80002f1c]:csrrs a7, fflags, zero
[0x80002f20]:sw t6, 1568(a5)
[0x80002f24]:sw a7, 1572(a5)
[0x80002f28]:fld ft11, 1696(a6)
[0x80002f2c]:fld ft10, 1704(a6)

[0x80002f30]:fle.d t6, ft11, ft10
[0x80002f34]:csrrs a7, fflags, zero
[0x80002f38]:sw t6, 1584(a5)
[0x80002f3c]:sw a7, 1588(a5)
[0x80002f40]:fld ft11, 1712(a6)
[0x80002f44]:fld ft10, 1720(a6)

[0x80002f48]:fle.d t6, ft11, ft10
[0x80002f4c]:csrrs a7, fflags, zero
[0x80002f50]:sw t6, 1600(a5)
[0x80002f54]:sw a7, 1604(a5)
[0x80002f58]:fld ft11, 1728(a6)
[0x80002f5c]:fld ft10, 1736(a6)

[0x80002f60]:fle.d t6, ft11, ft10
[0x80002f64]:csrrs a7, fflags, zero
[0x80002f68]:sw t6, 1616(a5)
[0x80002f6c]:sw a7, 1620(a5)
[0x80002f70]:fld ft11, 1744(a6)
[0x80002f74]:fld ft10, 1752(a6)

[0x80002f78]:fle.d t6, ft11, ft10
[0x80002f7c]:csrrs a7, fflags, zero
[0x80002f80]:sw t6, 1632(a5)
[0x80002f84]:sw a7, 1636(a5)
[0x80002f88]:fld ft11, 1760(a6)
[0x80002f8c]:fld ft10, 1768(a6)

[0x80002f90]:fle.d t6, ft11, ft10
[0x80002f94]:csrrs a7, fflags, zero
[0x80002f98]:sw t6, 1648(a5)
[0x80002f9c]:sw a7, 1652(a5)
[0x80002fa0]:fld ft11, 1776(a6)
[0x80002fa4]:fld ft10, 1784(a6)

[0x80002fa8]:fle.d t6, ft11, ft10
[0x80002fac]:csrrs a7, fflags, zero
[0x80002fb0]:sw t6, 1664(a5)
[0x80002fb4]:sw a7, 1668(a5)
[0x80002fb8]:fld ft11, 1792(a6)
[0x80002fbc]:fld ft10, 1800(a6)

[0x80002fc0]:fle.d t6, ft11, ft10
[0x80002fc4]:csrrs a7, fflags, zero
[0x80002fc8]:sw t6, 1680(a5)
[0x80002fcc]:sw a7, 1684(a5)
[0x80002fd0]:fld ft11, 1808(a6)
[0x80002fd4]:fld ft10, 1816(a6)

[0x80002fd8]:fle.d t6, ft11, ft10
[0x80002fdc]:csrrs a7, fflags, zero
[0x80002fe0]:sw t6, 1696(a5)
[0x80002fe4]:sw a7, 1700(a5)
[0x80002fe8]:fld ft11, 1824(a6)
[0x80002fec]:fld ft10, 1832(a6)

[0x80002ff0]:fle.d t6, ft11, ft10
[0x80002ff4]:csrrs a7, fflags, zero
[0x80002ff8]:sw t6, 1712(a5)
[0x80002ffc]:sw a7, 1716(a5)
[0x80003000]:fld ft11, 1840(a6)
[0x80003004]:fld ft10, 1848(a6)

[0x80003008]:fle.d t6, ft11, ft10
[0x8000300c]:csrrs a7, fflags, zero
[0x80003010]:sw t6, 1728(a5)
[0x80003014]:sw a7, 1732(a5)
[0x80003018]:fld ft11, 1856(a6)
[0x8000301c]:fld ft10, 1864(a6)

[0x80003020]:fle.d t6, ft11, ft10
[0x80003024]:csrrs a7, fflags, zero
[0x80003028]:sw t6, 1744(a5)
[0x8000302c]:sw a7, 1748(a5)
[0x80003030]:fld ft11, 1872(a6)
[0x80003034]:fld ft10, 1880(a6)

[0x80003038]:fle.d t6, ft11, ft10
[0x8000303c]:csrrs a7, fflags, zero
[0x80003040]:sw t6, 1760(a5)
[0x80003044]:sw a7, 1764(a5)
[0x80003048]:fld ft11, 1888(a6)
[0x8000304c]:fld ft10, 1896(a6)

[0x80003050]:fle.d t6, ft11, ft10
[0x80003054]:csrrs a7, fflags, zero
[0x80003058]:sw t6, 1776(a5)
[0x8000305c]:sw a7, 1780(a5)
[0x80003060]:fld ft11, 1904(a6)
[0x80003064]:fld ft10, 1912(a6)

[0x80003068]:fle.d t6, ft11, ft10
[0x8000306c]:csrrs a7, fflags, zero
[0x80003070]:sw t6, 1792(a5)
[0x80003074]:sw a7, 1796(a5)
[0x80003078]:fld ft11, 1920(a6)
[0x8000307c]:fld ft10, 1928(a6)

[0x80003080]:fle.d t6, ft11, ft10
[0x80003084]:csrrs a7, fflags, zero
[0x80003088]:sw t6, 1808(a5)
[0x8000308c]:sw a7, 1812(a5)
[0x80003090]:fld ft11, 1936(a6)
[0x80003094]:fld ft10, 1944(a6)

[0x80003098]:fle.d t6, ft11, ft10
[0x8000309c]:csrrs a7, fflags, zero
[0x800030a0]:sw t6, 1824(a5)
[0x800030a4]:sw a7, 1828(a5)
[0x800030a8]:fld ft11, 1952(a6)
[0x800030ac]:fld ft10, 1960(a6)

[0x800030b0]:fle.d t6, ft11, ft10
[0x800030b4]:csrrs a7, fflags, zero
[0x800030b8]:sw t6, 1840(a5)
[0x800030bc]:sw a7, 1844(a5)
[0x800030c0]:fld ft11, 1968(a6)
[0x800030c4]:fld ft10, 1976(a6)

[0x800030c8]:fle.d t6, ft11, ft10
[0x800030cc]:csrrs a7, fflags, zero
[0x800030d0]:sw t6, 1856(a5)
[0x800030d4]:sw a7, 1860(a5)
[0x800030d8]:fld ft11, 1984(a6)
[0x800030dc]:fld ft10, 1992(a6)

[0x800030e0]:fle.d t6, ft11, ft10
[0x800030e4]:csrrs a7, fflags, zero
[0x800030e8]:sw t6, 1872(a5)
[0x800030ec]:sw a7, 1876(a5)
[0x800030f0]:fld ft11, 2000(a6)
[0x800030f4]:fld ft10, 2008(a6)

[0x800030f8]:fle.d t6, ft11, ft10
[0x800030fc]:csrrs a7, fflags, zero
[0x80003100]:sw t6, 1888(a5)
[0x80003104]:sw a7, 1892(a5)
[0x80003108]:fld ft11, 2016(a6)
[0x8000310c]:fld ft10, 2024(a6)

[0x80003110]:fle.d t6, ft11, ft10
[0x80003114]:csrrs a7, fflags, zero
[0x80003118]:sw t6, 1904(a5)
[0x8000311c]:sw a7, 1908(a5)
[0x80003120]:addi a6, a6, 2032
[0x80003124]:fld ft11, 0(a6)
[0x80003128]:fld ft10, 8(a6)

[0x8000312c]:fle.d t6, ft11, ft10
[0x80003130]:csrrs a7, fflags, zero
[0x80003134]:sw t6, 1920(a5)
[0x80003138]:sw a7, 1924(a5)
[0x8000313c]:fld ft11, 16(a6)
[0x80003140]:fld ft10, 24(a6)

[0x80003144]:fle.d t6, ft11, ft10
[0x80003148]:csrrs a7, fflags, zero
[0x8000314c]:sw t6, 1936(a5)
[0x80003150]:sw a7, 1940(a5)
[0x80003154]:fld ft11, 32(a6)
[0x80003158]:fld ft10, 40(a6)

[0x8000315c]:fle.d t6, ft11, ft10
[0x80003160]:csrrs a7, fflags, zero
[0x80003164]:sw t6, 1952(a5)
[0x80003168]:sw a7, 1956(a5)
[0x8000316c]:fld ft11, 48(a6)
[0x80003170]:fld ft10, 56(a6)

[0x80003174]:fle.d t6, ft11, ft10
[0x80003178]:csrrs a7, fflags, zero
[0x8000317c]:sw t6, 1968(a5)
[0x80003180]:sw a7, 1972(a5)
[0x80003184]:fld ft11, 64(a6)
[0x80003188]:fld ft10, 72(a6)

[0x8000318c]:fle.d t6, ft11, ft10
[0x80003190]:csrrs a7, fflags, zero
[0x80003194]:sw t6, 1984(a5)
[0x80003198]:sw a7, 1988(a5)
[0x8000319c]:fld ft11, 80(a6)
[0x800031a0]:fld ft10, 88(a6)

[0x800031a4]:fle.d t6, ft11, ft10
[0x800031a8]:csrrs a7, fflags, zero
[0x800031ac]:sw t6, 2000(a5)
[0x800031b0]:sw a7, 2004(a5)
[0x800031b4]:fld ft11, 96(a6)
[0x800031b8]:fld ft10, 104(a6)

[0x800031bc]:fle.d t6, ft11, ft10
[0x800031c0]:csrrs a7, fflags, zero
[0x800031c4]:sw t6, 2016(a5)
[0x800031c8]:sw a7, 2020(a5)
[0x800031cc]:auipc a5, 5
[0x800031d0]:addi a5, a5, 1116
[0x800031d4]:fld ft11, 112(a6)
[0x800031d8]:fld ft10, 120(a6)

[0x800034dc]:fle.d t6, ft11, ft10
[0x800034e0]:csrrs a7, fflags, zero
[0x800034e4]:sw t6, 512(a5)
[0x800034e8]:sw a7, 516(a5)
[0x800034ec]:fld ft11, 640(a6)
[0x800034f0]:fld ft10, 648(a6)

[0x800034f4]:fle.d t6, ft11, ft10
[0x800034f8]:csrrs a7, fflags, zero
[0x800034fc]:sw t6, 528(a5)
[0x80003500]:sw a7, 532(a5)
[0x80003504]:fld ft11, 656(a6)
[0x80003508]:fld ft10, 664(a6)

[0x8000350c]:fle.d t6, ft11, ft10
[0x80003510]:csrrs a7, fflags, zero
[0x80003514]:sw t6, 544(a5)
[0x80003518]:sw a7, 548(a5)
[0x8000351c]:fld ft11, 672(a6)
[0x80003520]:fld ft10, 680(a6)

[0x80003524]:fle.d t6, ft11, ft10
[0x80003528]:csrrs a7, fflags, zero
[0x8000352c]:sw t6, 560(a5)
[0x80003530]:sw a7, 564(a5)
[0x80003534]:fld ft11, 688(a6)
[0x80003538]:fld ft10, 696(a6)

[0x8000353c]:fle.d t6, ft11, ft10
[0x80003540]:csrrs a7, fflags, zero
[0x80003544]:sw t6, 576(a5)
[0x80003548]:sw a7, 580(a5)
[0x8000354c]:fld ft11, 704(a6)
[0x80003550]:fld ft10, 712(a6)

[0x80003554]:fle.d t6, ft11, ft10
[0x80003558]:csrrs a7, fflags, zero
[0x8000355c]:sw t6, 592(a5)
[0x80003560]:sw a7, 596(a5)
[0x80003564]:fld ft11, 720(a6)
[0x80003568]:fld ft10, 728(a6)

[0x8000356c]:fle.d t6, ft11, ft10
[0x80003570]:csrrs a7, fflags, zero
[0x80003574]:sw t6, 608(a5)
[0x80003578]:sw a7, 612(a5)
[0x8000357c]:fld ft11, 736(a6)
[0x80003580]:fld ft10, 744(a6)

[0x80003584]:fle.d t6, ft11, ft10
[0x80003588]:csrrs a7, fflags, zero
[0x8000358c]:sw t6, 624(a5)
[0x80003590]:sw a7, 628(a5)
[0x80003594]:fld ft11, 752(a6)
[0x80003598]:fld ft10, 760(a6)

[0x8000359c]:fle.d t6, ft11, ft10
[0x800035a0]:csrrs a7, fflags, zero
[0x800035a4]:sw t6, 640(a5)
[0x800035a8]:sw a7, 644(a5)
[0x800035ac]:fld ft11, 768(a6)
[0x800035b0]:fld ft10, 776(a6)

[0x800035b4]:fle.d t6, ft11, ft10
[0x800035b8]:csrrs a7, fflags, zero
[0x800035bc]:sw t6, 656(a5)
[0x800035c0]:sw a7, 660(a5)
[0x800035c4]:fld ft11, 784(a6)
[0x800035c8]:fld ft10, 792(a6)

[0x800035cc]:fle.d t6, ft11, ft10
[0x800035d0]:csrrs a7, fflags, zero
[0x800035d4]:sw t6, 672(a5)
[0x800035d8]:sw a7, 676(a5)
[0x800035dc]:fld ft11, 800(a6)
[0x800035e0]:fld ft10, 808(a6)

[0x800035e4]:fle.d t6, ft11, ft10
[0x800035e8]:csrrs a7, fflags, zero
[0x800035ec]:sw t6, 688(a5)
[0x800035f0]:sw a7, 692(a5)
[0x800035f4]:fld ft11, 816(a6)
[0x800035f8]:fld ft10, 824(a6)

[0x800035fc]:fle.d t6, ft11, ft10
[0x80003600]:csrrs a7, fflags, zero
[0x80003604]:sw t6, 704(a5)
[0x80003608]:sw a7, 708(a5)
[0x8000360c]:fld ft11, 832(a6)
[0x80003610]:fld ft10, 840(a6)

[0x80003614]:fle.d t6, ft11, ft10
[0x80003618]:csrrs a7, fflags, zero
[0x8000361c]:sw t6, 720(a5)
[0x80003620]:sw a7, 724(a5)
[0x80003624]:fld ft11, 848(a6)
[0x80003628]:fld ft10, 856(a6)

[0x8000362c]:fle.d t6, ft11, ft10
[0x80003630]:csrrs a7, fflags, zero
[0x80003634]:sw t6, 736(a5)
[0x80003638]:sw a7, 740(a5)
[0x8000363c]:fld ft11, 864(a6)
[0x80003640]:fld ft10, 872(a6)

[0x80003644]:fle.d t6, ft11, ft10
[0x80003648]:csrrs a7, fflags, zero
[0x8000364c]:sw t6, 752(a5)
[0x80003650]:sw a7, 756(a5)
[0x80003654]:fld ft11, 880(a6)
[0x80003658]:fld ft10, 888(a6)

[0x8000365c]:fle.d t6, ft11, ft10
[0x80003660]:csrrs a7, fflags, zero
[0x80003664]:sw t6, 768(a5)
[0x80003668]:sw a7, 772(a5)
[0x8000366c]:fld ft11, 896(a6)
[0x80003670]:fld ft10, 904(a6)

[0x80003674]:fle.d t6, ft11, ft10
[0x80003678]:csrrs a7, fflags, zero
[0x8000367c]:sw t6, 784(a5)
[0x80003680]:sw a7, 788(a5)
[0x80003684]:fld ft11, 912(a6)
[0x80003688]:fld ft10, 920(a6)

[0x8000368c]:fle.d t6, ft11, ft10
[0x80003690]:csrrs a7, fflags, zero
[0x80003694]:sw t6, 800(a5)
[0x80003698]:sw a7, 804(a5)
[0x8000369c]:fld ft11, 928(a6)
[0x800036a0]:fld ft10, 936(a6)

[0x800036a4]:fle.d t6, ft11, ft10
[0x800036a8]:csrrs a7, fflags, zero
[0x800036ac]:sw t6, 816(a5)
[0x800036b0]:sw a7, 820(a5)
[0x800036b4]:fld ft11, 944(a6)
[0x800036b8]:fld ft10, 952(a6)

[0x800036bc]:fle.d t6, ft11, ft10
[0x800036c0]:csrrs a7, fflags, zero
[0x800036c4]:sw t6, 832(a5)
[0x800036c8]:sw a7, 836(a5)
[0x800036cc]:fld ft11, 960(a6)
[0x800036d0]:fld ft10, 968(a6)

[0x800036d4]:fle.d t6, ft11, ft10
[0x800036d8]:csrrs a7, fflags, zero
[0x800036dc]:sw t6, 848(a5)
[0x800036e0]:sw a7, 852(a5)
[0x800036e4]:fld ft11, 976(a6)
[0x800036e8]:fld ft10, 984(a6)

[0x800036ec]:fle.d t6, ft11, ft10
[0x800036f0]:csrrs a7, fflags, zero
[0x800036f4]:sw t6, 864(a5)
[0x800036f8]:sw a7, 868(a5)
[0x800036fc]:fld ft11, 992(a6)
[0x80003700]:fld ft10, 1000(a6)

[0x80003704]:fle.d t6, ft11, ft10
[0x80003708]:csrrs a7, fflags, zero
[0x8000370c]:sw t6, 880(a5)
[0x80003710]:sw a7, 884(a5)
[0x80003714]:fld ft11, 1008(a6)
[0x80003718]:fld ft10, 1016(a6)

[0x8000371c]:fle.d t6, ft11, ft10
[0x80003720]:csrrs a7, fflags, zero
[0x80003724]:sw t6, 896(a5)
[0x80003728]:sw a7, 900(a5)
[0x8000372c]:fld ft11, 1024(a6)
[0x80003730]:fld ft10, 1032(a6)

[0x80003734]:fle.d t6, ft11, ft10
[0x80003738]:csrrs a7, fflags, zero
[0x8000373c]:sw t6, 912(a5)
[0x80003740]:sw a7, 916(a5)
[0x80003744]:fld ft11, 1040(a6)
[0x80003748]:fld ft10, 1048(a6)

[0x8000374c]:fle.d t6, ft11, ft10
[0x80003750]:csrrs a7, fflags, zero
[0x80003754]:sw t6, 928(a5)
[0x80003758]:sw a7, 932(a5)
[0x8000375c]:fld ft11, 1056(a6)
[0x80003760]:fld ft10, 1064(a6)

[0x80003764]:fle.d t6, ft11, ft10
[0x80003768]:csrrs a7, fflags, zero
[0x8000376c]:sw t6, 944(a5)
[0x80003770]:sw a7, 948(a5)
[0x80003774]:fld ft11, 1072(a6)
[0x80003778]:fld ft10, 1080(a6)

[0x8000377c]:fle.d t6, ft11, ft10
[0x80003780]:csrrs a7, fflags, zero
[0x80003784]:sw t6, 960(a5)
[0x80003788]:sw a7, 964(a5)
[0x8000378c]:fld ft11, 1088(a6)
[0x80003790]:fld ft10, 1096(a6)

[0x80003794]:fle.d t6, ft11, ft10
[0x80003798]:csrrs a7, fflags, zero
[0x8000379c]:sw t6, 976(a5)
[0x800037a0]:sw a7, 980(a5)
[0x800037a4]:fld ft11, 1104(a6)
[0x800037a8]:fld ft10, 1112(a6)



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fle.d', 'rd : x10', 'rs1 : f24', 'rs2 : f17', 'rs1 != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fle.d a0, fs8, fa7
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw a0, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80007614]:0x00000000




Last Coverpoint : ['rd : x15', 'rs1 : f29', 'rs2 : f29', 'rs1 == rs2', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000140]:fle.d a5, ft9, ft9
	-[0x80000144]:csrrs s5, fflags, zero
	-[0x80000148]:sw a5, 0(s3)
Current Store : [0x8000014c] : sw s5, 4(s3) -- Store: [0x8000761c]:0x00000000




Last Coverpoint : ['rd : x18', 'rs1 : f7', 'rs2 : f9', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fle.d s2, ft7, fs1
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s2, 0(a5)
Current Store : [0x80000170] : sw a7, 4(a5) -- Store: [0x80007624]:0x00000000




Last Coverpoint : ['rd : x16', 'rs1 : f22', 'rs2 : f28', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000188]:fle.d a6, fs6, ft8
	-[0x8000018c]:csrrs s5, fflags, zero
	-[0x80000190]:sw a6, 0(s3)
Current Store : [0x80000194] : sw s5, 4(s3) -- Store: [0x8000762c]:0x00000010




Last Coverpoint : ['rd : x0', 'rs1 : f5', 'rs2 : f4', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fle.d zero, ft5, ft4
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw zero, 0(a5)
Current Store : [0x800001b8] : sw a7, 4(a5) -- Store: [0x80007634]:0x00000010




Last Coverpoint : ['rd : x1', 'rs1 : f4', 'rs2 : f18', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fle.d ra, ft4, fs2
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw ra, 16(a5)
Current Store : [0x800001d0] : sw a7, 20(a5) -- Store: [0x80007644]:0x00000010




Last Coverpoint : ['rd : x17', 'rs1 : f12', 'rs2 : f7', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001e8]:fle.d a7, fa2, ft7
	-[0x800001ec]:csrrs s5, fflags, zero
	-[0x800001f0]:sw a7, 0(s3)
Current Store : [0x800001f4] : sw s5, 4(s3) -- Store: [0x80007644]:0x00000010




Last Coverpoint : ['rd : x26', 'rs1 : f6', 'rs2 : f3', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fle.d s10, ft6, ft3
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s10, 0(a5)
Current Store : [0x80000218] : sw a7, 4(a5) -- Store: [0x8000764c]:0x00000010




Last Coverpoint : ['rd : x14', 'rs1 : f15', 'rs2 : f14', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fle.d a4, fa5, fa4
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw a4, 16(a5)
Current Store : [0x80000230] : sw a7, 20(a5) -- Store: [0x8000765c]:0x00000010




Last Coverpoint : ['rd : x29', 'rs1 : f3', 'rs2 : f31', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fle.d t4, ft3, ft11
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw t4, 32(a5)
Current Store : [0x80000248] : sw a7, 36(a5) -- Store: [0x8000766c]:0x00000010




Last Coverpoint : ['rd : x22', 'rs1 : f21', 'rs2 : f0', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fle.d s6, fs5, ft0
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw s6, 48(a5)
Current Store : [0x80000260] : sw a7, 52(a5) -- Store: [0x8000767c]:0x00000010




Last Coverpoint : ['rd : x8', 'rs1 : f17', 'rs2 : f6', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fle.d fp, fa7, ft6
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw fp, 64(a5)
Current Store : [0x80000278] : sw a7, 68(a5) -- Store: [0x8000768c]:0x00000010




Last Coverpoint : ['rd : x28', 'rs1 : f13', 'rs2 : f1', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000284]:fle.d t3, fa3, ft1
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw t3, 80(a5)
Current Store : [0x80000290] : sw a7, 84(a5) -- Store: [0x8000769c]:0x00000010




Last Coverpoint : ['rd : x27', 'rs1 : f9', 'rs2 : f25', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fle.d s11, fs1, fs9
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw s11, 96(a5)
Current Store : [0x800002a8] : sw a7, 100(a5) -- Store: [0x800076ac]:0x00000010




Last Coverpoint : ['rd : x25', 'rs1 : f30', 'rs2 : f20', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fle.d s9, ft10, fs4
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw s9, 112(a5)
Current Store : [0x800002c0] : sw a7, 116(a5) -- Store: [0x800076bc]:0x00000010




Last Coverpoint : ['rd : x4', 'rs1 : f20', 'rs2 : f13', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fle.d tp, fs4, fa3
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw tp, 128(a5)
Current Store : [0x800002d8] : sw a7, 132(a5) -- Store: [0x800076cc]:0x00000010




Last Coverpoint : ['rd : x5', 'rs1 : f28', 'rs2 : f21', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fle.d t0, ft8, fs5
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw t0, 144(a5)
Current Store : [0x800002f0] : sw a7, 148(a5) -- Store: [0x800076dc]:0x00000010




Last Coverpoint : ['rd : x21', 'rs1 : f27', 'rs2 : f8', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fle.d s5, fs11, fs0
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw s5, 160(a5)
Current Store : [0x80000308] : sw a7, 164(a5) -- Store: [0x800076ec]:0x00000010




Last Coverpoint : ['rd : x2', 'rs1 : f1', 'rs2 : f15', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000314]:fle.d sp, ft1, fa5
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw sp, 176(a5)
Current Store : [0x80000320] : sw a7, 180(a5) -- Store: [0x800076fc]:0x00000010




Last Coverpoint : ['rd : x3', 'rs1 : f25', 'rs2 : f16', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fle.d gp, fs9, fa6
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw gp, 192(a5)
Current Store : [0x80000338] : sw a7, 196(a5) -- Store: [0x8000770c]:0x00000010




Last Coverpoint : ['rd : x19', 'rs1 : f8', 'rs2 : f10', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fle.d s3, fs0, fa0
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw s3, 208(a5)
Current Store : [0x80000350] : sw a7, 212(a5) -- Store: [0x8000771c]:0x00000010




Last Coverpoint : ['rd : x23', 'rs1 : f31', 'rs2 : f5', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fle.d s7, ft11, ft5
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw s7, 224(a5)
Current Store : [0x80000368] : sw a7, 228(a5) -- Store: [0x8000772c]:0x00000010




Last Coverpoint : ['rd : x11', 'rs1 : f26', 'rs2 : f27', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000374]:fle.d a1, fs10, fs11
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw a1, 240(a5)
Current Store : [0x80000380] : sw a7, 244(a5) -- Store: [0x8000773c]:0x00000010




Last Coverpoint : ['rd : x13', 'rs1 : f16', 'rs2 : f30', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fle.d a3, fa6, ft10
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw a3, 256(a5)
Current Store : [0x80000398] : sw a7, 260(a5) -- Store: [0x8000774c]:0x00000010




Last Coverpoint : ['rd : x7', 'rs1 : f23', 'rs2 : f19', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fle.d t2, fs7, fs3
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw t2, 272(a5)
Current Store : [0x800003b0] : sw a7, 276(a5) -- Store: [0x8000775c]:0x00000010




Last Coverpoint : ['rd : x20', 'rs1 : f0', 'rs2 : f26', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fle.d s4, ft0, fs10
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw s4, 288(a5)
Current Store : [0x800003c8] : sw a7, 292(a5) -- Store: [0x8000776c]:0x00000010




Last Coverpoint : ['rd : x30', 'rs1 : f18', 'rs2 : f23', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fle.d t5, fs2, fs7
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw t5, 304(a5)
Current Store : [0x800003e0] : sw a7, 308(a5) -- Store: [0x8000777c]:0x00000010




Last Coverpoint : ['rd : x6', 'rs1 : f14', 'rs2 : f12', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fle.d t1, fa4, fa2
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw t1, 320(a5)
Current Store : [0x800003f8] : sw a7, 324(a5) -- Store: [0x8000778c]:0x00000010




Last Coverpoint : ['rd : x9', 'rs1 : f19', 'rs2 : f11', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000404]:fle.d s1, fs3, fa1
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw s1, 336(a5)
Current Store : [0x80000410] : sw a7, 340(a5) -- Store: [0x8000779c]:0x00000010




Last Coverpoint : ['rd : x12', 'rs1 : f10', 'rs2 : f2', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fle.d a2, fa0, ft2
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw a2, 352(a5)
Current Store : [0x80000428] : sw a7, 356(a5) -- Store: [0x800077ac]:0x00000010




Last Coverpoint : ['rd : x31', 'rs1 : f11', 'rs2 : f24', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000434]:fle.d t6, fa1, fs8
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw t6, 368(a5)
Current Store : [0x80000440] : sw a7, 372(a5) -- Store: [0x800077bc]:0x00000010




Last Coverpoint : ['rd : x24', 'rs1 : f2', 'rs2 : f22', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000044c]:fle.d s8, ft2, fs6
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw s8, 384(a5)
Current Store : [0x80000458] : sw a7, 388(a5) -- Store: [0x800077cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000464]:fle.d t6, ft11, ft10
	-[0x80000468]:csrrs a7, fflags, zero
	-[0x8000046c]:sw t6, 400(a5)
Current Store : [0x80000470] : sw a7, 404(a5) -- Store: [0x800077dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fle.d t6, ft11, ft10
	-[0x80000480]:csrrs a7, fflags, zero
	-[0x80000484]:sw t6, 416(a5)
Current Store : [0x80000488] : sw a7, 420(a5) -- Store: [0x800077ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000494]:fle.d t6, ft11, ft10
	-[0x80000498]:csrrs a7, fflags, zero
	-[0x8000049c]:sw t6, 432(a5)
Current Store : [0x800004a0] : sw a7, 436(a5) -- Store: [0x800077fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fle.d t6, ft11, ft10
	-[0x800004b0]:csrrs a7, fflags, zero
	-[0x800004b4]:sw t6, 448(a5)
Current Store : [0x800004b8] : sw a7, 452(a5) -- Store: [0x8000780c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fle.d t6, ft11, ft10
	-[0x800004c8]:csrrs a7, fflags, zero
	-[0x800004cc]:sw t6, 464(a5)
Current Store : [0x800004d0] : sw a7, 468(a5) -- Store: [0x8000781c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fle.d t6, ft11, ft10
	-[0x800004e0]:csrrs a7, fflags, zero
	-[0x800004e4]:sw t6, 480(a5)
Current Store : [0x800004e8] : sw a7, 484(a5) -- Store: [0x8000782c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fle.d t6, ft11, ft10
	-[0x800004f8]:csrrs a7, fflags, zero
	-[0x800004fc]:sw t6, 496(a5)
Current Store : [0x80000500] : sw a7, 500(a5) -- Store: [0x8000783c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000050c]:fle.d t6, ft11, ft10
	-[0x80000510]:csrrs a7, fflags, zero
	-[0x80000514]:sw t6, 512(a5)
Current Store : [0x80000518] : sw a7, 516(a5) -- Store: [0x8000784c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000524]:fle.d t6, ft11, ft10
	-[0x80000528]:csrrs a7, fflags, zero
	-[0x8000052c]:sw t6, 528(a5)
Current Store : [0x80000530] : sw a7, 532(a5) -- Store: [0x8000785c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fle.d t6, ft11, ft10
	-[0x80000540]:csrrs a7, fflags, zero
	-[0x80000544]:sw t6, 544(a5)
Current Store : [0x80000548] : sw a7, 548(a5) -- Store: [0x8000786c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000554]:fle.d t6, ft11, ft10
	-[0x80000558]:csrrs a7, fflags, zero
	-[0x8000055c]:sw t6, 560(a5)
Current Store : [0x80000560] : sw a7, 564(a5) -- Store: [0x8000787c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000056c]:fle.d t6, ft11, ft10
	-[0x80000570]:csrrs a7, fflags, zero
	-[0x80000574]:sw t6, 576(a5)
Current Store : [0x80000578] : sw a7, 580(a5) -- Store: [0x8000788c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000584]:fle.d t6, ft11, ft10
	-[0x80000588]:csrrs a7, fflags, zero
	-[0x8000058c]:sw t6, 592(a5)
Current Store : [0x80000590] : sw a7, 596(a5) -- Store: [0x8000789c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fle.d t6, ft11, ft10
	-[0x800005a0]:csrrs a7, fflags, zero
	-[0x800005a4]:sw t6, 608(a5)
Current Store : [0x800005a8] : sw a7, 612(a5) -- Store: [0x800078ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fle.d t6, ft11, ft10
	-[0x800005b8]:csrrs a7, fflags, zero
	-[0x800005bc]:sw t6, 624(a5)
Current Store : [0x800005c0] : sw a7, 628(a5) -- Store: [0x800078bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fle.d t6, ft11, ft10
	-[0x800005d0]:csrrs a7, fflags, zero
	-[0x800005d4]:sw t6, 640(a5)
Current Store : [0x800005d8] : sw a7, 644(a5) -- Store: [0x800078cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fle.d t6, ft11, ft10
	-[0x800005e8]:csrrs a7, fflags, zero
	-[0x800005ec]:sw t6, 656(a5)
Current Store : [0x800005f0] : sw a7, 660(a5) -- Store: [0x800078dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fle.d t6, ft11, ft10
	-[0x80000600]:csrrs a7, fflags, zero
	-[0x80000604]:sw t6, 672(a5)
Current Store : [0x80000608] : sw a7, 676(a5) -- Store: [0x800078ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000614]:fle.d t6, ft11, ft10
	-[0x80000618]:csrrs a7, fflags, zero
	-[0x8000061c]:sw t6, 688(a5)
Current Store : [0x80000620] : sw a7, 692(a5) -- Store: [0x800078fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000062c]:fle.d t6, ft11, ft10
	-[0x80000630]:csrrs a7, fflags, zero
	-[0x80000634]:sw t6, 704(a5)
Current Store : [0x80000638] : sw a7, 708(a5) -- Store: [0x8000790c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000644]:fle.d t6, ft11, ft10
	-[0x80000648]:csrrs a7, fflags, zero
	-[0x8000064c]:sw t6, 720(a5)
Current Store : [0x80000650] : sw a7, 724(a5) -- Store: [0x8000791c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fle.d t6, ft11, ft10
	-[0x80000660]:csrrs a7, fflags, zero
	-[0x80000664]:sw t6, 736(a5)
Current Store : [0x80000668] : sw a7, 740(a5) -- Store: [0x8000792c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000674]:fle.d t6, ft11, ft10
	-[0x80000678]:csrrs a7, fflags, zero
	-[0x8000067c]:sw t6, 752(a5)
Current Store : [0x80000680] : sw a7, 756(a5) -- Store: [0x8000793c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000068c]:fle.d t6, ft11, ft10
	-[0x80000690]:csrrs a7, fflags, zero
	-[0x80000694]:sw t6, 768(a5)
Current Store : [0x80000698] : sw a7, 772(a5) -- Store: [0x8000794c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fle.d t6, ft11, ft10
	-[0x800006a8]:csrrs a7, fflags, zero
	-[0x800006ac]:sw t6, 784(a5)
Current Store : [0x800006b0] : sw a7, 788(a5) -- Store: [0x8000795c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fle.d t6, ft11, ft10
	-[0x800006c0]:csrrs a7, fflags, zero
	-[0x800006c4]:sw t6, 800(a5)
Current Store : [0x800006c8] : sw a7, 804(a5) -- Store: [0x8000796c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fle.d t6, ft11, ft10
	-[0x800006d8]:csrrs a7, fflags, zero
	-[0x800006dc]:sw t6, 816(a5)
Current Store : [0x800006e0] : sw a7, 820(a5) -- Store: [0x8000797c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fle.d t6, ft11, ft10
	-[0x800006f0]:csrrs a7, fflags, zero
	-[0x800006f4]:sw t6, 832(a5)
Current Store : [0x800006f8] : sw a7, 836(a5) -- Store: [0x8000798c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000704]:fle.d t6, ft11, ft10
	-[0x80000708]:csrrs a7, fflags, zero
	-[0x8000070c]:sw t6, 848(a5)
Current Store : [0x80000710] : sw a7, 852(a5) -- Store: [0x8000799c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000071c]:fle.d t6, ft11, ft10
	-[0x80000720]:csrrs a7, fflags, zero
	-[0x80000724]:sw t6, 864(a5)
Current Store : [0x80000728] : sw a7, 868(a5) -- Store: [0x800079ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000734]:fle.d t6, ft11, ft10
	-[0x80000738]:csrrs a7, fflags, zero
	-[0x8000073c]:sw t6, 880(a5)
Current Store : [0x80000740] : sw a7, 884(a5) -- Store: [0x800079bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fle.d t6, ft11, ft10
	-[0x80000750]:csrrs a7, fflags, zero
	-[0x80000754]:sw t6, 896(a5)
Current Store : [0x80000758] : sw a7, 900(a5) -- Store: [0x800079cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000764]:fle.d t6, ft11, ft10
	-[0x80000768]:csrrs a7, fflags, zero
	-[0x8000076c]:sw t6, 912(a5)
Current Store : [0x80000770] : sw a7, 916(a5) -- Store: [0x800079dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000077c]:fle.d t6, ft11, ft10
	-[0x80000780]:csrrs a7, fflags, zero
	-[0x80000784]:sw t6, 928(a5)
Current Store : [0x80000788] : sw a7, 932(a5) -- Store: [0x800079ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000794]:fle.d t6, ft11, ft10
	-[0x80000798]:csrrs a7, fflags, zero
	-[0x8000079c]:sw t6, 944(a5)
Current Store : [0x800007a0] : sw a7, 948(a5) -- Store: [0x800079fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fle.d t6, ft11, ft10
	-[0x800007b0]:csrrs a7, fflags, zero
	-[0x800007b4]:sw t6, 960(a5)
Current Store : [0x800007b8] : sw a7, 964(a5) -- Store: [0x80007a0c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007c4]:fle.d t6, ft11, ft10
	-[0x800007c8]:csrrs a7, fflags, zero
	-[0x800007cc]:sw t6, 976(a5)
Current Store : [0x800007d0] : sw a7, 980(a5) -- Store: [0x80007a1c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007dc]:fle.d t6, ft11, ft10
	-[0x800007e0]:csrrs a7, fflags, zero
	-[0x800007e4]:sw t6, 992(a5)
Current Store : [0x800007e8] : sw a7, 996(a5) -- Store: [0x80007a2c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fle.d t6, ft11, ft10
	-[0x800007f8]:csrrs a7, fflags, zero
	-[0x800007fc]:sw t6, 1008(a5)
Current Store : [0x80000800] : sw a7, 1012(a5) -- Store: [0x80007a3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fle.d t6, ft11, ft10
	-[0x80000810]:csrrs a7, fflags, zero
	-[0x80000814]:sw t6, 1024(a5)
Current Store : [0x80000818] : sw a7, 1028(a5) -- Store: [0x80007a4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000824]:fle.d t6, ft11, ft10
	-[0x80000828]:csrrs a7, fflags, zero
	-[0x8000082c]:sw t6, 1040(a5)
Current Store : [0x80000830] : sw a7, 1044(a5) -- Store: [0x80007a5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000083c]:fle.d t6, ft11, ft10
	-[0x80000840]:csrrs a7, fflags, zero
	-[0x80000844]:sw t6, 1056(a5)
Current Store : [0x80000848] : sw a7, 1060(a5) -- Store: [0x80007a6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000854]:fle.d t6, ft11, ft10
	-[0x80000858]:csrrs a7, fflags, zero
	-[0x8000085c]:sw t6, 1072(a5)
Current Store : [0x80000860] : sw a7, 1076(a5) -- Store: [0x80007a7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fle.d t6, ft11, ft10
	-[0x80000870]:csrrs a7, fflags, zero
	-[0x80000874]:sw t6, 1088(a5)
Current Store : [0x80000878] : sw a7, 1092(a5) -- Store: [0x80007a8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000884]:fle.d t6, ft11, ft10
	-[0x80000888]:csrrs a7, fflags, zero
	-[0x8000088c]:sw t6, 1104(a5)
Current Store : [0x80000890] : sw a7, 1108(a5) -- Store: [0x80007a9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000089c]:fle.d t6, ft11, ft10
	-[0x800008a0]:csrrs a7, fflags, zero
	-[0x800008a4]:sw t6, 1120(a5)
Current Store : [0x800008a8] : sw a7, 1124(a5) -- Store: [0x80007aac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fle.d t6, ft11, ft10
	-[0x800008b8]:csrrs a7, fflags, zero
	-[0x800008bc]:sw t6, 1136(a5)
Current Store : [0x800008c0] : sw a7, 1140(a5) -- Store: [0x80007abc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fle.d t6, ft11, ft10
	-[0x800008d0]:csrrs a7, fflags, zero
	-[0x800008d4]:sw t6, 1152(a5)
Current Store : [0x800008d8] : sw a7, 1156(a5) -- Store: [0x80007acc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008e4]:fle.d t6, ft11, ft10
	-[0x800008e8]:csrrs a7, fflags, zero
	-[0x800008ec]:sw t6, 1168(a5)
Current Store : [0x800008f0] : sw a7, 1172(a5) -- Store: [0x80007adc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008fc]:fle.d t6, ft11, ft10
	-[0x80000900]:csrrs a7, fflags, zero
	-[0x80000904]:sw t6, 1184(a5)
Current Store : [0x80000908] : sw a7, 1188(a5) -- Store: [0x80007aec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000914]:fle.d t6, ft11, ft10
	-[0x80000918]:csrrs a7, fflags, zero
	-[0x8000091c]:sw t6, 1200(a5)
Current Store : [0x80000920] : sw a7, 1204(a5) -- Store: [0x80007afc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fle.d t6, ft11, ft10
	-[0x80000930]:csrrs a7, fflags, zero
	-[0x80000934]:sw t6, 1216(a5)
Current Store : [0x80000938] : sw a7, 1220(a5) -- Store: [0x80007b0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000944]:fle.d t6, ft11, ft10
	-[0x80000948]:csrrs a7, fflags, zero
	-[0x8000094c]:sw t6, 1232(a5)
Current Store : [0x80000950] : sw a7, 1236(a5) -- Store: [0x80007b1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000095c]:fle.d t6, ft11, ft10
	-[0x80000960]:csrrs a7, fflags, zero
	-[0x80000964]:sw t6, 1248(a5)
Current Store : [0x80000968] : sw a7, 1252(a5) -- Store: [0x80007b2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000974]:fle.d t6, ft11, ft10
	-[0x80000978]:csrrs a7, fflags, zero
	-[0x8000097c]:sw t6, 1264(a5)
Current Store : [0x80000980] : sw a7, 1268(a5) -- Store: [0x80007b3c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fle.d t6, ft11, ft10
	-[0x80000990]:csrrs a7, fflags, zero
	-[0x80000994]:sw t6, 1280(a5)
Current Store : [0x80000998] : sw a7, 1284(a5) -- Store: [0x80007b4c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009a4]:fle.d t6, ft11, ft10
	-[0x800009a8]:csrrs a7, fflags, zero
	-[0x800009ac]:sw t6, 1296(a5)
Current Store : [0x800009b0] : sw a7, 1300(a5) -- Store: [0x80007b5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009bc]:fle.d t6, ft11, ft10
	-[0x800009c0]:csrrs a7, fflags, zero
	-[0x800009c4]:sw t6, 1312(a5)
Current Store : [0x800009c8] : sw a7, 1316(a5) -- Store: [0x80007b6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fle.d t6, ft11, ft10
	-[0x800009d8]:csrrs a7, fflags, zero
	-[0x800009dc]:sw t6, 1328(a5)
Current Store : [0x800009e0] : sw a7, 1332(a5) -- Store: [0x80007b7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fle.d t6, ft11, ft10
	-[0x800009f0]:csrrs a7, fflags, zero
	-[0x800009f4]:sw t6, 1344(a5)
Current Store : [0x800009f8] : sw a7, 1348(a5) -- Store: [0x80007b8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fle.d t6, ft11, ft10
	-[0x80000a08]:csrrs a7, fflags, zero
	-[0x80000a0c]:sw t6, 1360(a5)
Current Store : [0x80000a10] : sw a7, 1364(a5) -- Store: [0x80007b9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a1c]:fle.d t6, ft11, ft10
	-[0x80000a20]:csrrs a7, fflags, zero
	-[0x80000a24]:sw t6, 1376(a5)
Current Store : [0x80000a28] : sw a7, 1380(a5) -- Store: [0x80007bac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fle.d t6, ft11, ft10
	-[0x80000a38]:csrrs a7, fflags, zero
	-[0x80000a3c]:sw t6, 1392(a5)
Current Store : [0x80000a40] : sw a7, 1396(a5) -- Store: [0x80007bbc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fle.d t6, ft11, ft10
	-[0x80000a50]:csrrs a7, fflags, zero
	-[0x80000a54]:sw t6, 1408(a5)
Current Store : [0x80000a58] : sw a7, 1412(a5) -- Store: [0x80007bcc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a64]:fle.d t6, ft11, ft10
	-[0x80000a68]:csrrs a7, fflags, zero
	-[0x80000a6c]:sw t6, 1424(a5)
Current Store : [0x80000a70] : sw a7, 1428(a5) -- Store: [0x80007bdc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a7c]:fle.d t6, ft11, ft10
	-[0x80000a80]:csrrs a7, fflags, zero
	-[0x80000a84]:sw t6, 1440(a5)
Current Store : [0x80000a88] : sw a7, 1444(a5) -- Store: [0x80007bec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fle.d t6, ft11, ft10
	-[0x80000a98]:csrrs a7, fflags, zero
	-[0x80000a9c]:sw t6, 1456(a5)
Current Store : [0x80000aa0] : sw a7, 1460(a5) -- Store: [0x80007bfc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fle.d t6, ft11, ft10
	-[0x80000ab0]:csrrs a7, fflags, zero
	-[0x80000ab4]:sw t6, 1472(a5)
Current Store : [0x80000ab8] : sw a7, 1476(a5) -- Store: [0x80007c0c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ac4]:fle.d t6, ft11, ft10
	-[0x80000ac8]:csrrs a7, fflags, zero
	-[0x80000acc]:sw t6, 1488(a5)
Current Store : [0x80000ad0] : sw a7, 1492(a5) -- Store: [0x80007c1c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000adc]:fle.d t6, ft11, ft10
	-[0x80000ae0]:csrrs a7, fflags, zero
	-[0x80000ae4]:sw t6, 1504(a5)
Current Store : [0x80000ae8] : sw a7, 1508(a5) -- Store: [0x80007c2c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fle.d t6, ft11, ft10
	-[0x80000af8]:csrrs a7, fflags, zero
	-[0x80000afc]:sw t6, 1520(a5)
Current Store : [0x80000b00] : sw a7, 1524(a5) -- Store: [0x80007c3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fle.d t6, ft11, ft10
	-[0x80000b10]:csrrs a7, fflags, zero
	-[0x80000b14]:sw t6, 1536(a5)
Current Store : [0x80000b18] : sw a7, 1540(a5) -- Store: [0x80007c4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b24]:fle.d t6, ft11, ft10
	-[0x80000b28]:csrrs a7, fflags, zero
	-[0x80000b2c]:sw t6, 1552(a5)
Current Store : [0x80000b30] : sw a7, 1556(a5) -- Store: [0x80007c5c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b3c]:fle.d t6, ft11, ft10
	-[0x80000b40]:csrrs a7, fflags, zero
	-[0x80000b44]:sw t6, 1568(a5)
Current Store : [0x80000b48] : sw a7, 1572(a5) -- Store: [0x80007c6c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fle.d t6, ft11, ft10
	-[0x80000b58]:csrrs a7, fflags, zero
	-[0x80000b5c]:sw t6, 1584(a5)
Current Store : [0x80000b60] : sw a7, 1588(a5) -- Store: [0x80007c7c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fle.d t6, ft11, ft10
	-[0x80000b70]:csrrs a7, fflags, zero
	-[0x80000b74]:sw t6, 1600(a5)
Current Store : [0x80000b78] : sw a7, 1604(a5) -- Store: [0x80007c8c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b84]:fle.d t6, ft11, ft10
	-[0x80000b88]:csrrs a7, fflags, zero
	-[0x80000b8c]:sw t6, 1616(a5)
Current Store : [0x80000b90] : sw a7, 1620(a5) -- Store: [0x80007c9c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000b9c]:fle.d t6, ft11, ft10
	-[0x80000ba0]:csrrs a7, fflags, zero
	-[0x80000ba4]:sw t6, 1632(a5)
Current Store : [0x80000ba8] : sw a7, 1636(a5) -- Store: [0x80007cac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fle.d t6, ft11, ft10
	-[0x80000bb8]:csrrs a7, fflags, zero
	-[0x80000bbc]:sw t6, 1648(a5)
Current Store : [0x80000bc0] : sw a7, 1652(a5) -- Store: [0x80007cbc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fle.d t6, ft11, ft10
	-[0x80000bd0]:csrrs a7, fflags, zero
	-[0x80000bd4]:sw t6, 1664(a5)
Current Store : [0x80000bd8] : sw a7, 1668(a5) -- Store: [0x80007ccc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000be4]:fle.d t6, ft11, ft10
	-[0x80000be8]:csrrs a7, fflags, zero
	-[0x80000bec]:sw t6, 1680(a5)
Current Store : [0x80000bf0] : sw a7, 1684(a5) -- Store: [0x80007cdc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fle.d t6, ft11, ft10
	-[0x80000c00]:csrrs a7, fflags, zero
	-[0x80000c04]:sw t6, 1696(a5)
Current Store : [0x80000c08] : sw a7, 1700(a5) -- Store: [0x80007cec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c14]:fle.d t6, ft11, ft10
	-[0x80000c18]:csrrs a7, fflags, zero
	-[0x80000c1c]:sw t6, 1712(a5)
Current Store : [0x80000c20] : sw a7, 1716(a5) -- Store: [0x80007cfc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fle.d t6, ft11, ft10
	-[0x80000c30]:csrrs a7, fflags, zero
	-[0x80000c34]:sw t6, 1728(a5)
Current Store : [0x80000c38] : sw a7, 1732(a5) -- Store: [0x80007d0c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c44]:fle.d t6, ft11, ft10
	-[0x80000c48]:csrrs a7, fflags, zero
	-[0x80000c4c]:sw t6, 1744(a5)
Current Store : [0x80000c50] : sw a7, 1748(a5) -- Store: [0x80007d1c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c5c]:fle.d t6, ft11, ft10
	-[0x80000c60]:csrrs a7, fflags, zero
	-[0x80000c64]:sw t6, 1760(a5)
Current Store : [0x80000c68] : sw a7, 1764(a5) -- Store: [0x80007d2c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fle.d t6, ft11, ft10
	-[0x80000c78]:csrrs a7, fflags, zero
	-[0x80000c7c]:sw t6, 1776(a5)
Current Store : [0x80000c80] : sw a7, 1780(a5) -- Store: [0x80007d3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fle.d t6, ft11, ft10
	-[0x80000c90]:csrrs a7, fflags, zero
	-[0x80000c94]:sw t6, 1792(a5)
Current Store : [0x80000c98] : sw a7, 1796(a5) -- Store: [0x80007d4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fle.d t6, ft11, ft10
	-[0x80000ca8]:csrrs a7, fflags, zero
	-[0x80000cac]:sw t6, 1808(a5)
Current Store : [0x80000cb0] : sw a7, 1812(a5) -- Store: [0x80007d5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000cbc]:fle.d t6, ft11, ft10
	-[0x80000cc0]:csrrs a7, fflags, zero
	-[0x80000cc4]:sw t6, 1824(a5)
Current Store : [0x80000cc8] : sw a7, 1828(a5) -- Store: [0x80007d6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fle.d t6, ft11, ft10
	-[0x80000cd8]:csrrs a7, fflags, zero
	-[0x80000cdc]:sw t6, 1840(a5)
Current Store : [0x80000ce0] : sw a7, 1844(a5) -- Store: [0x80007d7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fle.d t6, ft11, ft10
	-[0x80000cf0]:csrrs a7, fflags, zero
	-[0x80000cf4]:sw t6, 1856(a5)
Current Store : [0x80000cf8] : sw a7, 1860(a5) -- Store: [0x80007d8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d04]:fle.d t6, ft11, ft10
	-[0x80000d08]:csrrs a7, fflags, zero
	-[0x80000d0c]:sw t6, 1872(a5)
Current Store : [0x80000d10] : sw a7, 1876(a5) -- Store: [0x80007d9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d1c]:fle.d t6, ft11, ft10
	-[0x80000d20]:csrrs a7, fflags, zero
	-[0x80000d24]:sw t6, 1888(a5)
Current Store : [0x80000d28] : sw a7, 1892(a5) -- Store: [0x80007dac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fle.d t6, ft11, ft10
	-[0x80000d38]:csrrs a7, fflags, zero
	-[0x80000d3c]:sw t6, 1904(a5)
Current Store : [0x80000d40] : sw a7, 1908(a5) -- Store: [0x80007dbc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d50]:fle.d t6, ft11, ft10
	-[0x80000d54]:csrrs a7, fflags, zero
	-[0x80000d58]:sw t6, 1920(a5)
Current Store : [0x80000d5c] : sw a7, 1924(a5) -- Store: [0x80007dcc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d68]:fle.d t6, ft11, ft10
	-[0x80000d6c]:csrrs a7, fflags, zero
	-[0x80000d70]:sw t6, 1936(a5)
Current Store : [0x80000d74] : sw a7, 1940(a5) -- Store: [0x80007ddc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d80]:fle.d t6, ft11, ft10
	-[0x80000d84]:csrrs a7, fflags, zero
	-[0x80000d88]:sw t6, 1952(a5)
Current Store : [0x80000d8c] : sw a7, 1956(a5) -- Store: [0x80007dec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000d98]:fle.d t6, ft11, ft10
	-[0x80000d9c]:csrrs a7, fflags, zero
	-[0x80000da0]:sw t6, 1968(a5)
Current Store : [0x80000da4] : sw a7, 1972(a5) -- Store: [0x80007dfc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000db0]:fle.d t6, ft11, ft10
	-[0x80000db4]:csrrs a7, fflags, zero
	-[0x80000db8]:sw t6, 1984(a5)
Current Store : [0x80000dbc] : sw a7, 1988(a5) -- Store: [0x80007e0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000dc8]:fle.d t6, ft11, ft10
	-[0x80000dcc]:csrrs a7, fflags, zero
	-[0x80000dd0]:sw t6, 2000(a5)
Current Store : [0x80000dd4] : sw a7, 2004(a5) -- Store: [0x80007e1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000de0]:fle.d t6, ft11, ft10
	-[0x80000de4]:csrrs a7, fflags, zero
	-[0x80000de8]:sw t6, 2016(a5)
Current Store : [0x80000dec] : sw a7, 2020(a5) -- Store: [0x80007e2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e00]:fle.d t6, ft11, ft10
	-[0x80000e04]:csrrs a7, fflags, zero
	-[0x80000e08]:sw t6, 0(a5)
Current Store : [0x80000e0c] : sw a7, 4(a5) -- Store: [0x80007a44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e18]:fle.d t6, ft11, ft10
	-[0x80000e1c]:csrrs a7, fflags, zero
	-[0x80000e20]:sw t6, 16(a5)
Current Store : [0x80000e24] : sw a7, 20(a5) -- Store: [0x80007a54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e30]:fle.d t6, ft11, ft10
	-[0x80000e34]:csrrs a7, fflags, zero
	-[0x80000e38]:sw t6, 32(a5)
Current Store : [0x80000e3c] : sw a7, 36(a5) -- Store: [0x80007a64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e48]:fle.d t6, ft11, ft10
	-[0x80000e4c]:csrrs a7, fflags, zero
	-[0x80000e50]:sw t6, 48(a5)
Current Store : [0x80000e54] : sw a7, 52(a5) -- Store: [0x80007a74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e60]:fle.d t6, ft11, ft10
	-[0x80000e64]:csrrs a7, fflags, zero
	-[0x80000e68]:sw t6, 64(a5)
Current Store : [0x80000e6c] : sw a7, 68(a5) -- Store: [0x80007a84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e78]:fle.d t6, ft11, ft10
	-[0x80000e7c]:csrrs a7, fflags, zero
	-[0x80000e80]:sw t6, 80(a5)
Current Store : [0x80000e84] : sw a7, 84(a5) -- Store: [0x80007a94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000e90]:fle.d t6, ft11, ft10
	-[0x80000e94]:csrrs a7, fflags, zero
	-[0x80000e98]:sw t6, 96(a5)
Current Store : [0x80000e9c] : sw a7, 100(a5) -- Store: [0x80007aa4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ea8]:fle.d t6, ft11, ft10
	-[0x80000eac]:csrrs a7, fflags, zero
	-[0x80000eb0]:sw t6, 112(a5)
Current Store : [0x80000eb4] : sw a7, 116(a5) -- Store: [0x80007ab4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ec0]:fle.d t6, ft11, ft10
	-[0x80000ec4]:csrrs a7, fflags, zero
	-[0x80000ec8]:sw t6, 128(a5)
Current Store : [0x80000ecc] : sw a7, 132(a5) -- Store: [0x80007ac4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ed8]:fle.d t6, ft11, ft10
	-[0x80000edc]:csrrs a7, fflags, zero
	-[0x80000ee0]:sw t6, 144(a5)
Current Store : [0x80000ee4] : sw a7, 148(a5) -- Store: [0x80007ad4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fle.d t6, ft11, ft10
	-[0x80000ef4]:csrrs a7, fflags, zero
	-[0x80000ef8]:sw t6, 160(a5)
Current Store : [0x80000efc] : sw a7, 164(a5) -- Store: [0x80007ae4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f08]:fle.d t6, ft11, ft10
	-[0x80000f0c]:csrrs a7, fflags, zero
	-[0x80000f10]:sw t6, 176(a5)
Current Store : [0x80000f14] : sw a7, 180(a5) -- Store: [0x80007af4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f20]:fle.d t6, ft11, ft10
	-[0x80000f24]:csrrs a7, fflags, zero
	-[0x80000f28]:sw t6, 192(a5)
Current Store : [0x80000f2c] : sw a7, 196(a5) -- Store: [0x80007b04]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f38]:fle.d t6, ft11, ft10
	-[0x80000f3c]:csrrs a7, fflags, zero
	-[0x80000f40]:sw t6, 208(a5)
Current Store : [0x80000f44] : sw a7, 212(a5) -- Store: [0x80007b14]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f50]:fle.d t6, ft11, ft10
	-[0x80000f54]:csrrs a7, fflags, zero
	-[0x80000f58]:sw t6, 224(a5)
Current Store : [0x80000f5c] : sw a7, 228(a5) -- Store: [0x80007b24]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f68]:fle.d t6, ft11, ft10
	-[0x80000f6c]:csrrs a7, fflags, zero
	-[0x80000f70]:sw t6, 240(a5)
Current Store : [0x80000f74] : sw a7, 244(a5) -- Store: [0x80007b34]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f80]:fle.d t6, ft11, ft10
	-[0x80000f84]:csrrs a7, fflags, zero
	-[0x80000f88]:sw t6, 256(a5)
Current Store : [0x80000f8c] : sw a7, 260(a5) -- Store: [0x80007b44]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000f98]:fle.d t6, ft11, ft10
	-[0x80000f9c]:csrrs a7, fflags, zero
	-[0x80000fa0]:sw t6, 272(a5)
Current Store : [0x80000fa4] : sw a7, 276(a5) -- Store: [0x80007b54]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000fb0]:fle.d t6, ft11, ft10
	-[0x80000fb4]:csrrs a7, fflags, zero
	-[0x80000fb8]:sw t6, 288(a5)
Current Store : [0x80000fbc] : sw a7, 292(a5) -- Store: [0x80007b64]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000fc8]:fle.d t6, ft11, ft10
	-[0x80000fcc]:csrrs a7, fflags, zero
	-[0x80000fd0]:sw t6, 304(a5)
Current Store : [0x80000fd4] : sw a7, 308(a5) -- Store: [0x80007b74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000fe0]:fle.d t6, ft11, ft10
	-[0x80000fe4]:csrrs a7, fflags, zero
	-[0x80000fe8]:sw t6, 320(a5)
Current Store : [0x80000fec] : sw a7, 324(a5) -- Store: [0x80007b84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000ff8]:fle.d t6, ft11, ft10
	-[0x80000ffc]:csrrs a7, fflags, zero
	-[0x80001000]:sw t6, 336(a5)
Current Store : [0x80001004] : sw a7, 340(a5) -- Store: [0x80007b94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001010]:fle.d t6, ft11, ft10
	-[0x80001014]:csrrs a7, fflags, zero
	-[0x80001018]:sw t6, 352(a5)
Current Store : [0x8000101c] : sw a7, 356(a5) -- Store: [0x80007ba4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001028]:fle.d t6, ft11, ft10
	-[0x8000102c]:csrrs a7, fflags, zero
	-[0x80001030]:sw t6, 368(a5)
Current Store : [0x80001034] : sw a7, 372(a5) -- Store: [0x80007bb4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001040]:fle.d t6, ft11, ft10
	-[0x80001044]:csrrs a7, fflags, zero
	-[0x80001048]:sw t6, 384(a5)
Current Store : [0x8000104c] : sw a7, 388(a5) -- Store: [0x80007bc4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001058]:fle.d t6, ft11, ft10
	-[0x8000105c]:csrrs a7, fflags, zero
	-[0x80001060]:sw t6, 400(a5)
Current Store : [0x80001064] : sw a7, 404(a5) -- Store: [0x80007bd4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001070]:fle.d t6, ft11, ft10
	-[0x80001074]:csrrs a7, fflags, zero
	-[0x80001078]:sw t6, 416(a5)
Current Store : [0x8000107c] : sw a7, 420(a5) -- Store: [0x80007be4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001088]:fle.d t6, ft11, ft10
	-[0x8000108c]:csrrs a7, fflags, zero
	-[0x80001090]:sw t6, 432(a5)
Current Store : [0x80001094] : sw a7, 436(a5) -- Store: [0x80007bf4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800010a0]:fle.d t6, ft11, ft10
	-[0x800010a4]:csrrs a7, fflags, zero
	-[0x800010a8]:sw t6, 448(a5)
Current Store : [0x800010ac] : sw a7, 452(a5) -- Store: [0x80007c04]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800010b8]:fle.d t6, ft11, ft10
	-[0x800010bc]:csrrs a7, fflags, zero
	-[0x800010c0]:sw t6, 464(a5)
Current Store : [0x800010c4] : sw a7, 468(a5) -- Store: [0x80007c14]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800010d0]:fle.d t6, ft11, ft10
	-[0x800010d4]:csrrs a7, fflags, zero
	-[0x800010d8]:sw t6, 480(a5)
Current Store : [0x800010dc] : sw a7, 484(a5) -- Store: [0x80007c24]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800010e8]:fle.d t6, ft11, ft10
	-[0x800010ec]:csrrs a7, fflags, zero
	-[0x800010f0]:sw t6, 496(a5)
Current Store : [0x800010f4] : sw a7, 500(a5) -- Store: [0x80007c34]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001100]:fle.d t6, ft11, ft10
	-[0x80001104]:csrrs a7, fflags, zero
	-[0x80001108]:sw t6, 512(a5)
Current Store : [0x8000110c] : sw a7, 516(a5) -- Store: [0x80007c44]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001118]:fle.d t6, ft11, ft10
	-[0x8000111c]:csrrs a7, fflags, zero
	-[0x80001120]:sw t6, 528(a5)
Current Store : [0x80001124] : sw a7, 532(a5) -- Store: [0x80007c54]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001130]:fle.d t6, ft11, ft10
	-[0x80001134]:csrrs a7, fflags, zero
	-[0x80001138]:sw t6, 544(a5)
Current Store : [0x8000113c] : sw a7, 548(a5) -- Store: [0x80007c64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001148]:fle.d t6, ft11, ft10
	-[0x8000114c]:csrrs a7, fflags, zero
	-[0x80001150]:sw t6, 560(a5)
Current Store : [0x80001154] : sw a7, 564(a5) -- Store: [0x80007c74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001160]:fle.d t6, ft11, ft10
	-[0x80001164]:csrrs a7, fflags, zero
	-[0x80001168]:sw t6, 576(a5)
Current Store : [0x8000116c] : sw a7, 580(a5) -- Store: [0x80007c84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001178]:fle.d t6, ft11, ft10
	-[0x8000117c]:csrrs a7, fflags, zero
	-[0x80001180]:sw t6, 592(a5)
Current Store : [0x80001184] : sw a7, 596(a5) -- Store: [0x80007c94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001190]:fle.d t6, ft11, ft10
	-[0x80001194]:csrrs a7, fflags, zero
	-[0x80001198]:sw t6, 608(a5)
Current Store : [0x8000119c] : sw a7, 612(a5) -- Store: [0x80007ca4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800011a8]:fle.d t6, ft11, ft10
	-[0x800011ac]:csrrs a7, fflags, zero
	-[0x800011b0]:sw t6, 624(a5)
Current Store : [0x800011b4] : sw a7, 628(a5) -- Store: [0x80007cb4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800011c0]:fle.d t6, ft11, ft10
	-[0x800011c4]:csrrs a7, fflags, zero
	-[0x800011c8]:sw t6, 640(a5)
Current Store : [0x800011cc] : sw a7, 644(a5) -- Store: [0x80007cc4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800011d8]:fle.d t6, ft11, ft10
	-[0x800011dc]:csrrs a7, fflags, zero
	-[0x800011e0]:sw t6, 656(a5)
Current Store : [0x800011e4] : sw a7, 660(a5) -- Store: [0x80007cd4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800011f0]:fle.d t6, ft11, ft10
	-[0x800011f4]:csrrs a7, fflags, zero
	-[0x800011f8]:sw t6, 672(a5)
Current Store : [0x800011fc] : sw a7, 676(a5) -- Store: [0x80007ce4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001208]:fle.d t6, ft11, ft10
	-[0x8000120c]:csrrs a7, fflags, zero
	-[0x80001210]:sw t6, 688(a5)
Current Store : [0x80001214] : sw a7, 692(a5) -- Store: [0x80007cf4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001220]:fle.d t6, ft11, ft10
	-[0x80001224]:csrrs a7, fflags, zero
	-[0x80001228]:sw t6, 704(a5)
Current Store : [0x8000122c] : sw a7, 708(a5) -- Store: [0x80007d04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001238]:fle.d t6, ft11, ft10
	-[0x8000123c]:csrrs a7, fflags, zero
	-[0x80001240]:sw t6, 720(a5)
Current Store : [0x80001244] : sw a7, 724(a5) -- Store: [0x80007d14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001250]:fle.d t6, ft11, ft10
	-[0x80001254]:csrrs a7, fflags, zero
	-[0x80001258]:sw t6, 736(a5)
Current Store : [0x8000125c] : sw a7, 740(a5) -- Store: [0x80007d24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001268]:fle.d t6, ft11, ft10
	-[0x8000126c]:csrrs a7, fflags, zero
	-[0x80001270]:sw t6, 752(a5)
Current Store : [0x80001274] : sw a7, 756(a5) -- Store: [0x80007d34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001280]:fle.d t6, ft11, ft10
	-[0x80001284]:csrrs a7, fflags, zero
	-[0x80001288]:sw t6, 768(a5)
Current Store : [0x8000128c] : sw a7, 772(a5) -- Store: [0x80007d44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001298]:fle.d t6, ft11, ft10
	-[0x8000129c]:csrrs a7, fflags, zero
	-[0x800012a0]:sw t6, 784(a5)
Current Store : [0x800012a4] : sw a7, 788(a5) -- Store: [0x80007d54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800012b0]:fle.d t6, ft11, ft10
	-[0x800012b4]:csrrs a7, fflags, zero
	-[0x800012b8]:sw t6, 800(a5)
Current Store : [0x800012bc] : sw a7, 804(a5) -- Store: [0x80007d64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800012c8]:fle.d t6, ft11, ft10
	-[0x800012cc]:csrrs a7, fflags, zero
	-[0x800012d0]:sw t6, 816(a5)
Current Store : [0x800012d4] : sw a7, 820(a5) -- Store: [0x80007d74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800012e0]:fle.d t6, ft11, ft10
	-[0x800012e4]:csrrs a7, fflags, zero
	-[0x800012e8]:sw t6, 832(a5)
Current Store : [0x800012ec] : sw a7, 836(a5) -- Store: [0x80007d84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800012f8]:fle.d t6, ft11, ft10
	-[0x800012fc]:csrrs a7, fflags, zero
	-[0x80001300]:sw t6, 848(a5)
Current Store : [0x80001304] : sw a7, 852(a5) -- Store: [0x80007d94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001310]:fle.d t6, ft11, ft10
	-[0x80001314]:csrrs a7, fflags, zero
	-[0x80001318]:sw t6, 864(a5)
Current Store : [0x8000131c] : sw a7, 868(a5) -- Store: [0x80007da4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001328]:fle.d t6, ft11, ft10
	-[0x8000132c]:csrrs a7, fflags, zero
	-[0x80001330]:sw t6, 880(a5)
Current Store : [0x80001334] : sw a7, 884(a5) -- Store: [0x80007db4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001340]:fle.d t6, ft11, ft10
	-[0x80001344]:csrrs a7, fflags, zero
	-[0x80001348]:sw t6, 896(a5)
Current Store : [0x8000134c] : sw a7, 900(a5) -- Store: [0x80007dc4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001358]:fle.d t6, ft11, ft10
	-[0x8000135c]:csrrs a7, fflags, zero
	-[0x80001360]:sw t6, 912(a5)
Current Store : [0x80001364] : sw a7, 916(a5) -- Store: [0x80007dd4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001370]:fle.d t6, ft11, ft10
	-[0x80001374]:csrrs a7, fflags, zero
	-[0x80001378]:sw t6, 928(a5)
Current Store : [0x8000137c] : sw a7, 932(a5) -- Store: [0x80007de4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001388]:fle.d t6, ft11, ft10
	-[0x8000138c]:csrrs a7, fflags, zero
	-[0x80001390]:sw t6, 944(a5)
Current Store : [0x80001394] : sw a7, 948(a5) -- Store: [0x80007df4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800013a0]:fle.d t6, ft11, ft10
	-[0x800013a4]:csrrs a7, fflags, zero
	-[0x800013a8]:sw t6, 960(a5)
Current Store : [0x800013ac] : sw a7, 964(a5) -- Store: [0x80007e04]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800013b8]:fle.d t6, ft11, ft10
	-[0x800013bc]:csrrs a7, fflags, zero
	-[0x800013c0]:sw t6, 976(a5)
Current Store : [0x800013c4] : sw a7, 980(a5) -- Store: [0x80007e14]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800013d0]:fle.d t6, ft11, ft10
	-[0x800013d4]:csrrs a7, fflags, zero
	-[0x800013d8]:sw t6, 992(a5)
Current Store : [0x800013dc] : sw a7, 996(a5) -- Store: [0x80007e24]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800013e8]:fle.d t6, ft11, ft10
	-[0x800013ec]:csrrs a7, fflags, zero
	-[0x800013f0]:sw t6, 1008(a5)
Current Store : [0x800013f4] : sw a7, 1012(a5) -- Store: [0x80007e34]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001400]:fle.d t6, ft11, ft10
	-[0x80001404]:csrrs a7, fflags, zero
	-[0x80001408]:sw t6, 1024(a5)
Current Store : [0x8000140c] : sw a7, 1028(a5) -- Store: [0x80007e44]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001418]:fle.d t6, ft11, ft10
	-[0x8000141c]:csrrs a7, fflags, zero
	-[0x80001420]:sw t6, 1040(a5)
Current Store : [0x80001424] : sw a7, 1044(a5) -- Store: [0x80007e54]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001430]:fle.d t6, ft11, ft10
	-[0x80001434]:csrrs a7, fflags, zero
	-[0x80001438]:sw t6, 1056(a5)
Current Store : [0x8000143c] : sw a7, 1060(a5) -- Store: [0x80007e64]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001448]:fle.d t6, ft11, ft10
	-[0x8000144c]:csrrs a7, fflags, zero
	-[0x80001450]:sw t6, 1072(a5)
Current Store : [0x80001454] : sw a7, 1076(a5) -- Store: [0x80007e74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001460]:fle.d t6, ft11, ft10
	-[0x80001464]:csrrs a7, fflags, zero
	-[0x80001468]:sw t6, 1088(a5)
Current Store : [0x8000146c] : sw a7, 1092(a5) -- Store: [0x80007e84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001478]:fle.d t6, ft11, ft10
	-[0x8000147c]:csrrs a7, fflags, zero
	-[0x80001480]:sw t6, 1104(a5)
Current Store : [0x80001484] : sw a7, 1108(a5) -- Store: [0x80007e94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001490]:fle.d t6, ft11, ft10
	-[0x80001494]:csrrs a7, fflags, zero
	-[0x80001498]:sw t6, 1120(a5)
Current Store : [0x8000149c] : sw a7, 1124(a5) -- Store: [0x80007ea4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800014a8]:fle.d t6, ft11, ft10
	-[0x800014ac]:csrrs a7, fflags, zero
	-[0x800014b0]:sw t6, 1136(a5)
Current Store : [0x800014b4] : sw a7, 1140(a5) -- Store: [0x80007eb4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800014c0]:fle.d t6, ft11, ft10
	-[0x800014c4]:csrrs a7, fflags, zero
	-[0x800014c8]:sw t6, 1152(a5)
Current Store : [0x800014cc] : sw a7, 1156(a5) -- Store: [0x80007ec4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800014d8]:fle.d t6, ft11, ft10
	-[0x800014dc]:csrrs a7, fflags, zero
	-[0x800014e0]:sw t6, 1168(a5)
Current Store : [0x800014e4] : sw a7, 1172(a5) -- Store: [0x80007ed4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800014f0]:fle.d t6, ft11, ft10
	-[0x800014f4]:csrrs a7, fflags, zero
	-[0x800014f8]:sw t6, 1184(a5)
Current Store : [0x800014fc] : sw a7, 1188(a5) -- Store: [0x80007ee4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001508]:fle.d t6, ft11, ft10
	-[0x8000150c]:csrrs a7, fflags, zero
	-[0x80001510]:sw t6, 1200(a5)
Current Store : [0x80001514] : sw a7, 1204(a5) -- Store: [0x80007ef4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001520]:fle.d t6, ft11, ft10
	-[0x80001524]:csrrs a7, fflags, zero
	-[0x80001528]:sw t6, 1216(a5)
Current Store : [0x8000152c] : sw a7, 1220(a5) -- Store: [0x80007f04]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001538]:fle.d t6, ft11, ft10
	-[0x8000153c]:csrrs a7, fflags, zero
	-[0x80001540]:sw t6, 1232(a5)
Current Store : [0x80001544] : sw a7, 1236(a5) -- Store: [0x80007f14]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001550]:fle.d t6, ft11, ft10
	-[0x80001554]:csrrs a7, fflags, zero
	-[0x80001558]:sw t6, 1248(a5)
Current Store : [0x8000155c] : sw a7, 1252(a5) -- Store: [0x80007f24]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001568]:fle.d t6, ft11, ft10
	-[0x8000156c]:csrrs a7, fflags, zero
	-[0x80001570]:sw t6, 1264(a5)
Current Store : [0x80001574] : sw a7, 1268(a5) -- Store: [0x80007f34]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001580]:fle.d t6, ft11, ft10
	-[0x80001584]:csrrs a7, fflags, zero
	-[0x80001588]:sw t6, 1280(a5)
Current Store : [0x8000158c] : sw a7, 1284(a5) -- Store: [0x80007f44]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001598]:fle.d t6, ft11, ft10
	-[0x8000159c]:csrrs a7, fflags, zero
	-[0x800015a0]:sw t6, 1296(a5)
Current Store : [0x800015a4] : sw a7, 1300(a5) -- Store: [0x80007f54]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800015b0]:fle.d t6, ft11, ft10
	-[0x800015b4]:csrrs a7, fflags, zero
	-[0x800015b8]:sw t6, 1312(a5)
Current Store : [0x800015bc] : sw a7, 1316(a5) -- Store: [0x80007f64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800015c8]:fle.d t6, ft11, ft10
	-[0x800015cc]:csrrs a7, fflags, zero
	-[0x800015d0]:sw t6, 1328(a5)
Current Store : [0x800015d4] : sw a7, 1332(a5) -- Store: [0x80007f74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800015e0]:fle.d t6, ft11, ft10
	-[0x800015e4]:csrrs a7, fflags, zero
	-[0x800015e8]:sw t6, 1344(a5)
Current Store : [0x800015ec] : sw a7, 1348(a5) -- Store: [0x80007f84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800015f8]:fle.d t6, ft11, ft10
	-[0x800015fc]:csrrs a7, fflags, zero
	-[0x80001600]:sw t6, 1360(a5)
Current Store : [0x80001604] : sw a7, 1364(a5) -- Store: [0x80007f94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001610]:fle.d t6, ft11, ft10
	-[0x80001614]:csrrs a7, fflags, zero
	-[0x80001618]:sw t6, 1376(a5)
Current Store : [0x8000161c] : sw a7, 1380(a5) -- Store: [0x80007fa4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001628]:fle.d t6, ft11, ft10
	-[0x8000162c]:csrrs a7, fflags, zero
	-[0x80001630]:sw t6, 1392(a5)
Current Store : [0x80001634] : sw a7, 1396(a5) -- Store: [0x80007fb4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001640]:fle.d t6, ft11, ft10
	-[0x80001644]:csrrs a7, fflags, zero
	-[0x80001648]:sw t6, 1408(a5)
Current Store : [0x8000164c] : sw a7, 1412(a5) -- Store: [0x80007fc4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001658]:fle.d t6, ft11, ft10
	-[0x8000165c]:csrrs a7, fflags, zero
	-[0x80001660]:sw t6, 1424(a5)
Current Store : [0x80001664] : sw a7, 1428(a5) -- Store: [0x80007fd4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001670]:fle.d t6, ft11, ft10
	-[0x80001674]:csrrs a7, fflags, zero
	-[0x80001678]:sw t6, 1440(a5)
Current Store : [0x8000167c] : sw a7, 1444(a5) -- Store: [0x80007fe4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001688]:fle.d t6, ft11, ft10
	-[0x8000168c]:csrrs a7, fflags, zero
	-[0x80001690]:sw t6, 1456(a5)
Current Store : [0x80001694] : sw a7, 1460(a5) -- Store: [0x80007ff4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800016a0]:fle.d t6, ft11, ft10
	-[0x800016a4]:csrrs a7, fflags, zero
	-[0x800016a8]:sw t6, 1472(a5)
Current Store : [0x800016ac] : sw a7, 1476(a5) -- Store: [0x80008004]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800016b8]:fle.d t6, ft11, ft10
	-[0x800016bc]:csrrs a7, fflags, zero
	-[0x800016c0]:sw t6, 1488(a5)
Current Store : [0x800016c4] : sw a7, 1492(a5) -- Store: [0x80008014]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800016d0]:fle.d t6, ft11, ft10
	-[0x800016d4]:csrrs a7, fflags, zero
	-[0x800016d8]:sw t6, 1504(a5)
Current Store : [0x800016dc] : sw a7, 1508(a5) -- Store: [0x80008024]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800016e8]:fle.d t6, ft11, ft10
	-[0x800016ec]:csrrs a7, fflags, zero
	-[0x800016f0]:sw t6, 1520(a5)
Current Store : [0x800016f4] : sw a7, 1524(a5) -- Store: [0x80008034]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001700]:fle.d t6, ft11, ft10
	-[0x80001704]:csrrs a7, fflags, zero
	-[0x80001708]:sw t6, 1536(a5)
Current Store : [0x8000170c] : sw a7, 1540(a5) -- Store: [0x80008044]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001718]:fle.d t6, ft11, ft10
	-[0x8000171c]:csrrs a7, fflags, zero
	-[0x80001720]:sw t6, 1552(a5)
Current Store : [0x80001724] : sw a7, 1556(a5) -- Store: [0x80008054]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001730]:fle.d t6, ft11, ft10
	-[0x80001734]:csrrs a7, fflags, zero
	-[0x80001738]:sw t6, 1568(a5)
Current Store : [0x8000173c] : sw a7, 1572(a5) -- Store: [0x80008064]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001748]:fle.d t6, ft11, ft10
	-[0x8000174c]:csrrs a7, fflags, zero
	-[0x80001750]:sw t6, 1584(a5)
Current Store : [0x80001754] : sw a7, 1588(a5) -- Store: [0x80008074]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001760]:fle.d t6, ft11, ft10
	-[0x80001764]:csrrs a7, fflags, zero
	-[0x80001768]:sw t6, 1600(a5)
Current Store : [0x8000176c] : sw a7, 1604(a5) -- Store: [0x80008084]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001778]:fle.d t6, ft11, ft10
	-[0x8000177c]:csrrs a7, fflags, zero
	-[0x80001780]:sw t6, 1616(a5)
Current Store : [0x80001784] : sw a7, 1620(a5) -- Store: [0x80008094]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001790]:fle.d t6, ft11, ft10
	-[0x80001794]:csrrs a7, fflags, zero
	-[0x80001798]:sw t6, 1632(a5)
Current Store : [0x8000179c] : sw a7, 1636(a5) -- Store: [0x800080a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800017a8]:fle.d t6, ft11, ft10
	-[0x800017ac]:csrrs a7, fflags, zero
	-[0x800017b0]:sw t6, 1648(a5)
Current Store : [0x800017b4] : sw a7, 1652(a5) -- Store: [0x800080b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800017c0]:fle.d t6, ft11, ft10
	-[0x800017c4]:csrrs a7, fflags, zero
	-[0x800017c8]:sw t6, 1664(a5)
Current Store : [0x800017cc] : sw a7, 1668(a5) -- Store: [0x800080c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800017d8]:fle.d t6, ft11, ft10
	-[0x800017dc]:csrrs a7, fflags, zero
	-[0x800017e0]:sw t6, 1680(a5)
Current Store : [0x800017e4] : sw a7, 1684(a5) -- Store: [0x800080d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800017f0]:fle.d t6, ft11, ft10
	-[0x800017f4]:csrrs a7, fflags, zero
	-[0x800017f8]:sw t6, 1696(a5)
Current Store : [0x800017fc] : sw a7, 1700(a5) -- Store: [0x800080e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001808]:fle.d t6, ft11, ft10
	-[0x8000180c]:csrrs a7, fflags, zero
	-[0x80001810]:sw t6, 1712(a5)
Current Store : [0x80001814] : sw a7, 1716(a5) -- Store: [0x800080f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001820]:fle.d t6, ft11, ft10
	-[0x80001824]:csrrs a7, fflags, zero
	-[0x80001828]:sw t6, 1728(a5)
Current Store : [0x8000182c] : sw a7, 1732(a5) -- Store: [0x80008104]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001838]:fle.d t6, ft11, ft10
	-[0x8000183c]:csrrs a7, fflags, zero
	-[0x80001840]:sw t6, 1744(a5)
Current Store : [0x80001844] : sw a7, 1748(a5) -- Store: [0x80008114]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001850]:fle.d t6, ft11, ft10
	-[0x80001854]:csrrs a7, fflags, zero
	-[0x80001858]:sw t6, 1760(a5)
Current Store : [0x8000185c] : sw a7, 1764(a5) -- Store: [0x80008124]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001868]:fle.d t6, ft11, ft10
	-[0x8000186c]:csrrs a7, fflags, zero
	-[0x80001870]:sw t6, 1776(a5)
Current Store : [0x80001874] : sw a7, 1780(a5) -- Store: [0x80008134]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001880]:fle.d t6, ft11, ft10
	-[0x80001884]:csrrs a7, fflags, zero
	-[0x80001888]:sw t6, 1792(a5)
Current Store : [0x8000188c] : sw a7, 1796(a5) -- Store: [0x80008144]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001898]:fle.d t6, ft11, ft10
	-[0x8000189c]:csrrs a7, fflags, zero
	-[0x800018a0]:sw t6, 1808(a5)
Current Store : [0x800018a4] : sw a7, 1812(a5) -- Store: [0x80008154]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800018b0]:fle.d t6, ft11, ft10
	-[0x800018b4]:csrrs a7, fflags, zero
	-[0x800018b8]:sw t6, 1824(a5)
Current Store : [0x800018bc] : sw a7, 1828(a5) -- Store: [0x80008164]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800018c8]:fle.d t6, ft11, ft10
	-[0x800018cc]:csrrs a7, fflags, zero
	-[0x800018d0]:sw t6, 1840(a5)
Current Store : [0x800018d4] : sw a7, 1844(a5) -- Store: [0x80008174]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800018e0]:fle.d t6, ft11, ft10
	-[0x800018e4]:csrrs a7, fflags, zero
	-[0x800018e8]:sw t6, 1856(a5)
Current Store : [0x800018ec] : sw a7, 1860(a5) -- Store: [0x80008184]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800018f8]:fle.d t6, ft11, ft10
	-[0x800018fc]:csrrs a7, fflags, zero
	-[0x80001900]:sw t6, 1872(a5)
Current Store : [0x80001904] : sw a7, 1876(a5) -- Store: [0x80008194]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001910]:fle.d t6, ft11, ft10
	-[0x80001914]:csrrs a7, fflags, zero
	-[0x80001918]:sw t6, 1888(a5)
Current Store : [0x8000191c] : sw a7, 1892(a5) -- Store: [0x800081a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001928]:fle.d t6, ft11, ft10
	-[0x8000192c]:csrrs a7, fflags, zero
	-[0x80001930]:sw t6, 1904(a5)
Current Store : [0x80001934] : sw a7, 1908(a5) -- Store: [0x800081b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001944]:fle.d t6, ft11, ft10
	-[0x80001948]:csrrs a7, fflags, zero
	-[0x8000194c]:sw t6, 1920(a5)
Current Store : [0x80001950] : sw a7, 1924(a5) -- Store: [0x800081c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000195c]:fle.d t6, ft11, ft10
	-[0x80001960]:csrrs a7, fflags, zero
	-[0x80001964]:sw t6, 1936(a5)
Current Store : [0x80001968] : sw a7, 1940(a5) -- Store: [0x800081d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001974]:fle.d t6, ft11, ft10
	-[0x80001978]:csrrs a7, fflags, zero
	-[0x8000197c]:sw t6, 1952(a5)
Current Store : [0x80001980] : sw a7, 1956(a5) -- Store: [0x800081e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000198c]:fle.d t6, ft11, ft10
	-[0x80001990]:csrrs a7, fflags, zero
	-[0x80001994]:sw t6, 1968(a5)
Current Store : [0x80001998] : sw a7, 1972(a5) -- Store: [0x800081f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800019a4]:fle.d t6, ft11, ft10
	-[0x800019a8]:csrrs a7, fflags, zero
	-[0x800019ac]:sw t6, 1984(a5)
Current Store : [0x800019b0] : sw a7, 1988(a5) -- Store: [0x80008204]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800019bc]:fle.d t6, ft11, ft10
	-[0x800019c0]:csrrs a7, fflags, zero
	-[0x800019c4]:sw t6, 2000(a5)
Current Store : [0x800019c8] : sw a7, 2004(a5) -- Store: [0x80008214]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800019d4]:fle.d t6, ft11, ft10
	-[0x800019d8]:csrrs a7, fflags, zero
	-[0x800019dc]:sw t6, 2016(a5)
Current Store : [0x800019e0] : sw a7, 2020(a5) -- Store: [0x80008224]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800019f4]:fle.d t6, ft11, ft10
	-[0x800019f8]:csrrs a7, fflags, zero
	-[0x800019fc]:sw t6, 0(a5)
Current Store : [0x80001a00] : sw a7, 4(a5) -- Store: [0x80007e3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a0c]:fle.d t6, ft11, ft10
	-[0x80001a10]:csrrs a7, fflags, zero
	-[0x80001a14]:sw t6, 16(a5)
Current Store : [0x80001a18] : sw a7, 20(a5) -- Store: [0x80007e4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a24]:fle.d t6, ft11, ft10
	-[0x80001a28]:csrrs a7, fflags, zero
	-[0x80001a2c]:sw t6, 32(a5)
Current Store : [0x80001a30] : sw a7, 36(a5) -- Store: [0x80007e5c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a3c]:fle.d t6, ft11, ft10
	-[0x80001a40]:csrrs a7, fflags, zero
	-[0x80001a44]:sw t6, 48(a5)
Current Store : [0x80001a48] : sw a7, 52(a5) -- Store: [0x80007e6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a54]:fle.d t6, ft11, ft10
	-[0x80001a58]:csrrs a7, fflags, zero
	-[0x80001a5c]:sw t6, 64(a5)
Current Store : [0x80001a60] : sw a7, 68(a5) -- Store: [0x80007e7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a6c]:fle.d t6, ft11, ft10
	-[0x80001a70]:csrrs a7, fflags, zero
	-[0x80001a74]:sw t6, 80(a5)
Current Store : [0x80001a78] : sw a7, 84(a5) -- Store: [0x80007e8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a84]:fle.d t6, ft11, ft10
	-[0x80001a88]:csrrs a7, fflags, zero
	-[0x80001a8c]:sw t6, 96(a5)
Current Store : [0x80001a90] : sw a7, 100(a5) -- Store: [0x80007e9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001a9c]:fle.d t6, ft11, ft10
	-[0x80001aa0]:csrrs a7, fflags, zero
	-[0x80001aa4]:sw t6, 112(a5)
Current Store : [0x80001aa8] : sw a7, 116(a5) -- Store: [0x80007eac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ab4]:fle.d t6, ft11, ft10
	-[0x80001ab8]:csrrs a7, fflags, zero
	-[0x80001abc]:sw t6, 128(a5)
Current Store : [0x80001ac0] : sw a7, 132(a5) -- Store: [0x80007ebc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001acc]:fle.d t6, ft11, ft10
	-[0x80001ad0]:csrrs a7, fflags, zero
	-[0x80001ad4]:sw t6, 144(a5)
Current Store : [0x80001ad8] : sw a7, 148(a5) -- Store: [0x80007ecc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ae4]:fle.d t6, ft11, ft10
	-[0x80001ae8]:csrrs a7, fflags, zero
	-[0x80001aec]:sw t6, 160(a5)
Current Store : [0x80001af0] : sw a7, 164(a5) -- Store: [0x80007edc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001afc]:fle.d t6, ft11, ft10
	-[0x80001b00]:csrrs a7, fflags, zero
	-[0x80001b04]:sw t6, 176(a5)
Current Store : [0x80001b08] : sw a7, 180(a5) -- Store: [0x80007eec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b14]:fle.d t6, ft11, ft10
	-[0x80001b18]:csrrs a7, fflags, zero
	-[0x80001b1c]:sw t6, 192(a5)
Current Store : [0x80001b20] : sw a7, 196(a5) -- Store: [0x80007efc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b2c]:fle.d t6, ft11, ft10
	-[0x80001b30]:csrrs a7, fflags, zero
	-[0x80001b34]:sw t6, 208(a5)
Current Store : [0x80001b38] : sw a7, 212(a5) -- Store: [0x80007f0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b44]:fle.d t6, ft11, ft10
	-[0x80001b48]:csrrs a7, fflags, zero
	-[0x80001b4c]:sw t6, 224(a5)
Current Store : [0x80001b50] : sw a7, 228(a5) -- Store: [0x80007f1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b5c]:fle.d t6, ft11, ft10
	-[0x80001b60]:csrrs a7, fflags, zero
	-[0x80001b64]:sw t6, 240(a5)
Current Store : [0x80001b68] : sw a7, 244(a5) -- Store: [0x80007f2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b74]:fle.d t6, ft11, ft10
	-[0x80001b78]:csrrs a7, fflags, zero
	-[0x80001b7c]:sw t6, 256(a5)
Current Store : [0x80001b80] : sw a7, 260(a5) -- Store: [0x80007f3c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001b8c]:fle.d t6, ft11, ft10
	-[0x80001b90]:csrrs a7, fflags, zero
	-[0x80001b94]:sw t6, 272(a5)
Current Store : [0x80001b98] : sw a7, 276(a5) -- Store: [0x80007f4c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ba4]:fle.d t6, ft11, ft10
	-[0x80001ba8]:csrrs a7, fflags, zero
	-[0x80001bac]:sw t6, 288(a5)
Current Store : [0x80001bb0] : sw a7, 292(a5) -- Store: [0x80007f5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001bbc]:fle.d t6, ft11, ft10
	-[0x80001bc0]:csrrs a7, fflags, zero
	-[0x80001bc4]:sw t6, 304(a5)
Current Store : [0x80001bc8] : sw a7, 308(a5) -- Store: [0x80007f6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001bd4]:fle.d t6, ft11, ft10
	-[0x80001bd8]:csrrs a7, fflags, zero
	-[0x80001bdc]:sw t6, 320(a5)
Current Store : [0x80001be0] : sw a7, 324(a5) -- Store: [0x80007f7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001bec]:fle.d t6, ft11, ft10
	-[0x80001bf0]:csrrs a7, fflags, zero
	-[0x80001bf4]:sw t6, 336(a5)
Current Store : [0x80001bf8] : sw a7, 340(a5) -- Store: [0x80007f8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c04]:fle.d t6, ft11, ft10
	-[0x80001c08]:csrrs a7, fflags, zero
	-[0x80001c0c]:sw t6, 352(a5)
Current Store : [0x80001c10] : sw a7, 356(a5) -- Store: [0x80007f9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fle.d t6, ft11, ft10
	-[0x80001c20]:csrrs a7, fflags, zero
	-[0x80001c24]:sw t6, 368(a5)
Current Store : [0x80001c28] : sw a7, 372(a5) -- Store: [0x80007fac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c34]:fle.d t6, ft11, ft10
	-[0x80001c38]:csrrs a7, fflags, zero
	-[0x80001c3c]:sw t6, 384(a5)
Current Store : [0x80001c40] : sw a7, 388(a5) -- Store: [0x80007fbc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c4c]:fle.d t6, ft11, ft10
	-[0x80001c50]:csrrs a7, fflags, zero
	-[0x80001c54]:sw t6, 400(a5)
Current Store : [0x80001c58] : sw a7, 404(a5) -- Store: [0x80007fcc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c64]:fle.d t6, ft11, ft10
	-[0x80001c68]:csrrs a7, fflags, zero
	-[0x80001c6c]:sw t6, 416(a5)
Current Store : [0x80001c70] : sw a7, 420(a5) -- Store: [0x80007fdc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c7c]:fle.d t6, ft11, ft10
	-[0x80001c80]:csrrs a7, fflags, zero
	-[0x80001c84]:sw t6, 432(a5)
Current Store : [0x80001c88] : sw a7, 436(a5) -- Store: [0x80007fec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001c94]:fle.d t6, ft11, ft10
	-[0x80001c98]:csrrs a7, fflags, zero
	-[0x80001c9c]:sw t6, 448(a5)
Current Store : [0x80001ca0] : sw a7, 452(a5) -- Store: [0x80007ffc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001cac]:fle.d t6, ft11, ft10
	-[0x80001cb0]:csrrs a7, fflags, zero
	-[0x80001cb4]:sw t6, 464(a5)
Current Store : [0x80001cb8] : sw a7, 468(a5) -- Store: [0x8000800c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001cc4]:fle.d t6, ft11, ft10
	-[0x80001cc8]:csrrs a7, fflags, zero
	-[0x80001ccc]:sw t6, 480(a5)
Current Store : [0x80001cd0] : sw a7, 484(a5) -- Store: [0x8000801c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fle.d t6, ft11, ft10
	-[0x80001ce0]:csrrs a7, fflags, zero
	-[0x80001ce4]:sw t6, 496(a5)
Current Store : [0x80001ce8] : sw a7, 500(a5) -- Store: [0x8000802c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001cf4]:fle.d t6, ft11, ft10
	-[0x80001cf8]:csrrs a7, fflags, zero
	-[0x80001cfc]:sw t6, 512(a5)
Current Store : [0x80001d00] : sw a7, 516(a5) -- Store: [0x8000803c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d0c]:fle.d t6, ft11, ft10
	-[0x80001d10]:csrrs a7, fflags, zero
	-[0x80001d14]:sw t6, 528(a5)
Current Store : [0x80001d18] : sw a7, 532(a5) -- Store: [0x8000804c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d24]:fle.d t6, ft11, ft10
	-[0x80001d28]:csrrs a7, fflags, zero
	-[0x80001d2c]:sw t6, 544(a5)
Current Store : [0x80001d30] : sw a7, 548(a5) -- Store: [0x8000805c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d3c]:fle.d t6, ft11, ft10
	-[0x80001d40]:csrrs a7, fflags, zero
	-[0x80001d44]:sw t6, 560(a5)
Current Store : [0x80001d48] : sw a7, 564(a5) -- Store: [0x8000806c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d54]:fle.d t6, ft11, ft10
	-[0x80001d58]:csrrs a7, fflags, zero
	-[0x80001d5c]:sw t6, 576(a5)
Current Store : [0x80001d60] : sw a7, 580(a5) -- Store: [0x8000807c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d6c]:fle.d t6, ft11, ft10
	-[0x80001d70]:csrrs a7, fflags, zero
	-[0x80001d74]:sw t6, 592(a5)
Current Store : [0x80001d78] : sw a7, 596(a5) -- Store: [0x8000808c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d84]:fle.d t6, ft11, ft10
	-[0x80001d88]:csrrs a7, fflags, zero
	-[0x80001d8c]:sw t6, 608(a5)
Current Store : [0x80001d90] : sw a7, 612(a5) -- Store: [0x8000809c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001d9c]:fle.d t6, ft11, ft10
	-[0x80001da0]:csrrs a7, fflags, zero
	-[0x80001da4]:sw t6, 624(a5)
Current Store : [0x80001da8] : sw a7, 628(a5) -- Store: [0x800080ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001db4]:fle.d t6, ft11, ft10
	-[0x80001db8]:csrrs a7, fflags, zero
	-[0x80001dbc]:sw t6, 640(a5)
Current Store : [0x80001dc0] : sw a7, 644(a5) -- Store: [0x800080bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001dcc]:fle.d t6, ft11, ft10
	-[0x80001dd0]:csrrs a7, fflags, zero
	-[0x80001dd4]:sw t6, 656(a5)
Current Store : [0x80001dd8] : sw a7, 660(a5) -- Store: [0x800080cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001de4]:fle.d t6, ft11, ft10
	-[0x80001de8]:csrrs a7, fflags, zero
	-[0x80001dec]:sw t6, 672(a5)
Current Store : [0x80001df0] : sw a7, 676(a5) -- Store: [0x800080dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001dfc]:fle.d t6, ft11, ft10
	-[0x80001e00]:csrrs a7, fflags, zero
	-[0x80001e04]:sw t6, 688(a5)
Current Store : [0x80001e08] : sw a7, 692(a5) -- Store: [0x800080ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e14]:fle.d t6, ft11, ft10
	-[0x80001e18]:csrrs a7, fflags, zero
	-[0x80001e1c]:sw t6, 704(a5)
Current Store : [0x80001e20] : sw a7, 708(a5) -- Store: [0x800080fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e2c]:fle.d t6, ft11, ft10
	-[0x80001e30]:csrrs a7, fflags, zero
	-[0x80001e34]:sw t6, 720(a5)
Current Store : [0x80001e38] : sw a7, 724(a5) -- Store: [0x8000810c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e44]:fle.d t6, ft11, ft10
	-[0x80001e48]:csrrs a7, fflags, zero
	-[0x80001e4c]:sw t6, 736(a5)
Current Store : [0x80001e50] : sw a7, 740(a5) -- Store: [0x8000811c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e5c]:fle.d t6, ft11, ft10
	-[0x80001e60]:csrrs a7, fflags, zero
	-[0x80001e64]:sw t6, 752(a5)
Current Store : [0x80001e68] : sw a7, 756(a5) -- Store: [0x8000812c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e74]:fle.d t6, ft11, ft10
	-[0x80001e78]:csrrs a7, fflags, zero
	-[0x80001e7c]:sw t6, 768(a5)
Current Store : [0x80001e80] : sw a7, 772(a5) -- Store: [0x8000813c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001e8c]:fle.d t6, ft11, ft10
	-[0x80001e90]:csrrs a7, fflags, zero
	-[0x80001e94]:sw t6, 784(a5)
Current Store : [0x80001e98] : sw a7, 788(a5) -- Store: [0x8000814c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ea4]:fle.d t6, ft11, ft10
	-[0x80001ea8]:csrrs a7, fflags, zero
	-[0x80001eac]:sw t6, 800(a5)
Current Store : [0x80001eb0] : sw a7, 804(a5) -- Store: [0x8000815c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ebc]:fle.d t6, ft11, ft10
	-[0x80001ec0]:csrrs a7, fflags, zero
	-[0x80001ec4]:sw t6, 816(a5)
Current Store : [0x80001ec8] : sw a7, 820(a5) -- Store: [0x8000816c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ed4]:fle.d t6, ft11, ft10
	-[0x80001ed8]:csrrs a7, fflags, zero
	-[0x80001edc]:sw t6, 832(a5)
Current Store : [0x80001ee0] : sw a7, 836(a5) -- Store: [0x8000817c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001eec]:fle.d t6, ft11, ft10
	-[0x80001ef0]:csrrs a7, fflags, zero
	-[0x80001ef4]:sw t6, 848(a5)
Current Store : [0x80001ef8] : sw a7, 852(a5) -- Store: [0x8000818c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f04]:fle.d t6, ft11, ft10
	-[0x80001f08]:csrrs a7, fflags, zero
	-[0x80001f0c]:sw t6, 864(a5)
Current Store : [0x80001f10] : sw a7, 868(a5) -- Store: [0x8000819c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f1c]:fle.d t6, ft11, ft10
	-[0x80001f20]:csrrs a7, fflags, zero
	-[0x80001f24]:sw t6, 880(a5)
Current Store : [0x80001f28] : sw a7, 884(a5) -- Store: [0x800081ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f34]:fle.d t6, ft11, ft10
	-[0x80001f38]:csrrs a7, fflags, zero
	-[0x80001f3c]:sw t6, 896(a5)
Current Store : [0x80001f40] : sw a7, 900(a5) -- Store: [0x800081bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f4c]:fle.d t6, ft11, ft10
	-[0x80001f50]:csrrs a7, fflags, zero
	-[0x80001f54]:sw t6, 912(a5)
Current Store : [0x80001f58] : sw a7, 916(a5) -- Store: [0x800081cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f64]:fle.d t6, ft11, ft10
	-[0x80001f68]:csrrs a7, fflags, zero
	-[0x80001f6c]:sw t6, 928(a5)
Current Store : [0x80001f70] : sw a7, 932(a5) -- Store: [0x800081dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f7c]:fle.d t6, ft11, ft10
	-[0x80001f80]:csrrs a7, fflags, zero
	-[0x80001f84]:sw t6, 944(a5)
Current Store : [0x80001f88] : sw a7, 948(a5) -- Store: [0x800081ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001f94]:fle.d t6, ft11, ft10
	-[0x80001f98]:csrrs a7, fflags, zero
	-[0x80001f9c]:sw t6, 960(a5)
Current Store : [0x80001fa0] : sw a7, 964(a5) -- Store: [0x800081fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001fac]:fle.d t6, ft11, ft10
	-[0x80001fb0]:csrrs a7, fflags, zero
	-[0x80001fb4]:sw t6, 976(a5)
Current Store : [0x80001fb8] : sw a7, 980(a5) -- Store: [0x8000820c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001fc4]:fle.d t6, ft11, ft10
	-[0x80001fc8]:csrrs a7, fflags, zero
	-[0x80001fcc]:sw t6, 992(a5)
Current Store : [0x80001fd0] : sw a7, 996(a5) -- Store: [0x8000821c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001fdc]:fle.d t6, ft11, ft10
	-[0x80001fe0]:csrrs a7, fflags, zero
	-[0x80001fe4]:sw t6, 1008(a5)
Current Store : [0x80001fe8] : sw a7, 1012(a5) -- Store: [0x8000822c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80001ff4]:fle.d t6, ft11, ft10
	-[0x80001ff8]:csrrs a7, fflags, zero
	-[0x80001ffc]:sw t6, 1024(a5)
Current Store : [0x80002000] : sw a7, 1028(a5) -- Store: [0x8000823c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000200c]:fle.d t6, ft11, ft10
	-[0x80002010]:csrrs a7, fflags, zero
	-[0x80002014]:sw t6, 1040(a5)
Current Store : [0x80002018] : sw a7, 1044(a5) -- Store: [0x8000824c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002024]:fle.d t6, ft11, ft10
	-[0x80002028]:csrrs a7, fflags, zero
	-[0x8000202c]:sw t6, 1056(a5)
Current Store : [0x80002030] : sw a7, 1060(a5) -- Store: [0x8000825c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000203c]:fle.d t6, ft11, ft10
	-[0x80002040]:csrrs a7, fflags, zero
	-[0x80002044]:sw t6, 1072(a5)
Current Store : [0x80002048] : sw a7, 1076(a5) -- Store: [0x8000826c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002054]:fle.d t6, ft11, ft10
	-[0x80002058]:csrrs a7, fflags, zero
	-[0x8000205c]:sw t6, 1088(a5)
Current Store : [0x80002060] : sw a7, 1092(a5) -- Store: [0x8000827c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000206c]:fle.d t6, ft11, ft10
	-[0x80002070]:csrrs a7, fflags, zero
	-[0x80002074]:sw t6, 1104(a5)
Current Store : [0x80002078] : sw a7, 1108(a5) -- Store: [0x8000828c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002084]:fle.d t6, ft11, ft10
	-[0x80002088]:csrrs a7, fflags, zero
	-[0x8000208c]:sw t6, 1120(a5)
Current Store : [0x80002090] : sw a7, 1124(a5) -- Store: [0x8000829c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000209c]:fle.d t6, ft11, ft10
	-[0x800020a0]:csrrs a7, fflags, zero
	-[0x800020a4]:sw t6, 1136(a5)
Current Store : [0x800020a8] : sw a7, 1140(a5) -- Store: [0x800082ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800020b4]:fle.d t6, ft11, ft10
	-[0x800020b8]:csrrs a7, fflags, zero
	-[0x800020bc]:sw t6, 1152(a5)
Current Store : [0x800020c0] : sw a7, 1156(a5) -- Store: [0x800082bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800020cc]:fle.d t6, ft11, ft10
	-[0x800020d0]:csrrs a7, fflags, zero
	-[0x800020d4]:sw t6, 1168(a5)
Current Store : [0x800020d8] : sw a7, 1172(a5) -- Store: [0x800082cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800020e4]:fle.d t6, ft11, ft10
	-[0x800020e8]:csrrs a7, fflags, zero
	-[0x800020ec]:sw t6, 1184(a5)
Current Store : [0x800020f0] : sw a7, 1188(a5) -- Store: [0x800082dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800020fc]:fle.d t6, ft11, ft10
	-[0x80002100]:csrrs a7, fflags, zero
	-[0x80002104]:sw t6, 1200(a5)
Current Store : [0x80002108] : sw a7, 1204(a5) -- Store: [0x800082ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002114]:fle.d t6, ft11, ft10
	-[0x80002118]:csrrs a7, fflags, zero
	-[0x8000211c]:sw t6, 1216(a5)
Current Store : [0x80002120] : sw a7, 1220(a5) -- Store: [0x800082fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000212c]:fle.d t6, ft11, ft10
	-[0x80002130]:csrrs a7, fflags, zero
	-[0x80002134]:sw t6, 1232(a5)
Current Store : [0x80002138] : sw a7, 1236(a5) -- Store: [0x8000830c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002144]:fle.d t6, ft11, ft10
	-[0x80002148]:csrrs a7, fflags, zero
	-[0x8000214c]:sw t6, 1248(a5)
Current Store : [0x80002150] : sw a7, 1252(a5) -- Store: [0x8000831c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000215c]:fle.d t6, ft11, ft10
	-[0x80002160]:csrrs a7, fflags, zero
	-[0x80002164]:sw t6, 1264(a5)
Current Store : [0x80002168] : sw a7, 1268(a5) -- Store: [0x8000832c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002174]:fle.d t6, ft11, ft10
	-[0x80002178]:csrrs a7, fflags, zero
	-[0x8000217c]:sw t6, 1280(a5)
Current Store : [0x80002180] : sw a7, 1284(a5) -- Store: [0x8000833c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000218c]:fle.d t6, ft11, ft10
	-[0x80002190]:csrrs a7, fflags, zero
	-[0x80002194]:sw t6, 1296(a5)
Current Store : [0x80002198] : sw a7, 1300(a5) -- Store: [0x8000834c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800021a4]:fle.d t6, ft11, ft10
	-[0x800021a8]:csrrs a7, fflags, zero
	-[0x800021ac]:sw t6, 1312(a5)
Current Store : [0x800021b0] : sw a7, 1316(a5) -- Store: [0x8000835c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800021bc]:fle.d t6, ft11, ft10
	-[0x800021c0]:csrrs a7, fflags, zero
	-[0x800021c4]:sw t6, 1328(a5)
Current Store : [0x800021c8] : sw a7, 1332(a5) -- Store: [0x8000836c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800021d4]:fle.d t6, ft11, ft10
	-[0x800021d8]:csrrs a7, fflags, zero
	-[0x800021dc]:sw t6, 1344(a5)
Current Store : [0x800021e0] : sw a7, 1348(a5) -- Store: [0x8000837c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800021ec]:fle.d t6, ft11, ft10
	-[0x800021f0]:csrrs a7, fflags, zero
	-[0x800021f4]:sw t6, 1360(a5)
Current Store : [0x800021f8] : sw a7, 1364(a5) -- Store: [0x8000838c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002204]:fle.d t6, ft11, ft10
	-[0x80002208]:csrrs a7, fflags, zero
	-[0x8000220c]:sw t6, 1376(a5)
Current Store : [0x80002210] : sw a7, 1380(a5) -- Store: [0x8000839c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000221c]:fle.d t6, ft11, ft10
	-[0x80002220]:csrrs a7, fflags, zero
	-[0x80002224]:sw t6, 1392(a5)
Current Store : [0x80002228] : sw a7, 1396(a5) -- Store: [0x800083ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002234]:fle.d t6, ft11, ft10
	-[0x80002238]:csrrs a7, fflags, zero
	-[0x8000223c]:sw t6, 1408(a5)
Current Store : [0x80002240] : sw a7, 1412(a5) -- Store: [0x800083bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000224c]:fle.d t6, ft11, ft10
	-[0x80002250]:csrrs a7, fflags, zero
	-[0x80002254]:sw t6, 1424(a5)
Current Store : [0x80002258] : sw a7, 1428(a5) -- Store: [0x800083cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002264]:fle.d t6, ft11, ft10
	-[0x80002268]:csrrs a7, fflags, zero
	-[0x8000226c]:sw t6, 1440(a5)
Current Store : [0x80002270] : sw a7, 1444(a5) -- Store: [0x800083dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000227c]:fle.d t6, ft11, ft10
	-[0x80002280]:csrrs a7, fflags, zero
	-[0x80002284]:sw t6, 1456(a5)
Current Store : [0x80002288] : sw a7, 1460(a5) -- Store: [0x800083ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002294]:fle.d t6, ft11, ft10
	-[0x80002298]:csrrs a7, fflags, zero
	-[0x8000229c]:sw t6, 1472(a5)
Current Store : [0x800022a0] : sw a7, 1476(a5) -- Store: [0x800083fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800022ac]:fle.d t6, ft11, ft10
	-[0x800022b0]:csrrs a7, fflags, zero
	-[0x800022b4]:sw t6, 1488(a5)
Current Store : [0x800022b8] : sw a7, 1492(a5) -- Store: [0x8000840c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800022c4]:fle.d t6, ft11, ft10
	-[0x800022c8]:csrrs a7, fflags, zero
	-[0x800022cc]:sw t6, 1504(a5)
Current Store : [0x800022d0] : sw a7, 1508(a5) -- Store: [0x8000841c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800022dc]:fle.d t6, ft11, ft10
	-[0x800022e0]:csrrs a7, fflags, zero
	-[0x800022e4]:sw t6, 1520(a5)
Current Store : [0x800022e8] : sw a7, 1524(a5) -- Store: [0x8000842c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800022f4]:fle.d t6, ft11, ft10
	-[0x800022f8]:csrrs a7, fflags, zero
	-[0x800022fc]:sw t6, 1536(a5)
Current Store : [0x80002300] : sw a7, 1540(a5) -- Store: [0x8000843c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000230c]:fle.d t6, ft11, ft10
	-[0x80002310]:csrrs a7, fflags, zero
	-[0x80002314]:sw t6, 1552(a5)
Current Store : [0x80002318] : sw a7, 1556(a5) -- Store: [0x8000844c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002324]:fle.d t6, ft11, ft10
	-[0x80002328]:csrrs a7, fflags, zero
	-[0x8000232c]:sw t6, 1568(a5)
Current Store : [0x80002330] : sw a7, 1572(a5) -- Store: [0x8000845c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000233c]:fle.d t6, ft11, ft10
	-[0x80002340]:csrrs a7, fflags, zero
	-[0x80002344]:sw t6, 1584(a5)
Current Store : [0x80002348] : sw a7, 1588(a5) -- Store: [0x8000846c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002354]:fle.d t6, ft11, ft10
	-[0x80002358]:csrrs a7, fflags, zero
	-[0x8000235c]:sw t6, 1600(a5)
Current Store : [0x80002360] : sw a7, 1604(a5) -- Store: [0x8000847c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000236c]:fle.d t6, ft11, ft10
	-[0x80002370]:csrrs a7, fflags, zero
	-[0x80002374]:sw t6, 1616(a5)
Current Store : [0x80002378] : sw a7, 1620(a5) -- Store: [0x8000848c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002384]:fle.d t6, ft11, ft10
	-[0x80002388]:csrrs a7, fflags, zero
	-[0x8000238c]:sw t6, 1632(a5)
Current Store : [0x80002390] : sw a7, 1636(a5) -- Store: [0x8000849c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000239c]:fle.d t6, ft11, ft10
	-[0x800023a0]:csrrs a7, fflags, zero
	-[0x800023a4]:sw t6, 1648(a5)
Current Store : [0x800023a8] : sw a7, 1652(a5) -- Store: [0x800084ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800023b4]:fle.d t6, ft11, ft10
	-[0x800023b8]:csrrs a7, fflags, zero
	-[0x800023bc]:sw t6, 1664(a5)
Current Store : [0x800023c0] : sw a7, 1668(a5) -- Store: [0x800084bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800023cc]:fle.d t6, ft11, ft10
	-[0x800023d0]:csrrs a7, fflags, zero
	-[0x800023d4]:sw t6, 1680(a5)
Current Store : [0x800023d8] : sw a7, 1684(a5) -- Store: [0x800084cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800023e4]:fle.d t6, ft11, ft10
	-[0x800023e8]:csrrs a7, fflags, zero
	-[0x800023ec]:sw t6, 1696(a5)
Current Store : [0x800023f0] : sw a7, 1700(a5) -- Store: [0x800084dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800023fc]:fle.d t6, ft11, ft10
	-[0x80002400]:csrrs a7, fflags, zero
	-[0x80002404]:sw t6, 1712(a5)
Current Store : [0x80002408] : sw a7, 1716(a5) -- Store: [0x800084ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002414]:fle.d t6, ft11, ft10
	-[0x80002418]:csrrs a7, fflags, zero
	-[0x8000241c]:sw t6, 1728(a5)
Current Store : [0x80002420] : sw a7, 1732(a5) -- Store: [0x800084fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000242c]:fle.d t6, ft11, ft10
	-[0x80002430]:csrrs a7, fflags, zero
	-[0x80002434]:sw t6, 1744(a5)
Current Store : [0x80002438] : sw a7, 1748(a5) -- Store: [0x8000850c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002444]:fle.d t6, ft11, ft10
	-[0x80002448]:csrrs a7, fflags, zero
	-[0x8000244c]:sw t6, 1760(a5)
Current Store : [0x80002450] : sw a7, 1764(a5) -- Store: [0x8000851c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000245c]:fle.d t6, ft11, ft10
	-[0x80002460]:csrrs a7, fflags, zero
	-[0x80002464]:sw t6, 1776(a5)
Current Store : [0x80002468] : sw a7, 1780(a5) -- Store: [0x8000852c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002474]:fle.d t6, ft11, ft10
	-[0x80002478]:csrrs a7, fflags, zero
	-[0x8000247c]:sw t6, 1792(a5)
Current Store : [0x80002480] : sw a7, 1796(a5) -- Store: [0x8000853c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000248c]:fle.d t6, ft11, ft10
	-[0x80002490]:csrrs a7, fflags, zero
	-[0x80002494]:sw t6, 1808(a5)
Current Store : [0x80002498] : sw a7, 1812(a5) -- Store: [0x8000854c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800024a4]:fle.d t6, ft11, ft10
	-[0x800024a8]:csrrs a7, fflags, zero
	-[0x800024ac]:sw t6, 1824(a5)
Current Store : [0x800024b0] : sw a7, 1828(a5) -- Store: [0x8000855c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800024bc]:fle.d t6, ft11, ft10
	-[0x800024c0]:csrrs a7, fflags, zero
	-[0x800024c4]:sw t6, 1840(a5)
Current Store : [0x800024c8] : sw a7, 1844(a5) -- Store: [0x8000856c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800024d4]:fle.d t6, ft11, ft10
	-[0x800024d8]:csrrs a7, fflags, zero
	-[0x800024dc]:sw t6, 1856(a5)
Current Store : [0x800024e0] : sw a7, 1860(a5) -- Store: [0x8000857c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800024ec]:fle.d t6, ft11, ft10
	-[0x800024f0]:csrrs a7, fflags, zero
	-[0x800024f4]:sw t6, 1872(a5)
Current Store : [0x800024f8] : sw a7, 1876(a5) -- Store: [0x8000858c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002504]:fle.d t6, ft11, ft10
	-[0x80002508]:csrrs a7, fflags, zero
	-[0x8000250c]:sw t6, 1888(a5)
Current Store : [0x80002510] : sw a7, 1892(a5) -- Store: [0x8000859c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000251c]:fle.d t6, ft11, ft10
	-[0x80002520]:csrrs a7, fflags, zero
	-[0x80002524]:sw t6, 1904(a5)
Current Store : [0x80002528] : sw a7, 1908(a5) -- Store: [0x800085ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002538]:fle.d t6, ft11, ft10
	-[0x8000253c]:csrrs a7, fflags, zero
	-[0x80002540]:sw t6, 1920(a5)
Current Store : [0x80002544] : sw a7, 1924(a5) -- Store: [0x800085bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002550]:fle.d t6, ft11, ft10
	-[0x80002554]:csrrs a7, fflags, zero
	-[0x80002558]:sw t6, 1936(a5)
Current Store : [0x8000255c] : sw a7, 1940(a5) -- Store: [0x800085cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002568]:fle.d t6, ft11, ft10
	-[0x8000256c]:csrrs a7, fflags, zero
	-[0x80002570]:sw t6, 1952(a5)
Current Store : [0x80002574] : sw a7, 1956(a5) -- Store: [0x800085dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002580]:fle.d t6, ft11, ft10
	-[0x80002584]:csrrs a7, fflags, zero
	-[0x80002588]:sw t6, 1968(a5)
Current Store : [0x8000258c] : sw a7, 1972(a5) -- Store: [0x800085ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002598]:fle.d t6, ft11, ft10
	-[0x8000259c]:csrrs a7, fflags, zero
	-[0x800025a0]:sw t6, 1984(a5)
Current Store : [0x800025a4] : sw a7, 1988(a5) -- Store: [0x800085fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800025b0]:fle.d t6, ft11, ft10
	-[0x800025b4]:csrrs a7, fflags, zero
	-[0x800025b8]:sw t6, 2000(a5)
Current Store : [0x800025bc] : sw a7, 2004(a5) -- Store: [0x8000860c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800025c8]:fle.d t6, ft11, ft10
	-[0x800025cc]:csrrs a7, fflags, zero
	-[0x800025d0]:sw t6, 2016(a5)
Current Store : [0x800025d4] : sw a7, 2020(a5) -- Store: [0x8000861c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800025e8]:fle.d t6, ft11, ft10
	-[0x800025ec]:csrrs a7, fflags, zero
	-[0x800025f0]:sw t6, 0(a5)
Current Store : [0x800025f4] : sw a7, 4(a5) -- Store: [0x80008234]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002600]:fle.d t6, ft11, ft10
	-[0x80002604]:csrrs a7, fflags, zero
	-[0x80002608]:sw t6, 16(a5)
Current Store : [0x8000260c] : sw a7, 20(a5) -- Store: [0x80008244]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002618]:fle.d t6, ft11, ft10
	-[0x8000261c]:csrrs a7, fflags, zero
	-[0x80002620]:sw t6, 32(a5)
Current Store : [0x80002624] : sw a7, 36(a5) -- Store: [0x80008254]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002630]:fle.d t6, ft11, ft10
	-[0x80002634]:csrrs a7, fflags, zero
	-[0x80002638]:sw t6, 48(a5)
Current Store : [0x8000263c] : sw a7, 52(a5) -- Store: [0x80008264]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002648]:fle.d t6, ft11, ft10
	-[0x8000264c]:csrrs a7, fflags, zero
	-[0x80002650]:sw t6, 64(a5)
Current Store : [0x80002654] : sw a7, 68(a5) -- Store: [0x80008274]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002660]:fle.d t6, ft11, ft10
	-[0x80002664]:csrrs a7, fflags, zero
	-[0x80002668]:sw t6, 80(a5)
Current Store : [0x8000266c] : sw a7, 84(a5) -- Store: [0x80008284]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002678]:fle.d t6, ft11, ft10
	-[0x8000267c]:csrrs a7, fflags, zero
	-[0x80002680]:sw t6, 96(a5)
Current Store : [0x80002684] : sw a7, 100(a5) -- Store: [0x80008294]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002690]:fle.d t6, ft11, ft10
	-[0x80002694]:csrrs a7, fflags, zero
	-[0x80002698]:sw t6, 112(a5)
Current Store : [0x8000269c] : sw a7, 116(a5) -- Store: [0x800082a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800026a8]:fle.d t6, ft11, ft10
	-[0x800026ac]:csrrs a7, fflags, zero
	-[0x800026b0]:sw t6, 128(a5)
Current Store : [0x800026b4] : sw a7, 132(a5) -- Store: [0x800082b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800026c0]:fle.d t6, ft11, ft10
	-[0x800026c4]:csrrs a7, fflags, zero
	-[0x800026c8]:sw t6, 144(a5)
Current Store : [0x800026cc] : sw a7, 148(a5) -- Store: [0x800082c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800026d8]:fle.d t6, ft11, ft10
	-[0x800026dc]:csrrs a7, fflags, zero
	-[0x800026e0]:sw t6, 160(a5)
Current Store : [0x800026e4] : sw a7, 164(a5) -- Store: [0x800082d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800026f0]:fle.d t6, ft11, ft10
	-[0x800026f4]:csrrs a7, fflags, zero
	-[0x800026f8]:sw t6, 176(a5)
Current Store : [0x800026fc] : sw a7, 180(a5) -- Store: [0x800082e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002708]:fle.d t6, ft11, ft10
	-[0x8000270c]:csrrs a7, fflags, zero
	-[0x80002710]:sw t6, 192(a5)
Current Store : [0x80002714] : sw a7, 196(a5) -- Store: [0x800082f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002720]:fle.d t6, ft11, ft10
	-[0x80002724]:csrrs a7, fflags, zero
	-[0x80002728]:sw t6, 208(a5)
Current Store : [0x8000272c] : sw a7, 212(a5) -- Store: [0x80008304]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002738]:fle.d t6, ft11, ft10
	-[0x8000273c]:csrrs a7, fflags, zero
	-[0x80002740]:sw t6, 224(a5)
Current Store : [0x80002744] : sw a7, 228(a5) -- Store: [0x80008314]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002750]:fle.d t6, ft11, ft10
	-[0x80002754]:csrrs a7, fflags, zero
	-[0x80002758]:sw t6, 240(a5)
Current Store : [0x8000275c] : sw a7, 244(a5) -- Store: [0x80008324]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002768]:fle.d t6, ft11, ft10
	-[0x8000276c]:csrrs a7, fflags, zero
	-[0x80002770]:sw t6, 256(a5)
Current Store : [0x80002774] : sw a7, 260(a5) -- Store: [0x80008334]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002780]:fle.d t6, ft11, ft10
	-[0x80002784]:csrrs a7, fflags, zero
	-[0x80002788]:sw t6, 272(a5)
Current Store : [0x8000278c] : sw a7, 276(a5) -- Store: [0x80008344]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002798]:fle.d t6, ft11, ft10
	-[0x8000279c]:csrrs a7, fflags, zero
	-[0x800027a0]:sw t6, 288(a5)
Current Store : [0x800027a4] : sw a7, 292(a5) -- Store: [0x80008354]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800027b0]:fle.d t6, ft11, ft10
	-[0x800027b4]:csrrs a7, fflags, zero
	-[0x800027b8]:sw t6, 304(a5)
Current Store : [0x800027bc] : sw a7, 308(a5) -- Store: [0x80008364]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800027c8]:fle.d t6, ft11, ft10
	-[0x800027cc]:csrrs a7, fflags, zero
	-[0x800027d0]:sw t6, 320(a5)
Current Store : [0x800027d4] : sw a7, 324(a5) -- Store: [0x80008374]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800027e0]:fle.d t6, ft11, ft10
	-[0x800027e4]:csrrs a7, fflags, zero
	-[0x800027e8]:sw t6, 336(a5)
Current Store : [0x800027ec] : sw a7, 340(a5) -- Store: [0x80008384]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800027f8]:fle.d t6, ft11, ft10
	-[0x800027fc]:csrrs a7, fflags, zero
	-[0x80002800]:sw t6, 352(a5)
Current Store : [0x80002804] : sw a7, 356(a5) -- Store: [0x80008394]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002810]:fle.d t6, ft11, ft10
	-[0x80002814]:csrrs a7, fflags, zero
	-[0x80002818]:sw t6, 368(a5)
Current Store : [0x8000281c] : sw a7, 372(a5) -- Store: [0x800083a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002828]:fle.d t6, ft11, ft10
	-[0x8000282c]:csrrs a7, fflags, zero
	-[0x80002830]:sw t6, 384(a5)
Current Store : [0x80002834] : sw a7, 388(a5) -- Store: [0x800083b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002840]:fle.d t6, ft11, ft10
	-[0x80002844]:csrrs a7, fflags, zero
	-[0x80002848]:sw t6, 400(a5)
Current Store : [0x8000284c] : sw a7, 404(a5) -- Store: [0x800083c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002858]:fle.d t6, ft11, ft10
	-[0x8000285c]:csrrs a7, fflags, zero
	-[0x80002860]:sw t6, 416(a5)
Current Store : [0x80002864] : sw a7, 420(a5) -- Store: [0x800083d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002870]:fle.d t6, ft11, ft10
	-[0x80002874]:csrrs a7, fflags, zero
	-[0x80002878]:sw t6, 432(a5)
Current Store : [0x8000287c] : sw a7, 436(a5) -- Store: [0x800083e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002888]:fle.d t6, ft11, ft10
	-[0x8000288c]:csrrs a7, fflags, zero
	-[0x80002890]:sw t6, 448(a5)
Current Store : [0x80002894] : sw a7, 452(a5) -- Store: [0x800083f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800028a0]:fle.d t6, ft11, ft10
	-[0x800028a4]:csrrs a7, fflags, zero
	-[0x800028a8]:sw t6, 464(a5)
Current Store : [0x800028ac] : sw a7, 468(a5) -- Store: [0x80008404]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800028b8]:fle.d t6, ft11, ft10
	-[0x800028bc]:csrrs a7, fflags, zero
	-[0x800028c0]:sw t6, 480(a5)
Current Store : [0x800028c4] : sw a7, 484(a5) -- Store: [0x80008414]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800028d0]:fle.d t6, ft11, ft10
	-[0x800028d4]:csrrs a7, fflags, zero
	-[0x800028d8]:sw t6, 496(a5)
Current Store : [0x800028dc] : sw a7, 500(a5) -- Store: [0x80008424]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800028e8]:fle.d t6, ft11, ft10
	-[0x800028ec]:csrrs a7, fflags, zero
	-[0x800028f0]:sw t6, 512(a5)
Current Store : [0x800028f4] : sw a7, 516(a5) -- Store: [0x80008434]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002900]:fle.d t6, ft11, ft10
	-[0x80002904]:csrrs a7, fflags, zero
	-[0x80002908]:sw t6, 528(a5)
Current Store : [0x8000290c] : sw a7, 532(a5) -- Store: [0x80008444]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002918]:fle.d t6, ft11, ft10
	-[0x8000291c]:csrrs a7, fflags, zero
	-[0x80002920]:sw t6, 544(a5)
Current Store : [0x80002924] : sw a7, 548(a5) -- Store: [0x80008454]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002930]:fle.d t6, ft11, ft10
	-[0x80002934]:csrrs a7, fflags, zero
	-[0x80002938]:sw t6, 560(a5)
Current Store : [0x8000293c] : sw a7, 564(a5) -- Store: [0x80008464]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002948]:fle.d t6, ft11, ft10
	-[0x8000294c]:csrrs a7, fflags, zero
	-[0x80002950]:sw t6, 576(a5)
Current Store : [0x80002954] : sw a7, 580(a5) -- Store: [0x80008474]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002960]:fle.d t6, ft11, ft10
	-[0x80002964]:csrrs a7, fflags, zero
	-[0x80002968]:sw t6, 592(a5)
Current Store : [0x8000296c] : sw a7, 596(a5) -- Store: [0x80008484]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002978]:fle.d t6, ft11, ft10
	-[0x8000297c]:csrrs a7, fflags, zero
	-[0x80002980]:sw t6, 608(a5)
Current Store : [0x80002984] : sw a7, 612(a5) -- Store: [0x80008494]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002990]:fle.d t6, ft11, ft10
	-[0x80002994]:csrrs a7, fflags, zero
	-[0x80002998]:sw t6, 624(a5)
Current Store : [0x8000299c] : sw a7, 628(a5) -- Store: [0x800084a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800029a8]:fle.d t6, ft11, ft10
	-[0x800029ac]:csrrs a7, fflags, zero
	-[0x800029b0]:sw t6, 640(a5)
Current Store : [0x800029b4] : sw a7, 644(a5) -- Store: [0x800084b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800029c0]:fle.d t6, ft11, ft10
	-[0x800029c4]:csrrs a7, fflags, zero
	-[0x800029c8]:sw t6, 656(a5)
Current Store : [0x800029cc] : sw a7, 660(a5) -- Store: [0x800084c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800029d8]:fle.d t6, ft11, ft10
	-[0x800029dc]:csrrs a7, fflags, zero
	-[0x800029e0]:sw t6, 672(a5)
Current Store : [0x800029e4] : sw a7, 676(a5) -- Store: [0x800084d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800029f0]:fle.d t6, ft11, ft10
	-[0x800029f4]:csrrs a7, fflags, zero
	-[0x800029f8]:sw t6, 688(a5)
Current Store : [0x800029fc] : sw a7, 692(a5) -- Store: [0x800084e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a08]:fle.d t6, ft11, ft10
	-[0x80002a0c]:csrrs a7, fflags, zero
	-[0x80002a10]:sw t6, 704(a5)
Current Store : [0x80002a14] : sw a7, 708(a5) -- Store: [0x800084f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a20]:fle.d t6, ft11, ft10
	-[0x80002a24]:csrrs a7, fflags, zero
	-[0x80002a28]:sw t6, 720(a5)
Current Store : [0x80002a2c] : sw a7, 724(a5) -- Store: [0x80008504]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a38]:fle.d t6, ft11, ft10
	-[0x80002a3c]:csrrs a7, fflags, zero
	-[0x80002a40]:sw t6, 736(a5)
Current Store : [0x80002a44] : sw a7, 740(a5) -- Store: [0x80008514]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a50]:fle.d t6, ft11, ft10
	-[0x80002a54]:csrrs a7, fflags, zero
	-[0x80002a58]:sw t6, 752(a5)
Current Store : [0x80002a5c] : sw a7, 756(a5) -- Store: [0x80008524]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a68]:fle.d t6, ft11, ft10
	-[0x80002a6c]:csrrs a7, fflags, zero
	-[0x80002a70]:sw t6, 768(a5)
Current Store : [0x80002a74] : sw a7, 772(a5) -- Store: [0x80008534]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a80]:fle.d t6, ft11, ft10
	-[0x80002a84]:csrrs a7, fflags, zero
	-[0x80002a88]:sw t6, 784(a5)
Current Store : [0x80002a8c] : sw a7, 788(a5) -- Store: [0x80008544]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002a98]:fle.d t6, ft11, ft10
	-[0x80002a9c]:csrrs a7, fflags, zero
	-[0x80002aa0]:sw t6, 800(a5)
Current Store : [0x80002aa4] : sw a7, 804(a5) -- Store: [0x80008554]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ab0]:fle.d t6, ft11, ft10
	-[0x80002ab4]:csrrs a7, fflags, zero
	-[0x80002ab8]:sw t6, 816(a5)
Current Store : [0x80002abc] : sw a7, 820(a5) -- Store: [0x80008564]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ac8]:fle.d t6, ft11, ft10
	-[0x80002acc]:csrrs a7, fflags, zero
	-[0x80002ad0]:sw t6, 832(a5)
Current Store : [0x80002ad4] : sw a7, 836(a5) -- Store: [0x80008574]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ae0]:fle.d t6, ft11, ft10
	-[0x80002ae4]:csrrs a7, fflags, zero
	-[0x80002ae8]:sw t6, 848(a5)
Current Store : [0x80002aec] : sw a7, 852(a5) -- Store: [0x80008584]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002af8]:fle.d t6, ft11, ft10
	-[0x80002afc]:csrrs a7, fflags, zero
	-[0x80002b00]:sw t6, 864(a5)
Current Store : [0x80002b04] : sw a7, 868(a5) -- Store: [0x80008594]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b10]:fle.d t6, ft11, ft10
	-[0x80002b14]:csrrs a7, fflags, zero
	-[0x80002b18]:sw t6, 880(a5)
Current Store : [0x80002b1c] : sw a7, 884(a5) -- Store: [0x800085a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b28]:fle.d t6, ft11, ft10
	-[0x80002b2c]:csrrs a7, fflags, zero
	-[0x80002b30]:sw t6, 896(a5)
Current Store : [0x80002b34] : sw a7, 900(a5) -- Store: [0x800085b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b40]:fle.d t6, ft11, ft10
	-[0x80002b44]:csrrs a7, fflags, zero
	-[0x80002b48]:sw t6, 912(a5)
Current Store : [0x80002b4c] : sw a7, 916(a5) -- Store: [0x800085c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b58]:fle.d t6, ft11, ft10
	-[0x80002b5c]:csrrs a7, fflags, zero
	-[0x80002b60]:sw t6, 928(a5)
Current Store : [0x80002b64] : sw a7, 932(a5) -- Store: [0x800085d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b70]:fle.d t6, ft11, ft10
	-[0x80002b74]:csrrs a7, fflags, zero
	-[0x80002b78]:sw t6, 944(a5)
Current Store : [0x80002b7c] : sw a7, 948(a5) -- Store: [0x800085e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002b88]:fle.d t6, ft11, ft10
	-[0x80002b8c]:csrrs a7, fflags, zero
	-[0x80002b90]:sw t6, 960(a5)
Current Store : [0x80002b94] : sw a7, 964(a5) -- Store: [0x800085f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ba0]:fle.d t6, ft11, ft10
	-[0x80002ba4]:csrrs a7, fflags, zero
	-[0x80002ba8]:sw t6, 976(a5)
Current Store : [0x80002bac] : sw a7, 980(a5) -- Store: [0x80008604]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002bb8]:fle.d t6, ft11, ft10
	-[0x80002bbc]:csrrs a7, fflags, zero
	-[0x80002bc0]:sw t6, 992(a5)
Current Store : [0x80002bc4] : sw a7, 996(a5) -- Store: [0x80008614]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002bd0]:fle.d t6, ft11, ft10
	-[0x80002bd4]:csrrs a7, fflags, zero
	-[0x80002bd8]:sw t6, 1008(a5)
Current Store : [0x80002bdc] : sw a7, 1012(a5) -- Store: [0x80008624]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002be8]:fle.d t6, ft11, ft10
	-[0x80002bec]:csrrs a7, fflags, zero
	-[0x80002bf0]:sw t6, 1024(a5)
Current Store : [0x80002bf4] : sw a7, 1028(a5) -- Store: [0x80008634]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c00]:fle.d t6, ft11, ft10
	-[0x80002c04]:csrrs a7, fflags, zero
	-[0x80002c08]:sw t6, 1040(a5)
Current Store : [0x80002c0c] : sw a7, 1044(a5) -- Store: [0x80008644]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c18]:fle.d t6, ft11, ft10
	-[0x80002c1c]:csrrs a7, fflags, zero
	-[0x80002c20]:sw t6, 1056(a5)
Current Store : [0x80002c24] : sw a7, 1060(a5) -- Store: [0x80008654]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c30]:fle.d t6, ft11, ft10
	-[0x80002c34]:csrrs a7, fflags, zero
	-[0x80002c38]:sw t6, 1072(a5)
Current Store : [0x80002c3c] : sw a7, 1076(a5) -- Store: [0x80008664]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c48]:fle.d t6, ft11, ft10
	-[0x80002c4c]:csrrs a7, fflags, zero
	-[0x80002c50]:sw t6, 1088(a5)
Current Store : [0x80002c54] : sw a7, 1092(a5) -- Store: [0x80008674]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c60]:fle.d t6, ft11, ft10
	-[0x80002c64]:csrrs a7, fflags, zero
	-[0x80002c68]:sw t6, 1104(a5)
Current Store : [0x80002c6c] : sw a7, 1108(a5) -- Store: [0x80008684]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c78]:fle.d t6, ft11, ft10
	-[0x80002c7c]:csrrs a7, fflags, zero
	-[0x80002c80]:sw t6, 1120(a5)
Current Store : [0x80002c84] : sw a7, 1124(a5) -- Store: [0x80008694]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002c90]:fle.d t6, ft11, ft10
	-[0x80002c94]:csrrs a7, fflags, zero
	-[0x80002c98]:sw t6, 1136(a5)
Current Store : [0x80002c9c] : sw a7, 1140(a5) -- Store: [0x800086a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ca8]:fle.d t6, ft11, ft10
	-[0x80002cac]:csrrs a7, fflags, zero
	-[0x80002cb0]:sw t6, 1152(a5)
Current Store : [0x80002cb4] : sw a7, 1156(a5) -- Store: [0x800086b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002cc0]:fle.d t6, ft11, ft10
	-[0x80002cc4]:csrrs a7, fflags, zero
	-[0x80002cc8]:sw t6, 1168(a5)
Current Store : [0x80002ccc] : sw a7, 1172(a5) -- Store: [0x800086c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002cd8]:fle.d t6, ft11, ft10
	-[0x80002cdc]:csrrs a7, fflags, zero
	-[0x80002ce0]:sw t6, 1184(a5)
Current Store : [0x80002ce4] : sw a7, 1188(a5) -- Store: [0x800086d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002cf0]:fle.d t6, ft11, ft10
	-[0x80002cf4]:csrrs a7, fflags, zero
	-[0x80002cf8]:sw t6, 1200(a5)
Current Store : [0x80002cfc] : sw a7, 1204(a5) -- Store: [0x800086e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d08]:fle.d t6, ft11, ft10
	-[0x80002d0c]:csrrs a7, fflags, zero
	-[0x80002d10]:sw t6, 1216(a5)
Current Store : [0x80002d14] : sw a7, 1220(a5) -- Store: [0x800086f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d20]:fle.d t6, ft11, ft10
	-[0x80002d24]:csrrs a7, fflags, zero
	-[0x80002d28]:sw t6, 1232(a5)
Current Store : [0x80002d2c] : sw a7, 1236(a5) -- Store: [0x80008704]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d38]:fle.d t6, ft11, ft10
	-[0x80002d3c]:csrrs a7, fflags, zero
	-[0x80002d40]:sw t6, 1248(a5)
Current Store : [0x80002d44] : sw a7, 1252(a5) -- Store: [0x80008714]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d50]:fle.d t6, ft11, ft10
	-[0x80002d54]:csrrs a7, fflags, zero
	-[0x80002d58]:sw t6, 1264(a5)
Current Store : [0x80002d5c] : sw a7, 1268(a5) -- Store: [0x80008724]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d68]:fle.d t6, ft11, ft10
	-[0x80002d6c]:csrrs a7, fflags, zero
	-[0x80002d70]:sw t6, 1280(a5)
Current Store : [0x80002d74] : sw a7, 1284(a5) -- Store: [0x80008734]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d80]:fle.d t6, ft11, ft10
	-[0x80002d84]:csrrs a7, fflags, zero
	-[0x80002d88]:sw t6, 1296(a5)
Current Store : [0x80002d8c] : sw a7, 1300(a5) -- Store: [0x80008744]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002d98]:fle.d t6, ft11, ft10
	-[0x80002d9c]:csrrs a7, fflags, zero
	-[0x80002da0]:sw t6, 1312(a5)
Current Store : [0x80002da4] : sw a7, 1316(a5) -- Store: [0x80008754]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002db0]:fle.d t6, ft11, ft10
	-[0x80002db4]:csrrs a7, fflags, zero
	-[0x80002db8]:sw t6, 1328(a5)
Current Store : [0x80002dbc] : sw a7, 1332(a5) -- Store: [0x80008764]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002dc8]:fle.d t6, ft11, ft10
	-[0x80002dcc]:csrrs a7, fflags, zero
	-[0x80002dd0]:sw t6, 1344(a5)
Current Store : [0x80002dd4] : sw a7, 1348(a5) -- Store: [0x80008774]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002de0]:fle.d t6, ft11, ft10
	-[0x80002de4]:csrrs a7, fflags, zero
	-[0x80002de8]:sw t6, 1360(a5)
Current Store : [0x80002dec] : sw a7, 1364(a5) -- Store: [0x80008784]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002df8]:fle.d t6, ft11, ft10
	-[0x80002dfc]:csrrs a7, fflags, zero
	-[0x80002e00]:sw t6, 1376(a5)
Current Store : [0x80002e04] : sw a7, 1380(a5) -- Store: [0x80008794]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e10]:fle.d t6, ft11, ft10
	-[0x80002e14]:csrrs a7, fflags, zero
	-[0x80002e18]:sw t6, 1392(a5)
Current Store : [0x80002e1c] : sw a7, 1396(a5) -- Store: [0x800087a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e28]:fle.d t6, ft11, ft10
	-[0x80002e2c]:csrrs a7, fflags, zero
	-[0x80002e30]:sw t6, 1408(a5)
Current Store : [0x80002e34] : sw a7, 1412(a5) -- Store: [0x800087b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e40]:fle.d t6, ft11, ft10
	-[0x80002e44]:csrrs a7, fflags, zero
	-[0x80002e48]:sw t6, 1424(a5)
Current Store : [0x80002e4c] : sw a7, 1428(a5) -- Store: [0x800087c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e58]:fle.d t6, ft11, ft10
	-[0x80002e5c]:csrrs a7, fflags, zero
	-[0x80002e60]:sw t6, 1440(a5)
Current Store : [0x80002e64] : sw a7, 1444(a5) -- Store: [0x800087d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e70]:fle.d t6, ft11, ft10
	-[0x80002e74]:csrrs a7, fflags, zero
	-[0x80002e78]:sw t6, 1456(a5)
Current Store : [0x80002e7c] : sw a7, 1460(a5) -- Store: [0x800087e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002e88]:fle.d t6, ft11, ft10
	-[0x80002e8c]:csrrs a7, fflags, zero
	-[0x80002e90]:sw t6, 1472(a5)
Current Store : [0x80002e94] : sw a7, 1476(a5) -- Store: [0x800087f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002ea0]:fle.d t6, ft11, ft10
	-[0x80002ea4]:csrrs a7, fflags, zero
	-[0x80002ea8]:sw t6, 1488(a5)
Current Store : [0x80002eac] : sw a7, 1492(a5) -- Store: [0x80008804]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80002eb8]:fle.d t6, ft11, ft10
	-[0x80002ebc]:csrrs a7, fflags, zero
	-[0x80002ec0]:sw t6, 1504(a5)
Current Store : [0x80002ec4] : sw a7, 1508(a5) -- Store: [0x80008814]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800031dc]:fle.d t6, ft11, ft10
	-[0x800031e0]:csrrs a7, fflags, zero
	-[0x800031e4]:sw t6, 0(a5)
Current Store : [0x800031e8] : sw a7, 4(a5) -- Store: [0x8000862c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800031f4]:fle.d t6, ft11, ft10
	-[0x800031f8]:csrrs a7, fflags, zero
	-[0x800031fc]:sw t6, 16(a5)
Current Store : [0x80003200] : sw a7, 20(a5) -- Store: [0x8000863c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000320c]:fle.d t6, ft11, ft10
	-[0x80003210]:csrrs a7, fflags, zero
	-[0x80003214]:sw t6, 32(a5)
Current Store : [0x80003218] : sw a7, 36(a5) -- Store: [0x8000864c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003224]:fle.d t6, ft11, ft10
	-[0x80003228]:csrrs a7, fflags, zero
	-[0x8000322c]:sw t6, 48(a5)
Current Store : [0x80003230] : sw a7, 52(a5) -- Store: [0x8000865c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000323c]:fle.d t6, ft11, ft10
	-[0x80003240]:csrrs a7, fflags, zero
	-[0x80003244]:sw t6, 64(a5)
Current Store : [0x80003248] : sw a7, 68(a5) -- Store: [0x8000866c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003254]:fle.d t6, ft11, ft10
	-[0x80003258]:csrrs a7, fflags, zero
	-[0x8000325c]:sw t6, 80(a5)
Current Store : [0x80003260] : sw a7, 84(a5) -- Store: [0x8000867c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000326c]:fle.d t6, ft11, ft10
	-[0x80003270]:csrrs a7, fflags, zero
	-[0x80003274]:sw t6, 96(a5)
Current Store : [0x80003278] : sw a7, 100(a5) -- Store: [0x8000868c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003284]:fle.d t6, ft11, ft10
	-[0x80003288]:csrrs a7, fflags, zero
	-[0x8000328c]:sw t6, 112(a5)
Current Store : [0x80003290] : sw a7, 116(a5) -- Store: [0x8000869c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000329c]:fle.d t6, ft11, ft10
	-[0x800032a0]:csrrs a7, fflags, zero
	-[0x800032a4]:sw t6, 128(a5)
Current Store : [0x800032a8] : sw a7, 132(a5) -- Store: [0x800086ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800032b4]:fle.d t6, ft11, ft10
	-[0x800032b8]:csrrs a7, fflags, zero
	-[0x800032bc]:sw t6, 144(a5)
Current Store : [0x800032c0] : sw a7, 148(a5) -- Store: [0x800086bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800032cc]:fle.d t6, ft11, ft10
	-[0x800032d0]:csrrs a7, fflags, zero
	-[0x800032d4]:sw t6, 160(a5)
Current Store : [0x800032d8] : sw a7, 164(a5) -- Store: [0x800086cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800032e4]:fle.d t6, ft11, ft10
	-[0x800032e8]:csrrs a7, fflags, zero
	-[0x800032ec]:sw t6, 176(a5)
Current Store : [0x800032f0] : sw a7, 180(a5) -- Store: [0x800086dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800032fc]:fle.d t6, ft11, ft10
	-[0x80003300]:csrrs a7, fflags, zero
	-[0x80003304]:sw t6, 192(a5)
Current Store : [0x80003308] : sw a7, 196(a5) -- Store: [0x800086ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003314]:fle.d t6, ft11, ft10
	-[0x80003318]:csrrs a7, fflags, zero
	-[0x8000331c]:sw t6, 208(a5)
Current Store : [0x80003320] : sw a7, 212(a5) -- Store: [0x800086fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000332c]:fle.d t6, ft11, ft10
	-[0x80003330]:csrrs a7, fflags, zero
	-[0x80003334]:sw t6, 224(a5)
Current Store : [0x80003338] : sw a7, 228(a5) -- Store: [0x8000870c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003344]:fle.d t6, ft11, ft10
	-[0x80003348]:csrrs a7, fflags, zero
	-[0x8000334c]:sw t6, 240(a5)
Current Store : [0x80003350] : sw a7, 244(a5) -- Store: [0x8000871c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000335c]:fle.d t6, ft11, ft10
	-[0x80003360]:csrrs a7, fflags, zero
	-[0x80003364]:sw t6, 256(a5)
Current Store : [0x80003368] : sw a7, 260(a5) -- Store: [0x8000872c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003374]:fle.d t6, ft11, ft10
	-[0x80003378]:csrrs a7, fflags, zero
	-[0x8000337c]:sw t6, 272(a5)
Current Store : [0x80003380] : sw a7, 276(a5) -- Store: [0x8000873c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000338c]:fle.d t6, ft11, ft10
	-[0x80003390]:csrrs a7, fflags, zero
	-[0x80003394]:sw t6, 288(a5)
Current Store : [0x80003398] : sw a7, 292(a5) -- Store: [0x8000874c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800033a4]:fle.d t6, ft11, ft10
	-[0x800033a8]:csrrs a7, fflags, zero
	-[0x800033ac]:sw t6, 304(a5)
Current Store : [0x800033b0] : sw a7, 308(a5) -- Store: [0x8000875c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800033bc]:fle.d t6, ft11, ft10
	-[0x800033c0]:csrrs a7, fflags, zero
	-[0x800033c4]:sw t6, 320(a5)
Current Store : [0x800033c8] : sw a7, 324(a5) -- Store: [0x8000876c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800033d4]:fle.d t6, ft11, ft10
	-[0x800033d8]:csrrs a7, fflags, zero
	-[0x800033dc]:sw t6, 336(a5)
Current Store : [0x800033e0] : sw a7, 340(a5) -- Store: [0x8000877c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800033ec]:fle.d t6, ft11, ft10
	-[0x800033f0]:csrrs a7, fflags, zero
	-[0x800033f4]:sw t6, 352(a5)
Current Store : [0x800033f8] : sw a7, 356(a5) -- Store: [0x8000878c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003404]:fle.d t6, ft11, ft10
	-[0x80003408]:csrrs a7, fflags, zero
	-[0x8000340c]:sw t6, 368(a5)
Current Store : [0x80003410] : sw a7, 372(a5) -- Store: [0x8000879c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000341c]:fle.d t6, ft11, ft10
	-[0x80003420]:csrrs a7, fflags, zero
	-[0x80003424]:sw t6, 384(a5)
Current Store : [0x80003428] : sw a7, 388(a5) -- Store: [0x800087ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003434]:fle.d t6, ft11, ft10
	-[0x80003438]:csrrs a7, fflags, zero
	-[0x8000343c]:sw t6, 400(a5)
Current Store : [0x80003440] : sw a7, 404(a5) -- Store: [0x800087bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000344c]:fle.d t6, ft11, ft10
	-[0x80003450]:csrrs a7, fflags, zero
	-[0x80003454]:sw t6, 416(a5)
Current Store : [0x80003458] : sw a7, 420(a5) -- Store: [0x800087cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003464]:fle.d t6, ft11, ft10
	-[0x80003468]:csrrs a7, fflags, zero
	-[0x8000346c]:sw t6, 432(a5)
Current Store : [0x80003470] : sw a7, 436(a5) -- Store: [0x800087dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000347c]:fle.d t6, ft11, ft10
	-[0x80003480]:csrrs a7, fflags, zero
	-[0x80003484]:sw t6, 448(a5)
Current Store : [0x80003488] : sw a7, 452(a5) -- Store: [0x800087ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80003494]:fle.d t6, ft11, ft10
	-[0x80003498]:csrrs a7, fflags, zero
	-[0x8000349c]:sw t6, 464(a5)
Current Store : [0x800034a0] : sw a7, 468(a5) -- Store: [0x800087fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800034ac]:fle.d t6, ft11, ft10
	-[0x800034b0]:csrrs a7, fflags, zero
	-[0x800034b4]:sw t6, 480(a5)
Current Store : [0x800034b8] : sw a7, 484(a5) -- Store: [0x8000880c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800034c4]:fle.d t6, ft11, ft10
	-[0x800034c8]:csrrs a7, fflags, zero
	-[0x800034cc]:sw t6, 496(a5)
Current Store : [0x800034d0] : sw a7, 500(a5) -- Store: [0x8000881c]:0x00000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                           coverpoints                                                                                                           |                                                     code                                                      |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
|   1|[0x80007610]<br>0x00000001|- opcode : fle.d<br> - rd : x10<br> - rs1 : f24<br> - rs2 : f17<br> - rs1 != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fle.d a0, fs8, fa7<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw a0, 0(a5)<br>      |
|   2|[0x80007618]<br>0x00000001|- rd : x15<br> - rs1 : f29<br> - rs2 : f29<br> - rs1 == rs2<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                      |[0x80000140]:fle.d a5, ft9, ft9<br> [0x80000144]:csrrs s5, fflags, zero<br> [0x80000148]:sw a5, 0(s3)<br>      |
|   3|[0x80007620]<br>0x00000001|- rd : x18<br> - rs1 : f7<br> - rs2 : f9<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                         |[0x80000164]:fle.d s2, ft7, fs1<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s2, 0(a5)<br>      |
|   4|[0x80007628]<br>0x00000000|- rd : x16<br> - rs1 : f22<br> - rs2 : f28<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                       |[0x80000188]:fle.d a6, fs6, ft8<br> [0x8000018c]:csrrs s5, fflags, zero<br> [0x80000190]:sw a6, 0(s3)<br>      |
|   5|[0x80007630]<br>0x00000000|- rd : x0<br> - rs1 : f5<br> - rs2 : f4<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                          |[0x800001ac]:fle.d zero, ft5, ft4<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw zero, 0(a5)<br>  |
|   6|[0x80007640]<br>0x00000000|- rd : x1<br> - rs1 : f4<br> - rs2 : f18<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                         |[0x800001c4]:fle.d ra, ft4, fs2<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw ra, 16(a5)<br>     |
|   7|[0x80007640]<br>0x00000000|- rd : x17<br> - rs1 : f12<br> - rs2 : f7<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                        |[0x800001e8]:fle.d a7, fa2, ft7<br> [0x800001ec]:csrrs s5, fflags, zero<br> [0x800001f0]:sw a7, 0(s3)<br>      |
|   8|[0x80007648]<br>0x00000000|- rd : x26<br> - rs1 : f6<br> - rs2 : f3<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                         |[0x8000020c]:fle.d s10, ft6, ft3<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s10, 0(a5)<br>    |
|   9|[0x80007658]<br>0x00000000|- rd : x14<br> - rs1 : f15<br> - rs2 : f14<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                       |[0x80000224]:fle.d a4, fa5, fa4<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw a4, 16(a5)<br>     |
|  10|[0x80007668]<br>0x00000000|- rd : x29<br> - rs1 : f3<br> - rs2 : f31<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x8000023c]:fle.d t4, ft3, ft11<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw t4, 32(a5)<br>    |
|  11|[0x80007678]<br>0x00000001|- rd : x22<br> - rs1 : f21<br> - rs2 : f0<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x80000254]:fle.d s6, fs5, ft0<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw s6, 48(a5)<br>     |
|  12|[0x80007688]<br>0x00000000|- rd : x8<br> - rs1 : f17<br> - rs2 : f6<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                         |[0x8000026c]:fle.d fp, fa7, ft6<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw fp, 64(a5)<br>     |
|  13|[0x80007698]<br>0x00000001|- rd : x28<br> - rs1 : f13<br> - rs2 : f1<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                        |[0x80000284]:fle.d t3, fa3, ft1<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw t3, 80(a5)<br>     |
|  14|[0x800076a8]<br>0x00000001|- rd : x27<br> - rs1 : f9<br> - rs2 : f25<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x8000029c]:fle.d s11, fs1, fs9<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw s11, 96(a5)<br>   |
|  15|[0x800076b8]<br>0x00000001|- rd : x25<br> - rs1 : f30<br> - rs2 : f20<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                       |[0x800002b4]:fle.d s9, ft10, fs4<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw s9, 112(a5)<br>   |
|  16|[0x800076c8]<br>0x00000001|- rd : x4<br> - rs1 : f20<br> - rs2 : f13<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x800002cc]:fle.d tp, fs4, fa3<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw tp, 128(a5)<br>    |
|  17|[0x800076d8]<br>0x00000001|- rd : x5<br> - rs1 : f28<br> - rs2 : f21<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x800002e4]:fle.d t0, ft8, fs5<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw t0, 144(a5)<br>    |
|  18|[0x800076e8]<br>0x00000001|- rd : x21<br> - rs1 : f27<br> - rs2 : f8<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                        |[0x800002fc]:fle.d s5, fs11, fs0<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw s5, 160(a5)<br>   |
|  19|[0x800076f8]<br>0x00000001|- rd : x2<br> - rs1 : f1<br> - rs2 : f15<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                         |[0x80000314]:fle.d sp, ft1, fa5<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw sp, 176(a5)<br>    |
|  20|[0x80007708]<br>0x00000001|- rd : x3<br> - rs1 : f25<br> - rs2 : f16<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x8000032c]:fle.d gp, fs9, fa6<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw gp, 192(a5)<br>    |
|  21|[0x80007718]<br>0x00000001|- rd : x19<br> - rs1 : f8<br> - rs2 : f10<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x80000344]:fle.d s3, fs0, fa0<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw s3, 208(a5)<br>    |
|  22|[0x80007728]<br>0x00000001|- rd : x23<br> - rs1 : f31<br> - rs2 : f5<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                        |[0x8000035c]:fle.d s7, ft11, ft5<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw s7, 224(a5)<br>   |
|  23|[0x80007738]<br>0x00000001|- rd : x11<br> - rs1 : f26<br> - rs2 : f27<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                       |[0x80000374]:fle.d a1, fs10, fs11<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw a1, 240(a5)<br>  |
|  24|[0x80007748]<br>0x00000001|- rd : x13<br> - rs1 : f16<br> - rs2 : f30<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                       |[0x8000038c]:fle.d a3, fa6, ft10<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw a3, 256(a5)<br>   |
|  25|[0x80007758]<br>0x00000001|- rd : x7<br> - rs1 : f23<br> - rs2 : f19<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x800003a4]:fle.d t2, fs7, fs3<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw t2, 272(a5)<br>    |
|  26|[0x80007768]<br>0x00000000|- rd : x20<br> - rs1 : f0<br> - rs2 : f26<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x800003bc]:fle.d s4, ft0, fs10<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw s4, 288(a5)<br>   |
|  27|[0x80007778]<br>0x00000001|- rd : x30<br> - rs1 : f18<br> - rs2 : f23<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                       |[0x800003d4]:fle.d t5, fs2, fs7<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw t5, 304(a5)<br>    |
|  28|[0x80007788]<br>0x00000000|- rd : x6<br> - rs1 : f14<br> - rs2 : f12<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                        |[0x800003ec]:fle.d t1, fa4, fa2<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw t1, 320(a5)<br>    |
|  29|[0x80007798]<br>0x00000000|- rd : x9<br> - rs1 : f19<br> - rs2 : f11<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                        |[0x80000404]:fle.d s1, fs3, fa1<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw s1, 336(a5)<br>    |
|  30|[0x800077a8]<br>0x00000000|- rd : x12<br> - rs1 : f10<br> - rs2 : f2<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                        |[0x8000041c]:fle.d a2, fa0, ft2<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw a2, 352(a5)<br>    |
|  31|[0x800077b8]<br>0x00000000|- rd : x31<br> - rs1 : f11<br> - rs2 : f24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                       |[0x80000434]:fle.d t6, fa1, fs8<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw t6, 368(a5)<br>    |
|  32|[0x800077c8]<br>0x00000000|- rd : x24<br> - rs1 : f2<br> - rs2 : f22<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                        |[0x8000044c]:fle.d s8, ft2, fs6<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw s8, 384(a5)<br>    |
|  33|[0x800077d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000464]:fle.d t6, ft11, ft10<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 400(a5)<br>  |
|  34|[0x800077e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000047c]:fle.d t6, ft11, ft10<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:sw t6, 416(a5)<br>  |
|  35|[0x800077f8]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000494]:fle.d t6, ft11, ft10<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw t6, 432(a5)<br>  |
|  36|[0x80007808]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800004ac]:fle.d t6, ft11, ft10<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:sw t6, 448(a5)<br>  |
|  37|[0x80007818]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800004c4]:fle.d t6, ft11, ft10<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:sw t6, 464(a5)<br>  |
|  38|[0x80007828]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800004dc]:fle.d t6, ft11, ft10<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:sw t6, 480(a5)<br>  |
|  39|[0x80007838]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800004f4]:fle.d t6, ft11, ft10<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:sw t6, 496(a5)<br>  |
|  40|[0x80007848]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000050c]:fle.d t6, ft11, ft10<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw t6, 512(a5)<br>  |
|  41|[0x80007858]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000524]:fle.d t6, ft11, ft10<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:sw t6, 528(a5)<br>  |
|  42|[0x80007868]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000053c]:fle.d t6, ft11, ft10<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:sw t6, 544(a5)<br>  |
|  43|[0x80007878]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000554]:fle.d t6, ft11, ft10<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:sw t6, 560(a5)<br>  |
|  44|[0x80007888]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000056c]:fle.d t6, ft11, ft10<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:sw t6, 576(a5)<br>  |
|  45|[0x80007898]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000584]:fle.d t6, ft11, ft10<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:sw t6, 592(a5)<br>  |
|  46|[0x800078a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000059c]:fle.d t6, ft11, ft10<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:sw t6, 608(a5)<br>  |
|  47|[0x800078b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800005b4]:fle.d t6, ft11, ft10<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:sw t6, 624(a5)<br>  |
|  48|[0x800078c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800005cc]:fle.d t6, ft11, ft10<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:sw t6, 640(a5)<br>  |
|  49|[0x800078d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800005e4]:fle.d t6, ft11, ft10<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:sw t6, 656(a5)<br>  |
|  50|[0x800078e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800005fc]:fle.d t6, ft11, ft10<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:sw t6, 672(a5)<br>  |
|  51|[0x800078f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000614]:fle.d t6, ft11, ft10<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:sw t6, 688(a5)<br>  |
|  52|[0x80007908]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000062c]:fle.d t6, ft11, ft10<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:sw t6, 704(a5)<br>  |
|  53|[0x80007918]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000644]:fle.d t6, ft11, ft10<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:sw t6, 720(a5)<br>  |
|  54|[0x80007928]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000065c]:fle.d t6, ft11, ft10<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:sw t6, 736(a5)<br>  |
|  55|[0x80007938]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000674]:fle.d t6, ft11, ft10<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:sw t6, 752(a5)<br>  |
|  56|[0x80007948]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000068c]:fle.d t6, ft11, ft10<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:sw t6, 768(a5)<br>  |
|  57|[0x80007958]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800006a4]:fle.d t6, ft11, ft10<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:sw t6, 784(a5)<br>  |
|  58|[0x80007968]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800006bc]:fle.d t6, ft11, ft10<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:sw t6, 800(a5)<br>  |
|  59|[0x80007978]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800006d4]:fle.d t6, ft11, ft10<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:sw t6, 816(a5)<br>  |
|  60|[0x80007988]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800006ec]:fle.d t6, ft11, ft10<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:sw t6, 832(a5)<br>  |
|  61|[0x80007998]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000704]:fle.d t6, ft11, ft10<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:sw t6, 848(a5)<br>  |
|  62|[0x800079a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000071c]:fle.d t6, ft11, ft10<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:sw t6, 864(a5)<br>  |
|  63|[0x800079b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000734]:fle.d t6, ft11, ft10<br> [0x80000738]:csrrs a7, fflags, zero<br> [0x8000073c]:sw t6, 880(a5)<br>  |
|  64|[0x800079c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000074c]:fle.d t6, ft11, ft10<br> [0x80000750]:csrrs a7, fflags, zero<br> [0x80000754]:sw t6, 896(a5)<br>  |
|  65|[0x800079d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000764]:fle.d t6, ft11, ft10<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:sw t6, 912(a5)<br>  |
|  66|[0x800079e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000077c]:fle.d t6, ft11, ft10<br> [0x80000780]:csrrs a7, fflags, zero<br> [0x80000784]:sw t6, 928(a5)<br>  |
|  67|[0x800079f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000794]:fle.d t6, ft11, ft10<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:sw t6, 944(a5)<br>  |
|  68|[0x80007a08]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800007ac]:fle.d t6, ft11, ft10<br> [0x800007b0]:csrrs a7, fflags, zero<br> [0x800007b4]:sw t6, 960(a5)<br>  |
|  69|[0x80007a18]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800007c4]:fle.d t6, ft11, ft10<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:sw t6, 976(a5)<br>  |
|  70|[0x80007a28]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800007dc]:fle.d t6, ft11, ft10<br> [0x800007e0]:csrrs a7, fflags, zero<br> [0x800007e4]:sw t6, 992(a5)<br>  |
|  71|[0x80007a38]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800007f4]:fle.d t6, ft11, ft10<br> [0x800007f8]:csrrs a7, fflags, zero<br> [0x800007fc]:sw t6, 1008(a5)<br> |
|  72|[0x80007a48]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000080c]:fle.d t6, ft11, ft10<br> [0x80000810]:csrrs a7, fflags, zero<br> [0x80000814]:sw t6, 1024(a5)<br> |
|  73|[0x80007a58]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000824]:fle.d t6, ft11, ft10<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:sw t6, 1040(a5)<br> |
|  74|[0x80007a68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000083c]:fle.d t6, ft11, ft10<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:sw t6, 1056(a5)<br> |
|  75|[0x80007a78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000854]:fle.d t6, ft11, ft10<br> [0x80000858]:csrrs a7, fflags, zero<br> [0x8000085c]:sw t6, 1072(a5)<br> |
|  76|[0x80007a88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000086c]:fle.d t6, ft11, ft10<br> [0x80000870]:csrrs a7, fflags, zero<br> [0x80000874]:sw t6, 1088(a5)<br> |
|  77|[0x80007a98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000884]:fle.d t6, ft11, ft10<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:sw t6, 1104(a5)<br> |
|  78|[0x80007aa8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000089c]:fle.d t6, ft11, ft10<br> [0x800008a0]:csrrs a7, fflags, zero<br> [0x800008a4]:sw t6, 1120(a5)<br> |
|  79|[0x80007ab8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800008b4]:fle.d t6, ft11, ft10<br> [0x800008b8]:csrrs a7, fflags, zero<br> [0x800008bc]:sw t6, 1136(a5)<br> |
|  80|[0x80007ac8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800008cc]:fle.d t6, ft11, ft10<br> [0x800008d0]:csrrs a7, fflags, zero<br> [0x800008d4]:sw t6, 1152(a5)<br> |
|  81|[0x80007ad8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800008e4]:fle.d t6, ft11, ft10<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:sw t6, 1168(a5)<br> |
|  82|[0x80007ae8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800008fc]:fle.d t6, ft11, ft10<br> [0x80000900]:csrrs a7, fflags, zero<br> [0x80000904]:sw t6, 1184(a5)<br> |
|  83|[0x80007af8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000914]:fle.d t6, ft11, ft10<br> [0x80000918]:csrrs a7, fflags, zero<br> [0x8000091c]:sw t6, 1200(a5)<br> |
|  84|[0x80007b08]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000092c]:fle.d t6, ft11, ft10<br> [0x80000930]:csrrs a7, fflags, zero<br> [0x80000934]:sw t6, 1216(a5)<br> |
|  85|[0x80007b18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000944]:fle.d t6, ft11, ft10<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:sw t6, 1232(a5)<br> |
|  86|[0x80007b28]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000095c]:fle.d t6, ft11, ft10<br> [0x80000960]:csrrs a7, fflags, zero<br> [0x80000964]:sw t6, 1248(a5)<br> |
|  87|[0x80007b38]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000974]:fle.d t6, ft11, ft10<br> [0x80000978]:csrrs a7, fflags, zero<br> [0x8000097c]:sw t6, 1264(a5)<br> |
|  88|[0x80007b48]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000098c]:fle.d t6, ft11, ft10<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:sw t6, 1280(a5)<br> |
|  89|[0x80007b58]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800009a4]:fle.d t6, ft11, ft10<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:sw t6, 1296(a5)<br> |
|  90|[0x80007b68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800009bc]:fle.d t6, ft11, ft10<br> [0x800009c0]:csrrs a7, fflags, zero<br> [0x800009c4]:sw t6, 1312(a5)<br> |
|  91|[0x80007b78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800009d4]:fle.d t6, ft11, ft10<br> [0x800009d8]:csrrs a7, fflags, zero<br> [0x800009dc]:sw t6, 1328(a5)<br> |
|  92|[0x80007b88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800009ec]:fle.d t6, ft11, ft10<br> [0x800009f0]:csrrs a7, fflags, zero<br> [0x800009f4]:sw t6, 1344(a5)<br> |
|  93|[0x80007b98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a04]:fle.d t6, ft11, ft10<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:sw t6, 1360(a5)<br> |
|  94|[0x80007ba8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a1c]:fle.d t6, ft11, ft10<br> [0x80000a20]:csrrs a7, fflags, zero<br> [0x80000a24]:sw t6, 1376(a5)<br> |
|  95|[0x80007bb8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a34]:fle.d t6, ft11, ft10<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:sw t6, 1392(a5)<br> |
|  96|[0x80007bc8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a4c]:fle.d t6, ft11, ft10<br> [0x80000a50]:csrrs a7, fflags, zero<br> [0x80000a54]:sw t6, 1408(a5)<br> |
|  97|[0x80007bd8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a64]:fle.d t6, ft11, ft10<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:sw t6, 1424(a5)<br> |
|  98|[0x80007be8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a7c]:fle.d t6, ft11, ft10<br> [0x80000a80]:csrrs a7, fflags, zero<br> [0x80000a84]:sw t6, 1440(a5)<br> |
|  99|[0x80007bf8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000a94]:fle.d t6, ft11, ft10<br> [0x80000a98]:csrrs a7, fflags, zero<br> [0x80000a9c]:sw t6, 1456(a5)<br> |
| 100|[0x80007c08]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000aac]:fle.d t6, ft11, ft10<br> [0x80000ab0]:csrrs a7, fflags, zero<br> [0x80000ab4]:sw t6, 1472(a5)<br> |
| 101|[0x80007c18]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ac4]:fle.d t6, ft11, ft10<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:sw t6, 1488(a5)<br> |
| 102|[0x80007c28]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000adc]:fle.d t6, ft11, ft10<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:sw t6, 1504(a5)<br> |
| 103|[0x80007c38]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000af4]:fle.d t6, ft11, ft10<br> [0x80000af8]:csrrs a7, fflags, zero<br> [0x80000afc]:sw t6, 1520(a5)<br> |
| 104|[0x80007c48]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000b0c]:fle.d t6, ft11, ft10<br> [0x80000b10]:csrrs a7, fflags, zero<br> [0x80000b14]:sw t6, 1536(a5)<br> |
| 105|[0x80007c58]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000b24]:fle.d t6, ft11, ft10<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:sw t6, 1552(a5)<br> |
| 106|[0x80007c68]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000b3c]:fle.d t6, ft11, ft10<br> [0x80000b40]:csrrs a7, fflags, zero<br> [0x80000b44]:sw t6, 1568(a5)<br> |
| 107|[0x80007c78]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000b54]:fle.d t6, ft11, ft10<br> [0x80000b58]:csrrs a7, fflags, zero<br> [0x80000b5c]:sw t6, 1584(a5)<br> |
| 108|[0x80007c88]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000b6c]:fle.d t6, ft11, ft10<br> [0x80000b70]:csrrs a7, fflags, zero<br> [0x80000b74]:sw t6, 1600(a5)<br> |
| 109|[0x80007c98]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000b84]:fle.d t6, ft11, ft10<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:sw t6, 1616(a5)<br> |
| 110|[0x80007ca8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000b9c]:fle.d t6, ft11, ft10<br> [0x80000ba0]:csrrs a7, fflags, zero<br> [0x80000ba4]:sw t6, 1632(a5)<br> |
| 111|[0x80007cb8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000bb4]:fle.d t6, ft11, ft10<br> [0x80000bb8]:csrrs a7, fflags, zero<br> [0x80000bbc]:sw t6, 1648(a5)<br> |
| 112|[0x80007cc8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000bcc]:fle.d t6, ft11, ft10<br> [0x80000bd0]:csrrs a7, fflags, zero<br> [0x80000bd4]:sw t6, 1664(a5)<br> |
| 113|[0x80007cd8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000be4]:fle.d t6, ft11, ft10<br> [0x80000be8]:csrrs a7, fflags, zero<br> [0x80000bec]:sw t6, 1680(a5)<br> |
| 114|[0x80007ce8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000bfc]:fle.d t6, ft11, ft10<br> [0x80000c00]:csrrs a7, fflags, zero<br> [0x80000c04]:sw t6, 1696(a5)<br> |
| 115|[0x80007cf8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000c14]:fle.d t6, ft11, ft10<br> [0x80000c18]:csrrs a7, fflags, zero<br> [0x80000c1c]:sw t6, 1712(a5)<br> |
| 116|[0x80007d08]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000c2c]:fle.d t6, ft11, ft10<br> [0x80000c30]:csrrs a7, fflags, zero<br> [0x80000c34]:sw t6, 1728(a5)<br> |
| 117|[0x80007d18]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000c44]:fle.d t6, ft11, ft10<br> [0x80000c48]:csrrs a7, fflags, zero<br> [0x80000c4c]:sw t6, 1744(a5)<br> |
| 118|[0x80007d28]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000c5c]:fle.d t6, ft11, ft10<br> [0x80000c60]:csrrs a7, fflags, zero<br> [0x80000c64]:sw t6, 1760(a5)<br> |
| 119|[0x80007d38]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000c74]:fle.d t6, ft11, ft10<br> [0x80000c78]:csrrs a7, fflags, zero<br> [0x80000c7c]:sw t6, 1776(a5)<br> |
| 120|[0x80007d48]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000c8c]:fle.d t6, ft11, ft10<br> [0x80000c90]:csrrs a7, fflags, zero<br> [0x80000c94]:sw t6, 1792(a5)<br> |
| 121|[0x80007d58]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ca4]:fle.d t6, ft11, ft10<br> [0x80000ca8]:csrrs a7, fflags, zero<br> [0x80000cac]:sw t6, 1808(a5)<br> |
| 122|[0x80007d68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000cbc]:fle.d t6, ft11, ft10<br> [0x80000cc0]:csrrs a7, fflags, zero<br> [0x80000cc4]:sw t6, 1824(a5)<br> |
| 123|[0x80007d78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000cd4]:fle.d t6, ft11, ft10<br> [0x80000cd8]:csrrs a7, fflags, zero<br> [0x80000cdc]:sw t6, 1840(a5)<br> |
| 124|[0x80007d88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000cec]:fle.d t6, ft11, ft10<br> [0x80000cf0]:csrrs a7, fflags, zero<br> [0x80000cf4]:sw t6, 1856(a5)<br> |
| 125|[0x80007d98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d04]:fle.d t6, ft11, ft10<br> [0x80000d08]:csrrs a7, fflags, zero<br> [0x80000d0c]:sw t6, 1872(a5)<br> |
| 126|[0x80007da8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d1c]:fle.d t6, ft11, ft10<br> [0x80000d20]:csrrs a7, fflags, zero<br> [0x80000d24]:sw t6, 1888(a5)<br> |
| 127|[0x80007db8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d34]:fle.d t6, ft11, ft10<br> [0x80000d38]:csrrs a7, fflags, zero<br> [0x80000d3c]:sw t6, 1904(a5)<br> |
| 128|[0x80007dc8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d50]:fle.d t6, ft11, ft10<br> [0x80000d54]:csrrs a7, fflags, zero<br> [0x80000d58]:sw t6, 1920(a5)<br> |
| 129|[0x80007dd8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d68]:fle.d t6, ft11, ft10<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:sw t6, 1936(a5)<br> |
| 130|[0x80007de8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d80]:fle.d t6, ft11, ft10<br> [0x80000d84]:csrrs a7, fflags, zero<br> [0x80000d88]:sw t6, 1952(a5)<br> |
| 131|[0x80007df8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000d98]:fle.d t6, ft11, ft10<br> [0x80000d9c]:csrrs a7, fflags, zero<br> [0x80000da0]:sw t6, 1968(a5)<br> |
| 132|[0x80007e08]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000db0]:fle.d t6, ft11, ft10<br> [0x80000db4]:csrrs a7, fflags, zero<br> [0x80000db8]:sw t6, 1984(a5)<br> |
| 133|[0x80007e18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000dc8]:fle.d t6, ft11, ft10<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:sw t6, 2000(a5)<br> |
| 134|[0x80007e28]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000de0]:fle.d t6, ft11, ft10<br> [0x80000de4]:csrrs a7, fflags, zero<br> [0x80000de8]:sw t6, 2016(a5)<br> |
| 135|[0x80007a40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000e00]:fle.d t6, ft11, ft10<br> [0x80000e04]:csrrs a7, fflags, zero<br> [0x80000e08]:sw t6, 0(a5)<br>    |
| 136|[0x80007a50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000e18]:fle.d t6, ft11, ft10<br> [0x80000e1c]:csrrs a7, fflags, zero<br> [0x80000e20]:sw t6, 16(a5)<br>   |
| 137|[0x80007a60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000e30]:fle.d t6, ft11, ft10<br> [0x80000e34]:csrrs a7, fflags, zero<br> [0x80000e38]:sw t6, 32(a5)<br>   |
| 138|[0x80007a70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000e48]:fle.d t6, ft11, ft10<br> [0x80000e4c]:csrrs a7, fflags, zero<br> [0x80000e50]:sw t6, 48(a5)<br>   |
| 139|[0x80007a80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000e60]:fle.d t6, ft11, ft10<br> [0x80000e64]:csrrs a7, fflags, zero<br> [0x80000e68]:sw t6, 64(a5)<br>   |
| 140|[0x80007a90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000e78]:fle.d t6, ft11, ft10<br> [0x80000e7c]:csrrs a7, fflags, zero<br> [0x80000e80]:sw t6, 80(a5)<br>   |
| 141|[0x80007aa0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80000e90]:fle.d t6, ft11, ft10<br> [0x80000e94]:csrrs a7, fflags, zero<br> [0x80000e98]:sw t6, 96(a5)<br>   |
| 142|[0x80007ab0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ea8]:fle.d t6, ft11, ft10<br> [0x80000eac]:csrrs a7, fflags, zero<br> [0x80000eb0]:sw t6, 112(a5)<br>  |
| 143|[0x80007ac0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ec0]:fle.d t6, ft11, ft10<br> [0x80000ec4]:csrrs a7, fflags, zero<br> [0x80000ec8]:sw t6, 128(a5)<br>  |
| 144|[0x80007ad0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ed8]:fle.d t6, ft11, ft10<br> [0x80000edc]:csrrs a7, fflags, zero<br> [0x80000ee0]:sw t6, 144(a5)<br>  |
| 145|[0x80007ae0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000ef0]:fle.d t6, ft11, ft10<br> [0x80000ef4]:csrrs a7, fflags, zero<br> [0x80000ef8]:sw t6, 160(a5)<br>  |
| 146|[0x80007af0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f08]:fle.d t6, ft11, ft10<br> [0x80000f0c]:csrrs a7, fflags, zero<br> [0x80000f10]:sw t6, 176(a5)<br>  |
| 147|[0x80007b00]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f20]:fle.d t6, ft11, ft10<br> [0x80000f24]:csrrs a7, fflags, zero<br> [0x80000f28]:sw t6, 192(a5)<br>  |
| 148|[0x80007b10]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f38]:fle.d t6, ft11, ft10<br> [0x80000f3c]:csrrs a7, fflags, zero<br> [0x80000f40]:sw t6, 208(a5)<br>  |
| 149|[0x80007b20]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f50]:fle.d t6, ft11, ft10<br> [0x80000f54]:csrrs a7, fflags, zero<br> [0x80000f58]:sw t6, 224(a5)<br>  |
| 150|[0x80007b30]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f68]:fle.d t6, ft11, ft10<br> [0x80000f6c]:csrrs a7, fflags, zero<br> [0x80000f70]:sw t6, 240(a5)<br>  |
| 151|[0x80007b40]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f80]:fle.d t6, ft11, ft10<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:sw t6, 256(a5)<br>  |
| 152|[0x80007b50]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000f98]:fle.d t6, ft11, ft10<br> [0x80000f9c]:csrrs a7, fflags, zero<br> [0x80000fa0]:sw t6, 272(a5)<br>  |
| 153|[0x80007b60]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000fb0]:fle.d t6, ft11, ft10<br> [0x80000fb4]:csrrs a7, fflags, zero<br> [0x80000fb8]:sw t6, 288(a5)<br>  |
| 154|[0x80007b70]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000fc8]:fle.d t6, ft11, ft10<br> [0x80000fcc]:csrrs a7, fflags, zero<br> [0x80000fd0]:sw t6, 304(a5)<br>  |
| 155|[0x80007b80]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80000fe0]:fle.d t6, ft11, ft10<br> [0x80000fe4]:csrrs a7, fflags, zero<br> [0x80000fe8]:sw t6, 320(a5)<br>  |
| 156|[0x80007b90]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80000ff8]:fle.d t6, ft11, ft10<br> [0x80000ffc]:csrrs a7, fflags, zero<br> [0x80001000]:sw t6, 336(a5)<br>  |
| 157|[0x80007ba0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001010]:fle.d t6, ft11, ft10<br> [0x80001014]:csrrs a7, fflags, zero<br> [0x80001018]:sw t6, 352(a5)<br>  |
| 158|[0x80007bb0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001028]:fle.d t6, ft11, ft10<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:sw t6, 368(a5)<br>  |
| 159|[0x80007bc0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001040]:fle.d t6, ft11, ft10<br> [0x80001044]:csrrs a7, fflags, zero<br> [0x80001048]:sw t6, 384(a5)<br>  |
| 160|[0x80007bd0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001058]:fle.d t6, ft11, ft10<br> [0x8000105c]:csrrs a7, fflags, zero<br> [0x80001060]:sw t6, 400(a5)<br>  |
| 161|[0x80007be0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001070]:fle.d t6, ft11, ft10<br> [0x80001074]:csrrs a7, fflags, zero<br> [0x80001078]:sw t6, 416(a5)<br>  |
| 162|[0x80007bf0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001088]:fle.d t6, ft11, ft10<br> [0x8000108c]:csrrs a7, fflags, zero<br> [0x80001090]:sw t6, 432(a5)<br>  |
| 163|[0x80007c00]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800010a0]:fle.d t6, ft11, ft10<br> [0x800010a4]:csrrs a7, fflags, zero<br> [0x800010a8]:sw t6, 448(a5)<br>  |
| 164|[0x80007c10]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800010b8]:fle.d t6, ft11, ft10<br> [0x800010bc]:csrrs a7, fflags, zero<br> [0x800010c0]:sw t6, 464(a5)<br>  |
| 165|[0x80007c20]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800010d0]:fle.d t6, ft11, ft10<br> [0x800010d4]:csrrs a7, fflags, zero<br> [0x800010d8]:sw t6, 480(a5)<br>  |
| 166|[0x80007c30]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800010e8]:fle.d t6, ft11, ft10<br> [0x800010ec]:csrrs a7, fflags, zero<br> [0x800010f0]:sw t6, 496(a5)<br>  |
| 167|[0x80007c40]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001100]:fle.d t6, ft11, ft10<br> [0x80001104]:csrrs a7, fflags, zero<br> [0x80001108]:sw t6, 512(a5)<br>  |
| 168|[0x80007c50]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001118]:fle.d t6, ft11, ft10<br> [0x8000111c]:csrrs a7, fflags, zero<br> [0x80001120]:sw t6, 528(a5)<br>  |
| 169|[0x80007c60]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001130]:fle.d t6, ft11, ft10<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:sw t6, 544(a5)<br>  |
| 170|[0x80007c70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001148]:fle.d t6, ft11, ft10<br> [0x8000114c]:csrrs a7, fflags, zero<br> [0x80001150]:sw t6, 560(a5)<br>  |
| 171|[0x80007c80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001160]:fle.d t6, ft11, ft10<br> [0x80001164]:csrrs a7, fflags, zero<br> [0x80001168]:sw t6, 576(a5)<br>  |
| 172|[0x80007c90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001178]:fle.d t6, ft11, ft10<br> [0x8000117c]:csrrs a7, fflags, zero<br> [0x80001180]:sw t6, 592(a5)<br>  |
| 173|[0x80007ca0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001190]:fle.d t6, ft11, ft10<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:sw t6, 608(a5)<br>  |
| 174|[0x80007cb0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800011a8]:fle.d t6, ft11, ft10<br> [0x800011ac]:csrrs a7, fflags, zero<br> [0x800011b0]:sw t6, 624(a5)<br>  |
| 175|[0x80007cc0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800011c0]:fle.d t6, ft11, ft10<br> [0x800011c4]:csrrs a7, fflags, zero<br> [0x800011c8]:sw t6, 640(a5)<br>  |
| 176|[0x80007cd0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800011d8]:fle.d t6, ft11, ft10<br> [0x800011dc]:csrrs a7, fflags, zero<br> [0x800011e0]:sw t6, 656(a5)<br>  |
| 177|[0x80007ce0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800011f0]:fle.d t6, ft11, ft10<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:sw t6, 672(a5)<br>  |
| 178|[0x80007cf0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001208]:fle.d t6, ft11, ft10<br> [0x8000120c]:csrrs a7, fflags, zero<br> [0x80001210]:sw t6, 688(a5)<br>  |
| 179|[0x80007d00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001220]:fle.d t6, ft11, ft10<br> [0x80001224]:csrrs a7, fflags, zero<br> [0x80001228]:sw t6, 704(a5)<br>  |
| 180|[0x80007d10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001238]:fle.d t6, ft11, ft10<br> [0x8000123c]:csrrs a7, fflags, zero<br> [0x80001240]:sw t6, 720(a5)<br>  |
| 181|[0x80007d20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001250]:fle.d t6, ft11, ft10<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:sw t6, 736(a5)<br>  |
| 182|[0x80007d30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001268]:fle.d t6, ft11, ft10<br> [0x8000126c]:csrrs a7, fflags, zero<br> [0x80001270]:sw t6, 752(a5)<br>  |
| 183|[0x80007d40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001280]:fle.d t6, ft11, ft10<br> [0x80001284]:csrrs a7, fflags, zero<br> [0x80001288]:sw t6, 768(a5)<br>  |
| 184|[0x80007d50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001298]:fle.d t6, ft11, ft10<br> [0x8000129c]:csrrs a7, fflags, zero<br> [0x800012a0]:sw t6, 784(a5)<br>  |
| 185|[0x80007d60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800012b0]:fle.d t6, ft11, ft10<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:sw t6, 800(a5)<br>  |
| 186|[0x80007d70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800012c8]:fle.d t6, ft11, ft10<br> [0x800012cc]:csrrs a7, fflags, zero<br> [0x800012d0]:sw t6, 816(a5)<br>  |
| 187|[0x80007d80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800012e0]:fle.d t6, ft11, ft10<br> [0x800012e4]:csrrs a7, fflags, zero<br> [0x800012e8]:sw t6, 832(a5)<br>  |
| 188|[0x80007d90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800012f8]:fle.d t6, ft11, ft10<br> [0x800012fc]:csrrs a7, fflags, zero<br> [0x80001300]:sw t6, 848(a5)<br>  |
| 189|[0x80007da0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001310]:fle.d t6, ft11, ft10<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:sw t6, 864(a5)<br>  |
| 190|[0x80007db0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001328]:fle.d t6, ft11, ft10<br> [0x8000132c]:csrrs a7, fflags, zero<br> [0x80001330]:sw t6, 880(a5)<br>  |
| 191|[0x80007dc0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001340]:fle.d t6, ft11, ft10<br> [0x80001344]:csrrs a7, fflags, zero<br> [0x80001348]:sw t6, 896(a5)<br>  |
| 192|[0x80007dd0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001358]:fle.d t6, ft11, ft10<br> [0x8000135c]:csrrs a7, fflags, zero<br> [0x80001360]:sw t6, 912(a5)<br>  |
| 193|[0x80007de0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001370]:fle.d t6, ft11, ft10<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:sw t6, 928(a5)<br>  |
| 194|[0x80007df0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001388]:fle.d t6, ft11, ft10<br> [0x8000138c]:csrrs a7, fflags, zero<br> [0x80001390]:sw t6, 944(a5)<br>  |
| 195|[0x80007e00]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800013a0]:fle.d t6, ft11, ft10<br> [0x800013a4]:csrrs a7, fflags, zero<br> [0x800013a8]:sw t6, 960(a5)<br>  |
| 196|[0x80007e10]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800013b8]:fle.d t6, ft11, ft10<br> [0x800013bc]:csrrs a7, fflags, zero<br> [0x800013c0]:sw t6, 976(a5)<br>  |
| 197|[0x80007e20]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800013d0]:fle.d t6, ft11, ft10<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:sw t6, 992(a5)<br>  |
| 198|[0x80007e30]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800013e8]:fle.d t6, ft11, ft10<br> [0x800013ec]:csrrs a7, fflags, zero<br> [0x800013f0]:sw t6, 1008(a5)<br> |
| 199|[0x80007e40]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001400]:fle.d t6, ft11, ft10<br> [0x80001404]:csrrs a7, fflags, zero<br> [0x80001408]:sw t6, 1024(a5)<br> |
| 200|[0x80007e50]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001418]:fle.d t6, ft11, ft10<br> [0x8000141c]:csrrs a7, fflags, zero<br> [0x80001420]:sw t6, 1040(a5)<br> |
| 201|[0x80007e60]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001430]:fle.d t6, ft11, ft10<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:sw t6, 1056(a5)<br> |
| 202|[0x80007e70]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001448]:fle.d t6, ft11, ft10<br> [0x8000144c]:csrrs a7, fflags, zero<br> [0x80001450]:sw t6, 1072(a5)<br> |
| 203|[0x80007e80]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001460]:fle.d t6, ft11, ft10<br> [0x80001464]:csrrs a7, fflags, zero<br> [0x80001468]:sw t6, 1088(a5)<br> |
| 204|[0x80007e90]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001478]:fle.d t6, ft11, ft10<br> [0x8000147c]:csrrs a7, fflags, zero<br> [0x80001480]:sw t6, 1104(a5)<br> |
| 205|[0x80007ea0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001490]:fle.d t6, ft11, ft10<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:sw t6, 1120(a5)<br> |
| 206|[0x80007eb0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800014a8]:fle.d t6, ft11, ft10<br> [0x800014ac]:csrrs a7, fflags, zero<br> [0x800014b0]:sw t6, 1136(a5)<br> |
| 207|[0x80007ec0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800014c0]:fle.d t6, ft11, ft10<br> [0x800014c4]:csrrs a7, fflags, zero<br> [0x800014c8]:sw t6, 1152(a5)<br> |
| 208|[0x80007ed0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800014d8]:fle.d t6, ft11, ft10<br> [0x800014dc]:csrrs a7, fflags, zero<br> [0x800014e0]:sw t6, 1168(a5)<br> |
| 209|[0x80007ee0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800014f0]:fle.d t6, ft11, ft10<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:sw t6, 1184(a5)<br> |
| 210|[0x80007ef0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001508]:fle.d t6, ft11, ft10<br> [0x8000150c]:csrrs a7, fflags, zero<br> [0x80001510]:sw t6, 1200(a5)<br> |
| 211|[0x80007f00]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001520]:fle.d t6, ft11, ft10<br> [0x80001524]:csrrs a7, fflags, zero<br> [0x80001528]:sw t6, 1216(a5)<br> |
| 212|[0x80007f10]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001538]:fle.d t6, ft11, ft10<br> [0x8000153c]:csrrs a7, fflags, zero<br> [0x80001540]:sw t6, 1232(a5)<br> |
| 213|[0x80007f20]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001550]:fle.d t6, ft11, ft10<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:sw t6, 1248(a5)<br> |
| 214|[0x80007f30]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001568]:fle.d t6, ft11, ft10<br> [0x8000156c]:csrrs a7, fflags, zero<br> [0x80001570]:sw t6, 1264(a5)<br> |
| 215|[0x80007f40]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001580]:fle.d t6, ft11, ft10<br> [0x80001584]:csrrs a7, fflags, zero<br> [0x80001588]:sw t6, 1280(a5)<br> |
| 216|[0x80007f50]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001598]:fle.d t6, ft11, ft10<br> [0x8000159c]:csrrs a7, fflags, zero<br> [0x800015a0]:sw t6, 1296(a5)<br> |
| 217|[0x80007f60]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800015b0]:fle.d t6, ft11, ft10<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:sw t6, 1312(a5)<br> |
| 218|[0x80007f70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800015c8]:fle.d t6, ft11, ft10<br> [0x800015cc]:csrrs a7, fflags, zero<br> [0x800015d0]:sw t6, 1328(a5)<br> |
| 219|[0x80007f80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800015e0]:fle.d t6, ft11, ft10<br> [0x800015e4]:csrrs a7, fflags, zero<br> [0x800015e8]:sw t6, 1344(a5)<br> |
| 220|[0x80007f90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800015f8]:fle.d t6, ft11, ft10<br> [0x800015fc]:csrrs a7, fflags, zero<br> [0x80001600]:sw t6, 1360(a5)<br> |
| 221|[0x80007fa0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001610]:fle.d t6, ft11, ft10<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:sw t6, 1376(a5)<br> |
| 222|[0x80007fb0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001628]:fle.d t6, ft11, ft10<br> [0x8000162c]:csrrs a7, fflags, zero<br> [0x80001630]:sw t6, 1392(a5)<br> |
| 223|[0x80007fc0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001640]:fle.d t6, ft11, ft10<br> [0x80001644]:csrrs a7, fflags, zero<br> [0x80001648]:sw t6, 1408(a5)<br> |
| 224|[0x80007fd0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001658]:fle.d t6, ft11, ft10<br> [0x8000165c]:csrrs a7, fflags, zero<br> [0x80001660]:sw t6, 1424(a5)<br> |
| 225|[0x80007fe0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001670]:fle.d t6, ft11, ft10<br> [0x80001674]:csrrs a7, fflags, zero<br> [0x80001678]:sw t6, 1440(a5)<br> |
| 226|[0x80007ff0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001688]:fle.d t6, ft11, ft10<br> [0x8000168c]:csrrs a7, fflags, zero<br> [0x80001690]:sw t6, 1456(a5)<br> |
| 227|[0x80008000]<br>0x00000001|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800016a0]:fle.d t6, ft11, ft10<br> [0x800016a4]:csrrs a7, fflags, zero<br> [0x800016a8]:sw t6, 1472(a5)<br> |
| 228|[0x80008010]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800016b8]:fle.d t6, ft11, ft10<br> [0x800016bc]:csrrs a7, fflags, zero<br> [0x800016c0]:sw t6, 1488(a5)<br> |
| 229|[0x80008020]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800016d0]:fle.d t6, ft11, ft10<br> [0x800016d4]:csrrs a7, fflags, zero<br> [0x800016d8]:sw t6, 1504(a5)<br> |
| 230|[0x80008030]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800016e8]:fle.d t6, ft11, ft10<br> [0x800016ec]:csrrs a7, fflags, zero<br> [0x800016f0]:sw t6, 1520(a5)<br> |
| 231|[0x80008040]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001700]:fle.d t6, ft11, ft10<br> [0x80001704]:csrrs a7, fflags, zero<br> [0x80001708]:sw t6, 1536(a5)<br> |
| 232|[0x80008050]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001718]:fle.d t6, ft11, ft10<br> [0x8000171c]:csrrs a7, fflags, zero<br> [0x80001720]:sw t6, 1552(a5)<br> |
| 233|[0x80008060]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001730]:fle.d t6, ft11, ft10<br> [0x80001734]:csrrs a7, fflags, zero<br> [0x80001738]:sw t6, 1568(a5)<br> |
| 234|[0x80008070]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001748]:fle.d t6, ft11, ft10<br> [0x8000174c]:csrrs a7, fflags, zero<br> [0x80001750]:sw t6, 1584(a5)<br> |
| 235|[0x80008080]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001760]:fle.d t6, ft11, ft10<br> [0x80001764]:csrrs a7, fflags, zero<br> [0x80001768]:sw t6, 1600(a5)<br> |
| 236|[0x80008090]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001778]:fle.d t6, ft11, ft10<br> [0x8000177c]:csrrs a7, fflags, zero<br> [0x80001780]:sw t6, 1616(a5)<br> |
| 237|[0x800080a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001790]:fle.d t6, ft11, ft10<br> [0x80001794]:csrrs a7, fflags, zero<br> [0x80001798]:sw t6, 1632(a5)<br> |
| 238|[0x800080b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800017a8]:fle.d t6, ft11, ft10<br> [0x800017ac]:csrrs a7, fflags, zero<br> [0x800017b0]:sw t6, 1648(a5)<br> |
| 239|[0x800080c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800017c0]:fle.d t6, ft11, ft10<br> [0x800017c4]:csrrs a7, fflags, zero<br> [0x800017c8]:sw t6, 1664(a5)<br> |
| 240|[0x800080d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800017d8]:fle.d t6, ft11, ft10<br> [0x800017dc]:csrrs a7, fflags, zero<br> [0x800017e0]:sw t6, 1680(a5)<br> |
| 241|[0x800080e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800017f0]:fle.d t6, ft11, ft10<br> [0x800017f4]:csrrs a7, fflags, zero<br> [0x800017f8]:sw t6, 1696(a5)<br> |
| 242|[0x800080f0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001808]:fle.d t6, ft11, ft10<br> [0x8000180c]:csrrs a7, fflags, zero<br> [0x80001810]:sw t6, 1712(a5)<br> |
| 243|[0x80008100]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001820]:fle.d t6, ft11, ft10<br> [0x80001824]:csrrs a7, fflags, zero<br> [0x80001828]:sw t6, 1728(a5)<br> |
| 244|[0x80008110]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001838]:fle.d t6, ft11, ft10<br> [0x8000183c]:csrrs a7, fflags, zero<br> [0x80001840]:sw t6, 1744(a5)<br> |
| 245|[0x80008120]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001850]:fle.d t6, ft11, ft10<br> [0x80001854]:csrrs a7, fflags, zero<br> [0x80001858]:sw t6, 1760(a5)<br> |
| 246|[0x80008130]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001868]:fle.d t6, ft11, ft10<br> [0x8000186c]:csrrs a7, fflags, zero<br> [0x80001870]:sw t6, 1776(a5)<br> |
| 247|[0x80008140]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001880]:fle.d t6, ft11, ft10<br> [0x80001884]:csrrs a7, fflags, zero<br> [0x80001888]:sw t6, 1792(a5)<br> |
| 248|[0x80008150]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001898]:fle.d t6, ft11, ft10<br> [0x8000189c]:csrrs a7, fflags, zero<br> [0x800018a0]:sw t6, 1808(a5)<br> |
| 249|[0x80008160]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800018b0]:fle.d t6, ft11, ft10<br> [0x800018b4]:csrrs a7, fflags, zero<br> [0x800018b8]:sw t6, 1824(a5)<br> |
| 250|[0x80008170]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800018c8]:fle.d t6, ft11, ft10<br> [0x800018cc]:csrrs a7, fflags, zero<br> [0x800018d0]:sw t6, 1840(a5)<br> |
| 251|[0x80008180]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800018e0]:fle.d t6, ft11, ft10<br> [0x800018e4]:csrrs a7, fflags, zero<br> [0x800018e8]:sw t6, 1856(a5)<br> |
| 252|[0x80008190]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800018f8]:fle.d t6, ft11, ft10<br> [0x800018fc]:csrrs a7, fflags, zero<br> [0x80001900]:sw t6, 1872(a5)<br> |
| 253|[0x800081a0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001910]:fle.d t6, ft11, ft10<br> [0x80001914]:csrrs a7, fflags, zero<br> [0x80001918]:sw t6, 1888(a5)<br> |
| 254|[0x800081b0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001928]:fle.d t6, ft11, ft10<br> [0x8000192c]:csrrs a7, fflags, zero<br> [0x80001930]:sw t6, 1904(a5)<br> |
| 255|[0x800081c0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001944]:fle.d t6, ft11, ft10<br> [0x80001948]:csrrs a7, fflags, zero<br> [0x8000194c]:sw t6, 1920(a5)<br> |
| 256|[0x800081d0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000195c]:fle.d t6, ft11, ft10<br> [0x80001960]:csrrs a7, fflags, zero<br> [0x80001964]:sw t6, 1936(a5)<br> |
| 257|[0x800081e0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001974]:fle.d t6, ft11, ft10<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:sw t6, 1952(a5)<br> |
| 258|[0x800081f0]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000198c]:fle.d t6, ft11, ft10<br> [0x80001990]:csrrs a7, fflags, zero<br> [0x80001994]:sw t6, 1968(a5)<br> |
| 259|[0x80008200]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800019a4]:fle.d t6, ft11, ft10<br> [0x800019a8]:csrrs a7, fflags, zero<br> [0x800019ac]:sw t6, 1984(a5)<br> |
| 260|[0x80008210]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800019bc]:fle.d t6, ft11, ft10<br> [0x800019c0]:csrrs a7, fflags, zero<br> [0x800019c4]:sw t6, 2000(a5)<br> |
| 261|[0x80008220]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800019d4]:fle.d t6, ft11, ft10<br> [0x800019d8]:csrrs a7, fflags, zero<br> [0x800019dc]:sw t6, 2016(a5)<br> |
| 262|[0x80007e38]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800019f4]:fle.d t6, ft11, ft10<br> [0x800019f8]:csrrs a7, fflags, zero<br> [0x800019fc]:sw t6, 0(a5)<br>    |
| 263|[0x80007e48]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a0c]:fle.d t6, ft11, ft10<br> [0x80001a10]:csrrs a7, fflags, zero<br> [0x80001a14]:sw t6, 16(a5)<br>   |
| 264|[0x80007e58]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a24]:fle.d t6, ft11, ft10<br> [0x80001a28]:csrrs a7, fflags, zero<br> [0x80001a2c]:sw t6, 32(a5)<br>   |
| 265|[0x80007e68]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a3c]:fle.d t6, ft11, ft10<br> [0x80001a40]:csrrs a7, fflags, zero<br> [0x80001a44]:sw t6, 48(a5)<br>   |
| 266|[0x80007e78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a54]:fle.d t6, ft11, ft10<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:sw t6, 64(a5)<br>   |
| 267|[0x80007e88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a6c]:fle.d t6, ft11, ft10<br> [0x80001a70]:csrrs a7, fflags, zero<br> [0x80001a74]:sw t6, 80(a5)<br>   |
| 268|[0x80007e98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a84]:fle.d t6, ft11, ft10<br> [0x80001a88]:csrrs a7, fflags, zero<br> [0x80001a8c]:sw t6, 96(a5)<br>   |
| 269|[0x80007ea8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001a9c]:fle.d t6, ft11, ft10<br> [0x80001aa0]:csrrs a7, fflags, zero<br> [0x80001aa4]:sw t6, 112(a5)<br>  |
| 270|[0x80007eb8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ab4]:fle.d t6, ft11, ft10<br> [0x80001ab8]:csrrs a7, fflags, zero<br> [0x80001abc]:sw t6, 128(a5)<br>  |
| 271|[0x80007ec8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001acc]:fle.d t6, ft11, ft10<br> [0x80001ad0]:csrrs a7, fflags, zero<br> [0x80001ad4]:sw t6, 144(a5)<br>  |
| 272|[0x80007ed8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ae4]:fle.d t6, ft11, ft10<br> [0x80001ae8]:csrrs a7, fflags, zero<br> [0x80001aec]:sw t6, 160(a5)<br>  |
| 273|[0x80007ee8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001afc]:fle.d t6, ft11, ft10<br> [0x80001b00]:csrrs a7, fflags, zero<br> [0x80001b04]:sw t6, 176(a5)<br>  |
| 274|[0x80007ef8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001b14]:fle.d t6, ft11, ft10<br> [0x80001b18]:csrrs a7, fflags, zero<br> [0x80001b1c]:sw t6, 192(a5)<br>  |
| 275|[0x80007f08]<br>0x00000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001b2c]:fle.d t6, ft11, ft10<br> [0x80001b30]:csrrs a7, fflags, zero<br> [0x80001b34]:sw t6, 208(a5)<br>  |
| 276|[0x80007f18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001b44]:fle.d t6, ft11, ft10<br> [0x80001b48]:csrrs a7, fflags, zero<br> [0x80001b4c]:sw t6, 224(a5)<br>  |
| 277|[0x80007f28]<br>0x00000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001b5c]:fle.d t6, ft11, ft10<br> [0x80001b60]:csrrs a7, fflags, zero<br> [0x80001b64]:sw t6, 240(a5)<br>  |
| 278|[0x80007f38]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001b74]:fle.d t6, ft11, ft10<br> [0x80001b78]:csrrs a7, fflags, zero<br> [0x80001b7c]:sw t6, 256(a5)<br>  |
| 279|[0x80007f48]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001b8c]:fle.d t6, ft11, ft10<br> [0x80001b90]:csrrs a7, fflags, zero<br> [0x80001b94]:sw t6, 272(a5)<br>  |
| 280|[0x80007f58]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ba4]:fle.d t6, ft11, ft10<br> [0x80001ba8]:csrrs a7, fflags, zero<br> [0x80001bac]:sw t6, 288(a5)<br>  |
| 281|[0x80007f68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001bbc]:fle.d t6, ft11, ft10<br> [0x80001bc0]:csrrs a7, fflags, zero<br> [0x80001bc4]:sw t6, 304(a5)<br>  |
| 282|[0x80007f78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001bd4]:fle.d t6, ft11, ft10<br> [0x80001bd8]:csrrs a7, fflags, zero<br> [0x80001bdc]:sw t6, 320(a5)<br>  |
| 283|[0x80007f88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001bec]:fle.d t6, ft11, ft10<br> [0x80001bf0]:csrrs a7, fflags, zero<br> [0x80001bf4]:sw t6, 336(a5)<br>  |
| 284|[0x80007f98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c04]:fle.d t6, ft11, ft10<br> [0x80001c08]:csrrs a7, fflags, zero<br> [0x80001c0c]:sw t6, 352(a5)<br>  |
| 285|[0x80007fa8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c1c]:fle.d t6, ft11, ft10<br> [0x80001c20]:csrrs a7, fflags, zero<br> [0x80001c24]:sw t6, 368(a5)<br>  |
| 286|[0x80007fb8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c34]:fle.d t6, ft11, ft10<br> [0x80001c38]:csrrs a7, fflags, zero<br> [0x80001c3c]:sw t6, 384(a5)<br>  |
| 287|[0x80007fc8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c4c]:fle.d t6, ft11, ft10<br> [0x80001c50]:csrrs a7, fflags, zero<br> [0x80001c54]:sw t6, 400(a5)<br>  |
| 288|[0x80007fd8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c64]:fle.d t6, ft11, ft10<br> [0x80001c68]:csrrs a7, fflags, zero<br> [0x80001c6c]:sw t6, 416(a5)<br>  |
| 289|[0x80007fe8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c7c]:fle.d t6, ft11, ft10<br> [0x80001c80]:csrrs a7, fflags, zero<br> [0x80001c84]:sw t6, 432(a5)<br>  |
| 290|[0x80007ff8]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001c94]:fle.d t6, ft11, ft10<br> [0x80001c98]:csrrs a7, fflags, zero<br> [0x80001c9c]:sw t6, 448(a5)<br>  |
| 291|[0x80008008]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001cac]:fle.d t6, ft11, ft10<br> [0x80001cb0]:csrrs a7, fflags, zero<br> [0x80001cb4]:sw t6, 464(a5)<br>  |
| 292|[0x80008018]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001cc4]:fle.d t6, ft11, ft10<br> [0x80001cc8]:csrrs a7, fflags, zero<br> [0x80001ccc]:sw t6, 480(a5)<br>  |
| 293|[0x80008028]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001cdc]:fle.d t6, ft11, ft10<br> [0x80001ce0]:csrrs a7, fflags, zero<br> [0x80001ce4]:sw t6, 496(a5)<br>  |
| 294|[0x80008038]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001cf4]:fle.d t6, ft11, ft10<br> [0x80001cf8]:csrrs a7, fflags, zero<br> [0x80001cfc]:sw t6, 512(a5)<br>  |
| 295|[0x80008048]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001d0c]:fle.d t6, ft11, ft10<br> [0x80001d10]:csrrs a7, fflags, zero<br> [0x80001d14]:sw t6, 528(a5)<br>  |
| 296|[0x80008058]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001d24]:fle.d t6, ft11, ft10<br> [0x80001d28]:csrrs a7, fflags, zero<br> [0x80001d2c]:sw t6, 544(a5)<br>  |
| 297|[0x80008068]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001d3c]:fle.d t6, ft11, ft10<br> [0x80001d40]:csrrs a7, fflags, zero<br> [0x80001d44]:sw t6, 560(a5)<br>  |
| 298|[0x80008078]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001d54]:fle.d t6, ft11, ft10<br> [0x80001d58]:csrrs a7, fflags, zero<br> [0x80001d5c]:sw t6, 576(a5)<br>  |
| 299|[0x80008088]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001d6c]:fle.d t6, ft11, ft10<br> [0x80001d70]:csrrs a7, fflags, zero<br> [0x80001d74]:sw t6, 592(a5)<br>  |
| 300|[0x80008098]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001d84]:fle.d t6, ft11, ft10<br> [0x80001d88]:csrrs a7, fflags, zero<br> [0x80001d8c]:sw t6, 608(a5)<br>  |
| 301|[0x800080a8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001d9c]:fle.d t6, ft11, ft10<br> [0x80001da0]:csrrs a7, fflags, zero<br> [0x80001da4]:sw t6, 624(a5)<br>  |
| 302|[0x800080b8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001db4]:fle.d t6, ft11, ft10<br> [0x80001db8]:csrrs a7, fflags, zero<br> [0x80001dbc]:sw t6, 640(a5)<br>  |
| 303|[0x800080c8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001dcc]:fle.d t6, ft11, ft10<br> [0x80001dd0]:csrrs a7, fflags, zero<br> [0x80001dd4]:sw t6, 656(a5)<br>  |
| 304|[0x800080d8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001de4]:fle.d t6, ft11, ft10<br> [0x80001de8]:csrrs a7, fflags, zero<br> [0x80001dec]:sw t6, 672(a5)<br>  |
| 305|[0x800080e8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001dfc]:fle.d t6, ft11, ft10<br> [0x80001e00]:csrrs a7, fflags, zero<br> [0x80001e04]:sw t6, 688(a5)<br>  |
| 306|[0x800080f8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001e14]:fle.d t6, ft11, ft10<br> [0x80001e18]:csrrs a7, fflags, zero<br> [0x80001e1c]:sw t6, 704(a5)<br>  |
| 307|[0x80008108]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001e2c]:fle.d t6, ft11, ft10<br> [0x80001e30]:csrrs a7, fflags, zero<br> [0x80001e34]:sw t6, 720(a5)<br>  |
| 308|[0x80008118]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001e44]:fle.d t6, ft11, ft10<br> [0x80001e48]:csrrs a7, fflags, zero<br> [0x80001e4c]:sw t6, 736(a5)<br>  |
| 309|[0x80008128]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001e5c]:fle.d t6, ft11, ft10<br> [0x80001e60]:csrrs a7, fflags, zero<br> [0x80001e64]:sw t6, 752(a5)<br>  |
| 310|[0x80008138]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001e74]:fle.d t6, ft11, ft10<br> [0x80001e78]:csrrs a7, fflags, zero<br> [0x80001e7c]:sw t6, 768(a5)<br>  |
| 311|[0x80008148]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001e8c]:fle.d t6, ft11, ft10<br> [0x80001e90]:csrrs a7, fflags, zero<br> [0x80001e94]:sw t6, 784(a5)<br>  |
| 312|[0x80008158]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ea4]:fle.d t6, ft11, ft10<br> [0x80001ea8]:csrrs a7, fflags, zero<br> [0x80001eac]:sw t6, 800(a5)<br>  |
| 313|[0x80008168]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ebc]:fle.d t6, ft11, ft10<br> [0x80001ec0]:csrrs a7, fflags, zero<br> [0x80001ec4]:sw t6, 816(a5)<br>  |
| 314|[0x80008178]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ed4]:fle.d t6, ft11, ft10<br> [0x80001ed8]:csrrs a7, fflags, zero<br> [0x80001edc]:sw t6, 832(a5)<br>  |
| 315|[0x80008188]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001eec]:fle.d t6, ft11, ft10<br> [0x80001ef0]:csrrs a7, fflags, zero<br> [0x80001ef4]:sw t6, 848(a5)<br>  |
| 316|[0x80008198]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f04]:fle.d t6, ft11, ft10<br> [0x80001f08]:csrrs a7, fflags, zero<br> [0x80001f0c]:sw t6, 864(a5)<br>  |
| 317|[0x800081a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f1c]:fle.d t6, ft11, ft10<br> [0x80001f20]:csrrs a7, fflags, zero<br> [0x80001f24]:sw t6, 880(a5)<br>  |
| 318|[0x800081b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f34]:fle.d t6, ft11, ft10<br> [0x80001f38]:csrrs a7, fflags, zero<br> [0x80001f3c]:sw t6, 896(a5)<br>  |
| 319|[0x800081c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f4c]:fle.d t6, ft11, ft10<br> [0x80001f50]:csrrs a7, fflags, zero<br> [0x80001f54]:sw t6, 912(a5)<br>  |
| 320|[0x800081d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f64]:fle.d t6, ft11, ft10<br> [0x80001f68]:csrrs a7, fflags, zero<br> [0x80001f6c]:sw t6, 928(a5)<br>  |
| 321|[0x800081e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f7c]:fle.d t6, ft11, ft10<br> [0x80001f80]:csrrs a7, fflags, zero<br> [0x80001f84]:sw t6, 944(a5)<br>  |
| 322|[0x800081f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001f94]:fle.d t6, ft11, ft10<br> [0x80001f98]:csrrs a7, fflags, zero<br> [0x80001f9c]:sw t6, 960(a5)<br>  |
| 323|[0x80008208]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80001fac]:fle.d t6, ft11, ft10<br> [0x80001fb0]:csrrs a7, fflags, zero<br> [0x80001fb4]:sw t6, 976(a5)<br>  |
| 324|[0x80008218]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001fc4]:fle.d t6, ft11, ft10<br> [0x80001fc8]:csrrs a7, fflags, zero<br> [0x80001fcc]:sw t6, 992(a5)<br>  |
| 325|[0x80008228]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80001fdc]:fle.d t6, ft11, ft10<br> [0x80001fe0]:csrrs a7, fflags, zero<br> [0x80001fe4]:sw t6, 1008(a5)<br> |
| 326|[0x80008238]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80001ff4]:fle.d t6, ft11, ft10<br> [0x80001ff8]:csrrs a7, fflags, zero<br> [0x80001ffc]:sw t6, 1024(a5)<br> |
| 327|[0x80008248]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000200c]:fle.d t6, ft11, ft10<br> [0x80002010]:csrrs a7, fflags, zero<br> [0x80002014]:sw t6, 1040(a5)<br> |
| 328|[0x80008258]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002024]:fle.d t6, ft11, ft10<br> [0x80002028]:csrrs a7, fflags, zero<br> [0x8000202c]:sw t6, 1056(a5)<br> |
| 329|[0x80008268]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000203c]:fle.d t6, ft11, ft10<br> [0x80002040]:csrrs a7, fflags, zero<br> [0x80002044]:sw t6, 1072(a5)<br> |
| 330|[0x80008278]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002054]:fle.d t6, ft11, ft10<br> [0x80002058]:csrrs a7, fflags, zero<br> [0x8000205c]:sw t6, 1088(a5)<br> |
| 331|[0x80008288]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000206c]:fle.d t6, ft11, ft10<br> [0x80002070]:csrrs a7, fflags, zero<br> [0x80002074]:sw t6, 1104(a5)<br> |
| 332|[0x80008298]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002084]:fle.d t6, ft11, ft10<br> [0x80002088]:csrrs a7, fflags, zero<br> [0x8000208c]:sw t6, 1120(a5)<br> |
| 333|[0x800082a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000209c]:fle.d t6, ft11, ft10<br> [0x800020a0]:csrrs a7, fflags, zero<br> [0x800020a4]:sw t6, 1136(a5)<br> |
| 334|[0x800082b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800020b4]:fle.d t6, ft11, ft10<br> [0x800020b8]:csrrs a7, fflags, zero<br> [0x800020bc]:sw t6, 1152(a5)<br> |
| 335|[0x800082c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800020cc]:fle.d t6, ft11, ft10<br> [0x800020d0]:csrrs a7, fflags, zero<br> [0x800020d4]:sw t6, 1168(a5)<br> |
| 336|[0x800082d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800020e4]:fle.d t6, ft11, ft10<br> [0x800020e8]:csrrs a7, fflags, zero<br> [0x800020ec]:sw t6, 1184(a5)<br> |
| 337|[0x800082e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800020fc]:fle.d t6, ft11, ft10<br> [0x80002100]:csrrs a7, fflags, zero<br> [0x80002104]:sw t6, 1200(a5)<br> |
| 338|[0x800082f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002114]:fle.d t6, ft11, ft10<br> [0x80002118]:csrrs a7, fflags, zero<br> [0x8000211c]:sw t6, 1216(a5)<br> |
| 339|[0x80008308]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000212c]:fle.d t6, ft11, ft10<br> [0x80002130]:csrrs a7, fflags, zero<br> [0x80002134]:sw t6, 1232(a5)<br> |
| 340|[0x80008318]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002144]:fle.d t6, ft11, ft10<br> [0x80002148]:csrrs a7, fflags, zero<br> [0x8000214c]:sw t6, 1248(a5)<br> |
| 341|[0x80008328]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000215c]:fle.d t6, ft11, ft10<br> [0x80002160]:csrrs a7, fflags, zero<br> [0x80002164]:sw t6, 1264(a5)<br> |
| 342|[0x80008338]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002174]:fle.d t6, ft11, ft10<br> [0x80002178]:csrrs a7, fflags, zero<br> [0x8000217c]:sw t6, 1280(a5)<br> |
| 343|[0x80008348]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000218c]:fle.d t6, ft11, ft10<br> [0x80002190]:csrrs a7, fflags, zero<br> [0x80002194]:sw t6, 1296(a5)<br> |
| 344|[0x80008358]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800021a4]:fle.d t6, ft11, ft10<br> [0x800021a8]:csrrs a7, fflags, zero<br> [0x800021ac]:sw t6, 1312(a5)<br> |
| 345|[0x80008368]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800021bc]:fle.d t6, ft11, ft10<br> [0x800021c0]:csrrs a7, fflags, zero<br> [0x800021c4]:sw t6, 1328(a5)<br> |
| 346|[0x80008378]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800021d4]:fle.d t6, ft11, ft10<br> [0x800021d8]:csrrs a7, fflags, zero<br> [0x800021dc]:sw t6, 1344(a5)<br> |
| 347|[0x80008388]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800021ec]:fle.d t6, ft11, ft10<br> [0x800021f0]:csrrs a7, fflags, zero<br> [0x800021f4]:sw t6, 1360(a5)<br> |
| 348|[0x80008398]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002204]:fle.d t6, ft11, ft10<br> [0x80002208]:csrrs a7, fflags, zero<br> [0x8000220c]:sw t6, 1376(a5)<br> |
| 349|[0x800083a8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000221c]:fle.d t6, ft11, ft10<br> [0x80002220]:csrrs a7, fflags, zero<br> [0x80002224]:sw t6, 1392(a5)<br> |
| 350|[0x800083b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002234]:fle.d t6, ft11, ft10<br> [0x80002238]:csrrs a7, fflags, zero<br> [0x8000223c]:sw t6, 1408(a5)<br> |
| 351|[0x800083c8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000224c]:fle.d t6, ft11, ft10<br> [0x80002250]:csrrs a7, fflags, zero<br> [0x80002254]:sw t6, 1424(a5)<br> |
| 352|[0x800083d8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002264]:fle.d t6, ft11, ft10<br> [0x80002268]:csrrs a7, fflags, zero<br> [0x8000226c]:sw t6, 1440(a5)<br> |
| 353|[0x800083e8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000227c]:fle.d t6, ft11, ft10<br> [0x80002280]:csrrs a7, fflags, zero<br> [0x80002284]:sw t6, 1456(a5)<br> |
| 354|[0x800083f8]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002294]:fle.d t6, ft11, ft10<br> [0x80002298]:csrrs a7, fflags, zero<br> [0x8000229c]:sw t6, 1472(a5)<br> |
| 355|[0x80008408]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800022ac]:fle.d t6, ft11, ft10<br> [0x800022b0]:csrrs a7, fflags, zero<br> [0x800022b4]:sw t6, 1488(a5)<br> |
| 356|[0x80008418]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800022c4]:fle.d t6, ft11, ft10<br> [0x800022c8]:csrrs a7, fflags, zero<br> [0x800022cc]:sw t6, 1504(a5)<br> |
| 357|[0x80008428]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800022dc]:fle.d t6, ft11, ft10<br> [0x800022e0]:csrrs a7, fflags, zero<br> [0x800022e4]:sw t6, 1520(a5)<br> |
| 358|[0x80008438]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800022f4]:fle.d t6, ft11, ft10<br> [0x800022f8]:csrrs a7, fflags, zero<br> [0x800022fc]:sw t6, 1536(a5)<br> |
| 359|[0x80008448]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000230c]:fle.d t6, ft11, ft10<br> [0x80002310]:csrrs a7, fflags, zero<br> [0x80002314]:sw t6, 1552(a5)<br> |
| 360|[0x80008458]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002324]:fle.d t6, ft11, ft10<br> [0x80002328]:csrrs a7, fflags, zero<br> [0x8000232c]:sw t6, 1568(a5)<br> |
| 361|[0x80008468]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000233c]:fle.d t6, ft11, ft10<br> [0x80002340]:csrrs a7, fflags, zero<br> [0x80002344]:sw t6, 1584(a5)<br> |
| 362|[0x80008478]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002354]:fle.d t6, ft11, ft10<br> [0x80002358]:csrrs a7, fflags, zero<br> [0x8000235c]:sw t6, 1600(a5)<br> |
| 363|[0x80008488]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000236c]:fle.d t6, ft11, ft10<br> [0x80002370]:csrrs a7, fflags, zero<br> [0x80002374]:sw t6, 1616(a5)<br> |
| 364|[0x80008498]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002384]:fle.d t6, ft11, ft10<br> [0x80002388]:csrrs a7, fflags, zero<br> [0x8000238c]:sw t6, 1632(a5)<br> |
| 365|[0x800084a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000239c]:fle.d t6, ft11, ft10<br> [0x800023a0]:csrrs a7, fflags, zero<br> [0x800023a4]:sw t6, 1648(a5)<br> |
| 366|[0x800084b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800023b4]:fle.d t6, ft11, ft10<br> [0x800023b8]:csrrs a7, fflags, zero<br> [0x800023bc]:sw t6, 1664(a5)<br> |
| 367|[0x800084c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800023cc]:fle.d t6, ft11, ft10<br> [0x800023d0]:csrrs a7, fflags, zero<br> [0x800023d4]:sw t6, 1680(a5)<br> |
| 368|[0x800084d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800023e4]:fle.d t6, ft11, ft10<br> [0x800023e8]:csrrs a7, fflags, zero<br> [0x800023ec]:sw t6, 1696(a5)<br> |
| 369|[0x800084e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800023fc]:fle.d t6, ft11, ft10<br> [0x80002400]:csrrs a7, fflags, zero<br> [0x80002404]:sw t6, 1712(a5)<br> |
| 370|[0x800084f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002414]:fle.d t6, ft11, ft10<br> [0x80002418]:csrrs a7, fflags, zero<br> [0x8000241c]:sw t6, 1728(a5)<br> |
| 371|[0x80008508]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000242c]:fle.d t6, ft11, ft10<br> [0x80002430]:csrrs a7, fflags, zero<br> [0x80002434]:sw t6, 1744(a5)<br> |
| 372|[0x80008518]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002444]:fle.d t6, ft11, ft10<br> [0x80002448]:csrrs a7, fflags, zero<br> [0x8000244c]:sw t6, 1760(a5)<br> |
| 373|[0x80008528]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000245c]:fle.d t6, ft11, ft10<br> [0x80002460]:csrrs a7, fflags, zero<br> [0x80002464]:sw t6, 1776(a5)<br> |
| 374|[0x80008538]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002474]:fle.d t6, ft11, ft10<br> [0x80002478]:csrrs a7, fflags, zero<br> [0x8000247c]:sw t6, 1792(a5)<br> |
| 375|[0x80008548]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000248c]:fle.d t6, ft11, ft10<br> [0x80002490]:csrrs a7, fflags, zero<br> [0x80002494]:sw t6, 1808(a5)<br> |
| 376|[0x80008558]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800024a4]:fle.d t6, ft11, ft10<br> [0x800024a8]:csrrs a7, fflags, zero<br> [0x800024ac]:sw t6, 1824(a5)<br> |
| 377|[0x80008568]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800024bc]:fle.d t6, ft11, ft10<br> [0x800024c0]:csrrs a7, fflags, zero<br> [0x800024c4]:sw t6, 1840(a5)<br> |
| 378|[0x80008578]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800024d4]:fle.d t6, ft11, ft10<br> [0x800024d8]:csrrs a7, fflags, zero<br> [0x800024dc]:sw t6, 1856(a5)<br> |
| 379|[0x80008588]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800024ec]:fle.d t6, ft11, ft10<br> [0x800024f0]:csrrs a7, fflags, zero<br> [0x800024f4]:sw t6, 1872(a5)<br> |
| 380|[0x80008598]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002504]:fle.d t6, ft11, ft10<br> [0x80002508]:csrrs a7, fflags, zero<br> [0x8000250c]:sw t6, 1888(a5)<br> |
| 381|[0x800085a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000251c]:fle.d t6, ft11, ft10<br> [0x80002520]:csrrs a7, fflags, zero<br> [0x80002524]:sw t6, 1904(a5)<br> |
| 382|[0x800085b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002538]:fle.d t6, ft11, ft10<br> [0x8000253c]:csrrs a7, fflags, zero<br> [0x80002540]:sw t6, 1920(a5)<br> |
| 383|[0x800085c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002550]:fle.d t6, ft11, ft10<br> [0x80002554]:csrrs a7, fflags, zero<br> [0x80002558]:sw t6, 1936(a5)<br> |
| 384|[0x800085d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002568]:fle.d t6, ft11, ft10<br> [0x8000256c]:csrrs a7, fflags, zero<br> [0x80002570]:sw t6, 1952(a5)<br> |
| 385|[0x800085e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002580]:fle.d t6, ft11, ft10<br> [0x80002584]:csrrs a7, fflags, zero<br> [0x80002588]:sw t6, 1968(a5)<br> |
| 386|[0x800085f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002598]:fle.d t6, ft11, ft10<br> [0x8000259c]:csrrs a7, fflags, zero<br> [0x800025a0]:sw t6, 1984(a5)<br> |
| 387|[0x80008608]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800025b0]:fle.d t6, ft11, ft10<br> [0x800025b4]:csrrs a7, fflags, zero<br> [0x800025b8]:sw t6, 2000(a5)<br> |
| 388|[0x80008618]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800025c8]:fle.d t6, ft11, ft10<br> [0x800025cc]:csrrs a7, fflags, zero<br> [0x800025d0]:sw t6, 2016(a5)<br> |
| 389|[0x80008230]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800025e8]:fle.d t6, ft11, ft10<br> [0x800025ec]:csrrs a7, fflags, zero<br> [0x800025f0]:sw t6, 0(a5)<br>    |
| 390|[0x80008240]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002600]:fle.d t6, ft11, ft10<br> [0x80002604]:csrrs a7, fflags, zero<br> [0x80002608]:sw t6, 16(a5)<br>   |
| 391|[0x80008250]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002618]:fle.d t6, ft11, ft10<br> [0x8000261c]:csrrs a7, fflags, zero<br> [0x80002620]:sw t6, 32(a5)<br>   |
| 392|[0x80008260]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002630]:fle.d t6, ft11, ft10<br> [0x80002634]:csrrs a7, fflags, zero<br> [0x80002638]:sw t6, 48(a5)<br>   |
| 393|[0x80008270]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002648]:fle.d t6, ft11, ft10<br> [0x8000264c]:csrrs a7, fflags, zero<br> [0x80002650]:sw t6, 64(a5)<br>   |
| 394|[0x80008280]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002660]:fle.d t6, ft11, ft10<br> [0x80002664]:csrrs a7, fflags, zero<br> [0x80002668]:sw t6, 80(a5)<br>   |
| 395|[0x80008290]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002678]:fle.d t6, ft11, ft10<br> [0x8000267c]:csrrs a7, fflags, zero<br> [0x80002680]:sw t6, 96(a5)<br>   |
| 396|[0x800082a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002690]:fle.d t6, ft11, ft10<br> [0x80002694]:csrrs a7, fflags, zero<br> [0x80002698]:sw t6, 112(a5)<br>  |
| 397|[0x800082b0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800026a8]:fle.d t6, ft11, ft10<br> [0x800026ac]:csrrs a7, fflags, zero<br> [0x800026b0]:sw t6, 128(a5)<br>  |
| 398|[0x800082c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800026c0]:fle.d t6, ft11, ft10<br> [0x800026c4]:csrrs a7, fflags, zero<br> [0x800026c8]:sw t6, 144(a5)<br>  |
| 399|[0x800082d0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800026d8]:fle.d t6, ft11, ft10<br> [0x800026dc]:csrrs a7, fflags, zero<br> [0x800026e0]:sw t6, 160(a5)<br>  |
| 400|[0x800082e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800026f0]:fle.d t6, ft11, ft10<br> [0x800026f4]:csrrs a7, fflags, zero<br> [0x800026f8]:sw t6, 176(a5)<br>  |
| 401|[0x800082f0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002708]:fle.d t6, ft11, ft10<br> [0x8000270c]:csrrs a7, fflags, zero<br> [0x80002710]:sw t6, 192(a5)<br>  |
| 402|[0x80008300]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002720]:fle.d t6, ft11, ft10<br> [0x80002724]:csrrs a7, fflags, zero<br> [0x80002728]:sw t6, 208(a5)<br>  |
| 403|[0x80008310]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002738]:fle.d t6, ft11, ft10<br> [0x8000273c]:csrrs a7, fflags, zero<br> [0x80002740]:sw t6, 224(a5)<br>  |
| 404|[0x80008320]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002750]:fle.d t6, ft11, ft10<br> [0x80002754]:csrrs a7, fflags, zero<br> [0x80002758]:sw t6, 240(a5)<br>  |
| 405|[0x80008330]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002768]:fle.d t6, ft11, ft10<br> [0x8000276c]:csrrs a7, fflags, zero<br> [0x80002770]:sw t6, 256(a5)<br>  |
| 406|[0x80008340]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002780]:fle.d t6, ft11, ft10<br> [0x80002784]:csrrs a7, fflags, zero<br> [0x80002788]:sw t6, 272(a5)<br>  |
| 407|[0x80008350]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002798]:fle.d t6, ft11, ft10<br> [0x8000279c]:csrrs a7, fflags, zero<br> [0x800027a0]:sw t6, 288(a5)<br>  |
| 408|[0x80008360]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800027b0]:fle.d t6, ft11, ft10<br> [0x800027b4]:csrrs a7, fflags, zero<br> [0x800027b8]:sw t6, 304(a5)<br>  |
| 409|[0x80008370]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800027c8]:fle.d t6, ft11, ft10<br> [0x800027cc]:csrrs a7, fflags, zero<br> [0x800027d0]:sw t6, 320(a5)<br>  |
| 410|[0x80008380]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800027e0]:fle.d t6, ft11, ft10<br> [0x800027e4]:csrrs a7, fflags, zero<br> [0x800027e8]:sw t6, 336(a5)<br>  |
| 411|[0x80008390]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800027f8]:fle.d t6, ft11, ft10<br> [0x800027fc]:csrrs a7, fflags, zero<br> [0x80002800]:sw t6, 352(a5)<br>  |
| 412|[0x800083a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002810]:fle.d t6, ft11, ft10<br> [0x80002814]:csrrs a7, fflags, zero<br> [0x80002818]:sw t6, 368(a5)<br>  |
| 413|[0x800083b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002828]:fle.d t6, ft11, ft10<br> [0x8000282c]:csrrs a7, fflags, zero<br> [0x80002830]:sw t6, 384(a5)<br>  |
| 414|[0x800083c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002840]:fle.d t6, ft11, ft10<br> [0x80002844]:csrrs a7, fflags, zero<br> [0x80002848]:sw t6, 400(a5)<br>  |
| 415|[0x800083d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002858]:fle.d t6, ft11, ft10<br> [0x8000285c]:csrrs a7, fflags, zero<br> [0x80002860]:sw t6, 416(a5)<br>  |
| 416|[0x800083e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002870]:fle.d t6, ft11, ft10<br> [0x80002874]:csrrs a7, fflags, zero<br> [0x80002878]:sw t6, 432(a5)<br>  |
| 417|[0x800083f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002888]:fle.d t6, ft11, ft10<br> [0x8000288c]:csrrs a7, fflags, zero<br> [0x80002890]:sw t6, 448(a5)<br>  |
| 418|[0x80008400]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800028a0]:fle.d t6, ft11, ft10<br> [0x800028a4]:csrrs a7, fflags, zero<br> [0x800028a8]:sw t6, 464(a5)<br>  |
| 419|[0x80008410]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800028b8]:fle.d t6, ft11, ft10<br> [0x800028bc]:csrrs a7, fflags, zero<br> [0x800028c0]:sw t6, 480(a5)<br>  |
| 420|[0x80008420]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800028d0]:fle.d t6, ft11, ft10<br> [0x800028d4]:csrrs a7, fflags, zero<br> [0x800028d8]:sw t6, 496(a5)<br>  |
| 421|[0x80008430]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800028e8]:fle.d t6, ft11, ft10<br> [0x800028ec]:csrrs a7, fflags, zero<br> [0x800028f0]:sw t6, 512(a5)<br>  |
| 422|[0x80008440]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002900]:fle.d t6, ft11, ft10<br> [0x80002904]:csrrs a7, fflags, zero<br> [0x80002908]:sw t6, 528(a5)<br>  |
| 423|[0x80008450]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002918]:fle.d t6, ft11, ft10<br> [0x8000291c]:csrrs a7, fflags, zero<br> [0x80002920]:sw t6, 544(a5)<br>  |
| 424|[0x80008460]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002930]:fle.d t6, ft11, ft10<br> [0x80002934]:csrrs a7, fflags, zero<br> [0x80002938]:sw t6, 560(a5)<br>  |
| 425|[0x80008470]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002948]:fle.d t6, ft11, ft10<br> [0x8000294c]:csrrs a7, fflags, zero<br> [0x80002950]:sw t6, 576(a5)<br>  |
| 426|[0x80008480]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002960]:fle.d t6, ft11, ft10<br> [0x80002964]:csrrs a7, fflags, zero<br> [0x80002968]:sw t6, 592(a5)<br>  |
| 427|[0x80008490]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002978]:fle.d t6, ft11, ft10<br> [0x8000297c]:csrrs a7, fflags, zero<br> [0x80002980]:sw t6, 608(a5)<br>  |
| 428|[0x800084a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002990]:fle.d t6, ft11, ft10<br> [0x80002994]:csrrs a7, fflags, zero<br> [0x80002998]:sw t6, 624(a5)<br>  |
| 429|[0x800084b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800029a8]:fle.d t6, ft11, ft10<br> [0x800029ac]:csrrs a7, fflags, zero<br> [0x800029b0]:sw t6, 640(a5)<br>  |
| 430|[0x800084c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800029c0]:fle.d t6, ft11, ft10<br> [0x800029c4]:csrrs a7, fflags, zero<br> [0x800029c8]:sw t6, 656(a5)<br>  |
| 431|[0x800084d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800029d8]:fle.d t6, ft11, ft10<br> [0x800029dc]:csrrs a7, fflags, zero<br> [0x800029e0]:sw t6, 672(a5)<br>  |
| 432|[0x800084e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800029f0]:fle.d t6, ft11, ft10<br> [0x800029f4]:csrrs a7, fflags, zero<br> [0x800029f8]:sw t6, 688(a5)<br>  |
| 433|[0x800084f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a08]:fle.d t6, ft11, ft10<br> [0x80002a0c]:csrrs a7, fflags, zero<br> [0x80002a10]:sw t6, 704(a5)<br>  |
| 434|[0x80008500]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a20]:fle.d t6, ft11, ft10<br> [0x80002a24]:csrrs a7, fflags, zero<br> [0x80002a28]:sw t6, 720(a5)<br>  |
| 435|[0x80008510]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a38]:fle.d t6, ft11, ft10<br> [0x80002a3c]:csrrs a7, fflags, zero<br> [0x80002a40]:sw t6, 736(a5)<br>  |
| 436|[0x80008520]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a50]:fle.d t6, ft11, ft10<br> [0x80002a54]:csrrs a7, fflags, zero<br> [0x80002a58]:sw t6, 752(a5)<br>  |
| 437|[0x80008530]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a68]:fle.d t6, ft11, ft10<br> [0x80002a6c]:csrrs a7, fflags, zero<br> [0x80002a70]:sw t6, 768(a5)<br>  |
| 438|[0x80008540]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a80]:fle.d t6, ft11, ft10<br> [0x80002a84]:csrrs a7, fflags, zero<br> [0x80002a88]:sw t6, 784(a5)<br>  |
| 439|[0x80008550]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002a98]:fle.d t6, ft11, ft10<br> [0x80002a9c]:csrrs a7, fflags, zero<br> [0x80002aa0]:sw t6, 800(a5)<br>  |
| 440|[0x80008560]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ab0]:fle.d t6, ft11, ft10<br> [0x80002ab4]:csrrs a7, fflags, zero<br> [0x80002ab8]:sw t6, 816(a5)<br>  |
| 441|[0x80008570]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ac8]:fle.d t6, ft11, ft10<br> [0x80002acc]:csrrs a7, fflags, zero<br> [0x80002ad0]:sw t6, 832(a5)<br>  |
| 442|[0x80008580]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ae0]:fle.d t6, ft11, ft10<br> [0x80002ae4]:csrrs a7, fflags, zero<br> [0x80002ae8]:sw t6, 848(a5)<br>  |
| 443|[0x80008590]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002af8]:fle.d t6, ft11, ft10<br> [0x80002afc]:csrrs a7, fflags, zero<br> [0x80002b00]:sw t6, 864(a5)<br>  |
| 444|[0x800085a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002b10]:fle.d t6, ft11, ft10<br> [0x80002b14]:csrrs a7, fflags, zero<br> [0x80002b18]:sw t6, 880(a5)<br>  |
| 445|[0x800085b0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002b28]:fle.d t6, ft11, ft10<br> [0x80002b2c]:csrrs a7, fflags, zero<br> [0x80002b30]:sw t6, 896(a5)<br>  |
| 446|[0x800085c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002b40]:fle.d t6, ft11, ft10<br> [0x80002b44]:csrrs a7, fflags, zero<br> [0x80002b48]:sw t6, 912(a5)<br>  |
| 447|[0x800085d0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002b58]:fle.d t6, ft11, ft10<br> [0x80002b5c]:csrrs a7, fflags, zero<br> [0x80002b60]:sw t6, 928(a5)<br>  |
| 448|[0x800085e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002b70]:fle.d t6, ft11, ft10<br> [0x80002b74]:csrrs a7, fflags, zero<br> [0x80002b78]:sw t6, 944(a5)<br>  |
| 449|[0x800085f0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002b88]:fle.d t6, ft11, ft10<br> [0x80002b8c]:csrrs a7, fflags, zero<br> [0x80002b90]:sw t6, 960(a5)<br>  |
| 450|[0x80008600]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002ba0]:fle.d t6, ft11, ft10<br> [0x80002ba4]:csrrs a7, fflags, zero<br> [0x80002ba8]:sw t6, 976(a5)<br>  |
| 451|[0x80008610]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002bb8]:fle.d t6, ft11, ft10<br> [0x80002bbc]:csrrs a7, fflags, zero<br> [0x80002bc0]:sw t6, 992(a5)<br>  |
| 452|[0x80008620]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002bd0]:fle.d t6, ft11, ft10<br> [0x80002bd4]:csrrs a7, fflags, zero<br> [0x80002bd8]:sw t6, 1008(a5)<br> |
| 453|[0x80008630]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002be8]:fle.d t6, ft11, ft10<br> [0x80002bec]:csrrs a7, fflags, zero<br> [0x80002bf0]:sw t6, 1024(a5)<br> |
| 454|[0x80008640]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c00]:fle.d t6, ft11, ft10<br> [0x80002c04]:csrrs a7, fflags, zero<br> [0x80002c08]:sw t6, 1040(a5)<br> |
| 455|[0x80008650]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c18]:fle.d t6, ft11, ft10<br> [0x80002c1c]:csrrs a7, fflags, zero<br> [0x80002c20]:sw t6, 1056(a5)<br> |
| 456|[0x80008660]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c30]:fle.d t6, ft11, ft10<br> [0x80002c34]:csrrs a7, fflags, zero<br> [0x80002c38]:sw t6, 1072(a5)<br> |
| 457|[0x80008670]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c48]:fle.d t6, ft11, ft10<br> [0x80002c4c]:csrrs a7, fflags, zero<br> [0x80002c50]:sw t6, 1088(a5)<br> |
| 458|[0x80008680]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c60]:fle.d t6, ft11, ft10<br> [0x80002c64]:csrrs a7, fflags, zero<br> [0x80002c68]:sw t6, 1104(a5)<br> |
| 459|[0x80008690]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c78]:fle.d t6, ft11, ft10<br> [0x80002c7c]:csrrs a7, fflags, zero<br> [0x80002c80]:sw t6, 1120(a5)<br> |
| 460|[0x800086a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002c90]:fle.d t6, ft11, ft10<br> [0x80002c94]:csrrs a7, fflags, zero<br> [0x80002c98]:sw t6, 1136(a5)<br> |
| 461|[0x800086b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ca8]:fle.d t6, ft11, ft10<br> [0x80002cac]:csrrs a7, fflags, zero<br> [0x80002cb0]:sw t6, 1152(a5)<br> |
| 462|[0x800086c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002cc0]:fle.d t6, ft11, ft10<br> [0x80002cc4]:csrrs a7, fflags, zero<br> [0x80002cc8]:sw t6, 1168(a5)<br> |
| 463|[0x800086d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002cd8]:fle.d t6, ft11, ft10<br> [0x80002cdc]:csrrs a7, fflags, zero<br> [0x80002ce0]:sw t6, 1184(a5)<br> |
| 464|[0x800086e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002cf0]:fle.d t6, ft11, ft10<br> [0x80002cf4]:csrrs a7, fflags, zero<br> [0x80002cf8]:sw t6, 1200(a5)<br> |
| 465|[0x800086f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002d08]:fle.d t6, ft11, ft10<br> [0x80002d0c]:csrrs a7, fflags, zero<br> [0x80002d10]:sw t6, 1216(a5)<br> |
| 466|[0x80008700]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002d20]:fle.d t6, ft11, ft10<br> [0x80002d24]:csrrs a7, fflags, zero<br> [0x80002d28]:sw t6, 1232(a5)<br> |
| 467|[0x80008710]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002d38]:fle.d t6, ft11, ft10<br> [0x80002d3c]:csrrs a7, fflags, zero<br> [0x80002d40]:sw t6, 1248(a5)<br> |
| 468|[0x80008720]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002d50]:fle.d t6, ft11, ft10<br> [0x80002d54]:csrrs a7, fflags, zero<br> [0x80002d58]:sw t6, 1264(a5)<br> |
| 469|[0x80008730]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002d68]:fle.d t6, ft11, ft10<br> [0x80002d6c]:csrrs a7, fflags, zero<br> [0x80002d70]:sw t6, 1280(a5)<br> |
| 470|[0x80008740]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002d80]:fle.d t6, ft11, ft10<br> [0x80002d84]:csrrs a7, fflags, zero<br> [0x80002d88]:sw t6, 1296(a5)<br> |
| 471|[0x80008750]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002d98]:fle.d t6, ft11, ft10<br> [0x80002d9c]:csrrs a7, fflags, zero<br> [0x80002da0]:sw t6, 1312(a5)<br> |
| 472|[0x80008760]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002db0]:fle.d t6, ft11, ft10<br> [0x80002db4]:csrrs a7, fflags, zero<br> [0x80002db8]:sw t6, 1328(a5)<br> |
| 473|[0x80008770]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002dc8]:fle.d t6, ft11, ft10<br> [0x80002dcc]:csrrs a7, fflags, zero<br> [0x80002dd0]:sw t6, 1344(a5)<br> |
| 474|[0x80008780]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002de0]:fle.d t6, ft11, ft10<br> [0x80002de4]:csrrs a7, fflags, zero<br> [0x80002de8]:sw t6, 1360(a5)<br> |
| 475|[0x80008790]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80002df8]:fle.d t6, ft11, ft10<br> [0x80002dfc]:csrrs a7, fflags, zero<br> [0x80002e00]:sw t6, 1376(a5)<br> |
| 476|[0x800087a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e10]:fle.d t6, ft11, ft10<br> [0x80002e14]:csrrs a7, fflags, zero<br> [0x80002e18]:sw t6, 1392(a5)<br> |
| 477|[0x800087b0]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e28]:fle.d t6, ft11, ft10<br> [0x80002e2c]:csrrs a7, fflags, zero<br> [0x80002e30]:sw t6, 1408(a5)<br> |
| 478|[0x800087c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e40]:fle.d t6, ft11, ft10<br> [0x80002e44]:csrrs a7, fflags, zero<br> [0x80002e48]:sw t6, 1424(a5)<br> |
| 479|[0x800087d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e58]:fle.d t6, ft11, ft10<br> [0x80002e5c]:csrrs a7, fflags, zero<br> [0x80002e60]:sw t6, 1440(a5)<br> |
| 480|[0x800087e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e70]:fle.d t6, ft11, ft10<br> [0x80002e74]:csrrs a7, fflags, zero<br> [0x80002e78]:sw t6, 1456(a5)<br> |
| 481|[0x800087f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002e88]:fle.d t6, ft11, ft10<br> [0x80002e8c]:csrrs a7, fflags, zero<br> [0x80002e90]:sw t6, 1472(a5)<br> |
| 482|[0x80008800]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ea0]:fle.d t6, ft11, ft10<br> [0x80002ea4]:csrrs a7, fflags, zero<br> [0x80002ea8]:sw t6, 1488(a5)<br> |
| 483|[0x80008810]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80002eb8]:fle.d t6, ft11, ft10<br> [0x80002ebc]:csrrs a7, fflags, zero<br> [0x80002ec0]:sw t6, 1504(a5)<br> |
| 484|[0x80008820]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80002ed0]:fle.d t6, ft11, ft10<br> [0x80002ed4]:csrrs a7, fflags, zero<br> [0x80002ed8]:sw t6, 1520(a5)<br> |
| 485|[0x80008628]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800031dc]:fle.d t6, ft11, ft10<br> [0x800031e0]:csrrs a7, fflags, zero<br> [0x800031e4]:sw t6, 0(a5)<br>    |
| 486|[0x80008638]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800031f4]:fle.d t6, ft11, ft10<br> [0x800031f8]:csrrs a7, fflags, zero<br> [0x800031fc]:sw t6, 16(a5)<br>   |
| 487|[0x80008648]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000320c]:fle.d t6, ft11, ft10<br> [0x80003210]:csrrs a7, fflags, zero<br> [0x80003214]:sw t6, 32(a5)<br>   |
| 488|[0x80008658]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80003224]:fle.d t6, ft11, ft10<br> [0x80003228]:csrrs a7, fflags, zero<br> [0x8000322c]:sw t6, 48(a5)<br>   |
| 489|[0x80008668]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000323c]:fle.d t6, ft11, ft10<br> [0x80003240]:csrrs a7, fflags, zero<br> [0x80003244]:sw t6, 64(a5)<br>   |
| 490|[0x80008678]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80003254]:fle.d t6, ft11, ft10<br> [0x80003258]:csrrs a7, fflags, zero<br> [0x8000325c]:sw t6, 80(a5)<br>   |
| 491|[0x80008688]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000326c]:fle.d t6, ft11, ft10<br> [0x80003270]:csrrs a7, fflags, zero<br> [0x80003274]:sw t6, 96(a5)<br>   |
| 492|[0x80008698]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80003284]:fle.d t6, ft11, ft10<br> [0x80003288]:csrrs a7, fflags, zero<br> [0x8000328c]:sw t6, 112(a5)<br>  |
| 493|[0x800086a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000329c]:fle.d t6, ft11, ft10<br> [0x800032a0]:csrrs a7, fflags, zero<br> [0x800032a4]:sw t6, 128(a5)<br>  |
| 494|[0x800086b8]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x800032b4]:fle.d t6, ft11, ft10<br> [0x800032b8]:csrrs a7, fflags, zero<br> [0x800032bc]:sw t6, 144(a5)<br>  |
| 495|[0x800086c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800032cc]:fle.d t6, ft11, ft10<br> [0x800032d0]:csrrs a7, fflags, zero<br> [0x800032d4]:sw t6, 160(a5)<br>  |
| 496|[0x800086d8]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800032e4]:fle.d t6, ft11, ft10<br> [0x800032e8]:csrrs a7, fflags, zero<br> [0x800032ec]:sw t6, 176(a5)<br>  |
| 497|[0x800086e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800032fc]:fle.d t6, ft11, ft10<br> [0x80003300]:csrrs a7, fflags, zero<br> [0x80003304]:sw t6, 192(a5)<br>  |
| 498|[0x800086f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80003314]:fle.d t6, ft11, ft10<br> [0x80003318]:csrrs a7, fflags, zero<br> [0x8000331c]:sw t6, 208(a5)<br>  |
| 499|[0x80008708]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000332c]:fle.d t6, ft11, ft10<br> [0x80003330]:csrrs a7, fflags, zero<br> [0x80003334]:sw t6, 224(a5)<br>  |
| 500|[0x80008718]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80003344]:fle.d t6, ft11, ft10<br> [0x80003348]:csrrs a7, fflags, zero<br> [0x8000334c]:sw t6, 240(a5)<br>  |
| 501|[0x80008728]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000335c]:fle.d t6, ft11, ft10<br> [0x80003360]:csrrs a7, fflags, zero<br> [0x80003364]:sw t6, 256(a5)<br>  |
| 502|[0x80008738]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x80003374]:fle.d t6, ft11, ft10<br> [0x80003378]:csrrs a7, fflags, zero<br> [0x8000337c]:sw t6, 272(a5)<br>  |
| 503|[0x80008748]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x8000338c]:fle.d t6, ft11, ft10<br> [0x80003390]:csrrs a7, fflags, zero<br> [0x80003394]:sw t6, 288(a5)<br>  |
| 504|[0x80008758]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 0  #nosat<br>                                                                                      |[0x800033a4]:fle.d t6, ft11, ft10<br> [0x800033a8]:csrrs a7, fflags, zero<br> [0x800033ac]:sw t6, 304(a5)<br>  |
| 505|[0x80008768]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800033bc]:fle.d t6, ft11, ft10<br> [0x800033c0]:csrrs a7, fflags, zero<br> [0x800033c4]:sw t6, 320(a5)<br>  |
| 506|[0x80008778]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800033d4]:fle.d t6, ft11, ft10<br> [0x800033d8]:csrrs a7, fflags, zero<br> [0x800033dc]:sw t6, 336(a5)<br>  |
| 507|[0x80008788]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x800033ec]:fle.d t6, ft11, ft10<br> [0x800033f0]:csrrs a7, fflags, zero<br> [0x800033f4]:sw t6, 352(a5)<br>  |
| 508|[0x80008798]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80003404]:fle.d t6, ft11, ft10<br> [0x80003408]:csrrs a7, fflags, zero<br> [0x8000340c]:sw t6, 368(a5)<br>  |
| 509|[0x800087a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x8000341c]:fle.d t6, ft11, ft10<br> [0x80003420]:csrrs a7, fflags, zero<br> [0x80003424]:sw t6, 384(a5)<br>  |
| 510|[0x800087b8]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x80003434]:fle.d t6, ft11, ft10<br> [0x80003438]:csrrs a7, fflags, zero<br> [0x8000343c]:sw t6, 400(a5)<br>  |
| 511|[0x800087c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x8000344c]:fle.d t6, ft11, ft10<br> [0x80003450]:csrrs a7, fflags, zero<br> [0x80003454]:sw t6, 416(a5)<br>  |
| 512|[0x800087d8]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                      |[0x80003464]:fle.d t6, ft11, ft10<br> [0x80003468]:csrrs a7, fflags, zero<br> [0x8000346c]:sw t6, 432(a5)<br>  |
| 513|[0x800087e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x8000347c]:fle.d t6, ft11, ft10<br> [0x80003480]:csrrs a7, fflags, zero<br> [0x80003484]:sw t6, 448(a5)<br>  |
| 514|[0x800087f8]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                      |[0x80003494]:fle.d t6, ft11, ft10<br> [0x80003498]:csrrs a7, fflags, zero<br> [0x8000349c]:sw t6, 464(a5)<br>  |
| 515|[0x80008808]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800034ac]:fle.d t6, ft11, ft10<br> [0x800034b0]:csrrs a7, fflags, zero<br> [0x800034b4]:sw t6, 480(a5)<br>  |
| 516|[0x80008818]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                                                                      |[0x800034c4]:fle.d t6, ft11, ft10<br> [0x800034c8]:csrrs a7, fflags, zero<br> [0x800034cc]:sw t6, 496(a5)<br>  |
