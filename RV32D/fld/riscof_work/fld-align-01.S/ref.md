
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005e0')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fld-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fld/riscof_work/fld-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 50      |
| Total Signature Updates   | 21      |
| STAT1                     | 21      |
| STAT2                     | 0      |
| STAT3                     | 10     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000458]:fld ft5, 2048(a0)
[0x8000045c]:addi zero, zero, 0
[0x80000460]:addi zero, zero, 0
[0x80000464]:csrrs a7, fflags, zero
[0x80000468]:fsd ft5, 176(a5)
[0x8000046c]:sw a7, 180(a5)
[0x80000470]:addi a6, a6, 2040
[0x80000474]:auipc t1, 2
[0x80000478]:addi t1, t1, 908

[0x8000047c]:fld fa2, 2048(t1)
[0x80000480]:addi zero, zero, 0
[0x80000484]:addi zero, zero, 0
[0x80000488]:csrrs a7, fflags, zero
[0x8000048c]:fsd fa2, 192(a5)
[0x80000490]:sw a7, 196(a5)
[0x80000494]:addi a6, a6, 2040
[0x80000498]:auipc s3, 2
[0x8000049c]:addi s3, s3, 872

[0x800004a0]:fld fs10, 2048(s3)
[0x800004a4]:addi zero, zero, 0
[0x800004a8]:addi zero, zero, 0
[0x800004ac]:csrrs a7, fflags, zero
[0x800004b0]:fsd fs10, 208(a5)
[0x800004b4]:sw a7, 212(a5)
[0x800004b8]:addi a6, a6, 2040
[0x800004bc]:auipc t2, 2
[0x800004c0]:addi t2, t2, 836

[0x800004c4]:fld ft2, 2048(t2)
[0x800004c8]:addi zero, zero, 0
[0x800004cc]:addi zero, zero, 0
[0x800004d0]:csrrs a7, fflags, zero
[0x800004d4]:fsd ft2, 224(a5)
[0x800004d8]:sw a7, 228(a5)
[0x800004dc]:addi a6, a6, 2040
[0x800004e0]:auipc tp, 2
[0x800004e4]:addi tp, tp, 800

[0x800004e8]:fld fs4, 2048(tp)
[0x800004ec]:addi zero, zero, 0
[0x800004f0]:addi zero, zero, 0
[0x800004f4]:csrrs a7, fflags, zero
[0x800004f8]:fsd fs4, 240(a5)
[0x800004fc]:sw a7, 244(a5)
[0x80000500]:addi a6, a6, 2040
[0x80000504]:auipc s6, 2
[0x80000508]:addi s6, s6, 764

[0x8000050c]:fld fs0, 2048(s6)
[0x80000510]:addi zero, zero, 0
[0x80000514]:addi zero, zero, 0
[0x80000518]:csrrs a7, fflags, zero
[0x8000051c]:fsd fs0, 256(a5)
[0x80000520]:sw a7, 260(a5)
[0x80000524]:addi a6, a6, 2040
[0x80000528]:auipc s11, 2
[0x8000052c]:addi s11, s11, 728

[0x80000530]:fld fs6, 2048(s11)
[0x80000534]:addi zero, zero, 0
[0x80000538]:addi zero, zero, 0
[0x8000053c]:csrrs a7, fflags, zero
[0x80000540]:fsd fs6, 272(a5)
[0x80000544]:sw a7, 276(a5)
[0x80000548]:addi a6, a6, 2040
[0x8000054c]:auipc a1, 2
[0x80000550]:addi a1, a1, 692

[0x80000554]:fld fs1, 2048(a1)
[0x80000558]:addi zero, zero, 0
[0x8000055c]:addi zero, zero, 0
[0x80000560]:csrrs a7, fflags, zero
[0x80000564]:fsd fs1, 288(a5)
[0x80000568]:sw a7, 292(a5)
[0x8000056c]:addi a6, a6, 2040
[0x80000570]:auipc s7, 2
[0x80000574]:addi s7, s7, 656

[0x80000578]:fld fs9, 2048(s7)
[0x8000057c]:addi zero, zero, 0
[0x80000580]:addi zero, zero, 0
[0x80000584]:csrrs a7, fflags, zero
[0x80000588]:fsd fs9, 304(a5)
[0x8000058c]:sw a7, 308(a5)
[0x80000590]:addi a6, a6, 2040
[0x80000594]:auipc t3, 2
[0x80000598]:addi t3, t3, 620

[0x8000059c]:fld fa6, 2048(t3)
[0x800005a0]:addi zero, zero, 0
[0x800005a4]:addi zero, zero, 0
[0x800005a8]:csrrs a7, fflags, zero
[0x800005ac]:fsd fa6, 320(a5)
[0x800005b0]:sw a7, 324(a5)
[0x800005b4]:addi a6, a6, 2040
[0x800005b8]:auipc sp, 2
[0x800005bc]:addi sp, sp, 584



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                 coverpoints                                                  |                                                                                                         code                                                                                                          |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000|- opcode : fld<br> - rs1 : x5<br> - rd : f21<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x8000011c]:fld fs5, 8(t0)<br> [0x80000120]:addi zero, zero, 0<br> [0x80000124]:addi zero, zero, 0<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fs5, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>          |
|   2|[0x80002224]<br>0x00000000|- rs1 : x13<br> - rd : f11<br> - ea_align == 0 and (imm_val % 4) == 1<br> - imm_val < 0<br>                   |[0x80000140]:fld fa1, 4089(a3)<br> [0x80000144]:addi zero, zero, 0<br> [0x80000148]:addi zero, zero, 0<br> [0x8000014c]:csrrs a7, fflags, zero<br> [0x80000150]:fsd fa1, 16(a5)<br> [0x80000154]:sw a7, 20(a5)<br>     |
|   3|[0x80002234]<br>0x00000000|- rs1 : x9<br> - rd : f4<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                       |[0x80000164]:fld ft4, 4090(s1)<br> [0x80000168]:addi zero, zero, 0<br> [0x8000016c]:addi zero, zero, 0<br> [0x80000170]:csrrs a7, fflags, zero<br> [0x80000174]:fsd ft4, 32(a5)<br> [0x80000178]:sw a7, 36(a5)<br>     |
|   4|[0x80002244]<br>0x00000000|- rs1 : x12<br> - rd : f30<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                     |[0x80000188]:fld ft10, 3839(a2)<br> [0x8000018c]:addi zero, zero, 0<br> [0x80000190]:addi zero, zero, 0<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd ft10, 48(a5)<br> [0x8000019c]:sw a7, 52(a5)<br>   |
|   5|[0x80002254]<br>0x00000000|- rs1 : x30<br> - rd : f0<br> - imm_val == 0<br>                                                              |[0x800001ac]:fld ft0, 0(t5)<br> [0x800001b0]:addi zero, zero, 0<br> [0x800001b4]:addi zero, zero, 0<br> [0x800001b8]:csrrs a7, fflags, zero<br> [0x800001bc]:fsd ft0, 64(a5)<br> [0x800001c0]:sw a7, 68(a5)<br>        |
|   6|[0x8000223c]<br>0x00000000|- rs1 : x16<br> - rd : f3<br>                                                                                 |[0x800001dc]:fld ft3, 2048(a6)<br> [0x800001e0]:addi zero, zero, 0<br> [0x800001e4]:addi zero, zero, 0<br> [0x800001e8]:csrrs s5, fflags, zero<br> [0x800001ec]:fsd ft3, 0(s3)<br> [0x800001f0]:sw s5, 4(s3)<br>       |
|   7|[0x80002244]<br>0x00000000|- rs1 : x26<br> - rd : f14<br>                                                                                |[0x8000020c]:fld fa4, 2048(s10)<br> [0x80000210]:addi zero, zero, 0<br> [0x80000214]:addi zero, zero, 0<br> [0x80000218]:csrrs a7, fflags, zero<br> [0x8000021c]:fsd fa4, 0(a5)<br> [0x80000220]:sw a7, 4(a5)<br>      |
|   8|[0x8000224c]<br>0x00000000|- rs1 : x15<br> - rd : f15<br>                                                                                |[0x8000023c]:fld fa5, 2048(a5)<br> [0x80000240]:addi zero, zero, 0<br> [0x80000244]:addi zero, zero, 0<br> [0x80000248]:csrrs s5, fflags, zero<br> [0x8000024c]:fsd fa5, 0(s3)<br> [0x80000250]:sw s5, 4(s3)<br>       |
|   9|[0x80002254]<br>0x00000000|- rs1 : x2<br> - rd : f23<br>                                                                                 |[0x8000026c]:fld fs7, 2048(sp)<br> [0x80000270]:addi zero, zero, 0<br> [0x80000274]:addi zero, zero, 0<br> [0x80000278]:csrrs a7, fflags, zero<br> [0x8000027c]:fsd fs7, 0(a5)<br> [0x80000280]:sw a7, 4(a5)<br>       |
|  10|[0x8000225c]<br>0x00000000|- rs1 : x17<br> - rd : f18<br>                                                                                |[0x8000029c]:fld fs2, 2048(a7)<br> [0x800002a0]:addi zero, zero, 0<br> [0x800002a4]:addi zero, zero, 0<br> [0x800002a8]:csrrs s5, fflags, zero<br> [0x800002ac]:fsd fs2, 0(s3)<br> [0x800002b0]:sw s5, 4(s3)<br>       |
|  11|[0x80002264]<br>0x00000000|- rs1 : x1<br> - rd : f29<br>                                                                                 |[0x800002cc]:fld ft9, 2048(ra)<br> [0x800002d0]:addi zero, zero, 0<br> [0x800002d4]:addi zero, zero, 0<br> [0x800002d8]:csrrs a7, fflags, zero<br> [0x800002dc]:fsd ft9, 0(a5)<br> [0x800002e0]:sw a7, 4(a5)<br>       |
|  12|[0x80002274]<br>0x00000000|- rs1 : x8<br> - rd : f27<br>                                                                                 |[0x800002f0]:fld fs11, 2048(fp)<br> [0x800002f4]:addi zero, zero, 0<br> [0x800002f8]:addi zero, zero, 0<br> [0x800002fc]:csrrs a7, fflags, zero<br> [0x80000300]:fsd fs11, 16(a5)<br> [0x80000304]:sw a7, 20(a5)<br>   |
|  13|[0x80002284]<br>0x00000000|- rs1 : x20<br> - rd : f13<br>                                                                                |[0x80000314]:fld fa3, 2048(s4)<br> [0x80000318]:addi zero, zero, 0<br> [0x8000031c]:addi zero, zero, 0<br> [0x80000320]:csrrs a7, fflags, zero<br> [0x80000324]:fsd fa3, 32(a5)<br> [0x80000328]:sw a7, 36(a5)<br>     |
|  14|[0x80002294]<br>0x00000000|- rs1 : x3<br> - rd : f10<br>                                                                                 |[0x80000338]:fld fa0, 2048(gp)<br> [0x8000033c]:addi zero, zero, 0<br> [0x80000340]:addi zero, zero, 0<br> [0x80000344]:csrrs a7, fflags, zero<br> [0x80000348]:fsd fa0, 48(a5)<br> [0x8000034c]:sw a7, 52(a5)<br>     |
|  15|[0x800022a4]<br>0x00000000|- rs1 : x24<br> - rd : f17<br>                                                                                |[0x8000035c]:fld fa7, 2048(s8)<br> [0x80000360]:addi zero, zero, 0<br> [0x80000364]:addi zero, zero, 0<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd fa7, 64(a5)<br> [0x80000370]:sw a7, 68(a5)<br>     |
|  16|[0x800022b4]<br>0x00000000|- rs1 : x25<br> - rd : f7<br>                                                                                 |[0x80000380]:fld ft7, 2048(s9)<br> [0x80000384]:addi zero, zero, 0<br> [0x80000388]:addi zero, zero, 0<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd ft7, 80(a5)<br> [0x80000394]:sw a7, 84(a5)<br>     |
|  17|[0x800022c4]<br>0x00000000|- rs1 : x18<br> - rd : f1<br>                                                                                 |[0x800003a4]:fld ft1, 2048(s2)<br> [0x800003a8]:addi zero, zero, 0<br> [0x800003ac]:addi zero, zero, 0<br> [0x800003b0]:csrrs a7, fflags, zero<br> [0x800003b4]:fsd ft1, 96(a5)<br> [0x800003b8]:sw a7, 100(a5)<br>    |
|  18|[0x800022d4]<br>0x00000000|- rs1 : x31<br> - rd : f28<br>                                                                                |[0x800003c8]:fld ft8, 2048(t6)<br> [0x800003cc]:addi zero, zero, 0<br> [0x800003d0]:addi zero, zero, 0<br> [0x800003d4]:csrrs a7, fflags, zero<br> [0x800003d8]:fsd ft8, 112(a5)<br> [0x800003dc]:sw a7, 116(a5)<br>   |
|  19|[0x800022e4]<br>0x00000000|- rs1 : x29<br> - rd : f6<br>                                                                                 |[0x800003ec]:fld ft6, 2048(t4)<br> [0x800003f0]:addi zero, zero, 0<br> [0x800003f4]:addi zero, zero, 0<br> [0x800003f8]:csrrs a7, fflags, zero<br> [0x800003fc]:fsd ft6, 128(a5)<br> [0x80000400]:sw a7, 132(a5)<br>   |
|  20|[0x800022f4]<br>0x00000000|- rs1 : x21<br> - rd : f31<br>                                                                                |[0x80000410]:fld ft11, 2048(s5)<br> [0x80000414]:addi zero, zero, 0<br> [0x80000418]:addi zero, zero, 0<br> [0x8000041c]:csrrs a7, fflags, zero<br> [0x80000420]:fsd ft11, 144(a5)<br> [0x80000424]:sw a7, 148(a5)<br> |
|  21|[0x80002304]<br>0x00000000|- rs1 : x14<br> - rd : f24<br>                                                                                |[0x80000434]:fld fs8, 2048(a4)<br> [0x80000438]:addi zero, zero, 0<br> [0x8000043c]:addi zero, zero, 0<br> [0x80000440]:csrrs a7, fflags, zero<br> [0x80000444]:fsd fs8, 160(a5)<br> [0x80000448]:sw a7, 164(a5)<br>   |
