
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000420')]      |
| SIG_REGION                | [('0x80002310', '0x80002410', '64 words')]      |
| COV_LABELS                | fcvt.d.s_b29      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.d.s/riscof_work/fcvt.d.s_b29-01.S/ref.S    |
| Total Number of coverpoints| 88     |
| Total Coverpoints Hit     | 51      |
| Total Signature Updates   | 16      |
| STAT1                     | 16      |
| STAT2                     | 0      |
| STAT3                     | 15     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000029c]:fcvt.d.s ft5, fs8, rne
[0x800002a0]:csrrs a7, fflags, zero
[0x800002a4]:fsd ft5, 256(a5)
[0x800002a8]:sw a7, 260(a5)
[0x800002ac]:fld fa4, 136(a6)
[0x800002b0]:csrrwi zero, frm, 0

[0x800002b4]:fcvt.d.s fs10, fa4, rne
[0x800002b8]:csrrs a7, fflags, zero
[0x800002bc]:fsd fs10, 272(a5)
[0x800002c0]:sw a7, 276(a5)
[0x800002c4]:fld fa7, 144(a6)
[0x800002c8]:csrrwi zero, frm, 0

[0x800002cc]:fcvt.d.s fs11, fa7, rne
[0x800002d0]:csrrs a7, fflags, zero
[0x800002d4]:fsd fs11, 288(a5)
[0x800002d8]:sw a7, 292(a5)
[0x800002dc]:fld fa5, 152(a6)
[0x800002e0]:csrrwi zero, frm, 0

[0x800002e4]:fcvt.d.s fs3, fa5, rne
[0x800002e8]:csrrs a7, fflags, zero
[0x800002ec]:fsd fs3, 304(a5)
[0x800002f0]:sw a7, 308(a5)
[0x800002f4]:fld fs6, 160(a6)
[0x800002f8]:csrrwi zero, frm, 0

[0x800002fc]:fcvt.d.s fa3, fs6, rne
[0x80000300]:csrrs a7, fflags, zero
[0x80000304]:fsd fa3, 320(a5)
[0x80000308]:sw a7, 324(a5)
[0x8000030c]:fld fa2, 168(a6)
[0x80000310]:csrrwi zero, frm, 0

[0x80000314]:fcvt.d.s fs5, fa2, rne
[0x80000318]:csrrs a7, fflags, zero
[0x8000031c]:fsd fs5, 336(a5)
[0x80000320]:sw a7, 340(a5)
[0x80000324]:fld fs5, 176(a6)
[0x80000328]:csrrwi zero, frm, 0

[0x8000032c]:fcvt.d.s fs4, fs5, rne
[0x80000330]:csrrs a7, fflags, zero
[0x80000334]:fsd fs4, 352(a5)
[0x80000338]:sw a7, 356(a5)
[0x8000033c]:fld fs4, 184(a6)
[0x80000340]:csrrwi zero, frm, 0

[0x80000344]:fcvt.d.s fa7, fs4, rne
[0x80000348]:csrrs a7, fflags, zero
[0x8000034c]:fsd fa7, 368(a5)
[0x80000350]:sw a7, 372(a5)
[0x80000354]:fld ft10, 192(a6)
[0x80000358]:csrrwi zero, frm, 0

[0x8000035c]:fcvt.d.s ft3, ft10, rne
[0x80000360]:csrrs a7, fflags, zero
[0x80000364]:fsd ft3, 384(a5)
[0x80000368]:sw a7, 388(a5)
[0x8000036c]:fld ft3, 200(a6)
[0x80000370]:csrrwi zero, frm, 0

[0x80000374]:fcvt.d.s ft10, ft3, rne
[0x80000378]:csrrs a7, fflags, zero
[0x8000037c]:fsd ft10, 400(a5)
[0x80000380]:sw a7, 404(a5)
[0x80000384]:fld ft9, 208(a6)
[0x80000388]:csrrwi zero, frm, 0

[0x8000038c]:fcvt.d.s ft11, ft9, rne
[0x80000390]:csrrs a7, fflags, zero
[0x80000394]:fsd ft11, 416(a5)
[0x80000398]:sw a7, 420(a5)
[0x8000039c]:fld ft6, 216(a6)
[0x800003a0]:csrrwi zero, frm, 0

[0x800003a4]:fcvt.d.s fs9, ft6, rne
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:fsd fs9, 432(a5)
[0x800003b0]:sw a7, 436(a5)
[0x800003b4]:fld ft11, 224(a6)
[0x800003b8]:csrrwi zero, frm, 0

[0x800003bc]:fcvt.d.s fs7, ft11, rne
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:fsd fs7, 448(a5)
[0x800003c8]:sw a7, 452(a5)
[0x800003cc]:fld fa3, 232(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fcvt.d.s ft4, fa3, rne
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:fsd ft4, 464(a5)
[0x800003e0]:sw a7, 468(a5)
[0x800003e4]:fld fa1, 240(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fcvt.d.s fa6, fa1, rne
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:fsd fa6, 480(a5)
[0x800003f8]:sw a7, 484(a5)
[0x800003fc]:fld fs11, 248(a6)
[0x80000400]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                   coverpoints                                                                    |                                                                       code                                                                        |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000|- opcode : fcvt.d.s<br> - rs1 : f10<br> - rd : f10<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.d.s fa0, fa0, rne<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd fa0, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>      |
|   2|[0x80002324]<br>0x00000000|- rs1 : f4<br> - rd : f7<br> - rs1 != rd<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923bf and rm_val == 0  #nosat<br>                           |[0x80000134]:fcvt.d.s ft7, ft4, rne<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd ft7, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>    |
|   3|[0x80002334]<br>0x00000000|- rs1 : f2<br> - rd : f22<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923be and rm_val == 0  #nosat<br>                                          |[0x8000014c]:fcvt.d.s fs6, ft2, rne<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd fs6, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>    |
|   4|[0x80002344]<br>0x00000000|- rs1 : f28<br> - rd : f14<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923bd and rm_val == 0  #nosat<br>                                         |[0x80000164]:fcvt.d.s fa4, ft8, rne<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fa4, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>    |
|   5|[0x80002354]<br>0x00000000|- rs1 : f18<br> - rd : f12<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923bc and rm_val == 0  #nosat<br>                                         |[0x8000017c]:fcvt.d.s fa2, fs2, rne<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fa2, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>    |
|   6|[0x80002364]<br>0x00000000|- rs1 : f8<br> - rd : f1<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923bb and rm_val == 0  #nosat<br>                                           |[0x80000194]:fcvt.d.s ft1, fs0, rne<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd ft1, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>    |
|   7|[0x80002374]<br>0x00000000|- rs1 : f23<br> - rd : f29<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923ba and rm_val == 0  #nosat<br>                                         |[0x800001ac]:fcvt.d.s ft9, fs7, rne<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft9, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>   |
|   8|[0x80002384]<br>0x00000000|- rs1 : f0<br> - rd : f15<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923b9 and rm_val == 0  #nosat<br>                                          |[0x800001c4]:fcvt.d.s fa5, ft0, rne<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fa5, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>  |
|   9|[0x80002394]<br>0x00000000|- rs1 : f16<br> - rd : f8<br> - fs1 == 1 and fe1 == 0x7c and fm1 == 0x4923b8 and rm_val == 0  #nosat<br>                                          |[0x800001dc]:fcvt.d.s fs0, fa6, rne<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd fs0, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>  |
|  10|[0x800023a4]<br>0x00000000|- rs1 : f26<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923bf and rm_val == 0  #nosat<br>                                          |[0x800001f4]:fcvt.d.s ft2, fs10, rne<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd ft2, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br> |
|  11|[0x800023b4]<br>0x00000000|- rs1 : f5<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923be and rm_val == 0  #nosat<br>                                          |[0x8000020c]:fcvt.d.s ft8, ft5, rne<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd ft8, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>  |
|  12|[0x800023c4]<br>0x00000000|- rs1 : f9<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923bd and rm_val == 0  #nosat<br>                                           |[0x80000224]:fcvt.d.s ft6, fs1, rne<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft6, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>  |
|  13|[0x800023d4]<br>0x00000000|- rs1 : f1<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923bc and rm_val == 0  #nosat<br>                                          |[0x8000023c]:fcvt.d.s fs2, ft1, rne<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd fs2, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>  |
|  14|[0x800023e4]<br>0x00000000|- rs1 : f25<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923bb and rm_val == 0  #nosat<br>                                         |[0x80000254]:fcvt.d.s fs8, fs9, rne<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs8, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>  |
|  15|[0x800023f4]<br>0x00000000|- rs1 : f19<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923ba and rm_val == 0  #nosat<br>                                          |[0x8000026c]:fcvt.d.s ft0, fs3, rne<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd ft0, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>  |
|  16|[0x80002404]<br>0x00000000|- rs1 : f7<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7c and fm1 == 0x4923b9 and rm_val == 0  #nosat<br>                                           |[0x80000284]:fcvt.d.s fs1, ft7, rne<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fs1, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>  |
