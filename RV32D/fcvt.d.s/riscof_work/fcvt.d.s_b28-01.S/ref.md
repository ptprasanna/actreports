
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000420')]      |
| SIG_REGION                | [('0x80002310', '0x80002410', '64 words')]      |
| COV_LABELS                | fcvt.d.s_b28      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.d.s/riscof_work/fcvt.d.s_b28-01.S/ref.S    |
| Total Number of coverpoints| 100     |
| Total Coverpoints Hit     | 51      |
| Total Signature Updates   | 16      |
| STAT1                     | 16      |
| STAT2                     | 0      |
| STAT3                     | 15     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000029c]:fcvt.d.s ft4, fs8, rne
[0x800002a0]:csrrs a7, fflags, zero
[0x800002a4]:fsd ft4, 256(a5)
[0x800002a8]:sw a7, 260(a5)
[0x800002ac]:fld ft7, 136(a6)
[0x800002b0]:csrrwi zero, frm, 0

[0x800002b4]:fcvt.d.s fa1, ft7, rne
[0x800002b8]:csrrs a7, fflags, zero
[0x800002bc]:fsd fa1, 272(a5)
[0x800002c0]:sw a7, 276(a5)
[0x800002c4]:fld fa3, 144(a6)
[0x800002c8]:csrrwi zero, frm, 0

[0x800002cc]:fcvt.d.s fs5, fa3, rne
[0x800002d0]:csrrs a7, fflags, zero
[0x800002d4]:fsd fs5, 288(a5)
[0x800002d8]:sw a7, 292(a5)
[0x800002dc]:fld ft8, 152(a6)
[0x800002e0]:csrrwi zero, frm, 0

[0x800002e4]:fcvt.d.s ft5, ft8, rne
[0x800002e8]:csrrs a7, fflags, zero
[0x800002ec]:fsd ft5, 304(a5)
[0x800002f0]:sw a7, 308(a5)
[0x800002f4]:fld fa2, 160(a6)
[0x800002f8]:csrrwi zero, frm, 0

[0x800002fc]:fcvt.d.s fs7, fa2, rne
[0x80000300]:csrrs a7, fflags, zero
[0x80000304]:fsd fs7, 320(a5)
[0x80000308]:sw a7, 324(a5)
[0x8000030c]:fld ft1, 168(a6)
[0x80000310]:csrrwi zero, frm, 0

[0x80000314]:fcvt.d.s fa3, ft1, rne
[0x80000318]:csrrs a7, fflags, zero
[0x8000031c]:fsd fa3, 336(a5)
[0x80000320]:sw a7, 340(a5)
[0x80000324]:fld fs4, 176(a6)
[0x80000328]:csrrwi zero, frm, 0

[0x8000032c]:fcvt.d.s fs8, fs4, rne
[0x80000330]:csrrs a7, fflags, zero
[0x80000334]:fsd fs8, 352(a5)
[0x80000338]:sw a7, 356(a5)
[0x8000033c]:fld fs10, 184(a6)
[0x80000340]:csrrwi zero, frm, 0

[0x80000344]:fcvt.d.s fa7, fs10, rne
[0x80000348]:csrrs a7, fflags, zero
[0x8000034c]:fsd fa7, 368(a5)
[0x80000350]:sw a7, 372(a5)
[0x80000354]:fld fa0, 192(a6)
[0x80000358]:csrrwi zero, frm, 0

[0x8000035c]:fcvt.d.s ft8, fa0, rne
[0x80000360]:csrrs a7, fflags, zero
[0x80000364]:fsd ft8, 384(a5)
[0x80000368]:sw a7, 388(a5)
[0x8000036c]:fld fs2, 200(a6)
[0x80000370]:csrrwi zero, frm, 0

[0x80000374]:fcvt.d.s ft9, fs2, rne
[0x80000378]:csrrs a7, fflags, zero
[0x8000037c]:fsd ft9, 400(a5)
[0x80000380]:sw a7, 404(a5)
[0x80000384]:fld ft5, 208(a6)
[0x80000388]:csrrwi zero, frm, 0

[0x8000038c]:fcvt.d.s ft0, ft5, rne
[0x80000390]:csrrs a7, fflags, zero
[0x80000394]:fsd ft0, 416(a5)
[0x80000398]:sw a7, 420(a5)
[0x8000039c]:fld ft11, 216(a6)
[0x800003a0]:csrrwi zero, frm, 0

[0x800003a4]:fcvt.d.s ft6, ft11, rne
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:fsd ft6, 432(a5)
[0x800003b0]:sw a7, 436(a5)
[0x800003b4]:fld fa7, 224(a6)
[0x800003b8]:csrrwi zero, frm, 0

[0x800003bc]:fcvt.d.s fs11, fa7, rne
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:fsd fs11, 448(a5)
[0x800003c8]:sw a7, 452(a5)
[0x800003cc]:fld fs0, 232(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fcvt.d.s fs6, fs0, rne
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:fsd fs6, 464(a5)
[0x800003e0]:sw a7, 468(a5)
[0x800003e4]:fld ft6, 240(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fcvt.d.s ft10, ft6, rne
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:fsd ft10, 480(a5)
[0x800003f8]:sw a7, 484(a5)
[0x800003fc]:fld ft9, 248(a6)
[0x80000400]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                   coverpoints                                                                    |                                                                        code                                                                        |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000|- opcode : fcvt.d.s<br> - rs1 : f25<br> - rd : f25<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.d.s fs9, fs9, rne<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd fs9, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x80002324]<br>0x00000000|- rs1 : f9<br> - rd : f19<br> - rs1 != rd<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and rm_val == 0  #nosat<br>                          |[0x80000134]:fcvt.d.s fs3, fs1, rne<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd fs3, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>     |
|   3|[0x80002334]<br>0x00000000|- rs1 : f4<br> - rd : f2<br> - fs1 == 1 and fe1 == 0x9e and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                           |[0x8000014c]:fcvt.d.s ft2, ft4, rne<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd ft2, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>     |
|   4|[0x80002344]<br>0x00000000|- rs1 : f3<br> - rd : f1<br> - fs1 == 1 and fe1 == 0x9d and fm1 == 0x4b3d25 and rm_val == 0  #nosat<br>                                           |[0x80000164]:fcvt.d.s ft1, ft3, rne<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft1, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>     |
|   5|[0x80002354]<br>0x00000000|- rs1 : f27<br> - rd : f8<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x200000 and rm_val == 0  #nosat<br>                                          |[0x8000017c]:fcvt.d.s fs0, fs11, rne<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fs0, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>    |
|   6|[0x80002364]<br>0x00000000|- rs1 : f16<br> - rd : f15<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x400000 and rm_val == 0  #nosat<br>                                         |[0x80000194]:fcvt.d.s fa5, fa6, rne<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd fa5, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>     |
|   7|[0x80002374]<br>0x00000000|- rs1 : f0<br> - rd : f10<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x600000 and rm_val == 0  #nosat<br>                                          |[0x800001ac]:fcvt.d.s fa0, ft0, rne<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fa0, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>    |
|   8|[0x80002384]<br>0x00000000|- rs1 : f2<br> - rd : f18<br> - fs1 == 1 and fe1 == 0x80 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                          |[0x800001c4]:fcvt.d.s fs2, ft2, rne<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs2, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>   |
|   9|[0x80002394]<br>0x00000000|- rs1 : f14<br> - rd : f20<br> - fs1 == 1 and fe1 == 0x80 and fm1 == 0x100000 and rm_val == 0  #nosat<br>                                         |[0x800001dc]:fcvt.d.s fs4, fa4, rne<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd fs4, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>   |
|  10|[0x800023a4]<br>0x00000000|- rs1 : f21<br> - rd : f12<br> - fs1 == 1 and fe1 == 0x80 and fm1 == 0x200000 and rm_val == 0  #nosat<br>                                         |[0x800001f4]:fcvt.d.s fa2, fs5, rne<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd fa2, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br>   |
|  11|[0x800023b4]<br>0x00000000|- rs1 : f19<br> - rd : f3<br> - fs1 == 1 and fe1 == 0x80 and fm1 == 0x300000 and rm_val == 0  #nosat<br>                                          |[0x8000020c]:fcvt.d.s ft3, fs3, rne<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd ft3, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>   |
|  12|[0x800023c4]<br>0x00000000|- rs1 : f23<br> - rd : f9<br> - fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                          |[0x80000224]:fcvt.d.s fs1, fs7, rne<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs1, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>   |
|  13|[0x800023d4]<br>0x00000000|- rs1 : f30<br> - rd : f7<br> - fs1 == 1 and fe1 == 0x7d and fm1 == 0x58046a and rm_val == 0  #nosat<br>                                          |[0x8000023c]:fcvt.d.s ft7, ft10, rne<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd ft7, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>  |
|  14|[0x800023e4]<br>0x00000000|- rs1 : f15<br> - rd : f26<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and rm_val == 0  #nosat<br>                                         |[0x80000254]:fcvt.d.s fs10, fa5, rne<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs10, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br> |
|  15|[0x800023f4]<br>0x00000000|- rs1 : f11<br> - rd : f14<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and rm_val == 0  #nosat<br>                                         |[0x8000026c]:fcvt.d.s fa4, fa1, rne<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd fa4, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>   |
|  16|[0x80002404]<br>0x00000000|- rs1 : f22<br> - rd : f31<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and rm_val == 0  #nosat<br>                                         |[0x80000284]:fcvt.d.s ft11, fs6, rne<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd ft11, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br> |
