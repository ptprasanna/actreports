
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001b30')]      |
| SIG_REGION                | [('0x80004510', '0x80004b90', '416 words')]      |
| COV_LABELS                | fnmsub_b16      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fnmsub1/riscof_work/fnmsub_b16-01.S/ref.S    |
| Total Number of coverpoints| 348     |
| Total Coverpoints Hit     | 278      |
| Total Signature Updates   | 145      |
| STAT1                     | 145      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000e28]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000e2c]:csrrs a7, fflags, zero
[0x80000e30]:fsd ft11, 1664(a5)
[0x80000e34]:sw a7, 1668(a5)
[0x80000e38]:fld ft10, 480(a6)
[0x80000e3c]:fld ft9, 488(a6)
[0x80000e40]:fld ft8, 496(a6)
[0x80000e44]:csrrwi zero, frm, 0

[0x80000e48]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000e4c]:csrrs a7, fflags, zero
[0x80000e50]:fsd ft11, 1680(a5)
[0x80000e54]:sw a7, 1684(a5)
[0x80000e58]:fld ft10, 504(a6)
[0x80000e5c]:fld ft9, 512(a6)
[0x80000e60]:fld ft8, 520(a6)
[0x80000e64]:csrrwi zero, frm, 0

[0x80000e68]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000e6c]:csrrs a7, fflags, zero
[0x80000e70]:fsd ft11, 1696(a5)
[0x80000e74]:sw a7, 1700(a5)
[0x80000e78]:fld ft10, 528(a6)
[0x80000e7c]:fld ft9, 536(a6)
[0x80000e80]:fld ft8, 544(a6)
[0x80000e84]:csrrwi zero, frm, 0

[0x80000e88]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000e8c]:csrrs a7, fflags, zero
[0x80000e90]:fsd ft11, 1712(a5)
[0x80000e94]:sw a7, 1716(a5)
[0x80000e98]:fld ft10, 552(a6)
[0x80000e9c]:fld ft9, 560(a6)
[0x80000ea0]:fld ft8, 568(a6)
[0x80000ea4]:csrrwi zero, frm, 0

[0x80000ea8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000eac]:csrrs a7, fflags, zero
[0x80000eb0]:fsd ft11, 1728(a5)
[0x80000eb4]:sw a7, 1732(a5)
[0x80000eb8]:fld ft10, 576(a6)
[0x80000ebc]:fld ft9, 584(a6)
[0x80000ec0]:fld ft8, 592(a6)
[0x80000ec4]:csrrwi zero, frm, 0

[0x80000ec8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000ecc]:csrrs a7, fflags, zero
[0x80000ed0]:fsd ft11, 1744(a5)
[0x80000ed4]:sw a7, 1748(a5)
[0x80000ed8]:fld ft10, 600(a6)
[0x80000edc]:fld ft9, 608(a6)
[0x80000ee0]:fld ft8, 616(a6)
[0x80000ee4]:csrrwi zero, frm, 0

[0x80000ee8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 1760(a5)
[0x80000ef4]:sw a7, 1764(a5)
[0x80000ef8]:fld ft10, 624(a6)
[0x80000efc]:fld ft9, 632(a6)
[0x80000f00]:fld ft8, 640(a6)
[0x80000f04]:csrrwi zero, frm, 0

[0x80000f08]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f0c]:csrrs a7, fflags, zero
[0x80000f10]:fsd ft11, 1776(a5)
[0x80000f14]:sw a7, 1780(a5)
[0x80000f18]:fld ft10, 648(a6)
[0x80000f1c]:fld ft9, 656(a6)
[0x80000f20]:fld ft8, 664(a6)
[0x80000f24]:csrrwi zero, frm, 0

[0x80000f28]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f2c]:csrrs a7, fflags, zero
[0x80000f30]:fsd ft11, 1792(a5)
[0x80000f34]:sw a7, 1796(a5)
[0x80000f38]:fld ft10, 672(a6)
[0x80000f3c]:fld ft9, 680(a6)
[0x80000f40]:fld ft8, 688(a6)
[0x80000f44]:csrrwi zero, frm, 0

[0x80000f48]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f4c]:csrrs a7, fflags, zero
[0x80000f50]:fsd ft11, 1808(a5)
[0x80000f54]:sw a7, 1812(a5)
[0x80000f58]:fld ft10, 696(a6)
[0x80000f5c]:fld ft9, 704(a6)
[0x80000f60]:fld ft8, 712(a6)
[0x80000f64]:csrrwi zero, frm, 0

[0x80000f68]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f6c]:csrrs a7, fflags, zero
[0x80000f70]:fsd ft11, 1824(a5)
[0x80000f74]:sw a7, 1828(a5)
[0x80000f78]:fld ft10, 720(a6)
[0x80000f7c]:fld ft9, 728(a6)
[0x80000f80]:fld ft8, 736(a6)
[0x80000f84]:csrrwi zero, frm, 0

[0x80000f88]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f8c]:csrrs a7, fflags, zero
[0x80000f90]:fsd ft11, 1840(a5)
[0x80000f94]:sw a7, 1844(a5)
[0x80000f98]:fld ft10, 744(a6)
[0x80000f9c]:fld ft9, 752(a6)
[0x80000fa0]:fld ft8, 760(a6)
[0x80000fa4]:csrrwi zero, frm, 0

[0x80000fa8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fac]:csrrs a7, fflags, zero
[0x80000fb0]:fsd ft11, 1856(a5)
[0x80000fb4]:sw a7, 1860(a5)
[0x80000fb8]:fld ft10, 768(a6)
[0x80000fbc]:fld ft9, 776(a6)
[0x80000fc0]:fld ft8, 784(a6)
[0x80000fc4]:csrrwi zero, frm, 0

[0x80000fc8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs a7, fflags, zero
[0x80000fd0]:fsd ft11, 1872(a5)
[0x80000fd4]:sw a7, 1876(a5)
[0x80000fd8]:fld ft10, 792(a6)
[0x80000fdc]:fld ft9, 800(a6)
[0x80000fe0]:fld ft8, 808(a6)
[0x80000fe4]:csrrwi zero, frm, 0

[0x80000fe8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fec]:csrrs a7, fflags, zero
[0x80000ff0]:fsd ft11, 1888(a5)
[0x80000ff4]:sw a7, 1892(a5)
[0x80000ff8]:fld ft10, 816(a6)
[0x80000ffc]:fld ft9, 824(a6)
[0x80001000]:fld ft8, 832(a6)
[0x80001004]:csrrwi zero, frm, 0

[0x80001008]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000100c]:csrrs a7, fflags, zero
[0x80001010]:fsd ft11, 1904(a5)
[0x80001014]:sw a7, 1908(a5)
[0x80001018]:fld ft10, 840(a6)
[0x8000101c]:fld ft9, 848(a6)
[0x80001020]:fld ft8, 856(a6)
[0x80001024]:csrrwi zero, frm, 0

[0x80001028]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000102c]:csrrs a7, fflags, zero
[0x80001030]:fsd ft11, 1920(a5)
[0x80001034]:sw a7, 1924(a5)
[0x80001038]:fld ft10, 864(a6)
[0x8000103c]:fld ft9, 872(a6)
[0x80001040]:fld ft8, 880(a6)
[0x80001044]:csrrwi zero, frm, 0

[0x80001048]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000104c]:csrrs a7, fflags, zero
[0x80001050]:fsd ft11, 1936(a5)
[0x80001054]:sw a7, 1940(a5)
[0x80001058]:fld ft10, 888(a6)
[0x8000105c]:fld ft9, 896(a6)
[0x80001060]:fld ft8, 904(a6)
[0x80001064]:csrrwi zero, frm, 0

[0x80001068]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000106c]:csrrs a7, fflags, zero
[0x80001070]:fsd ft11, 1952(a5)
[0x80001074]:sw a7, 1956(a5)
[0x80001078]:fld ft10, 912(a6)
[0x8000107c]:fld ft9, 920(a6)
[0x80001080]:fld ft8, 928(a6)
[0x80001084]:csrrwi zero, frm, 0

[0x80001088]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000108c]:csrrs a7, fflags, zero
[0x80001090]:fsd ft11, 1968(a5)
[0x80001094]:sw a7, 1972(a5)
[0x80001098]:fld ft10, 936(a6)
[0x8000109c]:fld ft9, 944(a6)
[0x800010a0]:fld ft8, 952(a6)
[0x800010a4]:csrrwi zero, frm, 0

[0x800010a8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010ac]:csrrs a7, fflags, zero
[0x800010b0]:fsd ft11, 1984(a5)
[0x800010b4]:sw a7, 1988(a5)
[0x800010b8]:fld ft10, 960(a6)
[0x800010bc]:fld ft9, 968(a6)
[0x800010c0]:fld ft8, 976(a6)
[0x800010c4]:csrrwi zero, frm, 0

[0x800010c8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010cc]:csrrs a7, fflags, zero
[0x800010d0]:fsd ft11, 2000(a5)
[0x800010d4]:sw a7, 2004(a5)
[0x800010d8]:fld ft10, 984(a6)
[0x800010dc]:fld ft9, 992(a6)
[0x800010e0]:fld ft8, 1000(a6)
[0x800010e4]:csrrwi zero, frm, 0

[0x800010e8]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs a7, fflags, zero
[0x800010f0]:fsd ft11, 2016(a5)
[0x800010f4]:sw a7, 2020(a5)
[0x800010f8]:auipc a5, 4
[0x800010fc]:addi a5, a5, 2064
[0x80001100]:fld ft10, 1008(a6)
[0x80001104]:fld ft9, 1016(a6)
[0x80001108]:fld ft8, 1024(a6)
[0x8000110c]:csrrwi zero, frm, 0

[0x80001630]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001634]:csrrs a7, fflags, zero
[0x80001638]:fsd ft11, 656(a5)
[0x8000163c]:sw a7, 660(a5)
[0x80001640]:fld ft10, 2016(a6)
[0x80001644]:fld ft9, 2024(a6)
[0x80001648]:fld ft8, 2032(a6)
[0x8000164c]:csrrwi zero, frm, 0

[0x80001650]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001654]:csrrs a7, fflags, zero
[0x80001658]:fsd ft11, 672(a5)
[0x8000165c]:sw a7, 676(a5)
[0x80001660]:addi a6, a6, 2040
[0x80001664]:fld ft10, 0(a6)
[0x80001668]:fld ft9, 8(a6)
[0x8000166c]:fld ft8, 16(a6)
[0x80001670]:csrrwi zero, frm, 0

[0x80001674]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001678]:csrrs a7, fflags, zero
[0x8000167c]:fsd ft11, 688(a5)
[0x80001680]:sw a7, 692(a5)
[0x80001684]:fld ft10, 24(a6)
[0x80001688]:fld ft9, 32(a6)
[0x8000168c]:fld ft8, 40(a6)
[0x80001690]:csrrwi zero, frm, 0

[0x80001694]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001698]:csrrs a7, fflags, zero
[0x8000169c]:fsd ft11, 704(a5)
[0x800016a0]:sw a7, 708(a5)
[0x800016a4]:fld ft10, 48(a6)
[0x800016a8]:fld ft9, 56(a6)
[0x800016ac]:fld ft8, 64(a6)
[0x800016b0]:csrrwi zero, frm, 0

[0x800016b4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800016b8]:csrrs a7, fflags, zero
[0x800016bc]:fsd ft11, 720(a5)
[0x800016c0]:sw a7, 724(a5)
[0x800016c4]:fld ft10, 72(a6)
[0x800016c8]:fld ft9, 80(a6)
[0x800016cc]:fld ft8, 88(a6)
[0x800016d0]:csrrwi zero, frm, 0

[0x800016d4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800016d8]:csrrs a7, fflags, zero
[0x800016dc]:fsd ft11, 736(a5)
[0x800016e0]:sw a7, 740(a5)
[0x800016e4]:fld ft10, 96(a6)
[0x800016e8]:fld ft9, 104(a6)
[0x800016ec]:fld ft8, 112(a6)
[0x800016f0]:csrrwi zero, frm, 0

[0x800016f4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800016f8]:csrrs a7, fflags, zero
[0x800016fc]:fsd ft11, 752(a5)
[0x80001700]:sw a7, 756(a5)
[0x80001704]:fld ft10, 120(a6)
[0x80001708]:fld ft9, 128(a6)
[0x8000170c]:fld ft8, 136(a6)
[0x80001710]:csrrwi zero, frm, 0

[0x80001714]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001718]:csrrs a7, fflags, zero
[0x8000171c]:fsd ft11, 768(a5)
[0x80001720]:sw a7, 772(a5)
[0x80001724]:fld ft10, 144(a6)
[0x80001728]:fld ft9, 152(a6)
[0x8000172c]:fld ft8, 160(a6)
[0x80001730]:csrrwi zero, frm, 0

[0x80001734]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001738]:csrrs a7, fflags, zero
[0x8000173c]:fsd ft11, 784(a5)
[0x80001740]:sw a7, 788(a5)
[0x80001744]:fld ft10, 168(a6)
[0x80001748]:fld ft9, 176(a6)
[0x8000174c]:fld ft8, 184(a6)
[0x80001750]:csrrwi zero, frm, 0

[0x80001754]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001758]:csrrs a7, fflags, zero
[0x8000175c]:fsd ft11, 800(a5)
[0x80001760]:sw a7, 804(a5)
[0x80001764]:fld ft10, 192(a6)
[0x80001768]:fld ft9, 200(a6)
[0x8000176c]:fld ft8, 208(a6)
[0x80001770]:csrrwi zero, frm, 0

[0x80001774]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001778]:csrrs a7, fflags, zero
[0x8000177c]:fsd ft11, 816(a5)
[0x80001780]:sw a7, 820(a5)
[0x80001784]:fld ft10, 216(a6)
[0x80001788]:fld ft9, 224(a6)
[0x8000178c]:fld ft8, 232(a6)
[0x80001790]:csrrwi zero, frm, 0

[0x80001794]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001798]:csrrs a7, fflags, zero
[0x8000179c]:fsd ft11, 832(a5)
[0x800017a0]:sw a7, 836(a5)
[0x800017a4]:fld ft10, 240(a6)
[0x800017a8]:fld ft9, 248(a6)
[0x800017ac]:fld ft8, 256(a6)
[0x800017b0]:csrrwi zero, frm, 0

[0x800017b4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017b8]:csrrs a7, fflags, zero
[0x800017bc]:fsd ft11, 848(a5)
[0x800017c0]:sw a7, 852(a5)
[0x800017c4]:fld ft10, 264(a6)
[0x800017c8]:fld ft9, 272(a6)
[0x800017cc]:fld ft8, 280(a6)
[0x800017d0]:csrrwi zero, frm, 0

[0x800017d4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017d8]:csrrs a7, fflags, zero
[0x800017dc]:fsd ft11, 864(a5)
[0x800017e0]:sw a7, 868(a5)
[0x800017e4]:fld ft10, 288(a6)
[0x800017e8]:fld ft9, 296(a6)
[0x800017ec]:fld ft8, 304(a6)
[0x800017f0]:csrrwi zero, frm, 0

[0x800017f4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017f8]:csrrs a7, fflags, zero
[0x800017fc]:fsd ft11, 880(a5)
[0x80001800]:sw a7, 884(a5)
[0x80001804]:fld ft10, 312(a6)
[0x80001808]:fld ft9, 320(a6)
[0x8000180c]:fld ft8, 328(a6)
[0x80001810]:csrrwi zero, frm, 0

[0x80001814]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001818]:csrrs a7, fflags, zero
[0x8000181c]:fsd ft11, 896(a5)
[0x80001820]:sw a7, 900(a5)
[0x80001824]:fld ft10, 336(a6)
[0x80001828]:fld ft9, 344(a6)
[0x8000182c]:fld ft8, 352(a6)
[0x80001830]:csrrwi zero, frm, 0

[0x80001834]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001838]:csrrs a7, fflags, zero
[0x8000183c]:fsd ft11, 912(a5)
[0x80001840]:sw a7, 916(a5)
[0x80001844]:fld ft10, 360(a6)
[0x80001848]:fld ft9, 368(a6)
[0x8000184c]:fld ft8, 376(a6)
[0x80001850]:csrrwi zero, frm, 0

[0x80001854]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001858]:csrrs a7, fflags, zero
[0x8000185c]:fsd ft11, 928(a5)
[0x80001860]:sw a7, 932(a5)
[0x80001864]:fld ft10, 384(a6)
[0x80001868]:fld ft9, 392(a6)
[0x8000186c]:fld ft8, 400(a6)
[0x80001870]:csrrwi zero, frm, 0

[0x80001874]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001878]:csrrs a7, fflags, zero
[0x8000187c]:fsd ft11, 944(a5)
[0x80001880]:sw a7, 948(a5)
[0x80001884]:fld ft10, 408(a6)
[0x80001888]:fld ft9, 416(a6)
[0x8000188c]:fld ft8, 424(a6)
[0x80001890]:csrrwi zero, frm, 0

[0x80001894]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001898]:csrrs a7, fflags, zero
[0x8000189c]:fsd ft11, 960(a5)
[0x800018a0]:sw a7, 964(a5)
[0x800018a4]:fld ft10, 432(a6)
[0x800018a8]:fld ft9, 440(a6)
[0x800018ac]:fld ft8, 448(a6)
[0x800018b0]:csrrwi zero, frm, 0

[0x800018b4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018b8]:csrrs a7, fflags, zero
[0x800018bc]:fsd ft11, 976(a5)
[0x800018c0]:sw a7, 980(a5)
[0x800018c4]:fld ft10, 456(a6)
[0x800018c8]:fld ft9, 464(a6)
[0x800018cc]:fld ft8, 472(a6)
[0x800018d0]:csrrwi zero, frm, 0

[0x800018d4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018d8]:csrrs a7, fflags, zero
[0x800018dc]:fsd ft11, 992(a5)
[0x800018e0]:sw a7, 996(a5)
[0x800018e4]:fld ft10, 480(a6)
[0x800018e8]:fld ft9, 488(a6)
[0x800018ec]:fld ft8, 496(a6)
[0x800018f0]:csrrwi zero, frm, 0

[0x800018f4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018f8]:csrrs a7, fflags, zero
[0x800018fc]:fsd ft11, 1008(a5)
[0x80001900]:sw a7, 1012(a5)
[0x80001904]:fld ft10, 504(a6)
[0x80001908]:fld ft9, 512(a6)
[0x8000190c]:fld ft8, 520(a6)
[0x80001910]:csrrwi zero, frm, 0

[0x80001914]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001918]:csrrs a7, fflags, zero
[0x8000191c]:fsd ft11, 1024(a5)
[0x80001920]:sw a7, 1028(a5)
[0x80001924]:fld ft10, 528(a6)
[0x80001928]:fld ft9, 536(a6)
[0x8000192c]:fld ft8, 544(a6)
[0x80001930]:csrrwi zero, frm, 0

[0x80001934]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001938]:csrrs a7, fflags, zero
[0x8000193c]:fsd ft11, 1040(a5)
[0x80001940]:sw a7, 1044(a5)
[0x80001944]:fld ft10, 552(a6)
[0x80001948]:fld ft9, 560(a6)
[0x8000194c]:fld ft8, 568(a6)
[0x80001950]:csrrwi zero, frm, 0

[0x80001954]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001958]:csrrs a7, fflags, zero
[0x8000195c]:fsd ft11, 1056(a5)
[0x80001960]:sw a7, 1060(a5)
[0x80001964]:fld ft10, 576(a6)
[0x80001968]:fld ft9, 584(a6)
[0x8000196c]:fld ft8, 592(a6)
[0x80001970]:csrrwi zero, frm, 0

[0x80001974]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001978]:csrrs a7, fflags, zero
[0x8000197c]:fsd ft11, 1072(a5)
[0x80001980]:sw a7, 1076(a5)
[0x80001984]:fld ft10, 600(a6)
[0x80001988]:fld ft9, 608(a6)
[0x8000198c]:fld ft8, 616(a6)
[0x80001990]:csrrwi zero, frm, 0

[0x80001994]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001998]:csrrs a7, fflags, zero
[0x8000199c]:fsd ft11, 1088(a5)
[0x800019a0]:sw a7, 1092(a5)
[0x800019a4]:fld ft10, 624(a6)
[0x800019a8]:fld ft9, 632(a6)
[0x800019ac]:fld ft8, 640(a6)
[0x800019b0]:csrrwi zero, frm, 0

[0x800019b4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019b8]:csrrs a7, fflags, zero
[0x800019bc]:fsd ft11, 1104(a5)
[0x800019c0]:sw a7, 1108(a5)
[0x800019c4]:fld ft10, 648(a6)
[0x800019c8]:fld ft9, 656(a6)
[0x800019cc]:fld ft8, 664(a6)
[0x800019d0]:csrrwi zero, frm, 0

[0x800019d4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019d8]:csrrs a7, fflags, zero
[0x800019dc]:fsd ft11, 1120(a5)
[0x800019e0]:sw a7, 1124(a5)
[0x800019e4]:fld ft10, 672(a6)
[0x800019e8]:fld ft9, 680(a6)
[0x800019ec]:fld ft8, 688(a6)
[0x800019f0]:csrrwi zero, frm, 0

[0x800019f4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019f8]:csrrs a7, fflags, zero
[0x800019fc]:fsd ft11, 1136(a5)
[0x80001a00]:sw a7, 1140(a5)
[0x80001a04]:fld ft10, 696(a6)
[0x80001a08]:fld ft9, 704(a6)
[0x80001a0c]:fld ft8, 712(a6)
[0x80001a10]:csrrwi zero, frm, 0

[0x80001a14]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a18]:csrrs a7, fflags, zero
[0x80001a1c]:fsd ft11, 1152(a5)
[0x80001a20]:sw a7, 1156(a5)
[0x80001a24]:fld ft10, 720(a6)
[0x80001a28]:fld ft9, 728(a6)
[0x80001a2c]:fld ft8, 736(a6)
[0x80001a30]:csrrwi zero, frm, 0

[0x80001a34]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a38]:csrrs a7, fflags, zero
[0x80001a3c]:fsd ft11, 1168(a5)
[0x80001a40]:sw a7, 1172(a5)
[0x80001a44]:fld ft10, 744(a6)
[0x80001a48]:fld ft9, 752(a6)
[0x80001a4c]:fld ft8, 760(a6)
[0x80001a50]:csrrwi zero, frm, 0

[0x80001a54]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a58]:csrrs a7, fflags, zero
[0x80001a5c]:fsd ft11, 1184(a5)
[0x80001a60]:sw a7, 1188(a5)
[0x80001a64]:fld ft10, 768(a6)
[0x80001a68]:fld ft9, 776(a6)
[0x80001a6c]:fld ft8, 784(a6)
[0x80001a70]:csrrwi zero, frm, 0

[0x80001a74]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a78]:csrrs a7, fflags, zero
[0x80001a7c]:fsd ft11, 1200(a5)
[0x80001a80]:sw a7, 1204(a5)
[0x80001a84]:fld ft10, 792(a6)
[0x80001a88]:fld ft9, 800(a6)
[0x80001a8c]:fld ft8, 808(a6)
[0x80001a90]:csrrwi zero, frm, 0

[0x80001a94]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a98]:csrrs a7, fflags, zero
[0x80001a9c]:fsd ft11, 1216(a5)
[0x80001aa0]:sw a7, 1220(a5)
[0x80001aa4]:fld ft10, 816(a6)
[0x80001aa8]:fld ft9, 824(a6)
[0x80001aac]:fld ft8, 832(a6)
[0x80001ab0]:csrrwi zero, frm, 0

[0x80001ab4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001ab8]:csrrs a7, fflags, zero
[0x80001abc]:fsd ft11, 1232(a5)
[0x80001ac0]:sw a7, 1236(a5)
[0x80001ac4]:fld ft10, 840(a6)
[0x80001ac8]:fld ft9, 848(a6)
[0x80001acc]:fld ft8, 856(a6)
[0x80001ad0]:csrrwi zero, frm, 0

[0x80001ad4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001ad8]:csrrs a7, fflags, zero
[0x80001adc]:fsd ft11, 1248(a5)
[0x80001ae0]:sw a7, 1252(a5)
[0x80001ae4]:fld ft10, 864(a6)
[0x80001ae8]:fld ft9, 872(a6)
[0x80001aec]:fld ft8, 880(a6)
[0x80001af0]:csrrwi zero, frm, 0

[0x80001af4]:fnmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001af8]:csrrs a7, fflags, zero
[0x80001afc]:fsd ft11, 1264(a5)
[0x80001b00]:sw a7, 1268(a5)
[0x80001b04]:fld ft10, 888(a6)
[0x80001b08]:fld ft9, 896(a6)
[0x80001b0c]:fld ft8, 904(a6)
[0x80001b10]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                        coverpoints                                                                                                                                                                         |                                                                              code                                                                               |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004514]<br>0x00000001|- opcode : fnmsub.d<br> - rs1 : f30<br> - rs2 : f24<br> - rs3 : f24<br> - rd : f24<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                        |[0x80000124]:fnmsub.d fs8, ft10, fs8, fs8, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fs8, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>         |
|   2|[0x80004524]<br>0x00000001|- rs1 : f9<br> - rs2 : f29<br> - rs3 : f23<br> - rd : f28<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x8ff93428ba4ff and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xac9ee205b0abf and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0x88040dc1a880c and rm_val == 0  #nosat<br> |[0x80000144]:fnmsub.d ft8, fs1, ft9, fs7, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd ft8, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80004534]<br>0x00000001|- rs1 : f22<br> - rs2 : f13<br> - rs3 : f22<br> - rd : f22<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                |[0x80000164]:fnmsub.d fs6, fs6, fa3, fs6, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fs6, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80004544]<br>0x00000001|- rs1 : f29<br> - rs2 : f14<br> - rs3 : f8<br> - rd : f29<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xca7049f840037 and fs2 == 0 and fe2 == 0x5f2 and fm2 == 0x61f77377e85ff and fs3 == 0 and fe3 == 0x7ec and fm3 == 0x7f2d009694180 and rm_val == 0  #nosat<br>                               |[0x80000184]:fnmsub.d ft9, ft9, fa4, fs0, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd ft9, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>        |
|   5|[0x80004554]<br>0x00000001|- rs1 : f15<br> - rs2 : f15<br> - rs3 : f19<br> - rd : f15<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                |[0x800001a4]:fnmsub.d fa5, fa5, fa5, fs3, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fa5, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>        |
|   6|[0x80004564]<br>0x00000001|- rs1 : f10<br> - rs2 : f27<br> - rs3 : f2<br> - rd : f2<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xcc7fa2c262245 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x707b78d06c987 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xc50663f0b34df and rm_val == 0  #nosat<br>                                |[0x800001c4]:fnmsub.d ft2, fa0, fs11, ft2, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd ft2, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>       |
|   7|[0x80004574]<br>0x00000001|- rs1 : f17<br> - rs2 : f17<br> - rs3 : f17<br> - rd : f17<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                |[0x800001e4]:fnmsub.d fa7, fa7, fa7, fa7, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa7, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80004584]<br>0x00000001|- rs1 : f21<br> - rs2 : f21<br> - rs3 : f0<br> - rd : f16<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                    |[0x80000204]:fnmsub.d fa6, fs5, fs5, ft0, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fa6, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80004594]<br>0x00000001|- rs1 : f6<br> - rs2 : f10<br> - rs3 : f25<br> - rd : f10<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6607c34459dce and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x132ac57683a83 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x11a3e2c200294 and rm_val == 0  #nosat<br>                               |[0x80000224]:fnmsub.d fa0, ft6, fa0, fs9, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fa0, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>      |
|  10|[0x800045a4]<br>0x00000001|- rs1 : f5<br> - rs2 : f4<br> - rs3 : f5<br> - rd : f11<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                      |[0x80000244]:fnmsub.d fa1, ft5, ft4, ft5, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd fa1, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>      |
|  11|[0x800045b4]<br>0x00000001|- rs1 : f19<br> - rs2 : f7<br> - rs3 : f7<br> - rd : f9<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                      |[0x80000264]:fnmsub.d fs1, fs3, ft7, ft7, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fs1, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>      |
|  12|[0x800045c4]<br>0x00000001|- rs1 : f26<br> - rs2 : f26<br> - rs3 : f26<br> - rd : f14<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                |[0x80000284]:fnmsub.d fa4, fs10, fs10, fs10, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fa4, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>   |
|  13|[0x800045d4]<br>0x00000001|- rs1 : f23<br> - rs2 : f9<br> - rs3 : f6<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x9d05338d20148 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdd0d05440db8f and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x662ba7f57a5d0 and rm_val == 0  #nosat<br>                                                                                            |[0x800002a4]:fnmsub.d ft7, fs7, fs1, ft6, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd ft7, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>      |
|  14|[0x800045e4]<br>0x00000001|- rs1 : f18<br> - rs2 : f3<br> - rs3 : f29<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6e9146c51e452 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7abc88bf9fef0 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x6c5328f4e2d56 and rm_val == 0  #nosat<br>                                                                                          |[0x800002c4]:fnmsub.d fs3, fs2, ft3, ft9, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs3, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>      |
|  15|[0x800045f4]<br>0x00000001|- rs1 : f0<br> - rs2 : f18<br> - rs3 : f31<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xb771f235b3b5a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x33c6c6f19f493 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xfd089154c0c41 and rm_val == 0  #nosat<br>                                                                                          |[0x800002e4]:fnmsub.d fa2, ft0, fs2, ft11, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fa2, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>     |
|  16|[0x80004604]<br>0x00000001|- rs1 : f7<br> - rs2 : f31<br> - rs3 : f28<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xbdb3961600dfc and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x63aa702c60f2f and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x5f358c3520740 and rm_val == 0  #nosat<br>                                                                                           |[0x80000304]:fnmsub.d fs0, ft7, ft11, ft8, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd fs0, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>     |
|  17|[0x80004614]<br>0x00000001|- rs1 : f2<br> - rs2 : f12<br> - rs3 : f11<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x13e8154135a8d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdc11b455bc9be and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x5370259ff8df1 and rm_val == 0  #nosat<br>                                                                                          |[0x80000324]:fnmsub.d fs9, ft2, fa2, fa1, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd fs9, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>      |
|  18|[0x80004624]<br>0x00000001|- rs1 : f27<br> - rs2 : f28<br> - rs3 : f15<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x7089e44f25db5 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xde080c818631b and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x72dedc023a90a and rm_val == 0  #nosat<br>                                                                                          |[0x80000344]:fnmsub.d ft5, fs11, ft8, fa5, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd ft5, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>     |
|  19|[0x80004634]<br>0x00000001|- rs1 : f20<br> - rs2 : f1<br> - rs3 : f14<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x95b24345add6a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x3f1e47bb1b181 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x09a87ab0513d0 and rm_val == 0  #nosat<br>                                                                                          |[0x80000364]:fnmsub.d fs2, fs4, ft1, fa4, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd fs2, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>      |
|  20|[0x80004644]<br>0x00000001|- rs1 : f31<br> - rs2 : f30<br> - rs3 : f9<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xeb04ea6a14a83 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc8bad349c4595 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xf167373f3a798 and rm_val == 0  #nosat<br>                                                                                          |[0x80000384]:fnmsub.d fs7, ft11, ft10, fs1, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd fs7, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>    |
|  21|[0x80004654]<br>0x00000001|- rs1 : f28<br> - rs2 : f20<br> - rs3 : f18<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4abb0d7c973ba and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xc6e2320e480b1 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xcc25f4d938cc3 and rm_val == 0  #nosat<br>                                                                                         |[0x800003a4]:fnmsub.d ft11, ft8, fs4, fs2, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft11, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>    |
|  22|[0x80004664]<br>0x00000001|- rs1 : f3<br> - rs2 : f23<br> - rs3 : f12<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0f20e78d8f9d2 and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x2f9d33403a65f and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0xe27a740758a8e and rm_val == 0  #nosat<br>                                                                                           |[0x800003c4]:fnmsub.d ft6, ft3, fs7, fa2, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd ft6, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>      |
|  23|[0x80004674]<br>0x00000001|- rs1 : f14<br> - rs2 : f2<br> - rs3 : f1<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0c1a806800541 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xd796149975c91 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x26b56e6c526bb and rm_val == 0  #nosat<br>                                                                                           |[0x800003e4]:fnmsub.d fs4, fa4, ft2, ft1, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fs4, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80004684]<br>0x00000001|- rs1 : f11<br> - rs2 : f25<br> - rs3 : f21<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x8a7945b061b5b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x52e97b0ef6836 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x858da164e2118 and rm_val == 0  #nosat<br>                                                                                          |[0x80000404]:fnmsub.d ft1, fa1, fs9, fs5, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd ft1, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>      |
|  25|[0x80004694]<br>0x00000001|- rs1 : f16<br> - rs2 : f6<br> - rs3 : f13<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x33e5fa8c30e93 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7423a2e8941c5 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xc0a4efe244010 and rm_val == 0  #nosat<br>                                                                                          |[0x80000424]:fnmsub.d fs11, fa6, ft6, fa3, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd fs11, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>    |
|  26|[0x800046a4]<br>0x00000001|- rs1 : f4<br> - rs2 : f22<br> - rs3 : f30<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x3296d672a65d1 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb85355aabaa39 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x874e2b9c22e4e and rm_val == 0  #nosat<br>                                                                                          |[0x80000444]:fnmsub.d fs10, ft4, fs6, ft10, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs10, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>   |
|  27|[0x800046b4]<br>0x00000001|- rs1 : f1<br> - rs2 : f8<br> - rs3 : f4<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x446cb61fdc3b2 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xf4364961dc6b6 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x0b4b0ef0151e1 and rm_val == 0  #nosat<br>                                                                                            |[0x80000464]:fnmsub.d fs5, ft1, fs0, ft4, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd fs5, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x800046c4]<br>0x00000001|- rs1 : f24<br> - rs2 : f11<br> - rs3 : f3<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x8857812870f79 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x1aef39f9fe967 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x57393547a45c1 and rm_val == 0  #nosat<br>                                                                                          |[0x80000484]:fnmsub.d fa3, fs8, fa1, ft3, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fa3, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x800046d4]<br>0x00000001|- rs1 : f12<br> - rs2 : f19<br> - rs3 : f27<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x9b3752dc7833d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc84b619a10026 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x65309a7202f70 and rm_val == 0  #nosat<br>                                                                                         |[0x800004a4]:fnmsub.d ft10, fa2, fs3, fs11, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd ft10, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>   |
|  30|[0x800046e4]<br>0x00000001|- rs1 : f13<br> - rs2 : f0<br> - rs3 : f10<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x00277a2f4f0bf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x90362012c0cb9 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x9c80482a6805c and rm_val == 0  #nosat<br>                                                                                           |[0x800004c4]:fnmsub.d ft3, fa3, ft0, fa0, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd ft3, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>      |
|  31|[0x800046f4]<br>0x00000001|- rs1 : f8<br> - rs2 : f5<br> - rs3 : f20<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0e21bf0ae6b53 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2301284b72ea3 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x711ebfa469c8f and rm_val == 0  #nosat<br>                                                                                            |[0x800004e4]:fnmsub.d ft0, fs0, ft5, fs4, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd ft0, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>      |
|  32|[0x80004704]<br>0x00000001|- rs1 : f25<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x2bf911dc09a97 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x97fb42e62b463 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x1d50c466bbf9e and rm_val == 0  #nosat<br>                                                                                                                                        |[0x80000504]:fnmsub.d fs8, fs9, ft2, fs2, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fs8, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>      |
|  33|[0x80004714]<br>0x00000001|- rs2 : f16<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xa1a9bebef0303 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7f55a94838f42 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xf1cca628f268a and rm_val == 0  #nosat<br>                                                                                                                                        |[0x80000524]:fnmsub.d fs3, fa2, fa6, ft5, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd fs3, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>      |
|  34|[0x80004724]<br>0x00000001|- rs3 : f16<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x836b4f912cecf and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xaaa7b255c7729 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x196822539d977 and rm_val == 0  #nosat<br>                                                                                                                                        |[0x80000544]:fnmsub.d fs2, fs1, fs9, fa6, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd fs2, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>      |
|  35|[0x80004734]<br>0x00000001|- rd : f4<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x8ed6de3a3c8fa and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x745f44472c873 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x57b0acf311df6 and rm_val == 0  #nosat<br>                                                                                                                                          |[0x80000564]:fnmsub.d ft4, fa5, fs11, fs9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft4, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>     |
|  36|[0x80004744]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x14faadcb89035 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x98293d5ceb2ef and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0xeb5f64c7077bd and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000584]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80004754]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x695a923fbea6d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb19abcbfb877f and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x5456e59fed14e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005a4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80004764]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x9d04dcb95faaf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x153f3df3c9356 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x8c3dc40054d5a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005c4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80004774]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x89ad7120f1311 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x6a9ba5700822b and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x739d7a56e9fb8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005e4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80004784]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f6 and fm1 == 0x58f8cdea3010f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x026995915c74a and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0xb24d5920b3ef8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000604]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80004794]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x1039f3a2aafd1 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x8894af06d8327 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x86dbfa2d7dad0 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000624]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x800047a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x1615cea4dadaf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7ab1ecd91423c and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x17917ed98a956 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000644]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x800047b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x18f86836ffdff and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x6e81fb8b9962a and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x60e045de5a99d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000664]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x800047c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xd4b051bdbe619 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x3ff0fc641f811 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x5888e51534e0d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000684]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x800047d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x31e2b61ae3886 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x1c1ef9d33c7a6 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x17a651eb8d0fb and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006a4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x800047e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x218e10ebba3c3 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x537c93d28ea11 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x4a517db31106f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006c4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x800047f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4f8cd41d123b3 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xf858063768107 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x3f44c3edd4625 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006e4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80004804]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4303490992183 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x6ece4c5eeb64f and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xb559a62fe1ae3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000704]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80004814]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x21fe0e6f813e1 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x74b1d1a55566b and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x5b62bdb54a80e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000724]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80004824]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x689c0c079e743 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x6fa9ed12d7d63 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xb2a93fae87849 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000744]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80004834]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x12848561a4bfb and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xd00f85557199f and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x73b2da345b419 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000764]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80004844]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x822526bc71dac and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xec32f815aa7b3 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x367c665468d39 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000784]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80004854]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4ee35c3c6e18a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x9cb33db44f779 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x55a1b1bc17fde and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007a4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80004864]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x83bfe21cd38bb and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x2ea078d4d9f6b and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x80172b0401192 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007c4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80004874]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe1f070ecd84ee and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x7f346bd6d6bd3 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x3fa9389bdd569 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007e4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80004884]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xc63da361c24b5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd14940d00f35c and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xc4cc9176642bb and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000804]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80004894]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x195b0309c25cd and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x54b3a41baf155 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xa095261c3e457 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000824]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x800048a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x904d55100df07 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x16b5e65467710 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xe7c0c4191b0ff and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000844]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x800048b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xae46f3a4f112b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2b92ef2f82fa3 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xec455343906ba and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000864]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x800048c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x56d651aa6d989 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xbeb0ceb099365 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xbc71033620f1b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000884]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x800048d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x291d166efb64f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x59dc19d58b8b0 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x942b03f488597 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008a4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x800048e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xaaf8c2ba7ae5d and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x2ac405849fc2d and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x5202e714c20ac and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008c4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x800048f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x753b59b321231 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x5ff3543c663d7 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x1854465d630e5 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008e4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80004904]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xbaee3f82e1a4b and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x40f05297754b9 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x8b2b4931a8148 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000904]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80004914]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xb799d6825a449 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xccad57bbc2fe8 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xf0666506d7659 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000924]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80004924]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x08d4799198efb and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x7f5b3d9f355e3 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x5207f41d9edbb and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000944]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80004934]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xb52ff510445c5 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xefcdd3e3e2557 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xf7a2e75fbb289 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000964]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80004944]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6f74d931bfd19 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x59c795046ed37 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xe6141d033d05c and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000984]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80004954]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xde9cd9749822d and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xd63950e417d4b and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x0f6151b53208d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009a4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80004964]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x99de5ce43d1d9 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7798e778703f2 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x26b346513bcf3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009c4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80004974]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xecd9ba7f13b97 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7100c5c4a1c8c and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x49be161f3bc58 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009e4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80004984]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xab523ab9b6599 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xa4aabca9c0dd9 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x1158fdfe6abd5 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a04]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80004994]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xeb199ad9ca61b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x32b0364d2b1f9 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xafdb230685d28 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a24]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800049a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x8c6489f748893 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x261d116b1d5ef and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xc5688ba46673f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a44]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800049b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x60c36c91e43db and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x711f241e91d41 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x5039457fe9c19 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a64]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800049c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0x7ae042250e45f and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xeaff5366eca4b and fs3 == 0 and fe3 == 0x7f0 and fm3 == 0xf66682f529c03 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a84]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800049d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x2b5705fef5d3c and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x1195ef71d8287 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x8583e627948ff and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000aa4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800049e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x692171f6bd238 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xa71e144c30555 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x1499598e34a19 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ac4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800049f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x61dc1b626f27f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe82764f1e3e7a and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xb532779226ef2 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ae4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80004a04]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x2bd60860ffe3c and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x426763f7a72bf and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xd154db10de256 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b04]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80004a14]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x7d27c822e38f3 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x11c620e043403 and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0xe8cee9c58ed88 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b24]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80004a24]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x08d5803cd8234 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xc5afd2cb1163d and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x936101c2158cc and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b44]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80004a34]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x88a542fef4657 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7f0e259ef1af1 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x198311fb94276 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b64]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80004a44]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x493122d9aad5a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xcb782eb8cdb42 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x5387fa5dc3d0b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b84]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80004a54]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x5903a0fb21b3f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x08c04aebe0346 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x08b3aebab606d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ba4]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80004a64]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x48f3656a4f33b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x61b5886460bee and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x8f1494ddbd331 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000bc8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80004a74]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xf23474bb7cc16 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xa6bd446bfb2c7 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x6765cdf1c1485 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000be8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80004a84]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xac413f7bb0305 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x5508f2c02a8b7 and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0xabf8053f9d27b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c08]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80004a94]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x784a68ccbae49 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x9628b4f82971b and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x9310955e3dd56 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c28]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x80004aa4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x3b61dbeccea83 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x81d06dbc53f22 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x4b39cbde7ef51 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c48]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x80004ab4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xefdfc9ff93d7f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x702c88a55e7a7 and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0xbdb5e0ce16611 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c68]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x80004ac4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x4c075eef493d9 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x20c1a38b8c097 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x5650feeabcaed and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c88]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x80004ad4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x7f78548664fb5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xde7ef4a369968 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x7c49a20636046 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ca8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x80004ae4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x05cc7ce1db3f8 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x6e292e0f40648 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x9e6cf78e30774 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000cc8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x80004af4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x98f56645aea7f and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x291e2bfc27bff and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0xa3728ac925789 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ce8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80004b04]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xac200a6c90fc3 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe7a274d23529b and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x41ef6896a181b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d08]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80004b14]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0xf0c6f00d0a117 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb01954875dbe6 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x5ed0ee287a8c3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d28]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80004b24]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x6ee815d1c73e3 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0x80d12abb5bebf and fs3 == 0 and fe3 == 0x7ef and fm3 == 0xfbd4f6dd5db49 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d48]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80004b34]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x78021920eec47 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x96bb2b5183927 and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0x18d8fb6d62b73 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d68]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80004b44]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x25ecf43c688c3 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x8c719398e006e and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0xdb587e8ea0f70 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d88]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80004b54]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0xa66f0b9f8cc27 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2ceeb7addc7ab and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0xf3031df784e52 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000da8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80004b64]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x55caa186f3c8d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x77ac231460806 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x883011d03136f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000dc8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80004b74]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x53179f7662fcf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc651e321ee763 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x0dab53c0c8b18 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000de8]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80004b84]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xdb485bd8641e5 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xf867fde0ccba7 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0xa8816a080e712 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000e08]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x8000490c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x5d8b8d100d4ad and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x0288c3fc6a2e3 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x4896becffa1e0 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001110]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 106|[0x8000491c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xa7ae3286d0c8f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe0ca2c7a6ffb0 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x40b3f02037def and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001130]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 107|[0x8000492c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x3315db9e9910d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x53671e4145242 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x366e2e845b373 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001150]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 108|[0x8000493c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f4 and fm1 == 0xaeb807b25f33f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7d97bc07bb6a5 and fs3 == 0 and fe3 == 0x7f0 and fm3 == 0x782bd18a15ef7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001170]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 109|[0x8000494c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xcb0ad9f88c3fd and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xcca22e1b83439 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x45a1d240ee7c6 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001190]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 110|[0x8000495c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xcea5e0336397b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x632d61a0c97c8 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x34b8ef57e4607 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011b0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 111|[0x8000496c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x25e6420f5c09f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x9ed1784fa671a and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xf1b262489767c and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011d0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 112|[0x8000497c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xd1d9dedc8d4db and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x16ce13b7494bf and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0x9302003497b29 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011f0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 113|[0x8000498c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x09bc54bda7ca2 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x27fa95459d3d1 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x9c05ead4f204f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001210]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 114|[0x8000499c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x7db5311d3a19f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xac63c4f037629 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x098ec9c62b318 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001230]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 115|[0x800049ac]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe67b863f16ae2 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdad12fade7910 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x13acc1a4cf4b8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001250]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 116|[0x800049bc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xc28c8267d9ab4 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xf94cbf20a6254 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x3ee4f7bfa8720 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001270]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 117|[0x800049cc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x3f7bce331716b and fs2 == 0 and fe2 == 0x5f3 and fm2 == 0x3183ef4678c7f and fs3 == 0 and fe3 == 0x7ed and fm3 == 0x72766c18c9427 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001290]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 118|[0x800049dc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xadd87f48bf1c7 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xa359c4048cb6b and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x52168279e9c80 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012b0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 119|[0x800049ec]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x15aec43b7bf87 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x62aab2512cca5 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x108bda8d88793 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012d0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 120|[0x800049fc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x099a756bd881b and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x2af313c25ada5 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x16321baa11776 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012f0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 121|[0x80004a0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x48ac00057963a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x9ccdd3e7a322c and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x647e03afe5897 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001310]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 122|[0x80004a1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x77fc19dd1d407 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x1d47422b88b69 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x7a99d69de1b5b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001330]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 123|[0x80004a2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x86c42550ad12b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x9d11e7f58461f and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x21ccd9e5c06f2 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001350]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 124|[0x80004a3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xccdb65c979493 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xef491a115c81f and fs3 == 0 and fe3 == 0x7f2 and fm3 == 0x54f69aae033ec and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001370]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 125|[0x80004a4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4ceb420df408f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc51162e460b0e and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xab7e6e68b9501 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001390]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 126|[0x80004a5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xb608b57d7bf4f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x5ab1b114777e7 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x8c7af50268328 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013b0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 127|[0x80004a6c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x55c3845664053 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xaac59c0e5d8ef and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x99c54678908f8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013d0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 128|[0x80004a7c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6568f5c6359d5 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xa124c133921cf and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x80f2cda9c9f33 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013f0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 129|[0x80004a8c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xe8d90453e9cfb and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x7fc89aad95937 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0x06f4b410355ec and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001410]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 130|[0x80004a9c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x3d7504400059d and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xc7c1eba75a687 and fs3 == 0 and fe3 == 0x7f3 and fm3 == 0xfdba8faed90f0 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001430]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 131|[0x80004aac]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0xb913b7c55ffdf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2375c8ebc2475 and fs3 == 0 and fe3 == 0x7f1 and fm3 == 0xb3adc45eedd5a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001450]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 132|[0x80004abc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x339d1964c64f1 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x8236520d6c6fb and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x9b3cb7841f077 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001470]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 133|[0x80004acc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x402cbce109a77 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x7a037fec02fad and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x792f61f3327a7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001490]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 134|[0x80004adc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x2f72b0267e3ba and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x47b4bc16b5bb5 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x08e906aea4387 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014b0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 135|[0x80004aec]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xed366397a0657 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x012632d0614c9 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x68bf7297e9a68 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014d0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 136|[0x80004afc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0x6b7004b70b43f and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0xf03ec3d1c1d8f and fs3 == 0 and fe3 == 0x7ee and fm3 == 0x27314d13a5e49 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014f0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 137|[0x80004b0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x6909f0cdef409 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x69f56211d9e5b and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xbb9c1a2343426 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001510]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 138|[0x80004b1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4132da9546dfd and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xdd70c0ad2c58b and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xf6f99ba4944eb and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001530]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 139|[0x80004b2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x3b49de25b5810 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x43ad2ac887783 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0xa696c6dda9a21 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001550]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 140|[0x80004b3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xa92ce67e64f49 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x28f1335426ef4 and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0xf7103d55fe09a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001570]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 141|[0x80004b4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0ad13253643d4 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x18cbe0d5b0ab6 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x2803b62c23bf9 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001590]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 142|[0x80004b5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xe6ad80efba433 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x34501d37fe38d and fs3 == 0 and fe3 == 0x7f4 and fm3 == 0x21cfa6fc52470 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015b0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 143|[0x80004b6c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x039904d15cfd4 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xb98a4751306d7 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x812176d0bfffa and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015d0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 144|[0x80004b7c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x1e577746908d8 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x3d812c3e43b86 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x5897964725238 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015f0]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 145|[0x80004b8c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0a470198b7628 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x49abc8377a2f1 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x119439f965820 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001610]:fnmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
