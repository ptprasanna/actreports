
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001fb0')]      |
| SIG_REGION                | [('0x80004310', '0x80004bd0', '560 words')]      |
| COV_LABELS                | fmul_b2      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fmul/riscof_work/fmul_b2-01.S/ref.S    |
| Total Number of coverpoints| 384     |
| Total Coverpoints Hit     | 317      |
| Total Signature Updates   | 217      |
| STAT1                     | 217      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000177c]:fmul.d ft11, ft10, ft9, dyn
[0x80001780]:csrrs a7, fflags, zero
[0x80001784]:fsd ft11, 1232(a5)
[0x80001788]:sw a7, 1236(a5)
[0x8000178c]:fld ft10, 1248(a6)
[0x80001790]:fld ft9, 1256(a6)
[0x80001794]:csrrwi zero, frm, 0

[0x80001798]:fmul.d ft11, ft10, ft9, dyn
[0x8000179c]:csrrs a7, fflags, zero
[0x800017a0]:fsd ft11, 1248(a5)
[0x800017a4]:sw a7, 1252(a5)
[0x800017a8]:fld ft10, 1264(a6)
[0x800017ac]:fld ft9, 1272(a6)
[0x800017b0]:csrrwi zero, frm, 0

[0x800017b4]:fmul.d ft11, ft10, ft9, dyn
[0x800017b8]:csrrs a7, fflags, zero
[0x800017bc]:fsd ft11, 1264(a5)
[0x800017c0]:sw a7, 1268(a5)
[0x800017c4]:fld ft10, 1280(a6)
[0x800017c8]:fld ft9, 1288(a6)
[0x800017cc]:csrrwi zero, frm, 0

[0x800017d0]:fmul.d ft11, ft10, ft9, dyn
[0x800017d4]:csrrs a7, fflags, zero
[0x800017d8]:fsd ft11, 1280(a5)
[0x800017dc]:sw a7, 1284(a5)
[0x800017e0]:fld ft10, 1296(a6)
[0x800017e4]:fld ft9, 1304(a6)
[0x800017e8]:csrrwi zero, frm, 0

[0x800017ec]:fmul.d ft11, ft10, ft9, dyn
[0x800017f0]:csrrs a7, fflags, zero
[0x800017f4]:fsd ft11, 1296(a5)
[0x800017f8]:sw a7, 1300(a5)
[0x800017fc]:fld ft10, 1312(a6)
[0x80001800]:fld ft9, 1320(a6)
[0x80001804]:csrrwi zero, frm, 0

[0x80001808]:fmul.d ft11, ft10, ft9, dyn
[0x8000180c]:csrrs a7, fflags, zero
[0x80001810]:fsd ft11, 1312(a5)
[0x80001814]:sw a7, 1316(a5)
[0x80001818]:fld ft10, 1328(a6)
[0x8000181c]:fld ft9, 1336(a6)
[0x80001820]:csrrwi zero, frm, 0

[0x80001824]:fmul.d ft11, ft10, ft9, dyn
[0x80001828]:csrrs a7, fflags, zero
[0x8000182c]:fsd ft11, 1328(a5)
[0x80001830]:sw a7, 1332(a5)
[0x80001834]:fld ft10, 1344(a6)
[0x80001838]:fld ft9, 1352(a6)
[0x8000183c]:csrrwi zero, frm, 0

[0x80001840]:fmul.d ft11, ft10, ft9, dyn
[0x80001844]:csrrs a7, fflags, zero
[0x80001848]:fsd ft11, 1344(a5)
[0x8000184c]:sw a7, 1348(a5)
[0x80001850]:fld ft10, 1360(a6)
[0x80001854]:fld ft9, 1368(a6)
[0x80001858]:csrrwi zero, frm, 0

[0x8000185c]:fmul.d ft11, ft10, ft9, dyn
[0x80001860]:csrrs a7, fflags, zero
[0x80001864]:fsd ft11, 1360(a5)
[0x80001868]:sw a7, 1364(a5)
[0x8000186c]:fld ft10, 1376(a6)
[0x80001870]:fld ft9, 1384(a6)
[0x80001874]:csrrwi zero, frm, 0

[0x80001878]:fmul.d ft11, ft10, ft9, dyn
[0x8000187c]:csrrs a7, fflags, zero
[0x80001880]:fsd ft11, 1376(a5)
[0x80001884]:sw a7, 1380(a5)
[0x80001888]:fld ft10, 1392(a6)
[0x8000188c]:fld ft9, 1400(a6)
[0x80001890]:csrrwi zero, frm, 0

[0x80001894]:fmul.d ft11, ft10, ft9, dyn
[0x80001898]:csrrs a7, fflags, zero
[0x8000189c]:fsd ft11, 1392(a5)
[0x800018a0]:sw a7, 1396(a5)
[0x800018a4]:fld ft10, 1408(a6)
[0x800018a8]:fld ft9, 1416(a6)
[0x800018ac]:csrrwi zero, frm, 0

[0x800018b0]:fmul.d ft11, ft10, ft9, dyn
[0x800018b4]:csrrs a7, fflags, zero
[0x800018b8]:fsd ft11, 1408(a5)
[0x800018bc]:sw a7, 1412(a5)
[0x800018c0]:fld ft10, 1424(a6)
[0x800018c4]:fld ft9, 1432(a6)
[0x800018c8]:csrrwi zero, frm, 0

[0x800018cc]:fmul.d ft11, ft10, ft9, dyn
[0x800018d0]:csrrs a7, fflags, zero
[0x800018d4]:fsd ft11, 1424(a5)
[0x800018d8]:sw a7, 1428(a5)
[0x800018dc]:fld ft10, 1440(a6)
[0x800018e0]:fld ft9, 1448(a6)
[0x800018e4]:csrrwi zero, frm, 0

[0x800018e8]:fmul.d ft11, ft10, ft9, dyn
[0x800018ec]:csrrs a7, fflags, zero
[0x800018f0]:fsd ft11, 1440(a5)
[0x800018f4]:sw a7, 1444(a5)
[0x800018f8]:fld ft10, 1456(a6)
[0x800018fc]:fld ft9, 1464(a6)
[0x80001900]:csrrwi zero, frm, 0

[0x80001904]:fmul.d ft11, ft10, ft9, dyn
[0x80001908]:csrrs a7, fflags, zero
[0x8000190c]:fsd ft11, 1456(a5)
[0x80001910]:sw a7, 1460(a5)
[0x80001914]:fld ft10, 1472(a6)
[0x80001918]:fld ft9, 1480(a6)
[0x8000191c]:csrrwi zero, frm, 0

[0x80001920]:fmul.d ft11, ft10, ft9, dyn
[0x80001924]:csrrs a7, fflags, zero
[0x80001928]:fsd ft11, 1472(a5)
[0x8000192c]:sw a7, 1476(a5)
[0x80001930]:fld ft10, 1488(a6)
[0x80001934]:fld ft9, 1496(a6)
[0x80001938]:csrrwi zero, frm, 0

[0x8000193c]:fmul.d ft11, ft10, ft9, dyn
[0x80001940]:csrrs a7, fflags, zero
[0x80001944]:fsd ft11, 1488(a5)
[0x80001948]:sw a7, 1492(a5)
[0x8000194c]:fld ft10, 1504(a6)
[0x80001950]:fld ft9, 1512(a6)
[0x80001954]:csrrwi zero, frm, 0

[0x80001958]:fmul.d ft11, ft10, ft9, dyn
[0x8000195c]:csrrs a7, fflags, zero
[0x80001960]:fsd ft11, 1504(a5)
[0x80001964]:sw a7, 1508(a5)
[0x80001968]:fld ft10, 1520(a6)
[0x8000196c]:fld ft9, 1528(a6)
[0x80001970]:csrrwi zero, frm, 0

[0x80001974]:fmul.d ft11, ft10, ft9, dyn
[0x80001978]:csrrs a7, fflags, zero
[0x8000197c]:fsd ft11, 1520(a5)
[0x80001980]:sw a7, 1524(a5)
[0x80001984]:fld ft10, 1536(a6)
[0x80001988]:fld ft9, 1544(a6)
[0x8000198c]:csrrwi zero, frm, 0

[0x80001990]:fmul.d ft11, ft10, ft9, dyn
[0x80001994]:csrrs a7, fflags, zero
[0x80001998]:fsd ft11, 1536(a5)
[0x8000199c]:sw a7, 1540(a5)
[0x800019a0]:fld ft10, 1552(a6)
[0x800019a4]:fld ft9, 1560(a6)
[0x800019a8]:csrrwi zero, frm, 0

[0x800019ac]:fmul.d ft11, ft10, ft9, dyn
[0x800019b0]:csrrs a7, fflags, zero
[0x800019b4]:fsd ft11, 1552(a5)
[0x800019b8]:sw a7, 1556(a5)
[0x800019bc]:fld ft10, 1568(a6)
[0x800019c0]:fld ft9, 1576(a6)
[0x800019c4]:csrrwi zero, frm, 0

[0x800019c8]:fmul.d ft11, ft10, ft9, dyn
[0x800019cc]:csrrs a7, fflags, zero
[0x800019d0]:fsd ft11, 1568(a5)
[0x800019d4]:sw a7, 1572(a5)
[0x800019d8]:fld ft10, 1584(a6)
[0x800019dc]:fld ft9, 1592(a6)
[0x800019e0]:csrrwi zero, frm, 0

[0x800019e4]:fmul.d ft11, ft10, ft9, dyn
[0x800019e8]:csrrs a7, fflags, zero
[0x800019ec]:fsd ft11, 1584(a5)
[0x800019f0]:sw a7, 1588(a5)
[0x800019f4]:fld ft10, 1600(a6)
[0x800019f8]:fld ft9, 1608(a6)
[0x800019fc]:csrrwi zero, frm, 0

[0x80001a00]:fmul.d ft11, ft10, ft9, dyn
[0x80001a04]:csrrs a7, fflags, zero
[0x80001a08]:fsd ft11, 1600(a5)
[0x80001a0c]:sw a7, 1604(a5)
[0x80001a10]:fld ft10, 1616(a6)
[0x80001a14]:fld ft9, 1624(a6)
[0x80001a18]:csrrwi zero, frm, 0

[0x80001a1c]:fmul.d ft11, ft10, ft9, dyn
[0x80001a20]:csrrs a7, fflags, zero
[0x80001a24]:fsd ft11, 1616(a5)
[0x80001a28]:sw a7, 1620(a5)
[0x80001a2c]:fld ft10, 1632(a6)
[0x80001a30]:fld ft9, 1640(a6)
[0x80001a34]:csrrwi zero, frm, 0

[0x80001a38]:fmul.d ft11, ft10, ft9, dyn
[0x80001a3c]:csrrs a7, fflags, zero
[0x80001a40]:fsd ft11, 1632(a5)
[0x80001a44]:sw a7, 1636(a5)
[0x80001a48]:fld ft10, 1648(a6)
[0x80001a4c]:fld ft9, 1656(a6)
[0x80001a50]:csrrwi zero, frm, 0

[0x80001a54]:fmul.d ft11, ft10, ft9, dyn
[0x80001a58]:csrrs a7, fflags, zero
[0x80001a5c]:fsd ft11, 1648(a5)
[0x80001a60]:sw a7, 1652(a5)
[0x80001a64]:fld ft10, 1664(a6)
[0x80001a68]:fld ft9, 1672(a6)
[0x80001a6c]:csrrwi zero, frm, 0

[0x80001a70]:fmul.d ft11, ft10, ft9, dyn
[0x80001a74]:csrrs a7, fflags, zero
[0x80001a78]:fsd ft11, 1664(a5)
[0x80001a7c]:sw a7, 1668(a5)
[0x80001a80]:fld ft10, 1680(a6)
[0x80001a84]:fld ft9, 1688(a6)
[0x80001a88]:csrrwi zero, frm, 0

[0x80001a8c]:fmul.d ft11, ft10, ft9, dyn
[0x80001a90]:csrrs a7, fflags, zero
[0x80001a94]:fsd ft11, 1680(a5)
[0x80001a98]:sw a7, 1684(a5)
[0x80001a9c]:fld ft10, 1696(a6)
[0x80001aa0]:fld ft9, 1704(a6)
[0x80001aa4]:csrrwi zero, frm, 0

[0x80001aa8]:fmul.d ft11, ft10, ft9, dyn
[0x80001aac]:csrrs a7, fflags, zero
[0x80001ab0]:fsd ft11, 1696(a5)
[0x80001ab4]:sw a7, 1700(a5)
[0x80001ab8]:fld ft10, 1712(a6)
[0x80001abc]:fld ft9, 1720(a6)
[0x80001ac0]:csrrwi zero, frm, 0

[0x80001ac4]:fmul.d ft11, ft10, ft9, dyn
[0x80001ac8]:csrrs a7, fflags, zero
[0x80001acc]:fsd ft11, 1712(a5)
[0x80001ad0]:sw a7, 1716(a5)
[0x80001ad4]:fld ft10, 1728(a6)
[0x80001ad8]:fld ft9, 1736(a6)
[0x80001adc]:csrrwi zero, frm, 0

[0x80001ae0]:fmul.d ft11, ft10, ft9, dyn
[0x80001ae4]:csrrs a7, fflags, zero
[0x80001ae8]:fsd ft11, 1728(a5)
[0x80001aec]:sw a7, 1732(a5)
[0x80001af0]:fld ft10, 1744(a6)
[0x80001af4]:fld ft9, 1752(a6)
[0x80001af8]:csrrwi zero, frm, 0

[0x80001afc]:fmul.d ft11, ft10, ft9, dyn
[0x80001b00]:csrrs a7, fflags, zero
[0x80001b04]:fsd ft11, 1744(a5)
[0x80001b08]:sw a7, 1748(a5)
[0x80001b0c]:fld ft10, 1760(a6)
[0x80001b10]:fld ft9, 1768(a6)
[0x80001b14]:csrrwi zero, frm, 0

[0x80001b18]:fmul.d ft11, ft10, ft9, dyn
[0x80001b1c]:csrrs a7, fflags, zero
[0x80001b20]:fsd ft11, 1760(a5)
[0x80001b24]:sw a7, 1764(a5)
[0x80001b28]:fld ft10, 1776(a6)
[0x80001b2c]:fld ft9, 1784(a6)
[0x80001b30]:csrrwi zero, frm, 0

[0x80001b34]:fmul.d ft11, ft10, ft9, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1776(a5)
[0x80001b40]:sw a7, 1780(a5)
[0x80001b44]:fld ft10, 1792(a6)
[0x80001b48]:fld ft9, 1800(a6)
[0x80001b4c]:csrrwi zero, frm, 0

[0x80001b50]:fmul.d ft11, ft10, ft9, dyn
[0x80001b54]:csrrs a7, fflags, zero
[0x80001b58]:fsd ft11, 1792(a5)
[0x80001b5c]:sw a7, 1796(a5)
[0x80001b60]:fld ft10, 1808(a6)
[0x80001b64]:fld ft9, 1816(a6)
[0x80001b68]:csrrwi zero, frm, 0

[0x80001b6c]:fmul.d ft11, ft10, ft9, dyn
[0x80001b70]:csrrs a7, fflags, zero
[0x80001b74]:fsd ft11, 1808(a5)
[0x80001b78]:sw a7, 1812(a5)
[0x80001b7c]:fld ft10, 1824(a6)
[0x80001b80]:fld ft9, 1832(a6)
[0x80001b84]:csrrwi zero, frm, 0

[0x80001b88]:fmul.d ft11, ft10, ft9, dyn
[0x80001b8c]:csrrs a7, fflags, zero
[0x80001b90]:fsd ft11, 1824(a5)
[0x80001b94]:sw a7, 1828(a5)
[0x80001b98]:fld ft10, 1840(a6)
[0x80001b9c]:fld ft9, 1848(a6)
[0x80001ba0]:csrrwi zero, frm, 0

[0x80001ba4]:fmul.d ft11, ft10, ft9, dyn
[0x80001ba8]:csrrs a7, fflags, zero
[0x80001bac]:fsd ft11, 1840(a5)
[0x80001bb0]:sw a7, 1844(a5)
[0x80001bb4]:fld ft10, 1856(a6)
[0x80001bb8]:fld ft9, 1864(a6)
[0x80001bbc]:csrrwi zero, frm, 0

[0x80001bc0]:fmul.d ft11, ft10, ft9, dyn
[0x80001bc4]:csrrs a7, fflags, zero
[0x80001bc8]:fsd ft11, 1856(a5)
[0x80001bcc]:sw a7, 1860(a5)
[0x80001bd0]:fld ft10, 1872(a6)
[0x80001bd4]:fld ft9, 1880(a6)
[0x80001bd8]:csrrwi zero, frm, 0

[0x80001bdc]:fmul.d ft11, ft10, ft9, dyn
[0x80001be0]:csrrs a7, fflags, zero
[0x80001be4]:fsd ft11, 1872(a5)
[0x80001be8]:sw a7, 1876(a5)
[0x80001bec]:fld ft10, 1888(a6)
[0x80001bf0]:fld ft9, 1896(a6)
[0x80001bf4]:csrrwi zero, frm, 0

[0x80001bf8]:fmul.d ft11, ft10, ft9, dyn
[0x80001bfc]:csrrs a7, fflags, zero
[0x80001c00]:fsd ft11, 1888(a5)
[0x80001c04]:sw a7, 1892(a5)
[0x80001c08]:fld ft10, 1904(a6)
[0x80001c0c]:fld ft9, 1912(a6)
[0x80001c10]:csrrwi zero, frm, 0

[0x80001c14]:fmul.d ft11, ft10, ft9, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1904(a5)
[0x80001c20]:sw a7, 1908(a5)
[0x80001c24]:fld ft10, 1920(a6)
[0x80001c28]:fld ft9, 1928(a6)
[0x80001c2c]:csrrwi zero, frm, 0

[0x80001c30]:fmul.d ft11, ft10, ft9, dyn
[0x80001c34]:csrrs a7, fflags, zero
[0x80001c38]:fsd ft11, 1920(a5)
[0x80001c3c]:sw a7, 1924(a5)
[0x80001c40]:fld ft10, 1936(a6)
[0x80001c44]:fld ft9, 1944(a6)
[0x80001c48]:csrrwi zero, frm, 0

[0x80001c4c]:fmul.d ft11, ft10, ft9, dyn
[0x80001c50]:csrrs a7, fflags, zero
[0x80001c54]:fsd ft11, 1936(a5)
[0x80001c58]:sw a7, 1940(a5)
[0x80001c5c]:fld ft10, 1952(a6)
[0x80001c60]:fld ft9, 1960(a6)
[0x80001c64]:csrrwi zero, frm, 0

[0x80001c68]:fmul.d ft11, ft10, ft9, dyn
[0x80001c6c]:csrrs a7, fflags, zero
[0x80001c70]:fsd ft11, 1952(a5)
[0x80001c74]:sw a7, 1956(a5)
[0x80001c78]:fld ft10, 1968(a6)
[0x80001c7c]:fld ft9, 1976(a6)
[0x80001c80]:csrrwi zero, frm, 0

[0x80001c84]:fmul.d ft11, ft10, ft9, dyn
[0x80001c88]:csrrs a7, fflags, zero
[0x80001c8c]:fsd ft11, 1968(a5)
[0x80001c90]:sw a7, 1972(a5)
[0x80001c94]:fld ft10, 1984(a6)
[0x80001c98]:fld ft9, 1992(a6)
[0x80001c9c]:csrrwi zero, frm, 0

[0x80001ca0]:fmul.d ft11, ft10, ft9, dyn
[0x80001ca4]:csrrs a7, fflags, zero
[0x80001ca8]:fsd ft11, 1984(a5)
[0x80001cac]:sw a7, 1988(a5)
[0x80001cb0]:fld ft10, 2000(a6)
[0x80001cb4]:fld ft9, 2008(a6)
[0x80001cb8]:csrrwi zero, frm, 0

[0x80001cbc]:fmul.d ft11, ft10, ft9, dyn
[0x80001cc0]:csrrs a7, fflags, zero
[0x80001cc4]:fsd ft11, 2000(a5)
[0x80001cc8]:sw a7, 2004(a5)
[0x80001ccc]:fld ft10, 2016(a6)
[0x80001cd0]:fld ft9, 2024(a6)
[0x80001cd4]:csrrwi zero, frm, 0

[0x80001cd8]:fmul.d ft11, ft10, ft9, dyn
[0x80001cdc]:csrrs a7, fflags, zero
[0x80001ce0]:fsd ft11, 2016(a5)
[0x80001ce4]:sw a7, 2020(a5)
[0x80001ce8]:addi a6, a6, 2032
[0x80001cec]:auipc a5, 3
[0x80001cf0]:addi a5, a5, 3604
[0x80001cf4]:fld ft10, 0(a6)
[0x80001cf8]:fld ft9, 8(a6)
[0x80001cfc]:csrrwi zero, frm, 0

[0x80001e6c]:fmul.d ft11, ft10, ft9, dyn
[0x80001e70]:csrrs a7, fflags, zero
[0x80001e74]:fsd ft11, 208(a5)
[0x80001e78]:sw a7, 212(a5)
[0x80001e7c]:fld ft10, 224(a6)
[0x80001e80]:fld ft9, 232(a6)
[0x80001e84]:csrrwi zero, frm, 0

[0x80001e88]:fmul.d ft11, ft10, ft9, dyn
[0x80001e8c]:csrrs a7, fflags, zero
[0x80001e90]:fsd ft11, 224(a5)
[0x80001e94]:sw a7, 228(a5)
[0x80001e98]:fld ft10, 240(a6)
[0x80001e9c]:fld ft9, 248(a6)
[0x80001ea0]:csrrwi zero, frm, 0

[0x80001ea4]:fmul.d ft11, ft10, ft9, dyn
[0x80001ea8]:csrrs a7, fflags, zero
[0x80001eac]:fsd ft11, 240(a5)
[0x80001eb0]:sw a7, 244(a5)
[0x80001eb4]:fld ft10, 256(a6)
[0x80001eb8]:fld ft9, 264(a6)
[0x80001ebc]:csrrwi zero, frm, 0

[0x80001ec0]:fmul.d ft11, ft10, ft9, dyn
[0x80001ec4]:csrrs a7, fflags, zero
[0x80001ec8]:fsd ft11, 256(a5)
[0x80001ecc]:sw a7, 260(a5)
[0x80001ed0]:fld ft10, 272(a6)
[0x80001ed4]:fld ft9, 280(a6)
[0x80001ed8]:csrrwi zero, frm, 0

[0x80001edc]:fmul.d ft11, ft10, ft9, dyn
[0x80001ee0]:csrrs a7, fflags, zero
[0x80001ee4]:fsd ft11, 272(a5)
[0x80001ee8]:sw a7, 276(a5)
[0x80001eec]:fld ft10, 288(a6)
[0x80001ef0]:fld ft9, 296(a6)
[0x80001ef4]:csrrwi zero, frm, 0

[0x80001ef8]:fmul.d ft11, ft10, ft9, dyn
[0x80001efc]:csrrs a7, fflags, zero
[0x80001f00]:fsd ft11, 288(a5)
[0x80001f04]:sw a7, 292(a5)
[0x80001f08]:fld ft10, 304(a6)
[0x80001f0c]:fld ft9, 312(a6)
[0x80001f10]:csrrwi zero, frm, 0

[0x80001f14]:fmul.d ft11, ft10, ft9, dyn
[0x80001f18]:csrrs a7, fflags, zero
[0x80001f1c]:fsd ft11, 304(a5)
[0x80001f20]:sw a7, 308(a5)
[0x80001f24]:fld ft10, 320(a6)
[0x80001f28]:fld ft9, 328(a6)
[0x80001f2c]:csrrwi zero, frm, 0

[0x80001f30]:fmul.d ft11, ft10, ft9, dyn
[0x80001f34]:csrrs a7, fflags, zero
[0x80001f38]:fsd ft11, 320(a5)
[0x80001f3c]:sw a7, 324(a5)
[0x80001f40]:fld ft10, 336(a6)
[0x80001f44]:fld ft9, 344(a6)
[0x80001f48]:csrrwi zero, frm, 0

[0x80001f4c]:fmul.d ft11, ft10, ft9, dyn
[0x80001f50]:csrrs a7, fflags, zero
[0x80001f54]:fsd ft11, 336(a5)
[0x80001f58]:sw a7, 340(a5)
[0x80001f5c]:fld ft10, 352(a6)
[0x80001f60]:fld ft9, 360(a6)
[0x80001f64]:csrrwi zero, frm, 0

[0x80001f68]:fmul.d ft11, ft10, ft9, dyn
[0x80001f6c]:csrrs a7, fflags, zero
[0x80001f70]:fsd ft11, 352(a5)
[0x80001f74]:sw a7, 356(a5)
[0x80001f78]:fld ft10, 368(a6)
[0x80001f7c]:fld ft9, 376(a6)
[0x80001f80]:csrrwi zero, frm, 0

[0x80001f84]:fmul.d ft11, ft10, ft9, dyn
[0x80001f88]:csrrs a7, fflags, zero
[0x80001f8c]:fsd ft11, 368(a5)
[0x80001f90]:sw a7, 372(a5)
[0x80001f94]:fld ft10, 384(a6)
[0x80001f98]:fld ft9, 392(a6)
[0x80001f9c]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                               |                                                                           code                                                                           |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004314]<br>0x00000000|- opcode : fmul.d<br> - rs1 : f9<br> - rs2 : f28<br> - rd : f28<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000008 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>  |[0x80000120]:fmul.d ft8, fs1, ft8, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd ft8, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>          |
|   2|[0x80004324]<br>0x00000005|- rs1 : f8<br> - rs2 : f8<br> - rd : f16<br> - rs1 == rs2 != rd<br>                                                                                                                                                                     |[0x8000013c]:fmul.d fa6, fs0, fs0, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fa6, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>        |
|   3|[0x80004334]<br>0x00000005|- rs1 : f24<br> - rs2 : f4<br> - rd : f10<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffdfff61 and rm_val == 0  #nosat<br> |[0x80000158]:fmul.d fa0, fs8, ft4, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fa0, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>        |
|   4|[0x80004344]<br>0x00000005|- rs1 : f3<br> - rs2 : f3<br> - rd : f3<br> - rs1 == rs2 == rd<br>                                                                                                                                                                      |[0x80000174]:fmul.d ft3, ft3, ft3, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd ft3, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>        |
|   5|[0x80004354]<br>0x00000005|- rs1 : f0<br> - rs2 : f29<br> - rd : f0<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000041 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffff7ff7d and rm_val == 0  #nosat<br>                         |[0x80000190]:fmul.d ft0, ft0, ft9, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd ft0, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>        |
|   6|[0x80004364]<br>0x00000005|- rs1 : f26<br> - rs2 : f1<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffbff89 and rm_val == 0  #nosat<br>                                                |[0x800001ac]:fmul.d ft6, fs10, ft1, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft6, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>       |
|   7|[0x80004374]<br>0x00000005|- rs1 : f18<br> - rs2 : f30<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000057 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffdff51 and rm_val == 0  #nosat<br>                                              |[0x800001c8]:fmul.d fa2, fs2, ft10, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd fa2, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>      |
|   8|[0x80004384]<br>0x00000005|- rs1 : f23<br> - rs2 : f2<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffeff67 and rm_val == 0  #nosat<br>                                               |[0x800001e4]:fmul.d fa4, fs7, ft2, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa4, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>      |
|   9|[0x80004394]<br>0x00000005|- rs1 : f6<br> - rs2 : f13<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffff7fed and rm_val == 0  #nosat<br>                                               |[0x80000200]:fmul.d fs6, ft6, fa3, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fs6, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>      |
|  10|[0x800043a4]<br>0x00000005|- rs1 : f22<br> - rs2 : f20<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000056 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffbf53 and rm_val == 0  #nosat<br>                                              |[0x8000021c]:fmul.d ft10, fs6, fs4, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd ft10, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>    |
|  11|[0x800043b4]<br>0x00000005|- rs1 : f7<br> - rs2 : f27<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffdf81 and rm_val == 0  #nosat<br>                                               |[0x80000238]:fmul.d fa5, ft7, fs11, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd fa5, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>     |
|  12|[0x800043c4]<br>0x00000005|- rs1 : f11<br> - rs2 : f16<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffefa5 and rm_val == 0  #nosat<br>                                              |[0x80000254]:fmul.d fs7, fa1, fa6, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs7, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>      |
|  13|[0x800043d4]<br>0x00000005|- rs1 : f2<br> - rs2 : f17<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffff73b and rm_val == 0  #nosat<br>                                                |[0x80000270]:fmul.d fs0, ft2, fa7, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fs0, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>      |
|  14|[0x800043e4]<br>0x00000005|- rs1 : f25<br> - rs2 : f23<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffffb47 and rm_val == 0  #nosat<br>                                               |[0x8000028c]:fmul.d fs1, fs9, fs7, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd fs1, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>      |
|  15|[0x800043f4]<br>0x00000005|- rs1 : f12<br> - rs2 : f26<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000017 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffffdd1 and rm_val == 0  #nosat<br>                                               |[0x800002a8]:fmul.d ft5, fa2, fs10, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd ft5, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>     |
|  16|[0x80004404]<br>0x00000005|- rs1 : f27<br> - rs2 : f6<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffffe7b and rm_val == 0  #nosat<br>                                               |[0x800002c4]:fmul.d ft9, fs11, ft6, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd ft9, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>     |
|  17|[0x80004414]<br>0x00000005|- rs1 : f4<br> - rs2 : f0<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003e and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff03 and rm_val == 0  #nosat<br>                                                |[0x800002e0]:fmul.d fs4, ft4, ft0, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd fs4, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>      |
|  18|[0x80004424]<br>0x00000005|- rs1 : f30<br> - rs2 : f5<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000057 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff11 and rm_val == 0  #nosat<br>                                               |[0x800002fc]:fmul.d fs8, ft10, ft5, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fs8, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br>     |
|  19|[0x80004434]<br>0x00000005|- rs1 : f5<br> - rs2 : f14<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffdb and rm_val == 0  #nosat<br>                                               |[0x80000318]:fmul.d ft11, ft5, fa4, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd ft11, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>    |
|  20|[0x80004444]<br>0x00000005|- rs1 : f29<br> - rs2 : f12<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff51 and rm_val == 0  #nosat<br>                                               |[0x80000334]:fmul.d ft1, ft9, fa2, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd ft1, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>      |
|  21|[0x80004454]<br>0x00000005|- rs1 : f1<br> - rs2 : f21<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffc5 and rm_val == 0  #nosat<br>                                               |[0x80000350]:fmul.d fs9, ft1, fs5, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs9, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>      |
|  22|[0x80004464]<br>0x00000005|- rs1 : f21<br> - rs2 : f9<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff63 and rm_val == 0  #nosat<br>                                                |[0x8000036c]:fmul.d ft4, fs5, fs1, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd ft4, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>      |
|  23|[0x80004474]<br>0x00000005|- rs1 : f17<br> - rs2 : f24<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000024 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffb5 and rm_val == 0  #nosat<br>                                              |[0x80000388]:fmul.d fs11, fa7, fs8, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd fs11, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>    |
|  24|[0x80004484]<br>0x00000005|- rs1 : f31<br> - rs2 : f15<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000063 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff38 and rm_val == 0  #nosat<br>                                              |[0x800003a4]:fmul.d fa1, ft11, fa5, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fa1, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>     |
|  25|[0x80004494]<br>0x00000005|- rs1 : f13<br> - rs2 : f11<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffbfff9b and rm_val == 0  #nosat<br>                                              |[0x800003c0]:fmul.d fs10, fa3, fa1, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd fs10, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>    |
|  26|[0x800044a4]<br>0x00000005|- rs1 : f16<br> - rs2 : f7<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000001c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffdfffc7 and rm_val == 0  #nosat<br>                                               |[0x800003dc]:fmul.d fa7, fa6, ft7, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fa7, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>      |
|  27|[0x800044b4]<br>0x00000005|- rs1 : f20<br> - rs2 : f19<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004e and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffefff63 and rm_val == 0  #nosat<br>                                               |[0x800003f8]:fmul.d ft7, fs4, fs3, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd ft7, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>      |
|  28|[0x800044c4]<br>0x00000005|- rs1 : f10<br> - rs2 : f18<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffff7ff43 and rm_val == 0  #nosat<br>                                              |[0x80000414]:fmul.d fs3, fa0, fs2, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fs3, 432(a5)<br> [0x80000420]:sw a7, 436(a5)<br>      |
|  29|[0x800044d4]<br>0x00000005|- rs1 : f28<br> - rs2 : f31<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffbff97 and rm_val == 0  #nosat<br>                                              |[0x80000430]:fmul.d fs2, ft8, ft11, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsd fs2, 448(a5)<br> [0x8000043c]:sw a7, 452(a5)<br>     |
|  30|[0x800044e4]<br>0x00000005|- rs1 : f15<br> - rs2 : f10<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffdffe5 and rm_val == 0  #nosat<br>                                               |[0x8000044c]:fmul.d ft2, fa5, fa0, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd ft2, 464(a5)<br> [0x80000458]:sw a7, 468(a5)<br>      |
|  31|[0x800044f4]<br>0x00000005|- rs1 : f19<br> - rs2 : f22<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffeff57 and rm_val == 0  #nosat<br>                                              |[0x80000468]:fmul.d fa3, fs3, fs6, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsd fa3, 480(a5)<br> [0x80000474]:sw a7, 484(a5)<br>      |
|  32|[0x80004504]<br>0x00000005|- rs1 : f14<br> - rs2 : f25<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffff7f47 and rm_val == 0  #nosat<br>                                              |[0x80000484]:fmul.d fs5, fa4, fs9, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fs5, 496(a5)<br> [0x80000490]:sw a7, 500(a5)<br>      |
|  33|[0x80004514]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffbf87 and rm_val == 0  #nosat<br>                                                                                             |[0x800004a0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsd ft11, 512(a5)<br> [0x800004ac]:sw a7, 516(a5)<br>   |
|  34|[0x80004524]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffdffb and rm_val == 0  #nosat<br>                                                                                             |[0x800004bc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft11, 528(a5)<br> [0x800004c8]:sw a7, 532(a5)<br>   |
|  35|[0x80004534]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000031 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffef9d and rm_val == 0  #nosat<br>                                                                                             |[0x800004d8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsd ft11, 544(a5)<br> [0x800004e4]:sw a7, 548(a5)<br>   |
|  36|[0x80004544]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffff797 and rm_val == 0  #nosat<br>                                                                                             |[0x800004f4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 560(a5)<br> [0x80000500]:sw a7, 564(a5)<br>   |
|  37|[0x80004554]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffffb5f and rm_val == 0  #nosat<br>                                                                                             |[0x80000510]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd ft11, 576(a5)<br> [0x8000051c]:sw a7, 580(a5)<br>   |
|  38|[0x80004564]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffffdc5 and rm_val == 0  #nosat<br>                                                                                             |[0x8000052c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsd ft11, 592(a5)<br> [0x80000538]:sw a7, 596(a5)<br>   |
|  39|[0x80004574]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffffe3b and rm_val == 0  #nosat<br>                                                                                             |[0x80000548]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsd ft11, 608(a5)<br> [0x80000554]:sw a7, 612(a5)<br>   |
|  40|[0x80004584]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xffffffffffed5 and rm_val == 0  #nosat<br>                                                                                             |[0x80000564]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 624(a5)<br> [0x80000570]:sw a7, 628(a5)<br>   |
|  41|[0x80004594]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff07 and rm_val == 0  #nosat<br>                                                                                             |[0x80000580]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsd ft11, 640(a5)<br> [0x8000058c]:sw a7, 644(a5)<br>   |
|  42|[0x800045a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000047 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff51 and rm_val == 0  #nosat<br>                                                                                             |[0x8000059c]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 656(a5)<br> [0x800005a8]:sw a7, 660(a5)<br>   |
|  43|[0x800045b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff75 and rm_val == 0  #nosat<br>                                                                                             |[0x800005b8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsd ft11, 672(a5)<br> [0x800005c4]:sw a7, 676(a5)<br>   |
|  44|[0x800045c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff41 and rm_val == 0  #nosat<br>                                                                                             |[0x800005d4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsd ft11, 688(a5)<br> [0x800005e0]:sw a7, 692(a5)<br>   |
|  45|[0x800045d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff3f and rm_val == 0  #nosat<br>                                                                                             |[0x800005f0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsd ft11, 704(a5)<br> [0x800005fc]:sw a7, 708(a5)<br>   |
|  46|[0x800045e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffbb and rm_val == 0  #nosat<br>                                                                                             |[0x8000060c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft11, 720(a5)<br> [0x80000618]:sw a7, 724(a5)<br>   |
|  47|[0x800045f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffb6 and rm_val == 0  #nosat<br>                                                                                             |[0x80000628]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsd ft11, 736(a5)<br> [0x80000634]:sw a7, 740(a5)<br>   |
|  48|[0x80004604]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000007 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000003ffff9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000644]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 752(a5)<br> [0x80000650]:sw a7, 756(a5)<br>   |
|  49|[0x80004614]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000001fffcd and rm_val == 0  #nosat<br>                                                                                             |[0x80000660]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd ft11, 768(a5)<br> [0x8000066c]:sw a7, 772(a5)<br>   |
|  50|[0x80004624]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000fffa1 and rm_val == 0  #nosat<br>                                                                                             |[0x8000067c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsd ft11, 784(a5)<br> [0x80000688]:sw a7, 788(a5)<br>   |
|  51|[0x80004634]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000007ffb3 and rm_val == 0  #nosat<br>                                                                                             |[0x80000698]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsd ft11, 800(a5)<br> [0x800006a4]:sw a7, 804(a5)<br>   |
|  52|[0x80004644]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000003ffa3 and rm_val == 0  #nosat<br>                                                                                             |[0x800006b4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 816(a5)<br> [0x800006c0]:sw a7, 820(a5)<br>   |
|  53|[0x80004654]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000003c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000001ffc4 and rm_val == 0  #nosat<br>                                                                                             |[0x800006d0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006d4]:csrrs a7, fflags, zero<br> [0x800006d8]:fsd ft11, 832(a5)<br> [0x800006dc]:sw a7, 836(a5)<br>   |
|  54|[0x80004664]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000000ffa2 and rm_val == 0  #nosat<br>                                                                                             |[0x800006ec]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 848(a5)<br> [0x800006f8]:sw a7, 852(a5)<br>   |
|  55|[0x80004674]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000007f9e and rm_val == 0  #nosat<br>                                                                                             |[0x80000708]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:fsd ft11, 864(a5)<br> [0x80000714]:sw a7, 868(a5)<br>   |
|  56|[0x80004684]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000034 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000003fcc and rm_val == 0  #nosat<br>                                                                                             |[0x80000724]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 880(a5)<br> [0x80000730]:sw a7, 884(a5)<br>   |
|  57|[0x80004694]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000001fa5 and rm_val == 0  #nosat<br>                                                                                             |[0x80000740]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd ft11, 896(a5)<br> [0x8000074c]:sw a7, 900(a5)<br>   |
|  58|[0x800046a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000feb and rm_val == 0  #nosat<br>                                                                                             |[0x8000075c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 912(a5)<br> [0x80000768]:sw a7, 916(a5)<br>   |
|  59|[0x800046b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000007df and rm_val == 0  #nosat<br>                                                                                             |[0x80000778]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000077c]:csrrs a7, fflags, zero<br> [0x80000780]:fsd ft11, 928(a5)<br> [0x80000784]:sw a7, 932(a5)<br>   |
|  60|[0x800046c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000003a4 and rm_val == 0  #nosat<br>                                                                                             |[0x80000794]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:fsd ft11, 944(a5)<br> [0x800007a0]:sw a7, 948(a5)<br>   |
|  61|[0x800046d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000043 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000001bd and rm_val == 0  #nosat<br>                                                                                             |[0x800007b0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:fsd ft11, 960(a5)<br> [0x800007bc]:sw a7, 964(a5)<br>   |
|  62|[0x800046e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000000f3 and rm_val == 0  #nosat<br>                                                                                             |[0x800007cc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007d0]:csrrs a7, fflags, zero<br> [0x800007d4]:fsd ft11, 976(a5)<br> [0x800007d8]:sw a7, 980(a5)<br>   |
|  63|[0x800046f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000061 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000000001f and rm_val == 0  #nosat<br>                                                                                             |[0x800007e8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:fsd ft11, 992(a5)<br> [0x800007f4]:sw a7, 996(a5)<br>   |
|  64|[0x80004704]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000014 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                             |[0x80000804]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 1008(a5)<br> [0x80000810]:sw a7, 1012(a5)<br> |
|  65|[0x80004714]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000025 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffff6 and rm_val == 0  #nosat<br>                                                                                             |[0x80000820]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000824]:csrrs a7, fflags, zero<br> [0x80000828]:fsd ft11, 1024(a5)<br> [0x8000082c]:sw a7, 1028(a5)<br> |
|  66|[0x80004724]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000017 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffff2 and rm_val == 0  #nosat<br>                                                                                             |[0x8000083c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd ft11, 1040(a5)<br> [0x80000848]:sw a7, 1044(a5)<br> |
|  67|[0x80004734]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002b and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffffba and rm_val == 0  #nosat<br>                                                                                             |[0x80000858]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:fsd ft11, 1056(a5)<br> [0x80000864]:sw a7, 1060(a5)<br> |
|  68|[0x80004744]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000056 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff5c and rm_val == 0  #nosat<br>                                                                                             |[0x80000874]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000878]:csrrs a7, fflags, zero<br> [0x8000087c]:fsd ft11, 1072(a5)<br> [0x80000880]:sw a7, 1076(a5)<br> |
|  69|[0x80004754]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffffc8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000890]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:fsd ft11, 1088(a5)<br> [0x8000089c]:sw a7, 1092(a5)<br> |
|  70|[0x80004764]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffffb6 and rm_val == 0  #nosat<br>                                                                                             |[0x800008ac]:fmul.d ft11, ft10, ft9, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 1104(a5)<br> [0x800008b8]:sw a7, 1108(a5)<br> |
|  71|[0x80004774]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000003ffffb and rm_val == 0  #nosat<br>                                                                                             |[0x800008c8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:fsd ft11, 1120(a5)<br> [0x800008d4]:sw a7, 1124(a5)<br> |
|  72|[0x80004784]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000001fffa8 and rm_val == 0  #nosat<br>                                                                                             |[0x800008e4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 1136(a5)<br> [0x800008f0]:sw a7, 1140(a5)<br> |
|  73|[0x80004794]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000009 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000000ffff7 and rm_val == 0  #nosat<br>                                                                                             |[0x80000900]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000904]:csrrs a7, fflags, zero<br> [0x80000908]:fsd ft11, 1152(a5)<br> [0x8000090c]:sw a7, 1156(a5)<br> |
|  74|[0x800047a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000013 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x000000007ffed and rm_val == 0  #nosat<br>                                                                                             |[0x8000091c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000920]:csrrs a7, fflags, zero<br> [0x80000924]:fsd ft11, 1168(a5)<br> [0x80000928]:sw a7, 1172(a5)<br> |
|  75|[0x800047b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x000000003ffe9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000938]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000093c]:csrrs a7, fflags, zero<br> [0x80000940]:fsd ft11, 1184(a5)<br> [0x80000944]:sw a7, 1188(a5)<br> |
|  76|[0x800047c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x000000001fff6 and rm_val == 0  #nosat<br>                                                                                             |[0x80000954]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsd ft11, 1200(a5)<br> [0x80000960]:sw a7, 1204(a5)<br> |
|  77|[0x800047d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x000000000ffb6 and rm_val == 0  #nosat<br>                                                                                             |[0x80000970]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000974]:csrrs a7, fflags, zero<br> [0x80000978]:fsd ft11, 1216(a5)<br> [0x8000097c]:sw a7, 1220(a5)<br> |
|  78|[0x800047e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000007fb5 and rm_val == 0  #nosat<br>                                                                                             |[0x8000098c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:fsd ft11, 1232(a5)<br> [0x80000998]:sw a7, 1236(a5)<br> |
|  79|[0x800047f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000003fa6 and rm_val == 0  #nosat<br>                                                                                             |[0x800009a8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800009ac]:csrrs a7, fflags, zero<br> [0x800009b0]:fsd ft11, 1248(a5)<br> [0x800009b4]:sw a7, 1252(a5)<br> |
|  80|[0x80004804]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000001fa6 and rm_val == 0  #nosat<br>                                                                                             |[0x800009c4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1264(a5)<br> [0x800009d0]:sw a7, 1268(a5)<br> |
|  81|[0x80004814]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000047 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000fb9 and rm_val == 0  #nosat<br>                                                                                             |[0x800009e0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800009e4]:csrrs a7, fflags, zero<br> [0x800009e8]:fsd ft11, 1280(a5)<br> [0x800009ec]:sw a7, 1284(a5)<br> |
|  82|[0x80004824]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000000007b3 and rm_val == 0  #nosat<br>                                                                                             |[0x800009fc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:fsd ft11, 1296(a5)<br> [0x80000a08]:sw a7, 1300(a5)<br> |
|  83|[0x80004834]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000000003eb and rm_val == 0  #nosat<br>                                                                                             |[0x80000a18]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a1c]:csrrs a7, fflags, zero<br> [0x80000a20]:fsd ft11, 1312(a5)<br> [0x80000a24]:sw a7, 1316(a5)<br> |
|  84|[0x80004844]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000000001cd and rm_val == 0  #nosat<br>                                                                                             |[0x80000a34]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:fsd ft11, 1328(a5)<br> [0x80000a40]:sw a7, 1332(a5)<br> |
|  85|[0x80004854]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00000000000a8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000a50]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a54]:csrrs a7, fflags, zero<br> [0x80000a58]:fsd ft11, 1344(a5)<br> [0x80000a5c]:sw a7, 1348(a5)<br> |
|  86|[0x80004864]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x000000000004a and rm_val == 0  #nosat<br>                                                                                             |[0x80000a6c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a70]:csrrs a7, fflags, zero<br> [0x80000a74]:fsd ft11, 1360(a5)<br> [0x80000a78]:sw a7, 1364(a5)<br> |
|  87|[0x80004874]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffffc6 and rm_val == 0  #nosat<br>                                                                                             |[0x80000a88]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000a8c]:csrrs a7, fflags, zero<br> [0x80000a90]:fsd ft11, 1376(a5)<br> [0x80000a94]:sw a7, 1380(a5)<br> |
|  88|[0x80004884]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000044 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffffb8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000aa4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1392(a5)<br> [0x80000ab0]:sw a7, 1396(a5)<br> |
|  89|[0x80004894]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffffda and rm_val == 0  #nosat<br>                                                                                             |[0x80000ac0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ac4]:csrrs a7, fflags, zero<br> [0x80000ac8]:fsd ft11, 1408(a5)<br> [0x80000acc]:sw a7, 1412(a5)<br> |
|  90|[0x800048a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000013 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffffea and rm_val == 0  #nosat<br>                                                                                             |[0x80000adc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:fsd ft11, 1424(a5)<br> [0x80000ae8]:sw a7, 1428(a5)<br> |
|  91|[0x800048b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffffac and rm_val == 0  #nosat<br>                                                                                             |[0x80000af8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000afc]:csrrs a7, fflags, zero<br> [0x80000b00]:fsd ft11, 1440(a5)<br> [0x80000b04]:sw a7, 1444(a5)<br> |
|  92|[0x800048c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff84 and rm_val == 0  #nosat<br>                                                                                             |[0x80000b14]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000b18]:csrrs a7, fflags, zero<br> [0x80000b1c]:fsd ft11, 1456(a5)<br> [0x80000b20]:sw a7, 1460(a5)<br> |
|  93|[0x800048d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff48 and rm_val == 0  #nosat<br>                                                                                             |[0x80000b30]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000b34]:csrrs a7, fflags, zero<br> [0x80000b38]:fsd ft11, 1472(a5)<br> [0x80000b3c]:sw a7, 1476(a5)<br> |
|  94|[0x800048e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x42d and fm2 == 0x0c9714f79b474 and rm_val == 0  #nosat<br>                                                                                             |[0x80000b4c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000b50]:csrrs a7, fflags, zero<br> [0x80000b54]:fsd ft11, 1488(a5)<br> [0x80000b58]:sw a7, 1492(a5)<br> |
|  95|[0x800048f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x9999999666665 and rm_val == 0  #nosat<br>                                                                                             |[0x80000b68]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000b6c]:csrrs a7, fflags, zero<br> [0x80000b70]:fsd ft11, 1504(a5)<br> [0x80000b74]:sw a7, 1508(a5)<br> |
|  96|[0x80004904]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x42f and fm2 == 0x249249236db6d and rm_val == 0  #nosat<br>                                                                                             |[0x80000b84]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1520(a5)<br> [0x80000b90]:sw a7, 1524(a5)<br> |
|  97|[0x80004914]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x42e and fm2 == 0x24924923fffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000ba0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ba4]:csrrs a7, fflags, zero<br> [0x80000ba8]:fsd ft11, 1536(a5)<br> [0x80000bac]:sw a7, 1540(a5)<br> |
|  98|[0x80004924]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x8acb90f65c87a and rm_val == 0  #nosat<br>                                                                                             |[0x80000bbc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000bc0]:csrrs a7, fflags, zero<br> [0x80000bc4]:fsd ft11, 1552(a5)<br> [0x80000bc8]:sw a7, 1556(a5)<br> |
|  99|[0x80004934]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x42c and fm2 == 0x9ec8e950cf646 and rm_val == 0  #nosat<br>                                                                                             |[0x80000bd8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000bdc]:csrrs a7, fflags, zero<br> [0x80000be0]:fsd ft11, 1568(a5)<br> [0x80000be4]:sw a7, 1572(a5)<br> |
| 100|[0x80004944]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000010 and fs2 == 1 and fe2 == 0x42e and fm2 == 0xffffffffdfffe and rm_val == 0  #nosat<br>                                                                                             |[0x80000bf4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000bf8]:csrrs a7, fflags, zero<br> [0x80000bfc]:fsd ft11, 1584(a5)<br> [0x80000c00]:sw a7, 1588(a5)<br> |
| 101|[0x80004954]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xf07c1f07b26c8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000c10]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000c14]:csrrs a7, fflags, zero<br> [0x80000c18]:fsd ft11, 1600(a5)<br> [0x80000c1c]:sw a7, 1604(a5)<br> |
| 102|[0x80004964]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x414141413c3c3 and rm_val == 0  #nosat<br>                                                                                             |[0x80000c2c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000c30]:csrrs a7, fflags, zero<br> [0x80000c34]:fsd ft11, 1616(a5)<br> [0x80000c38]:sw a7, 1620(a5)<br> |
| 103|[0x80004974]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x51d07eae2cdda and rm_val == 0  #nosat<br>                                                                                             |[0x80000c48]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1632(a5)<br> [0x80000c54]:sw a7, 1636(a5)<br> |
| 104|[0x80004984]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003c and fs2 == 1 and fe2 == 0x42d and fm2 == 0x111111110ffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000c64]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000c68]:csrrs a7, fflags, zero<br> [0x80000c6c]:fsd ft11, 1648(a5)<br> [0x80000c70]:sw a7, 1652(a5)<br> |
| 105|[0x80004994]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x4141414140a09 and rm_val == 0  #nosat<br>                                                                                             |[0x80000c80]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000c84]:csrrs a7, fflags, zero<br> [0x80000c88]:fsd ft11, 1664(a5)<br> [0x80000c8c]:sw a7, 1668(a5)<br> |
| 106|[0x800049a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x42c and fm2 == 0xbacf914c1b3e3 and rm_val == 0  #nosat<br>                                                                                             |[0x80000c9c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ca0]:csrrs a7, fflags, zero<br> [0x80000ca4]:fsd ft11, 1680(a5)<br> [0x80000ca8]:sw a7, 1684(a5)<br> |
| 107|[0x800049b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x42e and fm2 == 0x47ae147ae11ea and rm_val == 0  #nosat<br>                                                                                             |[0x80000cb8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000cbc]:csrrs a7, fflags, zero<br> [0x80000cc0]:fsd ft11, 1696(a5)<br> [0x80000cc4]:sw a7, 1700(a5)<br> |
| 108|[0x800049c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x51d07eae2f6c2 and rm_val == 0  #nosat<br>                                                                                             |[0x80000cd4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000cd8]:csrrs a7, fflags, zero<br> [0x80000cdc]:fsd ft11, 1712(a5)<br> [0x80000ce0]:sw a7, 1716(a5)<br> |
| 109|[0x800049d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 1 and fe2 == 0x42f and fm2 == 0x99999999998cb and rm_val == 0  #nosat<br>                                                                                             |[0x80000cf0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000cf4]:csrrs a7, fflags, zero<br> [0x80000cf8]:fsd ft11, 1728(a5)<br> [0x80000cfc]:sw a7, 1732(a5)<br> |
| 110|[0x800049e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x9999999999932 and rm_val == 0  #nosat<br>                                                                                             |[0x80000d0c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d10]:csrrs a7, fflags, zero<br> [0x80000d14]:fsd ft11, 1744(a5)<br> [0x80000d18]:sw a7, 1748(a5)<br> |
| 111|[0x800049f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x430 and fm2 == 0xfffffffffffbe and rm_val == 0  #nosat<br>                                                                                             |[0x80000d28]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1760(a5)<br> [0x80000d34]:sw a7, 1764(a5)<br> |
| 112|[0x80004a04]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x432 and fm2 == 0xfffffffffffde and rm_val == 0  #nosat<br>                                                                                             |[0x80000d44]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d48]:csrrs a7, fflags, zero<br> [0x80000d4c]:fsd ft11, 1776(a5)<br> [0x80000d50]:sw a7, 1780(a5)<br> |
| 113|[0x80004a14]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x42c and fm2 == 0x9ec8e951033cb and rm_val == 0  #nosat<br>                                                                                             |[0x80000d60]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d64]:csrrs a7, fflags, zero<br> [0x80000d68]:fsd ft11, 1792(a5)<br> [0x80000d6c]:sw a7, 1796(a5)<br> |
| 114|[0x80004a24]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x42e and fm2 == 0x2f684bda12f62 and rm_val == 0  #nosat<br>                                                                                             |[0x80000d7c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d80]:csrrs a7, fflags, zero<br> [0x80000d84]:fsd ft11, 1808(a5)<br> [0x80000d88]:sw a7, 1812(a5)<br> |
| 115|[0x80004a34]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x42d and fm2 == 0x041041041040d and rm_val == 0  #nosat<br>                                                                                             |[0x80000d98]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000d9c]:csrrs a7, fflags, zero<br> [0x80000da0]:fsd ft11, 1824(a5)<br> [0x80000da4]:sw a7, 1828(a5)<br> |
| 116|[0x80004a44]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x3521cfb2b78bf and rm_val == 0  #nosat<br>                                                                                             |[0x80000db4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000db8]:csrrs a7, fflags, zero<br> [0x80000dbc]:fsd ft11, 1840(a5)<br> [0x80000dc0]:sw a7, 1844(a5)<br> |
| 117|[0x80004a54]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x42d and fm2 == 0x1a7b960d3dcaf and rm_val == 0  #nosat<br>                                                                                             |[0x80000dd0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000dd4]:csrrs a7, fflags, zero<br> [0x80000dd8]:fsd ft11, 1856(a5)<br> [0x80000ddc]:sw a7, 1860(a5)<br> |
| 118|[0x80004a64]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000057 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x78a4c8149902e and rm_val == 0  #nosat<br>                                                                                             |[0x80000dec]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000df0]:csrrs a7, fflags, zero<br> [0x80000df4]:fsd ft11, 1872(a5)<br> [0x80000df8]:sw a7, 1876(a5)<br> |
| 119|[0x80004a74]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x99999997ffffe and rm_val == 0  #nosat<br>                                                                                             |[0x80000e08]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1888(a5)<br> [0x80000e14]:sw a7, 1892(a5)<br> |
| 120|[0x80004a84]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x42f and fm2 == 0x3b13b13a76275 and rm_val == 0  #nosat<br>                                                                                             |[0x80000e24]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e28]:csrrs a7, fflags, zero<br> [0x80000e2c]:fsd ft11, 1904(a5)<br> [0x80000e30]:sw a7, 1908(a5)<br> |
| 121|[0x80004a94]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x745d1745745d0 and rm_val == 0  #nosat<br>                                                                                             |[0x80000e40]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e44]:csrrs a7, fflags, zero<br> [0x80000e48]:fsd ft11, 1920(a5)<br> [0x80000e4c]:sw a7, 1924(a5)<br> |
| 122|[0x80004aa4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x9999999966665 and rm_val == 0  #nosat<br>                                                                                             |[0x80000e5c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e60]:csrrs a7, fflags, zero<br> [0x80000e64]:fsd ft11, 1936(a5)<br> [0x80000e68]:sw a7, 1940(a5)<br> |
| 123|[0x80004ab4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x702e05c0a1141 and rm_val == 0  #nosat<br>                                                                                             |[0x80000e78]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e7c]:csrrs a7, fflags, zero<br> [0x80000e80]:fsd ft11, 1952(a5)<br> [0x80000e84]:sw a7, 1956(a5)<br> |
| 124|[0x80004ac4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x42d and fm2 == 0x5c9882b92620a and rm_val == 0  #nosat<br>                                                                                             |[0x80000e94]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000e98]:csrrs a7, fflags, zero<br> [0x80000e9c]:fsd ft11, 1968(a5)<br> [0x80000ea0]:sw a7, 1972(a5)<br> |
| 125|[0x80004ad4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000010 and fs2 == 0 and fe2 == 0x42e and fm2 == 0xfffffffff7ffe and rm_val == 0  #nosat<br>                                                                                             |[0x80000eb0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000eb4]:csrrs a7, fflags, zero<br> [0x80000eb8]:fsd ft11, 1984(a5)<br> [0x80000ebc]:sw a7, 1988(a5)<br> |
| 126|[0x80004ae4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x431 and fm2 == 0x5555555552aa9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000ecc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ed0]:csrrs a7, fflags, zero<br> [0x80000ed4]:fsd ft11, 2000(a5)<br> [0x80000ed8]:sw a7, 2004(a5)<br> |
| 127|[0x80004af4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x42d and fm2 == 0x084210841ffff and rm_val == 0  #nosat<br>                                                                                             |[0x80000ee8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 2016(a5)<br> [0x80000ef4]:sw a7, 2020(a5)<br> |
| 128|[0x8000470c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x42f and fm2 == 0x3b13b13b1313a and rm_val == 0  #nosat<br>                                                                                             |[0x80000f10]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f14]:csrrs a7, fflags, zero<br> [0x80000f18]:fsd ft11, 0(a5)<br> [0x80000f1c]:sw a7, 4(a5)<br>       |
| 129|[0x8000471c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x42c and fm2 == 0xb4e81b4e81479 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f2c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f30]:csrrs a7, fflags, zero<br> [0x80000f34]:fsd ft11, 16(a5)<br> [0x80000f38]:sw a7, 20(a5)<br>     |
| 130|[0x8000472c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x2f684bda12d08 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f48]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 32(a5)<br> [0x80000f54]:sw a7, 36(a5)<br>     |
| 131|[0x8000473c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004c and fs2 == 0 and fe2 == 0x42c and fm2 == 0xaf286bca1ad78 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f64]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs a7, fflags, zero<br> [0x80000f6c]:fsd ft11, 48(a5)<br> [0x80000f70]:sw a7, 52(a5)<br>     |
| 132|[0x8000474c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x431 and fm2 == 0x55555555554a9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f80]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:fsd ft11, 64(a5)<br> [0x80000f8c]:sw a7, 68(a5)<br>     |
| 133|[0x8000475c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x29e4129e41253 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f9c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fa0]:csrrs a7, fflags, zero<br> [0x80000fa4]:fsd ft11, 80(a5)<br> [0x80000fa8]:sw a7, 84(a5)<br>     |
| 134|[0x8000476c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x42c and fm2 == 0x5c9882b93102a and rm_val == 0  #nosat<br>                                                                                             |[0x80000fb8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fbc]:csrrs a7, fflags, zero<br> [0x80000fc0]:fsd ft11, 96(a5)<br> [0x80000fc4]:sw a7, 100(a5)<br>    |
| 135|[0x8000477c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x4e5e0a72f0523 and rm_val == 0  #nosat<br>                                                                                             |[0x80000fd4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:fsd ft11, 112(a5)<br> [0x80000fe0]:sw a7, 116(a5)<br>   |
| 136|[0x8000478c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 0 and fe2 == 0x42e and fm2 == 0x2492492492488 and rm_val == 0  #nosat<br>                                                                                             |[0x80000ff0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000ff4]:csrrs a7, fflags, zero<br> [0x80000ff8]:fsd ft11, 128(a5)<br> [0x80000ffc]:sw a7, 132(a5)<br>   |
| 137|[0x8000479c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x8acb90f6bf3a2 and rm_val == 0  #nosat<br>                                                                                             |[0x8000100c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001010]:csrrs a7, fflags, zero<br> [0x80001014]:fsd ft11, 144(a5)<br> [0x80001018]:sw a7, 148(a5)<br>   |
| 138|[0x800047ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x9999999999995 and rm_val == 0  #nosat<br>                                                                                             |[0x80001028]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 160(a5)<br> [0x80001034]:sw a7, 164(a5)<br>   |
| 139|[0x800047bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003c and fs2 == 0 and fe2 == 0x42d and fm2 == 0x111111111110f and rm_val == 0  #nosat<br>                                                                                             |[0x80001044]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001048]:csrrs a7, fflags, zero<br> [0x8000104c]:fsd ft11, 176(a5)<br> [0x80001050]:sw a7, 180(a5)<br>   |
| 140|[0x800047cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 1 and fe2 == 0x40e and fm2 == 0xe913226357e17 and rm_val == 0  #nosat<br>                                                                                             |[0x80001060]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001064]:csrrs a7, fflags, zero<br> [0x80001068]:fsd ft11, 192(a5)<br> [0x8000106c]:sw a7, 196(a5)<br>   |
| 141|[0x800047dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001c and fs2 == 1 and fe2 == 0x40f and fm2 == 0x2492524924925 and rm_val == 0  #nosat<br>                                                                                             |[0x8000107c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001080]:csrrs a7, fflags, zero<br> [0x80001084]:fsd ft11, 208(a5)<br> [0x80001088]:sw a7, 212(a5)<br>   |
| 142|[0x800047ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x40e and fm2 == 0x1111222222222 and rm_val == 0  #nosat<br>                                                                                             |[0x80001098]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000109c]:csrrs a7, fflags, zero<br> [0x800010a0]:fsd ft11, 224(a5)<br> [0x800010a4]:sw a7, 228(a5)<br>   |
| 143|[0x800047fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 1 and fe2 == 0x40d and fm2 == 0x5555800000000 and rm_val == 0  #nosat<br>                                                                                             |[0x800010b4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800010b8]:csrrs a7, fflags, zero<br> [0x800010bc]:fsd ft11, 240(a5)<br> [0x800010c0]:sw a7, 244(a5)<br>   |
| 144|[0x8000480c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x40d and fm2 == 0x3b14000000000 and rm_val == 0  #nosat<br>                                                                                             |[0x800010d0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800010d4]:csrrs a7, fflags, zero<br> [0x800010d8]:fsd ft11, 256(a5)<br> [0x800010dc]:sw a7, 260(a5)<br>   |
| 145|[0x8000481c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001f and fs2 == 1 and fe2 == 0x40b and fm2 == 0x084294a5294a5 and rm_val == 0  #nosat<br>                                                                                             |[0x800010ec]:fmul.d ft11, ft10, ft9, dyn<br> [0x800010f0]:csrrs a7, fflags, zero<br> [0x800010f4]:fsd ft11, 272(a5)<br> [0x800010f8]:sw a7, 276(a5)<br>   |
| 146|[0x8000482c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x408 and fm2 == 0x6059765d9765e and rm_val == 0  #nosat<br>                                                                                             |[0x80001108]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000110c]:csrrs a7, fflags, zero<br> [0x80001110]:fsd ft11, 288(a5)<br> [0x80001114]:sw a7, 292(a5)<br>   |
| 147|[0x8000483c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x40a and fm2 == 0x1113333333333 and rm_val == 0  #nosat<br>                                                                                             |[0x80001124]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001128]:csrrs a7, fflags, zero<br> [0x8000112c]:fsd ft11, 304(a5)<br> [0x80001130]:sw a7, 308(a5)<br>   |
| 148|[0x8000484c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x407 and fm2 == 0x3526a439f656f and rm_val == 0  #nosat<br>                                                                                             |[0x80001140]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001144]:csrrs a7, fflags, zero<br> [0x80001148]:fsd ft11, 320(a5)<br> [0x8000114c]:sw a7, 324(a5)<br>   |
| 149|[0x8000485c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x409 and fm2 == 0x99a6666666666 and rm_val == 0  #nosat<br>                                                                                             |[0x8000115c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001160]:csrrs a7, fflags, zero<br> [0x80001164]:fsd ft11, 336(a5)<br> [0x80001168]:sw a7, 340(a5)<br>   |
| 150|[0x8000486c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x40b and fm2 == 0x0010000000000 and rm_val == 0  #nosat<br>                                                                                             |[0x80001178]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000117c]:csrrs a7, fflags, zero<br> [0x80001180]:fsd ft11, 352(a5)<br> [0x80001184]:sw a7, 356(a5)<br>   |
| 151|[0x8000487c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x403 and fm2 == 0xe21e1e1e1e1e2 and rm_val == 0  #nosat<br>                                                                                             |[0x80001194]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001198]:csrrs a7, fflags, zero<br> [0x8000119c]:fsd ft11, 368(a5)<br> [0x800011a0]:sw a7, 372(a5)<br>   |
| 152|[0x8000488c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 1 and fe2 == 0x403 and fm2 == 0x2a2e8ba2e8ba3 and rm_val == 0  #nosat<br>                                                                                             |[0x800011b0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 384(a5)<br> [0x800011bc]:sw a7, 388(a5)<br>   |
| 153|[0x8000489c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x402 and fm2 == 0xf1745d1745d17 and rm_val == 0  #nosat<br>                                                                                             |[0x800011cc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800011d0]:csrrs a7, fflags, zero<br> [0x800011d4]:fsd ft11, 400(a5)<br> [0x800011d8]:sw a7, 404(a5)<br>   |
| 154|[0x800048ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x400 and fm2 == 0x697e97e97e97f and rm_val == 0  #nosat<br>                                                                                             |[0x800011e8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800011ec]:csrrs a7, fflags, zero<br> [0x800011f0]:fsd ft11, 416(a5)<br> [0x800011f4]:sw a7, 420(a5)<br>   |
| 155|[0x800048bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x9cccccccccccd and rm_val == 0  #nosat<br>                                                                                             |[0x80001204]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001208]:csrrs a7, fflags, zero<br> [0x8000120c]:fsd ft11, 432(a5)<br> [0x80001210]:sw a7, 436(a5)<br>   |
| 156|[0x800048cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xb5e50d79435e5 and rm_val == 0  #nosat<br>                                                                                             |[0x80001220]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001224]:csrrs a7, fflags, zero<br> [0x80001228]:fsd ft11, 448(a5)<br> [0x8000122c]:sw a7, 452(a5)<br>   |
| 157|[0x800048dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                             |[0x8000123c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001240]:csrrs a7, fflags, zero<br> [0x80001244]:fsd ft11, 464(a5)<br> [0x80001248]:sw a7, 468(a5)<br>   |
| 158|[0x800048ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000039 and fs2 == 1 and fe2 == 0x3fd and fm2 == 0x31674c59d3167 and rm_val == 0  #nosat<br>                                                                                             |[0x80001258]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000125c]:csrrs a7, fflags, zero<br> [0x80001260]:fsd ft11, 480(a5)<br> [0x80001264]:sw a7, 484(a5)<br>   |
| 159|[0x800048fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x3fd and fm2 == 0x3333333333333 and rm_val == 0  #nosat<br>                                                                                             |[0x80001274]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001278]:csrrs a7, fflags, zero<br> [0x8000127c]:fsd ft11, 496(a5)<br> [0x80001280]:sw a7, 500(a5)<br>   |
| 160|[0x8000490c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                             |[0x80001290]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 512(a5)<br> [0x8000129c]:sw a7, 516(a5)<br>   |
| 161|[0x8000491c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 1 and fe2 == 0x3fa and fm2 == 0x745d1745d1746 and rm_val == 0  #nosat<br>                                                                                             |[0x800012ac]:fmul.d ft11, ft10, ft9, dyn<br> [0x800012b0]:csrrs a7, fflags, zero<br> [0x800012b4]:fsd ft11, 528(a5)<br> [0x800012b8]:sw a7, 532(a5)<br>   |
| 162|[0x8000492c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000052 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000008000 and rm_val == 0  #nosat<br>                                                                                             |[0x800012c8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800012cc]:csrrs a7, fflags, zero<br> [0x800012d0]:fsd ft11, 544(a5)<br> [0x800012d4]:sw a7, 548(a5)<br>   |
| 163|[0x8000493c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 0 and fe2 == 0x411 and fm2 == 0x3b13b62762762 and rm_val == 0  #nosat<br>                                                                                             |[0x800012e4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800012e8]:csrrs a7, fflags, zero<br> [0x800012ec]:fsd ft11, 560(a5)<br> [0x800012f0]:sw a7, 564(a5)<br>   |
| 164|[0x8000494c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x86186db6db6db and rm_val == 0  #nosat<br>                                                                                             |[0x80001300]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001304]:csrrs a7, fflags, zero<br> [0x80001308]:fsd ft11, 576(a5)<br> [0x8000130c]:sw a7, 580(a5)<br>   |
| 165|[0x8000495c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x9999b33333333 and rm_val == 0  #nosat<br>                                                                                             |[0x8000131c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001320]:csrrs a7, fflags, zero<br> [0x80001324]:fsd ft11, 592(a5)<br> [0x80001328]:sw a7, 596(a5)<br>   |
| 166|[0x8000496c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000008 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x0000200000000 and rm_val == 0  #nosat<br>                                                                                             |[0x80001338]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000133c]:csrrs a7, fflags, zero<br> [0x80001340]:fsd ft11, 608(a5)<br> [0x80001344]:sw a7, 612(a5)<br>   |
| 167|[0x8000497c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000012 and fs2 == 0 and fe2 == 0x40c and fm2 == 0xc71ce38e38e39 and rm_val == 0  #nosat<br>                                                                                             |[0x80001354]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001358]:csrrs a7, fflags, zero<br> [0x8000135c]:fsd ft11, 624(a5)<br> [0x80001360]:sw a7, 628(a5)<br>   |
| 168|[0x8000498c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x409 and fm2 == 0x6c17777777777 and rm_val == 0  #nosat<br>                                                                                             |[0x80001370]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 640(a5)<br> [0x8000137c]:sw a7, 644(a5)<br>   |
| 169|[0x8000499c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x8619e79e79e7a and rm_val == 0  #nosat<br>                                                                                             |[0x8000138c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001390]:csrrs a7, fflags, zero<br> [0x80001394]:fsd ft11, 656(a5)<br> [0x80001398]:sw a7, 660(a5)<br>   |
| 170|[0x800049ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004e and fs2 == 0 and fe2 == 0x407 and fm2 == 0xa41d89d89d89e and rm_val == 0  #nosat<br>                                                                                             |[0x800013a8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800013ac]:csrrs a7, fflags, zero<br> [0x800013b0]:fsd ft11, 672(a5)<br> [0x800013b4]:sw a7, 676(a5)<br>   |
| 171|[0x800049bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000022 and fs2 == 0 and fe2 == 0x407 and fm2 == 0xe1e9696969697 and rm_val == 0  #nosat<br>                                                                                             |[0x800013c4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800013c8]:csrrs a7, fflags, zero<br> [0x800013cc]:fsd ft11, 688(a5)<br> [0x800013d0]:sw a7, 692(a5)<br>   |
| 172|[0x800049cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000013 and fs2 == 0 and fe2 == 0x407 and fm2 == 0xaf35e50d79436 and rm_val == 0  #nosat<br>                                                                                             |[0x800013e0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800013e4]:csrrs a7, fflags, zero<br> [0x800013e8]:fsd ft11, 704(a5)<br> [0x800013ec]:sw a7, 708(a5)<br>   |
| 173|[0x800049dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 0 and fe2 == 0x404 and fm2 == 0xf09b26c9b26ca and rm_val == 0  #nosat<br>                                                                                             |[0x800013fc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001400]:csrrs a7, fflags, zero<br> [0x80001404]:fsd ft11, 720(a5)<br> [0x80001408]:sw a7, 724(a5)<br>   |
| 174|[0x800049ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x404 and fm2 == 0x4169696969697 and rm_val == 0  #nosat<br>                                                                                             |[0x80001418]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000141c]:csrrs a7, fflags, zero<br> [0x80001420]:fsd ft11, 736(a5)<br> [0x80001424]:sw a7, 740(a5)<br>   |
| 175|[0x800049fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x402 and fm2 == 0xe98d5f85bb395 and rm_val == 0  #nosat<br>                                                                                             |[0x80001434]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001438]:csrrs a7, fflags, zero<br> [0x8000143c]:fsd ft11, 752(a5)<br> [0x80001440]:sw a7, 756(a5)<br>   |
| 176|[0x80004a0c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x402 and fm2 == 0x1b08d3dcb08d4 and rm_val == 0  #nosat<br>                                                                                             |[0x80001450]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 768(a5)<br> [0x8000145c]:sw a7, 772(a5)<br>   |
| 177|[0x80004a1c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 0 and fe2 == 0x403 and fm2 == 0x25b6db6db6db7 and rm_val == 0  #nosat<br>                                                                                             |[0x8000146c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001470]:csrrs a7, fflags, zero<br> [0x80001474]:fsd ft11, 784(a5)<br> [0x80001478]:sw a7, 788(a5)<br>   |
| 178|[0x80004a2c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x7745d1745d174 and rm_val == 0  #nosat<br>                                                                                             |[0x80001488]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000148c]:csrrs a7, fflags, zero<br> [0x80001490]:fsd ft11, 800(a5)<br> [0x80001494]:sw a7, 804(a5)<br>   |
| 179|[0x80004a3c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x401 and fm2 == 0x5aaaaaaaaaaab and rm_val == 0  #nosat<br>                                                                                             |[0x800014a4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800014a8]:csrrs a7, fflags, zero<br> [0x800014ac]:fsd ft11, 816(a5)<br> [0x800014b0]:sw a7, 820(a5)<br>   |
| 180|[0x80004a4c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x14fbcda3ac10d and rm_val == 0  #nosat<br>                                                                                             |[0x800014c0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800014c4]:csrrs a7, fflags, zero<br> [0x800014c8]:fsd ft11, 832(a5)<br> [0x800014cc]:sw a7, 836(a5)<br>   |
| 181|[0x80004a5c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x7e97e97e97e98 and rm_val == 0  #nosat<br>                                                                                             |[0x800014dc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800014e0]:csrrs a7, fflags, zero<br> [0x800014e4]:fsd ft11, 848(a5)<br> [0x800014e8]:sw a7, 852(a5)<br>   |
| 182|[0x80004a6c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000034 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x6276276276276 and rm_val == 0  #nosat<br>                                                                                             |[0x800014f8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800014fc]:csrrs a7, fflags, zero<br> [0x80001500]:fsd ft11, 864(a5)<br> [0x80001504]:sw a7, 868(a5)<br>   |
| 183|[0x80004a7c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000022 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x2d2d2d2d2d2d3 and rm_val == 0  #nosat<br>                                                                                             |[0x80001514]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001518]:csrrs a7, fflags, zero<br> [0x8000151c]:fsd ft11, 880(a5)<br> [0x80001520]:sw a7, 884(a5)<br>   |
| 184|[0x80004a8c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x02b1da46102b2 and rm_val == 0  #nosat<br>                                                                                             |[0x80001530]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 896(a5)<br> [0x8000153c]:sw a7, 900(a5)<br>   |
| 185|[0x80004a9c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000063 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                                                             |[0x8000154c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001550]:csrrs a7, fflags, zero<br> [0x80001554]:fsd ft11, 912(a5)<br> [0x80001558]:sw a7, 916(a5)<br>   |
| 186|[0x80004aac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003a and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000003fffc6 and rm_val == 0  #nosat<br>                                                                                             |[0x80001568]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000156c]:csrrs a7, fflags, zero<br> [0x80001570]:fsd ft11, 928(a5)<br> [0x80001574]:sw a7, 932(a5)<br>   |
| 187|[0x80004abc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000001ffffc and rm_val == 0  #nosat<br>                                                                                             |[0x80001584]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001588]:csrrs a7, fflags, zero<br> [0x8000158c]:fsd ft11, 944(a5)<br> [0x80001590]:sw a7, 948(a5)<br>   |
| 188|[0x80004acc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000fffd4 and rm_val == 0  #nosat<br>                                                                                             |[0x800015a0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800015a4]:csrrs a7, fflags, zero<br> [0x800015a8]:fsd ft11, 960(a5)<br> [0x800015ac]:sw a7, 964(a5)<br>   |
| 189|[0x80004adc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000007fff1 and rm_val == 0  #nosat<br>                                                                                             |[0x800015bc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800015c0]:csrrs a7, fflags, zero<br> [0x800015c4]:fsd ft11, 976(a5)<br> [0x800015c8]:sw a7, 980(a5)<br>   |
| 190|[0x80004aec]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000060 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000003ffa0 and rm_val == 0  #nosat<br>                                                                                             |[0x800015d8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800015dc]:csrrs a7, fflags, zero<br> [0x800015e0]:fsd ft11, 992(a5)<br> [0x800015e4]:sw a7, 996(a5)<br>   |
| 191|[0x80004afc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000001ffb1 and rm_val == 0  #nosat<br>                                                                                             |[0x800015f4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800015f8]:csrrs a7, fflags, zero<br> [0x800015fc]:fsd ft11, 1008(a5)<br> [0x80001600]:sw a7, 1012(a5)<br> |
| 192|[0x80004b0c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000000ffff and rm_val == 0  #nosat<br>                                                                                             |[0x80001610]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 1024(a5)<br> [0x8000161c]:sw a7, 1028(a5)<br> |
| 193|[0x80004b1c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000014 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000007fec and rm_val == 0  #nosat<br>                                                                                             |[0x8000162c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001630]:csrrs a7, fflags, zero<br> [0x80001634]:fsd ft11, 1040(a5)<br> [0x80001638]:sw a7, 1044(a5)<br> |
| 194|[0x80004b2c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000006 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000003ffa and rm_val == 0  #nosat<br>                                                                                             |[0x80001648]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000164c]:csrrs a7, fflags, zero<br> [0x80001650]:fsd ft11, 1056(a5)<br> [0x80001654]:sw a7, 1060(a5)<br> |
| 195|[0x80004b3c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000032 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000001fce and rm_val == 0  #nosat<br>                                                                                             |[0x80001664]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001668]:csrrs a7, fflags, zero<br> [0x8000166c]:fsd ft11, 1072(a5)<br> [0x80001670]:sw a7, 1076(a5)<br> |
| 196|[0x80004b4c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000fa7 and rm_val == 0  #nosat<br>                                                                                             |[0x80001680]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001684]:csrrs a7, fflags, zero<br> [0x80001688]:fsd ft11, 1088(a5)<br> [0x8000168c]:sw a7, 1092(a5)<br> |
| 197|[0x80004b5c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000018 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000007e8 and rm_val == 0  #nosat<br>                                                                                             |[0x8000169c]:fmul.d ft11, ft10, ft9, dyn<br> [0x800016a0]:csrrs a7, fflags, zero<br> [0x800016a4]:fsd ft11, 1104(a5)<br> [0x800016a8]:sw a7, 1108(a5)<br> |
| 198|[0x80004b6c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000003eb and rm_val == 0  #nosat<br>                                                                                             |[0x800016b8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800016bc]:csrrs a7, fflags, zero<br> [0x800016c0]:fsd ft11, 1120(a5)<br> [0x800016c4]:sw a7, 1124(a5)<br> |
| 199|[0x80004b7c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000001df and rm_val == 0  #nosat<br>                                                                                             |[0x800016d4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 1136(a5)<br> [0x800016e0]:sw a7, 1140(a5)<br> |
| 200|[0x80004b8c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000003 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x00000000000fd and rm_val == 0  #nosat<br>                                                                                             |[0x800016f0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800016f4]:csrrs a7, fflags, zero<br> [0x800016f8]:fsd ft11, 1152(a5)<br> [0x800016fc]:sw a7, 1156(a5)<br> |
| 201|[0x80004b9c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000006 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x000000000007a and rm_val == 0  #nosat<br>                                                                                             |[0x8000170c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001710]:csrrs a7, fflags, zero<br> [0x80001714]:fsd ft11, 1168(a5)<br> [0x80001718]:sw a7, 1172(a5)<br> |
| 202|[0x80004bac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000037 and rm_val == 0  #nosat<br>                                                                                             |[0x80001728]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000172c]:csrrs a7, fflags, zero<br> [0x80001730]:fsd ft11, 1184(a5)<br> [0x80001734]:sw a7, 1188(a5)<br> |
| 203|[0x80004bbc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000000b and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                             |[0x80001744]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001748]:csrrs a7, fflags, zero<br> [0x8000174c]:fsd ft11, 1200(a5)<br> [0x80001750]:sw a7, 1204(a5)<br> |
| 204|[0x80004bcc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000057 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff72 and rm_val == 0  #nosat<br>                                                                                             |[0x80001760]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001764]:csrrs a7, fflags, zero<br> [0x80001768]:fsd ft11, 1216(a5)<br> [0x8000176c]:sw a7, 1220(a5)<br> |
| 205|[0x80004b04]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 0 and fe2 == 0x40e and fm2 == 0xf07c1f07c1f08 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d00]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d04]:csrrs a7, fflags, zero<br> [0x80001d08]:fsd ft11, 0(a5)<br> [0x80001d0c]:sw a7, 4(a5)<br>       |
| 206|[0x80004b14]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x642c8590b2164 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d1c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d20]:csrrs a7, fflags, zero<br> [0x80001d24]:fsd ft11, 16(a5)<br> [0x80001d28]:sw a7, 20(a5)<br>     |
| 207|[0x80004b24]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x40e and fm2 == 0x642c8590b2164 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d38]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d3c]:csrrs a7, fflags, zero<br> [0x80001d40]:fsd ft11, 32(a5)<br> [0x80001d44]:sw a7, 36(a5)<br>     |
| 208|[0x80004b34]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001e and fs2 == 0 and fe2 == 0x40d and fm2 == 0x1111111111111 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d54]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d58]:csrrs a7, fflags, zero<br> [0x80001d5c]:fsd ft11, 48(a5)<br> [0x80001d60]:sw a7, 52(a5)<br>     |
| 209|[0x80004b44]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x40c and fm2 == 0x642c8590b2164 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d70]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d74]:csrrs a7, fflags, zero<br> [0x80001d78]:fsd ft11, 64(a5)<br> [0x80001d7c]:sw a7, 68(a5)<br>     |
| 210|[0x80004b54]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000016 and fs2 == 0 and fe2 == 0x40b and fm2 == 0x745d1745d1746 and rm_val == 0  #nosat<br>                                                                                             |[0x80001d8c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001d90]:csrrs a7, fflags, zero<br> [0x80001d94]:fsd ft11, 80(a5)<br> [0x80001d98]:sw a7, 84(a5)<br>     |
| 211|[0x80004b64]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x409 and fm2 == 0x29e4129e4129e and rm_val == 0  #nosat<br>                                                                                             |[0x80001da8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001dac]:csrrs a7, fflags, zero<br> [0x80001db0]:fsd ft11, 96(a5)<br> [0x80001db4]:sw a7, 100(a5)<br>    |
| 212|[0x80004b74]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000029 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x8f9c18f9c18fa and rm_val == 0  #nosat<br>                                                                                             |[0x80001dc4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001dc8]:csrrs a7, fflags, zero<br> [0x80001dcc]:fsd ft11, 112(a5)<br> [0x80001dd0]:sw a7, 116(a5)<br>   |
| 213|[0x80004b84]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x407 and fm2 == 0x5c9882b931057 and rm_val == 0  #nosat<br>                                                                                             |[0x80001de0]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001de4]:csrrs a7, fflags, zero<br> [0x80001de8]:fsd ft11, 128(a5)<br> [0x80001dec]:sw a7, 132(a5)<br>   |
| 214|[0x80004b94]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x409 and fm2 == 0x999999999999a and rm_val == 0  #nosat<br>                                                                                             |[0x80001dfc]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001e00]:csrrs a7, fflags, zero<br> [0x80001e04]:fsd ft11, 144(a5)<br> [0x80001e08]:sw a7, 148(a5)<br>   |
| 215|[0x80004ba4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x404 and fm2 == 0xf81f81f81f820 and rm_val == 0  #nosat<br>                                                                                             |[0x80001e18]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001e1c]:csrrs a7, fflags, zero<br> [0x80001e20]:fsd ft11, 160(a5)<br> [0x80001e24]:sw a7, 164(a5)<br>   |
| 216|[0x80004bb4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x403 and fm2 == 0xd41d41d41d41d and rm_val == 0  #nosat<br>                                                                                             |[0x80001e34]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001e38]:csrrs a7, fflags, zero<br> [0x80001e3c]:fsd ft11, 176(a5)<br> [0x80001e40]:sw a7, 180(a5)<br>   |
| 217|[0x80004bc4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 0 and fe2 == 0x402 and fm2 == 0xf07c1f07c1f08 and rm_val == 0  #nosat<br>                                                                                             |[0x80001e50]:fmul.d ft11, ft10, ft9, dyn<br> [0x80001e54]:csrrs a7, fflags, zero<br> [0x80001e58]:fsd ft11, 192(a5)<br> [0x80001e5c]:sw a7, 196(a5)<br>   |
