
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800010b0')]      |
| SIG_REGION                | [('0x80003a10', '0x80003e80', '284 words')]      |
| COV_LABELS                | fmul_b6      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fmul/riscof_work/fmul_b6-01.S/ref.S    |
| Total Number of coverpoints| 248     |
| Total Coverpoints Hit     | 179      |
| Total Signature Updates   | 79      |
| STAT1                     | 79      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800008e4]:fmul.d ft11, ft10, ft9, dyn
[0x800008e8]:csrrs a7, fflags, zero
[0x800008ec]:fsd ft11, 1136(a5)
[0x800008f0]:sw a7, 1140(a5)
[0x800008f4]:fld ft10, 1152(a6)
[0x800008f8]:fld ft9, 1160(a6)
[0x800008fc]:csrrwi zero, frm, 3

[0x80000900]:fmul.d ft11, ft10, ft9, dyn
[0x80000904]:csrrs a7, fflags, zero
[0x80000908]:fsd ft11, 1152(a5)
[0x8000090c]:sw a7, 1156(a5)
[0x80000910]:fld ft10, 1168(a6)
[0x80000914]:fld ft9, 1176(a6)
[0x80000918]:csrrwi zero, frm, 2

[0x8000091c]:fmul.d ft11, ft10, ft9, dyn
[0x80000920]:csrrs a7, fflags, zero
[0x80000924]:fsd ft11, 1168(a5)
[0x80000928]:sw a7, 1172(a5)
[0x8000092c]:fld ft10, 1184(a6)
[0x80000930]:fld ft9, 1192(a6)
[0x80000934]:csrrwi zero, frm, 1

[0x80000938]:fmul.d ft11, ft10, ft9, dyn
[0x8000093c]:csrrs a7, fflags, zero
[0x80000940]:fsd ft11, 1184(a5)
[0x80000944]:sw a7, 1188(a5)
[0x80000948]:fld ft10, 1200(a6)
[0x8000094c]:fld ft9, 1208(a6)
[0x80000950]:csrrwi zero, frm, 0

[0x80000954]:fmul.d ft11, ft10, ft9, dyn
[0x80000958]:csrrs a7, fflags, zero
[0x8000095c]:fsd ft11, 1200(a5)
[0x80000960]:sw a7, 1204(a5)
[0x80000964]:fld ft10, 1216(a6)
[0x80000968]:fld ft9, 1224(a6)
[0x8000096c]:csrrwi zero, frm, 4

[0x80000970]:fmul.d ft11, ft10, ft9, dyn
[0x80000974]:csrrs a7, fflags, zero
[0x80000978]:fsd ft11, 1216(a5)
[0x8000097c]:sw a7, 1220(a5)
[0x80000980]:fld ft10, 1232(a6)
[0x80000984]:fld ft9, 1240(a6)
[0x80000988]:csrrwi zero, frm, 3

[0x8000098c]:fmul.d ft11, ft10, ft9, dyn
[0x80000990]:csrrs a7, fflags, zero
[0x80000994]:fsd ft11, 1232(a5)
[0x80000998]:sw a7, 1236(a5)
[0x8000099c]:fld ft10, 1248(a6)
[0x800009a0]:fld ft9, 1256(a6)
[0x800009a4]:csrrwi zero, frm, 2

[0x800009a8]:fmul.d ft11, ft10, ft9, dyn
[0x800009ac]:csrrs a7, fflags, zero
[0x800009b0]:fsd ft11, 1248(a5)
[0x800009b4]:sw a7, 1252(a5)
[0x800009b8]:fld ft10, 1264(a6)
[0x800009bc]:fld ft9, 1272(a6)
[0x800009c0]:csrrwi zero, frm, 1

[0x800009c4]:fmul.d ft11, ft10, ft9, dyn
[0x800009c8]:csrrs a7, fflags, zero
[0x800009cc]:fsd ft11, 1264(a5)
[0x800009d0]:sw a7, 1268(a5)
[0x800009d4]:fld ft10, 1280(a6)
[0x800009d8]:fld ft9, 1288(a6)
[0x800009dc]:csrrwi zero, frm, 0

[0x800009e0]:fmul.d ft11, ft10, ft9, dyn
[0x800009e4]:csrrs a7, fflags, zero
[0x800009e8]:fsd ft11, 1280(a5)
[0x800009ec]:sw a7, 1284(a5)
[0x800009f0]:fld ft10, 1296(a6)
[0x800009f4]:fld ft9, 1304(a6)
[0x800009f8]:csrrwi zero, frm, 4

[0x800009fc]:fmul.d ft11, ft10, ft9, dyn
[0x80000a00]:csrrs a7, fflags, zero
[0x80000a04]:fsd ft11, 1296(a5)
[0x80000a08]:sw a7, 1300(a5)
[0x80000a0c]:fld ft10, 1312(a6)
[0x80000a10]:fld ft9, 1320(a6)
[0x80000a14]:csrrwi zero, frm, 3

[0x80000a18]:fmul.d ft11, ft10, ft9, dyn
[0x80000a1c]:csrrs a7, fflags, zero
[0x80000a20]:fsd ft11, 1312(a5)
[0x80000a24]:sw a7, 1316(a5)
[0x80000a28]:fld ft10, 1328(a6)
[0x80000a2c]:fld ft9, 1336(a6)
[0x80000a30]:csrrwi zero, frm, 2

[0x80000a34]:fmul.d ft11, ft10, ft9, dyn
[0x80000a38]:csrrs a7, fflags, zero
[0x80000a3c]:fsd ft11, 1328(a5)
[0x80000a40]:sw a7, 1332(a5)
[0x80000a44]:fld ft10, 1344(a6)
[0x80000a48]:fld ft9, 1352(a6)
[0x80000a4c]:csrrwi zero, frm, 1

[0x80000a50]:fmul.d ft11, ft10, ft9, dyn
[0x80000a54]:csrrs a7, fflags, zero
[0x80000a58]:fsd ft11, 1344(a5)
[0x80000a5c]:sw a7, 1348(a5)
[0x80000a60]:fld ft10, 1360(a6)
[0x80000a64]:fld ft9, 1368(a6)
[0x80000a68]:csrrwi zero, frm, 0

[0x80000a6c]:fmul.d ft11, ft10, ft9, dyn
[0x80000a70]:csrrs a7, fflags, zero
[0x80000a74]:fsd ft11, 1360(a5)
[0x80000a78]:sw a7, 1364(a5)
[0x80000a7c]:fld ft10, 1376(a6)
[0x80000a80]:fld ft9, 1384(a6)
[0x80000a84]:csrrwi zero, frm, 4

[0x80000a88]:fmul.d ft11, ft10, ft9, dyn
[0x80000a8c]:csrrs a7, fflags, zero
[0x80000a90]:fsd ft11, 1376(a5)
[0x80000a94]:sw a7, 1380(a5)
[0x80000a98]:fld ft10, 1392(a6)
[0x80000a9c]:fld ft9, 1400(a6)
[0x80000aa0]:csrrwi zero, frm, 3

[0x80000aa4]:fmul.d ft11, ft10, ft9, dyn
[0x80000aa8]:csrrs a7, fflags, zero
[0x80000aac]:fsd ft11, 1392(a5)
[0x80000ab0]:sw a7, 1396(a5)
[0x80000ab4]:fld ft10, 1408(a6)
[0x80000ab8]:fld ft9, 1416(a6)
[0x80000abc]:csrrwi zero, frm, 2

[0x80000ac0]:fmul.d ft11, ft10, ft9, dyn
[0x80000ac4]:csrrs a7, fflags, zero
[0x80000ac8]:fsd ft11, 1408(a5)
[0x80000acc]:sw a7, 1412(a5)
[0x80000ad0]:fld ft10, 1424(a6)
[0x80000ad4]:fld ft9, 1432(a6)
[0x80000ad8]:csrrwi zero, frm, 1

[0x80000adc]:fmul.d ft11, ft10, ft9, dyn
[0x80000ae0]:csrrs a7, fflags, zero
[0x80000ae4]:fsd ft11, 1424(a5)
[0x80000ae8]:sw a7, 1428(a5)
[0x80000aec]:fld ft10, 1440(a6)
[0x80000af0]:fld ft9, 1448(a6)
[0x80000af4]:csrrwi zero, frm, 0

[0x80000af8]:fmul.d ft11, ft10, ft9, dyn
[0x80000afc]:csrrs a7, fflags, zero
[0x80000b00]:fsd ft11, 1440(a5)
[0x80000b04]:sw a7, 1444(a5)
[0x80000b08]:fld ft10, 1456(a6)
[0x80000b0c]:fld ft9, 1464(a6)
[0x80000b10]:csrrwi zero, frm, 4

[0x80000b14]:fmul.d ft11, ft10, ft9, dyn
[0x80000b18]:csrrs a7, fflags, zero
[0x80000b1c]:fsd ft11, 1456(a5)
[0x80000b20]:sw a7, 1460(a5)
[0x80000b24]:fld ft10, 1472(a6)
[0x80000b28]:fld ft9, 1480(a6)
[0x80000b2c]:csrrwi zero, frm, 3

[0x80000b30]:fmul.d ft11, ft10, ft9, dyn
[0x80000b34]:csrrs a7, fflags, zero
[0x80000b38]:fsd ft11, 1472(a5)
[0x80000b3c]:sw a7, 1476(a5)
[0x80000b40]:fld ft10, 1488(a6)
[0x80000b44]:fld ft9, 1496(a6)
[0x80000b48]:csrrwi zero, frm, 2

[0x80000b4c]:fmul.d ft11, ft10, ft9, dyn
[0x80000b50]:csrrs a7, fflags, zero
[0x80000b54]:fsd ft11, 1488(a5)
[0x80000b58]:sw a7, 1492(a5)
[0x80000b5c]:fld ft10, 1504(a6)
[0x80000b60]:fld ft9, 1512(a6)
[0x80000b64]:csrrwi zero, frm, 1

[0x80000b68]:fmul.d ft11, ft10, ft9, dyn
[0x80000b6c]:csrrs a7, fflags, zero
[0x80000b70]:fsd ft11, 1504(a5)
[0x80000b74]:sw a7, 1508(a5)
[0x80000b78]:fld ft10, 1520(a6)
[0x80000b7c]:fld ft9, 1528(a6)
[0x80000b80]:csrrwi zero, frm, 0

[0x80000b84]:fmul.d ft11, ft10, ft9, dyn
[0x80000b88]:csrrs a7, fflags, zero
[0x80000b8c]:fsd ft11, 1520(a5)
[0x80000b90]:sw a7, 1524(a5)
[0x80000b94]:fld ft10, 1536(a6)
[0x80000b98]:fld ft9, 1544(a6)
[0x80000b9c]:csrrwi zero, frm, 4

[0x80000ba0]:fmul.d ft11, ft10, ft9, dyn
[0x80000ba4]:csrrs a7, fflags, zero
[0x80000ba8]:fsd ft11, 1536(a5)
[0x80000bac]:sw a7, 1540(a5)
[0x80000bb0]:fld ft10, 1552(a6)
[0x80000bb4]:fld ft9, 1560(a6)
[0x80000bb8]:csrrwi zero, frm, 3

[0x80000bbc]:fmul.d ft11, ft10, ft9, dyn
[0x80000bc0]:csrrs a7, fflags, zero
[0x80000bc4]:fsd ft11, 1552(a5)
[0x80000bc8]:sw a7, 1556(a5)
[0x80000bcc]:fld ft10, 1568(a6)
[0x80000bd0]:fld ft9, 1576(a6)
[0x80000bd4]:csrrwi zero, frm, 2

[0x80000bd8]:fmul.d ft11, ft10, ft9, dyn
[0x80000bdc]:csrrs a7, fflags, zero
[0x80000be0]:fsd ft11, 1568(a5)
[0x80000be4]:sw a7, 1572(a5)
[0x80000be8]:fld ft10, 1584(a6)
[0x80000bec]:fld ft9, 1592(a6)
[0x80000bf0]:csrrwi zero, frm, 1

[0x80000bf4]:fmul.d ft11, ft10, ft9, dyn
[0x80000bf8]:csrrs a7, fflags, zero
[0x80000bfc]:fsd ft11, 1584(a5)
[0x80000c00]:sw a7, 1588(a5)
[0x80000c04]:fld ft10, 1600(a6)
[0x80000c08]:fld ft9, 1608(a6)
[0x80000c0c]:csrrwi zero, frm, 0

[0x80000c10]:fmul.d ft11, ft10, ft9, dyn
[0x80000c14]:csrrs a7, fflags, zero
[0x80000c18]:fsd ft11, 1600(a5)
[0x80000c1c]:sw a7, 1604(a5)
[0x80000c20]:fld ft10, 1616(a6)
[0x80000c24]:fld ft9, 1624(a6)
[0x80000c28]:csrrwi zero, frm, 4

[0x80000c2c]:fmul.d ft11, ft10, ft9, dyn
[0x80000c30]:csrrs a7, fflags, zero
[0x80000c34]:fsd ft11, 1616(a5)
[0x80000c38]:sw a7, 1620(a5)
[0x80000c3c]:fld ft10, 1632(a6)
[0x80000c40]:fld ft9, 1640(a6)
[0x80000c44]:csrrwi zero, frm, 3

[0x80000c48]:fmul.d ft11, ft10, ft9, dyn
[0x80000c4c]:csrrs a7, fflags, zero
[0x80000c50]:fsd ft11, 1632(a5)
[0x80000c54]:sw a7, 1636(a5)
[0x80000c58]:fld ft10, 1648(a6)
[0x80000c5c]:fld ft9, 1656(a6)
[0x80000c60]:csrrwi zero, frm, 2

[0x80000c64]:fmul.d ft11, ft10, ft9, dyn
[0x80000c68]:csrrs a7, fflags, zero
[0x80000c6c]:fsd ft11, 1648(a5)
[0x80000c70]:sw a7, 1652(a5)
[0x80000c74]:fld ft10, 1664(a6)
[0x80000c78]:fld ft9, 1672(a6)
[0x80000c7c]:csrrwi zero, frm, 1

[0x80000c80]:fmul.d ft11, ft10, ft9, dyn
[0x80000c84]:csrrs a7, fflags, zero
[0x80000c88]:fsd ft11, 1664(a5)
[0x80000c8c]:sw a7, 1668(a5)
[0x80000c90]:fld ft10, 1680(a6)
[0x80000c94]:fld ft9, 1688(a6)
[0x80000c98]:csrrwi zero, frm, 0

[0x80000c9c]:fmul.d ft11, ft10, ft9, dyn
[0x80000ca0]:csrrs a7, fflags, zero
[0x80000ca4]:fsd ft11, 1680(a5)
[0x80000ca8]:sw a7, 1684(a5)
[0x80000cac]:fld ft10, 1696(a6)
[0x80000cb0]:fld ft9, 1704(a6)
[0x80000cb4]:csrrwi zero, frm, 4

[0x80000cb8]:fmul.d ft11, ft10, ft9, dyn
[0x80000cbc]:csrrs a7, fflags, zero
[0x80000cc0]:fsd ft11, 1696(a5)
[0x80000cc4]:sw a7, 1700(a5)
[0x80000cc8]:fld ft10, 1712(a6)
[0x80000ccc]:fld ft9, 1720(a6)
[0x80000cd0]:csrrwi zero, frm, 3

[0x80000cd4]:fmul.d ft11, ft10, ft9, dyn
[0x80000cd8]:csrrs a7, fflags, zero
[0x80000cdc]:fsd ft11, 1712(a5)
[0x80000ce0]:sw a7, 1716(a5)
[0x80000ce4]:fld ft10, 1728(a6)
[0x80000ce8]:fld ft9, 1736(a6)
[0x80000cec]:csrrwi zero, frm, 2

[0x80000cf0]:fmul.d ft11, ft10, ft9, dyn
[0x80000cf4]:csrrs a7, fflags, zero
[0x80000cf8]:fsd ft11, 1728(a5)
[0x80000cfc]:sw a7, 1732(a5)
[0x80000d00]:fld ft10, 1744(a6)
[0x80000d04]:fld ft9, 1752(a6)
[0x80000d08]:csrrwi zero, frm, 1

[0x80000d0c]:fmul.d ft11, ft10, ft9, dyn
[0x80000d10]:csrrs a7, fflags, zero
[0x80000d14]:fsd ft11, 1744(a5)
[0x80000d18]:sw a7, 1748(a5)
[0x80000d1c]:fld ft10, 1760(a6)
[0x80000d20]:fld ft9, 1768(a6)
[0x80000d24]:csrrwi zero, frm, 0

[0x80000d28]:fmul.d ft11, ft10, ft9, dyn
[0x80000d2c]:csrrs a7, fflags, zero
[0x80000d30]:fsd ft11, 1760(a5)
[0x80000d34]:sw a7, 1764(a5)
[0x80000d38]:fld ft10, 1776(a6)
[0x80000d3c]:fld ft9, 1784(a6)
[0x80000d40]:csrrwi zero, frm, 4

[0x80000d44]:fmul.d ft11, ft10, ft9, dyn
[0x80000d48]:csrrs a7, fflags, zero
[0x80000d4c]:fsd ft11, 1776(a5)
[0x80000d50]:sw a7, 1780(a5)
[0x80000d54]:fld ft10, 1792(a6)
[0x80000d58]:fld ft9, 1800(a6)
[0x80000d5c]:csrrwi zero, frm, 3

[0x80000d60]:fmul.d ft11, ft10, ft9, dyn
[0x80000d64]:csrrs a7, fflags, zero
[0x80000d68]:fsd ft11, 1792(a5)
[0x80000d6c]:sw a7, 1796(a5)
[0x80000d70]:fld ft10, 1808(a6)
[0x80000d74]:fld ft9, 1816(a6)
[0x80000d78]:csrrwi zero, frm, 2

[0x80000d7c]:fmul.d ft11, ft10, ft9, dyn
[0x80000d80]:csrrs a7, fflags, zero
[0x80000d84]:fsd ft11, 1808(a5)
[0x80000d88]:sw a7, 1812(a5)
[0x80000d8c]:fld ft10, 1824(a6)
[0x80000d90]:fld ft9, 1832(a6)
[0x80000d94]:csrrwi zero, frm, 1

[0x80000d98]:fmul.d ft11, ft10, ft9, dyn
[0x80000d9c]:csrrs a7, fflags, zero
[0x80000da0]:fsd ft11, 1824(a5)
[0x80000da4]:sw a7, 1828(a5)
[0x80000da8]:fld ft10, 1840(a6)
[0x80000dac]:fld ft9, 1848(a6)
[0x80000db0]:csrrwi zero, frm, 0

[0x80000db4]:fmul.d ft11, ft10, ft9, dyn
[0x80000db8]:csrrs a7, fflags, zero
[0x80000dbc]:fsd ft11, 1840(a5)
[0x80000dc0]:sw a7, 1844(a5)
[0x80000dc4]:fld ft10, 1856(a6)
[0x80000dc8]:fld ft9, 1864(a6)
[0x80000dcc]:csrrwi zero, frm, 4

[0x80000dd0]:fmul.d ft11, ft10, ft9, dyn
[0x80000dd4]:csrrs a7, fflags, zero
[0x80000dd8]:fsd ft11, 1856(a5)
[0x80000ddc]:sw a7, 1860(a5)
[0x80000de0]:fld ft10, 1872(a6)
[0x80000de4]:fld ft9, 1880(a6)
[0x80000de8]:csrrwi zero, frm, 3

[0x80000dec]:fmul.d ft11, ft10, ft9, dyn
[0x80000df0]:csrrs a7, fflags, zero
[0x80000df4]:fsd ft11, 1872(a5)
[0x80000df8]:sw a7, 1876(a5)
[0x80000dfc]:fld ft10, 1888(a6)
[0x80000e00]:fld ft9, 1896(a6)
[0x80000e04]:csrrwi zero, frm, 2

[0x80000e08]:fmul.d ft11, ft10, ft9, dyn
[0x80000e0c]:csrrs a7, fflags, zero
[0x80000e10]:fsd ft11, 1888(a5)
[0x80000e14]:sw a7, 1892(a5)
[0x80000e18]:fld ft10, 1904(a6)
[0x80000e1c]:fld ft9, 1912(a6)
[0x80000e20]:csrrwi zero, frm, 1

[0x80000e24]:fmul.d ft11, ft10, ft9, dyn
[0x80000e28]:csrrs a7, fflags, zero
[0x80000e2c]:fsd ft11, 1904(a5)
[0x80000e30]:sw a7, 1908(a5)
[0x80000e34]:fld ft10, 1920(a6)
[0x80000e38]:fld ft9, 1928(a6)
[0x80000e3c]:csrrwi zero, frm, 0

[0x80000e40]:fmul.d ft11, ft10, ft9, dyn
[0x80000e44]:csrrs a7, fflags, zero
[0x80000e48]:fsd ft11, 1920(a5)
[0x80000e4c]:sw a7, 1924(a5)
[0x80000e50]:fld ft10, 1936(a6)
[0x80000e54]:fld ft9, 1944(a6)
[0x80000e58]:csrrwi zero, frm, 4

[0x80000e5c]:fmul.d ft11, ft10, ft9, dyn
[0x80000e60]:csrrs a7, fflags, zero
[0x80000e64]:fsd ft11, 1936(a5)
[0x80000e68]:sw a7, 1940(a5)
[0x80000e6c]:fld ft10, 1952(a6)
[0x80000e70]:fld ft9, 1960(a6)
[0x80000e74]:csrrwi zero, frm, 3

[0x80000e78]:fmul.d ft11, ft10, ft9, dyn
[0x80000e7c]:csrrs a7, fflags, zero
[0x80000e80]:fsd ft11, 1952(a5)
[0x80000e84]:sw a7, 1956(a5)
[0x80000e88]:fld ft10, 1968(a6)
[0x80000e8c]:fld ft9, 1976(a6)
[0x80000e90]:csrrwi zero, frm, 2

[0x80000e94]:fmul.d ft11, ft10, ft9, dyn
[0x80000e98]:csrrs a7, fflags, zero
[0x80000e9c]:fsd ft11, 1968(a5)
[0x80000ea0]:sw a7, 1972(a5)
[0x80000ea4]:fld ft10, 1984(a6)
[0x80000ea8]:fld ft9, 1992(a6)
[0x80000eac]:csrrwi zero, frm, 1

[0x80000eb0]:fmul.d ft11, ft10, ft9, dyn
[0x80000eb4]:csrrs a7, fflags, zero
[0x80000eb8]:fsd ft11, 1984(a5)
[0x80000ebc]:sw a7, 1988(a5)
[0x80000ec0]:fld ft10, 2000(a6)
[0x80000ec4]:fld ft9, 2008(a6)
[0x80000ec8]:csrrwi zero, frm, 0

[0x80000ecc]:fmul.d ft11, ft10, ft9, dyn
[0x80000ed0]:csrrs a7, fflags, zero
[0x80000ed4]:fsd ft11, 2000(a5)
[0x80000ed8]:sw a7, 2004(a5)
[0x80000edc]:fld ft10, 2016(a6)
[0x80000ee0]:fld ft9, 2024(a6)
[0x80000ee4]:csrrwi zero, frm, 4

[0x80000ee8]:fmul.d ft11, ft10, ft9, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 2016(a5)
[0x80000ef4]:sw a7, 2020(a5)
[0x80000ef8]:addi a6, a6, 2032
[0x80000efc]:auipc a5, 3
[0x80000f00]:addi a5, a5, 3852
[0x80000f04]:fld ft10, 0(a6)
[0x80000f08]:fld ft9, 8(a6)
[0x80000f0c]:csrrwi zero, frm, 3

[0x80000ff0]:fmul.d ft11, ft10, ft9, dyn
[0x80000ff4]:csrrs a7, fflags, zero
[0x80000ff8]:fsd ft11, 128(a5)
[0x80000ffc]:sw a7, 132(a5)
[0x80001000]:fld ft10, 144(a6)
[0x80001004]:fld ft9, 152(a6)
[0x80001008]:csrrwi zero, frm, 4

[0x8000100c]:fmul.d ft11, ft10, ft9, dyn
[0x80001010]:csrrs a7, fflags, zero
[0x80001014]:fsd ft11, 144(a5)
[0x80001018]:sw a7, 148(a5)
[0x8000101c]:fld ft10, 160(a6)
[0x80001020]:fld ft9, 168(a6)
[0x80001024]:csrrwi zero, frm, 3

[0x80001028]:fmul.d ft11, ft10, ft9, dyn
[0x8000102c]:csrrs a7, fflags, zero
[0x80001030]:fsd ft11, 160(a5)
[0x80001034]:sw a7, 164(a5)
[0x80001038]:fld ft10, 176(a6)
[0x8000103c]:fld ft9, 184(a6)
[0x80001040]:csrrwi zero, frm, 2

[0x80001044]:fmul.d ft11, ft10, ft9, dyn
[0x80001048]:csrrs a7, fflags, zero
[0x8000104c]:fsd ft11, 176(a5)
[0x80001050]:sw a7, 180(a5)
[0x80001054]:fld ft10, 192(a6)
[0x80001058]:fld ft9, 200(a6)
[0x8000105c]:csrrwi zero, frm, 1

[0x80001060]:fmul.d ft11, ft10, ft9, dyn
[0x80001064]:csrrs a7, fflags, zero
[0x80001068]:fsd ft11, 192(a5)
[0x8000106c]:sw a7, 196(a5)
[0x80001070]:fld ft10, 208(a6)
[0x80001074]:fld ft9, 216(a6)
[0x80001078]:csrrwi zero, frm, 4

[0x8000107c]:fmul.d ft11, ft10, ft9, dyn
[0x80001080]:csrrs a7, fflags, zero
[0x80001084]:fsd ft11, 208(a5)
[0x80001088]:sw a7, 212(a5)
[0x8000108c]:fld ft10, 224(a6)
[0x80001090]:fld ft9, 232(a6)
[0x80001094]:csrrwi zero, frm, 2



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                               |                                                                           code                                                                           |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003a14]<br>0x00000001|- opcode : fmul.d<br> - rs1 : f6<br> - rs2 : f14<br> - rd : f14<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x4bd6158ab1629 and fs2 == 0 and fe2 == 0x41d and fm2 == 0x8afd6f4cb48b9 and rm_val == 0  #nosat<br>  |[0x80000120]:fmul.d fa4, ft6, fa4, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd fa4, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>          |
|   2|[0x80003a24]<br>0x00000001|- rs1 : f26<br> - rs2 : f26<br> - rd : f17<br> - rs1 == rs2 != rd<br>                                                                                                                                                                   |[0x8000013c]:fmul.d fa7, fs10, fs10, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fa7, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>      |
|   3|[0x80003a34]<br>0x00000001|- rs1 : f31<br> - rs2 : f1<br> - rd : f22<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xca95a69d95823 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0xcf5bf1cce7e94 and rm_val == 3  #nosat<br> |[0x80000158]:fmul.d fs6, ft11, ft1, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fs6, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>       |
|   4|[0x80003a44]<br>0x00000001|- rs1 : f23<br> - rs2 : f23<br> - rd : f23<br> - rs1 == rs2 == rd<br>                                                                                                                                                                   |[0x80000174]:fmul.d fs7, fs7, fs7, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd fs7, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>        |
|   5|[0x80003a54]<br>0x00000001|- rs1 : f7<br> - rs2 : f19<br> - rd : f7<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xca95a69d95823 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0xcf5bf1cce7e94 and rm_val == 1  #nosat<br>                         |[0x80000190]:fmul.d ft7, ft7, fs3, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd ft7, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>        |
|   6|[0x80003a64]<br>0x00000001|- rs1 : f13<br> - rs2 : f6<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0xca95a69d95823 and fs2 == 0 and fe2 == 0x3f5 and fm2 == 0xcf5bf1cce7e94 and rm_val == 0  #nosat<br>                                                |[0x800001ac]:fmul.d ft2, fa3, ft6, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft2, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>        |
|   7|[0x80003a74]<br>0x00000001|- rs1 : f17<br> - rs2 : f20<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0x1b215ef8ed139 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x95f66108549e1 and rm_val == 4  #nosat<br>                                               |[0x800001c8]:fmul.d ft5, fa7, fs4, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd ft5, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>       |
|   8|[0x80003a84]<br>0x00000001|- rs1 : f5<br> - rs2 : f15<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0x1b215ef8ed139 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x95f66108549e1 and rm_val == 3  #nosat<br>                                                |[0x800001e4]:fmul.d ft3, ft5, fa5, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd ft3, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>      |
|   9|[0x80003a94]<br>0x00000001|- rs1 : f27<br> - rs2 : f28<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0x1b215ef8ed139 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x95f66108549e1 and rm_val == 2  #nosat<br>                                              |[0x80000200]:fmul.d fa5, fs11, ft8, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fa5, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>     |
|  10|[0x80003aa4]<br>0x00000001|- rs1 : f2<br> - rs2 : f25<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0x1b215ef8ed139 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x95f66108549e1 and rm_val == 1  #nosat<br>                                                |[0x8000021c]:fmul.d fs1, ft2, fs9, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd fs1, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>      |
|  11|[0x80003ab4]<br>0x00000001|- rs1 : f25<br> - rs2 : f16<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0x1b215ef8ed139 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x95f66108549e1 and rm_val == 0  #nosat<br>                                               |[0x80000238]:fmul.d ft6, fs9, fa6, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd ft6, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>      |
|  12|[0x80003ac4]<br>0x00000001|- rs1 : f14<br> - rs2 : f5<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x49ac1d82f3a80 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x17c61ff07b709 and rm_val == 4  #nosat<br>                                               |[0x80000254]:fmul.d fs8, fa4, ft5, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs8, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>      |
|  13|[0x80003ad4]<br>0x00000001|- rs1 : f9<br> - rs2 : f2<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x49ac1d82f3a80 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x17c61ff07b709 and rm_val == 3  #nosat<br>                                                |[0x80000270]:fmul.d fs5, fs1, ft2, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fs5, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>      |
|  14|[0x80003ae4]<br>0x00000001|- rs1 : f11<br> - rs2 : f21<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x49ac1d82f3a80 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x17c61ff07b709 and rm_val == 2  #nosat<br>                                              |[0x8000028c]:fmul.d ft11, fa1, fs5, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd ft11, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>    |
|  15|[0x80003af4]<br>0x00000001|- rs1 : f8<br> - rs2 : f9<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x49ac1d82f3a80 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x17c61ff07b709 and rm_val == 1  #nosat<br>                                                |[0x800002a8]:fmul.d fa2, fs0, fs1, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd fa2, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>      |
|  16|[0x80003b04]<br>0x00000001|- rs1 : f21<br> - rs2 : f7<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x39a and fm1 == 0x49ac1d82f3a80 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x17c61ff07b709 and rm_val == 0  #nosat<br>                                               |[0x800002c4]:fmul.d fs3, fs5, ft7, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs3, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>      |
|  17|[0x80003b14]<br>0x00000001|- rs1 : f15<br> - rs2 : f30<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x397 and fm1 == 0xa1b2d58bc351e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x39cbb091c3aad and rm_val == 4  #nosat<br>                                              |[0x800002e0]:fmul.d fs11, fa5, ft10, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd fs11, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>   |
|  18|[0x80003b24]<br>0x00000001|- rs1 : f29<br> - rs2 : f3<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x397 and fm1 == 0xa1b2d58bc351e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x39cbb091c3aad and rm_val == 3  #nosat<br>                                               |[0x800002fc]:fmul.d fs4, ft9, ft3, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fs4, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br>      |
|  19|[0x80003b34]<br>0x00000001|- rs1 : f22<br> - rs2 : f29<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x397 and fm1 == 0xa1b2d58bc351e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x39cbb091c3aad and rm_val == 2  #nosat<br>                                              |[0x80000318]:fmul.d fa1, fs6, ft9, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd fa1, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>      |
|  20|[0x80003b44]<br>0x00000001|- rs1 : f19<br> - rs2 : f8<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x397 and fm1 == 0xa1b2d58bc351e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x39cbb091c3aad and rm_val == 1  #nosat<br>                                               |[0x80000334]:fmul.d fs2, fs3, fs0, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd fs2, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>      |
|  21|[0x80003b54]<br>0x00000001|- rs1 : f12<br> - rs2 : f24<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x397 and fm1 == 0xa1b2d58bc351e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x39cbb091c3aad and rm_val == 0  #nosat<br>                                               |[0x80000350]:fmul.d ft4, fa2, fs8, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd ft4, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>      |
|  22|[0x80003b64]<br>0x00000001|- rs1 : f16<br> - rs2 : f22<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x39b and fm1 == 0x04a58a2de6d70 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xb8fb33e372b83 and rm_val == 4  #nosat<br>                                              |[0x8000036c]:fmul.d ft8, fa6, fs6, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd ft8, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>      |
|  23|[0x80003b74]<br>0x00000001|- rs1 : f4<br> - rs2 : f18<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x39b and fm1 == 0x04a58a2de6d70 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xb8fb33e372b83 and rm_val == 3  #nosat<br>                                                |[0x80000388]:fmul.d ft0, ft4, fs2, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd ft0, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>      |
|  24|[0x80003b84]<br>0x00000001|- rs1 : f10<br> - rs2 : f4<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x39b and fm1 == 0x04a58a2de6d70 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xb8fb33e372b83 and rm_val == 2  #nosat<br>                                               |[0x800003a4]:fmul.d fs10, fa0, ft4, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fs10, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>    |
|  25|[0x80003b94]<br>0x00000001|- rs1 : f18<br> - rs2 : f0<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x39b and fm1 == 0x04a58a2de6d70 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xb8fb33e372b83 and rm_val == 1  #nosat<br>                                               |[0x800003c0]:fmul.d ft9, fs2, ft0, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd ft9, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>      |
|  26|[0x80003ba4]<br>0x00000001|- rs1 : f3<br> - rs2 : f12<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x39b and fm1 == 0x04a58a2de6d70 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xb8fb33e372b83 and rm_val == 0  #nosat<br>                                               |[0x800003dc]:fmul.d fa0, ft3, fa2, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fa0, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>      |
|  27|[0x80003bb4]<br>0x00000001|- rs1 : f1<br> - rs2 : f27<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x399 and fm1 == 0x35bdbe3e59b31 and fs2 == 0 and fe2 == 0x410 and fm2 == 0xa72a8c1c196d1 and rm_val == 4  #nosat<br>                                               |[0x800003f8]:fmul.d fs9, ft1, fs11, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd fs9, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>     |
|  28|[0x80003bc4]<br>0x00000001|- rs1 : f20<br> - rs2 : f31<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x399 and fm1 == 0x35bdbe3e59b31 and fs2 == 0 and fe2 == 0x410 and fm2 == 0xa72a8c1c196d1 and rm_val == 3  #nosat<br>                                              |[0x80000414]:fmul.d fa3, fs4, ft11, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fa3, 432(a5)<br> [0x80000420]:sw a7, 436(a5)<br>     |
|  29|[0x80003bd4]<br>0x00000001|- rs1 : f0<br> - rs2 : f13<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x399 and fm1 == 0x35bdbe3e59b31 and fs2 == 0 and fe2 == 0x410 and fm2 == 0xa72a8c1c196d1 and rm_val == 2  #nosat<br>                                               |[0x80000430]:fmul.d fa6, ft0, fa3, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsd fa6, 448(a5)<br> [0x8000043c]:sw a7, 452(a5)<br>      |
|  30|[0x80003be4]<br>0x00000001|- rs1 : f24<br> - rs2 : f10<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x399 and fm1 == 0x35bdbe3e59b31 and fs2 == 0 and fe2 == 0x410 and fm2 == 0xa72a8c1c196d1 and rm_val == 1  #nosat<br>                                               |[0x8000044c]:fmul.d ft1, fs8, fa0, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd ft1, 464(a5)<br> [0x80000458]:sw a7, 468(a5)<br>      |
|  31|[0x80003bf4]<br>0x00000001|- rs1 : f30<br> - rs2 : f11<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x399 and fm1 == 0x35bdbe3e59b31 and fs2 == 0 and fe2 == 0x410 and fm2 == 0xa72a8c1c196d1 and rm_val == 0  #nosat<br>                                               |[0x80000468]:fmul.d fs0, ft10, fa1, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsd fs0, 480(a5)<br> [0x80000474]:sw a7, 484(a5)<br>     |
|  32|[0x80003c04]<br>0x00000001|- rs1 : f28<br> - rs2 : f17<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x398 and fm1 == 0xf009dd118d92e and fs2 == 0 and fe2 == 0x41f and fm2 == 0x083ccf519948f and rm_val == 4  #nosat<br>                                              |[0x80000484]:fmul.d ft10, ft8, fa7, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd ft10, 496(a5)<br> [0x80000490]:sw a7, 500(a5)<br>    |
|  33|[0x80003c14]<br>0x00000001|- fs1 == 0 and fe1 == 0x398 and fm1 == 0xf009dd118d92e and fs2 == 0 and fe2 == 0x41f and fm2 == 0x083ccf519948f and rm_val == 3  #nosat<br>                                                                                             |[0x800004a0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsd ft11, 512(a5)<br> [0x800004ac]:sw a7, 516(a5)<br>   |
|  34|[0x80003c24]<br>0x00000001|- fs1 == 0 and fe1 == 0x398 and fm1 == 0xf009dd118d92e and fs2 == 0 and fe2 == 0x41f and fm2 == 0x083ccf519948f and rm_val == 2  #nosat<br>                                                                                             |[0x800004bc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft11, 528(a5)<br> [0x800004c8]:sw a7, 532(a5)<br>   |
|  35|[0x80003c34]<br>0x00000001|- fs1 == 0 and fe1 == 0x398 and fm1 == 0xf009dd118d92e and fs2 == 0 and fe2 == 0x41f and fm2 == 0x083ccf519948f and rm_val == 1  #nosat<br>                                                                                             |[0x800004d8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsd ft11, 544(a5)<br> [0x800004e4]:sw a7, 548(a5)<br>   |
|  36|[0x80003c44]<br>0x00000001|- fs1 == 0 and fe1 == 0x398 and fm1 == 0xf009dd118d92e and fs2 == 0 and fe2 == 0x41f and fm2 == 0x083ccf519948f and rm_val == 0  #nosat<br>                                                                                             |[0x800004f4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 560(a5)<br> [0x80000500]:sw a7, 564(a5)<br>   |
|  37|[0x80003c54]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0xa623af927a048 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0xf75ceede0345d and rm_val == 4  #nosat<br>                                                                                             |[0x80000510]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd ft11, 576(a5)<br> [0x8000051c]:sw a7, 580(a5)<br>   |
|  38|[0x80003c64]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0xa623af927a048 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0xf75ceede0345d and rm_val == 3  #nosat<br>                                                                                             |[0x8000052c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsd ft11, 592(a5)<br> [0x80000538]:sw a7, 596(a5)<br>   |
|  39|[0x80003c74]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0xa623af927a048 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0xf75ceede0345d and rm_val == 2  #nosat<br>                                                                                             |[0x80000548]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsd ft11, 608(a5)<br> [0x80000554]:sw a7, 612(a5)<br>   |
|  40|[0x80003c84]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0xa623af927a048 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0xf75ceede0345d and rm_val == 1  #nosat<br>                                                                                             |[0x80000564]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 624(a5)<br> [0x80000570]:sw a7, 628(a5)<br>   |
|  41|[0x80003c94]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0xa623af927a048 and fs2 == 0 and fe2 == 0x3f6 and fm2 == 0xf75ceede0345d and rm_val == 0  #nosat<br>                                                                                             |[0x80000580]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsd ft11, 640(a5)<br> [0x8000058c]:sw a7, 644(a5)<br>   |
|  42|[0x80003ca4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x198d9e5fdea7a and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x983c8952f329d and rm_val == 4  #nosat<br>                                                                                             |[0x8000059c]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 656(a5)<br> [0x800005a8]:sw a7, 660(a5)<br>   |
|  43|[0x80003cb4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x198d9e5fdea7a and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x983c8952f329d and rm_val == 3  #nosat<br>                                                                                             |[0x800005b8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsd ft11, 672(a5)<br> [0x800005c4]:sw a7, 676(a5)<br>   |
|  44|[0x80003cc4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x198d9e5fdea7a and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x983c8952f329d and rm_val == 2  #nosat<br>                                                                                             |[0x800005d4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsd ft11, 688(a5)<br> [0x800005e0]:sw a7, 692(a5)<br>   |
|  45|[0x80003cd4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x198d9e5fdea7a and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x983c8952f329d and rm_val == 1  #nosat<br>                                                                                             |[0x800005f0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsd ft11, 704(a5)<br> [0x800005fc]:sw a7, 708(a5)<br>   |
|  46|[0x80003ce4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x198d9e5fdea7a and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x983c8952f329d and rm_val == 0  #nosat<br>                                                                                             |[0x8000060c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft11, 720(a5)<br> [0x80000618]:sw a7, 724(a5)<br>   |
|  47|[0x80003cf4]<br>0x00000001|- fs1 == 0 and fe1 == 0x391 and fm1 == 0x7bc450b8c1914 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xe5bd183e19167 and rm_val == 4  #nosat<br>                                                                                             |[0x80000628]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsd ft11, 736(a5)<br> [0x80000634]:sw a7, 740(a5)<br>   |
|  48|[0x80003d04]<br>0x00000001|- fs1 == 0 and fe1 == 0x391 and fm1 == 0x7bc450b8c1914 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xe5bd183e19167 and rm_val == 3  #nosat<br>                                                                                             |[0x80000644]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 752(a5)<br> [0x80000650]:sw a7, 756(a5)<br>   |
|  49|[0x80003d14]<br>0x00000001|- fs1 == 0 and fe1 == 0x391 and fm1 == 0x7bc450b8c1914 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xe5bd183e19167 and rm_val == 2  #nosat<br>                                                                                             |[0x80000660]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd ft11, 768(a5)<br> [0x8000066c]:sw a7, 772(a5)<br>   |
|  50|[0x80003d24]<br>0x00000001|- fs1 == 0 and fe1 == 0x391 and fm1 == 0x7bc450b8c1914 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xe5bd183e19167 and rm_val == 1  #nosat<br>                                                                                             |[0x8000067c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsd ft11, 784(a5)<br> [0x80000688]:sw a7, 788(a5)<br>   |
|  51|[0x80003d34]<br>0x00000001|- fs1 == 0 and fe1 == 0x391 and fm1 == 0x7bc450b8c1914 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xe5bd183e19167 and rm_val == 0  #nosat<br>                                                                                             |[0x80000698]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsd ft11, 800(a5)<br> [0x800006a4]:sw a7, 804(a5)<br>   |
|  52|[0x80003d44]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x0bad7092ab68f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe9a9f35e937c9 and rm_val == 4  #nosat<br>                                                                                             |[0x800006b4]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 816(a5)<br> [0x800006c0]:sw a7, 820(a5)<br>   |
|  53|[0x80003d54]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x0bad7092ab68f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe9a9f35e937c9 and rm_val == 3  #nosat<br>                                                                                             |[0x800006d0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006d4]:csrrs a7, fflags, zero<br> [0x800006d8]:fsd ft11, 832(a5)<br> [0x800006dc]:sw a7, 836(a5)<br>   |
|  54|[0x80003d64]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x0bad7092ab68f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe9a9f35e937c9 and rm_val == 2  #nosat<br>                                                                                             |[0x800006ec]:fmul.d ft11, ft10, ft9, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 848(a5)<br> [0x800006f8]:sw a7, 852(a5)<br>   |
|  55|[0x80003d74]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x0bad7092ab68f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe9a9f35e937c9 and rm_val == 1  #nosat<br>                                                                                             |[0x80000708]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:fsd ft11, 864(a5)<br> [0x80000714]:sw a7, 868(a5)<br>   |
|  56|[0x80003d84]<br>0x00000001|- fs1 == 0 and fe1 == 0x39b and fm1 == 0x0bad7092ab68f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe9a9f35e937c9 and rm_val == 0  #nosat<br>                                                                                             |[0x80000724]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 880(a5)<br> [0x80000730]:sw a7, 884(a5)<br>   |
|  57|[0x80003d94]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xd31d73c58e5f0 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xec20dce896dad and rm_val == 4  #nosat<br>                                                                                             |[0x80000740]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd ft11, 896(a5)<br> [0x8000074c]:sw a7, 900(a5)<br>   |
|  58|[0x80003da4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xd31d73c58e5f0 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xec20dce896dad and rm_val == 3  #nosat<br>                                                                                             |[0x8000075c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 912(a5)<br> [0x80000768]:sw a7, 916(a5)<br>   |
|  59|[0x80003db4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xd31d73c58e5f0 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xec20dce896dad and rm_val == 2  #nosat<br>                                                                                             |[0x80000778]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000077c]:csrrs a7, fflags, zero<br> [0x80000780]:fsd ft11, 928(a5)<br> [0x80000784]:sw a7, 932(a5)<br>   |
|  60|[0x80003dc4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xd31d73c58e5f0 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xec20dce896dad and rm_val == 1  #nosat<br>                                                                                             |[0x80000794]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:fsd ft11, 944(a5)<br> [0x800007a0]:sw a7, 948(a5)<br>   |
|  61|[0x80003dd4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xd31d73c58e5f0 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xec20dce896dad and rm_val == 0  #nosat<br>                                                                                             |[0x800007b0]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:fsd ft11, 960(a5)<br> [0x800007bc]:sw a7, 964(a5)<br>   |
|  62|[0x80003de4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and rm_val == 4  #nosat<br>                                                                                             |[0x800007cc]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007d0]:csrrs a7, fflags, zero<br> [0x800007d4]:fsd ft11, 976(a5)<br> [0x800007d8]:sw a7, 980(a5)<br>   |
|  63|[0x80003df4]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and rm_val == 3  #nosat<br>                                                                                             |[0x800007e8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:fsd ft11, 992(a5)<br> [0x800007f4]:sw a7, 996(a5)<br>   |
|  64|[0x80003e04]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and rm_val == 2  #nosat<br>                                                                                             |[0x80000804]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 1008(a5)<br> [0x80000810]:sw a7, 1012(a5)<br> |
|  65|[0x80003e14]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and rm_val == 1  #nosat<br>                                                                                             |[0x80000820]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000824]:csrrs a7, fflags, zero<br> [0x80000828]:fsd ft11, 1024(a5)<br> [0x8000082c]:sw a7, 1028(a5)<br> |
|  66|[0x80003e24]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x64204f3ac913b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x700c9e9287c4d and rm_val == 0  #nosat<br>                                                                                             |[0x8000083c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd ft11, 1040(a5)<br> [0x80000848]:sw a7, 1044(a5)<br> |
|  67|[0x80003e34]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and rm_val == 4  #nosat<br>                                                                                             |[0x80000858]:fmul.d ft11, ft10, ft9, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:fsd ft11, 1056(a5)<br> [0x80000864]:sw a7, 1060(a5)<br> |
|  68|[0x80003e44]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and rm_val == 3  #nosat<br>                                                                                             |[0x80000874]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000878]:csrrs a7, fflags, zero<br> [0x8000087c]:fsd ft11, 1072(a5)<br> [0x80000880]:sw a7, 1076(a5)<br> |
|  69|[0x80003e54]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and rm_val == 2  #nosat<br>                                                                                             |[0x80000890]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:fsd ft11, 1088(a5)<br> [0x8000089c]:sw a7, 1092(a5)<br> |
|  70|[0x80003e64]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and rm_val == 1  #nosat<br>                                                                                             |[0x800008ac]:fmul.d ft11, ft10, ft9, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 1104(a5)<br> [0x800008b8]:sw a7, 1108(a5)<br> |
|  71|[0x80003e74]<br>0x00000001|- fs1 == 0 and fe1 == 0x399 and fm1 == 0x52236afc78e8d and fs2 == 0 and fe2 == 0x41e and fm2 == 0x83a0d96200227 and rm_val == 0  #nosat<br>                                                                                             |[0x800008c8]:fmul.d ft11, ft10, ft9, dyn<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:fsd ft11, 1120(a5)<br> [0x800008d4]:sw a7, 1124(a5)<br> |
|  72|[0x80003e0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f10]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f14]:csrrs a7, fflags, zero<br> [0x80000f18]:fsd ft11, 0(a5)<br> [0x80000f1c]:sw a7, 4(a5)<br>       |
|  73|[0x80003e1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and rm_val == 2  #nosat<br>                                                                                             |[0x80000f2c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f30]:csrrs a7, fflags, zero<br> [0x80000f34]:fsd ft11, 16(a5)<br> [0x80000f38]:sw a7, 20(a5)<br>     |
|  74|[0x80003e2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and rm_val == 1  #nosat<br>                                                                                             |[0x80000f48]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 32(a5)<br> [0x80000f54]:sw a7, 36(a5)<br>     |
|  75|[0x80003e3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0x355440740a6f0 and fs2 == 0 and fe2 == 0x407 and fm2 == 0x73944ae980930 and rm_val == 0  #nosat<br>                                                                                             |[0x80000f64]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs a7, fflags, zero<br> [0x80000f6c]:fsd ft11, 48(a5)<br> [0x80000f70]:sw a7, 52(a5)<br>     |
|  76|[0x80003e4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and rm_val == 4  #nosat<br>                                                                                             |[0x80000f80]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:fsd ft11, 64(a5)<br> [0x80000f8c]:sw a7, 68(a5)<br>     |
|  77|[0x80003e5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f9c]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fa0]:csrrs a7, fflags, zero<br> [0x80000fa4]:fsd ft11, 80(a5)<br> [0x80000fa8]:sw a7, 84(a5)<br>     |
|  78|[0x80003e6c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and rm_val == 2  #nosat<br>                                                                                             |[0x80000fb8]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fbc]:csrrs a7, fflags, zero<br> [0x80000fc0]:fsd ft11, 96(a5)<br> [0x80000fc4]:sw a7, 100(a5)<br>    |
|  79|[0x80003e7c]<br>0x00000001|- fs1 == 0 and fe1 == 0x39a and fm1 == 0xfcb6e0e21d091 and fs2 == 0 and fe2 == 0x40f and fm2 == 0x01a746ecaaba6 and rm_val == 1  #nosat<br>                                                                                             |[0x80000fd4]:fmul.d ft11, ft10, ft9, dyn<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:fsd ft11, 112(a5)<br> [0x80000fe0]:sw a7, 116(a5)<br>   |
