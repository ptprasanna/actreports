
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000550')]      |
| SIG_REGION                | [('0x80002310', '0x80002480', '92 words')]      |
| COV_LABELS                | fcvt.s.d_b23      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.s.d/riscof_work/fcvt.s.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 117     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 23      |
| STAT1                     | 23      |
| STAT2                     | 0      |
| STAT3                     | 21     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000344]:fcvt.s.d fs4, ft2, dyn
[0x80000348]:csrrs a7, fflags, zero
[0x8000034c]:fsd fs4, 368(a5)
[0x80000350]:sw a7, 372(a5)
[0x80000354]:fld ft1, 192(a6)
[0x80000358]:csrrwi zero, frm, 1

[0x8000035c]:fcvt.s.d fs7, ft1, dyn
[0x80000360]:csrrs a7, fflags, zero
[0x80000364]:fsd fs7, 384(a5)
[0x80000368]:sw a7, 388(a5)
[0x8000036c]:fld ft9, 200(a6)
[0x80000370]:csrrwi zero, frm, 0

[0x80000374]:fcvt.s.d fa4, ft9, dyn
[0x80000378]:csrrs a7, fflags, zero
[0x8000037c]:fsd fa4, 400(a5)
[0x80000380]:sw a7, 404(a5)
[0x80000384]:fld ft0, 208(a6)
[0x80000388]:csrrwi zero, frm, 4

[0x8000038c]:fcvt.s.d ft2, ft0, dyn
[0x80000390]:csrrs a7, fflags, zero
[0x80000394]:fsd ft2, 416(a5)
[0x80000398]:sw a7, 420(a5)
[0x8000039c]:fld ft8, 216(a6)
[0x800003a0]:csrrwi zero, frm, 3

[0x800003a4]:fcvt.s.d ft10, ft8, dyn
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:fsd ft10, 432(a5)
[0x800003b0]:sw a7, 436(a5)
[0x800003b4]:fld fa3, 224(a6)
[0x800003b8]:csrrwi zero, frm, 2

[0x800003bc]:fcvt.s.d fs5, fa3, dyn
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:fsd fs5, 448(a5)
[0x800003c8]:sw a7, 452(a5)
[0x800003cc]:fld fs6, 232(a6)
[0x800003d0]:csrrwi zero, frm, 1

[0x800003d4]:fcvt.s.d ft8, fs6, dyn
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:fsd ft8, 464(a5)
[0x800003e0]:sw a7, 468(a5)
[0x800003e4]:fld ft7, 240(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fcvt.s.d fs11, ft7, dyn
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:fsd fs11, 480(a5)
[0x800003f8]:sw a7, 484(a5)
[0x800003fc]:fld ft5, 248(a6)
[0x80000400]:csrrwi zero, frm, 4

[0x80000404]:fcvt.s.d fa5, ft5, dyn
[0x80000408]:csrrs a7, fflags, zero
[0x8000040c]:fsd fa5, 496(a5)
[0x80000410]:sw a7, 500(a5)
[0x80000414]:fld ft10, 256(a6)
[0x80000418]:csrrwi zero, frm, 3

[0x8000041c]:fcvt.s.d ft11, ft10, dyn
[0x80000420]:csrrs a7, fflags, zero
[0x80000424]:fsd ft11, 512(a5)
[0x80000428]:sw a7, 516(a5)
[0x8000042c]:fld ft10, 264(a6)
[0x80000430]:csrrwi zero, frm, 2

[0x80000434]:fcvt.s.d ft11, ft10, dyn
[0x80000438]:csrrs a7, fflags, zero
[0x8000043c]:fsd ft11, 528(a5)
[0x80000440]:sw a7, 532(a5)
[0x80000444]:fld ft10, 272(a6)
[0x80000448]:csrrwi zero, frm, 1

[0x8000044c]:fcvt.s.d ft11, ft10, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:fsd ft11, 544(a5)
[0x80000458]:sw a7, 548(a5)
[0x8000045c]:fld ft10, 280(a6)
[0x80000460]:csrrwi zero, frm, 0

[0x80000464]:fcvt.s.d ft11, ft10, dyn
[0x80000468]:csrrs a7, fflags, zero
[0x8000046c]:fsd ft11, 560(a5)
[0x80000470]:sw a7, 564(a5)
[0x80000474]:fld ft10, 288(a6)
[0x80000478]:csrrwi zero, frm, 4

[0x8000047c]:fcvt.s.d ft11, ft10, dyn
[0x80000480]:csrrs a7, fflags, zero
[0x80000484]:fsd ft11, 576(a5)
[0x80000488]:sw a7, 580(a5)
[0x8000048c]:fld ft10, 296(a6)
[0x80000490]:csrrwi zero, frm, 3

[0x80000494]:fcvt.s.d ft11, ft10, dyn
[0x80000498]:csrrs a7, fflags, zero
[0x8000049c]:fsd ft11, 592(a5)
[0x800004a0]:sw a7, 596(a5)
[0x800004a4]:fld ft10, 304(a6)
[0x800004a8]:csrrwi zero, frm, 2

[0x800004ac]:fcvt.s.d ft11, ft10, dyn
[0x800004b0]:csrrs a7, fflags, zero
[0x800004b4]:fsd ft11, 608(a5)
[0x800004b8]:sw a7, 612(a5)
[0x800004bc]:fld ft10, 312(a6)
[0x800004c0]:csrrwi zero, frm, 1

[0x800004c4]:fcvt.s.d ft11, ft10, dyn
[0x800004c8]:csrrs a7, fflags, zero
[0x800004cc]:fsd ft11, 624(a5)
[0x800004d0]:sw a7, 628(a5)
[0x800004d4]:fld ft10, 320(a6)
[0x800004d8]:csrrwi zero, frm, 0

[0x800004dc]:fcvt.s.d ft11, ft10, dyn
[0x800004e0]:csrrs a7, fflags, zero
[0x800004e4]:fsd ft11, 640(a5)
[0x800004e8]:sw a7, 644(a5)
[0x800004ec]:fld ft10, 328(a6)
[0x800004f0]:csrrwi zero, frm, 4

[0x800004f4]:fcvt.s.d ft11, ft10, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:fsd ft11, 656(a5)
[0x80000500]:sw a7, 660(a5)
[0x80000504]:fld ft10, 336(a6)
[0x80000508]:csrrwi zero, frm, 3

[0x8000050c]:fcvt.s.d ft11, ft10, dyn
[0x80000510]:csrrs a7, fflags, zero
[0x80000514]:fsd ft11, 672(a5)
[0x80000518]:sw a7, 676(a5)
[0x8000051c]:fld ft10, 344(a6)
[0x80000520]:csrrwi zero, frm, 2

[0x80000524]:fcvt.s.d ft11, ft10, dyn
[0x80000528]:csrrs a7, fflags, zero
[0x8000052c]:fsd ft11, 688(a5)
[0x80000530]:sw a7, 692(a5)
[0x80000534]:fld ft10, 352(a6)
[0x80000538]:csrrwi zero, frm, 1



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                       coverpoints                                                                       |                                                                        code                                                                        |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000001|- opcode : fcvt.s.d<br> - rs1 : f19<br> - rd : f1<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.s.d ft1, fs3, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd ft1, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x80002324]<br>0x00000001|- rs1 : f10<br> - rd : f10<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat<br>                        |[0x80000134]:fcvt.s.d fa0, fa0, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd fa0, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>     |
|   3|[0x80002334]<br>0x00000001|- rs1 : f25<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat<br>                                        |[0x8000014c]:fcvt.s.d fs2, fs9, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd fs2, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>     |
|   4|[0x80002344]<br>0x00000001|- rs1 : f18<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat<br>                                        |[0x80000164]:fcvt.s.d fs6, fs2, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fs6, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>     |
|   5|[0x80002354]<br>0x00000001|- rs1 : f15<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat<br>                                        |[0x8000017c]:fcvt.s.d fs8, fa5, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fs8, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>     |
|   6|[0x80002364]<br>0x00000001|- rs1 : f4<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat<br>                                         |[0x80000194]:fcvt.s.d ft11, ft4, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd ft11, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>   |
|   7|[0x80002374]<br>0x00000001|- rs1 : f8<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat<br>                                          |[0x800001ac]:fcvt.s.d ft3, fs0, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft3, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>    |
|   8|[0x80002384]<br>0x00000001|- rs1 : f30<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat<br>                                         |[0x800001c4]:fcvt.s.d fs0, ft10, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs0, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>  |
|   9|[0x80002394]<br>0x00000001|- rs1 : f11<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat<br>                                        |[0x800001dc]:fcvt.s.d fa2, fa1, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd fa2, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>   |
|  10|[0x800023a4]<br>0x00000001|- rs1 : f6<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat<br>                                         |[0x800001f4]:fcvt.s.d fa1, ft6, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd fa1, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br>   |
|  11|[0x800023b4]<br>0x00000001|- rs1 : f20<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat<br>                                         |[0x8000020c]:fcvt.s.d fs1, fs4, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd fs1, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>   |
|  12|[0x800023c4]<br>0x00000001|- rs1 : f12<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat<br>                                        |[0x80000224]:fcvt.s.d fs3, fa2, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs3, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>   |
|  13|[0x800023d4]<br>0x00000001|- rs1 : f26<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat<br>                                        |[0x8000023c]:fcvt.s.d fa3, fs10, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd fa3, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>  |
|  14|[0x800023e4]<br>0x00000001|- rs1 : f14<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat<br>                                         |[0x80000254]:fcvt.s.d ft7, fa4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd ft7, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>   |
|  15|[0x800023f4]<br>0x00000001|- rs1 : f31<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                                         |[0x8000026c]:fcvt.s.d ft0, ft11, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd ft0, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>  |
|  16|[0x80002404]<br>0x00000001|- rs1 : f9<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                         |[0x80000284]:fcvt.s.d fa6, fs1, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fa6, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>   |
|  17|[0x80002414]<br>0x00000001|- rs1 : f23<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat<br>                                         |[0x8000029c]:fcvt.s.d ft6, fs7, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsd ft6, 256(a5)<br> [0x800002a8]:sw a7, 260(a5)<br>   |
|  18|[0x80002424]<br>0x00000001|- rs1 : f16<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat<br>                                         |[0x800002b4]:fcvt.s.d ft5, fa6, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsd ft5, 272(a5)<br> [0x800002c0]:sw a7, 276(a5)<br>   |
|  19|[0x80002434]<br>0x00000001|- rs1 : f3<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat<br>                                         |[0x800002cc]:fcvt.s.d fs9, ft3, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsd fs9, 288(a5)<br> [0x800002d8]:sw a7, 292(a5)<br>   |
|  20|[0x80002444]<br>0x00000001|- rs1 : f27<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                                        |[0x800002e4]:fcvt.s.d fa7, fs11, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fa7, 304(a5)<br> [0x800002f0]:sw a7, 308(a5)<br>  |
|  21|[0x80002454]<br>0x00000001|- rs1 : f24<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                        |[0x800002fc]:fcvt.s.d ft9, fs8, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd ft9, 320(a5)<br> [0x80000308]:sw a7, 324(a5)<br>   |
|  22|[0x80002464]<br>0x00000001|- rs1 : f17<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                                        |[0x80000314]:fcvt.s.d fs10, fa7, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsd fs10, 336(a5)<br> [0x80000320]:sw a7, 340(a5)<br> |
|  23|[0x80002474]<br>0x00000001|- rs1 : f21<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                                         |[0x8000032c]:fcvt.s.d ft4, fs5, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsd ft4, 352(a5)<br> [0x80000338]:sw a7, 356(a5)<br>   |
