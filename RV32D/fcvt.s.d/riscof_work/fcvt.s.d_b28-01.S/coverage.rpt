---
fcvt.s.d_b28:
  config:
  - check ISA:=regex(.*I.*F.*)
  op_comb:
    rs1 != rd: 31
    rs1 == rd: 1
    coverage: 2/2
  opcode:
    fcvt.s.d: 32
    coverage: 1/1
  rd:
    f0: 1
    f1: 1
    f10: 1
    f11: 1
    f12: 1
    f13: 1
    f14: 1
    f15: 1
    f16: 1
    f17: 1
    f18: 1
    f19: 1
    f2: 1
    f20: 1
    f21: 1
    f22: 1
    f23: 1
    f24: 1
    f25: 1
    f26: 1
    f27: 1
    f28: 1
    f29: 1
    f3: 1
    f30: 1
    f31: 1
    f4: 1
    f5: 1
    f6: 1
    f7: 1
    f8: 1
    f9: 1
    coverage: 32/32
  rs1:
    f0: 1
    f1: 1
    f10: 1
    f11: 1
    f12: 1
    f13: 1
    f14: 1
    f15: 1
    f16: 1
    f17: 1
    f18: 1
    f19: 1
    f2: 1
    f20: 1
    f21: 1
    f22: 1
    f23: 1
    f24: 1
    f25: 1
    f26: 1
    f27: 1
    f28: 1
    f29: 1
    f3: 1
    f30: 1
    f31: 1
    f4: 1
    f5: 1
    f6: 1
    f7: 1
    f8: 1
    f9: 1
    coverage: 32/32
  val_comb:
    'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 5  #  rs1_val==dzero(0x0000000000000000) | +0
    'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dinfinity(0xFFF0000000000000) | -Infinity
    'fs1 == 1 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc3e0000000000000) | -MaxInt
    'fs1 == 1 and fe1 == 0x43d and fm1 == 0x967a4ae26514c and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc3d967a4ae26514c) | A random number in the range (-1.11..11*2^precision, -1)
    'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xbff4000000000000) | Number = -1.25 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xbff8000000000000) | Number = -1.5 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xbffc000000000000) | Number = -1.75 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc000000000000000) | Number = -2.0 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc002000000000000) | Number = -2.25 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc004000000000000) | Number = -2.5 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xc006000000000000) | Number = -2.75 => Number ∈ [-2.75,-1)
    'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==done(0xBF80000000000000) | -1
    'fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0xbfdb008d57e19f88) | A random number in the range (-1, -0)
    'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dzero(0x8000000000000000) | -0
    'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat': 1 #  rs1_val==dqnan(0x7FF8000000000001) | Quiet NaN
    'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat': 1 #  rs1_val==dsnan(0x7FF0000000000001) | Signaling NaN
    'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dinfinity(0x7FF0000000000000) | +Infinity
    'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x43e0000000000000) | MaxInt
    'fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x43cb72eb13dc494a) | A random number in the range (+1, +1.11..11*2^precision)
    'fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x4006000000000000) | Number = 2.75 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x4004000000000000) | Number = 2.5 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x4002000000000000) | Number = 2.25 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x4000000000000000) | Number = 2.0 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x3ffc000000000000) | Number = 1.75 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x3ff8000000000000) | Number = 1.5 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x3ff4000000000000) | Number = 1.25 => Number ∈ (1,2.75]
    'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat': 1 #  rs1_val==done(0x3FF0000000000000) | +1
    'fs1 == 0 and fe1 == 0x3fe and fm1 == 0x248ee18215dfa and rm_val == 0  #nosat': 1 #  rs1_val==dnorm(0x3fe248ee18215dfa) | A random number in the range (+0, +1)
    coverage: 28/28
  total_coverage: 95/95
