
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800008a0')]      |
| SIG_REGION                | [('0x80002410', '0x80002690', '160 words')]      |
| COV_LABELS                | fcvt.s.d_b29      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.s.d/riscof_work/fcvt.s.d_b29-01.S/ref.S    |
| Total Number of coverpoints| 152     |
| Total Coverpoints Hit     | 107      |
| Total Signature Updates   | 40      |
| STAT1                     | 40      |
| STAT2                     | 0      |
| STAT3                     | 39     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800004dc]:fcvt.s.d ft11, ft10, dyn
[0x800004e0]:csrrs a7, fflags, zero
[0x800004e4]:fsd ft11, 640(a5)
[0x800004e8]:sw a7, 644(a5)
[0x800004ec]:fld ft10, 328(a6)
[0x800004f0]:csrrwi zero, frm, 4

[0x800004f4]:fcvt.s.d ft11, ft10, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:fsd ft11, 656(a5)
[0x80000500]:sw a7, 660(a5)
[0x80000504]:fld ft10, 336(a6)
[0x80000508]:csrrwi zero, frm, 3

[0x8000050c]:fcvt.s.d ft11, ft10, dyn
[0x80000510]:csrrs a7, fflags, zero
[0x80000514]:fsd ft11, 672(a5)
[0x80000518]:sw a7, 676(a5)
[0x8000051c]:fld ft10, 344(a6)
[0x80000520]:csrrwi zero, frm, 2

[0x80000524]:fcvt.s.d ft11, ft10, dyn
[0x80000528]:csrrs a7, fflags, zero
[0x8000052c]:fsd ft11, 688(a5)
[0x80000530]:sw a7, 692(a5)
[0x80000534]:fld ft10, 352(a6)
[0x80000538]:csrrwi zero, frm, 1

[0x8000053c]:fcvt.s.d ft11, ft10, dyn
[0x80000540]:csrrs a7, fflags, zero
[0x80000544]:fsd ft11, 704(a5)
[0x80000548]:sw a7, 708(a5)
[0x8000054c]:fld ft10, 360(a6)
[0x80000550]:csrrwi zero, frm, 0

[0x80000554]:fcvt.s.d ft11, ft10, dyn
[0x80000558]:csrrs a7, fflags, zero
[0x8000055c]:fsd ft11, 720(a5)
[0x80000560]:sw a7, 724(a5)
[0x80000564]:fld ft10, 368(a6)
[0x80000568]:csrrwi zero, frm, 4

[0x8000056c]:fcvt.s.d ft11, ft10, dyn
[0x80000570]:csrrs a7, fflags, zero
[0x80000574]:fsd ft11, 736(a5)
[0x80000578]:sw a7, 740(a5)
[0x8000057c]:fld ft10, 376(a6)
[0x80000580]:csrrwi zero, frm, 3

[0x80000584]:fcvt.s.d ft11, ft10, dyn
[0x80000588]:csrrs a7, fflags, zero
[0x8000058c]:fsd ft11, 752(a5)
[0x80000590]:sw a7, 756(a5)
[0x80000594]:fld ft10, 384(a6)
[0x80000598]:csrrwi zero, frm, 2

[0x8000059c]:fcvt.s.d ft11, ft10, dyn
[0x800005a0]:csrrs a7, fflags, zero
[0x800005a4]:fsd ft11, 768(a5)
[0x800005a8]:sw a7, 772(a5)
[0x800005ac]:fld ft10, 392(a6)
[0x800005b0]:csrrwi zero, frm, 1

[0x800005b4]:fcvt.s.d ft11, ft10, dyn
[0x800005b8]:csrrs a7, fflags, zero
[0x800005bc]:fsd ft11, 784(a5)
[0x800005c0]:sw a7, 788(a5)
[0x800005c4]:fld ft10, 400(a6)
[0x800005c8]:csrrwi zero, frm, 0

[0x800005cc]:fcvt.s.d ft11, ft10, dyn
[0x800005d0]:csrrs a7, fflags, zero
[0x800005d4]:fsd ft11, 800(a5)
[0x800005d8]:sw a7, 804(a5)
[0x800005dc]:fld ft10, 408(a6)
[0x800005e0]:csrrwi zero, frm, 4

[0x800005e4]:fcvt.s.d ft11, ft10, dyn
[0x800005e8]:csrrs a7, fflags, zero
[0x800005ec]:fsd ft11, 816(a5)
[0x800005f0]:sw a7, 820(a5)
[0x800005f4]:fld ft10, 416(a6)
[0x800005f8]:csrrwi zero, frm, 3

[0x800005fc]:fcvt.s.d ft11, ft10, dyn
[0x80000600]:csrrs a7, fflags, zero
[0x80000604]:fsd ft11, 832(a5)
[0x80000608]:sw a7, 836(a5)
[0x8000060c]:fld ft10, 424(a6)
[0x80000610]:csrrwi zero, frm, 2

[0x80000614]:fcvt.s.d ft11, ft10, dyn
[0x80000618]:csrrs a7, fflags, zero
[0x8000061c]:fsd ft11, 848(a5)
[0x80000620]:sw a7, 852(a5)
[0x80000624]:fld ft10, 432(a6)
[0x80000628]:csrrwi zero, frm, 1

[0x8000062c]:fcvt.s.d ft11, ft10, dyn
[0x80000630]:csrrs a7, fflags, zero
[0x80000634]:fsd ft11, 864(a5)
[0x80000638]:sw a7, 868(a5)
[0x8000063c]:fld ft10, 440(a6)
[0x80000640]:csrrwi zero, frm, 0

[0x80000644]:fcvt.s.d ft11, ft10, dyn
[0x80000648]:csrrs a7, fflags, zero
[0x8000064c]:fsd ft11, 880(a5)
[0x80000650]:sw a7, 884(a5)
[0x80000654]:fld ft10, 448(a6)
[0x80000658]:csrrwi zero, frm, 4

[0x8000065c]:fcvt.s.d ft11, ft10, dyn
[0x80000660]:csrrs a7, fflags, zero
[0x80000664]:fsd ft11, 896(a5)
[0x80000668]:sw a7, 900(a5)
[0x8000066c]:fld ft10, 456(a6)
[0x80000670]:csrrwi zero, frm, 3

[0x80000674]:fcvt.s.d ft11, ft10, dyn
[0x80000678]:csrrs a7, fflags, zero
[0x8000067c]:fsd ft11, 912(a5)
[0x80000680]:sw a7, 916(a5)
[0x80000684]:fld ft10, 464(a6)
[0x80000688]:csrrwi zero, frm, 2

[0x8000068c]:fcvt.s.d ft11, ft10, dyn
[0x80000690]:csrrs a7, fflags, zero
[0x80000694]:fsd ft11, 928(a5)
[0x80000698]:sw a7, 932(a5)
[0x8000069c]:fld ft10, 472(a6)
[0x800006a0]:csrrwi zero, frm, 1

[0x800006a4]:fcvt.s.d ft11, ft10, dyn
[0x800006a8]:csrrs a7, fflags, zero
[0x800006ac]:fsd ft11, 944(a5)
[0x800006b0]:sw a7, 948(a5)
[0x800006b4]:fld ft10, 480(a6)
[0x800006b8]:csrrwi zero, frm, 0

[0x800006bc]:fcvt.s.d ft11, ft10, dyn
[0x800006c0]:csrrs a7, fflags, zero
[0x800006c4]:fsd ft11, 960(a5)
[0x800006c8]:sw a7, 964(a5)
[0x800006cc]:fld ft10, 488(a6)
[0x800006d0]:csrrwi zero, frm, 4

[0x800006d4]:fcvt.s.d ft11, ft10, dyn
[0x800006d8]:csrrs a7, fflags, zero
[0x800006dc]:fsd ft11, 976(a5)
[0x800006e0]:sw a7, 980(a5)
[0x800006e4]:fld ft10, 496(a6)
[0x800006e8]:csrrwi zero, frm, 3

[0x800006ec]:fcvt.s.d ft11, ft10, dyn
[0x800006f0]:csrrs a7, fflags, zero
[0x800006f4]:fsd ft11, 992(a5)
[0x800006f8]:sw a7, 996(a5)
[0x800006fc]:fld ft10, 504(a6)
[0x80000700]:csrrwi zero, frm, 2

[0x80000704]:fcvt.s.d ft11, ft10, dyn
[0x80000708]:csrrs a7, fflags, zero
[0x8000070c]:fsd ft11, 1008(a5)
[0x80000710]:sw a7, 1012(a5)
[0x80000714]:fld ft10, 512(a6)
[0x80000718]:csrrwi zero, frm, 1

[0x8000071c]:fcvt.s.d ft11, ft10, dyn
[0x80000720]:csrrs a7, fflags, zero
[0x80000724]:fsd ft11, 1024(a5)
[0x80000728]:sw a7, 1028(a5)
[0x8000072c]:fld ft10, 520(a6)
[0x80000730]:csrrwi zero, frm, 0

[0x80000734]:fcvt.s.d ft11, ft10, dyn
[0x80000738]:csrrs a7, fflags, zero
[0x8000073c]:fsd ft11, 1040(a5)
[0x80000740]:sw a7, 1044(a5)
[0x80000744]:fld ft10, 528(a6)
[0x80000748]:csrrwi zero, frm, 4

[0x8000074c]:fcvt.s.d ft11, ft10, dyn
[0x80000750]:csrrs a7, fflags, zero
[0x80000754]:fsd ft11, 1056(a5)
[0x80000758]:sw a7, 1060(a5)
[0x8000075c]:fld ft10, 536(a6)
[0x80000760]:csrrwi zero, frm, 3

[0x80000764]:fcvt.s.d ft11, ft10, dyn
[0x80000768]:csrrs a7, fflags, zero
[0x8000076c]:fsd ft11, 1072(a5)
[0x80000770]:sw a7, 1076(a5)
[0x80000774]:fld ft10, 544(a6)
[0x80000778]:csrrwi zero, frm, 2

[0x8000077c]:fcvt.s.d ft11, ft10, dyn
[0x80000780]:csrrs a7, fflags, zero
[0x80000784]:fsd ft11, 1088(a5)
[0x80000788]:sw a7, 1092(a5)
[0x8000078c]:fld ft10, 552(a6)
[0x80000790]:csrrwi zero, frm, 1

[0x80000794]:fcvt.s.d ft11, ft10, dyn
[0x80000798]:csrrs a7, fflags, zero
[0x8000079c]:fsd ft11, 1104(a5)
[0x800007a0]:sw a7, 1108(a5)
[0x800007a4]:fld ft10, 560(a6)
[0x800007a8]:csrrwi zero, frm, 0

[0x800007ac]:fcvt.s.d ft11, ft10, dyn
[0x800007b0]:csrrs a7, fflags, zero
[0x800007b4]:fsd ft11, 1120(a5)
[0x800007b8]:sw a7, 1124(a5)
[0x800007bc]:fld ft10, 568(a6)
[0x800007c0]:csrrwi zero, frm, 4

[0x800007c4]:fcvt.s.d ft11, ft10, dyn
[0x800007c8]:csrrs a7, fflags, zero
[0x800007cc]:fsd ft11, 1136(a5)
[0x800007d0]:sw a7, 1140(a5)
[0x800007d4]:fld ft10, 576(a6)
[0x800007d8]:csrrwi zero, frm, 3

[0x800007dc]:fcvt.s.d ft11, ft10, dyn
[0x800007e0]:csrrs a7, fflags, zero
[0x800007e4]:fsd ft11, 1152(a5)
[0x800007e8]:sw a7, 1156(a5)
[0x800007ec]:fld ft10, 584(a6)
[0x800007f0]:csrrwi zero, frm, 2

[0x800007f4]:fcvt.s.d ft11, ft10, dyn
[0x800007f8]:csrrs a7, fflags, zero
[0x800007fc]:fsd ft11, 1168(a5)
[0x80000800]:sw a7, 1172(a5)
[0x80000804]:fld ft10, 592(a6)
[0x80000808]:csrrwi zero, frm, 1

[0x8000080c]:fcvt.s.d ft11, ft10, dyn
[0x80000810]:csrrs a7, fflags, zero
[0x80000814]:fsd ft11, 1184(a5)
[0x80000818]:sw a7, 1188(a5)
[0x8000081c]:fld ft10, 600(a6)
[0x80000820]:csrrwi zero, frm, 0

[0x80000824]:fcvt.s.d ft11, ft10, dyn
[0x80000828]:csrrs a7, fflags, zero
[0x8000082c]:fsd ft11, 1200(a5)
[0x80000830]:sw a7, 1204(a5)
[0x80000834]:fld ft10, 608(a6)
[0x80000838]:csrrwi zero, frm, 4

[0x8000083c]:fcvt.s.d ft11, ft10, dyn
[0x80000840]:csrrs a7, fflags, zero
[0x80000844]:fsd ft11, 1216(a5)
[0x80000848]:sw a7, 1220(a5)
[0x8000084c]:fld ft10, 616(a6)
[0x80000850]:csrrwi zero, frm, 3

[0x80000854]:fcvt.s.d ft11, ft10, dyn
[0x80000858]:csrrs a7, fflags, zero
[0x8000085c]:fsd ft11, 1232(a5)
[0x80000860]:sw a7, 1236(a5)
[0x80000864]:fld ft10, 624(a6)
[0x80000868]:csrrwi zero, frm, 2

[0x8000086c]:fcvt.s.d ft11, ft10, dyn
[0x80000870]:csrrs a7, fflags, zero
[0x80000874]:fsd ft11, 1248(a5)
[0x80000878]:sw a7, 1252(a5)
[0x8000087c]:fld ft10, 632(a6)
[0x80000880]:csrrwi zero, frm, 1



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                       coverpoints                                                                        |                                                                        code                                                                         |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002414]<br>0x00000001|- opcode : fcvt.s.d<br> - rs1 : f14<br> - rd : f31<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.s.d ft11, fa4, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd ft11, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>      |
|   2|[0x80002424]<br>0x00000001|- rs1 : f22<br> - rd : f22<br> - rs1 == rd<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 4  #nosat<br>                         |[0x80000134]:fcvt.s.d fs6, fs6, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd fs6, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>      |
|   3|[0x80002434]<br>0x00000001|- rs1 : f12<br> - rd : f1<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 3  #nosat<br>                                          |[0x8000014c]:fcvt.s.d ft1, fa2, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd ft1, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>      |
|   4|[0x80002444]<br>0x00000001|- rs1 : f17<br> - rd : f8<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 2  #nosat<br>                                          |[0x80000164]:fcvt.s.d fs0, fa7, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fs0, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>      |
|   5|[0x80002454]<br>0x00000001|- rs1 : f2<br> - rd : f23<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 1  #nosat<br>                                          |[0x8000017c]:fcvt.s.d fs7, ft2, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fs7, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>      |
|   6|[0x80002464]<br>0x00000001|- rs1 : f18<br> - rd : f7<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 0  #nosat<br>                                          |[0x80000194]:fcvt.s.d ft7, fs2, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd ft7, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>      |
|   7|[0x80002474]<br>0x00000001|- rs1 : f23<br> - rd : f29<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 4  #nosat<br>                                         |[0x800001ac]:fcvt.s.d ft9, fs7, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft9, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>     |
|   8|[0x80002484]<br>0x00000001|- rs1 : f15<br> - rd : f5<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 3  #nosat<br>                                          |[0x800001c4]:fcvt.s.d ft5, fa5, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd ft5, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>    |
|   9|[0x80002494]<br>0x00000001|- rs1 : f5<br> - rd : f3<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 2  #nosat<br>                                           |[0x800001dc]:fcvt.s.d ft3, ft5, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd ft3, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>    |
|  10|[0x800024a4]<br>0x00000001|- rs1 : f0<br> - rd : f2<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 1  #nosat<br>                                           |[0x800001f4]:fcvt.s.d ft2, ft0, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd ft2, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br>    |
|  11|[0x800024b4]<br>0x00000001|- rs1 : f11<br> - rd : f13<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 0  #nosat<br>                                         |[0x8000020c]:fcvt.s.d fa3, fa1, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd fa3, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>    |
|  12|[0x800024c4]<br>0x00000001|- rs1 : f13<br> - rd : f20<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 4  #nosat<br>                                         |[0x80000224]:fcvt.s.d fs4, fa3, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs4, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>    |
|  13|[0x800024d4]<br>0x00000001|- rs1 : f16<br> - rd : f6<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 3  #nosat<br>                                          |[0x8000023c]:fcvt.s.d ft6, fa6, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd ft6, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>    |
|  14|[0x800024e4]<br>0x00000001|- rs1 : f25<br> - rd : f21<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 2  #nosat<br>                                         |[0x80000254]:fcvt.s.d fs5, fs9, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs5, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>    |
|  15|[0x800024f4]<br>0x00000001|- rs1 : f21<br> - rd : f26<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 1  #nosat<br>                                         |[0x8000026c]:fcvt.s.d fs10, fs5, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd fs10, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>  |
|  16|[0x80002504]<br>0x00000001|- rs1 : f4<br> - rd : f28<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 0  #nosat<br>                                          |[0x80000284]:fcvt.s.d ft8, ft4, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd ft8, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>    |
|  17|[0x80002514]<br>0x00000001|- rs1 : f9<br> - rd : f12<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 4  #nosat<br>                                          |[0x8000029c]:fcvt.s.d fa2, fs1, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsd fa2, 256(a5)<br> [0x800002a8]:sw a7, 260(a5)<br>    |
|  18|[0x80002524]<br>0x00000001|- rs1 : f1<br> - rd : f19<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 3  #nosat<br>                                          |[0x800002b4]:fcvt.s.d fs3, ft1, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsd fs3, 272(a5)<br> [0x800002c0]:sw a7, 276(a5)<br>    |
|  19|[0x80002534]<br>0x00000001|- rs1 : f8<br> - rd : f15<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 2  #nosat<br>                                          |[0x800002cc]:fcvt.s.d fa5, fs0, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsd fa5, 288(a5)<br> [0x800002d8]:sw a7, 292(a5)<br>    |
|  20|[0x80002544]<br>0x00000001|- rs1 : f28<br> - rd : f18<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 1  #nosat<br>                                         |[0x800002e4]:fcvt.s.d fs2, ft8, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fs2, 304(a5)<br> [0x800002f0]:sw a7, 308(a5)<br>    |
|  21|[0x80002554]<br>0x00000001|- rs1 : f27<br> - rd : f17<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 0  #nosat<br>                                         |[0x800002fc]:fcvt.s.d fa7, fs11, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fa7, 320(a5)<br> [0x80000308]:sw a7, 324(a5)<br>   |
|  22|[0x80002564]<br>0x00000001|- rs1 : f10<br> - rd : f4<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 4  #nosat<br>                                          |[0x80000314]:fcvt.s.d ft4, fa0, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsd ft4, 336(a5)<br> [0x80000320]:sw a7, 340(a5)<br>    |
|  23|[0x80002574]<br>0x00000001|- rs1 : f20<br> - rd : f24<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 3  #nosat<br>                                         |[0x8000032c]:fcvt.s.d fs8, fs4, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsd fs8, 352(a5)<br> [0x80000338]:sw a7, 356(a5)<br>    |
|  24|[0x80002584]<br>0x00000001|- rs1 : f6<br> - rd : f16<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 2  #nosat<br>                                          |[0x80000344]:fcvt.s.d fa6, ft6, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fa6, 368(a5)<br> [0x80000350]:sw a7, 372(a5)<br>    |
|  25|[0x80002594]<br>0x00000001|- rs1 : f30<br> - rd : f9<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 1  #nosat<br>                                          |[0x8000035c]:fcvt.s.d fs1, ft10, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsd fs1, 384(a5)<br> [0x80000368]:sw a7, 388(a5)<br>   |
|  26|[0x800025a4]<br>0x00000001|- rs1 : f24<br> - rd : f25<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 0  #nosat<br>                                         |[0x80000374]:fcvt.s.d fs9, fs8, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsd fs9, 400(a5)<br> [0x80000380]:sw a7, 404(a5)<br>    |
|  27|[0x800025b4]<br>0x00000001|- rs1 : f26<br> - rd : f0<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 4  #nosat<br>                                          |[0x8000038c]:fcvt.s.d ft0, fs10, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsd ft0, 416(a5)<br> [0x80000398]:sw a7, 420(a5)<br>   |
|  28|[0x800025c4]<br>0x00000001|- rs1 : f19<br> - rd : f27<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 3  #nosat<br>                                         |[0x800003a4]:fcvt.s.d fs11, fs3, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fs11, 432(a5)<br> [0x800003b0]:sw a7, 436(a5)<br>  |
|  29|[0x800025d4]<br>0x00000001|- rs1 : f31<br> - rd : f11<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 2  #nosat<br>                                         |[0x800003bc]:fcvt.s.d fa1, ft11, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsd fa1, 448(a5)<br> [0x800003c8]:sw a7, 452(a5)<br>   |
|  30|[0x800025e4]<br>0x00000001|- rs1 : f7<br> - rd : f30<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 1  #nosat<br>                                          |[0x800003d4]:fcvt.s.d ft10, ft7, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsd ft10, 464(a5)<br> [0x800003e0]:sw a7, 468(a5)<br>  |
|  31|[0x800025f4]<br>0x00000001|- rs1 : f29<br> - rd : f10<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 0  #nosat<br>                                         |[0x800003ec]:fcvt.s.d fa0, ft9, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsd fa0, 480(a5)<br> [0x800003f8]:sw a7, 484(a5)<br>    |
|  32|[0x80002604]<br>0x00000001|- rs1 : f3<br> - rd : f14<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 4  #nosat<br>                                          |[0x80000404]:fcvt.s.d fa4, ft3, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fa4, 496(a5)<br> [0x80000410]:sw a7, 500(a5)<br>    |
|  33|[0x80002614]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 3  #nosat<br>                                                                        |[0x8000041c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsd ft11, 512(a5)<br> [0x80000428]:sw a7, 516(a5)<br> |
|  34|[0x80002624]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 2  #nosat<br>                                                                        |[0x80000434]:fcvt.s.d ft11, ft10, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:fsd ft11, 528(a5)<br> [0x80000440]:sw a7, 532(a5)<br> |
|  35|[0x80002634]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 1  #nosat<br>                                                                        |[0x8000044c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd ft11, 544(a5)<br> [0x80000458]:sw a7, 548(a5)<br> |
|  36|[0x80002644]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 0  #nosat<br>                                                                        |[0x80000464]:fcvt.s.d ft11, ft10, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd ft11, 560(a5)<br> [0x80000470]:sw a7, 564(a5)<br> |
|  37|[0x80002654]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 4  #nosat<br>                                                                        |[0x8000047c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsd ft11, 576(a5)<br> [0x80000488]:sw a7, 580(a5)<br> |
|  38|[0x80002664]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 3  #nosat<br>                                                                        |[0x80000494]:fcvt.s.d ft11, ft10, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:fsd ft11, 592(a5)<br> [0x800004a0]:sw a7, 596(a5)<br> |
|  39|[0x80002674]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 2  #nosat<br>                                                                        |[0x800004ac]:fcvt.s.d ft11, ft10, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:fsd ft11, 608(a5)<br> [0x800004b8]:sw a7, 612(a5)<br> |
|  40|[0x80002684]<br>0x00000001|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 1  #nosat<br>                                                                        |[0x800004c4]:fcvt.s.d ft11, ft10, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd ft11, 624(a5)<br> [0x800004d0]:sw a7, 628(a5)<br> |
