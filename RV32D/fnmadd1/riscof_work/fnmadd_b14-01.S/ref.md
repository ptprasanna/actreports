
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001640')]      |
| SIG_REGION                | [('0x80004110', '0x80004660', '340 words')]      |
| COV_LABELS                | fnmadd_b14      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fnmadd1/riscof_work/fnmadd_b14-01.S/ref.S    |
| Total Number of coverpoints| 308     |
| Total Coverpoints Hit     | 240      |
| Total Signature Updates   | 107      |
| STAT1                     | 107      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000bc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000bcc]:csrrs a7, fflags, zero
[0x80000bd0]:fsd ft11, 1360(a5)
[0x80000bd4]:sw a7, 1364(a5)
[0x80000bd8]:fld ft10, 24(a6)
[0x80000bdc]:fld ft9, 32(a6)
[0x80000be0]:fld ft8, 40(a6)
[0x80000be4]:csrrwi zero, frm, 0

[0x80000be8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000bec]:csrrs a7, fflags, zero
[0x80000bf0]:fsd ft11, 1376(a5)
[0x80000bf4]:sw a7, 1380(a5)
[0x80000bf8]:fld ft10, 48(a6)
[0x80000bfc]:fld ft9, 56(a6)
[0x80000c00]:fld ft8, 64(a6)
[0x80000c04]:csrrwi zero, frm, 0

[0x80000c08]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000c0c]:csrrs a7, fflags, zero
[0x80000c10]:fsd ft11, 1392(a5)
[0x80000c14]:sw a7, 1396(a5)
[0x80000c18]:fld ft10, 72(a6)
[0x80000c1c]:fld ft9, 80(a6)
[0x80000c20]:fld ft8, 88(a6)
[0x80000c24]:csrrwi zero, frm, 0

[0x80000c28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000c2c]:csrrs a7, fflags, zero
[0x80000c30]:fsd ft11, 1408(a5)
[0x80000c34]:sw a7, 1412(a5)
[0x80000c38]:fld ft10, 96(a6)
[0x80000c3c]:fld ft9, 104(a6)
[0x80000c40]:fld ft8, 112(a6)
[0x80000c44]:csrrwi zero, frm, 0

[0x80000c48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000c4c]:csrrs a7, fflags, zero
[0x80000c50]:fsd ft11, 1424(a5)
[0x80000c54]:sw a7, 1428(a5)
[0x80000c58]:fld ft10, 120(a6)
[0x80000c5c]:fld ft9, 128(a6)
[0x80000c60]:fld ft8, 136(a6)
[0x80000c64]:csrrwi zero, frm, 0

[0x80000c68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000c6c]:csrrs a7, fflags, zero
[0x80000c70]:fsd ft11, 1440(a5)
[0x80000c74]:sw a7, 1444(a5)
[0x80000c78]:fld ft10, 144(a6)
[0x80000c7c]:fld ft9, 152(a6)
[0x80000c80]:fld ft8, 160(a6)
[0x80000c84]:csrrwi zero, frm, 0

[0x80000c88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000c8c]:csrrs a7, fflags, zero
[0x80000c90]:fsd ft11, 1456(a5)
[0x80000c94]:sw a7, 1460(a5)
[0x80000c98]:fld ft10, 168(a6)
[0x80000c9c]:fld ft9, 176(a6)
[0x80000ca0]:fld ft8, 184(a6)
[0x80000ca4]:csrrwi zero, frm, 0

[0x80000ca8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000cac]:csrrs a7, fflags, zero
[0x80000cb0]:fsd ft11, 1472(a5)
[0x80000cb4]:sw a7, 1476(a5)
[0x80000cb8]:fld ft10, 192(a6)
[0x80000cbc]:fld ft9, 200(a6)
[0x80000cc0]:fld ft8, 208(a6)
[0x80000cc4]:csrrwi zero, frm, 0

[0x80000cc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000ccc]:csrrs a7, fflags, zero
[0x80000cd0]:fsd ft11, 1488(a5)
[0x80000cd4]:sw a7, 1492(a5)
[0x80000cd8]:fld ft10, 216(a6)
[0x80000cdc]:fld ft9, 224(a6)
[0x80000ce0]:fld ft8, 232(a6)
[0x80000ce4]:csrrwi zero, frm, 0

[0x80000ce8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000cec]:csrrs a7, fflags, zero
[0x80000cf0]:fsd ft11, 1504(a5)
[0x80000cf4]:sw a7, 1508(a5)
[0x80000cf8]:fld ft10, 240(a6)
[0x80000cfc]:fld ft9, 248(a6)
[0x80000d00]:fld ft8, 256(a6)
[0x80000d04]:csrrwi zero, frm, 0

[0x80000d08]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000d0c]:csrrs a7, fflags, zero
[0x80000d10]:fsd ft11, 1520(a5)
[0x80000d14]:sw a7, 1524(a5)
[0x80000d18]:fld ft10, 264(a6)
[0x80000d1c]:fld ft9, 272(a6)
[0x80000d20]:fld ft8, 280(a6)
[0x80000d24]:csrrwi zero, frm, 0

[0x80000d28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000d2c]:csrrs a7, fflags, zero
[0x80000d30]:fsd ft11, 1536(a5)
[0x80000d34]:sw a7, 1540(a5)
[0x80000d38]:fld ft10, 288(a6)
[0x80000d3c]:fld ft9, 296(a6)
[0x80000d40]:fld ft8, 304(a6)
[0x80000d44]:csrrwi zero, frm, 0

[0x80000d48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000d4c]:csrrs a7, fflags, zero
[0x80000d50]:fsd ft11, 1552(a5)
[0x80000d54]:sw a7, 1556(a5)
[0x80000d58]:fld ft10, 312(a6)
[0x80000d5c]:fld ft9, 320(a6)
[0x80000d60]:fld ft8, 328(a6)
[0x80000d64]:csrrwi zero, frm, 0

[0x80000d68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000d6c]:csrrs a7, fflags, zero
[0x80000d70]:fsd ft11, 1568(a5)
[0x80000d74]:sw a7, 1572(a5)
[0x80000d78]:fld ft10, 336(a6)
[0x80000d7c]:fld ft9, 344(a6)
[0x80000d80]:fld ft8, 352(a6)
[0x80000d84]:csrrwi zero, frm, 0

[0x80000d88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000d8c]:csrrs a7, fflags, zero
[0x80000d90]:fsd ft11, 1584(a5)
[0x80000d94]:sw a7, 1588(a5)
[0x80000d98]:fld ft10, 360(a6)
[0x80000d9c]:fld ft9, 368(a6)
[0x80000da0]:fld ft8, 376(a6)
[0x80000da4]:csrrwi zero, frm, 0

[0x80000da8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000dac]:csrrs a7, fflags, zero
[0x80000db0]:fsd ft11, 1600(a5)
[0x80000db4]:sw a7, 1604(a5)
[0x80000db8]:fld ft10, 384(a6)
[0x80000dbc]:fld ft9, 392(a6)
[0x80000dc0]:fld ft8, 400(a6)
[0x80000dc4]:csrrwi zero, frm, 0

[0x80000dc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000dcc]:csrrs a7, fflags, zero
[0x80000dd0]:fsd ft11, 1616(a5)
[0x80000dd4]:sw a7, 1620(a5)
[0x80000dd8]:fld ft10, 408(a6)
[0x80000ddc]:fld ft9, 416(a6)
[0x80000de0]:fld ft8, 424(a6)
[0x80000de4]:csrrwi zero, frm, 0

[0x80000de8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000dec]:csrrs a7, fflags, zero
[0x80000df0]:fsd ft11, 1632(a5)
[0x80000df4]:sw a7, 1636(a5)
[0x80000df8]:fld ft10, 432(a6)
[0x80000dfc]:fld ft9, 440(a6)
[0x80000e00]:fld ft8, 448(a6)
[0x80000e04]:csrrwi zero, frm, 0

[0x80000e08]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e0c]:csrrs a7, fflags, zero
[0x80000e10]:fsd ft11, 1648(a5)
[0x80000e14]:sw a7, 1652(a5)
[0x80000e18]:fld ft10, 456(a6)
[0x80000e1c]:fld ft9, 464(a6)
[0x80000e20]:fld ft8, 472(a6)
[0x80000e24]:csrrwi zero, frm, 0

[0x80000e28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e2c]:csrrs a7, fflags, zero
[0x80000e30]:fsd ft11, 1664(a5)
[0x80000e34]:sw a7, 1668(a5)
[0x80000e38]:fld ft10, 480(a6)
[0x80000e3c]:fld ft9, 488(a6)
[0x80000e40]:fld ft8, 496(a6)
[0x80000e44]:csrrwi zero, frm, 0

[0x80000e48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e4c]:csrrs a7, fflags, zero
[0x80000e50]:fsd ft11, 1680(a5)
[0x80000e54]:sw a7, 1684(a5)
[0x80000e58]:fld ft10, 504(a6)
[0x80000e5c]:fld ft9, 512(a6)
[0x80000e60]:fld ft8, 520(a6)
[0x80000e64]:csrrwi zero, frm, 0

[0x80000e68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e6c]:csrrs a7, fflags, zero
[0x80000e70]:fsd ft11, 1696(a5)
[0x80000e74]:sw a7, 1700(a5)
[0x80000e78]:fld ft10, 528(a6)
[0x80000e7c]:fld ft9, 536(a6)
[0x80000e80]:fld ft8, 544(a6)
[0x80000e84]:csrrwi zero, frm, 0

[0x80000e88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e8c]:csrrs a7, fflags, zero
[0x80000e90]:fsd ft11, 1712(a5)
[0x80000e94]:sw a7, 1716(a5)
[0x80000e98]:fld ft10, 552(a6)
[0x80000e9c]:fld ft9, 560(a6)
[0x80000ea0]:fld ft8, 568(a6)
[0x80000ea4]:csrrwi zero, frm, 0

[0x80000ea8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000eac]:csrrs a7, fflags, zero
[0x80000eb0]:fsd ft11, 1728(a5)
[0x80000eb4]:sw a7, 1732(a5)
[0x80000eb8]:fld ft10, 576(a6)
[0x80000ebc]:fld ft9, 584(a6)
[0x80000ec0]:fld ft8, 592(a6)
[0x80000ec4]:csrrwi zero, frm, 0

[0x80000ec8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000ecc]:csrrs a7, fflags, zero
[0x80000ed0]:fsd ft11, 1744(a5)
[0x80000ed4]:sw a7, 1748(a5)
[0x80000ed8]:fld ft10, 600(a6)
[0x80000edc]:fld ft9, 608(a6)
[0x80000ee0]:fld ft8, 616(a6)
[0x80000ee4]:csrrwi zero, frm, 0

[0x80000ee8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 1760(a5)
[0x80000ef4]:sw a7, 1764(a5)
[0x80000ef8]:fld ft10, 624(a6)
[0x80000efc]:fld ft9, 632(a6)
[0x80000f00]:fld ft8, 640(a6)
[0x80000f04]:csrrwi zero, frm, 0

[0x80000f08]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f0c]:csrrs a7, fflags, zero
[0x80000f10]:fsd ft11, 1776(a5)
[0x80000f14]:sw a7, 1780(a5)
[0x80000f18]:fld ft10, 648(a6)
[0x80000f1c]:fld ft9, 656(a6)
[0x80000f20]:fld ft8, 664(a6)
[0x80000f24]:csrrwi zero, frm, 0

[0x80000f28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f2c]:csrrs a7, fflags, zero
[0x80000f30]:fsd ft11, 1792(a5)
[0x80000f34]:sw a7, 1796(a5)
[0x80000f38]:fld ft10, 672(a6)
[0x80000f3c]:fld ft9, 680(a6)
[0x80000f40]:fld ft8, 688(a6)
[0x80000f44]:csrrwi zero, frm, 0

[0x80000f48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f4c]:csrrs a7, fflags, zero
[0x80000f50]:fsd ft11, 1808(a5)
[0x80000f54]:sw a7, 1812(a5)
[0x80000f58]:fld ft10, 696(a6)
[0x80000f5c]:fld ft9, 704(a6)
[0x80000f60]:fld ft8, 712(a6)
[0x80000f64]:csrrwi zero, frm, 0

[0x80000f68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f6c]:csrrs a7, fflags, zero
[0x80000f70]:fsd ft11, 1824(a5)
[0x80000f74]:sw a7, 1828(a5)
[0x80000f78]:fld ft10, 720(a6)
[0x80000f7c]:fld ft9, 728(a6)
[0x80000f80]:fld ft8, 736(a6)
[0x80000f84]:csrrwi zero, frm, 0

[0x80000f88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f8c]:csrrs a7, fflags, zero
[0x80000f90]:fsd ft11, 1840(a5)
[0x80000f94]:sw a7, 1844(a5)
[0x80000f98]:fld ft10, 744(a6)
[0x80000f9c]:fld ft9, 752(a6)
[0x80000fa0]:fld ft8, 760(a6)
[0x80000fa4]:csrrwi zero, frm, 0

[0x80000fa8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fac]:csrrs a7, fflags, zero
[0x80000fb0]:fsd ft11, 1856(a5)
[0x80000fb4]:sw a7, 1860(a5)
[0x80000fb8]:fld ft10, 768(a6)
[0x80000fbc]:fld ft9, 776(a6)
[0x80000fc0]:fld ft8, 784(a6)
[0x80000fc4]:csrrwi zero, frm, 0

[0x80000fc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs a7, fflags, zero
[0x80000fd0]:fsd ft11, 1872(a5)
[0x80000fd4]:sw a7, 1876(a5)
[0x80000fd8]:fld ft10, 792(a6)
[0x80000fdc]:fld ft9, 800(a6)
[0x80000fe0]:fld ft8, 808(a6)
[0x80000fe4]:csrrwi zero, frm, 0

[0x80000fe8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fec]:csrrs a7, fflags, zero
[0x80000ff0]:fsd ft11, 1888(a5)
[0x80000ff4]:sw a7, 1892(a5)
[0x80000ff8]:fld ft10, 816(a6)
[0x80000ffc]:fld ft9, 824(a6)
[0x80001000]:fld ft8, 832(a6)
[0x80001004]:csrrwi zero, frm, 0

[0x80001008]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000100c]:csrrs a7, fflags, zero
[0x80001010]:fsd ft11, 1904(a5)
[0x80001014]:sw a7, 1908(a5)
[0x80001018]:fld ft10, 840(a6)
[0x8000101c]:fld ft9, 848(a6)
[0x80001020]:fld ft8, 856(a6)
[0x80001024]:csrrwi zero, frm, 0

[0x80001028]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000102c]:csrrs a7, fflags, zero
[0x80001030]:fsd ft11, 1920(a5)
[0x80001034]:sw a7, 1924(a5)
[0x80001038]:fld ft10, 864(a6)
[0x8000103c]:fld ft9, 872(a6)
[0x80001040]:fld ft8, 880(a6)
[0x80001044]:csrrwi zero, frm, 0

[0x80001048]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000104c]:csrrs a7, fflags, zero
[0x80001050]:fsd ft11, 1936(a5)
[0x80001054]:sw a7, 1940(a5)
[0x80001058]:fld ft10, 888(a6)
[0x8000105c]:fld ft9, 896(a6)
[0x80001060]:fld ft8, 904(a6)
[0x80001064]:csrrwi zero, frm, 0

[0x80001068]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000106c]:csrrs a7, fflags, zero
[0x80001070]:fsd ft11, 1952(a5)
[0x80001074]:sw a7, 1956(a5)
[0x80001078]:fld ft10, 912(a6)
[0x8000107c]:fld ft9, 920(a6)
[0x80001080]:fld ft8, 928(a6)
[0x80001084]:csrrwi zero, frm, 0

[0x80001088]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000108c]:csrrs a7, fflags, zero
[0x80001090]:fsd ft11, 1968(a5)
[0x80001094]:sw a7, 1972(a5)
[0x80001098]:fld ft10, 936(a6)
[0x8000109c]:fld ft9, 944(a6)
[0x800010a0]:fld ft8, 952(a6)
[0x800010a4]:csrrwi zero, frm, 0

[0x800010a8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010ac]:csrrs a7, fflags, zero
[0x800010b0]:fsd ft11, 1984(a5)
[0x800010b4]:sw a7, 1988(a5)
[0x800010b8]:fld ft10, 960(a6)
[0x800010bc]:fld ft9, 968(a6)
[0x800010c0]:fld ft8, 976(a6)
[0x800010c4]:csrrwi zero, frm, 0

[0x800010c8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010cc]:csrrs a7, fflags, zero
[0x800010d0]:fsd ft11, 2000(a5)
[0x800010d4]:sw a7, 2004(a5)
[0x800010d8]:fld ft10, 984(a6)
[0x800010dc]:fld ft9, 992(a6)
[0x800010e0]:fld ft8, 1000(a6)
[0x800010e4]:csrrwi zero, frm, 0

[0x800010e8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs a7, fflags, zero
[0x800010f0]:fsd ft11, 2016(a5)
[0x800010f4]:sw a7, 2020(a5)
[0x800010f8]:auipc a5, 3
[0x800010fc]:addi a5, a5, 1040
[0x80001100]:fld ft10, 1008(a6)
[0x80001104]:fld ft9, 1016(a6)
[0x80001108]:fld ft8, 1024(a6)
[0x8000110c]:csrrwi zero, frm, 0

[0x800013d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800013d4]:csrrs a7, fflags, zero
[0x800013d8]:fsd ft11, 352(a5)
[0x800013dc]:sw a7, 356(a5)
[0x800013e0]:fld ft10, 1560(a6)
[0x800013e4]:fld ft9, 1568(a6)
[0x800013e8]:fld ft8, 1576(a6)
[0x800013ec]:csrrwi zero, frm, 0

[0x800013f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800013f4]:csrrs a7, fflags, zero
[0x800013f8]:fsd ft11, 368(a5)
[0x800013fc]:sw a7, 372(a5)
[0x80001400]:fld ft10, 1584(a6)
[0x80001404]:fld ft9, 1592(a6)
[0x80001408]:fld ft8, 1600(a6)
[0x8000140c]:csrrwi zero, frm, 0

[0x80001410]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001414]:csrrs a7, fflags, zero
[0x80001418]:fsd ft11, 384(a5)
[0x8000141c]:sw a7, 388(a5)
[0x80001420]:fld ft10, 1608(a6)
[0x80001424]:fld ft9, 1616(a6)
[0x80001428]:fld ft8, 1624(a6)
[0x8000142c]:csrrwi zero, frm, 0

[0x80001430]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001434]:csrrs a7, fflags, zero
[0x80001438]:fsd ft11, 400(a5)
[0x8000143c]:sw a7, 404(a5)
[0x80001440]:fld ft10, 1632(a6)
[0x80001444]:fld ft9, 1640(a6)
[0x80001448]:fld ft8, 1648(a6)
[0x8000144c]:csrrwi zero, frm, 0

[0x80001450]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001454]:csrrs a7, fflags, zero
[0x80001458]:fsd ft11, 416(a5)
[0x8000145c]:sw a7, 420(a5)
[0x80001460]:fld ft10, 1656(a6)
[0x80001464]:fld ft9, 1664(a6)
[0x80001468]:fld ft8, 1672(a6)
[0x8000146c]:csrrwi zero, frm, 0

[0x80001470]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001474]:csrrs a7, fflags, zero
[0x80001478]:fsd ft11, 432(a5)
[0x8000147c]:sw a7, 436(a5)
[0x80001480]:fld ft10, 1680(a6)
[0x80001484]:fld ft9, 1688(a6)
[0x80001488]:fld ft8, 1696(a6)
[0x8000148c]:csrrwi zero, frm, 0

[0x80001490]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001494]:csrrs a7, fflags, zero
[0x80001498]:fsd ft11, 448(a5)
[0x8000149c]:sw a7, 452(a5)
[0x800014a0]:fld ft10, 1704(a6)
[0x800014a4]:fld ft9, 1712(a6)
[0x800014a8]:fld ft8, 1720(a6)
[0x800014ac]:csrrwi zero, frm, 0

[0x800014b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800014b4]:csrrs a7, fflags, zero
[0x800014b8]:fsd ft11, 464(a5)
[0x800014bc]:sw a7, 468(a5)
[0x800014c0]:fld ft10, 1728(a6)
[0x800014c4]:fld ft9, 1736(a6)
[0x800014c8]:fld ft8, 1744(a6)
[0x800014cc]:csrrwi zero, frm, 0

[0x800014d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800014d4]:csrrs a7, fflags, zero
[0x800014d8]:fsd ft11, 480(a5)
[0x800014dc]:sw a7, 484(a5)
[0x800014e0]:fld ft10, 1752(a6)
[0x800014e4]:fld ft9, 1760(a6)
[0x800014e8]:fld ft8, 1768(a6)
[0x800014ec]:csrrwi zero, frm, 0

[0x800014f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800014f4]:csrrs a7, fflags, zero
[0x800014f8]:fsd ft11, 496(a5)
[0x800014fc]:sw a7, 500(a5)
[0x80001500]:fld ft10, 1776(a6)
[0x80001504]:fld ft9, 1784(a6)
[0x80001508]:fld ft8, 1792(a6)
[0x8000150c]:csrrwi zero, frm, 0

[0x80001510]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001514]:csrrs a7, fflags, zero
[0x80001518]:fsd ft11, 512(a5)
[0x8000151c]:sw a7, 516(a5)
[0x80001520]:fld ft10, 1800(a6)
[0x80001524]:fld ft9, 1808(a6)
[0x80001528]:fld ft8, 1816(a6)
[0x8000152c]:csrrwi zero, frm, 0

[0x80001530]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001534]:csrrs a7, fflags, zero
[0x80001538]:fsd ft11, 528(a5)
[0x8000153c]:sw a7, 532(a5)
[0x80001540]:fld ft10, 1824(a6)
[0x80001544]:fld ft9, 1832(a6)
[0x80001548]:fld ft8, 1840(a6)
[0x8000154c]:csrrwi zero, frm, 0

[0x80001550]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001554]:csrrs a7, fflags, zero
[0x80001558]:fsd ft11, 544(a5)
[0x8000155c]:sw a7, 548(a5)
[0x80001560]:fld ft10, 1848(a6)
[0x80001564]:fld ft9, 1856(a6)
[0x80001568]:fld ft8, 1864(a6)
[0x8000156c]:csrrwi zero, frm, 0

[0x80001570]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001574]:csrrs a7, fflags, zero
[0x80001578]:fsd ft11, 560(a5)
[0x8000157c]:sw a7, 564(a5)
[0x80001580]:fld ft10, 1872(a6)
[0x80001584]:fld ft9, 1880(a6)
[0x80001588]:fld ft8, 1888(a6)
[0x8000158c]:csrrwi zero, frm, 0

[0x80001590]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001594]:csrrs a7, fflags, zero
[0x80001598]:fsd ft11, 576(a5)
[0x8000159c]:sw a7, 580(a5)
[0x800015a0]:fld ft10, 1896(a6)
[0x800015a4]:fld ft9, 1904(a6)
[0x800015a8]:fld ft8, 1912(a6)
[0x800015ac]:csrrwi zero, frm, 0

[0x800015b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800015b4]:csrrs a7, fflags, zero
[0x800015b8]:fsd ft11, 592(a5)
[0x800015bc]:sw a7, 596(a5)
[0x800015c0]:fld ft10, 1920(a6)
[0x800015c4]:fld ft9, 1928(a6)
[0x800015c8]:fld ft8, 1936(a6)
[0x800015cc]:csrrwi zero, frm, 0

[0x800015d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800015d4]:csrrs a7, fflags, zero
[0x800015d8]:fsd ft11, 608(a5)
[0x800015dc]:sw a7, 612(a5)
[0x800015e0]:fld ft10, 1944(a6)
[0x800015e4]:fld ft9, 1952(a6)
[0x800015e8]:fld ft8, 1960(a6)
[0x800015ec]:csrrwi zero, frm, 0

[0x800015f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800015f4]:csrrs a7, fflags, zero
[0x800015f8]:fsd ft11, 624(a5)
[0x800015fc]:sw a7, 628(a5)
[0x80001600]:fld ft10, 1968(a6)
[0x80001604]:fld ft9, 1976(a6)
[0x80001608]:fld ft8, 1984(a6)
[0x8000160c]:csrrwi zero, frm, 0

[0x80001610]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001614]:csrrs a7, fflags, zero
[0x80001618]:fsd ft11, 640(a5)
[0x8000161c]:sw a7, 644(a5)
[0x80001620]:fld ft10, 1992(a6)
[0x80001624]:fld ft9, 2000(a6)
[0x80001628]:fld ft8, 2008(a6)
[0x8000162c]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                        coverpoints                                                                                                                                                                         |                                                                              code                                                                               |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004114]<br>0x00000001|- opcode : fnmadd.d<br> - rs1 : f7<br> - rs2 : f7<br> - rs3 : f19<br> - rd : f7<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                           |[0x80000124]:fnmadd.d ft7, ft7, ft7, fs3, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd ft7, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>          |
|   2|[0x80004124]<br>0x00000001|- rs1 : f17<br> - rs2 : f8<br> - rs3 : f21<br> - rd : f8<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x602 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                |[0x80000144]:fnmadd.d fs0, fa7, fs0, fs5, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fs0, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80004134]<br>0x00000001|- rs1 : f29<br> - rs2 : f14<br> - rs3 : f14<br> - rd : f14<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                |[0x80000164]:fnmadd.d fa4, ft9, fa4, fa4, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fa4, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80004144]<br>0x00000001|- rs1 : f26<br> - rs2 : f6<br> - rs3 : f26<br> - rd : f26<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                 |[0x80000184]:fnmadd.d fs10, fs10, ft6, fs10, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd fs10, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>    |
|   5|[0x80004154]<br>0x00000001|- rs1 : f20<br> - rs2 : f23<br> - rs3 : f8<br> - rd : f17<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x48e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br> |[0x800001a4]:fnmadd.d fa7, fs4, fs7, fs0, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fa7, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>        |
|   6|[0x80004164]<br>0x00000001|- rs1 : f31<br> - rs2 : f31<br> - rs3 : f4<br> - rd : f27<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                    |[0x800001c4]:fnmadd.d fs11, ft11, ft11, ft4, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs11, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>    |
|   7|[0x80004174]<br>0x00000001|- rs1 : f11<br> - rs2 : f25<br> - rs3 : f23<br> - rd : f11<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x48c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                              |[0x800001e4]:fnmadd.d fa1, fa1, fs9, fs7, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa1, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80004184]<br>0x00000001|- rs1 : f5<br> - rs2 : f1<br> - rs3 : f0<br> - rd : f0<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x48b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                  |[0x80000204]:fnmadd.d ft0, ft5, ft1, ft0, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd ft0, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80004194]<br>0x00000001|- rs1 : f16<br> - rs2 : f16<br> - rs3 : f16<br> - rd : f1<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                 |[0x80000224]:fnmadd.d ft1, fa6, fa6, fa6, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft1, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>      |
|  10|[0x800041a4]<br>0x00000001|- rs1 : f23<br> - rs2 : f27<br> - rs3 : f27<br> - rd : f6<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                    |[0x80000244]:fnmadd.d ft6, fs7, fs11, fs11, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd ft6, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>    |
|  11|[0x800041b4]<br>0x00000001|- rs1 : f9<br> - rs2 : f9<br> - rs3 : f9<br> - rd : f9<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                    |[0x80000264]:fnmadd.d fs1, fs1, fs1, fs1, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fs1, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>      |
|  12|[0x800041c4]<br>0x00000001|- rs1 : f25<br> - rs2 : f10<br> - rs3 : f25<br> - rd : f21<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                   |[0x80000284]:fnmadd.d fs5, fs9, fa0, fs9, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fs5, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>      |
|  13|[0x800041d4]<br>0x00000001|- rs1 : f24<br> - rs2 : f20<br> - rs3 : f12<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x486 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                         |[0x800002a4]:fnmadd.d ft10, fs8, fs4, fa2, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd ft10, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>    |
|  14|[0x800041e4]<br>0x00000001|- rs1 : f10<br> - rs2 : f19<br> - rs3 : f24<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x485 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                         |[0x800002c4]:fnmadd.d fa3, fa0, fs3, fs8, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fa3, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>      |
|  15|[0x800041f4]<br>0x00000001|- rs1 : f13<br> - rs2 : f22<br> - rs3 : f31<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x484 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                         |[0x800002e4]:fnmadd.d fs7, fa3, fs6, ft11, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fs7, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>     |
|  16|[0x80004204]<br>0x00000001|- rs1 : f15<br> - rs2 : f12<br> - rs3 : f6<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x483 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x80000304]:fnmadd.d fs6, fa5, fa2, ft6, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd fs6, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>      |
|  17|[0x80004214]<br>0x00000001|- rs1 : f28<br> - rs2 : f3<br> - rs3 : f18<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x482 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x80000324]:fnmadd.d fs3, ft8, ft3, fs2, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd fs3, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>      |
|  18|[0x80004224]<br>0x00000001|- rs1 : f3<br> - rs2 : f4<br> - rs3 : f22<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x481 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                           |[0x80000344]:fnmadd.d fs8, ft3, ft4, fs6, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fs8, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>      |
|  19|[0x80004234]<br>0x00000001|- rs1 : f19<br> - rs2 : f15<br> - rs3 : f5<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x480 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x80000364]:fnmadd.d ft8, fs3, fa5, ft5, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd ft8, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>      |
|  20|[0x80004244]<br>0x00000001|- rs1 : f1<br> - rs2 : f28<br> - rs3 : f10<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x80000384]:fnmadd.d fs9, ft1, ft8, fa0, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd fs9, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>      |
|  21|[0x80004254]<br>0x00000001|- rs1 : f12<br> - rs2 : f2<br> - rs3 : f30<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                           |[0x800003a4]:fnmadd.d ft3, fa2, ft2, ft10, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft3, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>     |
|  22|[0x80004264]<br>0x00000001|- rs1 : f4<br> - rs2 : f26<br> - rs3 : f20<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47d and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x800003c4]:fnmadd.d ft11, ft4, fs10, fs4, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd ft11, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>   |
|  23|[0x80004274]<br>0x00000001|- rs1 : f18<br> - rs2 : f11<br> - rs3 : f7<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x800003e4]:fnmadd.d fa2, fs2, fa1, ft7, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fa2, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80004284]<br>0x00000001|- rs1 : f0<br> - rs2 : f17<br> - rs3 : f15<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                           |[0x80000404]:fnmadd.d ft2, ft0, fa7, fa5, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd ft2, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>      |
|  25|[0x80004294]<br>0x00000001|- rs1 : f8<br> - rs2 : f0<br> - rs3 : f13<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x47a and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                           |[0x80000424]:fnmadd.d fa6, fs0, ft0, fa3, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd fa6, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>      |
|  26|[0x800042a4]<br>0x00000001|- rs1 : f14<br> - rs2 : f21<br> - rs3 : f11<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x479 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                         |[0x80000444]:fnmadd.d fa5, fa4, fs5, fa1, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fa5, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>      |
|  27|[0x800042b4]<br>0x00000001|- rs1 : f21<br> - rs2 : f13<br> - rs3 : f3<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x478 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x80000464]:fnmadd.d fa0, fs5, fa3, ft3, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd fa0, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x800042c4]<br>0x00000001|- rs1 : f22<br> - rs2 : f29<br> - rs3 : f2<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x477 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                           |[0x80000484]:fnmadd.d ft5, fs6, ft9, ft2, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd ft5, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x800042d4]<br>0x00000001|- rs1 : f27<br> - rs2 : f5<br> - rs3 : f17<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x476 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x800004a4]:fnmadd.d ft9, fs11, ft5, fa7, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd ft9, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>     |
|  30|[0x800042e4]<br>0x00000001|- rs1 : f6<br> - rs2 : f24<br> - rs3 : f29<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x475 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                          |[0x800004c4]:fnmadd.d fs2, ft6, fs8, ft9, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd fs2, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>      |
|  31|[0x800042f4]<br>0x00000001|- rs1 : f30<br> - rs2 : f18<br> - rs3 : f28<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x474 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                         |[0x800004e4]:fnmadd.d fs4, ft10, fs2, ft8, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd fs4, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>     |
|  32|[0x80004304]<br>0x00000001|- rs1 : f2<br> - rs2 : f30<br> - rs3 : f1<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x473 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                            |[0x80000504]:fnmadd.d ft4, ft2, ft10, ft1, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd ft4, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>     |
|  33|[0x80004314]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x472 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000524]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft11, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>   |
|  34|[0x80004324]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x471 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000544]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd ft11, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>   |
|  35|[0x80004334]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x470 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000564]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>   |
|  36|[0x80004344]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000584]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80004354]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80004364]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46d and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80004374]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80004384]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000604]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80004394]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x46a and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000624]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x800043a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x469 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000644]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x800043b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x468 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000664]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x800043c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x467 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000684]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x800043d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x466 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x800043e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x465 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x800043f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x464 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80004404]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x463 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000704]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80004414]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x462 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000724]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80004424]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x461 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000744]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80004434]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x460 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000764]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80004444]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000784]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80004454]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80004464]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45d and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80004474]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80004484]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000804]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80004494]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x45a and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000824]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x800044a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x459 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000844]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x800044b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x458 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000864]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x800044c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x457 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000884]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x800044d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x456 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x800044e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x455 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x800044f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x454 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80004504]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x453 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000904]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80004514]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x452 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000924]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80004524]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x451 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000944]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80004534]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x450 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000964]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80004544]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000984]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80004554]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80004564]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44d and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80004574]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80004584]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80004594]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x44a and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800045a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x449 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800045b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x448 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800045c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x447 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800045d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x446 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000aa4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800045e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x445 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ac4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800045f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x444 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ae4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80004604]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x443 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80004614]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x442 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80004624]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x441 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80004634]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x440 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80004644]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x43f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80004654]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x43e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ba4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x8000450c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x413 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001110]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
|  87|[0x8000451c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x412 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001130]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
|  88|[0x8000452c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x411 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001150]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
|  89|[0x8000453c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x410 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001170]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
|  90|[0x8000454c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40f and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001190]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
|  91|[0x8000455c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40e and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
|  92|[0x8000456c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40d and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
|  93|[0x8000457c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40c and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
|  94|[0x8000458c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40b and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001210]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
|  95|[0x8000459c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x40a and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001230]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
|  96|[0x800045ac]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x409 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001250]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
|  97|[0x800045bc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x408 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001270]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
|  98|[0x800045cc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x407 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001290]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
|  99|[0x800045dc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x406 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 100|[0x800045ec]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x405 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 101|[0x800045fc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x404 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 102|[0x8000460c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x403 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001310]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 103|[0x8000461c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x402 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001330]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 104|[0x8000462c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x401 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001350]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 105|[0x8000463c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x400 and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001370]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 106|[0x8000464c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x3ff and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001390]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 107|[0x8000465c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe97d52f73d2ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xe549a4f656ebe and fs3 == 0 and fe3 == 0x3fe and fm3 == 0x2f4abd475417f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
