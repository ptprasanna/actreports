
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001b30')]      |
| SIG_REGION                | [('0x80004510', '0x80004b90', '416 words')]      |
| COV_LABELS                | fnmadd_b16      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fnmadd1/riscof_work/fnmadd_b16-01.S/ref.S    |
| Total Number of coverpoints| 348     |
| Total Coverpoints Hit     | 278      |
| Total Signature Updates   | 145      |
| STAT1                     | 145      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000e28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e2c]:csrrs a7, fflags, zero
[0x80000e30]:fsd ft11, 1664(a5)
[0x80000e34]:sw a7, 1668(a5)
[0x80000e38]:fld ft10, 480(a6)
[0x80000e3c]:fld ft9, 488(a6)
[0x80000e40]:fld ft8, 496(a6)
[0x80000e44]:csrrwi zero, frm, 0

[0x80000e48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e4c]:csrrs a7, fflags, zero
[0x80000e50]:fsd ft11, 1680(a5)
[0x80000e54]:sw a7, 1684(a5)
[0x80000e58]:fld ft10, 504(a6)
[0x80000e5c]:fld ft9, 512(a6)
[0x80000e60]:fld ft8, 520(a6)
[0x80000e64]:csrrwi zero, frm, 0

[0x80000e68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e6c]:csrrs a7, fflags, zero
[0x80000e70]:fsd ft11, 1696(a5)
[0x80000e74]:sw a7, 1700(a5)
[0x80000e78]:fld ft10, 528(a6)
[0x80000e7c]:fld ft9, 536(a6)
[0x80000e80]:fld ft8, 544(a6)
[0x80000e84]:csrrwi zero, frm, 0

[0x80000e88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000e8c]:csrrs a7, fflags, zero
[0x80000e90]:fsd ft11, 1712(a5)
[0x80000e94]:sw a7, 1716(a5)
[0x80000e98]:fld ft10, 552(a6)
[0x80000e9c]:fld ft9, 560(a6)
[0x80000ea0]:fld ft8, 568(a6)
[0x80000ea4]:csrrwi zero, frm, 0

[0x80000ea8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000eac]:csrrs a7, fflags, zero
[0x80000eb0]:fsd ft11, 1728(a5)
[0x80000eb4]:sw a7, 1732(a5)
[0x80000eb8]:fld ft10, 576(a6)
[0x80000ebc]:fld ft9, 584(a6)
[0x80000ec0]:fld ft8, 592(a6)
[0x80000ec4]:csrrwi zero, frm, 0

[0x80000ec8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000ecc]:csrrs a7, fflags, zero
[0x80000ed0]:fsd ft11, 1744(a5)
[0x80000ed4]:sw a7, 1748(a5)
[0x80000ed8]:fld ft10, 600(a6)
[0x80000edc]:fld ft9, 608(a6)
[0x80000ee0]:fld ft8, 616(a6)
[0x80000ee4]:csrrwi zero, frm, 0

[0x80000ee8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 1760(a5)
[0x80000ef4]:sw a7, 1764(a5)
[0x80000ef8]:fld ft10, 624(a6)
[0x80000efc]:fld ft9, 632(a6)
[0x80000f00]:fld ft8, 640(a6)
[0x80000f04]:csrrwi zero, frm, 0

[0x80000f08]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f0c]:csrrs a7, fflags, zero
[0x80000f10]:fsd ft11, 1776(a5)
[0x80000f14]:sw a7, 1780(a5)
[0x80000f18]:fld ft10, 648(a6)
[0x80000f1c]:fld ft9, 656(a6)
[0x80000f20]:fld ft8, 664(a6)
[0x80000f24]:csrrwi zero, frm, 0

[0x80000f28]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f2c]:csrrs a7, fflags, zero
[0x80000f30]:fsd ft11, 1792(a5)
[0x80000f34]:sw a7, 1796(a5)
[0x80000f38]:fld ft10, 672(a6)
[0x80000f3c]:fld ft9, 680(a6)
[0x80000f40]:fld ft8, 688(a6)
[0x80000f44]:csrrwi zero, frm, 0

[0x80000f48]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f4c]:csrrs a7, fflags, zero
[0x80000f50]:fsd ft11, 1808(a5)
[0x80000f54]:sw a7, 1812(a5)
[0x80000f58]:fld ft10, 696(a6)
[0x80000f5c]:fld ft9, 704(a6)
[0x80000f60]:fld ft8, 712(a6)
[0x80000f64]:csrrwi zero, frm, 0

[0x80000f68]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f6c]:csrrs a7, fflags, zero
[0x80000f70]:fsd ft11, 1824(a5)
[0x80000f74]:sw a7, 1828(a5)
[0x80000f78]:fld ft10, 720(a6)
[0x80000f7c]:fld ft9, 728(a6)
[0x80000f80]:fld ft8, 736(a6)
[0x80000f84]:csrrwi zero, frm, 0

[0x80000f88]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000f8c]:csrrs a7, fflags, zero
[0x80000f90]:fsd ft11, 1840(a5)
[0x80000f94]:sw a7, 1844(a5)
[0x80000f98]:fld ft10, 744(a6)
[0x80000f9c]:fld ft9, 752(a6)
[0x80000fa0]:fld ft8, 760(a6)
[0x80000fa4]:csrrwi zero, frm, 0

[0x80000fa8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fac]:csrrs a7, fflags, zero
[0x80000fb0]:fsd ft11, 1856(a5)
[0x80000fb4]:sw a7, 1860(a5)
[0x80000fb8]:fld ft10, 768(a6)
[0x80000fbc]:fld ft9, 776(a6)
[0x80000fc0]:fld ft8, 784(a6)
[0x80000fc4]:csrrwi zero, frm, 0

[0x80000fc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs a7, fflags, zero
[0x80000fd0]:fsd ft11, 1872(a5)
[0x80000fd4]:sw a7, 1876(a5)
[0x80000fd8]:fld ft10, 792(a6)
[0x80000fdc]:fld ft9, 800(a6)
[0x80000fe0]:fld ft8, 808(a6)
[0x80000fe4]:csrrwi zero, frm, 0

[0x80000fe8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80000fec]:csrrs a7, fflags, zero
[0x80000ff0]:fsd ft11, 1888(a5)
[0x80000ff4]:sw a7, 1892(a5)
[0x80000ff8]:fld ft10, 816(a6)
[0x80000ffc]:fld ft9, 824(a6)
[0x80001000]:fld ft8, 832(a6)
[0x80001004]:csrrwi zero, frm, 0

[0x80001008]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000100c]:csrrs a7, fflags, zero
[0x80001010]:fsd ft11, 1904(a5)
[0x80001014]:sw a7, 1908(a5)
[0x80001018]:fld ft10, 840(a6)
[0x8000101c]:fld ft9, 848(a6)
[0x80001020]:fld ft8, 856(a6)
[0x80001024]:csrrwi zero, frm, 0

[0x80001028]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000102c]:csrrs a7, fflags, zero
[0x80001030]:fsd ft11, 1920(a5)
[0x80001034]:sw a7, 1924(a5)
[0x80001038]:fld ft10, 864(a6)
[0x8000103c]:fld ft9, 872(a6)
[0x80001040]:fld ft8, 880(a6)
[0x80001044]:csrrwi zero, frm, 0

[0x80001048]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000104c]:csrrs a7, fflags, zero
[0x80001050]:fsd ft11, 1936(a5)
[0x80001054]:sw a7, 1940(a5)
[0x80001058]:fld ft10, 888(a6)
[0x8000105c]:fld ft9, 896(a6)
[0x80001060]:fld ft8, 904(a6)
[0x80001064]:csrrwi zero, frm, 0

[0x80001068]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000106c]:csrrs a7, fflags, zero
[0x80001070]:fsd ft11, 1952(a5)
[0x80001074]:sw a7, 1956(a5)
[0x80001078]:fld ft10, 912(a6)
[0x8000107c]:fld ft9, 920(a6)
[0x80001080]:fld ft8, 928(a6)
[0x80001084]:csrrwi zero, frm, 0

[0x80001088]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x8000108c]:csrrs a7, fflags, zero
[0x80001090]:fsd ft11, 1968(a5)
[0x80001094]:sw a7, 1972(a5)
[0x80001098]:fld ft10, 936(a6)
[0x8000109c]:fld ft9, 944(a6)
[0x800010a0]:fld ft8, 952(a6)
[0x800010a4]:csrrwi zero, frm, 0

[0x800010a8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010ac]:csrrs a7, fflags, zero
[0x800010b0]:fsd ft11, 1984(a5)
[0x800010b4]:sw a7, 1988(a5)
[0x800010b8]:fld ft10, 960(a6)
[0x800010bc]:fld ft9, 968(a6)
[0x800010c0]:fld ft8, 976(a6)
[0x800010c4]:csrrwi zero, frm, 0

[0x800010c8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010cc]:csrrs a7, fflags, zero
[0x800010d0]:fsd ft11, 2000(a5)
[0x800010d4]:sw a7, 2004(a5)
[0x800010d8]:fld ft10, 984(a6)
[0x800010dc]:fld ft9, 992(a6)
[0x800010e0]:fld ft8, 1000(a6)
[0x800010e4]:csrrwi zero, frm, 0

[0x800010e8]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs a7, fflags, zero
[0x800010f0]:fsd ft11, 2016(a5)
[0x800010f4]:sw a7, 2020(a5)
[0x800010f8]:auipc a5, 4
[0x800010fc]:addi a5, a5, 2064
[0x80001100]:fld ft10, 1008(a6)
[0x80001104]:fld ft9, 1016(a6)
[0x80001108]:fld ft8, 1024(a6)
[0x8000110c]:csrrwi zero, frm, 0

[0x80001630]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001634]:csrrs a7, fflags, zero
[0x80001638]:fsd ft11, 656(a5)
[0x8000163c]:sw a7, 660(a5)
[0x80001640]:fld ft10, 2016(a6)
[0x80001644]:fld ft9, 2024(a6)
[0x80001648]:fld ft8, 2032(a6)
[0x8000164c]:csrrwi zero, frm, 0

[0x80001650]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001654]:csrrs a7, fflags, zero
[0x80001658]:fsd ft11, 672(a5)
[0x8000165c]:sw a7, 676(a5)
[0x80001660]:addi a6, a6, 2040
[0x80001664]:fld ft10, 0(a6)
[0x80001668]:fld ft9, 8(a6)
[0x8000166c]:fld ft8, 16(a6)
[0x80001670]:csrrwi zero, frm, 0

[0x80001674]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001678]:csrrs a7, fflags, zero
[0x8000167c]:fsd ft11, 688(a5)
[0x80001680]:sw a7, 692(a5)
[0x80001684]:fld ft10, 24(a6)
[0x80001688]:fld ft9, 32(a6)
[0x8000168c]:fld ft8, 40(a6)
[0x80001690]:csrrwi zero, frm, 0

[0x80001694]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001698]:csrrs a7, fflags, zero
[0x8000169c]:fsd ft11, 704(a5)
[0x800016a0]:sw a7, 708(a5)
[0x800016a4]:fld ft10, 48(a6)
[0x800016a8]:fld ft9, 56(a6)
[0x800016ac]:fld ft8, 64(a6)
[0x800016b0]:csrrwi zero, frm, 0

[0x800016b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800016b8]:csrrs a7, fflags, zero
[0x800016bc]:fsd ft11, 720(a5)
[0x800016c0]:sw a7, 724(a5)
[0x800016c4]:fld ft10, 72(a6)
[0x800016c8]:fld ft9, 80(a6)
[0x800016cc]:fld ft8, 88(a6)
[0x800016d0]:csrrwi zero, frm, 0

[0x800016d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800016d8]:csrrs a7, fflags, zero
[0x800016dc]:fsd ft11, 736(a5)
[0x800016e0]:sw a7, 740(a5)
[0x800016e4]:fld ft10, 96(a6)
[0x800016e8]:fld ft9, 104(a6)
[0x800016ec]:fld ft8, 112(a6)
[0x800016f0]:csrrwi zero, frm, 0

[0x800016f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800016f8]:csrrs a7, fflags, zero
[0x800016fc]:fsd ft11, 752(a5)
[0x80001700]:sw a7, 756(a5)
[0x80001704]:fld ft10, 120(a6)
[0x80001708]:fld ft9, 128(a6)
[0x8000170c]:fld ft8, 136(a6)
[0x80001710]:csrrwi zero, frm, 0

[0x80001714]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001718]:csrrs a7, fflags, zero
[0x8000171c]:fsd ft11, 768(a5)
[0x80001720]:sw a7, 772(a5)
[0x80001724]:fld ft10, 144(a6)
[0x80001728]:fld ft9, 152(a6)
[0x8000172c]:fld ft8, 160(a6)
[0x80001730]:csrrwi zero, frm, 0

[0x80001734]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001738]:csrrs a7, fflags, zero
[0x8000173c]:fsd ft11, 784(a5)
[0x80001740]:sw a7, 788(a5)
[0x80001744]:fld ft10, 168(a6)
[0x80001748]:fld ft9, 176(a6)
[0x8000174c]:fld ft8, 184(a6)
[0x80001750]:csrrwi zero, frm, 0

[0x80001754]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001758]:csrrs a7, fflags, zero
[0x8000175c]:fsd ft11, 800(a5)
[0x80001760]:sw a7, 804(a5)
[0x80001764]:fld ft10, 192(a6)
[0x80001768]:fld ft9, 200(a6)
[0x8000176c]:fld ft8, 208(a6)
[0x80001770]:csrrwi zero, frm, 0

[0x80001774]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001778]:csrrs a7, fflags, zero
[0x8000177c]:fsd ft11, 816(a5)
[0x80001780]:sw a7, 820(a5)
[0x80001784]:fld ft10, 216(a6)
[0x80001788]:fld ft9, 224(a6)
[0x8000178c]:fld ft8, 232(a6)
[0x80001790]:csrrwi zero, frm, 0

[0x80001794]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001798]:csrrs a7, fflags, zero
[0x8000179c]:fsd ft11, 832(a5)
[0x800017a0]:sw a7, 836(a5)
[0x800017a4]:fld ft10, 240(a6)
[0x800017a8]:fld ft9, 248(a6)
[0x800017ac]:fld ft8, 256(a6)
[0x800017b0]:csrrwi zero, frm, 0

[0x800017b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800017b8]:csrrs a7, fflags, zero
[0x800017bc]:fsd ft11, 848(a5)
[0x800017c0]:sw a7, 852(a5)
[0x800017c4]:fld ft10, 264(a6)
[0x800017c8]:fld ft9, 272(a6)
[0x800017cc]:fld ft8, 280(a6)
[0x800017d0]:csrrwi zero, frm, 0

[0x800017d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800017d8]:csrrs a7, fflags, zero
[0x800017dc]:fsd ft11, 864(a5)
[0x800017e0]:sw a7, 868(a5)
[0x800017e4]:fld ft10, 288(a6)
[0x800017e8]:fld ft9, 296(a6)
[0x800017ec]:fld ft8, 304(a6)
[0x800017f0]:csrrwi zero, frm, 0

[0x800017f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800017f8]:csrrs a7, fflags, zero
[0x800017fc]:fsd ft11, 880(a5)
[0x80001800]:sw a7, 884(a5)
[0x80001804]:fld ft10, 312(a6)
[0x80001808]:fld ft9, 320(a6)
[0x8000180c]:fld ft8, 328(a6)
[0x80001810]:csrrwi zero, frm, 0

[0x80001814]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001818]:csrrs a7, fflags, zero
[0x8000181c]:fsd ft11, 896(a5)
[0x80001820]:sw a7, 900(a5)
[0x80001824]:fld ft10, 336(a6)
[0x80001828]:fld ft9, 344(a6)
[0x8000182c]:fld ft8, 352(a6)
[0x80001830]:csrrwi zero, frm, 0

[0x80001834]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001838]:csrrs a7, fflags, zero
[0x8000183c]:fsd ft11, 912(a5)
[0x80001840]:sw a7, 916(a5)
[0x80001844]:fld ft10, 360(a6)
[0x80001848]:fld ft9, 368(a6)
[0x8000184c]:fld ft8, 376(a6)
[0x80001850]:csrrwi zero, frm, 0

[0x80001854]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001858]:csrrs a7, fflags, zero
[0x8000185c]:fsd ft11, 928(a5)
[0x80001860]:sw a7, 932(a5)
[0x80001864]:fld ft10, 384(a6)
[0x80001868]:fld ft9, 392(a6)
[0x8000186c]:fld ft8, 400(a6)
[0x80001870]:csrrwi zero, frm, 0

[0x80001874]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001878]:csrrs a7, fflags, zero
[0x8000187c]:fsd ft11, 944(a5)
[0x80001880]:sw a7, 948(a5)
[0x80001884]:fld ft10, 408(a6)
[0x80001888]:fld ft9, 416(a6)
[0x8000188c]:fld ft8, 424(a6)
[0x80001890]:csrrwi zero, frm, 0

[0x80001894]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001898]:csrrs a7, fflags, zero
[0x8000189c]:fsd ft11, 960(a5)
[0x800018a0]:sw a7, 964(a5)
[0x800018a4]:fld ft10, 432(a6)
[0x800018a8]:fld ft9, 440(a6)
[0x800018ac]:fld ft8, 448(a6)
[0x800018b0]:csrrwi zero, frm, 0

[0x800018b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800018b8]:csrrs a7, fflags, zero
[0x800018bc]:fsd ft11, 976(a5)
[0x800018c0]:sw a7, 980(a5)
[0x800018c4]:fld ft10, 456(a6)
[0x800018c8]:fld ft9, 464(a6)
[0x800018cc]:fld ft8, 472(a6)
[0x800018d0]:csrrwi zero, frm, 0

[0x800018d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800018d8]:csrrs a7, fflags, zero
[0x800018dc]:fsd ft11, 992(a5)
[0x800018e0]:sw a7, 996(a5)
[0x800018e4]:fld ft10, 480(a6)
[0x800018e8]:fld ft9, 488(a6)
[0x800018ec]:fld ft8, 496(a6)
[0x800018f0]:csrrwi zero, frm, 0

[0x800018f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800018f8]:csrrs a7, fflags, zero
[0x800018fc]:fsd ft11, 1008(a5)
[0x80001900]:sw a7, 1012(a5)
[0x80001904]:fld ft10, 504(a6)
[0x80001908]:fld ft9, 512(a6)
[0x8000190c]:fld ft8, 520(a6)
[0x80001910]:csrrwi zero, frm, 0

[0x80001914]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001918]:csrrs a7, fflags, zero
[0x8000191c]:fsd ft11, 1024(a5)
[0x80001920]:sw a7, 1028(a5)
[0x80001924]:fld ft10, 528(a6)
[0x80001928]:fld ft9, 536(a6)
[0x8000192c]:fld ft8, 544(a6)
[0x80001930]:csrrwi zero, frm, 0

[0x80001934]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001938]:csrrs a7, fflags, zero
[0x8000193c]:fsd ft11, 1040(a5)
[0x80001940]:sw a7, 1044(a5)
[0x80001944]:fld ft10, 552(a6)
[0x80001948]:fld ft9, 560(a6)
[0x8000194c]:fld ft8, 568(a6)
[0x80001950]:csrrwi zero, frm, 0

[0x80001954]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001958]:csrrs a7, fflags, zero
[0x8000195c]:fsd ft11, 1056(a5)
[0x80001960]:sw a7, 1060(a5)
[0x80001964]:fld ft10, 576(a6)
[0x80001968]:fld ft9, 584(a6)
[0x8000196c]:fld ft8, 592(a6)
[0x80001970]:csrrwi zero, frm, 0

[0x80001974]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001978]:csrrs a7, fflags, zero
[0x8000197c]:fsd ft11, 1072(a5)
[0x80001980]:sw a7, 1076(a5)
[0x80001984]:fld ft10, 600(a6)
[0x80001988]:fld ft9, 608(a6)
[0x8000198c]:fld ft8, 616(a6)
[0x80001990]:csrrwi zero, frm, 0

[0x80001994]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001998]:csrrs a7, fflags, zero
[0x8000199c]:fsd ft11, 1088(a5)
[0x800019a0]:sw a7, 1092(a5)
[0x800019a4]:fld ft10, 624(a6)
[0x800019a8]:fld ft9, 632(a6)
[0x800019ac]:fld ft8, 640(a6)
[0x800019b0]:csrrwi zero, frm, 0

[0x800019b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800019b8]:csrrs a7, fflags, zero
[0x800019bc]:fsd ft11, 1104(a5)
[0x800019c0]:sw a7, 1108(a5)
[0x800019c4]:fld ft10, 648(a6)
[0x800019c8]:fld ft9, 656(a6)
[0x800019cc]:fld ft8, 664(a6)
[0x800019d0]:csrrwi zero, frm, 0

[0x800019d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800019d8]:csrrs a7, fflags, zero
[0x800019dc]:fsd ft11, 1120(a5)
[0x800019e0]:sw a7, 1124(a5)
[0x800019e4]:fld ft10, 672(a6)
[0x800019e8]:fld ft9, 680(a6)
[0x800019ec]:fld ft8, 688(a6)
[0x800019f0]:csrrwi zero, frm, 0

[0x800019f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800019f8]:csrrs a7, fflags, zero
[0x800019fc]:fsd ft11, 1136(a5)
[0x80001a00]:sw a7, 1140(a5)
[0x80001a04]:fld ft10, 696(a6)
[0x80001a08]:fld ft9, 704(a6)
[0x80001a0c]:fld ft8, 712(a6)
[0x80001a10]:csrrwi zero, frm, 0

[0x80001a14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001a18]:csrrs a7, fflags, zero
[0x80001a1c]:fsd ft11, 1152(a5)
[0x80001a20]:sw a7, 1156(a5)
[0x80001a24]:fld ft10, 720(a6)
[0x80001a28]:fld ft9, 728(a6)
[0x80001a2c]:fld ft8, 736(a6)
[0x80001a30]:csrrwi zero, frm, 0

[0x80001a34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001a38]:csrrs a7, fflags, zero
[0x80001a3c]:fsd ft11, 1168(a5)
[0x80001a40]:sw a7, 1172(a5)
[0x80001a44]:fld ft10, 744(a6)
[0x80001a48]:fld ft9, 752(a6)
[0x80001a4c]:fld ft8, 760(a6)
[0x80001a50]:csrrwi zero, frm, 0

[0x80001a54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001a58]:csrrs a7, fflags, zero
[0x80001a5c]:fsd ft11, 1184(a5)
[0x80001a60]:sw a7, 1188(a5)
[0x80001a64]:fld ft10, 768(a6)
[0x80001a68]:fld ft9, 776(a6)
[0x80001a6c]:fld ft8, 784(a6)
[0x80001a70]:csrrwi zero, frm, 0

[0x80001a74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001a78]:csrrs a7, fflags, zero
[0x80001a7c]:fsd ft11, 1200(a5)
[0x80001a80]:sw a7, 1204(a5)
[0x80001a84]:fld ft10, 792(a6)
[0x80001a88]:fld ft9, 800(a6)
[0x80001a8c]:fld ft8, 808(a6)
[0x80001a90]:csrrwi zero, frm, 0

[0x80001a94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001a98]:csrrs a7, fflags, zero
[0x80001a9c]:fsd ft11, 1216(a5)
[0x80001aa0]:sw a7, 1220(a5)
[0x80001aa4]:fld ft10, 816(a6)
[0x80001aa8]:fld ft9, 824(a6)
[0x80001aac]:fld ft8, 832(a6)
[0x80001ab0]:csrrwi zero, frm, 0

[0x80001ab4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ab8]:csrrs a7, fflags, zero
[0x80001abc]:fsd ft11, 1232(a5)
[0x80001ac0]:sw a7, 1236(a5)
[0x80001ac4]:fld ft10, 840(a6)
[0x80001ac8]:fld ft9, 848(a6)
[0x80001acc]:fld ft8, 856(a6)
[0x80001ad0]:csrrwi zero, frm, 0

[0x80001ad4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ad8]:csrrs a7, fflags, zero
[0x80001adc]:fsd ft11, 1248(a5)
[0x80001ae0]:sw a7, 1252(a5)
[0x80001ae4]:fld ft10, 864(a6)
[0x80001ae8]:fld ft9, 872(a6)
[0x80001aec]:fld ft8, 880(a6)
[0x80001af0]:csrrwi zero, frm, 0

[0x80001af4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001af8]:csrrs a7, fflags, zero
[0x80001afc]:fsd ft11, 1264(a5)
[0x80001b00]:sw a7, 1268(a5)
[0x80001b04]:fld ft10, 888(a6)
[0x80001b08]:fld ft9, 896(a6)
[0x80001b0c]:fld ft8, 904(a6)
[0x80001b10]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                        coverpoints                                                                                                                                                                         |                                                                              code                                                                               |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004514]<br>0x00000001|- opcode : fnmadd.d<br> - rs1 : f31<br> - rs2 : f31<br> - rs3 : f8<br> - rd : f31<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                         |[0x80000124]:fnmadd.d ft11, ft11, ft11, fs0, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd ft11, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>      |
|   2|[0x80004524]<br>0x00000001|- rs1 : f8<br> - rs2 : f11<br> - rs3 : f23<br> - rd : f11<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x5f6 and fm1 == 0x38aee2c19215f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x912bfdff44ba7 and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x0f8b22ffa1987 and rm_val == 0  #nosat<br>                               |[0x80000144]:fnmadd.d fa1, fs0, fa1, fs7, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fa1, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80004534]<br>0x00000001|- rs1 : f2<br> - rs2 : f17<br> - rs3 : f17<br> - rd : f17<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                 |[0x80000164]:fnmadd.d fa7, ft2, fa7, fa7, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fa7, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80004544]<br>0x00000001|- rs1 : f29<br> - rs2 : f10<br> - rs3 : f29<br> - rd : f29<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                |[0x80000184]:fnmadd.d ft9, ft9, fa0, ft9, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd ft9, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>        |
|   5|[0x80004554]<br>0x00000001|- rs1 : f7<br> - rs2 : f29<br> - rs3 : f14<br> - rd : f20<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4038aec1813f9 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7c017850ccf9e and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x94a9ed2abf1d5 and rm_val == 0  #nosat<br> |[0x800001a4]:fnmadd.d fs4, ft7, ft9, fa4, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fs4, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>        |
|   6|[0x80004564]<br>0x00000001|- rs1 : f25<br> - rs2 : f25<br> - rs3 : f27<br> - rd : f3<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                    |[0x800001c4]:fnmadd.d ft3, fs9, fs9, fs11, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd ft3, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>       |
|   7|[0x80004574]<br>0x00000001|- rs1 : f16<br> - rs2 : f22<br> - rs3 : f6<br> - rd : f16<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x5f5 and fm1 == 0xf59904d0ce0bf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x1a7baab01ceb9 and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0x4ca65e448b1e4 and rm_val == 0  #nosat<br>                               |[0x800001e4]:fnmadd.d fa6, fa6, fs6, ft6, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa6, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80004584]<br>0x00000001|- rs1 : f1<br> - rs2 : f21<br> - rs3 : f18<br> - rd : f18<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x67fb4908ceaaf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x65eaa9e302952 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0xa149f8995d019 and rm_val == 0  #nosat<br>                               |[0x80000204]:fnmadd.d fs2, ft1, fs5, fs2, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fs2, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80004594]<br>0x00000001|- rs1 : f15<br> - rs2 : f15<br> - rs3 : f15<br> - rd : f22<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                |[0x80000224]:fnmadd.d fs6, fa5, fa5, fa5, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs6, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>      |
|  10|[0x800045a4]<br>0x00000001|- rs1 : f20<br> - rs2 : f4<br> - rs3 : f4<br> - rd : f6<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                      |[0x80000244]:fnmadd.d ft6, fs4, ft4, ft4, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd ft6, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>      |
|  11|[0x800045b4]<br>0x00000001|- rs1 : f26<br> - rs2 : f26<br> - rs3 : f26<br> - rd : f26<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                |[0x80000264]:fnmadd.d fs10, fs10, fs10, fs10, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fs10, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br> |
|  12|[0x800045c4]<br>0x00000001|- rs1 : f30<br> - rs2 : f14<br> - rs3 : f30<br> - rd : f10<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                   |[0x80000284]:fnmadd.d fa0, ft10, fa4, ft10, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fa0, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>    |
|  13|[0x800045d4]<br>0x00000001|- rs1 : f6<br> - rs2 : f12<br> - rs3 : f16<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x41176abd4258d and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xaeb9622c6891f and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x74099544c727f and rm_val == 0  #nosat<br>                                                                                          |[0x800002a4]:fnmadd.d fs7, ft6, fa2, fa6, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd fs7, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>      |
|  14|[0x800045e4]<br>0x00000001|- rs1 : f10<br> - rs2 : f9<br> - rs3 : f11<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x4dd13d82cf047 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xafe78faaa8367 and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0xffe3517f7bf18 and rm_val == 0  #nosat<br>                                                                                           |[0x800002c4]:fnmadd.d ft7, fa0, fs1, fa1, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd ft7, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>      |
|  15|[0x800045f4]<br>0x00000001|- rs1 : f24<br> - rs2 : f5<br> - rs3 : f3<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6f674621915da and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x03a0ff71fb0c2 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x652550f234063 and rm_val == 0  #nosat<br>                                                                                           |[0x800002e4]:fnmadd.d fs9, fs8, ft5, ft3, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fs9, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>      |
|  16|[0x80004604]<br>0x00000001|- rs1 : f18<br> - rs2 : f28<br> - rs3 : f20<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x3ed0f2697260f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb79b2b1934a01 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x049f916bf2560 and rm_val == 0  #nosat<br>                                                                                          |[0x80000304]:fnmadd.d ft2, fs2, ft8, fs4, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd ft2, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>      |
|  17|[0x80004614]<br>0x00000001|- rs1 : f13<br> - rs2 : f2<br> - rs3 : f24<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x5f6 and fm1 == 0xb3756a76d237f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xb8c3b0a05a317 and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0x027fcc8b591c8 and rm_val == 0  #nosat<br>                                                                                          |[0x80000324]:fnmadd.d fs5, fa3, ft2, fs8, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd fs5, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>      |
|  18|[0x80004624]<br>0x00000001|- rs1 : f11<br> - rs2 : f7<br> - rs3 : f25<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x5f7 and fm1 == 0xdd44967e4f77f and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xbb5518eec7ff7 and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0xf4c7e1737c8fc and rm_val == 0  #nosat<br>                                                                                           |[0x80000344]:fnmadd.d ft5, fa1, ft7, fs9, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd ft5, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>      |
|  19|[0x80004634]<br>0x00000001|- rs1 : f3<br> - rs2 : f13<br> - rs3 : f22<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x1ea6995f1c073 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xf7e9d38031687 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x3f4c03dfcba0c and rm_val == 0  #nosat<br>                                                                                          |[0x80000364]:fnmadd.d fa5, ft3, fa3, fs6, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd fa5, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>      |
|  20|[0x80004644]<br>0x00000001|- rs1 : f28<br> - rs2 : f16<br> - rs3 : f12<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0bb792159b051 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x5d1ae1e1d28a7 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x565b8c4137e0d and rm_val == 0  #nosat<br>                                                                                         |[0x80000384]:fnmadd.d fa4, ft8, fa6, fa2, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd fa4, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>      |
|  21|[0x80004654]<br>0x00000001|- rs1 : f9<br> - rs2 : f30<br> - rs3 : f0<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x98cb938bd0d9b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd64f454066002 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x5618eb3f6a8f3 and rm_val == 0  #nosat<br>                                                                                           |[0x800003a4]:fnmadd.d fs3, fs1, ft10, ft0, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fs3, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>     |
|  22|[0x80004664]<br>0x00000001|- rs1 : f27<br> - rs2 : f8<br> - rs3 : f9<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x1bebe7b21cd5f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x011af8e2b2a8d and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x2e5825f882ec1 and rm_val == 0  #nosat<br>                                                                                           |[0x800003c4]:fnmadd.d ft10, fs11, fs0, fs1, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd ft10, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>   |
|  23|[0x80004674]<br>0x00000001|- rs1 : f22<br> - rs2 : f20<br> - rs3 : f21<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xeea576108affb and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xed04e711db0e1 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x733a2576cd48d and rm_val == 0  #nosat<br>                                                                                         |[0x800003e4]:fnmadd.d ft8, fs6, fs4, fs5, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd ft8, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80004684]<br>0x00000001|- rs1 : f21<br> - rs2 : f27<br> - rs3 : f31<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x4c791addc9912 and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x97b02f6ed223f and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0xf6a95c1bb8ef7 and rm_val == 0  #nosat<br>                                                                                         |[0x80000404]:fnmadd.d fa3, fs5, fs11, ft11, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fa3, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>    |
|  25|[0x80004694]<br>0x00000001|- rs1 : f17<br> - rs2 : f6<br> - rs3 : f1<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x56eb5abeef1c8 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x78842cac09a97 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0xf12b1fbe2cab1 and rm_val == 0  #nosat<br>                                                                                            |[0x80000424]:fnmadd.d fs0, fa7, ft6, ft1, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd fs0, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>      |
|  26|[0x800046a4]<br>0x00000001|- rs1 : f5<br> - rs2 : f1<br> - rs3 : f7<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x670e7d1c3c471 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x62964c066279b and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x1d2f4ea5729a9 and rm_val == 0  #nosat<br>                                                                                            |[0x80000444]:fnmadd.d fs11, ft5, ft1, ft7, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs11, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>    |
|  27|[0x800046b4]<br>0x00000001|- rs1 : f12<br> - rs2 : f0<br> - rs3 : f19<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x9889fc2d44e5e and fs2 == 0 and fe2 == 0x5f4 and fm2 == 0x21d9040c119bf and fs3 == 1 and fe3 == 0x7ef and fm3 == 0xff9140e19d949 and rm_val == 0  #nosat<br>                                                                                           |[0x80000464]:fnmadd.d ft1, fa2, ft0, fs3, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd ft1, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x800046c4]<br>0x00000001|- rs1 : f19<br> - rs2 : f24<br> - rs3 : f5<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xd486b68b34be3 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x3cc532c905347 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x7a850c645ac99 and rm_val == 0  #nosat<br>                                                                                          |[0x80000484]:fnmadd.d fa2, fs3, fs8, ft5, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fa2, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x800046d4]<br>0x00000001|- rs1 : f14<br> - rs2 : f18<br> - rs3 : f28<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x06933c1e52e8b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdff0d9faf6b37 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x9d702d38e63e4 and rm_val == 0  #nosat<br>                                                                                         |[0x800004a4]:fnmadd.d fs8, fa4, fs2, ft8, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd fs8, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>      |
|  30|[0x800046e4]<br>0x00000001|- rs1 : f0<br> - rs2 : f23<br> - rs3 : f10<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x5f6 and fm1 == 0x7a1eff83f19af and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xbb68e4e714e57 and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x46b0821802661 and rm_val == 0  #nosat<br>                                                                                           |[0x800004c4]:fnmadd.d fs1, ft0, fs7, fa0, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd fs1, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>      |
|  31|[0x800046f4]<br>0x00000001|- rs1 : f23<br> - rs2 : f3<br> - rs3 : f2<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xd17c6c95aefed and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xa5ac383168515 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x24455c7728a26 and rm_val == 0  #nosat<br>                                                                                            |[0x800004e4]:fnmadd.d ft0, fs7, ft3, ft2, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd ft0, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>      |
|  32|[0x80004704]<br>0x00000001|- rs1 : f4<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xf849379fb9b6b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xfa550bd9aed1a and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0xcb0130986796b and rm_val == 0  #nosat<br>                                                                                                                                         |[0x80000504]:fnmadd.d fa4, ft4, fa1, fs9, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fa4, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>      |
|  33|[0x80004714]<br>0x00000001|- rs2 : f19<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x31f03f05cb87a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2d0e69e0aad85 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x14c78561793cb and rm_val == 0  #nosat<br>                                                                                                                                        |[0x80000524]:fnmadd.d fs8, fs9, fs3, fs6, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd fs8, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>      |
|  34|[0x80004724]<br>0x00000001|- rs3 : f13<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0x3d2d3af7c48ae and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x447163c5b6799 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x7157ca9ebcb4a and rm_val == 0  #nosat<br>                                                                                                                                        |[0x80000544]:fnmadd.d fs11, fs9, ft10, fa3, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd fs11, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>   |
|  35|[0x80004734]<br>0x00000001|- rd : f4<br> - fs1 == 0 and fe1 == 0x5fa and fm1 == 0xfea4d203770af and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x91b7d2621f217 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x273d98180e11c and rm_val == 0  #nosat<br>                                                                                                                                          |[0x80000564]:fnmadd.d ft4, fa0, ft8, fa5, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft4, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>      |
|  36|[0x80004744]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x894eb52d7a53a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x3883363d45ce5 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x4ec9f86f5cd83 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000584]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80004754]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0bc55b64ea25c and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb0d4dc0773572 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x2ee34f1961391 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80004764]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xb8178fb8c7f25 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x8ebf551167019 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x844270f80e92d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80004774]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f6 and fm1 == 0xf959d372fdf5f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x26ffb22d6ee1f and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0x0d733a9338e84 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800005e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80004784]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x588877f85511b and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x7dbc6852f0d29 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xc61c354c7f4da and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000604]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80004794]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xf8cb3cb5140d7 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xebb37bba609a7 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x866521dedcf77 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000624]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x800047a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xae7cb22e21faf and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x002e328b68fcd and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x21358514700b7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000644]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x800047b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x2339bac8ac55f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xa27ff454d2247 and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x29c95c6687c75 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000664]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x800047c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x43f16419df5f9 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xda33f11fe6a09 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x488b8623cffd3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000684]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x800047d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x237c293c04d53 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x468e8279a5120 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xabcf67ab47356 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x800047e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xe4e789d337d77 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xaac467660ea9f and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x18e38211353ef and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x800047f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x85eb50a9b65e8 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xbee6f48112ca9 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x06857d17f9e6d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800006e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80004804]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x6fcb8489dc591 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd5d579f837c3e and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x3dcb317858039 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000704]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80004814]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x0435cbf7ce303 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0xcb3c58d58b2bf and fs3 == 1 and fe3 == 0x7ef and fm3 == 0x3b439dc268d31 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000724]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80004824]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xc9815c3b1adfc and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x7dd98b509becf and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x4e7f68bcdab8b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000744]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80004834]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xea594439af755 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x6a7fe63869fed and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x00525eda1d5a7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000764]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80004844]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x7840b0a77fbbb and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x94a97c3df3b51 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x5d58bc2e4fd7f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000784]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80004854]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x2a9ac14416973 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x3eb682e8c47fa and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x368985dc4dee6 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80004864]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xdf0325e8750cf and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x81cf3ffe0cb08 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x652cb8a3f1229 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80004874]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x7c30cfd9902ca and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x8083feedaf3d3 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x0ade42dab32c4 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800007e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80004884]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x01559da52cc50 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x61457deedafab and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x5a04d43f6744e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000804]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80004894]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x5e77a2a3ef6e5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc1d568ee159cf and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x28828379b625d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000824]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x800048a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x440c579831418 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x99ddc9e77cb45 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0xd908ada8b9c0d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000844]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x800048b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x97170988aa151 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xdd895344c54d7 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x575700b04d303 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000864]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x800048c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x5b608176286de and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x7ec25e17b909b and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x602a93732cb46 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000884]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x800048d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x97c657c682e1f and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xe719a478092fb and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0xcd7a17c66ca93 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x800048e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x477c4d4dd15f5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdbfa105179648 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x93c3240340b2a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x800048f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xca057fc89126a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd7eea06dfbab7 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x3063b52602e9f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800008e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80004904]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x54fc797f61a47 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xc49dffef48af1 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x85743659b13a8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000904]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80004914]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x46086cad941b7 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x3cf8a4ab5f917 and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x26291110ad9f3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000924]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80004924]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xa321af726492d and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x8450b36da4f99 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0xb1c82278f55e9 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000944]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80004934]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x1c256e07d7b03 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xea5ed10729949 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x2e9cf17e1c104 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000964]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80004944]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xdd01ebf317c47 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xd484e5c7d8c61 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x0ce829748d56f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000984]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80004954]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x9881bee04c84c and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x41f756c5b46c8 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x5006cf149aed7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80004964]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x908348cc50075 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x0f993cf648277 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x893f7fe0be35f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80004974]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xae70dafae96a3 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x7cb7e10454cbf and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x05638ab21778b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800009e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80004984]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x97787813faa38 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x4e8baea923265 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x72dedcdfdeaae and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80004994]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xa613e194097b8 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x377828aab6e99 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0xa6b75a2599da0 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800049a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x13bf56ad82c8a and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd453f7d35f923 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x0f938d0012b4a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800049b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x3bd530bfc7921 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x4c524788895e4 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x302ba08bafbad and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800049c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x87814b483b2ff and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xc56d7d1a2a465 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x444426e72046b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000a84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800049d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xef0f52001dd13 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x774135045aad3 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x1705e64f208a1 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000aa4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800049e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x2de4ff93f2d49 and fs2 == 0 and fe2 == 0x5f4 and fm2 == 0x54cb8485c10ff and fs3 == 1 and fe3 == 0x7ef and fm3 == 0x38f4ff02f8ed1 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ac4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800049f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x4cf1937fde173 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x86d2c67f038bd and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x743c2409eb867 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ae4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80004a04]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x932903b557086 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xba615dee0d545 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x8b52241b8e024 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80004a14]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x9199ba7fdacbd and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x9e32e020ad6fd and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x10179abe8eb1b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80004a24]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x820702d63fac0 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x63bca2c276bab and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xd57035175d798 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80004a34]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x017c339d75e4d and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0xc30407e58dcff and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0xe9d605ec1e199 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80004a44]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x597fb1bb06230 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x56ed923aca873 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x252e9c09634b1 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000b84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80004a54]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x511a1344303ed and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x82640d65dc24c and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x79c8ee5480d1b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ba4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80004a64]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f2 and fm1 == 0x8e1a79f69deff and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xf112c2c43eca3 and fs3 == 1 and fe3 == 0x7ee and fm3 == 0x6308e242db948 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000bc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80004a74]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0xa3d5b9f8ee473 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xeefde430a673b and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xb3727ee3c0285 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000be8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80004a84]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x03c79a41b870f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x21d7278b1bb7f and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xaf7fd02c04bab and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80004a94]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x1a5d3a022c06b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x39827e2b6fc0e and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0xf00286babe74d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x80004aa4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x2c2600e5225e4 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x882e3a7d63c53 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x9ecbb9e89d747 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x80004ab4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x88b452334d482 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x98a767463fb7b and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x115b37a3f916a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x80004ac4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xc4f4bdbe3ee53 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x9b7932c7ac007 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x4a76956747dfc and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000c88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x80004ad4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x757c41e46ee0f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x367fd258b0b63 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0xaf8d402ed4d82 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ca8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x80004ae4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xa1d3ea060b239 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0x18c773392efff and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0xbd433bf0d5f95 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000cc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x80004af4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6c53c0ba0796d and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc43588ddd7fbc and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x097bba91bea78 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000ce8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80004b04]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x45088c7a8eddc and fs2 == 0 and fe2 == 0x5f4 and fm2 == 0x7c4c7501c707f and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x6194442f94d73 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80004b14]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x9c63a6687c333 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x557d56987bca8 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x0febcf30a99f7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80004b24]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xb442f12e7354a and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xcccc36886926f and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x3e473b72c36be and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80004b34]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x7dc0f47a5db15 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x5124f30535e0b and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0xbad4e8fb1eecb and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80004b44]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xe3d6d32e17fa5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xec884da30b843 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x35b03f15411b2 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000d88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80004b54]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6bc16c6eccc22 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xdf93331a60977 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x22879c38e45b3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000da8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80004b64]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x053533036dba9 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xee098e2310cc3 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x6219989c7d7f0 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000dc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80004b74]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x30b95bd887309 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xc37d95ef26f70 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x3d316c619258e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000de8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80004b84]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x287a907776fa3 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xc3c58b5c03e1d and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0x0d4a450d85abf and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80000e08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x8000490c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xdedbc42e4ee38 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x398aa070366df and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x0152356c26a6b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001110]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 106|[0x8000491c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x2e3db402ba61f and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x232f190317157 and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0xd7fa045f8793f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001130]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 107|[0x8000492c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xf1860e3b4eb81 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xaa7d58e3b9047 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x49366e3f9dbbd and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001150]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 108|[0x8000493c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0x0197267f1985f and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xce41f387adc6f and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x4812b6bf8d6c3 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001170]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 109|[0x8000494c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x3943b61964e33 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xae64a7b19f21e and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0xb871d605df6ba and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001190]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 110|[0x8000495c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0x3832e6fea9a3f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x5ee10a5a625fc and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0xd731b74a534ea and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 111|[0x8000496c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x2887dc585eda5 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xf79012fbad378 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x9033b38fd9d06 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 112|[0x8000497c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x09d5da3d7b9db and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xd7c8570796fe8 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x8a804749eca98 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800011f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 113|[0x8000498c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f7 and fm1 == 0x802693539b05f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x9af59f9eb5168 and fs3 == 1 and fe3 == 0x7f3 and fm3 == 0xa53ffa5a270dc and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001210]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 114|[0x8000499c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xac0c7cf6e58fb and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x0cd5422534b0d and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x097bf15fcaf48 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001230]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 115|[0x800049ac]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x05e381015d598 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2982d565d88fc and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x3ff2ba26b011d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001250]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 116|[0x800049bc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xed1da04d72f12 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7536733396cf8 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x01ad09ae1295b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001270]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 117|[0x800049cc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x48949a9851f6d and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xb318d9af479ef and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x0a2ea67ab4e6e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001290]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 118|[0x800049dc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x0a9df4ead8eb3 and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xdb8c7d3a18027 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x4455b0e11d60e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 119|[0x800049ec]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x743ea0c02659b and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xda2a011aeffab and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0xa3aa8637b0e8c and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 120|[0x800049fc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x7291f0459edd6 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xed4ae61a16dab and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x88a6f73f25521 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800012f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 121|[0x80004a0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f6 and fm1 == 0x22cfa989fca0f and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0xa2892d94829ad and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0x08274724f19ab and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001310]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 122|[0x80004a1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xb12b5923ada87 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x0d68c4b00b1ad and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xca0ee02ad1bc6 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001330]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 123|[0x80004a2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x99b4caa7ee21f and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x6b435c9707703 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x6d474fad8ee6d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001350]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 124|[0x80004a3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x00ccac0a4b811 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x2eb6a94e9ae19 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xed311f5a05e8b and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001370]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 125|[0x80004a4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xfd32694fcaecb and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xdc1b3eb6c004b and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x560832458212e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001390]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 126|[0x80004a5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x043a8c3aa6439 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xf788de2d51675 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0xeefe93994c491 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 127|[0x80004a6c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x4a10e22bb3b33 and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0x88b104e822b8f and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0x9dec6c0993cbd and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 128|[0x80004a7c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x1fe2d6aba9e77 and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0xd126610309c1f and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x0e51db551c107 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800013f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 129|[0x80004a8c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xadd465d39fd03 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0x2774cd9885b7f and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x826e9daaa7e48 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001410]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 130|[0x80004a9c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xeb61e2d5d3c7a and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x095092a183e33 and fs3 == 1 and fe3 == 0x7f5 and fm3 == 0x98f4fb01003a1 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001430]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 131|[0x80004aac]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x9262273e53737 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x07943814fd4f4 and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0xd9e255bfde29e and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001450]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 132|[0x80004abc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xbfe0f0fcad936 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x0531d684ae65b and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x6a22dab2e89da and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001470]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 133|[0x80004acc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x43e5b36b50bcd and fs2 == 0 and fe2 == 0x5f7 and fm2 == 0xa1fe3e0c64717 and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x7ed8026cc0461 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001490]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 134|[0x80004adc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0xe4204ffab96f7 and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0xcde16617ec93f and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x6aabd19d990c4 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 135|[0x80004aec]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x82e62659b7f9b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0xb9927e27c836d and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x1c84f08b614c7 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 136|[0x80004afc]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x882d3626badfd and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7ada4a02edef8 and fs3 == 1 and fe3 == 0x7f7 and fm3 == 0x13f930a295467 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800014f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 137|[0x80004b0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x594e913e8fb89 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0xf8ce1a7792dff and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0xed33518b28b93 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001510]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 138|[0x80004b1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x3b00ab682d289 and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x7c23aaefd9f67 and fs3 == 1 and fe3 == 0x7f6 and fm3 == 0x0454e8106d5cf and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001530]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 139|[0x80004b2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x361bbef8877ab and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x291d98044bfbf and fs3 == 1 and fe3 == 0x7ef and fm3 == 0xe3cc0b403a893 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001550]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 140|[0x80004b3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f9 and fm1 == 0x8ad1c84b735e1 and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x61932b2d37baf and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x0f4a2e0ecd21d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001570]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 141|[0x80004b4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x41c73bbc1b00b and fs2 == 0 and fe2 == 0x5f8 and fm2 == 0x784c0d85e9517 and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x9006b360bbb6d and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001590]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 142|[0x80004b5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f5 and fm1 == 0x5a7002fc1a6bf and fs2 == 0 and fe2 == 0x5f9 and fm2 == 0x05d39d105b541 and fs3 == 1 and fe3 == 0x7f0 and fm3 == 0x4141940cdbe5f and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 143|[0x80004b6c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0xe8bc5f44515f5 and fs2 == 0 and fe2 == 0x5f5 and fm2 == 0x9f3f7053b60bf and fs3 == 1 and fe3 == 0x7f2 and fm3 == 0x56c11a9ac3ca8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 144|[0x80004b7c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5f8 and fm1 == 0x6003243fdf57b and fs2 == 0 and fe2 == 0x5fa and fm2 == 0x5874b6418015b and fs3 == 1 and fe3 == 0x7f4 and fm3 == 0x007478669dfb8 and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x800015f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 145|[0x80004b8c]<br>0x00000001|- fs1 == 0 and fe1 == 0x5fa and fm1 == 0x6f1586709a287 and fs2 == 0 and fe2 == 0x5f6 and fm2 == 0x051aac3a28d5f and fs3 == 1 and fe3 == 0x7f1 and fm3 == 0xfe1215b38304a and rm_val == 0  #nosat<br>                                                                                                                                                        |[0x80001610]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
