
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800024b0')]      |
| SIG_REGION                | [('0x80005c10', '0x800064f0', '568 words')]      |
| COV_LABELS                | fnmadd_b2      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fnmadd1/riscof_work/fnmadd_b2-01.S/ref.S    |
| Total Number of coverpoints| 424     |
| Total Coverpoints Hit     | 354      |
| Total Signature Updates   | 221      |
| STAT1                     | 221      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001af4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001af8]:csrrs a7, fflags, zero
[0x80001afc]:fsd ft11, 1264(a5)
[0x80001b00]:sw a7, 1268(a5)
[0x80001b04]:fld ft10, 888(a6)
[0x80001b08]:fld ft9, 896(a6)
[0x80001b0c]:fld ft8, 904(a6)
[0x80001b10]:csrrwi zero, frm, 0

[0x80001b14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b18]:csrrs a7, fflags, zero
[0x80001b1c]:fsd ft11, 1280(a5)
[0x80001b20]:sw a7, 1284(a5)
[0x80001b24]:fld ft10, 912(a6)
[0x80001b28]:fld ft9, 920(a6)
[0x80001b2c]:fld ft8, 928(a6)
[0x80001b30]:csrrwi zero, frm, 0

[0x80001b34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1296(a5)
[0x80001b40]:sw a7, 1300(a5)
[0x80001b44]:fld ft10, 936(a6)
[0x80001b48]:fld ft9, 944(a6)
[0x80001b4c]:fld ft8, 952(a6)
[0x80001b50]:csrrwi zero, frm, 0

[0x80001b54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b58]:csrrs a7, fflags, zero
[0x80001b5c]:fsd ft11, 1312(a5)
[0x80001b60]:sw a7, 1316(a5)
[0x80001b64]:fld ft10, 960(a6)
[0x80001b68]:fld ft9, 968(a6)
[0x80001b6c]:fld ft8, 976(a6)
[0x80001b70]:csrrwi zero, frm, 0

[0x80001b74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b78]:csrrs a7, fflags, zero
[0x80001b7c]:fsd ft11, 1328(a5)
[0x80001b80]:sw a7, 1332(a5)
[0x80001b84]:fld ft10, 984(a6)
[0x80001b88]:fld ft9, 992(a6)
[0x80001b8c]:fld ft8, 1000(a6)
[0x80001b90]:csrrwi zero, frm, 0

[0x80001b94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b98]:csrrs a7, fflags, zero
[0x80001b9c]:fsd ft11, 1344(a5)
[0x80001ba0]:sw a7, 1348(a5)
[0x80001ba4]:fld ft10, 1008(a6)
[0x80001ba8]:fld ft9, 1016(a6)
[0x80001bac]:fld ft8, 1024(a6)
[0x80001bb0]:csrrwi zero, frm, 0

[0x80001bb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bb8]:csrrs a7, fflags, zero
[0x80001bbc]:fsd ft11, 1360(a5)
[0x80001bc0]:sw a7, 1364(a5)
[0x80001bc4]:fld ft10, 1032(a6)
[0x80001bc8]:fld ft9, 1040(a6)
[0x80001bcc]:fld ft8, 1048(a6)
[0x80001bd0]:csrrwi zero, frm, 0

[0x80001bd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bd8]:csrrs a7, fflags, zero
[0x80001bdc]:fsd ft11, 1376(a5)
[0x80001be0]:sw a7, 1380(a5)
[0x80001be4]:fld ft10, 1056(a6)
[0x80001be8]:fld ft9, 1064(a6)
[0x80001bec]:fld ft8, 1072(a6)
[0x80001bf0]:csrrwi zero, frm, 0

[0x80001bf4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bf8]:csrrs a7, fflags, zero
[0x80001bfc]:fsd ft11, 1392(a5)
[0x80001c00]:sw a7, 1396(a5)
[0x80001c04]:fld ft10, 1080(a6)
[0x80001c08]:fld ft9, 1088(a6)
[0x80001c0c]:fld ft8, 1096(a6)
[0x80001c10]:csrrwi zero, frm, 0

[0x80001c14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1408(a5)
[0x80001c20]:sw a7, 1412(a5)
[0x80001c24]:fld ft10, 1104(a6)
[0x80001c28]:fld ft9, 1112(a6)
[0x80001c2c]:fld ft8, 1120(a6)
[0x80001c30]:csrrwi zero, frm, 0

[0x80001c34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c38]:csrrs a7, fflags, zero
[0x80001c3c]:fsd ft11, 1424(a5)
[0x80001c40]:sw a7, 1428(a5)
[0x80001c44]:fld ft10, 1128(a6)
[0x80001c48]:fld ft9, 1136(a6)
[0x80001c4c]:fld ft8, 1144(a6)
[0x80001c50]:csrrwi zero, frm, 0

[0x80001c54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c58]:csrrs a7, fflags, zero
[0x80001c5c]:fsd ft11, 1440(a5)
[0x80001c60]:sw a7, 1444(a5)
[0x80001c64]:fld ft10, 1152(a6)
[0x80001c68]:fld ft9, 1160(a6)
[0x80001c6c]:fld ft8, 1168(a6)
[0x80001c70]:csrrwi zero, frm, 0

[0x80001c74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c78]:csrrs a7, fflags, zero
[0x80001c7c]:fsd ft11, 1456(a5)
[0x80001c80]:sw a7, 1460(a5)
[0x80001c84]:fld ft10, 1176(a6)
[0x80001c88]:fld ft9, 1184(a6)
[0x80001c8c]:fld ft8, 1192(a6)
[0x80001c90]:csrrwi zero, frm, 0

[0x80001c94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c98]:csrrs a7, fflags, zero
[0x80001c9c]:fsd ft11, 1472(a5)
[0x80001ca0]:sw a7, 1476(a5)
[0x80001ca4]:fld ft10, 1200(a6)
[0x80001ca8]:fld ft9, 1208(a6)
[0x80001cac]:fld ft8, 1216(a6)
[0x80001cb0]:csrrwi zero, frm, 0

[0x80001cb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cb8]:csrrs a7, fflags, zero
[0x80001cbc]:fsd ft11, 1488(a5)
[0x80001cc0]:sw a7, 1492(a5)
[0x80001cc4]:fld ft10, 1224(a6)
[0x80001cc8]:fld ft9, 1232(a6)
[0x80001ccc]:fld ft8, 1240(a6)
[0x80001cd0]:csrrwi zero, frm, 0

[0x80001cd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cd8]:csrrs a7, fflags, zero
[0x80001cdc]:fsd ft11, 1504(a5)
[0x80001ce0]:sw a7, 1508(a5)
[0x80001ce4]:fld ft10, 1248(a6)
[0x80001ce8]:fld ft9, 1256(a6)
[0x80001cec]:fld ft8, 1264(a6)
[0x80001cf0]:csrrwi zero, frm, 0

[0x80001cf4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cf8]:csrrs a7, fflags, zero
[0x80001cfc]:fsd ft11, 1520(a5)
[0x80001d00]:sw a7, 1524(a5)
[0x80001d04]:fld ft10, 1272(a6)
[0x80001d08]:fld ft9, 1280(a6)
[0x80001d0c]:fld ft8, 1288(a6)
[0x80001d10]:csrrwi zero, frm, 0

[0x80001d14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d18]:csrrs a7, fflags, zero
[0x80001d1c]:fsd ft11, 1536(a5)
[0x80001d20]:sw a7, 1540(a5)
[0x80001d24]:fld ft10, 1296(a6)
[0x80001d28]:fld ft9, 1304(a6)
[0x80001d2c]:fld ft8, 1312(a6)
[0x80001d30]:csrrwi zero, frm, 0

[0x80001d34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d38]:csrrs a7, fflags, zero
[0x80001d3c]:fsd ft11, 1552(a5)
[0x80001d40]:sw a7, 1556(a5)
[0x80001d44]:fld ft10, 1320(a6)
[0x80001d48]:fld ft9, 1328(a6)
[0x80001d4c]:fld ft8, 1336(a6)
[0x80001d50]:csrrwi zero, frm, 0

[0x80001d54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d58]:csrrs a7, fflags, zero
[0x80001d5c]:fsd ft11, 1568(a5)
[0x80001d60]:sw a7, 1572(a5)
[0x80001d64]:fld ft10, 1344(a6)
[0x80001d68]:fld ft9, 1352(a6)
[0x80001d6c]:fld ft8, 1360(a6)
[0x80001d70]:csrrwi zero, frm, 0

[0x80001d74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d78]:csrrs a7, fflags, zero
[0x80001d7c]:fsd ft11, 1584(a5)
[0x80001d80]:sw a7, 1588(a5)
[0x80001d84]:fld ft10, 1368(a6)
[0x80001d88]:fld ft9, 1376(a6)
[0x80001d8c]:fld ft8, 1384(a6)
[0x80001d90]:csrrwi zero, frm, 0

[0x80001d94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d98]:csrrs a7, fflags, zero
[0x80001d9c]:fsd ft11, 1600(a5)
[0x80001da0]:sw a7, 1604(a5)
[0x80001da4]:fld ft10, 1392(a6)
[0x80001da8]:fld ft9, 1400(a6)
[0x80001dac]:fld ft8, 1408(a6)
[0x80001db0]:csrrwi zero, frm, 0

[0x80001db4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001db8]:csrrs a7, fflags, zero
[0x80001dbc]:fsd ft11, 1616(a5)
[0x80001dc0]:sw a7, 1620(a5)
[0x80001dc4]:fld ft10, 1416(a6)
[0x80001dc8]:fld ft9, 1424(a6)
[0x80001dcc]:fld ft8, 1432(a6)
[0x80001dd0]:csrrwi zero, frm, 0

[0x80001dd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001dd8]:csrrs a7, fflags, zero
[0x80001ddc]:fsd ft11, 1632(a5)
[0x80001de0]:sw a7, 1636(a5)
[0x80001de4]:fld ft10, 1440(a6)
[0x80001de8]:fld ft9, 1448(a6)
[0x80001dec]:fld ft8, 1456(a6)
[0x80001df0]:csrrwi zero, frm, 0

[0x80001df4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001df8]:csrrs a7, fflags, zero
[0x80001dfc]:fsd ft11, 1648(a5)
[0x80001e00]:sw a7, 1652(a5)
[0x80001e04]:fld ft10, 1464(a6)
[0x80001e08]:fld ft9, 1472(a6)
[0x80001e0c]:fld ft8, 1480(a6)
[0x80001e10]:csrrwi zero, frm, 0

[0x80001e14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e18]:csrrs a7, fflags, zero
[0x80001e1c]:fsd ft11, 1664(a5)
[0x80001e20]:sw a7, 1668(a5)
[0x80001e24]:fld ft10, 1488(a6)
[0x80001e28]:fld ft9, 1496(a6)
[0x80001e2c]:fld ft8, 1504(a6)
[0x80001e30]:csrrwi zero, frm, 0

[0x80001e34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e38]:csrrs a7, fflags, zero
[0x80001e3c]:fsd ft11, 1680(a5)
[0x80001e40]:sw a7, 1684(a5)
[0x80001e44]:fld ft10, 1512(a6)
[0x80001e48]:fld ft9, 1520(a6)
[0x80001e4c]:fld ft8, 1528(a6)
[0x80001e50]:csrrwi zero, frm, 0

[0x80001e54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e58]:csrrs a7, fflags, zero
[0x80001e5c]:fsd ft11, 1696(a5)
[0x80001e60]:sw a7, 1700(a5)
[0x80001e64]:fld ft10, 1536(a6)
[0x80001e68]:fld ft9, 1544(a6)
[0x80001e6c]:fld ft8, 1552(a6)
[0x80001e70]:csrrwi zero, frm, 0

[0x80001e74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e78]:csrrs a7, fflags, zero
[0x80001e7c]:fsd ft11, 1712(a5)
[0x80001e80]:sw a7, 1716(a5)
[0x80001e84]:fld ft10, 1560(a6)
[0x80001e88]:fld ft9, 1568(a6)
[0x80001e8c]:fld ft8, 1576(a6)
[0x80001e90]:csrrwi zero, frm, 0

[0x80001e94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e98]:csrrs a7, fflags, zero
[0x80001e9c]:fsd ft11, 1728(a5)
[0x80001ea0]:sw a7, 1732(a5)
[0x80001ea4]:fld ft10, 1584(a6)
[0x80001ea8]:fld ft9, 1592(a6)
[0x80001eac]:fld ft8, 1600(a6)
[0x80001eb0]:csrrwi zero, frm, 0

[0x80001eb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001eb8]:csrrs a7, fflags, zero
[0x80001ebc]:fsd ft11, 1744(a5)
[0x80001ec0]:sw a7, 1748(a5)
[0x80001ec4]:fld ft10, 1608(a6)
[0x80001ec8]:fld ft9, 1616(a6)
[0x80001ecc]:fld ft8, 1624(a6)
[0x80001ed0]:csrrwi zero, frm, 0

[0x80001ed4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ed8]:csrrs a7, fflags, zero
[0x80001edc]:fsd ft11, 1760(a5)
[0x80001ee0]:sw a7, 1764(a5)
[0x80001ee4]:fld ft10, 1632(a6)
[0x80001ee8]:fld ft9, 1640(a6)
[0x80001eec]:fld ft8, 1648(a6)
[0x80001ef0]:csrrwi zero, frm, 0

[0x80001ef4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ef8]:csrrs a7, fflags, zero
[0x80001efc]:fsd ft11, 1776(a5)
[0x80001f00]:sw a7, 1780(a5)
[0x80001f04]:fld ft10, 1656(a6)
[0x80001f08]:fld ft9, 1664(a6)
[0x80001f0c]:fld ft8, 1672(a6)
[0x80001f10]:csrrwi zero, frm, 0

[0x80001f14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f18]:csrrs a7, fflags, zero
[0x80001f1c]:fsd ft11, 1792(a5)
[0x80001f20]:sw a7, 1796(a5)
[0x80001f24]:fld ft10, 1680(a6)
[0x80001f28]:fld ft9, 1688(a6)
[0x80001f2c]:fld ft8, 1696(a6)
[0x80001f30]:csrrwi zero, frm, 0

[0x80001f34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f38]:csrrs a7, fflags, zero
[0x80001f3c]:fsd ft11, 1808(a5)
[0x80001f40]:sw a7, 1812(a5)
[0x80001f44]:fld ft10, 1704(a6)
[0x80001f48]:fld ft9, 1712(a6)
[0x80001f4c]:fld ft8, 1720(a6)
[0x80001f50]:csrrwi zero, frm, 0

[0x80001f54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f58]:csrrs a7, fflags, zero
[0x80001f5c]:fsd ft11, 1824(a5)
[0x80001f60]:sw a7, 1828(a5)
[0x80001f64]:fld ft10, 1728(a6)
[0x80001f68]:fld ft9, 1736(a6)
[0x80001f6c]:fld ft8, 1744(a6)
[0x80001f70]:csrrwi zero, frm, 0

[0x80001f74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f78]:csrrs a7, fflags, zero
[0x80001f7c]:fsd ft11, 1840(a5)
[0x80001f80]:sw a7, 1844(a5)
[0x80001f84]:fld ft10, 1752(a6)
[0x80001f88]:fld ft9, 1760(a6)
[0x80001f8c]:fld ft8, 1768(a6)
[0x80001f90]:csrrwi zero, frm, 0

[0x80001f94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f98]:csrrs a7, fflags, zero
[0x80001f9c]:fsd ft11, 1856(a5)
[0x80001fa0]:sw a7, 1860(a5)
[0x80001fa4]:fld ft10, 1776(a6)
[0x80001fa8]:fld ft9, 1784(a6)
[0x80001fac]:fld ft8, 1792(a6)
[0x80001fb0]:csrrwi zero, frm, 0

[0x80001fb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fb8]:csrrs a7, fflags, zero
[0x80001fbc]:fsd ft11, 1872(a5)
[0x80001fc0]:sw a7, 1876(a5)
[0x80001fc4]:fld ft10, 1800(a6)
[0x80001fc8]:fld ft9, 1808(a6)
[0x80001fcc]:fld ft8, 1816(a6)
[0x80001fd0]:csrrwi zero, frm, 0

[0x80001fd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fd8]:csrrs a7, fflags, zero
[0x80001fdc]:fsd ft11, 1888(a5)
[0x80001fe0]:sw a7, 1892(a5)
[0x80001fe4]:fld ft10, 1824(a6)
[0x80001fe8]:fld ft9, 1832(a6)
[0x80001fec]:fld ft8, 1840(a6)
[0x80001ff0]:csrrwi zero, frm, 0

[0x80001ff4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ff8]:csrrs a7, fflags, zero
[0x80001ffc]:fsd ft11, 1904(a5)
[0x80002000]:sw a7, 1908(a5)
[0x80002004]:fld ft10, 1848(a6)
[0x80002008]:fld ft9, 1856(a6)
[0x8000200c]:fld ft8, 1864(a6)
[0x80002010]:csrrwi zero, frm, 0

[0x80002014]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002018]:csrrs a7, fflags, zero
[0x8000201c]:fsd ft11, 1920(a5)
[0x80002020]:sw a7, 1924(a5)
[0x80002024]:fld ft10, 1872(a6)
[0x80002028]:fld ft9, 1880(a6)
[0x8000202c]:fld ft8, 1888(a6)
[0x80002030]:csrrwi zero, frm, 0

[0x80002034]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002038]:csrrs a7, fflags, zero
[0x8000203c]:fsd ft11, 1936(a5)
[0x80002040]:sw a7, 1940(a5)
[0x80002044]:fld ft10, 1896(a6)
[0x80002048]:fld ft9, 1904(a6)
[0x8000204c]:fld ft8, 1912(a6)
[0x80002050]:csrrwi zero, frm, 0

[0x80002054]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002058]:csrrs a7, fflags, zero
[0x8000205c]:fsd ft11, 1952(a5)
[0x80002060]:sw a7, 1956(a5)
[0x80002064]:fld ft10, 1920(a6)
[0x80002068]:fld ft9, 1928(a6)
[0x8000206c]:fld ft8, 1936(a6)
[0x80002070]:csrrwi zero, frm, 0

[0x80002074]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002078]:csrrs a7, fflags, zero
[0x8000207c]:fsd ft11, 1968(a5)
[0x80002080]:sw a7, 1972(a5)
[0x80002084]:fld ft10, 1944(a6)
[0x80002088]:fld ft9, 1952(a6)
[0x8000208c]:fld ft8, 1960(a6)
[0x80002090]:csrrwi zero, frm, 0

[0x80002094]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002098]:csrrs a7, fflags, zero
[0x8000209c]:fsd ft11, 1984(a5)
[0x800020a0]:sw a7, 1988(a5)
[0x800020a4]:fld ft10, 1968(a6)
[0x800020a8]:fld ft9, 1976(a6)
[0x800020ac]:fld ft8, 1984(a6)
[0x800020b0]:csrrwi zero, frm, 0

[0x800020b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020b8]:csrrs a7, fflags, zero
[0x800020bc]:fsd ft11, 2000(a5)
[0x800020c0]:sw a7, 2004(a5)
[0x800020c4]:fld ft10, 1992(a6)
[0x800020c8]:fld ft9, 2000(a6)
[0x800020cc]:fld ft8, 2008(a6)
[0x800020d0]:csrrwi zero, frm, 0

[0x800020d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020d8]:csrrs a7, fflags, zero
[0x800020dc]:fsd ft11, 2016(a5)
[0x800020e0]:sw a7, 2020(a5)
[0x800020e4]:auipc a5, 4
[0x800020e8]:addi a5, a5, 796
[0x800020ec]:fld ft10, 2016(a6)
[0x800020f0]:fld ft9, 2024(a6)
[0x800020f4]:fld ft8, 2032(a6)
[0x800020f8]:csrrwi zero, frm, 0

[0x800022e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800022e4]:csrrs a7, fflags, zero
[0x800022e8]:fsd ft11, 240(a5)
[0x800022ec]:sw a7, 244(a5)
[0x800022f0]:fld ft10, 360(a6)
[0x800022f4]:fld ft9, 368(a6)
[0x800022f8]:fld ft8, 376(a6)
[0x800022fc]:csrrwi zero, frm, 0

[0x80002300]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002304]:csrrs a7, fflags, zero
[0x80002308]:fsd ft11, 256(a5)
[0x8000230c]:sw a7, 260(a5)
[0x80002310]:fld ft10, 384(a6)
[0x80002314]:fld ft9, 392(a6)
[0x80002318]:fld ft8, 400(a6)
[0x8000231c]:csrrwi zero, frm, 0

[0x80002320]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002324]:csrrs a7, fflags, zero
[0x80002328]:fsd ft11, 272(a5)
[0x8000232c]:sw a7, 276(a5)
[0x80002330]:fld ft10, 408(a6)
[0x80002334]:fld ft9, 416(a6)
[0x80002338]:fld ft8, 424(a6)
[0x8000233c]:csrrwi zero, frm, 0

[0x80002340]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002344]:csrrs a7, fflags, zero
[0x80002348]:fsd ft11, 288(a5)
[0x8000234c]:sw a7, 292(a5)
[0x80002350]:fld ft10, 432(a6)
[0x80002354]:fld ft9, 440(a6)
[0x80002358]:fld ft8, 448(a6)
[0x8000235c]:csrrwi zero, frm, 0

[0x80002360]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002364]:csrrs a7, fflags, zero
[0x80002368]:fsd ft11, 304(a5)
[0x8000236c]:sw a7, 308(a5)
[0x80002370]:fld ft10, 456(a6)
[0x80002374]:fld ft9, 464(a6)
[0x80002378]:fld ft8, 472(a6)
[0x8000237c]:csrrwi zero, frm, 0

[0x80002380]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002384]:csrrs a7, fflags, zero
[0x80002388]:fsd ft11, 320(a5)
[0x8000238c]:sw a7, 324(a5)
[0x80002390]:fld ft10, 480(a6)
[0x80002394]:fld ft9, 488(a6)
[0x80002398]:fld ft8, 496(a6)
[0x8000239c]:csrrwi zero, frm, 0

[0x800023a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023a4]:csrrs a7, fflags, zero
[0x800023a8]:fsd ft11, 336(a5)
[0x800023ac]:sw a7, 340(a5)
[0x800023b0]:fld ft10, 504(a6)
[0x800023b4]:fld ft9, 512(a6)
[0x800023b8]:fld ft8, 520(a6)
[0x800023bc]:csrrwi zero, frm, 0

[0x800023c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023c4]:csrrs a7, fflags, zero
[0x800023c8]:fsd ft11, 352(a5)
[0x800023cc]:sw a7, 356(a5)
[0x800023d0]:fld ft10, 528(a6)
[0x800023d4]:fld ft9, 536(a6)
[0x800023d8]:fld ft8, 544(a6)
[0x800023dc]:csrrwi zero, frm, 0

[0x800023e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023e4]:csrrs a7, fflags, zero
[0x800023e8]:fsd ft11, 368(a5)
[0x800023ec]:sw a7, 372(a5)
[0x800023f0]:fld ft10, 552(a6)
[0x800023f4]:fld ft9, 560(a6)
[0x800023f8]:fld ft8, 568(a6)
[0x800023fc]:csrrwi zero, frm, 0

[0x80002400]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002404]:csrrs a7, fflags, zero
[0x80002408]:fsd ft11, 384(a5)
[0x8000240c]:sw a7, 388(a5)
[0x80002410]:fld ft10, 576(a6)
[0x80002414]:fld ft9, 584(a6)
[0x80002418]:fld ft8, 592(a6)
[0x8000241c]:csrrwi zero, frm, 0

[0x80002420]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002424]:csrrs a7, fflags, zero
[0x80002428]:fsd ft11, 400(a5)
[0x8000242c]:sw a7, 404(a5)
[0x80002430]:fld ft10, 600(a6)
[0x80002434]:fld ft9, 608(a6)
[0x80002438]:fld ft8, 616(a6)
[0x8000243c]:csrrwi zero, frm, 0

[0x80002440]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002444]:csrrs a7, fflags, zero
[0x80002448]:fsd ft11, 416(a5)
[0x8000244c]:sw a7, 420(a5)
[0x80002450]:fld ft10, 624(a6)
[0x80002454]:fld ft9, 632(a6)
[0x80002458]:fld ft8, 640(a6)
[0x8000245c]:csrrwi zero, frm, 0

[0x80002460]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002464]:csrrs a7, fflags, zero
[0x80002468]:fsd ft11, 432(a5)
[0x8000246c]:sw a7, 436(a5)
[0x80002470]:fld ft10, 648(a6)
[0x80002474]:fld ft9, 656(a6)
[0x80002478]:fld ft8, 664(a6)
[0x8000247c]:csrrwi zero, frm, 0

[0x80002480]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002484]:csrrs a7, fflags, zero
[0x80002488]:fsd ft11, 448(a5)
[0x8000248c]:sw a7, 452(a5)
[0x80002490]:fld ft10, 672(a6)
[0x80002494]:fld ft9, 680(a6)
[0x80002498]:fld ft8, 688(a6)
[0x8000249c]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                         coverpoints                                                                                                                                                                         |                                                                              code                                                                               |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005c14]<br>0x00000003|- opcode : fnmadd.d<br> - rs1 : f26<br> - rs2 : f26<br> - rs3 : f10<br> - rd : f26<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                         |[0x80000124]:fnmadd.d fs10, fs10, fs10, fa0, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fs10, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>      |
|   2|[0x80005c24]<br>0x00000003|- rs1 : f9<br> - rs2 : f16<br> - rs3 : f3<br> - rd : f16<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000031 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000035 and rm_val == 0  #nosat<br>                                 |[0x80000144]:fnmadd.d fa6, fs1, fa6, ft3, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fa6, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80005c34]<br>0x00000003|- rs1 : f12<br> - rs2 : f28<br> - rs3 : f28<br> - rd : f28<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                 |[0x80000164]:fnmadd.d ft8, fa2, ft8, ft8, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft8, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80005c44]<br>0x00000003|- rs1 : f5<br> - rs2 : f30<br> - rs3 : f5<br> - rd : f5<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                    |[0x80000184]:fnmadd.d ft5, ft5, ft10, ft5, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd ft5, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>       |
|   5|[0x80005c54]<br>0x00000003|- rs1 : f10<br> - rs2 : f21<br> - rs3 : f27<br> - rd : f25<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000063 and rm_val == 0  #nosat<br> |[0x800001a4]:fnmadd.d fs9, fa0, fs5, fs11, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fs9, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>       |
|   6|[0x80005c64]<br>0x00000007|- rs1 : f14<br> - rs2 : f14<br> - rs3 : f4<br> - rd : f18<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                     |[0x800001c4]:fnmadd.d fs2, fa4, fa4, ft4, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs2, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>        |
|   7|[0x80005c74]<br>0x00000007|- rs1 : f1<br> - rs2 : f25<br> - rs3 : f6<br> - rd : f1<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000000b and rm_val == 0  #nosat<br>                                  |[0x800001e4]:fnmadd.d ft1, ft1, fs9, ft6, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd ft1, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80005c84]<br>0x00000007|- rs1 : f13<br> - rs2 : f24<br> - rs3 : f12<br> - rd : f12<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000028 and rm_val == 0  #nosat<br>                               |[0x80000204]:fnmadd.d fa2, fa3, fs8, fa2, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fa2, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80005c94]<br>0x00000007|- rs1 : f8<br> - rs2 : f8<br> - rs3 : f8<br> - rd : f27<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                    |[0x80000224]:fnmadd.d fs11, fs0, fs0, fs0, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs11, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>    |
|  10|[0x80005ca4]<br>0x00000007|- rs1 : f7<br> - rs2 : f2<br> - rs3 : f2<br> - rd : f31<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                       |[0x80000244]:fnmadd.d ft11, ft7, ft2, ft2, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd ft11, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>    |
|  11|[0x80005cb4]<br>0x00000007|- rs1 : f15<br> - rs2 : f15<br> - rs3 : f15<br> - rd : f15<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                 |[0x80000264]:fnmadd.d fa5, fa5, fa5, fa5, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fa5, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>      |
|  12|[0x80005cc4]<br>0x00000007|- rs1 : f16<br> - rs2 : f5<br> - rs3 : f16<br> - rd : f21<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                     |[0x80000284]:fnmadd.d fs5, fa6, ft5, fa6, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fs5, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>      |
|  13|[0x80005cd4]<br>0x00000007|- rs1 : f21<br> - rs2 : f9<br> - rs3 : f30<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                           |[0x800002a4]:fnmadd.d fs8, fs5, fs1, ft10, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd fs8, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>     |
|  14|[0x80005ce4]<br>0x00000007|- rs1 : f28<br> - rs2 : f19<br> - rs3 : f11<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                          |[0x800002c4]:fnmadd.d fa0, ft8, fs3, fa1, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fa0, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>      |
|  15|[0x80005cf4]<br>0x00000007|- rs1 : f18<br> - rs2 : f12<br> - rs3 : f0<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000063 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003b and rm_val == 0  #nosat<br>                                                                                            |[0x800002e4]:fnmadd.d ft3, fs2, fa2, ft0, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd ft3, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>      |
|  16|[0x80005d04]<br>0x00000007|- rs1 : f2<br> - rs2 : f29<br> - rs3 : f17<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                           |[0x80000304]:fnmadd.d fa4, ft2, ft9, fa7, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd fa4, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>      |
|  17|[0x80005d14]<br>0x00000007|- rs1 : f3<br> - rs2 : f18<br> - rs3 : f23<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000001c and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000053 and rm_val == 0  #nosat<br>                                                                                           |[0x80000324]:fnmadd.d ft9, ft3, fs2, fs7, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd ft9, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>      |
|  18|[0x80005d24]<br>0x00000007|- rs1 : f20<br> - rs2 : f22<br> - rs3 : f25<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                           |[0x80000344]:fnmadd.d ft0, fs4, fs6, fs9, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd ft0, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>      |
|  19|[0x80005d34]<br>0x00000007|- rs1 : f30<br> - rs2 : f20<br> - rs3 : f18<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000063 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                           |[0x80000364]:fnmadd.d ft7, ft10, fs4, fs2, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd ft7, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>     |
|  20|[0x80005d44]<br>0x00000007|- rs1 : f24<br> - rs2 : f13<br> - rs3 : f9<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000044 and rm_val == 0  #nosat<br>                                                                                            |[0x80000384]:fnmadd.d ft6, fs8, fa3, fs1, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd ft6, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>      |
|  21|[0x80005d54]<br>0x00000007|- rs1 : f0<br> - rs2 : f23<br> - rs3 : f31<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                           |[0x800003a4]:fnmadd.d fa7, ft0, fs7, ft11, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fa7, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>     |
|  22|[0x80005d64]<br>0x00000007|- rs1 : f27<br> - rs2 : f11<br> - rs3 : f21<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000014 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000004c and rm_val == 0  #nosat<br>                                                                                          |[0x800003c4]:fnmadd.d fs6, fs11, fa1, fs5, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd fs6, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>     |
|  23|[0x80005d74]<br>0x00000007|- rs1 : f23<br> - rs2 : f10<br> - rs3 : f13<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000010 and rm_val == 0  #nosat<br>                                                                                          |[0x800003e4]:fnmadd.d fa1, fs7, fa0, fa3, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fa1, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80005d84]<br>0x00000007|- rs1 : f29<br> - rs2 : f3<br> - rs3 : f7<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000042 and rm_val == 0  #nosat<br>                                                                                            |[0x80000404]:fnmadd.d fs7, ft9, ft3, ft7, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fs7, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>      |
|  25|[0x80005d94]<br>0x00000007|- rs1 : f31<br> - rs2 : f1<br> - rs3 : f19<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffff7fff4e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000000e and rm_val == 0  #nosat<br>                                                                                            |[0x80000424]:fnmadd.d fs1, ft11, ft1, fs3, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd fs1, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>     |
|  26|[0x80005da4]<br>0x00000007|- rs1 : f19<br> - rs2 : f6<br> - rs3 : f1<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffbfffb4 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000017 and rm_val == 0  #nosat<br>                                                                                            |[0x80000444]:fnmadd.d ft10, fs3, ft6, ft1, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd ft10, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>    |
|  27|[0x80005db4]<br>0x00000007|- rs1 : f22<br> - rs2 : f4<br> - rs3 : f29<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffdffec6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003a and rm_val == 0  #nosat<br>                                                                                            |[0x80000464]:fnmadd.d fs0, fs6, ft4, ft9, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd fs0, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x80005dc4]<br>0x00000007|- rs1 : f17<br> - rs2 : f0<br> - rs3 : f20<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffefffc2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000019 and rm_val == 0  #nosat<br>                                                                                            |[0x80000484]:fnmadd.d ft4, fa7, ft0, fs4, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd ft4, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x80005dd4]<br>0x00000007|- rs1 : f11<br> - rs2 : f17<br> - rs3 : f26<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000012 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffff7ff8c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000027 and rm_val == 0  #nosat<br>                                                                                           |[0x800004a4]:fnmadd.d ft2, fa1, fa7, fs10, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd ft2, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>     |
|  30|[0x80005de4]<br>0x00000007|- rs1 : f25<br> - rs2 : f27<br> - rs3 : f22<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000023 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffbff76 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                          |[0x800004c4]:fnmadd.d fs4, fs9, fs11, fs6, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd fs4, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>     |
|  31|[0x80005df4]<br>0x00000007|- rs1 : f6<br> - rs2 : f7<br> - rs3 : f24<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000051 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffdfee0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                            |[0x800004e4]:fnmadd.d fa3, ft6, ft7, fs8, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd fa3, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>      |
|  32|[0x80005e04]<br>0x00000007|- rs1 : f4<br> - rs2 : f31<br> - rs3 : f14<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000049 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffefee6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                           |[0x80000504]:fnmadd.d fs3, ft4, ft11, fa4, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fs3, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>     |
|  33|[0x80005e14]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004d and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffff7ef6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000037 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000524]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft11, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>   |
|  34|[0x80005e24]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffbef0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000032 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000544]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd ft11, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>   |
|  35|[0x80005e34]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000018 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffdf8e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000020 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000564]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>   |
|  36|[0x80005e44]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000036 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffef06 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000046 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000584]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80005e54]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000011 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffff7ba and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000011 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800005a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80005e64]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000023 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffb7c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000001e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800005c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80005e74]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffcc8 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800005e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80005e84]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000000c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffe38 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000057 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000604]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80005e94]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xffffffffffec2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000022 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000624]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x80005ea4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff3e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000644]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x80005eb4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff74 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000009 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000664]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x80005ec4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffffe2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000684]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x80005ed4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff78 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800006a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x80005ee4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff3c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000007 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800006c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x80005ef4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xfffffffffff34 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000038 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800006e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80005f04]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000001ffffd and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000704]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80005f14]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000fffa0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000724]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80005f24]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000007ffff and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000001d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000744]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80005f34]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000003fffd and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000764]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80005f44]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000020013 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000784]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80005f54]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000ffdd and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000053 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800007a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80005f64]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000c and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000801a and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004b and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800007c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80005f74]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002b and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000003ff7 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800007e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80005f84]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000001fd1 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000058 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000804]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80005f94]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000fd2 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000060 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000824]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x80005fa4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000801 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000044 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000844]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x80005fb4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004c and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000003b5 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000864]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x80005fc4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000001c and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000001f6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000884]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x80005fd4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000045 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000000e5 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000053 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800008a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x80005fe4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000001a and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000090 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000054 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800008c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x80005ff4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000039 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000016 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000001e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800008e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80006004]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000025 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000000b and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000904]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80006014]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffd4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000005b and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000924]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80006024]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff8c and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000037 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000944]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80006034]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000061 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff9e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000057 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000964]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80006044]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffff58 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000984]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80006054]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffda and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000007 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800009a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80006064]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000001f and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800009c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80006074]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000039 and fs2 == 1 and fe2 == 0x3e0 and fm2 == 0xffff7ffffff8e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000010 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800009e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80006084]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x3df and fm2 == 0xfffaaffffffd4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000055 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80006094]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004a and fs2 == 1 and fe2 == 0x3de and fm2 == 0xfffefffffff6c and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000008 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800060a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000a and fs2 == 1 and fe2 == 0x3dd and fm2 == 0xffe8bffffffec and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000005d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800060b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000c and fs2 == 1 and fe2 == 0x3dc and fm2 == 0xffe17ffffffe8 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800060c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x3db and fm2 == 0xffd9fffffff4e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800060d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x3da and fm2 == 0xff6dfffffff3c and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000049 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000aa4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800060e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x3d9 and fm2 == 0xff8bfffffff96 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000001d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ac4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800060f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x3d8 and fm2 == 0xff17fffffff74 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000001d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ae4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80006104]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x3d7 and fm2 == 0xff7fffffffff6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000008 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80006114]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x3d6 and fm2 == 0xf65ffffffffa2 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80006124]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x3d5 and fm2 == 0xea3ffffffffe7 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000057 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80006134]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005c and fs2 == 1 and fe2 == 0x3d4 and fm2 == 0xdcfffffffff55 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000046 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80006144]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x3d3 and fm2 == 0xabfffffffffe7 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000054 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80006154]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x3d2 and fm2 == 0xeffffffffff42 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000008 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ba4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80006164]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000006 and fs2 == 1 and fe2 == 0x3d1 and fm2 == 0x0bffffffffffa and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000bc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80006174]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000008 and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0xefffffffffff1 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000be8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80006184]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000032 and fs2 == 1 and fe2 == 0x3cf and fm2 == 0x5ffffffffffbb and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000000a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80006194]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x3cb and fm2 == 0xfffffffffff5e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x800061a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000016 and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x43fffffffffe4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x800061b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x3ce and fm2 == 0xfffffffffffd0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x800061c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x3d1 and fm2 == 0x4ffffffffffd2 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000056 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x800061d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000060 and fs2 == 0 and fe2 == 0x3d0 and fm2 == 0x77fffffffff73 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000030 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ca8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x800061e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x8acb90f094130 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000046 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000cc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x800061f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005c and fs2 == 0 and fe2 == 0x42c and fm2 == 0x642c858de9bfc and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ce8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80006204]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x818181800001a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80006214]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xe1e1e1e0f0f93 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80006224]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x431 and fm2 == 0xffffffff80064 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000033 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80006234]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xe1e1e1e1a5ac4 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80006244]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002a and fs2 == 0 and fe2 == 0x42d and fm2 == 0x8618618600049 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80006254]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x47ae147ad7120 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000062 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000da8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80006264]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000049 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xc0e07038150fe and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000032 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000dc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80006274]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x42c and fm2 == 0xbacf914c18376 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000de8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80006284]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001f and fs2 == 0 and fe2 == 0x42e and fm2 == 0x0842108420049 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000048 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x80006294]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000061 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x51d07eae2edbb and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000029 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e2c]:csrrs a7, fflags, zero<br> [0x80000e30]:fsd ft11, 1664(a5)<br> [0x80000e34]:sw a7, 1668(a5)<br> |
| 106|[0x800062a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x702e05c0b7bcc and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e4c]:csrrs a7, fflags, zero<br> [0x80000e50]:fsd ft11, 1680(a5)<br> [0x80000e54]:sw a7, 1684(a5)<br> |
| 107|[0x800062b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x42e and fm2 == 0xe1e1e1e1e1abe and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e6c]:csrrs a7, fflags, zero<br> [0x80000e70]:fsd ft11, 1696(a5)<br> [0x80000e74]:sw a7, 1700(a5)<br> |
| 108|[0x800062c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000049 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xc0e070381bfbf and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e8c]:csrrs a7, fflags, zero<br> [0x80000e90]:fsd ft11, 1712(a5)<br> [0x80000e94]:sw a7, 1716(a5)<br> |
| 109|[0x800062d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x42e and fm2 == 0x1a7b9611a7b6b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ea8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eac]:csrrs a7, fflags, zero<br> [0x80000eb0]:fsd ft11, 1728(a5)<br> [0x80000eb4]:sw a7, 1732(a5)<br> |
| 110|[0x800062e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x47ae147ae144d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ec8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ecc]:csrrs a7, fflags, zero<br> [0x80000ed0]:fsd ft11, 1744(a5)<br> [0x80000ed4]:sw a7, 1748(a5)<br> |
| 111|[0x800062f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xf81f81f81f86e and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000049 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000ee8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 1760(a5)<br> [0x80000ef4]:sw a7, 1764(a5)<br> |
| 112|[0x80006304]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 0 and fe2 == 0x42c and fm2 == 0x58ed2308158f1 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000f08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f0c]:csrrs a7, fflags, zero<br> [0x80000f10]:fsd ft11, 1776(a5)<br> [0x80000f14]:sw a7, 1780(a5)<br> |
| 113|[0x80006314]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x555555555557b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000025 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000f28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f2c]:csrrs a7, fflags, zero<br> [0x80000f30]:fsd ft11, 1792(a5)<br> [0x80000f34]:sw a7, 1796(a5)<br> |
| 114|[0x80006324]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x42c and fm2 == 0x681681681682c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000f48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 1808(a5)<br> [0x80000f54]:sw a7, 1812(a5)<br> |
| 115|[0x80006334]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x55555555555ac and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000044 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000f68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f6c]:csrrs a7, fflags, zero<br> [0x80000f70]:fsd ft11, 1824(a5)<br> [0x80000f74]:sw a7, 1828(a5)<br> |
| 116|[0x80006344]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xe9131abf0b76b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000f88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f8c]:csrrs a7, fflags, zero<br> [0x80000f90]:fsd ft11, 1840(a5)<br> [0x80000f94]:sw a7, 1844(a5)<br> |
| 117|[0x80006354]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 1 and fe2 == 0x42f and fm2 == 0x99999993332ea and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000fa8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fac]:csrrs a7, fflags, zero<br> [0x80000fb0]:fsd ft11, 1856(a5)<br> [0x80000fb4]:sw a7, 1860(a5)<br> |
| 118|[0x80006364]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 1 and fe2 == 0x42f and fm2 == 0x999999966664a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000011 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000fc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fcc]:csrrs a7, fflags, zero<br> [0x80000fd0]:fsd ft11, 1872(a5)<br> [0x80000fd4]:sw a7, 1876(a5)<br> |
| 119|[0x80006374]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xfffffffdfff5a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000052 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000fe8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fec]:csrrs a7, fflags, zero<br> [0x80000ff0]:fsd ft11, 1888(a5)<br> [0x80000ff4]:sw a7, 1892(a5)<br> |
| 120|[0x80006384]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003c and fs2 == 1 and fe2 == 0x42d and fm2 == 0x1111111088821 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000060 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001008]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000100c]:csrrs a7, fflags, zero<br> [0x80001010]:fsd ft11, 1904(a5)<br> [0x80001014]:sw a7, 1908(a5)<br> |
| 121|[0x80006394]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000047 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xcd85688fc64d3 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001028]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 1920(a5)<br> [0x80001034]:sw a7, 1924(a5)<br> |
| 122|[0x800063a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x42f and fm2 == 0x11111110eeeb6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000034 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001048]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000104c]:csrrs a7, fflags, zero<br> [0x80001050]:fsd ft11, 1936(a5)<br> [0x80001054]:sw a7, 1940(a5)<br> |
| 123|[0x800063b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x42c and fm2 == 0x5c9882b91b347 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000057 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001068]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000106c]:csrrs a7, fflags, zero<br> [0x80001070]:fsd ft11, 1952(a5)<br> [0x80001074]:sw a7, 1956(a5)<br> |
| 124|[0x800063c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000a and fs2 == 1 and fe2 == 0x42f and fm2 == 0x999999998ccaa and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001088]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000108c]:csrrs a7, fflags, zero<br> [0x80001090]:fsd ft11, 1968(a5)<br> [0x80001094]:sw a7, 1972(a5)<br> |
| 125|[0x800063d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000038 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x249249248db3a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800010a8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ac]:csrrs a7, fflags, zero<br> [0x800010b0]:fsd ft11, 1984(a5)<br> [0x800010b4]:sw a7, 1988(a5)<br> |
| 126|[0x800063e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x42e and fm2 == 0x2f684bda1090f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800010c8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010cc]:csrrs a7, fflags, zero<br> [0x800010d0]:fsd ft11, 2000(a5)<br> [0x800010d4]:sw a7, 2004(a5)<br> |
| 127|[0x800063f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x4e5e0a72eefd4 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000061 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800010e8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ec]:csrrs a7, fflags, zero<br> [0x800010f0]:fsd ft11, 2016(a5)<br> [0x800010f4]:sw a7, 2020(a5)<br> |
| 128|[0x8000600c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000007 and fs2 == 1 and fe2 == 0x430 and fm2 == 0x2492492491b00 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001110]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 129|[0x8000601c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x5555555554ff8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001130]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 130|[0x8000602c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000017 and fs2 == 1 and fe2 == 0x42e and fm2 == 0x642c8590b1e5d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001150]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 131|[0x8000603c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x430 and fm2 == 0x9999999999766 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001170]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 132|[0x8000604c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000045 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xdae6076b9803a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000060 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001190]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 133|[0x8000605c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000052 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x8f9c18f9c1864 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800011b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 134|[0x8000606c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xfffffffffff22 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800011d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 135|[0x8000607c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005c and fs2 == 1 and fe2 == 0x42c and fm2 == 0x642c8590b213d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000b and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800011f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 136|[0x8000608c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x5555555555543 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001210]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 137|[0x8000609c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000038 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x249249249244b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001230]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 138|[0x800060ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xc71c71c71c6ab and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001250]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 139|[0x800060bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xc71c71c71c6d9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001270]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 140|[0x800060cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000b and fs2 == 0 and fe2 == 0x411 and fm2 == 0x745e9745d1746 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000041 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001290]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 141|[0x800060dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x40e and fm2 == 0x5c98c415c9883 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800012b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 142|[0x800060ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x40d and fm2 == 0x5c9bbea3677d4 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000025 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800012d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 143|[0x800060fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000009 and fs2 == 0 and fe2 == 0x40e and fm2 == 0xc7251c71c71c7 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800012f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 144|[0x8000610c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005c and fs2 == 0 and fe2 == 0x40a and fm2 == 0x6446f4de9bd38 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004b and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001310]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 145|[0x8000611c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x40a and fm2 == 0xc75471c71c71c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001330]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 146|[0x8000612c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x409 and fm2 == 0x1acc234f72c23 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000048 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001350]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 147|[0x8000613c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x408 and fm2 == 0x08b9ce739ce74 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001370]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 148|[0x8000614c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x48d70a3d70a3d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001390]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 149|[0x8000615c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000052 and fs2 == 0 and fe2 == 0x405 and fm2 == 0x90895da895da9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800013b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 150|[0x8000616c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x404 and fm2 == 0x708a114228451 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000003 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800013d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 151|[0x8000617c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003a and fs2 == 0 and fe2 == 0x404 and fm2 == 0x2372c234f72c2 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000040 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800013f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 152|[0x8000618c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000022 and fs2 == 0 and fe2 == 0x404 and fm2 == 0x04f0f0f0f0f0f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000054 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001410]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 153|[0x8000619c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x401 and fm2 == 0xdb33333333333 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001430]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 154|[0x800061ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000b and fs2 == 0 and fe2 == 0x403 and fm2 == 0x945d1745d1746 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001450]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 155|[0x800061bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x402 and fm2 == 0x8787878787878 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001470]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 156|[0x800061cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82d82d82d82d8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001490]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 157|[0x800061dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd3c0ca4587e6b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800014b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 158|[0x800061ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x1e4791e4791e4 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000009 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800014d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 159|[0x800061fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1ce739ce739ce and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800014f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 160|[0x8000620c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000006 and fs2 == 0 and fe2 == 0x402 and fm2 == 0xd555555555555 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000053 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001510]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 161|[0x8000621c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005c and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x7a6f4de9bd37a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001530]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 162|[0x8000622c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x3594d653594d6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001550]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 163|[0x8000623c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x412 and fm2 == 0x999799999999a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001570]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 164|[0x8000624c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000029 and fs2 == 1 and fe2 == 0x40e and fm2 == 0x8f9b3831f3832 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001590]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 165|[0x8000625c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 1 and fe2 == 0x40d and fm2 == 0xfff6600000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800015b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 166|[0x8000626c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x40b and fm2 == 0x744e8ba2e8ba3 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800015d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 167|[0x8000627c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 1 and fe2 == 0x40b and fm2 == 0x5c92620ae4c41 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800015f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 168|[0x8000628c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x40c and fm2 == 0x3af6c4ec4ec4f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000030 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001610]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
| 169|[0x8000629c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x409 and fm2 == 0x0c78a7de6d1d6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001630]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001634]:csrrs a7, fflags, zero<br> [0x80001638]:fsd ft11, 656(a5)<br> [0x8000163c]:sw a7, 660(a5)<br>   |
| 170|[0x800062ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x408 and fm2 == 0x03fdf7df7df7e and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001650]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001654]:csrrs a7, fflags, zero<br> [0x80001658]:fsd ft11, 672(a5)<br> [0x8000165c]:sw a7, 676(a5)<br>   |
| 171|[0x800062bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 1 and fe2 == 0x407 and fm2 == 0x28ac37dac37db and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000044 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001674]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001678]:csrrs a7, fflags, zero<br> [0x8000167c]:fsd ft11, 688(a5)<br> [0x80001680]:sw a7, 692(a5)<br>   |
| 172|[0x800062cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x405 and fm2 == 0x9ebbf309b8b57 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001694]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001698]:csrrs a7, fflags, zero<br> [0x8000169c]:fsd ft11, 704(a5)<br> [0x800016a0]:sw a7, 708(a5)<br>   |
| 173|[0x800062dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000014 and fs2 == 1 and fe2 == 0x406 and fm2 == 0x9633333333333 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800016b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016b8]:csrrs a7, fflags, zero<br> [0x800016bc]:fsd ft11, 720(a5)<br> [0x800016c0]:sw a7, 724(a5)<br>   |
| 174|[0x800062ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000008 and fs2 == 1 and fe2 == 0x406 and fm2 == 0xfa00000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000019 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800016d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 736(a5)<br> [0x800016e0]:sw a7, 740(a5)<br>   |
| 175|[0x800062fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x402 and fm2 == 0xda80000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800016f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016f8]:csrrs a7, fflags, zero<br> [0x800016fc]:fsd ft11, 752(a5)<br> [0x80001700]:sw a7, 756(a5)<br>   |
| 176|[0x8000630c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x402 and fm2 == 0x36aaaaaaaaaab and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001714]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001718]:csrrs a7, fflags, zero<br> [0x8000171c]:fsd ft11, 768(a5)<br> [0x80001720]:sw a7, 772(a5)<br>   |
| 177|[0x8000631c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xe000000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001734]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001738]:csrrs a7, fflags, zero<br> [0x8000173c]:fsd ft11, 784(a5)<br> [0x80001740]:sw a7, 788(a5)<br>   |
| 178|[0x8000632c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000049 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x8fc7e3f1f8fc8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001754]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001758]:csrrs a7, fflags, zero<br> [0x8000175c]:fsd ft11, 800(a5)<br> [0x80001760]:sw a7, 804(a5)<br>   |
| 179|[0x8000633c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 0 and fe2 == 0x400 and fm2 == 0xc000000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001774]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001778]:csrrs a7, fflags, zero<br> [0x8000177c]:fsd ft11, 816(a5)<br> [0x80001780]:sw a7, 820(a5)<br>   |
| 180|[0x8000634c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000008 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x2000000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000018 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001794]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001798]:csrrs a7, fflags, zero<br> [0x8000179c]:fsd ft11, 832(a5)<br> [0x800017a0]:sw a7, 836(a5)<br>   |
| 181|[0x8000635c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x0666666666666 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800017b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:fsd ft11, 848(a5)<br> [0x800017c0]:sw a7, 852(a5)<br>   |
| 182|[0x8000636c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000048 and fs2 == 1 and fe2 == 0x3fb and fm2 == 0xc71c71c71c71c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800017d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017d8]:csrrs a7, fflags, zero<br> [0x800017dc]:fsd ft11, 864(a5)<br> [0x800017e0]:sw a7, 868(a5)<br>   |
| 183|[0x8000637c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9249249249249 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800017f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017f8]:csrrs a7, fflags, zero<br> [0x800017fc]:fsd ft11, 880(a5)<br> [0x80001800]:sw a7, 884(a5)<br>   |
| 184|[0x8000638c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x71c71c71c71c7 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001814]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001818]:csrrs a7, fflags, zero<br> [0x8000181c]:fsd ft11, 896(a5)<br> [0x80001820]:sw a7, 900(a5)<br>   |
| 185|[0x8000639c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x3a9a3784a062b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000033 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001834]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001838]:csrrs a7, fflags, zero<br> [0x8000183c]:fsd ft11, 912(a5)<br> [0x80001840]:sw a7, 916(a5)<br>   |
| 186|[0x800063ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000001ffff1 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000029 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001854]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001858]:csrrs a7, fflags, zero<br> [0x8000185c]:fsd ft11, 928(a5)<br> [0x80001860]:sw a7, 932(a5)<br>   |
| 187|[0x800063bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000fffce and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000004e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001874]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001878]:csrrs a7, fflags, zero<br> [0x8000187c]:fsd ft11, 944(a5)<br> [0x80001880]:sw a7, 948(a5)<br>   |
| 188|[0x800063cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000007ffc8 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001894]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001898]:csrrs a7, fflags, zero<br> [0x8000189c]:fsd ft11, 960(a5)<br> [0x800018a0]:sw a7, 964(a5)<br>   |
| 189|[0x800063dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000003ffb5 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800018b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018b8]:csrrs a7, fflags, zero<br> [0x800018bc]:fsd ft11, 976(a5)<br> [0x800018c0]:sw a7, 980(a5)<br>   |
| 190|[0x800063ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002a and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000001ffee and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800018d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018d8]:csrrs a7, fflags, zero<br> [0x800018dc]:fsd ft11, 992(a5)<br> [0x800018e0]:sw a7, 996(a5)<br>   |
| 191|[0x800063fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000052 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000ffcc and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800018f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018f8]:csrrs a7, fflags, zero<br> [0x800018fc]:fsd ft11, 1008(a5)<br> [0x80001900]:sw a7, 1012(a5)<br> |
| 192|[0x8000640c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000007fd4 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000028 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001914]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001918]:csrrs a7, fflags, zero<br> [0x8000191c]:fsd ft11, 1024(a5)<br> [0x80001920]:sw a7, 1028(a5)<br> |
| 193|[0x8000641c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000003fbb and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001934]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001938]:csrrs a7, fflags, zero<br> [0x8000193c]:fsd ft11, 1040(a5)<br> [0x80001940]:sw a7, 1044(a5)<br> |
| 194|[0x8000642c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000001fcb and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001954]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001958]:csrrs a7, fflags, zero<br> [0x8000195c]:fsd ft11, 1056(a5)<br> [0x80001960]:sw a7, 1060(a5)<br> |
| 195|[0x8000643c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000fca and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001974]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:fsd ft11, 1072(a5)<br> [0x80001980]:sw a7, 1076(a5)<br> |
| 196|[0x8000644c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004e and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000007d0 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001994]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001998]:csrrs a7, fflags, zero<br> [0x8000199c]:fsd ft11, 1088(a5)<br> [0x800019a0]:sw a7, 1092(a5)<br> |
| 197|[0x8000645c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000003f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000003d3 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800019b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019b8]:csrrs a7, fflags, zero<br> [0x800019bc]:fsd ft11, 1104(a5)<br> [0x800019c0]:sw a7, 1108(a5)<br> |
| 198|[0x8000646c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000036 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000001f2 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800019d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019d8]:csrrs a7, fflags, zero<br> [0x800019dc]:fsd ft11, 1120(a5)<br> [0x800019e0]:sw a7, 1124(a5)<br> |
| 199|[0x8000647c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x00000000000f9 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000035 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800019f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019f8]:csrrs a7, fflags, zero<br> [0x800019fc]:fsd ft11, 1136(a5)<br> [0x80001a00]:sw a7, 1140(a5)<br> |
| 200|[0x8000648c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000002b and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000077 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001a14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a18]:csrrs a7, fflags, zero<br> [0x80001a1c]:fsd ft11, 1152(a5)<br> [0x80001a20]:sw a7, 1156(a5)<br> |
| 201|[0x8000649c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x000000000001c and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001a34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a38]:csrrs a7, fflags, zero<br> [0x80001a3c]:fsd ft11, 1168(a5)<br> [0x80001a40]:sw a7, 1172(a5)<br> |
| 202|[0x800064ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000017 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001a54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:fsd ft11, 1184(a5)<br> [0x80001a60]:sw a7, 1188(a5)<br> |
| 203|[0x800064bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffb8 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001a74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a78]:csrrs a7, fflags, zero<br> [0x80001a7c]:fsd ft11, 1200(a5)<br> [0x80001a80]:sw a7, 1204(a5)<br> |
| 204|[0x800064cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000010 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000008 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001a94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a98]:csrrs a7, fflags, zero<br> [0x80001a9c]:fsd ft11, 1216(a5)<br> [0x80001aa0]:sw a7, 1220(a5)<br> |
| 205|[0x800064dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0000000000023 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001ab4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ab8]:csrrs a7, fflags, zero<br> [0x80001abc]:fsd ft11, 1232(a5)<br> [0x80001ac0]:sw a7, 1236(a5)<br> |
| 206|[0x800064ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xfffffffffffda and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000003 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001ad4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ad8]:csrrs a7, fflags, zero<br> [0x80001adc]:fsd ft11, 1248(a5)<br> [0x80001ae0]:sw a7, 1252(a5)<br> |
| 207|[0x80006404]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x411 and fm2 == 0x3b11d3b13b13b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000061 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800020fc]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002100]:csrrs a7, fflags, zero<br> [0x80002104]:fsd ft11, 0(a5)<br> [0x80002108]:sw a7, 4(a5)<br>       |
| 208|[0x80006414]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000039 and fs2 == 1 and fe2 == 0x40e and fm2 == 0x1f6d1f7047dc1 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002120]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002124]:csrrs a7, fflags, zero<br> [0x80002128]:fsd ft11, 16(a5)<br> [0x8000212c]:sw a7, 20(a5)<br>     |
| 209|[0x80006424]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x40d and fm2 == 0x351f521cfb2b8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002140]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002144]:csrrs a7, fflags, zero<br> [0x80002148]:fsd ft11, 32(a5)<br> [0x8000214c]:sw a7, 36(a5)<br>     |
| 210|[0x80006434]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000c and fs2 == 1 and fe2 == 0x40e and fm2 == 0x554d800000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002160]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002164]:csrrs a7, fflags, zero<br> [0x80002168]:fsd ft11, 48(a5)<br> [0x8000216c]:sw a7, 52(a5)<br>     |
| 211|[0x80006444]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000037 and fs2 == 1 and fe2 == 0x40b and fm2 == 0x29d7904a7904a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002b and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002180]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002184]:csrrs a7, fflags, zero<br> [0x80002188]:fsd ft11, 64(a5)<br> [0x8000218c]:sw a7, 68(a5)<br>     |
| 212|[0x80006454]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x409 and fm2 == 0x74545d1745d17 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800021a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021a4]:csrrs a7, fflags, zero<br> [0x800021a8]:fsd ft11, 80(a5)<br> [0x800021ac]:sw a7, 84(a5)<br>     |
| 213|[0x80006464]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x408 and fm2 == 0x810a8a8a8a8a9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800021c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021c4]:csrrs a7, fflags, zero<br> [0x800021c8]:fsd ft11, 96(a5)<br> [0x800021cc]:sw a7, 100(a5)<br>    |
| 214|[0x80006474]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x407 and fm2 == 0x5c3bea3677d47 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000022 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800021e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021e4]:csrrs a7, fflags, zero<br> [0x800021e8]:fsd ft11, 112(a5)<br> [0x800021ec]:sw a7, 116(a5)<br>   |
| 215|[0x80006484]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000047 and fs2 == 1 and fe2 == 0x406 and fm2 == 0xcafc64f52edf9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002200]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002204]:csrrs a7, fflags, zero<br> [0x80002208]:fsd ft11, 128(a5)<br> [0x8000220c]:sw a7, 132(a5)<br>   |
| 216|[0x80006494]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 1 and fe2 == 0x407 and fm2 == 0x44c28f5c28f5c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000049 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002220]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002224]:csrrs a7, fflags, zero<br> [0x80002228]:fsd ft11, 144(a5)<br> [0x8000222c]:sw a7, 148(a5)<br>   |
| 217|[0x800064a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x404 and fm2 == 0x732e8ba2e8ba3 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000d and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002240]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002244]:csrrs a7, fflags, zero<br> [0x80002248]:fsd ft11, 160(a5)<br> [0x8000224c]:sw a7, 164(a5)<br>   |
| 218|[0x800064b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000045 and fs2 == 1 and fe2 == 0x403 and fm2 == 0xcaaaaaaaaaaab and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000046 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002260]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002264]:csrrs a7, fflags, zero<br> [0x80002268]:fsd ft11, 176(a5)<br> [0x8000226c]:sw a7, 180(a5)<br>   |
| 219|[0x800064c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x402 and fm2 == 0x4a8d9df51b3bf and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000035 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80002280]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002284]:csrrs a7, fflags, zero<br> [0x80002288]:fsd ft11, 192(a5)<br> [0x8000228c]:sw a7, 196(a5)<br>   |
| 220|[0x800064d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x401 and fm2 == 0xedb6db6db6db7 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001a and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800022a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022a4]:csrrs a7, fflags, zero<br> [0x800022a8]:fsd ft11, 208(a5)<br> [0x800022ac]:sw a7, 212(a5)<br>   |
| 221|[0x800064e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000003 and fs2 == 1 and fe2 == 0x405 and fm2 == 0x26aaaaaaaaaab and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800022c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022c4]:csrrs a7, fflags, zero<br> [0x800022c8]:fsd ft11, 224(a5)<br> [0x800022cc]:sw a7, 228(a5)<br>   |
