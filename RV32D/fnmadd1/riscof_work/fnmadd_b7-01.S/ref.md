
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002c40')]      |
| SIG_REGION                | [('0x80006210', '0x80006cd0', '688 words')]      |
| COV_LABELS                | fnmadd_b7      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fnmadd1/riscof_work/fnmadd_b7-01.S/ref.S    |
| Total Number of coverpoints| 484     |
| Total Coverpoints Hit     | 414      |
| Total Signature Updates   | 281      |
| STAT1                     | 281      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001eb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001eb8]:csrrs a7, fflags, zero
[0x80001ebc]:fsd ft11, 1744(a5)
[0x80001ec0]:sw a7, 1748(a5)
[0x80001ec4]:fld ft10, 1608(a6)
[0x80001ec8]:fld ft9, 1616(a6)
[0x80001ecc]:fld ft8, 1624(a6)
[0x80001ed0]:csrrwi zero, frm, 3

[0x80001ed4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ed8]:csrrs a7, fflags, zero
[0x80001edc]:fsd ft11, 1760(a5)
[0x80001ee0]:sw a7, 1764(a5)
[0x80001ee4]:fld ft10, 1632(a6)
[0x80001ee8]:fld ft9, 1640(a6)
[0x80001eec]:fld ft8, 1648(a6)
[0x80001ef0]:csrrwi zero, frm, 3

[0x80001ef4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ef8]:csrrs a7, fflags, zero
[0x80001efc]:fsd ft11, 1776(a5)
[0x80001f00]:sw a7, 1780(a5)
[0x80001f04]:fld ft10, 1656(a6)
[0x80001f08]:fld ft9, 1664(a6)
[0x80001f0c]:fld ft8, 1672(a6)
[0x80001f10]:csrrwi zero, frm, 3

[0x80001f14]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f18]:csrrs a7, fflags, zero
[0x80001f1c]:fsd ft11, 1792(a5)
[0x80001f20]:sw a7, 1796(a5)
[0x80001f24]:fld ft10, 1680(a6)
[0x80001f28]:fld ft9, 1688(a6)
[0x80001f2c]:fld ft8, 1696(a6)
[0x80001f30]:csrrwi zero, frm, 3

[0x80001f34]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f38]:csrrs a7, fflags, zero
[0x80001f3c]:fsd ft11, 1808(a5)
[0x80001f40]:sw a7, 1812(a5)
[0x80001f44]:fld ft10, 1704(a6)
[0x80001f48]:fld ft9, 1712(a6)
[0x80001f4c]:fld ft8, 1720(a6)
[0x80001f50]:csrrwi zero, frm, 3

[0x80001f54]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f58]:csrrs a7, fflags, zero
[0x80001f5c]:fsd ft11, 1824(a5)
[0x80001f60]:sw a7, 1828(a5)
[0x80001f64]:fld ft10, 1728(a6)
[0x80001f68]:fld ft9, 1736(a6)
[0x80001f6c]:fld ft8, 1744(a6)
[0x80001f70]:csrrwi zero, frm, 3

[0x80001f74]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f78]:csrrs a7, fflags, zero
[0x80001f7c]:fsd ft11, 1840(a5)
[0x80001f80]:sw a7, 1844(a5)
[0x80001f84]:fld ft10, 1752(a6)
[0x80001f88]:fld ft9, 1760(a6)
[0x80001f8c]:fld ft8, 1768(a6)
[0x80001f90]:csrrwi zero, frm, 3

[0x80001f94]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f98]:csrrs a7, fflags, zero
[0x80001f9c]:fsd ft11, 1856(a5)
[0x80001fa0]:sw a7, 1860(a5)
[0x80001fa4]:fld ft10, 1776(a6)
[0x80001fa8]:fld ft9, 1784(a6)
[0x80001fac]:fld ft8, 1792(a6)
[0x80001fb0]:csrrwi zero, frm, 3

[0x80001fb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fb8]:csrrs a7, fflags, zero
[0x80001fbc]:fsd ft11, 1872(a5)
[0x80001fc0]:sw a7, 1876(a5)
[0x80001fc4]:fld ft10, 1800(a6)
[0x80001fc8]:fld ft9, 1808(a6)
[0x80001fcc]:fld ft8, 1816(a6)
[0x80001fd0]:csrrwi zero, frm, 3

[0x80001fd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fd8]:csrrs a7, fflags, zero
[0x80001fdc]:fsd ft11, 1888(a5)
[0x80001fe0]:sw a7, 1892(a5)
[0x80001fe4]:fld ft10, 1824(a6)
[0x80001fe8]:fld ft9, 1832(a6)
[0x80001fec]:fld ft8, 1840(a6)
[0x80001ff0]:csrrwi zero, frm, 3

[0x80001ff4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ff8]:csrrs a7, fflags, zero
[0x80001ffc]:fsd ft11, 1904(a5)
[0x80002000]:sw a7, 1908(a5)
[0x80002004]:fld ft10, 1848(a6)
[0x80002008]:fld ft9, 1856(a6)
[0x8000200c]:fld ft8, 1864(a6)
[0x80002010]:csrrwi zero, frm, 3

[0x80002014]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002018]:csrrs a7, fflags, zero
[0x8000201c]:fsd ft11, 1920(a5)
[0x80002020]:sw a7, 1924(a5)
[0x80002024]:fld ft10, 1872(a6)
[0x80002028]:fld ft9, 1880(a6)
[0x8000202c]:fld ft8, 1888(a6)
[0x80002030]:csrrwi zero, frm, 3

[0x80002034]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002038]:csrrs a7, fflags, zero
[0x8000203c]:fsd ft11, 1936(a5)
[0x80002040]:sw a7, 1940(a5)
[0x80002044]:fld ft10, 1896(a6)
[0x80002048]:fld ft9, 1904(a6)
[0x8000204c]:fld ft8, 1912(a6)
[0x80002050]:csrrwi zero, frm, 3

[0x80002054]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002058]:csrrs a7, fflags, zero
[0x8000205c]:fsd ft11, 1952(a5)
[0x80002060]:sw a7, 1956(a5)
[0x80002064]:fld ft10, 1920(a6)
[0x80002068]:fld ft9, 1928(a6)
[0x8000206c]:fld ft8, 1936(a6)
[0x80002070]:csrrwi zero, frm, 3

[0x80002074]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002078]:csrrs a7, fflags, zero
[0x8000207c]:fsd ft11, 1968(a5)
[0x80002080]:sw a7, 1972(a5)
[0x80002084]:fld ft10, 1944(a6)
[0x80002088]:fld ft9, 1952(a6)
[0x8000208c]:fld ft8, 1960(a6)
[0x80002090]:csrrwi zero, frm, 3

[0x80002094]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002098]:csrrs a7, fflags, zero
[0x8000209c]:fsd ft11, 1984(a5)
[0x800020a0]:sw a7, 1988(a5)
[0x800020a4]:fld ft10, 1968(a6)
[0x800020a8]:fld ft9, 1976(a6)
[0x800020ac]:fld ft8, 1984(a6)
[0x800020b0]:csrrwi zero, frm, 3

[0x800020b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020b8]:csrrs a7, fflags, zero
[0x800020bc]:fsd ft11, 2000(a5)
[0x800020c0]:sw a7, 2004(a5)
[0x800020c4]:fld ft10, 1992(a6)
[0x800020c8]:fld ft9, 2000(a6)
[0x800020cc]:fld ft8, 2008(a6)
[0x800020d0]:csrrwi zero, frm, 3

[0x800020d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020d8]:csrrs a7, fflags, zero
[0x800020dc]:fsd ft11, 2016(a5)
[0x800020e0]:sw a7, 2020(a5)
[0x800020e4]:auipc a5, 5
[0x800020e8]:addi a5, a5, 2332
[0x800020ec]:fld ft10, 2016(a6)
[0x800020f0]:fld ft9, 2024(a6)
[0x800020f4]:fld ft8, 2032(a6)
[0x800020f8]:csrrwi zero, frm, 3

[0x800026a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800026a4]:csrrs a7, fflags, zero
[0x800026a8]:fsd ft11, 720(a5)
[0x800026ac]:sw a7, 724(a5)
[0x800026b0]:fld ft10, 1080(a6)
[0x800026b4]:fld ft9, 1088(a6)
[0x800026b8]:fld ft8, 1096(a6)
[0x800026bc]:csrrwi zero, frm, 3

[0x800026c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800026c4]:csrrs a7, fflags, zero
[0x800026c8]:fsd ft11, 736(a5)
[0x800026cc]:sw a7, 740(a5)
[0x800026d0]:fld ft10, 1104(a6)
[0x800026d4]:fld ft9, 1112(a6)
[0x800026d8]:fld ft8, 1120(a6)
[0x800026dc]:csrrwi zero, frm, 3

[0x800026e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800026e4]:csrrs a7, fflags, zero
[0x800026e8]:fsd ft11, 752(a5)
[0x800026ec]:sw a7, 756(a5)
[0x800026f0]:fld ft10, 1128(a6)
[0x800026f4]:fld ft9, 1136(a6)
[0x800026f8]:fld ft8, 1144(a6)
[0x800026fc]:csrrwi zero, frm, 3

[0x80002700]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002704]:csrrs a7, fflags, zero
[0x80002708]:fsd ft11, 768(a5)
[0x8000270c]:sw a7, 772(a5)
[0x80002710]:fld ft10, 1152(a6)
[0x80002714]:fld ft9, 1160(a6)
[0x80002718]:fld ft8, 1168(a6)
[0x8000271c]:csrrwi zero, frm, 3

[0x80002720]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002724]:csrrs a7, fflags, zero
[0x80002728]:fsd ft11, 784(a5)
[0x8000272c]:sw a7, 788(a5)
[0x80002730]:fld ft10, 1176(a6)
[0x80002734]:fld ft9, 1184(a6)
[0x80002738]:fld ft8, 1192(a6)
[0x8000273c]:csrrwi zero, frm, 3

[0x80002740]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002744]:csrrs a7, fflags, zero
[0x80002748]:fsd ft11, 800(a5)
[0x8000274c]:sw a7, 804(a5)
[0x80002750]:fld ft10, 1200(a6)
[0x80002754]:fld ft9, 1208(a6)
[0x80002758]:fld ft8, 1216(a6)
[0x8000275c]:csrrwi zero, frm, 3

[0x80002760]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002764]:csrrs a7, fflags, zero
[0x80002768]:fsd ft11, 816(a5)
[0x8000276c]:sw a7, 820(a5)
[0x80002770]:fld ft10, 1224(a6)
[0x80002774]:fld ft9, 1232(a6)
[0x80002778]:fld ft8, 1240(a6)
[0x8000277c]:csrrwi zero, frm, 3

[0x80002780]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002784]:csrrs a7, fflags, zero
[0x80002788]:fsd ft11, 832(a5)
[0x8000278c]:sw a7, 836(a5)
[0x80002790]:fld ft10, 1248(a6)
[0x80002794]:fld ft9, 1256(a6)
[0x80002798]:fld ft8, 1264(a6)
[0x8000279c]:csrrwi zero, frm, 3

[0x800027a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800027a4]:csrrs a7, fflags, zero
[0x800027a8]:fsd ft11, 848(a5)
[0x800027ac]:sw a7, 852(a5)
[0x800027b0]:fld ft10, 1272(a6)
[0x800027b4]:fld ft9, 1280(a6)
[0x800027b8]:fld ft8, 1288(a6)
[0x800027bc]:csrrwi zero, frm, 3

[0x800027c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800027c4]:csrrs a7, fflags, zero
[0x800027c8]:fsd ft11, 864(a5)
[0x800027cc]:sw a7, 868(a5)
[0x800027d0]:fld ft10, 1296(a6)
[0x800027d4]:fld ft9, 1304(a6)
[0x800027d8]:fld ft8, 1312(a6)
[0x800027dc]:csrrwi zero, frm, 3

[0x800027e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800027e4]:csrrs a7, fflags, zero
[0x800027e8]:fsd ft11, 880(a5)
[0x800027ec]:sw a7, 884(a5)
[0x800027f0]:fld ft10, 1320(a6)
[0x800027f4]:fld ft9, 1328(a6)
[0x800027f8]:fld ft8, 1336(a6)
[0x800027fc]:csrrwi zero, frm, 3

[0x80002800]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002804]:csrrs a7, fflags, zero
[0x80002808]:fsd ft11, 896(a5)
[0x8000280c]:sw a7, 900(a5)
[0x80002810]:fld ft10, 1344(a6)
[0x80002814]:fld ft9, 1352(a6)
[0x80002818]:fld ft8, 1360(a6)
[0x8000281c]:csrrwi zero, frm, 3

[0x80002820]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002824]:csrrs a7, fflags, zero
[0x80002828]:fsd ft11, 912(a5)
[0x8000282c]:sw a7, 916(a5)
[0x80002830]:fld ft10, 1368(a6)
[0x80002834]:fld ft9, 1376(a6)
[0x80002838]:fld ft8, 1384(a6)
[0x8000283c]:csrrwi zero, frm, 3

[0x80002840]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002844]:csrrs a7, fflags, zero
[0x80002848]:fsd ft11, 928(a5)
[0x8000284c]:sw a7, 932(a5)
[0x80002850]:fld ft10, 1392(a6)
[0x80002854]:fld ft9, 1400(a6)
[0x80002858]:fld ft8, 1408(a6)
[0x8000285c]:csrrwi zero, frm, 3

[0x80002860]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002864]:csrrs a7, fflags, zero
[0x80002868]:fsd ft11, 944(a5)
[0x8000286c]:sw a7, 948(a5)
[0x80002870]:fld ft10, 1416(a6)
[0x80002874]:fld ft9, 1424(a6)
[0x80002878]:fld ft8, 1432(a6)
[0x8000287c]:csrrwi zero, frm, 3

[0x80002880]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002884]:csrrs a7, fflags, zero
[0x80002888]:fsd ft11, 960(a5)
[0x8000288c]:sw a7, 964(a5)
[0x80002890]:fld ft10, 1440(a6)
[0x80002894]:fld ft9, 1448(a6)
[0x80002898]:fld ft8, 1456(a6)
[0x8000289c]:csrrwi zero, frm, 3

[0x800028a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800028a4]:csrrs a7, fflags, zero
[0x800028a8]:fsd ft11, 976(a5)
[0x800028ac]:sw a7, 980(a5)
[0x800028b0]:fld ft10, 1464(a6)
[0x800028b4]:fld ft9, 1472(a6)
[0x800028b8]:fld ft8, 1480(a6)
[0x800028bc]:csrrwi zero, frm, 3

[0x800028c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800028c4]:csrrs a7, fflags, zero
[0x800028c8]:fsd ft11, 992(a5)
[0x800028cc]:sw a7, 996(a5)
[0x800028d0]:fld ft10, 1488(a6)
[0x800028d4]:fld ft9, 1496(a6)
[0x800028d8]:fld ft8, 1504(a6)
[0x800028dc]:csrrwi zero, frm, 3

[0x800028e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800028e4]:csrrs a7, fflags, zero
[0x800028e8]:fsd ft11, 1008(a5)
[0x800028ec]:sw a7, 1012(a5)
[0x800028f0]:fld ft10, 1512(a6)
[0x800028f4]:fld ft9, 1520(a6)
[0x800028f8]:fld ft8, 1528(a6)
[0x800028fc]:csrrwi zero, frm, 3

[0x80002900]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002904]:csrrs a7, fflags, zero
[0x80002908]:fsd ft11, 1024(a5)
[0x8000290c]:sw a7, 1028(a5)
[0x80002910]:fld ft10, 1536(a6)
[0x80002914]:fld ft9, 1544(a6)
[0x80002918]:fld ft8, 1552(a6)
[0x8000291c]:csrrwi zero, frm, 3

[0x80002920]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002924]:csrrs a7, fflags, zero
[0x80002928]:fsd ft11, 1040(a5)
[0x8000292c]:sw a7, 1044(a5)
[0x80002930]:fld ft10, 1560(a6)
[0x80002934]:fld ft9, 1568(a6)
[0x80002938]:fld ft8, 1576(a6)
[0x8000293c]:csrrwi zero, frm, 3

[0x80002940]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002944]:csrrs a7, fflags, zero
[0x80002948]:fsd ft11, 1056(a5)
[0x8000294c]:sw a7, 1060(a5)
[0x80002950]:fld ft10, 1584(a6)
[0x80002954]:fld ft9, 1592(a6)
[0x80002958]:fld ft8, 1600(a6)
[0x8000295c]:csrrwi zero, frm, 3

[0x80002960]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002964]:csrrs a7, fflags, zero
[0x80002968]:fsd ft11, 1072(a5)
[0x8000296c]:sw a7, 1076(a5)
[0x80002970]:fld ft10, 1608(a6)
[0x80002974]:fld ft9, 1616(a6)
[0x80002978]:fld ft8, 1624(a6)
[0x8000297c]:csrrwi zero, frm, 3

[0x80002980]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002984]:csrrs a7, fflags, zero
[0x80002988]:fsd ft11, 1088(a5)
[0x8000298c]:sw a7, 1092(a5)
[0x80002990]:fld ft10, 1632(a6)
[0x80002994]:fld ft9, 1640(a6)
[0x80002998]:fld ft8, 1648(a6)
[0x8000299c]:csrrwi zero, frm, 3

[0x800029a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800029a4]:csrrs a7, fflags, zero
[0x800029a8]:fsd ft11, 1104(a5)
[0x800029ac]:sw a7, 1108(a5)
[0x800029b0]:fld ft10, 1656(a6)
[0x800029b4]:fld ft9, 1664(a6)
[0x800029b8]:fld ft8, 1672(a6)
[0x800029bc]:csrrwi zero, frm, 3

[0x800029c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800029c4]:csrrs a7, fflags, zero
[0x800029c8]:fsd ft11, 1120(a5)
[0x800029cc]:sw a7, 1124(a5)
[0x800029d0]:fld ft10, 1680(a6)
[0x800029d4]:fld ft9, 1688(a6)
[0x800029d8]:fld ft8, 1696(a6)
[0x800029dc]:csrrwi zero, frm, 3

[0x800029e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x800029e4]:csrrs a7, fflags, zero
[0x800029e8]:fsd ft11, 1136(a5)
[0x800029ec]:sw a7, 1140(a5)
[0x800029f0]:fld ft10, 1704(a6)
[0x800029f4]:fld ft9, 1712(a6)
[0x800029f8]:fld ft8, 1720(a6)
[0x800029fc]:csrrwi zero, frm, 3

[0x80002a00]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002a04]:csrrs a7, fflags, zero
[0x80002a08]:fsd ft11, 1152(a5)
[0x80002a0c]:sw a7, 1156(a5)
[0x80002a10]:fld ft10, 1728(a6)
[0x80002a14]:fld ft9, 1736(a6)
[0x80002a18]:fld ft8, 1744(a6)
[0x80002a1c]:csrrwi zero, frm, 3

[0x80002a20]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002a24]:csrrs a7, fflags, zero
[0x80002a28]:fsd ft11, 1168(a5)
[0x80002a2c]:sw a7, 1172(a5)
[0x80002a30]:fld ft10, 1752(a6)
[0x80002a34]:fld ft9, 1760(a6)
[0x80002a38]:fld ft8, 1768(a6)
[0x80002a3c]:csrrwi zero, frm, 3

[0x80002a40]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002a44]:csrrs a7, fflags, zero
[0x80002a48]:fsd ft11, 1184(a5)
[0x80002a4c]:sw a7, 1188(a5)
[0x80002a50]:fld ft10, 1776(a6)
[0x80002a54]:fld ft9, 1784(a6)
[0x80002a58]:fld ft8, 1792(a6)
[0x80002a5c]:csrrwi zero, frm, 3

[0x80002a60]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002a64]:csrrs a7, fflags, zero
[0x80002a68]:fsd ft11, 1200(a5)
[0x80002a6c]:sw a7, 1204(a5)
[0x80002a70]:fld ft10, 1800(a6)
[0x80002a74]:fld ft9, 1808(a6)
[0x80002a78]:fld ft8, 1816(a6)
[0x80002a7c]:csrrwi zero, frm, 3

[0x80002a80]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002a84]:csrrs a7, fflags, zero
[0x80002a88]:fsd ft11, 1216(a5)
[0x80002a8c]:sw a7, 1220(a5)
[0x80002a90]:fld ft10, 1824(a6)
[0x80002a94]:fld ft9, 1832(a6)
[0x80002a98]:fld ft8, 1840(a6)
[0x80002a9c]:csrrwi zero, frm, 3

[0x80002aa0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002aa4]:csrrs a7, fflags, zero
[0x80002aa8]:fsd ft11, 1232(a5)
[0x80002aac]:sw a7, 1236(a5)
[0x80002ab0]:fld ft10, 1848(a6)
[0x80002ab4]:fld ft9, 1856(a6)
[0x80002ab8]:fld ft8, 1864(a6)
[0x80002abc]:csrrwi zero, frm, 3

[0x80002ac0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002ac4]:csrrs a7, fflags, zero
[0x80002ac8]:fsd ft11, 1248(a5)
[0x80002acc]:sw a7, 1252(a5)
[0x80002ad0]:fld ft10, 1872(a6)
[0x80002ad4]:fld ft9, 1880(a6)
[0x80002ad8]:fld ft8, 1888(a6)
[0x80002adc]:csrrwi zero, frm, 3

[0x80002ae0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002ae4]:csrrs a7, fflags, zero
[0x80002ae8]:fsd ft11, 1264(a5)
[0x80002aec]:sw a7, 1268(a5)
[0x80002af0]:fld ft10, 1896(a6)
[0x80002af4]:fld ft9, 1904(a6)
[0x80002af8]:fld ft8, 1912(a6)
[0x80002afc]:csrrwi zero, frm, 3

[0x80002b00]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002b04]:csrrs a7, fflags, zero
[0x80002b08]:fsd ft11, 1280(a5)
[0x80002b0c]:sw a7, 1284(a5)
[0x80002b10]:fld ft10, 1920(a6)
[0x80002b14]:fld ft9, 1928(a6)
[0x80002b18]:fld ft8, 1936(a6)
[0x80002b1c]:csrrwi zero, frm, 3

[0x80002b20]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002b24]:csrrs a7, fflags, zero
[0x80002b28]:fsd ft11, 1296(a5)
[0x80002b2c]:sw a7, 1300(a5)
[0x80002b30]:fld ft10, 1944(a6)
[0x80002b34]:fld ft9, 1952(a6)
[0x80002b38]:fld ft8, 1960(a6)
[0x80002b3c]:csrrwi zero, frm, 3

[0x80002b40]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002b44]:csrrs a7, fflags, zero
[0x80002b48]:fsd ft11, 1312(a5)
[0x80002b4c]:sw a7, 1316(a5)
[0x80002b50]:fld ft10, 1968(a6)
[0x80002b54]:fld ft9, 1976(a6)
[0x80002b58]:fld ft8, 1984(a6)
[0x80002b5c]:csrrwi zero, frm, 3

[0x80002b60]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002b64]:csrrs a7, fflags, zero
[0x80002b68]:fsd ft11, 1328(a5)
[0x80002b6c]:sw a7, 1332(a5)
[0x80002b70]:fld ft10, 1992(a6)
[0x80002b74]:fld ft9, 2000(a6)
[0x80002b78]:fld ft8, 2008(a6)
[0x80002b7c]:csrrwi zero, frm, 3

[0x80002b80]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002b84]:csrrs a7, fflags, zero
[0x80002b88]:fsd ft11, 1344(a5)
[0x80002b8c]:sw a7, 1348(a5)
[0x80002b90]:fld ft10, 2016(a6)
[0x80002b94]:fld ft9, 2024(a6)
[0x80002b98]:fld ft8, 2032(a6)
[0x80002b9c]:csrrwi zero, frm, 3

[0x80002ba0]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002ba4]:csrrs a7, fflags, zero
[0x80002ba8]:fsd ft11, 1360(a5)
[0x80002bac]:sw a7, 1364(a5)
[0x80002bb0]:addi a6, a6, 2040
[0x80002bb4]:fld ft10, 0(a6)
[0x80002bb8]:fld ft9, 8(a6)
[0x80002bbc]:fld ft8, 16(a6)
[0x80002bc0]:csrrwi zero, frm, 3

[0x80002bc4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002bc8]:csrrs a7, fflags, zero
[0x80002bcc]:fsd ft11, 1376(a5)
[0x80002bd0]:sw a7, 1380(a5)
[0x80002bd4]:fld ft10, 24(a6)
[0x80002bd8]:fld ft9, 32(a6)
[0x80002bdc]:fld ft8, 40(a6)
[0x80002be0]:csrrwi zero, frm, 3

[0x80002be4]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002be8]:csrrs a7, fflags, zero
[0x80002bec]:fsd ft11, 1392(a5)
[0x80002bf0]:sw a7, 1396(a5)
[0x80002bf4]:fld ft10, 48(a6)
[0x80002bf8]:fld ft9, 56(a6)
[0x80002bfc]:fld ft8, 64(a6)
[0x80002c00]:csrrwi zero, frm, 3

[0x80002c04]:fnmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002c08]:csrrs a7, fflags, zero
[0x80002c0c]:fsd ft11, 1408(a5)
[0x80002c10]:sw a7, 1412(a5)
[0x80002c14]:fld ft10, 72(a6)
[0x80002c18]:fld ft9, 80(a6)
[0x80002c1c]:fld ft8, 88(a6)
[0x80002c20]:csrrwi zero, frm, 3



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                         coverpoints                                                                                                                                                                         |                                                                              code                                                                               |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80006214]<br>0x00000005|- opcode : fnmadd.d<br> - rs1 : f19<br> - rs2 : f19<br> - rs3 : f2<br> - rd : f19<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                          |[0x80000124]:fnmadd.d fs3, fs3, fs3, ft2, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fs3, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>          |
|   2|[0x80006224]<br>0x00000005|- rs1 : f16<br> - rs2 : f9<br> - rs3 : f1<br> - rd : f9<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfb5e7e30743be and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x989159062af1d and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x94df550962f1f and rm_val == 3  #nosat<br>                                  |[0x80000144]:fnmadd.d fs1, fa6, fs1, ft1, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fs1, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80006234]<br>0x00000005|- rs1 : f9<br> - rs2 : f8<br> - rs3 : f8<br> - rd : f8<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                     |[0x80000164]:fnmadd.d fs0, fs1, fs0, fs0, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fs0, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80006244]<br>0x00000005|- rs1 : f24<br> - rs2 : f13<br> - rs3 : f24<br> - rd : f24<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                 |[0x80000184]:fnmadd.d fs8, fs8, fa3, fs8, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd fs8, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>        |
|   5|[0x80006254]<br>0x00000005|- rs1 : f14<br> - rs2 : f26<br> - rs3 : f23<br> - rd : f28<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb58c32a8e8e81 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x6e4d1703200de and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x39091976d4e27 and rm_val == 3  #nosat<br> |[0x800001a4]:fnmadd.d ft8, fa4, fs10, fs7, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd ft8, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>       |
|   6|[0x80006264]<br>0x00000005|- rs1 : f23<br> - rs2 : f23<br> - rs3 : f20<br> - rd : f4<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                     |[0x800001c4]:fnmadd.d ft4, fs7, fs7, fs4, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd ft4, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>        |
|   7|[0x80006274]<br>0x00000005|- rs1 : f15<br> - rs2 : f3<br> - rs3 : f18<br> - rd : f15<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5909091b3111f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xe55dec4566519 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x47165a7d5849f and rm_val == 3  #nosat<br>                                |[0x800001e4]:fnmadd.d fa5, fa5, ft3, fs2, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa5, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80006284]<br>0x00000005|- rs1 : f13<br> - rs2 : f6<br> - rs3 : f11<br> - rd : f11<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0e995ebba2f4c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbcc8208896215 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd6258a82fd3ab and rm_val == 3  #nosat<br>                                |[0x80000204]:fnmadd.d fa1, fa3, ft6, fa1, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fa1, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80006294]<br>0x00000005|- rs1 : f25<br> - rs2 : f25<br> - rs3 : f25<br> - rd : f5<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                  |[0x80000224]:fnmadd.d ft5, fs9, fs9, fs9, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft5, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>      |
|  10|[0x800062a4]<br>0x00000005|- rs1 : f21<br> - rs2 : f14<br> - rs3 : f14<br> - rd : f22<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                    |[0x80000244]:fnmadd.d fs6, fs5, fa4, fa4, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd fs6, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>      |
|  11|[0x800062b4]<br>0x00000005|- rs1 : f10<br> - rs2 : f10<br> - rs3 : f10<br> - rd : f10<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                 |[0x80000264]:fnmadd.d fa0, fa0, fa0, fa0, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fa0, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>      |
|  12|[0x800062c4]<br>0x00000005|- rs1 : f28<br> - rs2 : f2<br> - rs3 : f28<br> - rd : f27<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                     |[0x80000284]:fnmadd.d fs11, ft8, ft2, ft8, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fs11, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>    |
|  13|[0x800062d4]<br>0x00000005|- rs1 : f12<br> - rs2 : f30<br> - rs3 : f21<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7b476ad8cf1d9 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0525d415c37d0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x82e7db78df76f and rm_val == 3  #nosat<br>                                                                                           |[0x800002a4]:fnmadd.d ft1, fa2, ft10, fs5, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd ft1, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>     |
|  14|[0x800062e4]<br>0x00000005|- rs1 : f18<br> - rs2 : f22<br> - rs3 : f27<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6d6f453cd09d5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0926f0344e2a9 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7a7fc41336119 and rm_val == 3  #nosat<br>                                                                                          |[0x800002c4]:fnmadd.d fs5, fs2, fs6, fs11, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs5, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>     |
|  15|[0x800062f4]<br>0x00000005|- rs1 : f0<br> - rs2 : f24<br> - rs3 : f12<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3fc9c113e7dd and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xb5f3a7712f3c1 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xabad0b1c364df and rm_val == 3  #nosat<br>                                                                                           |[0x800002e4]:fnmadd.d fa6, ft0, fs8, fa2, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fa6, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>      |
|  16|[0x80006304]<br>0x00000005|- rs1 : f17<br> - rs2 : f16<br> - rs3 : f29<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x96a2abbc7aa4f and fs2 == 0 and fe2 == 0x401 and fm2 == 0x1f3d2d0f403c9 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xc8418ade0b293 and rm_val == 3  #nosat<br>                                                                                          |[0x80000304]:fnmadd.d fs2, fa7, fa6, ft9, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd fs2, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>      |
|  17|[0x80006314]<br>0x00000005|- rs1 : f27<br> - rs2 : f1<br> - rs3 : f5<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfe323ec9363bd and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xc5131b1a6facf and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xc37a7e6c59187 and rm_val == 3  #nosat<br>                                                                                            |[0x80000324]:fnmadd.d fs9, fs11, ft1, ft5, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd fs9, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>     |
|  18|[0x80006324]<br>0x00000005|- rs1 : f7<br> - rs2 : f27<br> - rs3 : f30<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe1c0ed19e6571 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xbfc08e70f364a and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xa54d1d5fcf493 and rm_val == 3  #nosat<br>                                                                                           |[0x80000344]:fnmadd.d fa7, ft7, fs11, ft10, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fa7, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>    |
|  19|[0x80006334]<br>0x00000005|- rs1 : f30<br> - rs2 : f18<br> - rs3 : f4<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xef185a29ced8b and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xef1007cbbce4f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xdeb78b7814d4a and rm_val == 3  #nosat<br>                                                                                            |[0x80000364]:fnmadd.d ft3, ft10, fs2, ft4, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd ft3, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>     |
|  20|[0x80006344]<br>0x00000005|- rs1 : f26<br> - rs2 : f31<br> - rs3 : f15<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4a09cdc694ebe and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xbe77020e55a9a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x1fcb40da96ab7 and rm_val == 3  #nosat<br>                                                                                           |[0x80000384]:fnmadd.d ft0, fs10, ft11, fa5, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd ft0, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>    |
|  21|[0x80006354]<br>0x00000005|- rs1 : f31<br> - rs2 : f17<br> - rs3 : f26<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x6e2d1b4988eff and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5c166c3f1c196 and fs3 == 0 and fe3 == 0x7f9 and fm3 == 0xf1e563cd9b45f and rm_val == 3  #nosat<br>                                                                                          |[0x800003a4]:fnmadd.d ft9, ft11, fa7, fs10, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft9, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>    |
|  22|[0x80006364]<br>0x00000005|- rs1 : f6<br> - rs2 : f29<br> - rs3 : f31<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9b3a030d18301 and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0x0a3c1de5f287e and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0xabaad8cb4507f and rm_val == 3  #nosat<br>                                                                                            |[0x800003c4]:fnmadd.d ft7, ft6, ft9, ft11, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd ft7, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>     |
|  23|[0x80006374]<br>0x00000005|- rs1 : f5<br> - rs2 : f21<br> - rs3 : f3<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x69253e04d588d and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x6c86c89b48791 and fs3 == 0 and fe3 == 0x7f9 and fm3 == 0x011f8c56bf31f and rm_val == 3  #nosat<br>                                                                                             |[0x800003e4]:fnmadd.d ft6, ft5, fs5, ft3, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd ft6, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80006384]<br>0x00000005|- rs1 : f4<br> - rs2 : f15<br> - rs3 : f13<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x666bbd7e7025b and fs2 == 0 and fe2 == 0x401 and fm2 == 0x36129a359300e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xb22082f240cee and rm_val == 3  #nosat<br>                                                                                           |[0x80000404]:fnmadd.d ft11, ft4, fa5, fa3, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd ft11, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>    |
|  25|[0x80006394]<br>0x00000005|- rs1 : f3<br> - rs2 : f4<br> - rs3 : f6<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3d066f5604ff3 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x59bdfdd85538a and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xac28f426623cd and rm_val == 3  #nosat<br>                                                                                             |[0x80000424]:fnmadd.d fa4, ft3, ft4, ft6, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd fa4, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>      |
|  26|[0x800063a4]<br>0x00000005|- rs1 : f29<br> - rs2 : f28<br> - rs3 : f0<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x71a52f5af29fb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x4f28bc5b0c7dd and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3f1fac0303cf and rm_val == 3  #nosat<br>                                                                                           |[0x80000444]:fnmadd.d fa2, ft9, ft8, ft0, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fa2, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>      |
|  27|[0x800063b4]<br>0x00000005|- rs1 : f20<br> - rs2 : f11<br> - rs3 : f7<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0e8bdbea7bdad and fs2 == 0 and fe2 == 0x400 and fm2 == 0x7a9f4d5d84847 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x9022dd58b7848 and rm_val == 3  #nosat<br>                                                                                           |[0x80000464]:fnmadd.d fs10, fs4, fa1, ft7, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd fs10, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>    |
|  28|[0x800063c4]<br>0x00000005|- rs1 : f11<br> - rs2 : f0<br> - rs3 : f17<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0c1417801e13 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xac3c001bead80 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x9f7bc37b13ad5 and rm_val == 3  #nosat<br>                                                                                            |[0x80000484]:fnmadd.d ft2, fa1, ft0, fa7, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd ft2, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x800063d4]<br>0x00000005|- rs1 : f2<br> - rs2 : f20<br> - rs3 : f9<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8b98cfb11e19 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x77871c917f78d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x3783fb6167cc5 and rm_val == 3  #nosat<br>                                                                                            |[0x800004a4]:fnmadd.d fa3, ft2, fs4, fs1, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd fa3, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>      |
|  30|[0x800063e4]<br>0x00000005|- rs1 : f22<br> - rs2 : f7<br> - rs3 : f19<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x399c8f6f0efe3 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc0715de1e0272 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x12ae6e101fc86 and rm_val == 3  #nosat<br>                                                                                           |[0x800004c4]:fnmadd.d fs4, fs6, ft7, fs3, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd fs4, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>      |
|  31|[0x800063f4]<br>0x00000005|- rs1 : f1<br> - rs2 : f5<br> - rs3 : f16<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6313e36882553 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xe07720ca59181 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x4d35430ee7d31 and rm_val == 3  #nosat<br>                                                                                            |[0x800004e4]:fnmadd.d fs7, ft1, ft5, fa6, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd fs7, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>      |
|  32|[0x80006404]<br>0x00000005|- rs1 : f8<br> - rs2 : f12<br> - rs3 : f22<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2c5e54a65f99e and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x91318628166ab and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xd6b9de0f38f1f and rm_val == 3  #nosat<br>                                                                                           |[0x80000504]:fnmadd.d ft10, fs0, fa2, fs6, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd ft10, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>    |
|  33|[0x80006414]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xeb8e7ec407263 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x2d3dd97ec216c and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x2136a6cfefb0f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000524]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft11, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>   |
|  34|[0x80006424]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x732532148e14b and fs2 == 0 and fe2 == 0x400 and fm2 == 0x4196bbc146fc2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd23c2bcb7fbca and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000544]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd ft11, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>   |
|  35|[0x80006434]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x07bed521bd97f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5d29993db696e and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x67ba03ffc3d69 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000564]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>   |
|  36|[0x80006444]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa91cc2eeda2b6 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x2d16e851690ee and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf3fcdb74d8436 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000584]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80006454]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x46907283c3703 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x00c478a50612a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x478b1301a8d88 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800005a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80006464]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x36e9180424cab and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x959d3dfa16465 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xec9dbb378e271 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800005c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80006474]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0x85d0ddbbd337f and fs2 == 0 and fe2 == 0x405 and fm2 == 0x299735bdcbf4d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc5259146f1828 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800005e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80006484]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd08a643767a81 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x1a0673fe55155 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xffc4281f10349 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000604]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80006494]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xdfea3de67b7e5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x65efd3455ee9d and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4f81a00928bd3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000624]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x800064a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x26cb255b6eeae and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x2d14ada24d952 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5ab4aabd9ab21 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000644]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x800064b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc72503a4de826 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x7191a24724c63 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x4887a35dcf07f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000664]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x800064c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe38b7d9ecb85d and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xc34558619f959 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xaa30dcebfa687 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000684]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x800064d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x383b2c2519ccf and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xeedad634cfa76 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2dc68b6a87c8b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800006a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x800064e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa51d6c51d1ed3 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x6eab6238c9e77 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2d94fe8b81681 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800006c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x800064f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7d6910cab4645 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x36ff4f0cc8a27 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xcf599bc3a862a and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800006e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80006504]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x714a4b43230bb and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xf9eacd22ffdde and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6ce722259b5df and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000704]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80006514]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1b4269be8384 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xa81166763c48f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x81b903a6d3873 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000724]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80006524]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfef69952934f9 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x03b3eeb5931a8 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x032d4fffb6241 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000744]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80006534]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xce49d79429375 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x9cc1f0fdede96 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x74ae87e6dfde7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000764]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80006544]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe96a57d6251cf and fs2 == 0 and fe2 == 0x401 and fm2 == 0x590952559935c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x49d1114231f23 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000784]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80006554]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x961235fdcd361 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x748025647cfd9 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x276edd6fea238 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800007a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80006564]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x79650281bbbe5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x1a9fd102ebc82 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xa0a4de9f3ccff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800007c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80006574]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x8a83ab91caf67 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x2f370c755c338 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd346ad8e73db4 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800007e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80006584]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2f45af75a309d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x01a22e30fddc8 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x313515f6b0732 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000804]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80006594]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x20bf7a314584a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x25f0a317af1cd and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x4b8a92519446b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000824]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x800065a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc8641f9667eab and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x93df4ff1c631d and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x6801ddbb9b793 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000844]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x800065b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x56a890b49d515 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xaa3628a97a48b and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x1d3e7f6421b9f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000864]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x800065c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6b30ee882c6ff and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x462e47d248100 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xcec1f8712b967 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000884]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x800065d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3ab2eddd0f3e2 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x415a95626fb4b and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x8b09b6ccf616b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800008a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x800065e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf382adb9c5815 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x52e7ed1f2978b and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4aa3897b68fcb and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800008c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x800065f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x638e54de6d04d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc333b5612c3b1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x3955c864ad5e9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800008e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80006604]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x4f22bc7239e09 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x486ada8ee6c5d and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xadf063f3094db and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000904]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80006614]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x855816586cd5f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x24f3586e6e5d3 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xbd8a92984d74f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000924]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80006624]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x145339275d513 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x77052651de48d and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x94cb77b580367 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000944]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80006634]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1a477161cb929 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x45f106f9b2c4c and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x676677e3e5c5f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000964]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80006644]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf483ee1b37ee5 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x243d5e0d97642 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x1daf3b74e6337 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000984]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80006654]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe63ffe0fd8927 and fs2 == 0 and fe2 == 0x400 and fm2 == 0xadd326d56f47a and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x983526a0eda7b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800009a4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80006664]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9660d8bd8030d and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x5d23efa24aaa9 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x151d898f01e9f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800009c4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80006674]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd569780c8c32 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xe7e1913d68a82 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xb79b1c86ee13f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800009e4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80006684]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x483deba9b320d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x58f0df9c93a49 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xba480d6fab1f0 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80006694]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3a16c4383a8c7 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x4714c1b3c0a3e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x914c8c12db61b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800066a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x07de27a2afa00 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xb14515c250e82 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xbe95f6585ce60 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800066b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0xed42eebdd1ebf and fs2 == 0 and fe2 == 0x400 and fm2 == 0xb0a664a524543 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xa0d0c719bc5cf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800066c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc99fd413d99c8 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x137485fe7722f and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xec66fcc4567c3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800066d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb760eb93237f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x77763a9762a44 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x7422261073b74 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000aa4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800066e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7b45358759931 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x871d34c969c49 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x21b8fce69f86f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ac4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800066f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb49575754d072 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xaeb6741875624 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6f4519c4fc196 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ae4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80006704]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8dabedb3a5ce3 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x870ab5e9973bc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2fb91dad0b5d8 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b04]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80006714]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x104fc35b8270d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x29ae802602ecf and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x3ca6282c4328f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b24]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80006724]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0d7d042d02ad5 and fs2 == 0 and fe2 == 0x3f7 and fm2 == 0x2a4f5346337e1 and fs3 == 0 and fe3 == 0x7f5 and fm3 == 0x3a07081a1dbff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b44]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80006734]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1135ea05cc2f0 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x52d5efba6974d and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x699d80afa59c3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b64]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80006744]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc9eafa1f1cb2f and fs2 == 0 and fe2 == 0x400 and fm2 == 0xf62d4402b3650 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xc121df371c749 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b84]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80006754]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x03f40043aceb4 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x3b2ba282b67b2 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x40098b547482f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ba4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80006764]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x45e6d468a0e75 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x0ac0679f67fea and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5396c9bed3e11 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000bc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80006774]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1c4a6307cfbaf and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2b8fcfb4b3e01 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x4caa95e0431e2 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000be8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80006784]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcad2c729214f6 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x734112d1cad10 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x4cb20bf00026f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80006794]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc7f6930f6303 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x190027e4ff99f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x171414da8237e and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x800067a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb450d7dfdecd9 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xbdb995dd2fa53 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7bd66a8c17dbb and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x800067b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0c1cb571c349 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x3d5f3e658720e and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x33ec64e4a2957 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x800067c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x02656e42b3087 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0d7cc184c4c22 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x10028151f9755 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x800067d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x49b995a03d033 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x4cb5e9cad1dc3 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xac86fb61bd564 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ca8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x800067e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf1c4a6839c06f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xfc26f8f150ae1 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xee0700d4ccbb9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000cc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x800067f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13b6231c042cf and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2f60d35d570ec and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x46bcdb7ef9506 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ce8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80006804]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3ce9244d0afd4 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xf7b8e3a8066eb and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x37c97bdc1a0f1 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80006814]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb8405106f8121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x5c102516346d2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2b4998f90286e and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80006824]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4fdbdbeef1000 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x11af8b92258ec and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x670fc3f1446bf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80006834]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x180eff67d38ac and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x502ceab4da725 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x6fc4d28fba78d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80006844]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc4223879c5fc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbda611da201b7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x898a64f1e7db9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80006854]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7759ed7a3d8e3 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x42c7a02462fed and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd943ce68e9aa8 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000da8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80006864]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6ddcd949dccc7 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x0904064708fc6 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x7abf5d598ce6b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000dc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80006874]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x711155a15b39f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x36c03e4365613 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0002474f9eb1 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000de8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80006884]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x74f19f4afb06d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa1dc79c306d83 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x305f62c02cd2a and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x80006894]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc39c05d7f36f5 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xf9544103c9f73 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xbdb9b4b51b55f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e2c]:csrrs a7, fflags, zero<br> [0x80000e30]:fsd ft11, 1664(a5)<br> [0x80000e34]:sw a7, 1668(a5)<br> |
| 106|[0x800068a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x746e8a535d43e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xee75e5f53f4e6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x67ac69fee0d96 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e4c]:csrrs a7, fflags, zero<br> [0x80000e50]:fsd ft11, 1680(a5)<br> [0x80000e54]:sw a7, 1684(a5)<br> |
| 107|[0x800068b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3b6bd8472f313 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x830aa7f1aed35 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xdce129173123f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e6c]:csrrs a7, fflags, zero<br> [0x80000e70]:fsd ft11, 1696(a5)<br> [0x80000e74]:sw a7, 1700(a5)<br> |
| 108|[0x800068c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xadc74492126d3 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x73097a97bcf6f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x3773d91867257 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e8c]:csrrs a7, fflags, zero<br> [0x80000e90]:fsd ft11, 1712(a5)<br> [0x80000e94]:sw a7, 1716(a5)<br> |
| 109|[0x800068d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x22496d487a71f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x929068cc348a3 and fs3 == 0 and fe3 == 0x7f8 and fm3 == 0xc87b0dbeab87f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ea8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eac]:csrrs a7, fflags, zero<br> [0x80000eb0]:fsd ft11, 1728(a5)<br> [0x80000eb4]:sw a7, 1732(a5)<br> |
| 110|[0x800068e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x12d560504e4fe and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x44d092c7d6c22 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x5cb5f8d81fb73 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ec8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ecc]:csrrs a7, fflags, zero<br> [0x80000ed0]:fsd ft11, 1744(a5)<br> [0x80000ed4]:sw a7, 1748(a5)<br> |
| 111|[0x800068f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc1fe53f451b0e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2089a1202bd9b and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xfb300ac749523 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ee8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 1760(a5)<br> [0x80000ef4]:sw a7, 1764(a5)<br> |
| 112|[0x80006904]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3e7be93e2715d and fs2 == 0 and fe2 == 0x3f7 and fm2 == 0x5a6703b8657a9 and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0xaef3a1b9b3dff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000f08]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f0c]:csrrs a7, fflags, zero<br> [0x80000f10]:fsd ft11, 1776(a5)<br> [0x80000f14]:sw a7, 1780(a5)<br> |
| 113|[0x80006914]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7e65834ffa90e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc5815a072f545 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x52b56c7f0ea97 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000f28]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f2c]:csrrs a7, fflags, zero<br> [0x80000f30]:fsd ft11, 1792(a5)<br> [0x80000f34]:sw a7, 1796(a5)<br> |
| 114|[0x80006924]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6fcc78313f886 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x04ce8c2de8bd5 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x76b46a008334f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000f48]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 1808(a5)<br> [0x80000f54]:sw a7, 1812(a5)<br> |
| 115|[0x80006934]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xcfd26bc72299b and fs2 == 0 and fe2 == 0x400 and fm2 == 0x15481bb0f0293 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf66153f88d9a0 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000f68]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f6c]:csrrs a7, fflags, zero<br> [0x80000f70]:fsd ft11, 1824(a5)<br> [0x80000f74]:sw a7, 1828(a5)<br> |
| 116|[0x80006944]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x16f008ac14225 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9f6b9217e094f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc4a453917dc74 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000f88]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f8c]:csrrs a7, fflags, zero<br> [0x80000f90]:fsd ft11, 1840(a5)<br> [0x80000f94]:sw a7, 1844(a5)<br> |
| 117|[0x80006954]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x156c11719ec1a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x46db1d0dac269 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x62351125ab629 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000fa8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fac]:csrrs a7, fflags, zero<br> [0x80000fb0]:fsd ft11, 1856(a5)<br> [0x80000fb4]:sw a7, 1860(a5)<br> |
| 118|[0x80006964]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc06cd5aa5a5af and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a7bb5fe34980 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x056bb1e6989a9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000fc8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fcc]:csrrs a7, fflags, zero<br> [0x80000fd0]:fsd ft11, 1872(a5)<br> [0x80000fd4]:sw a7, 1876(a5)<br> |
| 119|[0x80006974]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfa68e3be688d9 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xbe12f565ed7fe and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xb9341ecae0589 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000fe8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fec]:csrrs a7, fflags, zero<br> [0x80000ff0]:fsd ft11, 1888(a5)<br> [0x80000ff4]:sw a7, 1892(a5)<br> |
| 120|[0x80006984]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x68f63eccb52d3 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x207f98e3bcff2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x96c8f062c5808 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001008]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000100c]:csrrs a7, fflags, zero<br> [0x80001010]:fsd ft11, 1904(a5)<br> [0x80001014]:sw a7, 1908(a5)<br> |
| 121|[0x80006994]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x204462f4d98ff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xb862baf8a644f and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xefe4b6cc650a7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001028]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 1920(a5)<br> [0x80001034]:sw a7, 1924(a5)<br> |
| 122|[0x800069a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b5915a348e93 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x6a569ee80546d and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xeba18021f5337 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001048]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000104c]:csrrs a7, fflags, zero<br> [0x80001050]:fsd ft11, 1936(a5)<br> [0x80001054]:sw a7, 1940(a5)<br> |
| 123|[0x800069b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfcfca53c51fe2 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xf353c5b2ee007 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf063828829338 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001068]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000106c]:csrrs a7, fflags, zero<br> [0x80001070]:fsd ft11, 1952(a5)<br> [0x80001074]:sw a7, 1956(a5)<br> |
| 124|[0x800069c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc876832fcf335 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0xc9cdda6e87c28 and fs3 == 0 and fe3 == 0x7f8 and fm3 == 0x98254e48b36ff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001088]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000108c]:csrrs a7, fflags, zero<br> [0x80001090]:fsd ft11, 1968(a5)<br> [0x80001094]:sw a7, 1972(a5)<br> |
| 125|[0x800069d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xad148409a7a4d and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xfbb22c4238730 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xa97921f8b9297 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800010a8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ac]:csrrs a7, fflags, zero<br> [0x800010b0]:fsd ft11, 1984(a5)<br> [0x800010b4]:sw a7, 1988(a5)<br> |
| 126|[0x800069e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x3cc38f90f797f and fs2 == 0 and fe2 == 0x403 and fm2 == 0xb8073605348df and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x103c8578e3d2d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800010c8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010cc]:csrrs a7, fflags, zero<br> [0x800010d0]:fsd ft11, 2000(a5)<br> [0x800010d4]:sw a7, 2004(a5)<br> |
| 127|[0x800069f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe04544df380a5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb97c5ea9a7a1f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9e205364cd953 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800010e8]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ec]:csrrs a7, fflags, zero<br> [0x800010f0]:fsd ft11, 2016(a5)<br> [0x800010f4]:sw a7, 2020(a5)<br> |
| 128|[0x8000660c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2daf305ed151f and fs2 == 0 and fe2 == 0x401 and fm2 == 0x21eb6f6396855 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x55a838b0c3724 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001110]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 129|[0x8000661c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7e1bc5122bc39 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x1da94c8484b62 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xaa619cf05ea0d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001130]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 130|[0x8000662c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xa181ad799b50f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x06578856de17d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xabd978f6f936d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001150]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 131|[0x8000663c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8949f5778119b and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9fd0507ccb587 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x3f6776558027a and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001170]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 132|[0x8000664c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x193ffdf23ced1 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x50276b6180376 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x714f4c067d1ec and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001190]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 133|[0x8000665c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd2aec528b7649 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x1363fa816771e and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xf608000b8eeef and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800011b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 134|[0x8000666c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xe97c53d658d5f and fs2 == 0 and fe2 == 0x404 and fm2 == 0x0a121df841057 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfcbdd3285fa40 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800011d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 135|[0x8000667c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f5 and fm1 == 0x91fb87d28a9ff and fs2 == 0 and fe2 == 0x408 and fm2 == 0x1fb84d9a794fa and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc3ca63e9e063f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800011f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 136|[0x8000668c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd2c6e3c45eb41 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x33b6e78f0a0df and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x188901605910f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001210]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 137|[0x8000669c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf133c25543bea and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x4a0b71feadc19 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x40817990df4c0 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001230]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 138|[0x800066ac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x24d08f17c0238 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x488d416b95f58 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x77ccc90941495 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001250]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 139|[0x800066bc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd27e1bfc4fb5f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xc3adeb30d049a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x9b888b8c9068f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001270]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 140|[0x800066cc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc4a83973f181f and fs2 == 0 and fe2 == 0x401 and fm2 == 0x63a715946bbbe and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x3a6e5bc8bf800 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001290]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 141|[0x800066dc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7a5876fb5e6ee and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x3d2c3c110ee11 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd4c0eb4fa8e3d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800012b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 142|[0x800066ec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x41fcd2880834a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x5de106ad85473 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xb810b29ea92a5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800012d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 143|[0x800066fc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x66ac2ff7114a7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0d0231972be25 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x78e60139bf53f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800012f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 144|[0x8000670c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x74b7ae7463e9a and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x426308fed6fc4 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd55f398fa1621 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001310]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 145|[0x8000671c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6776f6ab4f0a7 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xcd9eda2830b60 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x441823f93c0d7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001330]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 146|[0x8000672c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x546d58b0516c7 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x80a5f1550808a and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xff80b06ea7427 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001350]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 147|[0x8000673c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x62ff402c49baf and fs2 == 0 and fe2 == 0x402 and fm2 == 0x69d6ce2107cb9 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf5c3d0b54c586 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001370]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 148|[0x8000674c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x60093f30fd5e0 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x220d4421487d9 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8edcb7bdee2ac and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001390]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 149|[0x8000675c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02a158a06a947 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xcc8ef253ba14c and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xd14a559084937 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800013b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 150|[0x8000676c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7077053f4f8c6 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xb96fe80cefc2c and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x3daf0cd1e8f4b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800013d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 151|[0x8000677c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc023212d8c577 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x60ccb89feb1d2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x34cb56672140d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800013f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 152|[0x8000678c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x114c7285ec6df and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xf888257869929 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x0d4fed0a8cdaf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001410]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 153|[0x8000679c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4c83c1ea7b4eb and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x3a78a696dafd2 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x987651fde28b0 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001430]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 154|[0x800067ac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2ef0e618f004c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x7737f3972157f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xbc05171178e91 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001450]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 155|[0x800067bc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfd23aa95094cb and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb15f6ba660af1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xaef38ced97dfd and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001470]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 156|[0x800067cc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf3c4bdf5e6f2d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7e22304d57ca3 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x750128f1053a7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001490]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 157|[0x800067dc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0a6201a197557 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x7e08a8a7b3795 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8d8740fdfea01 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800014b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 158|[0x800067ec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0a23e2fb712a3 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xd5dbaca2d5c3f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe8781f0338de4 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800014d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 159|[0x800067fc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd22227ba705ff and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xd6b123fbc8aee and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xac86a00971463 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800014f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 160|[0x8000680c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x858851b4b719d and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x52cd08d9ba0ff and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x01c2fbc0cad77 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001510]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 161|[0x8000681c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xda6e456917f2c and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x8a60cf86df806 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x6d70906372287 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001530]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 162|[0x8000682c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3eb883a7b3908 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x3beeb9c2285de and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x895640ced1156 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001550]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 163|[0x8000683c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x383dd5169c637 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x1527d01b8191f and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x520b76d0b75a7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001570]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 164|[0x8000684c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7397d51c953cf and fs2 == 0 and fe2 == 0x3f8 and fm2 == 0xe3a5d84ef4fee and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x5f0418f60057f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001590]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 165|[0x8000685c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7be4444b4f445 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x270188837b78d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xb5c6512b6df3f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800015b0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 166|[0x8000686c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x01ddc928e0d81 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x0e118a6368321 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x1009954929043 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800015d0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 167|[0x8000687c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb5c21cc0aeb77 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x94872c41ac66c and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x59debd3573af1 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800015f0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 168|[0x8000688c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd69ac69d51396 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xe47ab05a54153 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xbd4f14ba45087 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001610]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
| 169|[0x8000689c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc7784d8dacdb and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x5f7c825fb0e2b and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x3c1be9c53e823 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001630]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001634]:csrrs a7, fflags, zero<br> [0x80001638]:fsd ft11, 656(a5)<br> [0x8000163c]:sw a7, 660(a5)<br>   |
| 170|[0x800068ac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe1bc7e1b91dcb and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xca213c005f7f5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xaf0ce1e93ae2d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001650]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001654]:csrrs a7, fflags, zero<br> [0x80001658]:fsd ft11, 672(a5)<br> [0x8000165c]:sw a7, 676(a5)<br>   |
| 171|[0x800068bc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63761262a7af5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x2b460c05202e0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x9f8c2a72e7ad4 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001674]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001678]:csrrs a7, fflags, zero<br> [0x8000167c]:fsd ft11, 688(a5)<br> [0x80001680]:sw a7, 692(a5)<br>   |
| 172|[0x800068cc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb9382d77a6dfe and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x3b5d5677dd48d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0fc47f311a49b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001694]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001698]:csrrs a7, fflags, zero<br> [0x8000169c]:fsd ft11, 704(a5)<br> [0x800016a0]:sw a7, 708(a5)<br>   |
| 173|[0x800068dc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2f75ebd929099 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x99337ab2ee957 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xe5106bbe3c07b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800016b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016b8]:csrrs a7, fflags, zero<br> [0x800016bc]:fsd ft11, 720(a5)<br> [0x800016c0]:sw a7, 724(a5)<br>   |
| 174|[0x800068ec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x06f26a8a4514a and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xde2ebabf2e17e and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xeb28a24dd172f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800016d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 736(a5)<br> [0x800016e0]:sw a7, 740(a5)<br>   |
| 175|[0x800068fc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe56a317b6b243 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb52e4839f4db4 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x9e7b04402a756 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800016f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016f8]:csrrs a7, fflags, zero<br> [0x800016fc]:fsd ft11, 752(a5)<br> [0x80001700]:sw a7, 756(a5)<br>   |
| 176|[0x8000690c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9de2bd8b45031 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x6cdc2677914ea and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x26f12946f0a28 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001714]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001718]:csrrs a7, fflags, zero<br> [0x8000171c]:fsd ft11, 768(a5)<br> [0x80001720]:sw a7, 772(a5)<br>   |
| 177|[0x8000691c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x8a4decc210893 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2a1f12d67acdb and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xcb2e920d67af3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001734]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001738]:csrrs a7, fflags, zero<br> [0x8000173c]:fsd ft11, 784(a5)<br> [0x80001740]:sw a7, 788(a5)<br>   |
| 178|[0x8000692c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x181c884c8efbf and fs2 == 0 and fe2 == 0x401 and fm2 == 0x9f8aed5259957 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc6ae44021a677 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001754]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001758]:csrrs a7, fflags, zero<br> [0x8000175c]:fsd ft11, 800(a5)<br> [0x80001760]:sw a7, 804(a5)<br>   |
| 179|[0x8000693c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xebf74076da493 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x1fe6bb3f80fcb and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x14a2cca067a89 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001774]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001778]:csrrs a7, fflags, zero<br> [0x8000177c]:fsd ft11, 816(a5)<br> [0x80001780]:sw a7, 820(a5)<br>   |
| 180|[0x8000694c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2088d3e11fda6 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1cc44e6d9293e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x40f50c3527b82 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001794]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001798]:csrrs a7, fflags, zero<br> [0x8000179c]:fsd ft11, 832(a5)<br> [0x800017a0]:sw a7, 836(a5)<br>   |
| 181|[0x8000695c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x891e2efdb19e1 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x8bfa6e87db32d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x300911db76428 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800017b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:fsd ft11, 848(a5)<br> [0x800017c0]:sw a7, 852(a5)<br>   |
| 182|[0x8000696c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x746a063e32f3f and fs2 == 0 and fe2 == 0x401 and fm2 == 0x57ebfae2b872f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf45158aa6d635 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800017d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017d8]:csrrs a7, fflags, zero<br> [0x800017dc]:fsd ft11, 864(a5)<br> [0x800017e0]:sw a7, 868(a5)<br>   |
| 183|[0x8000697c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x0f1d33203d307 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9558ab3e1c04a and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xad471945dbc7b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800017f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017f8]:csrrs a7, fflags, zero<br> [0x800017fc]:fsd ft11, 880(a5)<br> [0x80001800]:sw a7, 884(a5)<br>   |
| 184|[0x8000698c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2e74ca276ff7d and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd99ffd3a7ac76 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x17c9679d77d6b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001814]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001818]:csrrs a7, fflags, zero<br> [0x8000181c]:fsd ft11, 896(a5)<br> [0x80001820]:sw a7, 900(a5)<br>   |
| 185|[0x8000699c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa81b0950e06a0 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xb0c82a2c2a677 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x667c9d5aae277 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001834]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001838]:csrrs a7, fflags, zero<br> [0x8000183c]:fsd ft11, 912(a5)<br> [0x80001840]:sw a7, 916(a5)<br>   |
| 186|[0x800069ac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x16e32cf849ecf and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x36742322e556a and fs3 == 0 and fe3 == 0x7f6 and fm3 == 0x52359dac1d2ff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001854]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001858]:csrrs a7, fflags, zero<br> [0x8000185c]:fsd ft11, 928(a5)<br> [0x80001860]:sw a7, 932(a5)<br>   |
| 187|[0x800069bc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x1d7d6707c81f3 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x20c908f9b7990 and fs3 == 0 and fe3 == 0x7f8 and fm3 == 0x420d45610d2ff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001874]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001878]:csrrs a7, fflags, zero<br> [0x8000187c]:fsd ft11, 944(a5)<br> [0x80001880]:sw a7, 948(a5)<br>   |
| 188|[0x800069cc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaa38d0be280e1 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0xfd41d096ecf1d and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xa7f0456043a7f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001894]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001898]:csrrs a7, fflags, zero<br> [0x8000189c]:fsd ft11, 960(a5)<br> [0x800018a0]:sw a7, 964(a5)<br>   |
| 189|[0x800069dc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe550f455cc69b and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd69c0af4bbfae and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xbe153957c4525 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800018b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018b8]:csrrs a7, fflags, zero<br> [0x800018bc]:fsd ft11, 976(a5)<br> [0x800018c0]:sw a7, 980(a5)<br>   |
| 190|[0x800069ec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xcaf8bae2074df and fs2 == 0 and fe2 == 0x404 and fm2 == 0x071b7eb928643 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd7b6d36c4d7ca and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800018d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018d8]:csrrs a7, fflags, zero<br> [0x800018dc]:fsd ft11, 992(a5)<br> [0x800018e0]:sw a7, 996(a5)<br>   |
| 191|[0x800069fc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xdfb88e73d746f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x032591e533529 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe59e1f30d27e6 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800018f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018f8]:csrrs a7, fflags, zero<br> [0x800018fc]:fsd ft11, 1008(a5)<br> [0x80001900]:sw a7, 1012(a5)<br> |
| 192|[0x80006a0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x0c373d72bd3df and fs2 == 0 and fe2 == 0x402 and fm2 == 0x66d44a9bbe736 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x77f3abd87c689 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001914]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001918]:csrrs a7, fflags, zero<br> [0x8000191c]:fsd ft11, 1024(a5)<br> [0x80001920]:sw a7, 1028(a5)<br> |
| 193|[0x80006a1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x7fb56f2b7523f and fs2 == 0 and fe2 == 0x401 and fm2 == 0x96be7753f5751 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30d39ce94e382 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001934]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001938]:csrrs a7, fflags, zero<br> [0x8000193c]:fsd ft11, 1040(a5)<br> [0x80001940]:sw a7, 1044(a5)<br> |
| 194|[0x80006a2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5e26a2d52eba8 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x22dc44e238e68 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8dd50be11d539 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001954]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001958]:csrrs a7, fflags, zero<br> [0x8000195c]:fsd ft11, 1056(a5)<br> [0x80001960]:sw a7, 1060(a5)<br> |
| 195|[0x80006a3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfd7f793e002b0 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x6c604e008a06d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6a9875b7c4913 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001974]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:fsd ft11, 1072(a5)<br> [0x80001980]:sw a7, 1076(a5)<br> |
| 196|[0x80006a4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x94c1056ed6745 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xe25cdfd46ec52 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x7d5321be53296 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001994]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001998]:csrrs a7, fflags, zero<br> [0x8000199c]:fsd ft11, 1088(a5)<br> [0x800019a0]:sw a7, 1092(a5)<br> |
| 197|[0x80006a5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2f364dc0d6dea and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xb8d35f83ea038 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x050fd8437607f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800019b4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019b8]:csrrs a7, fflags, zero<br> [0x800019bc]:fsd ft11, 1104(a5)<br> [0x800019c0]:sw a7, 1108(a5)<br> |
| 198|[0x80006a6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9338daa943159 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x1632cecbb4fc2 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xb62fc4471f033 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800019d4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019d8]:csrrs a7, fflags, zero<br> [0x800019dc]:fsd ft11, 1120(a5)<br> [0x800019e0]:sw a7, 1124(a5)<br> |
| 199|[0x80006a7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7f239e9dfb7e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xd00ee4219c2c6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8ec05022473c9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800019f4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019f8]:csrrs a7, fflags, zero<br> [0x800019fc]:fsd ft11, 1136(a5)<br> [0x80001a00]:sw a7, 1140(a5)<br> |
| 200|[0x80006a8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc6036c1db05b5 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd2dc4a5d76216 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x9dfc74d0d944b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001a14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a18]:csrrs a7, fflags, zero<br> [0x80001a1c]:fsd ft11, 1152(a5)<br> [0x80001a20]:sw a7, 1156(a5)<br> |
| 201|[0x80006a9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d4b3ded55275 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xc381c96d1da30 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x25ea43a0ba6d5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001a34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a38]:csrrs a7, fflags, zero<br> [0x80001a3c]:fsd ft11, 1168(a5)<br> [0x80001a40]:sw a7, 1172(a5)<br> |
| 202|[0x80006aac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5a1465bf903f8 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x14437285df6c6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x75792bdb3899b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001a54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:fsd ft11, 1184(a5)<br> [0x80001a60]:sw a7, 1188(a5)<br> |
| 203|[0x80006abc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6f02241f1dfe and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa18b735e6fe18 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x800ed835cf169 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001a74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a78]:csrrs a7, fflags, zero<br> [0x80001a7c]:fsd ft11, 1200(a5)<br> [0x80001a80]:sw a7, 1204(a5)<br> |
| 204|[0x80006acc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf76c60cb611bb and fs2 == 0 and fe2 == 0x3fa and fm2 == 0xa0af53f41063f and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x99b472bc5fdef and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001a94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a98]:csrrs a7, fflags, zero<br> [0x80001a9c]:fsd ft11, 1216(a5)<br> [0x80001aa0]:sw a7, 1220(a5)<br> |
| 205|[0x80006adc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xebc259ea86bbf and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xde9682afb4452 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xcbab01676665f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001ab4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ab8]:csrrs a7, fflags, zero<br> [0x80001abc]:fsd ft11, 1232(a5)<br> [0x80001ac0]:sw a7, 1236(a5)<br> |
| 206|[0x80006aec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x595145e4cc727 and fs2 == 0 and fe2 == 0x400 and fm2 == 0xf61026fb4a229 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x529d945c07226 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001ad4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ad8]:csrrs a7, fflags, zero<br> [0x80001adc]:fsd ft11, 1248(a5)<br> [0x80001ae0]:sw a7, 1252(a5)<br> |
| 207|[0x80006afc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x72c391e4b2abf and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x3184dd5e108c3 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xba7b6e43c3844 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001af4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001af8]:csrrs a7, fflags, zero<br> [0x80001afc]:fsd ft11, 1264(a5)<br> [0x80001b00]:sw a7, 1268(a5)<br> |
| 208|[0x80006b0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2c8ac78bde04d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9addc8b902ed2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xe25aa309e4f15 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001b14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001b18]:csrrs a7, fflags, zero<br> [0x80001b1c]:fsd ft11, 1280(a5)<br> [0x80001b20]:sw a7, 1284(a5)<br> |
| 209|[0x80006b1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x216fdd2c02fec and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xa1a7ccc099bf6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd834eebcde902 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001b34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001b38]:csrrs a7, fflags, zero<br> [0x80001b3c]:fsd ft11, 1296(a5)<br> [0x80001b40]:sw a7, 1300(a5)<br> |
| 210|[0x80006b2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x92a40ada09bc7 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x25bb1366e5e7b and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xcdfbfcc37e2d7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001b54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001b58]:csrrs a7, fflags, zero<br> [0x80001b5c]:fsd ft11, 1312(a5)<br> [0x80001b60]:sw a7, 1316(a5)<br> |
| 211|[0x80006b3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x07a03c3d506bf and fs2 == 0 and fe2 == 0x403 and fm2 == 0x0dda3a9010ee0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x15e41a0e62eed and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001b74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001b78]:csrrs a7, fflags, zero<br> [0x80001b7c]:fsd ft11, 1328(a5)<br> [0x80001b80]:sw a7, 1332(a5)<br> |
| 212|[0x80006b4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a32fb58f33bf and fs2 == 0 and fe2 == 0x400 and fm2 == 0xd5dc4397c31a5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x78702b4c3d43b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001b94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001b98]:csrrs a7, fflags, zero<br> [0x80001b9c]:fsd ft11, 1344(a5)<br> [0x80001ba0]:sw a7, 1348(a5)<br> |
| 213|[0x80006b5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd55eb74d3e867 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x8d1eaf92ca954 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6c0e12a293e24 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001bb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001bb8]:csrrs a7, fflags, zero<br> [0x80001bbc]:fsd ft11, 1360(a5)<br> [0x80001bc0]:sw a7, 1364(a5)<br> |
| 214|[0x80006b6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x46a079e389d6f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x3090a9e293cab and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x849723c3ca606 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001bd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001bd8]:csrrs a7, fflags, zero<br> [0x80001bdc]:fsd ft11, 1376(a5)<br> [0x80001be0]:sw a7, 1380(a5)<br> |
| 215|[0x80006b7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x89ee780981d7f and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x4519b1a6eef31 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xf443480c8c16f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001bf4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001bf8]:csrrs a7, fflags, zero<br> [0x80001bfc]:fsd ft11, 1392(a5)<br> [0x80001c00]:sw a7, 1396(a5)<br> |
| 216|[0x80006b8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5022a4b0b3f6b and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe7c8f3de00c66 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x403ce14898e4f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001c14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001c18]:csrrs a7, fflags, zero<br> [0x80001c1c]:fsd ft11, 1408(a5)<br> [0x80001c20]:sw a7, 1412(a5)<br> |
| 217|[0x80006b9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5547440bd97d4 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x75882bb7e991b and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf1f65e498feec and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001c34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001c38]:csrrs a7, fflags, zero<br> [0x80001c3c]:fsd ft11, 1424(a5)<br> [0x80001c40]:sw a7, 1428(a5)<br> |
| 218|[0x80006bac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9c4ac16f8b53d and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd955fc1f7a7e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d284d22cd9ad and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001c54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001c58]:csrrs a7, fflags, zero<br> [0x80001c5c]:fsd ft11, 1440(a5)<br> [0x80001c60]:sw a7, 1444(a5)<br> |
| 219|[0x80006bbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6ec85d8ef4d3f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xcd177dd63d0aa and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x4a503c62cfadd and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001c74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001c78]:csrrs a7, fflags, zero<br> [0x80001c7c]:fsd ft11, 1456(a5)<br> [0x80001c80]:sw a7, 1460(a5)<br> |
| 220|[0x80006bcc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1a24627666f3c and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x3392e0c107348 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x52fb8288b883b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001c94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001c98]:csrrs a7, fflags, zero<br> [0x80001c9c]:fsd ft11, 1472(a5)<br> [0x80001ca0]:sw a7, 1476(a5)<br> |
| 221|[0x80006bdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x970d2dec24b47 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x41348151b216e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfebb02e1f878d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001cb4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001cb8]:csrrs a7, fflags, zero<br> [0x80001cbc]:fsd ft11, 1488(a5)<br> [0x80001cc0]:sw a7, 1492(a5)<br> |
| 222|[0x80006bec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x863a1b435dbbb and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x2836c6ec995be and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xc386af013d02d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001cd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001cd8]:csrrs a7, fflags, zero<br> [0x80001cdc]:fsd ft11, 1504(a5)<br> [0x80001ce0]:sw a7, 1508(a5)<br> |
| 223|[0x80006bfc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x8152cec29ef7f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xba9139c781bd0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4d11c7ed6eb1b and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001cf4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001cf8]:csrrs a7, fflags, zero<br> [0x80001cfc]:fsd ft11, 1520(a5)<br> [0x80001d00]:sw a7, 1524(a5)<br> |
| 224|[0x80006c0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x92a566f38682f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7264e5116401a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2348dfd8bf745 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001d14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001d18]:csrrs a7, fflags, zero<br> [0x80001d1c]:fsd ft11, 1536(a5)<br> [0x80001d20]:sw a7, 1540(a5)<br> |
| 225|[0x80006c1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xec34018f9e9cb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x30d63352622ac and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x250ccc004e0f7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001d34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001d38]:csrrs a7, fflags, zero<br> [0x80001d3c]:fsd ft11, 1552(a5)<br> [0x80001d40]:sw a7, 1556(a5)<br> |
| 226|[0x80006c2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x4e335bc1ddd33 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xf08485d88dfd4 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4418417f288ed and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001d54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001d58]:csrrs a7, fflags, zero<br> [0x80001d5c]:fsd ft11, 1568(a5)<br> [0x80001d60]:sw a7, 1572(a5)<br> |
| 227|[0x80006c3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd23797880df4f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xd756466804b1c and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xad30b3586df9d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001d74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001d78]:csrrs a7, fflags, zero<br> [0x80001d7c]:fsd ft11, 1584(a5)<br> [0x80001d80]:sw a7, 1588(a5)<br> |
| 228|[0x80006c4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc5d403b21099f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x45d0bd32bed6c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x20cc1a2dc61a1 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001d94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001d98]:csrrs a7, fflags, zero<br> [0x80001d9c]:fsd ft11, 1600(a5)<br> [0x80001da0]:sw a7, 1604(a5)<br> |
| 229|[0x80006c5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8da0eee5f982f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x262463c31332c and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xc8df57f1f0ae3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001db4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001db8]:csrrs a7, fflags, zero<br> [0x80001dbc]:fsd ft11, 1616(a5)<br> [0x80001dc0]:sw a7, 1620(a5)<br> |
| 230|[0x80006c6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x35406181dfc67 and fs2 == 0 and fe2 == 0x400 and fm2 == 0xe1d92982a5bdb and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x230a26766788c and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001dd4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001dd8]:csrrs a7, fflags, zero<br> [0x80001ddc]:fsd ft11, 1632(a5)<br> [0x80001de0]:sw a7, 1636(a5)<br> |
| 231|[0x80006c7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x33d0b32f07673 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xdfd140f0f49e2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x20778d5e9769f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001df4]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001df8]:csrrs a7, fflags, zero<br> [0x80001dfc]:fsd ft11, 1648(a5)<br> [0x80001e00]:sw a7, 1652(a5)<br> |
| 232|[0x80006c8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf350697c4563f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x13d9f585b265d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0d043fad51483 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001e14]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001e18]:csrrs a7, fflags, zero<br> [0x80001e1c]:fsd ft11, 1664(a5)<br> [0x80001e20]:sw a7, 1668(a5)<br> |
| 233|[0x80006c9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2efc48b367db7 and fs2 == 0 and fe2 == 0x401 and fm2 == 0xc222f1cec7041 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0a6069be9fb0f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001e34]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001e38]:csrrs a7, fflags, zero<br> [0x80001e3c]:fsd ft11, 1680(a5)<br> [0x80001e40]:sw a7, 1684(a5)<br> |
| 234|[0x80006cac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfa664451d7f79 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x37596618df133 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x33f18a418ff2f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001e54]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001e58]:csrrs a7, fflags, zero<br> [0x80001e5c]:fsd ft11, 1696(a5)<br> [0x80001e60]:sw a7, 1700(a5)<br> |
| 235|[0x80006cbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1820d688af302 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb2424702b6247 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xdb3031df1b4d9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001e74]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001e78]:csrrs a7, fflags, zero<br> [0x80001e7c]:fsd ft11, 1712(a5)<br> [0x80001e80]:sw a7, 1716(a5)<br> |
| 236|[0x80006ccc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7689c5eb40a1f and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x3f42185eb0526 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xd31660cc9f4c7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001e94]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001e98]:csrrs a7, fflags, zero<br> [0x80001e9c]:fsd ft11, 1728(a5)<br> [0x80001ea0]:sw a7, 1732(a5)<br> |
| 237|[0x80006a04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc95dea29dfb56 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x030f05c9863cc and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xced4dabff5ebf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800020fc]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002100]:csrrs a7, fflags, zero<br> [0x80002104]:fsd ft11, 0(a5)<br> [0x80002108]:sw a7, 4(a5)<br>       |
| 238|[0x80006a14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd2a2cdd0f8cb3 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfdf05ad534fe3 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xd0c1e8b5a87f3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002120]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002124]:csrrs a7, fflags, zero<br> [0x80002128]:fsd ft11, 16(a5)<br> [0x8000212c]:sw a7, 20(a5)<br>     |
| 239|[0x80006a24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x5631ca527644f and fs2 == 0 and fe2 == 0x401 and fm2 == 0xd985d78d050af and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x3c7a736865391 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002140]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002144]:csrrs a7, fflags, zero<br> [0x80002148]:fsd ft11, 32(a5)<br> [0x8000214c]:sw a7, 36(a5)<br>     |
| 240|[0x80006a34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x041c0b9ddaa1b and fs2 == 0 and fe2 == 0x401 and fm2 == 0xa396879bb08c6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xaa52d93b25d1f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002160]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002164]:csrrs a7, fflags, zero<br> [0x80002168]:fsd ft11, 48(a5)<br> [0x8000216c]:sw a7, 52(a5)<br>     |
| 241|[0x80006a44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x422628c1f9624 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1d5d6936bdb26 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x671a07a34a94c and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002180]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002184]:csrrs a7, fflags, zero<br> [0x80002188]:fsd ft11, 64(a5)<br> [0x8000218c]:sw a7, 68(a5)<br>     |
| 242|[0x80006a54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd9d183dcaf23f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xf1b40133408ed and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xcc9675446a76e and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800021a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021a4]:csrrs a7, fflags, zero<br> [0x800021a8]:fsd ft11, 80(a5)<br> [0x800021ac]:sw a7, 84(a5)<br>     |
| 243|[0x80006a64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xafd8e8172cff7 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x87e14bf298e14 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x4a882c1e9583d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800021c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021c4]:csrrs a7, fflags, zero<br> [0x800021c8]:fsd ft11, 96(a5)<br> [0x800021cc]:sw a7, 100(a5)<br>    |
| 244|[0x80006a74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x418a2feac319d and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x3d60d72ed2614 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x8ea1bf6d88f07 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800021e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021e4]:csrrs a7, fflags, zero<br> [0x800021e8]:fsd ft11, 112(a5)<br> [0x800021ec]:sw a7, 116(a5)<br>   |
| 245|[0x80006a84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3691acd45f727 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xe71eee6788145 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x277a5309a56af and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002200]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002204]:csrrs a7, fflags, zero<br> [0x80002208]:fsd ft11, 128(a5)<br> [0x8000220c]:sw a7, 132(a5)<br>   |
| 246|[0x80006a94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x28e011ae50327 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x1433eebd8af82 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x404dcc8f93360 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002220]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002224]:csrrs a7, fflags, zero<br> [0x80002228]:fsd ft11, 144(a5)<br> [0x8000222c]:sw a7, 148(a5)<br>   |
| 247|[0x80006aa4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a3c26bde85df and fs2 == 0 and fe2 == 0x400 and fm2 == 0xce9b661b8773e and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x72a8c9fb21963 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002240]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002244]:csrrs a7, fflags, zero<br> [0x80002248]:fsd ft11, 160(a5)<br> [0x8000224c]:sw a7, 164(a5)<br>   |
| 248|[0x80006ab4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf77ea389b4723 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xe84382d87f02b and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xe027179db45b7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002260]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002264]:csrrs a7, fflags, zero<br> [0x80002268]:fsd ft11, 176(a5)<br> [0x8000226c]:sw a7, 180(a5)<br>   |
| 249|[0x80006ac4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x90887e3335c40 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0a37a4da7a4ac and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa084e26197488 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002280]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002284]:csrrs a7, fflags, zero<br> [0x80002288]:fsd ft11, 192(a5)<br> [0x8000228c]:sw a7, 196(a5)<br>   |
| 250|[0x80006ad4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3a13d100f2dec and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x605083172db32 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xb03e0667320ab and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800022a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022a4]:csrrs a7, fflags, zero<br> [0x800022a8]:fsd ft11, 208(a5)<br> [0x800022ac]:sw a7, 212(a5)<br>   |
| 251|[0x80006ae4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x8a82752785fdf and fs2 == 0 and fe2 == 0x401 and fm2 == 0x3ac2fa289aaaf and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe5107c113b71d and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800022c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022c4]:csrrs a7, fflags, zero<br> [0x800022c8]:fsd ft11, 224(a5)<br> [0x800022cc]:sw a7, 228(a5)<br>   |
| 252|[0x80006af4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb4fcdae5be740 and fs2 == 0 and fe2 == 0x3fb and fm2 == 0x2add24a985c2b and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xfe27d3ad5610f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800022e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022e4]:csrrs a7, fflags, zero<br> [0x800022e8]:fsd ft11, 240(a5)<br> [0x800022ec]:sw a7, 244(a5)<br>   |
| 253|[0x80006b04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x72b318fdc5f95 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x4bd059a2635c8 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe07b449fd6117 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002300]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002304]:csrrs a7, fflags, zero<br> [0x80002308]:fsd ft11, 256(a5)<br> [0x8000230c]:sw a7, 260(a5)<br>   |
| 254|[0x80006b14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xcb550d5d3bb27 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x9a0e906be3f15 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6fe02c97bce1c and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002320]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002324]:csrrs a7, fflags, zero<br> [0x80002328]:fsd ft11, 272(a5)<br> [0x8000232c]:sw a7, 276(a5)<br>   |
| 255|[0x80006b24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xed5da4e13385f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x9b0757709ef4e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8c11bee4771c7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002340]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002344]:csrrs a7, fflags, zero<br> [0x80002348]:fsd ft11, 288(a5)<br> [0x8000234c]:sw a7, 292(a5)<br>   |
| 256|[0x80006b34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xcc2450fb79d8f and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x04435bc917dff and fs3 == 0 and fe3 == 0x7fa and fm3 == 0xd3cdf4baf5c7f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002360]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002364]:csrrs a7, fflags, zero<br> [0x80002368]:fsd ft11, 304(a5)<br> [0x8000236c]:sw a7, 308(a5)<br>   |
| 257|[0x80006b44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x29c4cac7a9799 and fs2 == 0 and fe2 == 0x3f9 and fm2 == 0x5ce4d98b74cf6 and fs3 == 0 and fe3 == 0x7f7 and fm3 == 0x95d1b3f609cff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002380]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002384]:csrrs a7, fflags, zero<br> [0x80002388]:fsd ft11, 320(a5)<br> [0x8000238c]:sw a7, 324(a5)<br>   |
| 258|[0x80006b54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x963da34a1fbd1 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x2e31bf46211f7 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xdf8ba7f4f8dff and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800023a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800023a4]:csrrs a7, fflags, zero<br> [0x800023a8]:fsd ft11, 336(a5)<br> [0x800023ac]:sw a7, 340(a5)<br>   |
| 259|[0x80006b64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x97d71ffccd475 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x39aaaebff3689 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf3b5f15d59e2f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800023c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800023c4]:csrrs a7, fflags, zero<br> [0x800023c8]:fsd ft11, 352(a5)<br> [0x800023cc]:sw a7, 356(a5)<br>   |
| 260|[0x80006b74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x34c088d102eed and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x0fca38061c1c4 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x47cbb452b35f3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800023e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800023e4]:csrrs a7, fflags, zero<br> [0x800023e8]:fsd ft11, 368(a5)<br> [0x800023ec]:sw a7, 372(a5)<br>   |
| 261|[0x80006b84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd89dbaa7a4f33 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0xa127f980d5f2f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x81115cd8e3d03 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002400]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002404]:csrrs a7, fflags, zero<br> [0x80002408]:fsd ft11, 384(a5)<br> [0x8000240c]:sw a7, 388(a5)<br>   |
| 262|[0x80006b94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5188518a6f19d and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x78f8cbc2f063e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xf108407a7033a and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002420]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002424]:csrrs a7, fflags, zero<br> [0x80002428]:fsd ft11, 400(a5)<br> [0x8000242c]:sw a7, 404(a5)<br>   |
| 263|[0x80006ba4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9c07029de79b8 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x9b8038f3396c0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4b26d02ee8b7f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002440]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002444]:csrrs a7, fflags, zero<br> [0x80002448]:fsd ft11, 416(a5)<br> [0x8000244c]:sw a7, 420(a5)<br>   |
| 264|[0x80006bb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x82899f3f923cd and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x9e9b3e1b10de0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x39027b5136447 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002460]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002464]:csrrs a7, fflags, zero<br> [0x80002468]:fsd ft11, 432(a5)<br> [0x8000246c]:sw a7, 436(a5)<br>   |
| 265|[0x80006bc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xea061fbefa949 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x30410dae8f33d and fs3 == 0 and fe3 == 0x7fc and fm3 == 0x2331e5b8a5787 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002480]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002484]:csrrs a7, fflags, zero<br> [0x80002488]:fsd ft11, 448(a5)<br> [0x8000248c]:sw a7, 452(a5)<br>   |
| 266|[0x80006bd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x125e354d1e3c9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xc58f1f9e05a4f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xe61a18d4013af and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800024a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800024a4]:csrrs a7, fflags, zero<br> [0x800024a8]:fsd ft11, 464(a5)<br> [0x800024ac]:sw a7, 468(a5)<br>   |
| 267|[0x80006be4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc232ce1afdd5d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x64763af91c8ad and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x396f48df10de5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800024c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800024c4]:csrrs a7, fflags, zero<br> [0x800024c8]:fsd ft11, 480(a5)<br> [0x800024cc]:sw a7, 484(a5)<br>   |
| 268|[0x80006bf4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x04e0b27da3cc5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9c9081d6ba08b and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xa46ce1f6a5a39 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800024e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800024e4]:csrrs a7, fflags, zero<br> [0x800024e8]:fsd ft11, 496(a5)<br> [0x800024ec]:sw a7, 500(a5)<br>   |
| 269|[0x80006c04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xce5ebb2a2b181 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x06f43318ba050 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xdaee022114709 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002500]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002504]:csrrs a7, fflags, zero<br> [0x80002508]:fsd ft11, 512(a5)<br> [0x8000250c]:sw a7, 516(a5)<br>   |
| 270|[0x80006c14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x71b6dc9801ef7 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x6b7942aa29dad and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x0676b54059abb and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002520]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002524]:csrrs a7, fflags, zero<br> [0x80002528]:fsd ft11, 528(a5)<br> [0x8000252c]:sw a7, 532(a5)<br>   |
| 271|[0x80006c24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xf9d6388095197 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x31752ce11af56 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2dc7e0735054e and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002540]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002544]:csrrs a7, fflags, zero<br> [0x80002548]:fsd ft11, 544(a5)<br> [0x8000254c]:sw a7, 548(a5)<br>   |
| 272|[0x80006c34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6084b304bf18f and fs2 == 0 and fe2 == 0x3fc and fm2 == 0x0440723dc138b and fs3 == 0 and fe3 == 0x7f9 and fm3 == 0x665f844db4adf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002560]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002564]:csrrs a7, fflags, zero<br> [0x80002568]:fsd ft11, 560(a5)<br> [0x8000256c]:sw a7, 564(a5)<br>   |
| 273|[0x80006c44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf35e30dc7f0d5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x9b93d26abc960 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x916c48fdc3c52 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002580]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002584]:csrrs a7, fflags, zero<br> [0x80002588]:fsd ft11, 576(a5)<br> [0x8000258c]:sw a7, 580(a5)<br>   |
| 274|[0x80006c54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7e3754ab88106 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ca05abdbc274 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x10330b39c61ab and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800025a0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800025a4]:csrrs a7, fflags, zero<br> [0x800025a8]:fsd ft11, 592(a5)<br> [0x800025ac]:sw a7, 596(a5)<br>   |
| 275|[0x80006c64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa2e19c7869ae7 and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xae37b5cd92b9d and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x5ff90e6260e0f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800025c0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800025c4]:csrrs a7, fflags, zero<br> [0x800025c8]:fsd ft11, 608(a5)<br> [0x800025cc]:sw a7, 612(a5)<br>   |
| 276|[0x80006c74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe23b6c26d7d59 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x7f9b8fee36b4a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x694df7f442112 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800025e0]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800025e4]:csrrs a7, fflags, zero<br> [0x800025e8]:fsd ft11, 624(a5)<br> [0x800025ec]:sw a7, 628(a5)<br>   |
| 277|[0x80006c84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x979a4444e4e5b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x4c4cc7050e2ce and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x088b279b4a7a5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002600]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002604]:csrrs a7, fflags, zero<br> [0x80002608]:fsd ft11, 640(a5)<br> [0x8000260c]:sw a7, 644(a5)<br>   |
| 278|[0x80006c94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc9ed4464571af and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xdbdc83df362e4 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa99ad8d852394 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002620]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002624]:csrrs a7, fflags, zero<br> [0x80002628]:fsd ft11, 656(a5)<br> [0x8000262c]:sw a7, 660(a5)<br>   |
| 279|[0x80006ca4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5726b277b5dce and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x53c36b188da64 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc76e305c8d1af and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002640]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002644]:csrrs a7, fflags, zero<br> [0x80002648]:fsd ft11, 672(a5)<br> [0x8000264c]:sw a7, 676(a5)<br>   |
| 280|[0x80006cb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5610c05b31c8 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xd2834dff0917f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc8d5e8a69a864 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002660]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002664]:csrrs a7, fflags, zero<br> [0x80002668]:fsd ft11, 688(a5)<br> [0x8000266c]:sw a7, 692(a5)<br>   |
| 281|[0x80006cc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf642299c3d7ea and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x001214aa3225f and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xf665a2ce400e3 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80002680]:fnmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002684]:csrrs a7, fflags, zero<br> [0x80002688]:fsd ft11, 704(a5)<br> [0x8000268c]:sw a7, 708(a5)<br>   |
