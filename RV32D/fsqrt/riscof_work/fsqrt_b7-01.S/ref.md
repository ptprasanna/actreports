
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000420')]      |
| SIG_REGION                | [('0x80002310', '0x80002410', '64 words')]      |
| COV_LABELS                | fsqrt_b7      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsqrt/riscof_work/fsqrt_b7-01.S/ref.S    |
| Total Number of coverpoints| 73     |
| Total Coverpoints Hit     | 36      |
| Total Signature Updates   | 16      |
| STAT1                     | 16      |
| STAT2                     | 0      |
| STAT3                     | 15     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000029c]:fsqrt.d ft10, fs3, dyn
[0x800002a0]:csrrs a7, fflags, zero
[0x800002a4]:fsd ft10, 256(a5)
[0x800002a8]:sw a7, 260(a5)
[0x800002ac]:fld fs6, 136(a6)
[0x800002b0]:csrrwi zero, frm, 0

[0x800002b4]:fsqrt.d ft6, fs6, dyn
[0x800002b8]:csrrs a7, fflags, zero
[0x800002bc]:fsd ft6, 272(a5)
[0x800002c0]:sw a7, 276(a5)
[0x800002c4]:fld fs5, 144(a6)
[0x800002c8]:csrrwi zero, frm, 0

[0x800002cc]:fsqrt.d fs10, fs5, dyn
[0x800002d0]:csrrs a7, fflags, zero
[0x800002d4]:fsd fs10, 288(a5)
[0x800002d8]:sw a7, 292(a5)
[0x800002dc]:fld fa5, 152(a6)
[0x800002e0]:csrrwi zero, frm, 0

[0x800002e4]:fsqrt.d fs5, fa5, dyn
[0x800002e8]:csrrs a7, fflags, zero
[0x800002ec]:fsd fs5, 304(a5)
[0x800002f0]:sw a7, 308(a5)
[0x800002f4]:fld fs7, 160(a6)
[0x800002f8]:csrrwi zero, frm, 0

[0x800002fc]:fsqrt.d fs9, fs7, dyn
[0x80000300]:csrrs a7, fflags, zero
[0x80000304]:fsd fs9, 320(a5)
[0x80000308]:sw a7, 324(a5)
[0x8000030c]:fld fs8, 168(a6)
[0x80000310]:csrrwi zero, frm, 0

[0x80000314]:fsqrt.d fs4, fs8, dyn
[0x80000318]:csrrs a7, fflags, zero
[0x8000031c]:fsd fs4, 336(a5)
[0x80000320]:sw a7, 340(a5)
[0x80000324]:fld fa6, 176(a6)
[0x80000328]:csrrwi zero, frm, 0

[0x8000032c]:fsqrt.d fs6, fa6, dyn
[0x80000330]:csrrs a7, fflags, zero
[0x80000334]:fsd fs6, 352(a5)
[0x80000338]:sw a7, 356(a5)
[0x8000033c]:fld fa0, 184(a6)
[0x80000340]:csrrwi zero, frm, 0

[0x80000344]:fsqrt.d fs7, fa0, dyn
[0x80000348]:csrrs a7, fflags, zero
[0x8000034c]:fsd fs7, 368(a5)
[0x80000350]:sw a7, 372(a5)
[0x80000354]:fld ft3, 192(a6)
[0x80000358]:csrrwi zero, frm, 0

[0x8000035c]:fsqrt.d fa4, ft3, dyn
[0x80000360]:csrrs a7, fflags, zero
[0x80000364]:fsd fa4, 384(a5)
[0x80000368]:sw a7, 388(a5)
[0x8000036c]:fld fa3, 200(a6)
[0x80000370]:csrrwi zero, frm, 0

[0x80000374]:fsqrt.d ft11, fa3, dyn
[0x80000378]:csrrs a7, fflags, zero
[0x8000037c]:fsd ft11, 400(a5)
[0x80000380]:sw a7, 404(a5)
[0x80000384]:fld ft4, 208(a6)
[0x80000388]:csrrwi zero, frm, 0

[0x8000038c]:fsqrt.d fs3, ft4, dyn
[0x80000390]:csrrs a7, fflags, zero
[0x80000394]:fsd fs3, 416(a5)
[0x80000398]:sw a7, 420(a5)
[0x8000039c]:fld fs10, 216(a6)
[0x800003a0]:csrrwi zero, frm, 0

[0x800003a4]:fsqrt.d ft4, fs10, dyn
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:fsd ft4, 432(a5)
[0x800003b0]:sw a7, 436(a5)
[0x800003b4]:fld fs2, 224(a6)
[0x800003b8]:csrrwi zero, frm, 0

[0x800003bc]:fsqrt.d ft1, fs2, dyn
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:fsd ft1, 448(a5)
[0x800003c8]:sw a7, 452(a5)
[0x800003cc]:fld fs0, 232(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fsqrt.d ft3, fs0, dyn
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:fsd ft3, 464(a5)
[0x800003e0]:sw a7, 468(a5)
[0x800003e4]:fld ft6, 240(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fsqrt.d fa5, ft6, dyn
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:fsd fa5, 480(a5)
[0x800003f8]:sw a7, 484(a5)
[0x800003fc]:fld fa2, 248(a6)
[0x80000400]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                       coverpoints                                                                       |                                                                       code                                                                       |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000|- opcode : fsqrt.d<br> - rs1 : f30<br> - rd : f28<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br> |[0x8000011c]:fsqrt.d ft8, ft10, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd ft8, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>     |
|   2|[0x80002324]<br>0x00000000|- rs1 : f27<br> - rd : f27<br> - rs1 == rd<br>                                                                                                           |[0x80000134]:fsqrt.d fs11, fs11, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd fs11, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br> |
|   3|[0x80002334]<br>0x00000000|- rs1 : f2<br> - rd : f5<br>                                                                                                                             |[0x8000014c]:fsqrt.d ft5, ft2, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd ft5, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>    |
|   4|[0x80002344]<br>0x00000000|- rs1 : f14<br> - rd : f18<br>                                                                                                                           |[0x80000164]:fsqrt.d fs2, fa4, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd fs2, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>    |
|   5|[0x80002354]<br>0x00000000|- rs1 : f9<br> - rd : f10<br>                                                                                                                            |[0x8000017c]:fsqrt.d fa0, fs1, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fa0, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>    |
|   6|[0x80002364]<br>0x00000000|- rs1 : f29<br> - rd : f16<br>                                                                                                                           |[0x80000194]:fsqrt.d fa6, ft9, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd fa6, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>    |
|   7|[0x80002374]<br>0x00000000|- rs1 : f1<br> - rd : f2<br>                                                                                                                             |[0x800001ac]:fsqrt.d ft2, ft1, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft2, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>   |
|   8|[0x80002384]<br>0x00000000|- rs1 : f28<br> - rd : f12<br>                                                                                                                           |[0x800001c4]:fsqrt.d fa2, ft8, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fa2, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>  |
|   9|[0x80002394]<br>0x00000000|- rs1 : f0<br> - rd : f17<br>                                                                                                                            |[0x800001dc]:fsqrt.d fa7, ft0, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd fa7, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>  |
|  10|[0x800023a4]<br>0x00000000|- rs1 : f25<br> - rd : f8<br>                                                                                                                            |[0x800001f4]:fsqrt.d fs0, fs9, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd fs0, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br>  |
|  11|[0x800023b4]<br>0x00000000|- rs1 : f5<br> - rd : f0<br>                                                                                                                             |[0x8000020c]:fsqrt.d ft0, ft5, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd ft0, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>  |
|  12|[0x800023c4]<br>0x00000000|- rs1 : f11<br> - rd : f7<br>                                                                                                                            |[0x80000224]:fsqrt.d ft7, fa1, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft7, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>  |
|  13|[0x800023d4]<br>0x00000000|- rs1 : f31<br> - rd : f24<br>                                                                                                                           |[0x8000023c]:fsqrt.d fs8, ft11, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd fs8, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br> |
|  14|[0x800023e4]<br>0x00000000|- rs1 : f17<br> - rd : f13<br>                                                                                                                           |[0x80000254]:fsqrt.d fa3, fa7, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fa3, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>  |
|  15|[0x800023f4]<br>0x00000000|- rs1 : f20<br> - rd : f9<br>                                                                                                                            |[0x8000026c]:fsqrt.d fs1, fs4, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd fs1, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>  |
|  16|[0x80002404]<br>0x00000000|- rs1 : f7<br> - rd : f29<br>                                                                                                                            |[0x80000284]:fsqrt.d ft9, ft7, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd ft9, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>  |
