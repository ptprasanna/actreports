
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005a0')]      |
| SIG_REGION                | [('0x80002310', '0x80002490', '96 words')]      |
| COV_LABELS                | fsqrt_b2      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsqrt/riscof_work/fsqrt_b2-01.S/ref.S    |
| Total Number of coverpoints| 120     |
| Total Coverpoints Hit     | 75      |
| Total Signature Updates   | 24      |
| STAT1                     | 24      |
| STAT2                     | 0      |
| STAT3                     | 23     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000035c]:fsqrt.d fs5, ft10, dyn
[0x80000360]:csrrs a7, fflags, zero
[0x80000364]:fsd fs5, 384(a5)
[0x80000368]:sw a7, 388(a5)
[0x8000036c]:fld ft5, 200(a6)
[0x80000370]:csrrwi zero, frm, 0

[0x80000374]:fsqrt.d fs9, ft5, dyn
[0x80000378]:csrrs a7, fflags, zero
[0x8000037c]:fsd fs9, 400(a5)
[0x80000380]:sw a7, 404(a5)
[0x80000384]:fld fs3, 208(a6)
[0x80000388]:csrrwi zero, frm, 0

[0x8000038c]:fsqrt.d ft0, fs3, dyn
[0x80000390]:csrrs a7, fflags, zero
[0x80000394]:fsd ft0, 416(a5)
[0x80000398]:sw a7, 420(a5)
[0x8000039c]:fld ft2, 216(a6)
[0x800003a0]:csrrwi zero, frm, 0

[0x800003a4]:fsqrt.d fa4, ft2, dyn
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:fsd fa4, 432(a5)
[0x800003b0]:sw a7, 436(a5)
[0x800003b4]:fld fa0, 224(a6)
[0x800003b8]:csrrwi zero, frm, 0

[0x800003bc]:fsqrt.d fs8, fa0, dyn
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:fsd fs8, 448(a5)
[0x800003c8]:sw a7, 452(a5)
[0x800003cc]:fld fa2, 232(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fsqrt.d fa7, fa2, dyn
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:fsd fa7, 464(a5)
[0x800003e0]:sw a7, 468(a5)
[0x800003e4]:fld fa1, 240(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fsqrt.d fs6, fa1, dyn
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:fsd fs6, 480(a5)
[0x800003f8]:sw a7, 484(a5)
[0x800003fc]:fld fs10, 248(a6)
[0x80000400]:csrrwi zero, frm, 0

[0x80000404]:fsqrt.d ft6, fs10, dyn
[0x80000408]:csrrs a7, fflags, zero
[0x8000040c]:fsd ft6, 496(a5)
[0x80000410]:sw a7, 500(a5)
[0x80000414]:fld ft10, 256(a6)
[0x80000418]:csrrwi zero, frm, 0

[0x8000041c]:fsqrt.d ft11, ft10, dyn
[0x80000420]:csrrs a7, fflags, zero
[0x80000424]:fsd ft11, 512(a5)
[0x80000428]:sw a7, 516(a5)
[0x8000042c]:fld ft10, 264(a6)
[0x80000430]:csrrwi zero, frm, 0

[0x80000434]:fsqrt.d ft11, ft10, dyn
[0x80000438]:csrrs a7, fflags, zero
[0x8000043c]:fsd ft11, 528(a5)
[0x80000440]:sw a7, 532(a5)
[0x80000444]:fld ft10, 272(a6)
[0x80000448]:csrrwi zero, frm, 0

[0x8000044c]:fsqrt.d ft11, ft10, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:fsd ft11, 544(a5)
[0x80000458]:sw a7, 548(a5)
[0x8000045c]:fld ft10, 280(a6)
[0x80000460]:csrrwi zero, frm, 0

[0x80000464]:fsqrt.d ft11, ft10, dyn
[0x80000468]:csrrs a7, fflags, zero
[0x8000046c]:fsd ft11, 560(a5)
[0x80000470]:sw a7, 564(a5)
[0x80000474]:fld ft10, 288(a6)
[0x80000478]:csrrwi zero, frm, 0

[0x8000047c]:fsqrt.d ft11, ft10, dyn
[0x80000480]:csrrs a7, fflags, zero
[0x80000484]:fsd ft11, 576(a5)
[0x80000488]:sw a7, 580(a5)
[0x8000048c]:fld ft10, 296(a6)
[0x80000490]:csrrwi zero, frm, 0

[0x80000494]:fsqrt.d ft11, ft10, dyn
[0x80000498]:csrrs a7, fflags, zero
[0x8000049c]:fsd ft11, 592(a5)
[0x800004a0]:sw a7, 596(a5)
[0x800004a4]:fld ft10, 304(a6)
[0x800004a8]:csrrwi zero, frm, 0

[0x800004ac]:fsqrt.d ft11, ft10, dyn
[0x800004b0]:csrrs a7, fflags, zero
[0x800004b4]:fsd ft11, 608(a5)
[0x800004b8]:sw a7, 612(a5)
[0x800004bc]:fld ft10, 312(a6)
[0x800004c0]:csrrwi zero, frm, 0

[0x800004c4]:fsqrt.d ft11, ft10, dyn
[0x800004c8]:csrrs a7, fflags, zero
[0x800004cc]:fsd ft11, 624(a5)
[0x800004d0]:sw a7, 628(a5)
[0x800004d4]:fld ft10, 320(a6)
[0x800004d8]:csrrwi zero, frm, 0

[0x800004dc]:fsqrt.d ft11, ft10, dyn
[0x800004e0]:csrrs a7, fflags, zero
[0x800004e4]:fsd ft11, 640(a5)
[0x800004e8]:sw a7, 644(a5)
[0x800004ec]:fld ft10, 328(a6)
[0x800004f0]:csrrwi zero, frm, 0

[0x800004f4]:fsqrt.d ft11, ft10, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:fsd ft11, 656(a5)
[0x80000500]:sw a7, 660(a5)
[0x80000504]:fld ft10, 336(a6)
[0x80000508]:csrrwi zero, frm, 0

[0x8000050c]:fsqrt.d ft11, ft10, dyn
[0x80000510]:csrrs a7, fflags, zero
[0x80000514]:fsd ft11, 672(a5)
[0x80000518]:sw a7, 676(a5)
[0x8000051c]:fld ft10, 344(a6)
[0x80000520]:csrrwi zero, frm, 0

[0x80000524]:fsqrt.d ft11, ft10, dyn
[0x80000528]:csrrs a7, fflags, zero
[0x8000052c]:fsd ft11, 688(a5)
[0x80000530]:sw a7, 692(a5)
[0x80000534]:fld ft10, 352(a6)
[0x80000538]:csrrwi zero, frm, 0

[0x8000053c]:fsqrt.d ft11, ft10, dyn
[0x80000540]:csrrs a7, fflags, zero
[0x80000544]:fsd ft11, 704(a5)
[0x80000548]:sw a7, 708(a5)
[0x8000054c]:fld ft10, 360(a6)
[0x80000550]:csrrwi zero, frm, 0

[0x80000554]:fsqrt.d ft11, ft10, dyn
[0x80000558]:csrrs a7, fflags, zero
[0x8000055c]:fsd ft11, 720(a5)
[0x80000560]:sw a7, 724(a5)
[0x80000564]:fld ft10, 368(a6)
[0x80000568]:csrrwi zero, frm, 0

[0x8000056c]:fsqrt.d ft11, ft10, dyn
[0x80000570]:csrrs a7, fflags, zero
[0x80000574]:fsd ft11, 736(a5)
[0x80000578]:sw a7, 740(a5)
[0x8000057c]:fld ft10, 376(a6)
[0x80000580]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                      coverpoints                                                                      |                                                                       code                                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000|- opcode : fsqrt.d<br> - rs1 : f4<br> - rd : f7<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fsqrt.d ft7, ft4, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd ft7, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>       |
|   2|[0x80002324]<br>0x00000000|- rs1 : f20<br> - rd : f20<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                      |[0x80000134]:fsqrt.d fs4, fs4, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd fs4, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>     |
|   3|[0x80002334]<br>0x00000001|- rs1 : f29<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000800000 and rm_val == 0  #nosat<br>                                      |[0x8000014c]:fsqrt.d fs3, ft9, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd fs3, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>     |
|   4|[0x80002344]<br>0x00000001|- rs1 : f3<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000400000 and rm_val == 0  #nosat<br>                                       |[0x80000164]:fsqrt.d ft9, ft3, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft9, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>     |
|   5|[0x80002354]<br>0x00000001|- rs1 : f17<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000200000 and rm_val == 0  #nosat<br>                                      |[0x8000017c]:fsqrt.d ft11, fa7, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd ft11, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>   |
|   6|[0x80002364]<br>0x00000001|- rs1 : f0<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000100000 and rm_val == 0  #nosat<br>                                        |[0x80000194]:fsqrt.d ft5, ft0, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd ft5, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>     |
|   7|[0x80002374]<br>0x00000001|- rs1 : f31<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000080000 and rm_val == 0  #nosat<br>                                       |[0x800001ac]:fsqrt.d ft3, ft11, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft3, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>   |
|   8|[0x80002384]<br>0x00000001|- rs1 : f6<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000040000 and rm_val == 0  #nosat<br>                                       |[0x800001c4]:fsqrt.d fa1, ft6, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fa1, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>   |
|   9|[0x80002394]<br>0x00000001|- rs1 : f23<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000020000 and rm_val == 0  #nosat<br>                                      |[0x800001dc]:fsqrt.d fa5, fs7, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd fa5, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>   |
|  10|[0x800023a4]<br>0x00000001|- rs1 : f22<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000010000 and rm_val == 0  #nosat<br>                                      |[0x800001f4]:fsqrt.d fs10, fs6, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd fs10, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br> |
|  11|[0x800023b4]<br>0x00000001|- rs1 : f8<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000008000 and rm_val == 0  #nosat<br>                                       |[0x8000020c]:fsqrt.d ft10, fs0, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd ft10, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br> |
|  12|[0x800023c4]<br>0x00000001|- rs1 : f9<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000004000 and rm_val == 0  #nosat<br>                                        |[0x80000224]:fsqrt.d ft2, fs1, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft2, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>   |
|  13|[0x800023d4]<br>0x00000001|- rs1 : f27<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000002000 and rm_val == 0  #nosat<br>                                      |[0x8000023c]:fsqrt.d fs2, fs11, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd fs2, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>  |
|  14|[0x800023e4]<br>0x00000001|- rs1 : f21<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000001000 and rm_val == 0  #nosat<br>                                      |[0x80000254]:fsqrt.d ft8, fs5, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd ft8, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>   |
|  15|[0x800023f4]<br>0x00000001|- rs1 : f13<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000800 and rm_val == 0  #nosat<br>                                      |[0x8000026c]:fsqrt.d fa0, fa3, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd fa0, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>   |
|  16|[0x80002404]<br>0x00000001|- rs1 : f15<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000400 and rm_val == 0  #nosat<br>                                       |[0x80000284]:fsqrt.d ft4, fa5, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd ft4, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>   |
|  17|[0x80002414]<br>0x00000001|- rs1 : f7<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000200 and rm_val == 0  #nosat<br>                                       |[0x8000029c]:fsqrt.d fa2, ft7, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsd fa2, 256(a5)<br> [0x800002a8]:sw a7, 260(a5)<br>   |
|  18|[0x80002424]<br>0x00000001|- rs1 : f18<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000100 and rm_val == 0  #nosat<br>                                      |[0x800002b4]:fsqrt.d fs7, fs2, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsd fs7, 272(a5)<br> [0x800002c0]:sw a7, 276(a5)<br>   |
|  19|[0x80002434]<br>0x00000001|- rs1 : f28<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000080 and rm_val == 0  #nosat<br>                                      |[0x800002cc]:fsqrt.d fs11, ft8, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsd fs11, 288(a5)<br> [0x800002d8]:sw a7, 292(a5)<br> |
|  20|[0x80002444]<br>0x00000001|- rs1 : f24<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000040 and rm_val == 0  #nosat<br>                                      |[0x800002e4]:fsqrt.d fa6, fs8, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fa6, 304(a5)<br> [0x800002f0]:sw a7, 308(a5)<br>   |
|  21|[0x80002454]<br>0x00000001|- rs1 : f1<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000020 and rm_val == 0  #nosat<br>                                       |[0x800002fc]:fsqrt.d fa3, ft1, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fa3, 320(a5)<br> [0x80000308]:sw a7, 324(a5)<br>   |
|  22|[0x80002464]<br>0x00000001|- rs1 : f16<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000010 and rm_val == 0  #nosat<br>                                       |[0x80000314]:fsqrt.d ft1, fa6, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsd ft1, 336(a5)<br> [0x80000320]:sw a7, 340(a5)<br>   |
|  23|[0x80002474]<br>0x00000001|- rs1 : f14<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000008 and rm_val == 0  #nosat<br>                                       |[0x8000032c]:fsqrt.d fs0, fa4, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsd fs0, 352(a5)<br> [0x80000338]:sw a7, 356(a5)<br>   |
|  24|[0x80002484]<br>0x00000001|- rs1 : f25<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x3f1 and fm1 == 0x0000000000004 and rm_val == 0  #nosat<br>                                       |[0x80000344]:fsqrt.d fs1, fs9, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fs1, 368(a5)<br> [0x80000350]:sw a7, 372(a5)<br>   |
