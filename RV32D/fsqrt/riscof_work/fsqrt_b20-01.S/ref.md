
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000e00')]      |
| SIG_REGION                | [('0x80002610', '0x80002a60', '276 words')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsqrt/riscof_work/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 209     |
| Total Coverpoints Hit     | 142      |
| Total Signature Updates   | 75      |
| STAT1                     | 75      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000794]:fsqrt.d ft11, ft10, dyn
[0x80000798]:csrrs a7, fflags, zero
[0x8000079c]:fsd ft11, 1104(a5)
[0x800007a0]:sw a7, 1108(a5)
[0x800007a4]:fld ft10, 560(a6)
[0x800007a8]:csrrwi zero, frm, 0

[0x800007ac]:fsqrt.d ft11, ft10, dyn
[0x800007b0]:csrrs a7, fflags, zero
[0x800007b4]:fsd ft11, 1120(a5)
[0x800007b8]:sw a7, 1124(a5)
[0x800007bc]:fld ft10, 568(a6)
[0x800007c0]:csrrwi zero, frm, 0

[0x800007c4]:fsqrt.d ft11, ft10, dyn
[0x800007c8]:csrrs a7, fflags, zero
[0x800007cc]:fsd ft11, 1136(a5)
[0x800007d0]:sw a7, 1140(a5)
[0x800007d4]:fld ft10, 576(a6)
[0x800007d8]:csrrwi zero, frm, 0

[0x800007dc]:fsqrt.d ft11, ft10, dyn
[0x800007e0]:csrrs a7, fflags, zero
[0x800007e4]:fsd ft11, 1152(a5)
[0x800007e8]:sw a7, 1156(a5)
[0x800007ec]:fld ft10, 584(a6)
[0x800007f0]:csrrwi zero, frm, 0

[0x800007f4]:fsqrt.d ft11, ft10, dyn
[0x800007f8]:csrrs a7, fflags, zero
[0x800007fc]:fsd ft11, 1168(a5)
[0x80000800]:sw a7, 1172(a5)
[0x80000804]:fld ft10, 592(a6)
[0x80000808]:csrrwi zero, frm, 0

[0x8000080c]:fsqrt.d ft11, ft10, dyn
[0x80000810]:csrrs a7, fflags, zero
[0x80000814]:fsd ft11, 1184(a5)
[0x80000818]:sw a7, 1188(a5)
[0x8000081c]:fld ft10, 600(a6)
[0x80000820]:csrrwi zero, frm, 0

[0x80000824]:fsqrt.d ft11, ft10, dyn
[0x80000828]:csrrs a7, fflags, zero
[0x8000082c]:fsd ft11, 1200(a5)
[0x80000830]:sw a7, 1204(a5)
[0x80000834]:fld ft10, 608(a6)
[0x80000838]:csrrwi zero, frm, 0

[0x8000083c]:fsqrt.d ft11, ft10, dyn
[0x80000840]:csrrs a7, fflags, zero
[0x80000844]:fsd ft11, 1216(a5)
[0x80000848]:sw a7, 1220(a5)
[0x8000084c]:fld ft10, 616(a6)
[0x80000850]:csrrwi zero, frm, 0

[0x80000854]:fsqrt.d ft11, ft10, dyn
[0x80000858]:csrrs a7, fflags, zero
[0x8000085c]:fsd ft11, 1232(a5)
[0x80000860]:sw a7, 1236(a5)
[0x80000864]:fld ft10, 624(a6)
[0x80000868]:csrrwi zero, frm, 0

[0x8000086c]:fsqrt.d ft11, ft10, dyn
[0x80000870]:csrrs a7, fflags, zero
[0x80000874]:fsd ft11, 1248(a5)
[0x80000878]:sw a7, 1252(a5)
[0x8000087c]:fld ft10, 632(a6)
[0x80000880]:csrrwi zero, frm, 0

[0x80000884]:fsqrt.d ft11, ft10, dyn
[0x80000888]:csrrs a7, fflags, zero
[0x8000088c]:fsd ft11, 1264(a5)
[0x80000890]:sw a7, 1268(a5)
[0x80000894]:fld ft10, 640(a6)
[0x80000898]:csrrwi zero, frm, 0

[0x8000089c]:fsqrt.d ft11, ft10, dyn
[0x800008a0]:csrrs a7, fflags, zero
[0x800008a4]:fsd ft11, 1280(a5)
[0x800008a8]:sw a7, 1284(a5)
[0x800008ac]:fld ft10, 648(a6)
[0x800008b0]:csrrwi zero, frm, 0

[0x800008b4]:fsqrt.d ft11, ft10, dyn
[0x800008b8]:csrrs a7, fflags, zero
[0x800008bc]:fsd ft11, 1296(a5)
[0x800008c0]:sw a7, 1300(a5)
[0x800008c4]:fld ft10, 656(a6)
[0x800008c8]:csrrwi zero, frm, 0

[0x800008cc]:fsqrt.d ft11, ft10, dyn
[0x800008d0]:csrrs a7, fflags, zero
[0x800008d4]:fsd ft11, 1312(a5)
[0x800008d8]:sw a7, 1316(a5)
[0x800008dc]:fld ft10, 664(a6)
[0x800008e0]:csrrwi zero, frm, 0

[0x800008e4]:fsqrt.d ft11, ft10, dyn
[0x800008e8]:csrrs a7, fflags, zero
[0x800008ec]:fsd ft11, 1328(a5)
[0x800008f0]:sw a7, 1332(a5)
[0x800008f4]:fld ft10, 672(a6)
[0x800008f8]:csrrwi zero, frm, 0

[0x800008fc]:fsqrt.d ft11, ft10, dyn
[0x80000900]:csrrs a7, fflags, zero
[0x80000904]:fsd ft11, 1344(a5)
[0x80000908]:sw a7, 1348(a5)
[0x8000090c]:fld ft10, 680(a6)
[0x80000910]:csrrwi zero, frm, 0

[0x80000914]:fsqrt.d ft11, ft10, dyn
[0x80000918]:csrrs a7, fflags, zero
[0x8000091c]:fsd ft11, 1360(a5)
[0x80000920]:sw a7, 1364(a5)
[0x80000924]:fld ft10, 688(a6)
[0x80000928]:csrrwi zero, frm, 0

[0x8000092c]:fsqrt.d ft11, ft10, dyn
[0x80000930]:csrrs a7, fflags, zero
[0x80000934]:fsd ft11, 1376(a5)
[0x80000938]:sw a7, 1380(a5)
[0x8000093c]:fld ft10, 696(a6)
[0x80000940]:csrrwi zero, frm, 0

[0x80000944]:fsqrt.d ft11, ft10, dyn
[0x80000948]:csrrs a7, fflags, zero
[0x8000094c]:fsd ft11, 1392(a5)
[0x80000950]:sw a7, 1396(a5)
[0x80000954]:fld ft10, 704(a6)
[0x80000958]:csrrwi zero, frm, 0

[0x8000095c]:fsqrt.d ft11, ft10, dyn
[0x80000960]:csrrs a7, fflags, zero
[0x80000964]:fsd ft11, 1408(a5)
[0x80000968]:sw a7, 1412(a5)
[0x8000096c]:fld ft10, 712(a6)
[0x80000970]:csrrwi zero, frm, 0

[0x80000974]:fsqrt.d ft11, ft10, dyn
[0x80000978]:csrrs a7, fflags, zero
[0x8000097c]:fsd ft11, 1424(a5)
[0x80000980]:sw a7, 1428(a5)
[0x80000984]:fld ft10, 720(a6)
[0x80000988]:csrrwi zero, frm, 0

[0x8000098c]:fsqrt.d ft11, ft10, dyn
[0x80000990]:csrrs a7, fflags, zero
[0x80000994]:fsd ft11, 1440(a5)
[0x80000998]:sw a7, 1444(a5)
[0x8000099c]:fld ft10, 728(a6)
[0x800009a0]:csrrwi zero, frm, 0

[0x800009a4]:fsqrt.d ft11, ft10, dyn
[0x800009a8]:csrrs a7, fflags, zero
[0x800009ac]:fsd ft11, 1456(a5)
[0x800009b0]:sw a7, 1460(a5)
[0x800009b4]:fld ft10, 736(a6)
[0x800009b8]:csrrwi zero, frm, 0

[0x800009bc]:fsqrt.d ft11, ft10, dyn
[0x800009c0]:csrrs a7, fflags, zero
[0x800009c4]:fsd ft11, 1472(a5)
[0x800009c8]:sw a7, 1476(a5)
[0x800009cc]:fld ft10, 744(a6)
[0x800009d0]:csrrwi zero, frm, 0

[0x800009d4]:fsqrt.d ft11, ft10, dyn
[0x800009d8]:csrrs a7, fflags, zero
[0x800009dc]:fsd ft11, 1488(a5)
[0x800009e0]:sw a7, 1492(a5)
[0x800009e4]:fld ft10, 752(a6)
[0x800009e8]:csrrwi zero, frm, 0

[0x800009ec]:fsqrt.d ft11, ft10, dyn
[0x800009f0]:csrrs a7, fflags, zero
[0x800009f4]:fsd ft11, 1504(a5)
[0x800009f8]:sw a7, 1508(a5)
[0x800009fc]:fld ft10, 760(a6)
[0x80000a00]:csrrwi zero, frm, 0

[0x80000a04]:fsqrt.d ft11, ft10, dyn
[0x80000a08]:csrrs a7, fflags, zero
[0x80000a0c]:fsd ft11, 1520(a5)
[0x80000a10]:sw a7, 1524(a5)
[0x80000a14]:fld ft10, 768(a6)
[0x80000a18]:csrrwi zero, frm, 0

[0x80000a1c]:fsqrt.d ft11, ft10, dyn
[0x80000a20]:csrrs a7, fflags, zero
[0x80000a24]:fsd ft11, 1536(a5)
[0x80000a28]:sw a7, 1540(a5)
[0x80000a2c]:fld ft10, 776(a6)
[0x80000a30]:csrrwi zero, frm, 0

[0x80000a34]:fsqrt.d ft11, ft10, dyn
[0x80000a38]:csrrs a7, fflags, zero
[0x80000a3c]:fsd ft11, 1552(a5)
[0x80000a40]:sw a7, 1556(a5)
[0x80000a44]:fld ft10, 784(a6)
[0x80000a48]:csrrwi zero, frm, 0

[0x80000a4c]:fsqrt.d ft11, ft10, dyn
[0x80000a50]:csrrs a7, fflags, zero
[0x80000a54]:fsd ft11, 1568(a5)
[0x80000a58]:sw a7, 1572(a5)
[0x80000a5c]:fld ft10, 792(a6)
[0x80000a60]:csrrwi zero, frm, 0

[0x80000a64]:fsqrt.d ft11, ft10, dyn
[0x80000a68]:csrrs a7, fflags, zero
[0x80000a6c]:fsd ft11, 1584(a5)
[0x80000a70]:sw a7, 1588(a5)
[0x80000a74]:fld ft10, 800(a6)
[0x80000a78]:csrrwi zero, frm, 0

[0x80000a7c]:fsqrt.d ft11, ft10, dyn
[0x80000a80]:csrrs a7, fflags, zero
[0x80000a84]:fsd ft11, 1600(a5)
[0x80000a88]:sw a7, 1604(a5)
[0x80000a8c]:fld ft10, 808(a6)
[0x80000a90]:csrrwi zero, frm, 0

[0x80000a94]:fsqrt.d ft11, ft10, dyn
[0x80000a98]:csrrs a7, fflags, zero
[0x80000a9c]:fsd ft11, 1616(a5)
[0x80000aa0]:sw a7, 1620(a5)
[0x80000aa4]:fld ft10, 816(a6)
[0x80000aa8]:csrrwi zero, frm, 0

[0x80000aac]:fsqrt.d ft11, ft10, dyn
[0x80000ab0]:csrrs a7, fflags, zero
[0x80000ab4]:fsd ft11, 1632(a5)
[0x80000ab8]:sw a7, 1636(a5)
[0x80000abc]:fld ft10, 824(a6)
[0x80000ac0]:csrrwi zero, frm, 0

[0x80000ac4]:fsqrt.d ft11, ft10, dyn
[0x80000ac8]:csrrs a7, fflags, zero
[0x80000acc]:fsd ft11, 1648(a5)
[0x80000ad0]:sw a7, 1652(a5)
[0x80000ad4]:fld ft10, 832(a6)
[0x80000ad8]:csrrwi zero, frm, 0

[0x80000adc]:fsqrt.d ft11, ft10, dyn
[0x80000ae0]:csrrs a7, fflags, zero
[0x80000ae4]:fsd ft11, 1664(a5)
[0x80000ae8]:sw a7, 1668(a5)
[0x80000aec]:fld ft10, 840(a6)
[0x80000af0]:csrrwi zero, frm, 0

[0x80000af4]:fsqrt.d ft11, ft10, dyn
[0x80000af8]:csrrs a7, fflags, zero
[0x80000afc]:fsd ft11, 1680(a5)
[0x80000b00]:sw a7, 1684(a5)
[0x80000b04]:fld ft10, 848(a6)
[0x80000b08]:csrrwi zero, frm, 0

[0x80000b0c]:fsqrt.d ft11, ft10, dyn
[0x80000b10]:csrrs a7, fflags, zero
[0x80000b14]:fsd ft11, 1696(a5)
[0x80000b18]:sw a7, 1700(a5)
[0x80000b1c]:fld ft10, 856(a6)
[0x80000b20]:csrrwi zero, frm, 0

[0x80000b24]:fsqrt.d ft11, ft10, dyn
[0x80000b28]:csrrs a7, fflags, zero
[0x80000b2c]:fsd ft11, 1712(a5)
[0x80000b30]:sw a7, 1716(a5)
[0x80000b34]:fld ft10, 864(a6)
[0x80000b38]:csrrwi zero, frm, 0

[0x80000b3c]:fsqrt.d ft11, ft10, dyn
[0x80000b40]:csrrs a7, fflags, zero
[0x80000b44]:fsd ft11, 1728(a5)
[0x80000b48]:sw a7, 1732(a5)
[0x80000b4c]:fld ft10, 872(a6)
[0x80000b50]:csrrwi zero, frm, 0

[0x80000b54]:fsqrt.d ft11, ft10, dyn
[0x80000b58]:csrrs a7, fflags, zero
[0x80000b5c]:fsd ft11, 1744(a5)
[0x80000b60]:sw a7, 1748(a5)
[0x80000b64]:fld ft10, 880(a6)
[0x80000b68]:csrrwi zero, frm, 0

[0x80000b6c]:fsqrt.d ft11, ft10, dyn
[0x80000b70]:csrrs a7, fflags, zero
[0x80000b74]:fsd ft11, 1760(a5)
[0x80000b78]:sw a7, 1764(a5)
[0x80000b7c]:fld ft10, 888(a6)
[0x80000b80]:csrrwi zero, frm, 0

[0x80000b84]:fsqrt.d ft11, ft10, dyn
[0x80000b88]:csrrs a7, fflags, zero
[0x80000b8c]:fsd ft11, 1776(a5)
[0x80000b90]:sw a7, 1780(a5)
[0x80000b94]:fld ft10, 896(a6)
[0x80000b98]:csrrwi zero, frm, 0

[0x80000b9c]:fsqrt.d ft11, ft10, dyn
[0x80000ba0]:csrrs a7, fflags, zero
[0x80000ba4]:fsd ft11, 1792(a5)
[0x80000ba8]:sw a7, 1796(a5)
[0x80000bac]:fld ft10, 904(a6)
[0x80000bb0]:csrrwi zero, frm, 0

[0x80000bb4]:fsqrt.d ft11, ft10, dyn
[0x80000bb8]:csrrs a7, fflags, zero
[0x80000bbc]:fsd ft11, 1808(a5)
[0x80000bc0]:sw a7, 1812(a5)
[0x80000bc4]:fld ft10, 912(a6)
[0x80000bc8]:csrrwi zero, frm, 0

[0x80000bcc]:fsqrt.d ft11, ft10, dyn
[0x80000bd0]:csrrs a7, fflags, zero
[0x80000bd4]:fsd ft11, 1824(a5)
[0x80000bd8]:sw a7, 1828(a5)
[0x80000bdc]:fld ft10, 920(a6)
[0x80000be0]:csrrwi zero, frm, 0

[0x80000be4]:fsqrt.d ft11, ft10, dyn
[0x80000be8]:csrrs a7, fflags, zero
[0x80000bec]:fsd ft11, 1840(a5)
[0x80000bf0]:sw a7, 1844(a5)
[0x80000bf4]:fld ft10, 928(a6)
[0x80000bf8]:csrrwi zero, frm, 0

[0x80000bfc]:fsqrt.d ft11, ft10, dyn
[0x80000c00]:csrrs a7, fflags, zero
[0x80000c04]:fsd ft11, 1856(a5)
[0x80000c08]:sw a7, 1860(a5)
[0x80000c0c]:fld ft10, 936(a6)
[0x80000c10]:csrrwi zero, frm, 0

[0x80000c14]:fsqrt.d ft11, ft10, dyn
[0x80000c18]:csrrs a7, fflags, zero
[0x80000c1c]:fsd ft11, 1872(a5)
[0x80000c20]:sw a7, 1876(a5)
[0x80000c24]:fld ft10, 944(a6)
[0x80000c28]:csrrwi zero, frm, 0

[0x80000c2c]:fsqrt.d ft11, ft10, dyn
[0x80000c30]:csrrs a7, fflags, zero
[0x80000c34]:fsd ft11, 1888(a5)
[0x80000c38]:sw a7, 1892(a5)
[0x80000c3c]:fld ft10, 952(a6)
[0x80000c40]:csrrwi zero, frm, 0

[0x80000c44]:fsqrt.d ft11, ft10, dyn
[0x80000c48]:csrrs a7, fflags, zero
[0x80000c4c]:fsd ft11, 1904(a5)
[0x80000c50]:sw a7, 1908(a5)
[0x80000c54]:fld ft10, 960(a6)
[0x80000c58]:csrrwi zero, frm, 0

[0x80000c5c]:fsqrt.d ft11, ft10, dyn
[0x80000c60]:csrrs a7, fflags, zero
[0x80000c64]:fsd ft11, 1920(a5)
[0x80000c68]:sw a7, 1924(a5)
[0x80000c6c]:fld ft10, 968(a6)
[0x80000c70]:csrrwi zero, frm, 0

[0x80000c74]:fsqrt.d ft11, ft10, dyn
[0x80000c78]:csrrs a7, fflags, zero
[0x80000c7c]:fsd ft11, 1936(a5)
[0x80000c80]:sw a7, 1940(a5)
[0x80000c84]:fld ft10, 976(a6)
[0x80000c88]:csrrwi zero, frm, 0

[0x80000c8c]:fsqrt.d ft11, ft10, dyn
[0x80000c90]:csrrs a7, fflags, zero
[0x80000c94]:fsd ft11, 1952(a5)
[0x80000c98]:sw a7, 1956(a5)
[0x80000c9c]:fld ft10, 984(a6)
[0x80000ca0]:csrrwi zero, frm, 0

[0x80000ca4]:fsqrt.d ft11, ft10, dyn
[0x80000ca8]:csrrs a7, fflags, zero
[0x80000cac]:fsd ft11, 1968(a5)
[0x80000cb0]:sw a7, 1972(a5)
[0x80000cb4]:fld ft10, 992(a6)
[0x80000cb8]:csrrwi zero, frm, 0

[0x80000cbc]:fsqrt.d ft11, ft10, dyn
[0x80000cc0]:csrrs a7, fflags, zero
[0x80000cc4]:fsd ft11, 1984(a5)
[0x80000cc8]:sw a7, 1988(a5)
[0x80000ccc]:fld ft10, 1000(a6)
[0x80000cd0]:csrrwi zero, frm, 0

[0x80000cd4]:fsqrt.d ft11, ft10, dyn
[0x80000cd8]:csrrs a7, fflags, zero
[0x80000cdc]:fsd ft11, 2000(a5)
[0x80000ce0]:sw a7, 2004(a5)
[0x80000ce4]:fld ft10, 1008(a6)
[0x80000ce8]:csrrwi zero, frm, 0

[0x80000cec]:fsqrt.d ft11, ft10, dyn
[0x80000cf0]:csrrs a7, fflags, zero
[0x80000cf4]:fsd ft11, 2016(a5)
[0x80000cf8]:sw a7, 2020(a5)
[0x80000cfc]:auipc a5, 2
[0x80000d00]:addi a5, a5, 3340
[0x80000d04]:fld ft10, 1016(a6)
[0x80000d08]:csrrwi zero, frm, 0

[0x80000d9c]:fsqrt.d ft11, ft10, dyn
[0x80000da0]:csrrs a7, fflags, zero
[0x80000da4]:fsd ft11, 96(a5)
[0x80000da8]:sw a7, 100(a5)
[0x80000dac]:fld ft10, 1072(a6)
[0x80000db0]:csrrwi zero, frm, 0

[0x80000db4]:fsqrt.d ft11, ft10, dyn
[0x80000db8]:csrrs a7, fflags, zero
[0x80000dbc]:fsd ft11, 112(a5)
[0x80000dc0]:sw a7, 116(a5)
[0x80000dc4]:fld ft10, 1080(a6)
[0x80000dc8]:csrrwi zero, frm, 0

[0x80000dcc]:fsqrt.d ft11, ft10, dyn
[0x80000dd0]:csrrs a7, fflags, zero
[0x80000dd4]:fsd ft11, 128(a5)
[0x80000dd8]:sw a7, 132(a5)
[0x80000ddc]:fld ft10, 1088(a6)
[0x80000de0]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                      coverpoints                                                                       |                                                                         code                                                                         |
|---:|--------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002614]<br>0x00000000|- opcode : fsqrt.d<br> - rs1 : f7<br> - rd : f16<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fsqrt.d fa6, ft7, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:fsd fa6, 0(a5)<br> [0x80000128]:sw a7, 4(a5)<br>          |
|   2|[0x80002624]<br>0x00000000|- rs1 : f6<br> - rd : f6<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x141 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x80000134]:fsqrt.d ft6, ft6, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:fsd ft6, 16(a5)<br> [0x80000140]:sw a7, 20(a5)<br>        |
|   3|[0x80002634]<br>0x00000000|- rs1 : f2<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x8000014c]:fsqrt.d fa1, ft2, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:fsd fa1, 32(a5)<br> [0x80000158]:sw a7, 36(a5)<br>        |
|   4|[0x80002644]<br>0x00000001|- rs1 : f24<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x1d6 and fm1 == 0xb935452b4bc7c and rm_val == 0  #nosat<br>                                       |[0x80000164]:fsqrt.d ft8, fs8, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft8, 48(a5)<br> [0x80000170]:sw a7, 52(a5)<br>        |
|   5|[0x80002654]<br>0x00000001|- rs1 : f18<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x610 and fm1 == 0x21ccea37c6190 and rm_val == 0  #nosat<br>                                       |[0x8000017c]:fsqrt.d fs9, fs2, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:fsd fs9, 64(a5)<br> [0x80000188]:sw a7, 68(a5)<br>        |
|   6|[0x80002664]<br>0x00000001|- rs1 : f20<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x243 and fm1 == 0x7730427032993 and rm_val == 0  #nosat<br>                                        |[0x80000194]:fsqrt.d ft4, fs4, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:fsd ft4, 80(a5)<br> [0x800001a0]:sw a7, 84(a5)<br>        |
|   7|[0x80002674]<br>0x00000001|- rs1 : f1<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x24a and fm1 == 0x596ffbdcf9515 and rm_val == 0  #nosat<br>                                         |[0x800001ac]:fsqrt.d fs0, ft1, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fs0, 96(a5)<br> [0x800001b8]:sw a7, 100(a5)<br>       |
|   8|[0x80002684]<br>0x00000001|- rs1 : f14<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x0dd and fm1 == 0xb962d97d9e0d3 and rm_val == 0  #nosat<br>                                       |[0x800001c4]:fsqrt.d fs10, fa4, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs10, 112(a5)<br> [0x800001d0]:sw a7, 116(a5)<br>    |
|   9|[0x80002694]<br>0x00000001|- rs1 : f9<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x49e and fm1 == 0x6d6aa7b5d5523 and rm_val == 0  #nosat<br>                                         |[0x800001dc]:fsqrt.d ft3, fs1, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:fsd ft3, 128(a5)<br> [0x800001e8]:sw a7, 132(a5)<br>      |
|  10|[0x800026a4]<br>0x00000001|- rs1 : f19<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x664 and fm1 == 0x1f53f3796faa0 and rm_val == 0  #nosat<br>                                       |[0x800001f4]:fsqrt.d fa0, fs3, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:fsd fa0, 144(a5)<br> [0x80000200]:sw a7, 148(a5)<br>      |
|  11|[0x800026b4]<br>0x00000001|- rs1 : f22<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x220 and fm1 == 0xb615804e82f0b and rm_val == 0  #nosat<br>                                       |[0x8000020c]:fsqrt.d fa2, fs6, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:fsd fa2, 160(a5)<br> [0x80000218]:sw a7, 164(a5)<br>      |
|  12|[0x800026c4]<br>0x00000001|- rs1 : f25<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x1dd and fm1 == 0x00eff45d8a020 and rm_val == 0  #nosat<br>                                       |[0x80000224]:fsqrt.d fs11, fs9, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fs11, 176(a5)<br> [0x80000230]:sw a7, 180(a5)<br>    |
|  13|[0x800026d4]<br>0x00000001|- rs1 : f3<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x4eb and fm1 == 0xa52bfc61f44e1 and rm_val == 0  #nosat<br>                                         |[0x8000023c]:fsqrt.d ft1, ft3, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:fsd ft1, 192(a5)<br> [0x80000248]:sw a7, 196(a5)<br>      |
|  14|[0x800026e4]<br>0x00000001|- rs1 : f29<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x0b8 and fm1 == 0x676d52bcd2ca2 and rm_val == 0  #nosat<br>                                       |[0x80000254]:fsqrt.d fa5, ft9, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fa5, 208(a5)<br> [0x80000260]:sw a7, 212(a5)<br>      |
|  15|[0x800026f4]<br>0x00000001|- rs1 : f26<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x21c and fm1 == 0x1fc5f6573038b and rm_val == 0  #nosat<br>                                        |[0x8000026c]:fsqrt.d ft0, fs10, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:fsd ft0, 224(a5)<br> [0x80000278]:sw a7, 228(a5)<br>     |
|  16|[0x80002704]<br>0x00000001|- rs1 : f27<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x27a and fm1 == 0xfedcb647b5255 and rm_val == 0  #nosat<br>                                       |[0x80000284]:fsqrt.d fa7, fs11, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fa7, 240(a5)<br> [0x80000290]:sw a7, 244(a5)<br>     |
|  17|[0x80002714]<br>0x00000001|- rs1 : f17<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x046 and fm1 == 0x10964a3288ede and rm_val == 0  #nosat<br>                                       |[0x8000029c]:fsqrt.d fs4, fa7, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:fsd fs4, 256(a5)<br> [0x800002a8]:sw a7, 260(a5)<br>      |
|  18|[0x80002724]<br>0x00000001|- rs1 : f0<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x6cc and fm1 == 0xda4837dc75a45 and rm_val == 0  #nosat<br>                                        |[0x800002b4]:fsqrt.d fs3, ft0, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:fsd fs3, 272(a5)<br> [0x800002c0]:sw a7, 276(a5)<br>      |
|  19|[0x80002734]<br>0x00000001|- rs1 : f4<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x379 and fm1 == 0x609ba7e28b6d6 and rm_val == 0  #nosat<br>                                        |[0x800002cc]:fsqrt.d fs6, ft4, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:fsd fs6, 288(a5)<br> [0x800002d8]:sw a7, 292(a5)<br>      |
|  20|[0x80002744]<br>0x00000001|- rs1 : f15<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x010 and fm1 == 0x19147937aef10 and rm_val == 0  #nosat<br>                                       |[0x800002e4]:fsqrt.d fs7, fa5, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd fs7, 304(a5)<br> [0x800002f0]:sw a7, 308(a5)<br>      |
|  21|[0x80002754]<br>0x00000001|- rs1 : f8<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x5cd and fm1 == 0x0415c96d286b2 and rm_val == 0  #nosat<br>                                        |[0x800002fc]:fsqrt.d fs2, fs0, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fs2, 320(a5)<br> [0x80000308]:sw a7, 324(a5)<br>      |
|  22|[0x80002764]<br>0x00000001|- rs1 : f5<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x562 and fm1 == 0xd8a88b54ddbd1 and rm_val == 0  #nosat<br>                                         |[0x80000314]:fsqrt.d ft2, ft5, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:fsd ft2, 336(a5)<br> [0x80000320]:sw a7, 340(a5)<br>      |
|  23|[0x80002774]<br>0x00000001|- rs1 : f11<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x5b4 and fm1 == 0x3b6ba19f71958 and rm_val == 0  #nosat<br>                                        |[0x8000032c]:fsqrt.d ft5, fa1, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:fsd ft5, 352(a5)<br> [0x80000338]:sw a7, 356(a5)<br>      |
|  24|[0x80002784]<br>0x00000001|- rs1 : f28<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x36f and fm1 == 0xb907cc9a3dfc5 and rm_val == 0  #nosat<br>                                       |[0x80000344]:fsqrt.d fs5, ft8, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fs5, 368(a5)<br> [0x80000350]:sw a7, 372(a5)<br>      |
|  25|[0x80002794]<br>0x00000001|- rs1 : f10<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x62a and fm1 == 0xc5b706ee884bd and rm_val == 0  #nosat<br>                                       |[0x8000035c]:fsqrt.d ft9, fa0, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:fsd ft9, 384(a5)<br> [0x80000368]:sw a7, 388(a5)<br>      |
|  26|[0x800027a4]<br>0x00000001|- rs1 : f30<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x6ac and fm1 == 0xa53af1e9e6297 and rm_val == 0  #nosat<br>                                       |[0x80000374]:fsqrt.d fa4, ft10, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:fsd fa4, 400(a5)<br> [0x80000380]:sw a7, 404(a5)<br>     |
|  27|[0x800027b4]<br>0x00000001|- rs1 : f16<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x5f2 and fm1 == 0x2c7839914630c and rm_val == 0  #nosat<br>                                       |[0x8000038c]:fsqrt.d ft10, fa6, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:fsd ft10, 416(a5)<br> [0x80000398]:sw a7, 420(a5)<br>    |
|  28|[0x800027c4]<br>0x00000001|- rs1 : f23<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x68a and fm1 == 0xd3f8d47593f76 and rm_val == 0  #nosat<br>                                        |[0x800003a4]:fsqrt.d ft7, fs7, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft7, 432(a5)<br> [0x800003b0]:sw a7, 436(a5)<br>      |
|  29|[0x800027d4]<br>0x00000001|- rs1 : f31<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x14c and fm1 == 0x3543bca6412dd and rm_val == 0  #nosat<br>                                        |[0x800003bc]:fsqrt.d fs1, ft11, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsd fs1, 448(a5)<br> [0x800003c8]:sw a7, 452(a5)<br>     |
|  30|[0x800027e4]<br>0x00000001|- rs1 : f12<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x390 and fm1 == 0x5ad9865ef1ae8 and rm_val == 0  #nosat<br>                                       |[0x800003d4]:fsqrt.d ft11, fa2, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsd ft11, 464(a5)<br> [0x800003e0]:sw a7, 468(a5)<br>    |
|  31|[0x800027f4]<br>0x00000001|- rs1 : f21<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x27b and fm1 == 0x9ef64199541d0 and rm_val == 0  #nosat<br>                                       |[0x800003ec]:fsqrt.d fa3, fs5, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:fsd fa3, 480(a5)<br> [0x800003f8]:sw a7, 484(a5)<br>      |
|  32|[0x80002804]<br>0x00000001|- rs1 : f13<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x429 and fm1 == 0xf1125eaac3f81 and rm_val == 0  #nosat<br>                                       |[0x80000404]:fsqrt.d fs8, fa3, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fs8, 496(a5)<br> [0x80000410]:sw a7, 500(a5)<br>      |
|  33|[0x80002814]<br>0x00000001|- fs1 == 0 and fe1 == 0x277 and fm1 == 0x8a162e2f42a21 and rm_val == 0  #nosat<br>                                                                      |[0x8000041c]:fsqrt.d ft11, ft10, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:fsd ft11, 512(a5)<br> [0x80000428]:sw a7, 516(a5)<br>   |
|  34|[0x80002824]<br>0x00000001|- fs1 == 0 and fe1 == 0x361 and fm1 == 0x67017bfbb2e14 and rm_val == 0  #nosat<br>                                                                      |[0x80000434]:fsqrt.d ft11, ft10, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:fsd ft11, 528(a5)<br> [0x80000440]:sw a7, 532(a5)<br>   |
|  35|[0x80002834]<br>0x00000001|- fs1 == 0 and fe1 == 0x01e and fm1 == 0x28a5fa032b6d5 and rm_val == 0  #nosat<br>                                                                      |[0x8000044c]:fsqrt.d ft11, ft10, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd ft11, 544(a5)<br> [0x80000458]:sw a7, 548(a5)<br>   |
|  36|[0x80002844]<br>0x00000001|- fs1 == 0 and fe1 == 0x443 and fm1 == 0xbb0584a3e9fb1 and rm_val == 0  #nosat<br>                                                                      |[0x80000464]:fsqrt.d ft11, ft10, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd ft11, 560(a5)<br> [0x80000470]:sw a7, 564(a5)<br>   |
|  37|[0x80002854]<br>0x00000001|- fs1 == 0 and fe1 == 0x4a7 and fm1 == 0x480c0e0c26ad5 and rm_val == 0  #nosat<br>                                                                      |[0x8000047c]:fsqrt.d ft11, ft10, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsd ft11, 576(a5)<br> [0x80000488]:sw a7, 580(a5)<br>   |
|  38|[0x80002864]<br>0x00000001|- fs1 == 0 and fe1 == 0x463 and fm1 == 0x53fdb488151bd and rm_val == 0  #nosat<br>                                                                      |[0x80000494]:fsqrt.d ft11, ft10, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:fsd ft11, 592(a5)<br> [0x800004a0]:sw a7, 596(a5)<br>   |
|  39|[0x80002874]<br>0x00000001|- fs1 == 0 and fe1 == 0x6e6 and fm1 == 0xd658cf235f718 and rm_val == 0  #nosat<br>                                                                      |[0x800004ac]:fsqrt.d ft11, ft10, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:fsd ft11, 608(a5)<br> [0x800004b8]:sw a7, 612(a5)<br>   |
|  40|[0x80002884]<br>0x00000001|- fs1 == 0 and fe1 == 0x2ac and fm1 == 0x903f3a50115bf and rm_val == 0  #nosat<br>                                                                      |[0x800004c4]:fsqrt.d ft11, ft10, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd ft11, 624(a5)<br> [0x800004d0]:sw a7, 628(a5)<br>   |
|  41|[0x80002894]<br>0x00000001|- fs1 == 0 and fe1 == 0x55f and fm1 == 0x831433085a13f and rm_val == 0  #nosat<br>                                                                      |[0x800004dc]:fsqrt.d ft11, ft10, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:fsd ft11, 640(a5)<br> [0x800004e8]:sw a7, 644(a5)<br>   |
|  42|[0x800028a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x6de and fm1 == 0x4a8493263d912 and rm_val == 0  #nosat<br>                                                                      |[0x800004f4]:fsqrt.d ft11, ft10, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 656(a5)<br> [0x80000500]:sw a7, 660(a5)<br>   |
|  43|[0x800028b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x028 and fm1 == 0x50d5c9d17a718 and rm_val == 0  #nosat<br>                                                                      |[0x8000050c]:fsqrt.d ft11, ft10, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:fsd ft11, 672(a5)<br> [0x80000518]:sw a7, 676(a5)<br>   |
|  44|[0x800028c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x3a6 and fm1 == 0x8dea07a7d2f66 and rm_val == 0  #nosat<br>                                                                      |[0x80000524]:fsqrt.d ft11, ft10, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft11, 688(a5)<br> [0x80000530]:sw a7, 692(a5)<br>   |
|  45|[0x800028d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x04d and fm1 == 0xf54d566d3af23 and rm_val == 0  #nosat<br>                                                                      |[0x8000053c]:fsqrt.d ft11, ft10, dyn<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:fsd ft11, 704(a5)<br> [0x80000548]:sw a7, 708(a5)<br>   |
|  46|[0x800028e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x352 and fm1 == 0x68bf7bba24887 and rm_val == 0  #nosat<br>                                                                      |[0x80000554]:fsqrt.d ft11, ft10, dyn<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:fsd ft11, 720(a5)<br> [0x80000560]:sw a7, 724(a5)<br>   |
|  47|[0x800028f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x74733452ff5d7 and rm_val == 0  #nosat<br>                                                                      |[0x8000056c]:fsqrt.d ft11, ft10, dyn<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:fsd ft11, 736(a5)<br> [0x80000578]:sw a7, 740(a5)<br>   |
|  48|[0x80002904]<br>0x00000001|- fs1 == 0 and fe1 == 0x68c and fm1 == 0x4f2b6e728ce0c and rm_val == 0  #nosat<br>                                                                      |[0x80000584]:fsqrt.d ft11, ft10, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 752(a5)<br> [0x80000590]:sw a7, 756(a5)<br>   |
|  49|[0x80002914]<br>0x00000001|- fs1 == 0 and fe1 == 0x15c and fm1 == 0xb61d12a3db43b and rm_val == 0  #nosat<br>                                                                      |[0x8000059c]:fsqrt.d ft11, ft10, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 768(a5)<br> [0x800005a8]:sw a7, 772(a5)<br>   |
|  50|[0x80002924]<br>0x00000001|- fs1 == 0 and fe1 == 0x011 and fm1 == 0x421e71936ce4f and rm_val == 0  #nosat<br>                                                                      |[0x800005b4]:fsqrt.d ft11, ft10, dyn<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:fsd ft11, 784(a5)<br> [0x800005c0]:sw a7, 788(a5)<br>   |
|  51|[0x80002934]<br>0x00000001|- fs1 == 0 and fe1 == 0x253 and fm1 == 0xcf11866f044c6 and rm_val == 0  #nosat<br>                                                                      |[0x800005cc]:fsqrt.d ft11, ft10, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:fsd ft11, 800(a5)<br> [0x800005d8]:sw a7, 804(a5)<br>   |
|  52|[0x80002944]<br>0x00000001|- fs1 == 0 and fe1 == 0x245 and fm1 == 0xe2ded1447a9b2 and rm_val == 0  #nosat<br>                                                                      |[0x800005e4]:fsqrt.d ft11, ft10, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 816(a5)<br> [0x800005f0]:sw a7, 820(a5)<br>   |
|  53|[0x80002954]<br>0x00000001|- fs1 == 0 and fe1 == 0x794 and fm1 == 0xdf650f96fc9dc and rm_val == 0  #nosat<br>                                                                      |[0x800005fc]:fsqrt.d ft11, ft10, dyn<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:fsd ft11, 832(a5)<br> [0x80000608]:sw a7, 836(a5)<br>   |
|  54|[0x80002964]<br>0x00000001|- fs1 == 0 and fe1 == 0x1f7 and fm1 == 0x0c82887c59b71 and rm_val == 0  #nosat<br>                                                                      |[0x80000614]:fsqrt.d ft11, ft10, dyn<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:fsd ft11, 848(a5)<br> [0x80000620]:sw a7, 852(a5)<br>   |
|  55|[0x80002974]<br>0x00000001|- fs1 == 0 and fe1 == 0x56c and fm1 == 0xc901d6ca9fe73 and rm_val == 0  #nosat<br>                                                                      |[0x8000062c]:fsqrt.d ft11, ft10, dyn<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:fsd ft11, 864(a5)<br> [0x80000638]:sw a7, 868(a5)<br>   |
|  56|[0x80002984]<br>0x00000001|- fs1 == 0 and fe1 == 0x36a and fm1 == 0x1a98d1d649d85 and rm_val == 0  #nosat<br>                                                                      |[0x80000644]:fsqrt.d ft11, ft10, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 880(a5)<br> [0x80000650]:sw a7, 884(a5)<br>   |
|  57|[0x80002994]<br>0x00000001|- fs1 == 0 and fe1 == 0x293 and fm1 == 0x1fa2f7bf8a3cd and rm_val == 0  #nosat<br>                                                                      |[0x8000065c]:fsqrt.d ft11, ft10, dyn<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:fsd ft11, 896(a5)<br> [0x80000668]:sw a7, 900(a5)<br>   |
|  58|[0x800029a4]<br>0x00000001|- fs1 == 0 and fe1 == 0x6c4 and fm1 == 0x936ef74f68734 and rm_val == 0  #nosat<br>                                                                      |[0x80000674]:fsqrt.d ft11, ft10, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:fsd ft11, 912(a5)<br> [0x80000680]:sw a7, 916(a5)<br>   |
|  59|[0x800029b4]<br>0x00000001|- fs1 == 0 and fe1 == 0x1f6 and fm1 == 0xddff45305d0a3 and rm_val == 0  #nosat<br>                                                                      |[0x8000068c]:fsqrt.d ft11, ft10, dyn<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:fsd ft11, 928(a5)<br> [0x80000698]:sw a7, 932(a5)<br>   |
|  60|[0x800029c4]<br>0x00000001|- fs1 == 0 and fe1 == 0x70b and fm1 == 0xa9bb9576e08fe and rm_val == 0  #nosat<br>                                                                      |[0x800006a4]:fsqrt.d ft11, ft10, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 944(a5)<br> [0x800006b0]:sw a7, 948(a5)<br>   |
|  61|[0x800029d4]<br>0x00000001|- fs1 == 0 and fe1 == 0x5c5 and fm1 == 0x4051a49e409ef and rm_val == 0  #nosat<br>                                                                      |[0x800006bc]:fsqrt.d ft11, ft10, dyn<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:fsd ft11, 960(a5)<br> [0x800006c8]:sw a7, 964(a5)<br>   |
|  62|[0x800029e4]<br>0x00000001|- fs1 == 0 and fe1 == 0x1dc and fm1 == 0x184be59c54b98 and rm_val == 0  #nosat<br>                                                                      |[0x800006d4]:fsqrt.d ft11, ft10, dyn<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:fsd ft11, 976(a5)<br> [0x800006e0]:sw a7, 980(a5)<br>   |
|  63|[0x800029f4]<br>0x00000001|- fs1 == 0 and fe1 == 0x69d and fm1 == 0x3245461ecff87 and rm_val == 0  #nosat<br>                                                                      |[0x800006ec]:fsqrt.d ft11, ft10, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 992(a5)<br> [0x800006f8]:sw a7, 996(a5)<br>   |
|  64|[0x80002a04]<br>0x00000001|- fs1 == 0 and fe1 == 0x2f2 and fm1 == 0xa186bad3f3b95 and rm_val == 0  #nosat<br>                                                                      |[0x80000704]:fsqrt.d ft11, ft10, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 1008(a5)<br> [0x80000710]:sw a7, 1012(a5)<br> |
|  65|[0x80002a14]<br>0x00000001|- fs1 == 0 and fe1 == 0x216 and fm1 == 0x5901f1856027c and rm_val == 0  #nosat<br>                                                                      |[0x8000071c]:fsqrt.d ft11, ft10, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:fsd ft11, 1024(a5)<br> [0x80000728]:sw a7, 1028(a5)<br> |
|  66|[0x80002a24]<br>0x00000001|- fs1 == 0 and fe1 == 0x0f6 and fm1 == 0x0e8dcc21fc0dc and rm_val == 0  #nosat<br>                                                                      |[0x80000734]:fsqrt.d ft11, ft10, dyn<br> [0x80000738]:csrrs a7, fflags, zero<br> [0x8000073c]:fsd ft11, 1040(a5)<br> [0x80000740]:sw a7, 1044(a5)<br> |
|  67|[0x80002a34]<br>0x00000001|- fs1 == 0 and fe1 == 0x6b8 and fm1 == 0x28048e71f9d08 and rm_val == 0  #nosat<br>                                                                      |[0x8000074c]:fsqrt.d ft11, ft10, dyn<br> [0x80000750]:csrrs a7, fflags, zero<br> [0x80000754]:fsd ft11, 1056(a5)<br> [0x80000758]:sw a7, 1060(a5)<br> |
|  68|[0x80002a44]<br>0x00000001|- fs1 == 0 and fe1 == 0x227 and fm1 == 0x127f90c3b9090 and rm_val == 0  #nosat<br>                                                                      |[0x80000764]:fsqrt.d ft11, ft10, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 1072(a5)<br> [0x80000770]:sw a7, 1076(a5)<br> |
|  69|[0x80002a54]<br>0x00000001|- fs1 == 0 and fe1 == 0x076 and fm1 == 0x033274a480488 and rm_val == 0  #nosat<br>                                                                      |[0x8000077c]:fsqrt.d ft11, ft10, dyn<br> [0x80000780]:csrrs a7, fflags, zero<br> [0x80000784]:fsd ft11, 1088(a5)<br> [0x80000788]:sw a7, 1092(a5)<br> |
|  70|[0x80002a0c]<br>0x00000001|- fs1 == 0 and fe1 == 0x11a and fm1 == 0xd120000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d0c]:fsqrt.d ft11, ft10, dyn<br> [0x80000d10]:csrrs a7, fflags, zero<br> [0x80000d14]:fsd ft11, 0(a5)<br> [0x80000d18]:sw a7, 4(a5)<br>       |
|  71|[0x80002a1c]<br>0x00000001|- fs1 == 0 and fe1 == 0x1d3 and fm1 == 0x2100000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d24]:fsqrt.d ft11, ft10, dyn<br> [0x80000d28]:csrrs a7, fflags, zero<br> [0x80000d2c]:fsd ft11, 16(a5)<br> [0x80000d30]:sw a7, 20(a5)<br>     |
|  72|[0x80002a2c]<br>0x00000001|- fs1 == 0 and fe1 == 0x40a and fm1 == 0x0880000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d3c]:fsqrt.d ft11, ft10, dyn<br> [0x80000d40]:csrrs a7, fflags, zero<br> [0x80000d44]:fsd ft11, 32(a5)<br> [0x80000d48]:sw a7, 36(a5)<br>     |
|  73|[0x80002a3c]<br>0x00000001|- fs1 == 0 and fe1 == 0x4b5 and fm1 == 0xb900000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d54]:fsqrt.d ft11, ft10, dyn<br> [0x80000d58]:csrrs a7, fflags, zero<br> [0x80000d5c]:fsd ft11, 48(a5)<br> [0x80000d60]:sw a7, 52(a5)<br>     |
|  74|[0x80002a4c]<br>0x00000001|- fs1 == 0 and fe1 == 0x426 and fm1 == 0xe080000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d6c]:fsqrt.d ft11, ft10, dyn<br> [0x80000d70]:csrrs a7, fflags, zero<br> [0x80000d74]:fsd ft11, 64(a5)<br> [0x80000d78]:sw a7, 68(a5)<br>     |
|  75|[0x80002a5c]<br>0x00000001|- fs1 == 0 and fe1 == 0x792 and fm1 == 0xc200000000000 and rm_val == 0  #nosat<br>                                                                      |[0x80000d84]:fsqrt.d ft11, ft10, dyn<br> [0x80000d88]:csrrs a7, fflags, zero<br> [0x80000d8c]:fsd ft11, 80(a5)<br> [0x80000d90]:sw a7, 84(a5)<br>     |
