
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002630')]      |
| SIG_REGION                | [('0x80005710', '0x800061a0', '676 words')]      |
| COV_LABELS                | fadd_b7      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fadd/riscof_work/fadd_b7-01.S/ref.S    |
| Total Number of coverpoints| 444     |
| Total Coverpoints Hit     | 375      |
| Total Signature Updates   | 275      |
| STAT1                     | 275      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001aa8]:fadd.d ft11, ft10, ft9, dyn
[0x80001aac]:csrrs a7, fflags, zero
[0x80001ab0]:fsd ft11, 1696(a5)
[0x80001ab4]:sw a7, 1700(a5)
[0x80001ab8]:fld ft10, 1712(a6)
[0x80001abc]:fld ft9, 1720(a6)
[0x80001ac0]:csrrwi zero, frm, 3

[0x80001ac4]:fadd.d ft11, ft10, ft9, dyn
[0x80001ac8]:csrrs a7, fflags, zero
[0x80001acc]:fsd ft11, 1712(a5)
[0x80001ad0]:sw a7, 1716(a5)
[0x80001ad4]:fld ft10, 1728(a6)
[0x80001ad8]:fld ft9, 1736(a6)
[0x80001adc]:csrrwi zero, frm, 3

[0x80001ae0]:fadd.d ft11, ft10, ft9, dyn
[0x80001ae4]:csrrs a7, fflags, zero
[0x80001ae8]:fsd ft11, 1728(a5)
[0x80001aec]:sw a7, 1732(a5)
[0x80001af0]:fld ft10, 1744(a6)
[0x80001af4]:fld ft9, 1752(a6)
[0x80001af8]:csrrwi zero, frm, 3

[0x80001afc]:fadd.d ft11, ft10, ft9, dyn
[0x80001b00]:csrrs a7, fflags, zero
[0x80001b04]:fsd ft11, 1744(a5)
[0x80001b08]:sw a7, 1748(a5)
[0x80001b0c]:fld ft10, 1760(a6)
[0x80001b10]:fld ft9, 1768(a6)
[0x80001b14]:csrrwi zero, frm, 3

[0x80001b18]:fadd.d ft11, ft10, ft9, dyn
[0x80001b1c]:csrrs a7, fflags, zero
[0x80001b20]:fsd ft11, 1760(a5)
[0x80001b24]:sw a7, 1764(a5)
[0x80001b28]:fld ft10, 1776(a6)
[0x80001b2c]:fld ft9, 1784(a6)
[0x80001b30]:csrrwi zero, frm, 3

[0x80001b34]:fadd.d ft11, ft10, ft9, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1776(a5)
[0x80001b40]:sw a7, 1780(a5)
[0x80001b44]:fld ft10, 1792(a6)
[0x80001b48]:fld ft9, 1800(a6)
[0x80001b4c]:csrrwi zero, frm, 3

[0x80001b50]:fadd.d ft11, ft10, ft9, dyn
[0x80001b54]:csrrs a7, fflags, zero
[0x80001b58]:fsd ft11, 1792(a5)
[0x80001b5c]:sw a7, 1796(a5)
[0x80001b60]:fld ft10, 1808(a6)
[0x80001b64]:fld ft9, 1816(a6)
[0x80001b68]:csrrwi zero, frm, 3

[0x80001b6c]:fadd.d ft11, ft10, ft9, dyn
[0x80001b70]:csrrs a7, fflags, zero
[0x80001b74]:fsd ft11, 1808(a5)
[0x80001b78]:sw a7, 1812(a5)
[0x80001b7c]:fld ft10, 1824(a6)
[0x80001b80]:fld ft9, 1832(a6)
[0x80001b84]:csrrwi zero, frm, 3

[0x80001b88]:fadd.d ft11, ft10, ft9, dyn
[0x80001b8c]:csrrs a7, fflags, zero
[0x80001b90]:fsd ft11, 1824(a5)
[0x80001b94]:sw a7, 1828(a5)
[0x80001b98]:fld ft10, 1840(a6)
[0x80001b9c]:fld ft9, 1848(a6)
[0x80001ba0]:csrrwi zero, frm, 3

[0x80001ba4]:fadd.d ft11, ft10, ft9, dyn
[0x80001ba8]:csrrs a7, fflags, zero
[0x80001bac]:fsd ft11, 1840(a5)
[0x80001bb0]:sw a7, 1844(a5)
[0x80001bb4]:fld ft10, 1856(a6)
[0x80001bb8]:fld ft9, 1864(a6)
[0x80001bbc]:csrrwi zero, frm, 3

[0x80001bc0]:fadd.d ft11, ft10, ft9, dyn
[0x80001bc4]:csrrs a7, fflags, zero
[0x80001bc8]:fsd ft11, 1856(a5)
[0x80001bcc]:sw a7, 1860(a5)
[0x80001bd0]:fld ft10, 1872(a6)
[0x80001bd4]:fld ft9, 1880(a6)
[0x80001bd8]:csrrwi zero, frm, 3

[0x80001bdc]:fadd.d ft11, ft10, ft9, dyn
[0x80001be0]:csrrs a7, fflags, zero
[0x80001be4]:fsd ft11, 1872(a5)
[0x80001be8]:sw a7, 1876(a5)
[0x80001bec]:fld ft10, 1888(a6)
[0x80001bf0]:fld ft9, 1896(a6)
[0x80001bf4]:csrrwi zero, frm, 3

[0x80001bf8]:fadd.d ft11, ft10, ft9, dyn
[0x80001bfc]:csrrs a7, fflags, zero
[0x80001c00]:fsd ft11, 1888(a5)
[0x80001c04]:sw a7, 1892(a5)
[0x80001c08]:fld ft10, 1904(a6)
[0x80001c0c]:fld ft9, 1912(a6)
[0x80001c10]:csrrwi zero, frm, 3

[0x80001c14]:fadd.d ft11, ft10, ft9, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1904(a5)
[0x80001c20]:sw a7, 1908(a5)
[0x80001c24]:fld ft10, 1920(a6)
[0x80001c28]:fld ft9, 1928(a6)
[0x80001c2c]:csrrwi zero, frm, 3

[0x80001c30]:fadd.d ft11, ft10, ft9, dyn
[0x80001c34]:csrrs a7, fflags, zero
[0x80001c38]:fsd ft11, 1920(a5)
[0x80001c3c]:sw a7, 1924(a5)
[0x80001c40]:fld ft10, 1936(a6)
[0x80001c44]:fld ft9, 1944(a6)
[0x80001c48]:csrrwi zero, frm, 3

[0x80001c4c]:fadd.d ft11, ft10, ft9, dyn
[0x80001c50]:csrrs a7, fflags, zero
[0x80001c54]:fsd ft11, 1936(a5)
[0x80001c58]:sw a7, 1940(a5)
[0x80001c5c]:fld ft10, 1952(a6)
[0x80001c60]:fld ft9, 1960(a6)
[0x80001c64]:csrrwi zero, frm, 3

[0x80001c68]:fadd.d ft11, ft10, ft9, dyn
[0x80001c6c]:csrrs a7, fflags, zero
[0x80001c70]:fsd ft11, 1952(a5)
[0x80001c74]:sw a7, 1956(a5)
[0x80001c78]:fld ft10, 1968(a6)
[0x80001c7c]:fld ft9, 1976(a6)
[0x80001c80]:csrrwi zero, frm, 3

[0x80001c84]:fadd.d ft11, ft10, ft9, dyn
[0x80001c88]:csrrs a7, fflags, zero
[0x80001c8c]:fsd ft11, 1968(a5)
[0x80001c90]:sw a7, 1972(a5)
[0x80001c94]:fld ft10, 1984(a6)
[0x80001c98]:fld ft9, 1992(a6)
[0x80001c9c]:csrrwi zero, frm, 3

[0x80001ca0]:fadd.d ft11, ft10, ft9, dyn
[0x80001ca4]:csrrs a7, fflags, zero
[0x80001ca8]:fsd ft11, 1984(a5)
[0x80001cac]:sw a7, 1988(a5)
[0x80001cb0]:fld ft10, 2000(a6)
[0x80001cb4]:fld ft9, 2008(a6)
[0x80001cb8]:csrrwi zero, frm, 3

[0x80001cbc]:fadd.d ft11, ft10, ft9, dyn
[0x80001cc0]:csrrs a7, fflags, zero
[0x80001cc4]:fsd ft11, 2000(a5)
[0x80001cc8]:sw a7, 2004(a5)
[0x80001ccc]:fld ft10, 2016(a6)
[0x80001cd0]:fld ft9, 2024(a6)
[0x80001cd4]:csrrwi zero, frm, 3

[0x80001cd8]:fadd.d ft11, ft10, ft9, dyn
[0x80001cdc]:csrrs a7, fflags, zero
[0x80001ce0]:fsd ft11, 2016(a5)
[0x80001ce4]:sw a7, 2020(a5)
[0x80001ce8]:addi a6, a6, 2032
[0x80001cec]:auipc a5, 4
[0x80001cf0]:addi a5, a5, 532
[0x80001cf4]:fld ft10, 0(a6)
[0x80001cf8]:fld ft9, 8(a6)
[0x80001cfc]:csrrwi zero, frm, 3

[0x80002198]:fadd.d ft11, ft10, ft9, dyn
[0x8000219c]:csrrs a7, fflags, zero
[0x800021a0]:fsd ft11, 672(a5)
[0x800021a4]:sw a7, 676(a5)
[0x800021a8]:fld ft10, 688(a6)
[0x800021ac]:fld ft9, 696(a6)
[0x800021b0]:csrrwi zero, frm, 3

[0x800021b4]:fadd.d ft11, ft10, ft9, dyn
[0x800021b8]:csrrs a7, fflags, zero
[0x800021bc]:fsd ft11, 688(a5)
[0x800021c0]:sw a7, 692(a5)
[0x800021c4]:fld ft10, 704(a6)
[0x800021c8]:fld ft9, 712(a6)
[0x800021cc]:csrrwi zero, frm, 3

[0x800021d0]:fadd.d ft11, ft10, ft9, dyn
[0x800021d4]:csrrs a7, fflags, zero
[0x800021d8]:fsd ft11, 704(a5)
[0x800021dc]:sw a7, 708(a5)
[0x800021e0]:fld ft10, 720(a6)
[0x800021e4]:fld ft9, 728(a6)
[0x800021e8]:csrrwi zero, frm, 3

[0x800021ec]:fadd.d ft11, ft10, ft9, dyn
[0x800021f0]:csrrs a7, fflags, zero
[0x800021f4]:fsd ft11, 720(a5)
[0x800021f8]:sw a7, 724(a5)
[0x800021fc]:fld ft10, 736(a6)
[0x80002200]:fld ft9, 744(a6)
[0x80002204]:csrrwi zero, frm, 3

[0x80002208]:fadd.d ft11, ft10, ft9, dyn
[0x8000220c]:csrrs a7, fflags, zero
[0x80002210]:fsd ft11, 736(a5)
[0x80002214]:sw a7, 740(a5)
[0x80002218]:fld ft10, 752(a6)
[0x8000221c]:fld ft9, 760(a6)
[0x80002220]:csrrwi zero, frm, 3

[0x80002224]:fadd.d ft11, ft10, ft9, dyn
[0x80002228]:csrrs a7, fflags, zero
[0x8000222c]:fsd ft11, 752(a5)
[0x80002230]:sw a7, 756(a5)
[0x80002234]:fld ft10, 768(a6)
[0x80002238]:fld ft9, 776(a6)
[0x8000223c]:csrrwi zero, frm, 3

[0x80002240]:fadd.d ft11, ft10, ft9, dyn
[0x80002244]:csrrs a7, fflags, zero
[0x80002248]:fsd ft11, 768(a5)
[0x8000224c]:sw a7, 772(a5)
[0x80002250]:fld ft10, 784(a6)
[0x80002254]:fld ft9, 792(a6)
[0x80002258]:csrrwi zero, frm, 3

[0x8000225c]:fadd.d ft11, ft10, ft9, dyn
[0x80002260]:csrrs a7, fflags, zero
[0x80002264]:fsd ft11, 784(a5)
[0x80002268]:sw a7, 788(a5)
[0x8000226c]:fld ft10, 800(a6)
[0x80002270]:fld ft9, 808(a6)
[0x80002274]:csrrwi zero, frm, 3

[0x80002278]:fadd.d ft11, ft10, ft9, dyn
[0x8000227c]:csrrs a7, fflags, zero
[0x80002280]:fsd ft11, 800(a5)
[0x80002284]:sw a7, 804(a5)
[0x80002288]:fld ft10, 816(a6)
[0x8000228c]:fld ft9, 824(a6)
[0x80002290]:csrrwi zero, frm, 3

[0x80002294]:fadd.d ft11, ft10, ft9, dyn
[0x80002298]:csrrs a7, fflags, zero
[0x8000229c]:fsd ft11, 816(a5)
[0x800022a0]:sw a7, 820(a5)
[0x800022a4]:fld ft10, 832(a6)
[0x800022a8]:fld ft9, 840(a6)
[0x800022ac]:csrrwi zero, frm, 3

[0x800022b0]:fadd.d ft11, ft10, ft9, dyn
[0x800022b4]:csrrs a7, fflags, zero
[0x800022b8]:fsd ft11, 832(a5)
[0x800022bc]:sw a7, 836(a5)
[0x800022c0]:fld ft10, 848(a6)
[0x800022c4]:fld ft9, 856(a6)
[0x800022c8]:csrrwi zero, frm, 3

[0x800022cc]:fadd.d ft11, ft10, ft9, dyn
[0x800022d0]:csrrs a7, fflags, zero
[0x800022d4]:fsd ft11, 848(a5)
[0x800022d8]:sw a7, 852(a5)
[0x800022dc]:fld ft10, 864(a6)
[0x800022e0]:fld ft9, 872(a6)
[0x800022e4]:csrrwi zero, frm, 3

[0x800022e8]:fadd.d ft11, ft10, ft9, dyn
[0x800022ec]:csrrs a7, fflags, zero
[0x800022f0]:fsd ft11, 864(a5)
[0x800022f4]:sw a7, 868(a5)
[0x800022f8]:fld ft10, 880(a6)
[0x800022fc]:fld ft9, 888(a6)
[0x80002300]:csrrwi zero, frm, 3

[0x80002304]:fadd.d ft11, ft10, ft9, dyn
[0x80002308]:csrrs a7, fflags, zero
[0x8000230c]:fsd ft11, 880(a5)
[0x80002310]:sw a7, 884(a5)
[0x80002314]:fld ft10, 896(a6)
[0x80002318]:fld ft9, 904(a6)
[0x8000231c]:csrrwi zero, frm, 3

[0x80002320]:fadd.d ft11, ft10, ft9, dyn
[0x80002324]:csrrs a7, fflags, zero
[0x80002328]:fsd ft11, 896(a5)
[0x8000232c]:sw a7, 900(a5)
[0x80002330]:fld ft10, 912(a6)
[0x80002334]:fld ft9, 920(a6)
[0x80002338]:csrrwi zero, frm, 3

[0x8000233c]:fadd.d ft11, ft10, ft9, dyn
[0x80002340]:csrrs a7, fflags, zero
[0x80002344]:fsd ft11, 912(a5)
[0x80002348]:sw a7, 916(a5)
[0x8000234c]:fld ft10, 928(a6)
[0x80002350]:fld ft9, 936(a6)
[0x80002354]:csrrwi zero, frm, 3

[0x80002358]:fadd.d ft11, ft10, ft9, dyn
[0x8000235c]:csrrs a7, fflags, zero
[0x80002360]:fsd ft11, 928(a5)
[0x80002364]:sw a7, 932(a5)
[0x80002368]:fld ft10, 944(a6)
[0x8000236c]:fld ft9, 952(a6)
[0x80002370]:csrrwi zero, frm, 3

[0x80002374]:fadd.d ft11, ft10, ft9, dyn
[0x80002378]:csrrs a7, fflags, zero
[0x8000237c]:fsd ft11, 944(a5)
[0x80002380]:sw a7, 948(a5)
[0x80002384]:fld ft10, 960(a6)
[0x80002388]:fld ft9, 968(a6)
[0x8000238c]:csrrwi zero, frm, 3

[0x80002390]:fadd.d ft11, ft10, ft9, dyn
[0x80002394]:csrrs a7, fflags, zero
[0x80002398]:fsd ft11, 960(a5)
[0x8000239c]:sw a7, 964(a5)
[0x800023a0]:fld ft10, 976(a6)
[0x800023a4]:fld ft9, 984(a6)
[0x800023a8]:csrrwi zero, frm, 3

[0x800023ac]:fadd.d ft11, ft10, ft9, dyn
[0x800023b0]:csrrs a7, fflags, zero
[0x800023b4]:fsd ft11, 976(a5)
[0x800023b8]:sw a7, 980(a5)
[0x800023bc]:fld ft10, 992(a6)
[0x800023c0]:fld ft9, 1000(a6)
[0x800023c4]:csrrwi zero, frm, 3

[0x800023c8]:fadd.d ft11, ft10, ft9, dyn
[0x800023cc]:csrrs a7, fflags, zero
[0x800023d0]:fsd ft11, 992(a5)
[0x800023d4]:sw a7, 996(a5)
[0x800023d8]:fld ft10, 1008(a6)
[0x800023dc]:fld ft9, 1016(a6)
[0x800023e0]:csrrwi zero, frm, 3

[0x800023e4]:fadd.d ft11, ft10, ft9, dyn
[0x800023e8]:csrrs a7, fflags, zero
[0x800023ec]:fsd ft11, 1008(a5)
[0x800023f0]:sw a7, 1012(a5)
[0x800023f4]:fld ft10, 1024(a6)
[0x800023f8]:fld ft9, 1032(a6)
[0x800023fc]:csrrwi zero, frm, 3

[0x80002400]:fadd.d ft11, ft10, ft9, dyn
[0x80002404]:csrrs a7, fflags, zero
[0x80002408]:fsd ft11, 1024(a5)
[0x8000240c]:sw a7, 1028(a5)
[0x80002410]:fld ft10, 1040(a6)
[0x80002414]:fld ft9, 1048(a6)
[0x80002418]:csrrwi zero, frm, 3

[0x8000241c]:fadd.d ft11, ft10, ft9, dyn
[0x80002420]:csrrs a7, fflags, zero
[0x80002424]:fsd ft11, 1040(a5)
[0x80002428]:sw a7, 1044(a5)
[0x8000242c]:fld ft10, 1056(a6)
[0x80002430]:fld ft9, 1064(a6)
[0x80002434]:csrrwi zero, frm, 3

[0x80002438]:fadd.d ft11, ft10, ft9, dyn
[0x8000243c]:csrrs a7, fflags, zero
[0x80002440]:fsd ft11, 1056(a5)
[0x80002444]:sw a7, 1060(a5)
[0x80002448]:fld ft10, 1072(a6)
[0x8000244c]:fld ft9, 1080(a6)
[0x80002450]:csrrwi zero, frm, 3

[0x80002454]:fadd.d ft11, ft10, ft9, dyn
[0x80002458]:csrrs a7, fflags, zero
[0x8000245c]:fsd ft11, 1072(a5)
[0x80002460]:sw a7, 1076(a5)
[0x80002464]:fld ft10, 1088(a6)
[0x80002468]:fld ft9, 1096(a6)
[0x8000246c]:csrrwi zero, frm, 3

[0x80002470]:fadd.d ft11, ft10, ft9, dyn
[0x80002474]:csrrs a7, fflags, zero
[0x80002478]:fsd ft11, 1088(a5)
[0x8000247c]:sw a7, 1092(a5)
[0x80002480]:fld ft10, 1104(a6)
[0x80002484]:fld ft9, 1112(a6)
[0x80002488]:csrrwi zero, frm, 3

[0x8000248c]:fadd.d ft11, ft10, ft9, dyn
[0x80002490]:csrrs a7, fflags, zero
[0x80002494]:fsd ft11, 1104(a5)
[0x80002498]:sw a7, 1108(a5)
[0x8000249c]:fld ft10, 1120(a6)
[0x800024a0]:fld ft9, 1128(a6)
[0x800024a4]:csrrwi zero, frm, 3

[0x800024a8]:fadd.d ft11, ft10, ft9, dyn
[0x800024ac]:csrrs a7, fflags, zero
[0x800024b0]:fsd ft11, 1120(a5)
[0x800024b4]:sw a7, 1124(a5)
[0x800024b8]:fld ft10, 1136(a6)
[0x800024bc]:fld ft9, 1144(a6)
[0x800024c0]:csrrwi zero, frm, 3

[0x800024c4]:fadd.d ft11, ft10, ft9, dyn
[0x800024c8]:csrrs a7, fflags, zero
[0x800024cc]:fsd ft11, 1136(a5)
[0x800024d0]:sw a7, 1140(a5)
[0x800024d4]:fld ft10, 1152(a6)
[0x800024d8]:fld ft9, 1160(a6)
[0x800024dc]:csrrwi zero, frm, 3

[0x800024e0]:fadd.d ft11, ft10, ft9, dyn
[0x800024e4]:csrrs a7, fflags, zero
[0x800024e8]:fsd ft11, 1152(a5)
[0x800024ec]:sw a7, 1156(a5)
[0x800024f0]:fld ft10, 1168(a6)
[0x800024f4]:fld ft9, 1176(a6)
[0x800024f8]:csrrwi zero, frm, 3

[0x800024fc]:fadd.d ft11, ft10, ft9, dyn
[0x80002500]:csrrs a7, fflags, zero
[0x80002504]:fsd ft11, 1168(a5)
[0x80002508]:sw a7, 1172(a5)
[0x8000250c]:fld ft10, 1184(a6)
[0x80002510]:fld ft9, 1192(a6)
[0x80002514]:csrrwi zero, frm, 3

[0x80002518]:fadd.d ft11, ft10, ft9, dyn
[0x8000251c]:csrrs a7, fflags, zero
[0x80002520]:fsd ft11, 1184(a5)
[0x80002524]:sw a7, 1188(a5)
[0x80002528]:fld ft10, 1200(a6)
[0x8000252c]:fld ft9, 1208(a6)
[0x80002530]:csrrwi zero, frm, 3

[0x80002534]:fadd.d ft11, ft10, ft9, dyn
[0x80002538]:csrrs a7, fflags, zero
[0x8000253c]:fsd ft11, 1200(a5)
[0x80002540]:sw a7, 1204(a5)
[0x80002544]:fld ft10, 1216(a6)
[0x80002548]:fld ft9, 1224(a6)
[0x8000254c]:csrrwi zero, frm, 3

[0x80002550]:fadd.d ft11, ft10, ft9, dyn
[0x80002554]:csrrs a7, fflags, zero
[0x80002558]:fsd ft11, 1216(a5)
[0x8000255c]:sw a7, 1220(a5)
[0x80002560]:fld ft10, 1232(a6)
[0x80002564]:fld ft9, 1240(a6)
[0x80002568]:csrrwi zero, frm, 3

[0x8000256c]:fadd.d ft11, ft10, ft9, dyn
[0x80002570]:csrrs a7, fflags, zero
[0x80002574]:fsd ft11, 1232(a5)
[0x80002578]:sw a7, 1236(a5)
[0x8000257c]:fld ft10, 1248(a6)
[0x80002580]:fld ft9, 1256(a6)
[0x80002584]:csrrwi zero, frm, 3

[0x80002588]:fadd.d ft11, ft10, ft9, dyn
[0x8000258c]:csrrs a7, fflags, zero
[0x80002590]:fsd ft11, 1248(a5)
[0x80002594]:sw a7, 1252(a5)
[0x80002598]:fld ft10, 1264(a6)
[0x8000259c]:fld ft9, 1272(a6)
[0x800025a0]:csrrwi zero, frm, 3

[0x800025a4]:fadd.d ft11, ft10, ft9, dyn
[0x800025a8]:csrrs a7, fflags, zero
[0x800025ac]:fsd ft11, 1264(a5)
[0x800025b0]:sw a7, 1268(a5)
[0x800025b4]:fld ft10, 1280(a6)
[0x800025b8]:fld ft9, 1288(a6)
[0x800025bc]:csrrwi zero, frm, 3

[0x800025c0]:fadd.d ft11, ft10, ft9, dyn
[0x800025c4]:csrrs a7, fflags, zero
[0x800025c8]:fsd ft11, 1280(a5)
[0x800025cc]:sw a7, 1284(a5)
[0x800025d0]:fld ft10, 1296(a6)
[0x800025d4]:fld ft9, 1304(a6)
[0x800025d8]:csrrwi zero, frm, 3

[0x800025dc]:fadd.d ft11, ft10, ft9, dyn
[0x800025e0]:csrrs a7, fflags, zero
[0x800025e4]:fsd ft11, 1296(a5)
[0x800025e8]:sw a7, 1300(a5)
[0x800025ec]:fld ft10, 1312(a6)
[0x800025f0]:fld ft9, 1320(a6)
[0x800025f4]:csrrwi zero, frm, 3

[0x800025f8]:fadd.d ft11, ft10, ft9, dyn
[0x800025fc]:csrrs a7, fflags, zero
[0x80002600]:fsd ft11, 1312(a5)
[0x80002604]:sw a7, 1316(a5)
[0x80002608]:fld ft10, 1328(a6)
[0x8000260c]:fld ft9, 1336(a6)
[0x80002610]:csrrwi zero, frm, 3



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                              |                                                                           code                                                                           |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005714]<br>0x00000005|- opcode : fadd.d<br> - rs1 : f8<br> - rs2 : f8<br> - rd : f2<br> - rs1 == rs2 != rd<br>                                                                                                                                               |[0x80000120]:fadd.d ft2, fs0, fs0, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd ft2, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>          |
|   2|[0x80005724]<br>0x00000005|- rs1 : f20<br> - rs2 : f23<br> - rd : f20<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd04149240396f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xd04149240396f and rm_val == 3  #nosat<br>                      |[0x8000013c]:fadd.d fs4, fs4, fs7, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fs4, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>        |
|   3|[0x80005734]<br>0x00000005|- rs1 : f28<br> - rs2 : f24<br> - rd : f24<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x663d37d2b8c0a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x663d37d2b8c0a and rm_val == 3  #nosat<br>                      |[0x80000158]:fadd.d fs8, ft8, fs8, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fs8, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>        |
|   4|[0x80005744]<br>0x00000005|- rs1 : f12<br> - rs2 : f6<br> - rd : f4<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xffd6ba914d0ca and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xffd6ba914d0ca and rm_val == 3  #nosat<br> |[0x80000174]:fadd.d ft4, fa2, ft6, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd ft4, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>        |
|   5|[0x80005754]<br>0x00000005|- rs1 : f25<br> - rs2 : f25<br> - rd : f25<br> - rs1 == rs2 == rd<br>                                                                                                                                                                  |[0x80000190]:fadd.d fs9, fs9, fs9, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd fs9, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>        |
|   6|[0x80005764]<br>0x00000005|- rs1 : f22<br> - rs2 : f19<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5688295949924 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5688295949924 and rm_val == 3  #nosat<br>                                             |[0x800001ac]:fadd.d fs2, fs6, fs3, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fs2, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>        |
|   7|[0x80005774]<br>0x00000005|- rs1 : f2<br> - rs2 : f21<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5f6241fcc17b9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5f6241fcc17b9 and rm_val == 3  #nosat<br>                                               |[0x800001c8]:fadd.d fs1, ft2, fs5, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd fs1, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>       |
|   8|[0x80005784]<br>0x00000005|- rs1 : f21<br> - rs2 : f13<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0x1035095fb0c7f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x1035095fb0c7f and rm_val == 3  #nosat<br>                                             |[0x800001e4]:fadd.d fa2, fs5, fa3, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fa2, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>      |
|   9|[0x80005794]<br>0x00000005|- rs1 : f13<br> - rs2 : f26<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7f8 and fm1 == 0x4b499d8a230bf and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0x4b499d8a230bf and rm_val == 3  #nosat<br>                                             |[0x80000200]:fadd.d fa4, fa3, fs10, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fa4, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>     |
|  10|[0x800057a4]<br>0x00000005|- rs1 : f4<br> - rs2 : f1<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe2d876b20b2cd and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe2d876b20b2cd and rm_val == 3  #nosat<br>                                               |[0x8000021c]:fadd.d ft8, ft4, ft1, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd ft8, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>      |
|  11|[0x800057b4]<br>0x00000005|- rs1 : f29<br> - rs2 : f14<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3e06598dce41c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3e06598dce41c and rm_val == 3  #nosat<br>                                              |[0x80000238]:fadd.d ft7, ft9, fa4, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd ft7, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>      |
|  12|[0x800057c4]<br>0x00000005|- rs1 : f18<br> - rs2 : f17<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7d8c1fdfb6a69 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x7d8c1fdfb6a69 and rm_val == 3  #nosat<br>                                             |[0x80000254]:fadd.d ft11, fs2, fa7, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd ft11, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>    |
|  13|[0x800057d4]<br>0x00000005|- rs1 : f30<br> - rs2 : f29<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xed56e9c6a326f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xed56e9c6a326f and rm_val == 3  #nosat<br>                                             |[0x80000270]:fadd.d fa3, ft10, ft9, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fa3, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>     |
|  14|[0x800057e4]<br>0x00000005|- rs1 : f26<br> - rs2 : f15<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x45e0c0bf1170b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x45e0c0bf1170b and rm_val == 3  #nosat<br>                                              |[0x8000028c]:fadd.d ft5, fs10, fa5, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd ft5, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>     |
|  15|[0x800057f4]<br>0x00000005|- rs1 : f7<br> - rs2 : f31<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe8280abe92e75 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe8280abe92e75 and rm_val == 3  #nosat<br>                                              |[0x800002a8]:fadd.d fa1, ft7, ft11, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd fa1, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>     |
|  16|[0x80005804]<br>0x00000005|- rs1 : f11<br> - rs2 : f16<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe40271df052d1 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe40271df052d1 and rm_val == 3  #nosat<br>                                             |[0x800002c4]:fadd.d fs7, fa1, fa6, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs7, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>      |
|  17|[0x80005814]<br>0x00000005|- rs1 : f9<br> - rs2 : f30<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5625f693222e6 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5625f693222e6 and rm_val == 3  #nosat<br>                                              |[0x800002e0]:fadd.d fs5, fs1, ft10, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd fs5, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>     |
|  18|[0x80005824]<br>0x00000005|- rs1 : f15<br> - rs2 : f18<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2e9fbd9df2c67 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x2e9fbd9df2c67 and rm_val == 3  #nosat<br>                                              |[0x800002fc]:fadd.d ft3, fa5, fs2, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd ft3, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br>      |
|  19|[0x80005834]<br>0x00000005|- rs1 : f5<br> - rs2 : f7<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x64e15e87b6907 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x64e15e87b6907 and rm_val == 3  #nosat<br>                                                |[0x80000318]:fadd.d ft1, ft5, ft7, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd ft1, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>      |
|  20|[0x80005844]<br>0x00000005|- rs1 : f16<br> - rs2 : f3<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x41f380c8d1ec8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x41f380c8d1ec8 and rm_val == 3  #nosat<br>                                               |[0x80000334]:fadd.d fs0, fa6, ft3, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd fs0, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>      |
|  21|[0x80005854]<br>0x00000005|- rs1 : f31<br> - rs2 : f27<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2b44ad389f673 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x2b44ad389f673 and rm_val == 3  #nosat<br>                                             |[0x80000350]:fadd.d fs10, ft11, fs11, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs10, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>  |
|  22|[0x80005864]<br>0x00000005|- rs1 : f14<br> - rs2 : f0<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8e9e23b9dbe28 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8e9e23b9dbe28 and rm_val == 3  #nosat<br>                                              |[0x8000036c]:fadd.d fs11, fa4, ft0, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd fs11, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>    |
|  23|[0x80005874]<br>0x00000005|- rs1 : f10<br> - rs2 : f11<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x44e981a2c9e6f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x44e981a2c9e6f and rm_val == 3  #nosat<br>                                             |[0x80000388]:fadd.d fa7, fa0, fa1, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd fa7, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>      |
|  24|[0x80005884]<br>0x00000005|- rs1 : f6<br> - rs2 : f4<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x10107d46bd56f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x10107d46bd56f and rm_val == 3  #nosat<br>                                               |[0x800003a4]:fadd.d fa5, ft6, ft4, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fa5, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>      |
|  25|[0x80005894]<br>0x00000005|- rs1 : f0<br> - rs2 : f10<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x25e0f16179d08 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x25e0f16179d08 and rm_val == 3  #nosat<br>                                              |[0x800003c0]:fadd.d ft9, ft0, fa0, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd ft9, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>      |
|  26|[0x800058a4]<br>0x00000005|- rs1 : f3<br> - rs2 : f28<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28a501a431151 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x28a501a431151 and rm_val == 3  #nosat<br>                                              |[0x800003dc]:fadd.d fs6, ft3, ft8, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fs6, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>      |
|  27|[0x800058b4]<br>0x00000005|- rs1 : f24<br> - rs2 : f2<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x4ef5cc116e8a3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4ef5cc116e8a3 and rm_val == 3  #nosat<br>                                              |[0x800003f8]:fadd.d fa0, fs8, ft2, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd fa0, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>      |
|  28|[0x800058c4]<br>0x00000005|- rs1 : f23<br> - rs2 : f5<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xfe0c60e404d7f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xfe0c60e404d7f and rm_val == 3  #nosat<br>                                              |[0x80000414]:fadd.d ft10, fs7, ft5, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd ft10, 432(a5)<br> [0x80000420]:sw a7, 436(a5)<br>    |
|  29|[0x800058d4]<br>0x00000005|- rs1 : f1<br> - rs2 : f12<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c0c3b0f20ae1 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x5c0c3b0f20ae1 and rm_val == 3  #nosat<br>                                               |[0x80000430]:fadd.d ft6, ft1, fa2, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsd ft6, 448(a5)<br> [0x8000043c]:sw a7, 452(a5)<br>      |
|  30|[0x800058e4]<br>0x00000005|- rs1 : f17<br> - rs2 : f9<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x95a004d0cc955 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x95a004d0cc955 and rm_val == 3  #nosat<br>                                              |[0x8000044c]:fadd.d fs3, fa7, fs1, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd fs3, 464(a5)<br> [0x80000458]:sw a7, 468(a5)<br>      |
|  31|[0x800058f4]<br>0x00000005|- rs1 : f27<br> - rs2 : f20<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x30d9918574e31 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x30d9918574e31 and rm_val == 3  #nosat<br>                                              |[0x80000468]:fadd.d ft0, fs11, fs4, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsd ft0, 480(a5)<br> [0x80000474]:sw a7, 484(a5)<br>     |
|  32|[0x80005904]<br>0x00000005|- rs1 : f19<br> - rs2 : f22<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x82ac91eb0b042 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x82ac91eb0b042 and rm_val == 3  #nosat<br>                                             |[0x80000484]:fadd.d fa6, fs3, fs6, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fa6, 496(a5)<br> [0x80000490]:sw a7, 500(a5)<br>      |
|  33|[0x80005914]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x02abb1ad4a0a3 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x02abb1ad4a0a3 and rm_val == 3  #nosat<br>                                                                                            |[0x800004a0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsd ft11, 512(a5)<br> [0x800004ac]:sw a7, 516(a5)<br>   |
|  34|[0x80005924]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x95be9fb8e8257 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x95be9fb8e8257 and rm_val == 3  #nosat<br>                                                                                            |[0x800004bc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft11, 528(a5)<br> [0x800004c8]:sw a7, 532(a5)<br>   |
|  35|[0x80005934]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d5d3ab8fef6e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x0d5d3ab8fef6e and rm_val == 3  #nosat<br>                                                                                            |[0x800004d8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsd ft11, 544(a5)<br> [0x800004e4]:sw a7, 548(a5)<br>   |
|  36|[0x80005944]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x4c297c00425ff and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x4c297c00425ff and rm_val == 3  #nosat<br>                                                                                            |[0x800004f4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 560(a5)<br> [0x80000500]:sw a7, 564(a5)<br>   |
|  37|[0x80005954]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xeeed208a47b6f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xeeed208a47b6f and rm_val == 3  #nosat<br>                                                                                            |[0x80000510]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd ft11, 576(a5)<br> [0x8000051c]:sw a7, 580(a5)<br>   |
|  38|[0x80005964]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1ecf7d50e7867 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1ecf7d50e7867 and rm_val == 3  #nosat<br>                                                                                            |[0x8000052c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsd ft11, 592(a5)<br> [0x80000538]:sw a7, 596(a5)<br>   |
|  39|[0x80005974]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdbe0fc8b3298f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xdbe0fc8b3298f and rm_val == 3  #nosat<br>                                                                                            |[0x80000548]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsd ft11, 608(a5)<br> [0x80000554]:sw a7, 612(a5)<br>   |
|  40|[0x80005984]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b3a3e9fd9fb7 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5b3a3e9fd9fb7 and rm_val == 3  #nosat<br>                                                                                            |[0x80000564]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 624(a5)<br> [0x80000570]:sw a7, 628(a5)<br>   |
|  41|[0x80005994]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x699f5f701628b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x699f5f701628b and rm_val == 3  #nosat<br>                                                                                            |[0x80000580]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsd ft11, 640(a5)<br> [0x8000058c]:sw a7, 644(a5)<br>   |
|  42|[0x800059a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x172584a6fc7c6 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x172584a6fc7c6 and rm_val == 3  #nosat<br>                                                                                            |[0x8000059c]:fadd.d ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 656(a5)<br> [0x800005a8]:sw a7, 660(a5)<br>   |
|  43|[0x800059b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x898a6dfea4b33 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x898a6dfea4b33 and rm_val == 3  #nosat<br>                                                                                            |[0x800005b8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsd ft11, 672(a5)<br> [0x800005c4]:sw a7, 676(a5)<br>   |
|  44|[0x800059c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f7 and fm1 == 0xfda686ffdecff and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xfda686ffdecff and rm_val == 3  #nosat<br>                                                                                            |[0x800005d4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsd ft11, 688(a5)<br> [0x800005e0]:sw a7, 692(a5)<br>   |
|  45|[0x800059d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x65e23ddcbddd1 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x65e23ddcbddd1 and rm_val == 3  #nosat<br>                                                                                            |[0x800005f0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsd ft11, 704(a5)<br> [0x800005fc]:sw a7, 708(a5)<br>   |
|  46|[0x800059e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa10df54b7350b and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa10df54b7350b and rm_val == 3  #nosat<br>                                                                                            |[0x8000060c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft11, 720(a5)<br> [0x80000618]:sw a7, 724(a5)<br>   |
|  47|[0x800059f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x464ca5c58934b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x464ca5c58934b and rm_val == 3  #nosat<br>                                                                                            |[0x80000628]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsd ft11, 736(a5)<br> [0x80000634]:sw a7, 740(a5)<br>   |
|  48|[0x80005a04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x23fa6c5af95c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x23fa6c5af95c3 and rm_val == 3  #nosat<br>                                                                                            |[0x80000644]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 752(a5)<br> [0x80000650]:sw a7, 756(a5)<br>   |
|  49|[0x80005a14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd0546b2b91d49 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xd0546b2b91d49 and rm_val == 3  #nosat<br>                                                                                            |[0x80000660]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd ft11, 768(a5)<br> [0x8000066c]:sw a7, 772(a5)<br>   |
|  50|[0x80005a24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xba13e3965dc9d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xba13e3965dc9d and rm_val == 3  #nosat<br>                                                                                            |[0x8000067c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsd ft11, 784(a5)<br> [0x80000688]:sw a7, 788(a5)<br>   |
|  51|[0x80005a34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9858f917ba8dd and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x9858f917ba8dd and rm_val == 3  #nosat<br>                                                                                            |[0x80000698]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsd ft11, 800(a5)<br> [0x800006a4]:sw a7, 804(a5)<br>   |
|  52|[0x80005a44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97605fecf75de and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x97605fecf75de and rm_val == 3  #nosat<br>                                                                                            |[0x800006b4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 816(a5)<br> [0x800006c0]:sw a7, 820(a5)<br>   |
|  53|[0x80005a54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1dc9fa26c1435 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x1dc9fa26c1435 and rm_val == 3  #nosat<br>                                                                                            |[0x800006d0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800006d4]:csrrs a7, fflags, zero<br> [0x800006d8]:fsd ft11, 832(a5)<br> [0x800006dc]:sw a7, 836(a5)<br>   |
|  54|[0x80005a64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97d11446f38d6 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x97d11446f38d6 and rm_val == 3  #nosat<br>                                                                                            |[0x800006ec]:fadd.d ft11, ft10, ft9, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 848(a5)<br> [0x800006f8]:sw a7, 852(a5)<br>   |
|  55|[0x80005a74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe230580ba7bd1 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe230580ba7bd1 and rm_val == 3  #nosat<br>                                                                                            |[0x80000708]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:fsd ft11, 864(a5)<br> [0x80000714]:sw a7, 868(a5)<br>   |
|  56|[0x80005a84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa1c5a75f20f3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa1c5a75f20f3f and rm_val == 3  #nosat<br>                                                                                            |[0x80000724]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 880(a5)<br> [0x80000730]:sw a7, 884(a5)<br>   |
|  57|[0x80005a94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x74d41339ae482 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x74d41339ae482 and rm_val == 3  #nosat<br>                                                                                            |[0x80000740]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd ft11, 896(a5)<br> [0x8000074c]:sw a7, 900(a5)<br>   |
|  58|[0x80005aa4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa26ee9811427d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xa26ee9811427d and rm_val == 3  #nosat<br>                                                                                            |[0x8000075c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 912(a5)<br> [0x80000768]:sw a7, 916(a5)<br>   |
|  59|[0x80005ab4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe5da67e1de883 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe5da67e1de883 and rm_val == 3  #nosat<br>                                                                                            |[0x80000778]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000077c]:csrrs a7, fflags, zero<br> [0x80000780]:fsd ft11, 928(a5)<br> [0x80000784]:sw a7, 932(a5)<br>   |
|  60|[0x80005ac4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc77c9350fee6d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xc77c9350fee6d and rm_val == 3  #nosat<br>                                                                                            |[0x80000794]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:fsd ft11, 944(a5)<br> [0x800007a0]:sw a7, 948(a5)<br>   |
|  61|[0x80005ad4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x46206996b12da and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x46206996b12da and rm_val == 3  #nosat<br>                                                                                            |[0x800007b0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:fsd ft11, 960(a5)<br> [0x800007bc]:sw a7, 964(a5)<br>   |
|  62|[0x80005ae4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2365849750ca3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x2365849750ca3 and rm_val == 3  #nosat<br>                                                                                            |[0x800007cc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800007d0]:csrrs a7, fflags, zero<br> [0x800007d4]:fsd ft11, 976(a5)<br> [0x800007d8]:sw a7, 980(a5)<br>   |
|  63|[0x80005af4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x19ff775aac054 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x19ff775aac054 and rm_val == 3  #nosat<br>                                                                                            |[0x800007e8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:fsd ft11, 992(a5)<br> [0x800007f4]:sw a7, 996(a5)<br>   |
|  64|[0x80005b04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2bdf74439828f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x2bdf74439828f and rm_val == 3  #nosat<br>                                                                                            |[0x80000804]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 1008(a5)<br> [0x80000810]:sw a7, 1012(a5)<br> |
|  65|[0x80005b14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x50af5b268139f and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0x50af5b268139f and rm_val == 3  #nosat<br>                                                                                            |[0x80000820]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000824]:csrrs a7, fflags, zero<br> [0x80000828]:fsd ft11, 1024(a5)<br> [0x8000082c]:sw a7, 1028(a5)<br> |
|  66|[0x80005b24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x7aed2f71a352f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x7aed2f71a352f and rm_val == 3  #nosat<br>                                                                                            |[0x8000083c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd ft11, 1040(a5)<br> [0x80000848]:sw a7, 1044(a5)<br> |
|  67|[0x80005b34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x2a0b81afacd4f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x2a0b81afacd4f and rm_val == 3  #nosat<br>                                                                                            |[0x80000858]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:fsd ft11, 1056(a5)<br> [0x80000864]:sw a7, 1060(a5)<br> |
|  68|[0x80005b44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf155693c9590b and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xf155693c9590b and rm_val == 3  #nosat<br>                                                                                            |[0x80000874]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000878]:csrrs a7, fflags, zero<br> [0x8000087c]:fsd ft11, 1072(a5)<br> [0x80000880]:sw a7, 1076(a5)<br> |
|  69|[0x80005b54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9bd90b8e42a3f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9bd90b8e42a3f and rm_val == 3  #nosat<br>                                                                                            |[0x80000890]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:fsd ft11, 1088(a5)<br> [0x8000089c]:sw a7, 1092(a5)<br> |
|  70|[0x80005b64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcb9c1949673fd and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcb9c1949673fd and rm_val == 3  #nosat<br>                                                                                            |[0x800008ac]:fadd.d ft11, ft10, ft9, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 1104(a5)<br> [0x800008b8]:sw a7, 1108(a5)<br> |
|  71|[0x80005b74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x29651713b2616 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x29651713b2616 and rm_val == 3  #nosat<br>                                                                                            |[0x800008c8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:fsd ft11, 1120(a5)<br> [0x800008d4]:sw a7, 1124(a5)<br> |
|  72|[0x80005b84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc642d452bf98 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfc642d452bf98 and rm_val == 3  #nosat<br>                                                                                            |[0x800008e4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 1136(a5)<br> [0x800008f0]:sw a7, 1140(a5)<br> |
|  73|[0x80005b94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4596be54ed4ed and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x4596be54ed4ed and rm_val == 3  #nosat<br>                                                                                            |[0x80000900]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000904]:csrrs a7, fflags, zero<br> [0x80000908]:fsd ft11, 1152(a5)<br> [0x8000090c]:sw a7, 1156(a5)<br> |
|  74|[0x80005ba4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdfcf16f837dfc and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xdfcf16f837dfc and rm_val == 3  #nosat<br>                                                                                            |[0x8000091c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000920]:csrrs a7, fflags, zero<br> [0x80000924]:fsd ft11, 1168(a5)<br> [0x80000928]:sw a7, 1172(a5)<br> |
|  75|[0x80005bb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe4bb35faff00f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe4bb35faff00f and rm_val == 3  #nosat<br>                                                                                            |[0x80000938]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000093c]:csrrs a7, fflags, zero<br> [0x80000940]:fsd ft11, 1184(a5)<br> [0x80000944]:sw a7, 1188(a5)<br> |
|  76|[0x80005bc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeedb9ccd51d70 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xeedb9ccd51d70 and rm_val == 3  #nosat<br>                                                                                            |[0x80000954]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsd ft11, 1200(a5)<br> [0x80000960]:sw a7, 1204(a5)<br> |
|  77|[0x80005bd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeb18879086a84 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xeb18879086a84 and rm_val == 3  #nosat<br>                                                                                            |[0x80000970]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000974]:csrrs a7, fflags, zero<br> [0x80000978]:fsd ft11, 1216(a5)<br> [0x8000097c]:sw a7, 1220(a5)<br> |
|  78|[0x80005be4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xcc5a8af41138f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xcc5a8af41138f and rm_val == 3  #nosat<br>                                                                                            |[0x8000098c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:fsd ft11, 1232(a5)<br> [0x80000998]:sw a7, 1236(a5)<br> |
|  79|[0x80005bf4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7e5dd8914aef and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb7e5dd8914aef and rm_val == 3  #nosat<br>                                                                                            |[0x800009a8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800009ac]:csrrs a7, fflags, zero<br> [0x800009b0]:fsd ft11, 1248(a5)<br> [0x800009b4]:sw a7, 1252(a5)<br> |
|  80|[0x80005c04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x93dda7765991f and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0x93dda7765991f and rm_val == 3  #nosat<br>                                                                                            |[0x800009c4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1264(a5)<br> [0x800009d0]:sw a7, 1268(a5)<br> |
|  81|[0x80005c14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f7 and fm1 == 0xd6d9096268f7f and fs2 == 1 and fe2 == 0x7f7 and fm2 == 0xd6d9096268f7f and rm_val == 3  #nosat<br>                                                                                            |[0x800009e0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800009e4]:csrrs a7, fflags, zero<br> [0x800009e8]:fsd ft11, 1280(a5)<br> [0x800009ec]:sw a7, 1284(a5)<br> |
|  82|[0x80005c24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe063e979a868f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe063e979a868f and rm_val == 3  #nosat<br>                                                                                            |[0x800009fc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:fsd ft11, 1296(a5)<br> [0x80000a08]:sw a7, 1300(a5)<br> |
|  83|[0x80005c34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa3695ba8b56f7 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa3695ba8b56f7 and rm_val == 3  #nosat<br>                                                                                            |[0x80000a18]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a1c]:csrrs a7, fflags, zero<br> [0x80000a20]:fsd ft11, 1312(a5)<br> [0x80000a24]:sw a7, 1316(a5)<br> |
|  84|[0x80005c44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc97053092bae8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xc97053092bae8 and rm_val == 3  #nosat<br>                                                                                            |[0x80000a34]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:fsd ft11, 1328(a5)<br> [0x80000a40]:sw a7, 1332(a5)<br> |
|  85|[0x80005c54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa354d897694eb and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa354d897694eb and rm_val == 3  #nosat<br>                                                                                            |[0x80000a50]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a54]:csrrs a7, fflags, zero<br> [0x80000a58]:fsd ft11, 1344(a5)<br> [0x80000a5c]:sw a7, 1348(a5)<br> |
|  86|[0x80005c64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0x46821d48c93bf and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0x46821d48c93bf and rm_val == 3  #nosat<br>                                                                                            |[0x80000a6c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a70]:csrrs a7, fflags, zero<br> [0x80000a74]:fsd ft11, 1360(a5)<br> [0x80000a78]:sw a7, 1364(a5)<br> |
|  87|[0x80005c74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xe15232c378b7f and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0xe15232c378b7f and rm_val == 3  #nosat<br>                                                                                            |[0x80000a88]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000a8c]:csrrs a7, fflags, zero<br> [0x80000a90]:fsd ft11, 1376(a5)<br> [0x80000a94]:sw a7, 1380(a5)<br> |
|  88|[0x80005c84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2011ca3e25417 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x2011ca3e25417 and rm_val == 3  #nosat<br>                                                                                            |[0x80000aa4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1392(a5)<br> [0x80000ab0]:sw a7, 1396(a5)<br> |
|  89|[0x80005c94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x71f120502a5e1 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x71f120502a5e1 and rm_val == 3  #nosat<br>                                                                                            |[0x80000ac0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ac4]:csrrs a7, fflags, zero<br> [0x80000ac8]:fsd ft11, 1408(a5)<br> [0x80000acc]:sw a7, 1412(a5)<br> |
|  90|[0x80005ca4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6ac2d374cb87 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd6ac2d374cb87 and rm_val == 3  #nosat<br>                                                                                            |[0x80000adc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:fsd ft11, 1424(a5)<br> [0x80000ae8]:sw a7, 1428(a5)<br> |
|  91|[0x80005cb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5f72319ab0728 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5f72319ab0728 and rm_val == 3  #nosat<br>                                                                                            |[0x80000af8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000afc]:csrrs a7, fflags, zero<br> [0x80000b00]:fsd ft11, 1440(a5)<br> [0x80000b04]:sw a7, 1444(a5)<br> |
|  92|[0x80005cc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf700ae54ab8df and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0xf700ae54ab8df and rm_val == 3  #nosat<br>                                                                                            |[0x80000b14]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000b18]:csrrs a7, fflags, zero<br> [0x80000b1c]:fsd ft11, 1456(a5)<br> [0x80000b20]:sw a7, 1460(a5)<br> |
|  93|[0x80005cd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8c31f809fe79b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8c31f809fe79b and rm_val == 3  #nosat<br>                                                                                            |[0x80000b30]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000b34]:csrrs a7, fflags, zero<br> [0x80000b38]:fsd ft11, 1472(a5)<br> [0x80000b3c]:sw a7, 1476(a5)<br> |
|  94|[0x80005ce4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa99dd8880ddad and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xa99dd8880ddad and rm_val == 3  #nosat<br>                                                                                            |[0x80000b4c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000b50]:csrrs a7, fflags, zero<br> [0x80000b54]:fsd ft11, 1488(a5)<br> [0x80000b58]:sw a7, 1492(a5)<br> |
|  95|[0x80005cf4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd883cdc560c7e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd883cdc560c7e and rm_val == 3  #nosat<br>                                                                                            |[0x80000b68]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000b6c]:csrrs a7, fflags, zero<br> [0x80000b70]:fsd ft11, 1504(a5)<br> [0x80000b74]:sw a7, 1508(a5)<br> |
|  96|[0x80005d04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb4318b7227e1b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xb4318b7227e1b and rm_val == 3  #nosat<br>                                                                                            |[0x80000b84]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1520(a5)<br> [0x80000b90]:sw a7, 1524(a5)<br> |
|  97|[0x80005d14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x38a399f905ab9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x38a399f905ab9 and rm_val == 3  #nosat<br>                                                                                            |[0x80000ba0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ba4]:csrrs a7, fflags, zero<br> [0x80000ba8]:fsd ft11, 1536(a5)<br> [0x80000bac]:sw a7, 1540(a5)<br> |
|  98|[0x80005d24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x65a21c61847d5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x65a21c61847d5 and rm_val == 3  #nosat<br>                                                                                            |[0x80000bbc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000bc0]:csrrs a7, fflags, zero<br> [0x80000bc4]:fsd ft11, 1552(a5)<br> [0x80000bc8]:sw a7, 1556(a5)<br> |
|  99|[0x80005d34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc70d21e827c6a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xc70d21e827c6a and rm_val == 3  #nosat<br>                                                                                            |[0x80000bd8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000bdc]:csrrs a7, fflags, zero<br> [0x80000be0]:fsd ft11, 1568(a5)<br> [0x80000be4]:sw a7, 1572(a5)<br> |
| 100|[0x80005d44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x7ee0eb8d7cc7f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x7ee0eb8d7cc7f and rm_val == 3  #nosat<br>                                                                                            |[0x80000bf4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000bf8]:csrrs a7, fflags, zero<br> [0x80000bfc]:fsd ft11, 1584(a5)<br> [0x80000c00]:sw a7, 1588(a5)<br> |
| 101|[0x80005d54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x24c28db80e5f8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x24c28db80e5f8 and rm_val == 3  #nosat<br>                                                                                            |[0x80000c10]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000c14]:csrrs a7, fflags, zero<br> [0x80000c18]:fsd ft11, 1600(a5)<br> [0x80000c1c]:sw a7, 1604(a5)<br> |
| 102|[0x80005d64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe3baa16148b70 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe3baa16148b70 and rm_val == 3  #nosat<br>                                                                                            |[0x80000c2c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000c30]:csrrs a7, fflags, zero<br> [0x80000c34]:fsd ft11, 1616(a5)<br> [0x80000c38]:sw a7, 1620(a5)<br> |
| 103|[0x80005d74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf1d543a0b07fb and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xf1d543a0b07fb and rm_val == 3  #nosat<br>                                                                                            |[0x80000c48]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1632(a5)<br> [0x80000c54]:sw a7, 1636(a5)<br> |
| 104|[0x80005d84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x6db2c39b92e2f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x6db2c39b92e2f and rm_val == 3  #nosat<br>                                                                                            |[0x80000c64]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000c68]:csrrs a7, fflags, zero<br> [0x80000c6c]:fsd ft11, 1648(a5)<br> [0x80000c70]:sw a7, 1652(a5)<br> |
| 105|[0x80005d94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x94410aa872e85 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x94410aa872e85 and rm_val == 3  #nosat<br>                                                                                            |[0x80000c80]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000c84]:csrrs a7, fflags, zero<br> [0x80000c88]:fsd ft11, 1664(a5)<br> [0x80000c8c]:sw a7, 1668(a5)<br> |
| 106|[0x80005da4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8363338c30c8b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x8363338c30c8b and rm_val == 3  #nosat<br>                                                                                            |[0x80000c9c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ca0]:csrrs a7, fflags, zero<br> [0x80000ca4]:fsd ft11, 1680(a5)<br> [0x80000ca8]:sw a7, 1684(a5)<br> |
| 107|[0x80005db4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9071429916f5c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9071429916f5c and rm_val == 3  #nosat<br>                                                                                            |[0x80000cb8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000cbc]:csrrs a7, fflags, zero<br> [0x80000cc0]:fsd ft11, 1696(a5)<br> [0x80000cc4]:sw a7, 1700(a5)<br> |
| 108|[0x80005dc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x922ba23fbbdc6 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x922ba23fbbdc6 and rm_val == 3  #nosat<br>                                                                                            |[0x80000cd4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000cd8]:csrrs a7, fflags, zero<br> [0x80000cdc]:fsd ft11, 1712(a5)<br> [0x80000ce0]:sw a7, 1716(a5)<br> |
| 109|[0x80005dd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9ed93307c783a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9ed93307c783a and rm_val == 3  #nosat<br>                                                                                            |[0x80000cf0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000cf4]:csrrs a7, fflags, zero<br> [0x80000cf8]:fsd ft11, 1728(a5)<br> [0x80000cfc]:sw a7, 1732(a5)<br> |
| 110|[0x80005de4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbebcdefd48729 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbebcdefd48729 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d0c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d10]:csrrs a7, fflags, zero<br> [0x80000d14]:fsd ft11, 1744(a5)<br> [0x80000d18]:sw a7, 1748(a5)<br> |
| 111|[0x80005df4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x99434052cdad4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x99434052cdad4 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d28]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1760(a5)<br> [0x80000d34]:sw a7, 1764(a5)<br> |
| 112|[0x80005e04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x0451c9f55e3a7 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x0451c9f55e3a7 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d44]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d48]:csrrs a7, fflags, zero<br> [0x80000d4c]:fsd ft11, 1776(a5)<br> [0x80000d50]:sw a7, 1780(a5)<br> |
| 113|[0x80005e14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3e7bb112f7fe8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3e7bb112f7fe8 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d60]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d64]:csrrs a7, fflags, zero<br> [0x80000d68]:fsd ft11, 1792(a5)<br> [0x80000d6c]:sw a7, 1796(a5)<br> |
| 114|[0x80005e24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xfd3af1f060647 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xfd3af1f060647 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d7c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d80]:csrrs a7, fflags, zero<br> [0x80000d84]:fsd ft11, 1808(a5)<br> [0x80000d88]:sw a7, 1812(a5)<br> |
| 115|[0x80005e34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x43be7b7bc5458 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x43be7b7bc5458 and rm_val == 3  #nosat<br>                                                                                            |[0x80000d98]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000d9c]:csrrs a7, fflags, zero<br> [0x80000da0]:fsd ft11, 1824(a5)<br> [0x80000da4]:sw a7, 1828(a5)<br> |
| 116|[0x80005e44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x38619d6cda314 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x38619d6cda314 and rm_val == 3  #nosat<br>                                                                                            |[0x80000db4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000db8]:csrrs a7, fflags, zero<br> [0x80000dbc]:fsd ft11, 1840(a5)<br> [0x80000dc0]:sw a7, 1844(a5)<br> |
| 117|[0x80005e54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf6165c8e35259 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xf6165c8e35259 and rm_val == 3  #nosat<br>                                                                                            |[0x80000dd0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000dd4]:csrrs a7, fflags, zero<br> [0x80000dd8]:fsd ft11, 1856(a5)<br> [0x80000ddc]:sw a7, 1860(a5)<br> |
| 118|[0x80005e64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x889db2e44701b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x889db2e44701b and rm_val == 3  #nosat<br>                                                                                            |[0x80000dec]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000df0]:csrrs a7, fflags, zero<br> [0x80000df4]:fsd ft11, 1872(a5)<br> [0x80000df8]:sw a7, 1876(a5)<br> |
| 119|[0x80005e74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x19295f298916c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x19295f298916c and rm_val == 3  #nosat<br>                                                                                            |[0x80000e08]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1888(a5)<br> [0x80000e14]:sw a7, 1892(a5)<br> |
| 120|[0x80005e84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x428af114baf6a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x428af114baf6a and rm_val == 3  #nosat<br>                                                                                            |[0x80000e24]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e28]:csrrs a7, fflags, zero<br> [0x80000e2c]:fsd ft11, 1904(a5)<br> [0x80000e30]:sw a7, 1908(a5)<br> |
| 121|[0x80005e94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x09f50264a8d1f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x09f50264a8d1f and rm_val == 3  #nosat<br>                                                                                            |[0x80000e40]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e44]:csrrs a7, fflags, zero<br> [0x80000e48]:fsd ft11, 1920(a5)<br> [0x80000e4c]:sw a7, 1924(a5)<br> |
| 122|[0x80005ea4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe230c7e39a5d7 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe230c7e39a5d7 and rm_val == 3  #nosat<br>                                                                                            |[0x80000e5c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e60]:csrrs a7, fflags, zero<br> [0x80000e64]:fsd ft11, 1936(a5)<br> [0x80000e68]:sw a7, 1940(a5)<br> |
| 123|[0x80005eb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8bf6a13abca7f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x8bf6a13abca7f and rm_val == 3  #nosat<br>                                                                                            |[0x80000e78]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e7c]:csrrs a7, fflags, zero<br> [0x80000e80]:fsd ft11, 1952(a5)<br> [0x80000e84]:sw a7, 1956(a5)<br> |
| 124|[0x80005ec4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa501ef8480c55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xa501ef8480c55 and rm_val == 3  #nosat<br>                                                                                            |[0x80000e94]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000e98]:csrrs a7, fflags, zero<br> [0x80000e9c]:fsd ft11, 1968(a5)<br> [0x80000ea0]:sw a7, 1972(a5)<br> |
| 125|[0x80005ed4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x360373cf6f10f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x360373cf6f10f and rm_val == 3  #nosat<br>                                                                                            |[0x80000eb0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000eb4]:csrrs a7, fflags, zero<br> [0x80000eb8]:fsd ft11, 1984(a5)<br> [0x80000ebc]:sw a7, 1988(a5)<br> |
| 126|[0x80005ee4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6f335d0539418 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6f335d0539418 and rm_val == 3  #nosat<br>                                                                                            |[0x80000ecc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ed0]:csrrs a7, fflags, zero<br> [0x80000ed4]:fsd ft11, 2000(a5)<br> [0x80000ed8]:sw a7, 2004(a5)<br> |
| 127|[0x80005ef4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf7a288f1ea41f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xf7a288f1ea41f and rm_val == 3  #nosat<br>                                                                                            |[0x80000ee8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 2016(a5)<br> [0x80000ef4]:sw a7, 2020(a5)<br> |
| 128|[0x80005b0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x265eb5ece1e0f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x265eb5ece1e0f and rm_val == 3  #nosat<br>                                                                                            |[0x80000f10]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000f14]:csrrs a7, fflags, zero<br> [0x80000f18]:fsd ft11, 0(a5)<br> [0x80000f1c]:sw a7, 4(a5)<br>       |
| 129|[0x80005b1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8edfc5560a8d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa8edfc5560a8d and rm_val == 3  #nosat<br>                                                                                            |[0x80000f2c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000f30]:csrrs a7, fflags, zero<br> [0x80000f34]:fsd ft11, 16(a5)<br> [0x80000f38]:sw a7, 20(a5)<br>     |
| 130|[0x80005b2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x04c0c63d2bf03 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x04c0c63d2bf03 and rm_val == 3  #nosat<br>                                                                                            |[0x80000f48]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 32(a5)<br> [0x80000f54]:sw a7, 36(a5)<br>     |
| 131|[0x80005b3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xfe0614a7b9fbf and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xfe0614a7b9fbf and rm_val == 3  #nosat<br>                                                                                            |[0x80000f64]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs a7, fflags, zero<br> [0x80000f6c]:fsd ft11, 48(a5)<br> [0x80000f70]:sw a7, 52(a5)<br>     |
| 132|[0x80005b4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa1ddeeb12c253 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xa1ddeeb12c253 and rm_val == 3  #nosat<br>                                                                                            |[0x80000f80]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:fsd ft11, 64(a5)<br> [0x80000f8c]:sw a7, 68(a5)<br>     |
| 133|[0x80005b5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe2c9f3b4cd220 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe2c9f3b4cd220 and rm_val == 3  #nosat<br>                                                                                            |[0x80000f9c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000fa0]:csrrs a7, fflags, zero<br> [0x80000fa4]:fsd ft11, 80(a5)<br> [0x80000fa8]:sw a7, 84(a5)<br>     |
| 134|[0x80005b6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x0cc870fcad57f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x0cc870fcad57f and rm_val == 3  #nosat<br>                                                                                            |[0x80000fb8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000fbc]:csrrs a7, fflags, zero<br> [0x80000fc0]:fsd ft11, 96(a5)<br> [0x80000fc4]:sw a7, 100(a5)<br>    |
| 135|[0x80005b7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0382dd247f3f9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x0382dd247f3f9 and rm_val == 3  #nosat<br>                                                                                            |[0x80000fd4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:fsd ft11, 112(a5)<br> [0x80000fe0]:sw a7, 116(a5)<br>   |
| 136|[0x80005b8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f2 and fm1 == 0xf8c6f685f5fff and fs2 == 1 and fe2 == 0x7f2 and fm2 == 0xf8c6f685f5fff and rm_val == 3  #nosat<br>                                                                                            |[0x80000ff0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80000ff4]:csrrs a7, fflags, zero<br> [0x80000ff8]:fsd ft11, 128(a5)<br> [0x80000ffc]:sw a7, 132(a5)<br>   |
| 137|[0x80005b9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x989b40414f92c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x989b40414f92c and rm_val == 3  #nosat<br>                                                                                            |[0x8000100c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001010]:csrrs a7, fflags, zero<br> [0x80001014]:fsd ft11, 144(a5)<br> [0x80001018]:sw a7, 148(a5)<br>   |
| 138|[0x80005bac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2c3b1b8ef2d41 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x2c3b1b8ef2d41 and rm_val == 3  #nosat<br>                                                                                            |[0x80001028]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 160(a5)<br> [0x80001034]:sw a7, 164(a5)<br>   |
| 139|[0x80005bbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x950338fe39141 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x950338fe39141 and rm_val == 3  #nosat<br>                                                                                            |[0x80001044]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001048]:csrrs a7, fflags, zero<br> [0x8000104c]:fsd ft11, 176(a5)<br> [0x80001050]:sw a7, 180(a5)<br>   |
| 140|[0x80005bcc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa6c1b4fe3e3c0 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa6c1b4fe3e3c0 and rm_val == 3  #nosat<br>                                                                                            |[0x80001060]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001064]:csrrs a7, fflags, zero<br> [0x80001068]:fsd ft11, 192(a5)<br> [0x8000106c]:sw a7, 196(a5)<br>   |
| 141|[0x80005bdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x122215f9ac41a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x122215f9ac41a and rm_val == 3  #nosat<br>                                                                                            |[0x8000107c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001080]:csrrs a7, fflags, zero<br> [0x80001084]:fsd ft11, 208(a5)<br> [0x80001088]:sw a7, 212(a5)<br>   |
| 142|[0x80005bec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcfc27db04baa5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcfc27db04baa5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001098]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000109c]:csrrs a7, fflags, zero<br> [0x800010a0]:fsd ft11, 224(a5)<br> [0x800010a4]:sw a7, 228(a5)<br>   |
| 143|[0x80005bfc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfb271584e30d0 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfb271584e30d0 and rm_val == 3  #nosat<br>                                                                                            |[0x800010b4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800010b8]:csrrs a7, fflags, zero<br> [0x800010bc]:fsd ft11, 240(a5)<br> [0x800010c0]:sw a7, 244(a5)<br>   |
| 144|[0x80005c0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9c37606126e28 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9c37606126e28 and rm_val == 3  #nosat<br>                                                                                            |[0x800010d0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800010d4]:csrrs a7, fflags, zero<br> [0x800010d8]:fsd ft11, 256(a5)<br> [0x800010dc]:sw a7, 260(a5)<br>   |
| 145|[0x80005c1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x25d29d05cd288 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x25d29d05cd288 and rm_val == 3  #nosat<br>                                                                                            |[0x800010ec]:fadd.d ft11, ft10, ft9, dyn<br> [0x800010f0]:csrrs a7, fflags, zero<br> [0x800010f4]:fsd ft11, 272(a5)<br> [0x800010f8]:sw a7, 276(a5)<br>   |
| 146|[0x80005c2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd5bbb21e85e5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcd5bbb21e85e5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001108]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000110c]:csrrs a7, fflags, zero<br> [0x80001110]:fsd ft11, 288(a5)<br> [0x80001114]:sw a7, 292(a5)<br>   |
| 147|[0x80005c3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xefec1cd7c3bcb and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xefec1cd7c3bcb and rm_val == 3  #nosat<br>                                                                                            |[0x80001124]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001128]:csrrs a7, fflags, zero<br> [0x8000112c]:fsd ft11, 304(a5)<br> [0x80001130]:sw a7, 308(a5)<br>   |
| 148|[0x80005c4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x30c845de62d3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x30c845de62d3f and rm_val == 3  #nosat<br>                                                                                            |[0x80001140]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001144]:csrrs a7, fflags, zero<br> [0x80001148]:fsd ft11, 320(a5)<br> [0x8000114c]:sw a7, 324(a5)<br>   |
| 149|[0x80005c5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5287546e52d99 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x5287546e52d99 and rm_val == 3  #nosat<br>                                                                                            |[0x8000115c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001160]:csrrs a7, fflags, zero<br> [0x80001164]:fsd ft11, 336(a5)<br> [0x80001168]:sw a7, 340(a5)<br>   |
| 150|[0x80005c6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0xf4dd0c2472fbf and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0xf4dd0c2472fbf and rm_val == 3  #nosat<br>                                                                                            |[0x80001178]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000117c]:csrrs a7, fflags, zero<br> [0x80001180]:fsd ft11, 352(a5)<br> [0x80001184]:sw a7, 356(a5)<br>   |
| 151|[0x80005c7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xccfc542168107 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xccfc542168107 and rm_val == 3  #nosat<br>                                                                                            |[0x80001194]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001198]:csrrs a7, fflags, zero<br> [0x8000119c]:fsd ft11, 368(a5)<br> [0x800011a0]:sw a7, 372(a5)<br>   |
| 152|[0x80005c8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbcdfd8ba97c91 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbcdfd8ba97c91 and rm_val == 3  #nosat<br>                                                                                            |[0x800011b0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 384(a5)<br> [0x800011bc]:sw a7, 388(a5)<br>   |
| 153|[0x80005c9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x0fce9799927f7 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x0fce9799927f7 and rm_val == 3  #nosat<br>                                                                                            |[0x800011cc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800011d0]:csrrs a7, fflags, zero<br> [0x800011d4]:fsd ft11, 400(a5)<br> [0x800011d8]:sw a7, 404(a5)<br>   |
| 154|[0x80005cac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6faef3ad3537e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6faef3ad3537e and rm_val == 3  #nosat<br>                                                                                            |[0x800011e8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800011ec]:csrrs a7, fflags, zero<br> [0x800011f0]:fsd ft11, 416(a5)<br> [0x800011f4]:sw a7, 420(a5)<br>   |
| 155|[0x80005cbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaf465058419e9 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xaf465058419e9 and rm_val == 3  #nosat<br>                                                                                            |[0x80001204]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001208]:csrrs a7, fflags, zero<br> [0x8000120c]:fsd ft11, 432(a5)<br> [0x80001210]:sw a7, 436(a5)<br>   |
| 156|[0x80005ccc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x070c0d4d218f9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x070c0d4d218f9 and rm_val == 3  #nosat<br>                                                                                            |[0x80001220]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001224]:csrrs a7, fflags, zero<br> [0x80001228]:fsd ft11, 448(a5)<br> [0x8000122c]:sw a7, 452(a5)<br>   |
| 157|[0x80005cdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x228e5619b5bff and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x228e5619b5bff and rm_val == 3  #nosat<br>                                                                                            |[0x8000123c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001240]:csrrs a7, fflags, zero<br> [0x80001244]:fsd ft11, 464(a5)<br> [0x80001248]:sw a7, 468(a5)<br>   |
| 158|[0x80005cec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcc4ac8145e5cc and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xcc4ac8145e5cc and rm_val == 3  #nosat<br>                                                                                            |[0x80001258]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000125c]:csrrs a7, fflags, zero<br> [0x80001260]:fsd ft11, 480(a5)<br> [0x80001264]:sw a7, 484(a5)<br>   |
| 159|[0x80005cfc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c62b6da50e51 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x5c62b6da50e51 and rm_val == 3  #nosat<br>                                                                                            |[0x80001274]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001278]:csrrs a7, fflags, zero<br> [0x8000127c]:fsd ft11, 496(a5)<br> [0x80001280]:sw a7, 500(a5)<br>   |
| 160|[0x80005d0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3833da7b9aa37 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3833da7b9aa37 and rm_val == 3  #nosat<br>                                                                                            |[0x80001290]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 512(a5)<br> [0x8000129c]:sw a7, 516(a5)<br>   |
| 161|[0x80005d1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x57132c37fb117 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x57132c37fb117 and rm_val == 3  #nosat<br>                                                                                            |[0x800012ac]:fadd.d ft11, ft10, ft9, dyn<br> [0x800012b0]:csrrs a7, fflags, zero<br> [0x800012b4]:fsd ft11, 528(a5)<br> [0x800012b8]:sw a7, 532(a5)<br>   |
| 162|[0x80005d2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfe6749ffc4763 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xfe6749ffc4763 and rm_val == 3  #nosat<br>                                                                                            |[0x800012c8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800012cc]:csrrs a7, fflags, zero<br> [0x800012d0]:fsd ft11, 544(a5)<br> [0x800012d4]:sw a7, 548(a5)<br>   |
| 163|[0x80005d3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd5872438d16b0 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd5872438d16b0 and rm_val == 3  #nosat<br>                                                                                            |[0x800012e4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800012e8]:csrrs a7, fflags, zero<br> [0x800012ec]:fsd ft11, 560(a5)<br> [0x800012f0]:sw a7, 564(a5)<br>   |
| 164|[0x80005d4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc15c34215bcf5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xc15c34215bcf5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001300]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001304]:csrrs a7, fflags, zero<br> [0x80001308]:fsd ft11, 576(a5)<br> [0x8000130c]:sw a7, 580(a5)<br>   |
| 165|[0x80005d5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9fa60dd1b5e57 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x9fa60dd1b5e57 and rm_val == 3  #nosat<br>                                                                                            |[0x8000131c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001320]:csrrs a7, fflags, zero<br> [0x80001324]:fsd ft11, 592(a5)<br> [0x80001328]:sw a7, 596(a5)<br>   |
| 166|[0x80005d6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf74a5c9f39c6c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf74a5c9f39c6c and rm_val == 3  #nosat<br>                                                                                            |[0x80001338]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000133c]:csrrs a7, fflags, zero<br> [0x80001340]:fsd ft11, 608(a5)<br> [0x80001344]:sw a7, 612(a5)<br>   |
| 167|[0x80005d7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x35eecb1ad0a6b and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x35eecb1ad0a6b and rm_val == 3  #nosat<br>                                                                                            |[0x80001354]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001358]:csrrs a7, fflags, zero<br> [0x8000135c]:fsd ft11, 624(a5)<br> [0x80001360]:sw a7, 628(a5)<br>   |
| 168|[0x80005d8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x831acfae4a49b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x831acfae4a49b and rm_val == 3  #nosat<br>                                                                                            |[0x80001370]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 640(a5)<br> [0x8000137c]:sw a7, 644(a5)<br>   |
| 169|[0x80005d9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bbbe71ac902b and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x2bbbe71ac902b and rm_val == 3  #nosat<br>                                                                                            |[0x8000138c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001390]:csrrs a7, fflags, zero<br> [0x80001394]:fsd ft11, 656(a5)<br> [0x80001398]:sw a7, 660(a5)<br>   |
| 170|[0x80005dac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe55b30b309254 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe55b30b309254 and rm_val == 3  #nosat<br>                                                                                            |[0x800013a8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800013ac]:csrrs a7, fflags, zero<br> [0x800013b0]:fsd ft11, 672(a5)<br> [0x800013b4]:sw a7, 676(a5)<br>   |
| 171|[0x80005dbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa8693ca418657 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa8693ca418657 and rm_val == 3  #nosat<br>                                                                                            |[0x800013c4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800013c8]:csrrs a7, fflags, zero<br> [0x800013cc]:fsd ft11, 688(a5)<br> [0x800013d0]:sw a7, 692(a5)<br>   |
| 172|[0x80005dcc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe70e78fe823f7 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xe70e78fe823f7 and rm_val == 3  #nosat<br>                                                                                            |[0x800013e0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800013e4]:csrrs a7, fflags, zero<br> [0x800013e8]:fsd ft11, 704(a5)<br> [0x800013ec]:sw a7, 708(a5)<br>   |
| 173|[0x80005ddc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xce7352604fe6b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xce7352604fe6b and rm_val == 3  #nosat<br>                                                                                            |[0x800013fc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001400]:csrrs a7, fflags, zero<br> [0x80001404]:fsd ft11, 720(a5)<br> [0x80001408]:sw a7, 724(a5)<br>   |
| 174|[0x80005dec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x676d1681c4823 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x676d1681c4823 and rm_val == 3  #nosat<br>                                                                                            |[0x80001418]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000141c]:csrrs a7, fflags, zero<br> [0x80001420]:fsd ft11, 736(a5)<br> [0x80001424]:sw a7, 740(a5)<br>   |
| 175|[0x80005dfc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x058fe9a4daa6f and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x058fe9a4daa6f and rm_val == 3  #nosat<br>                                                                                            |[0x80001434]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001438]:csrrs a7, fflags, zero<br> [0x8000143c]:fsd ft11, 752(a5)<br> [0x80001440]:sw a7, 756(a5)<br>   |
| 176|[0x80005e0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb992011891a75 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xb992011891a75 and rm_val == 3  #nosat<br>                                                                                            |[0x80001450]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 768(a5)<br> [0x8000145c]:sw a7, 772(a5)<br>   |
| 177|[0x80005e1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x0b2db44ae8c01 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x0b2db44ae8c01 and rm_val == 3  #nosat<br>                                                                                            |[0x8000146c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001470]:csrrs a7, fflags, zero<br> [0x80001474]:fsd ft11, 784(a5)<br> [0x80001478]:sw a7, 788(a5)<br>   |
| 178|[0x80005e2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x792be19c2d7a1 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x792be19c2d7a1 and rm_val == 3  #nosat<br>                                                                                            |[0x80001488]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000148c]:csrrs a7, fflags, zero<br> [0x80001490]:fsd ft11, 800(a5)<br> [0x80001494]:sw a7, 804(a5)<br>   |
| 179|[0x80005e3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5bcd8bcde77b5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5bcd8bcde77b5 and rm_val == 3  #nosat<br>                                                                                            |[0x800014a4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800014a8]:csrrs a7, fflags, zero<br> [0x800014ac]:fsd ft11, 816(a5)<br> [0x800014b0]:sw a7, 820(a5)<br>   |
| 180|[0x80005e4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf4587ce4e6a55 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xf4587ce4e6a55 and rm_val == 3  #nosat<br>                                                                                            |[0x800014c0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800014c4]:csrrs a7, fflags, zero<br> [0x800014c8]:fsd ft11, 832(a5)<br> [0x800014cc]:sw a7, 836(a5)<br>   |
| 181|[0x80005e5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x09badb528c6c8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x09badb528c6c8 and rm_val == 3  #nosat<br>                                                                                            |[0x800014dc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800014e0]:csrrs a7, fflags, zero<br> [0x800014e4]:fsd ft11, 848(a5)<br> [0x800014e8]:sw a7, 852(a5)<br>   |
| 182|[0x80005e6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0x01430191b8abf and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0x01430191b8abf and rm_val == 3  #nosat<br>                                                                                            |[0x800014f8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800014fc]:csrrs a7, fflags, zero<br> [0x80001500]:fsd ft11, 864(a5)<br> [0x80001504]:sw a7, 868(a5)<br>   |
| 183|[0x80005e7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7feee78e25d36 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x7feee78e25d36 and rm_val == 3  #nosat<br>                                                                                            |[0x80001514]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001518]:csrrs a7, fflags, zero<br> [0x8000151c]:fsd ft11, 880(a5)<br> [0x80001520]:sw a7, 884(a5)<br>   |
| 184|[0x80005e8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x0da8a99d945d7 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x0da8a99d945d7 and rm_val == 3  #nosat<br>                                                                                            |[0x80001530]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 896(a5)<br> [0x8000153c]:sw a7, 900(a5)<br>   |
| 185|[0x80005e9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf2f998bf74bb4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xf2f998bf74bb4 and rm_val == 3  #nosat<br>                                                                                            |[0x8000154c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001550]:csrrs a7, fflags, zero<br> [0x80001554]:fsd ft11, 912(a5)<br> [0x80001558]:sw a7, 916(a5)<br>   |
| 186|[0x80005eac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x40e45564208fa and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x40e45564208fa and rm_val == 3  #nosat<br>                                                                                            |[0x80001568]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000156c]:csrrs a7, fflags, zero<br> [0x80001570]:fsd ft11, 928(a5)<br> [0x80001574]:sw a7, 932(a5)<br>   |
| 187|[0x80005ebc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc9eec489f6667 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xc9eec489f6667 and rm_val == 3  #nosat<br>                                                                                            |[0x80001584]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001588]:csrrs a7, fflags, zero<br> [0x8000158c]:fsd ft11, 944(a5)<br> [0x80001590]:sw a7, 948(a5)<br>   |
| 188|[0x80005ecc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3526172ae3f6b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3526172ae3f6b and rm_val == 3  #nosat<br>                                                                                            |[0x800015a0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800015a4]:csrrs a7, fflags, zero<br> [0x800015a8]:fsd ft11, 960(a5)<br> [0x800015ac]:sw a7, 964(a5)<br>   |
| 189|[0x80005edc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x66315a9fdae1d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x66315a9fdae1d and rm_val == 3  #nosat<br>                                                                                            |[0x800015bc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800015c0]:csrrs a7, fflags, zero<br> [0x800015c4]:fsd ft11, 976(a5)<br> [0x800015c8]:sw a7, 980(a5)<br>   |
| 190|[0x80005eec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5fe6340fe9dff and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x5fe6340fe9dff and rm_val == 3  #nosat<br>                                                                                            |[0x800015d8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800015dc]:csrrs a7, fflags, zero<br> [0x800015e0]:fsd ft11, 992(a5)<br> [0x800015e4]:sw a7, 996(a5)<br>   |
| 191|[0x80005efc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae83ac33105f8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xae83ac33105f8 and rm_val == 3  #nosat<br>                                                                                            |[0x800015f4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800015f8]:csrrs a7, fflags, zero<br> [0x800015fc]:fsd ft11, 1008(a5)<br> [0x80001600]:sw a7, 1012(a5)<br> |
| 192|[0x80005f0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd13b901ecb86d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd13b901ecb86d and rm_val == 3  #nosat<br>                                                                                            |[0x80001610]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 1024(a5)<br> [0x8000161c]:sw a7, 1028(a5)<br> |
| 193|[0x80005f1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa8acc80de84a1 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xa8acc80de84a1 and rm_val == 3  #nosat<br>                                                                                            |[0x8000162c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001630]:csrrs a7, fflags, zero<br> [0x80001634]:fsd ft11, 1040(a5)<br> [0x80001638]:sw a7, 1044(a5)<br> |
| 194|[0x80005f2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9cd85f6af39ef and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x9cd85f6af39ef and rm_val == 3  #nosat<br>                                                                                            |[0x80001648]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000164c]:csrrs a7, fflags, zero<br> [0x80001650]:fsd ft11, 1056(a5)<br> [0x80001654]:sw a7, 1060(a5)<br> |
| 195|[0x80005f3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0bcd3d6ea260a and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x0bcd3d6ea260a and rm_val == 3  #nosat<br>                                                                                            |[0x80001664]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001668]:csrrs a7, fflags, zero<br> [0x8000166c]:fsd ft11, 1072(a5)<br> [0x80001670]:sw a7, 1076(a5)<br> |
| 196|[0x80005f4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x8b50ed3b44d4f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x8b50ed3b44d4f and rm_val == 3  #nosat<br>                                                                                            |[0x80001680]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001684]:csrrs a7, fflags, zero<br> [0x80001688]:fsd ft11, 1088(a5)<br> [0x8000168c]:sw a7, 1092(a5)<br> |
| 197|[0x80005f5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe989c8dd81bc5 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe989c8dd81bc5 and rm_val == 3  #nosat<br>                                                                                            |[0x8000169c]:fadd.d ft11, ft10, ft9, dyn<br> [0x800016a0]:csrrs a7, fflags, zero<br> [0x800016a4]:fsd ft11, 1104(a5)<br> [0x800016a8]:sw a7, 1108(a5)<br> |
| 198|[0x80005f6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x18d2ef084c097 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x18d2ef084c097 and rm_val == 3  #nosat<br>                                                                                            |[0x800016b8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800016bc]:csrrs a7, fflags, zero<br> [0x800016c0]:fsd ft11, 1120(a5)<br> [0x800016c4]:sw a7, 1124(a5)<br> |
| 199|[0x80005f7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x287ac6ae322ff and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x287ac6ae322ff and rm_val == 3  #nosat<br>                                                                                            |[0x800016d4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 1136(a5)<br> [0x800016e0]:sw a7, 1140(a5)<br> |
| 200|[0x80005f8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x4f961e264020f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x4f961e264020f and rm_val == 3  #nosat<br>                                                                                            |[0x800016f0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800016f4]:csrrs a7, fflags, zero<br> [0x800016f8]:fsd ft11, 1152(a5)<br> [0x800016fc]:sw a7, 1156(a5)<br> |
| 201|[0x80005f9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3db72bc24857c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x3db72bc24857c and rm_val == 3  #nosat<br>                                                                                            |[0x8000170c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001710]:csrrs a7, fflags, zero<br> [0x80001714]:fsd ft11, 1168(a5)<br> [0x80001718]:sw a7, 1172(a5)<br> |
| 202|[0x80005fac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6fdf2805ff4db and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6fdf2805ff4db and rm_val == 3  #nosat<br>                                                                                            |[0x80001728]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000172c]:csrrs a7, fflags, zero<br> [0x80001730]:fsd ft11, 1184(a5)<br> [0x80001734]:sw a7, 1188(a5)<br> |
| 203|[0x80005fbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5569022b338ff and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x5569022b338ff and rm_val == 3  #nosat<br>                                                                                            |[0x80001744]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001748]:csrrs a7, fflags, zero<br> [0x8000174c]:fsd ft11, 1200(a5)<br> [0x80001750]:sw a7, 1204(a5)<br> |
| 204|[0x80005fcc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f8 and fm1 == 0x238a22371e9ff and fs2 == 1 and fe2 == 0x7f8 and fm2 == 0x238a22371e9ff and rm_val == 3  #nosat<br>                                                                                            |[0x80001760]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001764]:csrrs a7, fflags, zero<br> [0x80001768]:fsd ft11, 1216(a5)<br> [0x8000176c]:sw a7, 1220(a5)<br> |
| 205|[0x80005fdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xa9aa2b6025f07 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa9aa2b6025f07 and rm_val == 3  #nosat<br>                                                                                            |[0x8000177c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001780]:csrrs a7, fflags, zero<br> [0x80001784]:fsd ft11, 1232(a5)<br> [0x80001788]:sw a7, 1236(a5)<br> |
| 206|[0x80005fec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7d6356ef8a62f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x7d6356ef8a62f and rm_val == 3  #nosat<br>                                                                                            |[0x80001798]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000179c]:csrrs a7, fflags, zero<br> [0x800017a0]:fsd ft11, 1248(a5)<br> [0x800017a4]:sw a7, 1252(a5)<br> |
| 207|[0x80005ffc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xce30065d5ac1b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xce30065d5ac1b and rm_val == 3  #nosat<br>                                                                                            |[0x800017b4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:fsd ft11, 1264(a5)<br> [0x800017c0]:sw a7, 1268(a5)<br> |
| 208|[0x8000600c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4f8b971fa5a72 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x4f8b971fa5a72 and rm_val == 3  #nosat<br>                                                                                            |[0x800017d0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800017d4]:csrrs a7, fflags, zero<br> [0x800017d8]:fsd ft11, 1280(a5)<br> [0x800017dc]:sw a7, 1284(a5)<br> |
| 209|[0x8000601c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7f8e997d84592 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x7f8e997d84592 and rm_val == 3  #nosat<br>                                                                                            |[0x800017ec]:fadd.d ft11, ft10, ft9, dyn<br> [0x800017f0]:csrrs a7, fflags, zero<br> [0x800017f4]:fsd ft11, 1296(a5)<br> [0x800017f8]:sw a7, 1300(a5)<br> |
| 210|[0x8000602c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1d803765d304 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1d803765d304 and rm_val == 3  #nosat<br>                                                                                            |[0x80001808]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000180c]:csrrs a7, fflags, zero<br> [0x80001810]:fsd ft11, 1312(a5)<br> [0x80001814]:sw a7, 1316(a5)<br> |
| 211|[0x8000603c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe49bfb977b300 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe49bfb977b300 and rm_val == 3  #nosat<br>                                                                                            |[0x80001824]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001828]:csrrs a7, fflags, zero<br> [0x8000182c]:fsd ft11, 1328(a5)<br> [0x80001830]:sw a7, 1332(a5)<br> |
| 212|[0x8000604c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5cab9bd09e6c4 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5cab9bd09e6c4 and rm_val == 3  #nosat<br>                                                                                            |[0x80001840]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001844]:csrrs a7, fflags, zero<br> [0x80001848]:fsd ft11, 1344(a5)<br> [0x8000184c]:sw a7, 1348(a5)<br> |
| 213|[0x8000605c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x30526056a01ff and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0x30526056a01ff and rm_val == 3  #nosat<br>                                                                                            |[0x8000185c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001860]:csrrs a7, fflags, zero<br> [0x80001864]:fsd ft11, 1360(a5)<br> [0x80001868]:sw a7, 1364(a5)<br> |
| 214|[0x8000606c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbe64efc9e258d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xbe64efc9e258d and rm_val == 3  #nosat<br>                                                                                            |[0x80001878]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000187c]:csrrs a7, fflags, zero<br> [0x80001880]:fsd ft11, 1376(a5)<br> [0x80001884]:sw a7, 1380(a5)<br> |
| 215|[0x8000607c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d3375e946b52 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x4d3375e946b52 and rm_val == 3  #nosat<br>                                                                                            |[0x80001894]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001898]:csrrs a7, fflags, zero<br> [0x8000189c]:fsd ft11, 1392(a5)<br> [0x800018a0]:sw a7, 1396(a5)<br> |
| 216|[0x8000608c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x113ecba7502a7 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x113ecba7502a7 and rm_val == 3  #nosat<br>                                                                                            |[0x800018b0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800018b4]:csrrs a7, fflags, zero<br> [0x800018b8]:fsd ft11, 1408(a5)<br> [0x800018bc]:sw a7, 1412(a5)<br> |
| 217|[0x8000609c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x95adca0768ede and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x95adca0768ede and rm_val == 3  #nosat<br>                                                                                            |[0x800018cc]:fadd.d ft11, ft10, ft9, dyn<br> [0x800018d0]:csrrs a7, fflags, zero<br> [0x800018d4]:fsd ft11, 1424(a5)<br> [0x800018d8]:sw a7, 1428(a5)<br> |
| 218|[0x800060ac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x194e95f4fa0e5 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x194e95f4fa0e5 and rm_val == 3  #nosat<br>                                                                                            |[0x800018e8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800018ec]:csrrs a7, fflags, zero<br> [0x800018f0]:fsd ft11, 1440(a5)<br> [0x800018f4]:sw a7, 1444(a5)<br> |
| 219|[0x800060bc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x963785d0567a5 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x963785d0567a5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001904]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001908]:csrrs a7, fflags, zero<br> [0x8000190c]:fsd ft11, 1456(a5)<br> [0x80001910]:sw a7, 1460(a5)<br> |
| 220|[0x800060cc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a5710f3828f7 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x9a5710f3828f7 and rm_val == 3  #nosat<br>                                                                                            |[0x80001920]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001924]:csrrs a7, fflags, zero<br> [0x80001928]:fsd ft11, 1472(a5)<br> [0x8000192c]:sw a7, 1476(a5)<br> |
| 221|[0x800060dc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1eb3cbd822141 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1eb3cbd822141 and rm_val == 3  #nosat<br>                                                                                            |[0x8000193c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001940]:csrrs a7, fflags, zero<br> [0x80001944]:fsd ft11, 1488(a5)<br> [0x80001948]:sw a7, 1492(a5)<br> |
| 222|[0x800060ec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4c6c848cb47df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x4c6c848cb47df and rm_val == 3  #nosat<br>                                                                                            |[0x80001958]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000195c]:csrrs a7, fflags, zero<br> [0x80001960]:fsd ft11, 1504(a5)<br> [0x80001964]:sw a7, 1508(a5)<br> |
| 223|[0x800060fc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9086506183f67 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x9086506183f67 and rm_val == 3  #nosat<br>                                                                                            |[0x80001974]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:fsd ft11, 1520(a5)<br> [0x80001980]:sw a7, 1524(a5)<br> |
| 224|[0x8000610c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa53d0d2b3faec and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa53d0d2b3faec and rm_val == 3  #nosat<br>                                                                                            |[0x80001990]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001994]:csrrs a7, fflags, zero<br> [0x80001998]:fsd ft11, 1536(a5)<br> [0x8000199c]:sw a7, 1540(a5)<br> |
| 225|[0x8000611c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6f451c304de2e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6f451c304de2e and rm_val == 3  #nosat<br>                                                                                            |[0x800019ac]:fadd.d ft11, ft10, ft9, dyn<br> [0x800019b0]:csrrs a7, fflags, zero<br> [0x800019b4]:fsd ft11, 1552(a5)<br> [0x800019b8]:sw a7, 1556(a5)<br> |
| 226|[0x8000612c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x43c3f0806f2cd and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x43c3f0806f2cd and rm_val == 3  #nosat<br>                                                                                            |[0x800019c8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800019cc]:csrrs a7, fflags, zero<br> [0x800019d0]:fsd ft11, 1568(a5)<br> [0x800019d4]:sw a7, 1572(a5)<br> |
| 227|[0x8000613c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9b75de798ac5f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x9b75de798ac5f and rm_val == 3  #nosat<br>                                                                                            |[0x800019e4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800019e8]:csrrs a7, fflags, zero<br> [0x800019ec]:fsd ft11, 1584(a5)<br> [0x800019f0]:sw a7, 1588(a5)<br> |
| 228|[0x8000614c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x737bdc485a77d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x737bdc485a77d and rm_val == 3  #nosat<br>                                                                                            |[0x80001a00]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a04]:csrrs a7, fflags, zero<br> [0x80001a08]:fsd ft11, 1600(a5)<br> [0x80001a0c]:sw a7, 1604(a5)<br> |
| 229|[0x8000615c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x442435bea0eb5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x442435bea0eb5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001a1c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a20]:csrrs a7, fflags, zero<br> [0x80001a24]:fsd ft11, 1616(a5)<br> [0x80001a28]:sw a7, 1620(a5)<br> |
| 230|[0x8000616c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x8106d28c6e8ff and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x8106d28c6e8ff and rm_val == 3  #nosat<br>                                                                                            |[0x80001a38]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a3c]:csrrs a7, fflags, zero<br> [0x80001a40]:fsd ft11, 1632(a5)<br> [0x80001a44]:sw a7, 1636(a5)<br> |
| 231|[0x8000617c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc00223fe58e9e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xc00223fe58e9e and rm_val == 3  #nosat<br>                                                                                            |[0x80001a54]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:fsd ft11, 1648(a5)<br> [0x80001a60]:sw a7, 1652(a5)<br> |
| 232|[0x8000618c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xde18ff8661b6b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xde18ff8661b6b and rm_val == 3  #nosat<br>                                                                                            |[0x80001a70]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a74]:csrrs a7, fflags, zero<br> [0x80001a78]:fsd ft11, 1664(a5)<br> [0x80001a7c]:sw a7, 1668(a5)<br> |
| 233|[0x8000619c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfa73e129b8879 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xfa73e129b8879 and rm_val == 3  #nosat<br>                                                                                            |[0x80001a8c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001a90]:csrrs a7, fflags, zero<br> [0x80001a94]:fsd ft11, 1680(a5)<br> [0x80001a98]:sw a7, 1684(a5)<br> |
| 234|[0x80005f04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3eebb35310409 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x3eebb35310409 and rm_val == 3  #nosat<br>                                                                                            |[0x80001d00]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d04]:csrrs a7, fflags, zero<br> [0x80001d08]:fsd ft11, 0(a5)<br> [0x80001d0c]:sw a7, 4(a5)<br>       |
| 235|[0x80005f14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2362beb7fcccc and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x2362beb7fcccc and rm_val == 3  #nosat<br>                                                                                            |[0x80001d1c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d20]:csrrs a7, fflags, zero<br> [0x80001d24]:fsd ft11, 16(a5)<br> [0x80001d28]:sw a7, 20(a5)<br>     |
| 236|[0x80005f24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe2f1c5d734347 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xe2f1c5d734347 and rm_val == 3  #nosat<br>                                                                                            |[0x80001d38]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d3c]:csrrs a7, fflags, zero<br> [0x80001d40]:fsd ft11, 32(a5)<br> [0x80001d44]:sw a7, 36(a5)<br>     |
| 237|[0x80005f34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc4edf85532923 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xc4edf85532923 and rm_val == 3  #nosat<br>                                                                                            |[0x80001d54]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d58]:csrrs a7, fflags, zero<br> [0x80001d5c]:fsd ft11, 48(a5)<br> [0x80001d60]:sw a7, 52(a5)<br>     |
| 238|[0x80005f44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x00b42e8f00d47 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x00b42e8f00d47 and rm_val == 3  #nosat<br>                                                                                            |[0x80001d70]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d74]:csrrs a7, fflags, zero<br> [0x80001d78]:fsd ft11, 64(a5)<br> [0x80001d7c]:sw a7, 68(a5)<br>     |
| 239|[0x80005f54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb0db7e0a5d748 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xb0db7e0a5d748 and rm_val == 3  #nosat<br>                                                                                            |[0x80001d8c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001d90]:csrrs a7, fflags, zero<br> [0x80001d94]:fsd ft11, 80(a5)<br> [0x80001d98]:sw a7, 84(a5)<br>     |
| 240|[0x80005f64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xdb8da7279369f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xdb8da7279369f and rm_val == 3  #nosat<br>                                                                                            |[0x80001da8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001dac]:csrrs a7, fflags, zero<br> [0x80001db0]:fsd ft11, 96(a5)<br> [0x80001db4]:sw a7, 100(a5)<br>    |
| 241|[0x80005f74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5864580d04bef and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5864580d04bef and rm_val == 3  #nosat<br>                                                                                            |[0x80001dc4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001dc8]:csrrs a7, fflags, zero<br> [0x80001dcc]:fsd ft11, 112(a5)<br> [0x80001dd0]:sw a7, 116(a5)<br>   |
| 242|[0x80005f84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd64347e477166 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd64347e477166 and rm_val == 3  #nosat<br>                                                                                            |[0x80001de0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001de4]:csrrs a7, fflags, zero<br> [0x80001de8]:fsd ft11, 128(a5)<br> [0x80001dec]:sw a7, 132(a5)<br>   |
| 243|[0x80005f94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x76940d9e18057 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x76940d9e18057 and rm_val == 3  #nosat<br>                                                                                            |[0x80001dfc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e00]:csrrs a7, fflags, zero<br> [0x80001e04]:fsd ft11, 144(a5)<br> [0x80001e08]:sw a7, 148(a5)<br>   |
| 244|[0x80005fa4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x9847d9429817b and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x9847d9429817b and rm_val == 3  #nosat<br>                                                                                            |[0x80001e18]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e1c]:csrrs a7, fflags, zero<br> [0x80001e20]:fsd ft11, 160(a5)<br> [0x80001e24]:sw a7, 164(a5)<br>   |
| 245|[0x80005fb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9d5f97660dadf and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x9d5f97660dadf and rm_val == 3  #nosat<br>                                                                                            |[0x80001e34]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e38]:csrrs a7, fflags, zero<br> [0x80001e3c]:fsd ft11, 176(a5)<br> [0x80001e40]:sw a7, 180(a5)<br>   |
| 246|[0x80005fc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x01dca4dde57a5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x01dca4dde57a5 and rm_val == 3  #nosat<br>                                                                                            |[0x80001e50]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e54]:csrrs a7, fflags, zero<br> [0x80001e58]:fsd ft11, 192(a5)<br> [0x80001e5c]:sw a7, 196(a5)<br>   |
| 247|[0x80005fd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x66b37637d118d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x66b37637d118d and rm_val == 3  #nosat<br>                                                                                            |[0x80001e6c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e70]:csrrs a7, fflags, zero<br> [0x80001e74]:fsd ft11, 208(a5)<br> [0x80001e78]:sw a7, 212(a5)<br>   |
| 248|[0x80005fe4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc44223126cbc7 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xc44223126cbc7 and rm_val == 3  #nosat<br>                                                                                            |[0x80001e88]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001e8c]:csrrs a7, fflags, zero<br> [0x80001e90]:fsd ft11, 224(a5)<br> [0x80001e94]:sw a7, 228(a5)<br>   |
| 249|[0x80005ff4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9fbeb1abfb6e7 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x9fbeb1abfb6e7 and rm_val == 3  #nosat<br>                                                                                            |[0x80001ea4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001ea8]:csrrs a7, fflags, zero<br> [0x80001eac]:fsd ft11, 240(a5)<br> [0x80001eb0]:sw a7, 244(a5)<br>   |
| 250|[0x80006004]<br>0x00000005|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x75450c5a9817f and fs2 == 1 and fe2 == 0x7f9 and fm2 == 0x75450c5a9817f and rm_val == 3  #nosat<br>                                                                                            |[0x80001ec0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001ec4]:csrrs a7, fflags, zero<br> [0x80001ec8]:fsd ft11, 256(a5)<br> [0x80001ecc]:sw a7, 260(a5)<br>   |
| 251|[0x80006014]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x99fb7503e8d08 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x99fb7503e8d08 and rm_val == 3  #nosat<br>                                                                                            |[0x80001edc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001ee0]:csrrs a7, fflags, zero<br> [0x80001ee4]:fsd ft11, 272(a5)<br> [0x80001ee8]:sw a7, 276(a5)<br>   |
| 252|[0x80006024]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xfb797ef55e1cf and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xfb797ef55e1cf and rm_val == 3  #nosat<br>                                                                                            |[0x80001ef8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001efc]:csrrs a7, fflags, zero<br> [0x80001f00]:fsd ft11, 288(a5)<br> [0x80001f04]:sw a7, 292(a5)<br>   |
| 253|[0x80006034]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xec0c4abe1fd0e and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xec0c4abe1fd0e and rm_val == 3  #nosat<br>                                                                                            |[0x80001f14]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001f18]:csrrs a7, fflags, zero<br> [0x80001f1c]:fsd ft11, 304(a5)<br> [0x80001f20]:sw a7, 308(a5)<br>   |
| 254|[0x80006044]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x2a1fa26c0948f and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x2a1fa26c0948f and rm_val == 3  #nosat<br>                                                                                            |[0x80001f30]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001f34]:csrrs a7, fflags, zero<br> [0x80001f38]:fsd ft11, 320(a5)<br> [0x80001f3c]:sw a7, 324(a5)<br>   |
| 255|[0x80006054]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7b05f6eabb69f and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x7b05f6eabb69f and rm_val == 3  #nosat<br>                                                                                            |[0x80001f4c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001f50]:csrrs a7, fflags, zero<br> [0x80001f54]:fsd ft11, 336(a5)<br> [0x80001f58]:sw a7, 340(a5)<br>   |
| 256|[0x80006064]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x26e34e07a9172 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x26e34e07a9172 and rm_val == 3  #nosat<br>                                                                                            |[0x80001f68]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001f6c]:csrrs a7, fflags, zero<br> [0x80001f70]:fsd ft11, 352(a5)<br> [0x80001f74]:sw a7, 356(a5)<br>   |
| 257|[0x80006074]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x451eb54c10b8b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x451eb54c10b8b and rm_val == 3  #nosat<br>                                                                                            |[0x80001f84]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001f88]:csrrs a7, fflags, zero<br> [0x80001f8c]:fsd ft11, 368(a5)<br> [0x80001f90]:sw a7, 372(a5)<br>   |
| 258|[0x80006084]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5c762dc4bc5d6 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x5c762dc4bc5d6 and rm_val == 3  #nosat<br>                                                                                            |[0x80001fa0]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001fa4]:csrrs a7, fflags, zero<br> [0x80001fa8]:fsd ft11, 384(a5)<br> [0x80001fac]:sw a7, 388(a5)<br>   |
| 259|[0x80006094]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd1ed9e7beff05 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xd1ed9e7beff05 and rm_val == 3  #nosat<br>                                                                                            |[0x80001fbc]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001fc0]:csrrs a7, fflags, zero<br> [0x80001fc4]:fsd ft11, 400(a5)<br> [0x80001fc8]:sw a7, 404(a5)<br>   |
| 260|[0x800060a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x728eb744bb2ef and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x728eb744bb2ef and rm_val == 3  #nosat<br>                                                                                            |[0x80001fd8]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001fdc]:csrrs a7, fflags, zero<br> [0x80001fe0]:fsd ft11, 416(a5)<br> [0x80001fe4]:sw a7, 420(a5)<br>   |
| 261|[0x800060b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bbdffdaf66c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x2bbdffdaf66c3 and rm_val == 3  #nosat<br>                                                                                            |[0x80001ff4]:fadd.d ft11, ft10, ft9, dyn<br> [0x80001ff8]:csrrs a7, fflags, zero<br> [0x80001ffc]:fsd ft11, 432(a5)<br> [0x80002000]:sw a7, 436(a5)<br>   |
| 262|[0x800060c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa1bf5c83faf60 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xa1bf5c83faf60 and rm_val == 3  #nosat<br>                                                                                            |[0x80002010]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002014]:csrrs a7, fflags, zero<br> [0x80002018]:fsd ft11, 448(a5)<br> [0x8000201c]:sw a7, 452(a5)<br>   |
| 263|[0x800060d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96d3944ae92c5 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x96d3944ae92c5 and rm_val == 3  #nosat<br>                                                                                            |[0x8000202c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002030]:csrrs a7, fflags, zero<br> [0x80002034]:fsd ft11, 464(a5)<br> [0x80002038]:sw a7, 468(a5)<br>   |
| 264|[0x800060e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xdfc83569216bf and fs2 == 1 and fe2 == 0x7fb and fm2 == 0xdfc83569216bf and rm_val == 3  #nosat<br>                                                                                            |[0x80002048]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000204c]:csrrs a7, fflags, zero<br> [0x80002050]:fsd ft11, 480(a5)<br> [0x80002054]:sw a7, 484(a5)<br>   |
| 265|[0x800060f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf1bca90426463 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xf1bca90426463 and rm_val == 3  #nosat<br>                                                                                            |[0x80002064]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002068]:csrrs a7, fflags, zero<br> [0x8000206c]:fsd ft11, 496(a5)<br> [0x80002070]:sw a7, 500(a5)<br>   |
| 266|[0x80006104]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x42a2ac1575123 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x42a2ac1575123 and rm_val == 3  #nosat<br>                                                                                            |[0x80002080]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002084]:csrrs a7, fflags, zero<br> [0x80002088]:fsd ft11, 512(a5)<br> [0x8000208c]:sw a7, 516(a5)<br>   |
| 267|[0x80006114]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x39beb50761e3d and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x39beb50761e3d and rm_val == 3  #nosat<br>                                                                                            |[0x8000209c]:fadd.d ft11, ft10, ft9, dyn<br> [0x800020a0]:csrrs a7, fflags, zero<br> [0x800020a4]:fsd ft11, 528(a5)<br> [0x800020a8]:sw a7, 532(a5)<br>   |
| 268|[0x80006124]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x962eb496df1c1 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x962eb496df1c1 and rm_val == 3  #nosat<br>                                                                                            |[0x800020b8]:fadd.d ft11, ft10, ft9, dyn<br> [0x800020bc]:csrrs a7, fflags, zero<br> [0x800020c0]:fsd ft11, 544(a5)<br> [0x800020c4]:sw a7, 548(a5)<br>   |
| 269|[0x80006134]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfe1581ecd07ea and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfe1581ecd07ea and rm_val == 3  #nosat<br>                                                                                            |[0x800020d4]:fadd.d ft11, ft10, ft9, dyn<br> [0x800020d8]:csrrs a7, fflags, zero<br> [0x800020dc]:fsd ft11, 560(a5)<br> [0x800020e0]:sw a7, 564(a5)<br>   |
| 270|[0x80006144]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xcd606a3f0f54d and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xcd606a3f0f54d and rm_val == 3  #nosat<br>                                                                                            |[0x800020f0]:fadd.d ft11, ft10, ft9, dyn<br> [0x800020f4]:csrrs a7, fflags, zero<br> [0x800020f8]:fsd ft11, 576(a5)<br> [0x800020fc]:sw a7, 580(a5)<br>   |
| 271|[0x80006154]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe64794dad7d48 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xe64794dad7d48 and rm_val == 3  #nosat<br>                                                                                            |[0x8000210c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002110]:csrrs a7, fflags, zero<br> [0x80002114]:fsd ft11, 592(a5)<br> [0x80002118]:sw a7, 596(a5)<br>   |
| 272|[0x80006164]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xca428c2b7c81f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xca428c2b7c81f and rm_val == 3  #nosat<br>                                                                                            |[0x80002128]:fadd.d ft11, ft10, ft9, dyn<br> [0x8000212c]:csrrs a7, fflags, zero<br> [0x80002130]:fsd ft11, 608(a5)<br> [0x80002134]:sw a7, 612(a5)<br>   |
| 273|[0x80006174]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9f8dcc4f1275c and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x9f8dcc4f1275c and rm_val == 3  #nosat<br>                                                                                            |[0x80002144]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002148]:csrrs a7, fflags, zero<br> [0x8000214c]:fsd ft11, 624(a5)<br> [0x80002150]:sw a7, 628(a5)<br>   |
| 274|[0x80006184]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x691ae7e1929e8 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x691ae7e1929e8 and rm_val == 3  #nosat<br>                                                                                            |[0x80002160]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002164]:csrrs a7, fflags, zero<br> [0x80002168]:fsd ft11, 640(a5)<br> [0x8000216c]:sw a7, 644(a5)<br>   |
| 275|[0x80006194]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x14c9836bbe6ff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x14c9836bbe6ff and rm_val == 3  #nosat<br>                                                                                            |[0x8000217c]:fadd.d ft11, ft10, ft9, dyn<br> [0x80002180]:csrrs a7, fflags, zero<br> [0x80002184]:fsd ft11, 656(a5)<br> [0x80002188]:sw a7, 660(a5)<br>   |
