
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800006e0')]      |
| SIG_REGION                | [('0x80002510', '0x800026c0', '108 words')]      |
| COV_LABELS                | fadd_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fadd/riscof_work/fadd_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 112      |
| Total Signature Updates   | 27      |
| STAT1                     | 27      |
| STAT2                     | 0      |
| STAT3                     | 25     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000414]:fadd.d ft7, ft2, fa5, dyn
[0x80000418]:csrrs a7, fflags, zero
[0x8000041c]:fsd ft7, 432(a5)
[0x80000420]:sw a7, 436(a5)
[0x80000424]:fld fs3, 448(a6)
[0x80000428]:fld ft6, 456(a6)
[0x8000042c]:csrrwi zero, frm, 0

[0x80000430]:fadd.d ft2, fs3, ft6, dyn
[0x80000434]:csrrs a7, fflags, zero
[0x80000438]:fsd ft2, 448(a5)
[0x8000043c]:sw a7, 452(a5)
[0x80000440]:fld fa6, 464(a6)
[0x80000444]:fld fa2, 472(a6)
[0x80000448]:csrrwi zero, frm, 0

[0x8000044c]:fadd.d ft10, fa6, fa2, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:fsd ft10, 464(a5)
[0x80000458]:sw a7, 468(a5)
[0x8000045c]:fld fs2, 480(a6)
[0x80000460]:fld ft11, 488(a6)
[0x80000464]:csrrwi zero, frm, 0

[0x80000468]:fadd.d ft9, fs2, ft11, dyn
[0x8000046c]:csrrs a7, fflags, zero
[0x80000470]:fsd ft9, 480(a5)
[0x80000474]:sw a7, 484(a5)
[0x80000478]:fld ft11, 496(a6)
[0x8000047c]:fld ft10, 504(a6)
[0x80000480]:csrrwi zero, frm, 0

[0x80000484]:fadd.d fa1, ft11, ft10, dyn
[0x80000488]:csrrs a7, fflags, zero
[0x8000048c]:fsd fa1, 496(a5)
[0x80000490]:sw a7, 500(a5)
[0x80000494]:fld ft10, 512(a6)
[0x80000498]:fld ft9, 520(a6)
[0x8000049c]:csrrwi zero, frm, 0

[0x800004a0]:fadd.d ft11, ft10, ft9, dyn
[0x800004a4]:csrrs a7, fflags, zero
[0x800004a8]:fsd ft11, 512(a5)
[0x800004ac]:sw a7, 516(a5)
[0x800004b0]:fld ft10, 528(a6)
[0x800004b4]:fld ft9, 536(a6)
[0x800004b8]:csrrwi zero, frm, 0

[0x800004bc]:fadd.d ft11, ft10, ft9, dyn
[0x800004c0]:csrrs a7, fflags, zero
[0x800004c4]:fsd ft11, 528(a5)
[0x800004c8]:sw a7, 532(a5)
[0x800004cc]:fld ft10, 544(a6)
[0x800004d0]:fld ft9, 552(a6)
[0x800004d4]:csrrwi zero, frm, 0

[0x800004d8]:fadd.d ft11, ft10, ft9, dyn
[0x800004dc]:csrrs a7, fflags, zero
[0x800004e0]:fsd ft11, 544(a5)
[0x800004e4]:sw a7, 548(a5)
[0x800004e8]:fld ft10, 560(a6)
[0x800004ec]:fld ft9, 568(a6)
[0x800004f0]:csrrwi zero, frm, 0

[0x800004f4]:fadd.d ft11, ft10, ft9, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:fsd ft11, 560(a5)
[0x80000500]:sw a7, 564(a5)
[0x80000504]:fld ft10, 576(a6)
[0x80000508]:fld ft9, 584(a6)
[0x8000050c]:csrrwi zero, frm, 0

[0x80000510]:fadd.d ft11, ft10, ft9, dyn
[0x80000514]:csrrs a7, fflags, zero
[0x80000518]:fsd ft11, 576(a5)
[0x8000051c]:sw a7, 580(a5)
[0x80000520]:fld ft10, 592(a6)
[0x80000524]:fld ft9, 600(a6)
[0x80000528]:csrrwi zero, frm, 0

[0x8000052c]:fadd.d ft11, ft10, ft9, dyn
[0x80000530]:csrrs a7, fflags, zero
[0x80000534]:fsd ft11, 592(a5)
[0x80000538]:sw a7, 596(a5)
[0x8000053c]:fld ft10, 608(a6)
[0x80000540]:fld ft9, 616(a6)
[0x80000544]:csrrwi zero, frm, 0

[0x80000548]:fadd.d ft11, ft10, ft9, dyn
[0x8000054c]:csrrs a7, fflags, zero
[0x80000550]:fsd ft11, 608(a5)
[0x80000554]:sw a7, 612(a5)
[0x80000558]:fld ft10, 624(a6)
[0x8000055c]:fld ft9, 632(a6)
[0x80000560]:csrrwi zero, frm, 0

[0x80000564]:fadd.d ft11, ft10, ft9, dyn
[0x80000568]:csrrs a7, fflags, zero
[0x8000056c]:fsd ft11, 624(a5)
[0x80000570]:sw a7, 628(a5)
[0x80000574]:fld ft10, 640(a6)
[0x80000578]:fld ft9, 648(a6)
[0x8000057c]:csrrwi zero, frm, 0

[0x80000580]:fadd.d ft11, ft10, ft9, dyn
[0x80000584]:csrrs a7, fflags, zero
[0x80000588]:fsd ft11, 640(a5)
[0x8000058c]:sw a7, 644(a5)
[0x80000590]:fld ft10, 656(a6)
[0x80000594]:fld ft9, 664(a6)
[0x80000598]:csrrwi zero, frm, 0

[0x8000059c]:fadd.d ft11, ft10, ft9, dyn
[0x800005a0]:csrrs a7, fflags, zero
[0x800005a4]:fsd ft11, 656(a5)
[0x800005a8]:sw a7, 660(a5)
[0x800005ac]:fld ft10, 672(a6)
[0x800005b0]:fld ft9, 680(a6)
[0x800005b4]:csrrwi zero, frm, 0

[0x800005b8]:fadd.d ft11, ft10, ft9, dyn
[0x800005bc]:csrrs a7, fflags, zero
[0x800005c0]:fsd ft11, 672(a5)
[0x800005c4]:sw a7, 676(a5)
[0x800005c8]:fld ft10, 688(a6)
[0x800005cc]:fld ft9, 696(a6)
[0x800005d0]:csrrwi zero, frm, 0

[0x800005d4]:fadd.d ft11, ft10, ft9, dyn
[0x800005d8]:csrrs a7, fflags, zero
[0x800005dc]:fsd ft11, 688(a5)
[0x800005e0]:sw a7, 692(a5)
[0x800005e4]:fld ft10, 704(a6)
[0x800005e8]:fld ft9, 712(a6)
[0x800005ec]:csrrwi zero, frm, 0

[0x800005f0]:fadd.d ft11, ft10, ft9, dyn
[0x800005f4]:csrrs a7, fflags, zero
[0x800005f8]:fsd ft11, 704(a5)
[0x800005fc]:sw a7, 708(a5)
[0x80000600]:fld ft10, 720(a6)
[0x80000604]:fld ft9, 728(a6)
[0x80000608]:csrrwi zero, frm, 0

[0x8000060c]:fadd.d ft11, ft10, ft9, dyn
[0x80000610]:csrrs a7, fflags, zero
[0x80000614]:fsd ft11, 720(a5)
[0x80000618]:sw a7, 724(a5)
[0x8000061c]:fld ft10, 736(a6)
[0x80000620]:fld ft9, 744(a6)
[0x80000624]:csrrwi zero, frm, 0

[0x80000628]:fadd.d ft11, ft10, ft9, dyn
[0x8000062c]:csrrs a7, fflags, zero
[0x80000630]:fsd ft11, 736(a5)
[0x80000634]:sw a7, 740(a5)
[0x80000638]:fld ft10, 752(a6)
[0x8000063c]:fld ft9, 760(a6)
[0x80000640]:csrrwi zero, frm, 0

[0x80000644]:fadd.d ft11, ft10, ft9, dyn
[0x80000648]:csrrs a7, fflags, zero
[0x8000064c]:fsd ft11, 752(a5)
[0x80000650]:sw a7, 756(a5)
[0x80000654]:fld ft10, 768(a6)
[0x80000658]:fld ft9, 776(a6)
[0x8000065c]:csrrwi zero, frm, 0

[0x80000660]:fadd.d ft11, ft10, ft9, dyn
[0x80000664]:csrrs a7, fflags, zero
[0x80000668]:fsd ft11, 768(a5)
[0x8000066c]:sw a7, 772(a5)
[0x80000670]:fld ft10, 784(a6)
[0x80000674]:fld ft9, 792(a6)
[0x80000678]:csrrwi zero, frm, 0

[0x8000067c]:fadd.d ft11, ft10, ft9, dyn
[0x80000680]:csrrs a7, fflags, zero
[0x80000684]:fsd ft11, 784(a5)
[0x80000688]:sw a7, 788(a5)
[0x8000068c]:fld ft10, 800(a6)
[0x80000690]:fld ft9, 808(a6)
[0x80000694]:csrrwi zero, frm, 0

[0x80000698]:fadd.d ft11, ft10, ft9, dyn
[0x8000069c]:csrrs a7, fflags, zero
[0x800006a0]:fsd ft11, 800(a5)
[0x800006a4]:sw a7, 804(a5)
[0x800006a8]:fld ft10, 816(a6)
[0x800006ac]:fld ft9, 824(a6)
[0x800006b0]:csrrwi zero, frm, 0

[0x800006b4]:fadd.d ft11, ft10, ft9, dyn
[0x800006b8]:csrrs a7, fflags, zero
[0x800006bc]:fsd ft11, 816(a5)
[0x800006c0]:sw a7, 820(a5)
[0x800006c4]:fld ft10, 832(a6)
[0x800006c8]:fld ft9, 840(a6)
[0x800006cc]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                              |                                                                         code                                                                          |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002514]<br>0x00000005|- opcode : fadd.d<br> - rs1 : f28<br> - rs2 : f28<br> - rd : f17<br> - rs1 == rs2 != rd<br>                                                                                                                                            |[0x80000120]:fadd.d fa7, ft8, ft8, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd fa7, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>       |
|   2|[0x80002524]<br>0x00000005|- rs1 : f0<br> - rs2 : f1<br> - rd : f0<br> - rs1 == rd != rs2<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x39beb50761e3d and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x8000013c]:fadd.d ft0, ft0, ft1, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd ft0, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>     |
|   3|[0x80002534]<br>0x00000005|- rs1 : f3<br> - rs2 : f8<br> - rd : f8<br> - rs2 == rd != rs1<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x962eb496df1c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc05b7f6ba0d90 and rm_val == 0  #nosat<br>                         |[0x80000158]:fadd.d fs0, ft3, fs0, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fs0, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>     |
|   4|[0x80002544]<br>0x00000005|- rs1 : f20<br> - rs2 : f9<br> - rd : f1<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfe1581ecd07ea and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x80000174]:fadd.d ft1, fs4, fs1, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd ft1, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>     |
|   5|[0x80002554]<br>0x00000005|- rs1 : f4<br> - rs2 : f4<br> - rd : f4<br> - rs1 == rs2 == rd<br>                                                                                                                                                                     |[0x80000190]:fadd.d ft4, ft4, ft4, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd ft4, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>     |
|   6|[0x80002564]<br>0x00000005|- rs1 : f11<br> - rs2 : f10<br> - rd : f20<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xe64794dad7d48 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x800001ac]:fadd.d fs4, fa1, fa0, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fs4, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>     |
|   7|[0x80002574]<br>0x00000005|- rs1 : f9<br> - rs2 : f23<br> - rd : f14<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xca428c2b7c81f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x800001c8]:fadd.d fa4, fs1, fs7, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd fa4, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>    |
|   8|[0x80002584]<br>0x00000005|- rs1 : f26<br> - rs2 : f27<br> - rd : f25<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9f8dcc4f1275c and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x800001e4]:fadd.d fs9, fs10, fs11, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fs9, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br> |
|   9|[0x80002594]<br>0x00000005|- rs1 : f21<br> - rs2 : f13<br> - rd : f5<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x691ae7e1929e8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf63ad242f7a0b and rm_val == 0  #nosat<br>                                              |[0x80000200]:fadd.d ft5, fs5, fa3, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd ft5, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>   |
|  10|[0x800025a4]<br>0x00000005|- rs1 : f7<br> - rs2 : f29<br> - rd : f13<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x14c9836bbe6ff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdd2178215e056 and rm_val == 0  #nosat<br>                                              |[0x8000021c]:fadd.d fa3, ft7, ft9, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd fa3, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>   |
|  11|[0x800025b4]<br>0x00000005|- rs1 : f22<br> - rs2 : f16<br> - rd : f26<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xcbdd58ecc1b45 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x80000238]:fadd.d fs10, fs6, fa6, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd fs10, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br> |
|  12|[0x800025c4]<br>0x00000005|- rs1 : f8<br> - rs2 : f20<br> - rd : f15<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xd8c56582791a6 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x80000254]:fadd.d fa5, fs0, fs4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fa5, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>   |
|  13|[0x800025d4]<br>0x00000005|- rs1 : f13<br> - rs2 : f21<br> - rd : f22<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x83e4a9485598d and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x80000270]:fadd.d fs6, fa3, fs5, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fs6, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>   |
|  14|[0x800025e4]<br>0x00000005|- rs1 : f30<br> - rs2 : f25<br> - rd : f9<br> - fs1 == 1 and fe1 == 0x7fd and fm1 == 0xe7f7bd88d7c8f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x21f9542fdc1b0 and rm_val == 0  #nosat<br>                                              |[0x8000028c]:fadd.d fs1, ft10, fs9, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd fs1, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>  |
|  15|[0x800025f4]<br>0x00000005|- rs1 : f1<br> - rs2 : f14<br> - rd : f27<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x39bd67fecd9d5 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x800002a8]:fadd.d fs11, ft1, fa4, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd fs11, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br> |
|  16|[0x80002604]<br>0x00000005|- rs1 : f17<br> - rs2 : f24<br> - rd : f12<br> - fs1 == 1 and fe1 == 0x7fc and fm1 == 0x83df99d24bacb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8125d36d5e46f and rm_val == 0  #nosat<br>                                             |[0x800002c4]:fadd.d fa2, fa7, fs8, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fa2, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>   |
|  17|[0x80002614]<br>0x00000005|- rs1 : f14<br> - rs2 : f0<br> - rd : f3<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x26bbbacf7eaef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbb61cc5b43304 and rm_val == 0  #nosat<br>                                               |[0x800002e0]:fadd.d ft3, fa4, ft0, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd ft3, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>   |
|  18|[0x80002624]<br>0x00000005|- rs1 : f25<br> - rs2 : f2<br> - rd : f31<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x314c82f3115df and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf65e46475bdcb and rm_val == 0  #nosat<br>                                              |[0x800002fc]:fadd.d ft11, fs9, ft2, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd ft11, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br> |
|  19|[0x80002634]<br>0x00000005|- rs1 : f24<br> - rs2 : f5<br> - rd : f10<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x2cdc24d268f9f and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x80000318]:fadd.d fa0, fs8, ft5, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd fa0, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>   |
|  20|[0x80002644]<br>0x00000005|- rs1 : f6<br> - rs2 : f17<br> - rd : f28<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xed7c3ef329d04 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x80000334]:fadd.d ft8, ft6, fa7, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd ft8, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>   |
|  21|[0x80002654]<br>0x00000005|- rs1 : f10<br> - rs2 : f3<br> - rd : f18<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xa101ccfb0623a and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x80000350]:fadd.d fs2, fa0, ft3, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs2, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>   |
|  22|[0x80002664]<br>0x00000005|- rs1 : f12<br> - rs2 : f19<br> - rd : f6<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x69c26ac7fce60 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x8000036c]:fadd.d ft6, fa2, fs3, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd ft6, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>   |
|  23|[0x80002674]<br>0x00000005|- rs1 : f29<br> - rs2 : f22<br> - rd : f24<br> - fs1 == 1 and fe1 == 0x7fb and fm1 == 0xbeb3709a573b7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x52162165ec222 and rm_val == 0  #nosat<br>                                             |[0x80000388]:fadd.d fs8, ft9, fs6, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd fs8, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>   |
|  24|[0x80002684]<br>0x00000005|- rs1 : f27<br> - rs2 : f7<br> - rd : f19<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x0410cbbfdec45 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                              |[0x800003a4]:fadd.d fs3, fs11, ft7, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fs3, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>  |
|  25|[0x80002694]<br>0x00000005|- rs1 : f5<br> - rs2 : f11<br> - rd : f16<br> - fs1 == 1 and fe1 == 0x7fb and fm1 == 0x49818dfc8788f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9a1cc86f24be5 and rm_val == 0  #nosat<br>                                              |[0x800003c0]:fadd.d fa6, ft5, fa1, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd fa6, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>   |
|  26|[0x800026a4]<br>0x00000005|- rs1 : f23<br> - rs2 : f26<br> - rd : f21<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0x9b3a56e2c058e and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x800003dc]:fadd.d fs5, fs7, fs10, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fs5, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>  |
|  27|[0x800026b4]<br>0x00000005|- rs1 : f15<br> - rs2 : f18<br> - rd : f23<br> - fs1 == 1 and fe1 == 0x7fc and fm1 == 0xe8af77cda8053 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 0  #nosat<br>                                             |[0x800003f8]:fadd.d fs7, fa5, fs2, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd fs7, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>   |
