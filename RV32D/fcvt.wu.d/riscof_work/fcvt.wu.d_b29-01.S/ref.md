
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000900')]      |
| SIG_REGION                | [('0x80002410', '0x800026a0', '164 words')]      |
| COV_LABELS                | fcvt.wu.d_b29      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.wu.d/riscof_work/fcvt.wu.d_b29-01.S/ref.S    |
| Total Number of coverpoints| 149     |
| Total Coverpoints Hit     | 123      |
| Total Signature Updates   | 115      |
| STAT1                     | 58      |
| STAT2                     | 0      |
| STAT3                     | 22     |
| STAT4                     | 57     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800006d4]:fcvt.wu.d t6, ft11, dyn
[0x800006d8]:csrrs a7, fflags, zero
[0x800006dc]:sw t6, 416(a5)
[0x800006e0]:sw a7, 420(a5)
[0x800006e4]:fld ft11, 472(a6)
[0x800006e8]:csrrwi zero, frm, 1

[0x800006ec]:fcvt.wu.d t6, ft11, dyn
[0x800006f0]:csrrs a7, fflags, zero
[0x800006f4]:sw t6, 432(a5)
[0x800006f8]:sw a7, 436(a5)
[0x800006fc]:fld ft11, 480(a6)
[0x80000700]:csrrwi zero, frm, 0

[0x80000704]:fcvt.wu.d t6, ft11, dyn
[0x80000708]:csrrs a7, fflags, zero
[0x8000070c]:sw t6, 448(a5)
[0x80000710]:sw a7, 452(a5)
[0x80000714]:fld ft11, 488(a6)
[0x80000718]:csrrwi zero, frm, 4

[0x8000071c]:fcvt.wu.d t6, ft11, dyn
[0x80000720]:csrrs a7, fflags, zero
[0x80000724]:sw t6, 464(a5)
[0x80000728]:sw a7, 468(a5)
[0x8000072c]:fld ft11, 496(a6)
[0x80000730]:csrrwi zero, frm, 3

[0x80000734]:fcvt.wu.d t6, ft11, dyn
[0x80000738]:csrrs a7, fflags, zero
[0x8000073c]:sw t6, 480(a5)
[0x80000740]:sw a7, 484(a5)
[0x80000744]:fld ft11, 504(a6)
[0x80000748]:csrrwi zero, frm, 2

[0x8000074c]:fcvt.wu.d t6, ft11, dyn
[0x80000750]:csrrs a7, fflags, zero
[0x80000754]:sw t6, 496(a5)
[0x80000758]:sw a7, 500(a5)
[0x8000075c]:fld ft11, 512(a6)
[0x80000760]:csrrwi zero, frm, 1

[0x80000764]:fcvt.wu.d t6, ft11, dyn
[0x80000768]:csrrs a7, fflags, zero
[0x8000076c]:sw t6, 512(a5)
[0x80000770]:sw a7, 516(a5)
[0x80000774]:fld ft11, 520(a6)
[0x80000778]:csrrwi zero, frm, 0

[0x8000077c]:fcvt.wu.d t6, ft11, dyn
[0x80000780]:csrrs a7, fflags, zero
[0x80000784]:sw t6, 528(a5)
[0x80000788]:sw a7, 532(a5)
[0x8000078c]:fld ft11, 528(a6)
[0x80000790]:csrrwi zero, frm, 4

[0x80000794]:fcvt.wu.d t6, ft11, dyn
[0x80000798]:csrrs a7, fflags, zero
[0x8000079c]:sw t6, 544(a5)
[0x800007a0]:sw a7, 548(a5)
[0x800007a4]:fld ft11, 536(a6)
[0x800007a8]:csrrwi zero, frm, 3

[0x800007ac]:fcvt.wu.d t6, ft11, dyn
[0x800007b0]:csrrs a7, fflags, zero
[0x800007b4]:sw t6, 560(a5)
[0x800007b8]:sw a7, 564(a5)
[0x800007bc]:fld ft11, 544(a6)
[0x800007c0]:csrrwi zero, frm, 2

[0x800007c4]:fcvt.wu.d t6, ft11, dyn
[0x800007c8]:csrrs a7, fflags, zero
[0x800007cc]:sw t6, 576(a5)
[0x800007d0]:sw a7, 580(a5)
[0x800007d4]:fld ft11, 552(a6)
[0x800007d8]:csrrwi zero, frm, 1

[0x800007dc]:fcvt.wu.d t6, ft11, dyn
[0x800007e0]:csrrs a7, fflags, zero
[0x800007e4]:sw t6, 592(a5)
[0x800007e8]:sw a7, 596(a5)
[0x800007ec]:fld ft11, 560(a6)
[0x800007f0]:csrrwi zero, frm, 0

[0x800007f4]:fcvt.wu.d t6, ft11, dyn
[0x800007f8]:csrrs a7, fflags, zero
[0x800007fc]:sw t6, 608(a5)
[0x80000800]:sw a7, 612(a5)
[0x80000804]:fld ft11, 568(a6)
[0x80000808]:csrrwi zero, frm, 4

[0x8000080c]:fcvt.wu.d t6, ft11, dyn
[0x80000810]:csrrs a7, fflags, zero
[0x80000814]:sw t6, 624(a5)
[0x80000818]:sw a7, 628(a5)
[0x8000081c]:fld ft11, 576(a6)
[0x80000820]:csrrwi zero, frm, 3

[0x80000824]:fcvt.wu.d t6, ft11, dyn
[0x80000828]:csrrs a7, fflags, zero
[0x8000082c]:sw t6, 640(a5)
[0x80000830]:sw a7, 644(a5)
[0x80000834]:fld ft11, 584(a6)
[0x80000838]:csrrwi zero, frm, 2

[0x8000083c]:fcvt.wu.d t6, ft11, dyn
[0x80000840]:csrrs a7, fflags, zero
[0x80000844]:sw t6, 656(a5)
[0x80000848]:sw a7, 660(a5)
[0x8000084c]:fld ft11, 592(a6)
[0x80000850]:csrrwi zero, frm, 1

[0x80000854]:fcvt.wu.d t6, ft11, dyn
[0x80000858]:csrrs a7, fflags, zero
[0x8000085c]:sw t6, 672(a5)
[0x80000860]:sw a7, 676(a5)
[0x80000864]:fld ft11, 600(a6)
[0x80000868]:csrrwi zero, frm, 0

[0x8000086c]:fcvt.wu.d t6, ft11, dyn
[0x80000870]:csrrs a7, fflags, zero
[0x80000874]:sw t6, 688(a5)
[0x80000878]:sw a7, 692(a5)
[0x8000087c]:fld ft11, 608(a6)
[0x80000880]:csrrwi zero, frm, 4

[0x80000884]:fcvt.wu.d t6, ft11, dyn
[0x80000888]:csrrs a7, fflags, zero
[0x8000088c]:sw t6, 704(a5)
[0x80000890]:sw a7, 708(a5)
[0x80000894]:fld ft11, 616(a6)
[0x80000898]:csrrwi zero, frm, 3

[0x8000089c]:fcvt.wu.d t6, ft11, dyn
[0x800008a0]:csrrs a7, fflags, zero
[0x800008a4]:sw t6, 720(a5)
[0x800008a8]:sw a7, 724(a5)
[0x800008ac]:fld ft11, 624(a6)
[0x800008b0]:csrrwi zero, frm, 2

[0x800008b4]:fcvt.wu.d t6, ft11, dyn
[0x800008b8]:csrrs a7, fflags, zero
[0x800008bc]:sw t6, 736(a5)
[0x800008c0]:sw a7, 740(a5)
[0x800008c4]:fld ft11, 632(a6)
[0x800008c8]:csrrwi zero, frm, 1

[0x800008cc]:fcvt.wu.d t6, ft11, dyn
[0x800008d0]:csrrs a7, fflags, zero
[0x800008d4]:sw t6, 752(a5)
[0x800008d8]:sw a7, 756(a5)
[0x800008dc]:fld ft11, 640(a6)
[0x800008e0]:csrrwi zero, frm, 3



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.d', 'rd : x30', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.wu.d t5, fs4, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw t5, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002414]:0x00000001




Last Coverpoint : ['rd : x17', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000140]:fcvt.wu.d a7, ft6, dyn
	-[0x80000144]:csrrs s5, fflags, zero
	-[0x80000148]:sw a7, 0(s3)
Current Store : [0x8000014c] : sw s5, 4(s3) -- Store: [0x8000241c]:0x00000001




Last Coverpoint : ['rd : x20', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.wu.d s4, fa1, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s4, 0(a5)
Current Store : [0x80000170] : sw a7, 4(a5) -- Store: [0x80002424]:0x00000001




Last Coverpoint : ['rd : x2', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.wu.d sp, fa5, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw sp, 16(a5)
Current Store : [0x80000188] : sw a7, 20(a5) -- Store: [0x80002434]:0x00000011




Last Coverpoint : ['rd : x26', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.wu.d s10, fs10, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw s10, 32(a5)
Current Store : [0x800001a0] : sw a7, 36(a5) -- Store: [0x80002444]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f25', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.wu.d a2, fs9, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw a2, 48(a5)
Current Store : [0x800001b8] : sw a7, 52(a5) -- Store: [0x80002454]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f14', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fcvt.wu.d t6, fa4, dyn
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw t6, 64(a5)
Current Store : [0x800001d0] : sw a7, 68(a5) -- Store: [0x80002464]:0x00000011




Last Coverpoint : ['rd : x6', 'rs1 : f23', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.wu.d t1, fs7, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw t1, 80(a5)
Current Store : [0x800001e8] : sw a7, 84(a5) -- Store: [0x80002474]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f18', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.wu.d s9, fs2, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw s9, 96(a5)
Current Store : [0x80000200] : sw a7, 100(a5) -- Store: [0x80002484]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.wu.d s2, fa0, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s2, 112(a5)
Current Store : [0x80000218] : sw a7, 116(a5) -- Store: [0x80002494]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.wu.d t2, fs3, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw t2, 128(a5)
Current Store : [0x80000230] : sw a7, 132(a5) -- Store: [0x800024a4]:0x00000011




Last Coverpoint : ['rd : x27', 'rs1 : f9', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.wu.d s11, fs1, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw s11, 144(a5)
Current Store : [0x80000248] : sw a7, 148(a5) -- Store: [0x800024b4]:0x00000011




Last Coverpoint : ['rd : x1', 'rs1 : f16', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.wu.d ra, fa6, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw ra, 160(a5)
Current Store : [0x80000260] : sw a7, 164(a5) -- Store: [0x800024c4]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.wu.d t3, fa7, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw t3, 176(a5)
Current Store : [0x80000278] : sw a7, 180(a5) -- Store: [0x800024d4]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.wu.d s3, ft0, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw s3, 192(a5)
Current Store : [0x80000290] : sw a7, 196(a5) -- Store: [0x800024e4]:0x00000011




Last Coverpoint : ['rd : x24', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.wu.d s8, fs8, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw s8, 208(a5)
Current Store : [0x800002a8] : sw a7, 212(a5) -- Store: [0x800024f4]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.wu.d t0, ft1, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw t0, 224(a5)
Current Store : [0x800002c0] : sw a7, 228(a5) -- Store: [0x80002504]:0x00000011




Last Coverpoint : ['rd : x3', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fcvt.wu.d gp, ft11, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw gp, 240(a5)
Current Store : [0x800002d8] : sw a7, 244(a5) -- Store: [0x80002514]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f13', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fcvt.wu.d s1, fa3, dyn
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw s1, 256(a5)
Current Store : [0x800002f0] : sw a7, 260(a5) -- Store: [0x80002524]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000308]:fcvt.wu.d a5, ft2, dyn
	-[0x8000030c]:csrrs s5, fflags, zero
	-[0x80000310]:sw a5, 0(s3)
Current Store : [0x80000314] : sw s5, 4(s3) -- Store: [0x800024ac]:0x00000011




Last Coverpoint : ['rd : x22', 'rs1 : f8', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.wu.d s6, fs0, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw s6, 0(a5)
Current Store : [0x80000338] : sw a7, 4(a5) -- Store: [0x800024b4]:0x00000011




Last Coverpoint : ['rd : x14', 'rs1 : f21', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.wu.d a4, fs5, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw a4, 16(a5)
Current Store : [0x80000350] : sw a7, 20(a5) -- Store: [0x800024c4]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f7', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.wu.d t4, ft7, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw t4, 32(a5)
Current Store : [0x80000368] : sw a7, 36(a5) -- Store: [0x800024d4]:0x00000011




Last Coverpoint : ['rd : x8', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.wu.d fp, fa2, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw fp, 48(a5)
Current Store : [0x80000380] : sw a7, 52(a5) -- Store: [0x800024e4]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f22', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.wu.d a3, fs6, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw a3, 64(a5)
Current Store : [0x80000398] : sw a7, 68(a5) -- Store: [0x800024f4]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.wu.d s5, ft8, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw s5, 80(a5)
Current Store : [0x800003b0] : sw a7, 84(a5) -- Store: [0x80002504]:0x00000011




Last Coverpoint : ['rd : x23', 'rs1 : f27', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.wu.d s7, fs11, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw s7, 96(a5)
Current Store : [0x800003c8] : sw a7, 100(a5) -- Store: [0x80002514]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.wu.d zero, ft9, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw zero, 112(a5)
Current Store : [0x800003e0] : sw a7, 116(a5) -- Store: [0x80002524]:0x00000011




Last Coverpoint : ['rd : x10', 'rs1 : f5', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.wu.d a0, ft5, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw a0, 128(a5)
Current Store : [0x800003f8] : sw a7, 132(a5) -- Store: [0x80002534]:0x00000011




Last Coverpoint : ['rd : x4', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000404]:fcvt.wu.d tp, ft4, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw tp, 144(a5)
Current Store : [0x80000410] : sw a7, 148(a5) -- Store: [0x80002544]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fcvt.wu.d a1, ft10, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw a1, 160(a5)
Current Store : [0x80000428] : sw a7, 164(a5) -- Store: [0x80002554]:0x00000011




Last Coverpoint : ['rd : x16', 'rs1 : f3', 'fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000440]:fcvt.wu.d a6, ft3, dyn
	-[0x80000444]:csrrs s5, fflags, zero
	-[0x80000448]:sw a6, 0(s3)
Current Store : [0x8000044c] : sw s5, 4(s3) -- Store: [0x8000250c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000464]:fcvt.wu.d t6, ft11, dyn
	-[0x80000468]:csrrs a7, fflags, zero
	-[0x8000046c]:sw t6, 0(a5)
Current Store : [0x80000470] : sw a7, 4(a5) -- Store: [0x80002514]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000480]:csrrs a7, fflags, zero
	-[0x80000484]:sw t6, 16(a5)
Current Store : [0x80000488] : sw a7, 20(a5) -- Store: [0x80002524]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000494]:fcvt.wu.d t6, ft11, dyn
	-[0x80000498]:csrrs a7, fflags, zero
	-[0x8000049c]:sw t6, 32(a5)
Current Store : [0x800004a0] : sw a7, 36(a5) -- Store: [0x80002534]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fcvt.wu.d t6, ft11, dyn
	-[0x800004b0]:csrrs a7, fflags, zero
	-[0x800004b4]:sw t6, 48(a5)
Current Store : [0x800004b8] : sw a7, 52(a5) -- Store: [0x80002544]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fcvt.wu.d t6, ft11, dyn
	-[0x800004c8]:csrrs a7, fflags, zero
	-[0x800004cc]:sw t6, 64(a5)
Current Store : [0x800004d0] : sw a7, 68(a5) -- Store: [0x80002554]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fcvt.wu.d t6, ft11, dyn
	-[0x800004e0]:csrrs a7, fflags, zero
	-[0x800004e4]:sw t6, 80(a5)
Current Store : [0x800004e8] : sw a7, 84(a5) -- Store: [0x80002564]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fcvt.wu.d t6, ft11, dyn
	-[0x800004f8]:csrrs a7, fflags, zero
	-[0x800004fc]:sw t6, 96(a5)
Current Store : [0x80000500] : sw a7, 100(a5) -- Store: [0x80002574]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000050c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000510]:csrrs a7, fflags, zero
	-[0x80000514]:sw t6, 112(a5)
Current Store : [0x80000518] : sw a7, 116(a5) -- Store: [0x80002584]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.wu.d t6, ft11, dyn
	-[0x80000528]:csrrs a7, fflags, zero
	-[0x8000052c]:sw t6, 128(a5)
Current Store : [0x80000530] : sw a7, 132(a5) -- Store: [0x80002594]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000540]:csrrs a7, fflags, zero
	-[0x80000544]:sw t6, 144(a5)
Current Store : [0x80000548] : sw a7, 148(a5) -- Store: [0x800025a4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000554]:fcvt.wu.d t6, ft11, dyn
	-[0x80000558]:csrrs a7, fflags, zero
	-[0x8000055c]:sw t6, 160(a5)
Current Store : [0x80000560] : sw a7, 164(a5) -- Store: [0x800025b4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000056c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000570]:csrrs a7, fflags, zero
	-[0x80000574]:sw t6, 176(a5)
Current Store : [0x80000578] : sw a7, 180(a5) -- Store: [0x800025c4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000584]:fcvt.wu.d t6, ft11, dyn
	-[0x80000588]:csrrs a7, fflags, zero
	-[0x8000058c]:sw t6, 192(a5)
Current Store : [0x80000590] : sw a7, 196(a5) -- Store: [0x800025d4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fcvt.wu.d t6, ft11, dyn
	-[0x800005a0]:csrrs a7, fflags, zero
	-[0x800005a4]:sw t6, 208(a5)
Current Store : [0x800005a8] : sw a7, 212(a5) -- Store: [0x800025e4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fcvt.wu.d t6, ft11, dyn
	-[0x800005b8]:csrrs a7, fflags, zero
	-[0x800005bc]:sw t6, 224(a5)
Current Store : [0x800005c0] : sw a7, 228(a5) -- Store: [0x800025f4]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fcvt.wu.d t6, ft11, dyn
	-[0x800005d0]:csrrs a7, fflags, zero
	-[0x800005d4]:sw t6, 240(a5)
Current Store : [0x800005d8] : sw a7, 244(a5) -- Store: [0x80002604]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fcvt.wu.d t6, ft11, dyn
	-[0x800005e8]:csrrs a7, fflags, zero
	-[0x800005ec]:sw t6, 256(a5)
Current Store : [0x800005f0] : sw a7, 260(a5) -- Store: [0x80002614]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fcvt.wu.d t6, ft11, dyn
	-[0x80000600]:csrrs a7, fflags, zero
	-[0x80000604]:sw t6, 272(a5)
Current Store : [0x80000608] : sw a7, 276(a5) -- Store: [0x80002624]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000614]:fcvt.wu.d t6, ft11, dyn
	-[0x80000618]:csrrs a7, fflags, zero
	-[0x8000061c]:sw t6, 288(a5)
Current Store : [0x80000620] : sw a7, 292(a5) -- Store: [0x80002634]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000062c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000630]:csrrs a7, fflags, zero
	-[0x80000634]:sw t6, 304(a5)
Current Store : [0x80000638] : sw a7, 308(a5) -- Store: [0x80002644]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000644]:fcvt.wu.d t6, ft11, dyn
	-[0x80000648]:csrrs a7, fflags, zero
	-[0x8000064c]:sw t6, 320(a5)
Current Store : [0x80000650] : sw a7, 324(a5) -- Store: [0x80002654]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000660]:csrrs a7, fflags, zero
	-[0x80000664]:sw t6, 336(a5)
Current Store : [0x80000668] : sw a7, 340(a5) -- Store: [0x80002664]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000674]:fcvt.wu.d t6, ft11, dyn
	-[0x80000678]:csrrs a7, fflags, zero
	-[0x8000067c]:sw t6, 352(a5)
Current Store : [0x80000680] : sw a7, 356(a5) -- Store: [0x80002674]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000068c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000690]:csrrs a7, fflags, zero
	-[0x80000694]:sw t6, 368(a5)
Current Store : [0x80000698] : sw a7, 372(a5) -- Store: [0x80002684]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fcvt.wu.d t6, ft11, dyn
	-[0x800006a8]:csrrs a7, fflags, zero
	-[0x800006ac]:sw t6, 384(a5)
Current Store : [0x800006b0] : sw a7, 388(a5) -- Store: [0x80002694]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                coverpoints                                                                |                                                        code                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002410]<br>0x00000000|- opcode : fcvt.wu.d<br> - rd : x30<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.wu.d t5, fs4, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw t5, 0(a5)<br>       |
|   2|[0x80002418]<br>0x00000000|- rd : x17<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 4  #nosat<br>                           |[0x80000140]:fcvt.wu.d a7, ft6, dyn<br> [0x80000144]:csrrs s5, fflags, zero<br> [0x80000148]:sw a7, 0(s3)<br>       |
|   3|[0x80002420]<br>0x00000000|- rd : x20<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 3  #nosat<br>                          |[0x80000164]:fcvt.wu.d s4, fa1, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s4, 0(a5)<br>       |
|   4|[0x80002430]<br>0x00000000|- rd : x2<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 2  #nosat<br>                           |[0x8000017c]:fcvt.wu.d sp, fa5, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw sp, 16(a5)<br>      |
|   5|[0x80002440]<br>0x00000000|- rd : x26<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 1  #nosat<br>                          |[0x80000194]:fcvt.wu.d s10, fs10, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw s10, 32(a5)<br>   |
|   6|[0x80002450]<br>0x00000000|- rd : x12<br> - rs1 : f25<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 0  #nosat<br>                          |[0x800001ac]:fcvt.wu.d a2, fs9, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw a2, 48(a5)<br>      |
|   7|[0x80002460]<br>0x00000000|- rd : x31<br> - rs1 : f14<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 4  #nosat<br>                          |[0x800001c4]:fcvt.wu.d t6, fa4, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw t6, 64(a5)<br>      |
|   8|[0x80002470]<br>0x00000000|- rd : x6<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 3  #nosat<br>                           |[0x800001dc]:fcvt.wu.d t1, fs7, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw t1, 80(a5)<br>      |
|   9|[0x80002480]<br>0x00000000|- rd : x25<br> - rs1 : f18<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 2  #nosat<br>                          |[0x800001f4]:fcvt.wu.d s9, fs2, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw s9, 96(a5)<br>      |
|  10|[0x80002490]<br>0x00000000|- rd : x18<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 1  #nosat<br>                          |[0x8000020c]:fcvt.wu.d s2, fa0, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s2, 112(a5)<br>     |
|  11|[0x800024a0]<br>0x00000000|- rd : x7<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 0  #nosat<br>                           |[0x80000224]:fcvt.wu.d t2, fs3, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw t2, 128(a5)<br>     |
|  12|[0x800024b0]<br>0x00000000|- rd : x27<br> - rs1 : f9<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 4  #nosat<br>                           |[0x8000023c]:fcvt.wu.d s11, fs1, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw s11, 144(a5)<br>   |
|  13|[0x800024c0]<br>0x00000000|- rd : x1<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 3  #nosat<br>                           |[0x80000254]:fcvt.wu.d ra, fa6, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw ra, 160(a5)<br>     |
|  14|[0x800024d0]<br>0x00000000|- rd : x28<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 2  #nosat<br>                          |[0x8000026c]:fcvt.wu.d t3, fa7, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw t3, 176(a5)<br>     |
|  15|[0x800024e0]<br>0x00000000|- rd : x19<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 1  #nosat<br>                           |[0x80000284]:fcvt.wu.d s3, ft0, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw s3, 192(a5)<br>     |
|  16|[0x800024f0]<br>0x00000000|- rd : x24<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 0  #nosat<br>                          |[0x8000029c]:fcvt.wu.d s8, fs8, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw s8, 208(a5)<br>     |
|  17|[0x80002500]<br>0x00000000|- rd : x5<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 4  #nosat<br>                            |[0x800002b4]:fcvt.wu.d t0, ft1, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw t0, 224(a5)<br>     |
|  18|[0x80002510]<br>0x00000000|- rd : x3<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 3  #nosat<br>                           |[0x800002cc]:fcvt.wu.d gp, ft11, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw gp, 240(a5)<br>    |
|  19|[0x80002520]<br>0x00000000|- rd : x9<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 2  #nosat<br>                           |[0x800002e4]:fcvt.wu.d s1, fa3, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw s1, 256(a5)<br>     |
|  20|[0x800024a8]<br>0x00000000|- rd : x15<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 1  #nosat<br>                           |[0x80000308]:fcvt.wu.d a5, ft2, dyn<br> [0x8000030c]:csrrs s5, fflags, zero<br> [0x80000310]:sw a5, 0(s3)<br>       |
|  21|[0x800024b0]<br>0x00000000|- rd : x22<br> - rs1 : f8<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 0  #nosat<br>                           |[0x8000032c]:fcvt.wu.d s6, fs0, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw s6, 0(a5)<br>       |
|  22|[0x800024c0]<br>0x00000000|- rd : x14<br> - rs1 : f21<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 4  #nosat<br>                          |[0x80000344]:fcvt.wu.d a4, fs5, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw a4, 16(a5)<br>      |
|  23|[0x800024d0]<br>0x00000000|- rd : x29<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 3  #nosat<br>                           |[0x8000035c]:fcvt.wu.d t4, ft7, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw t4, 32(a5)<br>      |
|  24|[0x800024e0]<br>0x00000000|- rd : x8<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 2  #nosat<br>                           |[0x80000374]:fcvt.wu.d fp, fa2, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw fp, 48(a5)<br>      |
|  25|[0x800024f0]<br>0x00000000|- rd : x13<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 1  #nosat<br>                          |[0x8000038c]:fcvt.wu.d a3, fs6, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw a3, 64(a5)<br>      |
|  26|[0x80002500]<br>0x00000000|- rd : x21<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869b and rm_val == 0  #nosat<br>                          |[0x800003a4]:fcvt.wu.d s5, ft8, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw s5, 80(a5)<br>      |
|  27|[0x80002510]<br>0x00000000|- rd : x23<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 4  #nosat<br>                          |[0x800003bc]:fcvt.wu.d s7, fs11, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw s7, 96(a5)<br>     |
|  28|[0x80002520]<br>0x00000000|- rd : x0<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 3  #nosat<br>                           |[0x800003d4]:fcvt.wu.d zero, ft9, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw zero, 112(a5)<br> |
|  29|[0x80002530]<br>0x00000000|- rd : x10<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 2  #nosat<br>                           |[0x800003ec]:fcvt.wu.d a0, ft5, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw a0, 128(a5)<br>     |
|  30|[0x80002540]<br>0x00000000|- rd : x4<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 1  #nosat<br>                            |[0x80000404]:fcvt.wu.d tp, ft4, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw tp, 144(a5)<br>     |
|  31|[0x80002550]<br>0x00000000|- rd : x11<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b869a and rm_val == 0  #nosat<br>                          |[0x8000041c]:fcvt.wu.d a1, ft10, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw a1, 160(a5)<br>    |
|  32|[0x80002508]<br>0x00000000|- rd : x16<br> - rs1 : f3<br> - fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 4  #nosat<br>                           |[0x80000440]:fcvt.wu.d a6, ft3, dyn<br> [0x80000444]:csrrs s5, fflags, zero<br> [0x80000448]:sw a6, 0(s3)<br>       |
|  33|[0x80002510]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 3  #nosat<br>                                                         |[0x80000464]:fcvt.wu.d t6, ft11, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 0(a5)<br>      |
|  34|[0x80002520]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 2  #nosat<br>                                                         |[0x8000047c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:sw t6, 16(a5)<br>     |
|  35|[0x80002530]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 1  #nosat<br>                                                         |[0x80000494]:fcvt.wu.d t6, ft11, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw t6, 32(a5)<br>     |
|  36|[0x80002540]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8699 and rm_val == 0  #nosat<br>                                                         |[0x800004ac]:fcvt.wu.d t6, ft11, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:sw t6, 48(a5)<br>     |
|  37|[0x80002550]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 4  #nosat<br>                                                         |[0x800004c4]:fcvt.wu.d t6, ft11, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:sw t6, 64(a5)<br>     |
|  38|[0x80002560]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 3  #nosat<br>                                                         |[0x800004dc]:fcvt.wu.d t6, ft11, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:sw t6, 80(a5)<br>     |
|  39|[0x80002570]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 2  #nosat<br>                                                         |[0x800004f4]:fcvt.wu.d t6, ft11, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:sw t6, 96(a5)<br>     |
|  40|[0x80002580]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 1  #nosat<br>                                                         |[0x8000050c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw t6, 112(a5)<br>    |
|  41|[0x80002590]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fc and fm1 == 0x08574923b8698 and rm_val == 0  #nosat<br>                                                         |[0x80000524]:fcvt.wu.d t6, ft11, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:sw t6, 128(a5)<br>    |
|  42|[0x800025a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 4  #nosat<br>                                                         |[0x8000053c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:sw t6, 144(a5)<br>    |
|  43|[0x800025b0]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 3  #nosat<br>                                                         |[0x80000554]:fcvt.wu.d t6, ft11, dyn<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:sw t6, 160(a5)<br>    |
|  44|[0x800025c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 2  #nosat<br>                                                         |[0x8000056c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:sw t6, 176(a5)<br>    |
|  45|[0x800025d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 1  #nosat<br>                                                         |[0x80000584]:fcvt.wu.d t6, ft11, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:sw t6, 192(a5)<br>    |
|  46|[0x800025e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869f and rm_val == 0  #nosat<br>                                                         |[0x8000059c]:fcvt.wu.d t6, ft11, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:sw t6, 208(a5)<br>    |
|  47|[0x800025f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 4  #nosat<br>                                                         |[0x800005b4]:fcvt.wu.d t6, ft11, dyn<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:sw t6, 224(a5)<br>    |
|  48|[0x80002600]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 3  #nosat<br>                                                         |[0x800005cc]:fcvt.wu.d t6, ft11, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:sw t6, 240(a5)<br>    |
|  49|[0x80002610]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 2  #nosat<br>                                                         |[0x800005e4]:fcvt.wu.d t6, ft11, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:sw t6, 256(a5)<br>    |
|  50|[0x80002620]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 1  #nosat<br>                                                         |[0x800005fc]:fcvt.wu.d t6, ft11, dyn<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:sw t6, 272(a5)<br>    |
|  51|[0x80002630]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869e and rm_val == 0  #nosat<br>                                                         |[0x80000614]:fcvt.wu.d t6, ft11, dyn<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:sw t6, 288(a5)<br>    |
|  52|[0x80002640]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 4  #nosat<br>                                                         |[0x8000062c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:sw t6, 304(a5)<br>    |
|  53|[0x80002650]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 3  #nosat<br>                                                         |[0x80000644]:fcvt.wu.d t6, ft11, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:sw t6, 320(a5)<br>    |
|  54|[0x80002660]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 2  #nosat<br>                                                         |[0x8000065c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:sw t6, 336(a5)<br>    |
|  55|[0x80002670]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 1  #nosat<br>                                                         |[0x80000674]:fcvt.wu.d t6, ft11, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:sw t6, 352(a5)<br>    |
|  56|[0x80002680]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869d and rm_val == 0  #nosat<br>                                                         |[0x8000068c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:sw t6, 368(a5)<br>    |
|  57|[0x80002690]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 4  #nosat<br>                                                         |[0x800006a4]:fcvt.wu.d t6, ft11, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:sw t6, 384(a5)<br>    |
|  58|[0x800026a0]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08574923b869c and rm_val == 3  #nosat<br>                                                         |[0x800006bc]:fcvt.wu.d t6, ft11, dyn<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:sw t6, 400(a5)<br>    |
