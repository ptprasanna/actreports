
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000480')]      |
| SIG_REGION                | [('0x80002310', '0x80002420', '68 words')]      |
| COV_LABELS                | fcvt.wu.d_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.wu.d/riscof_work/fcvt.wu.d_b1-01.S/ref.S    |
| Total Number of coverpoints| 93     |
| Total Coverpoints Hit     | 85      |
| Total Signature Updates   | 59      |
| STAT1                     | 30      |
| STAT2                     | 0      |
| STAT3                     | 2     |
| STAT4                     | 29     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003a4]:fcvt.wu.d a4, ft11, dyn
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:sw a4, 176(a5)
[0x800003b0]:sw a7, 180(a5)
[0x800003b4]:add s4, a6, zero
[0x800003b8]:auipc s3, 2
[0x800003bc]:addi s3, s3, 40
[0x800003c0]:fld fs10, 208(s4)
[0x800003c4]:csrrwi zero, frm, 0

[0x8000044c]:fcvt.wu.d fp, fs9, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:sw fp, 64(a5)
[0x80000458]:sw a7, 68(a5)
[0x8000045c]:fld ft11, 256(a6)
[0x80000460]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.d', 'rd : x10', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.wu.d a0, fs11, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw a0, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000000




Last Coverpoint : ['rd : x27', 'rs1 : f23', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.wu.d s11, fs7, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s11, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002324]:0x00000001




Last Coverpoint : ['rd : x24', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.wu.d s8, fs5, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw s8, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002334]:0x00000001




Last Coverpoint : ['rd : x25', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.wu.d s9, fa7, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s9, 48(a5)
Current Store : [0x80000170] : sw a7, 52(a5) -- Store: [0x80002344]:0x00000011




Last Coverpoint : ['rd : x6', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.wu.d t1, fs6, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw t1, 64(a5)
Current Store : [0x80000188] : sw a7, 68(a5) -- Store: [0x80002354]:0x00000011




Last Coverpoint : ['rd : x26', 'rs1 : f5', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.wu.d s10, ft5, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw s10, 80(a5)
Current Store : [0x800001a0] : sw a7, 84(a5) -- Store: [0x80002364]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.wu.d s1, fa3, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw s1, 96(a5)
Current Store : [0x800001b8] : sw a7, 100(a5) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fcvt.wu.d t5, fa2, dyn
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw t5, 112(a5)
Current Store : [0x800001d0] : sw a7, 116(a5) -- Store: [0x80002384]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.wu.d s5, ft4, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw s5, 128(a5)
Current Store : [0x800001e8] : sw a7, 132(a5) -- Store: [0x80002394]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.wu.d zero, ft9, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw zero, 144(a5)
Current Store : [0x80000200] : sw a7, 148(a5) -- Store: [0x800023a4]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.wu.d t2, ft2, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw t2, 160(a5)
Current Store : [0x80000218] : sw a7, 164(a5) -- Store: [0x800023b4]:0x00000011




Last Coverpoint : ['rd : x16', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000230]:fcvt.wu.d a6, ft0, dyn
	-[0x80000234]:csrrs s5, fflags, zero
	-[0x80000238]:sw a6, 0(s3)
Current Store : [0x8000023c] : sw s5, 4(s3) -- Store: [0x8000236c]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.wu.d t0, fs3, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw t0, 0(a5)
Current Store : [0x80000260] : sw a7, 4(a5) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f20', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000278]:fcvt.wu.d a5, fs4, dyn
	-[0x8000027c]:csrrs s5, fflags, zero
	-[0x80000280]:sw a5, 0(s3)
Current Store : [0x80000284] : sw s5, 4(s3) -- Store: [0x8000237c]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.wu.d t6, fs0, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw t6, 0(a5)
Current Store : [0x800002a8] : sw a7, 4(a5) -- Store: [0x80002384]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.wu.d a3, fa5, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw a3, 16(a5)
Current Store : [0x800002c0] : sw a7, 20(a5) -- Store: [0x80002394]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fcvt.wu.d t3, ft8, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw t3, 32(a5)
Current Store : [0x800002d8] : sw a7, 36(a5) -- Store: [0x800023a4]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fcvt.wu.d s2, fa0, dyn
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw s2, 48(a5)
Current Store : [0x800002f0] : sw a7, 52(a5) -- Store: [0x800023b4]:0x00000011




Last Coverpoint : ['rd : x2', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fcvt.wu.d sp, fa6, dyn
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw sp, 64(a5)
Current Store : [0x80000308] : sw a7, 68(a5) -- Store: [0x800023c4]:0x00000011




Last Coverpoint : ['rd : x1', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.wu.d ra, ft10, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw ra, 80(a5)
Current Store : [0x80000320] : sw a7, 84(a5) -- Store: [0x800023d4]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.wu.d s3, fs2, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw s3, 96(a5)
Current Store : [0x80000338] : sw a7, 100(a5) -- Store: [0x800023e4]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.wu.d a2, ft1, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw a2, 112(a5)
Current Store : [0x80000350] : sw a7, 116(a5) -- Store: [0x800023f4]:0x00000011




Last Coverpoint : ['rd : x20', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.wu.d s4, fa4, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw s4, 128(a5)
Current Store : [0x80000368] : sw a7, 132(a5) -- Store: [0x80002404]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f9', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.wu.d t4, fs1, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw t4, 144(a5)
Current Store : [0x80000380] : sw a7, 148(a5) -- Store: [0x80002414]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f26']
Last Code Sequence : 
	-[0x800003c8]:fcvt.wu.d a7, fs10, dyn
	-[0x800003cc]:csrrs s5, fflags, zero
	-[0x800003d0]:sw a7, 0(s3)
Current Store : [0x800003d4] : sw s5, 4(s3) -- Store: [0x800023e4]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f6']
Last Code Sequence : 
	-[0x800003ec]:fcvt.wu.d a1, ft6, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw a1, 0(a5)
Current Store : [0x800003f8] : sw a7, 4(a5) -- Store: [0x800023ec]:0x00000011




Last Coverpoint : ['rd : x3', 'rs1 : f7']
Last Code Sequence : 
	-[0x80000404]:fcvt.wu.d gp, ft7, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw gp, 16(a5)
Current Store : [0x80000410] : sw a7, 20(a5) -- Store: [0x800023fc]:0x00000011




Last Coverpoint : ['rd : x23', 'rs1 : f24']
Last Code Sequence : 
	-[0x8000041c]:fcvt.wu.d s7, fs8, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw s7, 32(a5)
Current Store : [0x80000428] : sw a7, 36(a5) -- Store: [0x8000240c]:0x00000011




Last Coverpoint : ['rd : x4', 'rs1 : f11']
Last Code Sequence : 
	-[0x80000434]:fcvt.wu.d tp, fa1, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw tp, 48(a5)
Current Store : [0x80000440] : sw a7, 52(a5) -- Store: [0x8000241c]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                coverpoints                                                                |                                                        code                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000000|- opcode : fcvt.wu.d<br> - rd : x10<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.wu.d a0, fs11, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw a0, 0(a5)<br>      |
|   2|[0x80002320]<br>0x00000000|- rd : x27<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x80000134]:fcvt.wu.d s11, fs7, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s11, 16(a5)<br>    |
|   3|[0x80002330]<br>0x00000001|- rd : x24<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x8000014c]:fcvt.wu.d s8, fs5, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw s8, 32(a5)<br>      |
|   4|[0x80002340]<br>0xFFFFFFFF|- rd : x25<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x80000164]:fcvt.wu.d s9, fa7, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s9, 48(a5)<br>      |
|   5|[0x80002350]<br>0xFFFFFFFF|- rd : x6<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                           |[0x8000017c]:fcvt.wu.d t1, fs6, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw t1, 64(a5)<br>      |
|   6|[0x80002360]<br>0xFFFFFFFF|- rd : x26<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                           |[0x80000194]:fcvt.wu.d s10, ft5, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw s10, 80(a5)<br>    |
|   7|[0x80002370]<br>0xFFFFFFFF|- rd : x9<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                           |[0x800001ac]:fcvt.wu.d s1, fa3, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw s1, 96(a5)<br>      |
|   8|[0x80002380]<br>0xFFFFFFFF|- rd : x30<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                          |[0x800001c4]:fcvt.wu.d t5, fa2, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw t5, 112(a5)<br>     |
|   9|[0x80002390]<br>0xFFFFFFFF|- rd : x21<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                           |[0x800001dc]:fcvt.wu.d s5, ft4, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw s5, 128(a5)<br>     |
|  10|[0x800023a0]<br>0x00000000|- rd : x0<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                           |[0x800001f4]:fcvt.wu.d zero, ft9, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw zero, 144(a5)<br> |
|  11|[0x800023b0]<br>0xFFFFFFFF|- rd : x7<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                            |[0x8000020c]:fcvt.wu.d t2, ft2, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw t2, 160(a5)<br>     |
|  12|[0x80002368]<br>0x00000000|- rd : x16<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                           |[0x80000230]:fcvt.wu.d a6, ft0, dyn<br> [0x80000234]:csrrs s5, fflags, zero<br> [0x80000238]:sw a6, 0(s3)<br>       |
|  13|[0x80002370]<br>0xFFFFFFFF|- rd : x5<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                           |[0x80000254]:fcvt.wu.d t0, fs3, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw t0, 0(a5)<br>       |
|  14|[0x80002378]<br>0x00000000|- rd : x15<br> - rs1 : f20<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                          |[0x80000278]:fcvt.wu.d a5, fs4, dyn<br> [0x8000027c]:csrrs s5, fflags, zero<br> [0x80000280]:sw a5, 0(s3)<br>       |
|  15|[0x80002380]<br>0x00000000|- rd : x31<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                           |[0x8000029c]:fcvt.wu.d t6, fs0, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw t6, 0(a5)<br>       |
|  16|[0x80002390]<br>0x00000000|- rd : x13<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800002b4]:fcvt.wu.d a3, fa5, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw a3, 16(a5)<br>      |
|  17|[0x800023a0]<br>0x00000000|- rd : x28<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800002cc]:fcvt.wu.d t3, ft8, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw t3, 32(a5)<br>      |
|  18|[0x800023b0]<br>0x00000000|- rd : x18<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                          |[0x800002e4]:fcvt.wu.d s2, fa0, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw s2, 48(a5)<br>      |
|  19|[0x800023c0]<br>0x00000000|- rd : x2<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                           |[0x800002fc]:fcvt.wu.d sp, fa6, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw sp, 64(a5)<br>      |
|  20|[0x800023d0]<br>0x00000000|- rd : x1<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                           |[0x80000314]:fcvt.wu.d ra, ft10, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw ra, 80(a5)<br>     |
|  21|[0x800023e0]<br>0x00000000|- rd : x19<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                          |[0x8000032c]:fcvt.wu.d s3, fs2, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw s3, 96(a5)<br>      |
|  22|[0x800023f0]<br>0x00000000|- rd : x12<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                           |[0x80000344]:fcvt.wu.d a2, ft1, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw a2, 112(a5)<br>     |
|  23|[0x80002400]<br>0x00000000|- rd : x20<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x8000035c]:fcvt.wu.d s4, fa4, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw s4, 128(a5)<br>     |
|  24|[0x80002410]<br>0x00000000|- rd : x29<br> - rs1 : f9<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                           |[0x80000374]:fcvt.wu.d t4, fs1, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw t4, 144(a5)<br>     |
|  25|[0x80002420]<br>0x00000000|- rd : x22<br> - rs1 : f3<br>                                                                                                              |[0x8000038c]:fcvt.wu.d s6, ft3, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw s6, 160(a5)<br>     |
|  26|[0x800023e0]<br>0x00000000|- rd : x17<br> - rs1 : f26<br>                                                                                                             |[0x800003c8]:fcvt.wu.d a7, fs10, dyn<br> [0x800003cc]:csrrs s5, fflags, zero<br> [0x800003d0]:sw a7, 0(s3)<br>      |
|  27|[0x800023e8]<br>0x00000000|- rd : x11<br> - rs1 : f6<br>                                                                                                              |[0x800003ec]:fcvt.wu.d a1, ft6, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw a1, 0(a5)<br>       |
|  28|[0x800023f8]<br>0x00000000|- rd : x3<br> - rs1 : f7<br>                                                                                                               |[0x80000404]:fcvt.wu.d gp, ft7, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw gp, 16(a5)<br>      |
|  29|[0x80002408]<br>0x00000000|- rd : x23<br> - rs1 : f24<br>                                                                                                             |[0x8000041c]:fcvt.wu.d s7, fs8, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw s7, 32(a5)<br>      |
|  30|[0x80002418]<br>0x00000000|- rd : x4<br> - rs1 : f11<br>                                                                                                              |[0x80000434]:fcvt.wu.d tp, fa1, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw tp, 48(a5)<br>      |
