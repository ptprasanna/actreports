
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000b40')]      |
| SIG_REGION                | [('0x80002510', '0x80002860', '212 words')]      |
| COV_LABELS                | fcvt.wu.d_b24      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.wu.d/riscof_work/fcvt.wu.d_b24-01.S/ref.S    |
| Total Number of coverpoints| 174     |
| Total Coverpoints Hit     | 128      |
| Total Signature Updates   | 126      |
| STAT1                     | 63      |
| STAT2                     | 0      |
| STAT3                     | 42     |
| STAT4                     | 63     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000734]:fcvt.wu.d t6, ft11, dyn
[0x80000738]:csrrs a7, fflags, zero
[0x8000073c]:sw t6, 704(a5)
[0x80000740]:sw a7, 708(a5)
[0x80000744]:fld ft11, 512(a6)
[0x80000748]:csrrwi zero, frm, 0

[0x8000074c]:fcvt.wu.d t6, ft11, dyn
[0x80000750]:csrrs a7, fflags, zero
[0x80000754]:sw t6, 720(a5)
[0x80000758]:sw a7, 724(a5)
[0x8000075c]:fld ft11, 520(a6)
[0x80000760]:csrrwi zero, frm, 4

[0x80000764]:fcvt.wu.d t6, ft11, dyn
[0x80000768]:csrrs a7, fflags, zero
[0x8000076c]:sw t6, 736(a5)
[0x80000770]:sw a7, 740(a5)
[0x80000774]:fld ft11, 528(a6)
[0x80000778]:csrrwi zero, frm, 3

[0x8000077c]:fcvt.wu.d t6, ft11, dyn
[0x80000780]:csrrs a7, fflags, zero
[0x80000784]:sw t6, 752(a5)
[0x80000788]:sw a7, 756(a5)
[0x8000078c]:fld ft11, 536(a6)
[0x80000790]:csrrwi zero, frm, 2

[0x80000794]:fcvt.wu.d t6, ft11, dyn
[0x80000798]:csrrs a7, fflags, zero
[0x8000079c]:sw t6, 768(a5)
[0x800007a0]:sw a7, 772(a5)
[0x800007a4]:fld ft11, 544(a6)
[0x800007a8]:csrrwi zero, frm, 1

[0x800007ac]:fcvt.wu.d t6, ft11, dyn
[0x800007b0]:csrrs a7, fflags, zero
[0x800007b4]:sw t6, 784(a5)
[0x800007b8]:sw a7, 788(a5)
[0x800007bc]:fld ft11, 552(a6)
[0x800007c0]:csrrwi zero, frm, 0

[0x800007c4]:fcvt.wu.d t6, ft11, dyn
[0x800007c8]:csrrs a7, fflags, zero
[0x800007cc]:sw t6, 800(a5)
[0x800007d0]:sw a7, 804(a5)
[0x800007d4]:fld ft11, 560(a6)
[0x800007d8]:csrrwi zero, frm, 4

[0x800007dc]:fcvt.wu.d t6, ft11, dyn
[0x800007e0]:csrrs a7, fflags, zero
[0x800007e4]:sw t6, 816(a5)
[0x800007e8]:sw a7, 820(a5)
[0x800007ec]:fld ft11, 568(a6)
[0x800007f0]:csrrwi zero, frm, 3

[0x800007f4]:fcvt.wu.d t6, ft11, dyn
[0x800007f8]:csrrs a7, fflags, zero
[0x800007fc]:sw t6, 832(a5)
[0x80000800]:sw a7, 836(a5)
[0x80000804]:fld ft11, 576(a6)
[0x80000808]:csrrwi zero, frm, 2

[0x8000080c]:fcvt.wu.d t6, ft11, dyn
[0x80000810]:csrrs a7, fflags, zero
[0x80000814]:sw t6, 848(a5)
[0x80000818]:sw a7, 852(a5)
[0x8000081c]:fld ft11, 584(a6)
[0x80000820]:csrrwi zero, frm, 1

[0x80000824]:fcvt.wu.d t6, ft11, dyn
[0x80000828]:csrrs a7, fflags, zero
[0x8000082c]:sw t6, 864(a5)
[0x80000830]:sw a7, 868(a5)
[0x80000834]:fld ft11, 592(a6)
[0x80000838]:csrrwi zero, frm, 0

[0x8000083c]:fcvt.wu.d t6, ft11, dyn
[0x80000840]:csrrs a7, fflags, zero
[0x80000844]:sw t6, 880(a5)
[0x80000848]:sw a7, 884(a5)
[0x8000084c]:fld ft11, 600(a6)
[0x80000850]:csrrwi zero, frm, 4

[0x80000854]:fcvt.wu.d t6, ft11, dyn
[0x80000858]:csrrs a7, fflags, zero
[0x8000085c]:sw t6, 896(a5)
[0x80000860]:sw a7, 900(a5)
[0x80000864]:fld ft11, 608(a6)
[0x80000868]:csrrwi zero, frm, 3

[0x8000086c]:fcvt.wu.d t6, ft11, dyn
[0x80000870]:csrrs a7, fflags, zero
[0x80000874]:sw t6, 912(a5)
[0x80000878]:sw a7, 916(a5)
[0x8000087c]:fld ft11, 616(a6)
[0x80000880]:csrrwi zero, frm, 2

[0x80000884]:fcvt.wu.d t6, ft11, dyn
[0x80000888]:csrrs a7, fflags, zero
[0x8000088c]:sw t6, 928(a5)
[0x80000890]:sw a7, 932(a5)
[0x80000894]:fld ft11, 624(a6)
[0x80000898]:csrrwi zero, frm, 1

[0x8000089c]:fcvt.wu.d t6, ft11, dyn
[0x800008a0]:csrrs a7, fflags, zero
[0x800008a4]:sw t6, 944(a5)
[0x800008a8]:sw a7, 948(a5)
[0x800008ac]:fld ft11, 632(a6)
[0x800008b0]:csrrwi zero, frm, 0

[0x800008b4]:fcvt.wu.d t6, ft11, dyn
[0x800008b8]:csrrs a7, fflags, zero
[0x800008bc]:sw t6, 960(a5)
[0x800008c0]:sw a7, 964(a5)
[0x800008c4]:fld ft11, 640(a6)
[0x800008c8]:csrrwi zero, frm, 4

[0x800008cc]:fcvt.wu.d t6, ft11, dyn
[0x800008d0]:csrrs a7, fflags, zero
[0x800008d4]:sw t6, 976(a5)
[0x800008d8]:sw a7, 980(a5)
[0x800008dc]:fld ft11, 648(a6)
[0x800008e0]:csrrwi zero, frm, 3

[0x800008e4]:fcvt.wu.d t6, ft11, dyn
[0x800008e8]:csrrs a7, fflags, zero
[0x800008ec]:sw t6, 992(a5)
[0x800008f0]:sw a7, 996(a5)
[0x800008f4]:fld ft11, 656(a6)
[0x800008f8]:csrrwi zero, frm, 2

[0x800008fc]:fcvt.wu.d t6, ft11, dyn
[0x80000900]:csrrs a7, fflags, zero
[0x80000904]:sw t6, 1008(a5)
[0x80000908]:sw a7, 1012(a5)
[0x8000090c]:fld ft11, 664(a6)
[0x80000910]:csrrwi zero, frm, 1

[0x80000914]:fcvt.wu.d t6, ft11, dyn
[0x80000918]:csrrs a7, fflags, zero
[0x8000091c]:sw t6, 1024(a5)
[0x80000920]:sw a7, 1028(a5)
[0x80000924]:fld ft11, 672(a6)
[0x80000928]:csrrwi zero, frm, 0

[0x8000092c]:fcvt.wu.d t6, ft11, dyn
[0x80000930]:csrrs a7, fflags, zero
[0x80000934]:sw t6, 1040(a5)
[0x80000938]:sw a7, 1044(a5)
[0x8000093c]:fld ft11, 680(a6)
[0x80000940]:csrrwi zero, frm, 4

[0x80000944]:fcvt.wu.d t6, ft11, dyn
[0x80000948]:csrrs a7, fflags, zero
[0x8000094c]:sw t6, 1056(a5)
[0x80000950]:sw a7, 1060(a5)
[0x80000954]:fld ft11, 688(a6)
[0x80000958]:csrrwi zero, frm, 3

[0x8000095c]:fcvt.wu.d t6, ft11, dyn
[0x80000960]:csrrs a7, fflags, zero
[0x80000964]:sw t6, 1072(a5)
[0x80000968]:sw a7, 1076(a5)
[0x8000096c]:fld ft11, 696(a6)
[0x80000970]:csrrwi zero, frm, 2

[0x80000974]:fcvt.wu.d t6, ft11, dyn
[0x80000978]:csrrs a7, fflags, zero
[0x8000097c]:sw t6, 1088(a5)
[0x80000980]:sw a7, 1092(a5)
[0x80000984]:fld ft11, 704(a6)
[0x80000988]:csrrwi zero, frm, 1

[0x8000098c]:fcvt.wu.d t6, ft11, dyn
[0x80000990]:csrrs a7, fflags, zero
[0x80000994]:sw t6, 1104(a5)
[0x80000998]:sw a7, 1108(a5)
[0x8000099c]:fld ft11, 712(a6)
[0x800009a0]:csrrwi zero, frm, 0

[0x800009a4]:fcvt.wu.d t6, ft11, dyn
[0x800009a8]:csrrs a7, fflags, zero
[0x800009ac]:sw t6, 1120(a5)
[0x800009b0]:sw a7, 1124(a5)
[0x800009b4]:fld ft11, 720(a6)
[0x800009b8]:csrrwi zero, frm, 4

[0x800009bc]:fcvt.wu.d t6, ft11, dyn
[0x800009c0]:csrrs a7, fflags, zero
[0x800009c4]:sw t6, 1136(a5)
[0x800009c8]:sw a7, 1140(a5)
[0x800009cc]:fld ft11, 728(a6)
[0x800009d0]:csrrwi zero, frm, 3

[0x800009d4]:fcvt.wu.d t6, ft11, dyn
[0x800009d8]:csrrs a7, fflags, zero
[0x800009dc]:sw t6, 1152(a5)
[0x800009e0]:sw a7, 1156(a5)
[0x800009e4]:fld ft11, 736(a6)
[0x800009e8]:csrrwi zero, frm, 2

[0x800009ec]:fcvt.wu.d t6, ft11, dyn
[0x800009f0]:csrrs a7, fflags, zero
[0x800009f4]:sw t6, 1168(a5)
[0x800009f8]:sw a7, 1172(a5)
[0x800009fc]:fld ft11, 744(a6)
[0x80000a00]:csrrwi zero, frm, 1

[0x80000a04]:fcvt.wu.d t6, ft11, dyn
[0x80000a08]:csrrs a7, fflags, zero
[0x80000a0c]:sw t6, 1184(a5)
[0x80000a10]:sw a7, 1188(a5)
[0x80000a14]:fld ft11, 752(a6)
[0x80000a18]:csrrwi zero, frm, 0

[0x80000a1c]:fcvt.wu.d t6, ft11, dyn
[0x80000a20]:csrrs a7, fflags, zero
[0x80000a24]:sw t6, 1200(a5)
[0x80000a28]:sw a7, 1204(a5)
[0x80000a2c]:fld ft11, 760(a6)
[0x80000a30]:csrrwi zero, frm, 4

[0x80000a34]:fcvt.wu.d t6, ft11, dyn
[0x80000a38]:csrrs a7, fflags, zero
[0x80000a3c]:sw t6, 1216(a5)
[0x80000a40]:sw a7, 1220(a5)
[0x80000a44]:fld ft11, 768(a6)
[0x80000a48]:csrrwi zero, frm, 3

[0x80000a4c]:fcvt.wu.d t6, ft11, dyn
[0x80000a50]:csrrs a7, fflags, zero
[0x80000a54]:sw t6, 1232(a5)
[0x80000a58]:sw a7, 1236(a5)
[0x80000a5c]:fld ft11, 776(a6)
[0x80000a60]:csrrwi zero, frm, 2

[0x80000a64]:fcvt.wu.d t6, ft11, dyn
[0x80000a68]:csrrs a7, fflags, zero
[0x80000a6c]:sw t6, 1248(a5)
[0x80000a70]:sw a7, 1252(a5)
[0x80000a74]:fld ft11, 784(a6)
[0x80000a78]:csrrwi zero, frm, 1

[0x80000a7c]:fcvt.wu.d t6, ft11, dyn
[0x80000a80]:csrrs a7, fflags, zero
[0x80000a84]:sw t6, 1264(a5)
[0x80000a88]:sw a7, 1268(a5)
[0x80000a8c]:fld ft11, 792(a6)
[0x80000a90]:csrrwi zero, frm, 0

[0x80000a94]:fcvt.wu.d t6, ft11, dyn
[0x80000a98]:csrrs a7, fflags, zero
[0x80000a9c]:sw t6, 1280(a5)
[0x80000aa0]:sw a7, 1284(a5)
[0x80000aa4]:fld ft11, 800(a6)
[0x80000aa8]:csrrwi zero, frm, 4

[0x80000aac]:fcvt.wu.d t6, ft11, dyn
[0x80000ab0]:csrrs a7, fflags, zero
[0x80000ab4]:sw t6, 1296(a5)
[0x80000ab8]:sw a7, 1300(a5)
[0x80000abc]:fld ft11, 808(a6)
[0x80000ac0]:csrrwi zero, frm, 3

[0x80000ac4]:fcvt.wu.d t6, ft11, dyn
[0x80000ac8]:csrrs a7, fflags, zero
[0x80000acc]:sw t6, 1312(a5)
[0x80000ad0]:sw a7, 1316(a5)
[0x80000ad4]:fld ft11, 816(a6)
[0x80000ad8]:csrrwi zero, frm, 2

[0x80000adc]:fcvt.wu.d t6, ft11, dyn
[0x80000ae0]:csrrs a7, fflags, zero
[0x80000ae4]:sw t6, 1328(a5)
[0x80000ae8]:sw a7, 1332(a5)
[0x80000aec]:fld ft11, 824(a6)
[0x80000af0]:csrrwi zero, frm, 1

[0x80000af4]:fcvt.wu.d t6, ft11, dyn
[0x80000af8]:csrrs a7, fflags, zero
[0x80000afc]:sw t6, 1344(a5)
[0x80000b00]:sw a7, 1348(a5)
[0x80000b04]:fld ft11, 832(a6)
[0x80000b08]:csrrwi zero, frm, 0

[0x80000b0c]:fcvt.wu.d t6, ft11, dyn
[0x80000b10]:csrrs a7, fflags, zero
[0x80000b14]:sw t6, 1360(a5)
[0x80000b18]:sw a7, 1364(a5)
[0x80000b1c]:fld ft11, 840(a6)
[0x80000b20]:csrrwi zero, frm, 1



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.d', 'rd : x12', 'rs1 : f23', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.wu.d a2, fs7, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw a2, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002514]:0x00000001




Last Coverpoint : ['rd : x2', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.wu.d sp, fs0, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw sp, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002524]:0x00000001




Last Coverpoint : ['rd : x22', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.wu.d s6, fa7, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw s6, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002534]:0x00000001




Last Coverpoint : ['rd : x27', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.wu.d s11, fs3, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s11, 48(a5)
Current Store : [0x80000170] : sw a7, 52(a5) -- Store: [0x80002544]:0x00000001




Last Coverpoint : ['rd : x8', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.wu.d fp, fs11, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw fp, 64(a5)
Current Store : [0x80000188] : sw a7, 68(a5) -- Store: [0x80002554]:0x00000001




Last Coverpoint : ['rd : x3', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.wu.d gp, ft3, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw gp, 80(a5)
Current Store : [0x800001a0] : sw a7, 84(a5) -- Store: [0x80002564]:0x00000001




Last Coverpoint : ['rd : x6', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.wu.d t1, fs6, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw t1, 96(a5)
Current Store : [0x800001b8] : sw a7, 100(a5) -- Store: [0x80002574]:0x00000001




Last Coverpoint : ['rd : x16', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800001d0]:fcvt.wu.d a6, fs4, dyn
	-[0x800001d4]:csrrs s5, fflags, zero
	-[0x800001d8]:sw a6, 0(s3)
Current Store : [0x800001dc] : sw s5, 4(s3) -- Store: [0x8000254c]:0x00000001




Last Coverpoint : ['rd : x13', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.wu.d a3, fa3, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw a3, 0(a5)
Current Store : [0x80000200] : sw a7, 4(a5) -- Store: [0x80002554]:0x00000001




Last Coverpoint : ['rd : x26', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.wu.d s10, fa6, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s10, 16(a5)
Current Store : [0x80000218] : sw a7, 20(a5) -- Store: [0x80002564]:0x00000001




Last Coverpoint : ['rd : x10', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.wu.d a0, fs2, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw a0, 32(a5)
Current Store : [0x80000230] : sw a7, 36(a5) -- Store: [0x80002574]:0x00000001




Last Coverpoint : ['rd : x20', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.wu.d s4, ft8, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw s4, 48(a5)
Current Store : [0x80000248] : sw a7, 52(a5) -- Store: [0x80002584]:0x00000011




Last Coverpoint : ['rd : x4', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.wu.d tp, ft0, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw tp, 64(a5)
Current Store : [0x80000260] : sw a7, 68(a5) -- Store: [0x80002594]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.wu.d t2, fs8, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw t2, 80(a5)
Current Store : [0x80000278] : sw a7, 84(a5) -- Store: [0x800025a4]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.wu.d t6, ft6, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw t6, 96(a5)
Current Store : [0x80000290] : sw a7, 100(a5) -- Store: [0x800025b4]:0x00000011




Last Coverpoint : ['rd : x14', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.wu.d a4, fa1, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw a4, 112(a5)
Current Store : [0x800002a8] : sw a7, 116(a5) -- Store: [0x800025c4]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f5', 'fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.wu.d a1, ft5, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw a1, 128(a5)
Current Store : [0x800002c0] : sw a7, 132(a5) -- Store: [0x800025d4]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fcvt.wu.d a7, ft2, dyn
	-[0x800002dc]:csrrs s5, fflags, zero
	-[0x800002e0]:sw a7, 0(s3)
Current Store : [0x800002e4] : sw s5, 4(s3) -- Store: [0x8000259c]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002f0]:fcvt.wu.d a5, ft1, dyn
	-[0x800002f4]:csrrs s5, fflags, zero
	-[0x800002f8]:sw a5, 16(s3)
Current Store : [0x800002fc] : sw s5, 20(s3) -- Store: [0x800025ac]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.wu.d t0, fa0, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw t0, 0(a5)
Current Store : [0x80000320] : sw a7, 4(a5) -- Store: [0x800025ac]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.wu.d s5, fs10, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw s5, 16(a5)
Current Store : [0x80000338] : sw a7, 20(a5) -- Store: [0x800025bc]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.wu.d s3, ft4, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw s3, 32(a5)
Current Store : [0x80000350] : sw a7, 36(a5) -- Store: [0x800025cc]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.wu.d s2, fa5, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw s2, 48(a5)
Current Store : [0x80000368] : sw a7, 52(a5) -- Store: [0x800025dc]:0x00000011




Last Coverpoint : ['rd : x24', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.wu.d s8, fa2, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw s8, 64(a5)
Current Store : [0x80000380] : sw a7, 68(a5) -- Store: [0x800025ec]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f14', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.wu.d t5, fa4, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw t5, 80(a5)
Current Store : [0x80000398] : sw a7, 84(a5) -- Store: [0x800025fc]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.wu.d t3, ft10, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw t3, 96(a5)
Current Store : [0x800003b0] : sw a7, 100(a5) -- Store: [0x8000260c]:0x00000011




Last Coverpoint : ['rd : x1', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.wu.d ra, fs9, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw ra, 112(a5)
Current Store : [0x800003c8] : sw a7, 116(a5) -- Store: [0x8000261c]:0x00000011




Last Coverpoint : ['rd : x23', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.wu.d s7, ft9, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s7, 128(a5)
Current Store : [0x800003e0] : sw a7, 132(a5) -- Store: [0x8000262c]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.wu.d t4, ft7, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw t4, 144(a5)
Current Store : [0x800003f8] : sw a7, 148(a5) -- Store: [0x8000263c]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000404]:fcvt.wu.d zero, fs1, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw zero, 160(a5)
Current Store : [0x80000410] : sw a7, 164(a5) -- Store: [0x8000264c]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fcvt.wu.d s1, fs5, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw s1, 176(a5)
Current Store : [0x80000428] : sw a7, 180(a5) -- Store: [0x8000265c]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000434]:fcvt.wu.d s9, ft11, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw s9, 192(a5)
Current Store : [0x80000440] : sw a7, 196(a5) -- Store: [0x8000266c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000044c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw t6, 208(a5)
Current Store : [0x80000458] : sw a7, 212(a5) -- Store: [0x8000267c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000464]:fcvt.wu.d t6, ft11, dyn
	-[0x80000468]:csrrs a7, fflags, zero
	-[0x8000046c]:sw t6, 224(a5)
Current Store : [0x80000470] : sw a7, 228(a5) -- Store: [0x8000268c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000480]:csrrs a7, fflags, zero
	-[0x80000484]:sw t6, 240(a5)
Current Store : [0x80000488] : sw a7, 244(a5) -- Store: [0x8000269c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000494]:fcvt.wu.d t6, ft11, dyn
	-[0x80000498]:csrrs a7, fflags, zero
	-[0x8000049c]:sw t6, 256(a5)
Current Store : [0x800004a0] : sw a7, 260(a5) -- Store: [0x800026ac]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004ac]:fcvt.wu.d t6, ft11, dyn
	-[0x800004b0]:csrrs a7, fflags, zero
	-[0x800004b4]:sw t6, 272(a5)
Current Store : [0x800004b8] : sw a7, 276(a5) -- Store: [0x800026bc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004c4]:fcvt.wu.d t6, ft11, dyn
	-[0x800004c8]:csrrs a7, fflags, zero
	-[0x800004cc]:sw t6, 288(a5)
Current Store : [0x800004d0] : sw a7, 292(a5) -- Store: [0x800026cc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fcvt.wu.d t6, ft11, dyn
	-[0x800004e0]:csrrs a7, fflags, zero
	-[0x800004e4]:sw t6, 304(a5)
Current Store : [0x800004e8] : sw a7, 308(a5) -- Store: [0x800026dc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fcvt.wu.d t6, ft11, dyn
	-[0x800004f8]:csrrs a7, fflags, zero
	-[0x800004fc]:sw t6, 320(a5)
Current Store : [0x80000500] : sw a7, 324(a5) -- Store: [0x800026ec]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000050c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000510]:csrrs a7, fflags, zero
	-[0x80000514]:sw t6, 336(a5)
Current Store : [0x80000518] : sw a7, 340(a5) -- Store: [0x800026fc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.wu.d t6, ft11, dyn
	-[0x80000528]:csrrs a7, fflags, zero
	-[0x8000052c]:sw t6, 352(a5)
Current Store : [0x80000530] : sw a7, 356(a5) -- Store: [0x8000270c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000540]:csrrs a7, fflags, zero
	-[0x80000544]:sw t6, 368(a5)
Current Store : [0x80000548] : sw a7, 372(a5) -- Store: [0x8000271c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000554]:fcvt.wu.d t6, ft11, dyn
	-[0x80000558]:csrrs a7, fflags, zero
	-[0x8000055c]:sw t6, 384(a5)
Current Store : [0x80000560] : sw a7, 388(a5) -- Store: [0x8000272c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000056c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000570]:csrrs a7, fflags, zero
	-[0x80000574]:sw t6, 400(a5)
Current Store : [0x80000578] : sw a7, 404(a5) -- Store: [0x8000273c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000584]:fcvt.wu.d t6, ft11, dyn
	-[0x80000588]:csrrs a7, fflags, zero
	-[0x8000058c]:sw t6, 416(a5)
Current Store : [0x80000590] : sw a7, 420(a5) -- Store: [0x8000274c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fcvt.wu.d t6, ft11, dyn
	-[0x800005a0]:csrrs a7, fflags, zero
	-[0x800005a4]:sw t6, 432(a5)
Current Store : [0x800005a8] : sw a7, 436(a5) -- Store: [0x8000275c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fcvt.wu.d t6, ft11, dyn
	-[0x800005b8]:csrrs a7, fflags, zero
	-[0x800005bc]:sw t6, 448(a5)
Current Store : [0x800005c0] : sw a7, 452(a5) -- Store: [0x8000276c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fcvt.wu.d t6, ft11, dyn
	-[0x800005d0]:csrrs a7, fflags, zero
	-[0x800005d4]:sw t6, 464(a5)
Current Store : [0x800005d8] : sw a7, 468(a5) -- Store: [0x8000277c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005e4]:fcvt.wu.d t6, ft11, dyn
	-[0x800005e8]:csrrs a7, fflags, zero
	-[0x800005ec]:sw t6, 480(a5)
Current Store : [0x800005f0] : sw a7, 484(a5) -- Store: [0x8000278c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fcvt.wu.d t6, ft11, dyn
	-[0x80000600]:csrrs a7, fflags, zero
	-[0x80000604]:sw t6, 496(a5)
Current Store : [0x80000608] : sw a7, 500(a5) -- Store: [0x8000279c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000614]:fcvt.wu.d t6, ft11, dyn
	-[0x80000618]:csrrs a7, fflags, zero
	-[0x8000061c]:sw t6, 512(a5)
Current Store : [0x80000620] : sw a7, 516(a5) -- Store: [0x800027ac]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000062c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000630]:csrrs a7, fflags, zero
	-[0x80000634]:sw t6, 528(a5)
Current Store : [0x80000638] : sw a7, 532(a5) -- Store: [0x800027bc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000644]:fcvt.wu.d t6, ft11, dyn
	-[0x80000648]:csrrs a7, fflags, zero
	-[0x8000064c]:sw t6, 544(a5)
Current Store : [0x80000650] : sw a7, 548(a5) -- Store: [0x800027cc]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000660]:csrrs a7, fflags, zero
	-[0x80000664]:sw t6, 560(a5)
Current Store : [0x80000668] : sw a7, 564(a5) -- Store: [0x800027dc]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000674]:fcvt.wu.d t6, ft11, dyn
	-[0x80000678]:csrrs a7, fflags, zero
	-[0x8000067c]:sw t6, 576(a5)
Current Store : [0x80000680] : sw a7, 580(a5) -- Store: [0x800027ec]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x8000068c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000690]:csrrs a7, fflags, zero
	-[0x80000694]:sw t6, 592(a5)
Current Store : [0x80000698] : sw a7, 596(a5) -- Store: [0x800027fc]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fcvt.wu.d t6, ft11, dyn
	-[0x800006a8]:csrrs a7, fflags, zero
	-[0x800006ac]:sw t6, 608(a5)
Current Store : [0x800006b0] : sw a7, 612(a5) -- Store: [0x8000280c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fcvt.wu.d t6, ft11, dyn
	-[0x800006c0]:csrrs a7, fflags, zero
	-[0x800006c4]:sw t6, 624(a5)
Current Store : [0x800006c8] : sw a7, 628(a5) -- Store: [0x8000281c]:0x00000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fcvt.wu.d t6, ft11, dyn
	-[0x800006d8]:csrrs a7, fflags, zero
	-[0x800006dc]:sw t6, 640(a5)
Current Store : [0x800006e0] : sw a7, 644(a5) -- Store: [0x8000282c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fcvt.wu.d t6, ft11, dyn
	-[0x800006f0]:csrrs a7, fflags, zero
	-[0x800006f4]:sw t6, 656(a5)
Current Store : [0x800006f8] : sw a7, 660(a5) -- Store: [0x8000283c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000704]:fcvt.wu.d t6, ft11, dyn
	-[0x80000708]:csrrs a7, fflags, zero
	-[0x8000070c]:sw t6, 672(a5)
Current Store : [0x80000710] : sw a7, 676(a5) -- Store: [0x8000284c]:0x00000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000071c]:fcvt.wu.d t6, ft11, dyn
	-[0x80000720]:csrrs a7, fflags, zero
	-[0x80000724]:sw t6, 688(a5)
Current Store : [0x80000728] : sw a7, 692(a5) -- Store: [0x8000285c]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                coverpoints                                                                |                                                        code                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002510]<br>0x00000000|- opcode : fcvt.wu.d<br> - rd : x12<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.wu.d a2, fs7, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw a2, 0(a5)<br>       |
|   2|[0x80002520]<br>0x00000000|- rd : x2<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                            |[0x80000134]:fcvt.wu.d sp, fs0, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw sp, 16(a5)<br>      |
|   3|[0x80002530]<br>0x00000000|- rd : x22<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                          |[0x8000014c]:fcvt.wu.d s6, fa7, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw s6, 32(a5)<br>      |
|   4|[0x80002540]<br>0x00000000|- rd : x27<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 2  #nosat<br>                          |[0x80000164]:fcvt.wu.d s11, fs3, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s11, 48(a5)<br>    |
|   5|[0x80002550]<br>0x00000000|- rd : x8<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                           |[0x8000017c]:fcvt.wu.d fp, fs11, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw fp, 64(a5)<br>     |
|   6|[0x80002560]<br>0x00000000|- rd : x3<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                            |[0x80000194]:fcvt.wu.d gp, ft3, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw gp, 80(a5)<br>      |
|   7|[0x80002570]<br>0x00000000|- rd : x6<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 4  #nosat<br>                           |[0x800001ac]:fcvt.wu.d t1, fs6, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw t1, 96(a5)<br>      |
|   8|[0x80002548]<br>0x00000001|- rd : x16<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 3  #nosat<br>                          |[0x800001d0]:fcvt.wu.d a6, fs4, dyn<br> [0x800001d4]:csrrs s5, fflags, zero<br> [0x800001d8]:sw a6, 0(s3)<br>       |
|   9|[0x80002550]<br>0x00000000|- rd : x13<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 2  #nosat<br>                          |[0x800001f4]:fcvt.wu.d a3, fa3, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw a3, 0(a5)<br>       |
|  10|[0x80002560]<br>0x00000000|- rd : x26<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 1  #nosat<br>                          |[0x8000020c]:fcvt.wu.d s10, fa6, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s10, 16(a5)<br>    |
|  11|[0x80002570]<br>0x00000000|- rd : x10<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x3fb and fm1 == 0xc28f5c28f5c29 and rm_val == 0  #nosat<br>                          |[0x80000224]:fcvt.wu.d a0, fs2, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw a0, 32(a5)<br>      |
|  12|[0x80002580]<br>0x00000000|- rd : x20<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 4  #nosat<br>                          |[0x8000023c]:fcvt.wu.d s4, ft8, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw s4, 48(a5)<br>      |
|  13|[0x80002590]<br>0x00000000|- rd : x4<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 3  #nosat<br>                            |[0x80000254]:fcvt.wu.d tp, ft0, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw tp, 64(a5)<br>      |
|  14|[0x800025a0]<br>0x00000000|- rd : x7<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 2  #nosat<br>                           |[0x8000026c]:fcvt.wu.d t2, fs8, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw t2, 80(a5)<br>      |
|  15|[0x800025b0]<br>0x00000000|- rd : x31<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 1  #nosat<br>                           |[0x80000284]:fcvt.wu.d t6, ft6, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw t6, 96(a5)<br>      |
|  16|[0x800025c0]<br>0x00000000|- rd : x14<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x1c28f5c28f5c3 and rm_val == 0  #nosat<br>                          |[0x8000029c]:fcvt.wu.d a4, fa1, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw a4, 112(a5)<br>     |
|  17|[0x800025d0]<br>0x00000000|- rd : x11<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 4  #nosat<br>                           |[0x800002b4]:fcvt.wu.d a1, ft5, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw a1, 128(a5)<br>     |
|  18|[0x80002598]<br>0x00000000|- rd : x17<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 3  #nosat<br>                           |[0x800002d8]:fcvt.wu.d a7, ft2, dyn<br> [0x800002dc]:csrrs s5, fflags, zero<br> [0x800002e0]:sw a7, 0(s3)<br>       |
|  19|[0x800025a8]<br>0x00000000|- rd : x15<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 2  #nosat<br>                           |[0x800002f0]:fcvt.wu.d a5, ft1, dyn<br> [0x800002f4]:csrrs s5, fflags, zero<br> [0x800002f8]:sw a5, 16(s3)<br>      |
|  20|[0x800025a8]<br>0x00000000|- rd : x5<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 1  #nosat<br>                           |[0x80000314]:fcvt.wu.d t0, fa0, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw t0, 0(a5)<br>       |
|  21|[0x800025b8]<br>0x00000000|- rd : x21<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x3fb and fm1 == 0x999999999999a and rm_val == 0  #nosat<br>                          |[0x8000032c]:fcvt.wu.d s5, fs10, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw s5, 16(a5)<br>     |
|  22|[0x800025c8]<br>0x00000000|- rd : x19<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 4  #nosat<br>                           |[0x80000344]:fcvt.wu.d s3, ft4, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw s3, 32(a5)<br>      |
|  23|[0x800025d8]<br>0x00000000|- rd : x18<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 3  #nosat<br>                          |[0x8000035c]:fcvt.wu.d s2, fa5, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw s2, 48(a5)<br>      |
|  24|[0x800025e8]<br>0x00000000|- rd : x24<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 2  #nosat<br>                          |[0x80000374]:fcvt.wu.d s8, fa2, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw s8, 64(a5)<br>      |
|  25|[0x800025f8]<br>0x00000000|- rd : x30<br> - rs1 : f14<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 1  #nosat<br>                          |[0x8000038c]:fcvt.wu.d t5, fa4, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw t5, 80(a5)<br>      |
|  26|[0x80002608]<br>0x00000000|- rd : x28<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 0  #nosat<br>                          |[0x800003a4]:fcvt.wu.d t3, ft10, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw t3, 96(a5)<br>     |
|  27|[0x80002618]<br>0x00000001|- rd : x1<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 4  #nosat<br>                           |[0x800003bc]:fcvt.wu.d ra, fs9, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw ra, 112(a5)<br>     |
|  28|[0x80002628]<br>0x00000002|- rd : x23<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 3  #nosat<br>                          |[0x800003d4]:fcvt.wu.d s7, ft9, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s7, 128(a5)<br>     |
|  29|[0x80002638]<br>0x00000001|- rd : x29<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 2  #nosat<br>                           |[0x800003ec]:fcvt.wu.d t4, ft7, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw t4, 144(a5)<br>     |
|  30|[0x80002648]<br>0x00000000|- rd : x0<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 1  #nosat<br>                            |[0x80000404]:fcvt.wu.d zero, fs1, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw zero, 160(a5)<br> |
|  31|[0x80002658]<br>0x00000001|- rd : x9<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x199999999999a and rm_val == 0  #nosat<br>                           |[0x8000041c]:fcvt.wu.d s1, fs5, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw s1, 176(a5)<br>     |
|  32|[0x80002668]<br>0x00000000|- rd : x25<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 4  #nosat<br>                          |[0x80000434]:fcvt.wu.d s9, ft11, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw s9, 192(a5)<br>    |
|  33|[0x80002678]<br>0x00000001|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 3  #nosat<br>                                                         |[0x8000044c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw t6, 208(a5)<br>    |
|  34|[0x80002688]<br>0x00000000|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 2  #nosat<br>                                                         |[0x80000464]:fcvt.wu.d t6, ft11, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 224(a5)<br>    |
|  35|[0x80002698]<br>0x00000000|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 1  #nosat<br>                                                         |[0x8000047c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:sw t6, 240(a5)<br>    |
|  36|[0x800026a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 0  #nosat<br>                                                         |[0x80000494]:fcvt.wu.d t6, ft11, dyn<br> [0x80000498]:csrrs a7, fflags, zero<br> [0x8000049c]:sw t6, 256(a5)<br>    |
|  37|[0x800026b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                                                         |[0x800004ac]:fcvt.wu.d t6, ft11, dyn<br> [0x800004b0]:csrrs a7, fflags, zero<br> [0x800004b4]:sw t6, 272(a5)<br>    |
|  38|[0x800026c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                                                         |[0x800004c4]:fcvt.wu.d t6, ft11, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:sw t6, 288(a5)<br>    |
|  39|[0x800026d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                         |[0x800004dc]:fcvt.wu.d t6, ft11, dyn<br> [0x800004e0]:csrrs a7, fflags, zero<br> [0x800004e4]:sw t6, 304(a5)<br>    |
|  40|[0x800026e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                                                         |[0x800004f4]:fcvt.wu.d t6, ft11, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:sw t6, 320(a5)<br>    |
|  41|[0x800026f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                                         |[0x8000050c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000510]:csrrs a7, fflags, zero<br> [0x80000514]:sw t6, 336(a5)<br>    |
|  42|[0x80002708]<br>0x00000000|- fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 4  #nosat<br>                                                         |[0x80000524]:fcvt.wu.d t6, ft11, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:sw t6, 352(a5)<br>    |
|  43|[0x80002718]<br>0x00000000|- fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 3  #nosat<br>                                                         |[0x8000053c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:sw t6, 368(a5)<br>    |
|  44|[0x80002728]<br>0x00000000|- fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 2  #nosat<br>                                                         |[0x80000554]:fcvt.wu.d t6, ft11, dyn<br> [0x80000558]:csrrs a7, fflags, zero<br> [0x8000055c]:sw t6, 384(a5)<br>    |
|  45|[0x80002738]<br>0x00000000|- fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x47ae147ae147b and rm_val == 1  #nosat<br>                                                         |[0x8000056c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000570]:csrrs a7, fflags, zero<br> [0x80000574]:sw t6, 400(a5)<br>    |
|  46|[0x80002748]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 4  #nosat<br>                                                         |[0x80000584]:fcvt.wu.d t6, ft11, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:sw t6, 416(a5)<br>    |
|  47|[0x80002758]<br>0x00000002|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 3  #nosat<br>                                                         |[0x8000059c]:fcvt.wu.d t6, ft11, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:sw t6, 432(a5)<br>    |
|  48|[0x80002768]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 2  #nosat<br>                                                         |[0x800005b4]:fcvt.wu.d t6, ft11, dyn<br> [0x800005b8]:csrrs a7, fflags, zero<br> [0x800005bc]:sw t6, 448(a5)<br>    |
|  49|[0x80002778]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 1  #nosat<br>                                                         |[0x800005cc]:fcvt.wu.d t6, ft11, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:sw t6, 464(a5)<br>    |
|  50|[0x80002788]<br>0x00000001|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 0  #nosat<br>                                                         |[0x800005e4]:fcvt.wu.d t6, ft11, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:sw t6, 480(a5)<br>    |
|  51|[0x80002798]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 4  #nosat<br>                                                         |[0x800005fc]:fcvt.wu.d t6, ft11, dyn<br> [0x80000600]:csrrs a7, fflags, zero<br> [0x80000604]:sw t6, 496(a5)<br>    |
|  52|[0x800027a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 3  #nosat<br>                                                         |[0x80000614]:fcvt.wu.d t6, ft11, dyn<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:sw t6, 512(a5)<br>    |
|  53|[0x800027b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 2  #nosat<br>                                                         |[0x8000062c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000630]:csrrs a7, fflags, zero<br> [0x80000634]:sw t6, 528(a5)<br>    |
|  54|[0x800027c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 1  #nosat<br>                                                         |[0x80000644]:fcvt.wu.d t6, ft11, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:sw t6, 544(a5)<br>    |
|  55|[0x800027d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0xc7ae147ae147b and rm_val == 0  #nosat<br>                                                         |[0x8000065c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000660]:csrrs a7, fflags, zero<br> [0x80000664]:sw t6, 560(a5)<br>    |
|  56|[0x800027e8]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 4  #nosat<br>                                                         |[0x80000674]:fcvt.wu.d t6, ft11, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:sw t6, 576(a5)<br>    |
|  57|[0x800027f8]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 3  #nosat<br>                                                         |[0x8000068c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:sw t6, 592(a5)<br>    |
|  58|[0x80002808]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 2  #nosat<br>                                                         |[0x800006a4]:fcvt.wu.d t6, ft11, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:sw t6, 608(a5)<br>    |
|  59|[0x80002818]<br>0x00000000|- fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 1  #nosat<br>                                                         |[0x800006bc]:fcvt.wu.d t6, ft11, dyn<br> [0x800006c0]:csrrs a7, fflags, zero<br> [0x800006c4]:sw t6, 624(a5)<br>    |
|  60|[0x80002828]<br>0x00000001|- fs1 == 0 and fe1 == 0x3fe and fm1 == 0xccccccccccccd and rm_val == 0  #nosat<br>                                                         |[0x800006d4]:fcvt.wu.d t6, ft11, dyn<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:sw t6, 640(a5)<br>    |
|  61|[0x80002838]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 4  #nosat<br>                                                         |[0x800006ec]:fcvt.wu.d t6, ft11, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:sw t6, 656(a5)<br>    |
|  62|[0x80002848]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 3  #nosat<br>                                                         |[0x80000704]:fcvt.wu.d t6, ft11, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:sw t6, 672(a5)<br>    |
|  63|[0x80002858]<br>0x00000000|- fs1 == 1 and fe1 == 0x3ff and fm1 == 0x028f5c28f5c29 and rm_val == 2  #nosat<br>                                                         |[0x8000071c]:fcvt.wu.d t6, ft11, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:sw t6, 688(a5)<br>    |
