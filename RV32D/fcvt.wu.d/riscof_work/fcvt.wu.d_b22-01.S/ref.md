
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000540')]      |
| SIG_REGION                | [('0x80002310', '0x80002460', '84 words')]      |
| COV_LABELS                | fcvt.wu.d_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.wu.d/riscof_work/fcvt.wu.d_b22-01.S/ref.S    |
| Total Number of coverpoints| 109     |
| Total Coverpoints Hit     | 98      |
| Total Signature Updates   | 65      |
| STAT1                     | 33      |
| STAT2                     | 0      |
| STAT3                     | 7     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x8000047c]:fcvt.wu.d t6, ft11, dyn
[0x80000480]:csrrs a7, fflags, zero
[0x80000484]:sw t6, 176(a5)
[0x80000488]:sw a7, 180(a5)
[0x8000048c]:fld ft11, 272(a6)
[0x80000490]:csrrwi zero, frm, 0

[0x80000494]:fcvt.wu.d t6, ft11, dyn
[0x80000498]:csrrs a7, fflags, zero
[0x8000049c]:sw t6, 192(a5)
[0x800004a0]:sw a7, 196(a5)
[0x800004a4]:fld ft11, 280(a6)
[0x800004a8]:csrrwi zero, frm, 0

[0x800004ac]:fcvt.wu.d t6, ft11, dyn
[0x800004b0]:csrrs a7, fflags, zero
[0x800004b4]:sw t6, 208(a5)
[0x800004b8]:sw a7, 212(a5)
[0x800004bc]:fld ft11, 288(a6)
[0x800004c0]:csrrwi zero, frm, 0

[0x800004c4]:fcvt.wu.d t6, ft11, dyn
[0x800004c8]:csrrs a7, fflags, zero
[0x800004cc]:sw t6, 224(a5)
[0x800004d0]:sw a7, 228(a5)
[0x800004d4]:fld ft11, 296(a6)
[0x800004d8]:csrrwi zero, frm, 0

[0x800004dc]:fcvt.wu.d t6, ft11, dyn
[0x800004e0]:csrrs a7, fflags, zero
[0x800004e4]:sw t6, 240(a5)
[0x800004e8]:sw a7, 244(a5)
[0x800004ec]:fld ft11, 304(a6)
[0x800004f0]:csrrwi zero, frm, 0

[0x800004f4]:fcvt.wu.d t6, ft11, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:sw t6, 256(a5)
[0x80000500]:sw a7, 260(a5)
[0x80000504]:fld ft11, 312(a6)
[0x80000508]:csrrwi zero, frm, 0

[0x8000050c]:fcvt.wu.d t6, ft11, dyn
[0x80000510]:csrrs a7, fflags, zero
[0x80000514]:sw t6, 272(a5)
[0x80000518]:sw a7, 276(a5)
[0x8000051c]:fld ft11, 320(a6)
[0x80000520]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.d', 'rd : x14', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.wu.d a4, fs11, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw a4, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000001




Last Coverpoint : ['rd : x23', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x5ca and fm1 == 0xf871c6ee84270 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.wu.d s7, fs7, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s7, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002324]:0x00000011




Last Coverpoint : ['rd : x8', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x3ca and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.wu.d fp, fs2, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw fp, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002334]:0x00000011




Last Coverpoint : ['rd : x24', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.wu.d s8, ft10, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s8, 48(a5)
Current Store : [0x80000170] : sw a7, 52(a5) -- Store: [0x80002344]:0x00000011




Last Coverpoint : ['rd : x16', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000188]:fcvt.wu.d a6, fa3, dyn
	-[0x8000018c]:csrrs s5, fflags, zero
	-[0x80000190]:sw a6, 0(s3)
Current Store : [0x80000194] : sw s5, 4(s3) -- Store: [0x80002334]:0x00000011




Last Coverpoint : ['rd : x4', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.wu.d tp, fs3, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw tp, 0(a5)
Current Store : [0x800001b8] : sw a7, 4(a5) -- Store: [0x8000233c]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fcvt.wu.d t3, fa0, dyn
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw t3, 16(a5)
Current Store : [0x800001d0] : sw a7, 20(a5) -- Store: [0x8000234c]:0x00000011




Last Coverpoint : ['rd : x6', 'rs1 : f20', 'fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.wu.d t1, fs4, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw t1, 32(a5)
Current Store : [0x800001e8] : sw a7, 36(a5) -- Store: [0x8000235c]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.wu.d t6, fs9, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw t6, 48(a5)
Current Store : [0x80000200] : sw a7, 52(a5) -- Store: [0x8000236c]:0x00000011




Last Coverpoint : ['rd : x2', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.wu.d sp, fa7, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw sp, 64(a5)
Current Store : [0x80000218] : sw a7, 68(a5) -- Store: [0x8000237c]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f5', 'fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.wu.d s5, ft5, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw s5, 80(a5)
Current Store : [0x80000230] : sw a7, 84(a5) -- Store: [0x8000238c]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.wu.d s2, ft0, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw s2, 96(a5)
Current Store : [0x80000248] : sw a7, 100(a5) -- Store: [0x8000239c]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000260]:fcvt.wu.d a7, ft8, dyn
	-[0x80000264]:csrrs s5, fflags, zero
	-[0x80000268]:sw a7, 0(s3)
Current Store : [0x8000026c] : sw s5, 4(s3) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x1', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.wu.d ra, ft11, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw ra, 0(a5)
Current Store : [0x80000290] : sw a7, 4(a5) -- Store: [0x8000237c]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.wu.d a1, fs1, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw a1, 16(a5)
Current Store : [0x800002a8] : sw a7, 20(a5) -- Store: [0x8000238c]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.wu.d s1, fa6, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw s1, 32(a5)
Current Store : [0x800002c0] : sw a7, 36(a5) -- Store: [0x8000239c]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fcvt.wu.d s3, fs6, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw s3, 48(a5)
Current Store : [0x800002d8] : sw a7, 52(a5) -- Store: [0x800023ac]:0x00000011




Last Coverpoint : ['rd : x10', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fcvt.wu.d a0, fs5, dyn
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw a0, 64(a5)
Current Store : [0x800002f0] : sw a7, 68(a5) -- Store: [0x800023bc]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fcvt.wu.d t4, fa1, dyn
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw t4, 80(a5)
Current Store : [0x80000308] : sw a7, 84(a5) -- Store: [0x800023cc]:0x00000011




Last Coverpoint : ['rd : x22', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.wu.d s6, ft1, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw s6, 96(a5)
Current Store : [0x80000320] : sw a7, 100(a5) -- Store: [0x800023dc]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.wu.d t0, ft6, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw t0, 112(a5)
Current Store : [0x80000338] : sw a7, 116(a5) -- Store: [0x800023ec]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000350]:fcvt.wu.d a5, fs8, dyn
	-[0x80000354]:csrrs s5, fflags, zero
	-[0x80000358]:sw a5, 0(s3)
Current Store : [0x8000035c] : sw s5, 4(s3) -- Store: [0x800023bc]:0x00000011




Last Coverpoint : ['rd : x3', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.wu.d gp, ft7, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw gp, 0(a5)
Current Store : [0x80000380] : sw a7, 4(a5) -- Store: [0x800023c4]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.wu.d t5, fa2, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw t5, 16(a5)
Current Store : [0x80000398] : sw a7, 20(a5) -- Store: [0x800023d4]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f14', 'fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.wu.d zero, fa4, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw zero, 32(a5)
Current Store : [0x800003b0] : sw a7, 36(a5) -- Store: [0x800023e4]:0x00000011




Last Coverpoint : ['rd : x20', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.wu.d s4, fa5, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw s4, 48(a5)
Current Store : [0x800003c8] : sw a7, 52(a5) -- Store: [0x800023f4]:0x00000011




Last Coverpoint : ['rd : x27', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.wu.d s11, fs0, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s11, 64(a5)
Current Store : [0x800003e0] : sw a7, 68(a5) -- Store: [0x80002404]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.wu.d a3, ft3, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw a3, 80(a5)
Current Store : [0x800003f8] : sw a7, 84(a5) -- Store: [0x80002414]:0x00000011




Last Coverpoint : ['rd : x26', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x408 and fm1 == 0x43277acca7f0d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000404]:fcvt.wu.d s10, fs10, dyn
	-[0x80000408]:csrrs a7, fflags, zero
	-[0x8000040c]:sw s10, 96(a5)
Current Store : [0x80000410] : sw a7, 100(a5) -- Store: [0x80002424]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x407 and fm1 == 0x489b36bd7f503 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fcvt.wu.d t2, ft2, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw t2, 112(a5)
Current Store : [0x80000428] : sw a7, 116(a5) -- Store: [0x80002434]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x406 and fm1 == 0x5ae6a9a6ab329 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000434]:fcvt.wu.d a2, ft4, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw a2, 128(a5)
Current Store : [0x80000440] : sw a7, 132(a5) -- Store: [0x80002444]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x405 and fm1 == 0xdc3386b9f15c4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000044c]:fcvt.wu.d s9, ft9, dyn
	-[0x80000450]:csrrs a7, fflags, zero
	-[0x80000454]:sw s9, 144(a5)
Current Store : [0x80000458] : sw a7, 148(a5) -- Store: [0x80002454]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                coverpoints                                                                |                                                       code                                                        |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000000|- opcode : fcvt.wu.d<br> - rd : x14<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.wu.d a4, fs11, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw a4, 0(a5)<br>     |
|   2|[0x80002320]<br>0xFFFFFFFF|- rd : x23<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x5ca and fm1 == 0xf871c6ee84270 and rm_val == 0  #nosat<br>                          |[0x80000134]:fcvt.wu.d s7, fs7, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s7, 16(a5)<br>     |
|   3|[0x80002330]<br>0x00000000|- rd : x8<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x3ca and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat<br>                           |[0x8000014c]:fcvt.wu.d fp, fs2, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw fp, 32(a5)<br>     |
|   4|[0x80002340]<br>0x00000000|- rd : x24<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat<br>                          |[0x80000164]:fcvt.wu.d s8, ft10, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s8, 48(a5)<br>    |
|   5|[0x80002330]<br>0xFFFFFFFF|- rd : x16<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat<br>                          |[0x80000188]:fcvt.wu.d a6, fa3, dyn<br> [0x8000018c]:csrrs s5, fflags, zero<br> [0x80000190]:sw a6, 0(s3)<br>      |
|   6|[0x80002338]<br>0x00000000|- rd : x4<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat<br>                           |[0x800001ac]:fcvt.wu.d tp, fs3, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw tp, 0(a5)<br>      |
|   7|[0x80002348]<br>0x00000000|- rd : x28<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat<br>                          |[0x800001c4]:fcvt.wu.d t3, fa0, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw t3, 16(a5)<br>     |
|   8|[0x80002358]<br>0x00000000|- rd : x6<br> - rs1 : f20<br> - fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat<br>                           |[0x800001dc]:fcvt.wu.d t1, fs4, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw t1, 32(a5)<br>     |
|   9|[0x80002368]<br>0x229723B6|- rd : x31<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat<br>                          |[0x800001f4]:fcvt.wu.d t6, fs9, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw t6, 48(a5)<br>     |
|  10|[0x80002378]<br>0x00000000|- rd : x2<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat<br>                           |[0x8000020c]:fcvt.wu.d sp, fa7, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw sp, 64(a5)<br>     |
|  11|[0x80002388]<br>0x00000000|- rd : x21<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat<br>                           |[0x80000224]:fcvt.wu.d s5, ft5, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw s5, 80(a5)<br>     |
|  12|[0x80002398]<br>0x05FC8582|- rd : x18<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat<br>                           |[0x8000023c]:fcvt.wu.d s2, ft0, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw s2, 96(a5)<br>     |
|  13|[0x80002370]<br>0x027A0C2D|- rd : x17<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat<br>                          |[0x80000260]:fcvt.wu.d a7, ft8, dyn<br> [0x80000264]:csrrs s5, fflags, zero<br> [0x80000268]:sw a7, 0(s3)<br>      |
|  14|[0x80002378]<br>0x00000000|- rd : x1<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat<br>                           |[0x80000284]:fcvt.wu.d ra, ft11, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw ra, 0(a5)<br>     |
|  15|[0x80002388]<br>0x00C03ED7|- rd : x11<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat<br>                           |[0x8000029c]:fcvt.wu.d a1, fs1, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw a1, 16(a5)<br>     |
|  16|[0x80002398]<br>0x00656937|- rd : x9<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat<br>                           |[0x800002b4]:fcvt.wu.d s1, fa6, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw s1, 32(a5)<br>     |
|  17|[0x800023a8]<br>0x002F0A07|- rd : x19<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat<br>                          |[0x800002cc]:fcvt.wu.d s3, fs6, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw s3, 48(a5)<br>     |
|  18|[0x800023b8]<br>0x0018C8A2|- rd : x10<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat<br>                          |[0x800002e4]:fcvt.wu.d a0, fs5, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw a0, 64(a5)<br>     |
|  19|[0x800023c8]<br>0x0009EBE5|- rd : x29<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat<br>                          |[0x800002fc]:fcvt.wu.d t4, fa1, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw t4, 80(a5)<br>     |
|  20|[0x800023d8]<br>0x00000000|- rd : x22<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat<br>                           |[0x80000314]:fcvt.wu.d s6, ft1, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw s6, 96(a5)<br>     |
|  21|[0x800023e8]<br>0x0003D1B6|- rd : x5<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat<br>                            |[0x8000032c]:fcvt.wu.d t0, ft6, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw t0, 112(a5)<br>    |
|  22|[0x800023b8]<br>0x0001224C|- rd : x15<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat<br>                          |[0x80000350]:fcvt.wu.d a5, fs8, dyn<br> [0x80000354]:csrrs s5, fflags, zero<br> [0x80000358]:sw a5, 0(s3)<br>      |
|  23|[0x800023c0]<br>0x0000CA9E|- rd : x3<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat<br>                            |[0x80000374]:fcvt.wu.d gp, ft7, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw gp, 0(a5)<br>      |
|  24|[0x800023d0]<br>0x00006741|- rd : x30<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat<br>                          |[0x8000038c]:fcvt.wu.d t5, fa2, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw t5, 16(a5)<br>     |
|  25|[0x800023e0]<br>0x00000000|- rd : x0<br> - rs1 : f14<br> - fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat<br>                           |[0x800003a4]:fcvt.wu.d zero, fa4, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw zero, 32(a5)<br> |
|  26|[0x800023f0]<br>0x00000000|- rd : x20<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat<br>                          |[0x800003bc]:fcvt.wu.d s4, fa5, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw s4, 48(a5)<br>     |
|  27|[0x80002400]<br>0x00000AE7|- rd : x27<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat<br>                           |[0x800003d4]:fcvt.wu.d s11, fs0, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s11, 64(a5)<br>   |
|  28|[0x80002410]<br>0x000006BE|- rd : x13<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat<br>                           |[0x800003ec]:fcvt.wu.d a3, ft3, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw a3, 80(a5)<br>     |
|  29|[0x80002420]<br>0x00000286|- rd : x26<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x408 and fm1 == 0x43277acca7f0d and rm_val == 0  #nosat<br>                          |[0x80000404]:fcvt.wu.d s10, fs10, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:sw s10, 96(a5)<br>  |
|  30|[0x80002430]<br>0x00000000|- rd : x7<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x407 and fm1 == 0x489b36bd7f503 and rm_val == 0  #nosat<br>                            |[0x8000041c]:fcvt.wu.d t2, ft2, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw t2, 112(a5)<br>    |
|  31|[0x80002440]<br>0x000000AD|- rd : x12<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x406 and fm1 == 0x5ae6a9a6ab329 and rm_val == 0  #nosat<br>                           |[0x80000434]:fcvt.wu.d a2, ft4, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw a2, 128(a5)<br>    |
|  32|[0x80002450]<br>0x00000077|- rd : x25<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x405 and fm1 == 0xdc3386b9f15c4 and rm_val == 0  #nosat<br>                          |[0x8000044c]:fcvt.wu.d s9, ft9, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:sw s9, 144(a5)<br>    |
|  33|[0x80002460]<br>0x0000002C|- fs1 == 0 and fe1 == 0x404 and fm1 == 0x5c74eff1e5bef and rm_val == 0  #nosat<br>                                                         |[0x80000464]:fcvt.wu.d t6, ft11, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:sw t6, 160(a5)<br>   |
