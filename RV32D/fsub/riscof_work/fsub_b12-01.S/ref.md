
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800006d0')]      |
| SIG_REGION                | [('0x80002510', '0x800026b0', '104 words')]      |
| COV_LABELS                | fsub_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsub/riscof_work/fsub_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 108      |
| Total Signature Updates   | 26      |
| STAT1                     | 26      |
| STAT2                     | 0      |
| STAT3                     | 25     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003f8]:fsub.d fa5, ft6, fa7, dyn
[0x800003fc]:csrrs a7, fflags, zero
[0x80000400]:fsd fa5, 416(a5)
[0x80000404]:sw a7, 420(a5)
[0x80000408]:fld ft3, 432(a6)
[0x8000040c]:fld ft4, 440(a6)
[0x80000410]:csrrwi zero, frm, 0

[0x80000414]:fsub.d fa4, ft3, ft4, dyn
[0x80000418]:csrrs a7, fflags, zero
[0x8000041c]:fsd fa4, 432(a5)
[0x80000420]:sw a7, 436(a5)
[0x80000424]:fld ft7, 448(a6)
[0x80000428]:fld fa6, 456(a6)
[0x8000042c]:csrrwi zero, frm, 0

[0x80000430]:fsub.d fs1, ft7, fa6, dyn
[0x80000434]:csrrs a7, fflags, zero
[0x80000438]:fsd fs1, 448(a5)
[0x8000043c]:sw a7, 452(a5)
[0x80000440]:fld fs10, 464(a6)
[0x80000444]:fld fa3, 472(a6)
[0x80000448]:csrrwi zero, frm, 0

[0x8000044c]:fsub.d ft10, fs10, fa3, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:fsd ft10, 464(a5)
[0x80000458]:sw a7, 468(a5)
[0x8000045c]:fld fa6, 480(a6)
[0x80000460]:fld ft8, 488(a6)
[0x80000464]:csrrwi zero, frm, 0

[0x80000468]:fsub.d ft9, fa6, ft8, dyn
[0x8000046c]:csrrs a7, fflags, zero
[0x80000470]:fsd ft9, 480(a5)
[0x80000474]:sw a7, 484(a5)
[0x80000478]:fld ft11, 496(a6)
[0x8000047c]:fld ft6, 504(a6)
[0x80000480]:csrrwi zero, frm, 0

[0x80000484]:fsub.d fs8, ft11, ft6, dyn
[0x80000488]:csrrs a7, fflags, zero
[0x8000048c]:fsd fs8, 496(a5)
[0x80000490]:sw a7, 500(a5)
[0x80000494]:fld fa6, 512(a6)
[0x80000498]:fld ft11, 520(a6)
[0x8000049c]:csrrwi zero, frm, 0

[0x800004a0]:fsub.d fs6, fa6, ft11, dyn
[0x800004a4]:csrrs a7, fflags, zero
[0x800004a8]:fsd fs6, 512(a5)
[0x800004ac]:sw a7, 516(a5)
[0x800004b0]:fld fs10, 528(a6)
[0x800004b4]:fld ft6, 536(a6)
[0x800004b8]:csrrwi zero, frm, 0

[0x800004bc]:fsub.d fs3, fs10, ft6, dyn
[0x800004c0]:csrrs a7, fflags, zero
[0x800004c4]:fsd fs3, 528(a5)
[0x800004c8]:sw a7, 532(a5)
[0x800004cc]:fld ft10, 544(a6)
[0x800004d0]:fld ft9, 552(a6)
[0x800004d4]:csrrwi zero, frm, 0

[0x800004d8]:fsub.d ft11, ft10, ft9, dyn
[0x800004dc]:csrrs a7, fflags, zero
[0x800004e0]:fsd ft11, 544(a5)
[0x800004e4]:sw a7, 548(a5)
[0x800004e8]:fld ft10, 560(a6)
[0x800004ec]:fld ft9, 568(a6)
[0x800004f0]:csrrwi zero, frm, 0

[0x800004f4]:fsub.d ft11, ft10, ft9, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:fsd ft11, 560(a5)
[0x80000500]:sw a7, 564(a5)
[0x80000504]:fld ft10, 576(a6)
[0x80000508]:fld ft9, 584(a6)
[0x8000050c]:csrrwi zero, frm, 0

[0x80000510]:fsub.d ft11, ft10, ft9, dyn
[0x80000514]:csrrs a7, fflags, zero
[0x80000518]:fsd ft11, 576(a5)
[0x8000051c]:sw a7, 580(a5)
[0x80000520]:fld ft10, 592(a6)
[0x80000524]:fld ft9, 600(a6)
[0x80000528]:csrrwi zero, frm, 0

[0x8000052c]:fsub.d ft11, ft10, ft9, dyn
[0x80000530]:csrrs a7, fflags, zero
[0x80000534]:fsd ft11, 592(a5)
[0x80000538]:sw a7, 596(a5)
[0x8000053c]:fld ft10, 608(a6)
[0x80000540]:fld ft9, 616(a6)
[0x80000544]:csrrwi zero, frm, 0

[0x80000548]:fsub.d ft11, ft10, ft9, dyn
[0x8000054c]:csrrs a7, fflags, zero
[0x80000550]:fsd ft11, 608(a5)
[0x80000554]:sw a7, 612(a5)
[0x80000558]:fld ft10, 624(a6)
[0x8000055c]:fld ft9, 632(a6)
[0x80000560]:csrrwi zero, frm, 0

[0x80000564]:fsub.d ft11, ft10, ft9, dyn
[0x80000568]:csrrs a7, fflags, zero
[0x8000056c]:fsd ft11, 624(a5)
[0x80000570]:sw a7, 628(a5)
[0x80000574]:fld ft10, 640(a6)
[0x80000578]:fld ft9, 648(a6)
[0x8000057c]:csrrwi zero, frm, 0

[0x80000580]:fsub.d ft11, ft10, ft9, dyn
[0x80000584]:csrrs a7, fflags, zero
[0x80000588]:fsd ft11, 640(a5)
[0x8000058c]:sw a7, 644(a5)
[0x80000590]:fld ft10, 656(a6)
[0x80000594]:fld ft9, 664(a6)
[0x80000598]:csrrwi zero, frm, 0

[0x8000059c]:fsub.d ft11, ft10, ft9, dyn
[0x800005a0]:csrrs a7, fflags, zero
[0x800005a4]:fsd ft11, 656(a5)
[0x800005a8]:sw a7, 660(a5)
[0x800005ac]:fld ft10, 672(a6)
[0x800005b0]:fld ft9, 680(a6)
[0x800005b4]:csrrwi zero, frm, 0

[0x800005b8]:fsub.d ft11, ft10, ft9, dyn
[0x800005bc]:csrrs a7, fflags, zero
[0x800005c0]:fsd ft11, 672(a5)
[0x800005c4]:sw a7, 676(a5)
[0x800005c8]:fld ft10, 688(a6)
[0x800005cc]:fld ft9, 696(a6)
[0x800005d0]:csrrwi zero, frm, 0

[0x800005d4]:fsub.d ft11, ft10, ft9, dyn
[0x800005d8]:csrrs a7, fflags, zero
[0x800005dc]:fsd ft11, 688(a5)
[0x800005e0]:sw a7, 692(a5)
[0x800005e4]:fld ft10, 704(a6)
[0x800005e8]:fld ft9, 712(a6)
[0x800005ec]:csrrwi zero, frm, 0

[0x800005f0]:fsub.d ft11, ft10, ft9, dyn
[0x800005f4]:csrrs a7, fflags, zero
[0x800005f8]:fsd ft11, 704(a5)
[0x800005fc]:sw a7, 708(a5)
[0x80000600]:fld ft10, 720(a6)
[0x80000604]:fld ft9, 728(a6)
[0x80000608]:csrrwi zero, frm, 0

[0x8000060c]:fsub.d ft11, ft10, ft9, dyn
[0x80000610]:csrrs a7, fflags, zero
[0x80000614]:fsd ft11, 720(a5)
[0x80000618]:sw a7, 724(a5)
[0x8000061c]:fld ft10, 736(a6)
[0x80000620]:fld ft9, 744(a6)
[0x80000624]:csrrwi zero, frm, 0

[0x80000628]:fsub.d ft11, ft10, ft9, dyn
[0x8000062c]:csrrs a7, fflags, zero
[0x80000630]:fsd ft11, 736(a5)
[0x80000634]:sw a7, 740(a5)
[0x80000638]:fld ft10, 752(a6)
[0x8000063c]:fld ft9, 760(a6)
[0x80000640]:csrrwi zero, frm, 0

[0x80000644]:fsub.d ft11, ft10, ft9, dyn
[0x80000648]:csrrs a7, fflags, zero
[0x8000064c]:fsd ft11, 752(a5)
[0x80000650]:sw a7, 756(a5)
[0x80000654]:fld ft10, 768(a6)
[0x80000658]:fld ft9, 776(a6)
[0x8000065c]:csrrwi zero, frm, 0

[0x80000660]:fsub.d ft11, ft10, ft9, dyn
[0x80000664]:csrrs a7, fflags, zero
[0x80000668]:fsd ft11, 768(a5)
[0x8000066c]:sw a7, 772(a5)
[0x80000670]:fld ft10, 784(a6)
[0x80000674]:fld ft9, 792(a6)
[0x80000678]:csrrwi zero, frm, 0

[0x8000067c]:fsub.d ft11, ft10, ft9, dyn
[0x80000680]:csrrs a7, fflags, zero
[0x80000684]:fsd ft11, 784(a5)
[0x80000688]:sw a7, 788(a5)
[0x8000068c]:fld ft10, 800(a6)
[0x80000690]:fld ft9, 808(a6)
[0x80000694]:csrrwi zero, frm, 0

[0x80000698]:fsub.d ft11, ft10, ft9, dyn
[0x8000069c]:csrrs a7, fflags, zero
[0x800006a0]:fsd ft11, 800(a5)
[0x800006a4]:sw a7, 804(a5)
[0x800006a8]:fld ft10, 816(a6)
[0x800006ac]:fld ft9, 824(a6)
[0x800006b0]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                               |                                                                         code                                                                          |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002514]<br>0x00000001|- opcode : fsub.d<br> - rs1 : f23<br> - rs2 : f10<br> - rd : f10<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6d1771ceea796 and rm_val == 0  #nosat<br> |[0x80000120]:fsub.d fa0, fs7, fa0, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd fa0, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>       |
|   2|[0x80002524]<br>0x00000001|- rs1 : f28<br> - rs2 : f5<br> - rd : f13<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x1506f64179e12 and rm_val == 0  #nosat<br> |[0x8000013c]:fsub.d fa3, ft8, ft5, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fa3, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>     |
|   3|[0x80002534]<br>0x00000001|- rs1 : f22<br> - rs2 : f22<br> - rd : f21<br> - rs1 == rs2 != rd<br>                                                                                                                                                                   |[0x80000158]:fsub.d fs5, fs6, fs6, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fs5, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>     |
|   4|[0x80002544]<br>0x00000001|- rs1 : f17<br> - rs2 : f12<br> - rd : f17<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2a58446d0baa8 and rm_val == 0  #nosat<br>                       |[0x80000174]:fsub.d fa7, fa7, fa2, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd fa7, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>     |
|   5|[0x80002554]<br>0x00000001|- rs1 : f27<br> - rs2 : f27<br> - rd : f27<br> - rs1 == rs2 == rd<br>                                                                                                                                                                   |[0x80000190]:fsub.d fs11, fs11, fs11, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd fs11, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br> |
|   6|[0x80002564]<br>0x00000001|- rs1 : f9<br> - rs2 : f2<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe71ed696201f1 and rm_val == 0  #nosat<br>                                                |[0x800001ac]:fsub.d fs8, fs1, ft2, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fs8, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>     |
|   7|[0x80002574]<br>0x00000001|- rs1 : f14<br> - rs2 : f8<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1d67f1f990c0b and rm_val == 0  #nosat<br>                                               |[0x800001c8]:fsub.d ft8, fa4, fs0, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd ft8, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>    |
|   8|[0x80002584]<br>0x00000001|- rs1 : f12<br> - rs2 : f23<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xe60b40e314f0c and rm_val == 0  #nosat<br>                                               |[0x800001e4]:fsub.d ft2, fa2, fs7, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd ft2, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>   |
|   9|[0x80002594]<br>0x00000001|- rs1 : f21<br> - rs2 : f7<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xc13973c0771d8 and rm_val == 0  #nosat<br>                                               |[0x80000200]:fsub.d fa6, fs5, ft7, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fa6, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>   |
|  10|[0x800025a4]<br>0x00000001|- rs1 : f29<br> - rs2 : f21<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x41981cc935638 and rm_val == 0  #nosat<br>                                              |[0x8000021c]:fsub.d fs4, ft9, fs5, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd fs4, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>   |
|  11|[0x800025b4]<br>0x00000001|- rs1 : f1<br> - rs2 : f9<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd8e5154788b84 and rm_val == 0  #nosat<br>                                                 |[0x80000238]:fsub.d ft6, ft1, fs1, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd ft6, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>   |
|  12|[0x800025c4]<br>0x00000001|- rs1 : f25<br> - rs2 : f26<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xeb5aac6486d0c and rm_val == 0  #nosat<br>                                              |[0x80000254]:fsub.d fs7, fs9, fs10, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs7, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>  |
|  13|[0x800025d4]<br>0x00000001|- rs1 : f30<br> - rs2 : f14<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc9378d7a8307f and rm_val == 0  #nosat<br>                                              |[0x80000270]:fsub.d fa2, ft10, fa4, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fa2, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>  |
|  14|[0x800025e4]<br>0x00000001|- rs1 : f24<br> - rs2 : f6<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8f5d3484b0730 and rm_val == 0  #nosat<br>                                               |[0x8000028c]:fsub.d fs2, fs8, ft6, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd fs2, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>   |
|  15|[0x800025f4]<br>0x00000001|- rs1 : f11<br> - rs2 : f19<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x51ddbb228ba06 and rm_val == 0  #nosat<br>                                               |[0x800002a8]:fsub.d ft7, fa1, fs3, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd ft7, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>   |
|  16|[0x80002604]<br>0x00000001|- rs1 : f19<br> - rs2 : f3<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd72951a1b8967 and rm_val == 0  #nosat<br>                                               |[0x800002c4]:fsub.d fa1, fs3, ft3, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fa1, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>   |
|  17|[0x80002614]<br>0x00000001|- rs1 : f0<br> - rs2 : f15<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x057ed5782c7d6 and rm_val == 0  #nosat<br>                                                |[0x800002e0]:fsub.d ft4, ft0, fa5, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd ft4, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>   |
|  18|[0x80002624]<br>0x00000001|- rs1 : f20<br> - rs2 : f0<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x147f1b87235fc and rm_val == 0  #nosat<br>                                               |[0x800002fc]:fsub.d fs10, fs4, ft0, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd fs10, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br> |
|  19|[0x80002634]<br>0x00000001|- rs1 : f10<br> - rs2 : f20<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x0734092792958 and rm_val == 0  #nosat<br>                                               |[0x80000318]:fsub.d ft1, fa0, fs4, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd ft1, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>   |
|  20|[0x80002644]<br>0x00000001|- rs1 : f13<br> - rs2 : f24<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xd3d8104d0cdc0 and rm_val == 0  #nosat<br>                                               |[0x80000334]:fsub.d ft3, fa3, fs8, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd ft3, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>   |
|  21|[0x80002654]<br>0x00000001|- rs1 : f5<br> - rs2 : f1<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x80cf7341ff72e and rm_val == 0  #nosat<br>                                                |[0x80000350]:fsub.d fs9, ft5, ft1, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs9, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>   |
|  22|[0x80002664]<br>0x00000001|- rs1 : f2<br> - rs2 : f25<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8848cf5ea9657 and rm_val == 0  #nosat<br>                                                |[0x8000036c]:fsub.d fs0, ft2, fs9, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd fs0, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>   |
|  23|[0x80002674]<br>0x00000001|- rs1 : f8<br> - rs2 : f29<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xaa6c2d4374fa3 and rm_val == 0  #nosat<br>                                               |[0x80000388]:fsub.d ft11, fs0, ft9, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd ft11, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br> |
|  24|[0x80002684]<br>0x00000001|- rs1 : f18<br> - rs2 : f30<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x56e924eb7c838 and rm_val == 0  #nosat<br>                                               |[0x800003a4]:fsub.d ft5, fs2, ft10, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft5, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>  |
|  25|[0x80002694]<br>0x00000001|- rs1 : f4<br> - rs2 : f18<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x2937fe3bd9f20 and rm_val == 0  #nosat<br>                                                |[0x800003c0]:fsub.d ft0, ft4, fs2, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd ft0, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>   |
|  26|[0x800026a4]<br>0x00000001|- rs1 : f15<br> - rs2 : f11<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x23087ed83ab89 and rm_val == 0  #nosat<br>                                              |[0x800003dc]:fsub.d fs6, fa5, fa1, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fs6, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>   |
