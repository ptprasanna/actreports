
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001850')]      |
| SIG_REGION                | [('0x80003f10', '0x800045b0', '424 words')]      |
| COV_LABELS                | fsub_b5      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsub/riscof_work/fsub_b5-01.S/ref.S    |
| Total Number of coverpoints| 318     |
| Total Coverpoints Hit     | 251      |
| Total Signature Updates   | 149      |
| STAT1                     | 149      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000cb8]:fsub.d ft11, ft10, ft9, dyn
[0x80000cbc]:csrrs a7, fflags, zero
[0x80000cc0]:fsd ft11, 1696(a5)
[0x80000cc4]:sw a7, 1700(a5)
[0x80000cc8]:fld ft10, 1712(a6)
[0x80000ccc]:fld ft9, 1720(a6)
[0x80000cd0]:csrrwi zero, frm, 3

[0x80000cd4]:fsub.d ft11, ft10, ft9, dyn
[0x80000cd8]:csrrs a7, fflags, zero
[0x80000cdc]:fsd ft11, 1712(a5)
[0x80000ce0]:sw a7, 1716(a5)
[0x80000ce4]:fld ft10, 1728(a6)
[0x80000ce8]:fld ft9, 1736(a6)
[0x80000cec]:csrrwi zero, frm, 2

[0x80000cf0]:fsub.d ft11, ft10, ft9, dyn
[0x80000cf4]:csrrs a7, fflags, zero
[0x80000cf8]:fsd ft11, 1728(a5)
[0x80000cfc]:sw a7, 1732(a5)
[0x80000d00]:fld ft10, 1744(a6)
[0x80000d04]:fld ft9, 1752(a6)
[0x80000d08]:csrrwi zero, frm, 1

[0x80000d0c]:fsub.d ft11, ft10, ft9, dyn
[0x80000d10]:csrrs a7, fflags, zero
[0x80000d14]:fsd ft11, 1744(a5)
[0x80000d18]:sw a7, 1748(a5)
[0x80000d1c]:fld ft10, 1760(a6)
[0x80000d20]:fld ft9, 1768(a6)
[0x80000d24]:csrrwi zero, frm, 0

[0x80000d28]:fsub.d ft11, ft10, ft9, dyn
[0x80000d2c]:csrrs a7, fflags, zero
[0x80000d30]:fsd ft11, 1760(a5)
[0x80000d34]:sw a7, 1764(a5)
[0x80000d38]:fld ft10, 1776(a6)
[0x80000d3c]:fld ft9, 1784(a6)
[0x80000d40]:csrrwi zero, frm, 4

[0x80000d44]:fsub.d ft11, ft10, ft9, dyn
[0x80000d48]:csrrs a7, fflags, zero
[0x80000d4c]:fsd ft11, 1776(a5)
[0x80000d50]:sw a7, 1780(a5)
[0x80000d54]:fld ft10, 1792(a6)
[0x80000d58]:fld ft9, 1800(a6)
[0x80000d5c]:csrrwi zero, frm, 3

[0x80000d60]:fsub.d ft11, ft10, ft9, dyn
[0x80000d64]:csrrs a7, fflags, zero
[0x80000d68]:fsd ft11, 1792(a5)
[0x80000d6c]:sw a7, 1796(a5)
[0x80000d70]:fld ft10, 1808(a6)
[0x80000d74]:fld ft9, 1816(a6)
[0x80000d78]:csrrwi zero, frm, 2

[0x80000d7c]:fsub.d ft11, ft10, ft9, dyn
[0x80000d80]:csrrs a7, fflags, zero
[0x80000d84]:fsd ft11, 1808(a5)
[0x80000d88]:sw a7, 1812(a5)
[0x80000d8c]:fld ft10, 1824(a6)
[0x80000d90]:fld ft9, 1832(a6)
[0x80000d94]:csrrwi zero, frm, 1

[0x80000d98]:fsub.d ft11, ft10, ft9, dyn
[0x80000d9c]:csrrs a7, fflags, zero
[0x80000da0]:fsd ft11, 1824(a5)
[0x80000da4]:sw a7, 1828(a5)
[0x80000da8]:fld ft10, 1840(a6)
[0x80000dac]:fld ft9, 1848(a6)
[0x80000db0]:csrrwi zero, frm, 0

[0x80000db4]:fsub.d ft11, ft10, ft9, dyn
[0x80000db8]:csrrs a7, fflags, zero
[0x80000dbc]:fsd ft11, 1840(a5)
[0x80000dc0]:sw a7, 1844(a5)
[0x80000dc4]:fld ft10, 1856(a6)
[0x80000dc8]:fld ft9, 1864(a6)
[0x80000dcc]:csrrwi zero, frm, 4

[0x80000dd0]:fsub.d ft11, ft10, ft9, dyn
[0x80000dd4]:csrrs a7, fflags, zero
[0x80000dd8]:fsd ft11, 1856(a5)
[0x80000ddc]:sw a7, 1860(a5)
[0x80000de0]:fld ft10, 1872(a6)
[0x80000de4]:fld ft9, 1880(a6)
[0x80000de8]:csrrwi zero, frm, 3

[0x80000dec]:fsub.d ft11, ft10, ft9, dyn
[0x80000df0]:csrrs a7, fflags, zero
[0x80000df4]:fsd ft11, 1872(a5)
[0x80000df8]:sw a7, 1876(a5)
[0x80000dfc]:fld ft10, 1888(a6)
[0x80000e00]:fld ft9, 1896(a6)
[0x80000e04]:csrrwi zero, frm, 2

[0x80000e08]:fsub.d ft11, ft10, ft9, dyn
[0x80000e0c]:csrrs a7, fflags, zero
[0x80000e10]:fsd ft11, 1888(a5)
[0x80000e14]:sw a7, 1892(a5)
[0x80000e18]:fld ft10, 1904(a6)
[0x80000e1c]:fld ft9, 1912(a6)
[0x80000e20]:csrrwi zero, frm, 1

[0x80000e24]:fsub.d ft11, ft10, ft9, dyn
[0x80000e28]:csrrs a7, fflags, zero
[0x80000e2c]:fsd ft11, 1904(a5)
[0x80000e30]:sw a7, 1908(a5)
[0x80000e34]:fld ft10, 1920(a6)
[0x80000e38]:fld ft9, 1928(a6)
[0x80000e3c]:csrrwi zero, frm, 0

[0x80000e40]:fsub.d ft11, ft10, ft9, dyn
[0x80000e44]:csrrs a7, fflags, zero
[0x80000e48]:fsd ft11, 1920(a5)
[0x80000e4c]:sw a7, 1924(a5)
[0x80000e50]:fld ft10, 1936(a6)
[0x80000e54]:fld ft9, 1944(a6)
[0x80000e58]:csrrwi zero, frm, 4

[0x80000e5c]:fsub.d ft11, ft10, ft9, dyn
[0x80000e60]:csrrs a7, fflags, zero
[0x80000e64]:fsd ft11, 1936(a5)
[0x80000e68]:sw a7, 1940(a5)
[0x80000e6c]:fld ft10, 1952(a6)
[0x80000e70]:fld ft9, 1960(a6)
[0x80000e74]:csrrwi zero, frm, 3

[0x80000e78]:fsub.d ft11, ft10, ft9, dyn
[0x80000e7c]:csrrs a7, fflags, zero
[0x80000e80]:fsd ft11, 1952(a5)
[0x80000e84]:sw a7, 1956(a5)
[0x80000e88]:fld ft10, 1968(a6)
[0x80000e8c]:fld ft9, 1976(a6)
[0x80000e90]:csrrwi zero, frm, 2

[0x80000e94]:fsub.d ft11, ft10, ft9, dyn
[0x80000e98]:csrrs a7, fflags, zero
[0x80000e9c]:fsd ft11, 1968(a5)
[0x80000ea0]:sw a7, 1972(a5)
[0x80000ea4]:fld ft10, 1984(a6)
[0x80000ea8]:fld ft9, 1992(a6)
[0x80000eac]:csrrwi zero, frm, 1

[0x80000eb0]:fsub.d ft11, ft10, ft9, dyn
[0x80000eb4]:csrrs a7, fflags, zero
[0x80000eb8]:fsd ft11, 1984(a5)
[0x80000ebc]:sw a7, 1988(a5)
[0x80000ec0]:fld ft10, 2000(a6)
[0x80000ec4]:fld ft9, 2008(a6)
[0x80000ec8]:csrrwi zero, frm, 0

[0x80000ecc]:fsub.d ft11, ft10, ft9, dyn
[0x80000ed0]:csrrs a7, fflags, zero
[0x80000ed4]:fsd ft11, 2000(a5)
[0x80000ed8]:sw a7, 2004(a5)
[0x80000edc]:fld ft10, 2016(a6)
[0x80000ee0]:fld ft9, 2024(a6)
[0x80000ee4]:csrrwi zero, frm, 4

[0x80000ee8]:fsub.d ft11, ft10, ft9, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 2016(a5)
[0x80000ef4]:sw a7, 2020(a5)
[0x80000ef8]:addi a6, a6, 2032
[0x80000efc]:auipc a5, 3
[0x80000f00]:addi a5, a5, 1036
[0x80000f04]:fld ft10, 0(a6)
[0x80000f08]:fld ft9, 8(a6)
[0x80000f0c]:csrrwi zero, frm, 3

[0x800013c4]:fsub.d ft11, ft10, ft9, dyn
[0x800013c8]:csrrs a7, fflags, zero
[0x800013cc]:fsd ft11, 688(a5)
[0x800013d0]:sw a7, 692(a5)
[0x800013d4]:fld ft10, 704(a6)
[0x800013d8]:fld ft9, 712(a6)
[0x800013dc]:csrrwi zero, frm, 4

[0x800013e0]:fsub.d ft11, ft10, ft9, dyn
[0x800013e4]:csrrs a7, fflags, zero
[0x800013e8]:fsd ft11, 704(a5)
[0x800013ec]:sw a7, 708(a5)
[0x800013f0]:fld ft10, 720(a6)
[0x800013f4]:fld ft9, 728(a6)
[0x800013f8]:csrrwi zero, frm, 3

[0x800013fc]:fsub.d ft11, ft10, ft9, dyn
[0x80001400]:csrrs a7, fflags, zero
[0x80001404]:fsd ft11, 720(a5)
[0x80001408]:sw a7, 724(a5)
[0x8000140c]:fld ft10, 736(a6)
[0x80001410]:fld ft9, 744(a6)
[0x80001414]:csrrwi zero, frm, 2

[0x80001418]:fsub.d ft11, ft10, ft9, dyn
[0x8000141c]:csrrs a7, fflags, zero
[0x80001420]:fsd ft11, 736(a5)
[0x80001424]:sw a7, 740(a5)
[0x80001428]:fld ft10, 752(a6)
[0x8000142c]:fld ft9, 760(a6)
[0x80001430]:csrrwi zero, frm, 1

[0x80001434]:fsub.d ft11, ft10, ft9, dyn
[0x80001438]:csrrs a7, fflags, zero
[0x8000143c]:fsd ft11, 752(a5)
[0x80001440]:sw a7, 756(a5)
[0x80001444]:fld ft10, 768(a6)
[0x80001448]:fld ft9, 776(a6)
[0x8000144c]:csrrwi zero, frm, 0

[0x80001450]:fsub.d ft11, ft10, ft9, dyn
[0x80001454]:csrrs a7, fflags, zero
[0x80001458]:fsd ft11, 768(a5)
[0x8000145c]:sw a7, 772(a5)
[0x80001460]:fld ft10, 784(a6)
[0x80001464]:fld ft9, 792(a6)
[0x80001468]:csrrwi zero, frm, 4

[0x8000146c]:fsub.d ft11, ft10, ft9, dyn
[0x80001470]:csrrs a7, fflags, zero
[0x80001474]:fsd ft11, 784(a5)
[0x80001478]:sw a7, 788(a5)
[0x8000147c]:fld ft10, 800(a6)
[0x80001480]:fld ft9, 808(a6)
[0x80001484]:csrrwi zero, frm, 3

[0x80001488]:fsub.d ft11, ft10, ft9, dyn
[0x8000148c]:csrrs a7, fflags, zero
[0x80001490]:fsd ft11, 800(a5)
[0x80001494]:sw a7, 804(a5)
[0x80001498]:fld ft10, 816(a6)
[0x8000149c]:fld ft9, 824(a6)
[0x800014a0]:csrrwi zero, frm, 2

[0x800014a4]:fsub.d ft11, ft10, ft9, dyn
[0x800014a8]:csrrs a7, fflags, zero
[0x800014ac]:fsd ft11, 816(a5)
[0x800014b0]:sw a7, 820(a5)
[0x800014b4]:fld ft10, 832(a6)
[0x800014b8]:fld ft9, 840(a6)
[0x800014bc]:csrrwi zero, frm, 1

[0x800014c0]:fsub.d ft11, ft10, ft9, dyn
[0x800014c4]:csrrs a7, fflags, zero
[0x800014c8]:fsd ft11, 832(a5)
[0x800014cc]:sw a7, 836(a5)
[0x800014d0]:fld ft10, 848(a6)
[0x800014d4]:fld ft9, 856(a6)
[0x800014d8]:csrrwi zero, frm, 0

[0x800014dc]:fsub.d ft11, ft10, ft9, dyn
[0x800014e0]:csrrs a7, fflags, zero
[0x800014e4]:fsd ft11, 848(a5)
[0x800014e8]:sw a7, 852(a5)
[0x800014ec]:fld ft10, 864(a6)
[0x800014f0]:fld ft9, 872(a6)
[0x800014f4]:csrrwi zero, frm, 4

[0x800014f8]:fsub.d ft11, ft10, ft9, dyn
[0x800014fc]:csrrs a7, fflags, zero
[0x80001500]:fsd ft11, 864(a5)
[0x80001504]:sw a7, 868(a5)
[0x80001508]:fld ft10, 880(a6)
[0x8000150c]:fld ft9, 888(a6)
[0x80001510]:csrrwi zero, frm, 3

[0x80001514]:fsub.d ft11, ft10, ft9, dyn
[0x80001518]:csrrs a7, fflags, zero
[0x8000151c]:fsd ft11, 880(a5)
[0x80001520]:sw a7, 884(a5)
[0x80001524]:fld ft10, 896(a6)
[0x80001528]:fld ft9, 904(a6)
[0x8000152c]:csrrwi zero, frm, 2

[0x80001530]:fsub.d ft11, ft10, ft9, dyn
[0x80001534]:csrrs a7, fflags, zero
[0x80001538]:fsd ft11, 896(a5)
[0x8000153c]:sw a7, 900(a5)
[0x80001540]:fld ft10, 912(a6)
[0x80001544]:fld ft9, 920(a6)
[0x80001548]:csrrwi zero, frm, 1

[0x8000154c]:fsub.d ft11, ft10, ft9, dyn
[0x80001550]:csrrs a7, fflags, zero
[0x80001554]:fsd ft11, 912(a5)
[0x80001558]:sw a7, 916(a5)
[0x8000155c]:fld ft10, 928(a6)
[0x80001560]:fld ft9, 936(a6)
[0x80001564]:csrrwi zero, frm, 0

[0x80001568]:fsub.d ft11, ft10, ft9, dyn
[0x8000156c]:csrrs a7, fflags, zero
[0x80001570]:fsd ft11, 928(a5)
[0x80001574]:sw a7, 932(a5)
[0x80001578]:fld ft10, 944(a6)
[0x8000157c]:fld ft9, 952(a6)
[0x80001580]:csrrwi zero, frm, 4

[0x80001584]:fsub.d ft11, ft10, ft9, dyn
[0x80001588]:csrrs a7, fflags, zero
[0x8000158c]:fsd ft11, 944(a5)
[0x80001590]:sw a7, 948(a5)
[0x80001594]:fld ft10, 960(a6)
[0x80001598]:fld ft9, 968(a6)
[0x8000159c]:csrrwi zero, frm, 3

[0x800015a0]:fsub.d ft11, ft10, ft9, dyn
[0x800015a4]:csrrs a7, fflags, zero
[0x800015a8]:fsd ft11, 960(a5)
[0x800015ac]:sw a7, 964(a5)
[0x800015b0]:fld ft10, 976(a6)
[0x800015b4]:fld ft9, 984(a6)
[0x800015b8]:csrrwi zero, frm, 2

[0x800015bc]:fsub.d ft11, ft10, ft9, dyn
[0x800015c0]:csrrs a7, fflags, zero
[0x800015c4]:fsd ft11, 976(a5)
[0x800015c8]:sw a7, 980(a5)
[0x800015cc]:fld ft10, 992(a6)
[0x800015d0]:fld ft9, 1000(a6)
[0x800015d4]:csrrwi zero, frm, 1

[0x800015d8]:fsub.d ft11, ft10, ft9, dyn
[0x800015dc]:csrrs a7, fflags, zero
[0x800015e0]:fsd ft11, 992(a5)
[0x800015e4]:sw a7, 996(a5)
[0x800015e8]:fld ft10, 1008(a6)
[0x800015ec]:fld ft9, 1016(a6)
[0x800015f0]:csrrwi zero, frm, 0

[0x800015f4]:fsub.d ft11, ft10, ft9, dyn
[0x800015f8]:csrrs a7, fflags, zero
[0x800015fc]:fsd ft11, 1008(a5)
[0x80001600]:sw a7, 1012(a5)
[0x80001604]:fld ft10, 1024(a6)
[0x80001608]:fld ft9, 1032(a6)
[0x8000160c]:csrrwi zero, frm, 4

[0x80001610]:fsub.d ft11, ft10, ft9, dyn
[0x80001614]:csrrs a7, fflags, zero
[0x80001618]:fsd ft11, 1024(a5)
[0x8000161c]:sw a7, 1028(a5)
[0x80001620]:fld ft10, 1040(a6)
[0x80001624]:fld ft9, 1048(a6)
[0x80001628]:csrrwi zero, frm, 3

[0x8000162c]:fsub.d ft11, ft10, ft9, dyn
[0x80001630]:csrrs a7, fflags, zero
[0x80001634]:fsd ft11, 1040(a5)
[0x80001638]:sw a7, 1044(a5)
[0x8000163c]:fld ft10, 1056(a6)
[0x80001640]:fld ft9, 1064(a6)
[0x80001644]:csrrwi zero, frm, 2

[0x80001648]:fsub.d ft11, ft10, ft9, dyn
[0x8000164c]:csrrs a7, fflags, zero
[0x80001650]:fsd ft11, 1056(a5)
[0x80001654]:sw a7, 1060(a5)
[0x80001658]:fld ft10, 1072(a6)
[0x8000165c]:fld ft9, 1080(a6)
[0x80001660]:csrrwi zero, frm, 1

[0x80001664]:fsub.d ft11, ft10, ft9, dyn
[0x80001668]:csrrs a7, fflags, zero
[0x8000166c]:fsd ft11, 1072(a5)
[0x80001670]:sw a7, 1076(a5)
[0x80001674]:fld ft10, 1088(a6)
[0x80001678]:fld ft9, 1096(a6)
[0x8000167c]:csrrwi zero, frm, 0

[0x80001680]:fsub.d ft11, ft10, ft9, dyn
[0x80001684]:csrrs a7, fflags, zero
[0x80001688]:fsd ft11, 1088(a5)
[0x8000168c]:sw a7, 1092(a5)
[0x80001690]:fld ft10, 1104(a6)
[0x80001694]:fld ft9, 1112(a6)
[0x80001698]:csrrwi zero, frm, 4

[0x8000169c]:fsub.d ft11, ft10, ft9, dyn
[0x800016a0]:csrrs a7, fflags, zero
[0x800016a4]:fsd ft11, 1104(a5)
[0x800016a8]:sw a7, 1108(a5)
[0x800016ac]:fld ft10, 1120(a6)
[0x800016b0]:fld ft9, 1128(a6)
[0x800016b4]:csrrwi zero, frm, 3

[0x800016b8]:fsub.d ft11, ft10, ft9, dyn
[0x800016bc]:csrrs a7, fflags, zero
[0x800016c0]:fsd ft11, 1120(a5)
[0x800016c4]:sw a7, 1124(a5)
[0x800016c8]:fld ft10, 1136(a6)
[0x800016cc]:fld ft9, 1144(a6)
[0x800016d0]:csrrwi zero, frm, 2

[0x800016d4]:fsub.d ft11, ft10, ft9, dyn
[0x800016d8]:csrrs a7, fflags, zero
[0x800016dc]:fsd ft11, 1136(a5)
[0x800016e0]:sw a7, 1140(a5)
[0x800016e4]:fld ft10, 1152(a6)
[0x800016e8]:fld ft9, 1160(a6)
[0x800016ec]:csrrwi zero, frm, 1

[0x800016f0]:fsub.d ft11, ft10, ft9, dyn
[0x800016f4]:csrrs a7, fflags, zero
[0x800016f8]:fsd ft11, 1152(a5)
[0x800016fc]:sw a7, 1156(a5)
[0x80001700]:fld ft10, 1168(a6)
[0x80001704]:fld ft9, 1176(a6)
[0x80001708]:csrrwi zero, frm, 0

[0x8000170c]:fsub.d ft11, ft10, ft9, dyn
[0x80001710]:csrrs a7, fflags, zero
[0x80001714]:fsd ft11, 1168(a5)
[0x80001718]:sw a7, 1172(a5)
[0x8000171c]:fld ft10, 1184(a6)
[0x80001720]:fld ft9, 1192(a6)
[0x80001724]:csrrwi zero, frm, 4

[0x80001728]:fsub.d ft11, ft10, ft9, dyn
[0x8000172c]:csrrs a7, fflags, zero
[0x80001730]:fsd ft11, 1184(a5)
[0x80001734]:sw a7, 1188(a5)
[0x80001738]:fld ft10, 1200(a6)
[0x8000173c]:fld ft9, 1208(a6)
[0x80001740]:csrrwi zero, frm, 3

[0x80001744]:fsub.d ft11, ft10, ft9, dyn
[0x80001748]:csrrs a7, fflags, zero
[0x8000174c]:fsd ft11, 1200(a5)
[0x80001750]:sw a7, 1204(a5)
[0x80001754]:fld ft10, 1216(a6)
[0x80001758]:fld ft9, 1224(a6)
[0x8000175c]:csrrwi zero, frm, 2

[0x80001760]:fsub.d ft11, ft10, ft9, dyn
[0x80001764]:csrrs a7, fflags, zero
[0x80001768]:fsd ft11, 1216(a5)
[0x8000176c]:sw a7, 1220(a5)
[0x80001770]:fld ft10, 1232(a6)
[0x80001774]:fld ft9, 1240(a6)
[0x80001778]:csrrwi zero, frm, 1

[0x8000177c]:fsub.d ft11, ft10, ft9, dyn
[0x80001780]:csrrs a7, fflags, zero
[0x80001784]:fsd ft11, 1232(a5)
[0x80001788]:sw a7, 1236(a5)
[0x8000178c]:fld ft10, 1248(a6)
[0x80001790]:fld ft9, 1256(a6)
[0x80001794]:csrrwi zero, frm, 0

[0x80001798]:fsub.d ft11, ft10, ft9, dyn
[0x8000179c]:csrrs a7, fflags, zero
[0x800017a0]:fsd ft11, 1248(a5)
[0x800017a4]:sw a7, 1252(a5)
[0x800017a8]:fld ft10, 1264(a6)
[0x800017ac]:fld ft9, 1272(a6)
[0x800017b0]:csrrwi zero, frm, 4

[0x800017b4]:fsub.d ft11, ft10, ft9, dyn
[0x800017b8]:csrrs a7, fflags, zero
[0x800017bc]:fsd ft11, 1264(a5)
[0x800017c0]:sw a7, 1268(a5)
[0x800017c4]:fld ft10, 1280(a6)
[0x800017c8]:fld ft9, 1288(a6)
[0x800017cc]:csrrwi zero, frm, 3

[0x800017d0]:fsub.d ft11, ft10, ft9, dyn
[0x800017d4]:csrrs a7, fflags, zero
[0x800017d8]:fsd ft11, 1280(a5)
[0x800017dc]:sw a7, 1284(a5)
[0x800017e0]:fld ft10, 1296(a6)
[0x800017e4]:fld ft9, 1304(a6)
[0x800017e8]:csrrwi zero, frm, 2

[0x800017ec]:fsub.d ft11, ft10, ft9, dyn
[0x800017f0]:csrrs a7, fflags, zero
[0x800017f4]:fsd ft11, 1296(a5)
[0x800017f8]:sw a7, 1300(a5)
[0x800017fc]:fld ft10, 1312(a6)
[0x80001800]:fld ft9, 1320(a6)
[0x80001804]:csrrwi zero, frm, 1

[0x80001808]:fsub.d ft11, ft10, ft9, dyn
[0x8000180c]:csrrs a7, fflags, zero
[0x80001810]:fsd ft11, 1312(a5)
[0x80001814]:sw a7, 1316(a5)
[0x80001818]:fld ft10, 1328(a6)
[0x8000181c]:fld ft9, 1336(a6)
[0x80001820]:csrrwi zero, frm, 3

[0x80001824]:fsub.d ft11, ft10, ft9, dyn
[0x80001828]:csrrs a7, fflags, zero
[0x8000182c]:fsd ft11, 1328(a5)
[0x80001830]:sw a7, 1332(a5)
[0x80001834]:fld ft10, 1344(a6)
[0x80001838]:fld ft9, 1352(a6)
[0x8000183c]:csrrwi zero, frm, 1



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                              |                                                                           code                                                                           |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80003f14]<br>0x00000000|- opcode : fsub.d<br> - rs1 : f11<br> - rs2 : f6<br> - rd : f6<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x132d8f91b7583 and rm_val == 0  #nosat<br>  |[0x80000120]:fsub.d ft6, fa1, ft6, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd ft6, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>          |
|   2|[0x80003f24]<br>0x00000000|- rs1 : f17<br> - rs2 : f2<br> - rd : f9<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 4  #nosat<br> |[0x8000013c]:fsub.d fs1, fa7, ft2, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fs1, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>        |
|   3|[0x80003f34]<br>0x00000000|- rs1 : f1<br> - rs2 : f1<br> - rd : f4<br> - rs1 == rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 3  #nosat<br>                         |[0x80000158]:fsub.d ft4, ft1, ft1, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd ft4, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>        |
|   4|[0x80003f44]<br>0x00000000|- rs1 : f13<br> - rs2 : f22<br> - rd : f13<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 2  #nosat<br>                      |[0x80000174]:fsub.d fa3, fa3, fs6, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd fa3, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>        |
|   5|[0x80003f54]<br>0x00000000|- rs1 : f21<br> - rs2 : f21<br> - rd : f21<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 1  #nosat<br>                      |[0x80000190]:fsub.d fs5, fs5, fs5, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd fs5, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>        |
|   6|[0x80003f64]<br>0x00000000|- rs1 : f25<br> - rs2 : f16<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 0  #nosat<br>                                              |[0x800001ac]:fsub.d ft5, fs9, fa6, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd ft5, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>        |
|   7|[0x80003f74]<br>0x00000000|- rs1 : f4<br> - rs2 : f17<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and rm_val == 4  #nosat<br>                                               |[0x800001c8]:fsub.d ft3, ft4, fa7, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd ft3, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>       |
|   8|[0x80003f84]<br>0x00000000|- rs1 : f30<br> - rs2 : f15<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and rm_val == 3  #nosat<br>                                              |[0x800001e4]:fsub.d ft2, ft10, fa5, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd ft2, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>     |
|   9|[0x80003f94]<br>0x00000000|- rs1 : f2<br> - rs2 : f23<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and rm_val == 2  #nosat<br>                                              |[0x80000200]:fsub.d fs11, ft2, fs7, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fs11, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>    |
|  10|[0x80003fa4]<br>0x00000000|- rs1 : f27<br> - rs2 : f20<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and rm_val == 1  #nosat<br>                                             |[0x8000021c]:fsub.d fs9, fs11, fs4, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd fs9, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>     |
|  11|[0x80003fb4]<br>0x00000000|- rs1 : f10<br> - rs2 : f26<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8a82024cc4e03 and rm_val == 0  #nosat<br>                                             |[0x80000238]:fsub.d fa1, fa0, fs10, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd fa1, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>     |
|  12|[0x80003fc4]<br>0x00000000|- rs1 : f16<br> - rs2 : f10<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and rm_val == 4  #nosat<br>                                             |[0x80000254]:fsub.d fs3, fa6, fa0, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fs3, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>      |
|  13|[0x80003fd4]<br>0x00000000|- rs1 : f26<br> - rs2 : f3<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and rm_val == 3  #nosat<br>                                              |[0x80000270]:fsub.d fa6, fs10, ft3, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd fa6, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>     |
|  14|[0x80003fe4]<br>0x00000000|- rs1 : f28<br> - rs2 : f18<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and rm_val == 2  #nosat<br>                                             |[0x8000028c]:fsub.d ft11, ft8, fs2, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd ft11, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>    |
|  15|[0x80003ff4]<br>0x00000000|- rs1 : f23<br> - rs2 : f4<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and rm_val == 1  #nosat<br>                                              |[0x800002a8]:fsub.d fs8, fs7, ft4, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd fs8, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>      |
|  16|[0x80004004]<br>0x00000000|- rs1 : f12<br> - rs2 : f9<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0125698e86242 and rm_val == 0  #nosat<br>                                              |[0x800002c4]:fsub.d fa5, fa2, fs1, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fa5, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>      |
|  17|[0x80004014]<br>0x00000000|- rs1 : f15<br> - rs2 : f30<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and rm_val == 4  #nosat<br>                                             |[0x800002e0]:fsub.d fa2, fa5, ft10, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd fa2, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>     |
|  18|[0x80004024]<br>0x00000000|- rs1 : f20<br> - rs2 : f12<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and rm_val == 3  #nosat<br>                                              |[0x800002fc]:fsub.d ft1, fs4, fa2, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd ft1, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br>      |
|  19|[0x80004034]<br>0x00000000|- rs1 : f18<br> - rs2 : f8<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and rm_val == 2  #nosat<br>                                               |[0x80000318]:fsub.d ft0, fs2, fs0, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd ft0, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>      |
|  20|[0x80004044]<br>0x00000000|- rs1 : f24<br> - rs2 : f5<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and rm_val == 1  #nosat<br>                                              |[0x80000334]:fsub.d fa7, fs8, ft5, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd fa7, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>      |
|  21|[0x80004054]<br>0x00000000|- rs1 : f0<br> - rs2 : f31<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x930bcbd2d6035 and rm_val == 0  #nosat<br>                                               |[0x80000350]:fsub.d fs0, ft0, ft11, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs0, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>     |
|  22|[0x80004064]<br>0x00000000|- rs1 : f29<br> - rs2 : f25<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and rm_val == 4  #nosat<br>                                             |[0x8000036c]:fsub.d fs10, ft9, fs9, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd fs10, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>    |
|  23|[0x80004074]<br>0x00000000|- rs1 : f19<br> - rs2 : f27<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and rm_val == 3  #nosat<br>                                             |[0x80000388]:fsub.d fs6, fs3, fs11, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd fs6, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>     |
|  24|[0x80004084]<br>0x00000000|- rs1 : f7<br> - rs2 : f11<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and rm_val == 2  #nosat<br>                                              |[0x800003a4]:fsub.d ft9, ft7, fa1, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft9, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>      |
|  25|[0x80004094]<br>0x00000000|- rs1 : f5<br> - rs2 : f24<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and rm_val == 1  #nosat<br>                                              |[0x800003c0]:fsub.d fa4, ft5, fs8, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd fa4, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>      |
|  26|[0x800040a4]<br>0x00000000|- rs1 : f3<br> - rs2 : f14<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf7646167590ef and rm_val == 0  #nosat<br>                                              |[0x800003dc]:fsub.d fa0, ft3, fa4, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fa0, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>      |
|  27|[0x800040b4]<br>0x00000000|- rs1 : f14<br> - rs2 : f19<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and rm_val == 4  #nosat<br>                                              |[0x800003f8]:fsub.d ft7, fa4, fs3, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd ft7, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>      |
|  28|[0x800040c4]<br>0x00000000|- rs1 : f9<br> - rs2 : f29<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and rm_val == 3  #nosat<br>                                              |[0x80000414]:fsub.d fs4, fs1, ft9, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fs4, 432(a5)<br> [0x80000420]:sw a7, 436(a5)<br>      |
|  29|[0x800040d4]<br>0x00000000|- rs1 : f8<br> - rs2 : f0<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and rm_val == 2  #nosat<br>                                               |[0x80000430]:fsub.d ft10, fs0, ft0, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsd ft10, 448(a5)<br> [0x8000043c]:sw a7, 452(a5)<br>    |
|  30|[0x800040e4]<br>0x00000000|- rs1 : f6<br> - rs2 : f7<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and rm_val == 1  #nosat<br>                                               |[0x8000044c]:fsub.d ft8, ft6, ft7, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd ft8, 464(a5)<br> [0x80000458]:sw a7, 468(a5)<br>      |
|  31|[0x800040f4]<br>0x00000000|- rs1 : f31<br> - rs2 : f28<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x643f753bef22f and rm_val == 0  #nosat<br>                                             |[0x80000468]:fsub.d fs7, ft11, ft8, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsd fs7, 480(a5)<br> [0x80000474]:sw a7, 484(a5)<br>     |
|  32|[0x80004104]<br>0x00000000|- rs1 : f22<br> - rs2 : f13<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and rm_val == 4  #nosat<br>                                             |[0x80000484]:fsub.d fs2, fs6, fa3, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fs2, 496(a5)<br> [0x80000490]:sw a7, 500(a5)<br>      |
|  33|[0x80004114]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and rm_val == 3  #nosat<br>                                                                                            |[0x800004a0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsd ft11, 512(a5)<br> [0x800004ac]:sw a7, 516(a5)<br>   |
|  34|[0x80004124]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and rm_val == 2  #nosat<br>                                                                                            |[0x800004bc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft11, 528(a5)<br> [0x800004c8]:sw a7, 532(a5)<br>   |
|  35|[0x80004134]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and rm_val == 1  #nosat<br>                                                                                            |[0x800004d8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsd ft11, 544(a5)<br> [0x800004e4]:sw a7, 548(a5)<br>   |
|  36|[0x80004144]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf57237ddcb451 and rm_val == 0  #nosat<br>                                                                                            |[0x800004f4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 560(a5)<br> [0x80000500]:sw a7, 564(a5)<br>   |
|  37|[0x80004154]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and rm_val == 4  #nosat<br>                                                                                            |[0x80000510]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd ft11, 576(a5)<br> [0x8000051c]:sw a7, 580(a5)<br>   |
|  38|[0x80004164]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and rm_val == 3  #nosat<br>                                                                                            |[0x8000052c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsd ft11, 592(a5)<br> [0x80000538]:sw a7, 596(a5)<br>   |
|  39|[0x80004174]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and rm_val == 2  #nosat<br>                                                                                            |[0x80000548]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsd ft11, 608(a5)<br> [0x80000554]:sw a7, 612(a5)<br>   |
|  40|[0x80004184]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and rm_val == 1  #nosat<br>                                                                                            |[0x80000564]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 624(a5)<br> [0x80000570]:sw a7, 628(a5)<br>   |
|  41|[0x80004194]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0ab870b5c1c40 and rm_val == 0  #nosat<br>                                                                                            |[0x80000580]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsd ft11, 640(a5)<br> [0x8000058c]:sw a7, 644(a5)<br>   |
|  42|[0x800041a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and rm_val == 4  #nosat<br>                                                                                            |[0x8000059c]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 656(a5)<br> [0x800005a8]:sw a7, 660(a5)<br>   |
|  43|[0x800041b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and rm_val == 3  #nosat<br>                                                                                            |[0x800005b8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsd ft11, 672(a5)<br> [0x800005c4]:sw a7, 676(a5)<br>   |
|  44|[0x800041c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and rm_val == 2  #nosat<br>                                                                                            |[0x800005d4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsd ft11, 688(a5)<br> [0x800005e0]:sw a7, 692(a5)<br>   |
|  45|[0x800041d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and rm_val == 1  #nosat<br>                                                                                            |[0x800005f0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsd ft11, 704(a5)<br> [0x800005fc]:sw a7, 708(a5)<br>   |
|  46|[0x800041e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04507a06e8587 and rm_val == 0  #nosat<br>                                                                                            |[0x8000060c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft11, 720(a5)<br> [0x80000618]:sw a7, 724(a5)<br>   |
|  47|[0x800041f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and rm_val == 4  #nosat<br>                                                                                            |[0x80000628]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsd ft11, 736(a5)<br> [0x80000634]:sw a7, 740(a5)<br>   |
|  48|[0x80004204]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and rm_val == 3  #nosat<br>                                                                                            |[0x80000644]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 752(a5)<br> [0x80000650]:sw a7, 756(a5)<br>   |
|  49|[0x80004214]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and rm_val == 2  #nosat<br>                                                                                            |[0x80000660]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd ft11, 768(a5)<br> [0x8000066c]:sw a7, 772(a5)<br>   |
|  50|[0x80004224]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and rm_val == 1  #nosat<br>                                                                                            |[0x8000067c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsd ft11, 784(a5)<br> [0x80000688]:sw a7, 788(a5)<br>   |
|  51|[0x80004234]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7fb2260b115e9 and rm_val == 0  #nosat<br>                                                                                            |[0x80000698]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsd ft11, 800(a5)<br> [0x800006a4]:sw a7, 804(a5)<br>   |
|  52|[0x80004244]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and rm_val == 4  #nosat<br>                                                                                            |[0x800006b4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 816(a5)<br> [0x800006c0]:sw a7, 820(a5)<br>   |
|  53|[0x80004254]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and rm_val == 3  #nosat<br>                                                                                            |[0x800006d0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006d4]:csrrs a7, fflags, zero<br> [0x800006d8]:fsd ft11, 832(a5)<br> [0x800006dc]:sw a7, 836(a5)<br>   |
|  54|[0x80004264]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and rm_val == 2  #nosat<br>                                                                                            |[0x800006ec]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 848(a5)<br> [0x800006f8]:sw a7, 852(a5)<br>   |
|  55|[0x80004274]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and rm_val == 1  #nosat<br>                                                                                            |[0x80000708]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:fsd ft11, 864(a5)<br> [0x80000714]:sw a7, 868(a5)<br>   |
|  56|[0x80004284]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x67f4f571a752e and rm_val == 0  #nosat<br>                                                                                            |[0x80000724]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 880(a5)<br> [0x80000730]:sw a7, 884(a5)<br>   |
|  57|[0x80004294]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and rm_val == 4  #nosat<br>                                                                                            |[0x80000740]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd ft11, 896(a5)<br> [0x8000074c]:sw a7, 900(a5)<br>   |
|  58|[0x800042a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and rm_val == 3  #nosat<br>                                                                                            |[0x8000075c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 912(a5)<br> [0x80000768]:sw a7, 916(a5)<br>   |
|  59|[0x800042b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and rm_val == 2  #nosat<br>                                                                                            |[0x80000778]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000077c]:csrrs a7, fflags, zero<br> [0x80000780]:fsd ft11, 928(a5)<br> [0x80000784]:sw a7, 932(a5)<br>   |
|  60|[0x800042c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and rm_val == 1  #nosat<br>                                                                                            |[0x80000794]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:fsd ft11, 944(a5)<br> [0x800007a0]:sw a7, 948(a5)<br>   |
|  61|[0x800042d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6251b45dfbd3b and rm_val == 0  #nosat<br>                                                                                            |[0x800007b0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:fsd ft11, 960(a5)<br> [0x800007bc]:sw a7, 964(a5)<br>   |
|  62|[0x800042e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and rm_val == 4  #nosat<br>                                                                                            |[0x800007cc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007d0]:csrrs a7, fflags, zero<br> [0x800007d4]:fsd ft11, 976(a5)<br> [0x800007d8]:sw a7, 980(a5)<br>   |
|  63|[0x800042f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and rm_val == 3  #nosat<br>                                                                                            |[0x800007e8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:fsd ft11, 992(a5)<br> [0x800007f4]:sw a7, 996(a5)<br>   |
|  64|[0x80004304]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and rm_val == 2  #nosat<br>                                                                                            |[0x80000804]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 1008(a5)<br> [0x80000810]:sw a7, 1012(a5)<br> |
|  65|[0x80004314]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and rm_val == 1  #nosat<br>                                                                                            |[0x80000820]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000824]:csrrs a7, fflags, zero<br> [0x80000828]:fsd ft11, 1024(a5)<br> [0x8000082c]:sw a7, 1028(a5)<br> |
|  66|[0x80004324]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x98455e99dfdb1 and rm_val == 0  #nosat<br>                                                                                            |[0x8000083c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd ft11, 1040(a5)<br> [0x80000848]:sw a7, 1044(a5)<br> |
|  67|[0x80004334]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and rm_val == 4  #nosat<br>                                                                                            |[0x80000858]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:fsd ft11, 1056(a5)<br> [0x80000864]:sw a7, 1060(a5)<br> |
|  68|[0x80004344]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and rm_val == 3  #nosat<br>                                                                                            |[0x80000874]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000878]:csrrs a7, fflags, zero<br> [0x8000087c]:fsd ft11, 1072(a5)<br> [0x80000880]:sw a7, 1076(a5)<br> |
|  69|[0x80004354]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and rm_val == 2  #nosat<br>                                                                                            |[0x80000890]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:fsd ft11, 1088(a5)<br> [0x8000089c]:sw a7, 1092(a5)<br> |
|  70|[0x80004364]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and rm_val == 1  #nosat<br>                                                                                            |[0x800008ac]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 1104(a5)<br> [0x800008b8]:sw a7, 1108(a5)<br> |
|  71|[0x80004374]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x1ad5e9ebc09df and rm_val == 0  #nosat<br>                                                                                            |[0x800008c8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:fsd ft11, 1120(a5)<br> [0x800008d4]:sw a7, 1124(a5)<br> |
|  72|[0x80004384]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and rm_val == 4  #nosat<br>                                                                                            |[0x800008e4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 1136(a5)<br> [0x800008f0]:sw a7, 1140(a5)<br> |
|  73|[0x80004394]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and rm_val == 3  #nosat<br>                                                                                            |[0x80000900]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000904]:csrrs a7, fflags, zero<br> [0x80000908]:fsd ft11, 1152(a5)<br> [0x8000090c]:sw a7, 1156(a5)<br> |
|  74|[0x800043a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and rm_val == 2  #nosat<br>                                                                                            |[0x8000091c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000920]:csrrs a7, fflags, zero<br> [0x80000924]:fsd ft11, 1168(a5)<br> [0x80000928]:sw a7, 1172(a5)<br> |
|  75|[0x800043b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and rm_val == 1  #nosat<br>                                                                                            |[0x80000938]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000093c]:csrrs a7, fflags, zero<br> [0x80000940]:fsd ft11, 1184(a5)<br> [0x80000944]:sw a7, 1188(a5)<br> |
|  76|[0x800043c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x02b48f992cb49 and rm_val == 0  #nosat<br>                                                                                            |[0x80000954]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsd ft11, 1200(a5)<br> [0x80000960]:sw a7, 1204(a5)<br> |
|  77|[0x800043d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and rm_val == 4  #nosat<br>                                                                                            |[0x80000970]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000974]:csrrs a7, fflags, zero<br> [0x80000978]:fsd ft11, 1216(a5)<br> [0x8000097c]:sw a7, 1220(a5)<br> |
|  78|[0x800043e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and rm_val == 3  #nosat<br>                                                                                            |[0x8000098c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:fsd ft11, 1232(a5)<br> [0x80000998]:sw a7, 1236(a5)<br> |
|  79|[0x800043f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and rm_val == 2  #nosat<br>                                                                                            |[0x800009a8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009ac]:csrrs a7, fflags, zero<br> [0x800009b0]:fsd ft11, 1248(a5)<br> [0x800009b4]:sw a7, 1252(a5)<br> |
|  80|[0x80004404]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and rm_val == 1  #nosat<br>                                                                                            |[0x800009c4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1264(a5)<br> [0x800009d0]:sw a7, 1268(a5)<br> |
|  81|[0x80004414]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3d4499ff58c3 and rm_val == 0  #nosat<br>                                                                                            |[0x800009e0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009e4]:csrrs a7, fflags, zero<br> [0x800009e8]:fsd ft11, 1280(a5)<br> [0x800009ec]:sw a7, 1284(a5)<br> |
|  82|[0x80004424]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and rm_val == 4  #nosat<br>                                                                                            |[0x800009fc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:fsd ft11, 1296(a5)<br> [0x80000a08]:sw a7, 1300(a5)<br> |
|  83|[0x80004434]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and rm_val == 3  #nosat<br>                                                                                            |[0x80000a18]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a1c]:csrrs a7, fflags, zero<br> [0x80000a20]:fsd ft11, 1312(a5)<br> [0x80000a24]:sw a7, 1316(a5)<br> |
|  84|[0x80004444]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and rm_val == 2  #nosat<br>                                                                                            |[0x80000a34]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:fsd ft11, 1328(a5)<br> [0x80000a40]:sw a7, 1332(a5)<br> |
|  85|[0x80004454]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and rm_val == 1  #nosat<br>                                                                                            |[0x80000a50]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a54]:csrrs a7, fflags, zero<br> [0x80000a58]:fsd ft11, 1344(a5)<br> [0x80000a5c]:sw a7, 1348(a5)<br> |
|  86|[0x80004464]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x36a63c245f557 and rm_val == 0  #nosat<br>                                                                                            |[0x80000a6c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a70]:csrrs a7, fflags, zero<br> [0x80000a74]:fsd ft11, 1360(a5)<br> [0x80000a78]:sw a7, 1364(a5)<br> |
|  87|[0x80004474]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and rm_val == 4  #nosat<br>                                                                                            |[0x80000a88]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a8c]:csrrs a7, fflags, zero<br> [0x80000a90]:fsd ft11, 1376(a5)<br> [0x80000a94]:sw a7, 1380(a5)<br> |
|  88|[0x80004484]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and rm_val == 3  #nosat<br>                                                                                            |[0x80000aa4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1392(a5)<br> [0x80000ab0]:sw a7, 1396(a5)<br> |
|  89|[0x80004494]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and rm_val == 2  #nosat<br>                                                                                            |[0x80000ac0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ac4]:csrrs a7, fflags, zero<br> [0x80000ac8]:fsd ft11, 1408(a5)<br> [0x80000acc]:sw a7, 1412(a5)<br> |
|  90|[0x800044a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and rm_val == 1  #nosat<br>                                                                                            |[0x80000adc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:fsd ft11, 1424(a5)<br> [0x80000ae8]:sw a7, 1428(a5)<br> |
|  91|[0x800044b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8fa703a4078c and rm_val == 0  #nosat<br>                                                                                            |[0x80000af8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000afc]:csrrs a7, fflags, zero<br> [0x80000b00]:fsd ft11, 1440(a5)<br> [0x80000b04]:sw a7, 1444(a5)<br> |
|  92|[0x800044c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and rm_val == 4  #nosat<br>                                                                                            |[0x80000b14]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b18]:csrrs a7, fflags, zero<br> [0x80000b1c]:fsd ft11, 1456(a5)<br> [0x80000b20]:sw a7, 1460(a5)<br> |
|  93|[0x800044d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and rm_val == 3  #nosat<br>                                                                                            |[0x80000b30]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b34]:csrrs a7, fflags, zero<br> [0x80000b38]:fsd ft11, 1472(a5)<br> [0x80000b3c]:sw a7, 1476(a5)<br> |
|  94|[0x800044e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and rm_val == 2  #nosat<br>                                                                                            |[0x80000b4c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b50]:csrrs a7, fflags, zero<br> [0x80000b54]:fsd ft11, 1488(a5)<br> [0x80000b58]:sw a7, 1492(a5)<br> |
|  95|[0x800044f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and rm_val == 1  #nosat<br>                                                                                            |[0x80000b68]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b6c]:csrrs a7, fflags, zero<br> [0x80000b70]:fsd ft11, 1504(a5)<br> [0x80000b74]:sw a7, 1508(a5)<br> |
|  96|[0x80004504]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdf7523fde6c5d and rm_val == 0  #nosat<br>                                                                                            |[0x80000b84]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1520(a5)<br> [0x80000b90]:sw a7, 1524(a5)<br> |
|  97|[0x80004514]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and rm_val == 4  #nosat<br>                                                                                            |[0x80000ba0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ba4]:csrrs a7, fflags, zero<br> [0x80000ba8]:fsd ft11, 1536(a5)<br> [0x80000bac]:sw a7, 1540(a5)<br> |
|  98|[0x80004524]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and rm_val == 3  #nosat<br>                                                                                            |[0x80000bbc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bc0]:csrrs a7, fflags, zero<br> [0x80000bc4]:fsd ft11, 1552(a5)<br> [0x80000bc8]:sw a7, 1556(a5)<br> |
|  99|[0x80004534]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and rm_val == 2  #nosat<br>                                                                                            |[0x80000bd8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bdc]:csrrs a7, fflags, zero<br> [0x80000be0]:fsd ft11, 1568(a5)<br> [0x80000be4]:sw a7, 1572(a5)<br> |
| 100|[0x80004544]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and rm_val == 1  #nosat<br>                                                                                            |[0x80000bf4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bf8]:csrrs a7, fflags, zero<br> [0x80000bfc]:fsd ft11, 1584(a5)<br> [0x80000c00]:sw a7, 1588(a5)<br> |
| 101|[0x80004554]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7166677e49c3c and rm_val == 0  #nosat<br>                                                                                            |[0x80000c10]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c14]:csrrs a7, fflags, zero<br> [0x80000c18]:fsd ft11, 1600(a5)<br> [0x80000c1c]:sw a7, 1604(a5)<br> |
| 102|[0x80004564]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and rm_val == 4  #nosat<br>                                                                                            |[0x80000c2c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c30]:csrrs a7, fflags, zero<br> [0x80000c34]:fsd ft11, 1616(a5)<br> [0x80000c38]:sw a7, 1620(a5)<br> |
| 103|[0x80004574]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and rm_val == 3  #nosat<br>                                                                                            |[0x80000c48]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1632(a5)<br> [0x80000c54]:sw a7, 1636(a5)<br> |
| 104|[0x80004584]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and rm_val == 2  #nosat<br>                                                                                            |[0x80000c64]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c68]:csrrs a7, fflags, zero<br> [0x80000c6c]:fsd ft11, 1648(a5)<br> [0x80000c70]:sw a7, 1652(a5)<br> |
| 105|[0x80004594]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and rm_val == 1  #nosat<br>                                                                                            |[0x80000c80]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c84]:csrrs a7, fflags, zero<br> [0x80000c88]:fsd ft11, 1664(a5)<br> [0x80000c8c]:sw a7, 1668(a5)<br> |
| 106|[0x800045a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xef2a4f7c7db7f and rm_val == 0  #nosat<br>                                                                                            |[0x80000c9c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ca0]:csrrs a7, fflags, zero<br> [0x80000ca4]:fsd ft11, 1680(a5)<br> [0x80000ca8]:sw a7, 1684(a5)<br> |
| 107|[0x8000430c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and rm_val == 3  #nosat<br>                                                                                            |[0x80000f10]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f14]:csrrs a7, fflags, zero<br> [0x80000f18]:fsd ft11, 0(a5)<br> [0x80000f1c]:sw a7, 4(a5)<br>       |
| 108|[0x8000431c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and rm_val == 2  #nosat<br>                                                                                            |[0x80000f2c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f30]:csrrs a7, fflags, zero<br> [0x80000f34]:fsd ft11, 16(a5)<br> [0x80000f38]:sw a7, 20(a5)<br>     |
| 109|[0x8000432c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and rm_val == 1  #nosat<br>                                                                                            |[0x80000f48]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 32(a5)<br> [0x80000f54]:sw a7, 36(a5)<br>     |
| 110|[0x8000433c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc01045c2cd787 and rm_val == 0  #nosat<br>                                                                                            |[0x80000f64]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs a7, fflags, zero<br> [0x80000f6c]:fsd ft11, 48(a5)<br> [0x80000f70]:sw a7, 52(a5)<br>     |
| 111|[0x8000434c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and rm_val == 4  #nosat<br>                                                                                            |[0x80000f80]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:fsd ft11, 64(a5)<br> [0x80000f8c]:sw a7, 68(a5)<br>     |
| 112|[0x8000435c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and rm_val == 3  #nosat<br>                                                                                            |[0x80000f9c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fa0]:csrrs a7, fflags, zero<br> [0x80000fa4]:fsd ft11, 80(a5)<br> [0x80000fa8]:sw a7, 84(a5)<br>     |
| 113|[0x8000436c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and rm_val == 2  #nosat<br>                                                                                            |[0x80000fb8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fbc]:csrrs a7, fflags, zero<br> [0x80000fc0]:fsd ft11, 96(a5)<br> [0x80000fc4]:sw a7, 100(a5)<br>    |
| 114|[0x8000437c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and rm_val == 1  #nosat<br>                                                                                            |[0x80000fd4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:fsd ft11, 112(a5)<br> [0x80000fe0]:sw a7, 116(a5)<br>   |
| 115|[0x8000438c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdd5b61587fd27 and rm_val == 0  #nosat<br>                                                                                            |[0x80000ff0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ff4]:csrrs a7, fflags, zero<br> [0x80000ff8]:fsd ft11, 128(a5)<br> [0x80000ffc]:sw a7, 132(a5)<br>   |
| 116|[0x8000439c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and rm_val == 4  #nosat<br>                                                                                            |[0x8000100c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001010]:csrrs a7, fflags, zero<br> [0x80001014]:fsd ft11, 144(a5)<br> [0x80001018]:sw a7, 148(a5)<br>   |
| 117|[0x800043ac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and rm_val == 3  #nosat<br>                                                                                            |[0x80001028]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 160(a5)<br> [0x80001034]:sw a7, 164(a5)<br>   |
| 118|[0x800043bc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and rm_val == 2  #nosat<br>                                                                                            |[0x80001044]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001048]:csrrs a7, fflags, zero<br> [0x8000104c]:fsd ft11, 176(a5)<br> [0x80001050]:sw a7, 180(a5)<br>   |
| 119|[0x800043cc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and rm_val == 1  #nosat<br>                                                                                            |[0x80001060]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001064]:csrrs a7, fflags, zero<br> [0x80001068]:fsd ft11, 192(a5)<br> [0x8000106c]:sw a7, 196(a5)<br>   |
| 120|[0x800043dc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc0659af8369fd and rm_val == 0  #nosat<br>                                                                                            |[0x8000107c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001080]:csrrs a7, fflags, zero<br> [0x80001084]:fsd ft11, 208(a5)<br> [0x80001088]:sw a7, 212(a5)<br>   |
| 121|[0x800043ec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and rm_val == 4  #nosat<br>                                                                                            |[0x80001098]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000109c]:csrrs a7, fflags, zero<br> [0x800010a0]:fsd ft11, 224(a5)<br> [0x800010a4]:sw a7, 228(a5)<br>   |
| 122|[0x800043fc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and rm_val == 3  #nosat<br>                                                                                            |[0x800010b4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010b8]:csrrs a7, fflags, zero<br> [0x800010bc]:fsd ft11, 240(a5)<br> [0x800010c0]:sw a7, 244(a5)<br>   |
| 123|[0x8000440c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and rm_val == 2  #nosat<br>                                                                                            |[0x800010d0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010d4]:csrrs a7, fflags, zero<br> [0x800010d8]:fsd ft11, 256(a5)<br> [0x800010dc]:sw a7, 260(a5)<br>   |
| 124|[0x8000441c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and rm_val == 1  #nosat<br>                                                                                            |[0x800010ec]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010f0]:csrrs a7, fflags, zero<br> [0x800010f4]:fsd ft11, 272(a5)<br> [0x800010f8]:sw a7, 276(a5)<br>   |
| 125|[0x8000442c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xdbcde43895c3f and rm_val == 0  #nosat<br>                                                                                            |[0x80001108]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000110c]:csrrs a7, fflags, zero<br> [0x80001110]:fsd ft11, 288(a5)<br> [0x80001114]:sw a7, 292(a5)<br>   |
| 126|[0x8000443c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and rm_val == 4  #nosat<br>                                                                                            |[0x80001124]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001128]:csrrs a7, fflags, zero<br> [0x8000112c]:fsd ft11, 304(a5)<br> [0x80001130]:sw a7, 308(a5)<br>   |
| 127|[0x8000444c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and rm_val == 3  #nosat<br>                                                                                            |[0x80001140]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001144]:csrrs a7, fflags, zero<br> [0x80001148]:fsd ft11, 320(a5)<br> [0x8000114c]:sw a7, 324(a5)<br>   |
| 128|[0x8000445c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and rm_val == 2  #nosat<br>                                                                                            |[0x8000115c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001160]:csrrs a7, fflags, zero<br> [0x80001164]:fsd ft11, 336(a5)<br> [0x80001168]:sw a7, 340(a5)<br>   |
| 129|[0x8000446c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and rm_val == 1  #nosat<br>                                                                                            |[0x80001178]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000117c]:csrrs a7, fflags, zero<br> [0x80001180]:fsd ft11, 352(a5)<br> [0x80001184]:sw a7, 356(a5)<br>   |
| 130|[0x8000447c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb9876f8130c3 and rm_val == 0  #nosat<br>                                                                                            |[0x80001194]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001198]:csrrs a7, fflags, zero<br> [0x8000119c]:fsd ft11, 368(a5)<br> [0x800011a0]:sw a7, 372(a5)<br>   |
| 131|[0x8000448c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and rm_val == 4  #nosat<br>                                                                                            |[0x800011b0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 384(a5)<br> [0x800011bc]:sw a7, 388(a5)<br>   |
| 132|[0x8000449c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and rm_val == 3  #nosat<br>                                                                                            |[0x800011cc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011d0]:csrrs a7, fflags, zero<br> [0x800011d4]:fsd ft11, 400(a5)<br> [0x800011d8]:sw a7, 404(a5)<br>   |
| 133|[0x800044ac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and rm_val == 2  #nosat<br>                                                                                            |[0x800011e8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011ec]:csrrs a7, fflags, zero<br> [0x800011f0]:fsd ft11, 416(a5)<br> [0x800011f4]:sw a7, 420(a5)<br>   |
| 134|[0x800044bc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and rm_val == 1  #nosat<br>                                                                                            |[0x80001204]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001208]:csrrs a7, fflags, zero<br> [0x8000120c]:fsd ft11, 432(a5)<br> [0x80001210]:sw a7, 436(a5)<br>   |
| 135|[0x800044cc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe0d828b86622a and rm_val == 0  #nosat<br>                                                                                            |[0x80001220]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001224]:csrrs a7, fflags, zero<br> [0x80001228]:fsd ft11, 448(a5)<br> [0x8000122c]:sw a7, 452(a5)<br>   |
| 136|[0x800044dc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and rm_val == 4  #nosat<br>                                                                                            |[0x8000123c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001240]:csrrs a7, fflags, zero<br> [0x80001244]:fsd ft11, 464(a5)<br> [0x80001248]:sw a7, 468(a5)<br>   |
| 137|[0x800044ec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and rm_val == 3  #nosat<br>                                                                                            |[0x80001258]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000125c]:csrrs a7, fflags, zero<br> [0x80001260]:fsd ft11, 480(a5)<br> [0x80001264]:sw a7, 484(a5)<br>   |
| 138|[0x800044fc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and rm_val == 2  #nosat<br>                                                                                            |[0x80001274]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001278]:csrrs a7, fflags, zero<br> [0x8000127c]:fsd ft11, 496(a5)<br> [0x80001280]:sw a7, 500(a5)<br>   |
| 139|[0x8000450c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and rm_val == 1  #nosat<br>                                                                                            |[0x80001290]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 512(a5)<br> [0x8000129c]:sw a7, 516(a5)<br>   |
| 140|[0x8000451c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xa0e7ad32453df and rm_val == 0  #nosat<br>                                                                                            |[0x800012ac]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012b0]:csrrs a7, fflags, zero<br> [0x800012b4]:fsd ft11, 528(a5)<br> [0x800012b8]:sw a7, 532(a5)<br>   |
| 141|[0x8000452c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and rm_val == 4  #nosat<br>                                                                                            |[0x800012c8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012cc]:csrrs a7, fflags, zero<br> [0x800012d0]:fsd ft11, 544(a5)<br> [0x800012d4]:sw a7, 548(a5)<br>   |
| 142|[0x8000453c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and rm_val == 3  #nosat<br>                                                                                            |[0x800012e4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012e8]:csrrs a7, fflags, zero<br> [0x800012ec]:fsd ft11, 560(a5)<br> [0x800012f0]:sw a7, 564(a5)<br>   |
| 143|[0x8000454c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and rm_val == 2  #nosat<br>                                                                                            |[0x80001300]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001304]:csrrs a7, fflags, zero<br> [0x80001308]:fsd ft11, 576(a5)<br> [0x8000130c]:sw a7, 580(a5)<br>   |
| 144|[0x8000455c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and rm_val == 1  #nosat<br>                                                                                            |[0x8000131c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001320]:csrrs a7, fflags, zero<br> [0x80001324]:fsd ft11, 592(a5)<br> [0x80001328]:sw a7, 596(a5)<br>   |
| 145|[0x8000456c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcd87e65450c45 and rm_val == 0  #nosat<br>                                                                                            |[0x80001338]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000133c]:csrrs a7, fflags, zero<br> [0x80001340]:fsd ft11, 608(a5)<br> [0x80001344]:sw a7, 612(a5)<br>   |
| 146|[0x8000457c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and rm_val == 4  #nosat<br>                                                                                            |[0x80001354]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001358]:csrrs a7, fflags, zero<br> [0x8000135c]:fsd ft11, 624(a5)<br> [0x80001360]:sw a7, 628(a5)<br>   |
| 147|[0x8000458c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and rm_val == 3  #nosat<br>                                                                                            |[0x80001370]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 640(a5)<br> [0x8000137c]:sw a7, 644(a5)<br>   |
| 148|[0x8000459c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and rm_val == 2  #nosat<br>                                                                                            |[0x8000138c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001390]:csrrs a7, fflags, zero<br> [0x80001394]:fsd ft11, 656(a5)<br> [0x80001398]:sw a7, 660(a5)<br>   |
| 149|[0x800045ac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd481499755d4b and rm_val == 1  #nosat<br>                                                                                            |[0x800013a8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800013ac]:csrrs a7, fflags, zero<br> [0x800013b0]:fsd ft11, 672(a5)<br> [0x800013b4]:sw a7, 676(a5)<br>   |
