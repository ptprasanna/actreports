
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80002630')]      |
| SIG_REGION                | [('0x80005710', '0x800061a0', '676 words')]      |
| COV_LABELS                | fsub_b7      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsub/riscof_work/fsub_b7-01.S/ref.S    |
| Total Number of coverpoints| 444     |
| Total Coverpoints Hit     | 377      |
| Total Signature Updates   | 275      |
| STAT1                     | 275      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001aa8]:fsub.d ft11, ft10, ft9, dyn
[0x80001aac]:csrrs a7, fflags, zero
[0x80001ab0]:fsd ft11, 1696(a5)
[0x80001ab4]:sw a7, 1700(a5)
[0x80001ab8]:fld ft10, 1712(a6)
[0x80001abc]:fld ft9, 1720(a6)
[0x80001ac0]:csrrwi zero, frm, 3

[0x80001ac4]:fsub.d ft11, ft10, ft9, dyn
[0x80001ac8]:csrrs a7, fflags, zero
[0x80001acc]:fsd ft11, 1712(a5)
[0x80001ad0]:sw a7, 1716(a5)
[0x80001ad4]:fld ft10, 1728(a6)
[0x80001ad8]:fld ft9, 1736(a6)
[0x80001adc]:csrrwi zero, frm, 3

[0x80001ae0]:fsub.d ft11, ft10, ft9, dyn
[0x80001ae4]:csrrs a7, fflags, zero
[0x80001ae8]:fsd ft11, 1728(a5)
[0x80001aec]:sw a7, 1732(a5)
[0x80001af0]:fld ft10, 1744(a6)
[0x80001af4]:fld ft9, 1752(a6)
[0x80001af8]:csrrwi zero, frm, 3

[0x80001afc]:fsub.d ft11, ft10, ft9, dyn
[0x80001b00]:csrrs a7, fflags, zero
[0x80001b04]:fsd ft11, 1744(a5)
[0x80001b08]:sw a7, 1748(a5)
[0x80001b0c]:fld ft10, 1760(a6)
[0x80001b10]:fld ft9, 1768(a6)
[0x80001b14]:csrrwi zero, frm, 3

[0x80001b18]:fsub.d ft11, ft10, ft9, dyn
[0x80001b1c]:csrrs a7, fflags, zero
[0x80001b20]:fsd ft11, 1760(a5)
[0x80001b24]:sw a7, 1764(a5)
[0x80001b28]:fld ft10, 1776(a6)
[0x80001b2c]:fld ft9, 1784(a6)
[0x80001b30]:csrrwi zero, frm, 3

[0x80001b34]:fsub.d ft11, ft10, ft9, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1776(a5)
[0x80001b40]:sw a7, 1780(a5)
[0x80001b44]:fld ft10, 1792(a6)
[0x80001b48]:fld ft9, 1800(a6)
[0x80001b4c]:csrrwi zero, frm, 3

[0x80001b50]:fsub.d ft11, ft10, ft9, dyn
[0x80001b54]:csrrs a7, fflags, zero
[0x80001b58]:fsd ft11, 1792(a5)
[0x80001b5c]:sw a7, 1796(a5)
[0x80001b60]:fld ft10, 1808(a6)
[0x80001b64]:fld ft9, 1816(a6)
[0x80001b68]:csrrwi zero, frm, 3

[0x80001b6c]:fsub.d ft11, ft10, ft9, dyn
[0x80001b70]:csrrs a7, fflags, zero
[0x80001b74]:fsd ft11, 1808(a5)
[0x80001b78]:sw a7, 1812(a5)
[0x80001b7c]:fld ft10, 1824(a6)
[0x80001b80]:fld ft9, 1832(a6)
[0x80001b84]:csrrwi zero, frm, 3

[0x80001b88]:fsub.d ft11, ft10, ft9, dyn
[0x80001b8c]:csrrs a7, fflags, zero
[0x80001b90]:fsd ft11, 1824(a5)
[0x80001b94]:sw a7, 1828(a5)
[0x80001b98]:fld ft10, 1840(a6)
[0x80001b9c]:fld ft9, 1848(a6)
[0x80001ba0]:csrrwi zero, frm, 3

[0x80001ba4]:fsub.d ft11, ft10, ft9, dyn
[0x80001ba8]:csrrs a7, fflags, zero
[0x80001bac]:fsd ft11, 1840(a5)
[0x80001bb0]:sw a7, 1844(a5)
[0x80001bb4]:fld ft10, 1856(a6)
[0x80001bb8]:fld ft9, 1864(a6)
[0x80001bbc]:csrrwi zero, frm, 3

[0x80001bc0]:fsub.d ft11, ft10, ft9, dyn
[0x80001bc4]:csrrs a7, fflags, zero
[0x80001bc8]:fsd ft11, 1856(a5)
[0x80001bcc]:sw a7, 1860(a5)
[0x80001bd0]:fld ft10, 1872(a6)
[0x80001bd4]:fld ft9, 1880(a6)
[0x80001bd8]:csrrwi zero, frm, 3

[0x80001bdc]:fsub.d ft11, ft10, ft9, dyn
[0x80001be0]:csrrs a7, fflags, zero
[0x80001be4]:fsd ft11, 1872(a5)
[0x80001be8]:sw a7, 1876(a5)
[0x80001bec]:fld ft10, 1888(a6)
[0x80001bf0]:fld ft9, 1896(a6)
[0x80001bf4]:csrrwi zero, frm, 3

[0x80001bf8]:fsub.d ft11, ft10, ft9, dyn
[0x80001bfc]:csrrs a7, fflags, zero
[0x80001c00]:fsd ft11, 1888(a5)
[0x80001c04]:sw a7, 1892(a5)
[0x80001c08]:fld ft10, 1904(a6)
[0x80001c0c]:fld ft9, 1912(a6)
[0x80001c10]:csrrwi zero, frm, 3

[0x80001c14]:fsub.d ft11, ft10, ft9, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1904(a5)
[0x80001c20]:sw a7, 1908(a5)
[0x80001c24]:fld ft10, 1920(a6)
[0x80001c28]:fld ft9, 1928(a6)
[0x80001c2c]:csrrwi zero, frm, 3

[0x80001c30]:fsub.d ft11, ft10, ft9, dyn
[0x80001c34]:csrrs a7, fflags, zero
[0x80001c38]:fsd ft11, 1920(a5)
[0x80001c3c]:sw a7, 1924(a5)
[0x80001c40]:fld ft10, 1936(a6)
[0x80001c44]:fld ft9, 1944(a6)
[0x80001c48]:csrrwi zero, frm, 3

[0x80001c4c]:fsub.d ft11, ft10, ft9, dyn
[0x80001c50]:csrrs a7, fflags, zero
[0x80001c54]:fsd ft11, 1936(a5)
[0x80001c58]:sw a7, 1940(a5)
[0x80001c5c]:fld ft10, 1952(a6)
[0x80001c60]:fld ft9, 1960(a6)
[0x80001c64]:csrrwi zero, frm, 3

[0x80001c68]:fsub.d ft11, ft10, ft9, dyn
[0x80001c6c]:csrrs a7, fflags, zero
[0x80001c70]:fsd ft11, 1952(a5)
[0x80001c74]:sw a7, 1956(a5)
[0x80001c78]:fld ft10, 1968(a6)
[0x80001c7c]:fld ft9, 1976(a6)
[0x80001c80]:csrrwi zero, frm, 3

[0x80001c84]:fsub.d ft11, ft10, ft9, dyn
[0x80001c88]:csrrs a7, fflags, zero
[0x80001c8c]:fsd ft11, 1968(a5)
[0x80001c90]:sw a7, 1972(a5)
[0x80001c94]:fld ft10, 1984(a6)
[0x80001c98]:fld ft9, 1992(a6)
[0x80001c9c]:csrrwi zero, frm, 3

[0x80001ca0]:fsub.d ft11, ft10, ft9, dyn
[0x80001ca4]:csrrs a7, fflags, zero
[0x80001ca8]:fsd ft11, 1984(a5)
[0x80001cac]:sw a7, 1988(a5)
[0x80001cb0]:fld ft10, 2000(a6)
[0x80001cb4]:fld ft9, 2008(a6)
[0x80001cb8]:csrrwi zero, frm, 3

[0x80001cbc]:fsub.d ft11, ft10, ft9, dyn
[0x80001cc0]:csrrs a7, fflags, zero
[0x80001cc4]:fsd ft11, 2000(a5)
[0x80001cc8]:sw a7, 2004(a5)
[0x80001ccc]:fld ft10, 2016(a6)
[0x80001cd0]:fld ft9, 2024(a6)
[0x80001cd4]:csrrwi zero, frm, 3

[0x80001cd8]:fsub.d ft11, ft10, ft9, dyn
[0x80001cdc]:csrrs a7, fflags, zero
[0x80001ce0]:fsd ft11, 2016(a5)
[0x80001ce4]:sw a7, 2020(a5)
[0x80001ce8]:addi a6, a6, 2032
[0x80001cec]:auipc a5, 4
[0x80001cf0]:addi a5, a5, 532
[0x80001cf4]:fld ft10, 0(a6)
[0x80001cf8]:fld ft9, 8(a6)
[0x80001cfc]:csrrwi zero, frm, 3

[0x80002198]:fsub.d ft11, ft10, ft9, dyn
[0x8000219c]:csrrs a7, fflags, zero
[0x800021a0]:fsd ft11, 672(a5)
[0x800021a4]:sw a7, 676(a5)
[0x800021a8]:fld ft10, 688(a6)
[0x800021ac]:fld ft9, 696(a6)
[0x800021b0]:csrrwi zero, frm, 3

[0x800021b4]:fsub.d ft11, ft10, ft9, dyn
[0x800021b8]:csrrs a7, fflags, zero
[0x800021bc]:fsd ft11, 688(a5)
[0x800021c0]:sw a7, 692(a5)
[0x800021c4]:fld ft10, 704(a6)
[0x800021c8]:fld ft9, 712(a6)
[0x800021cc]:csrrwi zero, frm, 3

[0x800021d0]:fsub.d ft11, ft10, ft9, dyn
[0x800021d4]:csrrs a7, fflags, zero
[0x800021d8]:fsd ft11, 704(a5)
[0x800021dc]:sw a7, 708(a5)
[0x800021e0]:fld ft10, 720(a6)
[0x800021e4]:fld ft9, 728(a6)
[0x800021e8]:csrrwi zero, frm, 3

[0x800021ec]:fsub.d ft11, ft10, ft9, dyn
[0x800021f0]:csrrs a7, fflags, zero
[0x800021f4]:fsd ft11, 720(a5)
[0x800021f8]:sw a7, 724(a5)
[0x800021fc]:fld ft10, 736(a6)
[0x80002200]:fld ft9, 744(a6)
[0x80002204]:csrrwi zero, frm, 3

[0x80002208]:fsub.d ft11, ft10, ft9, dyn
[0x8000220c]:csrrs a7, fflags, zero
[0x80002210]:fsd ft11, 736(a5)
[0x80002214]:sw a7, 740(a5)
[0x80002218]:fld ft10, 752(a6)
[0x8000221c]:fld ft9, 760(a6)
[0x80002220]:csrrwi zero, frm, 3

[0x80002224]:fsub.d ft11, ft10, ft9, dyn
[0x80002228]:csrrs a7, fflags, zero
[0x8000222c]:fsd ft11, 752(a5)
[0x80002230]:sw a7, 756(a5)
[0x80002234]:fld ft10, 768(a6)
[0x80002238]:fld ft9, 776(a6)
[0x8000223c]:csrrwi zero, frm, 3

[0x80002240]:fsub.d ft11, ft10, ft9, dyn
[0x80002244]:csrrs a7, fflags, zero
[0x80002248]:fsd ft11, 768(a5)
[0x8000224c]:sw a7, 772(a5)
[0x80002250]:fld ft10, 784(a6)
[0x80002254]:fld ft9, 792(a6)
[0x80002258]:csrrwi zero, frm, 3

[0x8000225c]:fsub.d ft11, ft10, ft9, dyn
[0x80002260]:csrrs a7, fflags, zero
[0x80002264]:fsd ft11, 784(a5)
[0x80002268]:sw a7, 788(a5)
[0x8000226c]:fld ft10, 800(a6)
[0x80002270]:fld ft9, 808(a6)
[0x80002274]:csrrwi zero, frm, 3

[0x80002278]:fsub.d ft11, ft10, ft9, dyn
[0x8000227c]:csrrs a7, fflags, zero
[0x80002280]:fsd ft11, 800(a5)
[0x80002284]:sw a7, 804(a5)
[0x80002288]:fld ft10, 816(a6)
[0x8000228c]:fld ft9, 824(a6)
[0x80002290]:csrrwi zero, frm, 3

[0x80002294]:fsub.d ft11, ft10, ft9, dyn
[0x80002298]:csrrs a7, fflags, zero
[0x8000229c]:fsd ft11, 816(a5)
[0x800022a0]:sw a7, 820(a5)
[0x800022a4]:fld ft10, 832(a6)
[0x800022a8]:fld ft9, 840(a6)
[0x800022ac]:csrrwi zero, frm, 3

[0x800022b0]:fsub.d ft11, ft10, ft9, dyn
[0x800022b4]:csrrs a7, fflags, zero
[0x800022b8]:fsd ft11, 832(a5)
[0x800022bc]:sw a7, 836(a5)
[0x800022c0]:fld ft10, 848(a6)
[0x800022c4]:fld ft9, 856(a6)
[0x800022c8]:csrrwi zero, frm, 3

[0x800022cc]:fsub.d ft11, ft10, ft9, dyn
[0x800022d0]:csrrs a7, fflags, zero
[0x800022d4]:fsd ft11, 848(a5)
[0x800022d8]:sw a7, 852(a5)
[0x800022dc]:fld ft10, 864(a6)
[0x800022e0]:fld ft9, 872(a6)
[0x800022e4]:csrrwi zero, frm, 3

[0x800022e8]:fsub.d ft11, ft10, ft9, dyn
[0x800022ec]:csrrs a7, fflags, zero
[0x800022f0]:fsd ft11, 864(a5)
[0x800022f4]:sw a7, 868(a5)
[0x800022f8]:fld ft10, 880(a6)
[0x800022fc]:fld ft9, 888(a6)
[0x80002300]:csrrwi zero, frm, 3

[0x80002304]:fsub.d ft11, ft10, ft9, dyn
[0x80002308]:csrrs a7, fflags, zero
[0x8000230c]:fsd ft11, 880(a5)
[0x80002310]:sw a7, 884(a5)
[0x80002314]:fld ft10, 896(a6)
[0x80002318]:fld ft9, 904(a6)
[0x8000231c]:csrrwi zero, frm, 3

[0x80002320]:fsub.d ft11, ft10, ft9, dyn
[0x80002324]:csrrs a7, fflags, zero
[0x80002328]:fsd ft11, 896(a5)
[0x8000232c]:sw a7, 900(a5)
[0x80002330]:fld ft10, 912(a6)
[0x80002334]:fld ft9, 920(a6)
[0x80002338]:csrrwi zero, frm, 3

[0x8000233c]:fsub.d ft11, ft10, ft9, dyn
[0x80002340]:csrrs a7, fflags, zero
[0x80002344]:fsd ft11, 912(a5)
[0x80002348]:sw a7, 916(a5)
[0x8000234c]:fld ft10, 928(a6)
[0x80002350]:fld ft9, 936(a6)
[0x80002354]:csrrwi zero, frm, 3

[0x80002358]:fsub.d ft11, ft10, ft9, dyn
[0x8000235c]:csrrs a7, fflags, zero
[0x80002360]:fsd ft11, 928(a5)
[0x80002364]:sw a7, 932(a5)
[0x80002368]:fld ft10, 944(a6)
[0x8000236c]:fld ft9, 952(a6)
[0x80002370]:csrrwi zero, frm, 3

[0x80002374]:fsub.d ft11, ft10, ft9, dyn
[0x80002378]:csrrs a7, fflags, zero
[0x8000237c]:fsd ft11, 944(a5)
[0x80002380]:sw a7, 948(a5)
[0x80002384]:fld ft10, 960(a6)
[0x80002388]:fld ft9, 968(a6)
[0x8000238c]:csrrwi zero, frm, 3

[0x80002390]:fsub.d ft11, ft10, ft9, dyn
[0x80002394]:csrrs a7, fflags, zero
[0x80002398]:fsd ft11, 960(a5)
[0x8000239c]:sw a7, 964(a5)
[0x800023a0]:fld ft10, 976(a6)
[0x800023a4]:fld ft9, 984(a6)
[0x800023a8]:csrrwi zero, frm, 3

[0x800023ac]:fsub.d ft11, ft10, ft9, dyn
[0x800023b0]:csrrs a7, fflags, zero
[0x800023b4]:fsd ft11, 976(a5)
[0x800023b8]:sw a7, 980(a5)
[0x800023bc]:fld ft10, 992(a6)
[0x800023c0]:fld ft9, 1000(a6)
[0x800023c4]:csrrwi zero, frm, 3

[0x800023c8]:fsub.d ft11, ft10, ft9, dyn
[0x800023cc]:csrrs a7, fflags, zero
[0x800023d0]:fsd ft11, 992(a5)
[0x800023d4]:sw a7, 996(a5)
[0x800023d8]:fld ft10, 1008(a6)
[0x800023dc]:fld ft9, 1016(a6)
[0x800023e0]:csrrwi zero, frm, 3

[0x800023e4]:fsub.d ft11, ft10, ft9, dyn
[0x800023e8]:csrrs a7, fflags, zero
[0x800023ec]:fsd ft11, 1008(a5)
[0x800023f0]:sw a7, 1012(a5)
[0x800023f4]:fld ft10, 1024(a6)
[0x800023f8]:fld ft9, 1032(a6)
[0x800023fc]:csrrwi zero, frm, 3

[0x80002400]:fsub.d ft11, ft10, ft9, dyn
[0x80002404]:csrrs a7, fflags, zero
[0x80002408]:fsd ft11, 1024(a5)
[0x8000240c]:sw a7, 1028(a5)
[0x80002410]:fld ft10, 1040(a6)
[0x80002414]:fld ft9, 1048(a6)
[0x80002418]:csrrwi zero, frm, 3

[0x8000241c]:fsub.d ft11, ft10, ft9, dyn
[0x80002420]:csrrs a7, fflags, zero
[0x80002424]:fsd ft11, 1040(a5)
[0x80002428]:sw a7, 1044(a5)
[0x8000242c]:fld ft10, 1056(a6)
[0x80002430]:fld ft9, 1064(a6)
[0x80002434]:csrrwi zero, frm, 3

[0x80002438]:fsub.d ft11, ft10, ft9, dyn
[0x8000243c]:csrrs a7, fflags, zero
[0x80002440]:fsd ft11, 1056(a5)
[0x80002444]:sw a7, 1060(a5)
[0x80002448]:fld ft10, 1072(a6)
[0x8000244c]:fld ft9, 1080(a6)
[0x80002450]:csrrwi zero, frm, 3

[0x80002454]:fsub.d ft11, ft10, ft9, dyn
[0x80002458]:csrrs a7, fflags, zero
[0x8000245c]:fsd ft11, 1072(a5)
[0x80002460]:sw a7, 1076(a5)
[0x80002464]:fld ft10, 1088(a6)
[0x80002468]:fld ft9, 1096(a6)
[0x8000246c]:csrrwi zero, frm, 3

[0x80002470]:fsub.d ft11, ft10, ft9, dyn
[0x80002474]:csrrs a7, fflags, zero
[0x80002478]:fsd ft11, 1088(a5)
[0x8000247c]:sw a7, 1092(a5)
[0x80002480]:fld ft10, 1104(a6)
[0x80002484]:fld ft9, 1112(a6)
[0x80002488]:csrrwi zero, frm, 3

[0x8000248c]:fsub.d ft11, ft10, ft9, dyn
[0x80002490]:csrrs a7, fflags, zero
[0x80002494]:fsd ft11, 1104(a5)
[0x80002498]:sw a7, 1108(a5)
[0x8000249c]:fld ft10, 1120(a6)
[0x800024a0]:fld ft9, 1128(a6)
[0x800024a4]:csrrwi zero, frm, 3

[0x800024a8]:fsub.d ft11, ft10, ft9, dyn
[0x800024ac]:csrrs a7, fflags, zero
[0x800024b0]:fsd ft11, 1120(a5)
[0x800024b4]:sw a7, 1124(a5)
[0x800024b8]:fld ft10, 1136(a6)
[0x800024bc]:fld ft9, 1144(a6)
[0x800024c0]:csrrwi zero, frm, 3

[0x800024c4]:fsub.d ft11, ft10, ft9, dyn
[0x800024c8]:csrrs a7, fflags, zero
[0x800024cc]:fsd ft11, 1136(a5)
[0x800024d0]:sw a7, 1140(a5)
[0x800024d4]:fld ft10, 1152(a6)
[0x800024d8]:fld ft9, 1160(a6)
[0x800024dc]:csrrwi zero, frm, 3

[0x800024e0]:fsub.d ft11, ft10, ft9, dyn
[0x800024e4]:csrrs a7, fflags, zero
[0x800024e8]:fsd ft11, 1152(a5)
[0x800024ec]:sw a7, 1156(a5)
[0x800024f0]:fld ft10, 1168(a6)
[0x800024f4]:fld ft9, 1176(a6)
[0x800024f8]:csrrwi zero, frm, 3

[0x800024fc]:fsub.d ft11, ft10, ft9, dyn
[0x80002500]:csrrs a7, fflags, zero
[0x80002504]:fsd ft11, 1168(a5)
[0x80002508]:sw a7, 1172(a5)
[0x8000250c]:fld ft10, 1184(a6)
[0x80002510]:fld ft9, 1192(a6)
[0x80002514]:csrrwi zero, frm, 3

[0x80002518]:fsub.d ft11, ft10, ft9, dyn
[0x8000251c]:csrrs a7, fflags, zero
[0x80002520]:fsd ft11, 1184(a5)
[0x80002524]:sw a7, 1188(a5)
[0x80002528]:fld ft10, 1200(a6)
[0x8000252c]:fld ft9, 1208(a6)
[0x80002530]:csrrwi zero, frm, 3

[0x80002534]:fsub.d ft11, ft10, ft9, dyn
[0x80002538]:csrrs a7, fflags, zero
[0x8000253c]:fsd ft11, 1200(a5)
[0x80002540]:sw a7, 1204(a5)
[0x80002544]:fld ft10, 1216(a6)
[0x80002548]:fld ft9, 1224(a6)
[0x8000254c]:csrrwi zero, frm, 3

[0x80002550]:fsub.d ft11, ft10, ft9, dyn
[0x80002554]:csrrs a7, fflags, zero
[0x80002558]:fsd ft11, 1216(a5)
[0x8000255c]:sw a7, 1220(a5)
[0x80002560]:fld ft10, 1232(a6)
[0x80002564]:fld ft9, 1240(a6)
[0x80002568]:csrrwi zero, frm, 3

[0x8000256c]:fsub.d ft11, ft10, ft9, dyn
[0x80002570]:csrrs a7, fflags, zero
[0x80002574]:fsd ft11, 1232(a5)
[0x80002578]:sw a7, 1236(a5)
[0x8000257c]:fld ft10, 1248(a6)
[0x80002580]:fld ft9, 1256(a6)
[0x80002584]:csrrwi zero, frm, 3

[0x80002588]:fsub.d ft11, ft10, ft9, dyn
[0x8000258c]:csrrs a7, fflags, zero
[0x80002590]:fsd ft11, 1248(a5)
[0x80002594]:sw a7, 1252(a5)
[0x80002598]:fld ft10, 1264(a6)
[0x8000259c]:fld ft9, 1272(a6)
[0x800025a0]:csrrwi zero, frm, 3

[0x800025a4]:fsub.d ft11, ft10, ft9, dyn
[0x800025a8]:csrrs a7, fflags, zero
[0x800025ac]:fsd ft11, 1264(a5)
[0x800025b0]:sw a7, 1268(a5)
[0x800025b4]:fld ft10, 1280(a6)
[0x800025b8]:fld ft9, 1288(a6)
[0x800025bc]:csrrwi zero, frm, 3

[0x800025c0]:fsub.d ft11, ft10, ft9, dyn
[0x800025c4]:csrrs a7, fflags, zero
[0x800025c8]:fsd ft11, 1280(a5)
[0x800025cc]:sw a7, 1284(a5)
[0x800025d0]:fld ft10, 1296(a6)
[0x800025d4]:fld ft9, 1304(a6)
[0x800025d8]:csrrwi zero, frm, 3

[0x800025dc]:fsub.d ft11, ft10, ft9, dyn
[0x800025e0]:csrrs a7, fflags, zero
[0x800025e4]:fsd ft11, 1296(a5)
[0x800025e8]:sw a7, 1300(a5)
[0x800025ec]:fld ft10, 1312(a6)
[0x800025f0]:fld ft9, 1320(a6)
[0x800025f4]:csrrwi zero, frm, 3

[0x800025f8]:fsub.d ft11, ft10, ft9, dyn
[0x800025fc]:csrrs a7, fflags, zero
[0x80002600]:fsd ft11, 1312(a5)
[0x80002604]:sw a7, 1316(a5)
[0x80002608]:fld ft10, 1328(a6)
[0x8000260c]:fld ft9, 1336(a6)
[0x80002610]:csrrwi zero, frm, 3



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                              coverpoints                                                                                                               |                                                                           code                                                                           |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005714]<br>0x00000000|- opcode : fsub.d<br> - rs1 : f11<br> - rs2 : f13<br> - rd : f13<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x132d8f91b7583 and rm_val == 3  #nosat<br> |[0x80000120]:fsub.d fa3, fa1, fa3, dyn<br> [0x80000124]:csrrs a7, fflags, zero<br> [0x80000128]:fsd fa3, 0(a5)<br> [0x8000012c]:sw a7, 4(a5)<br>          |
|   2|[0x80005724]<br>0x00000000|- rs1 : f14<br> - rs2 : f6<br> - rd : f22<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x84620ba958ca7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84620ba958ca7 and rm_val == 3  #nosat<br> |[0x8000013c]:fsub.d fs6, fa4, ft6, dyn<br> [0x80000140]:csrrs a7, fflags, zero<br> [0x80000144]:fsd fs6, 16(a5)<br> [0x80000148]:sw a7, 20(a5)<br>        |
|   3|[0x80005734]<br>0x00000000|- rs1 : f2<br> - rs2 : f2<br> - rd : f8<br> - rs1 == rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9986e1947d1af and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9986e1947d1af and rm_val == 3  #nosat<br>                          |[0x80000158]:fsub.d fs0, ft2, ft2, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:fsd fs0, 32(a5)<br> [0x80000164]:sw a7, 36(a5)<br>        |
|   4|[0x80005744]<br>0x00000000|- rs1 : f31<br> - rs2 : f5<br> - rd : f31<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2917055261bcd and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2917055261bcd and rm_val == 3  #nosat<br>                        |[0x80000174]:fsub.d ft11, ft11, ft5, dyn<br> [0x80000178]:csrrs a7, fflags, zero<br> [0x8000017c]:fsd ft11, 48(a5)<br> [0x80000180]:sw a7, 52(a5)<br>     |
|   5|[0x80005754]<br>0x00000000|- rs1 : f1<br> - rs2 : f1<br> - rd : f1<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x473e8571c52cb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x473e8571c52cb and rm_val == 3  #nosat<br>                          |[0x80000190]:fsub.d ft1, ft1, ft1, dyn<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:fsd ft1, 64(a5)<br> [0x8000019c]:sw a7, 68(a5)<br>        |
|   6|[0x80005764]<br>0x00000000|- rs1 : f19<br> - rs2 : f17<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x984a694055a54 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x984a694055a54 and rm_val == 3  #nosat<br>                                              |[0x800001ac]:fsub.d fa6, fs3, fa7, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:fsd fa6, 80(a5)<br> [0x800001b8]:sw a7, 84(a5)<br>        |
|   7|[0x80005774]<br>0x00000000|- rs1 : f9<br> - rs2 : f26<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfd2122050beac and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfd2122050beac and rm_val == 3  #nosat<br>                                               |[0x800001c8]:fsub.d fs3, fs1, fs10, dyn<br> [0x800001cc]:csrrs a7, fflags, zero<br> [0x800001d0]:fsd fs3, 96(a5)<br> [0x800001d4]:sw a7, 100(a5)<br>      |
|   8|[0x80005784]<br>0x00000000|- rs1 : f21<br> - rs2 : f28<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2feec68719bba and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2feec68719bba and rm_val == 3  #nosat<br>                                               |[0x800001e4]:fsub.d ft7, fs5, ft8, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd ft7, 112(a5)<br> [0x800001f0]:sw a7, 116(a5)<br>      |
|   9|[0x80005794]<br>0x00000000|- rs1 : f16<br> - rs2 : f12<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6e0bf7d08105c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6e0bf7d08105c and rm_val == 3  #nosat<br>                                              |[0x80000200]:fsub.d fa5, fa6, fa2, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:fsd fa5, 128(a5)<br> [0x8000020c]:sw a7, 132(a5)<br>      |
|  10|[0x800057a4]<br>0x00000000|- rs1 : f23<br> - rs2 : f8<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28ecf1d8ef197 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x28ecf1d8ef197 and rm_val == 3  #nosat<br>                                                |[0x8000021c]:fsub.d ft4, fs7, fs0, dyn<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:fsd ft4, 144(a5)<br> [0x80000228]:sw a7, 148(a5)<br>      |
|  11|[0x800057b4]<br>0x00000000|- rs1 : f20<br> - rs2 : f31<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa3eab352272ea and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa3eab352272ea and rm_val == 3  #nosat<br>                                               |[0x80000238]:fsub.d ft2, fs4, ft11, dyn<br> [0x8000023c]:csrrs a7, fflags, zero<br> [0x80000240]:fsd ft2, 160(a5)<br> [0x80000244]:sw a7, 164(a5)<br>     |
|  12|[0x800057c4]<br>0x00000000|- rs1 : f28<br> - rs2 : f20<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5949aff9333f3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5949aff9333f3 and rm_val == 3  #nosat<br>                                              |[0x80000254]:fsub.d fa7, ft8, fs4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:fsd fa7, 176(a5)<br> [0x80000260]:sw a7, 180(a5)<br>      |
|  13|[0x800057d4]<br>0x00000000|- rs1 : f4<br> - rs2 : f25<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfae7473993807 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xfae7473993807 and rm_val == 3  #nosat<br>                                                |[0x80000270]:fsub.d ft5, ft4, fs9, dyn<br> [0x80000274]:csrrs a7, fflags, zero<br> [0x80000278]:fsd ft5, 192(a5)<br> [0x8000027c]:sw a7, 196(a5)<br>      |
|  14|[0x800057e4]<br>0x00000000|- rs1 : f15<br> - rs2 : f19<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0x8610c871b285f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x8610c871b285f and rm_val == 3  #nosat<br>                                              |[0x8000028c]:fsub.d ft9, fa5, fs3, dyn<br> [0x80000290]:csrrs a7, fflags, zero<br> [0x80000294]:fsd ft9, 208(a5)<br> [0x80000298]:sw a7, 212(a5)<br>      |
|  15|[0x800057f4]<br>0x00000000|- rs1 : f3<br> - rs2 : f9<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe39a5539fae27 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xe39a5539fae27 and rm_val == 3  #nosat<br>                                                |[0x800002a8]:fsub.d fa2, ft3, fs1, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:fsd fa2, 224(a5)<br> [0x800002b4]:sw a7, 228(a5)<br>      |
|  16|[0x80005804]<br>0x00000000|- rs1 : f26<br> - rs2 : f21<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5ea96bd4dabb5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5ea96bd4dabb5 and rm_val == 3  #nosat<br>                                              |[0x800002c4]:fsub.d fs7, fs10, fs5, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs7, 240(a5)<br> [0x800002d0]:sw a7, 244(a5)<br>     |
|  17|[0x80005814]<br>0x00000000|- rs1 : f22<br> - rs2 : f30<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeda15838c7849 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xeda15838c7849 and rm_val == 3  #nosat<br>                                              |[0x800002e0]:fsub.d fs8, fs6, ft10, dyn<br> [0x800002e4]:csrrs a7, fflags, zero<br> [0x800002e8]:fsd fs8, 256(a5)<br> [0x800002ec]:sw a7, 260(a5)<br>     |
|  18|[0x80005824]<br>0x00000000|- rs1 : f17<br> - rs2 : f7<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6868ac61d3897 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6868ac61d3897 and rm_val == 3  #nosat<br>                                               |[0x800002fc]:fsub.d ft10, fa7, ft7, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:fsd ft10, 272(a5)<br> [0x80000308]:sw a7, 276(a5)<br>    |
|  19|[0x80005834]<br>0x00000000|- rs1 : f29<br> - rs2 : f16<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb82a6aeecb53 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbb82a6aeecb53 and rm_val == 3  #nosat<br>                                              |[0x80000318]:fsub.d fs10, ft9, fa6, dyn<br> [0x8000031c]:csrrs a7, fflags, zero<br> [0x80000320]:fsd fs10, 288(a5)<br> [0x80000324]:sw a7, 292(a5)<br>    |
|  20|[0x80005844]<br>0x00000000|- rs1 : f8<br> - rs2 : f0<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x31ed4c817d79b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x31ed4c817d79b and rm_val == 3  #nosat<br>                                                |[0x80000334]:fsub.d fs2, fs0, ft0, dyn<br> [0x80000338]:csrrs a7, fflags, zero<br> [0x8000033c]:fsd fs2, 304(a5)<br> [0x80000340]:sw a7, 308(a5)<br>      |
|  21|[0x80005854]<br>0x00000000|- rs1 : f10<br> - rs2 : f14<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7e7cb00b83da3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x7e7cb00b83da3 and rm_val == 3  #nosat<br>                                              |[0x80000350]:fsub.d fs9, fa0, fa4, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:fsd fs9, 320(a5)<br> [0x8000035c]:sw a7, 324(a5)<br>      |
|  22|[0x80005864]<br>0x00000000|- rs1 : f27<br> - rs2 : f10<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf82d9cf6dc925 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf82d9cf6dc925 and rm_val == 3  #nosat<br>                                              |[0x8000036c]:fsub.d fs4, fs11, fa0, dyn<br> [0x80000370]:csrrs a7, fflags, zero<br> [0x80000374]:fsd fs4, 336(a5)<br> [0x80000378]:sw a7, 340(a5)<br>     |
|  23|[0x80005874]<br>0x00000000|- rs1 : f7<br> - rs2 : f11<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0xb3c136748a917 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xb3c136748a917 and rm_val == 3  #nosat<br>                                                |[0x80000388]:fsub.d ft6, ft7, fa1, dyn<br> [0x8000038c]:csrrs a7, fflags, zero<br> [0x80000390]:fsd ft6, 352(a5)<br> [0x80000394]:sw a7, 356(a5)<br>      |
|  24|[0x80005884]<br>0x00000000|- rs1 : f5<br> - rs2 : f23<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6fdc352b9c092 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6fdc352b9c092 and rm_val == 3  #nosat<br>                                                |[0x800003a4]:fsub.d ft3, ft5, fs7, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft3, 368(a5)<br> [0x800003b0]:sw a7, 372(a5)<br>      |
|  25|[0x80005894]<br>0x00000000|- rs1 : f13<br> - rs2 : f27<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0xec87e91da77d7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xec87e91da77d7 and rm_val == 3  #nosat<br>                                              |[0x800003c0]:fsub.d fa0, fa3, fs11, dyn<br> [0x800003c4]:csrrs a7, fflags, zero<br> [0x800003c8]:fsd fa0, 384(a5)<br> [0x800003cc]:sw a7, 388(a5)<br>     |
|  26|[0x800058a4]<br>0x00000000|- rs1 : f0<br> - rs2 : f22<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x71c18427a646b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x71c18427a646b and rm_val == 3  #nosat<br>                                               |[0x800003dc]:fsub.d fs11, ft0, fs6, dyn<br> [0x800003e0]:csrrs a7, fflags, zero<br> [0x800003e4]:fsd fs11, 400(a5)<br> [0x800003e8]:sw a7, 404(a5)<br>    |
|  27|[0x800058b4]<br>0x00000000|- rs1 : f6<br> - rs2 : f4<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x1ad31ee4d4ad7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x1ad31ee4d4ad7 and rm_val == 3  #nosat<br>                                                |[0x800003f8]:fsub.d fs5, ft6, ft4, dyn<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:fsd fs5, 416(a5)<br> [0x80000404]:sw a7, 420(a5)<br>      |
|  28|[0x800058c4]<br>0x00000000|- rs1 : f24<br> - rs2 : f29<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0d9d824a66fc7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0d9d824a66fc7 and rm_val == 3  #nosat<br>                                              |[0x80000414]:fsub.d ft8, fs8, ft9, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd ft8, 432(a5)<br> [0x80000420]:sw a7, 436(a5)<br>      |
|  29|[0x800058d4]<br>0x00000000|- rs1 : f12<br> - rs2 : f3<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3667b2bc82acb and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3667b2bc82acb and rm_val == 3  #nosat<br>                                                |[0x80000430]:fsub.d fs1, fa2, ft3, dyn<br> [0x80000434]:csrrs a7, fflags, zero<br> [0x80000438]:fsd fs1, 448(a5)<br> [0x8000043c]:sw a7, 452(a5)<br>      |
|  30|[0x800058e4]<br>0x00000000|- rs1 : f18<br> - rs2 : f15<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd387bdfbb52c6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd387bdfbb52c6 and rm_val == 3  #nosat<br>                                              |[0x8000044c]:fsub.d fa1, fs2, fa5, dyn<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd fa1, 464(a5)<br> [0x80000458]:sw a7, 468(a5)<br>      |
|  31|[0x800058f4]<br>0x00000000|- rs1 : f25<br> - rs2 : f24<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf233966510bcc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf233966510bcc and rm_val == 3  #nosat<br>                                               |[0x80000468]:fsub.d ft0, fs9, fs8, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:fsd ft0, 480(a5)<br> [0x80000474]:sw a7, 484(a5)<br>      |
|  32|[0x80005904]<br>0x00000000|- rs1 : f30<br> - rs2 : f18<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4074322ede639 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4074322ede639 and rm_val == 3  #nosat<br>                                              |[0x80000484]:fsub.d fa4, ft10, fs2, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fa4, 496(a5)<br> [0x80000490]:sw a7, 500(a5)<br>     |
|  33|[0x80005914]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9221841138cb5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9221841138cb5 and rm_val == 3  #nosat<br>                                                                                             |[0x800004a0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:fsd ft11, 512(a5)<br> [0x800004ac]:sw a7, 516(a5)<br>   |
|  34|[0x80005924]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xba20c4777099d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xba20c4777099d and rm_val == 3  #nosat<br>                                                                                             |[0x800004bc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft11, 528(a5)<br> [0x800004c8]:sw a7, 532(a5)<br>   |
|  35|[0x80005934]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3eb20959c42c2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3eb20959c42c2 and rm_val == 3  #nosat<br>                                                                                             |[0x800004d8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004dc]:csrrs a7, fflags, zero<br> [0x800004e0]:fsd ft11, 544(a5)<br> [0x800004e4]:sw a7, 548(a5)<br>   |
|  36|[0x80005944]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xf446ded06de1f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xf446ded06de1f and rm_val == 3  #nosat<br>                                                                                             |[0x800004f4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd ft11, 560(a5)<br> [0x80000500]:sw a7, 564(a5)<br>   |
|  37|[0x80005954]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x69d3500fa16c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x69d3500fa16c1 and rm_val == 3  #nosat<br>                                                                                             |[0x80000510]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd ft11, 576(a5)<br> [0x8000051c]:sw a7, 580(a5)<br>   |
|  38|[0x80005964]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc74ef4423e96b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc74ef4423e96b and rm_val == 3  #nosat<br>                                                                                             |[0x8000052c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000530]:csrrs a7, fflags, zero<br> [0x80000534]:fsd ft11, 592(a5)<br> [0x80000538]:sw a7, 596(a5)<br>   |
|  39|[0x80005974]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbbc271a710d1b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbbc271a710d1b and rm_val == 3  #nosat<br>                                                                                             |[0x80000548]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:fsd ft11, 608(a5)<br> [0x80000554]:sw a7, 612(a5)<br>   |
|  40|[0x80005984]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x60ffd67bcec83 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x60ffd67bcec83 and rm_val == 3  #nosat<br>                                                                                             |[0x80000564]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 624(a5)<br> [0x80000570]:sw a7, 628(a5)<br>   |
|  41|[0x80005994]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdb5e85647ec13 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xdb5e85647ec13 and rm_val == 3  #nosat<br>                                                                                             |[0x80000580]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000584]:csrrs a7, fflags, zero<br> [0x80000588]:fsd ft11, 640(a5)<br> [0x8000058c]:sw a7, 644(a5)<br>   |
|  42|[0x800059a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x143c21ad8c8b5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x143c21ad8c8b5 and rm_val == 3  #nosat<br>                                                                                             |[0x8000059c]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005a0]:csrrs a7, fflags, zero<br> [0x800005a4]:fsd ft11, 656(a5)<br> [0x800005a8]:sw a7, 660(a5)<br>   |
|  43|[0x800059b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6d479d3fc4771 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6d479d3fc4771 and rm_val == 3  #nosat<br>                                                                                             |[0x800005b8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:fsd ft11, 672(a5)<br> [0x800005c4]:sw a7, 676(a5)<br>   |
|  44|[0x800059c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5932a24c0014f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5932a24c0014f and rm_val == 3  #nosat<br>                                                                                             |[0x800005d4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005d8]:csrrs a7, fflags, zero<br> [0x800005dc]:fsd ft11, 688(a5)<br> [0x800005e0]:sw a7, 692(a5)<br>   |
|  45|[0x800059d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05e5cee3b08d7 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05e5cee3b08d7 and rm_val == 3  #nosat<br>                                                                                             |[0x800005f0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:fsd ft11, 704(a5)<br> [0x800005fc]:sw a7, 708(a5)<br>   |
|  46|[0x800059e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0be093ea29884 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0be093ea29884 and rm_val == 3  #nosat<br>                                                                                             |[0x8000060c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft11, 720(a5)<br> [0x80000618]:sw a7, 724(a5)<br>   |
|  47|[0x800059f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x32ba6165fce3f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x32ba6165fce3f and rm_val == 3  #nosat<br>                                                                                             |[0x80000628]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000062c]:csrrs a7, fflags, zero<br> [0x80000630]:fsd ft11, 736(a5)<br> [0x80000634]:sw a7, 740(a5)<br>   |
|  48|[0x80005a04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5a51b555f5c9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5a51b555f5c9 and rm_val == 3  #nosat<br>                                                                                             |[0x80000644]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 752(a5)<br> [0x80000650]:sw a7, 756(a5)<br>   |
|  49|[0x80005a14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xabce33873116b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xabce33873116b and rm_val == 3  #nosat<br>                                                                                             |[0x80000660]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd ft11, 768(a5)<br> [0x8000066c]:sw a7, 772(a5)<br>   |
|  50|[0x80005a24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x60b89491a6a27 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x60b89491a6a27 and rm_val == 3  #nosat<br>                                                                                             |[0x8000067c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000680]:csrrs a7, fflags, zero<br> [0x80000684]:fsd ft11, 784(a5)<br> [0x80000688]:sw a7, 788(a5)<br>   |
|  51|[0x80005a34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x618258c5f4965 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x618258c5f4965 and rm_val == 3  #nosat<br>                                                                                             |[0x80000698]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:fsd ft11, 800(a5)<br> [0x800006a4]:sw a7, 804(a5)<br>   |
|  52|[0x80005a44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x03aaf26d74a36 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x03aaf26d74a36 and rm_val == 3  #nosat<br>                                                                                             |[0x800006b4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 816(a5)<br> [0x800006c0]:sw a7, 820(a5)<br>   |
|  53|[0x80005a54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x504dcbdc51a65 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x504dcbdc51a65 and rm_val == 3  #nosat<br>                                                                                             |[0x800006d0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006d4]:csrrs a7, fflags, zero<br> [0x800006d8]:fsd ft11, 832(a5)<br> [0x800006dc]:sw a7, 836(a5)<br>   |
|  54|[0x80005a64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xb0bd7b08edb55 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xb0bd7b08edb55 and rm_val == 3  #nosat<br>                                                                                             |[0x800006ec]:fsub.d ft11, ft10, ft9, dyn<br> [0x800006f0]:csrrs a7, fflags, zero<br> [0x800006f4]:fsd ft11, 848(a5)<br> [0x800006f8]:sw a7, 852(a5)<br>   |
|  55|[0x80005a74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05438a864ff48 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05438a864ff48 and rm_val == 3  #nosat<br>                                                                                             |[0x80000708]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:fsd ft11, 864(a5)<br> [0x80000714]:sw a7, 868(a5)<br>   |
|  56|[0x80005a84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xde5026c152607 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xde5026c152607 and rm_val == 3  #nosat<br>                                                                                             |[0x80000724]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 880(a5)<br> [0x80000730]:sw a7, 884(a5)<br>   |
|  57|[0x80005a94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaa7bbc9099344 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaa7bbc9099344 and rm_val == 3  #nosat<br>                                                                                             |[0x80000740]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd ft11, 896(a5)<br> [0x8000074c]:sw a7, 900(a5)<br>   |
|  58|[0x80005aa4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x16a782d36f4f6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x16a782d36f4f6 and rm_val == 3  #nosat<br>                                                                                             |[0x8000075c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 912(a5)<br> [0x80000768]:sw a7, 916(a5)<br>   |
|  59|[0x80005ab4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6a8199da501dc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6a8199da501dc and rm_val == 3  #nosat<br>                                                                                             |[0x80000778]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000077c]:csrrs a7, fflags, zero<br> [0x80000780]:fsd ft11, 928(a5)<br> [0x80000784]:sw a7, 932(a5)<br>   |
|  60|[0x80005ac4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x65d63e74d209d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x65d63e74d209d and rm_val == 3  #nosat<br>                                                                                             |[0x80000794]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000798]:csrrs a7, fflags, zero<br> [0x8000079c]:fsd ft11, 944(a5)<br> [0x800007a0]:sw a7, 948(a5)<br>   |
|  61|[0x80005ad4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa1726431ab40b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa1726431ab40b and rm_val == 3  #nosat<br>                                                                                             |[0x800007b0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:fsd ft11, 960(a5)<br> [0x800007bc]:sw a7, 964(a5)<br>   |
|  62|[0x80005ae4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb14a0c4b66d3b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb14a0c4b66d3b and rm_val == 3  #nosat<br>                                                                                             |[0x800007cc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007d0]:csrrs a7, fflags, zero<br> [0x800007d4]:fsd ft11, 976(a5)<br> [0x800007d8]:sw a7, 980(a5)<br>   |
|  63|[0x80005af4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdfcdecd96da66 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdfcdecd96da66 and rm_val == 3  #nosat<br>                                                                                             |[0x800007e8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:fsd ft11, 992(a5)<br> [0x800007f4]:sw a7, 996(a5)<br>   |
|  64|[0x80005b04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x422ea209fd4bd and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x422ea209fd4bd and rm_val == 3  #nosat<br>                                                                                             |[0x80000804]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 1008(a5)<br> [0x80000810]:sw a7, 1012(a5)<br> |
|  65|[0x80005b14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4264cf0154662 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4264cf0154662 and rm_val == 3  #nosat<br>                                                                                             |[0x80000820]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000824]:csrrs a7, fflags, zero<br> [0x80000828]:fsd ft11, 1024(a5)<br> [0x8000082c]:sw a7, 1028(a5)<br> |
|  66|[0x80005b24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x47dca9bde3664 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x47dca9bde3664 and rm_val == 3  #nosat<br>                                                                                             |[0x8000083c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd ft11, 1040(a5)<br> [0x80000848]:sw a7, 1044(a5)<br> |
|  67|[0x80005b34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf886e2fe6ac5f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf886e2fe6ac5f and rm_val == 3  #nosat<br>                                                                                             |[0x80000858]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000085c]:csrrs a7, fflags, zero<br> [0x80000860]:fsd ft11, 1056(a5)<br> [0x80000864]:sw a7, 1060(a5)<br> |
|  68|[0x80005b44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcbbac03deb701 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xcbbac03deb701 and rm_val == 3  #nosat<br>                                                                                             |[0x80000874]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000878]:csrrs a7, fflags, zero<br> [0x8000087c]:fsd ft11, 1072(a5)<br> [0x80000880]:sw a7, 1076(a5)<br> |
|  69|[0x80005b54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2b3a267e5dfb6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2b3a267e5dfb6 and rm_val == 3  #nosat<br>                                                                                             |[0x80000890]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:fsd ft11, 1088(a5)<br> [0x8000089c]:sw a7, 1092(a5)<br> |
|  70|[0x80005b64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9efa662b0261b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9efa662b0261b and rm_val == 3  #nosat<br>                                                                                             |[0x800008ac]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 1104(a5)<br> [0x800008b8]:sw a7, 1108(a5)<br> |
|  71|[0x80005b74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8e80a6ca28041 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8e80a6ca28041 and rm_val == 3  #nosat<br>                                                                                             |[0x800008c8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:fsd ft11, 1120(a5)<br> [0x800008d4]:sw a7, 1124(a5)<br> |
|  72|[0x80005b84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x700c54435a377 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x700c54435a377 and rm_val == 3  #nosat<br>                                                                                             |[0x800008e4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 1136(a5)<br> [0x800008f0]:sw a7, 1140(a5)<br> |
|  73|[0x80005b94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3d750eeace47f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3d750eeace47f and rm_val == 3  #nosat<br>                                                                                             |[0x80000900]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000904]:csrrs a7, fflags, zero<br> [0x80000908]:fsd ft11, 1152(a5)<br> [0x8000090c]:sw a7, 1156(a5)<br> |
|  74|[0x80005ba4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x38aa27d9f85c9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x38aa27d9f85c9 and rm_val == 3  #nosat<br>                                                                                             |[0x8000091c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000920]:csrrs a7, fflags, zero<br> [0x80000924]:fsd ft11, 1168(a5)<br> [0x80000928]:sw a7, 1172(a5)<br> |
|  75|[0x80005bb4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7d40396d9385b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x7d40396d9385b and rm_val == 3  #nosat<br>                                                                                             |[0x80000938]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000093c]:csrrs a7, fflags, zero<br> [0x80000940]:fsd ft11, 1184(a5)<br> [0x80000944]:sw a7, 1188(a5)<br> |
|  76|[0x80005bc4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xab4fd6611517f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xab4fd6611517f and rm_val == 3  #nosat<br>                                                                                             |[0x80000954]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsd ft11, 1200(a5)<br> [0x80000960]:sw a7, 1204(a5)<br> |
|  77|[0x80005bd4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf95a713b177ca and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf95a713b177ca and rm_val == 3  #nosat<br>                                                                                             |[0x80000970]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000974]:csrrs a7, fflags, zero<br> [0x80000978]:fsd ft11, 1216(a5)<br> [0x8000097c]:sw a7, 1220(a5)<br> |
|  78|[0x80005be4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8a9e6ee9dc95 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa8a9e6ee9dc95 and rm_val == 3  #nosat<br>                                                                                             |[0x8000098c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000990]:csrrs a7, fflags, zero<br> [0x80000994]:fsd ft11, 1232(a5)<br> [0x80000998]:sw a7, 1236(a5)<br> |
|  79|[0x80005bf4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x324293ee39f7d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x324293ee39f7d and rm_val == 3  #nosat<br>                                                                                             |[0x800009a8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009ac]:csrrs a7, fflags, zero<br> [0x800009b0]:fsd ft11, 1248(a5)<br> [0x800009b4]:sw a7, 1252(a5)<br> |
|  80|[0x80005c04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f6 and fm1 == 0xfbfd7fab4eeff and fs2 == 0 and fe2 == 0x7f6 and fm2 == 0xfbfd7fab4eeff and rm_val == 3  #nosat<br>                                                                                             |[0x800009c4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1264(a5)<br> [0x800009d0]:sw a7, 1268(a5)<br> |
|  81|[0x80005c14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x829e9eb0f2033 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x829e9eb0f2033 and rm_val == 3  #nosat<br>                                                                                             |[0x800009e0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800009e4]:csrrs a7, fflags, zero<br> [0x800009e8]:fsd ft11, 1280(a5)<br> [0x800009ec]:sw a7, 1284(a5)<br> |
|  82|[0x80005c24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf81d438e79e89 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf81d438e79e89 and rm_val == 3  #nosat<br>                                                                                             |[0x800009fc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:fsd ft11, 1296(a5)<br> [0x80000a08]:sw a7, 1300(a5)<br> |
|  83|[0x80005c34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x90f0d1eecae4a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x90f0d1eecae4a and rm_val == 3  #nosat<br>                                                                                             |[0x80000a18]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a1c]:csrrs a7, fflags, zero<br> [0x80000a20]:fsd ft11, 1312(a5)<br> [0x80000a24]:sw a7, 1316(a5)<br> |
|  84|[0x80005c44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x381d474507a13 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x381d474507a13 and rm_val == 3  #nosat<br>                                                                                             |[0x80000a34]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a38]:csrrs a7, fflags, zero<br> [0x80000a3c]:fsd ft11, 1328(a5)<br> [0x80000a40]:sw a7, 1332(a5)<br> |
|  85|[0x80005c54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc4ee0c5be65d1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc4ee0c5be65d1 and rm_val == 3  #nosat<br>                                                                                             |[0x80000a50]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a54]:csrrs a7, fflags, zero<br> [0x80000a58]:fsd ft11, 1344(a5)<br> [0x80000a5c]:sw a7, 1348(a5)<br> |
|  86|[0x80005c64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0e89a794b74d2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0e89a794b74d2 and rm_val == 3  #nosat<br>                                                                                             |[0x80000a6c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a70]:csrrs a7, fflags, zero<br> [0x80000a74]:fsd ft11, 1360(a5)<br> [0x80000a78]:sw a7, 1364(a5)<br> |
|  87|[0x80005c74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x018d796b58467 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x018d796b58467 and rm_val == 3  #nosat<br>                                                                                             |[0x80000a88]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000a8c]:csrrs a7, fflags, zero<br> [0x80000a90]:fsd ft11, 1376(a5)<br> [0x80000a94]:sw a7, 1380(a5)<br> |
|  88|[0x80005c84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2528fb338cf74 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2528fb338cf74 and rm_val == 3  #nosat<br>                                                                                             |[0x80000aa4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1392(a5)<br> [0x80000ab0]:sw a7, 1396(a5)<br> |
|  89|[0x80005c94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x17be9a133f3af and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x17be9a133f3af and rm_val == 3  #nosat<br>                                                                                             |[0x80000ac0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ac4]:csrrs a7, fflags, zero<br> [0x80000ac8]:fsd ft11, 1408(a5)<br> [0x80000acc]:sw a7, 1412(a5)<br> |
|  90|[0x80005ca4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x85aa65ee5b308 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x85aa65ee5b308 and rm_val == 3  #nosat<br>                                                                                             |[0x80000adc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ae0]:csrrs a7, fflags, zero<br> [0x80000ae4]:fsd ft11, 1424(a5)<br> [0x80000ae8]:sw a7, 1428(a5)<br> |
|  91|[0x80005cb4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x172fde92f86c8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x172fde92f86c8 and rm_val == 3  #nosat<br>                                                                                             |[0x80000af8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000afc]:csrrs a7, fflags, zero<br> [0x80000b00]:fsd ft11, 1440(a5)<br> [0x80000b04]:sw a7, 1444(a5)<br> |
|  92|[0x80005cc4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe39ef9237c697 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xe39ef9237c697 and rm_val == 3  #nosat<br>                                                                                             |[0x80000b14]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b18]:csrrs a7, fflags, zero<br> [0x80000b1c]:fsd ft11, 1456(a5)<br> [0x80000b20]:sw a7, 1460(a5)<br> |
|  93|[0x80005cd4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xbaf02dcedb6b7 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xbaf02dcedb6b7 and rm_val == 3  #nosat<br>                                                                                             |[0x80000b30]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b34]:csrrs a7, fflags, zero<br> [0x80000b38]:fsd ft11, 1472(a5)<br> [0x80000b3c]:sw a7, 1476(a5)<br> |
|  94|[0x80005ce4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x88745c9a37993 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x88745c9a37993 and rm_val == 3  #nosat<br>                                                                                             |[0x80000b4c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b50]:csrrs a7, fflags, zero<br> [0x80000b54]:fsd ft11, 1488(a5)<br> [0x80000b58]:sw a7, 1492(a5)<br> |
|  95|[0x80005cf4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x89c3334d5f5bb and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x89c3334d5f5bb and rm_val == 3  #nosat<br>                                                                                             |[0x80000b68]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b6c]:csrrs a7, fflags, zero<br> [0x80000b70]:fsd ft11, 1504(a5)<br> [0x80000b74]:sw a7, 1508(a5)<br> |
|  96|[0x80005d04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf696b535c1769 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf696b535c1769 and rm_val == 3  #nosat<br>                                                                                             |[0x80000b84]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1520(a5)<br> [0x80000b90]:sw a7, 1524(a5)<br> |
|  97|[0x80005d14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf27dcf8ac02d4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf27dcf8ac02d4 and rm_val == 3  #nosat<br>                                                                                             |[0x80000ba0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ba4]:csrrs a7, fflags, zero<br> [0x80000ba8]:fsd ft11, 1536(a5)<br> [0x80000bac]:sw a7, 1540(a5)<br> |
|  98|[0x80005d24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9509d7b71e92e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9509d7b71e92e and rm_val == 3  #nosat<br>                                                                                             |[0x80000bbc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bc0]:csrrs a7, fflags, zero<br> [0x80000bc4]:fsd ft11, 1552(a5)<br> [0x80000bc8]:sw a7, 1556(a5)<br> |
|  99|[0x80005d34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x00e7456a8a9b1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x00e7456a8a9b1 and rm_val == 3  #nosat<br>                                                                                             |[0x80000bd8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bdc]:csrrs a7, fflags, zero<br> [0x80000be0]:fsd ft11, 1568(a5)<br> [0x80000be4]:sw a7, 1572(a5)<br> |
| 100|[0x80005d44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x17c87a27d34af and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x17c87a27d34af and rm_val == 3  #nosat<br>                                                                                             |[0x80000bf4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000bf8]:csrrs a7, fflags, zero<br> [0x80000bfc]:fsd ft11, 1584(a5)<br> [0x80000c00]:sw a7, 1588(a5)<br> |
| 101|[0x80005d54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x239dca92ff1cf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x239dca92ff1cf and rm_val == 3  #nosat<br>                                                                                             |[0x80000c10]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c14]:csrrs a7, fflags, zero<br> [0x80000c18]:fsd ft11, 1600(a5)<br> [0x80000c1c]:sw a7, 1604(a5)<br> |
| 102|[0x80005d64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabb0ae90aa573 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xabb0ae90aa573 and rm_val == 3  #nosat<br>                                                                                             |[0x80000c2c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c30]:csrrs a7, fflags, zero<br> [0x80000c34]:fsd ft11, 1616(a5)<br> [0x80000c38]:sw a7, 1620(a5)<br> |
| 103|[0x80005d74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x414b2a3e47216 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x414b2a3e47216 and rm_val == 3  #nosat<br>                                                                                             |[0x80000c48]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1632(a5)<br> [0x80000c54]:sw a7, 1636(a5)<br> |
| 104|[0x80005d84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc14dba4a1f611 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc14dba4a1f611 and rm_val == 3  #nosat<br>                                                                                             |[0x80000c64]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c68]:csrrs a7, fflags, zero<br> [0x80000c6c]:fsd ft11, 1648(a5)<br> [0x80000c70]:sw a7, 1652(a5)<br> |
| 105|[0x80005d94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f7 and fm1 == 0x06bb1eb6b71ff and fs2 == 0 and fe2 == 0x7f7 and fm2 == 0x06bb1eb6b71ff and rm_val == 3  #nosat<br>                                                                                             |[0x80000c80]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000c84]:csrrs a7, fflags, zero<br> [0x80000c88]:fsd ft11, 1664(a5)<br> [0x80000c8c]:sw a7, 1668(a5)<br> |
| 106|[0x80005da4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xfc58dd60fc47b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xfc58dd60fc47b and rm_val == 3  #nosat<br>                                                                                             |[0x80000c9c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ca0]:csrrs a7, fflags, zero<br> [0x80000ca4]:fsd ft11, 1680(a5)<br> [0x80000ca8]:sw a7, 1684(a5)<br> |
| 107|[0x80005db4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xe3b25f522e53f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xe3b25f522e53f and rm_val == 3  #nosat<br>                                                                                             |[0x80000cb8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000cbc]:csrrs a7, fflags, zero<br> [0x80000cc0]:fsd ft11, 1696(a5)<br> [0x80000cc4]:sw a7, 1700(a5)<br> |
| 108|[0x80005dc4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x338c35622df30 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x338c35622df30 and rm_val == 3  #nosat<br>                                                                                             |[0x80000cd4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000cd8]:csrrs a7, fflags, zero<br> [0x80000cdc]:fsd ft11, 1712(a5)<br> [0x80000ce0]:sw a7, 1716(a5)<br> |
| 109|[0x80005dd4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeb39a20d91a7d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xeb39a20d91a7d and rm_val == 3  #nosat<br>                                                                                             |[0x80000cf0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000cf4]:csrrs a7, fflags, zero<br> [0x80000cf8]:fsd ft11, 1728(a5)<br> [0x80000cfc]:sw a7, 1732(a5)<br> |
| 110|[0x80005de4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x02b9579f55c5b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x02b9579f55c5b and rm_val == 3  #nosat<br>                                                                                             |[0x80000d0c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d10]:csrrs a7, fflags, zero<br> [0x80000d14]:fsd ft11, 1744(a5)<br> [0x80000d18]:sw a7, 1748(a5)<br> |
| 111|[0x80005df4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x516aa8e8fb467 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x516aa8e8fb467 and rm_val == 3  #nosat<br>                                                                                             |[0x80000d28]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1760(a5)<br> [0x80000d34]:sw a7, 1764(a5)<br> |
| 112|[0x80005e04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9afd0179d1bae and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9afd0179d1bae and rm_val == 3  #nosat<br>                                                                                             |[0x80000d44]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d48]:csrrs a7, fflags, zero<br> [0x80000d4c]:fsd ft11, 1776(a5)<br> [0x80000d50]:sw a7, 1780(a5)<br> |
| 113|[0x80005e14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf1421cf676cc1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf1421cf676cc1 and rm_val == 3  #nosat<br>                                                                                             |[0x80000d60]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d64]:csrrs a7, fflags, zero<br> [0x80000d68]:fsd ft11, 1792(a5)<br> [0x80000d6c]:sw a7, 1796(a5)<br> |
| 114|[0x80005e24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc7bd79ecec98f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xc7bd79ecec98f and rm_val == 3  #nosat<br>                                                                                             |[0x80000d7c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d80]:csrrs a7, fflags, zero<br> [0x80000d84]:fsd ft11, 1808(a5)<br> [0x80000d88]:sw a7, 1812(a5)<br> |
| 115|[0x80005e34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x51c6792bf1bb8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x51c6792bf1bb8 and rm_val == 3  #nosat<br>                                                                                             |[0x80000d98]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000d9c]:csrrs a7, fflags, zero<br> [0x80000da0]:fsd ft11, 1824(a5)<br> [0x80000da4]:sw a7, 1828(a5)<br> |
| 116|[0x80005e44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8d300de77b552 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8d300de77b552 and rm_val == 3  #nosat<br>                                                                                             |[0x80000db4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000db8]:csrrs a7, fflags, zero<br> [0x80000dbc]:fsd ft11, 1840(a5)<br> [0x80000dc0]:sw a7, 1844(a5)<br> |
| 117|[0x80005e54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeb3b913e63771 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xeb3b913e63771 and rm_val == 3  #nosat<br>                                                                                             |[0x80000dd0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000dd4]:csrrs a7, fflags, zero<br> [0x80000dd8]:fsd ft11, 1856(a5)<br> [0x80000ddc]:sw a7, 1860(a5)<br> |
| 118|[0x80005e64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x11f2665e52fc1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x11f2665e52fc1 and rm_val == 3  #nosat<br>                                                                                             |[0x80000dec]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000df0]:csrrs a7, fflags, zero<br> [0x80000df4]:fsd ft11, 1872(a5)<br> [0x80000df8]:sw a7, 1876(a5)<br> |
| 119|[0x80005e74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9daacd1054eee and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9daacd1054eee and rm_val == 3  #nosat<br>                                                                                             |[0x80000e08]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1888(a5)<br> [0x80000e14]:sw a7, 1892(a5)<br> |
| 120|[0x80005e84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x4d7c4e18c10ef and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x4d7c4e18c10ef and rm_val == 3  #nosat<br>                                                                                             |[0x80000e24]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e28]:csrrs a7, fflags, zero<br> [0x80000e2c]:fsd ft11, 1904(a5)<br> [0x80000e30]:sw a7, 1908(a5)<br> |
| 121|[0x80005e94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5181b18b5230b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x5181b18b5230b and rm_val == 3  #nosat<br>                                                                                             |[0x80000e40]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e44]:csrrs a7, fflags, zero<br> [0x80000e48]:fsd ft11, 1920(a5)<br> [0x80000e4c]:sw a7, 1924(a5)<br> |
| 122|[0x80005ea4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x645543b126259 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x645543b126259 and rm_val == 3  #nosat<br>                                                                                             |[0x80000e5c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e60]:csrrs a7, fflags, zero<br> [0x80000e64]:fsd ft11, 1936(a5)<br> [0x80000e68]:sw a7, 1940(a5)<br> |
| 123|[0x80005eb4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5cff741930dc6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5cff741930dc6 and rm_val == 3  #nosat<br>                                                                                             |[0x80000e78]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e7c]:csrrs a7, fflags, zero<br> [0x80000e80]:fsd ft11, 1952(a5)<br> [0x80000e84]:sw a7, 1956(a5)<br> |
| 124|[0x80005ec4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x83f7d2b210b05 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x83f7d2b210b05 and rm_val == 3  #nosat<br>                                                                                             |[0x80000e94]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000e98]:csrrs a7, fflags, zero<br> [0x80000e9c]:fsd ft11, 1968(a5)<br> [0x80000ea0]:sw a7, 1972(a5)<br> |
| 125|[0x80005ed4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbdaeddf112cfb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xbdaeddf112cfb and rm_val == 3  #nosat<br>                                                                                             |[0x80000eb0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000eb4]:csrrs a7, fflags, zero<br> [0x80000eb8]:fsd ft11, 1984(a5)<br> [0x80000ebc]:sw a7, 1988(a5)<br> |
| 126|[0x80005ee4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x69035627e1257 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x69035627e1257 and rm_val == 3  #nosat<br>                                                                                             |[0x80000ecc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ed0]:csrrs a7, fflags, zero<br> [0x80000ed4]:fsd ft11, 2000(a5)<br> [0x80000ed8]:sw a7, 2004(a5)<br> |
| 127|[0x80005ef4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xeb8f7360e493b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xeb8f7360e493b and rm_val == 3  #nosat<br>                                                                                             |[0x80000ee8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 2016(a5)<br> [0x80000ef4]:sw a7, 2020(a5)<br> |
| 128|[0x80005b0c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb669f507e33a4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb669f507e33a4 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f10]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f14]:csrrs a7, fflags, zero<br> [0x80000f18]:fsd ft11, 0(a5)<br> [0x80000f1c]:sw a7, 4(a5)<br>       |
| 129|[0x80005b1c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x790bcb9dbeeda and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x790bcb9dbeeda and rm_val == 3  #nosat<br>                                                                                             |[0x80000f2c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f30]:csrrs a7, fflags, zero<br> [0x80000f34]:fsd ft11, 16(a5)<br> [0x80000f38]:sw a7, 20(a5)<br>     |
| 130|[0x80005b2c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7c88779524935 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x7c88779524935 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f48]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 32(a5)<br> [0x80000f54]:sw a7, 36(a5)<br>     |
| 131|[0x80005b3c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6296d3932c17a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6296d3932c17a and rm_val == 3  #nosat<br>                                                                                             |[0x80000f64]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f68]:csrrs a7, fflags, zero<br> [0x80000f6c]:fsd ft11, 48(a5)<br> [0x80000f70]:sw a7, 52(a5)<br>     |
| 132|[0x80005b4c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc419d48d0bc89 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc419d48d0bc89 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f80]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000f84]:csrrs a7, fflags, zero<br> [0x80000f88]:fsd ft11, 64(a5)<br> [0x80000f8c]:sw a7, 68(a5)<br>     |
| 133|[0x80005b5c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x46970482fa4d3 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x46970482fa4d3 and rm_val == 3  #nosat<br>                                                                                             |[0x80000f9c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fa0]:csrrs a7, fflags, zero<br> [0x80000fa4]:fsd ft11, 80(a5)<br> [0x80000fa8]:sw a7, 84(a5)<br>     |
| 134|[0x80005b6c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x05fc74a94c67c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x05fc74a94c67c and rm_val == 3  #nosat<br>                                                                                             |[0x80000fb8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fbc]:csrrs a7, fflags, zero<br> [0x80000fc0]:fsd ft11, 96(a5)<br> [0x80000fc4]:sw a7, 100(a5)<br>    |
| 135|[0x80005b7c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x8ad527afb8d3f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x8ad527afb8d3f and rm_val == 3  #nosat<br>                                                                                             |[0x80000fd4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:fsd ft11, 112(a5)<br> [0x80000fe0]:sw a7, 116(a5)<br>   |
| 136|[0x80005b8c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x19d4ad7c76167 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x19d4ad7c76167 and rm_val == 3  #nosat<br>                                                                                             |[0x80000ff0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000ff4]:csrrs a7, fflags, zero<br> [0x80000ff8]:fsd ft11, 128(a5)<br> [0x80000ffc]:sw a7, 132(a5)<br>   |
| 137|[0x80005b9c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd05a5fee9b2b0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd05a5fee9b2b0 and rm_val == 3  #nosat<br>                                                                                             |[0x8000100c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001010]:csrrs a7, fflags, zero<br> [0x80001014]:fsd ft11, 144(a5)<br> [0x80001018]:sw a7, 148(a5)<br>   |
| 138|[0x80005bac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xa623d9ab2139f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xa623d9ab2139f and rm_val == 3  #nosat<br>                                                                                             |[0x80001028]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 160(a5)<br> [0x80001034]:sw a7, 164(a5)<br>   |
| 139|[0x80005bbc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xea51987a6fe4b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xea51987a6fe4b and rm_val == 3  #nosat<br>                                                                                             |[0x80001044]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001048]:csrrs a7, fflags, zero<br> [0x8000104c]:fsd ft11, 176(a5)<br> [0x80001050]:sw a7, 180(a5)<br>   |
| 140|[0x80005bcc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe830fb501fc6b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xe830fb501fc6b and rm_val == 3  #nosat<br>                                                                                             |[0x80001060]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001064]:csrrs a7, fflags, zero<br> [0x80001068]:fsd ft11, 192(a5)<br> [0x8000106c]:sw a7, 196(a5)<br>   |
| 141|[0x80005bdc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5f7ea628e7311 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5f7ea628e7311 and rm_val == 3  #nosat<br>                                                                                             |[0x8000107c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001080]:csrrs a7, fflags, zero<br> [0x80001084]:fsd ft11, 208(a5)<br> [0x80001088]:sw a7, 212(a5)<br>   |
| 142|[0x80005bec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4c34b3fae86a6 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4c34b3fae86a6 and rm_val == 3  #nosat<br>                                                                                             |[0x80001098]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000109c]:csrrs a7, fflags, zero<br> [0x800010a0]:fsd ft11, 224(a5)<br> [0x800010a4]:sw a7, 228(a5)<br>   |
| 143|[0x80005bfc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0eb1fe944dafc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0eb1fe944dafc and rm_val == 3  #nosat<br>                                                                                             |[0x800010b4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010b8]:csrrs a7, fflags, zero<br> [0x800010bc]:fsd ft11, 240(a5)<br> [0x800010c0]:sw a7, 244(a5)<br>   |
| 144|[0x80005c0c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xde44cb7c6a477 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xde44cb7c6a477 and rm_val == 3  #nosat<br>                                                                                             |[0x800010d0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010d4]:csrrs a7, fflags, zero<br> [0x800010d8]:fsd ft11, 256(a5)<br> [0x800010dc]:sw a7, 260(a5)<br>   |
| 145|[0x80005c1c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x9ab5479609cdf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9ab5479609cdf and rm_val == 3  #nosat<br>                                                                                             |[0x800010ec]:fsub.d ft11, ft10, ft9, dyn<br> [0x800010f0]:csrrs a7, fflags, zero<br> [0x800010f4]:fsd ft11, 272(a5)<br> [0x800010f8]:sw a7, 276(a5)<br>   |
| 146|[0x80005c2c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa86a1651b8f6d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa86a1651b8f6d and rm_val == 3  #nosat<br>                                                                                             |[0x80001108]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000110c]:csrrs a7, fflags, zero<br> [0x80001110]:fsd ft11, 288(a5)<br> [0x80001114]:sw a7, 292(a5)<br>   |
| 147|[0x80005c3c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x0043a4237475b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x0043a4237475b and rm_val == 3  #nosat<br>                                                                                             |[0x80001124]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001128]:csrrs a7, fflags, zero<br> [0x8000112c]:fsd ft11, 304(a5)<br> [0x80001130]:sw a7, 308(a5)<br>   |
| 148|[0x80005c4c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6b764b4a3fc09 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x6b764b4a3fc09 and rm_val == 3  #nosat<br>                                                                                             |[0x80001140]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001144]:csrrs a7, fflags, zero<br> [0x80001148]:fsd ft11, 320(a5)<br> [0x8000114c]:sw a7, 324(a5)<br>   |
| 149|[0x80005c5c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x242628c135d65 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x242628c135d65 and rm_val == 3  #nosat<br>                                                                                             |[0x8000115c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001160]:csrrs a7, fflags, zero<br> [0x80001164]:fsd ft11, 336(a5)<br> [0x80001168]:sw a7, 340(a5)<br>   |
| 150|[0x80005c6c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4afa083bb05d4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4afa083bb05d4 and rm_val == 3  #nosat<br>                                                                                             |[0x80001178]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000117c]:csrrs a7, fflags, zero<br> [0x80001180]:fsd ft11, 352(a5)<br> [0x80001184]:sw a7, 356(a5)<br>   |
| 151|[0x80005c7c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x08290cbe2e23f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x08290cbe2e23f and rm_val == 3  #nosat<br>                                                                                             |[0x80001194]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001198]:csrrs a7, fflags, zero<br> [0x8000119c]:fsd ft11, 368(a5)<br> [0x800011a0]:sw a7, 372(a5)<br>   |
| 152|[0x80005c8c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x95351e6b0b955 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x95351e6b0b955 and rm_val == 3  #nosat<br>                                                                                             |[0x800011b0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 384(a5)<br> [0x800011bc]:sw a7, 388(a5)<br>   |
| 153|[0x80005c9c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb3dca1e26f92c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb3dca1e26f92c and rm_val == 3  #nosat<br>                                                                                             |[0x800011cc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011d0]:csrrs a7, fflags, zero<br> [0x800011d4]:fsd ft11, 400(a5)<br> [0x800011d8]:sw a7, 404(a5)<br>   |
| 154|[0x80005cac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1939e8900399e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1939e8900399e and rm_val == 3  #nosat<br>                                                                                             |[0x800011e8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800011ec]:csrrs a7, fflags, zero<br> [0x800011f0]:fsd ft11, 416(a5)<br> [0x800011f4]:sw a7, 420(a5)<br>   |
| 155|[0x80005cbc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xed40ea1c96a68 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xed40ea1c96a68 and rm_val == 3  #nosat<br>                                                                                             |[0x80001204]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001208]:csrrs a7, fflags, zero<br> [0x8000120c]:fsd ft11, 432(a5)<br> [0x80001210]:sw a7, 436(a5)<br>   |
| 156|[0x80005ccc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x02a602e38e2e5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x02a602e38e2e5 and rm_val == 3  #nosat<br>                                                                                             |[0x80001220]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001224]:csrrs a7, fflags, zero<br> [0x80001228]:fsd ft11, 448(a5)<br> [0x8000122c]:sw a7, 452(a5)<br>   |
| 157|[0x80005cdc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6dfd78772ca12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6dfd78772ca12 and rm_val == 3  #nosat<br>                                                                                             |[0x8000123c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001240]:csrrs a7, fflags, zero<br> [0x80001244]:fsd ft11, 464(a5)<br> [0x80001248]:sw a7, 468(a5)<br>   |
| 158|[0x80005cec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xbb0574c4cc8c3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xbb0574c4cc8c3 and rm_val == 3  #nosat<br>                                                                                             |[0x80001258]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000125c]:csrrs a7, fflags, zero<br> [0x80001260]:fsd ft11, 480(a5)<br> [0x80001264]:sw a7, 484(a5)<br>   |
| 159|[0x80005cfc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x61129e8d25d53 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x61129e8d25d53 and rm_val == 3  #nosat<br>                                                                                             |[0x80001274]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001278]:csrrs a7, fflags, zero<br> [0x8000127c]:fsd ft11, 496(a5)<br> [0x80001280]:sw a7, 500(a5)<br>   |
| 160|[0x80005d0c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae72a87c61e34 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xae72a87c61e34 and rm_val == 3  #nosat<br>                                                                                             |[0x80001290]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 512(a5)<br> [0x8000129c]:sw a7, 516(a5)<br>   |
| 161|[0x80005d1c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9b930ceb054c0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9b930ceb054c0 and rm_val == 3  #nosat<br>                                                                                             |[0x800012ac]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012b0]:csrrs a7, fflags, zero<br> [0x800012b4]:fsd ft11, 528(a5)<br> [0x800012b8]:sw a7, 532(a5)<br>   |
| 162|[0x80005d2c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x43a781e917815 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x43a781e917815 and rm_val == 3  #nosat<br>                                                                                             |[0x800012c8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012cc]:csrrs a7, fflags, zero<br> [0x800012d0]:fsd ft11, 544(a5)<br> [0x800012d4]:sw a7, 548(a5)<br>   |
| 163|[0x80005d3c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd22aa76e3f8bc and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd22aa76e3f8bc and rm_val == 3  #nosat<br>                                                                                             |[0x800012e4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800012e8]:csrrs a7, fflags, zero<br> [0x800012ec]:fsd ft11, 560(a5)<br> [0x800012f0]:sw a7, 564(a5)<br>   |
| 164|[0x80005d4c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc99ac0cd3b3ca and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc99ac0cd3b3ca and rm_val == 3  #nosat<br>                                                                                             |[0x80001300]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001304]:csrrs a7, fflags, zero<br> [0x80001308]:fsd ft11, 576(a5)<br> [0x8000130c]:sw a7, 580(a5)<br>   |
| 165|[0x80005d5c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd6b5f3e68568b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd6b5f3e68568b and rm_val == 3  #nosat<br>                                                                                             |[0x8000131c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001320]:csrrs a7, fflags, zero<br> [0x80001324]:fsd ft11, 592(a5)<br> [0x80001328]:sw a7, 596(a5)<br>   |
| 166|[0x80005d6c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xb5c56d6b2c837 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xb5c56d6b2c837 and rm_val == 3  #nosat<br>                                                                                             |[0x80001338]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000133c]:csrrs a7, fflags, zero<br> [0x80001340]:fsd ft11, 608(a5)<br> [0x80001344]:sw a7, 612(a5)<br>   |
| 167|[0x80005d7c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4a2387765198 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa4a2387765198 and rm_val == 3  #nosat<br>                                                                                             |[0x80001354]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001358]:csrrs a7, fflags, zero<br> [0x8000135c]:fsd ft11, 624(a5)<br> [0x80001360]:sw a7, 628(a5)<br>   |
| 168|[0x80005d8c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8d6b438992705 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x8d6b438992705 and rm_val == 3  #nosat<br>                                                                                             |[0x80001370]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 640(a5)<br> [0x8000137c]:sw a7, 644(a5)<br>   |
| 169|[0x80005d9c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe60134aa9369f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe60134aa9369f and rm_val == 3  #nosat<br>                                                                                             |[0x8000138c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001390]:csrrs a7, fflags, zero<br> [0x80001394]:fsd ft11, 656(a5)<br> [0x80001398]:sw a7, 660(a5)<br>   |
| 170|[0x80005dac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x97081394ff7c0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x97081394ff7c0 and rm_val == 3  #nosat<br>                                                                                             |[0x800013a8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800013ac]:csrrs a7, fflags, zero<br> [0x800013b0]:fsd ft11, 672(a5)<br> [0x800013b4]:sw a7, 676(a5)<br>   |
| 171|[0x80005dbc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3bc28319d6d6f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x3bc28319d6d6f and rm_val == 3  #nosat<br>                                                                                             |[0x800013c4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800013c8]:csrrs a7, fflags, zero<br> [0x800013cc]:fsd ft11, 688(a5)<br> [0x800013d0]:sw a7, 692(a5)<br>   |
| 172|[0x80005dcc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf9196c3c02c3d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf9196c3c02c3d and rm_val == 3  #nosat<br>                                                                                             |[0x800013e0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800013e4]:csrrs a7, fflags, zero<br> [0x800013e8]:fsd ft11, 704(a5)<br> [0x800013ec]:sw a7, 708(a5)<br>   |
| 173|[0x80005ddc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x29cd1fe017e0f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x29cd1fe017e0f and rm_val == 3  #nosat<br>                                                                                             |[0x800013fc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001400]:csrrs a7, fflags, zero<br> [0x80001404]:fsd ft11, 720(a5)<br> [0x80001408]:sw a7, 724(a5)<br>   |
| 174|[0x80005dec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x33bb4c0b03e47 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x33bb4c0b03e47 and rm_val == 3  #nosat<br>                                                                                             |[0x80001418]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000141c]:csrrs a7, fflags, zero<br> [0x80001420]:fsd ft11, 736(a5)<br> [0x80001424]:sw a7, 740(a5)<br>   |
| 175|[0x80005dfc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1a3782778609c and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1a3782778609c and rm_val == 3  #nosat<br>                                                                                             |[0x80001434]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001438]:csrrs a7, fflags, zero<br> [0x8000143c]:fsd ft11, 752(a5)<br> [0x80001440]:sw a7, 756(a5)<br>   |
| 176|[0x80005e0c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3381366daa33 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf3381366daa33 and rm_val == 3  #nosat<br>                                                                                             |[0x80001450]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 768(a5)<br> [0x8000145c]:sw a7, 772(a5)<br>   |
| 177|[0x80005e1c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf5f5f1385c1af and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xf5f5f1385c1af and rm_val == 3  #nosat<br>                                                                                             |[0x8000146c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001470]:csrrs a7, fflags, zero<br> [0x80001474]:fsd ft11, 784(a5)<br> [0x80001478]:sw a7, 788(a5)<br>   |
| 178|[0x80005e2c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x2870c773af305 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x2870c773af305 and rm_val == 3  #nosat<br>                                                                                             |[0x80001488]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000148c]:csrrs a7, fflags, zero<br> [0x80001490]:fsd ft11, 800(a5)<br> [0x80001494]:sw a7, 804(a5)<br>   |
| 179|[0x80005e3c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x565b7f0cebd9f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x565b7f0cebd9f and rm_val == 3  #nosat<br>                                                                                             |[0x800014a4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800014a8]:csrrs a7, fflags, zero<br> [0x800014ac]:fsd ft11, 816(a5)<br> [0x800014b0]:sw a7, 820(a5)<br>   |
| 180|[0x80005e4c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc978dd3af76c1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc978dd3af76c1 and rm_val == 3  #nosat<br>                                                                                             |[0x800014c0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800014c4]:csrrs a7, fflags, zero<br> [0x800014c8]:fsd ft11, 832(a5)<br> [0x800014cc]:sw a7, 836(a5)<br>   |
| 181|[0x80005e5c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x445637e5783c3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x445637e5783c3 and rm_val == 3  #nosat<br>                                                                                             |[0x800014dc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800014e0]:csrrs a7, fflags, zero<br> [0x800014e4]:fsd ft11, 848(a5)<br> [0x800014e8]:sw a7, 852(a5)<br>   |
| 182|[0x80005e6c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3a25a98541333 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3a25a98541333 and rm_val == 3  #nosat<br>                                                                                             |[0x800014f8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800014fc]:csrrs a7, fflags, zero<br> [0x80001500]:fsd ft11, 864(a5)<br> [0x80001504]:sw a7, 868(a5)<br>   |
| 183|[0x80005e7c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe1a7f48e8e26b and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe1a7f48e8e26b and rm_val == 3  #nosat<br>                                                                                             |[0x80001514]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001518]:csrrs a7, fflags, zero<br> [0x8000151c]:fsd ft11, 880(a5)<br> [0x80001520]:sw a7, 884(a5)<br>   |
| 184|[0x80005e8c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4dd45324c2409 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x4dd45324c2409 and rm_val == 3  #nosat<br>                                                                                             |[0x80001530]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 896(a5)<br> [0x8000153c]:sw a7, 900(a5)<br>   |
| 185|[0x80005e9c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf77d273035d94 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf77d273035d94 and rm_val == 3  #nosat<br>                                                                                             |[0x8000154c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001550]:csrrs a7, fflags, zero<br> [0x80001554]:fsd ft11, 912(a5)<br> [0x80001558]:sw a7, 916(a5)<br>   |
| 186|[0x80005eac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4d4955a3d407f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4d4955a3d407f and rm_val == 3  #nosat<br>                                                                                             |[0x80001568]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000156c]:csrrs a7, fflags, zero<br> [0x80001570]:fsd ft11, 928(a5)<br> [0x80001574]:sw a7, 932(a5)<br>   |
| 187|[0x80005ebc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3ab263197fe7f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x3ab263197fe7f and rm_val == 3  #nosat<br>                                                                                             |[0x80001584]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001588]:csrrs a7, fflags, zero<br> [0x8000158c]:fsd ft11, 944(a5)<br> [0x80001590]:sw a7, 948(a5)<br>   |
| 188|[0x80005ecc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x094dd69773d7b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x094dd69773d7b and rm_val == 3  #nosat<br>                                                                                             |[0x800015a0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800015a4]:csrrs a7, fflags, zero<br> [0x800015a8]:fsd ft11, 960(a5)<br> [0x800015ac]:sw a7, 964(a5)<br>   |
| 189|[0x80005edc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x605a6a1e02c96 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x605a6a1e02c96 and rm_val == 3  #nosat<br>                                                                                             |[0x800015bc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800015c0]:csrrs a7, fflags, zero<br> [0x800015c4]:fsd ft11, 976(a5)<br> [0x800015c8]:sw a7, 980(a5)<br>   |
| 190|[0x80005eec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5bc627909931 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf5bc627909931 and rm_val == 3  #nosat<br>                                                                                             |[0x800015d8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800015dc]:csrrs a7, fflags, zero<br> [0x800015e0]:fsd ft11, 992(a5)<br> [0x800015e4]:sw a7, 996(a5)<br>   |
| 191|[0x80005efc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xb8b73fc8fea5b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xb8b73fc8fea5b and rm_val == 3  #nosat<br>                                                                                             |[0x800015f4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800015f8]:csrrs a7, fflags, zero<br> [0x800015fc]:fsd ft11, 1008(a5)<br> [0x80001600]:sw a7, 1012(a5)<br> |
| 192|[0x80005f0c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf0892add2cc6e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf0892add2cc6e and rm_val == 3  #nosat<br>                                                                                             |[0x80001610]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 1024(a5)<br> [0x8000161c]:sw a7, 1028(a5)<br> |
| 193|[0x80005f1c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd3a81e544f745 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd3a81e544f745 and rm_val == 3  #nosat<br>                                                                                             |[0x8000162c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001630]:csrrs a7, fflags, zero<br> [0x80001634]:fsd ft11, 1040(a5)<br> [0x80001638]:sw a7, 1044(a5)<br> |
| 194|[0x80005f2c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x40ccb2b303daf and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x40ccb2b303daf and rm_val == 3  #nosat<br>                                                                                             |[0x80001648]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000164c]:csrrs a7, fflags, zero<br> [0x80001650]:fsd ft11, 1056(a5)<br> [0x80001654]:sw a7, 1060(a5)<br> |
| 195|[0x80005f3c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3171b5147eff2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3171b5147eff2 and rm_val == 3  #nosat<br>                                                                                             |[0x80001664]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001668]:csrrs a7, fflags, zero<br> [0x8000166c]:fsd ft11, 1072(a5)<br> [0x80001670]:sw a7, 1076(a5)<br> |
| 196|[0x80005f4c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf715337b3d172 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf715337b3d172 and rm_val == 3  #nosat<br>                                                                                             |[0x80001680]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001684]:csrrs a7, fflags, zero<br> [0x80001688]:fsd ft11, 1088(a5)<br> [0x8000168c]:sw a7, 1092(a5)<br> |
| 197|[0x80005f5c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x44919c1beab5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x44919c1beab5f and rm_val == 3  #nosat<br>                                                                                             |[0x8000169c]:fsub.d ft11, ft10, ft9, dyn<br> [0x800016a0]:csrrs a7, fflags, zero<br> [0x800016a4]:fsd ft11, 1104(a5)<br> [0x800016a8]:sw a7, 1108(a5)<br> |
| 198|[0x80005f6c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x9a21046a4c767 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x9a21046a4c767 and rm_val == 3  #nosat<br>                                                                                             |[0x800016b8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800016bc]:csrrs a7, fflags, zero<br> [0x800016c0]:fsd ft11, 1120(a5)<br> [0x800016c4]:sw a7, 1124(a5)<br> |
| 199|[0x80005f7c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x037df25b16113 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x037df25b16113 and rm_val == 3  #nosat<br>                                                                                             |[0x800016d4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 1136(a5)<br> [0x800016e0]:sw a7, 1140(a5)<br> |
| 200|[0x80005f8c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd185a4345fd91 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd185a4345fd91 and rm_val == 3  #nosat<br>                                                                                             |[0x800016f0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800016f4]:csrrs a7, fflags, zero<br> [0x800016f8]:fsd ft11, 1152(a5)<br> [0x800016fc]:sw a7, 1156(a5)<br> |
| 201|[0x80005f9c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x835b1de73afa3 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x835b1de73afa3 and rm_val == 3  #nosat<br>                                                                                             |[0x8000170c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001710]:csrrs a7, fflags, zero<br> [0x80001714]:fsd ft11, 1168(a5)<br> [0x80001718]:sw a7, 1172(a5)<br> |
| 202|[0x80005fac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xa5356adec5cbf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xa5356adec5cbf and rm_val == 3  #nosat<br>                                                                                             |[0x80001728]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000172c]:csrrs a7, fflags, zero<br> [0x80001730]:fsd ft11, 1184(a5)<br> [0x80001734]:sw a7, 1188(a5)<br> |
| 203|[0x80005fbc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xee6dc228b09a7 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xee6dc228b09a7 and rm_val == 3  #nosat<br>                                                                                             |[0x80001744]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001748]:csrrs a7, fflags, zero<br> [0x8000174c]:fsd ft11, 1200(a5)<br> [0x80001750]:sw a7, 1204(a5)<br> |
| 204|[0x80005fcc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd95388e6dd7e7 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd95388e6dd7e7 and rm_val == 3  #nosat<br>                                                                                             |[0x80001760]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001764]:csrrs a7, fflags, zero<br> [0x80001768]:fsd ft11, 1216(a5)<br> [0x8000176c]:sw a7, 1220(a5)<br> |
| 205|[0x80005fdc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf9efe9258e03a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xf9efe9258e03a and rm_val == 3  #nosat<br>                                                                                             |[0x8000177c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001780]:csrrs a7, fflags, zero<br> [0x80001784]:fsd ft11, 1232(a5)<br> [0x80001788]:sw a7, 1236(a5)<br> |
| 206|[0x80005fec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x47df70c06ea5f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x47df70c06ea5f and rm_val == 3  #nosat<br>                                                                                             |[0x80001798]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000179c]:csrrs a7, fflags, zero<br> [0x800017a0]:fsd ft11, 1248(a5)<br> [0x800017a4]:sw a7, 1252(a5)<br> |
| 207|[0x80005ffc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xd775b7a6f9327 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xd775b7a6f9327 and rm_val == 3  #nosat<br>                                                                                             |[0x800017b4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:fsd ft11, 1264(a5)<br> [0x800017c0]:sw a7, 1268(a5)<br> |
| 208|[0x8000600c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x574031c0ee5b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x574031c0ee5b5 and rm_val == 3  #nosat<br>                                                                                             |[0x800017d0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800017d4]:csrrs a7, fflags, zero<br> [0x800017d8]:fsd ft11, 1280(a5)<br> [0x800017dc]:sw a7, 1284(a5)<br> |
| 209|[0x8000601c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa7d13a52ed5ec and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xa7d13a52ed5ec and rm_val == 3  #nosat<br>                                                                                             |[0x800017ec]:fsub.d ft11, ft10, ft9, dyn<br> [0x800017f0]:csrrs a7, fflags, zero<br> [0x800017f4]:fsd ft11, 1296(a5)<br> [0x800017f8]:sw a7, 1300(a5)<br> |
| 210|[0x8000602c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x1bd5cc8dca1e5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1bd5cc8dca1e5 and rm_val == 3  #nosat<br>                                                                                             |[0x80001808]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000180c]:csrrs a7, fflags, zero<br> [0x80001810]:fsd ft11, 1312(a5)<br> [0x80001814]:sw a7, 1316(a5)<br> |
| 211|[0x8000603c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd9a2688750f46 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd9a2688750f46 and rm_val == 3  #nosat<br>                                                                                             |[0x80001824]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001828]:csrrs a7, fflags, zero<br> [0x8000182c]:fsd ft11, 1328(a5)<br> [0x80001830]:sw a7, 1332(a5)<br> |
| 212|[0x8000604c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc812c292ea556 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc812c292ea556 and rm_val == 3  #nosat<br>                                                                                             |[0x80001840]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001844]:csrrs a7, fflags, zero<br> [0x80001848]:fsd ft11, 1344(a5)<br> [0x8000184c]:sw a7, 1348(a5)<br> |
| 213|[0x8000605c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x4ed205e78cd0f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x4ed205e78cd0f and rm_val == 3  #nosat<br>                                                                                             |[0x8000185c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001860]:csrrs a7, fflags, zero<br> [0x80001864]:fsd ft11, 1360(a5)<br> [0x80001868]:sw a7, 1364(a5)<br> |
| 214|[0x8000606c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x243d79e337b38 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x243d79e337b38 and rm_val == 3  #nosat<br>                                                                                             |[0x80001878]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000187c]:csrrs a7, fflags, zero<br> [0x80001880]:fsd ft11, 1376(a5)<br> [0x80001884]:sw a7, 1380(a5)<br> |
| 215|[0x8000607c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9055ab3b464b5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9055ab3b464b5 and rm_val == 3  #nosat<br>                                                                                             |[0x80001894]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001898]:csrrs a7, fflags, zero<br> [0x8000189c]:fsd ft11, 1392(a5)<br> [0x800018a0]:sw a7, 1396(a5)<br> |
| 216|[0x8000608c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5d14398eae23f and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5d14398eae23f and rm_val == 3  #nosat<br>                                                                                             |[0x800018b0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800018b4]:csrrs a7, fflags, zero<br> [0x800018b8]:fsd ft11, 1408(a5)<br> [0x800018bc]:sw a7, 1412(a5)<br> |
| 217|[0x8000609c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9383ffc96dd3f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9383ffc96dd3f and rm_val == 3  #nosat<br>                                                                                             |[0x800018cc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800018d0]:csrrs a7, fflags, zero<br> [0x800018d4]:fsd ft11, 1424(a5)<br> [0x800018d8]:sw a7, 1428(a5)<br> |
| 218|[0x800060ac]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x2bccdcc2ad897 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x2bccdcc2ad897 and rm_val == 3  #nosat<br>                                                                                             |[0x800018e8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800018ec]:csrrs a7, fflags, zero<br> [0x800018f0]:fsd ft11, 1440(a5)<br> [0x800018f4]:sw a7, 1444(a5)<br> |
| 219|[0x800060bc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xab1c42a43630f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xab1c42a43630f and rm_val == 3  #nosat<br>                                                                                             |[0x80001904]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001908]:csrrs a7, fflags, zero<br> [0x8000190c]:fsd ft11, 1456(a5)<br> [0x80001910]:sw a7, 1460(a5)<br> |
| 220|[0x800060cc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x153045947810b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x153045947810b and rm_val == 3  #nosat<br>                                                                                             |[0x80001920]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001924]:csrrs a7, fflags, zero<br> [0x80001928]:fsd ft11, 1472(a5)<br> [0x8000192c]:sw a7, 1476(a5)<br> |
| 221|[0x800060dc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe89afcadc456f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe89afcadc456f and rm_val == 3  #nosat<br>                                                                                             |[0x8000193c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001940]:csrrs a7, fflags, zero<br> [0x80001944]:fsd ft11, 1488(a5)<br> [0x80001948]:sw a7, 1492(a5)<br> |
| 222|[0x800060ec]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xbc1e737c6a698 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbc1e737c6a698 and rm_val == 3  #nosat<br>                                                                                             |[0x80001958]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000195c]:csrrs a7, fflags, zero<br> [0x80001960]:fsd ft11, 1504(a5)<br> [0x80001964]:sw a7, 1508(a5)<br> |
| 223|[0x800060fc]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5c73bb8e94b2b and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5c73bb8e94b2b and rm_val == 3  #nosat<br>                                                                                             |[0x80001974]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:fsd ft11, 1520(a5)<br> [0x80001980]:sw a7, 1524(a5)<br> |
| 224|[0x8000610c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xaea8e11056b0f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xaea8e11056b0f and rm_val == 3  #nosat<br>                                                                                             |[0x80001990]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001994]:csrrs a7, fflags, zero<br> [0x80001998]:fsd ft11, 1536(a5)<br> [0x8000199c]:sw a7, 1540(a5)<br> |
| 225|[0x8000611c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x84aae05543502 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x84aae05543502 and rm_val == 3  #nosat<br>                                                                                             |[0x800019ac]:fsub.d ft11, ft10, ft9, dyn<br> [0x800019b0]:csrrs a7, fflags, zero<br> [0x800019b4]:fsd ft11, 1552(a5)<br> [0x800019b8]:sw a7, 1556(a5)<br> |
| 226|[0x8000612c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xd26cfda272030 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xd26cfda272030 and rm_val == 3  #nosat<br>                                                                                             |[0x800019c8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800019cc]:csrrs a7, fflags, zero<br> [0x800019d0]:fsd ft11, 1568(a5)<br> [0x800019d4]:sw a7, 1572(a5)<br> |
| 227|[0x8000613c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x5ad9a8441acdf and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x5ad9a8441acdf and rm_val == 3  #nosat<br>                                                                                             |[0x800019e4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800019e8]:csrrs a7, fflags, zero<br> [0x800019ec]:fsd ft11, 1584(a5)<br> [0x800019f0]:sw a7, 1588(a5)<br> |
| 228|[0x8000614c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xe08b2a10b8fdf and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xe08b2a10b8fdf and rm_val == 3  #nosat<br>                                                                                             |[0x80001a00]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a04]:csrrs a7, fflags, zero<br> [0x80001a08]:fsd ft11, 1600(a5)<br> [0x80001a0c]:sw a7, 1604(a5)<br> |
| 229|[0x8000615c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf0206ee24c395 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xf0206ee24c395 and rm_val == 3  #nosat<br>                                                                                             |[0x80001a1c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a20]:csrrs a7, fflags, zero<br> [0x80001a24]:fsd ft11, 1616(a5)<br> [0x80001a28]:sw a7, 1620(a5)<br> |
| 230|[0x8000616c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3c90ab59cc1f and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc3c90ab59cc1f and rm_val == 3  #nosat<br>                                                                                             |[0x80001a38]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a3c]:csrrs a7, fflags, zero<br> [0x80001a40]:fsd ft11, 1632(a5)<br> [0x80001a44]:sw a7, 1636(a5)<br> |
| 231|[0x8000617c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdd47ad230c500 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xdd47ad230c500 and rm_val == 3  #nosat<br>                                                                                             |[0x80001a54]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:fsd ft11, 1648(a5)<br> [0x80001a60]:sw a7, 1652(a5)<br> |
| 232|[0x8000618c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x59522cc62b803 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x59522cc62b803 and rm_val == 3  #nosat<br>                                                                                             |[0x80001a70]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a74]:csrrs a7, fflags, zero<br> [0x80001a78]:fsd ft11, 1664(a5)<br> [0x80001a7c]:sw a7, 1668(a5)<br> |
| 233|[0x8000619c]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x5b3be3b6f1597 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x5b3be3b6f1597 and rm_val == 3  #nosat<br>                                                                                             |[0x80001a8c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001a90]:csrrs a7, fflags, zero<br> [0x80001a94]:fsd ft11, 1680(a5)<br> [0x80001a98]:sw a7, 1684(a5)<br> |
| 234|[0x80005f04]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x946024d663351 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x946024d663351 and rm_val == 3  #nosat<br>                                                                                             |[0x80001d00]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d04]:csrrs a7, fflags, zero<br> [0x80001d08]:fsd ft11, 0(a5)<br> [0x80001d0c]:sw a7, 4(a5)<br>       |
| 235|[0x80005f14]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc91ade861e02b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc91ade861e02b and rm_val == 3  #nosat<br>                                                                                             |[0x80001d1c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d20]:csrrs a7, fflags, zero<br> [0x80001d24]:fsd ft11, 16(a5)<br> [0x80001d28]:sw a7, 20(a5)<br>     |
| 236|[0x80005f24]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x987aaa2c7bb6a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x987aaa2c7bb6a and rm_val == 3  #nosat<br>                                                                                             |[0x80001d38]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d3c]:csrrs a7, fflags, zero<br> [0x80001d40]:fsd ft11, 32(a5)<br> [0x80001d44]:sw a7, 36(a5)<br>     |
| 237|[0x80005f34]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc3c9ccfa1b1bb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc3c9ccfa1b1bb and rm_val == 3  #nosat<br>                                                                                             |[0x80001d54]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d58]:csrrs a7, fflags, zero<br> [0x80001d5c]:fsd ft11, 48(a5)<br> [0x80001d60]:sw a7, 52(a5)<br>     |
| 238|[0x80005f44]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x5b39db9b4e7ac and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x5b39db9b4e7ac and rm_val == 3  #nosat<br>                                                                                             |[0x80001d70]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d74]:csrrs a7, fflags, zero<br> [0x80001d78]:fsd ft11, 64(a5)<br> [0x80001d7c]:sw a7, 68(a5)<br>     |
| 239|[0x80005f54]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x482567721754b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x482567721754b and rm_val == 3  #nosat<br>                                                                                             |[0x80001d8c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001d90]:csrrs a7, fflags, zero<br> [0x80001d94]:fsd ft11, 80(a5)<br> [0x80001d98]:sw a7, 84(a5)<br>     |
| 240|[0x80005f64]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x2bf422090b207 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2bf422090b207 and rm_val == 3  #nosat<br>                                                                                             |[0x80001da8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001dac]:csrrs a7, fflags, zero<br> [0x80001db0]:fsd ft11, 96(a5)<br> [0x80001db4]:sw a7, 100(a5)<br>    |
| 241|[0x80005f74]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x551579cd90e3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x551579cd90e3f and rm_val == 3  #nosat<br>                                                                                             |[0x80001dc4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001dc8]:csrrs a7, fflags, zero<br> [0x80001dcc]:fsd ft11, 112(a5)<br> [0x80001dd0]:sw a7, 116(a5)<br>   |
| 242|[0x80005f84]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x3617941ba03e8 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x3617941ba03e8 and rm_val == 3  #nosat<br>                                                                                             |[0x80001de0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001de4]:csrrs a7, fflags, zero<br> [0x80001de8]:fsd ft11, 128(a5)<br> [0x80001dec]:sw a7, 132(a5)<br>   |
| 243|[0x80005f94]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x052debfe82e13 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x052debfe82e13 and rm_val == 3  #nosat<br>                                                                                             |[0x80001dfc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e00]:csrrs a7, fflags, zero<br> [0x80001e04]:fsd ft11, 144(a5)<br> [0x80001e08]:sw a7, 148(a5)<br>   |
| 244|[0x80005fa4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xbd7ce681c543f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xbd7ce681c543f and rm_val == 3  #nosat<br>                                                                                             |[0x80001e18]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e1c]:csrrs a7, fflags, zero<br> [0x80001e20]:fsd ft11, 160(a5)<br> [0x80001e24]:sw a7, 164(a5)<br>   |
| 245|[0x80005fb4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xaf054e65e9fad and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xaf054e65e9fad and rm_val == 3  #nosat<br>                                                                                             |[0x80001e34]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e38]:csrrs a7, fflags, zero<br> [0x80001e3c]:fsd ft11, 176(a5)<br> [0x80001e40]:sw a7, 180(a5)<br>   |
| 246|[0x80005fc4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x833a9a7efc6ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x833a9a7efc6ff and rm_val == 3  #nosat<br>                                                                                             |[0x80001e50]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e54]:csrrs a7, fflags, zero<br> [0x80001e58]:fsd ft11, 192(a5)<br> [0x80001e5c]:sw a7, 196(a5)<br>   |
| 247|[0x80005fd4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x49bad4bf8d1a9 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x49bad4bf8d1a9 and rm_val == 3  #nosat<br>                                                                                             |[0x80001e6c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e70]:csrrs a7, fflags, zero<br> [0x80001e74]:fsd ft11, 208(a5)<br> [0x80001e78]:sw a7, 212(a5)<br>   |
| 248|[0x80005fe4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x6c5583d2d8f82 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x6c5583d2d8f82 and rm_val == 3  #nosat<br>                                                                                             |[0x80001e88]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001e8c]:csrrs a7, fflags, zero<br> [0x80001e90]:fsd ft11, 224(a5)<br> [0x80001e94]:sw a7, 228(a5)<br>   |
| 249|[0x80005ff4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x585c60a81aa3f and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x585c60a81aa3f and rm_val == 3  #nosat<br>                                                                                             |[0x80001ea4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001ea8]:csrrs a7, fflags, zero<br> [0x80001eac]:fsd ft11, 240(a5)<br> [0x80001eb0]:sw a7, 244(a5)<br>   |
| 250|[0x80006004]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x46e9bf4155d7b and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x46e9bf4155d7b and rm_val == 3  #nosat<br>                                                                                             |[0x80001ec0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001ec4]:csrrs a7, fflags, zero<br> [0x80001ec8]:fsd ft11, 256(a5)<br> [0x80001ecc]:sw a7, 260(a5)<br>   |
| 251|[0x80006014]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x60b0632528095 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x60b0632528095 and rm_val == 3  #nosat<br>                                                                                             |[0x80001edc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001ee0]:csrrs a7, fflags, zero<br> [0x80001ee4]:fsd ft11, 272(a5)<br> [0x80001ee8]:sw a7, 276(a5)<br>   |
| 252|[0x80006024]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xc07725983617f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xc07725983617f and rm_val == 3  #nosat<br>                                                                                             |[0x80001ef8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001efc]:csrrs a7, fflags, zero<br> [0x80001f00]:fsd ft11, 288(a5)<br> [0x80001f04]:sw a7, 292(a5)<br>   |
| 253|[0x80006034]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x82dc4511ff204 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x82dc4511ff204 and rm_val == 3  #nosat<br>                                                                                             |[0x80001f14]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001f18]:csrrs a7, fflags, zero<br> [0x80001f1c]:fsd ft11, 304(a5)<br> [0x80001f20]:sw a7, 308(a5)<br>   |
| 254|[0x80006044]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x1175939fbdd3f and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0x1175939fbdd3f and rm_val == 3  #nosat<br>                                                                                             |[0x80001f30]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001f34]:csrrs a7, fflags, zero<br> [0x80001f38]:fsd ft11, 320(a5)<br> [0x80001f3c]:sw a7, 324(a5)<br>   |
| 255|[0x80006054]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x58a25604824f3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x58a25604824f3 and rm_val == 3  #nosat<br>                                                                                             |[0x80001f4c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001f50]:csrrs a7, fflags, zero<br> [0x80001f54]:fsd ft11, 336(a5)<br> [0x80001f58]:sw a7, 340(a5)<br>   |
| 256|[0x80006064]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x056bcd04279ed and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x056bcd04279ed and rm_val == 3  #nosat<br>                                                                                             |[0x80001f68]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001f6c]:csrrs a7, fflags, zero<br> [0x80001f70]:fsd ft11, 352(a5)<br> [0x80001f74]:sw a7, 356(a5)<br>   |
| 257|[0x80006074]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x981d2bf67b45e and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x981d2bf67b45e and rm_val == 3  #nosat<br>                                                                                             |[0x80001f84]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001f88]:csrrs a7, fflags, zero<br> [0x80001f8c]:fsd ft11, 368(a5)<br> [0x80001f90]:sw a7, 372(a5)<br>   |
| 258|[0x80006084]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb8d62745dccc1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb8d62745dccc1 and rm_val == 3  #nosat<br>                                                                                             |[0x80001fa0]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001fa4]:csrrs a7, fflags, zero<br> [0x80001fa8]:fsd ft11, 384(a5)<br> [0x80001fac]:sw a7, 388(a5)<br>   |
| 259|[0x80006094]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6aedbc8cfe5cb and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x6aedbc8cfe5cb and rm_val == 3  #nosat<br>                                                                                             |[0x80001fbc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001fc0]:csrrs a7, fflags, zero<br> [0x80001fc4]:fsd ft11, 400(a5)<br> [0x80001fc8]:sw a7, 404(a5)<br>   |
| 260|[0x800060a4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xca57966fc21ff and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xca57966fc21ff and rm_val == 3  #nosat<br>                                                                                             |[0x80001fd8]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001fdc]:csrrs a7, fflags, zero<br> [0x80001fe0]:fsd ft11, 416(a5)<br> [0x80001fe4]:sw a7, 420(a5)<br>   |
| 261|[0x800060b4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x39935e95315b1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x39935e95315b1 and rm_val == 3  #nosat<br>                                                                                             |[0x80001ff4]:fsub.d ft11, ft10, ft9, dyn<br> [0x80001ff8]:csrrs a7, fflags, zero<br> [0x80001ffc]:fsd ft11, 432(a5)<br> [0x80002000]:sw a7, 436(a5)<br>   |
| 262|[0x800060c4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x13b37e2291279 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x13b37e2291279 and rm_val == 3  #nosat<br>                                                                                             |[0x80002010]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002014]:csrrs a7, fflags, zero<br> [0x80002018]:fsd ft11, 448(a5)<br> [0x8000201c]:sw a7, 452(a5)<br>   |
| 263|[0x800060d4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x6d5a59350bdcb and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x6d5a59350bdcb and rm_val == 3  #nosat<br>                                                                                             |[0x8000202c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002030]:csrrs a7, fflags, zero<br> [0x80002034]:fsd ft11, 464(a5)<br> [0x80002038]:sw a7, 468(a5)<br>   |
| 264|[0x800060e4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x026ab89a75256 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x026ab89a75256 and rm_val == 3  #nosat<br>                                                                                             |[0x80002048]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000204c]:csrrs a7, fflags, zero<br> [0x80002050]:fsd ft11, 480(a5)<br> [0x80002054]:sw a7, 484(a5)<br>   |
| 265|[0x800060f4]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x23d6f3e37b4f1 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x23d6f3e37b4f1 and rm_val == 3  #nosat<br>                                                                                             |[0x80002064]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002068]:csrrs a7, fflags, zero<br> [0x8000206c]:fsd ft11, 496(a5)<br> [0x80002070]:sw a7, 500(a5)<br>   |
| 266|[0x80006104]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xcbc315eca5f3f and fs2 == 0 and fe2 == 0x7fa and fm2 == 0xcbc315eca5f3f and rm_val == 3  #nosat<br>                                                                                             |[0x80002080]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002084]:csrrs a7, fflags, zero<br> [0x80002088]:fsd ft11, 512(a5)<br> [0x8000208c]:sw a7, 516(a5)<br>   |
| 267|[0x80006114]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1f6a4c4d26ab9 and rm_val == 3  #nosat<br>                                                                                             |[0x8000209c]:fsub.d ft11, ft10, ft9, dyn<br> [0x800020a0]:csrrs a7, fflags, zero<br> [0x800020a4]:fsd ft11, 528(a5)<br> [0x800020a8]:sw a7, 532(a5)<br>   |
| 268|[0x80006124]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9e4795c8459f5 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x9e4795c8459f5 and rm_val == 3  #nosat<br>                                                                                             |[0x800020b8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800020bc]:csrrs a7, fflags, zero<br> [0x800020c0]:fsd ft11, 544(a5)<br> [0x800020c4]:sw a7, 548(a5)<br>   |
| 269|[0x80006134]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xc5b9547c0fb71 and rm_val == 3  #nosat<br>                                                                                             |[0x800020d4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800020d8]:csrrs a7, fflags, zero<br> [0x800020dc]:fsd ft11, 560(a5)<br> [0x800020e0]:sw a7, 564(a5)<br>   |
| 270|[0x80006144]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x913b4236d8411 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x913b4236d8411 and rm_val == 3  #nosat<br>                                                                                             |[0x800020f0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800020f4]:csrrs a7, fflags, zero<br> [0x800020f8]:fsd ft11, 576(a5)<br> [0x800020fc]:sw a7, 580(a5)<br>   |
| 271|[0x80006154]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xea0b252eae7e0 and rm_val == 3  #nosat<br>                                                                                             |[0x8000210c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002110]:csrrs a7, fflags, zero<br> [0x80002114]:fsd ft11, 592(a5)<br> [0x80002118]:sw a7, 596(a5)<br>   |
| 272|[0x80006164]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x13bdffd461269 and rm_val == 3  #nosat<br>                                                                                             |[0x80002128]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000212c]:csrrs a7, fflags, zero<br> [0x80002130]:fsd ft11, 608(a5)<br> [0x80002134]:sw a7, 612(a5)<br>   |
| 273|[0x80006174]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x27d4b8969c0b2 and rm_val == 3  #nosat<br>                                                                                             |[0x80002144]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002148]:csrrs a7, fflags, zero<br> [0x8000214c]:fsd ft11, 624(a5)<br> [0x80002150]:sw a7, 628(a5)<br>   |
| 274|[0x80006184]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x070d1456013e3 and rm_val == 3  #nosat<br>                                                                                             |[0x80002160]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002164]:csrrs a7, fflags, zero<br> [0x80002168]:fsd ft11, 640(a5)<br> [0x8000216c]:sw a7, 644(a5)<br>   |
| 275|[0x80006194]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xb877e6e317fa2 and rm_val == 3  #nosat<br>                                                                                             |[0x8000217c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80002180]:csrrs a7, fflags, zero<br> [0x80002184]:fsd ft11, 656(a5)<br> [0x80002188]:sw a7, 660(a5)<br>   |
