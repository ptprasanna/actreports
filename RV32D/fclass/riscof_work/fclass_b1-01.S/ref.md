
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000460')]      |
| SIG_REGION                | [('0x80002310', '0x80002420', '68 words')]      |
| COV_LABELS                | fclass_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fclass/riscof_work/fclass_b1-01.S/ref.S    |
| Total Number of coverpoints| 93     |
| Total Coverpoints Hit     | 83      |
| Total Signature Updates   | 59      |
| STAT1                     | 29      |
| STAT2                     | 1      |
| STAT3                     | 3     |
| STAT4                     | 29     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000044c]:fclass.d t6, ft11
      [0x80000450]:csrrs a7, fflags, zero
      [0x80000454]:sw t6, 32(a5)
 -- Signature Address: 0x80002420 Data: 0x00000002
 -- Redundant Coverpoints hit by the op
      - opcode : fclass.d
      - rd : x31
      - rs1 : f31
      - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 1  #nosat






```

## Details of STAT3

```
[0x800003a4]:fclass.d a3, fa6
[0x800003a8]:csrrs a7, fflags, zero
[0x800003ac]:sw a3, 144(a5)
[0x800003b0]:sw a7, 148(a5)
[0x800003b4]:fld ft6, 216(a6)
[0x800003b8]:csrrwi zero, frm, 0

[0x800003bc]:fclass.d s2, ft6
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:sw s2, 160(a5)
[0x800003c8]:sw a7, 164(a5)
[0x800003cc]:fld fa4, 224(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fclass.d s11, fa4
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:sw s11, 176(a5)
[0x800003e0]:sw a7, 180(a5)
[0x800003e4]:add s4, a6, zero
[0x800003e8]:auipc s3, 2
[0x800003ec]:addi s3, s3, 16
[0x800003f0]:fld fa2, 232(s4)
[0x800003f4]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fclass.d', 'rd : x2', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fclass.d sp, ft7
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw sp, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000000




Last Coverpoint : ['rd : x0', 'rs1 : f13', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000134]:fclass.d zero, fa3
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw zero, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002324]:0x00000000




Last Coverpoint : ['rd : x3', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fclass.d gp, fa7
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw gp, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002334]:0x00000000




Last Coverpoint : ['rd : x22', 'rs1 : f20', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000164]:fclass.d s6, fs4
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s6, 48(a5)
Current Store : [0x80000170] : sw a7, 52(a5) -- Store: [0x80002344]:0x00000000




Last Coverpoint : ['rd : x11', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fclass.d a1, ft3
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw a1, 64(a5)
Current Store : [0x80000188] : sw a7, 68(a5) -- Store: [0x80002354]:0x00000000




Last Coverpoint : ['rd : x21', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000194]:fclass.d s5, ft11
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw s5, 80(a5)
Current Store : [0x800001a0] : sw a7, 84(a5) -- Store: [0x80002364]:0x00000000




Last Coverpoint : ['rd : x19', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fclass.d s3, fs7
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw s3, 96(a5)
Current Store : [0x800001b8] : sw a7, 100(a5) -- Store: [0x80002374]:0x00000000




Last Coverpoint : ['rd : x12', 'rs1 : f25', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fclass.d a2, fs9
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw a2, 112(a5)
Current Store : [0x800001d0] : sw a7, 116(a5) -- Store: [0x80002384]:0x00000000




Last Coverpoint : ['rd : x30', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fclass.d t5, fa1
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw t5, 128(a5)
Current Store : [0x800001e8] : sw a7, 132(a5) -- Store: [0x80002394]:0x00000000




Last Coverpoint : ['rd : x24', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fclass.d s8, ft10
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw s8, 144(a5)
Current Store : [0x80000200] : sw a7, 148(a5) -- Store: [0x800023a4]:0x00000000




Last Coverpoint : ['rd : x7', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fclass.d t2, ft5
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw t2, 160(a5)
Current Store : [0x80000218] : sw a7, 164(a5) -- Store: [0x800023b4]:0x00000000




Last Coverpoint : ['rd : x25', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000224]:fclass.d s9, ft0
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw s9, 176(a5)
Current Store : [0x80000230] : sw a7, 180(a5) -- Store: [0x800023c4]:0x00000000




Last Coverpoint : ['rd : x31', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fclass.d t6, fs3
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw t6, 192(a5)
Current Store : [0x80000248] : sw a7, 196(a5) -- Store: [0x800023d4]:0x00000000




Last Coverpoint : ['rd : x9', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000254]:fclass.d s1, ft4
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw s1, 208(a5)
Current Store : [0x80000260] : sw a7, 212(a5) -- Store: [0x800023e4]:0x00000000




Last Coverpoint : ['rd : x29', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fclass.d t4, ft1
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw t4, 224(a5)
Current Store : [0x80000278] : sw a7, 228(a5) -- Store: [0x800023f4]:0x00000000




Last Coverpoint : ['rd : x16', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000290]:fclass.d a6, fs10
	-[0x80000294]:csrrs s5, fflags, zero
	-[0x80000298]:sw a6, 0(s3)
Current Store : [0x8000029c] : sw s5, 4(s3) -- Store: [0x8000238c]:0x00000000




Last Coverpoint : ['rd : x17', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800002a8]:fclass.d a7, fs8
	-[0x800002ac]:csrrs s5, fflags, zero
	-[0x800002b0]:sw a7, 16(s3)
Current Store : [0x800002b4] : sw s5, 20(s3) -- Store: [0x8000239c]:0x00000000




Last Coverpoint : ['rd : x14', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fclass.d a4, fa5
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw a4, 0(a5)
Current Store : [0x800002d8] : sw a7, 4(a5) -- Store: [0x8000239c]:0x00000000




Last Coverpoint : ['rd : x8', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fclass.d fp, fa0
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw fp, 16(a5)
Current Store : [0x800002f0] : sw a7, 20(a5) -- Store: [0x800023ac]:0x00000000




Last Coverpoint : ['rd : x5', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fclass.d t0, ft8
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw t0, 32(a5)
Current Store : [0x80000308] : sw a7, 36(a5) -- Store: [0x800023bc]:0x00000000




Last Coverpoint : ['rd : x10', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000314]:fclass.d a0, fs2
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw a0, 48(a5)
Current Store : [0x80000320] : sw a7, 52(a5) -- Store: [0x800023cc]:0x00000000




Last Coverpoint : ['rd : x20', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fclass.d s4, ft9
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw s4, 64(a5)
Current Store : [0x80000338] : sw a7, 68(a5) -- Store: [0x800023dc]:0x00000000




Last Coverpoint : ['rd : x1', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000344]:fclass.d ra, fs6
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw ra, 80(a5)
Current Store : [0x80000350] : sw a7, 84(a5) -- Store: [0x800023ec]:0x00000000




Last Coverpoint : ['rd : x26', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fclass.d s10, ft2
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw s10, 96(a5)
Current Store : [0x80000368] : sw a7, 100(a5) -- Store: [0x800023fc]:0x00000000




Last Coverpoint : ['rd : x6', 'rs1 : f9']
Last Code Sequence : 
	-[0x80000374]:fclass.d t1, fs1
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw t1, 112(a5)
Current Store : [0x80000380] : sw a7, 116(a5) -- Store: [0x8000240c]:0x00000000




Last Coverpoint : ['rd : x23', 'rs1 : f21']
Last Code Sequence : 
	-[0x8000038c]:fclass.d s7, fs5
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw s7, 128(a5)
Current Store : [0x80000398] : sw a7, 132(a5) -- Store: [0x8000241c]:0x00000000




Last Coverpoint : ['rd : x15', 'rs1 : f12']
Last Code Sequence : 
	-[0x800003f8]:fclass.d a5, fa2
	-[0x800003fc]:csrrs s5, fflags, zero
	-[0x80000400]:sw a5, 0(s3)
Current Store : [0x80000404] : sw s5, 4(s3) -- Store: [0x800023fc]:0x00000000




Last Coverpoint : ['rd : x4', 'rs1 : f27']
Last Code Sequence : 
	-[0x8000041c]:fclass.d tp, fs11
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw tp, 0(a5)
Current Store : [0x80000428] : sw a7, 4(a5) -- Store: [0x80002404]:0x00000000




Last Coverpoint : ['rd : x28', 'rs1 : f8']
Last Code Sequence : 
	-[0x80000434]:fclass.d t3, fs0
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw t3, 16(a5)
Current Store : [0x80000440] : sw a7, 20(a5) -- Store: [0x80002414]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                              coverpoints                                                               |                                                    code                                                     |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000010|- opcode : fclass.d<br> - rd : x2<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br> |[0x8000011c]:fclass.d sp, ft7<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw sp, 0(a5)<br>      |
|   2|[0x80002320]<br>0x00000000|- rd : x0<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                        |[0x80000134]:fclass.d zero, fa3<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw zero, 16(a5)<br> |
|   3|[0x80002330]<br>0x00000040|- rd : x3<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                        |[0x8000014c]:fclass.d gp, fa7<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw gp, 32(a5)<br>     |
|   4|[0x80002340]<br>0x00000100|- rd : x22<br> - rs1 : f20<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                       |[0x80000164]:fclass.d s6, fs4<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s6, 48(a5)<br>     |
|   5|[0x80002350]<br>0x00000100|- rd : x11<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                        |[0x8000017c]:fclass.d a1, ft3<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw a1, 64(a5)<br>     |
|   6|[0x80002360]<br>0x00000200|- rd : x21<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 1  #nosat<br>                       |[0x80000194]:fclass.d s5, ft11<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw s5, 80(a5)<br>    |
|   7|[0x80002370]<br>0x00000200|- rd : x19<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 1  #nosat<br>                       |[0x800001ac]:fclass.d s3, fs7<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw s3, 96(a5)<br>     |
|   8|[0x80002380]<br>0x00000200|- rd : x12<br> - rs1 : f25<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 1  #nosat<br>                       |[0x800001c4]:fclass.d a2, fs9<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw a2, 112(a5)<br>    |
|   9|[0x80002390]<br>0x00000200|- rd : x30<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 1  #nosat<br>                       |[0x800001dc]:fclass.d t5, fa1<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw t5, 128(a5)<br>    |
|  10|[0x800023a0]<br>0x00000001|- rd : x24<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                       |[0x800001f4]:fclass.d s8, ft10<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw s8, 144(a5)<br>   |
|  11|[0x800023b0]<br>0x00000080|- rd : x7<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                         |[0x8000020c]:fclass.d t2, ft5<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw t2, 160(a5)<br>    |
|  12|[0x800023c0]<br>0x00000002|- rd : x25<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                        |[0x80000224]:fclass.d s9, ft0<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw s9, 176(a5)<br>    |
|  13|[0x800023d0]<br>0x00000040|- rd : x31<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                       |[0x8000023c]:fclass.d t6, fs3<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw t6, 192(a5)<br>    |
|  14|[0x800023e0]<br>0x00000002|- rd : x9<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                         |[0x80000254]:fclass.d s1, ft4<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw s1, 208(a5)<br>    |
|  15|[0x800023f0]<br>0x00000040|- rd : x29<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                        |[0x8000026c]:fclass.d t4, ft1<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw t4, 224(a5)<br>    |
|  16|[0x80002388]<br>0x00000002|- rd : x16<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                       |[0x80000290]:fclass.d a6, fs10<br> [0x80000294]:csrrs s5, fflags, zero<br> [0x80000298]:sw a6, 0(s3)<br>     |
|  17|[0x80002398]<br>0x00000040|- rd : x17<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                       |[0x800002a8]:fclass.d a7, fs8<br> [0x800002ac]:csrrs s5, fflags, zero<br> [0x800002b0]:sw a7, 16(s3)<br>     |
|  18|[0x80002398]<br>0x00000004|- rd : x14<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                       |[0x800002cc]:fclass.d a4, fa5<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw a4, 0(a5)<br>      |
|  19|[0x800023a8]<br>0x00000020|- rd : x8<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                        |[0x800002e4]:fclass.d fp, fa0<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw fp, 16(a5)<br>     |
|  20|[0x800023b8]<br>0x00000004|- rd : x5<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                        |[0x800002fc]:fclass.d t0, ft8<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw t0, 32(a5)<br>     |
|  21|[0x800023c8]<br>0x00000020|- rd : x10<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                       |[0x80000314]:fclass.d a0, fs2<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw a0, 48(a5)<br>     |
|  22|[0x800023d8]<br>0x00000004|- rd : x20<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                       |[0x8000032c]:fclass.d s4, ft9<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw s4, 64(a5)<br>     |
|  23|[0x800023e8]<br>0x00000020|- rd : x1<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                        |[0x80000344]:fclass.d ra, fs6<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw ra, 80(a5)<br>     |
|  24|[0x800023f8]<br>0x00000008|- rd : x26<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                        |[0x8000035c]:fclass.d s10, ft2<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw s10, 96(a5)<br>   |
|  25|[0x80002408]<br>0x00000010|- rd : x6<br> - rs1 : f9<br>                                                                                                            |[0x80000374]:fclass.d t1, fs1<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw t1, 112(a5)<br>    |
|  26|[0x80002418]<br>0x00000010|- rd : x23<br> - rs1 : f21<br>                                                                                                          |[0x8000038c]:fclass.d s7, fs5<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw s7, 128(a5)<br>    |
|  27|[0x800023f8]<br>0x00000010|- rd : x15<br> - rs1 : f12<br>                                                                                                          |[0x800003f8]:fclass.d a5, fa2<br> [0x800003fc]:csrrs s5, fflags, zero<br> [0x80000400]:sw a5, 0(s3)<br>      |
|  28|[0x80002400]<br>0x00000010|- rd : x4<br> - rs1 : f27<br>                                                                                                           |[0x8000041c]:fclass.d tp, fs11<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw tp, 0(a5)<br>     |
|  29|[0x80002410]<br>0x00000010|- rd : x28<br> - rs1 : f8<br>                                                                                                           |[0x80000434]:fclass.d t3, fs0<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw t3, 16(a5)<br>     |
