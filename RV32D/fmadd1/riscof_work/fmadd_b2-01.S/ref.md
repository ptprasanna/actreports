
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800024b0')]      |
| SIG_REGION                | [('0x80005c10', '0x800064f0', '568 words')]      |
| COV_LABELS                | fmadd_b2      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fmadd1/riscof_work/fmadd_b2-01.S/ref.S    |
| Total Number of coverpoints| 424     |
| Total Coverpoints Hit     | 354      |
| Total Signature Updates   | 221      |
| STAT1                     | 221      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80001af4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001af8]:csrrs a7, fflags, zero
[0x80001afc]:fsd ft11, 1264(a5)
[0x80001b00]:sw a7, 1268(a5)
[0x80001b04]:fld ft10, 888(a6)
[0x80001b08]:fld ft9, 896(a6)
[0x80001b0c]:fld ft8, 904(a6)
[0x80001b10]:csrrwi zero, frm, 0

[0x80001b14]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b18]:csrrs a7, fflags, zero
[0x80001b1c]:fsd ft11, 1280(a5)
[0x80001b20]:sw a7, 1284(a5)
[0x80001b24]:fld ft10, 912(a6)
[0x80001b28]:fld ft9, 920(a6)
[0x80001b2c]:fld ft8, 928(a6)
[0x80001b30]:csrrwi zero, frm, 0

[0x80001b34]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1296(a5)
[0x80001b40]:sw a7, 1300(a5)
[0x80001b44]:fld ft10, 936(a6)
[0x80001b48]:fld ft9, 944(a6)
[0x80001b4c]:fld ft8, 952(a6)
[0x80001b50]:csrrwi zero, frm, 0

[0x80001b54]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b58]:csrrs a7, fflags, zero
[0x80001b5c]:fsd ft11, 1312(a5)
[0x80001b60]:sw a7, 1316(a5)
[0x80001b64]:fld ft10, 960(a6)
[0x80001b68]:fld ft9, 968(a6)
[0x80001b6c]:fld ft8, 976(a6)
[0x80001b70]:csrrwi zero, frm, 0

[0x80001b74]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b78]:csrrs a7, fflags, zero
[0x80001b7c]:fsd ft11, 1328(a5)
[0x80001b80]:sw a7, 1332(a5)
[0x80001b84]:fld ft10, 984(a6)
[0x80001b88]:fld ft9, 992(a6)
[0x80001b8c]:fld ft8, 1000(a6)
[0x80001b90]:csrrwi zero, frm, 0

[0x80001b94]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001b98]:csrrs a7, fflags, zero
[0x80001b9c]:fsd ft11, 1344(a5)
[0x80001ba0]:sw a7, 1348(a5)
[0x80001ba4]:fld ft10, 1008(a6)
[0x80001ba8]:fld ft9, 1016(a6)
[0x80001bac]:fld ft8, 1024(a6)
[0x80001bb0]:csrrwi zero, frm, 0

[0x80001bb4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bb8]:csrrs a7, fflags, zero
[0x80001bbc]:fsd ft11, 1360(a5)
[0x80001bc0]:sw a7, 1364(a5)
[0x80001bc4]:fld ft10, 1032(a6)
[0x80001bc8]:fld ft9, 1040(a6)
[0x80001bcc]:fld ft8, 1048(a6)
[0x80001bd0]:csrrwi zero, frm, 0

[0x80001bd4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bd8]:csrrs a7, fflags, zero
[0x80001bdc]:fsd ft11, 1376(a5)
[0x80001be0]:sw a7, 1380(a5)
[0x80001be4]:fld ft10, 1056(a6)
[0x80001be8]:fld ft9, 1064(a6)
[0x80001bec]:fld ft8, 1072(a6)
[0x80001bf0]:csrrwi zero, frm, 0

[0x80001bf4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001bf8]:csrrs a7, fflags, zero
[0x80001bfc]:fsd ft11, 1392(a5)
[0x80001c00]:sw a7, 1396(a5)
[0x80001c04]:fld ft10, 1080(a6)
[0x80001c08]:fld ft9, 1088(a6)
[0x80001c0c]:fld ft8, 1096(a6)
[0x80001c10]:csrrwi zero, frm, 0

[0x80001c14]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1408(a5)
[0x80001c20]:sw a7, 1412(a5)
[0x80001c24]:fld ft10, 1104(a6)
[0x80001c28]:fld ft9, 1112(a6)
[0x80001c2c]:fld ft8, 1120(a6)
[0x80001c30]:csrrwi zero, frm, 0

[0x80001c34]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c38]:csrrs a7, fflags, zero
[0x80001c3c]:fsd ft11, 1424(a5)
[0x80001c40]:sw a7, 1428(a5)
[0x80001c44]:fld ft10, 1128(a6)
[0x80001c48]:fld ft9, 1136(a6)
[0x80001c4c]:fld ft8, 1144(a6)
[0x80001c50]:csrrwi zero, frm, 0

[0x80001c54]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c58]:csrrs a7, fflags, zero
[0x80001c5c]:fsd ft11, 1440(a5)
[0x80001c60]:sw a7, 1444(a5)
[0x80001c64]:fld ft10, 1152(a6)
[0x80001c68]:fld ft9, 1160(a6)
[0x80001c6c]:fld ft8, 1168(a6)
[0x80001c70]:csrrwi zero, frm, 0

[0x80001c74]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c78]:csrrs a7, fflags, zero
[0x80001c7c]:fsd ft11, 1456(a5)
[0x80001c80]:sw a7, 1460(a5)
[0x80001c84]:fld ft10, 1176(a6)
[0x80001c88]:fld ft9, 1184(a6)
[0x80001c8c]:fld ft8, 1192(a6)
[0x80001c90]:csrrwi zero, frm, 0

[0x80001c94]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001c98]:csrrs a7, fflags, zero
[0x80001c9c]:fsd ft11, 1472(a5)
[0x80001ca0]:sw a7, 1476(a5)
[0x80001ca4]:fld ft10, 1200(a6)
[0x80001ca8]:fld ft9, 1208(a6)
[0x80001cac]:fld ft8, 1216(a6)
[0x80001cb0]:csrrwi zero, frm, 0

[0x80001cb4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cb8]:csrrs a7, fflags, zero
[0x80001cbc]:fsd ft11, 1488(a5)
[0x80001cc0]:sw a7, 1492(a5)
[0x80001cc4]:fld ft10, 1224(a6)
[0x80001cc8]:fld ft9, 1232(a6)
[0x80001ccc]:fld ft8, 1240(a6)
[0x80001cd0]:csrrwi zero, frm, 0

[0x80001cd4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cd8]:csrrs a7, fflags, zero
[0x80001cdc]:fsd ft11, 1504(a5)
[0x80001ce0]:sw a7, 1508(a5)
[0x80001ce4]:fld ft10, 1248(a6)
[0x80001ce8]:fld ft9, 1256(a6)
[0x80001cec]:fld ft8, 1264(a6)
[0x80001cf0]:csrrwi zero, frm, 0

[0x80001cf4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001cf8]:csrrs a7, fflags, zero
[0x80001cfc]:fsd ft11, 1520(a5)
[0x80001d00]:sw a7, 1524(a5)
[0x80001d04]:fld ft10, 1272(a6)
[0x80001d08]:fld ft9, 1280(a6)
[0x80001d0c]:fld ft8, 1288(a6)
[0x80001d10]:csrrwi zero, frm, 0

[0x80001d14]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d18]:csrrs a7, fflags, zero
[0x80001d1c]:fsd ft11, 1536(a5)
[0x80001d20]:sw a7, 1540(a5)
[0x80001d24]:fld ft10, 1296(a6)
[0x80001d28]:fld ft9, 1304(a6)
[0x80001d2c]:fld ft8, 1312(a6)
[0x80001d30]:csrrwi zero, frm, 0

[0x80001d34]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d38]:csrrs a7, fflags, zero
[0x80001d3c]:fsd ft11, 1552(a5)
[0x80001d40]:sw a7, 1556(a5)
[0x80001d44]:fld ft10, 1320(a6)
[0x80001d48]:fld ft9, 1328(a6)
[0x80001d4c]:fld ft8, 1336(a6)
[0x80001d50]:csrrwi zero, frm, 0

[0x80001d54]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d58]:csrrs a7, fflags, zero
[0x80001d5c]:fsd ft11, 1568(a5)
[0x80001d60]:sw a7, 1572(a5)
[0x80001d64]:fld ft10, 1344(a6)
[0x80001d68]:fld ft9, 1352(a6)
[0x80001d6c]:fld ft8, 1360(a6)
[0x80001d70]:csrrwi zero, frm, 0

[0x80001d74]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d78]:csrrs a7, fflags, zero
[0x80001d7c]:fsd ft11, 1584(a5)
[0x80001d80]:sw a7, 1588(a5)
[0x80001d84]:fld ft10, 1368(a6)
[0x80001d88]:fld ft9, 1376(a6)
[0x80001d8c]:fld ft8, 1384(a6)
[0x80001d90]:csrrwi zero, frm, 0

[0x80001d94]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001d98]:csrrs a7, fflags, zero
[0x80001d9c]:fsd ft11, 1600(a5)
[0x80001da0]:sw a7, 1604(a5)
[0x80001da4]:fld ft10, 1392(a6)
[0x80001da8]:fld ft9, 1400(a6)
[0x80001dac]:fld ft8, 1408(a6)
[0x80001db0]:csrrwi zero, frm, 0

[0x80001db4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001db8]:csrrs a7, fflags, zero
[0x80001dbc]:fsd ft11, 1616(a5)
[0x80001dc0]:sw a7, 1620(a5)
[0x80001dc4]:fld ft10, 1416(a6)
[0x80001dc8]:fld ft9, 1424(a6)
[0x80001dcc]:fld ft8, 1432(a6)
[0x80001dd0]:csrrwi zero, frm, 0

[0x80001dd4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001dd8]:csrrs a7, fflags, zero
[0x80001ddc]:fsd ft11, 1632(a5)
[0x80001de0]:sw a7, 1636(a5)
[0x80001de4]:fld ft10, 1440(a6)
[0x80001de8]:fld ft9, 1448(a6)
[0x80001dec]:fld ft8, 1456(a6)
[0x80001df0]:csrrwi zero, frm, 0

[0x80001df4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001df8]:csrrs a7, fflags, zero
[0x80001dfc]:fsd ft11, 1648(a5)
[0x80001e00]:sw a7, 1652(a5)
[0x80001e04]:fld ft10, 1464(a6)
[0x80001e08]:fld ft9, 1472(a6)
[0x80001e0c]:fld ft8, 1480(a6)
[0x80001e10]:csrrwi zero, frm, 0

[0x80001e14]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e18]:csrrs a7, fflags, zero
[0x80001e1c]:fsd ft11, 1664(a5)
[0x80001e20]:sw a7, 1668(a5)
[0x80001e24]:fld ft10, 1488(a6)
[0x80001e28]:fld ft9, 1496(a6)
[0x80001e2c]:fld ft8, 1504(a6)
[0x80001e30]:csrrwi zero, frm, 0

[0x80001e34]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e38]:csrrs a7, fflags, zero
[0x80001e3c]:fsd ft11, 1680(a5)
[0x80001e40]:sw a7, 1684(a5)
[0x80001e44]:fld ft10, 1512(a6)
[0x80001e48]:fld ft9, 1520(a6)
[0x80001e4c]:fld ft8, 1528(a6)
[0x80001e50]:csrrwi zero, frm, 0

[0x80001e54]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e58]:csrrs a7, fflags, zero
[0x80001e5c]:fsd ft11, 1696(a5)
[0x80001e60]:sw a7, 1700(a5)
[0x80001e64]:fld ft10, 1536(a6)
[0x80001e68]:fld ft9, 1544(a6)
[0x80001e6c]:fld ft8, 1552(a6)
[0x80001e70]:csrrwi zero, frm, 0

[0x80001e74]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e78]:csrrs a7, fflags, zero
[0x80001e7c]:fsd ft11, 1712(a5)
[0x80001e80]:sw a7, 1716(a5)
[0x80001e84]:fld ft10, 1560(a6)
[0x80001e88]:fld ft9, 1568(a6)
[0x80001e8c]:fld ft8, 1576(a6)
[0x80001e90]:csrrwi zero, frm, 0

[0x80001e94]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001e98]:csrrs a7, fflags, zero
[0x80001e9c]:fsd ft11, 1728(a5)
[0x80001ea0]:sw a7, 1732(a5)
[0x80001ea4]:fld ft10, 1584(a6)
[0x80001ea8]:fld ft9, 1592(a6)
[0x80001eac]:fld ft8, 1600(a6)
[0x80001eb0]:csrrwi zero, frm, 0

[0x80001eb4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001eb8]:csrrs a7, fflags, zero
[0x80001ebc]:fsd ft11, 1744(a5)
[0x80001ec0]:sw a7, 1748(a5)
[0x80001ec4]:fld ft10, 1608(a6)
[0x80001ec8]:fld ft9, 1616(a6)
[0x80001ecc]:fld ft8, 1624(a6)
[0x80001ed0]:csrrwi zero, frm, 0

[0x80001ed4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ed8]:csrrs a7, fflags, zero
[0x80001edc]:fsd ft11, 1760(a5)
[0x80001ee0]:sw a7, 1764(a5)
[0x80001ee4]:fld ft10, 1632(a6)
[0x80001ee8]:fld ft9, 1640(a6)
[0x80001eec]:fld ft8, 1648(a6)
[0x80001ef0]:csrrwi zero, frm, 0

[0x80001ef4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ef8]:csrrs a7, fflags, zero
[0x80001efc]:fsd ft11, 1776(a5)
[0x80001f00]:sw a7, 1780(a5)
[0x80001f04]:fld ft10, 1656(a6)
[0x80001f08]:fld ft9, 1664(a6)
[0x80001f0c]:fld ft8, 1672(a6)
[0x80001f10]:csrrwi zero, frm, 0

[0x80001f14]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f18]:csrrs a7, fflags, zero
[0x80001f1c]:fsd ft11, 1792(a5)
[0x80001f20]:sw a7, 1796(a5)
[0x80001f24]:fld ft10, 1680(a6)
[0x80001f28]:fld ft9, 1688(a6)
[0x80001f2c]:fld ft8, 1696(a6)
[0x80001f30]:csrrwi zero, frm, 0

[0x80001f34]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f38]:csrrs a7, fflags, zero
[0x80001f3c]:fsd ft11, 1808(a5)
[0x80001f40]:sw a7, 1812(a5)
[0x80001f44]:fld ft10, 1704(a6)
[0x80001f48]:fld ft9, 1712(a6)
[0x80001f4c]:fld ft8, 1720(a6)
[0x80001f50]:csrrwi zero, frm, 0

[0x80001f54]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f58]:csrrs a7, fflags, zero
[0x80001f5c]:fsd ft11, 1824(a5)
[0x80001f60]:sw a7, 1828(a5)
[0x80001f64]:fld ft10, 1728(a6)
[0x80001f68]:fld ft9, 1736(a6)
[0x80001f6c]:fld ft8, 1744(a6)
[0x80001f70]:csrrwi zero, frm, 0

[0x80001f74]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f78]:csrrs a7, fflags, zero
[0x80001f7c]:fsd ft11, 1840(a5)
[0x80001f80]:sw a7, 1844(a5)
[0x80001f84]:fld ft10, 1752(a6)
[0x80001f88]:fld ft9, 1760(a6)
[0x80001f8c]:fld ft8, 1768(a6)
[0x80001f90]:csrrwi zero, frm, 0

[0x80001f94]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001f98]:csrrs a7, fflags, zero
[0x80001f9c]:fsd ft11, 1856(a5)
[0x80001fa0]:sw a7, 1860(a5)
[0x80001fa4]:fld ft10, 1776(a6)
[0x80001fa8]:fld ft9, 1784(a6)
[0x80001fac]:fld ft8, 1792(a6)
[0x80001fb0]:csrrwi zero, frm, 0

[0x80001fb4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fb8]:csrrs a7, fflags, zero
[0x80001fbc]:fsd ft11, 1872(a5)
[0x80001fc0]:sw a7, 1876(a5)
[0x80001fc4]:fld ft10, 1800(a6)
[0x80001fc8]:fld ft9, 1808(a6)
[0x80001fcc]:fld ft8, 1816(a6)
[0x80001fd0]:csrrwi zero, frm, 0

[0x80001fd4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001fd8]:csrrs a7, fflags, zero
[0x80001fdc]:fsd ft11, 1888(a5)
[0x80001fe0]:sw a7, 1892(a5)
[0x80001fe4]:fld ft10, 1824(a6)
[0x80001fe8]:fld ft9, 1832(a6)
[0x80001fec]:fld ft8, 1840(a6)
[0x80001ff0]:csrrwi zero, frm, 0

[0x80001ff4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80001ff8]:csrrs a7, fflags, zero
[0x80001ffc]:fsd ft11, 1904(a5)
[0x80002000]:sw a7, 1908(a5)
[0x80002004]:fld ft10, 1848(a6)
[0x80002008]:fld ft9, 1856(a6)
[0x8000200c]:fld ft8, 1864(a6)
[0x80002010]:csrrwi zero, frm, 0

[0x80002014]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002018]:csrrs a7, fflags, zero
[0x8000201c]:fsd ft11, 1920(a5)
[0x80002020]:sw a7, 1924(a5)
[0x80002024]:fld ft10, 1872(a6)
[0x80002028]:fld ft9, 1880(a6)
[0x8000202c]:fld ft8, 1888(a6)
[0x80002030]:csrrwi zero, frm, 0

[0x80002034]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002038]:csrrs a7, fflags, zero
[0x8000203c]:fsd ft11, 1936(a5)
[0x80002040]:sw a7, 1940(a5)
[0x80002044]:fld ft10, 1896(a6)
[0x80002048]:fld ft9, 1904(a6)
[0x8000204c]:fld ft8, 1912(a6)
[0x80002050]:csrrwi zero, frm, 0

[0x80002054]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002058]:csrrs a7, fflags, zero
[0x8000205c]:fsd ft11, 1952(a5)
[0x80002060]:sw a7, 1956(a5)
[0x80002064]:fld ft10, 1920(a6)
[0x80002068]:fld ft9, 1928(a6)
[0x8000206c]:fld ft8, 1936(a6)
[0x80002070]:csrrwi zero, frm, 0

[0x80002074]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002078]:csrrs a7, fflags, zero
[0x8000207c]:fsd ft11, 1968(a5)
[0x80002080]:sw a7, 1972(a5)
[0x80002084]:fld ft10, 1944(a6)
[0x80002088]:fld ft9, 1952(a6)
[0x8000208c]:fld ft8, 1960(a6)
[0x80002090]:csrrwi zero, frm, 0

[0x80002094]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002098]:csrrs a7, fflags, zero
[0x8000209c]:fsd ft11, 1984(a5)
[0x800020a0]:sw a7, 1988(a5)
[0x800020a4]:fld ft10, 1968(a6)
[0x800020a8]:fld ft9, 1976(a6)
[0x800020ac]:fld ft8, 1984(a6)
[0x800020b0]:csrrwi zero, frm, 0

[0x800020b4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020b8]:csrrs a7, fflags, zero
[0x800020bc]:fsd ft11, 2000(a5)
[0x800020c0]:sw a7, 2004(a5)
[0x800020c4]:fld ft10, 1992(a6)
[0x800020c8]:fld ft9, 2000(a6)
[0x800020cc]:fld ft8, 2008(a6)
[0x800020d0]:csrrwi zero, frm, 0

[0x800020d4]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800020d8]:csrrs a7, fflags, zero
[0x800020dc]:fsd ft11, 2016(a5)
[0x800020e0]:sw a7, 2020(a5)
[0x800020e4]:auipc a5, 4
[0x800020e8]:addi a5, a5, 796
[0x800020ec]:fld ft10, 2016(a6)
[0x800020f0]:fld ft9, 2024(a6)
[0x800020f4]:fld ft8, 2032(a6)
[0x800020f8]:csrrwi zero, frm, 0

[0x800022e0]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800022e4]:csrrs a7, fflags, zero
[0x800022e8]:fsd ft11, 240(a5)
[0x800022ec]:sw a7, 244(a5)
[0x800022f0]:fld ft10, 360(a6)
[0x800022f4]:fld ft9, 368(a6)
[0x800022f8]:fld ft8, 376(a6)
[0x800022fc]:csrrwi zero, frm, 0

[0x80002300]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002304]:csrrs a7, fflags, zero
[0x80002308]:fsd ft11, 256(a5)
[0x8000230c]:sw a7, 260(a5)
[0x80002310]:fld ft10, 384(a6)
[0x80002314]:fld ft9, 392(a6)
[0x80002318]:fld ft8, 400(a6)
[0x8000231c]:csrrwi zero, frm, 0

[0x80002320]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002324]:csrrs a7, fflags, zero
[0x80002328]:fsd ft11, 272(a5)
[0x8000232c]:sw a7, 276(a5)
[0x80002330]:fld ft10, 408(a6)
[0x80002334]:fld ft9, 416(a6)
[0x80002338]:fld ft8, 424(a6)
[0x8000233c]:csrrwi zero, frm, 0

[0x80002340]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002344]:csrrs a7, fflags, zero
[0x80002348]:fsd ft11, 288(a5)
[0x8000234c]:sw a7, 292(a5)
[0x80002350]:fld ft10, 432(a6)
[0x80002354]:fld ft9, 440(a6)
[0x80002358]:fld ft8, 448(a6)
[0x8000235c]:csrrwi zero, frm, 0

[0x80002360]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002364]:csrrs a7, fflags, zero
[0x80002368]:fsd ft11, 304(a5)
[0x8000236c]:sw a7, 308(a5)
[0x80002370]:fld ft10, 456(a6)
[0x80002374]:fld ft9, 464(a6)
[0x80002378]:fld ft8, 472(a6)
[0x8000237c]:csrrwi zero, frm, 0

[0x80002380]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002384]:csrrs a7, fflags, zero
[0x80002388]:fsd ft11, 320(a5)
[0x8000238c]:sw a7, 324(a5)
[0x80002390]:fld ft10, 480(a6)
[0x80002394]:fld ft9, 488(a6)
[0x80002398]:fld ft8, 496(a6)
[0x8000239c]:csrrwi zero, frm, 0

[0x800023a0]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023a4]:csrrs a7, fflags, zero
[0x800023a8]:fsd ft11, 336(a5)
[0x800023ac]:sw a7, 340(a5)
[0x800023b0]:fld ft10, 504(a6)
[0x800023b4]:fld ft9, 512(a6)
[0x800023b8]:fld ft8, 520(a6)
[0x800023bc]:csrrwi zero, frm, 0

[0x800023c0]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023c4]:csrrs a7, fflags, zero
[0x800023c8]:fsd ft11, 352(a5)
[0x800023cc]:sw a7, 356(a5)
[0x800023d0]:fld ft10, 528(a6)
[0x800023d4]:fld ft9, 536(a6)
[0x800023d8]:fld ft8, 544(a6)
[0x800023dc]:csrrwi zero, frm, 0

[0x800023e0]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x800023e4]:csrrs a7, fflags, zero
[0x800023e8]:fsd ft11, 368(a5)
[0x800023ec]:sw a7, 372(a5)
[0x800023f0]:fld ft10, 552(a6)
[0x800023f4]:fld ft9, 560(a6)
[0x800023f8]:fld ft8, 568(a6)
[0x800023fc]:csrrwi zero, frm, 0

[0x80002400]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002404]:csrrs a7, fflags, zero
[0x80002408]:fsd ft11, 384(a5)
[0x8000240c]:sw a7, 388(a5)
[0x80002410]:fld ft10, 576(a6)
[0x80002414]:fld ft9, 584(a6)
[0x80002418]:fld ft8, 592(a6)
[0x8000241c]:csrrwi zero, frm, 0

[0x80002420]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002424]:csrrs a7, fflags, zero
[0x80002428]:fsd ft11, 400(a5)
[0x8000242c]:sw a7, 404(a5)
[0x80002430]:fld ft10, 600(a6)
[0x80002434]:fld ft9, 608(a6)
[0x80002438]:fld ft8, 616(a6)
[0x8000243c]:csrrwi zero, frm, 0

[0x80002440]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002444]:csrrs a7, fflags, zero
[0x80002448]:fsd ft11, 416(a5)
[0x8000244c]:sw a7, 420(a5)
[0x80002450]:fld ft10, 624(a6)
[0x80002454]:fld ft9, 632(a6)
[0x80002458]:fld ft8, 640(a6)
[0x8000245c]:csrrwi zero, frm, 0

[0x80002460]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002464]:csrrs a7, fflags, zero
[0x80002468]:fsd ft11, 432(a5)
[0x8000246c]:sw a7, 436(a5)
[0x80002470]:fld ft10, 648(a6)
[0x80002474]:fld ft9, 656(a6)
[0x80002478]:fld ft8, 664(a6)
[0x8000247c]:csrrwi zero, frm, 0

[0x80002480]:fmadd.d ft11, ft10, ft9, ft8, dyn
[0x80002484]:csrrs a7, fflags, zero
[0x80002488]:fsd ft11, 448(a5)
[0x8000248c]:sw a7, 452(a5)
[0x80002490]:fld ft10, 672(a6)
[0x80002494]:fld ft9, 680(a6)
[0x80002498]:fld ft8, 688(a6)
[0x8000249c]:csrrwi zero, frm, 0



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                                    coverpoints                                                                                                                                                                                    |                                                                              code                                                                              |
|---:|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80005c14]<br>0x00000003|- opcode : fmadd.d<br> - rs1 : f20<br> - rs2 : f9<br> - rs3 : f16<br> - rd : f10<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 1 and fe2 == 0x3fd and fm2 == 0x999999999999a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000021 and rm_val == 0  #nosat<br> |[0x80000124]:fmadd.d fa0, fs4, fs1, fa6, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fa0, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>          |
|   2|[0x80005c24]<br>0x00000007|- rs1 : f14<br> - rs2 : f14<br> - rs3 : f14<br> - rd : f14<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                                                                       |[0x80000144]:fmadd.d fa4, fa4, fa4, fa4, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fa4, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80005c34]<br>0x00000007|- rs1 : f28<br> - rs2 : f28<br> - rs3 : f6<br> - rd : f28<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                                        |[0x80000164]:fmadd.d ft8, ft8, ft8, ft6, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft8, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>        |
|   4|[0x80005c44]<br>0x00000007|- rs1 : f8<br> - rs2 : f6<br> - rs3 : f0<br> - rd : f6<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000006 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000048 and rm_val == 0  #nosat<br>                                                         |[0x80000184]:fmadd.d ft6, fs0, ft6, ft0, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd ft6, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>        |
|   5|[0x80005c54]<br>0x00000007|- rs1 : f17<br> - rs2 : f1<br> - rs3 : f11<br> - rd : f17<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000050 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000001c and rm_val == 0  #nosat<br>                                                      |[0x800001a4]:fmadd.d fa7, fa7, ft1, fa1, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fa7, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>        |
|   6|[0x80005c64]<br>0x00000007|- rs1 : f22<br> - rs2 : f22<br> - rs3 : f31<br> - rd : f18<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                                          |[0x800001c4]:fmadd.d fs2, fs6, fs6, ft11, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fs2, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>       |
|   7|[0x80005c74]<br>0x00000007|- rs1 : f24<br> - rs2 : f12<br> - rs3 : f24<br> - rd : f24<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                                       |[0x800001e4]:fmadd.d fs8, fs8, fa2, fs8, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fs8, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80005c84]<br>0x00000007|- rs1 : f21<br> - rs2 : f8<br> - rs3 : f8<br> - rd : f8<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                                          |[0x80000204]:fmadd.d fs0, fs5, fs0, fs0, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fs0, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>      |
|   9|[0x80005c94]<br>0x00000007|- rs1 : f1<br> - rs2 : f24<br> - rs3 : f1<br> - rd : f30<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                                            |[0x80000224]:fmadd.d ft10, ft1, fs8, ft1, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd ft10, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>    |
|  10|[0x80005ca4]<br>0x00000007|- rs1 : f29<br> - rs2 : f23<br> - rs3 : f23<br> - rd : f11<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                                          |[0x80000244]:fmadd.d fa1, ft9, fs7, fs7, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd fa1, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>      |
|  11|[0x80005cb4]<br>0x00000007|- rs1 : f7<br> - rs2 : f31<br> - rs3 : f27<br> - rd : f27<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005c and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                      |[0x80000264]:fmadd.d fs11, ft7, ft11, fs11, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fs11, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>  |
|  12|[0x80005cc4]<br>0x00000007|- rs1 : f15<br> - rs2 : f15<br> - rs3 : f15<br> - rd : f25<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                                       |[0x80000284]:fmadd.d fs9, fa5, fa5, fa5, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fs9, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>      |
|  13|[0x80005cd4]<br>0x00000007|- rs1 : f5<br> - rs2 : f29<br> - rs3 : f30<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000001d and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                  |[0x800002a4]:fmadd.d ft1, ft5, ft9, ft10, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd ft1, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>     |
|  14|[0x80005ce4]<br>0x00000007|- rs1 : f26<br> - rs2 : f3<br> - rs3 : f18<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000010 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000010 and rm_val == 0  #nosat<br>                                                                                                                 |[0x800002c4]:fmadd.d fs4, fs10, ft3, fs2, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs4, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>     |
|  15|[0x80005cf4]<br>0x00000007|- rs1 : f10<br> - rs2 : f19<br> - rs3 : f21<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000007 and rm_val == 0  #nosat<br>                                                                                                                 |[0x800002e4]:fmadd.d ft0, fa0, fs3, fs5, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd ft0, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>      |
|  16|[0x80005d04]<br>0x00000007|- rs1 : f9<br> - rs2 : f10<br> - rs3 : f26<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000034 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000005a and rm_val == 0  #nosat<br>                                                                                                                 |[0x80000304]:fmadd.d fa3, fs1, fa0, fs10, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd fa3, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>     |
|  17|[0x80005d14]<br>0x00000007|- rs1 : f16<br> - rs2 : f21<br> - rs3 : f2<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000037 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000009 and rm_val == 0  #nosat<br>                                                                                                                 |[0x80000324]:fmadd.d ft11, fa6, fs5, ft2, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd ft11, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>    |
|  18|[0x80005d24]<br>0x00000007|- rs1 : f0<br> - rs2 : f2<br> - rs3 : f3<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000002b and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000005d and rm_val == 0  #nosat<br>                                                                                                                   |[0x80000344]:fmadd.d fs5, ft0, ft2, ft3, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd fs5, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>      |
|  19|[0x80005d34]<br>0x00000007|- rs1 : f12<br> - rs2 : f11<br> - rs3 : f7<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000055 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                  |[0x80000364]:fmadd.d ft4, fa2, fa1, ft7, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd ft4, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>      |
|  20|[0x80005d44]<br>0x00000007|- rs1 : f11<br> - rs2 : f5<br> - rs3 : f25<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000000b and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000009 and rm_val == 0  #nosat<br>                                                                                                                 |[0x80000384]:fmadd.d fs7, fa1, ft5, fs9, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd fs7, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>      |
|  21|[0x80005d54]<br>0x00000007|- rs1 : f27<br> - rs2 : f0<br> - rs3 : f9<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000001e and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                   |[0x800003a4]:fmadd.d ft5, fs11, ft0, fs1, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd ft5, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>     |
|  22|[0x80005d64]<br>0x00000007|- rs1 : f30<br> - rs2 : f13<br> - rs3 : f5<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000009 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000005e and rm_val == 0  #nosat<br>                                                                                                                 |[0x800003c4]:fmadd.d fa5, ft10, fa3, ft5, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd fa5, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>     |
|  23|[0x80005d74]<br>0x00000007|- rs1 : f6<br> - rs2 : f18<br> - rs3 : f13<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000007 and rm_val == 0  #nosat<br>                                                                                                                 |[0x800003e4]:fmadd.d fs6, ft6, fs2, fa3, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fs6, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80005d84]<br>0x00000007|- rs1 : f31<br> - rs2 : f7<br> - rs3 : f4<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000037 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000004b and rm_val == 0  #nosat<br>                                                                                                                  |[0x80000404]:fmadd.d fs10, ft11, ft7, ft4, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fs10, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>   |
|  25|[0x80005d94]<br>0x00000007|- rs1 : f19<br> - rs2 : f20<br> - rs3 : f29<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000045 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffff7fff4c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                                                 |[0x80000424]:fmadd.d ft7, fs3, fs4, ft9, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd ft7, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>      |
|  26|[0x80005da4]<br>0x00000007|- rs1 : f3<br> - rs2 : f26<br> - rs3 : f10<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffbfff28 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000002b and rm_val == 0  #nosat<br>                                                                                                                  |[0x80000444]:fmadd.d fs1, ft3, fs10, fa0, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs1, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>     |
|  27|[0x80005db4]<br>0x00000007|- rs1 : f23<br> - rs2 : f25<br> - rs3 : f22<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffdffebe and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                |[0x80000464]:fmadd.d ft9, fs7, fs9, fs6, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd ft9, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x80005dc4]<br>0x00000007|- rs1 : f18<br> - rs2 : f4<br> - rs3 : f17<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000016 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffefff56 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                 |[0x80000484]:fmadd.d fa2, fs2, ft4, fa7, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fa2, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x80005dd4]<br>0x00000007|- rs1 : f2<br> - rs2 : f16<br> - rs3 : f12<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffff7ff88 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                 |[0x800004a4]:fmadd.d fs3, ft2, fa6, fa2, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd fs3, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>      |
|  30|[0x80005de4]<br>0x00000007|- rs1 : f25<br> - rs2 : f17<br> - rs3 : f19<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000053 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffbfede and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                 |[0x800004c4]:fmadd.d ft2, fs9, fa7, fs3, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd ft2, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>      |
|  31|[0x80005df4]<br>0x00000007|- rs1 : f4<br> - rs2 : f27<br> - rs3 : f28<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffdff1e and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000025 and rm_val == 0  #nosat<br>                                                                                                                  |[0x800004e4]:fmadd.d ft3, ft4, fs11, ft8, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd ft3, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>     |
|  32|[0x80005e04]<br>0x00000007|- rs1 : f13<br> - rs2 : f30<br> - rs3 : f20<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffefedc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000038 and rm_val == 0  #nosat<br>                                                                                                                |[0x80000504]:fmadd.d fa6, fa3, ft10, fs4, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fa6, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>     |
|  33|[0x80005e14]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000027 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffff7fae and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000524]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft11, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>   |
|  34|[0x80005e24]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffbf28 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000009 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000544]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd ft11, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>   |
|  35|[0x80005e34]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000026 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffdf3a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000564]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft11, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>   |
|  36|[0x80005e44]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005d and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffef10 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000001a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000584]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80005e54]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000033 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffff6e8 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000058 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800005a4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80005e64]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffb20 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800005c4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80005e74]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffcda and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800005e4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80005e84]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffda6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000604]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80005e94]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000004a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffe38 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000624]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x80005ea4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffeb6 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x000000000004d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000644]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x80005eb4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff6c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000037 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000664]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x80005ec4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000046 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff30 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000019 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000684]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x80005ed4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000000e and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff2a and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000058 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800006a4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x80005ee4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x000000000003b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xffffffffffed4 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000058 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800006c4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x80005ef4]<br>0x00000007|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xfffffffffff2c and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800006e4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80005f04]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000001ffffb and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000704]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80005f14]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000fffbc and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000011 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000724]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80005f24]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000058 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000007ffd8 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000060 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000744]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80005f34]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000003ffeb and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000764]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80005f44]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000004 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000002001a and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000784]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80005f54]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000ffc4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000044 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800007a4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80005f64]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000007fc4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800007c4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80005f74]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005b and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000003fbb and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800007e4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80005f84]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000025 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000001ffd and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000804]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80005f94]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000011 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000001003 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000027 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000824]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x80005fa4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000003 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000082e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000062 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000844]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x80005fb4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000060 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000003a8 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000010 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000864]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x80005fc4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000001d4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000884]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x80005fd4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000000e5 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800008a4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x80005fe4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000067 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800008c4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x80005ff4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000025 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000002e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800008e4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80006004]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000014 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000003a and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000005d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000904]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80006014]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000003f and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffc6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000924]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80006024]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000013 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000020 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000944]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80006034]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000020 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000002 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000964]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80006044]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000048 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff98 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000984]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80006054]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000003e and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffcc and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800009a4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80006064]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000033 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffff9e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800009c4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80006074]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000006 and fs2 == 0 and fe2 == 0x3e0 and fm2 == 0xffff6fffffff4 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800009e4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80006084]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000020 and fs2 == 0 and fe2 == 0x3df and fm2 == 0xfffd9ffffffc0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000a04]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80006094]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x3de and fm2 == 0xfff8dffffff60 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000a24]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x800060a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000010 and fs2 == 0 and fe2 == 0x3dd and fm2 == 0xffeabffffffe0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000055 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000a44]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x800060b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000003e and fs2 == 0 and fe2 == 0x3dc and fm2 == 0xfffdfffffff84 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000004 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000a64]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x800060c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000025 and fs2 == 0 and fe2 == 0x3db and fm2 == 0xffecfffffffb6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000a84]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x800060d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000028 and fs2 == 0 and fe2 == 0x3da and fm2 == 0xff81fffffffb0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000aa4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x800060e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000003 and fs2 == 0 and fe2 == 0x3d9 and fm2 == 0xff5bffffffffa and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000029 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ac4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x800060f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000021 and fs2 == 0 and fe2 == 0x3d8 and fm2 == 0xfe4ffffffffbe and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ae4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80006104]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000027 and fs2 == 0 and fe2 == 0x3d7 and fm2 == 0xfbfffffffffb3 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000040 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000b04]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80006114]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000022 and fs2 == 0 and fe2 == 0x3d6 and fm2 == 0xf41ffffffffbe and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000005f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000b24]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80006124]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000004d and fs2 == 0 and fe2 == 0x3d5 and fm2 == 0xecbffffffff6c and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000b44]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80006134]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x3d4 and fm2 == 0xf57ffffffff9e and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000b64]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80006144]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x3d3 and fm2 == 0xcefffffffff5b and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000b84]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80006154]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000026 and fs2 == 0 and fe2 == 0x3d2 and fm2 == 0x83fffffffffc6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ba4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80006164]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x3d0 and fm2 == 0xfffffffffffc6 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000040 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000bc8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80006174]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000037 and fs2 == 0 and fe2 == 0x3cd and fm2 == 0xfffffffffff92 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000038 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000be8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80006184]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000049 and fs2 == 1 and fe2 == 0x3cf and fm2 == 0x0ffffffffffb2 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000c08]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80006194]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000002a and fs2 == 1 and fe2 == 0x3d1 and fm2 == 0x03fffffffffd5 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000c28]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x800061a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x3ce and fm2 == 0x5ffffffffffa0 and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000c48]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x800061b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000049 and fs2 == 1 and fe2 == 0x3d1 and fm2 == 0x2bfffffffffaa and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000c68]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x800061c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x000000000000e and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0x3ffffffffffef and fs3 == 0 and fe3 == 0x001 and fm3 == 0x000000000002a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000c88]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x800061d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3d0 and fm2 == 0x7ffffffffffff and fs3 == 0 and fe3 == 0x001 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ca8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x800061e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x42d and fm2 == 0x6c16c166666de and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000055 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000cc8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x800061f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004e and fs2 == 1 and fe2 == 0x42c and fm2 == 0xa41a41a0d20e4 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ce8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80006204]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001a and fs2 == 1 and fe2 == 0x42e and fm2 == 0x3b13b139d89ee and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000d08]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80006214]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000049 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xc0e070373ba48 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000d28]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80006224]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004d and fs2 == 1 and fe2 == 0x42c and fm2 == 0xa98ef6063bdd1 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000031 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000d48]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80006234]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000013 and fs2 == 1 and fe2 == 0x42e and fm2 == 0xaf286bc9e50f9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000d68]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80006244]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x555555554004f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000d88]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80006254]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x42c and fm2 == 0x8acb90f6b2ead and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000da8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80006264]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x3521cfb2b2bd0 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000049 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000dc8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80006274]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 1 and fe2 == 0x42e and fm2 == 0xe1e1e1e1de246 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000de8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80006284]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xaf286bca19482 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000e08]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x80006294]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002c and fs2 == 1 and fe2 == 0x42d and fm2 == 0x745d1745d0c0f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000e28]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e2c]:csrrs a7, fflags, zero<br> [0x80000e30]:fsd ft11, 1664(a5)<br> [0x80000e34]:sw a7, 1668(a5)<br> |
| 106|[0x800062a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000046 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xd41d41d41cd44 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000042 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000e48]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e4c]:csrrs a7, fflags, zero<br> [0x80000e50]:fsd ft11, 1680(a5)<br> [0x80000e54]:sw a7, 1684(a5)<br> |
| 107|[0x800062b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000d and fs2 == 1 and fe2 == 0x42f and fm2 == 0x3b13b13b138cb and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000e68]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e6c]:csrrs a7, fflags, zero<br> [0x80000e70]:fsd ft11, 1696(a5)<br> [0x80000e74]:sw a7, 1700(a5)<br> |
| 108|[0x800062c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004f and fs2 == 1 and fe2 == 0x42c and fm2 == 0x9ec8e95103278 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000027 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000e88]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e8c]:csrrs a7, fflags, zero<br> [0x80000e90]:fsd ft11, 1712(a5)<br> [0x80000e94]:sw a7, 1716(a5)<br> |
| 109|[0x800062d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 1 and fe2 == 0x430 and fm2 == 0x999999999993d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ea8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eac]:csrrs a7, fflags, zero<br> [0x80000eb0]:fsd ft11, 1728(a5)<br> [0x80000eb4]:sw a7, 1732(a5)<br> |
| 110|[0x800062e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000047 and fs2 == 1 and fe2 == 0x42c and fm2 == 0xcd85689039aec and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000030 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ec8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ecc]:csrrs a7, fflags, zero<br> [0x80000ed0]:fsd ft11, 1744(a5)<br> [0x80000ed4]:sw a7, 1748(a5)<br> |
| 111|[0x800062f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 1 and fe2 == 0x42f and fm2 == 0x111111111114d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000ee8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eec]:csrrs a7, fflags, zero<br> [0x80000ef0]:fsd ft11, 1760(a5)<br> [0x80000ef4]:sw a7, 1764(a5)<br> |
| 112|[0x80006304]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 1 and fe2 == 0x42d and fm2 == 0xc71c71c71c75c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000035 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000f08]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f0c]:csrrs a7, fflags, zero<br> [0x80000f10]:fsd ft11, 1776(a5)<br> [0x80000f14]:sw a7, 1780(a5)<br> |
| 113|[0x80006314]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x42d and fm2 == 0x6c16c16c16c54 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000034 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000f28]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f2c]:csrrs a7, fflags, zero<br> [0x80000f30]:fsd ft11, 1792(a5)<br> [0x80000f34]:sw a7, 1796(a5)<br> |
| 114|[0x80006324]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x42d and fm2 == 0x5555555555550 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000f48]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f4c]:csrrs a7, fflags, zero<br> [0x80000f50]:fsd ft11, 1808(a5)<br> [0x80000f54]:sw a7, 1812(a5)<br> |
| 115|[0x80006334]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001f and fs2 == 1 and fe2 == 0x42e and fm2 == 0x08421084210e0 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000f68]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f6c]:csrrs a7, fflags, zero<br> [0x80000f70]:fsd ft11, 1824(a5)<br> [0x80000f74]:sw a7, 1828(a5)<br> |
| 116|[0x80006344]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004c and fs2 == 1 and fe2 == 0x42c and fm2 == 0xaf286bca1af53 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000f88]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000f8c]:csrrs a7, fflags, zero<br> [0x80000f90]:fsd ft11, 1840(a5)<br> [0x80000f94]:sw a7, 1844(a5)<br> |
| 117|[0x80006354]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004f and fs2 == 0 and fe2 == 0x42c and fm2 == 0x9ec8e94a8818d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000fa8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fac]:csrrs a7, fflags, zero<br> [0x80000fb0]:fsd ft11, 1856(a5)<br> [0x80000fb4]:sw a7, 1860(a5)<br> |
| 118|[0x80006364]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x4e5e0a7053908 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000055 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000fc8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fcc]:csrrs a7, fflags, zero<br> [0x80000fd0]:fsd ft11, 1872(a5)<br> [0x80000fd4]:sw a7, 1876(a5)<br> |
| 119|[0x80006374]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003c and fs2 == 0 and fe2 == 0x42d and fm2 == 0x1111110ffffcf and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80000fe8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80000fec]:csrrs a7, fflags, zero<br> [0x80000ff0]:fsd ft11, 1888(a5)<br> [0x80000ff4]:sw a7, 1892(a5)<br> |
| 120|[0x80006384]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000005 and fs2 == 0 and fe2 == 0x430 and fm2 == 0x99999998ccc75 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001008]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000100c]:csrrs a7, fflags, zero<br> [0x80001010]:fsd ft11, 1904(a5)<br> [0x80001014]:sw a7, 1908(a5)<br> |
| 121|[0x80006394]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000019 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x47ae147a8f566 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001028]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000102c]:csrrs a7, fflags, zero<br> [0x80001030]:fsd ft11, 1920(a5)<br> [0x80001034]:sw a7, 1924(a5)<br> |
| 122|[0x800063a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x555555552aa57 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001048]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000104c]:csrrs a7, fflags, zero<br> [0x80001050]:fsd ft11, 1936(a5)<br> [0x80001054]:sw a7, 1940(a5)<br> |
| 123|[0x800063b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000f and fs2 == 0 and fe2 == 0x42f and fm2 == 0x11111110fffd3 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000029 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001068]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000106c]:csrrs a7, fflags, zero<br> [0x80001070]:fsd ft11, 1952(a5)<br> [0x80001074]:sw a7, 1956(a5)<br> |
| 124|[0x800063c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000007 and fs2 == 0 and fe2 == 0x430 and fm2 == 0x2492492489233 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000012 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001088]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x8000108c]:csrrs a7, fflags, zero<br> [0x80001090]:fsd ft11, 1968(a5)<br> [0x80001094]:sw a7, 1972(a5)<br> |
| 125|[0x800063d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x42e and fm2 == 0x1a7b9611a34de and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000016 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800010a8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ac]:csrrs a7, fflags, zero<br> [0x800010b0]:fsd ft11, 1984(a5)<br> [0x800010b4]:sw a7, 1988(a5)<br> |
| 126|[0x800063e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000006 and fs2 == 0 and fe2 == 0x430 and fm2 == 0x5555555552a58 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800010c8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010cc]:csrrs a7, fflags, zero<br> [0x800010d0]:fsd ft11, 2000(a5)<br> [0x800010d4]:sw a7, 2004(a5)<br> |
| 127|[0x800063f4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000051 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x948b0fcd6d005 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800010e8]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800010ec]:csrrs a7, fflags, zero<br> [0x800010f0]:fsd ft11, 2016(a5)<br> [0x800010f4]:sw a7, 2020(a5)<br> |
| 128|[0x8000600c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000058 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x745d1745d0b7f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000018 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001110]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 129|[0x8000601c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001d and fs2 == 0 and fe2 == 0x42e and fm2 == 0x1a7b9611a76bf and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000062 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001130]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 130|[0x8000602c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x86186186182e6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000018 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001150]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 131|[0x8000603c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000039 and fs2 == 0 and fe2 == 0x42d and fm2 == 0x1f7047dc11df6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000050 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001170]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 132|[0x8000604c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000015 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x86186186184e0 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001190]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 133|[0x8000605c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000004 and fs2 == 0 and fe2 == 0x430 and fm2 == 0xfffffffffff4a and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800011b0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 134|[0x8000606c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000023 and fs2 == 0 and fe2 == 0x42d and fm2 == 0xd41d41d41d337 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800011d0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 135|[0x8000607c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000047 and fs2 == 0 and fe2 == 0x42c and fm2 == 0xcd85689039a58 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000052 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800011f0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 136|[0x8000608c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000027 and fs2 == 0 and fe2 == 0x42d and fm2 == 0xa41a41a41a388 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000050 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001210]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 137|[0x8000609c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 0 and fe2 == 0x42c and fm2 == 0x7d05f417d059c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001230]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 138|[0x800060ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004b and fs2 == 0 and fe2 == 0x42c and fm2 == 0xb4e81b4e81ad7 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001250]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 139|[0x800060bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000016 and fs2 == 0 and fe2 == 0x42e and fm2 == 0x745d1745d16d0 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001270]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 140|[0x800060cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x40e and fm2 == 0x5c99ae4c415ca and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001290]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 141|[0x800060dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x40d and fm2 == 0x605bd3f4fd3f5 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000056 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800012b0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 142|[0x800060ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x40c and fm2 == 0x702f19e33c679 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800012d0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 143|[0x800060fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003e and fs2 == 1 and fe2 == 0x40c and fm2 == 0x0846f7bdef7be and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000025 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800012f0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 144|[0x8000610c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001a and fs2 == 1 and fe2 == 0x40c and fm2 == 0x3b22762762762 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001310]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 145|[0x8000611c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000044 and fs2 == 1 and fe2 == 0x409 and fm2 == 0xe1f7878787878 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000016 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001330]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 146|[0x8000612c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000043 and fs2 == 1 and fe2 == 0x408 and fm2 == 0xe939503d22635 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001350]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 147|[0x8000613c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 1 and fe2 == 0x408 and fm2 == 0x5d10572620ae5 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001370]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 148|[0x8000614c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x406 and fm2 == 0x7181702e05c0c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001390]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 149|[0x8000615c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x406 and fm2 == 0x6d27d27d27d28 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000017 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800013b0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 150|[0x8000616c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005f and fs2 == 1 and fe2 == 0x404 and fm2 == 0x5d79435e50d79 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000035 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800013d0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 151|[0x8000617c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000053 and fs2 == 1 and fe2 == 0x403 and fm2 == 0x93a9a3784a063 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800013f0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 152|[0x8000618c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002d and fs2 == 1 and fe2 == 0x403 and fm2 == 0x793e93e93e93f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001410]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 153|[0x8000619c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000057 and fs2 == 1 and fe2 == 0x401 and fm2 == 0x9b37e875b37e8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001430]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 154|[0x800061ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 1 and fe2 == 0x401 and fm2 == 0x4ec4ec4ec4ec5 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000053 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001450]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 155|[0x800061bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003c and fs2 == 1 and fe2 == 0x400 and fm2 == 0x6222222222222 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000025 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001470]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 156|[0x800061cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000056 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x2ca6b29aca6b3 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001490]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 157|[0x800061dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x22e8ba2e8ba2f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800014b0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 158|[0x800061ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x714fbcda3ac11 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800014d0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 159|[0x800061fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000004b and fs2 == 1 and fe2 == 0x3fc and fm2 == 0x2c5f92c5f92c6 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800014f0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 160|[0x8000620c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000054 and fs2 == 1 and fe2 == 0x3fd and fm2 == 0x5555555555555 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000017 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001510]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 161|[0x8000621c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x3ca1af286bca2 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001530]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 162|[0x8000622c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x08a1142284509 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001550]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 163|[0x8000623c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000022 and fs2 == 0 and fe2 == 0x40f and fm2 == 0xe1df000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000063 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001570]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 164|[0x8000624c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 0 and fe2 == 0x40e and fm2 == 0xc719000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001590]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 165|[0x8000625c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005b and fs2 == 0 and fe2 == 0x40c and fm2 == 0x680f087087087 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000056 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800015b0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 166|[0x8000626c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000002f and fs2 == 0 and fe2 == 0x40c and fm2 == 0x5c8c6cefa8d9e and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000048 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800015d0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 167|[0x8000627c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x40a and fm2 == 0x4e514e5e0a72f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000028 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800015f0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 168|[0x8000628c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000055 and fs2 == 0 and fe2 == 0x409 and fm2 == 0x817e7e7e7e7e8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001610]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
| 169|[0x8000629c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x6fc2284508a11 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001630]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001634]:csrrs a7, fflags, zero<br> [0x80001638]:fsd ft11, 656(a5)<br> [0x8000163c]:sw a7, 660(a5)<br>   |
| 170|[0x800062ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000009 and fs2 == 0 and fe2 == 0x40a and fm2 == 0xc67c71c71c71c and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001650]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001654]:csrrs a7, fflags, zero<br> [0x80001658]:fsd ft11, 672(a5)<br> [0x8000165c]:sw a7, 676(a5)<br>   |
| 171|[0x800062bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000041 and fs2 == 0 and fe2 == 0x406 and fm2 == 0xf6b52b52b52b5 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001674]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001678]:csrrs a7, fflags, zero<br> [0x8000167c]:fsd ft11, 688(a5)<br> [0x80001680]:sw a7, 692(a5)<br>   |
| 172|[0x800062cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000042 and fs2 == 0 and fe2 == 0x405 and fm2 == 0xec3e0f83e0f84 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000047 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001694]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001698]:csrrs a7, fflags, zero<br> [0x8000169c]:fsd ft11, 704(a5)<br> [0x800016a0]:sw a7, 708(a5)<br>   |
| 173|[0x800062dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003f and fs2 == 0 and fe2 == 0x405 and fm2 == 0x01e79e79e79e8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000023 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800016b4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016b8]:csrrs a7, fflags, zero<br> [0x800016bc]:fsd ft11, 720(a5)<br> [0x800016c0]:sw a7, 724(a5)<br>   |
| 174|[0x800062ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x405 and fm2 == 0x5380000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800016d4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016d8]:csrrs a7, fflags, zero<br> [0x800016dc]:fsd ft11, 736(a5)<br> [0x800016e0]:sw a7, 740(a5)<br>   |
| 175|[0x800062fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000040 and fs2 == 0 and fe2 == 0x402 and fm2 == 0xea80000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000002c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800016f4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800016f8]:csrrs a7, fflags, zero<br> [0x800016fc]:fsd ft11, 752(a5)<br> [0x80001700]:sw a7, 756(a5)<br>   |
| 176|[0x8000630c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000017 and fs2 == 0 and fe2 == 0x403 and fm2 == 0x2c8590b21642d and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001714]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001718]:csrrs a7, fflags, zero<br> [0x8000171c]:fsd ft11, 768(a5)<br> [0x80001720]:sw a7, 772(a5)<br>   |
| 177|[0x8000631c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 0 and fe2 == 0x400 and fm2 == 0xeb1a1f58d0fac and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001734]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001738]:csrrs a7, fflags, zero<br> [0x8000173c]:fsd ft11, 784(a5)<br> [0x80001740]:sw a7, 788(a5)<br>   |
| 178|[0x8000632c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000013 and fs2 == 0 and fe2 == 0x401 and fm2 == 0x8000000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001754]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001758]:csrrs a7, fflags, zero<br> [0x8000175c]:fsd ft11, 800(a5)<br> [0x80001760]:sw a7, 804(a5)<br>   |
| 179|[0x8000633c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 0 and fe2 == 0x3fc and fm2 == 0xb47582192e29f and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000034 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001774]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001778]:csrrs a7, fflags, zero<br> [0x8000177c]:fsd ft11, 816(a5)<br> [0x80001780]:sw a7, 820(a5)<br>   |
| 180|[0x8000634c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000024 and fs2 == 1 and fe2 == 0x3fc and fm2 == 0x1c71c71c71c72 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000026 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001794]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001798]:csrrs a7, fflags, zero<br> [0x8000179c]:fsd ft11, 832(a5)<br> [0x800017a0]:sw a7, 836(a5)<br>   |
| 181|[0x8000635c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000029 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x18f9c18f9c190 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800017b4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:fsd ft11, 848(a5)<br> [0x800017c0]:sw a7, 852(a5)<br>   |
| 182|[0x8000636c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000059 and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xe8fd1fa3f47e9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000005e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800017d4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017d8]:csrrs a7, fflags, zero<br> [0x800017dc]:fsd ft11, 864(a5)<br> [0x800017e0]:sw a7, 868(a5)<br>   |
| 183|[0x8000637c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3fe and fm2 == 0xf75270d0456c8 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000003f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800017f4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800017f8]:csrrs a7, fflags, zero<br> [0x800017fc]:fsd ft11, 880(a5)<br> [0x80001800]:sw a7, 884(a5)<br>   |
| 184|[0x8000638c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x745d1745d1746 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000043 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001814]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001818]:csrrs a7, fflags, zero<br> [0x8000181c]:fsd ft11, 896(a5)<br> [0x80001820]:sw a7, 900(a5)<br>   |
| 185|[0x8000639c]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003b and fs2 == 1 and fe2 == 0x3ff and fm2 == 0x5b1e5f75270d0 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000050 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001834]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001838]:csrrs a7, fflags, zero<br> [0x8000183c]:fsd ft11, 912(a5)<br> [0x80001840]:sw a7, 916(a5)<br>   |
| 186|[0x800063ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000026 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000001fffe4 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000013 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001854]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001858]:csrrs a7, fflags, zero<br> [0x8000185c]:fsd ft11, 928(a5)<br> [0x80001860]:sw a7, 932(a5)<br>   |
| 187|[0x800063bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000008 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000100013 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001874]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001878]:csrrs a7, fflags, zero<br> [0x8000187c]:fsd ft11, 944(a5)<br> [0x80001880]:sw a7, 948(a5)<br>   |
| 188|[0x800063cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000030 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000007ffda and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000014 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001894]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001898]:csrrs a7, fflags, zero<br> [0x8000189c]:fsd ft11, 960(a5)<br> [0x800018a0]:sw a7, 964(a5)<br>   |
| 189|[0x800063dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000016 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000040006 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000038 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800018b4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018b8]:csrrs a7, fflags, zero<br> [0x800018bc]:fsd ft11, 976(a5)<br> [0x800018c0]:sw a7, 980(a5)<br>   |
| 190|[0x800063ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000035 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000001ffd2 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000000e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800018d4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018d8]:csrrs a7, fflags, zero<br> [0x800018dc]:fsd ft11, 992(a5)<br> [0x800018e0]:sw a7, 996(a5)<br>   |
| 191|[0x800063fc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000011 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000010017 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000051 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800018f4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800018f8]:csrrs a7, fflags, zero<br> [0x800018fc]:fsd ft11, 1008(a5)<br> [0x80001900]:sw a7, 1012(a5)<br> |
| 192|[0x8000640c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005e and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000007fc6 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000049 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001914]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001918]:csrrs a7, fflags, zero<br> [0x8000191c]:fsd ft11, 1024(a5)<br> [0x80001920]:sw a7, 1028(a5)<br> |
| 193|[0x8000641c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001b and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000003fed and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000011 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001934]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001938]:csrrs a7, fflags, zero<br> [0x8000193c]:fsd ft11, 1040(a5)<br> [0x80001940]:sw a7, 1044(a5)<br> |
| 194|[0x8000642c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000040 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000001fd6 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000002b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001954]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001958]:csrrs a7, fflags, zero<br> [0x8000195c]:fsd ft11, 1056(a5)<br> [0x80001960]:sw a7, 1060(a5)<br> |
| 195|[0x8000643c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000052 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000fb0 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001974]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001978]:csrrs a7, fflags, zero<br> [0x8000197c]:fsd ft11, 1072(a5)<br> [0x80001980]:sw a7, 1076(a5)<br> |
| 196|[0x8000644c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000013 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000007f1 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000008 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001994]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001998]:csrrs a7, fflags, zero<br> [0x8000199c]:fsd ft11, 1088(a5)<br> [0x800019a0]:sw a7, 1092(a5)<br> |
| 197|[0x8000645c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000054 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000003c7 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000036 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800019b4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019b8]:csrrs a7, fflags, zero<br> [0x800019bc]:fsd ft11, 1104(a5)<br> [0x800019c0]:sw a7, 1108(a5)<br> |
| 198|[0x8000646c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000021 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000001fd and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800019d4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019d8]:csrrs a7, fflags, zero<br> [0x800019dc]:fsd ft11, 1120(a5)<br> [0x800019e0]:sw a7, 1124(a5)<br> |
| 199|[0x8000647c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004d and fs2 == 1 and fe2 == 0x400 and fm2 == 0x00000000000c9 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000002d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800019f4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800019f8]:csrrs a7, fflags, zero<br> [0x800019fc]:fsd ft11, 1136(a5)<br> [0x80001a00]:sw a7, 1140(a5)<br> |
| 200|[0x8000648c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000001f and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000091 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000005f and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001a14]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a18]:csrrs a7, fflags, zero<br> [0x80001a1c]:fsd ft11, 1152(a5)<br> [0x80001a20]:sw a7, 1156(a5)<br> |
| 201|[0x8000649c]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000034 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000000e and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000005 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001a34]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a38]:csrrs a7, fflags, zero<br> [0x80001a3c]:fsd ft11, 1168(a5)<br> [0x80001a40]:sw a7, 1172(a5)<br> |
| 202|[0x800064ac]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000004d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffea and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000045 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001a54]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a58]:csrrs a7, fflags, zero<br> [0x80001a5c]:fsd ft11, 1184(a5)<br> [0x80001a60]:sw a7, 1188(a5)<br> |
| 203|[0x800064bc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000007 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x000000000000f and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000000b and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001a74]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a78]:csrrs a7, fflags, zero<br> [0x80001a7c]:fsd ft11, 1200(a5)<br> [0x80001a80]:sw a7, 1204(a5)<br> |
| 204|[0x800064cc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000015 and fs2 == 1 and fe2 == 0x400 and fm2 == 0x0000000000021 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000005d and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001a94]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001a98]:csrrs a7, fflags, zero<br> [0x80001a9c]:fsd ft11, 1216(a5)<br> [0x80001aa0]:sw a7, 1220(a5)<br> |
| 205|[0x800064dc]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x0000000000028 and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xffffffffffff6 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x000000000003e and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001ab4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ab8]:csrrs a7, fflags, zero<br> [0x80001abc]:fsd ft11, 1232(a5)<br> [0x80001ac0]:sw a7, 1236(a5)<br> |
| 206|[0x800064ec]<br>0x00000007|- fs1 == 0 and fe1 == 0x3f8 and fm1 == 0x000000000005d and fs2 == 1 and fe2 == 0x3ff and fm2 == 0xfffffffffffa2 and fs3 == 0 and fe3 == 0x3f8 and fm3 == 0x0000000000059 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80001ad4]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80001ad8]:csrrs a7, fflags, zero<br> [0x80001adc]:fsd ft11, 1248(a5)<br> [0x80001ae0]:sw a7, 1252(a5)<br> |
| 207|[0x80006404]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000016 and fs2 == 0 and fe2 == 0x410 and fm2 == 0x745c9745d1746 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000016 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800020fc]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002100]:csrrs a7, fflags, zero<br> [0x80002104]:fsd ft11, 0(a5)<br> [0x80002108]:sw a7, 4(a5)<br>       |
| 208|[0x80006414]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000001b and fs2 == 0 and fe2 == 0x40f and fm2 == 0x2f67425ed097b and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002120]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002124]:csrrs a7, fflags, zero<br> [0x80002128]:fsd ft11, 16(a5)<br> [0x8000212c]:sw a7, 20(a5)<br>     |
| 209|[0x80006424]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x412 and fm2 == 0xffffe00000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000001 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002140]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002144]:csrrs a7, fflags, zero<br> [0x80002148]:fsd ft11, 32(a5)<br> [0x8000214c]:sw a7, 36(a5)<br>     |
| 210|[0x80006434]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000011 and fs2 == 0 and fe2 == 0x40d and fm2 == 0xe1dde1e1e1e1e and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000011 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002160]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002164]:csrrs a7, fflags, zero<br> [0x80002168]:fsd ft11, 48(a5)<br> [0x8000216c]:sw a7, 52(a5)<br>     |
| 211|[0x80006444]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000050 and fs2 == 0 and fe2 == 0x40a and fm2 == 0x9982ccccccccd and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000039 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002180]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002184]:csrrs a7, fflags, zero<br> [0x80002188]:fsd ft11, 64(a5)<br> [0x8000218c]:sw a7, 68(a5)<br>     |
| 212|[0x80006454]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000012 and fs2 == 0 and fe2 == 0x40b and fm2 == 0xc6d5555555555 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000050 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800021a0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021a4]:csrrs a7, fflags, zero<br> [0x800021a8]:fsd ft11, 80(a5)<br> [0x800021ac]:sw a7, 84(a5)<br>     |
| 213|[0x80006464]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000062 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x4e50fac687d63 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000000a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800021c0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021c4]:csrrs a7, fflags, zero<br> [0x800021c8]:fsd ft11, 96(a5)<br> [0x800021cc]:sw a7, 100(a5)<br>    |
| 214|[0x80006474]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000032 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x477851eb851ec and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000015 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800021e0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800021e4]:csrrs a7, fflags, zero<br> [0x800021e8]:fsd ft11, 112(a5)<br> [0x800021ec]:sw a7, 116(a5)<br>   |
| 215|[0x80006484]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000018 and fs2 == 0 and fe2 == 0x408 and fm2 == 0x5345555555555 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000063 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002200]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002204]:csrrs a7, fflags, zero<br> [0x80002208]:fsd ft11, 128(a5)<br> [0x8000220c]:sw a7, 132(a5)<br>   |
| 216|[0x80006494]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000035 and fs2 == 0 and fe2 == 0x406 and fm2 == 0x33c609a90e7d9 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000024 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002220]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002224]:csrrs a7, fflags, zero<br> [0x80002228]:fsd ft11, 144(a5)<br> [0x8000222c]:sw a7, 148(a5)<br>   |
| 217|[0x800064a4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000005e and fs2 == 0 and fe2 == 0x404 and fm2 == 0x5a3677d46cefb and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000001c and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002240]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002244]:csrrs a7, fflags, zero<br> [0x80002248]:fsd ft11, 160(a5)<br> [0x8000224c]:sw a7, 164(a5)<br>   |
| 218|[0x800064b4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000020 and fs2 == 0 and fe2 == 0x404 and fm2 == 0xff80000000000 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000002 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002260]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002264]:csrrs a7, fflags, zero<br> [0x80002268]:fsd ft11, 176(a5)<br> [0x8000226c]:sw a7, 180(a5)<br>   |
| 219|[0x800064c4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000000e and fs2 == 0 and fe2 == 0x405 and fm2 == 0x0f6db6db6db6e and fs3 == 0 and fe3 == 0x000 and fm3 == 0x000000000004a and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x80002280]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x80002284]:csrrs a7, fflags, zero<br> [0x80002288]:fsd ft11, 192(a5)<br> [0x8000228c]:sw a7, 196(a5)<br>   |
| 220|[0x800064d4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000031 and fs2 == 0 and fe2 == 0x402 and fm2 == 0x20a72f0539783 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000046 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800022a0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022a4]:csrrs a7, fflags, zero<br> [0x800022a8]:fsd ft11, 208(a5)<br> [0x800022ac]:sw a7, 212(a5)<br>   |
| 221|[0x800064e4]<br>0x00000007|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x000000000003d and fs2 == 0 and fe2 == 0x400 and fm2 == 0xd60864b8a7de7 and fs3 == 0 and fe3 == 0x000 and fm3 == 0x0000000000020 and rm_val == 0  #nosat<br>                                                                                                                                                                               |[0x800022c0]:fmadd.d ft11, ft10, ft9, ft8, dyn<br> [0x800022c4]:csrrs a7, fflags, zero<br> [0x800022c8]:fsd ft11, 224(a5)<br> [0x800022cc]:sw a7, 228(a5)<br>   |
