
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800037b0')]      |
| SIG_REGION                | [('0x80007610', '0x80008820', '1156 words')]      |
| COV_LABELS                | feq_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/feq/riscof_work/feq_b1-01.S/ref.S    |
| Total Number of coverpoints| 681     |
| Total Coverpoints Hit     | 615      |
| Total Signature Updates   | 1031      |
| STAT1                     | 516      |
| STAT2                     | 0      |
| STAT3                     | 61     |
| STAT4                     | 515     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80002f84]:feq.d t6, ft11, ft10
[0x80002f88]:csrrs a7, fflags, zero
[0x80002f8c]:sw t6, 1408(a5)
[0x80002f90]:sw a7, 1412(a5)
[0x80002f94]:fld ft11, 1776(a6)
[0x80002f98]:fld ft10, 1784(a6)

[0x80002f9c]:feq.d t6, ft11, ft10
[0x80002fa0]:csrrs a7, fflags, zero
[0x80002fa4]:sw t6, 1424(a5)
[0x80002fa8]:sw a7, 1428(a5)
[0x80002fac]:fld ft11, 1792(a6)
[0x80002fb0]:fld ft10, 1800(a6)

[0x80002fb4]:feq.d t6, ft11, ft10
[0x80002fb8]:csrrs a7, fflags, zero
[0x80002fbc]:sw t6, 1440(a5)
[0x80002fc0]:sw a7, 1444(a5)
[0x80002fc4]:fld ft11, 1808(a6)
[0x80002fc8]:fld ft10, 1816(a6)

[0x80002fcc]:feq.d t6, ft11, ft10
[0x80002fd0]:csrrs a7, fflags, zero
[0x80002fd4]:sw t6, 1456(a5)
[0x80002fd8]:sw a7, 1460(a5)
[0x80002fdc]:fld ft11, 1824(a6)
[0x80002fe0]:fld ft10, 1832(a6)

[0x80002fe4]:feq.d t6, ft11, ft10
[0x80002fe8]:csrrs a7, fflags, zero
[0x80002fec]:sw t6, 1472(a5)
[0x80002ff0]:sw a7, 1476(a5)
[0x80002ff4]:fld ft11, 1840(a6)
[0x80002ff8]:fld ft10, 1848(a6)

[0x80002ffc]:feq.d t6, ft11, ft10
[0x80003000]:csrrs a7, fflags, zero
[0x80003004]:sw t6, 1488(a5)
[0x80003008]:sw a7, 1492(a5)
[0x8000300c]:fld ft11, 1856(a6)
[0x80003010]:fld ft10, 1864(a6)

[0x80003014]:feq.d t6, ft11, ft10
[0x80003018]:csrrs a7, fflags, zero
[0x8000301c]:sw t6, 1504(a5)
[0x80003020]:sw a7, 1508(a5)
[0x80003024]:fld ft11, 1872(a6)
[0x80003028]:fld ft10, 1880(a6)

[0x8000302c]:feq.d t6, ft11, ft10
[0x80003030]:csrrs a7, fflags, zero
[0x80003034]:sw t6, 1520(a5)
[0x80003038]:sw a7, 1524(a5)
[0x8000303c]:fld ft11, 1888(a6)
[0x80003040]:fld ft10, 1896(a6)

[0x80003044]:feq.d t6, ft11, ft10
[0x80003048]:csrrs a7, fflags, zero
[0x8000304c]:sw t6, 1536(a5)
[0x80003050]:sw a7, 1540(a5)
[0x80003054]:fld ft11, 1904(a6)
[0x80003058]:fld ft10, 1912(a6)

[0x8000305c]:feq.d t6, ft11, ft10
[0x80003060]:csrrs a7, fflags, zero
[0x80003064]:sw t6, 1552(a5)
[0x80003068]:sw a7, 1556(a5)
[0x8000306c]:fld ft11, 1920(a6)
[0x80003070]:fld ft10, 1928(a6)

[0x80003074]:feq.d t6, ft11, ft10
[0x80003078]:csrrs a7, fflags, zero
[0x8000307c]:sw t6, 1568(a5)
[0x80003080]:sw a7, 1572(a5)
[0x80003084]:fld ft11, 1936(a6)
[0x80003088]:fld ft10, 1944(a6)

[0x8000308c]:feq.d t6, ft11, ft10
[0x80003090]:csrrs a7, fflags, zero
[0x80003094]:sw t6, 1584(a5)
[0x80003098]:sw a7, 1588(a5)
[0x8000309c]:fld ft11, 1952(a6)
[0x800030a0]:fld ft10, 1960(a6)

[0x800030a4]:feq.d t6, ft11, ft10
[0x800030a8]:csrrs a7, fflags, zero
[0x800030ac]:sw t6, 1600(a5)
[0x800030b0]:sw a7, 1604(a5)
[0x800030b4]:fld ft11, 1968(a6)
[0x800030b8]:fld ft10, 1976(a6)

[0x800030bc]:feq.d t6, ft11, ft10
[0x800030c0]:csrrs a7, fflags, zero
[0x800030c4]:sw t6, 1616(a5)
[0x800030c8]:sw a7, 1620(a5)
[0x800030cc]:fld ft11, 1984(a6)
[0x800030d0]:fld ft10, 1992(a6)

[0x800030d4]:feq.d t6, ft11, ft10
[0x800030d8]:csrrs a7, fflags, zero
[0x800030dc]:sw t6, 1632(a5)
[0x800030e0]:sw a7, 1636(a5)
[0x800030e4]:fld ft11, 2000(a6)
[0x800030e8]:fld ft10, 2008(a6)

[0x800030ec]:feq.d t6, ft11, ft10
[0x800030f0]:csrrs a7, fflags, zero
[0x800030f4]:sw t6, 1648(a5)
[0x800030f8]:sw a7, 1652(a5)
[0x800030fc]:fld ft11, 2016(a6)
[0x80003100]:fld ft10, 2024(a6)

[0x80003104]:feq.d t6, ft11, ft10
[0x80003108]:csrrs a7, fflags, zero
[0x8000310c]:sw t6, 1664(a5)
[0x80003110]:sw a7, 1668(a5)
[0x80003114]:addi a6, a6, 2032
[0x80003118]:fld ft11, 0(a6)
[0x8000311c]:fld ft10, 8(a6)

[0x80003120]:feq.d t6, ft11, ft10
[0x80003124]:csrrs a7, fflags, zero
[0x80003128]:sw t6, 1680(a5)
[0x8000312c]:sw a7, 1684(a5)
[0x80003130]:fld ft11, 16(a6)
[0x80003134]:fld ft10, 24(a6)

[0x80003138]:feq.d t6, ft11, ft10
[0x8000313c]:csrrs a7, fflags, zero
[0x80003140]:sw t6, 1696(a5)
[0x80003144]:sw a7, 1700(a5)
[0x80003148]:fld ft11, 32(a6)
[0x8000314c]:fld ft10, 40(a6)

[0x80003150]:feq.d t6, ft11, ft10
[0x80003154]:csrrs a7, fflags, zero
[0x80003158]:sw t6, 1712(a5)
[0x8000315c]:sw a7, 1716(a5)
[0x80003160]:fld ft11, 48(a6)
[0x80003164]:fld ft10, 56(a6)

[0x80003168]:feq.d t6, ft11, ft10
[0x8000316c]:csrrs a7, fflags, zero
[0x80003170]:sw t6, 1728(a5)
[0x80003174]:sw a7, 1732(a5)
[0x80003178]:fld ft11, 64(a6)
[0x8000317c]:fld ft10, 72(a6)

[0x80003180]:feq.d t6, ft11, ft10
[0x80003184]:csrrs a7, fflags, zero
[0x80003188]:sw t6, 1744(a5)
[0x8000318c]:sw a7, 1748(a5)
[0x80003190]:fld ft11, 80(a6)
[0x80003194]:fld ft10, 88(a6)

[0x80003198]:feq.d t6, ft11, ft10
[0x8000319c]:csrrs a7, fflags, zero
[0x800031a0]:sw t6, 1760(a5)
[0x800031a4]:sw a7, 1764(a5)
[0x800031a8]:fld ft11, 96(a6)
[0x800031ac]:fld ft10, 104(a6)

[0x800031b0]:feq.d t6, ft11, ft10
[0x800031b4]:csrrs a7, fflags, zero
[0x800031b8]:sw t6, 1776(a5)
[0x800031bc]:sw a7, 1780(a5)
[0x800031c0]:fld ft11, 112(a6)
[0x800031c4]:fld ft10, 120(a6)

[0x800031c8]:feq.d t6, ft11, ft10
[0x800031cc]:csrrs a7, fflags, zero
[0x800031d0]:sw t6, 1792(a5)
[0x800031d4]:sw a7, 1796(a5)
[0x800031d8]:fld ft11, 128(a6)
[0x800031dc]:fld ft10, 136(a6)

[0x800031e0]:feq.d t6, ft11, ft10
[0x800031e4]:csrrs a7, fflags, zero
[0x800031e8]:sw t6, 1808(a5)
[0x800031ec]:sw a7, 1812(a5)
[0x800031f0]:fld ft11, 144(a6)
[0x800031f4]:fld ft10, 152(a6)

[0x800031f8]:feq.d t6, ft11, ft10
[0x800031fc]:csrrs a7, fflags, zero
[0x80003200]:sw t6, 1824(a5)
[0x80003204]:sw a7, 1828(a5)
[0x80003208]:fld ft11, 160(a6)
[0x8000320c]:fld ft10, 168(a6)

[0x80003210]:feq.d t6, ft11, ft10
[0x80003214]:csrrs a7, fflags, zero
[0x80003218]:sw t6, 1840(a5)
[0x8000321c]:sw a7, 1844(a5)
[0x80003220]:fld ft11, 176(a6)
[0x80003224]:fld ft10, 184(a6)

[0x80003228]:feq.d t6, ft11, ft10
[0x8000322c]:csrrs a7, fflags, zero
[0x80003230]:sw t6, 1856(a5)
[0x80003234]:sw a7, 1860(a5)
[0x80003238]:fld ft11, 192(a6)
[0x8000323c]:fld ft10, 200(a6)

[0x80003240]:feq.d t6, ft11, ft10
[0x80003244]:csrrs a7, fflags, zero
[0x80003248]:sw t6, 1872(a5)
[0x8000324c]:sw a7, 1876(a5)
[0x80003250]:fld ft11, 208(a6)
[0x80003254]:fld ft10, 216(a6)

[0x80003258]:feq.d t6, ft11, ft10
[0x8000325c]:csrrs a7, fflags, zero
[0x80003260]:sw t6, 1888(a5)
[0x80003264]:sw a7, 1892(a5)
[0x80003268]:fld ft11, 224(a6)
[0x8000326c]:fld ft10, 232(a6)

[0x80003270]:feq.d t6, ft11, ft10
[0x80003274]:csrrs a7, fflags, zero
[0x80003278]:sw t6, 1904(a5)
[0x8000327c]:sw a7, 1908(a5)
[0x80003280]:fld ft11, 240(a6)
[0x80003284]:fld ft10, 248(a6)

[0x80003288]:feq.d t6, ft11, ft10
[0x8000328c]:csrrs a7, fflags, zero
[0x80003290]:sw t6, 1920(a5)
[0x80003294]:sw a7, 1924(a5)
[0x80003298]:fld ft11, 256(a6)
[0x8000329c]:fld ft10, 264(a6)

[0x800032a0]:feq.d t6, ft11, ft10
[0x800032a4]:csrrs a7, fflags, zero
[0x800032a8]:sw t6, 1936(a5)
[0x800032ac]:sw a7, 1940(a5)
[0x800032b0]:fld ft11, 272(a6)
[0x800032b4]:fld ft10, 280(a6)

[0x800032b8]:feq.d t6, ft11, ft10
[0x800032bc]:csrrs a7, fflags, zero
[0x800032c0]:sw t6, 1952(a5)
[0x800032c4]:sw a7, 1956(a5)
[0x800032c8]:fld ft11, 288(a6)
[0x800032cc]:fld ft10, 296(a6)

[0x800032d0]:feq.d t6, ft11, ft10
[0x800032d4]:csrrs a7, fflags, zero
[0x800032d8]:sw t6, 1968(a5)
[0x800032dc]:sw a7, 1972(a5)
[0x800032e0]:fld ft11, 304(a6)
[0x800032e4]:fld ft10, 312(a6)

[0x800032e8]:feq.d t6, ft11, ft10
[0x800032ec]:csrrs a7, fflags, zero
[0x800032f0]:sw t6, 1984(a5)
[0x800032f4]:sw a7, 1988(a5)
[0x800032f8]:fld ft11, 320(a6)
[0x800032fc]:fld ft10, 328(a6)

[0x80003300]:feq.d t6, ft11, ft10
[0x80003304]:csrrs a7, fflags, zero
[0x80003308]:sw t6, 2000(a5)
[0x8000330c]:sw a7, 2004(a5)
[0x80003310]:fld ft11, 336(a6)
[0x80003314]:fld ft10, 344(a6)

[0x80003318]:feq.d t6, ft11, ft10
[0x8000331c]:csrrs a7, fflags, zero
[0x80003320]:sw t6, 2016(a5)
[0x80003324]:sw a7, 2020(a5)
[0x80003328]:auipc a5, 5
[0x8000332c]:addi a5, a5, 888
[0x80003330]:fld ft11, 352(a6)
[0x80003334]:fld ft10, 360(a6)

[0x80003590]:feq.d t6, ft11, ft10
[0x80003594]:csrrs a7, fflags, zero
[0x80003598]:sw t6, 400(a5)
[0x8000359c]:sw a7, 404(a5)
[0x800035a0]:fld ft11, 768(a6)
[0x800035a4]:fld ft10, 776(a6)

[0x800035a8]:feq.d t6, ft11, ft10
[0x800035ac]:csrrs a7, fflags, zero
[0x800035b0]:sw t6, 416(a5)
[0x800035b4]:sw a7, 420(a5)
[0x800035b8]:fld ft11, 784(a6)
[0x800035bc]:fld ft10, 792(a6)

[0x800035c0]:feq.d t6, ft11, ft10
[0x800035c4]:csrrs a7, fflags, zero
[0x800035c8]:sw t6, 432(a5)
[0x800035cc]:sw a7, 436(a5)
[0x800035d0]:fld ft11, 800(a6)
[0x800035d4]:fld ft10, 808(a6)

[0x800035d8]:feq.d t6, ft11, ft10
[0x800035dc]:csrrs a7, fflags, zero
[0x800035e0]:sw t6, 448(a5)
[0x800035e4]:sw a7, 452(a5)
[0x800035e8]:fld ft11, 816(a6)
[0x800035ec]:fld ft10, 824(a6)

[0x800035f0]:feq.d t6, ft11, ft10
[0x800035f4]:csrrs a7, fflags, zero
[0x800035f8]:sw t6, 464(a5)
[0x800035fc]:sw a7, 468(a5)
[0x80003600]:fld ft11, 832(a6)
[0x80003604]:fld ft10, 840(a6)

[0x80003608]:feq.d t6, ft11, ft10
[0x8000360c]:csrrs a7, fflags, zero
[0x80003610]:sw t6, 480(a5)
[0x80003614]:sw a7, 484(a5)
[0x80003618]:fld ft11, 848(a6)
[0x8000361c]:fld ft10, 856(a6)

[0x80003620]:feq.d t6, ft11, ft10
[0x80003624]:csrrs a7, fflags, zero
[0x80003628]:sw t6, 496(a5)
[0x8000362c]:sw a7, 500(a5)
[0x80003630]:fld ft11, 864(a6)
[0x80003634]:fld ft10, 872(a6)

[0x80003638]:feq.d t6, ft11, ft10
[0x8000363c]:csrrs a7, fflags, zero
[0x80003640]:sw t6, 512(a5)
[0x80003644]:sw a7, 516(a5)
[0x80003648]:fld ft11, 880(a6)
[0x8000364c]:fld ft10, 888(a6)

[0x80003650]:feq.d t6, ft11, ft10
[0x80003654]:csrrs a7, fflags, zero
[0x80003658]:sw t6, 528(a5)
[0x8000365c]:sw a7, 532(a5)
[0x80003660]:fld ft11, 896(a6)
[0x80003664]:fld ft10, 904(a6)

[0x80003668]:feq.d t6, ft11, ft10
[0x8000366c]:csrrs a7, fflags, zero
[0x80003670]:sw t6, 544(a5)
[0x80003674]:sw a7, 548(a5)
[0x80003678]:fld ft11, 912(a6)
[0x8000367c]:fld ft10, 920(a6)

[0x80003680]:feq.d t6, ft11, ft10
[0x80003684]:csrrs a7, fflags, zero
[0x80003688]:sw t6, 560(a5)
[0x8000368c]:sw a7, 564(a5)
[0x80003690]:fld ft11, 928(a6)
[0x80003694]:fld ft10, 936(a6)

[0x80003698]:feq.d t6, ft11, ft10
[0x8000369c]:csrrs a7, fflags, zero
[0x800036a0]:sw t6, 576(a5)
[0x800036a4]:sw a7, 580(a5)
[0x800036a8]:fld ft11, 944(a6)
[0x800036ac]:fld ft10, 952(a6)

[0x800036b0]:feq.d t6, ft11, ft10
[0x800036b4]:csrrs a7, fflags, zero
[0x800036b8]:sw t6, 592(a5)
[0x800036bc]:sw a7, 596(a5)
[0x800036c0]:fld ft11, 960(a6)
[0x800036c4]:fld ft10, 968(a6)

[0x800036c8]:feq.d t6, ft11, ft10
[0x800036cc]:csrrs a7, fflags, zero
[0x800036d0]:sw t6, 608(a5)
[0x800036d4]:sw a7, 612(a5)
[0x800036d8]:fld ft11, 976(a6)
[0x800036dc]:fld ft10, 984(a6)

[0x800036e0]:feq.d t6, ft11, ft10
[0x800036e4]:csrrs a7, fflags, zero
[0x800036e8]:sw t6, 624(a5)
[0x800036ec]:sw a7, 628(a5)
[0x800036f0]:fld ft11, 992(a6)
[0x800036f4]:fld ft10, 1000(a6)

[0x800036f8]:feq.d t6, ft11, ft10
[0x800036fc]:csrrs a7, fflags, zero
[0x80003700]:sw t6, 640(a5)
[0x80003704]:sw a7, 644(a5)
[0x80003708]:fld ft11, 1008(a6)
[0x8000370c]:fld ft10, 1016(a6)

[0x80003710]:feq.d t6, ft11, ft10
[0x80003714]:csrrs a7, fflags, zero
[0x80003718]:sw t6, 656(a5)
[0x8000371c]:sw a7, 660(a5)
[0x80003720]:fld ft11, 1024(a6)
[0x80003724]:fld ft10, 1032(a6)

[0x80003728]:feq.d t6, ft11, ft10
[0x8000372c]:csrrs a7, fflags, zero
[0x80003730]:sw t6, 672(a5)
[0x80003734]:sw a7, 676(a5)
[0x80003738]:fld ft11, 1040(a6)
[0x8000373c]:fld ft10, 1048(a6)

[0x80003740]:feq.d t6, ft11, ft10
[0x80003744]:csrrs a7, fflags, zero
[0x80003748]:sw t6, 688(a5)
[0x8000374c]:sw a7, 692(a5)
[0x80003750]:fld ft11, 1056(a6)
[0x80003754]:fld ft10, 1064(a6)

[0x80003758]:feq.d t6, ft11, ft10
[0x8000375c]:csrrs a7, fflags, zero
[0x80003760]:sw t6, 704(a5)
[0x80003764]:sw a7, 708(a5)
[0x80003768]:fld ft11, 1072(a6)
[0x8000376c]:fld ft10, 1080(a6)

[0x80003770]:feq.d t6, ft11, ft10
[0x80003774]:csrrs a7, fflags, zero
[0x80003778]:sw t6, 720(a5)
[0x8000377c]:sw a7, 724(a5)
[0x80003780]:fld ft11, 1088(a6)
[0x80003784]:fld ft10, 1096(a6)

[0x80003788]:feq.d t6, ft11, ft10
[0x8000378c]:csrrs a7, fflags, zero
[0x80003790]:sw t6, 736(a5)
[0x80003794]:sw a7, 740(a5)
[0x80003798]:fld ft11, 1104(a6)
[0x8000379c]:fld ft10, 1112(a6)



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : feq.d', 'rd : x15', 'rs1 : f21', 'rs2 : f26', 'rs1 != rs2', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000011c]:feq.d a5, fs5, fs10
	-[0x80000120]:csrrs s5, fflags, zero
	-[0x80000124]:sw a5, 0(s3)
Current Store : [0x80000128] : sw s5, 4(s3) -- Store: [0x80007614]:0x00000000




Last Coverpoint : ['rd : x31', 'rs1 : f7', 'rs2 : f7', 'rs1 == rs2', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000140]:feq.d t6, ft7, ft7
	-[0x80000144]:csrrs a7, fflags, zero
	-[0x80000148]:sw t6, 0(a5)
Current Store : [0x8000014c] : sw a7, 4(a5) -- Store: [0x8000761c]:0x00000000




Last Coverpoint : ['rd : x29', 'rs1 : f12', 'rs2 : f21', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000158]:feq.d t4, fa2, fs5
	-[0x8000015c]:csrrs a7, fflags, zero
	-[0x80000160]:sw t4, 16(a5)
Current Store : [0x80000164] : sw a7, 20(a5) -- Store: [0x8000762c]:0x00000000




Last Coverpoint : ['rd : x17', 'rs1 : f29', 'rs2 : f20', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000017c]:feq.d a7, ft9, fs4
	-[0x80000180]:csrrs s5, fflags, zero
	-[0x80000184]:sw a7, 0(s3)
Current Store : [0x80000188] : sw s5, 4(s3) -- Store: [0x8000762c]:0x00000010




Last Coverpoint : ['rd : x4', 'rs1 : f15', 'rs2 : f12', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001a0]:feq.d tp, fa5, fa2
	-[0x800001a4]:csrrs a7, fflags, zero
	-[0x800001a8]:sw tp, 0(a5)
Current Store : [0x800001ac] : sw a7, 4(a5) -- Store: [0x80007634]:0x00000010




Last Coverpoint : ['rd : x13', 'rs1 : f19', 'rs2 : f4', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001b8]:feq.d a3, fs3, ft4
	-[0x800001bc]:csrrs a7, fflags, zero
	-[0x800001c0]:sw a3, 16(a5)
Current Store : [0x800001c4] : sw a7, 20(a5) -- Store: [0x80007644]:0x00000010




Last Coverpoint : ['rd : x9', 'rs1 : f4', 'rs2 : f24', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001d0]:feq.d s1, ft4, fs8
	-[0x800001d4]:csrrs a7, fflags, zero
	-[0x800001d8]:sw s1, 32(a5)
Current Store : [0x800001dc] : sw a7, 36(a5) -- Store: [0x80007654]:0x00000010




Last Coverpoint : ['rd : x20', 'rs1 : f20', 'rs2 : f18', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800001e8]:feq.d s4, fs4, fs2
	-[0x800001ec]:csrrs a7, fflags, zero
	-[0x800001f0]:sw s4, 48(a5)
Current Store : [0x800001f4] : sw a7, 52(a5) -- Store: [0x80007664]:0x00000010




Last Coverpoint : ['rd : x27', 'rs1 : f11', 'rs2 : f22', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000200]:feq.d s11, fa1, fs6
	-[0x80000204]:csrrs a7, fflags, zero
	-[0x80000208]:sw s11, 64(a5)
Current Store : [0x8000020c] : sw a7, 68(a5) -- Store: [0x80007674]:0x00000010




Last Coverpoint : ['rd : x23', 'rs1 : f24', 'rs2 : f8', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000218]:feq.d s7, fs8, fs0
	-[0x8000021c]:csrrs a7, fflags, zero
	-[0x80000220]:sw s7, 80(a5)
Current Store : [0x80000224] : sw a7, 84(a5) -- Store: [0x80007684]:0x00000010




Last Coverpoint : ['rd : x11', 'rs1 : f6', 'rs2 : f25', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000230]:feq.d a1, ft6, fs9
	-[0x80000234]:csrrs a7, fflags, zero
	-[0x80000238]:sw a1, 96(a5)
Current Store : [0x8000023c] : sw a7, 100(a5) -- Store: [0x80007694]:0x00000010




Last Coverpoint : ['rd : x22', 'rs1 : f10', 'rs2 : f5', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000248]:feq.d s6, fa0, ft5
	-[0x8000024c]:csrrs a7, fflags, zero
	-[0x80000250]:sw s6, 112(a5)
Current Store : [0x80000254] : sw a7, 116(a5) -- Store: [0x800076a4]:0x00000010




Last Coverpoint : ['rd : x19', 'rs1 : f13', 'rs2 : f3', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000260]:feq.d s3, fa3, ft3
	-[0x80000264]:csrrs a7, fflags, zero
	-[0x80000268]:sw s3, 128(a5)
Current Store : [0x8000026c] : sw a7, 132(a5) -- Store: [0x800076b4]:0x00000010




Last Coverpoint : ['rd : x5', 'rs1 : f26', 'rs2 : f23', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000278]:feq.d t0, fs10, fs7
	-[0x8000027c]:csrrs a7, fflags, zero
	-[0x80000280]:sw t0, 144(a5)
Current Store : [0x80000284] : sw a7, 148(a5) -- Store: [0x800076c4]:0x00000010




Last Coverpoint : ['rd : x18', 'rs1 : f8', 'rs2 : f27', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000290]:feq.d s2, fs0, fs11
	-[0x80000294]:csrrs a7, fflags, zero
	-[0x80000298]:sw s2, 160(a5)
Current Store : [0x8000029c] : sw a7, 164(a5) -- Store: [0x800076d4]:0x00000010




Last Coverpoint : ['rd : x6', 'rs1 : f23', 'rs2 : f0', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002a8]:feq.d t1, fs7, ft0
	-[0x800002ac]:csrrs a7, fflags, zero
	-[0x800002b0]:sw t1, 176(a5)
Current Store : [0x800002b4] : sw a7, 180(a5) -- Store: [0x800076e4]:0x00000010




Last Coverpoint : ['rd : x2', 'rs1 : f2', 'rs2 : f11', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002c0]:feq.d sp, ft2, fa1
	-[0x800002c4]:csrrs a7, fflags, zero
	-[0x800002c8]:sw sp, 192(a5)
Current Store : [0x800002cc] : sw a7, 196(a5) -- Store: [0x800076f4]:0x00000010




Last Coverpoint : ['rd : x10', 'rs1 : f27', 'rs2 : f19', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002d8]:feq.d a0, fs11, fs3
	-[0x800002dc]:csrrs a7, fflags, zero
	-[0x800002e0]:sw a0, 208(a5)
Current Store : [0x800002e4] : sw a7, 212(a5) -- Store: [0x80007704]:0x00000010




Last Coverpoint : ['rd : x3', 'rs1 : f1', 'rs2 : f13', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800002f0]:feq.d gp, ft1, fa3
	-[0x800002f4]:csrrs a7, fflags, zero
	-[0x800002f8]:sw gp, 224(a5)
Current Store : [0x800002fc] : sw a7, 228(a5) -- Store: [0x80007714]:0x00000010




Last Coverpoint : ['rd : x7', 'rs1 : f31', 'rs2 : f28', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000308]:feq.d t2, ft11, ft8
	-[0x8000030c]:csrrs a7, fflags, zero
	-[0x80000310]:sw t2, 240(a5)
Current Store : [0x80000314] : sw a7, 244(a5) -- Store: [0x80007724]:0x00000010




Last Coverpoint : ['rd : x0', 'rs1 : f9', 'rs2 : f17', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000320]:feq.d zero, fs1, fa7
	-[0x80000324]:csrrs a7, fflags, zero
	-[0x80000328]:sw zero, 256(a5)
Current Store : [0x8000032c] : sw a7, 260(a5) -- Store: [0x80007734]:0x00000010




Last Coverpoint : ['rd : x16', 'rs1 : f14', 'rs2 : f30', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000344]:feq.d a6, fa4, ft10
	-[0x80000348]:csrrs s5, fflags, zero
	-[0x8000034c]:sw a6, 0(s3)
Current Store : [0x80000350] : sw s5, 4(s3) -- Store: [0x800076bc]:0x00000010




Last Coverpoint : ['rd : x12', 'rs1 : f5', 'rs2 : f1', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000368]:feq.d a2, ft5, ft1
	-[0x8000036c]:csrrs a7, fflags, zero
	-[0x80000370]:sw a2, 0(a5)
Current Store : [0x80000374] : sw a7, 4(a5) -- Store: [0x800076c4]:0x00000010




Last Coverpoint : ['rd : x1', 'rs1 : f3', 'rs2 : f10', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000380]:feq.d ra, ft3, fa0
	-[0x80000384]:csrrs a7, fflags, zero
	-[0x80000388]:sw ra, 16(a5)
Current Store : [0x8000038c] : sw a7, 20(a5) -- Store: [0x800076d4]:0x00000010




Last Coverpoint : ['rd : x8', 'rs1 : f25', 'rs2 : f15', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000398]:feq.d fp, fs9, fa5
	-[0x8000039c]:csrrs a7, fflags, zero
	-[0x800003a0]:sw fp, 32(a5)
Current Store : [0x800003a4] : sw a7, 36(a5) -- Store: [0x800076e4]:0x00000010




Last Coverpoint : ['rd : x28', 'rs1 : f16', 'rs2 : f29', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003b0]:feq.d t3, fa6, ft9
	-[0x800003b4]:csrrs a7, fflags, zero
	-[0x800003b8]:sw t3, 48(a5)
Current Store : [0x800003bc] : sw a7, 52(a5) -- Store: [0x800076f4]:0x00000010




Last Coverpoint : ['rd : x26', 'rs1 : f22', 'rs2 : f9', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003c8]:feq.d s10, fs6, fs1
	-[0x800003cc]:csrrs a7, fflags, zero
	-[0x800003d0]:sw s10, 64(a5)
Current Store : [0x800003d4] : sw a7, 68(a5) -- Store: [0x80007704]:0x00000010




Last Coverpoint : ['rd : x30', 'rs1 : f0', 'rs2 : f14', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003e0]:feq.d t5, ft0, fa4
	-[0x800003e4]:csrrs a7, fflags, zero
	-[0x800003e8]:sw t5, 80(a5)
Current Store : [0x800003ec] : sw a7, 84(a5) -- Store: [0x80007714]:0x00000010




Last Coverpoint : ['rd : x24', 'rs1 : f28', 'rs2 : f31', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800003f8]:feq.d s8, ft8, ft11
	-[0x800003fc]:csrrs a7, fflags, zero
	-[0x80000400]:sw s8, 96(a5)
Current Store : [0x80000404] : sw a7, 100(a5) -- Store: [0x80007724]:0x00000010




Last Coverpoint : ['rd : x25', 'rs1 : f17', 'rs2 : f6', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000410]:feq.d s9, fa7, ft6
	-[0x80000414]:csrrs a7, fflags, zero
	-[0x80000418]:sw s9, 112(a5)
Current Store : [0x8000041c] : sw a7, 116(a5) -- Store: [0x80007734]:0x00000010




Last Coverpoint : ['rd : x21', 'rs1 : f18', 'rs2 : f16', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000428]:feq.d s5, fs2, fa6
	-[0x8000042c]:csrrs a7, fflags, zero
	-[0x80000430]:sw s5, 128(a5)
Current Store : [0x80000434] : sw a7, 132(a5) -- Store: [0x80007744]:0x00000010




Last Coverpoint : ['rd : x14', 'rs1 : f30', 'rs2 : f2', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000440]:feq.d a4, ft10, ft2
	-[0x80000444]:csrrs a7, fflags, zero
	-[0x80000448]:sw a4, 144(a5)
Current Store : [0x8000044c] : sw a7, 148(a5) -- Store: [0x80007754]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000458]:feq.d t6, ft11, ft10
	-[0x8000045c]:csrrs a7, fflags, zero
	-[0x80000460]:sw t6, 160(a5)
Current Store : [0x80000464] : sw a7, 164(a5) -- Store: [0x80007764]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000470]:feq.d t6, ft11, ft10
	-[0x80000474]:csrrs a7, fflags, zero
	-[0x80000478]:sw t6, 176(a5)
Current Store : [0x8000047c] : sw a7, 180(a5) -- Store: [0x80007774]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000488]:feq.d t6, ft11, ft10
	-[0x8000048c]:csrrs a7, fflags, zero
	-[0x80000490]:sw t6, 192(a5)
Current Store : [0x80000494] : sw a7, 196(a5) -- Store: [0x80007784]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004a0]:feq.d t6, ft11, ft10
	-[0x800004a4]:csrrs a7, fflags, zero
	-[0x800004a8]:sw t6, 208(a5)
Current Store : [0x800004ac] : sw a7, 212(a5) -- Store: [0x80007794]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004b8]:feq.d t6, ft11, ft10
	-[0x800004bc]:csrrs a7, fflags, zero
	-[0x800004c0]:sw t6, 224(a5)
Current Store : [0x800004c4] : sw a7, 228(a5) -- Store: [0x800077a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004d0]:feq.d t6, ft11, ft10
	-[0x800004d4]:csrrs a7, fflags, zero
	-[0x800004d8]:sw t6, 240(a5)
Current Store : [0x800004dc] : sw a7, 244(a5) -- Store: [0x800077b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800004e8]:feq.d t6, ft11, ft10
	-[0x800004ec]:csrrs a7, fflags, zero
	-[0x800004f0]:sw t6, 256(a5)
Current Store : [0x800004f4] : sw a7, 260(a5) -- Store: [0x800077c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000500]:feq.d t6, ft11, ft10
	-[0x80000504]:csrrs a7, fflags, zero
	-[0x80000508]:sw t6, 272(a5)
Current Store : [0x8000050c] : sw a7, 276(a5) -- Store: [0x800077d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000518]:feq.d t6, ft11, ft10
	-[0x8000051c]:csrrs a7, fflags, zero
	-[0x80000520]:sw t6, 288(a5)
Current Store : [0x80000524] : sw a7, 292(a5) -- Store: [0x800077e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000530]:feq.d t6, ft11, ft10
	-[0x80000534]:csrrs a7, fflags, zero
	-[0x80000538]:sw t6, 304(a5)
Current Store : [0x8000053c] : sw a7, 308(a5) -- Store: [0x800077f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000548]:feq.d t6, ft11, ft10
	-[0x8000054c]:csrrs a7, fflags, zero
	-[0x80000550]:sw t6, 320(a5)
Current Store : [0x80000554] : sw a7, 324(a5) -- Store: [0x80007804]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000560]:feq.d t6, ft11, ft10
	-[0x80000564]:csrrs a7, fflags, zero
	-[0x80000568]:sw t6, 336(a5)
Current Store : [0x8000056c] : sw a7, 340(a5) -- Store: [0x80007814]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000578]:feq.d t6, ft11, ft10
	-[0x8000057c]:csrrs a7, fflags, zero
	-[0x80000580]:sw t6, 352(a5)
Current Store : [0x80000584] : sw a7, 356(a5) -- Store: [0x80007824]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000590]:feq.d t6, ft11, ft10
	-[0x80000594]:csrrs a7, fflags, zero
	-[0x80000598]:sw t6, 368(a5)
Current Store : [0x8000059c] : sw a7, 372(a5) -- Store: [0x80007834]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005a8]:feq.d t6, ft11, ft10
	-[0x800005ac]:csrrs a7, fflags, zero
	-[0x800005b0]:sw t6, 384(a5)
Current Store : [0x800005b4] : sw a7, 388(a5) -- Store: [0x80007844]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005c0]:feq.d t6, ft11, ft10
	-[0x800005c4]:csrrs a7, fflags, zero
	-[0x800005c8]:sw t6, 400(a5)
Current Store : [0x800005cc] : sw a7, 404(a5) -- Store: [0x80007854]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005d8]:feq.d t6, ft11, ft10
	-[0x800005dc]:csrrs a7, fflags, zero
	-[0x800005e0]:sw t6, 416(a5)
Current Store : [0x800005e4] : sw a7, 420(a5) -- Store: [0x80007864]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800005f0]:feq.d t6, ft11, ft10
	-[0x800005f4]:csrrs a7, fflags, zero
	-[0x800005f8]:sw t6, 432(a5)
Current Store : [0x800005fc] : sw a7, 436(a5) -- Store: [0x80007874]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000608]:feq.d t6, ft11, ft10
	-[0x8000060c]:csrrs a7, fflags, zero
	-[0x80000610]:sw t6, 448(a5)
Current Store : [0x80000614] : sw a7, 452(a5) -- Store: [0x80007884]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000620]:feq.d t6, ft11, ft10
	-[0x80000624]:csrrs a7, fflags, zero
	-[0x80000628]:sw t6, 464(a5)
Current Store : [0x8000062c] : sw a7, 468(a5) -- Store: [0x80007894]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000638]:feq.d t6, ft11, ft10
	-[0x8000063c]:csrrs a7, fflags, zero
	-[0x80000640]:sw t6, 480(a5)
Current Store : [0x80000644] : sw a7, 484(a5) -- Store: [0x800078a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000650]:feq.d t6, ft11, ft10
	-[0x80000654]:csrrs a7, fflags, zero
	-[0x80000658]:sw t6, 496(a5)
Current Store : [0x8000065c] : sw a7, 500(a5) -- Store: [0x800078b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000668]:feq.d t6, ft11, ft10
	-[0x8000066c]:csrrs a7, fflags, zero
	-[0x80000670]:sw t6, 512(a5)
Current Store : [0x80000674] : sw a7, 516(a5) -- Store: [0x800078c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000680]:feq.d t6, ft11, ft10
	-[0x80000684]:csrrs a7, fflags, zero
	-[0x80000688]:sw t6, 528(a5)
Current Store : [0x8000068c] : sw a7, 532(a5) -- Store: [0x800078d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000698]:feq.d t6, ft11, ft10
	-[0x8000069c]:csrrs a7, fflags, zero
	-[0x800006a0]:sw t6, 544(a5)
Current Store : [0x800006a4] : sw a7, 548(a5) -- Store: [0x800078e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006b0]:feq.d t6, ft11, ft10
	-[0x800006b4]:csrrs a7, fflags, zero
	-[0x800006b8]:sw t6, 560(a5)
Current Store : [0x800006bc] : sw a7, 564(a5) -- Store: [0x800078f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006c8]:feq.d t6, ft11, ft10
	-[0x800006cc]:csrrs a7, fflags, zero
	-[0x800006d0]:sw t6, 576(a5)
Current Store : [0x800006d4] : sw a7, 580(a5) -- Store: [0x80007904]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006e0]:feq.d t6, ft11, ft10
	-[0x800006e4]:csrrs a7, fflags, zero
	-[0x800006e8]:sw t6, 592(a5)
Current Store : [0x800006ec] : sw a7, 596(a5) -- Store: [0x80007914]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800006f8]:feq.d t6, ft11, ft10
	-[0x800006fc]:csrrs a7, fflags, zero
	-[0x80000700]:sw t6, 608(a5)
Current Store : [0x80000704] : sw a7, 612(a5) -- Store: [0x80007924]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000710]:feq.d t6, ft11, ft10
	-[0x80000714]:csrrs a7, fflags, zero
	-[0x80000718]:sw t6, 624(a5)
Current Store : [0x8000071c] : sw a7, 628(a5) -- Store: [0x80007934]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000728]:feq.d t6, ft11, ft10
	-[0x8000072c]:csrrs a7, fflags, zero
	-[0x80000730]:sw t6, 640(a5)
Current Store : [0x80000734] : sw a7, 644(a5) -- Store: [0x80007944]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000740]:feq.d t6, ft11, ft10
	-[0x80000744]:csrrs a7, fflags, zero
	-[0x80000748]:sw t6, 656(a5)
Current Store : [0x8000074c] : sw a7, 660(a5) -- Store: [0x80007954]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000758]:feq.d t6, ft11, ft10
	-[0x8000075c]:csrrs a7, fflags, zero
	-[0x80000760]:sw t6, 672(a5)
Current Store : [0x80000764] : sw a7, 676(a5) -- Store: [0x80007964]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000770]:feq.d t6, ft11, ft10
	-[0x80000774]:csrrs a7, fflags, zero
	-[0x80000778]:sw t6, 688(a5)
Current Store : [0x8000077c] : sw a7, 692(a5) -- Store: [0x80007974]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000788]:feq.d t6, ft11, ft10
	-[0x8000078c]:csrrs a7, fflags, zero
	-[0x80000790]:sw t6, 704(a5)
Current Store : [0x80000794] : sw a7, 708(a5) -- Store: [0x80007984]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007a0]:feq.d t6, ft11, ft10
	-[0x800007a4]:csrrs a7, fflags, zero
	-[0x800007a8]:sw t6, 720(a5)
Current Store : [0x800007ac] : sw a7, 724(a5) -- Store: [0x80007994]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007b8]:feq.d t6, ft11, ft10
	-[0x800007bc]:csrrs a7, fflags, zero
	-[0x800007c0]:sw t6, 736(a5)
Current Store : [0x800007c4] : sw a7, 740(a5) -- Store: [0x800079a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007d0]:feq.d t6, ft11, ft10
	-[0x800007d4]:csrrs a7, fflags, zero
	-[0x800007d8]:sw t6, 752(a5)
Current Store : [0x800007dc] : sw a7, 756(a5) -- Store: [0x800079b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007e8]:feq.d t6, ft11, ft10
	-[0x800007ec]:csrrs a7, fflags, zero
	-[0x800007f0]:sw t6, 768(a5)
Current Store : [0x800007f4] : sw a7, 772(a5) -- Store: [0x800079c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000800]:feq.d t6, ft11, ft10
	-[0x80000804]:csrrs a7, fflags, zero
	-[0x80000808]:sw t6, 784(a5)
Current Store : [0x8000080c] : sw a7, 788(a5) -- Store: [0x800079d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000818]:feq.d t6, ft11, ft10
	-[0x8000081c]:csrrs a7, fflags, zero
	-[0x80000820]:sw t6, 800(a5)
Current Store : [0x80000824] : sw a7, 804(a5) -- Store: [0x800079e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000830]:feq.d t6, ft11, ft10
	-[0x80000834]:csrrs a7, fflags, zero
	-[0x80000838]:sw t6, 816(a5)
Current Store : [0x8000083c] : sw a7, 820(a5) -- Store: [0x800079f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000848]:feq.d t6, ft11, ft10
	-[0x8000084c]:csrrs a7, fflags, zero
	-[0x80000850]:sw t6, 832(a5)
Current Store : [0x80000854] : sw a7, 836(a5) -- Store: [0x80007a04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000860]:feq.d t6, ft11, ft10
	-[0x80000864]:csrrs a7, fflags, zero
	-[0x80000868]:sw t6, 848(a5)
Current Store : [0x8000086c] : sw a7, 852(a5) -- Store: [0x80007a14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000878]:feq.d t6, ft11, ft10
	-[0x8000087c]:csrrs a7, fflags, zero
	-[0x80000880]:sw t6, 864(a5)
Current Store : [0x80000884] : sw a7, 868(a5) -- Store: [0x80007a24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000890]:feq.d t6, ft11, ft10
	-[0x80000894]:csrrs a7, fflags, zero
	-[0x80000898]:sw t6, 880(a5)
Current Store : [0x8000089c] : sw a7, 884(a5) -- Store: [0x80007a34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800008a8]:feq.d t6, ft11, ft10
	-[0x800008ac]:csrrs a7, fflags, zero
	-[0x800008b0]:sw t6, 896(a5)
Current Store : [0x800008b4] : sw a7, 900(a5) -- Store: [0x80007a44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800008c0]:feq.d t6, ft11, ft10
	-[0x800008c4]:csrrs a7, fflags, zero
	-[0x800008c8]:sw t6, 912(a5)
Current Store : [0x800008cc] : sw a7, 916(a5) -- Store: [0x80007a54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800008d8]:feq.d t6, ft11, ft10
	-[0x800008dc]:csrrs a7, fflags, zero
	-[0x800008e0]:sw t6, 928(a5)
Current Store : [0x800008e4] : sw a7, 932(a5) -- Store: [0x80007a64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800008f0]:feq.d t6, ft11, ft10
	-[0x800008f4]:csrrs a7, fflags, zero
	-[0x800008f8]:sw t6, 944(a5)
Current Store : [0x800008fc] : sw a7, 948(a5) -- Store: [0x80007a74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000908]:feq.d t6, ft11, ft10
	-[0x8000090c]:csrrs a7, fflags, zero
	-[0x80000910]:sw t6, 960(a5)
Current Store : [0x80000914] : sw a7, 964(a5) -- Store: [0x80007a84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000920]:feq.d t6, ft11, ft10
	-[0x80000924]:csrrs a7, fflags, zero
	-[0x80000928]:sw t6, 976(a5)
Current Store : [0x8000092c] : sw a7, 980(a5) -- Store: [0x80007a94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000938]:feq.d t6, ft11, ft10
	-[0x8000093c]:csrrs a7, fflags, zero
	-[0x80000940]:sw t6, 992(a5)
Current Store : [0x80000944] : sw a7, 996(a5) -- Store: [0x80007aa4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000950]:feq.d t6, ft11, ft10
	-[0x80000954]:csrrs a7, fflags, zero
	-[0x80000958]:sw t6, 1008(a5)
Current Store : [0x8000095c] : sw a7, 1012(a5) -- Store: [0x80007ab4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000968]:feq.d t6, ft11, ft10
	-[0x8000096c]:csrrs a7, fflags, zero
	-[0x80000970]:sw t6, 1024(a5)
Current Store : [0x80000974] : sw a7, 1028(a5) -- Store: [0x80007ac4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000980]:feq.d t6, ft11, ft10
	-[0x80000984]:csrrs a7, fflags, zero
	-[0x80000988]:sw t6, 1040(a5)
Current Store : [0x8000098c] : sw a7, 1044(a5) -- Store: [0x80007ad4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000998]:feq.d t6, ft11, ft10
	-[0x8000099c]:csrrs a7, fflags, zero
	-[0x800009a0]:sw t6, 1056(a5)
Current Store : [0x800009a4] : sw a7, 1060(a5) -- Store: [0x80007ae4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800009b0]:feq.d t6, ft11, ft10
	-[0x800009b4]:csrrs a7, fflags, zero
	-[0x800009b8]:sw t6, 1072(a5)
Current Store : [0x800009bc] : sw a7, 1076(a5) -- Store: [0x80007af4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800009c8]:feq.d t6, ft11, ft10
	-[0x800009cc]:csrrs a7, fflags, zero
	-[0x800009d0]:sw t6, 1088(a5)
Current Store : [0x800009d4] : sw a7, 1092(a5) -- Store: [0x80007b04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800009e0]:feq.d t6, ft11, ft10
	-[0x800009e4]:csrrs a7, fflags, zero
	-[0x800009e8]:sw t6, 1104(a5)
Current Store : [0x800009ec] : sw a7, 1108(a5) -- Store: [0x80007b14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800009f8]:feq.d t6, ft11, ft10
	-[0x800009fc]:csrrs a7, fflags, zero
	-[0x80000a00]:sw t6, 1120(a5)
Current Store : [0x80000a04] : sw a7, 1124(a5) -- Store: [0x80007b24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a10]:feq.d t6, ft11, ft10
	-[0x80000a14]:csrrs a7, fflags, zero
	-[0x80000a18]:sw t6, 1136(a5)
Current Store : [0x80000a1c] : sw a7, 1140(a5) -- Store: [0x80007b34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a28]:feq.d t6, ft11, ft10
	-[0x80000a2c]:csrrs a7, fflags, zero
	-[0x80000a30]:sw t6, 1152(a5)
Current Store : [0x80000a34] : sw a7, 1156(a5) -- Store: [0x80007b44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a40]:feq.d t6, ft11, ft10
	-[0x80000a44]:csrrs a7, fflags, zero
	-[0x80000a48]:sw t6, 1168(a5)
Current Store : [0x80000a4c] : sw a7, 1172(a5) -- Store: [0x80007b54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a58]:feq.d t6, ft11, ft10
	-[0x80000a5c]:csrrs a7, fflags, zero
	-[0x80000a60]:sw t6, 1184(a5)
Current Store : [0x80000a64] : sw a7, 1188(a5) -- Store: [0x80007b64]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a70]:feq.d t6, ft11, ft10
	-[0x80000a74]:csrrs a7, fflags, zero
	-[0x80000a78]:sw t6, 1200(a5)
Current Store : [0x80000a7c] : sw a7, 1204(a5) -- Store: [0x80007b74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000a88]:feq.d t6, ft11, ft10
	-[0x80000a8c]:csrrs a7, fflags, zero
	-[0x80000a90]:sw t6, 1216(a5)
Current Store : [0x80000a94] : sw a7, 1220(a5) -- Store: [0x80007b84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000aa0]:feq.d t6, ft11, ft10
	-[0x80000aa4]:csrrs a7, fflags, zero
	-[0x80000aa8]:sw t6, 1232(a5)
Current Store : [0x80000aac] : sw a7, 1236(a5) -- Store: [0x80007b94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ab8]:feq.d t6, ft11, ft10
	-[0x80000abc]:csrrs a7, fflags, zero
	-[0x80000ac0]:sw t6, 1248(a5)
Current Store : [0x80000ac4] : sw a7, 1252(a5) -- Store: [0x80007ba4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ad0]:feq.d t6, ft11, ft10
	-[0x80000ad4]:csrrs a7, fflags, zero
	-[0x80000ad8]:sw t6, 1264(a5)
Current Store : [0x80000adc] : sw a7, 1268(a5) -- Store: [0x80007bb4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ae8]:feq.d t6, ft11, ft10
	-[0x80000aec]:csrrs a7, fflags, zero
	-[0x80000af0]:sw t6, 1280(a5)
Current Store : [0x80000af4] : sw a7, 1284(a5) -- Store: [0x80007bc4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b00]:feq.d t6, ft11, ft10
	-[0x80000b04]:csrrs a7, fflags, zero
	-[0x80000b08]:sw t6, 1296(a5)
Current Store : [0x80000b0c] : sw a7, 1300(a5) -- Store: [0x80007bd4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b18]:feq.d t6, ft11, ft10
	-[0x80000b1c]:csrrs a7, fflags, zero
	-[0x80000b20]:sw t6, 1312(a5)
Current Store : [0x80000b24] : sw a7, 1316(a5) -- Store: [0x80007be4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b30]:feq.d t6, ft11, ft10
	-[0x80000b34]:csrrs a7, fflags, zero
	-[0x80000b38]:sw t6, 1328(a5)
Current Store : [0x80000b3c] : sw a7, 1332(a5) -- Store: [0x80007bf4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b48]:feq.d t6, ft11, ft10
	-[0x80000b4c]:csrrs a7, fflags, zero
	-[0x80000b50]:sw t6, 1344(a5)
Current Store : [0x80000b54] : sw a7, 1348(a5) -- Store: [0x80007c04]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b60]:feq.d t6, ft11, ft10
	-[0x80000b64]:csrrs a7, fflags, zero
	-[0x80000b68]:sw t6, 1360(a5)
Current Store : [0x80000b6c] : sw a7, 1364(a5) -- Store: [0x80007c14]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b78]:feq.d t6, ft11, ft10
	-[0x80000b7c]:csrrs a7, fflags, zero
	-[0x80000b80]:sw t6, 1376(a5)
Current Store : [0x80000b84] : sw a7, 1380(a5) -- Store: [0x80007c24]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000b90]:feq.d t6, ft11, ft10
	-[0x80000b94]:csrrs a7, fflags, zero
	-[0x80000b98]:sw t6, 1392(a5)
Current Store : [0x80000b9c] : sw a7, 1396(a5) -- Store: [0x80007c34]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ba8]:feq.d t6, ft11, ft10
	-[0x80000bac]:csrrs a7, fflags, zero
	-[0x80000bb0]:sw t6, 1408(a5)
Current Store : [0x80000bb4] : sw a7, 1412(a5) -- Store: [0x80007c44]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000bc0]:feq.d t6, ft11, ft10
	-[0x80000bc4]:csrrs a7, fflags, zero
	-[0x80000bc8]:sw t6, 1424(a5)
Current Store : [0x80000bcc] : sw a7, 1428(a5) -- Store: [0x80007c54]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000bd8]:feq.d t6, ft11, ft10
	-[0x80000bdc]:csrrs a7, fflags, zero
	-[0x80000be0]:sw t6, 1440(a5)
Current Store : [0x80000be4] : sw a7, 1444(a5) -- Store: [0x80007c64]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000bf0]:feq.d t6, ft11, ft10
	-[0x80000bf4]:csrrs a7, fflags, zero
	-[0x80000bf8]:sw t6, 1456(a5)
Current Store : [0x80000bfc] : sw a7, 1460(a5) -- Store: [0x80007c74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c08]:feq.d t6, ft11, ft10
	-[0x80000c0c]:csrrs a7, fflags, zero
	-[0x80000c10]:sw t6, 1472(a5)
Current Store : [0x80000c14] : sw a7, 1476(a5) -- Store: [0x80007c84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c20]:feq.d t6, ft11, ft10
	-[0x80000c24]:csrrs a7, fflags, zero
	-[0x80000c28]:sw t6, 1488(a5)
Current Store : [0x80000c2c] : sw a7, 1492(a5) -- Store: [0x80007c94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c38]:feq.d t6, ft11, ft10
	-[0x80000c3c]:csrrs a7, fflags, zero
	-[0x80000c40]:sw t6, 1504(a5)
Current Store : [0x80000c44] : sw a7, 1508(a5) -- Store: [0x80007ca4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c50]:feq.d t6, ft11, ft10
	-[0x80000c54]:csrrs a7, fflags, zero
	-[0x80000c58]:sw t6, 1520(a5)
Current Store : [0x80000c5c] : sw a7, 1524(a5) -- Store: [0x80007cb4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c68]:feq.d t6, ft11, ft10
	-[0x80000c6c]:csrrs a7, fflags, zero
	-[0x80000c70]:sw t6, 1536(a5)
Current Store : [0x80000c74] : sw a7, 1540(a5) -- Store: [0x80007cc4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c80]:feq.d t6, ft11, ft10
	-[0x80000c84]:csrrs a7, fflags, zero
	-[0x80000c88]:sw t6, 1552(a5)
Current Store : [0x80000c8c] : sw a7, 1556(a5) -- Store: [0x80007cd4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000c98]:feq.d t6, ft11, ft10
	-[0x80000c9c]:csrrs a7, fflags, zero
	-[0x80000ca0]:sw t6, 1568(a5)
Current Store : [0x80000ca4] : sw a7, 1572(a5) -- Store: [0x80007ce4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000cb0]:feq.d t6, ft11, ft10
	-[0x80000cb4]:csrrs a7, fflags, zero
	-[0x80000cb8]:sw t6, 1584(a5)
Current Store : [0x80000cbc] : sw a7, 1588(a5) -- Store: [0x80007cf4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000cc8]:feq.d t6, ft11, ft10
	-[0x80000ccc]:csrrs a7, fflags, zero
	-[0x80000cd0]:sw t6, 1600(a5)
Current Store : [0x80000cd4] : sw a7, 1604(a5) -- Store: [0x80007d04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ce0]:feq.d t6, ft11, ft10
	-[0x80000ce4]:csrrs a7, fflags, zero
	-[0x80000ce8]:sw t6, 1616(a5)
Current Store : [0x80000cec] : sw a7, 1620(a5) -- Store: [0x80007d14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000cf8]:feq.d t6, ft11, ft10
	-[0x80000cfc]:csrrs a7, fflags, zero
	-[0x80000d00]:sw t6, 1632(a5)
Current Store : [0x80000d04] : sw a7, 1636(a5) -- Store: [0x80007d24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d10]:feq.d t6, ft11, ft10
	-[0x80000d14]:csrrs a7, fflags, zero
	-[0x80000d18]:sw t6, 1648(a5)
Current Store : [0x80000d1c] : sw a7, 1652(a5) -- Store: [0x80007d34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d28]:feq.d t6, ft11, ft10
	-[0x80000d2c]:csrrs a7, fflags, zero
	-[0x80000d30]:sw t6, 1664(a5)
Current Store : [0x80000d34] : sw a7, 1668(a5) -- Store: [0x80007d44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d44]:feq.d t6, ft11, ft10
	-[0x80000d48]:csrrs a7, fflags, zero
	-[0x80000d4c]:sw t6, 1680(a5)
Current Store : [0x80000d50] : sw a7, 1684(a5) -- Store: [0x80007d54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d5c]:feq.d t6, ft11, ft10
	-[0x80000d60]:csrrs a7, fflags, zero
	-[0x80000d64]:sw t6, 1696(a5)
Current Store : [0x80000d68] : sw a7, 1700(a5) -- Store: [0x80007d64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d74]:feq.d t6, ft11, ft10
	-[0x80000d78]:csrrs a7, fflags, zero
	-[0x80000d7c]:sw t6, 1712(a5)
Current Store : [0x80000d80] : sw a7, 1716(a5) -- Store: [0x80007d74]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:feq.d t6, ft11, ft10
	-[0x80000d90]:csrrs a7, fflags, zero
	-[0x80000d94]:sw t6, 1728(a5)
Current Store : [0x80000d98] : sw a7, 1732(a5) -- Store: [0x80007d84]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000da4]:feq.d t6, ft11, ft10
	-[0x80000da8]:csrrs a7, fflags, zero
	-[0x80000dac]:sw t6, 1744(a5)
Current Store : [0x80000db0] : sw a7, 1748(a5) -- Store: [0x80007d94]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000dbc]:feq.d t6, ft11, ft10
	-[0x80000dc0]:csrrs a7, fflags, zero
	-[0x80000dc4]:sw t6, 1760(a5)
Current Store : [0x80000dc8] : sw a7, 1764(a5) -- Store: [0x80007da4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:feq.d t6, ft11, ft10
	-[0x80000dd8]:csrrs a7, fflags, zero
	-[0x80000ddc]:sw t6, 1776(a5)
Current Store : [0x80000de0] : sw a7, 1780(a5) -- Store: [0x80007db4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000dec]:feq.d t6, ft11, ft10
	-[0x80000df0]:csrrs a7, fflags, zero
	-[0x80000df4]:sw t6, 1792(a5)
Current Store : [0x80000df8] : sw a7, 1796(a5) -- Store: [0x80007dc4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e04]:feq.d t6, ft11, ft10
	-[0x80000e08]:csrrs a7, fflags, zero
	-[0x80000e0c]:sw t6, 1808(a5)
Current Store : [0x80000e10] : sw a7, 1812(a5) -- Store: [0x80007dd4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e1c]:feq.d t6, ft11, ft10
	-[0x80000e20]:csrrs a7, fflags, zero
	-[0x80000e24]:sw t6, 1824(a5)
Current Store : [0x80000e28] : sw a7, 1828(a5) -- Store: [0x80007de4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e34]:feq.d t6, ft11, ft10
	-[0x80000e38]:csrrs a7, fflags, zero
	-[0x80000e3c]:sw t6, 1840(a5)
Current Store : [0x80000e40] : sw a7, 1844(a5) -- Store: [0x80007df4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:feq.d t6, ft11, ft10
	-[0x80000e50]:csrrs a7, fflags, zero
	-[0x80000e54]:sw t6, 1856(a5)
Current Store : [0x80000e58] : sw a7, 1860(a5) -- Store: [0x80007e04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e64]:feq.d t6, ft11, ft10
	-[0x80000e68]:csrrs a7, fflags, zero
	-[0x80000e6c]:sw t6, 1872(a5)
Current Store : [0x80000e70] : sw a7, 1876(a5) -- Store: [0x80007e14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e7c]:feq.d t6, ft11, ft10
	-[0x80000e80]:csrrs a7, fflags, zero
	-[0x80000e84]:sw t6, 1888(a5)
Current Store : [0x80000e88] : sw a7, 1892(a5) -- Store: [0x80007e24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000e94]:feq.d t6, ft11, ft10
	-[0x80000e98]:csrrs a7, fflags, zero
	-[0x80000e9c]:sw t6, 1904(a5)
Current Store : [0x80000ea0] : sw a7, 1908(a5) -- Store: [0x80007e34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000eac]:feq.d t6, ft11, ft10
	-[0x80000eb0]:csrrs a7, fflags, zero
	-[0x80000eb4]:sw t6, 1920(a5)
Current Store : [0x80000eb8] : sw a7, 1924(a5) -- Store: [0x80007e44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ec4]:feq.d t6, ft11, ft10
	-[0x80000ec8]:csrrs a7, fflags, zero
	-[0x80000ecc]:sw t6, 1936(a5)
Current Store : [0x80000ed0] : sw a7, 1940(a5) -- Store: [0x80007e54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000edc]:feq.d t6, ft11, ft10
	-[0x80000ee0]:csrrs a7, fflags, zero
	-[0x80000ee4]:sw t6, 1952(a5)
Current Store : [0x80000ee8] : sw a7, 1956(a5) -- Store: [0x80007e64]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000ef4]:feq.d t6, ft11, ft10
	-[0x80000ef8]:csrrs a7, fflags, zero
	-[0x80000efc]:sw t6, 1968(a5)
Current Store : [0x80000f00] : sw a7, 1972(a5) -- Store: [0x80007e74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:feq.d t6, ft11, ft10
	-[0x80000f10]:csrrs a7, fflags, zero
	-[0x80000f14]:sw t6, 1984(a5)
Current Store : [0x80000f18] : sw a7, 1988(a5) -- Store: [0x80007e84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f24]:feq.d t6, ft11, ft10
	-[0x80000f28]:csrrs a7, fflags, zero
	-[0x80000f2c]:sw t6, 2000(a5)
Current Store : [0x80000f30] : sw a7, 2004(a5) -- Store: [0x80007e94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f3c]:feq.d t6, ft11, ft10
	-[0x80000f40]:csrrs a7, fflags, zero
	-[0x80000f44]:sw t6, 2016(a5)
Current Store : [0x80000f48] : sw a7, 2020(a5) -- Store: [0x80007ea4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f5c]:feq.d t6, ft11, ft10
	-[0x80000f60]:csrrs a7, fflags, zero
	-[0x80000f64]:sw t6, 0(a5)
Current Store : [0x80000f68] : sw a7, 4(a5) -- Store: [0x80007abc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f74]:feq.d t6, ft11, ft10
	-[0x80000f78]:csrrs a7, fflags, zero
	-[0x80000f7c]:sw t6, 16(a5)
Current Store : [0x80000f80] : sw a7, 20(a5) -- Store: [0x80007acc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:feq.d t6, ft11, ft10
	-[0x80000f90]:csrrs a7, fflags, zero
	-[0x80000f94]:sw t6, 32(a5)
Current Store : [0x80000f98] : sw a7, 36(a5) -- Store: [0x80007adc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000fa4]:feq.d t6, ft11, ft10
	-[0x80000fa8]:csrrs a7, fflags, zero
	-[0x80000fac]:sw t6, 48(a5)
Current Store : [0x80000fb0] : sw a7, 52(a5) -- Store: [0x80007aec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000fbc]:feq.d t6, ft11, ft10
	-[0x80000fc0]:csrrs a7, fflags, zero
	-[0x80000fc4]:sw t6, 64(a5)
Current Store : [0x80000fc8] : sw a7, 68(a5) -- Store: [0x80007afc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000fd4]:feq.d t6, ft11, ft10
	-[0x80000fd8]:csrrs a7, fflags, zero
	-[0x80000fdc]:sw t6, 80(a5)
Current Store : [0x80000fe0] : sw a7, 84(a5) -- Store: [0x80007b0c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000fec]:feq.d t6, ft11, ft10
	-[0x80000ff0]:csrrs a7, fflags, zero
	-[0x80000ff4]:sw t6, 96(a5)
Current Store : [0x80000ff8] : sw a7, 100(a5) -- Store: [0x80007b1c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001004]:feq.d t6, ft11, ft10
	-[0x80001008]:csrrs a7, fflags, zero
	-[0x8000100c]:sw t6, 112(a5)
Current Store : [0x80001010] : sw a7, 116(a5) -- Store: [0x80007b2c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000101c]:feq.d t6, ft11, ft10
	-[0x80001020]:csrrs a7, fflags, zero
	-[0x80001024]:sw t6, 128(a5)
Current Store : [0x80001028] : sw a7, 132(a5) -- Store: [0x80007b3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001034]:feq.d t6, ft11, ft10
	-[0x80001038]:csrrs a7, fflags, zero
	-[0x8000103c]:sw t6, 144(a5)
Current Store : [0x80001040] : sw a7, 148(a5) -- Store: [0x80007b4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000104c]:feq.d t6, ft11, ft10
	-[0x80001050]:csrrs a7, fflags, zero
	-[0x80001054]:sw t6, 160(a5)
Current Store : [0x80001058] : sw a7, 164(a5) -- Store: [0x80007b5c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001064]:feq.d t6, ft11, ft10
	-[0x80001068]:csrrs a7, fflags, zero
	-[0x8000106c]:sw t6, 176(a5)
Current Store : [0x80001070] : sw a7, 180(a5) -- Store: [0x80007b6c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000107c]:feq.d t6, ft11, ft10
	-[0x80001080]:csrrs a7, fflags, zero
	-[0x80001084]:sw t6, 192(a5)
Current Store : [0x80001088] : sw a7, 196(a5) -- Store: [0x80007b7c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001094]:feq.d t6, ft11, ft10
	-[0x80001098]:csrrs a7, fflags, zero
	-[0x8000109c]:sw t6, 208(a5)
Current Store : [0x800010a0] : sw a7, 212(a5) -- Store: [0x80007b8c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800010ac]:feq.d t6, ft11, ft10
	-[0x800010b0]:csrrs a7, fflags, zero
	-[0x800010b4]:sw t6, 224(a5)
Current Store : [0x800010b8] : sw a7, 228(a5) -- Store: [0x80007b9c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800010c4]:feq.d t6, ft11, ft10
	-[0x800010c8]:csrrs a7, fflags, zero
	-[0x800010cc]:sw t6, 240(a5)
Current Store : [0x800010d0] : sw a7, 244(a5) -- Store: [0x80007bac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800010dc]:feq.d t6, ft11, ft10
	-[0x800010e0]:csrrs a7, fflags, zero
	-[0x800010e4]:sw t6, 256(a5)
Current Store : [0x800010e8] : sw a7, 260(a5) -- Store: [0x80007bbc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800010f4]:feq.d t6, ft11, ft10
	-[0x800010f8]:csrrs a7, fflags, zero
	-[0x800010fc]:sw t6, 272(a5)
Current Store : [0x80001100] : sw a7, 276(a5) -- Store: [0x80007bcc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000110c]:feq.d t6, ft11, ft10
	-[0x80001110]:csrrs a7, fflags, zero
	-[0x80001114]:sw t6, 288(a5)
Current Store : [0x80001118] : sw a7, 292(a5) -- Store: [0x80007bdc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001124]:feq.d t6, ft11, ft10
	-[0x80001128]:csrrs a7, fflags, zero
	-[0x8000112c]:sw t6, 304(a5)
Current Store : [0x80001130] : sw a7, 308(a5) -- Store: [0x80007bec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000113c]:feq.d t6, ft11, ft10
	-[0x80001140]:csrrs a7, fflags, zero
	-[0x80001144]:sw t6, 320(a5)
Current Store : [0x80001148] : sw a7, 324(a5) -- Store: [0x80007bfc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001154]:feq.d t6, ft11, ft10
	-[0x80001158]:csrrs a7, fflags, zero
	-[0x8000115c]:sw t6, 336(a5)
Current Store : [0x80001160] : sw a7, 340(a5) -- Store: [0x80007c0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000116c]:feq.d t6, ft11, ft10
	-[0x80001170]:csrrs a7, fflags, zero
	-[0x80001174]:sw t6, 352(a5)
Current Store : [0x80001178] : sw a7, 356(a5) -- Store: [0x80007c1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001184]:feq.d t6, ft11, ft10
	-[0x80001188]:csrrs a7, fflags, zero
	-[0x8000118c]:sw t6, 368(a5)
Current Store : [0x80001190] : sw a7, 372(a5) -- Store: [0x80007c2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000119c]:feq.d t6, ft11, ft10
	-[0x800011a0]:csrrs a7, fflags, zero
	-[0x800011a4]:sw t6, 384(a5)
Current Store : [0x800011a8] : sw a7, 388(a5) -- Store: [0x80007c3c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800011b4]:feq.d t6, ft11, ft10
	-[0x800011b8]:csrrs a7, fflags, zero
	-[0x800011bc]:sw t6, 400(a5)
Current Store : [0x800011c0] : sw a7, 404(a5) -- Store: [0x80007c4c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800011cc]:feq.d t6, ft11, ft10
	-[0x800011d0]:csrrs a7, fflags, zero
	-[0x800011d4]:sw t6, 416(a5)
Current Store : [0x800011d8] : sw a7, 420(a5) -- Store: [0x80007c5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800011e4]:feq.d t6, ft11, ft10
	-[0x800011e8]:csrrs a7, fflags, zero
	-[0x800011ec]:sw t6, 432(a5)
Current Store : [0x800011f0] : sw a7, 436(a5) -- Store: [0x80007c6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800011fc]:feq.d t6, ft11, ft10
	-[0x80001200]:csrrs a7, fflags, zero
	-[0x80001204]:sw t6, 448(a5)
Current Store : [0x80001208] : sw a7, 452(a5) -- Store: [0x80007c7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001214]:feq.d t6, ft11, ft10
	-[0x80001218]:csrrs a7, fflags, zero
	-[0x8000121c]:sw t6, 464(a5)
Current Store : [0x80001220] : sw a7, 468(a5) -- Store: [0x80007c8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000122c]:feq.d t6, ft11, ft10
	-[0x80001230]:csrrs a7, fflags, zero
	-[0x80001234]:sw t6, 480(a5)
Current Store : [0x80001238] : sw a7, 484(a5) -- Store: [0x80007c9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001244]:feq.d t6, ft11, ft10
	-[0x80001248]:csrrs a7, fflags, zero
	-[0x8000124c]:sw t6, 496(a5)
Current Store : [0x80001250] : sw a7, 500(a5) -- Store: [0x80007cac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000125c]:feq.d t6, ft11, ft10
	-[0x80001260]:csrrs a7, fflags, zero
	-[0x80001264]:sw t6, 512(a5)
Current Store : [0x80001268] : sw a7, 516(a5) -- Store: [0x80007cbc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001274]:feq.d t6, ft11, ft10
	-[0x80001278]:csrrs a7, fflags, zero
	-[0x8000127c]:sw t6, 528(a5)
Current Store : [0x80001280] : sw a7, 532(a5) -- Store: [0x80007ccc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000128c]:feq.d t6, ft11, ft10
	-[0x80001290]:csrrs a7, fflags, zero
	-[0x80001294]:sw t6, 544(a5)
Current Store : [0x80001298] : sw a7, 548(a5) -- Store: [0x80007cdc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800012a4]:feq.d t6, ft11, ft10
	-[0x800012a8]:csrrs a7, fflags, zero
	-[0x800012ac]:sw t6, 560(a5)
Current Store : [0x800012b0] : sw a7, 564(a5) -- Store: [0x80007cec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800012bc]:feq.d t6, ft11, ft10
	-[0x800012c0]:csrrs a7, fflags, zero
	-[0x800012c4]:sw t6, 576(a5)
Current Store : [0x800012c8] : sw a7, 580(a5) -- Store: [0x80007cfc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800012d4]:feq.d t6, ft11, ft10
	-[0x800012d8]:csrrs a7, fflags, zero
	-[0x800012dc]:sw t6, 592(a5)
Current Store : [0x800012e0] : sw a7, 596(a5) -- Store: [0x80007d0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800012ec]:feq.d t6, ft11, ft10
	-[0x800012f0]:csrrs a7, fflags, zero
	-[0x800012f4]:sw t6, 608(a5)
Current Store : [0x800012f8] : sw a7, 612(a5) -- Store: [0x80007d1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001304]:feq.d t6, ft11, ft10
	-[0x80001308]:csrrs a7, fflags, zero
	-[0x8000130c]:sw t6, 624(a5)
Current Store : [0x80001310] : sw a7, 628(a5) -- Store: [0x80007d2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000131c]:feq.d t6, ft11, ft10
	-[0x80001320]:csrrs a7, fflags, zero
	-[0x80001324]:sw t6, 640(a5)
Current Store : [0x80001328] : sw a7, 644(a5) -- Store: [0x80007d3c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001334]:feq.d t6, ft11, ft10
	-[0x80001338]:csrrs a7, fflags, zero
	-[0x8000133c]:sw t6, 656(a5)
Current Store : [0x80001340] : sw a7, 660(a5) -- Store: [0x80007d4c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000134c]:feq.d t6, ft11, ft10
	-[0x80001350]:csrrs a7, fflags, zero
	-[0x80001354]:sw t6, 672(a5)
Current Store : [0x80001358] : sw a7, 676(a5) -- Store: [0x80007d5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001364]:feq.d t6, ft11, ft10
	-[0x80001368]:csrrs a7, fflags, zero
	-[0x8000136c]:sw t6, 688(a5)
Current Store : [0x80001370] : sw a7, 692(a5) -- Store: [0x80007d6c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000137c]:feq.d t6, ft11, ft10
	-[0x80001380]:csrrs a7, fflags, zero
	-[0x80001384]:sw t6, 704(a5)
Current Store : [0x80001388] : sw a7, 708(a5) -- Store: [0x80007d7c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001394]:feq.d t6, ft11, ft10
	-[0x80001398]:csrrs a7, fflags, zero
	-[0x8000139c]:sw t6, 720(a5)
Current Store : [0x800013a0] : sw a7, 724(a5) -- Store: [0x80007d8c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800013ac]:feq.d t6, ft11, ft10
	-[0x800013b0]:csrrs a7, fflags, zero
	-[0x800013b4]:sw t6, 736(a5)
Current Store : [0x800013b8] : sw a7, 740(a5) -- Store: [0x80007d9c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800013c4]:feq.d t6, ft11, ft10
	-[0x800013c8]:csrrs a7, fflags, zero
	-[0x800013cc]:sw t6, 752(a5)
Current Store : [0x800013d0] : sw a7, 756(a5) -- Store: [0x80007dac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800013dc]:feq.d t6, ft11, ft10
	-[0x800013e0]:csrrs a7, fflags, zero
	-[0x800013e4]:sw t6, 768(a5)
Current Store : [0x800013e8] : sw a7, 772(a5) -- Store: [0x80007dbc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800013f4]:feq.d t6, ft11, ft10
	-[0x800013f8]:csrrs a7, fflags, zero
	-[0x800013fc]:sw t6, 784(a5)
Current Store : [0x80001400] : sw a7, 788(a5) -- Store: [0x80007dcc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000140c]:feq.d t6, ft11, ft10
	-[0x80001410]:csrrs a7, fflags, zero
	-[0x80001414]:sw t6, 800(a5)
Current Store : [0x80001418] : sw a7, 804(a5) -- Store: [0x80007ddc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001424]:feq.d t6, ft11, ft10
	-[0x80001428]:csrrs a7, fflags, zero
	-[0x8000142c]:sw t6, 816(a5)
Current Store : [0x80001430] : sw a7, 820(a5) -- Store: [0x80007dec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000143c]:feq.d t6, ft11, ft10
	-[0x80001440]:csrrs a7, fflags, zero
	-[0x80001444]:sw t6, 832(a5)
Current Store : [0x80001448] : sw a7, 836(a5) -- Store: [0x80007dfc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001454]:feq.d t6, ft11, ft10
	-[0x80001458]:csrrs a7, fflags, zero
	-[0x8000145c]:sw t6, 848(a5)
Current Store : [0x80001460] : sw a7, 852(a5) -- Store: [0x80007e0c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000146c]:feq.d t6, ft11, ft10
	-[0x80001470]:csrrs a7, fflags, zero
	-[0x80001474]:sw t6, 864(a5)
Current Store : [0x80001478] : sw a7, 868(a5) -- Store: [0x80007e1c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001484]:feq.d t6, ft11, ft10
	-[0x80001488]:csrrs a7, fflags, zero
	-[0x8000148c]:sw t6, 880(a5)
Current Store : [0x80001490] : sw a7, 884(a5) -- Store: [0x80007e2c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000149c]:feq.d t6, ft11, ft10
	-[0x800014a0]:csrrs a7, fflags, zero
	-[0x800014a4]:sw t6, 896(a5)
Current Store : [0x800014a8] : sw a7, 900(a5) -- Store: [0x80007e3c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800014b4]:feq.d t6, ft11, ft10
	-[0x800014b8]:csrrs a7, fflags, zero
	-[0x800014bc]:sw t6, 912(a5)
Current Store : [0x800014c0] : sw a7, 916(a5) -- Store: [0x80007e4c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800014cc]:feq.d t6, ft11, ft10
	-[0x800014d0]:csrrs a7, fflags, zero
	-[0x800014d4]:sw t6, 928(a5)
Current Store : [0x800014d8] : sw a7, 932(a5) -- Store: [0x80007e5c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800014e4]:feq.d t6, ft11, ft10
	-[0x800014e8]:csrrs a7, fflags, zero
	-[0x800014ec]:sw t6, 944(a5)
Current Store : [0x800014f0] : sw a7, 948(a5) -- Store: [0x80007e6c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800014fc]:feq.d t6, ft11, ft10
	-[0x80001500]:csrrs a7, fflags, zero
	-[0x80001504]:sw t6, 960(a5)
Current Store : [0x80001508] : sw a7, 964(a5) -- Store: [0x80007e7c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001514]:feq.d t6, ft11, ft10
	-[0x80001518]:csrrs a7, fflags, zero
	-[0x8000151c]:sw t6, 976(a5)
Current Store : [0x80001520] : sw a7, 980(a5) -- Store: [0x80007e8c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000152c]:feq.d t6, ft11, ft10
	-[0x80001530]:csrrs a7, fflags, zero
	-[0x80001534]:sw t6, 992(a5)
Current Store : [0x80001538] : sw a7, 996(a5) -- Store: [0x80007e9c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001544]:feq.d t6, ft11, ft10
	-[0x80001548]:csrrs a7, fflags, zero
	-[0x8000154c]:sw t6, 1008(a5)
Current Store : [0x80001550] : sw a7, 1012(a5) -- Store: [0x80007eac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000155c]:feq.d t6, ft11, ft10
	-[0x80001560]:csrrs a7, fflags, zero
	-[0x80001564]:sw t6, 1024(a5)
Current Store : [0x80001568] : sw a7, 1028(a5) -- Store: [0x80007ebc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001574]:feq.d t6, ft11, ft10
	-[0x80001578]:csrrs a7, fflags, zero
	-[0x8000157c]:sw t6, 1040(a5)
Current Store : [0x80001580] : sw a7, 1044(a5) -- Store: [0x80007ecc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000158c]:feq.d t6, ft11, ft10
	-[0x80001590]:csrrs a7, fflags, zero
	-[0x80001594]:sw t6, 1056(a5)
Current Store : [0x80001598] : sw a7, 1060(a5) -- Store: [0x80007edc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800015a4]:feq.d t6, ft11, ft10
	-[0x800015a8]:csrrs a7, fflags, zero
	-[0x800015ac]:sw t6, 1072(a5)
Current Store : [0x800015b0] : sw a7, 1076(a5) -- Store: [0x80007eec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800015bc]:feq.d t6, ft11, ft10
	-[0x800015c0]:csrrs a7, fflags, zero
	-[0x800015c4]:sw t6, 1088(a5)
Current Store : [0x800015c8] : sw a7, 1092(a5) -- Store: [0x80007efc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800015d4]:feq.d t6, ft11, ft10
	-[0x800015d8]:csrrs a7, fflags, zero
	-[0x800015dc]:sw t6, 1104(a5)
Current Store : [0x800015e0] : sw a7, 1108(a5) -- Store: [0x80007f0c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800015ec]:feq.d t6, ft11, ft10
	-[0x800015f0]:csrrs a7, fflags, zero
	-[0x800015f4]:sw t6, 1120(a5)
Current Store : [0x800015f8] : sw a7, 1124(a5) -- Store: [0x80007f1c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001604]:feq.d t6, ft11, ft10
	-[0x80001608]:csrrs a7, fflags, zero
	-[0x8000160c]:sw t6, 1136(a5)
Current Store : [0x80001610] : sw a7, 1140(a5) -- Store: [0x80007f2c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000161c]:feq.d t6, ft11, ft10
	-[0x80001620]:csrrs a7, fflags, zero
	-[0x80001624]:sw t6, 1152(a5)
Current Store : [0x80001628] : sw a7, 1156(a5) -- Store: [0x80007f3c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001634]:feq.d t6, ft11, ft10
	-[0x80001638]:csrrs a7, fflags, zero
	-[0x8000163c]:sw t6, 1168(a5)
Current Store : [0x80001640] : sw a7, 1172(a5) -- Store: [0x80007f4c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000164c]:feq.d t6, ft11, ft10
	-[0x80001650]:csrrs a7, fflags, zero
	-[0x80001654]:sw t6, 1184(a5)
Current Store : [0x80001658] : sw a7, 1188(a5) -- Store: [0x80007f5c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001664]:feq.d t6, ft11, ft10
	-[0x80001668]:csrrs a7, fflags, zero
	-[0x8000166c]:sw t6, 1200(a5)
Current Store : [0x80001670] : sw a7, 1204(a5) -- Store: [0x80007f6c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000167c]:feq.d t6, ft11, ft10
	-[0x80001680]:csrrs a7, fflags, zero
	-[0x80001684]:sw t6, 1216(a5)
Current Store : [0x80001688] : sw a7, 1220(a5) -- Store: [0x80007f7c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001694]:feq.d t6, ft11, ft10
	-[0x80001698]:csrrs a7, fflags, zero
	-[0x8000169c]:sw t6, 1232(a5)
Current Store : [0x800016a0] : sw a7, 1236(a5) -- Store: [0x80007f8c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800016ac]:feq.d t6, ft11, ft10
	-[0x800016b0]:csrrs a7, fflags, zero
	-[0x800016b4]:sw t6, 1248(a5)
Current Store : [0x800016b8] : sw a7, 1252(a5) -- Store: [0x80007f9c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800016c4]:feq.d t6, ft11, ft10
	-[0x800016c8]:csrrs a7, fflags, zero
	-[0x800016cc]:sw t6, 1264(a5)
Current Store : [0x800016d0] : sw a7, 1268(a5) -- Store: [0x80007fac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800016dc]:feq.d t6, ft11, ft10
	-[0x800016e0]:csrrs a7, fflags, zero
	-[0x800016e4]:sw t6, 1280(a5)
Current Store : [0x800016e8] : sw a7, 1284(a5) -- Store: [0x80007fbc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800016f4]:feq.d t6, ft11, ft10
	-[0x800016f8]:csrrs a7, fflags, zero
	-[0x800016fc]:sw t6, 1296(a5)
Current Store : [0x80001700] : sw a7, 1300(a5) -- Store: [0x80007fcc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000170c]:feq.d t6, ft11, ft10
	-[0x80001710]:csrrs a7, fflags, zero
	-[0x80001714]:sw t6, 1312(a5)
Current Store : [0x80001718] : sw a7, 1316(a5) -- Store: [0x80007fdc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001724]:feq.d t6, ft11, ft10
	-[0x80001728]:csrrs a7, fflags, zero
	-[0x8000172c]:sw t6, 1328(a5)
Current Store : [0x80001730] : sw a7, 1332(a5) -- Store: [0x80007fec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000173c]:feq.d t6, ft11, ft10
	-[0x80001740]:csrrs a7, fflags, zero
	-[0x80001744]:sw t6, 1344(a5)
Current Store : [0x80001748] : sw a7, 1348(a5) -- Store: [0x80007ffc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001754]:feq.d t6, ft11, ft10
	-[0x80001758]:csrrs a7, fflags, zero
	-[0x8000175c]:sw t6, 1360(a5)
Current Store : [0x80001760] : sw a7, 1364(a5) -- Store: [0x8000800c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000176c]:feq.d t6, ft11, ft10
	-[0x80001770]:csrrs a7, fflags, zero
	-[0x80001774]:sw t6, 1376(a5)
Current Store : [0x80001778] : sw a7, 1380(a5) -- Store: [0x8000801c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001784]:feq.d t6, ft11, ft10
	-[0x80001788]:csrrs a7, fflags, zero
	-[0x8000178c]:sw t6, 1392(a5)
Current Store : [0x80001790] : sw a7, 1396(a5) -- Store: [0x8000802c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000179c]:feq.d t6, ft11, ft10
	-[0x800017a0]:csrrs a7, fflags, zero
	-[0x800017a4]:sw t6, 1408(a5)
Current Store : [0x800017a8] : sw a7, 1412(a5) -- Store: [0x8000803c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800017b4]:feq.d t6, ft11, ft10
	-[0x800017b8]:csrrs a7, fflags, zero
	-[0x800017bc]:sw t6, 1424(a5)
Current Store : [0x800017c0] : sw a7, 1428(a5) -- Store: [0x8000804c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800017cc]:feq.d t6, ft11, ft10
	-[0x800017d0]:csrrs a7, fflags, zero
	-[0x800017d4]:sw t6, 1440(a5)
Current Store : [0x800017d8] : sw a7, 1444(a5) -- Store: [0x8000805c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800017e4]:feq.d t6, ft11, ft10
	-[0x800017e8]:csrrs a7, fflags, zero
	-[0x800017ec]:sw t6, 1456(a5)
Current Store : [0x800017f0] : sw a7, 1460(a5) -- Store: [0x8000806c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800017fc]:feq.d t6, ft11, ft10
	-[0x80001800]:csrrs a7, fflags, zero
	-[0x80001804]:sw t6, 1472(a5)
Current Store : [0x80001808] : sw a7, 1476(a5) -- Store: [0x8000807c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001814]:feq.d t6, ft11, ft10
	-[0x80001818]:csrrs a7, fflags, zero
	-[0x8000181c]:sw t6, 1488(a5)
Current Store : [0x80001820] : sw a7, 1492(a5) -- Store: [0x8000808c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000182c]:feq.d t6, ft11, ft10
	-[0x80001830]:csrrs a7, fflags, zero
	-[0x80001834]:sw t6, 1504(a5)
Current Store : [0x80001838] : sw a7, 1508(a5) -- Store: [0x8000809c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001844]:feq.d t6, ft11, ft10
	-[0x80001848]:csrrs a7, fflags, zero
	-[0x8000184c]:sw t6, 1520(a5)
Current Store : [0x80001850] : sw a7, 1524(a5) -- Store: [0x800080ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000185c]:feq.d t6, ft11, ft10
	-[0x80001860]:csrrs a7, fflags, zero
	-[0x80001864]:sw t6, 1536(a5)
Current Store : [0x80001868] : sw a7, 1540(a5) -- Store: [0x800080bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001874]:feq.d t6, ft11, ft10
	-[0x80001878]:csrrs a7, fflags, zero
	-[0x8000187c]:sw t6, 1552(a5)
Current Store : [0x80001880] : sw a7, 1556(a5) -- Store: [0x800080cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000188c]:feq.d t6, ft11, ft10
	-[0x80001890]:csrrs a7, fflags, zero
	-[0x80001894]:sw t6, 1568(a5)
Current Store : [0x80001898] : sw a7, 1572(a5) -- Store: [0x800080dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800018a4]:feq.d t6, ft11, ft10
	-[0x800018a8]:csrrs a7, fflags, zero
	-[0x800018ac]:sw t6, 1584(a5)
Current Store : [0x800018b0] : sw a7, 1588(a5) -- Store: [0x800080ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800018bc]:feq.d t6, ft11, ft10
	-[0x800018c0]:csrrs a7, fflags, zero
	-[0x800018c4]:sw t6, 1600(a5)
Current Store : [0x800018c8] : sw a7, 1604(a5) -- Store: [0x800080fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800018d4]:feq.d t6, ft11, ft10
	-[0x800018d8]:csrrs a7, fflags, zero
	-[0x800018dc]:sw t6, 1616(a5)
Current Store : [0x800018e0] : sw a7, 1620(a5) -- Store: [0x8000810c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800018ec]:feq.d t6, ft11, ft10
	-[0x800018f0]:csrrs a7, fflags, zero
	-[0x800018f4]:sw t6, 1632(a5)
Current Store : [0x800018f8] : sw a7, 1636(a5) -- Store: [0x8000811c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001904]:feq.d t6, ft11, ft10
	-[0x80001908]:csrrs a7, fflags, zero
	-[0x8000190c]:sw t6, 1648(a5)
Current Store : [0x80001910] : sw a7, 1652(a5) -- Store: [0x8000812c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000191c]:feq.d t6, ft11, ft10
	-[0x80001920]:csrrs a7, fflags, zero
	-[0x80001924]:sw t6, 1664(a5)
Current Store : [0x80001928] : sw a7, 1668(a5) -- Store: [0x8000813c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001938]:feq.d t6, ft11, ft10
	-[0x8000193c]:csrrs a7, fflags, zero
	-[0x80001940]:sw t6, 1680(a5)
Current Store : [0x80001944] : sw a7, 1684(a5) -- Store: [0x8000814c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001950]:feq.d t6, ft11, ft10
	-[0x80001954]:csrrs a7, fflags, zero
	-[0x80001958]:sw t6, 1696(a5)
Current Store : [0x8000195c] : sw a7, 1700(a5) -- Store: [0x8000815c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001968]:feq.d t6, ft11, ft10
	-[0x8000196c]:csrrs a7, fflags, zero
	-[0x80001970]:sw t6, 1712(a5)
Current Store : [0x80001974] : sw a7, 1716(a5) -- Store: [0x8000816c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001980]:feq.d t6, ft11, ft10
	-[0x80001984]:csrrs a7, fflags, zero
	-[0x80001988]:sw t6, 1728(a5)
Current Store : [0x8000198c] : sw a7, 1732(a5) -- Store: [0x8000817c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001998]:feq.d t6, ft11, ft10
	-[0x8000199c]:csrrs a7, fflags, zero
	-[0x800019a0]:sw t6, 1744(a5)
Current Store : [0x800019a4] : sw a7, 1748(a5) -- Store: [0x8000818c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800019b0]:feq.d t6, ft11, ft10
	-[0x800019b4]:csrrs a7, fflags, zero
	-[0x800019b8]:sw t6, 1760(a5)
Current Store : [0x800019bc] : sw a7, 1764(a5) -- Store: [0x8000819c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800019c8]:feq.d t6, ft11, ft10
	-[0x800019cc]:csrrs a7, fflags, zero
	-[0x800019d0]:sw t6, 1776(a5)
Current Store : [0x800019d4] : sw a7, 1780(a5) -- Store: [0x800081ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800019e0]:feq.d t6, ft11, ft10
	-[0x800019e4]:csrrs a7, fflags, zero
	-[0x800019e8]:sw t6, 1792(a5)
Current Store : [0x800019ec] : sw a7, 1796(a5) -- Store: [0x800081bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800019f8]:feq.d t6, ft11, ft10
	-[0x800019fc]:csrrs a7, fflags, zero
	-[0x80001a00]:sw t6, 1808(a5)
Current Store : [0x80001a04] : sw a7, 1812(a5) -- Store: [0x800081cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a10]:feq.d t6, ft11, ft10
	-[0x80001a14]:csrrs a7, fflags, zero
	-[0x80001a18]:sw t6, 1824(a5)
Current Store : [0x80001a1c] : sw a7, 1828(a5) -- Store: [0x800081dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a28]:feq.d t6, ft11, ft10
	-[0x80001a2c]:csrrs a7, fflags, zero
	-[0x80001a30]:sw t6, 1840(a5)
Current Store : [0x80001a34] : sw a7, 1844(a5) -- Store: [0x800081ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a40]:feq.d t6, ft11, ft10
	-[0x80001a44]:csrrs a7, fflags, zero
	-[0x80001a48]:sw t6, 1856(a5)
Current Store : [0x80001a4c] : sw a7, 1860(a5) -- Store: [0x800081fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a58]:feq.d t6, ft11, ft10
	-[0x80001a5c]:csrrs a7, fflags, zero
	-[0x80001a60]:sw t6, 1872(a5)
Current Store : [0x80001a64] : sw a7, 1876(a5) -- Store: [0x8000820c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a70]:feq.d t6, ft11, ft10
	-[0x80001a74]:csrrs a7, fflags, zero
	-[0x80001a78]:sw t6, 1888(a5)
Current Store : [0x80001a7c] : sw a7, 1892(a5) -- Store: [0x8000821c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001a88]:feq.d t6, ft11, ft10
	-[0x80001a8c]:csrrs a7, fflags, zero
	-[0x80001a90]:sw t6, 1904(a5)
Current Store : [0x80001a94] : sw a7, 1908(a5) -- Store: [0x8000822c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001aa0]:feq.d t6, ft11, ft10
	-[0x80001aa4]:csrrs a7, fflags, zero
	-[0x80001aa8]:sw t6, 1920(a5)
Current Store : [0x80001aac] : sw a7, 1924(a5) -- Store: [0x8000823c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ab8]:feq.d t6, ft11, ft10
	-[0x80001abc]:csrrs a7, fflags, zero
	-[0x80001ac0]:sw t6, 1936(a5)
Current Store : [0x80001ac4] : sw a7, 1940(a5) -- Store: [0x8000824c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ad0]:feq.d t6, ft11, ft10
	-[0x80001ad4]:csrrs a7, fflags, zero
	-[0x80001ad8]:sw t6, 1952(a5)
Current Store : [0x80001adc] : sw a7, 1956(a5) -- Store: [0x8000825c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ae8]:feq.d t6, ft11, ft10
	-[0x80001aec]:csrrs a7, fflags, zero
	-[0x80001af0]:sw t6, 1968(a5)
Current Store : [0x80001af4] : sw a7, 1972(a5) -- Store: [0x8000826c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b00]:feq.d t6, ft11, ft10
	-[0x80001b04]:csrrs a7, fflags, zero
	-[0x80001b08]:sw t6, 1984(a5)
Current Store : [0x80001b0c] : sw a7, 1988(a5) -- Store: [0x8000827c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b18]:feq.d t6, ft11, ft10
	-[0x80001b1c]:csrrs a7, fflags, zero
	-[0x80001b20]:sw t6, 2000(a5)
Current Store : [0x80001b24] : sw a7, 2004(a5) -- Store: [0x8000828c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b30]:feq.d t6, ft11, ft10
	-[0x80001b34]:csrrs a7, fflags, zero
	-[0x80001b38]:sw t6, 2016(a5)
Current Store : [0x80001b3c] : sw a7, 2020(a5) -- Store: [0x8000829c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b50]:feq.d t6, ft11, ft10
	-[0x80001b54]:csrrs a7, fflags, zero
	-[0x80001b58]:sw t6, 0(a5)
Current Store : [0x80001b5c] : sw a7, 4(a5) -- Store: [0x80007eb4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b68]:feq.d t6, ft11, ft10
	-[0x80001b6c]:csrrs a7, fflags, zero
	-[0x80001b70]:sw t6, 16(a5)
Current Store : [0x80001b74] : sw a7, 20(a5) -- Store: [0x80007ec4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b80]:feq.d t6, ft11, ft10
	-[0x80001b84]:csrrs a7, fflags, zero
	-[0x80001b88]:sw t6, 32(a5)
Current Store : [0x80001b8c] : sw a7, 36(a5) -- Store: [0x80007ed4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001b98]:feq.d t6, ft11, ft10
	-[0x80001b9c]:csrrs a7, fflags, zero
	-[0x80001ba0]:sw t6, 48(a5)
Current Store : [0x80001ba4] : sw a7, 52(a5) -- Store: [0x80007ee4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001bb0]:feq.d t6, ft11, ft10
	-[0x80001bb4]:csrrs a7, fflags, zero
	-[0x80001bb8]:sw t6, 64(a5)
Current Store : [0x80001bbc] : sw a7, 68(a5) -- Store: [0x80007ef4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001bc8]:feq.d t6, ft11, ft10
	-[0x80001bcc]:csrrs a7, fflags, zero
	-[0x80001bd0]:sw t6, 80(a5)
Current Store : [0x80001bd4] : sw a7, 84(a5) -- Store: [0x80007f04]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001be0]:feq.d t6, ft11, ft10
	-[0x80001be4]:csrrs a7, fflags, zero
	-[0x80001be8]:sw t6, 96(a5)
Current Store : [0x80001bec] : sw a7, 100(a5) -- Store: [0x80007f14]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001bf8]:feq.d t6, ft11, ft10
	-[0x80001bfc]:csrrs a7, fflags, zero
	-[0x80001c00]:sw t6, 112(a5)
Current Store : [0x80001c04] : sw a7, 116(a5) -- Store: [0x80007f24]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c10]:feq.d t6, ft11, ft10
	-[0x80001c14]:csrrs a7, fflags, zero
	-[0x80001c18]:sw t6, 128(a5)
Current Store : [0x80001c1c] : sw a7, 132(a5) -- Store: [0x80007f34]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c28]:feq.d t6, ft11, ft10
	-[0x80001c2c]:csrrs a7, fflags, zero
	-[0x80001c30]:sw t6, 144(a5)
Current Store : [0x80001c34] : sw a7, 148(a5) -- Store: [0x80007f44]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c40]:feq.d t6, ft11, ft10
	-[0x80001c44]:csrrs a7, fflags, zero
	-[0x80001c48]:sw t6, 160(a5)
Current Store : [0x80001c4c] : sw a7, 164(a5) -- Store: [0x80007f54]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c58]:feq.d t6, ft11, ft10
	-[0x80001c5c]:csrrs a7, fflags, zero
	-[0x80001c60]:sw t6, 176(a5)
Current Store : [0x80001c64] : sw a7, 180(a5) -- Store: [0x80007f64]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c70]:feq.d t6, ft11, ft10
	-[0x80001c74]:csrrs a7, fflags, zero
	-[0x80001c78]:sw t6, 192(a5)
Current Store : [0x80001c7c] : sw a7, 196(a5) -- Store: [0x80007f74]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001c88]:feq.d t6, ft11, ft10
	-[0x80001c8c]:csrrs a7, fflags, zero
	-[0x80001c90]:sw t6, 208(a5)
Current Store : [0x80001c94] : sw a7, 212(a5) -- Store: [0x80007f84]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ca0]:feq.d t6, ft11, ft10
	-[0x80001ca4]:csrrs a7, fflags, zero
	-[0x80001ca8]:sw t6, 224(a5)
Current Store : [0x80001cac] : sw a7, 228(a5) -- Store: [0x80007f94]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001cb8]:feq.d t6, ft11, ft10
	-[0x80001cbc]:csrrs a7, fflags, zero
	-[0x80001cc0]:sw t6, 240(a5)
Current Store : [0x80001cc4] : sw a7, 244(a5) -- Store: [0x80007fa4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001cd0]:feq.d t6, ft11, ft10
	-[0x80001cd4]:csrrs a7, fflags, zero
	-[0x80001cd8]:sw t6, 256(a5)
Current Store : [0x80001cdc] : sw a7, 260(a5) -- Store: [0x80007fb4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ce8]:feq.d t6, ft11, ft10
	-[0x80001cec]:csrrs a7, fflags, zero
	-[0x80001cf0]:sw t6, 272(a5)
Current Store : [0x80001cf4] : sw a7, 276(a5) -- Store: [0x80007fc4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d00]:feq.d t6, ft11, ft10
	-[0x80001d04]:csrrs a7, fflags, zero
	-[0x80001d08]:sw t6, 288(a5)
Current Store : [0x80001d0c] : sw a7, 292(a5) -- Store: [0x80007fd4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d18]:feq.d t6, ft11, ft10
	-[0x80001d1c]:csrrs a7, fflags, zero
	-[0x80001d20]:sw t6, 304(a5)
Current Store : [0x80001d24] : sw a7, 308(a5) -- Store: [0x80007fe4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d30]:feq.d t6, ft11, ft10
	-[0x80001d34]:csrrs a7, fflags, zero
	-[0x80001d38]:sw t6, 320(a5)
Current Store : [0x80001d3c] : sw a7, 324(a5) -- Store: [0x80007ff4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d48]:feq.d t6, ft11, ft10
	-[0x80001d4c]:csrrs a7, fflags, zero
	-[0x80001d50]:sw t6, 336(a5)
Current Store : [0x80001d54] : sw a7, 340(a5) -- Store: [0x80008004]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d60]:feq.d t6, ft11, ft10
	-[0x80001d64]:csrrs a7, fflags, zero
	-[0x80001d68]:sw t6, 352(a5)
Current Store : [0x80001d6c] : sw a7, 356(a5) -- Store: [0x80008014]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d78]:feq.d t6, ft11, ft10
	-[0x80001d7c]:csrrs a7, fflags, zero
	-[0x80001d80]:sw t6, 368(a5)
Current Store : [0x80001d84] : sw a7, 372(a5) -- Store: [0x80008024]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001d90]:feq.d t6, ft11, ft10
	-[0x80001d94]:csrrs a7, fflags, zero
	-[0x80001d98]:sw t6, 384(a5)
Current Store : [0x80001d9c] : sw a7, 388(a5) -- Store: [0x80008034]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001da8]:feq.d t6, ft11, ft10
	-[0x80001dac]:csrrs a7, fflags, zero
	-[0x80001db0]:sw t6, 400(a5)
Current Store : [0x80001db4] : sw a7, 404(a5) -- Store: [0x80008044]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001dc0]:feq.d t6, ft11, ft10
	-[0x80001dc4]:csrrs a7, fflags, zero
	-[0x80001dc8]:sw t6, 416(a5)
Current Store : [0x80001dcc] : sw a7, 420(a5) -- Store: [0x80008054]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001dd8]:feq.d t6, ft11, ft10
	-[0x80001ddc]:csrrs a7, fflags, zero
	-[0x80001de0]:sw t6, 432(a5)
Current Store : [0x80001de4] : sw a7, 436(a5) -- Store: [0x80008064]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001df0]:feq.d t6, ft11, ft10
	-[0x80001df4]:csrrs a7, fflags, zero
	-[0x80001df8]:sw t6, 448(a5)
Current Store : [0x80001dfc] : sw a7, 452(a5) -- Store: [0x80008074]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e08]:feq.d t6, ft11, ft10
	-[0x80001e0c]:csrrs a7, fflags, zero
	-[0x80001e10]:sw t6, 464(a5)
Current Store : [0x80001e14] : sw a7, 468(a5) -- Store: [0x80008084]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e20]:feq.d t6, ft11, ft10
	-[0x80001e24]:csrrs a7, fflags, zero
	-[0x80001e28]:sw t6, 480(a5)
Current Store : [0x80001e2c] : sw a7, 484(a5) -- Store: [0x80008094]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e38]:feq.d t6, ft11, ft10
	-[0x80001e3c]:csrrs a7, fflags, zero
	-[0x80001e40]:sw t6, 496(a5)
Current Store : [0x80001e44] : sw a7, 500(a5) -- Store: [0x800080a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e50]:feq.d t6, ft11, ft10
	-[0x80001e54]:csrrs a7, fflags, zero
	-[0x80001e58]:sw t6, 512(a5)
Current Store : [0x80001e5c] : sw a7, 516(a5) -- Store: [0x800080b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e68]:feq.d t6, ft11, ft10
	-[0x80001e6c]:csrrs a7, fflags, zero
	-[0x80001e70]:sw t6, 528(a5)
Current Store : [0x80001e74] : sw a7, 532(a5) -- Store: [0x800080c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e80]:feq.d t6, ft11, ft10
	-[0x80001e84]:csrrs a7, fflags, zero
	-[0x80001e88]:sw t6, 544(a5)
Current Store : [0x80001e8c] : sw a7, 548(a5) -- Store: [0x800080d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001e98]:feq.d t6, ft11, ft10
	-[0x80001e9c]:csrrs a7, fflags, zero
	-[0x80001ea0]:sw t6, 560(a5)
Current Store : [0x80001ea4] : sw a7, 564(a5) -- Store: [0x800080e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001eb0]:feq.d t6, ft11, ft10
	-[0x80001eb4]:csrrs a7, fflags, zero
	-[0x80001eb8]:sw t6, 576(a5)
Current Store : [0x80001ebc] : sw a7, 580(a5) -- Store: [0x800080f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ec8]:feq.d t6, ft11, ft10
	-[0x80001ecc]:csrrs a7, fflags, zero
	-[0x80001ed0]:sw t6, 592(a5)
Current Store : [0x80001ed4] : sw a7, 596(a5) -- Store: [0x80008104]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ee0]:feq.d t6, ft11, ft10
	-[0x80001ee4]:csrrs a7, fflags, zero
	-[0x80001ee8]:sw t6, 608(a5)
Current Store : [0x80001eec] : sw a7, 612(a5) -- Store: [0x80008114]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001ef8]:feq.d t6, ft11, ft10
	-[0x80001efc]:csrrs a7, fflags, zero
	-[0x80001f00]:sw t6, 624(a5)
Current Store : [0x80001f04] : sw a7, 628(a5) -- Store: [0x80008124]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f10]:feq.d t6, ft11, ft10
	-[0x80001f14]:csrrs a7, fflags, zero
	-[0x80001f18]:sw t6, 640(a5)
Current Store : [0x80001f1c] : sw a7, 644(a5) -- Store: [0x80008134]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f28]:feq.d t6, ft11, ft10
	-[0x80001f2c]:csrrs a7, fflags, zero
	-[0x80001f30]:sw t6, 656(a5)
Current Store : [0x80001f34] : sw a7, 660(a5) -- Store: [0x80008144]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f40]:feq.d t6, ft11, ft10
	-[0x80001f44]:csrrs a7, fflags, zero
	-[0x80001f48]:sw t6, 672(a5)
Current Store : [0x80001f4c] : sw a7, 676(a5) -- Store: [0x80008154]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f58]:feq.d t6, ft11, ft10
	-[0x80001f5c]:csrrs a7, fflags, zero
	-[0x80001f60]:sw t6, 688(a5)
Current Store : [0x80001f64] : sw a7, 692(a5) -- Store: [0x80008164]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f70]:feq.d t6, ft11, ft10
	-[0x80001f74]:csrrs a7, fflags, zero
	-[0x80001f78]:sw t6, 704(a5)
Current Store : [0x80001f7c] : sw a7, 708(a5) -- Store: [0x80008174]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001f88]:feq.d t6, ft11, ft10
	-[0x80001f8c]:csrrs a7, fflags, zero
	-[0x80001f90]:sw t6, 720(a5)
Current Store : [0x80001f94] : sw a7, 724(a5) -- Store: [0x80008184]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001fa0]:feq.d t6, ft11, ft10
	-[0x80001fa4]:csrrs a7, fflags, zero
	-[0x80001fa8]:sw t6, 736(a5)
Current Store : [0x80001fac] : sw a7, 740(a5) -- Store: [0x80008194]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001fb8]:feq.d t6, ft11, ft10
	-[0x80001fbc]:csrrs a7, fflags, zero
	-[0x80001fc0]:sw t6, 752(a5)
Current Store : [0x80001fc4] : sw a7, 756(a5) -- Store: [0x800081a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001fd0]:feq.d t6, ft11, ft10
	-[0x80001fd4]:csrrs a7, fflags, zero
	-[0x80001fd8]:sw t6, 768(a5)
Current Store : [0x80001fdc] : sw a7, 772(a5) -- Store: [0x800081b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80001fe8]:feq.d t6, ft11, ft10
	-[0x80001fec]:csrrs a7, fflags, zero
	-[0x80001ff0]:sw t6, 784(a5)
Current Store : [0x80001ff4] : sw a7, 788(a5) -- Store: [0x800081c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002000]:feq.d t6, ft11, ft10
	-[0x80002004]:csrrs a7, fflags, zero
	-[0x80002008]:sw t6, 800(a5)
Current Store : [0x8000200c] : sw a7, 804(a5) -- Store: [0x800081d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002018]:feq.d t6, ft11, ft10
	-[0x8000201c]:csrrs a7, fflags, zero
	-[0x80002020]:sw t6, 816(a5)
Current Store : [0x80002024] : sw a7, 820(a5) -- Store: [0x800081e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002030]:feq.d t6, ft11, ft10
	-[0x80002034]:csrrs a7, fflags, zero
	-[0x80002038]:sw t6, 832(a5)
Current Store : [0x8000203c] : sw a7, 836(a5) -- Store: [0x800081f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002048]:feq.d t6, ft11, ft10
	-[0x8000204c]:csrrs a7, fflags, zero
	-[0x80002050]:sw t6, 848(a5)
Current Store : [0x80002054] : sw a7, 852(a5) -- Store: [0x80008204]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002060]:feq.d t6, ft11, ft10
	-[0x80002064]:csrrs a7, fflags, zero
	-[0x80002068]:sw t6, 864(a5)
Current Store : [0x8000206c] : sw a7, 868(a5) -- Store: [0x80008214]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002078]:feq.d t6, ft11, ft10
	-[0x8000207c]:csrrs a7, fflags, zero
	-[0x80002080]:sw t6, 880(a5)
Current Store : [0x80002084] : sw a7, 884(a5) -- Store: [0x80008224]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002090]:feq.d t6, ft11, ft10
	-[0x80002094]:csrrs a7, fflags, zero
	-[0x80002098]:sw t6, 896(a5)
Current Store : [0x8000209c] : sw a7, 900(a5) -- Store: [0x80008234]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800020a8]:feq.d t6, ft11, ft10
	-[0x800020ac]:csrrs a7, fflags, zero
	-[0x800020b0]:sw t6, 912(a5)
Current Store : [0x800020b4] : sw a7, 916(a5) -- Store: [0x80008244]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800020c0]:feq.d t6, ft11, ft10
	-[0x800020c4]:csrrs a7, fflags, zero
	-[0x800020c8]:sw t6, 928(a5)
Current Store : [0x800020cc] : sw a7, 932(a5) -- Store: [0x80008254]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800020d8]:feq.d t6, ft11, ft10
	-[0x800020dc]:csrrs a7, fflags, zero
	-[0x800020e0]:sw t6, 944(a5)
Current Store : [0x800020e4] : sw a7, 948(a5) -- Store: [0x80008264]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800020f0]:feq.d t6, ft11, ft10
	-[0x800020f4]:csrrs a7, fflags, zero
	-[0x800020f8]:sw t6, 960(a5)
Current Store : [0x800020fc] : sw a7, 964(a5) -- Store: [0x80008274]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002108]:feq.d t6, ft11, ft10
	-[0x8000210c]:csrrs a7, fflags, zero
	-[0x80002110]:sw t6, 976(a5)
Current Store : [0x80002114] : sw a7, 980(a5) -- Store: [0x80008284]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002120]:feq.d t6, ft11, ft10
	-[0x80002124]:csrrs a7, fflags, zero
	-[0x80002128]:sw t6, 992(a5)
Current Store : [0x8000212c] : sw a7, 996(a5) -- Store: [0x80008294]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002138]:feq.d t6, ft11, ft10
	-[0x8000213c]:csrrs a7, fflags, zero
	-[0x80002140]:sw t6, 1008(a5)
Current Store : [0x80002144] : sw a7, 1012(a5) -- Store: [0x800082a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002150]:feq.d t6, ft11, ft10
	-[0x80002154]:csrrs a7, fflags, zero
	-[0x80002158]:sw t6, 1024(a5)
Current Store : [0x8000215c] : sw a7, 1028(a5) -- Store: [0x800082b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002168]:feq.d t6, ft11, ft10
	-[0x8000216c]:csrrs a7, fflags, zero
	-[0x80002170]:sw t6, 1040(a5)
Current Store : [0x80002174] : sw a7, 1044(a5) -- Store: [0x800082c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002180]:feq.d t6, ft11, ft10
	-[0x80002184]:csrrs a7, fflags, zero
	-[0x80002188]:sw t6, 1056(a5)
Current Store : [0x8000218c] : sw a7, 1060(a5) -- Store: [0x800082d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002198]:feq.d t6, ft11, ft10
	-[0x8000219c]:csrrs a7, fflags, zero
	-[0x800021a0]:sw t6, 1072(a5)
Current Store : [0x800021a4] : sw a7, 1076(a5) -- Store: [0x800082e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800021b0]:feq.d t6, ft11, ft10
	-[0x800021b4]:csrrs a7, fflags, zero
	-[0x800021b8]:sw t6, 1088(a5)
Current Store : [0x800021bc] : sw a7, 1092(a5) -- Store: [0x800082f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800021c8]:feq.d t6, ft11, ft10
	-[0x800021cc]:csrrs a7, fflags, zero
	-[0x800021d0]:sw t6, 1104(a5)
Current Store : [0x800021d4] : sw a7, 1108(a5) -- Store: [0x80008304]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800021e0]:feq.d t6, ft11, ft10
	-[0x800021e4]:csrrs a7, fflags, zero
	-[0x800021e8]:sw t6, 1120(a5)
Current Store : [0x800021ec] : sw a7, 1124(a5) -- Store: [0x80008314]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800021f8]:feq.d t6, ft11, ft10
	-[0x800021fc]:csrrs a7, fflags, zero
	-[0x80002200]:sw t6, 1136(a5)
Current Store : [0x80002204] : sw a7, 1140(a5) -- Store: [0x80008324]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002210]:feq.d t6, ft11, ft10
	-[0x80002214]:csrrs a7, fflags, zero
	-[0x80002218]:sw t6, 1152(a5)
Current Store : [0x8000221c] : sw a7, 1156(a5) -- Store: [0x80008334]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002228]:feq.d t6, ft11, ft10
	-[0x8000222c]:csrrs a7, fflags, zero
	-[0x80002230]:sw t6, 1168(a5)
Current Store : [0x80002234] : sw a7, 1172(a5) -- Store: [0x80008344]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002240]:feq.d t6, ft11, ft10
	-[0x80002244]:csrrs a7, fflags, zero
	-[0x80002248]:sw t6, 1184(a5)
Current Store : [0x8000224c] : sw a7, 1188(a5) -- Store: [0x80008354]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002258]:feq.d t6, ft11, ft10
	-[0x8000225c]:csrrs a7, fflags, zero
	-[0x80002260]:sw t6, 1200(a5)
Current Store : [0x80002264] : sw a7, 1204(a5) -- Store: [0x80008364]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002270]:feq.d t6, ft11, ft10
	-[0x80002274]:csrrs a7, fflags, zero
	-[0x80002278]:sw t6, 1216(a5)
Current Store : [0x8000227c] : sw a7, 1220(a5) -- Store: [0x80008374]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002288]:feq.d t6, ft11, ft10
	-[0x8000228c]:csrrs a7, fflags, zero
	-[0x80002290]:sw t6, 1232(a5)
Current Store : [0x80002294] : sw a7, 1236(a5) -- Store: [0x80008384]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800022a0]:feq.d t6, ft11, ft10
	-[0x800022a4]:csrrs a7, fflags, zero
	-[0x800022a8]:sw t6, 1248(a5)
Current Store : [0x800022ac] : sw a7, 1252(a5) -- Store: [0x80008394]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800022b8]:feq.d t6, ft11, ft10
	-[0x800022bc]:csrrs a7, fflags, zero
	-[0x800022c0]:sw t6, 1264(a5)
Current Store : [0x800022c4] : sw a7, 1268(a5) -- Store: [0x800083a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800022d0]:feq.d t6, ft11, ft10
	-[0x800022d4]:csrrs a7, fflags, zero
	-[0x800022d8]:sw t6, 1280(a5)
Current Store : [0x800022dc] : sw a7, 1284(a5) -- Store: [0x800083b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800022e8]:feq.d t6, ft11, ft10
	-[0x800022ec]:csrrs a7, fflags, zero
	-[0x800022f0]:sw t6, 1296(a5)
Current Store : [0x800022f4] : sw a7, 1300(a5) -- Store: [0x800083c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002300]:feq.d t6, ft11, ft10
	-[0x80002304]:csrrs a7, fflags, zero
	-[0x80002308]:sw t6, 1312(a5)
Current Store : [0x8000230c] : sw a7, 1316(a5) -- Store: [0x800083d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002318]:feq.d t6, ft11, ft10
	-[0x8000231c]:csrrs a7, fflags, zero
	-[0x80002320]:sw t6, 1328(a5)
Current Store : [0x80002324] : sw a7, 1332(a5) -- Store: [0x800083e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002330]:feq.d t6, ft11, ft10
	-[0x80002334]:csrrs a7, fflags, zero
	-[0x80002338]:sw t6, 1344(a5)
Current Store : [0x8000233c] : sw a7, 1348(a5) -- Store: [0x800083f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002348]:feq.d t6, ft11, ft10
	-[0x8000234c]:csrrs a7, fflags, zero
	-[0x80002350]:sw t6, 1360(a5)
Current Store : [0x80002354] : sw a7, 1364(a5) -- Store: [0x80008404]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002360]:feq.d t6, ft11, ft10
	-[0x80002364]:csrrs a7, fflags, zero
	-[0x80002368]:sw t6, 1376(a5)
Current Store : [0x8000236c] : sw a7, 1380(a5) -- Store: [0x80008414]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002378]:feq.d t6, ft11, ft10
	-[0x8000237c]:csrrs a7, fflags, zero
	-[0x80002380]:sw t6, 1392(a5)
Current Store : [0x80002384] : sw a7, 1396(a5) -- Store: [0x80008424]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002390]:feq.d t6, ft11, ft10
	-[0x80002394]:csrrs a7, fflags, zero
	-[0x80002398]:sw t6, 1408(a5)
Current Store : [0x8000239c] : sw a7, 1412(a5) -- Store: [0x80008434]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800023a8]:feq.d t6, ft11, ft10
	-[0x800023ac]:csrrs a7, fflags, zero
	-[0x800023b0]:sw t6, 1424(a5)
Current Store : [0x800023b4] : sw a7, 1428(a5) -- Store: [0x80008444]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800023c0]:feq.d t6, ft11, ft10
	-[0x800023c4]:csrrs a7, fflags, zero
	-[0x800023c8]:sw t6, 1440(a5)
Current Store : [0x800023cc] : sw a7, 1444(a5) -- Store: [0x80008454]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800023d8]:feq.d t6, ft11, ft10
	-[0x800023dc]:csrrs a7, fflags, zero
	-[0x800023e0]:sw t6, 1456(a5)
Current Store : [0x800023e4] : sw a7, 1460(a5) -- Store: [0x80008464]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800023f0]:feq.d t6, ft11, ft10
	-[0x800023f4]:csrrs a7, fflags, zero
	-[0x800023f8]:sw t6, 1472(a5)
Current Store : [0x800023fc] : sw a7, 1476(a5) -- Store: [0x80008474]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002408]:feq.d t6, ft11, ft10
	-[0x8000240c]:csrrs a7, fflags, zero
	-[0x80002410]:sw t6, 1488(a5)
Current Store : [0x80002414] : sw a7, 1492(a5) -- Store: [0x80008484]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002420]:feq.d t6, ft11, ft10
	-[0x80002424]:csrrs a7, fflags, zero
	-[0x80002428]:sw t6, 1504(a5)
Current Store : [0x8000242c] : sw a7, 1508(a5) -- Store: [0x80008494]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002438]:feq.d t6, ft11, ft10
	-[0x8000243c]:csrrs a7, fflags, zero
	-[0x80002440]:sw t6, 1520(a5)
Current Store : [0x80002444] : sw a7, 1524(a5) -- Store: [0x800084a4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002450]:feq.d t6, ft11, ft10
	-[0x80002454]:csrrs a7, fflags, zero
	-[0x80002458]:sw t6, 1536(a5)
Current Store : [0x8000245c] : sw a7, 1540(a5) -- Store: [0x800084b4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002468]:feq.d t6, ft11, ft10
	-[0x8000246c]:csrrs a7, fflags, zero
	-[0x80002470]:sw t6, 1552(a5)
Current Store : [0x80002474] : sw a7, 1556(a5) -- Store: [0x800084c4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002480]:feq.d t6, ft11, ft10
	-[0x80002484]:csrrs a7, fflags, zero
	-[0x80002488]:sw t6, 1568(a5)
Current Store : [0x8000248c] : sw a7, 1572(a5) -- Store: [0x800084d4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002498]:feq.d t6, ft11, ft10
	-[0x8000249c]:csrrs a7, fflags, zero
	-[0x800024a0]:sw t6, 1584(a5)
Current Store : [0x800024a4] : sw a7, 1588(a5) -- Store: [0x800084e4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800024b0]:feq.d t6, ft11, ft10
	-[0x800024b4]:csrrs a7, fflags, zero
	-[0x800024b8]:sw t6, 1600(a5)
Current Store : [0x800024bc] : sw a7, 1604(a5) -- Store: [0x800084f4]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800024c8]:feq.d t6, ft11, ft10
	-[0x800024cc]:csrrs a7, fflags, zero
	-[0x800024d0]:sw t6, 1616(a5)
Current Store : [0x800024d4] : sw a7, 1620(a5) -- Store: [0x80008504]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800024e0]:feq.d t6, ft11, ft10
	-[0x800024e4]:csrrs a7, fflags, zero
	-[0x800024e8]:sw t6, 1632(a5)
Current Store : [0x800024ec] : sw a7, 1636(a5) -- Store: [0x80008514]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800024f8]:feq.d t6, ft11, ft10
	-[0x800024fc]:csrrs a7, fflags, zero
	-[0x80002500]:sw t6, 1648(a5)
Current Store : [0x80002504] : sw a7, 1652(a5) -- Store: [0x80008524]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002510]:feq.d t6, ft11, ft10
	-[0x80002514]:csrrs a7, fflags, zero
	-[0x80002518]:sw t6, 1664(a5)
Current Store : [0x8000251c] : sw a7, 1668(a5) -- Store: [0x80008534]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000252c]:feq.d t6, ft11, ft10
	-[0x80002530]:csrrs a7, fflags, zero
	-[0x80002534]:sw t6, 1680(a5)
Current Store : [0x80002538] : sw a7, 1684(a5) -- Store: [0x80008544]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002544]:feq.d t6, ft11, ft10
	-[0x80002548]:csrrs a7, fflags, zero
	-[0x8000254c]:sw t6, 1696(a5)
Current Store : [0x80002550] : sw a7, 1700(a5) -- Store: [0x80008554]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000255c]:feq.d t6, ft11, ft10
	-[0x80002560]:csrrs a7, fflags, zero
	-[0x80002564]:sw t6, 1712(a5)
Current Store : [0x80002568] : sw a7, 1716(a5) -- Store: [0x80008564]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002574]:feq.d t6, ft11, ft10
	-[0x80002578]:csrrs a7, fflags, zero
	-[0x8000257c]:sw t6, 1728(a5)
Current Store : [0x80002580] : sw a7, 1732(a5) -- Store: [0x80008574]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000258c]:feq.d t6, ft11, ft10
	-[0x80002590]:csrrs a7, fflags, zero
	-[0x80002594]:sw t6, 1744(a5)
Current Store : [0x80002598] : sw a7, 1748(a5) -- Store: [0x80008584]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800025a4]:feq.d t6, ft11, ft10
	-[0x800025a8]:csrrs a7, fflags, zero
	-[0x800025ac]:sw t6, 1760(a5)
Current Store : [0x800025b0] : sw a7, 1764(a5) -- Store: [0x80008594]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800025bc]:feq.d t6, ft11, ft10
	-[0x800025c0]:csrrs a7, fflags, zero
	-[0x800025c4]:sw t6, 1776(a5)
Current Store : [0x800025c8] : sw a7, 1780(a5) -- Store: [0x800085a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800025d4]:feq.d t6, ft11, ft10
	-[0x800025d8]:csrrs a7, fflags, zero
	-[0x800025dc]:sw t6, 1792(a5)
Current Store : [0x800025e0] : sw a7, 1796(a5) -- Store: [0x800085b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800025ec]:feq.d t6, ft11, ft10
	-[0x800025f0]:csrrs a7, fflags, zero
	-[0x800025f4]:sw t6, 1808(a5)
Current Store : [0x800025f8] : sw a7, 1812(a5) -- Store: [0x800085c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002604]:feq.d t6, ft11, ft10
	-[0x80002608]:csrrs a7, fflags, zero
	-[0x8000260c]:sw t6, 1824(a5)
Current Store : [0x80002610] : sw a7, 1828(a5) -- Store: [0x800085d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000261c]:feq.d t6, ft11, ft10
	-[0x80002620]:csrrs a7, fflags, zero
	-[0x80002624]:sw t6, 1840(a5)
Current Store : [0x80002628] : sw a7, 1844(a5) -- Store: [0x800085e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002634]:feq.d t6, ft11, ft10
	-[0x80002638]:csrrs a7, fflags, zero
	-[0x8000263c]:sw t6, 1856(a5)
Current Store : [0x80002640] : sw a7, 1860(a5) -- Store: [0x800085f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000264c]:feq.d t6, ft11, ft10
	-[0x80002650]:csrrs a7, fflags, zero
	-[0x80002654]:sw t6, 1872(a5)
Current Store : [0x80002658] : sw a7, 1876(a5) -- Store: [0x80008604]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002664]:feq.d t6, ft11, ft10
	-[0x80002668]:csrrs a7, fflags, zero
	-[0x8000266c]:sw t6, 1888(a5)
Current Store : [0x80002670] : sw a7, 1892(a5) -- Store: [0x80008614]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000267c]:feq.d t6, ft11, ft10
	-[0x80002680]:csrrs a7, fflags, zero
	-[0x80002684]:sw t6, 1904(a5)
Current Store : [0x80002688] : sw a7, 1908(a5) -- Store: [0x80008624]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002694]:feq.d t6, ft11, ft10
	-[0x80002698]:csrrs a7, fflags, zero
	-[0x8000269c]:sw t6, 1920(a5)
Current Store : [0x800026a0] : sw a7, 1924(a5) -- Store: [0x80008634]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800026ac]:feq.d t6, ft11, ft10
	-[0x800026b0]:csrrs a7, fflags, zero
	-[0x800026b4]:sw t6, 1936(a5)
Current Store : [0x800026b8] : sw a7, 1940(a5) -- Store: [0x80008644]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800026c4]:feq.d t6, ft11, ft10
	-[0x800026c8]:csrrs a7, fflags, zero
	-[0x800026cc]:sw t6, 1952(a5)
Current Store : [0x800026d0] : sw a7, 1956(a5) -- Store: [0x80008654]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800026dc]:feq.d t6, ft11, ft10
	-[0x800026e0]:csrrs a7, fflags, zero
	-[0x800026e4]:sw t6, 1968(a5)
Current Store : [0x800026e8] : sw a7, 1972(a5) -- Store: [0x80008664]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800026f4]:feq.d t6, ft11, ft10
	-[0x800026f8]:csrrs a7, fflags, zero
	-[0x800026fc]:sw t6, 1984(a5)
Current Store : [0x80002700] : sw a7, 1988(a5) -- Store: [0x80008674]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000270c]:feq.d t6, ft11, ft10
	-[0x80002710]:csrrs a7, fflags, zero
	-[0x80002714]:sw t6, 2000(a5)
Current Store : [0x80002718] : sw a7, 2004(a5) -- Store: [0x80008684]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002724]:feq.d t6, ft11, ft10
	-[0x80002728]:csrrs a7, fflags, zero
	-[0x8000272c]:sw t6, 2016(a5)
Current Store : [0x80002730] : sw a7, 2020(a5) -- Store: [0x80008694]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002744]:feq.d t6, ft11, ft10
	-[0x80002748]:csrrs a7, fflags, zero
	-[0x8000274c]:sw t6, 0(a5)
Current Store : [0x80002750] : sw a7, 4(a5) -- Store: [0x800082ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000275c]:feq.d t6, ft11, ft10
	-[0x80002760]:csrrs a7, fflags, zero
	-[0x80002764]:sw t6, 16(a5)
Current Store : [0x80002768] : sw a7, 20(a5) -- Store: [0x800082bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002774]:feq.d t6, ft11, ft10
	-[0x80002778]:csrrs a7, fflags, zero
	-[0x8000277c]:sw t6, 32(a5)
Current Store : [0x80002780] : sw a7, 36(a5) -- Store: [0x800082cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000278c]:feq.d t6, ft11, ft10
	-[0x80002790]:csrrs a7, fflags, zero
	-[0x80002794]:sw t6, 48(a5)
Current Store : [0x80002798] : sw a7, 52(a5) -- Store: [0x800082dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800027a4]:feq.d t6, ft11, ft10
	-[0x800027a8]:csrrs a7, fflags, zero
	-[0x800027ac]:sw t6, 64(a5)
Current Store : [0x800027b0] : sw a7, 68(a5) -- Store: [0x800082ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800027bc]:feq.d t6, ft11, ft10
	-[0x800027c0]:csrrs a7, fflags, zero
	-[0x800027c4]:sw t6, 80(a5)
Current Store : [0x800027c8] : sw a7, 84(a5) -- Store: [0x800082fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800027d4]:feq.d t6, ft11, ft10
	-[0x800027d8]:csrrs a7, fflags, zero
	-[0x800027dc]:sw t6, 96(a5)
Current Store : [0x800027e0] : sw a7, 100(a5) -- Store: [0x8000830c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800027ec]:feq.d t6, ft11, ft10
	-[0x800027f0]:csrrs a7, fflags, zero
	-[0x800027f4]:sw t6, 112(a5)
Current Store : [0x800027f8] : sw a7, 116(a5) -- Store: [0x8000831c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002804]:feq.d t6, ft11, ft10
	-[0x80002808]:csrrs a7, fflags, zero
	-[0x8000280c]:sw t6, 128(a5)
Current Store : [0x80002810] : sw a7, 132(a5) -- Store: [0x8000832c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000281c]:feq.d t6, ft11, ft10
	-[0x80002820]:csrrs a7, fflags, zero
	-[0x80002824]:sw t6, 144(a5)
Current Store : [0x80002828] : sw a7, 148(a5) -- Store: [0x8000833c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002834]:feq.d t6, ft11, ft10
	-[0x80002838]:csrrs a7, fflags, zero
	-[0x8000283c]:sw t6, 160(a5)
Current Store : [0x80002840] : sw a7, 164(a5) -- Store: [0x8000834c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000284c]:feq.d t6, ft11, ft10
	-[0x80002850]:csrrs a7, fflags, zero
	-[0x80002854]:sw t6, 176(a5)
Current Store : [0x80002858] : sw a7, 180(a5) -- Store: [0x8000835c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002864]:feq.d t6, ft11, ft10
	-[0x80002868]:csrrs a7, fflags, zero
	-[0x8000286c]:sw t6, 192(a5)
Current Store : [0x80002870] : sw a7, 196(a5) -- Store: [0x8000836c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000287c]:feq.d t6, ft11, ft10
	-[0x80002880]:csrrs a7, fflags, zero
	-[0x80002884]:sw t6, 208(a5)
Current Store : [0x80002888] : sw a7, 212(a5) -- Store: [0x8000837c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002894]:feq.d t6, ft11, ft10
	-[0x80002898]:csrrs a7, fflags, zero
	-[0x8000289c]:sw t6, 224(a5)
Current Store : [0x800028a0] : sw a7, 228(a5) -- Store: [0x8000838c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800028ac]:feq.d t6, ft11, ft10
	-[0x800028b0]:csrrs a7, fflags, zero
	-[0x800028b4]:sw t6, 240(a5)
Current Store : [0x800028b8] : sw a7, 244(a5) -- Store: [0x8000839c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800028c4]:feq.d t6, ft11, ft10
	-[0x800028c8]:csrrs a7, fflags, zero
	-[0x800028cc]:sw t6, 256(a5)
Current Store : [0x800028d0] : sw a7, 260(a5) -- Store: [0x800083ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800028dc]:feq.d t6, ft11, ft10
	-[0x800028e0]:csrrs a7, fflags, zero
	-[0x800028e4]:sw t6, 272(a5)
Current Store : [0x800028e8] : sw a7, 276(a5) -- Store: [0x800083bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800028f4]:feq.d t6, ft11, ft10
	-[0x800028f8]:csrrs a7, fflags, zero
	-[0x800028fc]:sw t6, 288(a5)
Current Store : [0x80002900] : sw a7, 292(a5) -- Store: [0x800083cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000290c]:feq.d t6, ft11, ft10
	-[0x80002910]:csrrs a7, fflags, zero
	-[0x80002914]:sw t6, 304(a5)
Current Store : [0x80002918] : sw a7, 308(a5) -- Store: [0x800083dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002924]:feq.d t6, ft11, ft10
	-[0x80002928]:csrrs a7, fflags, zero
	-[0x8000292c]:sw t6, 320(a5)
Current Store : [0x80002930] : sw a7, 324(a5) -- Store: [0x800083ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000293c]:feq.d t6, ft11, ft10
	-[0x80002940]:csrrs a7, fflags, zero
	-[0x80002944]:sw t6, 336(a5)
Current Store : [0x80002948] : sw a7, 340(a5) -- Store: [0x800083fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002954]:feq.d t6, ft11, ft10
	-[0x80002958]:csrrs a7, fflags, zero
	-[0x8000295c]:sw t6, 352(a5)
Current Store : [0x80002960] : sw a7, 356(a5) -- Store: [0x8000840c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000296c]:feq.d t6, ft11, ft10
	-[0x80002970]:csrrs a7, fflags, zero
	-[0x80002974]:sw t6, 368(a5)
Current Store : [0x80002978] : sw a7, 372(a5) -- Store: [0x8000841c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002984]:feq.d t6, ft11, ft10
	-[0x80002988]:csrrs a7, fflags, zero
	-[0x8000298c]:sw t6, 384(a5)
Current Store : [0x80002990] : sw a7, 388(a5) -- Store: [0x8000842c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000299c]:feq.d t6, ft11, ft10
	-[0x800029a0]:csrrs a7, fflags, zero
	-[0x800029a4]:sw t6, 400(a5)
Current Store : [0x800029a8] : sw a7, 404(a5) -- Store: [0x8000843c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800029b4]:feq.d t6, ft11, ft10
	-[0x800029b8]:csrrs a7, fflags, zero
	-[0x800029bc]:sw t6, 416(a5)
Current Store : [0x800029c0] : sw a7, 420(a5) -- Store: [0x8000844c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800029cc]:feq.d t6, ft11, ft10
	-[0x800029d0]:csrrs a7, fflags, zero
	-[0x800029d4]:sw t6, 432(a5)
Current Store : [0x800029d8] : sw a7, 436(a5) -- Store: [0x8000845c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800029e4]:feq.d t6, ft11, ft10
	-[0x800029e8]:csrrs a7, fflags, zero
	-[0x800029ec]:sw t6, 448(a5)
Current Store : [0x800029f0] : sw a7, 452(a5) -- Store: [0x8000846c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800029fc]:feq.d t6, ft11, ft10
	-[0x80002a00]:csrrs a7, fflags, zero
	-[0x80002a04]:sw t6, 464(a5)
Current Store : [0x80002a08] : sw a7, 468(a5) -- Store: [0x8000847c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a14]:feq.d t6, ft11, ft10
	-[0x80002a18]:csrrs a7, fflags, zero
	-[0x80002a1c]:sw t6, 480(a5)
Current Store : [0x80002a20] : sw a7, 484(a5) -- Store: [0x8000848c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a2c]:feq.d t6, ft11, ft10
	-[0x80002a30]:csrrs a7, fflags, zero
	-[0x80002a34]:sw t6, 496(a5)
Current Store : [0x80002a38] : sw a7, 500(a5) -- Store: [0x8000849c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a44]:feq.d t6, ft11, ft10
	-[0x80002a48]:csrrs a7, fflags, zero
	-[0x80002a4c]:sw t6, 512(a5)
Current Store : [0x80002a50] : sw a7, 516(a5) -- Store: [0x800084ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a5c]:feq.d t6, ft11, ft10
	-[0x80002a60]:csrrs a7, fflags, zero
	-[0x80002a64]:sw t6, 528(a5)
Current Store : [0x80002a68] : sw a7, 532(a5) -- Store: [0x800084bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a74]:feq.d t6, ft11, ft10
	-[0x80002a78]:csrrs a7, fflags, zero
	-[0x80002a7c]:sw t6, 544(a5)
Current Store : [0x80002a80] : sw a7, 548(a5) -- Store: [0x800084cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002a8c]:feq.d t6, ft11, ft10
	-[0x80002a90]:csrrs a7, fflags, zero
	-[0x80002a94]:sw t6, 560(a5)
Current Store : [0x80002a98] : sw a7, 564(a5) -- Store: [0x800084dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:feq.d t6, ft11, ft10
	-[0x80002aa8]:csrrs a7, fflags, zero
	-[0x80002aac]:sw t6, 576(a5)
Current Store : [0x80002ab0] : sw a7, 580(a5) -- Store: [0x800084ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002abc]:feq.d t6, ft11, ft10
	-[0x80002ac0]:csrrs a7, fflags, zero
	-[0x80002ac4]:sw t6, 592(a5)
Current Store : [0x80002ac8] : sw a7, 596(a5) -- Store: [0x800084fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002ad4]:feq.d t6, ft11, ft10
	-[0x80002ad8]:csrrs a7, fflags, zero
	-[0x80002adc]:sw t6, 608(a5)
Current Store : [0x80002ae0] : sw a7, 612(a5) -- Store: [0x8000850c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002aec]:feq.d t6, ft11, ft10
	-[0x80002af0]:csrrs a7, fflags, zero
	-[0x80002af4]:sw t6, 624(a5)
Current Store : [0x80002af8] : sw a7, 628(a5) -- Store: [0x8000851c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b04]:feq.d t6, ft11, ft10
	-[0x80002b08]:csrrs a7, fflags, zero
	-[0x80002b0c]:sw t6, 640(a5)
Current Store : [0x80002b10] : sw a7, 644(a5) -- Store: [0x8000852c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b1c]:feq.d t6, ft11, ft10
	-[0x80002b20]:csrrs a7, fflags, zero
	-[0x80002b24]:sw t6, 656(a5)
Current Store : [0x80002b28] : sw a7, 660(a5) -- Store: [0x8000853c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b34]:feq.d t6, ft11, ft10
	-[0x80002b38]:csrrs a7, fflags, zero
	-[0x80002b3c]:sw t6, 672(a5)
Current Store : [0x80002b40] : sw a7, 676(a5) -- Store: [0x8000854c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b4c]:feq.d t6, ft11, ft10
	-[0x80002b50]:csrrs a7, fflags, zero
	-[0x80002b54]:sw t6, 688(a5)
Current Store : [0x80002b58] : sw a7, 692(a5) -- Store: [0x8000855c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b64]:feq.d t6, ft11, ft10
	-[0x80002b68]:csrrs a7, fflags, zero
	-[0x80002b6c]:sw t6, 704(a5)
Current Store : [0x80002b70] : sw a7, 708(a5) -- Store: [0x8000856c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b7c]:feq.d t6, ft11, ft10
	-[0x80002b80]:csrrs a7, fflags, zero
	-[0x80002b84]:sw t6, 720(a5)
Current Store : [0x80002b88] : sw a7, 724(a5) -- Store: [0x8000857c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002b94]:feq.d t6, ft11, ft10
	-[0x80002b98]:csrrs a7, fflags, zero
	-[0x80002b9c]:sw t6, 736(a5)
Current Store : [0x80002ba0] : sw a7, 740(a5) -- Store: [0x8000858c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002bac]:feq.d t6, ft11, ft10
	-[0x80002bb0]:csrrs a7, fflags, zero
	-[0x80002bb4]:sw t6, 752(a5)
Current Store : [0x80002bb8] : sw a7, 756(a5) -- Store: [0x8000859c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002bc4]:feq.d t6, ft11, ft10
	-[0x80002bc8]:csrrs a7, fflags, zero
	-[0x80002bcc]:sw t6, 768(a5)
Current Store : [0x80002bd0] : sw a7, 772(a5) -- Store: [0x800085ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002bdc]:feq.d t6, ft11, ft10
	-[0x80002be0]:csrrs a7, fflags, zero
	-[0x80002be4]:sw t6, 784(a5)
Current Store : [0x80002be8] : sw a7, 788(a5) -- Store: [0x800085bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002bf4]:feq.d t6, ft11, ft10
	-[0x80002bf8]:csrrs a7, fflags, zero
	-[0x80002bfc]:sw t6, 800(a5)
Current Store : [0x80002c00] : sw a7, 804(a5) -- Store: [0x800085cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c0c]:feq.d t6, ft11, ft10
	-[0x80002c10]:csrrs a7, fflags, zero
	-[0x80002c14]:sw t6, 816(a5)
Current Store : [0x80002c18] : sw a7, 820(a5) -- Store: [0x800085dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c24]:feq.d t6, ft11, ft10
	-[0x80002c28]:csrrs a7, fflags, zero
	-[0x80002c2c]:sw t6, 832(a5)
Current Store : [0x80002c30] : sw a7, 836(a5) -- Store: [0x800085ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c3c]:feq.d t6, ft11, ft10
	-[0x80002c40]:csrrs a7, fflags, zero
	-[0x80002c44]:sw t6, 848(a5)
Current Store : [0x80002c48] : sw a7, 852(a5) -- Store: [0x800085fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c54]:feq.d t6, ft11, ft10
	-[0x80002c58]:csrrs a7, fflags, zero
	-[0x80002c5c]:sw t6, 864(a5)
Current Store : [0x80002c60] : sw a7, 868(a5) -- Store: [0x8000860c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c6c]:feq.d t6, ft11, ft10
	-[0x80002c70]:csrrs a7, fflags, zero
	-[0x80002c74]:sw t6, 880(a5)
Current Store : [0x80002c78] : sw a7, 884(a5) -- Store: [0x8000861c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c84]:feq.d t6, ft11, ft10
	-[0x80002c88]:csrrs a7, fflags, zero
	-[0x80002c8c]:sw t6, 896(a5)
Current Store : [0x80002c90] : sw a7, 900(a5) -- Store: [0x8000862c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002c9c]:feq.d t6, ft11, ft10
	-[0x80002ca0]:csrrs a7, fflags, zero
	-[0x80002ca4]:sw t6, 912(a5)
Current Store : [0x80002ca8] : sw a7, 916(a5) -- Store: [0x8000863c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:feq.d t6, ft11, ft10
	-[0x80002cb8]:csrrs a7, fflags, zero
	-[0x80002cbc]:sw t6, 928(a5)
Current Store : [0x80002cc0] : sw a7, 932(a5) -- Store: [0x8000864c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002ccc]:feq.d t6, ft11, ft10
	-[0x80002cd0]:csrrs a7, fflags, zero
	-[0x80002cd4]:sw t6, 944(a5)
Current Store : [0x80002cd8] : sw a7, 948(a5) -- Store: [0x8000865c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002ce4]:feq.d t6, ft11, ft10
	-[0x80002ce8]:csrrs a7, fflags, zero
	-[0x80002cec]:sw t6, 960(a5)
Current Store : [0x80002cf0] : sw a7, 964(a5) -- Store: [0x8000866c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002cfc]:feq.d t6, ft11, ft10
	-[0x80002d00]:csrrs a7, fflags, zero
	-[0x80002d04]:sw t6, 976(a5)
Current Store : [0x80002d08] : sw a7, 980(a5) -- Store: [0x8000867c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d14]:feq.d t6, ft11, ft10
	-[0x80002d18]:csrrs a7, fflags, zero
	-[0x80002d1c]:sw t6, 992(a5)
Current Store : [0x80002d20] : sw a7, 996(a5) -- Store: [0x8000868c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d2c]:feq.d t6, ft11, ft10
	-[0x80002d30]:csrrs a7, fflags, zero
	-[0x80002d34]:sw t6, 1008(a5)
Current Store : [0x80002d38] : sw a7, 1012(a5) -- Store: [0x8000869c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d44]:feq.d t6, ft11, ft10
	-[0x80002d48]:csrrs a7, fflags, zero
	-[0x80002d4c]:sw t6, 1024(a5)
Current Store : [0x80002d50] : sw a7, 1028(a5) -- Store: [0x800086ac]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d5c]:feq.d t6, ft11, ft10
	-[0x80002d60]:csrrs a7, fflags, zero
	-[0x80002d64]:sw t6, 1040(a5)
Current Store : [0x80002d68] : sw a7, 1044(a5) -- Store: [0x800086bc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d74]:feq.d t6, ft11, ft10
	-[0x80002d78]:csrrs a7, fflags, zero
	-[0x80002d7c]:sw t6, 1056(a5)
Current Store : [0x80002d80] : sw a7, 1060(a5) -- Store: [0x800086cc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002d8c]:feq.d t6, ft11, ft10
	-[0x80002d90]:csrrs a7, fflags, zero
	-[0x80002d94]:sw t6, 1072(a5)
Current Store : [0x80002d98] : sw a7, 1076(a5) -- Store: [0x800086dc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002da4]:feq.d t6, ft11, ft10
	-[0x80002da8]:csrrs a7, fflags, zero
	-[0x80002dac]:sw t6, 1088(a5)
Current Store : [0x80002db0] : sw a7, 1092(a5) -- Store: [0x800086ec]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002dbc]:feq.d t6, ft11, ft10
	-[0x80002dc0]:csrrs a7, fflags, zero
	-[0x80002dc4]:sw t6, 1104(a5)
Current Store : [0x80002dc8] : sw a7, 1108(a5) -- Store: [0x800086fc]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002dd4]:feq.d t6, ft11, ft10
	-[0x80002dd8]:csrrs a7, fflags, zero
	-[0x80002ddc]:sw t6, 1120(a5)
Current Store : [0x80002de0] : sw a7, 1124(a5) -- Store: [0x8000870c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002dec]:feq.d t6, ft11, ft10
	-[0x80002df0]:csrrs a7, fflags, zero
	-[0x80002df4]:sw t6, 1136(a5)
Current Store : [0x80002df8] : sw a7, 1140(a5) -- Store: [0x8000871c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e04]:feq.d t6, ft11, ft10
	-[0x80002e08]:csrrs a7, fflags, zero
	-[0x80002e0c]:sw t6, 1152(a5)
Current Store : [0x80002e10] : sw a7, 1156(a5) -- Store: [0x8000872c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e1c]:feq.d t6, ft11, ft10
	-[0x80002e20]:csrrs a7, fflags, zero
	-[0x80002e24]:sw t6, 1168(a5)
Current Store : [0x80002e28] : sw a7, 1172(a5) -- Store: [0x8000873c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e34]:feq.d t6, ft11, ft10
	-[0x80002e38]:csrrs a7, fflags, zero
	-[0x80002e3c]:sw t6, 1184(a5)
Current Store : [0x80002e40] : sw a7, 1188(a5) -- Store: [0x8000874c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e4c]:feq.d t6, ft11, ft10
	-[0x80002e50]:csrrs a7, fflags, zero
	-[0x80002e54]:sw t6, 1200(a5)
Current Store : [0x80002e58] : sw a7, 1204(a5) -- Store: [0x8000875c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e64]:feq.d t6, ft11, ft10
	-[0x80002e68]:csrrs a7, fflags, zero
	-[0x80002e6c]:sw t6, 1216(a5)
Current Store : [0x80002e70] : sw a7, 1220(a5) -- Store: [0x8000876c]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e7c]:feq.d t6, ft11, ft10
	-[0x80002e80]:csrrs a7, fflags, zero
	-[0x80002e84]:sw t6, 1232(a5)
Current Store : [0x80002e88] : sw a7, 1236(a5) -- Store: [0x8000877c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002e94]:feq.d t6, ft11, ft10
	-[0x80002e98]:csrrs a7, fflags, zero
	-[0x80002e9c]:sw t6, 1248(a5)
Current Store : [0x80002ea0] : sw a7, 1252(a5) -- Store: [0x8000878c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002eac]:feq.d t6, ft11, ft10
	-[0x80002eb0]:csrrs a7, fflags, zero
	-[0x80002eb4]:sw t6, 1264(a5)
Current Store : [0x80002eb8] : sw a7, 1268(a5) -- Store: [0x8000879c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002ec4]:feq.d t6, ft11, ft10
	-[0x80002ec8]:csrrs a7, fflags, zero
	-[0x80002ecc]:sw t6, 1280(a5)
Current Store : [0x80002ed0] : sw a7, 1284(a5) -- Store: [0x800087ac]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002edc]:feq.d t6, ft11, ft10
	-[0x80002ee0]:csrrs a7, fflags, zero
	-[0x80002ee4]:sw t6, 1296(a5)
Current Store : [0x80002ee8] : sw a7, 1300(a5) -- Store: [0x800087bc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002ef4]:feq.d t6, ft11, ft10
	-[0x80002ef8]:csrrs a7, fflags, zero
	-[0x80002efc]:sw t6, 1312(a5)
Current Store : [0x80002f00] : sw a7, 1316(a5) -- Store: [0x800087cc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002f0c]:feq.d t6, ft11, ft10
	-[0x80002f10]:csrrs a7, fflags, zero
	-[0x80002f14]:sw t6, 1328(a5)
Current Store : [0x80002f18] : sw a7, 1332(a5) -- Store: [0x800087dc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002f24]:feq.d t6, ft11, ft10
	-[0x80002f28]:csrrs a7, fflags, zero
	-[0x80002f2c]:sw t6, 1344(a5)
Current Store : [0x80002f30] : sw a7, 1348(a5) -- Store: [0x800087ec]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002f3c]:feq.d t6, ft11, ft10
	-[0x80002f40]:csrrs a7, fflags, zero
	-[0x80002f44]:sw t6, 1360(a5)
Current Store : [0x80002f48] : sw a7, 1364(a5) -- Store: [0x800087fc]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002f54]:feq.d t6, ft11, ft10
	-[0x80002f58]:csrrs a7, fflags, zero
	-[0x80002f5c]:sw t6, 1376(a5)
Current Store : [0x80002f60] : sw a7, 1380(a5) -- Store: [0x8000880c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80002f6c]:feq.d t6, ft11, ft10
	-[0x80002f70]:csrrs a7, fflags, zero
	-[0x80002f74]:sw t6, 1392(a5)
Current Store : [0x80002f78] : sw a7, 1396(a5) -- Store: [0x8000881c]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003338]:feq.d t6, ft11, ft10
	-[0x8000333c]:csrrs a7, fflags, zero
	-[0x80003340]:sw t6, 0(a5)
Current Store : [0x80003344] : sw a7, 4(a5) -- Store: [0x800086a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003350]:feq.d t6, ft11, ft10
	-[0x80003354]:csrrs a7, fflags, zero
	-[0x80003358]:sw t6, 16(a5)
Current Store : [0x8000335c] : sw a7, 20(a5) -- Store: [0x800086b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003368]:feq.d t6, ft11, ft10
	-[0x8000336c]:csrrs a7, fflags, zero
	-[0x80003370]:sw t6, 32(a5)
Current Store : [0x80003374] : sw a7, 36(a5) -- Store: [0x800086c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003380]:feq.d t6, ft11, ft10
	-[0x80003384]:csrrs a7, fflags, zero
	-[0x80003388]:sw t6, 48(a5)
Current Store : [0x8000338c] : sw a7, 52(a5) -- Store: [0x800086d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003398]:feq.d t6, ft11, ft10
	-[0x8000339c]:csrrs a7, fflags, zero
	-[0x800033a0]:sw t6, 64(a5)
Current Store : [0x800033a4] : sw a7, 68(a5) -- Store: [0x800086e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800033b0]:feq.d t6, ft11, ft10
	-[0x800033b4]:csrrs a7, fflags, zero
	-[0x800033b8]:sw t6, 80(a5)
Current Store : [0x800033bc] : sw a7, 84(a5) -- Store: [0x800086f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800033c8]:feq.d t6, ft11, ft10
	-[0x800033cc]:csrrs a7, fflags, zero
	-[0x800033d0]:sw t6, 96(a5)
Current Store : [0x800033d4] : sw a7, 100(a5) -- Store: [0x80008704]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800033e0]:feq.d t6, ft11, ft10
	-[0x800033e4]:csrrs a7, fflags, zero
	-[0x800033e8]:sw t6, 112(a5)
Current Store : [0x800033ec] : sw a7, 116(a5) -- Store: [0x80008714]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800033f8]:feq.d t6, ft11, ft10
	-[0x800033fc]:csrrs a7, fflags, zero
	-[0x80003400]:sw t6, 128(a5)
Current Store : [0x80003404] : sw a7, 132(a5) -- Store: [0x80008724]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003410]:feq.d t6, ft11, ft10
	-[0x80003414]:csrrs a7, fflags, zero
	-[0x80003418]:sw t6, 144(a5)
Current Store : [0x8000341c] : sw a7, 148(a5) -- Store: [0x80008734]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003428]:feq.d t6, ft11, ft10
	-[0x8000342c]:csrrs a7, fflags, zero
	-[0x80003430]:sw t6, 160(a5)
Current Store : [0x80003434] : sw a7, 164(a5) -- Store: [0x80008744]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003440]:feq.d t6, ft11, ft10
	-[0x80003444]:csrrs a7, fflags, zero
	-[0x80003448]:sw t6, 176(a5)
Current Store : [0x8000344c] : sw a7, 180(a5) -- Store: [0x80008754]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003458]:feq.d t6, ft11, ft10
	-[0x8000345c]:csrrs a7, fflags, zero
	-[0x80003460]:sw t6, 192(a5)
Current Store : [0x80003464] : sw a7, 196(a5) -- Store: [0x80008764]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003470]:feq.d t6, ft11, ft10
	-[0x80003474]:csrrs a7, fflags, zero
	-[0x80003478]:sw t6, 208(a5)
Current Store : [0x8000347c] : sw a7, 212(a5) -- Store: [0x80008774]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003488]:feq.d t6, ft11, ft10
	-[0x8000348c]:csrrs a7, fflags, zero
	-[0x80003490]:sw t6, 224(a5)
Current Store : [0x80003494] : sw a7, 228(a5) -- Store: [0x80008784]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800034a0]:feq.d t6, ft11, ft10
	-[0x800034a4]:csrrs a7, fflags, zero
	-[0x800034a8]:sw t6, 240(a5)
Current Store : [0x800034ac] : sw a7, 244(a5) -- Store: [0x80008794]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800034b8]:feq.d t6, ft11, ft10
	-[0x800034bc]:csrrs a7, fflags, zero
	-[0x800034c0]:sw t6, 256(a5)
Current Store : [0x800034c4] : sw a7, 260(a5) -- Store: [0x800087a4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800034d0]:feq.d t6, ft11, ft10
	-[0x800034d4]:csrrs a7, fflags, zero
	-[0x800034d8]:sw t6, 272(a5)
Current Store : [0x800034dc] : sw a7, 276(a5) -- Store: [0x800087b4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800034e8]:feq.d t6, ft11, ft10
	-[0x800034ec]:csrrs a7, fflags, zero
	-[0x800034f0]:sw t6, 288(a5)
Current Store : [0x800034f4] : sw a7, 292(a5) -- Store: [0x800087c4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003500]:feq.d t6, ft11, ft10
	-[0x80003504]:csrrs a7, fflags, zero
	-[0x80003508]:sw t6, 304(a5)
Current Store : [0x8000350c] : sw a7, 308(a5) -- Store: [0x800087d4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003518]:feq.d t6, ft11, ft10
	-[0x8000351c]:csrrs a7, fflags, zero
	-[0x80003520]:sw t6, 320(a5)
Current Store : [0x80003524] : sw a7, 324(a5) -- Store: [0x800087e4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003530]:feq.d t6, ft11, ft10
	-[0x80003534]:csrrs a7, fflags, zero
	-[0x80003538]:sw t6, 336(a5)
Current Store : [0x8000353c] : sw a7, 340(a5) -- Store: [0x800087f4]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003548]:feq.d t6, ft11, ft10
	-[0x8000354c]:csrrs a7, fflags, zero
	-[0x80003550]:sw t6, 352(a5)
Current Store : [0x80003554] : sw a7, 356(a5) -- Store: [0x80008804]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80003560]:feq.d t6, ft11, ft10
	-[0x80003564]:csrrs a7, fflags, zero
	-[0x80003568]:sw t6, 368(a5)
Current Store : [0x8000356c] : sw a7, 372(a5) -- Store: [0x80008814]:0x00000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                           coverpoints                                                                                                           |                                                      code                                                      |
|---:|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
|   1|[0x80007610]<br>0x00000001|- opcode : feq.d<br> - rd : x15<br> - rs1 : f21<br> - rs2 : f26<br> - rs1 != rs2<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br> |[0x8000011c]:feq.d a5, fs5, fs10<br> [0x80000120]:csrrs s5, fflags, zero<br> [0x80000124]:sw a5, 0(s3)<br>      |
|   2|[0x80007618]<br>0x00000001|- rd : x31<br> - rs1 : f7<br> - rs2 : f7<br> - rs1 == rs2<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                        |[0x80000140]:feq.d t6, ft7, ft7<br> [0x80000144]:csrrs a7, fflags, zero<br> [0x80000148]:sw t6, 0(a5)<br>       |
|   3|[0x80007628]<br>0x00000000|- rd : x29<br> - rs1 : f12<br> - rs2 : f21<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                       |[0x80000158]:feq.d t4, fa2, fs5<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:sw t4, 16(a5)<br>      |
|   4|[0x80007628]<br>0x00000000|- rd : x17<br> - rs1 : f29<br> - rs2 : f20<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                       |[0x8000017c]:feq.d a7, ft9, fs4<br> [0x80000180]:csrrs s5, fflags, zero<br> [0x80000184]:sw a7, 0(s3)<br>       |
|   5|[0x80007630]<br>0x00000000|- rd : x4<br> - rs1 : f15<br> - rs2 : f12<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                        |[0x800001a0]:feq.d tp, fa5, fa2<br> [0x800001a4]:csrrs a7, fflags, zero<br> [0x800001a8]:sw tp, 0(a5)<br>       |
|   6|[0x80007640]<br>0x00000000|- rd : x13<br> - rs1 : f19<br> - rs2 : f4<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                        |[0x800001b8]:feq.d a3, fs3, ft4<br> [0x800001bc]:csrrs a7, fflags, zero<br> [0x800001c0]:sw a3, 16(a5)<br>      |
|   7|[0x80007650]<br>0x00000000|- rd : x9<br> - rs1 : f4<br> - rs2 : f24<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                         |[0x800001d0]:feq.d s1, ft4, fs8<br> [0x800001d4]:csrrs a7, fflags, zero<br> [0x800001d8]:sw s1, 32(a5)<br>      |
|   8|[0x80007660]<br>0x00000000|- rd : x20<br> - rs1 : f20<br> - rs2 : f18<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                       |[0x800001e8]:feq.d s4, fs4, fs2<br> [0x800001ec]:csrrs a7, fflags, zero<br> [0x800001f0]:sw s4, 48(a5)<br>      |
|   9|[0x80007670]<br>0x00000000|- rd : x27<br> - rs1 : f11<br> - rs2 : f22<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                       |[0x80000200]:feq.d s11, fa1, fs6<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:sw s11, 64(a5)<br>    |
|  10|[0x80007680]<br>0x00000000|- rd : x23<br> - rs1 : f24<br> - rs2 : f8<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                        |[0x80000218]:feq.d s7, fs8, fs0<br> [0x8000021c]:csrrs a7, fflags, zero<br> [0x80000220]:sw s7, 80(a5)<br>      |
|  11|[0x80007690]<br>0x00000000|- rd : x11<br> - rs1 : f6<br> - rs2 : f25<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                        |[0x80000230]:feq.d a1, ft6, fs9<br> [0x80000234]:csrrs a7, fflags, zero<br> [0x80000238]:sw a1, 96(a5)<br>      |
|  12|[0x800076a0]<br>0x00000000|- rd : x22<br> - rs1 : f10<br> - rs2 : f5<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                        |[0x80000248]:feq.d s6, fa0, ft5<br> [0x8000024c]:csrrs a7, fflags, zero<br> [0x80000250]:sw s6, 112(a5)<br>     |
|  13|[0x800076b0]<br>0x00000000|- rd : x19<br> - rs1 : f13<br> - rs2 : f3<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                        |[0x80000260]:feq.d s3, fa3, ft3<br> [0x80000264]:csrrs a7, fflags, zero<br> [0x80000268]:sw s3, 128(a5)<br>     |
|  14|[0x800076c0]<br>0x00000000|- rd : x5<br> - rs1 : f26<br> - rs2 : f23<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                        |[0x80000278]:feq.d t0, fs10, fs7<br> [0x8000027c]:csrrs a7, fflags, zero<br> [0x80000280]:sw t0, 144(a5)<br>    |
|  15|[0x800076d0]<br>0x00000000|- rd : x18<br> - rs1 : f8<br> - rs2 : f27<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                        |[0x80000290]:feq.d s2, fs0, fs11<br> [0x80000294]:csrrs a7, fflags, zero<br> [0x80000298]:sw s2, 160(a5)<br>    |
|  16|[0x800076e0]<br>0x00000000|- rd : x6<br> - rs1 : f23<br> - rs2 : f0<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                         |[0x800002a8]:feq.d t1, fs7, ft0<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:sw t1, 176(a5)<br>     |
|  17|[0x800076f0]<br>0x00000000|- rd : x2<br> - rs1 : f2<br> - rs2 : f11<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                         |[0x800002c0]:feq.d sp, ft2, fa1<br> [0x800002c4]:csrrs a7, fflags, zero<br> [0x800002c8]:sw sp, 192(a5)<br>     |
|  18|[0x80007700]<br>0x00000000|- rd : x10<br> - rs1 : f27<br> - rs2 : f19<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                       |[0x800002d8]:feq.d a0, fs11, fs3<br> [0x800002dc]:csrrs a7, fflags, zero<br> [0x800002e0]:sw a0, 208(a5)<br>    |
|  19|[0x80007710]<br>0x00000000|- rd : x3<br> - rs1 : f1<br> - rs2 : f13<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                         |[0x800002f0]:feq.d gp, ft1, fa3<br> [0x800002f4]:csrrs a7, fflags, zero<br> [0x800002f8]:sw gp, 224(a5)<br>     |
|  20|[0x80007720]<br>0x00000000|- rd : x7<br> - rs1 : f31<br> - rs2 : f28<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                        |[0x80000308]:feq.d t2, ft11, ft8<br> [0x8000030c]:csrrs a7, fflags, zero<br> [0x80000310]:sw t2, 240(a5)<br>    |
|  21|[0x80007730]<br>0x00000000|- rd : x0<br> - rs1 : f9<br> - rs2 : f17<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                         |[0x80000320]:feq.d zero, fs1, fa7<br> [0x80000324]:csrrs a7, fflags, zero<br> [0x80000328]:sw zero, 256(a5)<br> |
|  22|[0x800076b8]<br>0x00000000|- rd : x16<br> - rs1 : f14<br> - rs2 : f30<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                       |[0x80000344]:feq.d a6, fa4, ft10<br> [0x80000348]:csrrs s5, fflags, zero<br> [0x8000034c]:sw a6, 0(s3)<br>      |
|  23|[0x800076c0]<br>0x00000000|- rd : x12<br> - rs1 : f5<br> - rs2 : f1<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                         |[0x80000368]:feq.d a2, ft5, ft1<br> [0x8000036c]:csrrs a7, fflags, zero<br> [0x80000370]:sw a2, 0(a5)<br>       |
|  24|[0x800076d0]<br>0x00000000|- rd : x1<br> - rs1 : f3<br> - rs2 : f10<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                         |[0x80000380]:feq.d ra, ft3, fa0<br> [0x80000384]:csrrs a7, fflags, zero<br> [0x80000388]:sw ra, 16(a5)<br>      |
|  25|[0x800076e0]<br>0x00000000|- rd : x8<br> - rs1 : f25<br> - rs2 : f15<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                        |[0x80000398]:feq.d fp, fs9, fa5<br> [0x8000039c]:csrrs a7, fflags, zero<br> [0x800003a0]:sw fp, 32(a5)<br>      |
|  26|[0x800076f0]<br>0x00000000|- rd : x28<br> - rs1 : f16<br> - rs2 : f29<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                       |[0x800003b0]:feq.d t3, fa6, ft9<br> [0x800003b4]:csrrs a7, fflags, zero<br> [0x800003b8]:sw t3, 48(a5)<br>      |
|  27|[0x80007700]<br>0x00000001|- rd : x26<br> - rs1 : f22<br> - rs2 : f9<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                        |[0x800003c8]:feq.d s10, fs6, fs1<br> [0x800003cc]:csrrs a7, fflags, zero<br> [0x800003d0]:sw s10, 64(a5)<br>    |
|  28|[0x80007710]<br>0x00000000|- rd : x30<br> - rs1 : f0<br> - rs2 : f14<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                        |[0x800003e0]:feq.d t5, ft0, fa4<br> [0x800003e4]:csrrs a7, fflags, zero<br> [0x800003e8]:sw t5, 80(a5)<br>      |
|  29|[0x80007720]<br>0x00000000|- rd : x24<br> - rs1 : f28<br> - rs2 : f31<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                       |[0x800003f8]:feq.d s8, ft8, ft11<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:sw s8, 96(a5)<br>     |
|  30|[0x80007730]<br>0x00000000|- rd : x25<br> - rs1 : f17<br> - rs2 : f6<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                        |[0x80000410]:feq.d s9, fa7, ft6<br> [0x80000414]:csrrs a7, fflags, zero<br> [0x80000418]:sw s9, 112(a5)<br>     |
|  31|[0x80007740]<br>0x00000000|- rd : x21<br> - rs1 : f18<br> - rs2 : f16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                       |[0x80000428]:feq.d s5, fs2, fa6<br> [0x8000042c]:csrrs a7, fflags, zero<br> [0x80000430]:sw s5, 128(a5)<br>     |
|  32|[0x80007750]<br>0x00000000|- rd : x14<br> - rs1 : f30<br> - rs2 : f2<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                        |[0x80000440]:feq.d a4, ft10, ft2<br> [0x80000444]:csrrs a7, fflags, zero<br> [0x80000448]:sw a4, 144(a5)<br>    |
|  33|[0x80007760]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000458]:feq.d t6, ft11, ft10<br> [0x8000045c]:csrrs a7, fflags, zero<br> [0x80000460]:sw t6, 160(a5)<br>   |
|  34|[0x80007770]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000470]:feq.d t6, ft11, ft10<br> [0x80000474]:csrrs a7, fflags, zero<br> [0x80000478]:sw t6, 176(a5)<br>   |
|  35|[0x80007780]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000488]:feq.d t6, ft11, ft10<br> [0x8000048c]:csrrs a7, fflags, zero<br> [0x80000490]:sw t6, 192(a5)<br>   |
|  36|[0x80007790]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800004a0]:feq.d t6, ft11, ft10<br> [0x800004a4]:csrrs a7, fflags, zero<br> [0x800004a8]:sw t6, 208(a5)<br>   |
|  37|[0x800077a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800004b8]:feq.d t6, ft11, ft10<br> [0x800004bc]:csrrs a7, fflags, zero<br> [0x800004c0]:sw t6, 224(a5)<br>   |
|  38|[0x800077b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800004d0]:feq.d t6, ft11, ft10<br> [0x800004d4]:csrrs a7, fflags, zero<br> [0x800004d8]:sw t6, 240(a5)<br>   |
|  39|[0x800077c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800004e8]:feq.d t6, ft11, ft10<br> [0x800004ec]:csrrs a7, fflags, zero<br> [0x800004f0]:sw t6, 256(a5)<br>   |
|  40|[0x800077d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000500]:feq.d t6, ft11, ft10<br> [0x80000504]:csrrs a7, fflags, zero<br> [0x80000508]:sw t6, 272(a5)<br>   |
|  41|[0x800077e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000518]:feq.d t6, ft11, ft10<br> [0x8000051c]:csrrs a7, fflags, zero<br> [0x80000520]:sw t6, 288(a5)<br>   |
|  42|[0x800077f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000530]:feq.d t6, ft11, ft10<br> [0x80000534]:csrrs a7, fflags, zero<br> [0x80000538]:sw t6, 304(a5)<br>   |
|  43|[0x80007800]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000548]:feq.d t6, ft11, ft10<br> [0x8000054c]:csrrs a7, fflags, zero<br> [0x80000550]:sw t6, 320(a5)<br>   |
|  44|[0x80007810]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000560]:feq.d t6, ft11, ft10<br> [0x80000564]:csrrs a7, fflags, zero<br> [0x80000568]:sw t6, 336(a5)<br>   |
|  45|[0x80007820]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000578]:feq.d t6, ft11, ft10<br> [0x8000057c]:csrrs a7, fflags, zero<br> [0x80000580]:sw t6, 352(a5)<br>   |
|  46|[0x80007830]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000590]:feq.d t6, ft11, ft10<br> [0x80000594]:csrrs a7, fflags, zero<br> [0x80000598]:sw t6, 368(a5)<br>   |
|  47|[0x80007840]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800005a8]:feq.d t6, ft11, ft10<br> [0x800005ac]:csrrs a7, fflags, zero<br> [0x800005b0]:sw t6, 384(a5)<br>   |
|  48|[0x80007850]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800005c0]:feq.d t6, ft11, ft10<br> [0x800005c4]:csrrs a7, fflags, zero<br> [0x800005c8]:sw t6, 400(a5)<br>   |
|  49|[0x80007860]<br>0x00000000|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800005d8]:feq.d t6, ft11, ft10<br> [0x800005dc]:csrrs a7, fflags, zero<br> [0x800005e0]:sw t6, 416(a5)<br>   |
|  50|[0x80007870]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800005f0]:feq.d t6, ft11, ft10<br> [0x800005f4]:csrrs a7, fflags, zero<br> [0x800005f8]:sw t6, 432(a5)<br>   |
|  51|[0x80007880]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000608]:feq.d t6, ft11, ft10<br> [0x8000060c]:csrrs a7, fflags, zero<br> [0x80000610]:sw t6, 448(a5)<br>   |
|  52|[0x80007890]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000620]:feq.d t6, ft11, ft10<br> [0x80000624]:csrrs a7, fflags, zero<br> [0x80000628]:sw t6, 464(a5)<br>   |
|  53|[0x800078a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000638]:feq.d t6, ft11, ft10<br> [0x8000063c]:csrrs a7, fflags, zero<br> [0x80000640]:sw t6, 480(a5)<br>   |
|  54|[0x800078b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000650]:feq.d t6, ft11, ft10<br> [0x80000654]:csrrs a7, fflags, zero<br> [0x80000658]:sw t6, 496(a5)<br>   |
|  55|[0x800078c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000668]:feq.d t6, ft11, ft10<br> [0x8000066c]:csrrs a7, fflags, zero<br> [0x80000670]:sw t6, 512(a5)<br>   |
|  56|[0x800078d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000680]:feq.d t6, ft11, ft10<br> [0x80000684]:csrrs a7, fflags, zero<br> [0x80000688]:sw t6, 528(a5)<br>   |
|  57|[0x800078e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000698]:feq.d t6, ft11, ft10<br> [0x8000069c]:csrrs a7, fflags, zero<br> [0x800006a0]:sw t6, 544(a5)<br>   |
|  58|[0x800078f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800006b0]:feq.d t6, ft11, ft10<br> [0x800006b4]:csrrs a7, fflags, zero<br> [0x800006b8]:sw t6, 560(a5)<br>   |
|  59|[0x80007900]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800006c8]:feq.d t6, ft11, ft10<br> [0x800006cc]:csrrs a7, fflags, zero<br> [0x800006d0]:sw t6, 576(a5)<br>   |
|  60|[0x80007910]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800006e0]:feq.d t6, ft11, ft10<br> [0x800006e4]:csrrs a7, fflags, zero<br> [0x800006e8]:sw t6, 592(a5)<br>   |
|  61|[0x80007920]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800006f8]:feq.d t6, ft11, ft10<br> [0x800006fc]:csrrs a7, fflags, zero<br> [0x80000700]:sw t6, 608(a5)<br>   |
|  62|[0x80007930]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000710]:feq.d t6, ft11, ft10<br> [0x80000714]:csrrs a7, fflags, zero<br> [0x80000718]:sw t6, 624(a5)<br>   |
|  63|[0x80007940]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000728]:feq.d t6, ft11, ft10<br> [0x8000072c]:csrrs a7, fflags, zero<br> [0x80000730]:sw t6, 640(a5)<br>   |
|  64|[0x80007950]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000740]:feq.d t6, ft11, ft10<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:sw t6, 656(a5)<br>   |
|  65|[0x80007960]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000758]:feq.d t6, ft11, ft10<br> [0x8000075c]:csrrs a7, fflags, zero<br> [0x80000760]:sw t6, 672(a5)<br>   |
|  66|[0x80007970]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000770]:feq.d t6, ft11, ft10<br> [0x80000774]:csrrs a7, fflags, zero<br> [0x80000778]:sw t6, 688(a5)<br>   |
|  67|[0x80007980]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000788]:feq.d t6, ft11, ft10<br> [0x8000078c]:csrrs a7, fflags, zero<br> [0x80000790]:sw t6, 704(a5)<br>   |
|  68|[0x80007990]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800007a0]:feq.d t6, ft11, ft10<br> [0x800007a4]:csrrs a7, fflags, zero<br> [0x800007a8]:sw t6, 720(a5)<br>   |
|  69|[0x800079a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800007b8]:feq.d t6, ft11, ft10<br> [0x800007bc]:csrrs a7, fflags, zero<br> [0x800007c0]:sw t6, 736(a5)<br>   |
|  70|[0x800079b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800007d0]:feq.d t6, ft11, ft10<br> [0x800007d4]:csrrs a7, fflags, zero<br> [0x800007d8]:sw t6, 752(a5)<br>   |
|  71|[0x800079c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800007e8]:feq.d t6, ft11, ft10<br> [0x800007ec]:csrrs a7, fflags, zero<br> [0x800007f0]:sw t6, 768(a5)<br>   |
|  72|[0x800079d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000800]:feq.d t6, ft11, ft10<br> [0x80000804]:csrrs a7, fflags, zero<br> [0x80000808]:sw t6, 784(a5)<br>   |
|  73|[0x800079e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000818]:feq.d t6, ft11, ft10<br> [0x8000081c]:csrrs a7, fflags, zero<br> [0x80000820]:sw t6, 800(a5)<br>   |
|  74|[0x800079f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000830]:feq.d t6, ft11, ft10<br> [0x80000834]:csrrs a7, fflags, zero<br> [0x80000838]:sw t6, 816(a5)<br>   |
|  75|[0x80007a00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000848]:feq.d t6, ft11, ft10<br> [0x8000084c]:csrrs a7, fflags, zero<br> [0x80000850]:sw t6, 832(a5)<br>   |
|  76|[0x80007a10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000860]:feq.d t6, ft11, ft10<br> [0x80000864]:csrrs a7, fflags, zero<br> [0x80000868]:sw t6, 848(a5)<br>   |
|  77|[0x80007a20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000878]:feq.d t6, ft11, ft10<br> [0x8000087c]:csrrs a7, fflags, zero<br> [0x80000880]:sw t6, 864(a5)<br>   |
|  78|[0x80007a30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000890]:feq.d t6, ft11, ft10<br> [0x80000894]:csrrs a7, fflags, zero<br> [0x80000898]:sw t6, 880(a5)<br>   |
|  79|[0x80007a40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800008a8]:feq.d t6, ft11, ft10<br> [0x800008ac]:csrrs a7, fflags, zero<br> [0x800008b0]:sw t6, 896(a5)<br>   |
|  80|[0x80007a50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800008c0]:feq.d t6, ft11, ft10<br> [0x800008c4]:csrrs a7, fflags, zero<br> [0x800008c8]:sw t6, 912(a5)<br>   |
|  81|[0x80007a60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800008d8]:feq.d t6, ft11, ft10<br> [0x800008dc]:csrrs a7, fflags, zero<br> [0x800008e0]:sw t6, 928(a5)<br>   |
|  82|[0x80007a70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800008f0]:feq.d t6, ft11, ft10<br> [0x800008f4]:csrrs a7, fflags, zero<br> [0x800008f8]:sw t6, 944(a5)<br>   |
|  83|[0x80007a80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000908]:feq.d t6, ft11, ft10<br> [0x8000090c]:csrrs a7, fflags, zero<br> [0x80000910]:sw t6, 960(a5)<br>   |
|  84|[0x80007a90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000920]:feq.d t6, ft11, ft10<br> [0x80000924]:csrrs a7, fflags, zero<br> [0x80000928]:sw t6, 976(a5)<br>   |
|  85|[0x80007aa0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000938]:feq.d t6, ft11, ft10<br> [0x8000093c]:csrrs a7, fflags, zero<br> [0x80000940]:sw t6, 992(a5)<br>   |
|  86|[0x80007ab0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000950]:feq.d t6, ft11, ft10<br> [0x80000954]:csrrs a7, fflags, zero<br> [0x80000958]:sw t6, 1008(a5)<br>  |
|  87|[0x80007ac0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000968]:feq.d t6, ft11, ft10<br> [0x8000096c]:csrrs a7, fflags, zero<br> [0x80000970]:sw t6, 1024(a5)<br>  |
|  88|[0x80007ad0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000980]:feq.d t6, ft11, ft10<br> [0x80000984]:csrrs a7, fflags, zero<br> [0x80000988]:sw t6, 1040(a5)<br>  |
|  89|[0x80007ae0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000998]:feq.d t6, ft11, ft10<br> [0x8000099c]:csrrs a7, fflags, zero<br> [0x800009a0]:sw t6, 1056(a5)<br>  |
|  90|[0x80007af0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800009b0]:feq.d t6, ft11, ft10<br> [0x800009b4]:csrrs a7, fflags, zero<br> [0x800009b8]:sw t6, 1072(a5)<br>  |
|  91|[0x80007b00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800009c8]:feq.d t6, ft11, ft10<br> [0x800009cc]:csrrs a7, fflags, zero<br> [0x800009d0]:sw t6, 1088(a5)<br>  |
|  92|[0x80007b10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800009e0]:feq.d t6, ft11, ft10<br> [0x800009e4]:csrrs a7, fflags, zero<br> [0x800009e8]:sw t6, 1104(a5)<br>  |
|  93|[0x80007b20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800009f8]:feq.d t6, ft11, ft10<br> [0x800009fc]:csrrs a7, fflags, zero<br> [0x80000a00]:sw t6, 1120(a5)<br>  |
|  94|[0x80007b30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a10]:feq.d t6, ft11, ft10<br> [0x80000a14]:csrrs a7, fflags, zero<br> [0x80000a18]:sw t6, 1136(a5)<br>  |
|  95|[0x80007b40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a28]:feq.d t6, ft11, ft10<br> [0x80000a2c]:csrrs a7, fflags, zero<br> [0x80000a30]:sw t6, 1152(a5)<br>  |
|  96|[0x80007b50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a40]:feq.d t6, ft11, ft10<br> [0x80000a44]:csrrs a7, fflags, zero<br> [0x80000a48]:sw t6, 1168(a5)<br>  |
|  97|[0x80007b60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a58]:feq.d t6, ft11, ft10<br> [0x80000a5c]:csrrs a7, fflags, zero<br> [0x80000a60]:sw t6, 1184(a5)<br>  |
|  98|[0x80007b70]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a70]:feq.d t6, ft11, ft10<br> [0x80000a74]:csrrs a7, fflags, zero<br> [0x80000a78]:sw t6, 1200(a5)<br>  |
|  99|[0x80007b80]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000a88]:feq.d t6, ft11, ft10<br> [0x80000a8c]:csrrs a7, fflags, zero<br> [0x80000a90]:sw t6, 1216(a5)<br>  |
| 100|[0x80007b90]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000aa0]:feq.d t6, ft11, ft10<br> [0x80000aa4]:csrrs a7, fflags, zero<br> [0x80000aa8]:sw t6, 1232(a5)<br>  |
| 101|[0x80007ba0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ab8]:feq.d t6, ft11, ft10<br> [0x80000abc]:csrrs a7, fflags, zero<br> [0x80000ac0]:sw t6, 1248(a5)<br>  |
| 102|[0x80007bb0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ad0]:feq.d t6, ft11, ft10<br> [0x80000ad4]:csrrs a7, fflags, zero<br> [0x80000ad8]:sw t6, 1264(a5)<br>  |
| 103|[0x80007bc0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ae8]:feq.d t6, ft11, ft10<br> [0x80000aec]:csrrs a7, fflags, zero<br> [0x80000af0]:sw t6, 1280(a5)<br>  |
| 104|[0x80007bd0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000b00]:feq.d t6, ft11, ft10<br> [0x80000b04]:csrrs a7, fflags, zero<br> [0x80000b08]:sw t6, 1296(a5)<br>  |
| 105|[0x80007be0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000b18]:feq.d t6, ft11, ft10<br> [0x80000b1c]:csrrs a7, fflags, zero<br> [0x80000b20]:sw t6, 1312(a5)<br>  |
| 106|[0x80007bf0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000b30]:feq.d t6, ft11, ft10<br> [0x80000b34]:csrrs a7, fflags, zero<br> [0x80000b38]:sw t6, 1328(a5)<br>  |
| 107|[0x80007c00]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000b48]:feq.d t6, ft11, ft10<br> [0x80000b4c]:csrrs a7, fflags, zero<br> [0x80000b50]:sw t6, 1344(a5)<br>  |
| 108|[0x80007c10]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000b60]:feq.d t6, ft11, ft10<br> [0x80000b64]:csrrs a7, fflags, zero<br> [0x80000b68]:sw t6, 1360(a5)<br>  |
| 109|[0x80007c20]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000b78]:feq.d t6, ft11, ft10<br> [0x80000b7c]:csrrs a7, fflags, zero<br> [0x80000b80]:sw t6, 1376(a5)<br>  |
| 110|[0x80007c30]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000b90]:feq.d t6, ft11, ft10<br> [0x80000b94]:csrrs a7, fflags, zero<br> [0x80000b98]:sw t6, 1392(a5)<br>  |
| 111|[0x80007c40]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ba8]:feq.d t6, ft11, ft10<br> [0x80000bac]:csrrs a7, fflags, zero<br> [0x80000bb0]:sw t6, 1408(a5)<br>  |
| 112|[0x80007c50]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000bc0]:feq.d t6, ft11, ft10<br> [0x80000bc4]:csrrs a7, fflags, zero<br> [0x80000bc8]:sw t6, 1424(a5)<br>  |
| 113|[0x80007c60]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000bd8]:feq.d t6, ft11, ft10<br> [0x80000bdc]:csrrs a7, fflags, zero<br> [0x80000be0]:sw t6, 1440(a5)<br>  |
| 114|[0x80007c70]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000bf0]:feq.d t6, ft11, ft10<br> [0x80000bf4]:csrrs a7, fflags, zero<br> [0x80000bf8]:sw t6, 1456(a5)<br>  |
| 115|[0x80007c80]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000c08]:feq.d t6, ft11, ft10<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:sw t6, 1472(a5)<br>  |
| 116|[0x80007c90]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c20]:feq.d t6, ft11, ft10<br> [0x80000c24]:csrrs a7, fflags, zero<br> [0x80000c28]:sw t6, 1488(a5)<br>  |
| 117|[0x80007ca0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c38]:feq.d t6, ft11, ft10<br> [0x80000c3c]:csrrs a7, fflags, zero<br> [0x80000c40]:sw t6, 1504(a5)<br>  |
| 118|[0x80007cb0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c50]:feq.d t6, ft11, ft10<br> [0x80000c54]:csrrs a7, fflags, zero<br> [0x80000c58]:sw t6, 1520(a5)<br>  |
| 119|[0x80007cc0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c68]:feq.d t6, ft11, ft10<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:sw t6, 1536(a5)<br>  |
| 120|[0x80007cd0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c80]:feq.d t6, ft11, ft10<br> [0x80000c84]:csrrs a7, fflags, zero<br> [0x80000c88]:sw t6, 1552(a5)<br>  |
| 121|[0x80007ce0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000c98]:feq.d t6, ft11, ft10<br> [0x80000c9c]:csrrs a7, fflags, zero<br> [0x80000ca0]:sw t6, 1568(a5)<br>  |
| 122|[0x80007cf0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000cb0]:feq.d t6, ft11, ft10<br> [0x80000cb4]:csrrs a7, fflags, zero<br> [0x80000cb8]:sw t6, 1584(a5)<br>  |
| 123|[0x80007d00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000cc8]:feq.d t6, ft11, ft10<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:sw t6, 1600(a5)<br>  |
| 124|[0x80007d10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ce0]:feq.d t6, ft11, ft10<br> [0x80000ce4]:csrrs a7, fflags, zero<br> [0x80000ce8]:sw t6, 1616(a5)<br>  |
| 125|[0x80007d20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000cf8]:feq.d t6, ft11, ft10<br> [0x80000cfc]:csrrs a7, fflags, zero<br> [0x80000d00]:sw t6, 1632(a5)<br>  |
| 126|[0x80007d30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d10]:feq.d t6, ft11, ft10<br> [0x80000d14]:csrrs a7, fflags, zero<br> [0x80000d18]:sw t6, 1648(a5)<br>  |
| 127|[0x80007d40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d28]:feq.d t6, ft11, ft10<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:sw t6, 1664(a5)<br>  |
| 128|[0x80007d50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d44]:feq.d t6, ft11, ft10<br> [0x80000d48]:csrrs a7, fflags, zero<br> [0x80000d4c]:sw t6, 1680(a5)<br>  |
| 129|[0x80007d60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d5c]:feq.d t6, ft11, ft10<br> [0x80000d60]:csrrs a7, fflags, zero<br> [0x80000d64]:sw t6, 1696(a5)<br>  |
| 130|[0x80007d70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d74]:feq.d t6, ft11, ft10<br> [0x80000d78]:csrrs a7, fflags, zero<br> [0x80000d7c]:sw t6, 1712(a5)<br>  |
| 131|[0x80007d80]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000d8c]:feq.d t6, ft11, ft10<br> [0x80000d90]:csrrs a7, fflags, zero<br> [0x80000d94]:sw t6, 1728(a5)<br>  |
| 132|[0x80007d90]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000da4]:feq.d t6, ft11, ft10<br> [0x80000da8]:csrrs a7, fflags, zero<br> [0x80000dac]:sw t6, 1744(a5)<br>  |
| 133|[0x80007da0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000dbc]:feq.d t6, ft11, ft10<br> [0x80000dc0]:csrrs a7, fflags, zero<br> [0x80000dc4]:sw t6, 1760(a5)<br>  |
| 134|[0x80007db0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000dd4]:feq.d t6, ft11, ft10<br> [0x80000dd8]:csrrs a7, fflags, zero<br> [0x80000ddc]:sw t6, 1776(a5)<br>  |
| 135|[0x80007dc0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000dec]:feq.d t6, ft11, ft10<br> [0x80000df0]:csrrs a7, fflags, zero<br> [0x80000df4]:sw t6, 1792(a5)<br>  |
| 136|[0x80007dd0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000e04]:feq.d t6, ft11, ft10<br> [0x80000e08]:csrrs a7, fflags, zero<br> [0x80000e0c]:sw t6, 1808(a5)<br>  |
| 137|[0x80007de0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000e1c]:feq.d t6, ft11, ft10<br> [0x80000e20]:csrrs a7, fflags, zero<br> [0x80000e24]:sw t6, 1824(a5)<br>  |
| 138|[0x80007df0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000e34]:feq.d t6, ft11, ft10<br> [0x80000e38]:csrrs a7, fflags, zero<br> [0x80000e3c]:sw t6, 1840(a5)<br>  |
| 139|[0x80007e00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000e4c]:feq.d t6, ft11, ft10<br> [0x80000e50]:csrrs a7, fflags, zero<br> [0x80000e54]:sw t6, 1856(a5)<br>  |
| 140|[0x80007e10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000e64]:feq.d t6, ft11, ft10<br> [0x80000e68]:csrrs a7, fflags, zero<br> [0x80000e6c]:sw t6, 1872(a5)<br>  |
| 141|[0x80007e20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80000e7c]:feq.d t6, ft11, ft10<br> [0x80000e80]:csrrs a7, fflags, zero<br> [0x80000e84]:sw t6, 1888(a5)<br>  |
| 142|[0x80007e30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000e94]:feq.d t6, ft11, ft10<br> [0x80000e98]:csrrs a7, fflags, zero<br> [0x80000e9c]:sw t6, 1904(a5)<br>  |
| 143|[0x80007e40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000eac]:feq.d t6, ft11, ft10<br> [0x80000eb0]:csrrs a7, fflags, zero<br> [0x80000eb4]:sw t6, 1920(a5)<br>  |
| 144|[0x80007e50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ec4]:feq.d t6, ft11, ft10<br> [0x80000ec8]:csrrs a7, fflags, zero<br> [0x80000ecc]:sw t6, 1936(a5)<br>  |
| 145|[0x80007e60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000edc]:feq.d t6, ft11, ft10<br> [0x80000ee0]:csrrs a7, fflags, zero<br> [0x80000ee4]:sw t6, 1952(a5)<br>  |
| 146|[0x80007e70]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000ef4]:feq.d t6, ft11, ft10<br> [0x80000ef8]:csrrs a7, fflags, zero<br> [0x80000efc]:sw t6, 1968(a5)<br>  |
| 147|[0x80007e80]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f0c]:feq.d t6, ft11, ft10<br> [0x80000f10]:csrrs a7, fflags, zero<br> [0x80000f14]:sw t6, 1984(a5)<br>  |
| 148|[0x80007e90]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f24]:feq.d t6, ft11, ft10<br> [0x80000f28]:csrrs a7, fflags, zero<br> [0x80000f2c]:sw t6, 2000(a5)<br>  |
| 149|[0x80007ea0]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f3c]:feq.d t6, ft11, ft10<br> [0x80000f40]:csrrs a7, fflags, zero<br> [0x80000f44]:sw t6, 2016(a5)<br>  |
| 150|[0x80007ab8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f5c]:feq.d t6, ft11, ft10<br> [0x80000f60]:csrrs a7, fflags, zero<br> [0x80000f64]:sw t6, 0(a5)<br>     |
| 151|[0x80007ac8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f74]:feq.d t6, ft11, ft10<br> [0x80000f78]:csrrs a7, fflags, zero<br> [0x80000f7c]:sw t6, 16(a5)<br>    |
| 152|[0x80007ad8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000f8c]:feq.d t6, ft11, ft10<br> [0x80000f90]:csrrs a7, fflags, zero<br> [0x80000f94]:sw t6, 32(a5)<br>    |
| 153|[0x80007ae8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000fa4]:feq.d t6, ft11, ft10<br> [0x80000fa8]:csrrs a7, fflags, zero<br> [0x80000fac]:sw t6, 48(a5)<br>    |
| 154|[0x80007af8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000fbc]:feq.d t6, ft11, ft10<br> [0x80000fc0]:csrrs a7, fflags, zero<br> [0x80000fc4]:sw t6, 64(a5)<br>    |
| 155|[0x80007b08]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80000fd4]:feq.d t6, ft11, ft10<br> [0x80000fd8]:csrrs a7, fflags, zero<br> [0x80000fdc]:sw t6, 80(a5)<br>    |
| 156|[0x80007b18]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80000fec]:feq.d t6, ft11, ft10<br> [0x80000ff0]:csrrs a7, fflags, zero<br> [0x80000ff4]:sw t6, 96(a5)<br>    |
| 157|[0x80007b28]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001004]:feq.d t6, ft11, ft10<br> [0x80001008]:csrrs a7, fflags, zero<br> [0x8000100c]:sw t6, 112(a5)<br>   |
| 158|[0x80007b38]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000101c]:feq.d t6, ft11, ft10<br> [0x80001020]:csrrs a7, fflags, zero<br> [0x80001024]:sw t6, 128(a5)<br>   |
| 159|[0x80007b48]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001034]:feq.d t6, ft11, ft10<br> [0x80001038]:csrrs a7, fflags, zero<br> [0x8000103c]:sw t6, 144(a5)<br>   |
| 160|[0x80007b58]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000104c]:feq.d t6, ft11, ft10<br> [0x80001050]:csrrs a7, fflags, zero<br> [0x80001054]:sw t6, 160(a5)<br>   |
| 161|[0x80007b68]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001064]:feq.d t6, ft11, ft10<br> [0x80001068]:csrrs a7, fflags, zero<br> [0x8000106c]:sw t6, 176(a5)<br>   |
| 162|[0x80007b78]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000107c]:feq.d t6, ft11, ft10<br> [0x80001080]:csrrs a7, fflags, zero<br> [0x80001084]:sw t6, 192(a5)<br>   |
| 163|[0x80007b88]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001094]:feq.d t6, ft11, ft10<br> [0x80001098]:csrrs a7, fflags, zero<br> [0x8000109c]:sw t6, 208(a5)<br>   |
| 164|[0x80007b98]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800010ac]:feq.d t6, ft11, ft10<br> [0x800010b0]:csrrs a7, fflags, zero<br> [0x800010b4]:sw t6, 224(a5)<br>   |
| 165|[0x80007ba8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800010c4]:feq.d t6, ft11, ft10<br> [0x800010c8]:csrrs a7, fflags, zero<br> [0x800010cc]:sw t6, 240(a5)<br>   |
| 166|[0x80007bb8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800010dc]:feq.d t6, ft11, ft10<br> [0x800010e0]:csrrs a7, fflags, zero<br> [0x800010e4]:sw t6, 256(a5)<br>   |
| 167|[0x80007bc8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800010f4]:feq.d t6, ft11, ft10<br> [0x800010f8]:csrrs a7, fflags, zero<br> [0x800010fc]:sw t6, 272(a5)<br>   |
| 168|[0x80007bd8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000110c]:feq.d t6, ft11, ft10<br> [0x80001110]:csrrs a7, fflags, zero<br> [0x80001114]:sw t6, 288(a5)<br>   |
| 169|[0x80007be8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001124]:feq.d t6, ft11, ft10<br> [0x80001128]:csrrs a7, fflags, zero<br> [0x8000112c]:sw t6, 304(a5)<br>   |
| 170|[0x80007bf8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000113c]:feq.d t6, ft11, ft10<br> [0x80001140]:csrrs a7, fflags, zero<br> [0x80001144]:sw t6, 320(a5)<br>   |
| 171|[0x80007c08]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001154]:feq.d t6, ft11, ft10<br> [0x80001158]:csrrs a7, fflags, zero<br> [0x8000115c]:sw t6, 336(a5)<br>   |
| 172|[0x80007c18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000116c]:feq.d t6, ft11, ft10<br> [0x80001170]:csrrs a7, fflags, zero<br> [0x80001174]:sw t6, 352(a5)<br>   |
| 173|[0x80007c28]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001184]:feq.d t6, ft11, ft10<br> [0x80001188]:csrrs a7, fflags, zero<br> [0x8000118c]:sw t6, 368(a5)<br>   |
| 174|[0x80007c38]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000119c]:feq.d t6, ft11, ft10<br> [0x800011a0]:csrrs a7, fflags, zero<br> [0x800011a4]:sw t6, 384(a5)<br>   |
| 175|[0x80007c48]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800011b4]:feq.d t6, ft11, ft10<br> [0x800011b8]:csrrs a7, fflags, zero<br> [0x800011bc]:sw t6, 400(a5)<br>   |
| 176|[0x80007c58]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800011cc]:feq.d t6, ft11, ft10<br> [0x800011d0]:csrrs a7, fflags, zero<br> [0x800011d4]:sw t6, 416(a5)<br>   |
| 177|[0x80007c68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800011e4]:feq.d t6, ft11, ft10<br> [0x800011e8]:csrrs a7, fflags, zero<br> [0x800011ec]:sw t6, 432(a5)<br>   |
| 178|[0x80007c78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800011fc]:feq.d t6, ft11, ft10<br> [0x80001200]:csrrs a7, fflags, zero<br> [0x80001204]:sw t6, 448(a5)<br>   |
| 179|[0x80007c88]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001214]:feq.d t6, ft11, ft10<br> [0x80001218]:csrrs a7, fflags, zero<br> [0x8000121c]:sw t6, 464(a5)<br>   |
| 180|[0x80007c98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000122c]:feq.d t6, ft11, ft10<br> [0x80001230]:csrrs a7, fflags, zero<br> [0x80001234]:sw t6, 480(a5)<br>   |
| 181|[0x80007ca8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001244]:feq.d t6, ft11, ft10<br> [0x80001248]:csrrs a7, fflags, zero<br> [0x8000124c]:sw t6, 496(a5)<br>   |
| 182|[0x80007cb8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000125c]:feq.d t6, ft11, ft10<br> [0x80001260]:csrrs a7, fflags, zero<br> [0x80001264]:sw t6, 512(a5)<br>   |
| 183|[0x80007cc8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001274]:feq.d t6, ft11, ft10<br> [0x80001278]:csrrs a7, fflags, zero<br> [0x8000127c]:sw t6, 528(a5)<br>   |
| 184|[0x80007cd8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000128c]:feq.d t6, ft11, ft10<br> [0x80001290]:csrrs a7, fflags, zero<br> [0x80001294]:sw t6, 544(a5)<br>   |
| 185|[0x80007ce8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800012a4]:feq.d t6, ft11, ft10<br> [0x800012a8]:csrrs a7, fflags, zero<br> [0x800012ac]:sw t6, 560(a5)<br>   |
| 186|[0x80007cf8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800012bc]:feq.d t6, ft11, ft10<br> [0x800012c0]:csrrs a7, fflags, zero<br> [0x800012c4]:sw t6, 576(a5)<br>   |
| 187|[0x80007d08]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800012d4]:feq.d t6, ft11, ft10<br> [0x800012d8]:csrrs a7, fflags, zero<br> [0x800012dc]:sw t6, 592(a5)<br>   |
| 188|[0x80007d18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800012ec]:feq.d t6, ft11, ft10<br> [0x800012f0]:csrrs a7, fflags, zero<br> [0x800012f4]:sw t6, 608(a5)<br>   |
| 189|[0x80007d28]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001304]:feq.d t6, ft11, ft10<br> [0x80001308]:csrrs a7, fflags, zero<br> [0x8000130c]:sw t6, 624(a5)<br>   |
| 190|[0x80007d38]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000131c]:feq.d t6, ft11, ft10<br> [0x80001320]:csrrs a7, fflags, zero<br> [0x80001324]:sw t6, 640(a5)<br>   |
| 191|[0x80007d48]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001334]:feq.d t6, ft11, ft10<br> [0x80001338]:csrrs a7, fflags, zero<br> [0x8000133c]:sw t6, 656(a5)<br>   |
| 192|[0x80007d58]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000134c]:feq.d t6, ft11, ft10<br> [0x80001350]:csrrs a7, fflags, zero<br> [0x80001354]:sw t6, 672(a5)<br>   |
| 193|[0x80007d68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001364]:feq.d t6, ft11, ft10<br> [0x80001368]:csrrs a7, fflags, zero<br> [0x8000136c]:sw t6, 688(a5)<br>   |
| 194|[0x80007d78]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000137c]:feq.d t6, ft11, ft10<br> [0x80001380]:csrrs a7, fflags, zero<br> [0x80001384]:sw t6, 704(a5)<br>   |
| 195|[0x80007d88]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001394]:feq.d t6, ft11, ft10<br> [0x80001398]:csrrs a7, fflags, zero<br> [0x8000139c]:sw t6, 720(a5)<br>   |
| 196|[0x80007d98]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800013ac]:feq.d t6, ft11, ft10<br> [0x800013b0]:csrrs a7, fflags, zero<br> [0x800013b4]:sw t6, 736(a5)<br>   |
| 197|[0x80007da8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800013c4]:feq.d t6, ft11, ft10<br> [0x800013c8]:csrrs a7, fflags, zero<br> [0x800013cc]:sw t6, 752(a5)<br>   |
| 198|[0x80007db8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800013dc]:feq.d t6, ft11, ft10<br> [0x800013e0]:csrrs a7, fflags, zero<br> [0x800013e4]:sw t6, 768(a5)<br>   |
| 199|[0x80007dc8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800013f4]:feq.d t6, ft11, ft10<br> [0x800013f8]:csrrs a7, fflags, zero<br> [0x800013fc]:sw t6, 784(a5)<br>   |
| 200|[0x80007dd8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000140c]:feq.d t6, ft11, ft10<br> [0x80001410]:csrrs a7, fflags, zero<br> [0x80001414]:sw t6, 800(a5)<br>   |
| 201|[0x80007de8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001424]:feq.d t6, ft11, ft10<br> [0x80001428]:csrrs a7, fflags, zero<br> [0x8000142c]:sw t6, 816(a5)<br>   |
| 202|[0x80007df8]<br>0x00000001|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000143c]:feq.d t6, ft11, ft10<br> [0x80001440]:csrrs a7, fflags, zero<br> [0x80001444]:sw t6, 832(a5)<br>   |
| 203|[0x80007e08]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001454]:feq.d t6, ft11, ft10<br> [0x80001458]:csrrs a7, fflags, zero<br> [0x8000145c]:sw t6, 848(a5)<br>   |
| 204|[0x80007e18]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000146c]:feq.d t6, ft11, ft10<br> [0x80001470]:csrrs a7, fflags, zero<br> [0x80001474]:sw t6, 864(a5)<br>   |
| 205|[0x80007e28]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001484]:feq.d t6, ft11, ft10<br> [0x80001488]:csrrs a7, fflags, zero<br> [0x8000148c]:sw t6, 880(a5)<br>   |
| 206|[0x80007e38]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000149c]:feq.d t6, ft11, ft10<br> [0x800014a0]:csrrs a7, fflags, zero<br> [0x800014a4]:sw t6, 896(a5)<br>   |
| 207|[0x80007e48]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800014b4]:feq.d t6, ft11, ft10<br> [0x800014b8]:csrrs a7, fflags, zero<br> [0x800014bc]:sw t6, 912(a5)<br>   |
| 208|[0x80007e58]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800014cc]:feq.d t6, ft11, ft10<br> [0x800014d0]:csrrs a7, fflags, zero<br> [0x800014d4]:sw t6, 928(a5)<br>   |
| 209|[0x80007e68]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800014e4]:feq.d t6, ft11, ft10<br> [0x800014e8]:csrrs a7, fflags, zero<br> [0x800014ec]:sw t6, 944(a5)<br>   |
| 210|[0x80007e78]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800014fc]:feq.d t6, ft11, ft10<br> [0x80001500]:csrrs a7, fflags, zero<br> [0x80001504]:sw t6, 960(a5)<br>   |
| 211|[0x80007e88]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001514]:feq.d t6, ft11, ft10<br> [0x80001518]:csrrs a7, fflags, zero<br> [0x8000151c]:sw t6, 976(a5)<br>   |
| 212|[0x80007e98]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000152c]:feq.d t6, ft11, ft10<br> [0x80001530]:csrrs a7, fflags, zero<br> [0x80001534]:sw t6, 992(a5)<br>   |
| 213|[0x80007ea8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001544]:feq.d t6, ft11, ft10<br> [0x80001548]:csrrs a7, fflags, zero<br> [0x8000154c]:sw t6, 1008(a5)<br>  |
| 214|[0x80007eb8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000155c]:feq.d t6, ft11, ft10<br> [0x80001560]:csrrs a7, fflags, zero<br> [0x80001564]:sw t6, 1024(a5)<br>  |
| 215|[0x80007ec8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001574]:feq.d t6, ft11, ft10<br> [0x80001578]:csrrs a7, fflags, zero<br> [0x8000157c]:sw t6, 1040(a5)<br>  |
| 216|[0x80007ed8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000158c]:feq.d t6, ft11, ft10<br> [0x80001590]:csrrs a7, fflags, zero<br> [0x80001594]:sw t6, 1056(a5)<br>  |
| 217|[0x80007ee8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800015a4]:feq.d t6, ft11, ft10<br> [0x800015a8]:csrrs a7, fflags, zero<br> [0x800015ac]:sw t6, 1072(a5)<br>  |
| 218|[0x80007ef8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800015bc]:feq.d t6, ft11, ft10<br> [0x800015c0]:csrrs a7, fflags, zero<br> [0x800015c4]:sw t6, 1088(a5)<br>  |
| 219|[0x80007f08]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800015d4]:feq.d t6, ft11, ft10<br> [0x800015d8]:csrrs a7, fflags, zero<br> [0x800015dc]:sw t6, 1104(a5)<br>  |
| 220|[0x80007f18]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800015ec]:feq.d t6, ft11, ft10<br> [0x800015f0]:csrrs a7, fflags, zero<br> [0x800015f4]:sw t6, 1120(a5)<br>  |
| 221|[0x80007f28]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001604]:feq.d t6, ft11, ft10<br> [0x80001608]:csrrs a7, fflags, zero<br> [0x8000160c]:sw t6, 1136(a5)<br>  |
| 222|[0x80007f38]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000161c]:feq.d t6, ft11, ft10<br> [0x80001620]:csrrs a7, fflags, zero<br> [0x80001624]:sw t6, 1152(a5)<br>  |
| 223|[0x80007f48]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001634]:feq.d t6, ft11, ft10<br> [0x80001638]:csrrs a7, fflags, zero<br> [0x8000163c]:sw t6, 1168(a5)<br>  |
| 224|[0x80007f58]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000164c]:feq.d t6, ft11, ft10<br> [0x80001650]:csrrs a7, fflags, zero<br> [0x80001654]:sw t6, 1184(a5)<br>  |
| 225|[0x80007f68]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001664]:feq.d t6, ft11, ft10<br> [0x80001668]:csrrs a7, fflags, zero<br> [0x8000166c]:sw t6, 1200(a5)<br>  |
| 226|[0x80007f78]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000167c]:feq.d t6, ft11, ft10<br> [0x80001680]:csrrs a7, fflags, zero<br> [0x80001684]:sw t6, 1216(a5)<br>  |
| 227|[0x80007f88]<br>0x00000001|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001694]:feq.d t6, ft11, ft10<br> [0x80001698]:csrrs a7, fflags, zero<br> [0x8000169c]:sw t6, 1232(a5)<br>  |
| 228|[0x80007f98]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800016ac]:feq.d t6, ft11, ft10<br> [0x800016b0]:csrrs a7, fflags, zero<br> [0x800016b4]:sw t6, 1248(a5)<br>  |
| 229|[0x80007fa8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800016c4]:feq.d t6, ft11, ft10<br> [0x800016c8]:csrrs a7, fflags, zero<br> [0x800016cc]:sw t6, 1264(a5)<br>  |
| 230|[0x80007fb8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800016dc]:feq.d t6, ft11, ft10<br> [0x800016e0]:csrrs a7, fflags, zero<br> [0x800016e4]:sw t6, 1280(a5)<br>  |
| 231|[0x80007fc8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800016f4]:feq.d t6, ft11, ft10<br> [0x800016f8]:csrrs a7, fflags, zero<br> [0x800016fc]:sw t6, 1296(a5)<br>  |
| 232|[0x80007fd8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000170c]:feq.d t6, ft11, ft10<br> [0x80001710]:csrrs a7, fflags, zero<br> [0x80001714]:sw t6, 1312(a5)<br>  |
| 233|[0x80007fe8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001724]:feq.d t6, ft11, ft10<br> [0x80001728]:csrrs a7, fflags, zero<br> [0x8000172c]:sw t6, 1328(a5)<br>  |
| 234|[0x80007ff8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000173c]:feq.d t6, ft11, ft10<br> [0x80001740]:csrrs a7, fflags, zero<br> [0x80001744]:sw t6, 1344(a5)<br>  |
| 235|[0x80008008]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001754]:feq.d t6, ft11, ft10<br> [0x80001758]:csrrs a7, fflags, zero<br> [0x8000175c]:sw t6, 1360(a5)<br>  |
| 236|[0x80008018]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000176c]:feq.d t6, ft11, ft10<br> [0x80001770]:csrrs a7, fflags, zero<br> [0x80001774]:sw t6, 1376(a5)<br>  |
| 237|[0x80008028]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001784]:feq.d t6, ft11, ft10<br> [0x80001788]:csrrs a7, fflags, zero<br> [0x8000178c]:sw t6, 1392(a5)<br>  |
| 238|[0x80008038]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000179c]:feq.d t6, ft11, ft10<br> [0x800017a0]:csrrs a7, fflags, zero<br> [0x800017a4]:sw t6, 1408(a5)<br>  |
| 239|[0x80008048]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800017b4]:feq.d t6, ft11, ft10<br> [0x800017b8]:csrrs a7, fflags, zero<br> [0x800017bc]:sw t6, 1424(a5)<br>  |
| 240|[0x80008058]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800017cc]:feq.d t6, ft11, ft10<br> [0x800017d0]:csrrs a7, fflags, zero<br> [0x800017d4]:sw t6, 1440(a5)<br>  |
| 241|[0x80008068]<br>0x00000000|- fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800017e4]:feq.d t6, ft11, ft10<br> [0x800017e8]:csrrs a7, fflags, zero<br> [0x800017ec]:sw t6, 1456(a5)<br>  |
| 242|[0x80008078]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800017fc]:feq.d t6, ft11, ft10<br> [0x80001800]:csrrs a7, fflags, zero<br> [0x80001804]:sw t6, 1472(a5)<br>  |
| 243|[0x80008088]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001814]:feq.d t6, ft11, ft10<br> [0x80001818]:csrrs a7, fflags, zero<br> [0x8000181c]:sw t6, 1488(a5)<br>  |
| 244|[0x80008098]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000182c]:feq.d t6, ft11, ft10<br> [0x80001830]:csrrs a7, fflags, zero<br> [0x80001834]:sw t6, 1504(a5)<br>  |
| 245|[0x800080a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001844]:feq.d t6, ft11, ft10<br> [0x80001848]:csrrs a7, fflags, zero<br> [0x8000184c]:sw t6, 1520(a5)<br>  |
| 246|[0x800080b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000185c]:feq.d t6, ft11, ft10<br> [0x80001860]:csrrs a7, fflags, zero<br> [0x80001864]:sw t6, 1536(a5)<br>  |
| 247|[0x800080c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001874]:feq.d t6, ft11, ft10<br> [0x80001878]:csrrs a7, fflags, zero<br> [0x8000187c]:sw t6, 1552(a5)<br>  |
| 248|[0x800080d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000188c]:feq.d t6, ft11, ft10<br> [0x80001890]:csrrs a7, fflags, zero<br> [0x80001894]:sw t6, 1568(a5)<br>  |
| 249|[0x800080e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800018a4]:feq.d t6, ft11, ft10<br> [0x800018a8]:csrrs a7, fflags, zero<br> [0x800018ac]:sw t6, 1584(a5)<br>  |
| 250|[0x800080f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800018bc]:feq.d t6, ft11, ft10<br> [0x800018c0]:csrrs a7, fflags, zero<br> [0x800018c4]:sw t6, 1600(a5)<br>  |
| 251|[0x80008108]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800018d4]:feq.d t6, ft11, ft10<br> [0x800018d8]:csrrs a7, fflags, zero<br> [0x800018dc]:sw t6, 1616(a5)<br>  |
| 252|[0x80008118]<br>0x00000001|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800018ec]:feq.d t6, ft11, ft10<br> [0x800018f0]:csrrs a7, fflags, zero<br> [0x800018f4]:sw t6, 1632(a5)<br>  |
| 253|[0x80008128]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001904]:feq.d t6, ft11, ft10<br> [0x80001908]:csrrs a7, fflags, zero<br> [0x8000190c]:sw t6, 1648(a5)<br>  |
| 254|[0x80008138]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000191c]:feq.d t6, ft11, ft10<br> [0x80001920]:csrrs a7, fflags, zero<br> [0x80001924]:sw t6, 1664(a5)<br>  |
| 255|[0x80008148]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001938]:feq.d t6, ft11, ft10<br> [0x8000193c]:csrrs a7, fflags, zero<br> [0x80001940]:sw t6, 1680(a5)<br>  |
| 256|[0x80008158]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001950]:feq.d t6, ft11, ft10<br> [0x80001954]:csrrs a7, fflags, zero<br> [0x80001958]:sw t6, 1696(a5)<br>  |
| 257|[0x80008168]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001968]:feq.d t6, ft11, ft10<br> [0x8000196c]:csrrs a7, fflags, zero<br> [0x80001970]:sw t6, 1712(a5)<br>  |
| 258|[0x80008178]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001980]:feq.d t6, ft11, ft10<br> [0x80001984]:csrrs a7, fflags, zero<br> [0x80001988]:sw t6, 1728(a5)<br>  |
| 259|[0x80008188]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001998]:feq.d t6, ft11, ft10<br> [0x8000199c]:csrrs a7, fflags, zero<br> [0x800019a0]:sw t6, 1744(a5)<br>  |
| 260|[0x80008198]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800019b0]:feq.d t6, ft11, ft10<br> [0x800019b4]:csrrs a7, fflags, zero<br> [0x800019b8]:sw t6, 1760(a5)<br>  |
| 261|[0x800081a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800019c8]:feq.d t6, ft11, ft10<br> [0x800019cc]:csrrs a7, fflags, zero<br> [0x800019d0]:sw t6, 1776(a5)<br>  |
| 262|[0x800081b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800019e0]:feq.d t6, ft11, ft10<br> [0x800019e4]:csrrs a7, fflags, zero<br> [0x800019e8]:sw t6, 1792(a5)<br>  |
| 263|[0x800081c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800019f8]:feq.d t6, ft11, ft10<br> [0x800019fc]:csrrs a7, fflags, zero<br> [0x80001a00]:sw t6, 1808(a5)<br>  |
| 264|[0x800081d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a10]:feq.d t6, ft11, ft10<br> [0x80001a14]:csrrs a7, fflags, zero<br> [0x80001a18]:sw t6, 1824(a5)<br>  |
| 265|[0x800081e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a28]:feq.d t6, ft11, ft10<br> [0x80001a2c]:csrrs a7, fflags, zero<br> [0x80001a30]:sw t6, 1840(a5)<br>  |
| 266|[0x800081f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a40]:feq.d t6, ft11, ft10<br> [0x80001a44]:csrrs a7, fflags, zero<br> [0x80001a48]:sw t6, 1856(a5)<br>  |
| 267|[0x80008208]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a58]:feq.d t6, ft11, ft10<br> [0x80001a5c]:csrrs a7, fflags, zero<br> [0x80001a60]:sw t6, 1872(a5)<br>  |
| 268|[0x80008218]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a70]:feq.d t6, ft11, ft10<br> [0x80001a74]:csrrs a7, fflags, zero<br> [0x80001a78]:sw t6, 1888(a5)<br>  |
| 269|[0x80008228]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001a88]:feq.d t6, ft11, ft10<br> [0x80001a8c]:csrrs a7, fflags, zero<br> [0x80001a90]:sw t6, 1904(a5)<br>  |
| 270|[0x80008238]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001aa0]:feq.d t6, ft11, ft10<br> [0x80001aa4]:csrrs a7, fflags, zero<br> [0x80001aa8]:sw t6, 1920(a5)<br>  |
| 271|[0x80008248]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ab8]:feq.d t6, ft11, ft10<br> [0x80001abc]:csrrs a7, fflags, zero<br> [0x80001ac0]:sw t6, 1936(a5)<br>  |
| 272|[0x80008258]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ad0]:feq.d t6, ft11, ft10<br> [0x80001ad4]:csrrs a7, fflags, zero<br> [0x80001ad8]:sw t6, 1952(a5)<br>  |
| 273|[0x80008268]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ae8]:feq.d t6, ft11, ft10<br> [0x80001aec]:csrrs a7, fflags, zero<br> [0x80001af0]:sw t6, 1968(a5)<br>  |
| 274|[0x80008278]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001b00]:feq.d t6, ft11, ft10<br> [0x80001b04]:csrrs a7, fflags, zero<br> [0x80001b08]:sw t6, 1984(a5)<br>  |
| 275|[0x80008288]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001b18]:feq.d t6, ft11, ft10<br> [0x80001b1c]:csrrs a7, fflags, zero<br> [0x80001b20]:sw t6, 2000(a5)<br>  |
| 276|[0x80008298]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001b30]:feq.d t6, ft11, ft10<br> [0x80001b34]:csrrs a7, fflags, zero<br> [0x80001b38]:sw t6, 2016(a5)<br>  |
| 277|[0x80007eb0]<br>0x00000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001b50]:feq.d t6, ft11, ft10<br> [0x80001b54]:csrrs a7, fflags, zero<br> [0x80001b58]:sw t6, 0(a5)<br>     |
| 278|[0x80007ec0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001b68]:feq.d t6, ft11, ft10<br> [0x80001b6c]:csrrs a7, fflags, zero<br> [0x80001b70]:sw t6, 16(a5)<br>    |
| 279|[0x80007ed0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001b80]:feq.d t6, ft11, ft10<br> [0x80001b84]:csrrs a7, fflags, zero<br> [0x80001b88]:sw t6, 32(a5)<br>    |
| 280|[0x80007ee0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001b98]:feq.d t6, ft11, ft10<br> [0x80001b9c]:csrrs a7, fflags, zero<br> [0x80001ba0]:sw t6, 48(a5)<br>    |
| 281|[0x80007ef0]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001bb0]:feq.d t6, ft11, ft10<br> [0x80001bb4]:csrrs a7, fflags, zero<br> [0x80001bb8]:sw t6, 64(a5)<br>    |
| 282|[0x80007f00]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001bc8]:feq.d t6, ft11, ft10<br> [0x80001bcc]:csrrs a7, fflags, zero<br> [0x80001bd0]:sw t6, 80(a5)<br>    |
| 283|[0x80007f10]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001be0]:feq.d t6, ft11, ft10<br> [0x80001be4]:csrrs a7, fflags, zero<br> [0x80001be8]:sw t6, 96(a5)<br>    |
| 284|[0x80007f20]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001bf8]:feq.d t6, ft11, ft10<br> [0x80001bfc]:csrrs a7, fflags, zero<br> [0x80001c00]:sw t6, 112(a5)<br>   |
| 285|[0x80007f30]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c10]:feq.d t6, ft11, ft10<br> [0x80001c14]:csrrs a7, fflags, zero<br> [0x80001c18]:sw t6, 128(a5)<br>   |
| 286|[0x80007f40]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c28]:feq.d t6, ft11, ft10<br> [0x80001c2c]:csrrs a7, fflags, zero<br> [0x80001c30]:sw t6, 144(a5)<br>   |
| 287|[0x80007f50]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c40]:feq.d t6, ft11, ft10<br> [0x80001c44]:csrrs a7, fflags, zero<br> [0x80001c48]:sw t6, 160(a5)<br>   |
| 288|[0x80007f60]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c58]:feq.d t6, ft11, ft10<br> [0x80001c5c]:csrrs a7, fflags, zero<br> [0x80001c60]:sw t6, 176(a5)<br>   |
| 289|[0x80007f70]<br>0x00000000|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c70]:feq.d t6, ft11, ft10<br> [0x80001c74]:csrrs a7, fflags, zero<br> [0x80001c78]:sw t6, 192(a5)<br>   |
| 290|[0x80007f80]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001c88]:feq.d t6, ft11, ft10<br> [0x80001c8c]:csrrs a7, fflags, zero<br> [0x80001c90]:sw t6, 208(a5)<br>   |
| 291|[0x80007f90]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ca0]:feq.d t6, ft11, ft10<br> [0x80001ca4]:csrrs a7, fflags, zero<br> [0x80001ca8]:sw t6, 224(a5)<br>   |
| 292|[0x80007fa0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001cb8]:feq.d t6, ft11, ft10<br> [0x80001cbc]:csrrs a7, fflags, zero<br> [0x80001cc0]:sw t6, 240(a5)<br>   |
| 293|[0x80007fb0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001cd0]:feq.d t6, ft11, ft10<br> [0x80001cd4]:csrrs a7, fflags, zero<br> [0x80001cd8]:sw t6, 256(a5)<br>   |
| 294|[0x80007fc0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ce8]:feq.d t6, ft11, ft10<br> [0x80001cec]:csrrs a7, fflags, zero<br> [0x80001cf0]:sw t6, 272(a5)<br>   |
| 295|[0x80007fd0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001d00]:feq.d t6, ft11, ft10<br> [0x80001d04]:csrrs a7, fflags, zero<br> [0x80001d08]:sw t6, 288(a5)<br>   |
| 296|[0x80007fe0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001d18]:feq.d t6, ft11, ft10<br> [0x80001d1c]:csrrs a7, fflags, zero<br> [0x80001d20]:sw t6, 304(a5)<br>   |
| 297|[0x80007ff0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001d30]:feq.d t6, ft11, ft10<br> [0x80001d34]:csrrs a7, fflags, zero<br> [0x80001d38]:sw t6, 320(a5)<br>   |
| 298|[0x80008000]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001d48]:feq.d t6, ft11, ft10<br> [0x80001d4c]:csrrs a7, fflags, zero<br> [0x80001d50]:sw t6, 336(a5)<br>   |
| 299|[0x80008010]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001d60]:feq.d t6, ft11, ft10<br> [0x80001d64]:csrrs a7, fflags, zero<br> [0x80001d68]:sw t6, 352(a5)<br>   |
| 300|[0x80008020]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001d78]:feq.d t6, ft11, ft10<br> [0x80001d7c]:csrrs a7, fflags, zero<br> [0x80001d80]:sw t6, 368(a5)<br>   |
| 301|[0x80008030]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001d90]:feq.d t6, ft11, ft10<br> [0x80001d94]:csrrs a7, fflags, zero<br> [0x80001d98]:sw t6, 384(a5)<br>   |
| 302|[0x80008040]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001da8]:feq.d t6, ft11, ft10<br> [0x80001dac]:csrrs a7, fflags, zero<br> [0x80001db0]:sw t6, 400(a5)<br>   |
| 303|[0x80008050]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001dc0]:feq.d t6, ft11, ft10<br> [0x80001dc4]:csrrs a7, fflags, zero<br> [0x80001dc8]:sw t6, 416(a5)<br>   |
| 304|[0x80008060]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001dd8]:feq.d t6, ft11, ft10<br> [0x80001ddc]:csrrs a7, fflags, zero<br> [0x80001de0]:sw t6, 432(a5)<br>   |
| 305|[0x80008070]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001df0]:feq.d t6, ft11, ft10<br> [0x80001df4]:csrrs a7, fflags, zero<br> [0x80001df8]:sw t6, 448(a5)<br>   |
| 306|[0x80008080]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001e08]:feq.d t6, ft11, ft10<br> [0x80001e0c]:csrrs a7, fflags, zero<br> [0x80001e10]:sw t6, 464(a5)<br>   |
| 307|[0x80008090]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001e20]:feq.d t6, ft11, ft10<br> [0x80001e24]:csrrs a7, fflags, zero<br> [0x80001e28]:sw t6, 480(a5)<br>   |
| 308|[0x800080a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001e38]:feq.d t6, ft11, ft10<br> [0x80001e3c]:csrrs a7, fflags, zero<br> [0x80001e40]:sw t6, 496(a5)<br>   |
| 309|[0x800080b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001e50]:feq.d t6, ft11, ft10<br> [0x80001e54]:csrrs a7, fflags, zero<br> [0x80001e58]:sw t6, 512(a5)<br>   |
| 310|[0x800080c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001e68]:feq.d t6, ft11, ft10<br> [0x80001e6c]:csrrs a7, fflags, zero<br> [0x80001e70]:sw t6, 528(a5)<br>   |
| 311|[0x800080d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001e80]:feq.d t6, ft11, ft10<br> [0x80001e84]:csrrs a7, fflags, zero<br> [0x80001e88]:sw t6, 544(a5)<br>   |
| 312|[0x800080e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001e98]:feq.d t6, ft11, ft10<br> [0x80001e9c]:csrrs a7, fflags, zero<br> [0x80001ea0]:sw t6, 560(a5)<br>   |
| 313|[0x800080f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001eb0]:feq.d t6, ft11, ft10<br> [0x80001eb4]:csrrs a7, fflags, zero<br> [0x80001eb8]:sw t6, 576(a5)<br>   |
| 314|[0x80008100]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ec8]:feq.d t6, ft11, ft10<br> [0x80001ecc]:csrrs a7, fflags, zero<br> [0x80001ed0]:sw t6, 592(a5)<br>   |
| 315|[0x80008110]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ee0]:feq.d t6, ft11, ft10<br> [0x80001ee4]:csrrs a7, fflags, zero<br> [0x80001ee8]:sw t6, 608(a5)<br>   |
| 316|[0x80008120]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001ef8]:feq.d t6, ft11, ft10<br> [0x80001efc]:csrrs a7, fflags, zero<br> [0x80001f00]:sw t6, 624(a5)<br>   |
| 317|[0x80008130]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f10]:feq.d t6, ft11, ft10<br> [0x80001f14]:csrrs a7, fflags, zero<br> [0x80001f18]:sw t6, 640(a5)<br>   |
| 318|[0x80008140]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f28]:feq.d t6, ft11, ft10<br> [0x80001f2c]:csrrs a7, fflags, zero<br> [0x80001f30]:sw t6, 656(a5)<br>   |
| 319|[0x80008150]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f40]:feq.d t6, ft11, ft10<br> [0x80001f44]:csrrs a7, fflags, zero<br> [0x80001f48]:sw t6, 672(a5)<br>   |
| 320|[0x80008160]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f58]:feq.d t6, ft11, ft10<br> [0x80001f5c]:csrrs a7, fflags, zero<br> [0x80001f60]:sw t6, 688(a5)<br>   |
| 321|[0x80008170]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f70]:feq.d t6, ft11, ft10<br> [0x80001f74]:csrrs a7, fflags, zero<br> [0x80001f78]:sw t6, 704(a5)<br>   |
| 322|[0x80008180]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001f88]:feq.d t6, ft11, ft10<br> [0x80001f8c]:csrrs a7, fflags, zero<br> [0x80001f90]:sw t6, 720(a5)<br>   |
| 323|[0x80008190]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80001fa0]:feq.d t6, ft11, ft10<br> [0x80001fa4]:csrrs a7, fflags, zero<br> [0x80001fa8]:sw t6, 736(a5)<br>   |
| 324|[0x800081a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001fb8]:feq.d t6, ft11, ft10<br> [0x80001fbc]:csrrs a7, fflags, zero<br> [0x80001fc0]:sw t6, 752(a5)<br>   |
| 325|[0x800081b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80001fd0]:feq.d t6, ft11, ft10<br> [0x80001fd4]:csrrs a7, fflags, zero<br> [0x80001fd8]:sw t6, 768(a5)<br>   |
| 326|[0x800081c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80001fe8]:feq.d t6, ft11, ft10<br> [0x80001fec]:csrrs a7, fflags, zero<br> [0x80001ff0]:sw t6, 784(a5)<br>   |
| 327|[0x800081d0]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002000]:feq.d t6, ft11, ft10<br> [0x80002004]:csrrs a7, fflags, zero<br> [0x80002008]:sw t6, 800(a5)<br>   |
| 328|[0x800081e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002018]:feq.d t6, ft11, ft10<br> [0x8000201c]:csrrs a7, fflags, zero<br> [0x80002020]:sw t6, 816(a5)<br>   |
| 329|[0x800081f0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002030]:feq.d t6, ft11, ft10<br> [0x80002034]:csrrs a7, fflags, zero<br> [0x80002038]:sw t6, 832(a5)<br>   |
| 330|[0x80008200]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002048]:feq.d t6, ft11, ft10<br> [0x8000204c]:csrrs a7, fflags, zero<br> [0x80002050]:sw t6, 848(a5)<br>   |
| 331|[0x80008210]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002060]:feq.d t6, ft11, ft10<br> [0x80002064]:csrrs a7, fflags, zero<br> [0x80002068]:sw t6, 864(a5)<br>   |
| 332|[0x80008220]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002078]:feq.d t6, ft11, ft10<br> [0x8000207c]:csrrs a7, fflags, zero<br> [0x80002080]:sw t6, 880(a5)<br>   |
| 333|[0x80008230]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002090]:feq.d t6, ft11, ft10<br> [0x80002094]:csrrs a7, fflags, zero<br> [0x80002098]:sw t6, 896(a5)<br>   |
| 334|[0x80008240]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800020a8]:feq.d t6, ft11, ft10<br> [0x800020ac]:csrrs a7, fflags, zero<br> [0x800020b0]:sw t6, 912(a5)<br>   |
| 335|[0x80008250]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800020c0]:feq.d t6, ft11, ft10<br> [0x800020c4]:csrrs a7, fflags, zero<br> [0x800020c8]:sw t6, 928(a5)<br>   |
| 336|[0x80008260]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800020d8]:feq.d t6, ft11, ft10<br> [0x800020dc]:csrrs a7, fflags, zero<br> [0x800020e0]:sw t6, 944(a5)<br>   |
| 337|[0x80008270]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800020f0]:feq.d t6, ft11, ft10<br> [0x800020f4]:csrrs a7, fflags, zero<br> [0x800020f8]:sw t6, 960(a5)<br>   |
| 338|[0x80008280]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002108]:feq.d t6, ft11, ft10<br> [0x8000210c]:csrrs a7, fflags, zero<br> [0x80002110]:sw t6, 976(a5)<br>   |
| 339|[0x80008290]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002120]:feq.d t6, ft11, ft10<br> [0x80002124]:csrrs a7, fflags, zero<br> [0x80002128]:sw t6, 992(a5)<br>   |
| 340|[0x800082a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002138]:feq.d t6, ft11, ft10<br> [0x8000213c]:csrrs a7, fflags, zero<br> [0x80002140]:sw t6, 1008(a5)<br>  |
| 341|[0x800082b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002150]:feq.d t6, ft11, ft10<br> [0x80002154]:csrrs a7, fflags, zero<br> [0x80002158]:sw t6, 1024(a5)<br>  |
| 342|[0x800082c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002168]:feq.d t6, ft11, ft10<br> [0x8000216c]:csrrs a7, fflags, zero<br> [0x80002170]:sw t6, 1040(a5)<br>  |
| 343|[0x800082d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002180]:feq.d t6, ft11, ft10<br> [0x80002184]:csrrs a7, fflags, zero<br> [0x80002188]:sw t6, 1056(a5)<br>  |
| 344|[0x800082e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002198]:feq.d t6, ft11, ft10<br> [0x8000219c]:csrrs a7, fflags, zero<br> [0x800021a0]:sw t6, 1072(a5)<br>  |
| 345|[0x800082f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800021b0]:feq.d t6, ft11, ft10<br> [0x800021b4]:csrrs a7, fflags, zero<br> [0x800021b8]:sw t6, 1088(a5)<br>  |
| 346|[0x80008300]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800021c8]:feq.d t6, ft11, ft10<br> [0x800021cc]:csrrs a7, fflags, zero<br> [0x800021d0]:sw t6, 1104(a5)<br>  |
| 347|[0x80008310]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800021e0]:feq.d t6, ft11, ft10<br> [0x800021e4]:csrrs a7, fflags, zero<br> [0x800021e8]:sw t6, 1120(a5)<br>  |
| 348|[0x80008320]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800021f8]:feq.d t6, ft11, ft10<br> [0x800021fc]:csrrs a7, fflags, zero<br> [0x80002200]:sw t6, 1136(a5)<br>  |
| 349|[0x80008330]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002210]:feq.d t6, ft11, ft10<br> [0x80002214]:csrrs a7, fflags, zero<br> [0x80002218]:sw t6, 1152(a5)<br>  |
| 350|[0x80008340]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002228]:feq.d t6, ft11, ft10<br> [0x8000222c]:csrrs a7, fflags, zero<br> [0x80002230]:sw t6, 1168(a5)<br>  |
| 351|[0x80008350]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002240]:feq.d t6, ft11, ft10<br> [0x80002244]:csrrs a7, fflags, zero<br> [0x80002248]:sw t6, 1184(a5)<br>  |
| 352|[0x80008360]<br>0x00000001|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002258]:feq.d t6, ft11, ft10<br> [0x8000225c]:csrrs a7, fflags, zero<br> [0x80002260]:sw t6, 1200(a5)<br>  |
| 353|[0x80008370]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002270]:feq.d t6, ft11, ft10<br> [0x80002274]:csrrs a7, fflags, zero<br> [0x80002278]:sw t6, 1216(a5)<br>  |
| 354|[0x80008380]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002288]:feq.d t6, ft11, ft10<br> [0x8000228c]:csrrs a7, fflags, zero<br> [0x80002290]:sw t6, 1232(a5)<br>  |
| 355|[0x80008390]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800022a0]:feq.d t6, ft11, ft10<br> [0x800022a4]:csrrs a7, fflags, zero<br> [0x800022a8]:sw t6, 1248(a5)<br>  |
| 356|[0x800083a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800022b8]:feq.d t6, ft11, ft10<br> [0x800022bc]:csrrs a7, fflags, zero<br> [0x800022c0]:sw t6, 1264(a5)<br>  |
| 357|[0x800083b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800022d0]:feq.d t6, ft11, ft10<br> [0x800022d4]:csrrs a7, fflags, zero<br> [0x800022d8]:sw t6, 1280(a5)<br>  |
| 358|[0x800083c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800022e8]:feq.d t6, ft11, ft10<br> [0x800022ec]:csrrs a7, fflags, zero<br> [0x800022f0]:sw t6, 1296(a5)<br>  |
| 359|[0x800083d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002300]:feq.d t6, ft11, ft10<br> [0x80002304]:csrrs a7, fflags, zero<br> [0x80002308]:sw t6, 1312(a5)<br>  |
| 360|[0x800083e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002318]:feq.d t6, ft11, ft10<br> [0x8000231c]:csrrs a7, fflags, zero<br> [0x80002320]:sw t6, 1328(a5)<br>  |
| 361|[0x800083f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002330]:feq.d t6, ft11, ft10<br> [0x80002334]:csrrs a7, fflags, zero<br> [0x80002338]:sw t6, 1344(a5)<br>  |
| 362|[0x80008400]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002348]:feq.d t6, ft11, ft10<br> [0x8000234c]:csrrs a7, fflags, zero<br> [0x80002350]:sw t6, 1360(a5)<br>  |
| 363|[0x80008410]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002360]:feq.d t6, ft11, ft10<br> [0x80002364]:csrrs a7, fflags, zero<br> [0x80002368]:sw t6, 1376(a5)<br>  |
| 364|[0x80008420]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002378]:feq.d t6, ft11, ft10<br> [0x8000237c]:csrrs a7, fflags, zero<br> [0x80002380]:sw t6, 1392(a5)<br>  |
| 365|[0x80008430]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002390]:feq.d t6, ft11, ft10<br> [0x80002394]:csrrs a7, fflags, zero<br> [0x80002398]:sw t6, 1408(a5)<br>  |
| 366|[0x80008440]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800023a8]:feq.d t6, ft11, ft10<br> [0x800023ac]:csrrs a7, fflags, zero<br> [0x800023b0]:sw t6, 1424(a5)<br>  |
| 367|[0x80008450]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800023c0]:feq.d t6, ft11, ft10<br> [0x800023c4]:csrrs a7, fflags, zero<br> [0x800023c8]:sw t6, 1440(a5)<br>  |
| 368|[0x80008460]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800023d8]:feq.d t6, ft11, ft10<br> [0x800023dc]:csrrs a7, fflags, zero<br> [0x800023e0]:sw t6, 1456(a5)<br>  |
| 369|[0x80008470]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800023f0]:feq.d t6, ft11, ft10<br> [0x800023f4]:csrrs a7, fflags, zero<br> [0x800023f8]:sw t6, 1472(a5)<br>  |
| 370|[0x80008480]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002408]:feq.d t6, ft11, ft10<br> [0x8000240c]:csrrs a7, fflags, zero<br> [0x80002410]:sw t6, 1488(a5)<br>  |
| 371|[0x80008490]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002420]:feq.d t6, ft11, ft10<br> [0x80002424]:csrrs a7, fflags, zero<br> [0x80002428]:sw t6, 1504(a5)<br>  |
| 372|[0x800084a0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002438]:feq.d t6, ft11, ft10<br> [0x8000243c]:csrrs a7, fflags, zero<br> [0x80002440]:sw t6, 1520(a5)<br>  |
| 373|[0x800084b0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002450]:feq.d t6, ft11, ft10<br> [0x80002454]:csrrs a7, fflags, zero<br> [0x80002458]:sw t6, 1536(a5)<br>  |
| 374|[0x800084c0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002468]:feq.d t6, ft11, ft10<br> [0x8000246c]:csrrs a7, fflags, zero<br> [0x80002470]:sw t6, 1552(a5)<br>  |
| 375|[0x800084d0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002480]:feq.d t6, ft11, ft10<br> [0x80002484]:csrrs a7, fflags, zero<br> [0x80002488]:sw t6, 1568(a5)<br>  |
| 376|[0x800084e0]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002498]:feq.d t6, ft11, ft10<br> [0x8000249c]:csrrs a7, fflags, zero<br> [0x800024a0]:sw t6, 1584(a5)<br>  |
| 377|[0x800084f0]<br>0x00000001|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800024b0]:feq.d t6, ft11, ft10<br> [0x800024b4]:csrrs a7, fflags, zero<br> [0x800024b8]:sw t6, 1600(a5)<br>  |
| 378|[0x80008500]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800024c8]:feq.d t6, ft11, ft10<br> [0x800024cc]:csrrs a7, fflags, zero<br> [0x800024d0]:sw t6, 1616(a5)<br>  |
| 379|[0x80008510]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800024e0]:feq.d t6, ft11, ft10<br> [0x800024e4]:csrrs a7, fflags, zero<br> [0x800024e8]:sw t6, 1632(a5)<br>  |
| 380|[0x80008520]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800024f8]:feq.d t6, ft11, ft10<br> [0x800024fc]:csrrs a7, fflags, zero<br> [0x80002500]:sw t6, 1648(a5)<br>  |
| 381|[0x80008530]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002510]:feq.d t6, ft11, ft10<br> [0x80002514]:csrrs a7, fflags, zero<br> [0x80002518]:sw t6, 1664(a5)<br>  |
| 382|[0x80008540]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000252c]:feq.d t6, ft11, ft10<br> [0x80002530]:csrrs a7, fflags, zero<br> [0x80002534]:sw t6, 1680(a5)<br>  |
| 383|[0x80008550]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002544]:feq.d t6, ft11, ft10<br> [0x80002548]:csrrs a7, fflags, zero<br> [0x8000254c]:sw t6, 1696(a5)<br>  |
| 384|[0x80008560]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000255c]:feq.d t6, ft11, ft10<br> [0x80002560]:csrrs a7, fflags, zero<br> [0x80002564]:sw t6, 1712(a5)<br>  |
| 385|[0x80008570]<br>0x00000000|- fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002574]:feq.d t6, ft11, ft10<br> [0x80002578]:csrrs a7, fflags, zero<br> [0x8000257c]:sw t6, 1728(a5)<br>  |
| 386|[0x80008580]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000258c]:feq.d t6, ft11, ft10<br> [0x80002590]:csrrs a7, fflags, zero<br> [0x80002594]:sw t6, 1744(a5)<br>  |
| 387|[0x80008590]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800025a4]:feq.d t6, ft11, ft10<br> [0x800025a8]:csrrs a7, fflags, zero<br> [0x800025ac]:sw t6, 1760(a5)<br>  |
| 388|[0x800085a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800025bc]:feq.d t6, ft11, ft10<br> [0x800025c0]:csrrs a7, fflags, zero<br> [0x800025c4]:sw t6, 1776(a5)<br>  |
| 389|[0x800085b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800025d4]:feq.d t6, ft11, ft10<br> [0x800025d8]:csrrs a7, fflags, zero<br> [0x800025dc]:sw t6, 1792(a5)<br>  |
| 390|[0x800085c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800025ec]:feq.d t6, ft11, ft10<br> [0x800025f0]:csrrs a7, fflags, zero<br> [0x800025f4]:sw t6, 1808(a5)<br>  |
| 391|[0x800085d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002604]:feq.d t6, ft11, ft10<br> [0x80002608]:csrrs a7, fflags, zero<br> [0x8000260c]:sw t6, 1824(a5)<br>  |
| 392|[0x800085e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000261c]:feq.d t6, ft11, ft10<br> [0x80002620]:csrrs a7, fflags, zero<br> [0x80002624]:sw t6, 1840(a5)<br>  |
| 393|[0x800085f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002634]:feq.d t6, ft11, ft10<br> [0x80002638]:csrrs a7, fflags, zero<br> [0x8000263c]:sw t6, 1856(a5)<br>  |
| 394|[0x80008600]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000264c]:feq.d t6, ft11, ft10<br> [0x80002650]:csrrs a7, fflags, zero<br> [0x80002654]:sw t6, 1872(a5)<br>  |
| 395|[0x80008610]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002664]:feq.d t6, ft11, ft10<br> [0x80002668]:csrrs a7, fflags, zero<br> [0x8000266c]:sw t6, 1888(a5)<br>  |
| 396|[0x80008620]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000267c]:feq.d t6, ft11, ft10<br> [0x80002680]:csrrs a7, fflags, zero<br> [0x80002684]:sw t6, 1904(a5)<br>  |
| 397|[0x80008630]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002694]:feq.d t6, ft11, ft10<br> [0x80002698]:csrrs a7, fflags, zero<br> [0x8000269c]:sw t6, 1920(a5)<br>  |
| 398|[0x80008640]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800026ac]:feq.d t6, ft11, ft10<br> [0x800026b0]:csrrs a7, fflags, zero<br> [0x800026b4]:sw t6, 1936(a5)<br>  |
| 399|[0x80008650]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800026c4]:feq.d t6, ft11, ft10<br> [0x800026c8]:csrrs a7, fflags, zero<br> [0x800026cc]:sw t6, 1952(a5)<br>  |
| 400|[0x80008660]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800026dc]:feq.d t6, ft11, ft10<br> [0x800026e0]:csrrs a7, fflags, zero<br> [0x800026e4]:sw t6, 1968(a5)<br>  |
| 401|[0x80008670]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800026f4]:feq.d t6, ft11, ft10<br> [0x800026f8]:csrrs a7, fflags, zero<br> [0x800026fc]:sw t6, 1984(a5)<br>  |
| 402|[0x80008680]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000270c]:feq.d t6, ft11, ft10<br> [0x80002710]:csrrs a7, fflags, zero<br> [0x80002714]:sw t6, 2000(a5)<br>  |
| 403|[0x80008690]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002724]:feq.d t6, ft11, ft10<br> [0x80002728]:csrrs a7, fflags, zero<br> [0x8000272c]:sw t6, 2016(a5)<br>  |
| 404|[0x800082a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002744]:feq.d t6, ft11, ft10<br> [0x80002748]:csrrs a7, fflags, zero<br> [0x8000274c]:sw t6, 0(a5)<br>     |
| 405|[0x800082b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000275c]:feq.d t6, ft11, ft10<br> [0x80002760]:csrrs a7, fflags, zero<br> [0x80002764]:sw t6, 16(a5)<br>    |
| 406|[0x800082c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002774]:feq.d t6, ft11, ft10<br> [0x80002778]:csrrs a7, fflags, zero<br> [0x8000277c]:sw t6, 32(a5)<br>    |
| 407|[0x800082d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000278c]:feq.d t6, ft11, ft10<br> [0x80002790]:csrrs a7, fflags, zero<br> [0x80002794]:sw t6, 48(a5)<br>    |
| 408|[0x800082e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800027a4]:feq.d t6, ft11, ft10<br> [0x800027a8]:csrrs a7, fflags, zero<br> [0x800027ac]:sw t6, 64(a5)<br>    |
| 409|[0x800082f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800027bc]:feq.d t6, ft11, ft10<br> [0x800027c0]:csrrs a7, fflags, zero<br> [0x800027c4]:sw t6, 80(a5)<br>    |
| 410|[0x80008308]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800027d4]:feq.d t6, ft11, ft10<br> [0x800027d8]:csrrs a7, fflags, zero<br> [0x800027dc]:sw t6, 96(a5)<br>    |
| 411|[0x80008318]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800027ec]:feq.d t6, ft11, ft10<br> [0x800027f0]:csrrs a7, fflags, zero<br> [0x800027f4]:sw t6, 112(a5)<br>   |
| 412|[0x80008328]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002804]:feq.d t6, ft11, ft10<br> [0x80002808]:csrrs a7, fflags, zero<br> [0x8000280c]:sw t6, 128(a5)<br>   |
| 413|[0x80008338]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000281c]:feq.d t6, ft11, ft10<br> [0x80002820]:csrrs a7, fflags, zero<br> [0x80002824]:sw t6, 144(a5)<br>   |
| 414|[0x80008348]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002834]:feq.d t6, ft11, ft10<br> [0x80002838]:csrrs a7, fflags, zero<br> [0x8000283c]:sw t6, 160(a5)<br>   |
| 415|[0x80008358]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x8000284c]:feq.d t6, ft11, ft10<br> [0x80002850]:csrrs a7, fflags, zero<br> [0x80002854]:sw t6, 176(a5)<br>   |
| 416|[0x80008368]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002864]:feq.d t6, ft11, ft10<br> [0x80002868]:csrrs a7, fflags, zero<br> [0x8000286c]:sw t6, 192(a5)<br>   |
| 417|[0x80008378]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000287c]:feq.d t6, ft11, ft10<br> [0x80002880]:csrrs a7, fflags, zero<br> [0x80002884]:sw t6, 208(a5)<br>   |
| 418|[0x80008388]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002894]:feq.d t6, ft11, ft10<br> [0x80002898]:csrrs a7, fflags, zero<br> [0x8000289c]:sw t6, 224(a5)<br>   |
| 419|[0x80008398]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800028ac]:feq.d t6, ft11, ft10<br> [0x800028b0]:csrrs a7, fflags, zero<br> [0x800028b4]:sw t6, 240(a5)<br>   |
| 420|[0x800083a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800028c4]:feq.d t6, ft11, ft10<br> [0x800028c8]:csrrs a7, fflags, zero<br> [0x800028cc]:sw t6, 256(a5)<br>   |
| 421|[0x800083b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800028dc]:feq.d t6, ft11, ft10<br> [0x800028e0]:csrrs a7, fflags, zero<br> [0x800028e4]:sw t6, 272(a5)<br>   |
| 422|[0x800083c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800028f4]:feq.d t6, ft11, ft10<br> [0x800028f8]:csrrs a7, fflags, zero<br> [0x800028fc]:sw t6, 288(a5)<br>   |
| 423|[0x800083d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000290c]:feq.d t6, ft11, ft10<br> [0x80002910]:csrrs a7, fflags, zero<br> [0x80002914]:sw t6, 304(a5)<br>   |
| 424|[0x800083e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002924]:feq.d t6, ft11, ft10<br> [0x80002928]:csrrs a7, fflags, zero<br> [0x8000292c]:sw t6, 320(a5)<br>   |
| 425|[0x800083f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x8000293c]:feq.d t6, ft11, ft10<br> [0x80002940]:csrrs a7, fflags, zero<br> [0x80002944]:sw t6, 336(a5)<br>   |
| 426|[0x80008408]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002954]:feq.d t6, ft11, ft10<br> [0x80002958]:csrrs a7, fflags, zero<br> [0x8000295c]:sw t6, 352(a5)<br>   |
| 427|[0x80008418]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x8000296c]:feq.d t6, ft11, ft10<br> [0x80002970]:csrrs a7, fflags, zero<br> [0x80002974]:sw t6, 368(a5)<br>   |
| 428|[0x80008428]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002984]:feq.d t6, ft11, ft10<br> [0x80002988]:csrrs a7, fflags, zero<br> [0x8000298c]:sw t6, 384(a5)<br>   |
| 429|[0x80008438]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x8000299c]:feq.d t6, ft11, ft10<br> [0x800029a0]:csrrs a7, fflags, zero<br> [0x800029a4]:sw t6, 400(a5)<br>   |
| 430|[0x80008448]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800029b4]:feq.d t6, ft11, ft10<br> [0x800029b8]:csrrs a7, fflags, zero<br> [0x800029bc]:sw t6, 416(a5)<br>   |
| 431|[0x80008458]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x800029cc]:feq.d t6, ft11, ft10<br> [0x800029d0]:csrrs a7, fflags, zero<br> [0x800029d4]:sw t6, 432(a5)<br>   |
| 432|[0x80008468]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800029e4]:feq.d t6, ft11, ft10<br> [0x800029e8]:csrrs a7, fflags, zero<br> [0x800029ec]:sw t6, 448(a5)<br>   |
| 433|[0x80008478]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800029fc]:feq.d t6, ft11, ft10<br> [0x80002a00]:csrrs a7, fflags, zero<br> [0x80002a04]:sw t6, 464(a5)<br>   |
| 434|[0x80008488]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a14]:feq.d t6, ft11, ft10<br> [0x80002a18]:csrrs a7, fflags, zero<br> [0x80002a1c]:sw t6, 480(a5)<br>   |
| 435|[0x80008498]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a2c]:feq.d t6, ft11, ft10<br> [0x80002a30]:csrrs a7, fflags, zero<br> [0x80002a34]:sw t6, 496(a5)<br>   |
| 436|[0x800084a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a44]:feq.d t6, ft11, ft10<br> [0x80002a48]:csrrs a7, fflags, zero<br> [0x80002a4c]:sw t6, 512(a5)<br>   |
| 437|[0x800084b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a5c]:feq.d t6, ft11, ft10<br> [0x80002a60]:csrrs a7, fflags, zero<br> [0x80002a64]:sw t6, 528(a5)<br>   |
| 438|[0x800084c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a74]:feq.d t6, ft11, ft10<br> [0x80002a78]:csrrs a7, fflags, zero<br> [0x80002a7c]:sw t6, 544(a5)<br>   |
| 439|[0x800084d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002a8c]:feq.d t6, ft11, ft10<br> [0x80002a90]:csrrs a7, fflags, zero<br> [0x80002a94]:sw t6, 560(a5)<br>   |
| 440|[0x800084e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002aa4]:feq.d t6, ft11, ft10<br> [0x80002aa8]:csrrs a7, fflags, zero<br> [0x80002aac]:sw t6, 576(a5)<br>   |
| 441|[0x800084f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002abc]:feq.d t6, ft11, ft10<br> [0x80002ac0]:csrrs a7, fflags, zero<br> [0x80002ac4]:sw t6, 592(a5)<br>   |
| 442|[0x80008508]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002ad4]:feq.d t6, ft11, ft10<br> [0x80002ad8]:csrrs a7, fflags, zero<br> [0x80002adc]:sw t6, 608(a5)<br>   |
| 443|[0x80008518]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002aec]:feq.d t6, ft11, ft10<br> [0x80002af0]:csrrs a7, fflags, zero<br> [0x80002af4]:sw t6, 624(a5)<br>   |
| 444|[0x80008528]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002b04]:feq.d t6, ft11, ft10<br> [0x80002b08]:csrrs a7, fflags, zero<br> [0x80002b0c]:sw t6, 640(a5)<br>   |
| 445|[0x80008538]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002b1c]:feq.d t6, ft11, ft10<br> [0x80002b20]:csrrs a7, fflags, zero<br> [0x80002b24]:sw t6, 656(a5)<br>   |
| 446|[0x80008548]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002b34]:feq.d t6, ft11, ft10<br> [0x80002b38]:csrrs a7, fflags, zero<br> [0x80002b3c]:sw t6, 672(a5)<br>   |
| 447|[0x80008558]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002b4c]:feq.d t6, ft11, ft10<br> [0x80002b50]:csrrs a7, fflags, zero<br> [0x80002b54]:sw t6, 688(a5)<br>   |
| 448|[0x80008568]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002b64]:feq.d t6, ft11, ft10<br> [0x80002b68]:csrrs a7, fflags, zero<br> [0x80002b6c]:sw t6, 704(a5)<br>   |
| 449|[0x80008578]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002b7c]:feq.d t6, ft11, ft10<br> [0x80002b80]:csrrs a7, fflags, zero<br> [0x80002b84]:sw t6, 720(a5)<br>   |
| 450|[0x80008588]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002b94]:feq.d t6, ft11, ft10<br> [0x80002b98]:csrrs a7, fflags, zero<br> [0x80002b9c]:sw t6, 736(a5)<br>   |
| 451|[0x80008598]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002bac]:feq.d t6, ft11, ft10<br> [0x80002bb0]:csrrs a7, fflags, zero<br> [0x80002bb4]:sw t6, 752(a5)<br>   |
| 452|[0x800085a8]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002bc4]:feq.d t6, ft11, ft10<br> [0x80002bc8]:csrrs a7, fflags, zero<br> [0x80002bcc]:sw t6, 768(a5)<br>   |
| 453|[0x800085b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002bdc]:feq.d t6, ft11, ft10<br> [0x80002be0]:csrrs a7, fflags, zero<br> [0x80002be4]:sw t6, 784(a5)<br>   |
| 454|[0x800085c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002bf4]:feq.d t6, ft11, ft10<br> [0x80002bf8]:csrrs a7, fflags, zero<br> [0x80002bfc]:sw t6, 800(a5)<br>   |
| 455|[0x800085d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c0c]:feq.d t6, ft11, ft10<br> [0x80002c10]:csrrs a7, fflags, zero<br> [0x80002c14]:sw t6, 816(a5)<br>   |
| 456|[0x800085e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c24]:feq.d t6, ft11, ft10<br> [0x80002c28]:csrrs a7, fflags, zero<br> [0x80002c2c]:sw t6, 832(a5)<br>   |
| 457|[0x800085f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c3c]:feq.d t6, ft11, ft10<br> [0x80002c40]:csrrs a7, fflags, zero<br> [0x80002c44]:sw t6, 848(a5)<br>   |
| 458|[0x80008608]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c54]:feq.d t6, ft11, ft10<br> [0x80002c58]:csrrs a7, fflags, zero<br> [0x80002c5c]:sw t6, 864(a5)<br>   |
| 459|[0x80008618]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c6c]:feq.d t6, ft11, ft10<br> [0x80002c70]:csrrs a7, fflags, zero<br> [0x80002c74]:sw t6, 880(a5)<br>   |
| 460|[0x80008628]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c84]:feq.d t6, ft11, ft10<br> [0x80002c88]:csrrs a7, fflags, zero<br> [0x80002c8c]:sw t6, 896(a5)<br>   |
| 461|[0x80008638]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002c9c]:feq.d t6, ft11, ft10<br> [0x80002ca0]:csrrs a7, fflags, zero<br> [0x80002ca4]:sw t6, 912(a5)<br>   |
| 462|[0x80008648]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002cb4]:feq.d t6, ft11, ft10<br> [0x80002cb8]:csrrs a7, fflags, zero<br> [0x80002cbc]:sw t6, 928(a5)<br>   |
| 463|[0x80008658]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002ccc]:feq.d t6, ft11, ft10<br> [0x80002cd0]:csrrs a7, fflags, zero<br> [0x80002cd4]:sw t6, 944(a5)<br>   |
| 464|[0x80008668]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002ce4]:feq.d t6, ft11, ft10<br> [0x80002ce8]:csrrs a7, fflags, zero<br> [0x80002cec]:sw t6, 960(a5)<br>   |
| 465|[0x80008678]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002cfc]:feq.d t6, ft11, ft10<br> [0x80002d00]:csrrs a7, fflags, zero<br> [0x80002d04]:sw t6, 976(a5)<br>   |
| 466|[0x80008688]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002d14]:feq.d t6, ft11, ft10<br> [0x80002d18]:csrrs a7, fflags, zero<br> [0x80002d1c]:sw t6, 992(a5)<br>   |
| 467|[0x80008698]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002d2c]:feq.d t6, ft11, ft10<br> [0x80002d30]:csrrs a7, fflags, zero<br> [0x80002d34]:sw t6, 1008(a5)<br>  |
| 468|[0x800086a8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002d44]:feq.d t6, ft11, ft10<br> [0x80002d48]:csrrs a7, fflags, zero<br> [0x80002d4c]:sw t6, 1024(a5)<br>  |
| 469|[0x800086b8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002d5c]:feq.d t6, ft11, ft10<br> [0x80002d60]:csrrs a7, fflags, zero<br> [0x80002d64]:sw t6, 1040(a5)<br>  |
| 470|[0x800086c8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002d74]:feq.d t6, ft11, ft10<br> [0x80002d78]:csrrs a7, fflags, zero<br> [0x80002d7c]:sw t6, 1056(a5)<br>  |
| 471|[0x800086d8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002d8c]:feq.d t6, ft11, ft10<br> [0x80002d90]:csrrs a7, fflags, zero<br> [0x80002d94]:sw t6, 1072(a5)<br>  |
| 472|[0x800086e8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002da4]:feq.d t6, ft11, ft10<br> [0x80002da8]:csrrs a7, fflags, zero<br> [0x80002dac]:sw t6, 1088(a5)<br>  |
| 473|[0x800086f8]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002dbc]:feq.d t6, ft11, ft10<br> [0x80002dc0]:csrrs a7, fflags, zero<br> [0x80002dc4]:sw t6, 1104(a5)<br>  |
| 474|[0x80008708]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002dd4]:feq.d t6, ft11, ft10<br> [0x80002dd8]:csrrs a7, fflags, zero<br> [0x80002ddc]:sw t6, 1120(a5)<br>  |
| 475|[0x80008718]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80002dec]:feq.d t6, ft11, ft10<br> [0x80002df0]:csrrs a7, fflags, zero<br> [0x80002df4]:sw t6, 1136(a5)<br>  |
| 476|[0x80008728]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e04]:feq.d t6, ft11, ft10<br> [0x80002e08]:csrrs a7, fflags, zero<br> [0x80002e0c]:sw t6, 1152(a5)<br>  |
| 477|[0x80008738]<br>0x00000001|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e1c]:feq.d t6, ft11, ft10<br> [0x80002e20]:csrrs a7, fflags, zero<br> [0x80002e24]:sw t6, 1168(a5)<br>  |
| 478|[0x80008748]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e34]:feq.d t6, ft11, ft10<br> [0x80002e38]:csrrs a7, fflags, zero<br> [0x80002e3c]:sw t6, 1184(a5)<br>  |
| 479|[0x80008758]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e4c]:feq.d t6, ft11, ft10<br> [0x80002e50]:csrrs a7, fflags, zero<br> [0x80002e54]:sw t6, 1200(a5)<br>  |
| 480|[0x80008768]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e64]:feq.d t6, ft11, ft10<br> [0x80002e68]:csrrs a7, fflags, zero<br> [0x80002e6c]:sw t6, 1216(a5)<br>  |
| 481|[0x80008778]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e7c]:feq.d t6, ft11, ft10<br> [0x80002e80]:csrrs a7, fflags, zero<br> [0x80002e84]:sw t6, 1232(a5)<br>  |
| 482|[0x80008788]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002e94]:feq.d t6, ft11, ft10<br> [0x80002e98]:csrrs a7, fflags, zero<br> [0x80002e9c]:sw t6, 1248(a5)<br>  |
| 483|[0x80008798]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002eac]:feq.d t6, ft11, ft10<br> [0x80002eb0]:csrrs a7, fflags, zero<br> [0x80002eb4]:sw t6, 1264(a5)<br>  |
| 484|[0x800087a8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002ec4]:feq.d t6, ft11, ft10<br> [0x80002ec8]:csrrs a7, fflags, zero<br> [0x80002ecc]:sw t6, 1280(a5)<br>  |
| 485|[0x800087b8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002edc]:feq.d t6, ft11, ft10<br> [0x80002ee0]:csrrs a7, fflags, zero<br> [0x80002ee4]:sw t6, 1296(a5)<br>  |
| 486|[0x800087c8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002ef4]:feq.d t6, ft11, ft10<br> [0x80002ef8]:csrrs a7, fflags, zero<br> [0x80002efc]:sw t6, 1312(a5)<br>  |
| 487|[0x800087d8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80002f0c]:feq.d t6, ft11, ft10<br> [0x80002f10]:csrrs a7, fflags, zero<br> [0x80002f14]:sw t6, 1328(a5)<br>  |
| 488|[0x800087e8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002f24]:feq.d t6, ft11, ft10<br> [0x80002f28]:csrrs a7, fflags, zero<br> [0x80002f2c]:sw t6, 1344(a5)<br>  |
| 489|[0x800087f8]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002f3c]:feq.d t6, ft11, ft10<br> [0x80002f40]:csrrs a7, fflags, zero<br> [0x80002f44]:sw t6, 1360(a5)<br>  |
| 490|[0x80008808]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002f54]:feq.d t6, ft11, ft10<br> [0x80002f58]:csrrs a7, fflags, zero<br> [0x80002f5c]:sw t6, 1376(a5)<br>  |
| 491|[0x80008818]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80002f6c]:feq.d t6, ft11, ft10<br> [0x80002f70]:csrrs a7, fflags, zero<br> [0x80002f74]:sw t6, 1392(a5)<br>  |
| 492|[0x800086a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003338]:feq.d t6, ft11, ft10<br> [0x8000333c]:csrrs a7, fflags, zero<br> [0x80003340]:sw t6, 0(a5)<br>     |
| 493|[0x800086b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003350]:feq.d t6, ft11, ft10<br> [0x80003354]:csrrs a7, fflags, zero<br> [0x80003358]:sw t6, 16(a5)<br>    |
| 494|[0x800086c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003368]:feq.d t6, ft11, ft10<br> [0x8000336c]:csrrs a7, fflags, zero<br> [0x80003370]:sw t6, 32(a5)<br>    |
| 495|[0x800086d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003380]:feq.d t6, ft11, ft10<br> [0x80003384]:csrrs a7, fflags, zero<br> [0x80003388]:sw t6, 48(a5)<br>    |
| 496|[0x800086e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003398]:feq.d t6, ft11, ft10<br> [0x8000339c]:csrrs a7, fflags, zero<br> [0x800033a0]:sw t6, 64(a5)<br>    |
| 497|[0x800086f0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800033b0]:feq.d t6, ft11, ft10<br> [0x800033b4]:csrrs a7, fflags, zero<br> [0x800033b8]:sw t6, 80(a5)<br>    |
| 498|[0x80008700]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x8000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800033c8]:feq.d t6, ft11, ft10<br> [0x800033cc]:csrrs a7, fflags, zero<br> [0x800033d0]:sw t6, 96(a5)<br>    |
| 499|[0x80008710]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800033e0]:feq.d t6, ft11, ft10<br> [0x800033e4]:csrrs a7, fflags, zero<br> [0x800033e8]:sw t6, 112(a5)<br>   |
| 500|[0x80008720]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x800033f8]:feq.d t6, ft11, ft10<br> [0x800033fc]:csrrs a7, fflags, zero<br> [0x80003400]:sw t6, 128(a5)<br>   |
| 501|[0x80008730]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80003410]:feq.d t6, ft11, ft10<br> [0x80003414]:csrrs a7, fflags, zero<br> [0x80003418]:sw t6, 144(a5)<br>   |
| 502|[0x80008740]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x80003428]:feq.d t6, ft11, ft10<br> [0x8000342c]:csrrs a7, fflags, zero<br> [0x80003430]:sw t6, 160(a5)<br>   |
| 503|[0x80008750]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80003440]:feq.d t6, ft11, ft10<br> [0x80003444]:csrrs a7, fflags, zero<br> [0x80003448]:sw t6, 176(a5)<br>   |
| 504|[0x80008760]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x80003458]:feq.d t6, ft11, ft10<br> [0x8000345c]:csrrs a7, fflags, zero<br> [0x80003460]:sw t6, 192(a5)<br>   |
| 505|[0x80008770]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003470]:feq.d t6, ft11, ft10<br> [0x80003474]:csrrs a7, fflags, zero<br> [0x80003478]:sw t6, 208(a5)<br>   |
| 506|[0x80008780]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x001 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003488]:feq.d t6, ft11, ft10<br> [0x8000348c]:csrrs a7, fflags, zero<br> [0x80003490]:sw t6, 224(a5)<br>   |
| 507|[0x80008790]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800034a0]:feq.d t6, ft11, ft10<br> [0x800034a4]:csrrs a7, fflags, zero<br> [0x800034a8]:sw t6, 240(a5)<br>   |
| 508|[0x800087a0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                                                                      |[0x800034b8]:feq.d t6, ft11, ft10<br> [0x800034bc]:csrrs a7, fflags, zero<br> [0x800034c0]:sw t6, 256(a5)<br>   |
| 509|[0x800087b0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800034d0]:feq.d t6, ft11, ft10<br> [0x800034d4]:csrrs a7, fflags, zero<br> [0x800034d8]:sw t6, 272(a5)<br>   |
| 510|[0x800087c0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000002 and rm_val == 2  #nosat<br>                                                                                      |[0x800034e8]:feq.d t6, ft11, ft10<br> [0x800034ec]:csrrs a7, fflags, zero<br> [0x800034f0]:sw t6, 288(a5)<br>   |
| 511|[0x800087d0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003500]:feq.d t6, ft11, ft10<br> [0x80003504]:csrrs a7, fflags, zero<br> [0x80003508]:sw t6, 304(a5)<br>   |
| 512|[0x800087e0]<br>0x00000000|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000001 and rm_val == 2  #nosat<br>                                                                                      |[0x80003518]:feq.d t6, ft11, ft10<br> [0x8000351c]:csrrs a7, fflags, zero<br> [0x80003520]:sw t6, 320(a5)<br>   |
| 513|[0x800087f0]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003530]:feq.d t6, ft11, ft10<br> [0x80003534]:csrrs a7, fflags, zero<br> [0x80003538]:sw t6, 336(a5)<br>   |
| 514|[0x80008800]<br>0x00000001|- fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x000 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003548]:feq.d t6, ft11, ft10<br> [0x8000354c]:csrrs a7, fflags, zero<br> [0x80003550]:sw t6, 352(a5)<br>   |
| 515|[0x80008810]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 1 and fe2 == 0x3f8 and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003560]:feq.d t6, ft11, ft10<br> [0x80003564]:csrrs a7, fflags, zero<br> [0x80003568]:sw t6, 368(a5)<br>   |
| 516|[0x80008820]<br>0x00000000|- fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x0000000000000 and rm_val == 2  #nosat<br>                                                                                      |[0x80003578]:feq.d t6, ft11, ft10<br> [0x8000357c]:csrrs a7, fflags, zero<br> [0x80003580]:sw t6, 384(a5)<br>   |
