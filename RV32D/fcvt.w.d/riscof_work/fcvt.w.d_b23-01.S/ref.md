
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800005b0')]      |
| SIG_REGION                | [('0x80002310', '0x80002480', '92 words')]      |
| COV_LABELS                | fcvt.w.d_b23      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.w.d/riscof_work/fcvt.w.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 114     |
| Total Coverpoints Hit     | 85      |
| Total Signature Updates   | 56      |
| STAT1                     | 28      |
| STAT2                     | 0      |
| STAT3                     | 17     |
| STAT4                     | 28     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000404]:fcvt.w.d s1, ft0, dyn
[0x80000408]:csrrs a7, fflags, zero
[0x8000040c]:sw s1, 304(a5)
[0x80000410]:sw a7, 308(a5)
[0x80000414]:fld ft4, 232(a6)
[0x80000418]:csrrwi zero, frm, 1

[0x8000041c]:fcvt.w.d s9, ft4, dyn
[0x80000420]:csrrs a7, fflags, zero
[0x80000424]:sw s9, 320(a5)
[0x80000428]:sw a7, 324(a5)
[0x8000042c]:fld fs1, 240(a6)
[0x80000430]:csrrwi zero, frm, 0

[0x80000434]:fcvt.w.d a1, fs1, dyn
[0x80000438]:csrrs a7, fflags, zero
[0x8000043c]:sw a1, 336(a5)
[0x80000440]:sw a7, 340(a5)
[0x80000444]:fld fa3, 248(a6)
[0x80000448]:csrrwi zero, frm, 4

[0x8000044c]:fcvt.w.d s7, fa3, dyn
[0x80000450]:csrrs a7, fflags, zero
[0x80000454]:sw s7, 352(a5)
[0x80000458]:sw a7, 356(a5)
[0x8000045c]:fld ft11, 256(a6)
[0x80000460]:csrrwi zero, frm, 3

[0x80000464]:fcvt.w.d t6, ft11, dyn
[0x80000468]:csrrs a7, fflags, zero
[0x8000046c]:sw t6, 368(a5)
[0x80000470]:sw a7, 372(a5)
[0x80000474]:fld ft11, 264(a6)
[0x80000478]:csrrwi zero, frm, 2

[0x8000047c]:fcvt.w.d t6, ft11, dyn
[0x80000480]:csrrs a7, fflags, zero
[0x80000484]:sw t6, 384(a5)
[0x80000488]:sw a7, 388(a5)
[0x8000048c]:fld ft11, 272(a6)
[0x80000490]:csrrwi zero, frm, 1

[0x80000494]:fcvt.w.d t6, ft11, dyn
[0x80000498]:csrrs a7, fflags, zero
[0x8000049c]:sw t6, 400(a5)
[0x800004a0]:sw a7, 404(a5)
[0x800004a4]:fld ft11, 280(a6)
[0x800004a8]:csrrwi zero, frm, 0

[0x800004ac]:fcvt.w.d t6, ft11, dyn
[0x800004b0]:csrrs a7, fflags, zero
[0x800004b4]:sw t6, 416(a5)
[0x800004b8]:sw a7, 420(a5)
[0x800004bc]:fld ft11, 288(a6)
[0x800004c0]:csrrwi zero, frm, 4

[0x800004c4]:fcvt.w.d t6, ft11, dyn
[0x800004c8]:csrrs a7, fflags, zero
[0x800004cc]:sw t6, 432(a5)
[0x800004d0]:sw a7, 436(a5)
[0x800004d4]:fld ft11, 296(a6)
[0x800004d8]:csrrwi zero, frm, 3

[0x800004dc]:fcvt.w.d t6, ft11, dyn
[0x800004e0]:csrrs a7, fflags, zero
[0x800004e4]:sw t6, 448(a5)
[0x800004e8]:sw a7, 452(a5)
[0x800004ec]:fld ft11, 304(a6)
[0x800004f0]:csrrwi zero, frm, 2

[0x800004f4]:fcvt.w.d t6, ft11, dyn
[0x800004f8]:csrrs a7, fflags, zero
[0x800004fc]:sw t6, 464(a5)
[0x80000500]:sw a7, 468(a5)
[0x80000504]:fld ft11, 312(a6)
[0x80000508]:csrrwi zero, frm, 1

[0x8000050c]:fcvt.w.d t6, ft11, dyn
[0x80000510]:csrrs a7, fflags, zero
[0x80000514]:sw t6, 480(a5)
[0x80000518]:sw a7, 484(a5)
[0x8000051c]:fld ft11, 320(a6)
[0x80000520]:csrrwi zero, frm, 0

[0x80000524]:fcvt.w.d t6, ft11, dyn
[0x80000528]:csrrs a7, fflags, zero
[0x8000052c]:sw t6, 496(a5)
[0x80000530]:sw a7, 500(a5)
[0x80000534]:fld ft11, 328(a6)
[0x80000538]:csrrwi zero, frm, 4

[0x8000053c]:fcvt.w.d t6, ft11, dyn
[0x80000540]:csrrs a7, fflags, zero
[0x80000544]:sw t6, 512(a5)
[0x80000548]:sw a7, 516(a5)
[0x8000054c]:fld ft11, 336(a6)
[0x80000550]:csrrwi zero, frm, 3

[0x80000554]:fcvt.w.d t6, ft11, dyn
[0x80000558]:csrrs a7, fflags, zero
[0x8000055c]:sw t6, 528(a5)
[0x80000560]:sw a7, 532(a5)
[0x80000564]:fld ft11, 344(a6)
[0x80000568]:csrrwi zero, frm, 2

[0x8000056c]:fcvt.w.d t6, ft11, dyn
[0x80000570]:csrrs a7, fflags, zero
[0x80000574]:sw t6, 544(a5)
[0x80000578]:sw a7, 548(a5)
[0x8000057c]:fld ft11, 352(a6)
[0x80000580]:csrrwi zero, frm, 1

[0x80000584]:fcvt.w.d t6, ft11, dyn
[0x80000588]:csrrs a7, fflags, zero
[0x8000058c]:sw t6, 560(a5)
[0x80000590]:sw a7, 564(a5)
[0x80000594]:fld ft11, 360(a6)
[0x80000598]:csrrwi zero, frm, 1



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x26', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.d s10, fs11, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw s10, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000010




Last Coverpoint : ['rd : x15', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000140]:fcvt.w.d a5, fa0, dyn
	-[0x80000144]:csrrs s5, fflags, zero
	-[0x80000148]:sw a5, 0(s3)
Current Store : [0x8000014c] : sw s5, 4(s3) -- Store: [0x8000231c]:0x00000010




Last Coverpoint : ['rd : x18', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.w.d s2, fa6, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw s2, 0(a5)
Current Store : [0x80000170] : sw a7, 4(a5) -- Store: [0x80002324]:0x00000010




Last Coverpoint : ['rd : x21', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.w.d s5, fa5, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw s5, 16(a5)
Current Store : [0x80000188] : sw a7, 20(a5) -- Store: [0x80002334]:0x00000010




Last Coverpoint : ['rd : x24', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.w.d s8, fs0, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw s8, 32(a5)
Current Store : [0x800001a0] : sw a7, 36(a5) -- Store: [0x80002344]:0x00000010




Last Coverpoint : ['rd : x17', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001b8]:fcvt.w.d a7, ft3, dyn
	-[0x800001bc]:csrrs s5, fflags, zero
	-[0x800001c0]:sw a7, 0(s3)
Current Store : [0x800001c4] : sw s5, 4(s3) -- Store: [0x8000233c]:0x00000010




Last Coverpoint : ['rd : x31', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.w.d t6, fs6, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw t6, 0(a5)
Current Store : [0x800001e8] : sw a7, 4(a5) -- Store: [0x80002344]:0x00000010




Last Coverpoint : ['rd : x10', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.w.d a0, ft1, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw a0, 16(a5)
Current Store : [0x80000200] : sw a7, 20(a5) -- Store: [0x80002354]:0x00000010




Last Coverpoint : ['rd : x16', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000218]:fcvt.w.d a6, fa2, dyn
	-[0x8000021c]:csrrs s5, fflags, zero
	-[0x80000220]:sw a6, 0(s3)
Current Store : [0x80000224] : sw s5, 4(s3) -- Store: [0x80002354]:0x00000010




Last Coverpoint : ['rd : x27', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.w.d s11, fs9, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw s11, 0(a5)
Current Store : [0x80000248] : sw a7, 4(a5) -- Store: [0x8000235c]:0x00000010




Last Coverpoint : ['rd : x6', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.w.d t1, fa4, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw t1, 16(a5)
Current Store : [0x80000260] : sw a7, 20(a5) -- Store: [0x8000236c]:0x00000010




Last Coverpoint : ['rd : x1', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.w.d ra, ft8, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw ra, 32(a5)
Current Store : [0x80000278] : sw a7, 36(a5) -- Store: [0x8000237c]:0x00000010




Last Coverpoint : ['rd : x14', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.w.d a4, fa1, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw a4, 48(a5)
Current Store : [0x80000290] : sw a7, 52(a5) -- Store: [0x8000238c]:0x00000010




Last Coverpoint : ['rd : x29', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.w.d t4, fs7, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw t4, 64(a5)
Current Store : [0x800002a8] : sw a7, 68(a5) -- Store: [0x8000239c]:0x00000010




Last Coverpoint : ['rd : x0', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.w.d zero, ft10, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw zero, 80(a5)
Current Store : [0x800002c0] : sw a7, 84(a5) -- Store: [0x800023ac]:0x00000010




Last Coverpoint : ['rd : x3', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002cc]:fcvt.w.d gp, fs10, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw gp, 96(a5)
Current Store : [0x800002d8] : sw a7, 100(a5) -- Store: [0x800023bc]:0x00000010




Last Coverpoint : ['rd : x22', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fcvt.w.d s6, fs8, dyn
	-[0x800002e8]:csrrs a7, fflags, zero
	-[0x800002ec]:sw s6, 112(a5)
Current Store : [0x800002f0] : sw a7, 116(a5) -- Store: [0x800023cc]:0x00000010




Last Coverpoint : ['rd : x5', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800002fc]:fcvt.w.d t0, fs4, dyn
	-[0x80000300]:csrrs a7, fflags, zero
	-[0x80000304]:sw t0, 128(a5)
Current Store : [0x80000308] : sw a7, 132(a5) -- Store: [0x800023dc]:0x00000010




Last Coverpoint : ['rd : x7', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.w.d t2, ft6, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw t2, 144(a5)
Current Store : [0x80000320] : sw a7, 148(a5) -- Store: [0x800023ec]:0x00000010




Last Coverpoint : ['rd : x4', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.w.d tp, fs5, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw tp, 160(a5)
Current Store : [0x80000338] : sw a7, 164(a5) -- Store: [0x800023fc]:0x00000010




Last Coverpoint : ['rd : x8', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.w.d fp, fs2, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw fp, 176(a5)
Current Store : [0x80000350] : sw a7, 180(a5) -- Store: [0x8000240c]:0x00000010




Last Coverpoint : ['rd : x2', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.w.d sp, ft2, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw sp, 192(a5)
Current Store : [0x80000368] : sw a7, 196(a5) -- Store: [0x8000241c]:0x00000010




Last Coverpoint : ['rd : x28', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.w.d t3, fs3, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw t3, 208(a5)
Current Store : [0x80000380] : sw a7, 212(a5) -- Store: [0x8000242c]:0x00000010




Last Coverpoint : ['rd : x13', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.w.d a3, ft9, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw a3, 224(a5)
Current Store : [0x80000398] : sw a7, 228(a5) -- Store: [0x8000243c]:0x00000010




Last Coverpoint : ['rd : x20', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.w.d s4, ft11, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw s4, 240(a5)
Current Store : [0x800003b0] : sw a7, 244(a5) -- Store: [0x8000244c]:0x00000010




Last Coverpoint : ['rd : x12', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fcvt.w.d a2, ft5, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw a2, 256(a5)
Current Store : [0x800003c8] : sw a7, 260(a5) -- Store: [0x8000245c]:0x00000010




Last Coverpoint : ['rd : x19', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.w.d s3, fa7, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s3, 272(a5)
Current Store : [0x800003e0] : sw a7, 276(a5) -- Store: [0x8000246c]:0x00000010




Last Coverpoint : ['rd : x30', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fcvt.w.d t5, ft7, dyn
	-[0x800003f0]:csrrs a7, fflags, zero
	-[0x800003f4]:sw t5, 288(a5)
Current Store : [0x800003f8] : sw a7, 292(a5) -- Store: [0x8000247c]:0x00000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                               coverpoints                                                                |                                                       code                                                        |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x7FFFFFFF|- opcode : fcvt.w.d<br> - rd : x26<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.d s10, fs11, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw s10, 0(a5)<br>    |
|   2|[0x80002318]<br>0x7FFFFFFF|- rd : x15<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat<br>                         |[0x80000140]:fcvt.w.d a5, fa0, dyn<br> [0x80000144]:csrrs s5, fflags, zero<br> [0x80000148]:sw a5, 0(s3)<br>       |
|   3|[0x80002320]<br>0x7FFFFFFF|- rd : x18<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat<br>                         |[0x80000164]:fcvt.w.d s2, fa6, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw s2, 0(a5)<br>       |
|   4|[0x80002330]<br>0x7FFFFFFF|- rd : x21<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat<br>                         |[0x8000017c]:fcvt.w.d s5, fa5, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw s5, 16(a5)<br>      |
|   5|[0x80002340]<br>0x7FFFFFFF|- rd : x24<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat<br>                          |[0x80000194]:fcvt.w.d s8, fs0, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw s8, 32(a5)<br>      |
|   6|[0x80002338]<br>0x7FFFFFFF|- rd : x17<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat<br>                          |[0x800001b8]:fcvt.w.d a7, ft3, dyn<br> [0x800001bc]:csrrs s5, fflags, zero<br> [0x800001c0]:sw a7, 0(s3)<br>       |
|   7|[0x80002340]<br>0x7FFFFFFF|- rd : x31<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat<br>                         |[0x800001dc]:fcvt.w.d t6, fs6, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw t6, 0(a5)<br>       |
|   8|[0x80002350]<br>0x7FFFFFFF|- rd : x10<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat<br>                          |[0x800001f4]:fcvt.w.d a0, ft1, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw a0, 16(a5)<br>      |
|   9|[0x80002350]<br>0x7FFFFFFF|- rd : x16<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat<br>                         |[0x80000218]:fcvt.w.d a6, fa2, dyn<br> [0x8000021c]:csrrs s5, fflags, zero<br> [0x80000220]:sw a6, 0(s3)<br>       |
|  10|[0x80002358]<br>0x7FFFFFFF|- rd : x27<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat<br>                         |[0x8000023c]:fcvt.w.d s11, fs9, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw s11, 0(a5)<br>     |
|  11|[0x80002368]<br>0x7FFFFFFF|- rd : x6<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat<br>                          |[0x80000254]:fcvt.w.d t1, fa4, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw t1, 16(a5)<br>      |
|  12|[0x80002378]<br>0x7FFFFFFF|- rd : x1<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat<br>                          |[0x8000026c]:fcvt.w.d ra, ft8, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw ra, 32(a5)<br>      |
|  13|[0x80002388]<br>0x7FFFFFFF|- rd : x14<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat<br>                         |[0x80000284]:fcvt.w.d a4, fa1, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw a4, 48(a5)<br>      |
|  14|[0x80002398]<br>0x7FFFFFFF|- rd : x29<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat<br>                         |[0x8000029c]:fcvt.w.d t4, fs7, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw t4, 64(a5)<br>      |
|  15|[0x800023a8]<br>0x00000000|- rd : x0<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                          |[0x800002b4]:fcvt.w.d zero, ft10, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw zero, 80(a5)<br> |
|  16|[0x800023b8]<br>0x7FFFFFFF|- rd : x3<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                          |[0x800002cc]:fcvt.w.d gp, fs10, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw gp, 96(a5)<br>     |
|  17|[0x800023c8]<br>0x7FFFFFFF|- rd : x22<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat<br>                         |[0x800002e4]:fcvt.w.d s6, fs8, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw s6, 112(a5)<br>     |
|  18|[0x800023d8]<br>0x7FFFFFFF|- rd : x5<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat<br>                          |[0x800002fc]:fcvt.w.d t0, fs4, dyn<br> [0x80000300]:csrrs a7, fflags, zero<br> [0x80000304]:sw t0, 128(a5)<br>     |
|  19|[0x800023e8]<br>0x7FFFFFFF|- rd : x7<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat<br>                           |[0x80000314]:fcvt.w.d t2, ft6, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw t2, 144(a5)<br>     |
|  20|[0x800023f8]<br>0x7FFFFFFF|- rd : x4<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                          |[0x8000032c]:fcvt.w.d tp, fs5, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw tp, 160(a5)<br>     |
|  21|[0x80002408]<br>0x7FFFFFFF|- rd : x8<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x80000344]:fcvt.w.d fp, fs2, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw fp, 176(a5)<br>     |
|  22|[0x80002418]<br>0x7FFFFFFF|- rd : x2<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                           |[0x8000035c]:fcvt.w.d sp, ft2, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw sp, 192(a5)<br>     |
|  23|[0x80002428]<br>0x7FFFFFFF|- rd : x28<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                         |[0x80000374]:fcvt.w.d t3, fs3, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw t3, 208(a5)<br>     |
|  24|[0x80002438]<br>0x7FFFFFFF|- rd : x13<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 2  #nosat<br>                         |[0x8000038c]:fcvt.w.d a3, ft9, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw a3, 224(a5)<br>     |
|  25|[0x80002448]<br>0x7FFFFFFF|- rd : x20<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                         |[0x800003a4]:fcvt.w.d s4, ft11, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw s4, 240(a5)<br>    |
|  26|[0x80002458]<br>0x7FFFFFFF|- rd : x12<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800003bc]:fcvt.w.d a2, ft5, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw a2, 256(a5)<br>     |
|  27|[0x80002468]<br>0x7FFFFFFF|- rd : x19<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 4  #nosat<br>                         |[0x800003d4]:fcvt.w.d s3, fa7, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s3, 272(a5)<br>     |
|  28|[0x80002478]<br>0x7FFFFFFF|- rd : x30<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 3  #nosat<br>                          |[0x800003ec]:fcvt.w.d t5, ft7, dyn<br> [0x800003f0]:csrrs a7, fflags, zero<br> [0x800003f4]:sw t5, 288(a5)<br>     |
