
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000450')]      |
| SIG_REGION                | [('0x80002310', '0x80002410', '64 words')]      |
| COV_LABELS                | fcvt.w.d_b28      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.w.d/riscof_work/fcvt.w.d_b28-01.S/ref.S    |
| Total Number of coverpoints| 97     |
| Total Coverpoints Hit     | 79      |
| Total Signature Updates   | 52      |
| STAT1                     | 26      |
| STAT2                     | 0      |
| STAT3                     | 5     |
| STAT4                     | 26     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003bc]:fcvt.w.d a1, ft9, dyn
[0x800003c0]:csrrs a7, fflags, zero
[0x800003c4]:sw a1, 112(a5)
[0x800003c8]:sw a7, 116(a5)
[0x800003cc]:fld fs2, 216(a6)
[0x800003d0]:csrrwi zero, frm, 0

[0x800003d4]:fcvt.w.d t3, fs2, dyn
[0x800003d8]:csrrs a7, fflags, zero
[0x800003dc]:sw t3, 128(a5)
[0x800003e0]:sw a7, 132(a5)
[0x800003e4]:fld fa4, 224(a6)
[0x800003e8]:csrrwi zero, frm, 0

[0x800003ec]:fcvt.w.d zero, fa4, dyn
[0x800003f0]:csrrs a7, fflags, zero
[0x800003f4]:sw zero, 144(a5)
[0x800003f8]:sw a7, 148(a5)
[0x800003fc]:fld fs11, 232(a6)
[0x80000400]:csrrwi zero, frm, 0

[0x80000404]:fcvt.w.d a0, fs11, dyn
[0x80000408]:csrrs a7, fflags, zero
[0x8000040c]:sw a0, 160(a5)
[0x80000410]:sw a7, 164(a5)
[0x80000414]:fld fs0, 240(a6)
[0x80000418]:csrrwi zero, frm, 0

[0x8000041c]:fcvt.w.d t4, fs0, dyn
[0x80000420]:csrrs a7, fflags, zero
[0x80000424]:sw t4, 176(a5)
[0x80000428]:sw a7, 180(a5)
[0x8000042c]:fld ft7, 248(a6)
[0x80000430]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x3', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.d gp, ft0, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw gp, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000000




Last Coverpoint : ['rd : x1', 'rs1 : f1', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.w.d ra, ft1, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw ra, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002324]:0x00000010




Last Coverpoint : ['rd : x9', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.w.d s1, ft4, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw s1, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002334]:0x00000010




Last Coverpoint : ['rd : x7', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x43d and fm1 == 0x967a4ae26514c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000164]:fcvt.w.d t2, fa0, dyn
	-[0x80000168]:csrrs a7, fflags, zero
	-[0x8000016c]:sw t2, 48(a5)
Current Store : [0x80000170] : sw a7, 52(a5) -- Store: [0x80002344]:0x00000010




Last Coverpoint : ['rd : x24', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000017c]:fcvt.w.d s8, ft6, dyn
	-[0x80000180]:csrrs a7, fflags, zero
	-[0x80000184]:sw s8, 64(a5)
Current Store : [0x80000188] : sw a7, 68(a5) -- Store: [0x80002354]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.w.d t0, fa2, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw t0, 80(a5)
Current Store : [0x800001a0] : sw a7, 84(a5) -- Store: [0x80002364]:0x00000011




Last Coverpoint : ['rd : x27', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.w.d s11, fa7, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw s11, 96(a5)
Current Store : [0x800001b8] : sw a7, 100(a5) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x15', 'rs1 : f13', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001d0]:fcvt.w.d a5, fa3, dyn
	-[0x800001d4]:csrrs s5, fflags, zero
	-[0x800001d8]:sw a5, 0(s3)
Current Store : [0x800001dc] : sw s5, 4(s3) -- Store: [0x8000234c]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001f4]:fcvt.w.d s3, fa1, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw s3, 0(a5)
Current Store : [0x80000200] : sw a7, 4(a5) -- Store: [0x80002354]:0x00000011




Last Coverpoint : ['rd : x20', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000020c]:fcvt.w.d s4, ft8, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw s4, 16(a5)
Current Store : [0x80000218] : sw a7, 20(a5) -- Store: [0x80002364]:0x00000011




Last Coverpoint : ['rd : x22', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000224]:fcvt.w.d s6, fa5, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw s6, 32(a5)
Current Store : [0x80000230] : sw a7, 36(a5) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000023c]:fcvt.w.d t5, fs8, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw t5, 48(a5)
Current Store : [0x80000248] : sw a7, 52(a5) -- Store: [0x80002384]:0x00000011




Last Coverpoint : ['rd : x14', 'rs1 : f3', 'fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.w.d a4, ft3, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw a4, 64(a5)
Current Store : [0x80000260] : sw a7, 68(a5) -- Store: [0x80002394]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000026c]:fcvt.w.d s9, fs3, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw s9, 80(a5)
Current Store : [0x80000278] : sw a7, 84(a5) -- Store: [0x800023a4]:0x00000011




Last Coverpoint : ['rd : x2', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000284]:fcvt.w.d sp, fs7, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw sp, 96(a5)
Current Store : [0x80000290] : sw a7, 100(a5) -- Store: [0x800023b4]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000029c]:fcvt.w.d s5, fs5, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw s5, 112(a5)
Current Store : [0x800002a8] : sw a7, 116(a5) -- Store: [0x800023c4]:0x00000011




Last Coverpoint : ['rd : x23', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002b4]:fcvt.w.d s7, ft11, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw s7, 128(a5)
Current Store : [0x800002c0] : sw a7, 132(a5) -- Store: [0x800023d4]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fcvt.w.d a7, ft5, dyn
	-[0x800002dc]:csrrs s5, fflags, zero
	-[0x800002e0]:sw a7, 0(s3)
Current Store : [0x800002e4] : sw s5, 4(s3) -- Store: [0x8000239c]:0x00000011




Last Coverpoint : ['rd : x16', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002f0]:fcvt.w.d a6, fs1, dyn
	-[0x800002f4]:csrrs s5, fflags, zero
	-[0x800002f8]:sw a6, 16(s3)
Current Store : [0x800002fc] : sw s5, 20(s3) -- Store: [0x800023ac]:0x00000011




Last Coverpoint : ['rd : x6', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000314]:fcvt.w.d t1, fs9, dyn
	-[0x80000318]:csrrs a7, fflags, zero
	-[0x8000031c]:sw t1, 0(a5)
Current Store : [0x80000320] : sw a7, 4(a5) -- Store: [0x800023ac]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000032c]:fcvt.w.d a3, fs6, dyn
	-[0x80000330]:csrrs a7, fflags, zero
	-[0x80000334]:sw a3, 16(a5)
Current Store : [0x80000338] : sw a7, 20(a5) -- Store: [0x800023bc]:0x00000011




Last Coverpoint : ['rd : x26', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000344]:fcvt.w.d s10, ft10, dyn
	-[0x80000348]:csrrs a7, fflags, zero
	-[0x8000034c]:sw s10, 32(a5)
Current Store : [0x80000350] : sw a7, 36(a5) -- Store: [0x800023cc]:0x00000011




Last Coverpoint : ['rd : x8', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000035c]:fcvt.w.d fp, fa6, dyn
	-[0x80000360]:csrrs a7, fflags, zero
	-[0x80000364]:sw fp, 48(a5)
Current Store : [0x80000368] : sw a7, 52(a5) -- Store: [0x800023dc]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000374]:fcvt.w.d a2, fs4, dyn
	-[0x80000378]:csrrs a7, fflags, zero
	-[0x8000037c]:sw a2, 64(a5)
Current Store : [0x80000380] : sw a7, 68(a5) -- Store: [0x800023ec]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000038c]:fcvt.w.d t6, fs10, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw t6, 80(a5)
Current Store : [0x80000398] : sw a7, 84(a5) -- Store: [0x800023fc]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fcvt.w.d s2, ft2, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw s2, 96(a5)
Current Store : [0x800003b0] : sw a7, 100(a5) -- Store: [0x8000240c]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                              coverpoints                                                               |                                                      code                                                       |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000000|- opcode : fcvt.w.d<br> - rd : x3<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.d gp, ft0, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw gp, 0(a5)<br>     |
|   2|[0x80002320]<br>0x80000000|- rd : x1<br> - rs1 : f1<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x80000134]:fcvt.w.d ra, ft1, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw ra, 16(a5)<br>    |
|   3|[0x80002330]<br>0x80000000|- rd : x9<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x8000014c]:fcvt.w.d s1, ft4, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw s1, 32(a5)<br>    |
|   4|[0x80002340]<br>0x80000000|- rd : x7<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x43d and fm1 == 0x967a4ae26514c and rm_val == 0  #nosat<br>                        |[0x80000164]:fcvt.w.d t2, fa0, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:sw t2, 48(a5)<br>    |
|   5|[0x80002350]<br>0xFFFFFFFF|- rd : x24<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                        |[0x8000017c]:fcvt.w.d s8, ft6, dyn<br> [0x80000180]:csrrs a7, fflags, zero<br> [0x80000184]:sw s8, 64(a5)<br>    |
|   6|[0x80002360]<br>0xFFFFFFFE|- rd : x5<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                        |[0x80000194]:fcvt.w.d t0, fa2, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw t0, 80(a5)<br>    |
|   7|[0x80002370]<br>0xFFFFFFFE|- rd : x27<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat<br>                       |[0x800001ac]:fcvt.w.d s11, fa7, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw s11, 96(a5)<br>  |
|   8|[0x80002348]<br>0xFFFFFFFE|- rd : x15<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                       |[0x800001d0]:fcvt.w.d a5, fa3, dyn<br> [0x800001d4]:csrrs s5, fflags, zero<br> [0x800001d8]:sw a5, 0(s3)<br>     |
|   9|[0x80002350]<br>0xFFFFFFFE|- rd : x19<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat<br>                       |[0x800001f4]:fcvt.w.d s3, fa1, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw s3, 0(a5)<br>     |
|  10|[0x80002360]<br>0xFFFFFFFE|- rd : x20<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                       |[0x8000020c]:fcvt.w.d s4, ft8, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw s4, 16(a5)<br>    |
|  11|[0x80002370]<br>0xFFFFFFFD|- rd : x22<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat<br>                       |[0x80000224]:fcvt.w.d s6, fa5, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw s6, 32(a5)<br>    |
|  12|[0x80002380]<br>0x00000000|- rd : x30<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                       |[0x8000023c]:fcvt.w.d t5, fs8, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw t5, 48(a5)<br>    |
|  13|[0x80002390]<br>0x00000000|- rd : x14<br> - rs1 : f3<br> - fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and rm_val == 0  #nosat<br>                        |[0x80000254]:fcvt.w.d a4, ft3, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw a4, 64(a5)<br>    |
|  14|[0x800023a0]<br>0x00000000|- rd : x25<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                       |[0x8000026c]:fcvt.w.d s9, fs3, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw s9, 80(a5)<br>    |
|  15|[0x800023b0]<br>0x7FFFFFFF|- rd : x2<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                        |[0x80000284]:fcvt.w.d sp, fs7, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw sp, 96(a5)<br>    |
|  16|[0x800023c0]<br>0x7FFFFFFF|- rd : x21<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                       |[0x8000029c]:fcvt.w.d s5, fs5, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw s5, 112(a5)<br>   |
|  17|[0x800023d0]<br>0x7FFFFFFF|- rd : x23<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                       |[0x800002b4]:fcvt.w.d s7, ft11, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw s7, 128(a5)<br>  |
|  18|[0x80002398]<br>0x7FFFFFFF|- rd : x17<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                        |[0x800002d8]:fcvt.w.d a7, ft5, dyn<br> [0x800002dc]:csrrs s5, fflags, zero<br> [0x800002e0]:sw a7, 0(s3)<br>     |
|  19|[0x800023a8]<br>0x7FFFFFFF|- rd : x16<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and rm_val == 0  #nosat<br>                        |[0x800002f0]:fcvt.w.d a6, fs1, dyn<br> [0x800002f4]:csrrs s5, fflags, zero<br> [0x800002f8]:sw a6, 16(s3)<br>    |
|  20|[0x800023a8]<br>0x00000003|- rd : x6<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat<br>                        |[0x80000314]:fcvt.w.d t1, fs9, dyn<br> [0x80000318]:csrrs a7, fflags, zero<br> [0x8000031c]:sw t1, 0(a5)<br>     |
|  21|[0x800023b8]<br>0x00000002|- rd : x13<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                       |[0x8000032c]:fcvt.w.d a3, fs6, dyn<br> [0x80000330]:csrrs a7, fflags, zero<br> [0x80000334]:sw a3, 16(a5)<br>    |
|  22|[0x800023c8]<br>0x00000002|- rd : x26<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat<br>                       |[0x80000344]:fcvt.w.d s10, ft10, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:sw s10, 32(a5)<br> |
|  23|[0x800023d8]<br>0x00000002|- rd : x8<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                        |[0x8000035c]:fcvt.w.d fp, fa6, dyn<br> [0x80000360]:csrrs a7, fflags, zero<br> [0x80000364]:sw fp, 48(a5)<br>    |
|  24|[0x800023e8]<br>0x00000002|- rd : x12<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat<br>                       |[0x80000374]:fcvt.w.d a2, fs4, dyn<br> [0x80000378]:csrrs a7, fflags, zero<br> [0x8000037c]:sw a2, 64(a5)<br>    |
|  25|[0x800023f8]<br>0x00000002|- rd : x31<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                       |[0x8000038c]:fcvt.w.d t6, fs10, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw t6, 80(a5)<br>   |
|  26|[0x80002408]<br>0x00000001|- rd : x18<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                        |[0x800003a4]:fcvt.w.d s2, ft2, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw s2, 96(a5)<br>    |
