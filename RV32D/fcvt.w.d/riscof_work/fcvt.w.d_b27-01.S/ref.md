
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000460')]      |
| SIG_REGION                | [('0x80002310', '0x80002410', '64 words')]      |
| COV_LABELS                | fcvt.w.d_b27      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.w.d/riscof_work/fcvt.w.d_b27-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 63      |
| Total Signature Updates   | 53      |
| STAT1                     | 27      |
| STAT2                     | 0      |
| STAT3                     | 4     |
| STAT4                     | 26     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800002fc]:fcvt.w.d s3, fs8, dyn
[0x80000300]:csrrs a7, fflags, zero
[0x80000304]:sw s3, 240(a5)
[0x80000308]:sw a7, 244(a5)
[0x8000030c]:fld ft10, 160(a6)
[0x80000310]:csrrwi zero, frm, 0

[0x80000314]:fcvt.w.d t6, ft10, dyn
[0x80000318]:csrrs a7, fflags, zero
[0x8000031c]:sw t6, 256(a5)
[0x80000320]:sw a7, 260(a5)
[0x80000324]:fld fs2, 168(a6)
[0x80000328]:csrrwi zero, frm, 0

[0x8000032c]:fcvt.w.d t2, fs2, dyn
[0x80000330]:csrrs a7, fflags, zero
[0x80000334]:sw t2, 272(a5)
[0x80000338]:sw a7, 276(a5)
[0x8000033c]:fld ft5, 176(a6)
[0x80000340]:csrrwi zero, frm, 0

[0x80000344]:fcvt.w.d a2, ft5, dyn
[0x80000348]:csrrs a7, fflags, zero
[0x8000034c]:sw a2, 288(a5)
[0x80000350]:sw a7, 292(a5)
[0x80000354]:add s4, a6, zero
[0x80000358]:auipc s3, 2
[0x8000035c]:addi s3, s3, 112
[0x80000360]:fld fa6, 184(s4)
[0x80000364]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x26', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.d s10, fa0, dyn
	-[0x80000120]:csrrs a7, fflags, zero
	-[0x80000124]:sw s10, 0(a5)
Current Store : [0x80000128] : sw a7, 4(a5) -- Store: [0x80002314]:0x00000010




Last Coverpoint : ['rd : x22', 'rs1 : f8', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.w.d s6, fs0, dyn
	-[0x80000138]:csrrs a7, fflags, zero
	-[0x8000013c]:sw s6, 16(a5)
Current Store : [0x80000140] : sw a7, 20(a5) -- Store: [0x80002324]:0x00000010




Last Coverpoint : ['rd : x29', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000014c]:fcvt.w.d t4, fa2, dyn
	-[0x80000150]:csrrs a7, fflags, zero
	-[0x80000154]:sw t4, 32(a5)
Current Store : [0x80000158] : sw a7, 36(a5) -- Store: [0x80002334]:0x00000010




Last Coverpoint : ['rd : x17', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000170]:fcvt.w.d a7, ft11, dyn
	-[0x80000174]:csrrs s5, fflags, zero
	-[0x80000178]:sw a7, 0(s3)
Current Store : [0x8000017c] : sw s5, 4(s3) -- Store: [0x8000232c]:0x00000010




Last Coverpoint : ['rd : x13', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000194]:fcvt.w.d a3, fs3, dyn
	-[0x80000198]:csrrs a7, fflags, zero
	-[0x8000019c]:sw a3, 0(a5)
Current Store : [0x800001a0] : sw a7, 4(a5) -- Store: [0x80002334]:0x00000010




Last Coverpoint : ['rd : x24', 'rs1 : f13', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001ac]:fcvt.w.d s8, fa3, dyn
	-[0x800001b0]:csrrs a7, fflags, zero
	-[0x800001b4]:sw s8, 16(a5)
Current Store : [0x800001b8] : sw a7, 20(a5) -- Store: [0x80002344]:0x00000010




Last Coverpoint : ['rd : x4', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fcvt.w.d tp, fs11, dyn
	-[0x800001c8]:csrrs a7, fflags, zero
	-[0x800001cc]:sw tp, 32(a5)
Current Store : [0x800001d0] : sw a7, 36(a5) -- Store: [0x80002354]:0x00000010




Last Coverpoint : ['rd : x18', 'rs1 : f3', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001dc]:fcvt.w.d s2, ft3, dyn
	-[0x800001e0]:csrrs a7, fflags, zero
	-[0x800001e4]:sw s2, 48(a5)
Current Store : [0x800001e8] : sw a7, 52(a5) -- Store: [0x80002364]:0x00000010




Last Coverpoint : ['rd : x20', 'rs1 : f22']
Last Code Sequence : 
	-[0x800001f4]:fcvt.w.d s4, fs6, dyn
	-[0x800001f8]:csrrs a7, fflags, zero
	-[0x800001fc]:sw s4, 64(a5)
Current Store : [0x80000200] : sw a7, 68(a5) -- Store: [0x80002374]:0x00000010




Last Coverpoint : ['rd : x6', 'rs1 : f1']
Last Code Sequence : 
	-[0x8000020c]:fcvt.w.d t1, ft1, dyn
	-[0x80000210]:csrrs a7, fflags, zero
	-[0x80000214]:sw t1, 80(a5)
Current Store : [0x80000218] : sw a7, 84(a5) -- Store: [0x80002384]:0x00000010




Last Coverpoint : ['rd : x5', 'rs1 : f20']
Last Code Sequence : 
	-[0x80000224]:fcvt.w.d t0, fs4, dyn
	-[0x80000228]:csrrs a7, fflags, zero
	-[0x8000022c]:sw t0, 96(a5)
Current Store : [0x80000230] : sw a7, 100(a5) -- Store: [0x80002394]:0x00000010




Last Coverpoint : ['rd : x10', 'rs1 : f23']
Last Code Sequence : 
	-[0x8000023c]:fcvt.w.d a0, fs7, dyn
	-[0x80000240]:csrrs a7, fflags, zero
	-[0x80000244]:sw a0, 112(a5)
Current Store : [0x80000248] : sw a7, 116(a5) -- Store: [0x800023a4]:0x00000010




Last Coverpoint : ['rd : x2', 'rs1 : f11']
Last Code Sequence : 
	-[0x80000254]:fcvt.w.d sp, fa1, dyn
	-[0x80000258]:csrrs a7, fflags, zero
	-[0x8000025c]:sw sp, 128(a5)
Current Store : [0x80000260] : sw a7, 132(a5) -- Store: [0x800023b4]:0x00000010




Last Coverpoint : ['rd : x9', 'rs1 : f25']
Last Code Sequence : 
	-[0x8000026c]:fcvt.w.d s1, fs9, dyn
	-[0x80000270]:csrrs a7, fflags, zero
	-[0x80000274]:sw s1, 144(a5)
Current Store : [0x80000278] : sw a7, 148(a5) -- Store: [0x800023c4]:0x00000010




Last Coverpoint : ['rd : x0', 'rs1 : f15']
Last Code Sequence : 
	-[0x80000284]:fcvt.w.d zero, fa5, dyn
	-[0x80000288]:csrrs a7, fflags, zero
	-[0x8000028c]:sw zero, 160(a5)
Current Store : [0x80000290] : sw a7, 164(a5) -- Store: [0x800023d4]:0x00000010




Last Coverpoint : ['rd : x28', 'rs1 : f26']
Last Code Sequence : 
	-[0x8000029c]:fcvt.w.d t3, fs10, dyn
	-[0x800002a0]:csrrs a7, fflags, zero
	-[0x800002a4]:sw t3, 176(a5)
Current Store : [0x800002a8] : sw a7, 180(a5) -- Store: [0x800023e4]:0x00000010




Last Coverpoint : ['rd : x8', 'rs1 : f0']
Last Code Sequence : 
	-[0x800002b4]:fcvt.w.d fp, ft0, dyn
	-[0x800002b8]:csrrs a7, fflags, zero
	-[0x800002bc]:sw fp, 192(a5)
Current Store : [0x800002c0] : sw a7, 196(a5) -- Store: [0x800023f4]:0x00000010




Last Coverpoint : ['rd : x3', 'rs1 : f7']
Last Code Sequence : 
	-[0x800002cc]:fcvt.w.d gp, ft7, dyn
	-[0x800002d0]:csrrs a7, fflags, zero
	-[0x800002d4]:sw gp, 208(a5)
Current Store : [0x800002d8] : sw a7, 212(a5) -- Store: [0x80002404]:0x00000010




Last Coverpoint : ['rd : x15', 'rs1 : f16']
Last Code Sequence : 
	-[0x80000368]:fcvt.w.d a5, fa6, dyn
	-[0x8000036c]:csrrs s5, fflags, zero
	-[0x80000370]:sw a5, 0(s3)
Current Store : [0x80000374] : sw s5, 4(s3) -- Store: [0x800023cc]:0x00000010




Last Coverpoint : ['rd : x30', 'rs1 : f29']
Last Code Sequence : 
	-[0x8000038c]:fcvt.w.d t5, ft9, dyn
	-[0x80000390]:csrrs a7, fflags, zero
	-[0x80000394]:sw t5, 0(a5)
Current Store : [0x80000398] : sw a7, 4(a5) -- Store: [0x800023d4]:0x00000010




Last Coverpoint : ['rd : x11', 'rs1 : f4']
Last Code Sequence : 
	-[0x800003a4]:fcvt.w.d a1, ft4, dyn
	-[0x800003a8]:csrrs a7, fflags, zero
	-[0x800003ac]:sw a1, 16(a5)
Current Store : [0x800003b0] : sw a7, 20(a5) -- Store: [0x800023e4]:0x00000010




Last Coverpoint : ['rd : x25', 'rs1 : f9']
Last Code Sequence : 
	-[0x800003bc]:fcvt.w.d s9, fs1, dyn
	-[0x800003c0]:csrrs a7, fflags, zero
	-[0x800003c4]:sw s9, 32(a5)
Current Store : [0x800003c8] : sw a7, 36(a5) -- Store: [0x800023f4]:0x00000010




Last Coverpoint : ['rd : x27', 'rs1 : f17']
Last Code Sequence : 
	-[0x800003d4]:fcvt.w.d s11, fa7, dyn
	-[0x800003d8]:csrrs a7, fflags, zero
	-[0x800003dc]:sw s11, 48(a5)
Current Store : [0x800003e0] : sw a7, 52(a5) -- Store: [0x80002404]:0x00000010




Last Coverpoint : ['rd : x16', 'rs1 : f2']
Last Code Sequence : 
	-[0x800003f8]:fcvt.w.d a6, ft2, dyn
	-[0x800003fc]:csrrs s5, fflags, zero
	-[0x80000400]:sw a6, 0(s3)
Current Store : [0x80000404] : sw s5, 4(s3) -- Store: [0x800023f4]:0x00000010




Last Coverpoint : ['rd : x21', 'rs1 : f21']
Last Code Sequence : 
	-[0x8000041c]:fcvt.w.d s5, fs5, dyn
	-[0x80000420]:csrrs a7, fflags, zero
	-[0x80000424]:sw s5, 0(a5)
Current Store : [0x80000428] : sw a7, 4(a5) -- Store: [0x800023fc]:0x00000010




Last Coverpoint : ['rd : x1', 'rs1 : f6']
Last Code Sequence : 
	-[0x80000434]:fcvt.w.d ra, ft6, dyn
	-[0x80000438]:csrrs a7, fflags, zero
	-[0x8000043c]:sw ra, 16(a5)
Current Store : [0x80000440] : sw a7, 20(a5) -- Store: [0x8000240c]:0x00000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                               coverpoints                                                                |                                                       code                                                        |
|---:|--------------------------|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x7FFFFFFF|- opcode : fcvt.w.d<br> - rd : x26<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.d s10, fa0, dyn<br> [0x80000120]:csrrs a7, fflags, zero<br> [0x80000124]:sw s10, 0(a5)<br>     |
|   2|[0x80002320]<br>0x7FFFFFFF|- rd : x22<br> - rs1 : f8<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat<br>                          |[0x80000134]:fcvt.w.d s6, fs0, dyn<br> [0x80000138]:csrrs a7, fflags, zero<br> [0x8000013c]:sw s6, 16(a5)<br>      |
|   3|[0x80002330]<br>0x7FFFFFFF|- rd : x29<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat<br>                         |[0x8000014c]:fcvt.w.d t4, fa2, dyn<br> [0x80000150]:csrrs a7, fflags, zero<br> [0x80000154]:sw t4, 32(a5)<br>      |
|   4|[0x80002328]<br>0x7FFFFFFF|- rd : x17<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                         |[0x80000170]:fcvt.w.d a7, ft11, dyn<br> [0x80000174]:csrrs s5, fflags, zero<br> [0x80000178]:sw a7, 0(s3)<br>      |
|   5|[0x80002330]<br>0x7FFFFFFF|- rd : x13<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                         |[0x80000194]:fcvt.w.d a3, fs3, dyn<br> [0x80000198]:csrrs a7, fflags, zero<br> [0x8000019c]:sw a3, 0(a5)<br>       |
|   6|[0x80002340]<br>0x7FFFFFFF|- rd : x24<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat<br>                         |[0x800001ac]:fcvt.w.d s8, fa3, dyn<br> [0x800001b0]:csrrs a7, fflags, zero<br> [0x800001b4]:sw s8, 16(a5)<br>      |
|   7|[0x80002350]<br>0x7FFFFFFF|- rd : x4<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat<br>                          |[0x800001c4]:fcvt.w.d tp, fs11, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:sw tp, 32(a5)<br>     |
|   8|[0x80002360]<br>0x7FFFFFFF|- rd : x18<br> - rs1 : f3<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x800001dc]:fcvt.w.d s2, ft3, dyn<br> [0x800001e0]:csrrs a7, fflags, zero<br> [0x800001e4]:sw s2, 48(a5)<br>      |
|   9|[0x80002370]<br>0x00000000|- rd : x20<br> - rs1 : f22<br>                                                                                                            |[0x800001f4]:fcvt.w.d s4, fs6, dyn<br> [0x800001f8]:csrrs a7, fflags, zero<br> [0x800001fc]:sw s4, 64(a5)<br>      |
|  10|[0x80002380]<br>0x00000000|- rd : x6<br> - rs1 : f1<br>                                                                                                              |[0x8000020c]:fcvt.w.d t1, ft1, dyn<br> [0x80000210]:csrrs a7, fflags, zero<br> [0x80000214]:sw t1, 80(a5)<br>      |
|  11|[0x80002390]<br>0x00000000|- rd : x5<br> - rs1 : f20<br>                                                                                                             |[0x80000224]:fcvt.w.d t0, fs4, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:sw t0, 96(a5)<br>      |
|  12|[0x800023a0]<br>0x00000000|- rd : x10<br> - rs1 : f23<br>                                                                                                            |[0x8000023c]:fcvt.w.d a0, fs7, dyn<br> [0x80000240]:csrrs a7, fflags, zero<br> [0x80000244]:sw a0, 112(a5)<br>     |
|  13|[0x800023b0]<br>0x00000000|- rd : x2<br> - rs1 : f11<br>                                                                                                             |[0x80000254]:fcvt.w.d sp, fa1, dyn<br> [0x80000258]:csrrs a7, fflags, zero<br> [0x8000025c]:sw sp, 128(a5)<br>     |
|  14|[0x800023c0]<br>0x00000000|- rd : x9<br> - rs1 : f25<br>                                                                                                             |[0x8000026c]:fcvt.w.d s1, fs9, dyn<br> [0x80000270]:csrrs a7, fflags, zero<br> [0x80000274]:sw s1, 144(a5)<br>     |
|  15|[0x800023d0]<br>0x00000000|- rd : x0<br> - rs1 : f15<br>                                                                                                             |[0x80000284]:fcvt.w.d zero, fa5, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:sw zero, 160(a5)<br> |
|  16|[0x800023e0]<br>0x00000000|- rd : x28<br> - rs1 : f26<br>                                                                                                            |[0x8000029c]:fcvt.w.d t3, fs10, dyn<br> [0x800002a0]:csrrs a7, fflags, zero<br> [0x800002a4]:sw t3, 176(a5)<br>    |
|  17|[0x800023f0]<br>0x00000000|- rd : x8<br> - rs1 : f0<br>                                                                                                              |[0x800002b4]:fcvt.w.d fp, ft0, dyn<br> [0x800002b8]:csrrs a7, fflags, zero<br> [0x800002bc]:sw fp, 192(a5)<br>     |
|  18|[0x80002400]<br>0x00000000|- rd : x3<br> - rs1 : f7<br>                                                                                                              |[0x800002cc]:fcvt.w.d gp, ft7, dyn<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw gp, 208(a5)<br>     |
|  19|[0x80002410]<br>0x00000000|- rd : x14<br> - rs1 : f28<br>                                                                                                            |[0x800002e4]:fcvt.w.d a4, ft8, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:sw a4, 224(a5)<br>     |
|  20|[0x800023c8]<br>0x00000000|- rd : x15<br> - rs1 : f16<br>                                                                                                            |[0x80000368]:fcvt.w.d a5, fa6, dyn<br> [0x8000036c]:csrrs s5, fflags, zero<br> [0x80000370]:sw a5, 0(s3)<br>       |
|  21|[0x800023d0]<br>0x00000000|- rd : x30<br> - rs1 : f29<br>                                                                                                            |[0x8000038c]:fcvt.w.d t5, ft9, dyn<br> [0x80000390]:csrrs a7, fflags, zero<br> [0x80000394]:sw t5, 0(a5)<br>       |
|  22|[0x800023e0]<br>0x00000000|- rd : x11<br> - rs1 : f4<br>                                                                                                             |[0x800003a4]:fcvt.w.d a1, ft4, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:sw a1, 16(a5)<br>      |
|  23|[0x800023f0]<br>0x00000000|- rd : x25<br> - rs1 : f9<br>                                                                                                             |[0x800003bc]:fcvt.w.d s9, fs1, dyn<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:sw s9, 32(a5)<br>      |
|  24|[0x80002400]<br>0x00000000|- rd : x27<br> - rs1 : f17<br>                                                                                                            |[0x800003d4]:fcvt.w.d s11, fa7, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw s11, 48(a5)<br>    |
|  25|[0x800023f0]<br>0x00000000|- rd : x16<br> - rs1 : f2<br>                                                                                                             |[0x800003f8]:fcvt.w.d a6, ft2, dyn<br> [0x800003fc]:csrrs s5, fflags, zero<br> [0x80000400]:sw a6, 0(s3)<br>       |
|  26|[0x800023f8]<br>0x00000000|- rd : x21<br> - rs1 : f21<br>                                                                                                            |[0x8000041c]:fcvt.w.d s5, fs5, dyn<br> [0x80000420]:csrrs a7, fflags, zero<br> [0x80000424]:sw s5, 0(a5)<br>       |
|  27|[0x80002408]<br>0x00000000|- rd : x1<br> - rs1 : f6<br>                                                                                                              |[0x80000434]:fcvt.w.d ra, ft6, dyn<br> [0x80000438]:csrrs a7, fflags, zero<br> [0x8000043c]:sw ra, 16(a5)<br>      |
