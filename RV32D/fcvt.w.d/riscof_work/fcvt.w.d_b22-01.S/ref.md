
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000510')]      |
| SIG_REGION                | [('0x80002310', '0x80002460', '84 words')]      |
| COV_LABELS                | fcvt.w.d_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fcvt.w.d/riscof_work/fcvt.w.d_b22-01.S/ref.S    |
| Total Number of coverpoints| 109     |
| Total Coverpoints Hit     | 85      |
| Total Signature Updates   | 56      |
| STAT1                     | 28      |
| STAT2                     | 0      |
| STAT3                     | 12     |
| STAT4                     | 28     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003e0]:fcvt.w.d t1, fa1, dyn
[0x800003e4]:csrrs a7, fflags, zero
[0x800003e8]:sw t1, 240(a5)
[0x800003ec]:sw a7, 244(a5)
[0x800003f0]:fld fa0, 232(a6)
[0x800003f4]:csrrwi zero, frm, 0

[0x800003f8]:fcvt.w.d tp, fa0, dyn
[0x800003fc]:csrrs a7, fflags, zero
[0x80000400]:sw tp, 256(a5)
[0x80000404]:sw a7, 260(a5)
[0x80000408]:fld fs0, 240(a6)
[0x8000040c]:csrrwi zero, frm, 0

[0x80000410]:fcvt.w.d s10, fs0, dyn
[0x80000414]:csrrs a7, fflags, zero
[0x80000418]:sw s10, 272(a5)
[0x8000041c]:sw a7, 276(a5)
[0x80000420]:fld ft9, 248(a6)
[0x80000424]:csrrwi zero, frm, 0

[0x80000428]:fcvt.w.d ra, ft9, dyn
[0x8000042c]:csrrs a7, fflags, zero
[0x80000430]:sw ra, 288(a5)
[0x80000434]:sw a7, 292(a5)
[0x80000438]:fld ft11, 256(a6)
[0x8000043c]:csrrwi zero, frm, 0

[0x80000440]:fcvt.w.d t6, ft11, dyn
[0x80000444]:csrrs a7, fflags, zero
[0x80000448]:sw t6, 304(a5)
[0x8000044c]:sw a7, 308(a5)
[0x80000450]:fld ft11, 264(a6)
[0x80000454]:csrrwi zero, frm, 0

[0x80000458]:fcvt.w.d t6, ft11, dyn
[0x8000045c]:csrrs a7, fflags, zero
[0x80000460]:sw t6, 320(a5)
[0x80000464]:sw a7, 324(a5)
[0x80000468]:fld ft11, 272(a6)
[0x8000046c]:csrrwi zero, frm, 0

[0x80000470]:fcvt.w.d t6, ft11, dyn
[0x80000474]:csrrs a7, fflags, zero
[0x80000478]:sw t6, 336(a5)
[0x8000047c]:sw a7, 340(a5)
[0x80000480]:fld ft11, 280(a6)
[0x80000484]:csrrwi zero, frm, 0

[0x80000488]:fcvt.w.d t6, ft11, dyn
[0x8000048c]:csrrs a7, fflags, zero
[0x80000490]:sw t6, 352(a5)
[0x80000494]:sw a7, 356(a5)
[0x80000498]:fld ft11, 288(a6)
[0x8000049c]:csrrwi zero, frm, 0

[0x800004a0]:fcvt.w.d t6, ft11, dyn
[0x800004a4]:csrrs a7, fflags, zero
[0x800004a8]:sw t6, 368(a5)
[0x800004ac]:sw a7, 372(a5)
[0x800004b0]:fld ft11, 296(a6)
[0x800004b4]:csrrwi zero, frm, 0

[0x800004b8]:fcvt.w.d t6, ft11, dyn
[0x800004bc]:csrrs a7, fflags, zero
[0x800004c0]:sw t6, 384(a5)
[0x800004c4]:sw a7, 388(a5)
[0x800004c8]:fld ft11, 304(a6)
[0x800004cc]:csrrwi zero, frm, 0

[0x800004d0]:fcvt.w.d t6, ft11, dyn
[0x800004d4]:csrrs a7, fflags, zero
[0x800004d8]:sw t6, 400(a5)
[0x800004dc]:sw a7, 404(a5)
[0x800004e0]:fld ft11, 312(a6)
[0x800004e4]:csrrwi zero, frm, 0

[0x800004e8]:fcvt.w.d t6, ft11, dyn
[0x800004ec]:csrrs a7, fflags, zero
[0x800004f0]:sw t6, 416(a5)
[0x800004f4]:sw a7, 420(a5)
[0x800004f8]:fld ft11, 320(a6)
[0x800004fc]:csrrwi zero, frm, 0



```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x15', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000011c]:fcvt.w.d a5, ft0, dyn
	-[0x80000120]:csrrs s5, fflags, zero
	-[0x80000124]:sw a5, 0(s3)
Current Store : [0x80000128] : sw s5, 4(s3) -- Store: [0x80002314]:0x00000001




Last Coverpoint : ['rd : x16', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x5ca and fm1 == 0xf871c6ee84270 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000134]:fcvt.w.d a6, fa4, dyn
	-[0x80000138]:csrrs s5, fflags, zero
	-[0x8000013c]:sw a6, 16(s3)
Current Store : [0x80000140] : sw s5, 20(s3) -- Store: [0x80002324]:0x00000011




Last Coverpoint : ['rd : x18', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x3ca and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000158]:fcvt.w.d s2, fa3, dyn
	-[0x8000015c]:csrrs a7, fflags, zero
	-[0x80000160]:sw s2, 0(a5)
Current Store : [0x80000164] : sw a7, 4(a5) -- Store: [0x80002324]:0x00000011




Last Coverpoint : ['rd : x24', 'rs1 : f24', 'fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000170]:fcvt.w.d s8, fs8, dyn
	-[0x80000174]:csrrs a7, fflags, zero
	-[0x80000178]:sw s8, 16(a5)
Current Store : [0x8000017c] : sw a7, 20(a5) -- Store: [0x80002334]:0x00000011




Last Coverpoint : ['rd : x13', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000188]:fcvt.w.d a3, ft8, dyn
	-[0x8000018c]:csrrs a7, fflags, zero
	-[0x80000190]:sw a3, 32(a5)
Current Store : [0x80000194] : sw a7, 36(a5) -- Store: [0x80002344]:0x00000011




Last Coverpoint : ['rd : x29', 'rs1 : f20', 'fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001a0]:fcvt.w.d t4, fs4, dyn
	-[0x800001a4]:csrrs a7, fflags, zero
	-[0x800001a8]:sw t4, 48(a5)
Current Store : [0x800001ac] : sw a7, 52(a5) -- Store: [0x80002354]:0x00000011




Last Coverpoint : ['rd : x30', 'rs1 : f9', 'fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001b8]:fcvt.w.d t5, fs1, dyn
	-[0x800001bc]:csrrs a7, fflags, zero
	-[0x800001c0]:sw t5, 64(a5)
Current Store : [0x800001c4] : sw a7, 68(a5) -- Store: [0x80002364]:0x00000011




Last Coverpoint : ['rd : x2', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001d0]:fcvt.w.d sp, ft4, dyn
	-[0x800001d4]:csrrs a7, fflags, zero
	-[0x800001d8]:sw sp, 80(a5)
Current Store : [0x800001dc] : sw a7, 84(a5) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x23', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800001e8]:fcvt.w.d s7, ft2, dyn
	-[0x800001ec]:csrrs a7, fflags, zero
	-[0x800001f0]:sw s7, 96(a5)
Current Store : [0x800001f4] : sw a7, 100(a5) -- Store: [0x80002384]:0x00000011




Last Coverpoint : ['rd : x3', 'rs1 : f25', 'fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000200]:fcvt.w.d gp, fs9, dyn
	-[0x80000204]:csrrs a7, fflags, zero
	-[0x80000208]:sw gp, 112(a5)
Current Store : [0x8000020c] : sw a7, 116(a5) -- Store: [0x80002394]:0x00000011




Last Coverpoint : ['rd : x10', 'rs1 : f27', 'fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000218]:fcvt.w.d a0, fs11, dyn
	-[0x8000021c]:csrrs a7, fflags, zero
	-[0x80000220]:sw a0, 128(a5)
Current Store : [0x80000224] : sw a7, 132(a5) -- Store: [0x800023a4]:0x00000011




Last Coverpoint : ['rd : x20', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000230]:fcvt.w.d s4, ft11, dyn
	-[0x80000234]:csrrs a7, fflags, zero
	-[0x80000238]:sw s4, 144(a5)
Current Store : [0x8000023c] : sw a7, 148(a5) -- Store: [0x800023b4]:0x00000011




Last Coverpoint : ['rd : x17', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000254]:fcvt.w.d a7, ft3, dyn
	-[0x80000258]:csrrs s5, fflags, zero
	-[0x8000025c]:sw a7, 0(s3)
Current Store : [0x80000260] : sw s5, 4(s3) -- Store: [0x80002374]:0x00000011




Last Coverpoint : ['rd : x9', 'rs1 : f22', 'fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000278]:fcvt.w.d s1, fs6, dyn
	-[0x8000027c]:csrrs a7, fflags, zero
	-[0x80000280]:sw s1, 0(a5)
Current Store : [0x80000284] : sw a7, 4(a5) -- Store: [0x8000237c]:0x00000011




Last Coverpoint : ['rd : x21', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000290]:fcvt.w.d s5, ft10, dyn
	-[0x80000294]:csrrs a7, fflags, zero
	-[0x80000298]:sw s5, 16(a5)
Current Store : [0x8000029c] : sw a7, 20(a5) -- Store: [0x8000238c]:0x00000011




Last Coverpoint : ['rd : x5', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002a8]:fcvt.w.d t0, fa2, dyn
	-[0x800002ac]:csrrs a7, fflags, zero
	-[0x800002b0]:sw t0, 32(a5)
Current Store : [0x800002b4] : sw a7, 36(a5) -- Store: [0x8000239c]:0x00000011




Last Coverpoint : ['rd : x8', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002c0]:fcvt.w.d fp, fa5, dyn
	-[0x800002c4]:csrrs a7, fflags, zero
	-[0x800002c8]:sw fp, 48(a5)
Current Store : [0x800002cc] : sw a7, 52(a5) -- Store: [0x800023ac]:0x00000011




Last Coverpoint : ['rd : x27', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002d8]:fcvt.w.d s11, fs3, dyn
	-[0x800002dc]:csrrs a7, fflags, zero
	-[0x800002e0]:sw s11, 64(a5)
Current Store : [0x800002e4] : sw a7, 68(a5) -- Store: [0x800023bc]:0x00000011




Last Coverpoint : ['rd : x0', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800002f0]:fcvt.w.d zero, ft1, dyn
	-[0x800002f4]:csrrs a7, fflags, zero
	-[0x800002f8]:sw zero, 80(a5)
Current Store : [0x800002fc] : sw a7, 84(a5) -- Store: [0x800023cc]:0x00000011




Last Coverpoint : ['rd : x11', 'rs1 : f21', 'fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000308]:fcvt.w.d a1, fs5, dyn
	-[0x8000030c]:csrrs a7, fflags, zero
	-[0x80000310]:sw a1, 96(a5)
Current Store : [0x80000314] : sw a7, 100(a5) -- Store: [0x800023dc]:0x00000011




Last Coverpoint : ['rd : x19', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000320]:fcvt.w.d s3, fa7, dyn
	-[0x80000324]:csrrs a7, fflags, zero
	-[0x80000328]:sw s3, 112(a5)
Current Store : [0x8000032c] : sw a7, 116(a5) -- Store: [0x800023ec]:0x00000011




Last Coverpoint : ['rd : x12', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000338]:fcvt.w.d a2, ft6, dyn
	-[0x8000033c]:csrrs a7, fflags, zero
	-[0x80000340]:sw a2, 128(a5)
Current Store : [0x80000344] : sw a7, 132(a5) -- Store: [0x800023fc]:0x00000011




Last Coverpoint : ['rd : x25', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000350]:fcvt.w.d s9, ft5, dyn
	-[0x80000354]:csrrs a7, fflags, zero
	-[0x80000358]:sw s9, 144(a5)
Current Store : [0x8000035c] : sw a7, 148(a5) -- Store: [0x8000240c]:0x00000011




Last Coverpoint : ['rd : x22', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000368]:fcvt.w.d s6, fs2, dyn
	-[0x8000036c]:csrrs a7, fflags, zero
	-[0x80000370]:sw s6, 160(a5)
Current Store : [0x80000374] : sw a7, 164(a5) -- Store: [0x8000241c]:0x00000011




Last Coverpoint : ['rd : x7', 'rs1 : f7', 'fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000380]:fcvt.w.d t2, ft7, dyn
	-[0x80000384]:csrrs a7, fflags, zero
	-[0x80000388]:sw t2, 176(a5)
Current Store : [0x8000038c] : sw a7, 180(a5) -- Store: [0x8000242c]:0x00000011




Last Coverpoint : ['rd : x28', 'rs1 : f23', 'fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000398]:fcvt.w.d t3, fs7, dyn
	-[0x8000039c]:csrrs a7, fflags, zero
	-[0x800003a0]:sw t3, 192(a5)
Current Store : [0x800003a4] : sw a7, 196(a5) -- Store: [0x8000243c]:0x00000011




Last Coverpoint : ['rd : x14', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b0]:fcvt.w.d a4, fa6, dyn
	-[0x800003b4]:csrrs a7, fflags, zero
	-[0x800003b8]:sw a4, 208(a5)
Current Store : [0x800003bc] : sw a7, 212(a5) -- Store: [0x8000244c]:0x00000011




Last Coverpoint : ['rd : x31', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003c8]:fcvt.w.d t6, fs10, dyn
	-[0x800003cc]:csrrs a7, fflags, zero
	-[0x800003d0]:sw t6, 224(a5)
Current Store : [0x800003d4] : sw a7, 228(a5) -- Store: [0x8000245c]:0x00000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                               coverpoints                                                               |                                                       code                                                       |
|---:|--------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x00000000|- opcode : fcvt.w.d<br> - rd : x15<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat<br> |[0x8000011c]:fcvt.w.d a5, ft0, dyn<br> [0x80000120]:csrrs s5, fflags, zero<br> [0x80000124]:sw a5, 0(s3)<br>      |
|   2|[0x80002320]<br>0x7FFFFFFF|- rd : x16<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x5ca and fm1 == 0xf871c6ee84270 and rm_val == 0  #nosat<br>                        |[0x80000134]:fcvt.w.d a6, fa4, dyn<br> [0x80000138]:csrrs s5, fflags, zero<br> [0x8000013c]:sw a6, 16(s3)<br>     |
|   3|[0x80002320]<br>0x00000000|- rd : x18<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x3ca and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat<br>                        |[0x80000158]:fcvt.w.d s2, fa3, dyn<br> [0x8000015c]:csrrs a7, fflags, zero<br> [0x80000160]:sw s2, 0(a5)<br>      |
|   4|[0x80002330]<br>0x80000000|- rd : x24<br> - rs1 : f24<br> - fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat<br>                        |[0x80000170]:fcvt.w.d s8, fs8, dyn<br> [0x80000174]:csrrs a7, fflags, zero<br> [0x80000178]:sw s8, 16(a5)<br>     |
|   5|[0x80002340]<br>0x7FFFFFFF|- rd : x13<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat<br>                        |[0x80000188]:fcvt.w.d a3, ft8, dyn<br> [0x8000018c]:csrrs a7, fflags, zero<br> [0x80000190]:sw a3, 32(a5)<br>     |
|   6|[0x80002350]<br>0x80000000|- rd : x29<br> - rs1 : f20<br> - fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat<br>                        |[0x800001a0]:fcvt.w.d t4, fs4, dyn<br> [0x800001a4]:csrrs a7, fflags, zero<br> [0x800001a8]:sw t4, 48(a5)<br>     |
|   7|[0x80002360]<br>0x80000000|- rd : x30<br> - rs1 : f9<br> - fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat<br>                         |[0x800001b8]:fcvt.w.d t5, fs1, dyn<br> [0x800001bc]:csrrs a7, fflags, zero<br> [0x800001c0]:sw t5, 64(a5)<br>     |
|   8|[0x80002370]<br>0x9BB26A76|- rd : x2<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat<br>                          |[0x800001d0]:fcvt.w.d sp, ft4, dyn<br> [0x800001d4]:csrrs a7, fflags, zero<br> [0x800001d8]:sw sp, 80(a5)<br>     |
|   9|[0x80002380]<br>0x229723B6|- rd : x23<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat<br>                         |[0x800001e8]:fcvt.w.d s7, ft2, dyn<br> [0x800001ec]:csrrs a7, fflags, zero<br> [0x800001f0]:sw s7, 96(a5)<br>     |
|  10|[0x80002390]<br>0xE776D9EE|- rd : x3<br> - rs1 : f25<br> - fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat<br>                         |[0x80000200]:fcvt.w.d gp, fs9, dyn<br> [0x80000204]:csrrs a7, fflags, zero<br> [0x80000208]:sw gp, 112(a5)<br>    |
|  11|[0x800023a0]<br>0xF3258617|- rd : x10<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat<br>                        |[0x80000218]:fcvt.w.d a0, fs11, dyn<br> [0x8000021c]:csrrs a7, fflags, zero<br> [0x80000220]:sw a0, 128(a5)<br>   |
|  12|[0x800023b0]<br>0x05FC8582|- rd : x20<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat<br>                        |[0x80000230]:fcvt.w.d s4, ft11, dyn<br> [0x80000234]:csrrs a7, fflags, zero<br> [0x80000238]:sw s4, 144(a5)<br>   |
|  13|[0x80002370]<br>0x027A0C2D|- rd : x17<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat<br>                         |[0x80000254]:fcvt.w.d a7, ft3, dyn<br> [0x80000258]:csrrs s5, fflags, zero<br> [0x8000025c]:sw a7, 0(s3)<br>      |
|  14|[0x80002378]<br>0xFEC69453|- rd : x9<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat<br>                         |[0x80000278]:fcvt.w.d s1, fs6, dyn<br> [0x8000027c]:csrrs a7, fflags, zero<br> [0x80000280]:sw s1, 0(a5)<br>      |
|  15|[0x80002388]<br>0x00C03ED7|- rd : x21<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat<br>                        |[0x80000290]:fcvt.w.d s5, ft10, dyn<br> [0x80000294]:csrrs a7, fflags, zero<br> [0x80000298]:sw s5, 16(a5)<br>    |
|  16|[0x80002398]<br>0x00656937|- rd : x5<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat<br>                         |[0x800002a8]:fcvt.w.d t0, fa2, dyn<br> [0x800002ac]:csrrs a7, fflags, zero<br> [0x800002b0]:sw t0, 32(a5)<br>     |
|  17|[0x800023a8]<br>0x002F0A07|- rd : x8<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat<br>                         |[0x800002c0]:fcvt.w.d fp, fa5, dyn<br> [0x800002c4]:csrrs a7, fflags, zero<br> [0x800002c8]:sw fp, 48(a5)<br>     |
|  18|[0x800023b8]<br>0x0018C8A2|- rd : x27<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat<br>                        |[0x800002d8]:fcvt.w.d s11, fs3, dyn<br> [0x800002dc]:csrrs a7, fflags, zero<br> [0x800002e0]:sw s11, 64(a5)<br>   |
|  19|[0x800023c8]<br>0x00000000|- rd : x0<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat<br>                          |[0x800002f0]:fcvt.w.d zero, ft1, dyn<br> [0x800002f4]:csrrs a7, fflags, zero<br> [0x800002f8]:sw zero, 80(a5)<br> |
|  20|[0x800023d8]<br>0xFFFA8911|- rd : x11<br> - rs1 : f21<br> - fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat<br>                        |[0x80000308]:fcvt.w.d a1, fs5, dyn<br> [0x8000030c]:csrrs a7, fflags, zero<br> [0x80000310]:sw a1, 96(a5)<br>     |
|  21|[0x800023e8]<br>0x0003D1B6|- rd : x19<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat<br>                        |[0x80000320]:fcvt.w.d s3, fa7, dyn<br> [0x80000324]:csrrs a7, fflags, zero<br> [0x80000328]:sw s3, 112(a5)<br>    |
|  22|[0x800023f8]<br>0x0001224C|- rd : x12<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat<br>                         |[0x80000338]:fcvt.w.d a2, ft6, dyn<br> [0x8000033c]:csrrs a7, fflags, zero<br> [0x80000340]:sw a2, 128(a5)<br>    |
|  23|[0x80002408]<br>0x0000CA9E|- rd : x25<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat<br>                         |[0x80000350]:fcvt.w.d s9, ft5, dyn<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:sw s9, 144(a5)<br>    |
|  24|[0x80002418]<br>0x00006741|- rd : x22<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat<br>                        |[0x80000368]:fcvt.w.d s6, fs2, dyn<br> [0x8000036c]:csrrs a7, fflags, zero<br> [0x80000370]:sw s6, 160(a5)<br>    |
|  25|[0x80002428]<br>0xFFFFD857|- rd : x7<br> - rs1 : f7<br> - fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat<br>                          |[0x80000380]:fcvt.w.d t2, ft7, dyn<br> [0x80000384]:csrrs a7, fflags, zero<br> [0x80000388]:sw t2, 176(a5)<br>    |
|  26|[0x80002438]<br>0xFFFFE3B7|- rd : x28<br> - rs1 : f23<br> - fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat<br>                        |[0x80000398]:fcvt.w.d t3, fs7, dyn<br> [0x8000039c]:csrrs a7, fflags, zero<br> [0x800003a0]:sw t3, 192(a5)<br>    |
|  27|[0x80002448]<br>0x00000AE7|- rd : x14<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat<br>                        |[0x800003b0]:fcvt.w.d a4, fa6, dyn<br> [0x800003b4]:csrrs a7, fflags, zero<br> [0x800003b8]:sw a4, 208(a5)<br>    |
|  28|[0x80002458]<br>0x000006BE|- rd : x31<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat<br>                        |[0x800003c8]:fcvt.w.d t6, fs10, dyn<br> [0x800003cc]:csrrs a7, fflags, zero<br> [0x800003d0]:sw t6, 224(a5)<br>   |
