
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80001c70')]      |
| SIG_REGION                | [('0x80004610', '0x80004ce0', '436 words')]      |
| COV_LABELS                | fmsub_b5      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fmsub1/riscof_work/fmsub_b5-01.S/ref.S    |
| Total Number of coverpoints| 358     |
| Total Coverpoints Hit     | 288      |
| Total Signature Updates   | 155      |
| STAT1                     | 155      |
| STAT2                     | 0      |
| STAT3                     | 62     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000ec8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000ecc]:csrrs a7, fflags, zero
[0x80000ed0]:fsd ft11, 1744(a5)
[0x80000ed4]:sw a7, 1748(a5)
[0x80000ed8]:fld ft10, 600(a6)
[0x80000edc]:fld ft9, 608(a6)
[0x80000ee0]:fld ft8, 616(a6)
[0x80000ee4]:csrrwi zero, frm, 0

[0x80000ee8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000eec]:csrrs a7, fflags, zero
[0x80000ef0]:fsd ft11, 1760(a5)
[0x80000ef4]:sw a7, 1764(a5)
[0x80000ef8]:fld ft10, 624(a6)
[0x80000efc]:fld ft9, 632(a6)
[0x80000f00]:fld ft8, 640(a6)
[0x80000f04]:csrrwi zero, frm, 4

[0x80000f08]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f0c]:csrrs a7, fflags, zero
[0x80000f10]:fsd ft11, 1776(a5)
[0x80000f14]:sw a7, 1780(a5)
[0x80000f18]:fld ft10, 648(a6)
[0x80000f1c]:fld ft9, 656(a6)
[0x80000f20]:fld ft8, 664(a6)
[0x80000f24]:csrrwi zero, frm, 3

[0x80000f28]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f2c]:csrrs a7, fflags, zero
[0x80000f30]:fsd ft11, 1792(a5)
[0x80000f34]:sw a7, 1796(a5)
[0x80000f38]:fld ft10, 672(a6)
[0x80000f3c]:fld ft9, 680(a6)
[0x80000f40]:fld ft8, 688(a6)
[0x80000f44]:csrrwi zero, frm, 2

[0x80000f48]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f4c]:csrrs a7, fflags, zero
[0x80000f50]:fsd ft11, 1808(a5)
[0x80000f54]:sw a7, 1812(a5)
[0x80000f58]:fld ft10, 696(a6)
[0x80000f5c]:fld ft9, 704(a6)
[0x80000f60]:fld ft8, 712(a6)
[0x80000f64]:csrrwi zero, frm, 1

[0x80000f68]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f6c]:csrrs a7, fflags, zero
[0x80000f70]:fsd ft11, 1824(a5)
[0x80000f74]:sw a7, 1828(a5)
[0x80000f78]:fld ft10, 720(a6)
[0x80000f7c]:fld ft9, 728(a6)
[0x80000f80]:fld ft8, 736(a6)
[0x80000f84]:csrrwi zero, frm, 0

[0x80000f88]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000f8c]:csrrs a7, fflags, zero
[0x80000f90]:fsd ft11, 1840(a5)
[0x80000f94]:sw a7, 1844(a5)
[0x80000f98]:fld ft10, 744(a6)
[0x80000f9c]:fld ft9, 752(a6)
[0x80000fa0]:fld ft8, 760(a6)
[0x80000fa4]:csrrwi zero, frm, 4

[0x80000fa8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fac]:csrrs a7, fflags, zero
[0x80000fb0]:fsd ft11, 1856(a5)
[0x80000fb4]:sw a7, 1860(a5)
[0x80000fb8]:fld ft10, 768(a6)
[0x80000fbc]:fld ft9, 776(a6)
[0x80000fc0]:fld ft8, 784(a6)
[0x80000fc4]:csrrwi zero, frm, 3

[0x80000fc8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fcc]:csrrs a7, fflags, zero
[0x80000fd0]:fsd ft11, 1872(a5)
[0x80000fd4]:sw a7, 1876(a5)
[0x80000fd8]:fld ft10, 792(a6)
[0x80000fdc]:fld ft9, 800(a6)
[0x80000fe0]:fld ft8, 808(a6)
[0x80000fe4]:csrrwi zero, frm, 2

[0x80000fe8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80000fec]:csrrs a7, fflags, zero
[0x80000ff0]:fsd ft11, 1888(a5)
[0x80000ff4]:sw a7, 1892(a5)
[0x80000ff8]:fld ft10, 816(a6)
[0x80000ffc]:fld ft9, 824(a6)
[0x80001000]:fld ft8, 832(a6)
[0x80001004]:csrrwi zero, frm, 1

[0x80001008]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000100c]:csrrs a7, fflags, zero
[0x80001010]:fsd ft11, 1904(a5)
[0x80001014]:sw a7, 1908(a5)
[0x80001018]:fld ft10, 840(a6)
[0x8000101c]:fld ft9, 848(a6)
[0x80001020]:fld ft8, 856(a6)
[0x80001024]:csrrwi zero, frm, 0

[0x80001028]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000102c]:csrrs a7, fflags, zero
[0x80001030]:fsd ft11, 1920(a5)
[0x80001034]:sw a7, 1924(a5)
[0x80001038]:fld ft10, 864(a6)
[0x8000103c]:fld ft9, 872(a6)
[0x80001040]:fld ft8, 880(a6)
[0x80001044]:csrrwi zero, frm, 4

[0x80001048]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000104c]:csrrs a7, fflags, zero
[0x80001050]:fsd ft11, 1936(a5)
[0x80001054]:sw a7, 1940(a5)
[0x80001058]:fld ft10, 888(a6)
[0x8000105c]:fld ft9, 896(a6)
[0x80001060]:fld ft8, 904(a6)
[0x80001064]:csrrwi zero, frm, 3

[0x80001068]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000106c]:csrrs a7, fflags, zero
[0x80001070]:fsd ft11, 1952(a5)
[0x80001074]:sw a7, 1956(a5)
[0x80001078]:fld ft10, 912(a6)
[0x8000107c]:fld ft9, 920(a6)
[0x80001080]:fld ft8, 928(a6)
[0x80001084]:csrrwi zero, frm, 2

[0x80001088]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x8000108c]:csrrs a7, fflags, zero
[0x80001090]:fsd ft11, 1968(a5)
[0x80001094]:sw a7, 1972(a5)
[0x80001098]:fld ft10, 936(a6)
[0x8000109c]:fld ft9, 944(a6)
[0x800010a0]:fld ft8, 952(a6)
[0x800010a4]:csrrwi zero, frm, 1

[0x800010a8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010ac]:csrrs a7, fflags, zero
[0x800010b0]:fsd ft11, 1984(a5)
[0x800010b4]:sw a7, 1988(a5)
[0x800010b8]:fld ft10, 960(a6)
[0x800010bc]:fld ft9, 968(a6)
[0x800010c0]:fld ft8, 976(a6)
[0x800010c4]:csrrwi zero, frm, 0

[0x800010c8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010cc]:csrrs a7, fflags, zero
[0x800010d0]:fsd ft11, 2000(a5)
[0x800010d4]:sw a7, 2004(a5)
[0x800010d8]:fld ft10, 984(a6)
[0x800010dc]:fld ft9, 992(a6)
[0x800010e0]:fld ft8, 1000(a6)
[0x800010e4]:csrrwi zero, frm, 4

[0x800010e8]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800010ec]:csrrs a7, fflags, zero
[0x800010f0]:fsd ft11, 2016(a5)
[0x800010f4]:sw a7, 2020(a5)
[0x800010f8]:auipc a5, 4
[0x800010fc]:addi a5, a5, 2320
[0x80001100]:fld ft10, 1008(a6)
[0x80001104]:fld ft9, 1016(a6)
[0x80001108]:fld ft8, 1024(a6)
[0x8000110c]:csrrwi zero, frm, 3

[0x800016d4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800016d8]:csrrs a7, fflags, zero
[0x800016dc]:fsd ft11, 736(a5)
[0x800016e0]:sw a7, 740(a5)
[0x800016e4]:fld ft10, 96(a6)
[0x800016e8]:fld ft9, 104(a6)
[0x800016ec]:fld ft8, 112(a6)
[0x800016f0]:csrrwi zero, frm, 1

[0x800016f4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800016f8]:csrrs a7, fflags, zero
[0x800016fc]:fsd ft11, 752(a5)
[0x80001700]:sw a7, 756(a5)
[0x80001704]:fld ft10, 120(a6)
[0x80001708]:fld ft9, 128(a6)
[0x8000170c]:fld ft8, 136(a6)
[0x80001710]:csrrwi zero, frm, 0

[0x80001714]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001718]:csrrs a7, fflags, zero
[0x8000171c]:fsd ft11, 768(a5)
[0x80001720]:sw a7, 772(a5)
[0x80001724]:fld ft10, 144(a6)
[0x80001728]:fld ft9, 152(a6)
[0x8000172c]:fld ft8, 160(a6)
[0x80001730]:csrrwi zero, frm, 4

[0x80001734]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001738]:csrrs a7, fflags, zero
[0x8000173c]:fsd ft11, 784(a5)
[0x80001740]:sw a7, 788(a5)
[0x80001744]:fld ft10, 168(a6)
[0x80001748]:fld ft9, 176(a6)
[0x8000174c]:fld ft8, 184(a6)
[0x80001750]:csrrwi zero, frm, 3

[0x80001754]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001758]:csrrs a7, fflags, zero
[0x8000175c]:fsd ft11, 800(a5)
[0x80001760]:sw a7, 804(a5)
[0x80001764]:fld ft10, 192(a6)
[0x80001768]:fld ft9, 200(a6)
[0x8000176c]:fld ft8, 208(a6)
[0x80001770]:csrrwi zero, frm, 2

[0x80001774]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001778]:csrrs a7, fflags, zero
[0x8000177c]:fsd ft11, 816(a5)
[0x80001780]:sw a7, 820(a5)
[0x80001784]:fld ft10, 216(a6)
[0x80001788]:fld ft9, 224(a6)
[0x8000178c]:fld ft8, 232(a6)
[0x80001790]:csrrwi zero, frm, 1

[0x80001794]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001798]:csrrs a7, fflags, zero
[0x8000179c]:fsd ft11, 832(a5)
[0x800017a0]:sw a7, 836(a5)
[0x800017a4]:fld ft10, 240(a6)
[0x800017a8]:fld ft9, 248(a6)
[0x800017ac]:fld ft8, 256(a6)
[0x800017b0]:csrrwi zero, frm, 0

[0x800017b4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017b8]:csrrs a7, fflags, zero
[0x800017bc]:fsd ft11, 848(a5)
[0x800017c0]:sw a7, 852(a5)
[0x800017c4]:fld ft10, 264(a6)
[0x800017c8]:fld ft9, 272(a6)
[0x800017cc]:fld ft8, 280(a6)
[0x800017d0]:csrrwi zero, frm, 4

[0x800017d4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017d8]:csrrs a7, fflags, zero
[0x800017dc]:fsd ft11, 864(a5)
[0x800017e0]:sw a7, 868(a5)
[0x800017e4]:fld ft10, 288(a6)
[0x800017e8]:fld ft9, 296(a6)
[0x800017ec]:fld ft8, 304(a6)
[0x800017f0]:csrrwi zero, frm, 3

[0x800017f4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800017f8]:csrrs a7, fflags, zero
[0x800017fc]:fsd ft11, 880(a5)
[0x80001800]:sw a7, 884(a5)
[0x80001804]:fld ft10, 312(a6)
[0x80001808]:fld ft9, 320(a6)
[0x8000180c]:fld ft8, 328(a6)
[0x80001810]:csrrwi zero, frm, 2

[0x80001814]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001818]:csrrs a7, fflags, zero
[0x8000181c]:fsd ft11, 896(a5)
[0x80001820]:sw a7, 900(a5)
[0x80001824]:fld ft10, 336(a6)
[0x80001828]:fld ft9, 344(a6)
[0x8000182c]:fld ft8, 352(a6)
[0x80001830]:csrrwi zero, frm, 1

[0x80001834]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001838]:csrrs a7, fflags, zero
[0x8000183c]:fsd ft11, 912(a5)
[0x80001840]:sw a7, 916(a5)
[0x80001844]:fld ft10, 360(a6)
[0x80001848]:fld ft9, 368(a6)
[0x8000184c]:fld ft8, 376(a6)
[0x80001850]:csrrwi zero, frm, 0

[0x80001854]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001858]:csrrs a7, fflags, zero
[0x8000185c]:fsd ft11, 928(a5)
[0x80001860]:sw a7, 932(a5)
[0x80001864]:fld ft10, 384(a6)
[0x80001868]:fld ft9, 392(a6)
[0x8000186c]:fld ft8, 400(a6)
[0x80001870]:csrrwi zero, frm, 4

[0x80001874]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001878]:csrrs a7, fflags, zero
[0x8000187c]:fsd ft11, 944(a5)
[0x80001880]:sw a7, 948(a5)
[0x80001884]:fld ft10, 408(a6)
[0x80001888]:fld ft9, 416(a6)
[0x8000188c]:fld ft8, 424(a6)
[0x80001890]:csrrwi zero, frm, 3

[0x80001894]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001898]:csrrs a7, fflags, zero
[0x8000189c]:fsd ft11, 960(a5)
[0x800018a0]:sw a7, 964(a5)
[0x800018a4]:fld ft10, 432(a6)
[0x800018a8]:fld ft9, 440(a6)
[0x800018ac]:fld ft8, 448(a6)
[0x800018b0]:csrrwi zero, frm, 2

[0x800018b4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018b8]:csrrs a7, fflags, zero
[0x800018bc]:fsd ft11, 976(a5)
[0x800018c0]:sw a7, 980(a5)
[0x800018c4]:fld ft10, 456(a6)
[0x800018c8]:fld ft9, 464(a6)
[0x800018cc]:fld ft8, 472(a6)
[0x800018d0]:csrrwi zero, frm, 1

[0x800018d4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018d8]:csrrs a7, fflags, zero
[0x800018dc]:fsd ft11, 992(a5)
[0x800018e0]:sw a7, 996(a5)
[0x800018e4]:fld ft10, 480(a6)
[0x800018e8]:fld ft9, 488(a6)
[0x800018ec]:fld ft8, 496(a6)
[0x800018f0]:csrrwi zero, frm, 0

[0x800018f4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800018f8]:csrrs a7, fflags, zero
[0x800018fc]:fsd ft11, 1008(a5)
[0x80001900]:sw a7, 1012(a5)
[0x80001904]:fld ft10, 504(a6)
[0x80001908]:fld ft9, 512(a6)
[0x8000190c]:fld ft8, 520(a6)
[0x80001910]:csrrwi zero, frm, 4

[0x80001914]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001918]:csrrs a7, fflags, zero
[0x8000191c]:fsd ft11, 1024(a5)
[0x80001920]:sw a7, 1028(a5)
[0x80001924]:fld ft10, 528(a6)
[0x80001928]:fld ft9, 536(a6)
[0x8000192c]:fld ft8, 544(a6)
[0x80001930]:csrrwi zero, frm, 3

[0x80001934]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001938]:csrrs a7, fflags, zero
[0x8000193c]:fsd ft11, 1040(a5)
[0x80001940]:sw a7, 1044(a5)
[0x80001944]:fld ft10, 552(a6)
[0x80001948]:fld ft9, 560(a6)
[0x8000194c]:fld ft8, 568(a6)
[0x80001950]:csrrwi zero, frm, 2

[0x80001954]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001958]:csrrs a7, fflags, zero
[0x8000195c]:fsd ft11, 1056(a5)
[0x80001960]:sw a7, 1060(a5)
[0x80001964]:fld ft10, 576(a6)
[0x80001968]:fld ft9, 584(a6)
[0x8000196c]:fld ft8, 592(a6)
[0x80001970]:csrrwi zero, frm, 1

[0x80001974]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001978]:csrrs a7, fflags, zero
[0x8000197c]:fsd ft11, 1072(a5)
[0x80001980]:sw a7, 1076(a5)
[0x80001984]:fld ft10, 600(a6)
[0x80001988]:fld ft9, 608(a6)
[0x8000198c]:fld ft8, 616(a6)
[0x80001990]:csrrwi zero, frm, 0

[0x80001994]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001998]:csrrs a7, fflags, zero
[0x8000199c]:fsd ft11, 1088(a5)
[0x800019a0]:sw a7, 1092(a5)
[0x800019a4]:fld ft10, 624(a6)
[0x800019a8]:fld ft9, 632(a6)
[0x800019ac]:fld ft8, 640(a6)
[0x800019b0]:csrrwi zero, frm, 4

[0x800019b4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019b8]:csrrs a7, fflags, zero
[0x800019bc]:fsd ft11, 1104(a5)
[0x800019c0]:sw a7, 1108(a5)
[0x800019c4]:fld ft10, 648(a6)
[0x800019c8]:fld ft9, 656(a6)
[0x800019cc]:fld ft8, 664(a6)
[0x800019d0]:csrrwi zero, frm, 3

[0x800019d4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019d8]:csrrs a7, fflags, zero
[0x800019dc]:fsd ft11, 1120(a5)
[0x800019e0]:sw a7, 1124(a5)
[0x800019e4]:fld ft10, 672(a6)
[0x800019e8]:fld ft9, 680(a6)
[0x800019ec]:fld ft8, 688(a6)
[0x800019f0]:csrrwi zero, frm, 2

[0x800019f4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x800019f8]:csrrs a7, fflags, zero
[0x800019fc]:fsd ft11, 1136(a5)
[0x80001a00]:sw a7, 1140(a5)
[0x80001a04]:fld ft10, 696(a6)
[0x80001a08]:fld ft9, 704(a6)
[0x80001a0c]:fld ft8, 712(a6)
[0x80001a10]:csrrwi zero, frm, 1

[0x80001a14]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a18]:csrrs a7, fflags, zero
[0x80001a1c]:fsd ft11, 1152(a5)
[0x80001a20]:sw a7, 1156(a5)
[0x80001a24]:fld ft10, 720(a6)
[0x80001a28]:fld ft9, 728(a6)
[0x80001a2c]:fld ft8, 736(a6)
[0x80001a30]:csrrwi zero, frm, 0

[0x80001a34]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a38]:csrrs a7, fflags, zero
[0x80001a3c]:fsd ft11, 1168(a5)
[0x80001a40]:sw a7, 1172(a5)
[0x80001a44]:fld ft10, 744(a6)
[0x80001a48]:fld ft9, 752(a6)
[0x80001a4c]:fld ft8, 760(a6)
[0x80001a50]:csrrwi zero, frm, 4

[0x80001a54]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a58]:csrrs a7, fflags, zero
[0x80001a5c]:fsd ft11, 1184(a5)
[0x80001a60]:sw a7, 1188(a5)
[0x80001a64]:fld ft10, 768(a6)
[0x80001a68]:fld ft9, 776(a6)
[0x80001a6c]:fld ft8, 784(a6)
[0x80001a70]:csrrwi zero, frm, 3

[0x80001a74]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a78]:csrrs a7, fflags, zero
[0x80001a7c]:fsd ft11, 1200(a5)
[0x80001a80]:sw a7, 1204(a5)
[0x80001a84]:fld ft10, 792(a6)
[0x80001a88]:fld ft9, 800(a6)
[0x80001a8c]:fld ft8, 808(a6)
[0x80001a90]:csrrwi zero, frm, 2

[0x80001a94]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001a98]:csrrs a7, fflags, zero
[0x80001a9c]:fsd ft11, 1216(a5)
[0x80001aa0]:sw a7, 1220(a5)
[0x80001aa4]:fld ft10, 816(a6)
[0x80001aa8]:fld ft9, 824(a6)
[0x80001aac]:fld ft8, 832(a6)
[0x80001ab0]:csrrwi zero, frm, 1

[0x80001ab4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001ab8]:csrrs a7, fflags, zero
[0x80001abc]:fsd ft11, 1232(a5)
[0x80001ac0]:sw a7, 1236(a5)
[0x80001ac4]:fld ft10, 840(a6)
[0x80001ac8]:fld ft9, 848(a6)
[0x80001acc]:fld ft8, 856(a6)
[0x80001ad0]:csrrwi zero, frm, 0

[0x80001ad4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001ad8]:csrrs a7, fflags, zero
[0x80001adc]:fsd ft11, 1248(a5)
[0x80001ae0]:sw a7, 1252(a5)
[0x80001ae4]:fld ft10, 864(a6)
[0x80001ae8]:fld ft9, 872(a6)
[0x80001aec]:fld ft8, 880(a6)
[0x80001af0]:csrrwi zero, frm, 4

[0x80001af4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001af8]:csrrs a7, fflags, zero
[0x80001afc]:fsd ft11, 1264(a5)
[0x80001b00]:sw a7, 1268(a5)
[0x80001b04]:fld ft10, 888(a6)
[0x80001b08]:fld ft9, 896(a6)
[0x80001b0c]:fld ft8, 904(a6)
[0x80001b10]:csrrwi zero, frm, 3

[0x80001b14]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001b18]:csrrs a7, fflags, zero
[0x80001b1c]:fsd ft11, 1280(a5)
[0x80001b20]:sw a7, 1284(a5)
[0x80001b24]:fld ft10, 912(a6)
[0x80001b28]:fld ft9, 920(a6)
[0x80001b2c]:fld ft8, 928(a6)
[0x80001b30]:csrrwi zero, frm, 2

[0x80001b34]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001b38]:csrrs a7, fflags, zero
[0x80001b3c]:fsd ft11, 1296(a5)
[0x80001b40]:sw a7, 1300(a5)
[0x80001b44]:fld ft10, 936(a6)
[0x80001b48]:fld ft9, 944(a6)
[0x80001b4c]:fld ft8, 952(a6)
[0x80001b50]:csrrwi zero, frm, 1

[0x80001b54]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001b58]:csrrs a7, fflags, zero
[0x80001b5c]:fsd ft11, 1312(a5)
[0x80001b60]:sw a7, 1316(a5)
[0x80001b64]:fld ft10, 960(a6)
[0x80001b68]:fld ft9, 968(a6)
[0x80001b6c]:fld ft8, 976(a6)
[0x80001b70]:csrrwi zero, frm, 0

[0x80001b74]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001b78]:csrrs a7, fflags, zero
[0x80001b7c]:fsd ft11, 1328(a5)
[0x80001b80]:sw a7, 1332(a5)
[0x80001b84]:fld ft10, 984(a6)
[0x80001b88]:fld ft9, 992(a6)
[0x80001b8c]:fld ft8, 1000(a6)
[0x80001b90]:csrrwi zero, frm, 3

[0x80001b94]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001b98]:csrrs a7, fflags, zero
[0x80001b9c]:fsd ft11, 1344(a5)
[0x80001ba0]:sw a7, 1348(a5)
[0x80001ba4]:fld ft10, 1008(a6)
[0x80001ba8]:fld ft9, 1016(a6)
[0x80001bac]:fld ft8, 1024(a6)
[0x80001bb0]:csrrwi zero, frm, 2

[0x80001bb4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001bb8]:csrrs a7, fflags, zero
[0x80001bbc]:fsd ft11, 1360(a5)
[0x80001bc0]:sw a7, 1364(a5)
[0x80001bc4]:fld ft10, 1032(a6)
[0x80001bc8]:fld ft9, 1040(a6)
[0x80001bcc]:fld ft8, 1048(a6)
[0x80001bd0]:csrrwi zero, frm, 4

[0x80001bd4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001bd8]:csrrs a7, fflags, zero
[0x80001bdc]:fsd ft11, 1376(a5)
[0x80001be0]:sw a7, 1380(a5)
[0x80001be4]:fld ft10, 1056(a6)
[0x80001be8]:fld ft9, 1064(a6)
[0x80001bec]:fld ft8, 1072(a6)
[0x80001bf0]:csrrwi zero, frm, 3

[0x80001bf4]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001bf8]:csrrs a7, fflags, zero
[0x80001bfc]:fsd ft11, 1392(a5)
[0x80001c00]:sw a7, 1396(a5)
[0x80001c04]:fld ft10, 1080(a6)
[0x80001c08]:fld ft9, 1088(a6)
[0x80001c0c]:fld ft8, 1096(a6)
[0x80001c10]:csrrwi zero, frm, 2

[0x80001c14]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001c18]:csrrs a7, fflags, zero
[0x80001c1c]:fsd ft11, 1408(a5)
[0x80001c20]:sw a7, 1412(a5)
[0x80001c24]:fld ft10, 1104(a6)
[0x80001c28]:fld ft9, 1112(a6)
[0x80001c2c]:fld ft8, 1120(a6)
[0x80001c30]:csrrwi zero, frm, 1

[0x80001c34]:fmsub.d ft11, ft10, ft9, ft8, dyn
[0x80001c38]:csrrs a7, fflags, zero
[0x80001c3c]:fsd ft11, 1424(a5)
[0x80001c40]:sw a7, 1428(a5)
[0x80001c44]:fld ft10, 1128(a6)
[0x80001c48]:fld ft9, 1136(a6)
[0x80001c4c]:fld ft8, 1144(a6)
[0x80001c50]:csrrwi zero, frm, 4



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                                                                                                                                         coverpoints                                                                                                                                                                         |                                                                              code                                                                              |
|---:|--------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80004614]<br>0x00000005|- opcode : fmsub.d<br> - rs1 : f10<br> - rs2 : f10<br> - rs3 : f10<br> - rd : f10<br> - rs1 == rs2 == rs3 == rd<br>                                                                                                                                                                                                                                          |[0x80000124]:fmsub.d fa0, fa0, fa0, fa0, dyn<br> [0x80000128]:csrrs a7, fflags, zero<br> [0x8000012c]:fsd fa0, 0(a5)<br> [0x80000130]:sw a7, 4(a5)<br>          |
|   2|[0x80004624]<br>0x00000005|- rs1 : f12<br> - rs2 : f5<br> - rs3 : f14<br> - rd : f14<br> - rs3 == rd != rs1 and rs3 == rd != rs2 and rs2 != rs1<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe2f460df71daf and fs2 == 0 and fe2 == 0x402 and fm2 == 0xa0fdaacb5fbcf and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8955d5926548d and rm_val == 4  #nosat<br>                                |[0x80000144]:fmsub.d fa4, fa2, ft5, fa4, dyn<br> [0x80000148]:csrrs a7, fflags, zero<br> [0x8000014c]:fsd fa4, 16(a5)<br> [0x80000150]:sw a7, 20(a5)<br>        |
|   3|[0x80004634]<br>0x00000005|- rs1 : f31<br> - rs2 : f21<br> - rs3 : f31<br> - rd : f30<br> - rs1 == rs3 != rs2 and rs1 == rs3 != rd and rd != rs2<br>                                                                                                                                                                                                                                    |[0x80000164]:fmsub.d ft10, ft11, fs5, ft11, dyn<br> [0x80000168]:csrrs a7, fflags, zero<br> [0x8000016c]:fsd ft10, 32(a5)<br> [0x80000170]:sw a7, 36(a5)<br>    |
|   4|[0x80004644]<br>0x00000005|- rs1 : f13<br> - rs2 : f11<br> - rs3 : f11<br> - rd : f9<br> - rs2 == rs3 != rs1 and rs2 == rs3 != rd and rd != rs1<br>                                                                                                                                                                                                                                     |[0x80000184]:fmsub.d fs1, fa3, fa1, fa1, dyn<br> [0x80000188]:csrrs a7, fflags, zero<br> [0x8000018c]:fsd fs1, 48(a5)<br> [0x80000190]:sw a7, 52(a5)<br>        |
|   5|[0x80004654]<br>0x00000005|- rs1 : f19<br> - rs2 : f25<br> - rs3 : f3<br> - rd : f19<br> - rs1 == rd != rs2 and rs1 == rd != rs3 and rs3 != rs2<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe2f460df71daf and fs2 == 0 and fe2 == 0x402 and fm2 == 0xa0fdaacb5fbcf and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8955d5926548d and rm_val == 1  #nosat<br>                                |[0x800001a4]:fmsub.d fs3, fs3, fs9, ft3, dyn<br> [0x800001a8]:csrrs a7, fflags, zero<br> [0x800001ac]:fsd fs3, 64(a5)<br> [0x800001b0]:sw a7, 68(a5)<br>        |
|   6|[0x80004664]<br>0x00000005|- rs1 : f15<br> - rs2 : f17<br> - rs3 : f24<br> - rd : f16<br> - rs1 != rs2 and rs1 != rd and rs1 != rs3 and rs2 != rs3 and rs2 != rd and rs3 != rd<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xe2f460df71daf and fs2 == 0 and fe2 == 0x402 and fm2 == 0xa0fdaacb5fbcf and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x8955d5926548d and rm_val == 0  #nosat<br> |[0x800001c4]:fmsub.d fa6, fa5, fa7, fs8, dyn<br> [0x800001c8]:csrrs a7, fflags, zero<br> [0x800001cc]:fsd fa6, 80(a5)<br> [0x800001d0]:sw a7, 84(a5)<br>        |
|   7|[0x80004674]<br>0x00000005|- rs1 : f22<br> - rs2 : f22<br> - rs3 : f18<br> - rd : f22<br> - rs1 == rs2 == rd != rs3<br>                                                                                                                                                                                                                                                                 |[0x800001e4]:fmsub.d fs6, fs6, fs6, fs2, dyn<br> [0x800001e8]:csrrs a7, fflags, zero<br> [0x800001ec]:fsd fs6, 96(a5)<br> [0x800001f0]:sw a7, 100(a5)<br>       |
|   8|[0x80004684]<br>0x00000005|- rs1 : f30<br> - rs2 : f8<br> - rs3 : f8<br> - rd : f8<br> - rd == rs2 == rs3 != rs1<br>                                                                                                                                                                                                                                                                    |[0x80000204]:fmsub.d fs0, ft10, fs0, fs0, dyn<br> [0x80000208]:csrrs a7, fflags, zero<br> [0x8000020c]:fsd fs0, 112(a5)<br> [0x80000210]:sw a7, 116(a5)<br>     |
|   9|[0x80004694]<br>0x00000005|- rs1 : f17<br> - rs2 : f0<br> - rs3 : f17<br> - rd : f17<br> - rs1 == rd == rs3 != rs2<br>                                                                                                                                                                                                                                                                  |[0x80000224]:fmsub.d fa7, fa7, ft0, fa7, dyn<br> [0x80000228]:csrrs a7, fflags, zero<br> [0x8000022c]:fsd fa7, 128(a5)<br> [0x80000230]:sw a7, 132(a5)<br>      |
|  10|[0x800046a4]<br>0x00000005|- rs1 : f18<br> - rs2 : f18<br> - rs3 : f5<br> - rd : f29<br> - rs1 == rs2 != rs3 and rs1 == rs2 != rd and rd != rs3<br>                                                                                                                                                                                                                                     |[0x80000244]:fmsub.d ft9, fs2, fs2, ft5, dyn<br> [0x80000248]:csrrs a7, fflags, zero<br> [0x8000024c]:fsd ft9, 144(a5)<br> [0x80000250]:sw a7, 148(a5)<br>      |
|  11|[0x800046b4]<br>0x00000005|- rs1 : f16<br> - rs2 : f13<br> - rs3 : f23<br> - rd : f13<br> - rs2 == rd != rs1 and rs2 == rd != rs3 and rs3 != rs1<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x5829bf9c6538f and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x64c2b92225f5e and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xdf9fd6fcc553f and rm_val == 0  #nosat<br>                               |[0x80000264]:fmsub.d fa3, fa6, fa3, fs7, dyn<br> [0x80000268]:csrrs a7, fflags, zero<br> [0x8000026c]:fsd fa3, 160(a5)<br> [0x80000270]:sw a7, 164(a5)<br>      |
|  12|[0x800046c4]<br>0x00000005|- rs1 : f26<br> - rs2 : f26<br> - rs3 : f26<br> - rd : f11<br> - rs1 == rs2 == rs3 != rd<br>                                                                                                                                                                                                                                                                 |[0x80000284]:fmsub.d fa1, fs10, fs10, fs10, dyn<br> [0x80000288]:csrrs a7, fflags, zero<br> [0x8000028c]:fsd fa1, 176(a5)<br> [0x80000290]:sw a7, 180(a5)<br>   |
|  13|[0x800046d4]<br>0x00000005|- rs1 : f3<br> - rs2 : f27<br> - rs3 : f4<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x71abc78672bff and fs2 == 0 and fe2 == 0x403 and fm2 == 0x4766a61cffe7f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd8c6a62d7fd8f and rm_val == 3  #nosat<br>                                                                                             |[0x800002a4]:fmsub.d ft2, ft3, fs11, ft4, dyn<br> [0x800002a8]:csrrs a7, fflags, zero<br> [0x800002ac]:fsd ft2, 192(a5)<br> [0x800002b0]:sw a7, 196(a5)<br>     |
|  14|[0x800046e4]<br>0x00000005|- rs1 : f24<br> - rs2 : f6<br> - rs3 : f16<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x71abc78672bff and fs2 == 0 and fe2 == 0x403 and fm2 == 0x4766a61cffe7f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd8c6a62d7fd8f and rm_val == 2  #nosat<br>                                                                                           |[0x800002c4]:fmsub.d fs9, fs8, ft6, fa6, dyn<br> [0x800002c8]:csrrs a7, fflags, zero<br> [0x800002cc]:fsd fs9, 208(a5)<br> [0x800002d0]:sw a7, 212(a5)<br>      |
|  15|[0x800046f4]<br>0x00000005|- rs1 : f27<br> - rs2 : f1<br> - rs3 : f13<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x71abc78672bff and fs2 == 0 and fe2 == 0x403 and fm2 == 0x4766a61cffe7f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd8c6a62d7fd8f and rm_val == 1  #nosat<br>                                                                                            |[0x800002e4]:fmsub.d ft5, fs11, ft1, fa3, dyn<br> [0x800002e8]:csrrs a7, fflags, zero<br> [0x800002ec]:fsd ft5, 224(a5)<br> [0x800002f0]:sw a7, 228(a5)<br>     |
|  16|[0x80004704]<br>0x00000005|- rs1 : f28<br> - rs2 : f16<br> - rs3 : f30<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7f9 and fm1 == 0x71abc78672bff and fs2 == 0 and fe2 == 0x403 and fm2 == 0x4766a61cffe7f and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd8c6a62d7fd8f and rm_val == 0  #nosat<br>                                                                                           |[0x80000304]:fmsub.d ft1, ft8, fa6, ft10, dyn<br> [0x80000308]:csrrs a7, fflags, zero<br> [0x8000030c]:fsd ft1, 240(a5)<br> [0x80000310]:sw a7, 244(a5)<br>     |
|  17|[0x80004714]<br>0x00000005|- rs1 : f5<br> - rs2 : f15<br> - rs3 : f6<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8b0ce9718a893 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbbb2d2c120e46 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5659a61635557 and rm_val == 4  #nosat<br>                                                                                            |[0x80000324]:fmsub.d fs8, ft5, fa5, ft6, dyn<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:fsd fs8, 256(a5)<br> [0x80000330]:sw a7, 260(a5)<br>      |
|  18|[0x80004724]<br>0x00000005|- rs1 : f21<br> - rs2 : f14<br> - rs3 : f25<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8b0ce9718a893 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbbb2d2c120e46 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5659a61635557 and rm_val == 3  #nosat<br>                                                                                          |[0x80000344]:fmsub.d ft11, fs5, fa4, fs9, dyn<br> [0x80000348]:csrrs a7, fflags, zero<br> [0x8000034c]:fsd ft11, 272(a5)<br> [0x80000350]:sw a7, 276(a5)<br>    |
|  19|[0x80004734]<br>0x00000005|- rs1 : f25<br> - rs2 : f19<br> - rs3 : f22<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8b0ce9718a893 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbbb2d2c120e46 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5659a61635557 and rm_val == 2  #nosat<br>                                                                                          |[0x80000364]:fmsub.d fs11, fs9, fs3, fs6, dyn<br> [0x80000368]:csrrs a7, fflags, zero<br> [0x8000036c]:fsd fs11, 288(a5)<br> [0x80000370]:sw a7, 292(a5)<br>    |
|  20|[0x80004744]<br>0x00000005|- rs1 : f4<br> - rs2 : f9<br> - rs3 : f15<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8b0ce9718a893 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbbb2d2c120e46 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5659a61635557 and rm_val == 1  #nosat<br>                                                                                             |[0x80000384]:fmsub.d ft7, ft4, fs1, fa5, dyn<br> [0x80000388]:csrrs a7, fflags, zero<br> [0x8000038c]:fsd ft7, 304(a5)<br> [0x80000390]:sw a7, 308(a5)<br>      |
|  21|[0x80004754]<br>0x00000005|- rs1 : f6<br> - rs2 : f4<br> - rs3 : f2<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x8b0ce9718a893 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xbbb2d2c120e46 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x5659a61635557 and rm_val == 0  #nosat<br>                                                                                             |[0x800003a4]:fmsub.d fs4, ft6, ft4, ft2, dyn<br> [0x800003a8]:csrrs a7, fflags, zero<br> [0x800003ac]:fsd fs4, 320(a5)<br> [0x800003b0]:sw a7, 324(a5)<br>      |
|  22|[0x80004764]<br>0x00000005|- rs1 : f1<br> - rs2 : f7<br> - rs3 : f21<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4d3535c8560c and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6290a8daf6d85 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x236d02dbba759 and rm_val == 4  #nosat<br>                                                                                             |[0x800003c4]:fmsub.d ft3, ft1, ft7, fs5, dyn<br> [0x800003c8]:csrrs a7, fflags, zero<br> [0x800003cc]:fsd ft3, 336(a5)<br> [0x800003d0]:sw a7, 340(a5)<br>      |
|  23|[0x80004774]<br>0x00000005|- rs1 : f14<br> - rs2 : f23<br> - rs3 : f7<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4d3535c8560c and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6290a8daf6d85 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x236d02dbba759 and rm_val == 3  #nosat<br>                                                                                           |[0x800003e4]:fmsub.d fa2, fa4, fs7, ft7, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fa2, 352(a5)<br> [0x800003f0]:sw a7, 356(a5)<br>      |
|  24|[0x80004784]<br>0x00000005|- rs1 : f23<br> - rs2 : f30<br> - rs3 : f9<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4d3535c8560c and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6290a8daf6d85 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x236d02dbba759 and rm_val == 2  #nosat<br>                                                                                            |[0x80000404]:fmsub.d ft4, fs7, ft10, fs1, dyn<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd ft4, 368(a5)<br> [0x80000410]:sw a7, 372(a5)<br>     |
|  25|[0x80004794]<br>0x00000005|- rs1 : f20<br> - rs2 : f3<br> - rs3 : f12<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4d3535c8560c and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6290a8daf6d85 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x236d02dbba759 and rm_val == 1  #nosat<br>                                                                                            |[0x80000424]:fmsub.d ft6, fs4, ft3, fa2, dyn<br> [0x80000428]:csrrs a7, fflags, zero<br> [0x8000042c]:fsd ft6, 384(a5)<br> [0x80000430]:sw a7, 388(a5)<br>      |
|  26|[0x800047a4]<br>0x00000005|- rs1 : f8<br> - rs2 : f20<br> - rs3 : f19<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa4d3535c8560c and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6290a8daf6d85 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x236d02dbba759 and rm_val == 0  #nosat<br>                                                                                           |[0x80000444]:fmsub.d ft8, fs0, fs4, fs3, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd ft8, 400(a5)<br> [0x80000450]:sw a7, 404(a5)<br>      |
|  27|[0x800047b4]<br>0x00000005|- rs1 : f29<br> - rs2 : f28<br> - rs3 : f1<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4b547924fd121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x88dcc2c35a5a1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfc77122b9963b and rm_val == 4  #nosat<br>                                                                                           |[0x80000464]:fmsub.d fs2, ft9, ft8, ft1, dyn<br> [0x80000468]:csrrs a7, fflags, zero<br> [0x8000046c]:fsd fs2, 416(a5)<br> [0x80000470]:sw a7, 420(a5)<br>      |
|  28|[0x800047c4]<br>0x00000005|- rs1 : f2<br> - rs2 : f24<br> - rs3 : f28<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4b547924fd121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x88dcc2c35a5a1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfc77122b9963b and rm_val == 3  #nosat<br>                                                                                           |[0x80000484]:fmsub.d fs5, ft2, fs8, ft8, dyn<br> [0x80000488]:csrrs a7, fflags, zero<br> [0x8000048c]:fsd fs5, 432(a5)<br> [0x80000490]:sw a7, 436(a5)<br>      |
|  29|[0x800047d4]<br>0x00000005|- rs1 : f0<br> - rs2 : f12<br> - rs3 : f20<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4b547924fd121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x88dcc2c35a5a1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfc77122b9963b and rm_val == 2  #nosat<br>                                                                                           |[0x800004a4]:fmsub.d fs7, ft0, fa2, fs4, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd fs7, 448(a5)<br> [0x800004b0]:sw a7, 452(a5)<br>      |
|  30|[0x800047e4]<br>0x00000005|- rs1 : f11<br> - rs2 : f29<br> - rs3 : f27<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4b547924fd121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x88dcc2c35a5a1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfc77122b9963b and rm_val == 1  #nosat<br>                                                                                          |[0x800004c4]:fmsub.d fs10, fa1, ft9, fs11, dyn<br> [0x800004c8]:csrrs a7, fflags, zero<br> [0x800004cc]:fsd fs10, 464(a5)<br> [0x800004d0]:sw a7, 468(a5)<br>   |
|  31|[0x800047f4]<br>0x00000005|- rs1 : f7<br> - rs2 : f31<br> - rs3 : f29<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4b547924fd121 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x88dcc2c35a5a1 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xfc77122b9963b and rm_val == 0  #nosat<br>                                                                                           |[0x800004e4]:fmsub.d fa5, ft7, ft11, ft9, dyn<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:fsd fa5, 480(a5)<br> [0x800004f0]:sw a7, 484(a5)<br>     |
|  32|[0x80004804]<br>0x00000005|- rs1 : f9<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xf10102ecb507f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x72003d0023fd5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6729f653d09b6 and rm_val == 4  #nosat<br>                                                                                                                                          |[0x80000504]:fmsub.d fs11, fs1, ft11, ft7, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fs11, 496(a5)<br> [0x80000510]:sw a7, 500(a5)<br>   |
|  33|[0x80004814]<br>0x00000005|- rs2 : f2<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xf10102ecb507f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x72003d0023fd5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6729f653d09b6 and rm_val == 3  #nosat<br>                                                                                                                                          |[0x80000524]:fmsub.d ft5, ft4, ft2, ft10, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd ft5, 512(a5)<br> [0x80000530]:sw a7, 516(a5)<br>     |
|  34|[0x80004824]<br>0x00000005|- rs3 : f0<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xf10102ecb507f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x72003d0023fd5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6729f653d09b6 and rm_val == 2  #nosat<br>                                                                                                                                          |[0x80000544]:fmsub.d ft1, fs4, ft11, ft0, dyn<br> [0x80000548]:csrrs a7, fflags, zero<br> [0x8000054c]:fsd ft1, 528(a5)<br> [0x80000550]:sw a7, 532(a5)<br>     |
|  35|[0x80004834]<br>0x00000005|- rd : f0<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0xf10102ecb507f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x72003d0023fd5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6729f653d09b6 and rm_val == 1  #nosat<br>                                                                                                                                           |[0x80000564]:fmsub.d ft0, fs2, fa6, fa0, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd ft0, 544(a5)<br> [0x80000570]:sw a7, 548(a5)<br>      |
|  36|[0x80004844]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0xf10102ecb507f and fs2 == 0 and fe2 == 0x402 and fm2 == 0x72003d0023fd5 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x6729f653d09b6 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000584]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd ft11, 560(a5)<br> [0x80000590]:sw a7, 564(a5)<br>   |
|  37|[0x80004854]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe57a08d938ac9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x669fb3be375cc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x540bc20428f59 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800005a4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005a8]:csrrs a7, fflags, zero<br> [0x800005ac]:fsd ft11, 576(a5)<br> [0x800005b0]:sw a7, 580(a5)<br>   |
|  38|[0x80004864]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe57a08d938ac9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x669fb3be375cc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x540bc20428f59 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800005c4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd ft11, 592(a5)<br> [0x800005d0]:sw a7, 596(a5)<br>   |
|  39|[0x80004874]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe57a08d938ac9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x669fb3be375cc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x540bc20428f59 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800005e4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd ft11, 608(a5)<br> [0x800005f0]:sw a7, 612(a5)<br>   |
|  40|[0x80004884]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe57a08d938ac9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x669fb3be375cc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x540bc20428f59 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000604]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd ft11, 624(a5)<br> [0x80000610]:sw a7, 628(a5)<br>   |
|  41|[0x80004894]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xe57a08d938ac9 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x669fb3be375cc and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x540bc20428f59 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000624]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft11, 640(a5)<br> [0x80000630]:sw a7, 644(a5)<br>   |
|  42|[0x800048a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae1041c5fd79f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1fe9f24a7455f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3ad3d9146af5 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000644]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft11, 656(a5)<br> [0x80000650]:sw a7, 660(a5)<br>   |
|  43|[0x800048b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae1041c5fd79f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1fe9f24a7455f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3ad3d9146af5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000664]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000668]:csrrs a7, fflags, zero<br> [0x8000066c]:fsd ft11, 672(a5)<br> [0x80000670]:sw a7, 676(a5)<br>   |
|  44|[0x800048c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae1041c5fd79f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1fe9f24a7455f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3ad3d9146af5 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000684]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd ft11, 688(a5)<br> [0x80000690]:sw a7, 692(a5)<br>   |
|  45|[0x800048d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae1041c5fd79f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1fe9f24a7455f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3ad3d9146af5 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800006a4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006a8]:csrrs a7, fflags, zero<br> [0x800006ac]:fsd ft11, 704(a5)<br> [0x800006b0]:sw a7, 708(a5)<br>   |
|  46|[0x800048e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xae1041c5fd79f and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x1fe9f24a7455f and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe3ad3d9146af5 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800006c4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006c8]:csrrs a7, fflags, zero<br> [0x800006cc]:fsd ft11, 720(a5)<br> [0x800006d0]:sw a7, 724(a5)<br>   |
|  47|[0x800048f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28e8063300472 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x92cabe4efe922 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd3277d88cd395 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800006e4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 736(a5)<br> [0x800006f0]:sw a7, 740(a5)<br>   |
|  48|[0x80004904]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28e8063300472 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x92cabe4efe922 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd3277d88cd395 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000704]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000708]:csrrs a7, fflags, zero<br> [0x8000070c]:fsd ft11, 752(a5)<br> [0x80000710]:sw a7, 756(a5)<br>   |
|  49|[0x80004914]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28e8063300472 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x92cabe4efe922 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd3277d88cd395 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000724]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000728]:csrrs a7, fflags, zero<br> [0x8000072c]:fsd ft11, 768(a5)<br> [0x80000730]:sw a7, 772(a5)<br>   |
|  50|[0x80004924]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28e8063300472 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x92cabe4efe922 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd3277d88cd395 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000744]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 784(a5)<br> [0x80000750]:sw a7, 788(a5)<br>   |
|  51|[0x80004934]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x28e8063300472 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x92cabe4efe922 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xd3277d88cd395 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000764]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd ft11, 800(a5)<br> [0x80000770]:sw a7, 804(a5)<br>   |
|  52|[0x80004944]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63fdc11669528 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb5ae43b7daad0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30513f9fc9850 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000784]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000788]:csrrs a7, fflags, zero<br> [0x8000078c]:fsd ft11, 816(a5)<br> [0x80000790]:sw a7, 820(a5)<br>   |
|  53|[0x80004954]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63fdc11669528 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb5ae43b7daad0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30513f9fc9850 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800007a4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 832(a5)<br> [0x800007b0]:sw a7, 836(a5)<br>   |
|  54|[0x80004964]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63fdc11669528 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb5ae43b7daad0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30513f9fc9850 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800007c4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 848(a5)<br> [0x800007d0]:sw a7, 852(a5)<br>   |
|  55|[0x80004974]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63fdc11669528 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb5ae43b7daad0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30513f9fc9850 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800007e4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800007e8]:csrrs a7, fflags, zero<br> [0x800007ec]:fsd ft11, 864(a5)<br> [0x800007f0]:sw a7, 868(a5)<br>   |
|  56|[0x80004984]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x63fdc11669528 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xb5ae43b7daad0 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x30513f9fc9850 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000804]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 880(a5)<br> [0x80000810]:sw a7, 884(a5)<br>   |
|  57|[0x80004994]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc03ea0b45fe6a and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ef94d7b62f69 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4147072b89211 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000824]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000828]:csrrs a7, fflags, zero<br> [0x8000082c]:fsd ft11, 896(a5)<br> [0x80000830]:sw a7, 900(a5)<br>   |
|  58|[0x800049a4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc03ea0b45fe6a and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ef94d7b62f69 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4147072b89211 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000844]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:fsd ft11, 912(a5)<br> [0x80000850]:sw a7, 916(a5)<br>   |
|  59|[0x800049b4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc03ea0b45fe6a and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ef94d7b62f69 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4147072b89211 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000864]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 928(a5)<br> [0x80000870]:sw a7, 932(a5)<br>   |
|  60|[0x800049c4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc03ea0b45fe6a and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ef94d7b62f69 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4147072b89211 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000884]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000888]:csrrs a7, fflags, zero<br> [0x8000088c]:fsd ft11, 944(a5)<br> [0x80000890]:sw a7, 948(a5)<br>   |
|  61|[0x800049d4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc03ea0b45fe6a and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x6ef94d7b62f69 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x4147072b89211 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800008a4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 960(a5)<br> [0x800008b0]:sw a7, 964(a5)<br>   |
|  62|[0x800049e4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8775d523b7834 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7fbb0c63a2134 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2563a7f28084c and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800008c4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 976(a5)<br> [0x800008d0]:sw a7, 980(a5)<br>   |
|  63|[0x800049f4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8775d523b7834 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7fbb0c63a2134 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2563a7f28084c and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800008e4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800008e8]:csrrs a7, fflags, zero<br> [0x800008ec]:fsd ft11, 992(a5)<br> [0x800008f0]:sw a7, 996(a5)<br>   |
|  64|[0x80004a04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8775d523b7834 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7fbb0c63a2134 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2563a7f28084c and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000904]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000908]:csrrs a7, fflags, zero<br> [0x8000090c]:fsd ft11, 1008(a5)<br> [0x80000910]:sw a7, 1012(a5)<br> |
|  65|[0x80004a14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8775d523b7834 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7fbb0c63a2134 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2563a7f28084c and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000924]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 1024(a5)<br> [0x80000930]:sw a7, 1028(a5)<br> |
|  66|[0x80004a24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8775d523b7834 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x7fbb0c63a2134 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x2563a7f28084c and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000944]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000948]:csrrs a7, fflags, zero<br> [0x8000094c]:fsd ft11, 1040(a5)<br> [0x80000950]:sw a7, 1044(a5)<br> |
|  67|[0x80004a34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x412f93d91b86f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x10b0d09a9e321 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x56206384f7bbd and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000964]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000968]:csrrs a7, fflags, zero<br> [0x8000096c]:fsd ft11, 1056(a5)<br> [0x80000970]:sw a7, 1060(a5)<br> |
|  68|[0x80004a44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x412f93d91b86f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x10b0d09a9e321 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x56206384f7bbd and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000984]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 1072(a5)<br> [0x80000990]:sw a7, 1076(a5)<br> |
|  69|[0x80004a54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x412f93d91b86f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x10b0d09a9e321 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x56206384f7bbd and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800009a4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009a8]:csrrs a7, fflags, zero<br> [0x800009ac]:fsd ft11, 1088(a5)<br> [0x800009b0]:sw a7, 1092(a5)<br> |
|  70|[0x80004a64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x412f93d91b86f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x10b0d09a9e321 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x56206384f7bbd and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800009c4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009c8]:csrrs a7, fflags, zero<br> [0x800009cc]:fsd ft11, 1104(a5)<br> [0x800009d0]:sw a7, 1108(a5)<br> |
|  71|[0x80004a74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fa and fm1 == 0x412f93d91b86f and fs2 == 0 and fe2 == 0x403 and fm2 == 0x10b0d09a9e321 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x56206384f7bbd and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800009e4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 1120(a5)<br> [0x800009f0]:sw a7, 1124(a5)<br> |
|  72|[0x80004a84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3743aaeeeacbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9bcce06a14cb0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf4b2be35fa361 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000a04]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a08]:csrrs a7, fflags, zero<br> [0x80000a0c]:fsd ft11, 1136(a5)<br> [0x80000a10]:sw a7, 1140(a5)<br> |
|  73|[0x80004a94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3743aaeeeacbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9bcce06a14cb0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf4b2be35fa361 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000a24]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a28]:csrrs a7, fflags, zero<br> [0x80000a2c]:fsd ft11, 1152(a5)<br> [0x80000a30]:sw a7, 1156(a5)<br> |
|  74|[0x80004aa4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3743aaeeeacbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9bcce06a14cb0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf4b2be35fa361 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000a44]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:fsd ft11, 1168(a5)<br> [0x80000a50]:sw a7, 1172(a5)<br> |
|  75|[0x80004ab4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3743aaeeeacbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9bcce06a14cb0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf4b2be35fa361 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000a64]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a68]:csrrs a7, fflags, zero<br> [0x80000a6c]:fsd ft11, 1184(a5)<br> [0x80000a70]:sw a7, 1188(a5)<br> |
|  76|[0x80004ac4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x3743aaeeeacbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9bcce06a14cb0 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0xf4b2be35fa361 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000a84]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000a88]:csrrs a7, fflags, zero<br> [0x80000a8c]:fsd ft11, 1200(a5)<br> [0x80000a90]:sw a7, 1204(a5)<br> |
|  77|[0x80004ad4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xac2863951d7a5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcf913c3426f6d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x83a7f9d5e43df and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000aa4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:fsd ft11, 1216(a5)<br> [0x80000ab0]:sw a7, 1220(a5)<br> |
|  78|[0x80004ae4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xac2863951d7a5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcf913c3426f6d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x83a7f9d5e43df and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ac4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ac8]:csrrs a7, fflags, zero<br> [0x80000acc]:fsd ft11, 1232(a5)<br> [0x80000ad0]:sw a7, 1236(a5)<br> |
|  79|[0x80004af4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xac2863951d7a5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcf913c3426f6d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x83a7f9d5e43df and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000ae4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ae8]:csrrs a7, fflags, zero<br> [0x80000aec]:fsd ft11, 1248(a5)<br> [0x80000af0]:sw a7, 1252(a5)<br> |
|  80|[0x80004b04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xac2863951d7a5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcf913c3426f6d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x83a7f9d5e43df and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000b04]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b08]:csrrs a7, fflags, zero<br> [0x80000b0c]:fsd ft11, 1264(a5)<br> [0x80000b10]:sw a7, 1268(a5)<br> |
|  81|[0x80004b14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xac2863951d7a5 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcf913c3426f6d and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x83a7f9d5e43df and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000b24]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b28]:csrrs a7, fflags, zero<br> [0x80000b2c]:fsd ft11, 1280(a5)<br> [0x80000b30]:sw a7, 1284(a5)<br> |
|  82|[0x80004b24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5d97a44af52a and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xed60f4ded2fb1 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe398f1061c0ef and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000b44]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b48]:csrrs a7, fflags, zero<br> [0x80000b4c]:fsd ft11, 1296(a5)<br> [0x80000b50]:sw a7, 1300(a5)<br> |
|  83|[0x80004b34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5d97a44af52a and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xed60f4ded2fb1 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe398f1061c0ef and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000b64]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b68]:csrrs a7, fflags, zero<br> [0x80000b6c]:fsd ft11, 1312(a5)<br> [0x80000b70]:sw a7, 1316(a5)<br> |
|  84|[0x80004b44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5d97a44af52a and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xed60f4ded2fb1 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe398f1061c0ef and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000b84]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000b88]:csrrs a7, fflags, zero<br> [0x80000b8c]:fsd ft11, 1328(a5)<br> [0x80000b90]:sw a7, 1332(a5)<br> |
|  85|[0x80004b54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5d97a44af52a and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xed60f4ded2fb1 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe398f1061c0ef and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000ba4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ba8]:csrrs a7, fflags, zero<br> [0x80000bac]:fsd ft11, 1344(a5)<br> [0x80000bb0]:sw a7, 1348(a5)<br> |
|  86|[0x80004b64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf5d97a44af52a and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xed60f4ded2fb1 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe398f1061c0ef and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000bc8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bcc]:csrrs a7, fflags, zero<br> [0x80000bd0]:fsd ft11, 1360(a5)<br> [0x80000bd4]:sw a7, 1364(a5)<br> |
|  87|[0x80004b74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7579da5cb9b2f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x943579dbdd6c5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x26d9284df25e5 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000be8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000bec]:csrrs a7, fflags, zero<br> [0x80000bf0]:fsd ft11, 1376(a5)<br> [0x80000bf4]:sw a7, 1380(a5)<br> |
|  88|[0x80004b84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7579da5cb9b2f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x943579dbdd6c5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x26d9284df25e5 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000c08]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c0c]:csrrs a7, fflags, zero<br> [0x80000c10]:fsd ft11, 1392(a5)<br> [0x80000c14]:sw a7, 1396(a5)<br> |
|  89|[0x80004b94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7579da5cb9b2f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x943579dbdd6c5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x26d9284df25e5 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000c28]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c2c]:csrrs a7, fflags, zero<br> [0x80000c30]:fsd ft11, 1408(a5)<br> [0x80000c34]:sw a7, 1412(a5)<br> |
|  90|[0x80004ba4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7579da5cb9b2f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x943579dbdd6c5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x26d9284df25e5 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000c48]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c4c]:csrrs a7, fflags, zero<br> [0x80000c50]:fsd ft11, 1424(a5)<br> [0x80000c54]:sw a7, 1428(a5)<br> |
|  91|[0x80004bb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7579da5cb9b2f and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x943579dbdd6c5 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x26d9284df25e5 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000c68]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c6c]:csrrs a7, fflags, zero<br> [0x80000c70]:fsd ft11, 1440(a5)<br> [0x80000c74]:sw a7, 1444(a5)<br> |
|  92|[0x80004bc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ce68e5faffc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xaa92f4d6f9193 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0120595fb816 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000c88]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000c8c]:csrrs a7, fflags, zero<br> [0x80000c90]:fsd ft11, 1456(a5)<br> [0x80000c94]:sw a7, 1460(a5)<br> |
|  93|[0x80004bd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ce68e5faffc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xaa92f4d6f9193 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0120595fb816 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000ca8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cac]:csrrs a7, fflags, zero<br> [0x80000cb0]:fsd ft11, 1472(a5)<br> [0x80000cb4]:sw a7, 1476(a5)<br> |
|  94|[0x80004be4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ce68e5faffc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xaa92f4d6f9193 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0120595fb816 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000cc8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000ccc]:csrrs a7, fflags, zero<br> [0x80000cd0]:fsd ft11, 1488(a5)<br> [0x80000cd4]:sw a7, 1492(a5)<br> |
|  95|[0x80004bf4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ce68e5faffc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xaa92f4d6f9193 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0120595fb816 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000ce8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000cec]:csrrs a7, fflags, zero<br> [0x80000cf0]:fsd ft11, 1504(a5)<br> [0x80000cf4]:sw a7, 1508(a5)<br> |
|  96|[0x80004c04]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ce68e5faffc7 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xaa92f4d6f9193 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xc0120595fb816 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000d08]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d0c]:csrrs a7, fflags, zero<br> [0x80000d10]:fsd ft11, 1520(a5)<br> [0x80000d14]:sw a7, 1524(a5)<br> |
|  97|[0x80004c14]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3e8df14f472f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2d37c75ea8563 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x261add5337480 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000d28]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d2c]:csrrs a7, fflags, zero<br> [0x80000d30]:fsd ft11, 1536(a5)<br> [0x80000d34]:sw a7, 1540(a5)<br> |
|  98|[0x80004c24]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3e8df14f472f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2d37c75ea8563 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x261add5337480 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000d48]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d4c]:csrrs a7, fflags, zero<br> [0x80000d50]:fsd ft11, 1552(a5)<br> [0x80000d54]:sw a7, 1556(a5)<br> |
|  99|[0x80004c34]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3e8df14f472f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2d37c75ea8563 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x261add5337480 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000d68]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d6c]:csrrs a7, fflags, zero<br> [0x80000d70]:fsd ft11, 1568(a5)<br> [0x80000d74]:sw a7, 1572(a5)<br> |
| 100|[0x80004c44]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3e8df14f472f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2d37c75ea8563 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x261add5337480 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000d88]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000d8c]:csrrs a7, fflags, zero<br> [0x80000d90]:fsd ft11, 1584(a5)<br> [0x80000d94]:sw a7, 1588(a5)<br> |
| 101|[0x80004c54]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xf3e8df14f472f and fs2 == 0 and fe2 == 0x400 and fm2 == 0x2d37c75ea8563 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x261add5337480 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000da8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dac]:csrrs a7, fflags, zero<br> [0x80000db0]:fsd ft11, 1600(a5)<br> [0x80000db4]:sw a7, 1604(a5)<br> |
| 102|[0x80004c64]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96b8e84b814c5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc2858a780d3ff and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x65e29931e8ba7 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000dc8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dcc]:csrrs a7, fflags, zero<br> [0x80000dd0]:fsd ft11, 1616(a5)<br> [0x80000dd4]:sw a7, 1620(a5)<br> |
| 103|[0x80004c74]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96b8e84b814c5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc2858a780d3ff and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x65e29931e8ba7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000de8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000dec]:csrrs a7, fflags, zero<br> [0x80000df0]:fsd ft11, 1632(a5)<br> [0x80000df4]:sw a7, 1636(a5)<br> |
| 104|[0x80004c84]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96b8e84b814c5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc2858a780d3ff and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x65e29931e8ba7 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000e08]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e0c]:csrrs a7, fflags, zero<br> [0x80000e10]:fsd ft11, 1648(a5)<br> [0x80000e14]:sw a7, 1652(a5)<br> |
| 105|[0x80004c94]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96b8e84b814c5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc2858a780d3ff and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x65e29931e8ba7 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80000e28]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e2c]:csrrs a7, fflags, zero<br> [0x80000e30]:fsd ft11, 1664(a5)<br> [0x80000e34]:sw a7, 1668(a5)<br> |
| 106|[0x80004ca4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x96b8e84b814c5 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xc2858a780d3ff and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x65e29931e8ba7 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80000e48]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e4c]:csrrs a7, fflags, zero<br> [0x80000e50]:fsd ft11, 1680(a5)<br> [0x80000e54]:sw a7, 1684(a5)<br> |
| 107|[0x80004cb4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd00f90ae48549 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x52abf187430ec and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x32f61e9fcc711 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80000e68]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e6c]:csrrs a7, fflags, zero<br> [0x80000e70]:fsd ft11, 1696(a5)<br> [0x80000e74]:sw a7, 1700(a5)<br> |
| 108|[0x80004cc4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd00f90ae48549 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x52abf187430ec and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x32f61e9fcc711 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80000e88]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000e8c]:csrrs a7, fflags, zero<br> [0x80000e90]:fsd ft11, 1712(a5)<br> [0x80000e94]:sw a7, 1716(a5)<br> |
| 109|[0x80004cd4]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xd00f90ae48549 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x52abf187430ec and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x32f61e9fcc711 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80000ea8]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80000eac]:csrrs a7, fflags, zero<br> [0x80000eb0]:fsd ft11, 1728(a5)<br> [0x80000eb4]:sw a7, 1732(a5)<br> |
| 110|[0x80004a0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1871f66d9338f and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xd5883b90379fc and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x012f0320f4e17 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001110]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001114]:csrrs a7, fflags, zero<br> [0x80001118]:fsd ft11, 0(a5)<br> [0x8000111c]:sw a7, 4(a5)<br>       |
| 111|[0x80004a1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1871f66d9338f and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xd5883b90379fc and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x012f0320f4e17 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001130]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001134]:csrrs a7, fflags, zero<br> [0x80001138]:fsd ft11, 16(a5)<br> [0x8000113c]:sw a7, 20(a5)<br>     |
| 112|[0x80004a2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1871f66d9338f and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xd5883b90379fc and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x012f0320f4e17 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001150]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001154]:csrrs a7, fflags, zero<br> [0x80001158]:fsd ft11, 32(a5)<br> [0x8000115c]:sw a7, 36(a5)<br>     |
| 113|[0x80004a3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1871f66d9338f and fs2 == 0 and fe2 == 0x3fb and fm2 == 0xd5883b90379fc and fs3 == 0 and fe3 == 0x7fb and fm3 == 0x012f0320f4e17 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001170]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001174]:csrrs a7, fflags, zero<br> [0x80001178]:fsd ft11, 48(a5)<br> [0x8000117c]:sw a7, 52(a5)<br>     |
| 114|[0x80004a4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x471faa8c06142 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a6cb740159e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d55c80c789ad and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001190]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001194]:csrrs a7, fflags, zero<br> [0x80001198]:fsd ft11, 64(a5)<br> [0x8000119c]:sw a7, 68(a5)<br>     |
| 115|[0x80004a5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x471faa8c06142 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a6cb740159e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d55c80c789ad and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800011b0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011b4]:csrrs a7, fflags, zero<br> [0x800011b8]:fsd ft11, 80(a5)<br> [0x800011bc]:sw a7, 84(a5)<br>     |
| 116|[0x80004a6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x471faa8c06142 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a6cb740159e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d55c80c789ad and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800011d0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011d4]:csrrs a7, fflags, zero<br> [0x800011d8]:fsd ft11, 96(a5)<br> [0x800011dc]:sw a7, 100(a5)<br>    |
| 117|[0x80004a7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x471faa8c06142 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a6cb740159e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d55c80c789ad and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800011f0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800011f4]:csrrs a7, fflags, zero<br> [0x800011f8]:fsd ft11, 112(a5)<br> [0x800011fc]:sw a7, 116(a5)<br>   |
| 118|[0x80004a8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x471faa8c06142 and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x2a6cb740159e7 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x7d55c80c789ad and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001210]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001214]:csrrs a7, fflags, zero<br> [0x80001218]:fsd ft11, 128(a5)<br> [0x8000121c]:sw a7, 132(a5)<br>   |
| 119|[0x80004a9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7220ac1a61dbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9beb88f04a963 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x29c77f248e9cc and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001230]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001234]:csrrs a7, fflags, zero<br> [0x80001238]:fsd ft11, 144(a5)<br> [0x8000123c]:sw a7, 148(a5)<br>   |
| 120|[0x80004aac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7220ac1a61dbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9beb88f04a963 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x29c77f248e9cc and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001250]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001254]:csrrs a7, fflags, zero<br> [0x80001258]:fsd ft11, 160(a5)<br> [0x8000125c]:sw a7, 164(a5)<br>   |
| 121|[0x80004abc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7220ac1a61dbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9beb88f04a963 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x29c77f248e9cc and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001270]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001274]:csrrs a7, fflags, zero<br> [0x80001278]:fsd ft11, 176(a5)<br> [0x8000127c]:sw a7, 180(a5)<br>   |
| 122|[0x80004acc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7220ac1a61dbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9beb88f04a963 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x29c77f248e9cc and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001290]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001294]:csrrs a7, fflags, zero<br> [0x80001298]:fsd ft11, 192(a5)<br> [0x8000129c]:sw a7, 196(a5)<br>   |
| 123|[0x80004adc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x7220ac1a61dbb and fs2 == 0 and fe2 == 0x400 and fm2 == 0x9beb88f04a963 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0x29c77f248e9cc and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800012b0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012b4]:csrrs a7, fflags, zero<br> [0x800012b8]:fsd ft11, 208(a5)<br> [0x800012bc]:sw a7, 212(a5)<br>   |
| 124|[0x80004aec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3be2a8c7b6829 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x52a3bf7842274 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa1db506ba5ee9 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800012d0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012d4]:csrrs a7, fflags, zero<br> [0x800012d8]:fsd ft11, 224(a5)<br> [0x800012dc]:sw a7, 228(a5)<br>   |
| 125|[0x80004afc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3be2a8c7b6829 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x52a3bf7842274 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa1db506ba5ee9 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800012f0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800012f4]:csrrs a7, fflags, zero<br> [0x800012f8]:fsd ft11, 240(a5)<br> [0x800012fc]:sw a7, 244(a5)<br>   |
| 126|[0x80004b0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3be2a8c7b6829 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x52a3bf7842274 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa1db506ba5ee9 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001310]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001314]:csrrs a7, fflags, zero<br> [0x80001318]:fsd ft11, 256(a5)<br> [0x8000131c]:sw a7, 260(a5)<br>   |
| 127|[0x80004b1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3be2a8c7b6829 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x52a3bf7842274 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa1db506ba5ee9 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001330]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001334]:csrrs a7, fflags, zero<br> [0x80001338]:fsd ft11, 272(a5)<br> [0x8000133c]:sw a7, 276(a5)<br>   |
| 128|[0x80004b2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x3be2a8c7b6829 and fs2 == 0 and fe2 == 0x400 and fm2 == 0x52a3bf7842274 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xa1db506ba5ee9 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001350]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001354]:csrrs a7, fflags, zero<br> [0x80001358]:fsd ft11, 288(a5)<br> [0x8000135c]:sw a7, 292(a5)<br>   |
| 129|[0x80004b3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x276f358a6a63b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa21385d91b7e5 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe27a1d244ecc7 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001370]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001374]:csrrs a7, fflags, zero<br> [0x80001378]:fsd ft11, 304(a5)<br> [0x8000137c]:sw a7, 308(a5)<br>   |
| 130|[0x80004b4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x276f358a6a63b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa21385d91b7e5 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe27a1d244ecc7 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001390]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001394]:csrrs a7, fflags, zero<br> [0x80001398]:fsd ft11, 320(a5)<br> [0x8000139c]:sw a7, 324(a5)<br>   |
| 131|[0x80004b5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x276f358a6a63b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa21385d91b7e5 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe27a1d244ecc7 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800013b0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013b4]:csrrs a7, fflags, zero<br> [0x800013b8]:fsd ft11, 336(a5)<br> [0x800013bc]:sw a7, 340(a5)<br>   |
| 132|[0x80004b6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x276f358a6a63b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa21385d91b7e5 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe27a1d244ecc7 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800013d0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013d4]:csrrs a7, fflags, zero<br> [0x800013d8]:fsd ft11, 352(a5)<br> [0x800013dc]:sw a7, 356(a5)<br>   |
| 133|[0x80004b7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x276f358a6a63b and fs2 == 0 and fe2 == 0x3fe and fm2 == 0xa21385d91b7e5 and fs3 == 0 and fe3 == 0x7fb and fm3 == 0xe27a1d244ecc7 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800013f0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800013f4]:csrrs a7, fflags, zero<br> [0x800013f8]:fsd ft11, 368(a5)<br> [0x800013fc]:sw a7, 372(a5)<br>   |
| 134|[0x80004b8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7892d8885ef9 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x5973ca5b38043 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x288f5635a6591 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001410]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001414]:csrrs a7, fflags, zero<br> [0x80001418]:fsd ft11, 384(a5)<br> [0x8000141c]:sw a7, 388(a5)<br>   |
| 135|[0x80004b9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7892d8885ef9 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x5973ca5b38043 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x288f5635a6591 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001430]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001434]:csrrs a7, fflags, zero<br> [0x80001438]:fsd ft11, 400(a5)<br> [0x8000143c]:sw a7, 404(a5)<br>   |
| 136|[0x80004bac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7892d8885ef9 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x5973ca5b38043 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x288f5635a6591 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001450]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001454]:csrrs a7, fflags, zero<br> [0x80001458]:fsd ft11, 416(a5)<br> [0x8000145c]:sw a7, 420(a5)<br>   |
| 137|[0x80004bbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7892d8885ef9 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x5973ca5b38043 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x288f5635a6591 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001470]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001474]:csrrs a7, fflags, zero<br> [0x80001478]:fsd ft11, 432(a5)<br> [0x8000147c]:sw a7, 436(a5)<br>   |
| 138|[0x80004bcc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb7892d8885ef9 and fs2 == 0 and fe2 == 0x3fd and fm2 == 0x5973ca5b38043 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x288f5635a6591 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001490]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001494]:csrrs a7, fflags, zero<br> [0x80001498]:fsd ft11, 448(a5)<br> [0x8000149c]:sw a7, 452(a5)<br>   |
| 139|[0x80004bdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3d7b58e26345 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x86e30b0ef95b6 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x7d9af63a065bf and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800014b0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014b4]:csrrs a7, fflags, zero<br> [0x800014b8]:fsd ft11, 464(a5)<br> [0x800014bc]:sw a7, 468(a5)<br>   |
| 140|[0x80004bec]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3d7b58e26345 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x86e30b0ef95b6 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x7d9af63a065bf and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800014d0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014d4]:csrrs a7, fflags, zero<br> [0x800014d8]:fsd ft11, 480(a5)<br> [0x800014dc]:sw a7, 484(a5)<br>   |
| 141|[0x80004bfc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3d7b58e26345 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x86e30b0ef95b6 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x7d9af63a065bf and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x800014f0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800014f4]:csrrs a7, fflags, zero<br> [0x800014f8]:fsd ft11, 496(a5)<br> [0x800014fc]:sw a7, 500(a5)<br>   |
| 142|[0x80004c0c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3d7b58e26345 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x86e30b0ef95b6 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x7d9af63a065bf and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001510]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001514]:csrrs a7, fflags, zero<br> [0x80001518]:fsd ft11, 512(a5)<br> [0x8000151c]:sw a7, 516(a5)<br>   |
| 143|[0x80004c1c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf3d7b58e26345 and fs2 == 0 and fe2 == 0x3fa and fm2 == 0x86e30b0ef95b6 and fs3 == 0 and fe3 == 0x7fa and fm3 == 0x7d9af63a065bf and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001530]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001534]:csrrs a7, fflags, zero<br> [0x80001538]:fsd ft11, 528(a5)<br> [0x8000153c]:sw a7, 532(a5)<br>   |
| 144|[0x80004c2c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x277a09a57982a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5fee54562abf2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9633680658f13 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001550]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001554]:csrrs a7, fflags, zero<br> [0x80001558]:fsd ft11, 544(a5)<br> [0x8000155c]:sw a7, 548(a5)<br>   |
| 145|[0x80004c3c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x277a09a57982a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5fee54562abf2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9633680658f13 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001570]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001574]:csrrs a7, fflags, zero<br> [0x80001578]:fsd ft11, 560(a5)<br> [0x8000157c]:sw a7, 564(a5)<br>   |
| 146|[0x80004c4c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x277a09a57982a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5fee54562abf2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9633680658f13 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001590]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001594]:csrrs a7, fflags, zero<br> [0x80001598]:fsd ft11, 576(a5)<br> [0x8000159c]:sw a7, 580(a5)<br>   |
| 147|[0x80004c5c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x277a09a57982a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5fee54562abf2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9633680658f13 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x800015b0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015b4]:csrrs a7, fflags, zero<br> [0x800015b8]:fsd ft11, 592(a5)<br> [0x800015bc]:sw a7, 596(a5)<br>   |
| 148|[0x80004c6c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x277a09a57982a and fs2 == 0 and fe2 == 0x3fe and fm2 == 0x5fee54562abf2 and fs3 == 0 and fe3 == 0x7fd and fm3 == 0x9633680658f13 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x800015d0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015d4]:csrrs a7, fflags, zero<br> [0x800015d8]:fsd ft11, 608(a5)<br> [0x800015dc]:sw a7, 612(a5)<br>   |
| 149|[0x80004c7c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x413eed654fd22 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82ad1aedc7830 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe53a1b43f58c2 and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x800015f0]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800015f4]:csrrs a7, fflags, zero<br> [0x800015f8]:fsd ft11, 624(a5)<br> [0x800015fc]:sw a7, 628(a5)<br>   |
| 150|[0x80004c8c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x413eed654fd22 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82ad1aedc7830 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe53a1b43f58c2 and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x80001610]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001614]:csrrs a7, fflags, zero<br> [0x80001618]:fsd ft11, 640(a5)<br> [0x8000161c]:sw a7, 644(a5)<br>   |
| 151|[0x80004c9c]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x413eed654fd22 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82ad1aedc7830 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe53a1b43f58c2 and rm_val == 2  #nosat<br>                                                                                                                                                         |[0x80001630]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001634]:csrrs a7, fflags, zero<br> [0x80001638]:fsd ft11, 656(a5)<br> [0x8000163c]:sw a7, 660(a5)<br>   |
| 152|[0x80004cac]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x413eed654fd22 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82ad1aedc7830 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe53a1b43f58c2 and rm_val == 1  #nosat<br>                                                                                                                                                         |[0x80001650]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001654]:csrrs a7, fflags, zero<br> [0x80001658]:fsd ft11, 672(a5)<br> [0x8000165c]:sw a7, 676(a5)<br>   |
| 153|[0x80004cbc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x413eed654fd22 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0x82ad1aedc7830 and fs3 == 0 and fe3 == 0x7fe and fm3 == 0xe53a1b43f58c2 and rm_val == 0  #nosat<br>                                                                                                                                                         |[0x80001674]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001678]:csrrs a7, fflags, zero<br> [0x8000167c]:fsd ft11, 688(a5)<br> [0x80001680]:sw a7, 692(a5)<br>   |
| 154|[0x80004ccc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xfb17c3e518207 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcd9e20ba842b1 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xc93182a170b2f and rm_val == 4  #nosat<br>                                                                                                                                                         |[0x80001694]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x80001698]:csrrs a7, fflags, zero<br> [0x8000169c]:fsd ft11, 704(a5)<br> [0x800016a0]:sw a7, 708(a5)<br>   |
| 155|[0x80004cdc]<br>0x00000005|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0xfb17c3e518207 and fs2 == 0 and fe2 == 0x3ff and fm2 == 0xcd9e20ba842b1 and fs3 == 0 and fe3 == 0x7fc and fm3 == 0xc93182a170b2f and rm_val == 3  #nosat<br>                                                                                                                                                         |[0x800016b4]:fmsub.d ft11, ft10, ft9, ft8, dyn<br> [0x800016b8]:csrrs a7, fflags, zero<br> [0x800016bc]:fsd ft11, 720(a5)<br> [0x800016c0]:sw a7, 724(a5)<br>   |
