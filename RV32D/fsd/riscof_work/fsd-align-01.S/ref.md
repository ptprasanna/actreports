
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800006d0')]      |
| SIG_REGION                | [('0x80002210', '0x80002310', '64 words')]      |
| COV_LABELS                | fsd-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch12/fsd/riscof_work/fsd-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 60      |
| Total Signature Updates   | 26      |
| STAT1                     | 26      |
| STAT2                     | 0      |
| STAT3                     | 5     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000424]:fsd ft2, 2048(a1)
[0x80000428]:addi zero, zero, 0
[0x8000042c]:addi zero, zero, 0
[0x80000430]:csrrs a7, fflags, zero
[0x80000434]:sw a7, 272(a5)
[0x80000438]:addi a6, a6, 2040
[0x8000043c]:lui a7, 524288
[0x80000440]:fmv.w.x ft4, a7
[0x80000444]:addi ra, a5, 288
[0x80000448]:addi s2, zero, 2048
[0x8000044c]:sub ra, ra, s2

[0x80000450]:fsd ft4, 2048(ra)
[0x80000454]:addi zero, zero, 0
[0x80000458]:addi zero, zero, 0
[0x8000045c]:csrrs a7, fflags, zero
[0x80000460]:sw a7, 288(a5)
[0x80000464]:addi a6, a6, 2040
[0x80000468]:add s4, a6, zero
[0x8000046c]:auipc s3, 2
[0x80000470]:addi s3, s3, 3644
[0x80000474]:lui s5, 524288
[0x80000478]:fmv.w.x ft5, s5
[0x8000047c]:addi a6, s3, 0
[0x80000480]:addi s2, zero, 2048
[0x80000484]:sub a6, a6, s2

[0x80000638]:fsd fs1, 2048(t0)
[0x8000063c]:addi zero, zero, 0
[0x80000640]:addi zero, zero, 0
[0x80000644]:csrrs a7, fflags, zero
[0x80000648]:sw a7, 80(a5)
[0x8000064c]:addi a6, a6, 2040
[0x80000650]:lui a7, 524288
[0x80000654]:fmv.w.x fa4, a7
[0x80000658]:addi s3, a5, 96
[0x8000065c]:addi s2, zero, 2048
[0x80000660]:sub s3, s3, s2

[0x80000664]:fsd fa4, 2048(s3)
[0x80000668]:addi zero, zero, 0
[0x8000066c]:addi zero, zero, 0
[0x80000670]:csrrs a7, fflags, zero
[0x80000674]:sw a7, 96(a5)
[0x80000678]:addi a6, a6, 2040
[0x8000067c]:lui a7, 524288
[0x80000680]:fmv.w.x fs8, a7
[0x80000684]:addi fp, a5, 112
[0x80000688]:addi s2, zero, 2048
[0x8000068c]:sub fp, fp, s2

[0x80000690]:fsd fs8, 2048(fp)
[0x80000694]:addi zero, zero, 0
[0x80000698]:addi zero, zero, 0
[0x8000069c]:csrrs a7, fflags, zero
[0x800006a0]:sw a7, 112(a5)
[0x800006a4]:addi a6, a6, 2040
[0x800006a8]:lui a7, 524288
[0x800006ac]:fmv.w.x fs11, a7
[0x800006b0]:addi s8, a5, 128
[0x800006b4]:addi s2, zero, 2048
[0x800006b8]:sub s8, s8, s2



```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|        signature         |                                                  coverpoints                                                   |                                                                                        code                                                                                        |
|---:|--------------------------|----------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x00000000|- opcode : fsd<br> - rs1 : x28<br> - rs2 : f10<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x80000128]:fsd fa0, 1024(t3)<br> [0x8000012c]:addi zero, zero, 0<br> [0x80000130]:addi zero, zero, 0<br> [0x80000134]:csrrs a7, fflags, zero<br> [0x80000138]:sw a7, 0(a5)<br>    |
|   2|[0x80002220]<br>0x00000000|- rs1 : x4<br> - rs2 : f12<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                       |[0x80000158]:fsd fa2, 1365(tp)<br> [0x8000015c]:addi zero, zero, 0<br> [0x80000160]:addi zero, zero, 0<br> [0x80000164]:csrrs a7, fflags, zero<br> [0x80000168]:sw a7, 16(a5)<br>   |
|   3|[0x80002230]<br>0x00000000|- rs1 : x25<br> - rs2 : f7<br> - ea_align == 0 and (imm_val % 4) == 2<br> - imm_val < 0<br>                     |[0x80000188]:fsd ft7, 4086(s9)<br> [0x8000018c]:addi zero, zero, 0<br> [0x80000190]:addi zero, zero, 0<br> [0x80000194]:csrrs a7, fflags, zero<br> [0x80000198]:sw a7, 32(a5)<br>   |
|   4|[0x80002240]<br>0x00000000|- rs1 : x6<br> - rs2 : f3<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                        |[0x800001b8]:fsd ft3, 4091(t1)<br> [0x800001bc]:addi zero, zero, 0<br> [0x800001c0]:addi zero, zero, 0<br> [0x800001c4]:csrrs a7, fflags, zero<br> [0x800001c8]:sw a7, 48(a5)<br>   |
|   5|[0x80002250]<br>0x00000000|- rs1 : x21<br> - rs2 : f17<br> - imm_val == 0<br>                                                              |[0x800001e8]:fsd fa7, 0(s5)<br> [0x800001ec]:addi zero, zero, 0<br> [0x800001f0]:addi zero, zero, 0<br> [0x800001f4]:csrrs a7, fflags, zero<br> [0x800001f8]:sw a7, 64(a5)<br>      |
|   6|[0x80002260]<br>0x00000000|- rs1 : x9<br> - rs2 : f13<br>                                                                                  |[0x80000214]:fsd fa3, 2048(s1)<br> [0x80000218]:addi zero, zero, 0<br> [0x8000021c]:addi zero, zero, 0<br> [0x80000220]:csrrs a7, fflags, zero<br> [0x80000224]:sw a7, 80(a5)<br>   |
|   7|[0x80002270]<br>0x00000000|- rs1 : x24<br> - rs2 : f25<br>                                                                                 |[0x80000240]:fsd fs9, 2048(s8)<br> [0x80000244]:addi zero, zero, 0<br> [0x80000248]:addi zero, zero, 0<br> [0x8000024c]:csrrs a7, fflags, zero<br> [0x80000250]:sw a7, 96(a5)<br>   |
|   8|[0x80002280]<br>0x00000000|- rs1 : x2<br> - rs2 : f29<br>                                                                                  |[0x8000026c]:fsd ft9, 2048(sp)<br> [0x80000270]:addi zero, zero, 0<br> [0x80000274]:addi zero, zero, 0<br> [0x80000278]:csrrs a7, fflags, zero<br> [0x8000027c]:sw a7, 112(a5)<br>  |
|   9|[0x80002290]<br>0x00000000|- rs1 : x23<br> - rs2 : f20<br>                                                                                 |[0x80000298]:fsd fs4, 2048(s7)<br> [0x8000029c]:addi zero, zero, 0<br> [0x800002a0]:addi zero, zero, 0<br> [0x800002a4]:csrrs a7, fflags, zero<br> [0x800002a8]:sw a7, 128(a5)<br>  |
|  10|[0x800022a0]<br>0x00000000|- rs1 : x20<br> - rs2 : f26<br>                                                                                 |[0x800002c4]:fsd fs10, 2048(s4)<br> [0x800002c8]:addi zero, zero, 0<br> [0x800002cc]:addi zero, zero, 0<br> [0x800002d0]:csrrs a7, fflags, zero<br> [0x800002d4]:sw a7, 144(a5)<br> |
|  11|[0x800022b0]<br>0x00000000|- rs1 : x31<br> - rs2 : f30<br>                                                                                 |[0x800002f0]:fsd ft10, 2048(t6)<br> [0x800002f4]:addi zero, zero, 0<br> [0x800002f8]:addi zero, zero, 0<br> [0x800002fc]:csrrs a7, fflags, zero<br> [0x80000300]:sw a7, 160(a5)<br> |
|  12|[0x800022c0]<br>0x00000000|- rs1 : x10<br> - rs2 : f28<br>                                                                                 |[0x8000031c]:fsd ft8, 2048(a0)<br> [0x80000320]:addi zero, zero, 0<br> [0x80000324]:addi zero, zero, 0<br> [0x80000328]:csrrs a7, fflags, zero<br> [0x8000032c]:sw a7, 176(a5)<br>  |
|  13|[0x800022d0]<br>0x00000000|- rs1 : x30<br> - rs2 : f0<br>                                                                                  |[0x80000348]:fsd ft0, 2048(t5)<br> [0x8000034c]:addi zero, zero, 0<br> [0x80000350]:addi zero, zero, 0<br> [0x80000354]:csrrs a7, fflags, zero<br> [0x80000358]:sw a7, 192(a5)<br>  |
|  14|[0x800022e0]<br>0x00000000|- rs1 : x22<br> - rs2 : f11<br>                                                                                 |[0x80000374]:fsd fa1, 2048(s6)<br> [0x80000378]:addi zero, zero, 0<br> [0x8000037c]:addi zero, zero, 0<br> [0x80000380]:csrrs a7, fflags, zero<br> [0x80000384]:sw a7, 208(a5)<br>  |
|  15|[0x800022f0]<br>0x00000000|- rs1 : x18<br> - rs2 : f21<br>                                                                                 |[0x800003a0]:fsd fs5, 2048(s2)<br> [0x800003a4]:addi zero, zero, 0<br> [0x800003a8]:addi zero, zero, 0<br> [0x800003ac]:csrrs a7, fflags, zero<br> [0x800003b0]:sw a7, 224(a5)<br>  |
|  16|[0x80002300]<br>0x00000000|- rs1 : x26<br> - rs2 : f22<br>                                                                                 |[0x800003cc]:fsd fs6, 2048(s10)<br> [0x800003d0]:addi zero, zero, 0<br> [0x800003d4]:addi zero, zero, 0<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:sw a7, 240(a5)<br> |
|  17|[0x80002310]<br>0x00000000|- rs1 : x7<br> - rs2 : f19<br>                                                                                  |[0x800003f8]:fsd fs3, 2048(t2)<br> [0x800003fc]:addi zero, zero, 0<br> [0x80000400]:addi zero, zero, 0<br> [0x80000404]:csrrs a7, fflags, zero<br> [0x80000408]:sw a7, 256(a5)<br>  |
|  18|[0x800022a8]<br>0x00000000|- rs1 : x16<br> - rs2 : f5<br>                                                                                  |[0x80000488]:fsd ft5, 2048(a6)<br> [0x8000048c]:addi zero, zero, 0<br> [0x80000490]:addi zero, zero, 0<br> [0x80000494]:csrrs s5, fflags, zero<br> [0x80000498]:sw s5, 0(s3)<br>    |
|  19|[0x800022b0]<br>0x00000000|- rs1 : x29<br> - rs2 : f18<br>                                                                                 |[0x800004c0]:fsd fs2, 2048(t4)<br> [0x800004c4]:addi zero, zero, 0<br> [0x800004c8]:addi zero, zero, 0<br> [0x800004cc]:csrrs a7, fflags, zero<br> [0x800004d0]:sw a7, 0(a5)<br>    |
|  20|[0x800022b8]<br>0x00000000|- rs1 : x15<br> - rs2 : f16<br>                                                                                 |[0x800004f8]:fsd fa6, 2048(a5)<br> [0x800004fc]:addi zero, zero, 0<br> [0x80000500]:addi zero, zero, 0<br> [0x80000504]:csrrs s5, fflags, zero<br> [0x80000508]:sw s5, 0(s3)<br>    |
|  21|[0x800022c8]<br>0x00000000|- rs1 : x17<br> - rs2 : f31<br>                                                                                 |[0x80000524]:fsd ft11, 2048(a7)<br> [0x80000528]:addi zero, zero, 0<br> [0x8000052c]:addi zero, zero, 0<br> [0x80000530]:csrrs s5, fflags, zero<br> [0x80000534]:sw s5, 16(s3)<br>  |
|  22|[0x800022c8]<br>0x00000000|- rs1 : x3<br> - rs2 : f1<br>                                                                                   |[0x8000055c]:fsd ft1, 2048(gp)<br> [0x80000560]:addi zero, zero, 0<br> [0x80000564]:addi zero, zero, 0<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:sw a7, 0(a5)<br>    |
|  23|[0x800022d8]<br>0x00000000|- rs1 : x13<br> - rs2 : f6<br>                                                                                  |[0x80000588]:fsd ft6, 2048(a3)<br> [0x8000058c]:addi zero, zero, 0<br> [0x80000590]:addi zero, zero, 0<br> [0x80000594]:csrrs a7, fflags, zero<br> [0x80000598]:sw a7, 16(a5)<br>   |
|  24|[0x800022e8]<br>0x00000000|- rs1 : x14<br> - rs2 : f8<br>                                                                                  |[0x800005b4]:fsd fs0, 2048(a4)<br> [0x800005b8]:addi zero, zero, 0<br> [0x800005bc]:addi zero, zero, 0<br> [0x800005c0]:csrrs a7, fflags, zero<br> [0x800005c4]:sw a7, 32(a5)<br>   |
|  25|[0x800022f8]<br>0x00000000|- rs1 : x12<br> - rs2 : f23<br>                                                                                 |[0x800005e0]:fsd fs7, 2048(a2)<br> [0x800005e4]:addi zero, zero, 0<br> [0x800005e8]:addi zero, zero, 0<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:sw a7, 48(a5)<br>   |
|  26|[0x80002308]<br>0x00000000|- rs1 : x27<br> - rs2 : f15<br>                                                                                 |[0x8000060c]:fsd fa5, 2048(s11)<br> [0x80000610]:addi zero, zero, 0<br> [0x80000614]:addi zero, zero, 0<br> [0x80000618]:csrrs a7, fflags, zero<br> [0x8000061c]:sw a7, 64(a5)<br>  |
