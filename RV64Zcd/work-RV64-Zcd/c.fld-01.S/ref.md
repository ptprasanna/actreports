
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000590')]      |
| SIG_REGION                | [('0x80002210', '0x80002300', '30 dwords')]      |
| COV_LABELS                | c.fld      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV64-Zcd/c.fld-01.S/ref.S    |
| Total Number of coverpoints| 32     |
| Total Coverpoints Hit     | 32      |
| Total Signature Updates   | 28      |
| STAT1                     | 14      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 14     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fld', 'rs1 : x15', 'rd : f15', 'rs1 != rd', 'imm_val == 0 and fcsr == 0']
Last Code Sequence : 
	-[0x800003ba]:c.fld
	-[0x800003bc]:c.nop
	-[0x800003be]:c.nop
	-[0x800003c0]:csrrs
	-[0x800003c4]:fsd
Current Store : [0x800003c8] : None -- Store: [0x80002220]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : f14', 'imm_val > 0  and fcsr == 0']
Last Code Sequence : 
	-[0x800003dc]:c.fld
	-[0x800003de]:c.nop
	-[0x800003e0]:c.nop
	-[0x800003e2]:csrrs
	-[0x800003e6]:fsd
Current Store : [0x800003ea] : None -- Store: [0x80002230]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : f13', 'imm_val == 168']
Last Code Sequence : 
	-[0x800003fe]:c.fld
	-[0x80000400]:c.nop
	-[0x80000402]:c.nop
	-[0x80000404]:csrrs
	-[0x80000408]:fsd
Current Store : [0x8000040c] : None -- Store: [0x80002240]:0x000000000000009f




Last Coverpoint : ['rs1 : x12', 'rd : f12', 'imm_val == 80']
Last Code Sequence : 
	-[0x80000420]:c.fld
	-[0x80000422]:c.nop
	-[0x80000424]:c.nop
	-[0x80000426]:csrrs
	-[0x8000042a]:fsd
Current Store : [0x8000042e] : None -- Store: [0x80002250]:0x000000000000009f




Last Coverpoint : ['rs1 : x11', 'rd : f11', 'imm_val == 8']
Last Code Sequence : 
	-[0x80000442]:c.fld
	-[0x80000444]:c.nop
	-[0x80000446]:c.nop
	-[0x80000448]:csrrs
	-[0x8000044c]:fsd
Current Store : [0x80000450] : None -- Store: [0x80002260]:0x000000000000009f




Last Coverpoint : ['rs1 : x10', 'rd : f10', 'imm_val == 16']
Last Code Sequence : 
	-[0x80000464]:c.fld
	-[0x80000466]:c.nop
	-[0x80000468]:c.nop
	-[0x8000046a]:csrrs
	-[0x8000046e]:fsd
Current Store : [0x80000472] : None -- Store: [0x80002270]:0x000000000000009f




Last Coverpoint : ['rs1 : x9', 'rd : f9', 'imm_val == 240']
Last Code Sequence : 
	-[0x80000486]:c.fld
	-[0x80000488]:c.nop
	-[0x8000048a]:c.nop
	-[0x8000048c]:csrrs
	-[0x80000490]:fsd
Current Store : [0x80000494] : None -- Store: [0x80002280]:0x000000000000009f




Last Coverpoint : ['rs1 : x8', 'rd : f8', 'imm_val == 232']
Last Code Sequence : 
	-[0x800004a8]:c.fld
	-[0x800004aa]:c.nop
	-[0x800004ac]:c.nop
	-[0x800004ae]:csrrs
	-[0x800004b2]:fsd
Current Store : [0x800004b6] : None -- Store: [0x80002290]:0x000000000000009f




Last Coverpoint : ['imm_val == 216']
Last Code Sequence : 
	-[0x800004ca]:c.fld
	-[0x800004cc]:c.nop
	-[0x800004ce]:c.nop
	-[0x800004d0]:csrrs
	-[0x800004d4]:fsd
Current Store : [0x800004d8] : None -- Store: [0x800022a0]:0x000000000000009f




Last Coverpoint : ['imm_val == 184']
Last Code Sequence : 
	-[0x800004ec]:c.fld
	-[0x800004ee]:c.nop
	-[0x800004f0]:c.nop
	-[0x800004f2]:csrrs
	-[0x800004f6]:fsd
Current Store : [0x800004fa] : None -- Store: [0x800022b0]:0x000000000000009f




Last Coverpoint : ['imm_val == 120']
Last Code Sequence : 
	-[0x8000050e]:c.fld
	-[0x80000510]:c.nop
	-[0x80000512]:c.nop
	-[0x80000514]:csrrs
	-[0x80000518]:fsd
Current Store : [0x8000051c] : None -- Store: [0x800022c0]:0x000000000000009f




Last Coverpoint : ['imm_val == 32']
Last Code Sequence : 
	-[0x80000530]:c.fld
	-[0x80000532]:c.nop
	-[0x80000534]:c.nop
	-[0x80000536]:csrrs
	-[0x8000053a]:fsd
Current Store : [0x8000053e] : None -- Store: [0x800022d0]:0x000000000000009f




Last Coverpoint : ['imm_val == 64']
Last Code Sequence : 
	-[0x80000552]:c.fld
	-[0x80000554]:c.nop
	-[0x80000556]:c.nop
	-[0x80000558]:csrrs
	-[0x8000055c]:fsd
Current Store : [0x80000560] : None -- Store: [0x800022e0]:0x000000000000009f




Last Coverpoint : ['imm_val == 128']
Last Code Sequence : 
	-[0x80000574]:c.fld
	-[0x80000576]:c.nop
	-[0x80000578]:c.nop
	-[0x8000057a]:csrrs
	-[0x8000057e]:fsd
Current Store : [0x80000582] : None -- Store: [0x800022f0]:0x000000000000009f





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                              coverpoints                                              |                                                      code                                                       |
|---:|---------------------------------------|-------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x0000000080002000<br> |- mnemonic : c.fld<br> - rs1 : x15<br> - rd : f15<br> - rs1 != rd<br> - imm_val == 0 and fcsr == 0<br> |[0x800003ba]:c.fld<br> [0x800003bc]:c.nop<br> [0x800003be]:c.nop<br> [0x800003c0]:csrrs<br> [0x800003c4]:fsd<br> |
|   2|[0x80002228]<br>0x0000000080001f08<br> |- rs1 : x14<br> - rd : f14<br> - imm_val > 0  and fcsr == 0<br>                                        |[0x800003dc]:c.fld<br> [0x800003de]:c.nop<br> [0x800003e0]:c.nop<br> [0x800003e2]:csrrs<br> [0x800003e6]:fsd<br> |
|   3|[0x80002238]<br>0x0000000080001f58<br> |- rs1 : x13<br> - rd : f13<br> - imm_val == 168<br>                                                    |[0x800003fe]:c.fld<br> [0x80000400]:c.nop<br> [0x80000402]:c.nop<br> [0x80000404]:csrrs<br> [0x80000408]:fsd<br> |
|   4|[0x80002248]<br>0x0000000080001fb0<br> |- rs1 : x12<br> - rd : f12<br> - imm_val == 80<br>                                                     |[0x80000420]:c.fld<br> [0x80000422]:c.nop<br> [0x80000424]:c.nop<br> [0x80000426]:csrrs<br> [0x8000042a]:fsd<br> |
|   5|[0x80002258]<br>0x0000000080001ff8<br> |- rs1 : x11<br> - rd : f11<br> - imm_val == 8<br>                                                      |[0x80000442]:c.fld<br> [0x80000444]:c.nop<br> [0x80000446]:c.nop<br> [0x80000448]:csrrs<br> [0x8000044c]:fsd<br> |
|   6|[0x80002268]<br>0x0000000080001ff0<br> |- rs1 : x10<br> - rd : f10<br> - imm_val == 16<br>                                                     |[0x80000464]:c.fld<br> [0x80000466]:c.nop<br> [0x80000468]:c.nop<br> [0x8000046a]:csrrs<br> [0x8000046e]:fsd<br> |
|   7|[0x80002278]<br>0x0000000080001f10<br> |- rs1 : x9<br> - rd : f9<br> - imm_val == 240<br>                                                      |[0x80000486]:c.fld<br> [0x80000488]:c.nop<br> [0x8000048a]:c.nop<br> [0x8000048c]:csrrs<br> [0x80000490]:fsd<br> |
|   8|[0x80002288]<br>0x0000000080001f18<br> |- rs1 : x8<br> - rd : f8<br> - imm_val == 232<br>                                                      |[0x800004a8]:c.fld<br> [0x800004aa]:c.nop<br> [0x800004ac]:c.nop<br> [0x800004ae]:csrrs<br> [0x800004b2]:fsd<br> |
|   9|[0x80002298]<br>0x0000000080001f28<br> |- imm_val == 216<br>                                                                                   |[0x800004ca]:c.fld<br> [0x800004cc]:c.nop<br> [0x800004ce]:c.nop<br> [0x800004d0]:csrrs<br> [0x800004d4]:fsd<br> |
|  10|[0x800022a8]<br>0x0000000080001f48<br> |- imm_val == 184<br>                                                                                   |[0x800004ec]:c.fld<br> [0x800004ee]:c.nop<br> [0x800004f0]:c.nop<br> [0x800004f2]:csrrs<br> [0x800004f6]:fsd<br> |
|  11|[0x800022b8]<br>0x0000000080001f88<br> |- imm_val == 120<br>                                                                                   |[0x8000050e]:c.fld<br> [0x80000510]:c.nop<br> [0x80000512]:c.nop<br> [0x80000514]:csrrs<br> [0x80000518]:fsd<br> |
|  12|[0x800022c8]<br>0x0000000080001fe0<br> |- imm_val == 32<br>                                                                                    |[0x80000530]:c.fld<br> [0x80000532]:c.nop<br> [0x80000534]:c.nop<br> [0x80000536]:csrrs<br> [0x8000053a]:fsd<br> |
|  13|[0x800022d8]<br>0x0000000080001fc0<br> |- imm_val == 64<br>                                                                                    |[0x80000552]:c.fld<br> [0x80000554]:c.nop<br> [0x80000556]:c.nop<br> [0x80000558]:csrrs<br> [0x8000055c]:fsd<br> |
|  14|[0x800022e8]<br>0x0000000080001f80<br> |- imm_val == 128<br>                                                                                   |[0x80000574]:c.fld<br> [0x80000576]:c.nop<br> [0x80000578]:c.nop<br> [0x8000057a]:csrrs<br> [0x8000057e]:fsd<br> |
