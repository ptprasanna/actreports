
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000860')]      |
| SIG_REGION                | [('0x80002210', '0x80002420', '66 dwords')]      |
| COV_LABELS                | c.fsdsp      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV64-Zcd/c.fsdsp-01.S/ref.S    |
| Total Number of coverpoints| 49     |
| Total Coverpoints Hit     | 49      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fsdsp', 'rs2 : f31', 'imm_val == 0']
Last Code Sequence : 
	-[0x800003b8]:c.fsdsp
Current Store : [0x800003c2] : None -- Store: [0x80002220]:0x0000000000000000




Last Coverpoint : ['rs2 : f30', 'imm_val > 0']
Last Code Sequence : 
	-[0x800003de]:c.fsdsp
Current Store : [0x800003e8] : None -- Store: [0x80002230]:0x0000000000000000




Last Coverpoint : ['rs2 : f29', 'imm_val == 168']
Last Code Sequence : 
	-[0x80000404]:c.fsdsp
Current Store : [0x8000040e] : None -- Store: [0x80002240]:0x0000000000000000




Last Coverpoint : ['rs2 : f28', 'imm_val == 336']
Last Code Sequence : 
	-[0x8000042a]:c.fsdsp
Current Store : [0x80000434] : None -- Store: [0x80002250]:0x0000000000000000




Last Coverpoint : ['rs2 : f27', 'imm_val == 496']
Last Code Sequence : 
	-[0x80000450]:c.fsdsp
Current Store : [0x8000045a] : None -- Store: [0x80002260]:0x0000000000000000




Last Coverpoint : ['rs2 : f26', 'imm_val == 488']
Last Code Sequence : 
	-[0x80000476]:c.fsdsp
Current Store : [0x80000480] : None -- Store: [0x80002270]:0x0000000000000000




Last Coverpoint : ['rs2 : f25', 'imm_val == 472']
Last Code Sequence : 
	-[0x8000049c]:c.fsdsp
Current Store : [0x800004a6] : None -- Store: [0x80002280]:0x0000000000000000




Last Coverpoint : ['rs2 : f24', 'imm_val == 440']
Last Code Sequence : 
	-[0x800004c2]:c.fsdsp
Current Store : [0x800004cc] : None -- Store: [0x80002290]:0x0000000000000000




Last Coverpoint : ['rs2 : f23', 'imm_val == 376']
Last Code Sequence : 
	-[0x800004e8]:c.fsdsp
Current Store : [0x800004f2] : None -- Store: [0x800022a0]:0x0000000000000000




Last Coverpoint : ['rs2 : f22', 'imm_val == 248']
Last Code Sequence : 
	-[0x8000050e]:c.fsdsp
Current Store : [0x80000518] : None -- Store: [0x800022b0]:0x0000000000000000




Last Coverpoint : ['rs2 : f21', 'imm_val == 8']
Last Code Sequence : 
	-[0x80000534]:c.fsdsp
Current Store : [0x8000053e] : None -- Store: [0x800022c0]:0x0000000000000000




Last Coverpoint : ['rs2 : f20', 'imm_val == 16']
Last Code Sequence : 
	-[0x8000055a]:c.fsdsp
Current Store : [0x80000564] : None -- Store: [0x800022d0]:0x0000000000000000




Last Coverpoint : ['rs2 : f19', 'imm_val == 32']
Last Code Sequence : 
	-[0x80000580]:c.fsdsp
Current Store : [0x8000058a] : None -- Store: [0x800022e0]:0x0000000000000000




Last Coverpoint : ['rs2 : f18', 'imm_val == 64']
Last Code Sequence : 
	-[0x800005a6]:c.fsdsp
Current Store : [0x800005b0] : None -- Store: [0x800022f0]:0x0000000000000000




Last Coverpoint : ['rs2 : f17', 'imm_val == 128']
Last Code Sequence : 
	-[0x800005cc]:c.fsdsp
Current Store : [0x800005d6] : None -- Store: [0x80002300]:0x0000000000000000




Last Coverpoint : ['rs2 : f16', 'imm_val == 256']
Last Code Sequence : 
	-[0x800005f2]:c.fsdsp
Current Store : [0x800005fc] : None -- Store: [0x80002310]:0x0000000000000000




Last Coverpoint : ['rs2 : f15']
Last Code Sequence : 
	-[0x80000618]:c.fsdsp
Current Store : [0x80000622] : None -- Store: [0x80002320]:0x0000000000000000




Last Coverpoint : ['rs2 : f14']
Last Code Sequence : 
	-[0x8000063e]:c.fsdsp
Current Store : [0x80000648] : None -- Store: [0x80002330]:0x0000000000000000




Last Coverpoint : ['rs2 : f13']
Last Code Sequence : 
	-[0x80000664]:c.fsdsp
Current Store : [0x8000066e] : None -- Store: [0x80002340]:0x0000000000000000




Last Coverpoint : ['rs2 : f12']
Last Code Sequence : 
	-[0x8000068a]:c.fsdsp
Current Store : [0x80000694] : None -- Store: [0x80002350]:0x0000000000000000




Last Coverpoint : ['rs2 : f11']
Last Code Sequence : 
	-[0x800006b0]:c.fsdsp
Current Store : [0x800006ba] : None -- Store: [0x80002360]:0x0000000000000000




Last Coverpoint : ['rs2 : f10']
Last Code Sequence : 
	-[0x800006d6]:c.fsdsp
Current Store : [0x800006e0] : None -- Store: [0x80002370]:0x0000000000000000




Last Coverpoint : ['rs2 : f9']
Last Code Sequence : 
	-[0x800006fc]:c.fsdsp
Current Store : [0x80000706] : None -- Store: [0x80002380]:0x0000000000000000




Last Coverpoint : ['rs2 : f8']
Last Code Sequence : 
	-[0x80000722]:c.fsdsp
Current Store : [0x8000072c] : None -- Store: [0x80002390]:0x0000000000000000




Last Coverpoint : ['rs2 : f7']
Last Code Sequence : 
	-[0x80000748]:c.fsdsp
Current Store : [0x80000752] : None -- Store: [0x800023a0]:0x0000000000000000




Last Coverpoint : ['rs2 : f6']
Last Code Sequence : 
	-[0x8000076e]:c.fsdsp
Current Store : [0x80000778] : None -- Store: [0x800023b0]:0x0000000000000000




Last Coverpoint : ['rs2 : f5']
Last Code Sequence : 
	-[0x80000794]:c.fsdsp
Current Store : [0x8000079e] : None -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rs2 : f4']
Last Code Sequence : 
	-[0x800007ba]:c.fsdsp
Current Store : [0x800007c4] : None -- Store: [0x800023d0]:0x0000000000000000




Last Coverpoint : ['rs2 : f3']
Last Code Sequence : 
	-[0x800007e0]:c.fsdsp
Current Store : [0x800007ea] : None -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rs2 : f2']
Last Code Sequence : 
	-[0x80000806]:c.fsdsp
Current Store : [0x80000810] : None -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rs2 : f1']
Last Code Sequence : 
	-[0x8000082c]:c.fsdsp
Current Store : [0x80000836] : None -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs2 : f0']
Last Code Sequence : 
	-[0x80000852]:c.fsdsp
Current Store : [0x8000085c] : None -- Store: [0x80002410]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                        coverpoints                         |          code           |
|---:|---------------------------------------|------------------------------------------------------------|-------------------------|
|   1|[0x80002218]<br>0xfbb6fab7fbb6fab7<br> |- mnemonic : c.fsdsp<br> - rs2 : f31<br> - imm_val == 0<br> |[0x800003b8]:c.fsdsp<br> |
|   2|[0x80002228]<br>0xf76df56ff76df56f<br> |- rs2 : f30<br> - imm_val > 0<br>                           |[0x800003de]:c.fsdsp<br> |
|   3|[0x80002238]<br>0xeedbeadfeedbeadf<br> |- rs2 : f29<br> - imm_val == 168<br>                        |[0x80000404]:c.fsdsp<br> |
|   4|[0x80002248]<br>0xddb7d5bfddb7d5bf<br> |- rs2 : f28<br> - imm_val == 336<br>                        |[0x8000042a]:c.fsdsp<br> |
|   5|[0x80002258]<br>0xbb6fab7fbb6fab7f<br> |- rs2 : f27<br> - imm_val == 496<br>                        |[0x80000450]:c.fsdsp<br> |
|   6|[0x80002268]<br>0x76df56ff76df56ff<br> |- rs2 : f26<br> - imm_val == 488<br>                        |[0x80000476]:c.fsdsp<br> |
|   7|[0x80002278]<br>0xedbeadfeedbeadfe<br> |- rs2 : f25<br> - imm_val == 472<br>                        |[0x8000049c]:c.fsdsp<br> |
|   8|[0x80002288]<br>0xdb7d5bfddb7d5bfd<br> |- rs2 : f24<br> - imm_val == 440<br>                        |[0x800004c2]:c.fsdsp<br> |
|   9|[0x80002298]<br>0xb6fab7fbb6fab7fb<br> |- rs2 : f23<br> - imm_val == 376<br>                        |[0x800004e8]:c.fsdsp<br> |
|  10|[0x800022a8]<br>0x6df56ff76df56ff7<br> |- rs2 : f22<br> - imm_val == 248<br>                        |[0x8000050e]:c.fsdsp<br> |
|  11|[0x800022b8]<br>0xdbeadfeedbeadfee<br> |- rs2 : f21<br> - imm_val == 8<br>                          |[0x80000534]:c.fsdsp<br> |
|  12|[0x800022c8]<br>0xb7d5bfddb7d5bfdd<br> |- rs2 : f20<br> - imm_val == 16<br>                         |[0x8000055a]:c.fsdsp<br> |
|  13|[0x800022d8]<br>0x6fab7fbb6fab7fbb<br> |- rs2 : f19<br> - imm_val == 32<br>                         |[0x80000580]:c.fsdsp<br> |
|  14|[0x800022e8]<br>0xdf56ff76df56ff76<br> |- rs2 : f18<br> - imm_val == 64<br>                         |[0x800005a6]:c.fsdsp<br> |
|  15|[0x800022f8]<br>0xbeadfeedbeadfeed<br> |- rs2 : f17<br> - imm_val == 128<br>                        |[0x800005cc]:c.fsdsp<br> |
|  16|[0x80002308]<br>0x7d5bfddb7d5bfddb<br> |- rs2 : f16<br> - imm_val == 256<br>                        |[0x800005f2]:c.fsdsp<br> |
|  17|[0x80002318]<br>0xfab7fbb6fab7fbb6<br> |- rs2 : f15<br>                                             |[0x80000618]:c.fsdsp<br> |
|  18|[0x80002328]<br>0xf56ff76df56ff76d<br> |- rs2 : f14<br>                                             |[0x8000063e]:c.fsdsp<br> |
|  19|[0x80002338]<br>0xeadfeedbeadfeedb<br> |- rs2 : f13<br>                                             |[0x80000664]:c.fsdsp<br> |
|  20|[0x80002348]<br>0xd5bfddb7d5bfddb7<br> |- rs2 : f12<br>                                             |[0x8000068a]:c.fsdsp<br> |
|  21|[0x80002358]<br>0xab7fbb6fab7fbb6f<br> |- rs2 : f11<br>                                             |[0x800006b0]:c.fsdsp<br> |
|  22|[0x80002368]<br>0x56ff76df56ff76df<br> |- rs2 : f10<br>                                             |[0x800006d6]:c.fsdsp<br> |
|  23|[0x80002378]<br>0xadfeedbeadfeedbe<br> |- rs2 : f9<br>                                              |[0x800006fc]:c.fsdsp<br> |
|  24|[0x80002388]<br>0x5bfddb7d5bfddb7d<br> |- rs2 : f8<br>                                              |[0x80000722]:c.fsdsp<br> |
|  25|[0x80002398]<br>0xb7fbb6fab7fbb6fa<br> |- rs2 : f7<br>                                              |[0x80000748]:c.fsdsp<br> |
|  26|[0x800023a8]<br>0x0000000080002000<br> |- rs2 : f6<br>                                              |[0x8000076e]:c.fsdsp<br> |
|  27|[0x800023b8]<br>0x0000000000000000<br> |- rs2 : f5<br>                                              |[0x80000794]:c.fsdsp<br> |
|  28|[0x800023c8]<br>0x0000000080002010<br> |- rs2 : f4<br>                                              |[0x800007ba]:c.fsdsp<br> |
|  29|[0x800023d8]<br>0x0000000000000000<br> |- rs2 : f3<br>                                              |[0x800007e0]:c.fsdsp<br> |
|  30|[0x800023e8]<br>0x00000000800023e8<br> |- rs2 : f2<br>                                              |[0x80000806]:c.fsdsp<br> |
|  31|[0x800023f8]<br>0x0000000080002218<br> |- rs2 : f1<br>                                              |[0x8000082c]:c.fsdsp<br> |
|  32|[0x80002408]<br>0x0000000000000000<br> |- rs2 : f0<br>                                              |[0x80000852]:c.fsdsp<br> |
