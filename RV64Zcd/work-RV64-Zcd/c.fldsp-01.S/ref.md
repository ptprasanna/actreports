
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800007f0')]      |
| SIG_REGION                | [('0x80002210', '0x80002420', '66 dwords')]      |
| COV_LABELS                | c.fldsp      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV64-Zcd/c.fldsp-01.S/ref.S    |
| Total Number of coverpoints| 49     |
| Total Coverpoints Hit     | 49      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fldsp', 'rd : f31', 'imm_val == 0']
Last Code Sequence : 
	-[0x800003b2]:c.fldsp
	-[0x800003b4]:c.nop
	-[0x800003b6]:c.nop
	-[0x800003b8]:csrrs
	-[0x800003bc]:fsd
Current Store : [0x800003c0] : None -- Store: [0x80002220]:0x0000000000000000




Last Coverpoint : ['rd : f30', 'imm_val > 0']
Last Code Sequence : 
	-[0x800003d4]:c.fldsp
	-[0x800003d6]:c.nop
	-[0x800003d8]:c.nop
	-[0x800003da]:csrrs
	-[0x800003de]:fsd
Current Store : [0x800003e2] : None -- Store: [0x80002230]:0x0000000000000000




Last Coverpoint : ['rd : f29', 'imm_val == 168']
Last Code Sequence : 
	-[0x800003f6]:c.fldsp
	-[0x800003f8]:c.nop
	-[0x800003fa]:c.nop
	-[0x800003fc]:csrrs
	-[0x80000400]:fsd
Current Store : [0x80000404] : None -- Store: [0x80002240]:0x0000000000000000




Last Coverpoint : ['rd : f28', 'imm_val == 336']
Last Code Sequence : 
	-[0x80000418]:c.fldsp
	-[0x8000041a]:c.nop
	-[0x8000041c]:c.nop
	-[0x8000041e]:csrrs
	-[0x80000422]:fsd
Current Store : [0x80000426] : None -- Store: [0x80002250]:0x0000000000000000




Last Coverpoint : ['rd : f27', 'imm_val == 496']
Last Code Sequence : 
	-[0x8000043a]:c.fldsp
	-[0x8000043c]:c.nop
	-[0x8000043e]:c.nop
	-[0x80000440]:csrrs
	-[0x80000444]:fsd
Current Store : [0x80000448] : None -- Store: [0x80002260]:0x0000000000000000




Last Coverpoint : ['rd : f26', 'imm_val == 488']
Last Code Sequence : 
	-[0x8000045c]:c.fldsp
	-[0x8000045e]:c.nop
	-[0x80000460]:c.nop
	-[0x80000462]:csrrs
	-[0x80000466]:fsd
Current Store : [0x8000046a] : None -- Store: [0x80002270]:0x0000000000000000




Last Coverpoint : ['rd : f25', 'imm_val == 472']
Last Code Sequence : 
	-[0x8000047e]:c.fldsp
	-[0x80000480]:c.nop
	-[0x80000482]:c.nop
	-[0x80000484]:csrrs
	-[0x80000488]:fsd
Current Store : [0x8000048c] : None -- Store: [0x80002280]:0x0000000000000000




Last Coverpoint : ['rd : f24', 'imm_val == 440']
Last Code Sequence : 
	-[0x800004a0]:c.fldsp
	-[0x800004a2]:c.nop
	-[0x800004a4]:c.nop
	-[0x800004a6]:csrrs
	-[0x800004aa]:fsd
Current Store : [0x800004ae] : None -- Store: [0x80002290]:0x0000000000000000




Last Coverpoint : ['rd : f23', 'imm_val == 376']
Last Code Sequence : 
	-[0x800004c2]:c.fldsp
	-[0x800004c4]:c.nop
	-[0x800004c6]:c.nop
	-[0x800004c8]:csrrs
	-[0x800004cc]:fsd
Current Store : [0x800004d0] : None -- Store: [0x800022a0]:0x0000000000000000




Last Coverpoint : ['rd : f22', 'imm_val == 248']
Last Code Sequence : 
	-[0x800004e4]:c.fldsp
	-[0x800004e6]:c.nop
	-[0x800004e8]:c.nop
	-[0x800004ea]:csrrs
	-[0x800004ee]:fsd
Current Store : [0x800004f2] : None -- Store: [0x800022b0]:0x0000000000000000




Last Coverpoint : ['rd : f21', 'imm_val == 8']
Last Code Sequence : 
	-[0x80000506]:c.fldsp
	-[0x80000508]:c.nop
	-[0x8000050a]:c.nop
	-[0x8000050c]:csrrs
	-[0x80000510]:fsd
Current Store : [0x80000514] : None -- Store: [0x800022c0]:0x0000000000000000




Last Coverpoint : ['rd : f20', 'imm_val == 16']
Last Code Sequence : 
	-[0x80000528]:c.fldsp
	-[0x8000052a]:c.nop
	-[0x8000052c]:c.nop
	-[0x8000052e]:csrrs
	-[0x80000532]:fsd
Current Store : [0x80000536] : None -- Store: [0x800022d0]:0x0000000000000000




Last Coverpoint : ['rd : f19', 'imm_val == 32']
Last Code Sequence : 
	-[0x8000054a]:c.fldsp
	-[0x8000054c]:c.nop
	-[0x8000054e]:c.nop
	-[0x80000550]:csrrs
	-[0x80000554]:fsd
Current Store : [0x80000558] : None -- Store: [0x800022e0]:0x0000000000000000




Last Coverpoint : ['rd : f18', 'imm_val == 64']
Last Code Sequence : 
	-[0x8000056c]:c.fldsp
	-[0x8000056e]:c.nop
	-[0x80000570]:c.nop
	-[0x80000572]:csrrs
	-[0x80000576]:fsd
Current Store : [0x8000057a] : None -- Store: [0x800022f0]:0x0000000000000000




Last Coverpoint : ['rd : f17', 'imm_val == 128']
Last Code Sequence : 
	-[0x8000058e]:c.fldsp
	-[0x80000590]:c.nop
	-[0x80000592]:c.nop
	-[0x80000594]:csrrs
	-[0x80000598]:fsd
Current Store : [0x8000059c] : None -- Store: [0x80002300]:0x0000000000000000




Last Coverpoint : ['rd : f16', 'imm_val == 256']
Last Code Sequence : 
	-[0x800005b0]:c.fldsp
	-[0x800005b2]:c.nop
	-[0x800005b4]:c.nop
	-[0x800005b6]:csrrs
	-[0x800005ba]:fsd
Current Store : [0x800005be] : None -- Store: [0x80002310]:0x0000000000000000




Last Coverpoint : ['rd : f15']
Last Code Sequence : 
	-[0x800005d2]:c.fldsp
	-[0x800005d4]:c.nop
	-[0x800005d6]:c.nop
	-[0x800005d8]:csrrs
	-[0x800005dc]:fsd
Current Store : [0x800005e0] : None -- Store: [0x80002320]:0x0000000000000000




Last Coverpoint : ['rd : f14']
Last Code Sequence : 
	-[0x800005f4]:c.fldsp
	-[0x800005f6]:c.nop
	-[0x800005f8]:c.nop
	-[0x800005fa]:csrrs
	-[0x800005fe]:fsd
Current Store : [0x80000602] : None -- Store: [0x80002330]:0x0000000000000000




Last Coverpoint : ['rd : f13']
Last Code Sequence : 
	-[0x80000616]:c.fldsp
	-[0x80000618]:c.nop
	-[0x8000061a]:c.nop
	-[0x8000061c]:csrrs
	-[0x80000620]:fsd
Current Store : [0x80000624] : None -- Store: [0x80002340]:0x0000000000000000




Last Coverpoint : ['rd : f12']
Last Code Sequence : 
	-[0x80000638]:c.fldsp
	-[0x8000063a]:c.nop
	-[0x8000063c]:c.nop
	-[0x8000063e]:csrrs
	-[0x80000642]:fsd
Current Store : [0x80000646] : None -- Store: [0x80002350]:0x0000000000000000




Last Coverpoint : ['rd : f11']
Last Code Sequence : 
	-[0x8000065a]:c.fldsp
	-[0x8000065c]:c.nop
	-[0x8000065e]:c.nop
	-[0x80000660]:csrrs
	-[0x80000664]:fsd
Current Store : [0x80000668] : None -- Store: [0x80002360]:0x0000000000000000




Last Coverpoint : ['rd : f10']
Last Code Sequence : 
	-[0x8000067c]:c.fldsp
	-[0x8000067e]:c.nop
	-[0x80000680]:c.nop
	-[0x80000682]:csrrs
	-[0x80000686]:fsd
Current Store : [0x8000068a] : None -- Store: [0x80002370]:0x0000000000000000




Last Coverpoint : ['rd : f9']
Last Code Sequence : 
	-[0x8000069e]:c.fldsp
	-[0x800006a0]:c.nop
	-[0x800006a2]:c.nop
	-[0x800006a4]:csrrs
	-[0x800006a8]:fsd
Current Store : [0x800006ac] : None -- Store: [0x80002380]:0x0000000000000000




Last Coverpoint : ['rd : f8']
Last Code Sequence : 
	-[0x800006c0]:c.fldsp
	-[0x800006c2]:c.nop
	-[0x800006c4]:c.nop
	-[0x800006c6]:csrrs
	-[0x800006ca]:fsd
Current Store : [0x800006ce] : None -- Store: [0x80002390]:0x0000000000000000




Last Coverpoint : ['rd : f7']
Last Code Sequence : 
	-[0x800006e2]:c.fldsp
	-[0x800006e4]:c.nop
	-[0x800006e6]:c.nop
	-[0x800006e8]:csrrs
	-[0x800006ec]:fsd
Current Store : [0x800006f0] : None -- Store: [0x800023a0]:0x0000000000000000




Last Coverpoint : ['rd : f6']
Last Code Sequence : 
	-[0x80000704]:c.fldsp
	-[0x80000706]:c.nop
	-[0x80000708]:c.nop
	-[0x8000070a]:csrrs
	-[0x8000070e]:fsd
Current Store : [0x80000712] : None -- Store: [0x800023b0]:0x0000000000000000




Last Coverpoint : ['rd : f5']
Last Code Sequence : 
	-[0x80000726]:c.fldsp
	-[0x80000728]:c.nop
	-[0x8000072a]:c.nop
	-[0x8000072c]:csrrs
	-[0x80000730]:fsd
Current Store : [0x80000734] : None -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rd : f4']
Last Code Sequence : 
	-[0x80000748]:c.fldsp
	-[0x8000074a]:c.nop
	-[0x8000074c]:c.nop
	-[0x8000074e]:csrrs
	-[0x80000752]:fsd
Current Store : [0x80000756] : None -- Store: [0x800023d0]:0x0000000000000000




Last Coverpoint : ['rd : f3']
Last Code Sequence : 
	-[0x8000076a]:c.fldsp
	-[0x8000076c]:c.nop
	-[0x8000076e]:c.nop
	-[0x80000770]:csrrs
	-[0x80000774]:fsd
Current Store : [0x80000778] : None -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rd : f2']
Last Code Sequence : 
	-[0x8000078c]:c.fldsp
	-[0x8000078e]:c.nop
	-[0x80000790]:c.nop
	-[0x80000792]:csrrs
	-[0x80000796]:fsd
Current Store : [0x8000079a] : None -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rd : f1']
Last Code Sequence : 
	-[0x800007ae]:c.fldsp
	-[0x800007b0]:c.nop
	-[0x800007b2]:c.nop
	-[0x800007b4]:csrrs
	-[0x800007b8]:fsd
Current Store : [0x800007bc] : None -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rd : f0']
Last Code Sequence : 
	-[0x800007d0]:c.fldsp
	-[0x800007d2]:c.nop
	-[0x800007d4]:c.nop
	-[0x800007d6]:csrrs
	-[0x800007da]:fsd
Current Store : [0x800007de] : None -- Store: [0x80002410]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                        coverpoints                        |                                                       code                                                        |
|---:|---------------------------------------|-----------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0xfbb6fab7fbb6fab7<br> |- mnemonic : c.fldsp<br> - rd : f31<br> - imm_val == 0<br> |[0x800003b2]:c.fldsp<br> [0x800003b4]:c.nop<br> [0x800003b6]:c.nop<br> [0x800003b8]:csrrs<br> [0x800003bc]:fsd<br> |
|   2|[0x80002228]<br>0xf76df56ff76df56f<br> |- rd : f30<br> - imm_val > 0<br>                           |[0x800003d4]:c.fldsp<br> [0x800003d6]:c.nop<br> [0x800003d8]:c.nop<br> [0x800003da]:csrrs<br> [0x800003de]:fsd<br> |
|   3|[0x80002238]<br>0xeedbeadfeedbeadf<br> |- rd : f29<br> - imm_val == 168<br>                        |[0x800003f6]:c.fldsp<br> [0x800003f8]:c.nop<br> [0x800003fa]:c.nop<br> [0x800003fc]:csrrs<br> [0x80000400]:fsd<br> |
|   4|[0x80002248]<br>0xddb7d5bfddb7d5bf<br> |- rd : f28<br> - imm_val == 336<br>                        |[0x80000418]:c.fldsp<br> [0x8000041a]:c.nop<br> [0x8000041c]:c.nop<br> [0x8000041e]:csrrs<br> [0x80000422]:fsd<br> |
|   5|[0x80002258]<br>0xbb6fab7fbb6fab7f<br> |- rd : f27<br> - imm_val == 496<br>                        |[0x8000043a]:c.fldsp<br> [0x8000043c]:c.nop<br> [0x8000043e]:c.nop<br> [0x80000440]:csrrs<br> [0x80000444]:fsd<br> |
|   6|[0x80002268]<br>0x76df56ff76df56ff<br> |- rd : f26<br> - imm_val == 488<br>                        |[0x8000045c]:c.fldsp<br> [0x8000045e]:c.nop<br> [0x80000460]:c.nop<br> [0x80000462]:csrrs<br> [0x80000466]:fsd<br> |
|   7|[0x80002278]<br>0xedbeadfeedbeadfe<br> |- rd : f25<br> - imm_val == 472<br>                        |[0x8000047e]:c.fldsp<br> [0x80000480]:c.nop<br> [0x80000482]:c.nop<br> [0x80000484]:csrrs<br> [0x80000488]:fsd<br> |
|   8|[0x80002288]<br>0xdb7d5bfddb7d5bfd<br> |- rd : f24<br> - imm_val == 440<br>                        |[0x800004a0]:c.fldsp<br> [0x800004a2]:c.nop<br> [0x800004a4]:c.nop<br> [0x800004a6]:csrrs<br> [0x800004aa]:fsd<br> |
|   9|[0x80002298]<br>0xb6fab7fbb6fab7fb<br> |- rd : f23<br> - imm_val == 376<br>                        |[0x800004c2]:c.fldsp<br> [0x800004c4]:c.nop<br> [0x800004c6]:c.nop<br> [0x800004c8]:csrrs<br> [0x800004cc]:fsd<br> |
|  10|[0x800022a8]<br>0x6df56ff76df56ff7<br> |- rd : f22<br> - imm_val == 248<br>                        |[0x800004e4]:c.fldsp<br> [0x800004e6]:c.nop<br> [0x800004e8]:c.nop<br> [0x800004ea]:csrrs<br> [0x800004ee]:fsd<br> |
|  11|[0x800022b8]<br>0xdbeadfeedbeadfee<br> |- rd : f21<br> - imm_val == 8<br>                          |[0x80000506]:c.fldsp<br> [0x80000508]:c.nop<br> [0x8000050a]:c.nop<br> [0x8000050c]:csrrs<br> [0x80000510]:fsd<br> |
|  12|[0x800022c8]<br>0xb7d5bfddb7d5bfdd<br> |- rd : f20<br> - imm_val == 16<br>                         |[0x80000528]:c.fldsp<br> [0x8000052a]:c.nop<br> [0x8000052c]:c.nop<br> [0x8000052e]:csrrs<br> [0x80000532]:fsd<br> |
|  13|[0x800022d8]<br>0x6fab7fbb6fab7fbb<br> |- rd : f19<br> - imm_val == 32<br>                         |[0x8000054a]:c.fldsp<br> [0x8000054c]:c.nop<br> [0x8000054e]:c.nop<br> [0x80000550]:csrrs<br> [0x80000554]:fsd<br> |
|  14|[0x800022e8]<br>0xdf56ff76df56ff76<br> |- rd : f18<br> - imm_val == 64<br>                         |[0x8000056c]:c.fldsp<br> [0x8000056e]:c.nop<br> [0x80000570]:c.nop<br> [0x80000572]:csrrs<br> [0x80000576]:fsd<br> |
|  15|[0x800022f8]<br>0xbeadfeedbeadfeed<br> |- rd : f17<br> - imm_val == 128<br>                        |[0x8000058e]:c.fldsp<br> [0x80000590]:c.nop<br> [0x80000592]:c.nop<br> [0x80000594]:csrrs<br> [0x80000598]:fsd<br> |
|  16|[0x80002308]<br>0x7d5bfddb7d5bfddb<br> |- rd : f16<br> - imm_val == 256<br>                        |[0x800005b0]:c.fldsp<br> [0x800005b2]:c.nop<br> [0x800005b4]:c.nop<br> [0x800005b6]:csrrs<br> [0x800005ba]:fsd<br> |
|  17|[0x80002318]<br>0xfab7fbb6fab7fbb6<br> |- rd : f15<br>                                             |[0x800005d2]:c.fldsp<br> [0x800005d4]:c.nop<br> [0x800005d6]:c.nop<br> [0x800005d8]:csrrs<br> [0x800005dc]:fsd<br> |
|  18|[0x80002328]<br>0xf56ff76df56ff76d<br> |- rd : f14<br>                                             |[0x800005f4]:c.fldsp<br> [0x800005f6]:c.nop<br> [0x800005f8]:c.nop<br> [0x800005fa]:csrrs<br> [0x800005fe]:fsd<br> |
|  19|[0x80002338]<br>0xeadfeedbeadfeedb<br> |- rd : f13<br>                                             |[0x80000616]:c.fldsp<br> [0x80000618]:c.nop<br> [0x8000061a]:c.nop<br> [0x8000061c]:csrrs<br> [0x80000620]:fsd<br> |
|  20|[0x80002348]<br>0xd5bfddb7d5bfddb7<br> |- rd : f12<br>                                             |[0x80000638]:c.fldsp<br> [0x8000063a]:c.nop<br> [0x8000063c]:c.nop<br> [0x8000063e]:csrrs<br> [0x80000642]:fsd<br> |
|  21|[0x80002358]<br>0xab7fbb6fab7fbb6f<br> |- rd : f11<br>                                             |[0x8000065a]:c.fldsp<br> [0x8000065c]:c.nop<br> [0x8000065e]:c.nop<br> [0x80000660]:csrrs<br> [0x80000664]:fsd<br> |
|  22|[0x80002368]<br>0x56ff76df56ff76df<br> |- rd : f10<br>                                             |[0x8000067c]:c.fldsp<br> [0x8000067e]:c.nop<br> [0x80000680]:c.nop<br> [0x80000682]:csrrs<br> [0x80000686]:fsd<br> |
|  23|[0x80002378]<br>0xadfeedbeadfeedbe<br> |- rd : f9<br>                                              |[0x8000069e]:c.fldsp<br> [0x800006a0]:c.nop<br> [0x800006a2]:c.nop<br> [0x800006a4]:csrrs<br> [0x800006a8]:fsd<br> |
|  24|[0x80002388]<br>0x5bfddb7d5bfddb7d<br> |- rd : f8<br>                                              |[0x800006c0]:c.fldsp<br> [0x800006c2]:c.nop<br> [0x800006c4]:c.nop<br> [0x800006c6]:csrrs<br> [0x800006ca]:fsd<br> |
|  25|[0x80002398]<br>0xb7fbb6fab7fbb6fa<br> |- rd : f7<br>                                              |[0x800006e2]:c.fldsp<br> [0x800006e4]:c.nop<br> [0x800006e6]:c.nop<br> [0x800006e8]:csrrs<br> [0x800006ec]:fsd<br> |
|  26|[0x800023a8]<br>0x0000000080002000<br> |- rd : f6<br>                                              |[0x80000704]:c.fldsp<br> [0x80000706]:c.nop<br> [0x80000708]:c.nop<br> [0x8000070a]:csrrs<br> [0x8000070e]:fsd<br> |
|  27|[0x800023b8]<br>0x0000000080000390<br> |- rd : f5<br>                                              |[0x80000726]:c.fldsp<br> [0x80000728]:c.nop<br> [0x8000072a]:c.nop<br> [0x8000072c]:csrrs<br> [0x80000730]:fsd<br> |
|  28|[0x800023c8]<br>0x0000000000000000<br> |- rd : f4<br>                                              |[0x80000748]:c.fldsp<br> [0x8000074a]:c.nop<br> [0x8000074c]:c.nop<br> [0x8000074e]:csrrs<br> [0x80000752]:fsd<br> |
|  29|[0x800023d8]<br>0x0000000000000000<br> |- rd : f3<br>                                              |[0x8000076a]:c.fldsp<br> [0x8000076c]:c.nop<br> [0x8000076e]:c.nop<br> [0x80000770]:csrrs<br> [0x80000774]:fsd<br> |
|  30|[0x800023e8]<br>0x0000000080002000<br> |- rd : f2<br>                                              |[0x8000078c]:c.fldsp<br> [0x8000078e]:c.nop<br> [0x80000790]:c.nop<br> [0x80000792]:csrrs<br> [0x80000796]:fsd<br> |
|  31|[0x800023f8]<br>0x0000000080002218<br> |- rd : f1<br>                                              |[0x800007ae]:c.fldsp<br> [0x800007b0]:c.nop<br> [0x800007b2]:c.nop<br> [0x800007b4]:csrrs<br> [0x800007b8]:fsd<br> |
|  32|[0x80002408]<br>0x0000000000000000<br> |- rd : f0<br>                                              |[0x800007d0]:c.fldsp<br> [0x800007d2]:c.nop<br> [0x800007d4]:c.nop<br> [0x800007d6]:csrrs<br> [0x800007da]:fsd<br> |
