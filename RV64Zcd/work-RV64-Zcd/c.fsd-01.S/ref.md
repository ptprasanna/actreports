
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800005c0')]      |
| SIG_REGION                | [('0x80002210', '0x80002300', '30 dwords')]      |
| COV_LABELS                | c.fsd      |
| TEST_NAME                 | /home/riscv/riscv-ctg/work-RV64-Zcd/c.fsd-01.S/ref.S    |
| Total Number of coverpoints| 32     |
| Total Coverpoints Hit     | 32      |
| Total Signature Updates   | 28      |
| STAT1                     | 14      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 14     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : c.fsd', 'rs1 : x15', 'rs2 : f15', 'rs1 != rs2', 'imm_val == 0']
Last Code Sequence : 
	-[0x800003b8]:c.fsd
Current Store : [0x800003c2] : None -- Store: [0x80002220]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rs2 : f14', 'imm_val > 0']
Last Code Sequence : 
	-[0x800003de]:c.fsd
Current Store : [0x800003e8] : None -- Store: [0x80002230]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rs2 : f13', 'imm_val == 168']
Last Code Sequence : 
	-[0x80000404]:c.fsd
Current Store : [0x8000040e] : None -- Store: [0x80002240]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rs2 : f12', 'imm_val == 80']
Last Code Sequence : 
	-[0x8000042a]:c.fsd
Current Store : [0x80000434] : None -- Store: [0x80002250]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rs2 : f11', 'imm_val == 8']
Last Code Sequence : 
	-[0x80000450]:c.fsd
Current Store : [0x8000045a] : None -- Store: [0x80002260]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rs2 : f10', 'imm_val == 16']
Last Code Sequence : 
	-[0x80000476]:c.fsd
Current Store : [0x80000480] : None -- Store: [0x80002270]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rs2 : f9', 'imm_val == 240']
Last Code Sequence : 
	-[0x8000049c]:c.fsd
Current Store : [0x800004a6] : None -- Store: [0x80002280]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rs2 : f8', 'imm_val == 232']
Last Code Sequence : 
	-[0x800004c2]:c.fsd
Current Store : [0x800004cc] : None -- Store: [0x80002290]:0x0000000000000000




Last Coverpoint : ['imm_val == 216']
Last Code Sequence : 
	-[0x800004e8]:c.fsd
Current Store : [0x800004f2] : None -- Store: [0x800022a0]:0x0000000000000000




Last Coverpoint : ['imm_val == 184']
Last Code Sequence : 
	-[0x8000050e]:c.fsd
Current Store : [0x80000518] : None -- Store: [0x800022b0]:0x0000000000000000




Last Coverpoint : ['imm_val == 120']
Last Code Sequence : 
	-[0x80000534]:c.fsd
Current Store : [0x8000053e] : None -- Store: [0x800022c0]:0x0000000000000000




Last Coverpoint : ['imm_val == 32']
Last Code Sequence : 
	-[0x8000055a]:c.fsd
Current Store : [0x80000564] : None -- Store: [0x800022d0]:0x0000000000000000




Last Coverpoint : ['imm_val == 64']
Last Code Sequence : 
	-[0x80000580]:c.fsd
Current Store : [0x8000058a] : None -- Store: [0x800022e0]:0x0000000000000000




Last Coverpoint : ['imm_val == 128']
Last Code Sequence : 
	-[0x800005a6]:c.fsd
Current Store : [0x800005b0] : None -- Store: [0x800022f0]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                        coverpoints                                        |         code          |
|---:|---------------------------------------|-------------------------------------------------------------------------------------------|-----------------------|
|   1|[0x80002218]<br>0x0000000080002218<br> |- mnemonic : c.fsd<br> - rs1 : x15<br> - rs2 : f15<br> - rs1 != rs2<br> - imm_val == 0<br> |[0x800003b8]:c.fsd<br> |
|   2|[0x80002228]<br>0x0000000080002130<br> |- rs1 : x14<br> - rs2 : f14<br> - imm_val > 0<br>                                          |[0x800003de]:c.fsd<br> |
|   3|[0x80002238]<br>0x0000000080002190<br> |- rs1 : x13<br> - rs2 : f13<br> - imm_val == 168<br>                                       |[0x80000404]:c.fsd<br> |
|   4|[0x80002248]<br>0x00000000800021f8<br> |- rs1 : x12<br> - rs2 : f12<br> - imm_val == 80<br>                                        |[0x8000042a]:c.fsd<br> |
|   5|[0x80002258]<br>0x0000000080002250<br> |- rs1 : x11<br> - rs2 : f11<br> - imm_val == 8<br>                                         |[0x80000450]:c.fsd<br> |
|   6|[0x80002268]<br>0x0000000080002258<br> |- rs1 : x10<br> - rs2 : f10<br> - imm_val == 16<br>                                        |[0x80000476]:c.fsd<br> |
|   7|[0x80002278]<br>0x0000000080002188<br> |- rs1 : x9<br> - rs2 : f9<br> - imm_val == 240<br>                                         |[0x8000049c]:c.fsd<br> |
|   8|[0x80002288]<br>0x00000000800021a0<br> |- rs1 : x8<br> - rs2 : f8<br> - imm_val == 232<br>                                         |[0x800004c2]:c.fsd<br> |
|   9|[0x80002298]<br>0x00000000800021c0<br> |- imm_val == 216<br>                                                                       |[0x800004e8]:c.fsd<br> |
|  10|[0x800022a8]<br>0x00000000800021f0<br> |- imm_val == 184<br>                                                                       |[0x8000050e]:c.fsd<br> |
|  11|[0x800022b8]<br>0x0000000080002240<br> |- imm_val == 120<br>                                                                       |[0x80000534]:c.fsd<br> |
|  12|[0x800022c8]<br>0x00000000800022a8<br> |- imm_val == 32<br>                                                                        |[0x8000055a]:c.fsd<br> |
|  13|[0x800022d8]<br>0x0000000080002298<br> |- imm_val == 64<br>                                                                        |[0x80000580]:c.fsd<br> |
|  14|[0x800022e8]<br>0x0000000080002268<br> |- imm_val == 128<br>                                                                       |[0x800005a6]:c.fsd<br> |
