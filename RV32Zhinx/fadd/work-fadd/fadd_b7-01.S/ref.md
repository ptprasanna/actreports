
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800008b0')]      |
| SIG_REGION                | [('0x80002310', '0x80002500', '124 words')]      |
| COV_LABELS                | fadd_b7      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-fadd/fadd_b7-01.S/ref.S    |
| Total Number of coverpoints| 157     |
| Total Coverpoints Hit     | 157      |
| Total Signature Updates   | 120      |
| STAT1                     | 59      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 60     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000894]:fadd.h t6, t5, t4, dyn
      [0x80000898]:csrrs a2, fcsr, zero
      [0x8000089c]:sw t6, 272(fp)
 -- Signature Addresses:
      Address: 0x800024ec Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1c and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1c and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.h', 'rs1 : x31', 'rs2 : x30', 'rd : x31', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c0 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x2c0 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fadd.h t6, t6, t5, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002318]:0x00000060




Last Coverpoint : ['rs1 : x29', 'rs2 : x31', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2b9 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x2b9 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fadd.h t5, t4, t6, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80002320]:0x00000060




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x80000164]:fadd.h t3, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t3, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002328]:0x00000065




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000184]:fadd.h t4, s11, s11, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t4, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80002330]:0x00000065




Last Coverpoint : ['rs1 : x30', 'rs2 : x26', 'rd : x26', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x39f and fs2 == 1 and fe2 == 0x1d and fm2 == 0x39f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001a4]:fadd.h s10, t5, s10, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s10, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002338]:0x00000060




Last Coverpoint : ['rs1 : x26', 'rs2 : x29', 'rd : x27', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x342 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fadd.h s11, s10, t4, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s11, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80002340]:0x00000060




Last Coverpoint : ['rs1 : x24', 'rs2 : x23', 'rd : x25', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x081 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x081 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fadd.h s9, s8, s7, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002348]:0x00000060




Last Coverpoint : ['rs1 : x23', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0f1 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0f1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fadd.h s8, s7, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80002350]:0x00000060




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x346 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x346 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fadd.h s7, s9, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002358]:0x00000060




Last Coverpoint : ['rs1 : x21', 'rs2 : x20', 'rd : x22', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x27a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x27a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fadd.h s6, s5, s4, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80002360]:0x00000060




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f5 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x0f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fadd.h s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002368]:0x00000060




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x20', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x32f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x32f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fadd.h s4, s6, s5, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80002370]:0x00000060




Last Coverpoint : ['rs1 : x18', 'rs2 : x17', 'rd : x19', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x38c and fs2 == 1 and fe2 == 0x1d and fm2 == 0x38c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fadd.h s3, s2, a7, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002378]:0x00000060




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f1 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x2f1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fadd.h s2, a7, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80002380]:0x00000060




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x34c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x34c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fadd.h a7, s3, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002388]:0x00000060




Last Coverpoint : ['rs1 : x15', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x3a0 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fadd.h a6, a5, a4, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80002390]:0x00000060




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x02a and fs2 == 1 and fe2 == 0x1d and fm2 == 0x02a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fadd.h a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002398]:0x00000060




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x14', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x063 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x063 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fadd.h a4, a6, a5, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800023a0]:0x00000060




Last Coverpoint : ['rs1 : x12', 'rs2 : x11', 'rd : x13', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c1 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1c1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fadd.h a3, a2, a1, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800023a8]:0x00000060




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x298 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x298 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fadd.h a2, a1, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800023b0]:0x00000060




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x15 and fm1 == 0x0bd and fs2 == 1 and fe2 == 0x15 and fm2 == 0x0bd and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fadd.h a1, a3, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800023b8]:0x00000060




Last Coverpoint : ['rs1 : x9', 'rs2 : x8', 'rd : x10', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x1e and fm2 == 0x2ef and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:fadd.h a0, s1, fp, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800023c0]:0x00000060




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x133 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fadd.h s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800023c8]:0x00000060




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x21c and fs2 == 1 and fe2 == 0x1c and fm2 == 0x21c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fadd.h fp, a0, s1, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800023d0]:0x00000060




Last Coverpoint : ['rs1 : x6', 'rs2 : x5', 'rd : x7', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x3a1 and fs2 == 1 and fe2 == 0x1c and fm2 == 0x3a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fadd.h t2, t1, t0, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800023d8]:0x00000060




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x26c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x26c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:fadd.h t1, t0, t2, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800023e0]:0x00000060




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x125 and fs2 == 1 and fe2 == 0x1b and fm2 == 0x125 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:fadd.h t0, t2, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800023e8]:0x00000060




Last Coverpoint : ['rs1 : x3', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x00f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x00f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:fadd.h tp, gp, sp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800023f0]:0x00000060




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x2fa and fs2 == 1 and fe2 == 0x1b and fm2 == 0x2fa and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fadd.h gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800023f8]:0x00000060




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1a6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fadd.h sp, tp, gp, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x80002400]:0x00000060




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x283 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x283 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004f4]:fadd.h t6, ra, t5, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw t6, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80002408]:0x00000060




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x80000514]:fadd.h t6, zero, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x80002410]:0x00000060




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0b2 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0b2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000534]:fadd.h t6, t5, ra, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80002418]:0x00000060




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000554]:fadd.h t6, t5, zero, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw t6, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x80002420]:0x00000060




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x09a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x09a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:fadd.h ra, t6, t5, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw ra, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80002428]:0x00000060




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1c and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fadd.h zero, t6, t5, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw zero, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x80002430]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0e6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fadd.h t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80002438]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x39e and fs2 == 1 and fe2 == 0x1d and fm2 == 0x39e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:fadd.h t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x80002440]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fadd.h t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80002448]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x362 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x362 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fadd.h t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x80002450]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x32e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x32e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:fadd.h t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80002458]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x052 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x052 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:fadd.h t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x80002460]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:fadd.h t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80002468]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x27d and fs2 == 1 and fe2 == 0x1e and fm2 == 0x27b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:fadd.h t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x80002470]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x328 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x326 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fadd.h t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80002478]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x396 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fadd.h t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x80002480]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x334 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x330 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fadd.h t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80002488]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f7 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:fadd.h t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x80002490]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x257 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x255 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fadd.h t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80002498]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x109 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x109 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:fadd.h t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x800024a0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x3c6 and fs2 == 1 and fe2 == 0x1c and fm2 == 0x3c6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:fadd.h t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800024a8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x37e and fs2 == 1 and fe2 == 0x1b and fm2 == 0x37e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:fadd.h t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800024b0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x25a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:fadd.h t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800024b8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x286 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x286 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fadd.h t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800024c0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0ae and fs2 == 1 and fe2 == 0x1c and fm2 == 0x0ae and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fadd.h t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800024c8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x016 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x016 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:fadd.h t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800024d0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x244 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x244 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:fadd.h t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800024d8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b4 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3b4 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fadd.h t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800024e0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c4 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0c4 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:fadd.h t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800024e8]:0x00000060




Last Coverpoint : ['mnemonic : fadd.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1c and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1c and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:fadd.h t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800024f0]:0x00000060





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                           coverpoints                                                                                                                                           |                                                      code                                                       |
|---:|-------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0x00000000<br> |- mnemonic : fadd.h<br> - rs1 : x31<br> - rs2 : x30<br> - rd : x31<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2c0 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x2c0 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fadd.h t6, t6, t5, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000231c]<br>0x00000000<br> |- rs1 : x29<br> - rs2 : x31<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2b9 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x2b9 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>  |[0x80000144]:fadd.h t5, t4, t6, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 8(ra)<br>      |
|   3|[0x80002324]<br>0x00007C00<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x28<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                            |[0x80000164]:fadd.h t3, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t3, 16(ra)<br>     |
|   4|[0x8000232c]<br>0x00007C00<br> |- rs1 : x27<br> - rs2 : x27<br> - rd : x29<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                            |[0x80000184]:fadd.h t4, s11, s11, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t4, 24(ra)<br>   |
|   5|[0x80002334]<br>0x00000000<br> |- rs1 : x30<br> - rs2 : x26<br> - rd : x26<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x39f and fs2 == 1 and fe2 == 0x1d and fm2 == 0x39f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x800001a4]:fadd.h s10, t5, s10, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s10, 32(ra)<br>  |
|   6|[0x8000233c]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x29<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x342 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fadd.h s11, s10, t4, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s11, 40(ra)<br>  |
|   7|[0x80002344]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x23<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x081 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x081 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001e4]:fadd.h s9, s8, s7, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>     |
|   8|[0x8000234c]<br>0x00000000<br> |- rs1 : x23<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0f1 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0f1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000204]:fadd.h s8, s7, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80002354]<br>0x00000000<br> |- rs1 : x25<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x346 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x346 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000224]:fadd.h s7, s9, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000235c]<br>0x00000000<br> |- rs1 : x21<br> - rs2 : x20<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x27a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x27a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000244]:fadd.h s6, s5, s4, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002364]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f5 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x0f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fadd.h s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000236c]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x21<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x32f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x32f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000284]:fadd.h s4, s6, s5, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002374]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x17<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x38c and fs2 == 1 and fe2 == 0x1d and fm2 == 0x38c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002a4]:fadd.h s3, s2, a7, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000237c]<br>0x00000000<br> |- rs1 : x17<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f1 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x2f1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002c4]:fadd.h s2, a7, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002384]<br>0x00000000<br> |- rs1 : x19<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x34c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x34c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002e4]:fadd.h a7, s3, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000238c]<br>0x00000000<br> |- rs1 : x15<br> - rs2 : x14<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x3a0 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x3a0 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fadd.h a6, a5, a4, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002394]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x02a and fs2 == 1 and fe2 == 0x1d and fm2 == 0x02a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000324]:fadd.h a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000239c]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x15<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x063 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x063 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000344]:fadd.h a4, a6, a5, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800023a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x11<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c1 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1c1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000364]:fadd.h a3, a2, a1, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800023ac]<br>0x00000000<br> |- rs1 : x11<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x298 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x298 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000384]:fadd.h a2, a1, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800023b4]<br>0x00000000<br> |- rs1 : x13<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x15 and fm1 == 0x0bd and fs2 == 1 and fe2 == 0x15 and fm2 == 0x0bd and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fadd.h a1, a3, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800023bc]<br>0x00000000<br> |- rs1 : x9<br> - rs2 : x8<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x1e and fm2 == 0x2ef and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800003c4]:fadd.h a0, s1, fp, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800023c4]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x133 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800003ec]:fadd.h s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800023cc]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x9<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x21c and fs2 == 1 and fe2 == 0x1c and fm2 == 0x21c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x8000040c]:fadd.h fp, a0, s1, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800023d4]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x5<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x3a1 and fs2 == 1 and fe2 == 0x1c and fm2 == 0x3a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x8000042c]:fadd.h t2, t1, t0, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800023dc]<br>0x00000000<br> |- rs1 : x5<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x26c and fs2 == 1 and fe2 == 0x1e and fm2 == 0x26c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000454]:fadd.h t1, t0, t2, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800023e4]<br>0x00000000<br> |- rs1 : x7<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x125 and fs2 == 1 and fe2 == 0x1b and fm2 == 0x125 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000474]:fadd.h t0, t2, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800023ec]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x2<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x00f and fs2 == 1 and fe2 == 0x1e and fm2 == 0x00f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000494]:fadd.h tp, gp, sp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800023f4]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1b and fm1 == 0x2fa and fs2 == 1 and fe2 == 0x1b and fm2 == 0x2fa and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004b4]:fadd.h gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800023fc]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x3<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1a6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004d4]:fadd.h sp, tp, gp, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80002404]<br>0x00000000<br> |- rs1 : x1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x283 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x283 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x800004f4]:fadd.h t6, ra, t5, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw t6, 40(fp)<br>     |
|  32|[0x8000240c]<br>0xFFFFFBB4<br> |- rs1 : x0<br>                                                                                                                                                                                                                                                                                   |[0x80000514]:fadd.h t6, zero, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>   |
|  33|[0x80002414]<br>0x00000000<br> |- rs2 : x1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0b2 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0b2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000534]:fadd.h t6, t5, ra, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>     |
|  34|[0x8000241c]<br>0x000078C4<br> |- rs2 : x0<br>                                                                                                                                                                                                                                                                                   |[0x80000554]:fadd.h t6, t5, zero, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw t6, 64(fp)<br>   |
|  35|[0x80002424]<br>0x00000000<br> |- rd : x1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x09a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x09a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                 |[0x80000574]:fadd.h ra, t6, t5, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw ra, 72(fp)<br>     |
|  36|[0x8000242c]<br>0x00000000<br> |- rd : x0<br> - fs1 == 0 and fe1 == 0x1c and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1c and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                 |[0x80000594]:fadd.h zero, t6, t5, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw zero, 80(fp)<br> |
|  37|[0x80002434]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e6 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0e6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005b4]:fadd.h t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x8000243c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x39e and fs2 == 1 and fe2 == 0x1d and fm2 == 0x39e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005d4]:fadd.h t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80002444]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x20e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x20e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005f4]:fadd.h t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x8000244c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x362 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x362 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000614]:fadd.h t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80002454]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x32e and fs2 == 1 and fe2 == 0x1e and fm2 == 0x32e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000634]:fadd.h t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x8000245c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x052 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x052 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000654]:fadd.h t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80002464]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x1a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000674]:fadd.h t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x8000246c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x27d and fs2 == 1 and fe2 == 0x1e and fm2 == 0x27b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000694]:fadd.h t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80002474]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x328 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x326 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006b4]:fadd.h t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x8000247c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x396 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006d4]:fadd.h t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80002484]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x334 and fs2 == 1 and fe2 == 0x1d and fm2 == 0x330 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006f4]:fadd.h t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x8000248c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f7 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000714]:fadd.h t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80002494]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x257 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x255 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000734]:fadd.h t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x8000249c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x109 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x109 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000754]:fadd.h t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800024a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x3c6 and fs2 == 1 and fe2 == 0x1c and fm2 == 0x3c6 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000774]:fadd.h t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800024ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x37e and fs2 == 1 and fe2 == 0x1b and fm2 == 0x37e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000794]:fadd.h t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800024b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25a and fs2 == 1 and fe2 == 0x1e and fm2 == 0x25a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007b4]:fadd.h t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800024bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x286 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x286 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007d4]:fadd.h t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800024c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0ae and fs2 == 1 and fe2 == 0x1c and fm2 == 0x0ae and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007f4]:fadd.h t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800024cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x016 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x016 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000814]:fadd.h t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800024d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x244 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x244 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000834]:fadd.h t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800024dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b4 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3b4 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000854]:fadd.h t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800024e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c4 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x0c4 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000874]:fadd.h t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
