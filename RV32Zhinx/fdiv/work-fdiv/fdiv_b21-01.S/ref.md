
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80008440')]      |
| SIG_REGION                | [('0x8000b710', '0x8000cc60', '1364 words')]      |
| COV_LABELS                | fdiv_b21      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-fdiv/fdiv_b21-01.S/ref.S    |
| Total Number of coverpoints| 778     |
| Total Coverpoints Hit     | 778      |
| Total Signature Updates   | 1362      |
| STAT1                     | 679      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 681     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80008344]:fdiv.h t6, t5, t4, dyn
      [0x80008348]:csrrs a2, fcsr, zero
      [0x8000834c]:sw t6, 88(fp)
 -- Signature Addresses:
      Address: 0x8000cc34 Data: 0x00007E00
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80008424]:fdiv.h t6, t5, t4, dyn
      [0x80008428]:csrrs a2, fcsr, zero
      [0x8000842c]:sw t6, 120(fp)
 -- Signature Addresses:
      Address: 0x8000cc54 Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2 == rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fdiv.h t6, t6, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x8000b718]:0x00000010




Last Coverpoint : ['rs1 : x29', 'rs2 : x28', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fdiv.h t5, t4, t3, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x8000b720]:0x00000010




Last Coverpoint : ['rs1 : x30', 'rs2 : x30', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fdiv.h t4, t5, t5, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t4, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x8000b728]:0x00000010




Last Coverpoint : ['rs1 : x28', 'rs2 : x29', 'rd : x28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fdiv.h t3, t3, t4, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x8000b730]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001a4]:fdiv.h s11, s10, s11, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x8000b738]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fdiv.h s10, s11, s9, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x8000b740]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fdiv.h s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x8000b748]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fdiv.h s8, s9, s7, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x8000b750]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fdiv.h s7, s6, s8, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x8000b758]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fdiv.h s6, s7, s5, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x8000b760]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fdiv.h s5, s4, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x8000b768]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fdiv.h s4, s5, s3, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x8000b770]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fdiv.h s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x8000b778]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fdiv.h s2, s3, a7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x8000b780]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fdiv.h a7, a6, s2, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x8000b788]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fdiv.h a6, a7, a5, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x8000b790]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fdiv.h a5, a4, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x8000b798]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fdiv.h a4, a5, a3, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x8000b7a0]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fdiv.h a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x8000b7a8]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fdiv.h a2, a3, a1, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x8000b7b0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fdiv.h a1, a0, a2, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x8000b7b8]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:fdiv.h a0, a1, s1, dyn
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x8000b7c0]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fdiv.h s1, fp, a0, dyn
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x8000b7c8]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fdiv.h fp, s1, t2, dyn
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x8000b7d0]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fdiv.h t2, t1, fp, dyn
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x8000b7d8]:0x00000010




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:fdiv.h t1, t2, t0, dyn
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x8000b7e0]:0x00000010




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:fdiv.h t0, tp, t1, dyn
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x8000b7e8]:0x00000010




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:fdiv.h tp, t0, gp, dyn
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x8000b7f0]:0x00000010




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fdiv.h gp, sp, tp, dyn
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x8000b7f8]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fdiv.h sp, gp, ra, dyn
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x8000b800]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fdiv.h ra, zero, sp, dyn
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x8000b808]:0x00000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000514]:fdiv.h t6, ra, t5, dyn
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x8000b810]:0x00000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000534]:fdiv.h t6, t5, zero, dyn
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x8000b818]:0x00000010




Last Coverpoint : ['rd : x0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:fdiv.h zero, t6, t5, dyn
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x8000b820]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:fdiv.h t6, t5, t4, dyn
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x8000b828]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fdiv.h t6, t5, t4, dyn
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x8000b830]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fdiv.h t6, t5, t4, dyn
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x8000b838]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:fdiv.h t6, t5, t4, dyn
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x8000b840]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fdiv.h t6, t5, t4, dyn
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x8000b848]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fdiv.h t6, t5, t4, dyn
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x8000b850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:fdiv.h t6, t5, t4, dyn
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x8000b858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:fdiv.h t6, t5, t4, dyn
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x8000b860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:fdiv.h t6, t5, t4, dyn
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x8000b868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:fdiv.h t6, t5, t4, dyn
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x8000b870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fdiv.h t6, t5, t4, dyn
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x8000b878]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fdiv.h t6, t5, t4, dyn
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x8000b880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fdiv.h t6, t5, t4, dyn
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x8000b888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:fdiv.h t6, t5, t4, dyn
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x8000b890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fdiv.h t6, t5, t4, dyn
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x8000b898]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:fdiv.h t6, t5, t4, dyn
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x8000b8a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:fdiv.h t6, t5, t4, dyn
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x8000b8a8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:fdiv.h t6, t5, t4, dyn
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x8000b8b0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:fdiv.h t6, t5, t4, dyn
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x8000b8b8]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fdiv.h t6, t5, t4, dyn
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x8000b8c0]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fdiv.h t6, t5, t4, dyn
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x8000b8c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:fdiv.h t6, t5, t4, dyn
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x8000b8d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:fdiv.h t6, t5, t4, dyn
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x8000b8d8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fdiv.h t6, t5, t4, dyn
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x8000b8e0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:fdiv.h t6, t5, t4, dyn
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x8000b8e8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:fdiv.h t6, t5, t4, dyn
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x8000b8f0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fdiv.h t6, t5, t4, dyn
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x8000b8f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d4]:fdiv.h t6, t5, t4, dyn
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x8000b900]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fdiv.h t6, t5, t4, dyn
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x8000b908]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000914]:fdiv.h t6, t5, t4, dyn
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x8000b910]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000934]:fdiv.h t6, t5, t4, dyn
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x8000b918]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000954]:fdiv.h t6, t5, t4, dyn
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x8000b920]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fdiv.h t6, t5, t4, dyn
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x8000b928]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fdiv.h t6, t5, t4, dyn
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x8000b930]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fdiv.h t6, t5, t4, dyn
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x8000b938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fdiv.h t6, t5, t4, dyn
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x8000b940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009f4]:fdiv.h t6, t5, t4, dyn
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x8000b948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fdiv.h t6, t5, t4, dyn
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x8000b950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fdiv.h t6, t5, t4, dyn
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x8000b958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a54]:fdiv.h t6, t5, t4, dyn
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x8000b960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fdiv.h t6, t5, t4, dyn
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x8000b968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fdiv.h t6, t5, t4, dyn
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x8000b970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x8000b978]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x8000b980]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fdiv.h t6, t5, t4, dyn
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x8000b988]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b14]:fdiv.h t6, t5, t4, dyn
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x8000b990]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fdiv.h t6, t5, t4, dyn
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x8000b998]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fdiv.h t6, t5, t4, dyn
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x8000b9a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fdiv.h t6, t5, t4, dyn
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x8000b9a8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b94]:fdiv.h t6, t5, t4, dyn
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x8000b9b0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x8000b9b8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x8000b9c0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fdiv.h t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x8000b9c8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c14]:fdiv.h t6, t5, t4, dyn
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x8000b9d0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fdiv.h t6, t5, t4, dyn
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x8000b9d8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c54]:fdiv.h t6, t5, t4, dyn
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x8000b9e0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fdiv.h t6, t5, t4, dyn
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x8000b9e8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c94]:fdiv.h t6, t5, t4, dyn
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x8000b9f0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x8000b9f8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x8000ba00]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fdiv.h t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x8000ba08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fdiv.h t6, t5, t4, dyn
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x8000ba10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fdiv.h t6, t5, t4, dyn
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x8000ba18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d54]:fdiv.h t6, t5, t4, dyn
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x8000ba20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fdiv.h t6, t5, t4, dyn
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x8000ba28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d94]:fdiv.h t6, t5, t4, dyn
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x8000ba30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fdiv.h t6, t5, t4, dyn
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x8000ba38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x8000ba40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fdiv.h t6, t5, t4, dyn
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x8000ba48]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e14]:fdiv.h t6, t5, t4, dyn
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x8000ba50]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fdiv.h t6, t5, t4, dyn
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x8000ba58]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e54]:fdiv.h t6, t5, t4, dyn
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x8000ba60]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fdiv.h t6, t5, t4, dyn
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x8000ba68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e94]:fdiv.h t6, t5, t4, dyn
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x8000ba70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x8000ba78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x8000ba80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x8000ba88]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f14]:fdiv.h t6, t5, t4, dyn
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x8000ba90]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f34]:fdiv.h t6, t5, t4, dyn
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x8000ba98]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f54]:fdiv.h t6, t5, t4, dyn
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x8000baa0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f74]:fdiv.h t6, t5, t4, dyn
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x8000baa8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f94]:fdiv.h t6, t5, t4, dyn
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x8000bab0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fdiv.h t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x8000bab8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fdiv.h t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x8000bac0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fdiv.h t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x8000bac8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001014]:fdiv.h t6, t5, t4, dyn
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x8000bad0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001034]:fdiv.h t6, t5, t4, dyn
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x8000bad8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001054]:fdiv.h t6, t5, t4, dyn
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x8000bae0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001074]:fdiv.h t6, t5, t4, dyn
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x8000bae8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001094]:fdiv.h t6, t5, t4, dyn
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x8000baf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010b4]:fdiv.h t6, t5, t4, dyn
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x8000baf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010d4]:fdiv.h t6, t5, t4, dyn
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x8000bb00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010f4]:fdiv.h t6, t5, t4, dyn
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x8000bb08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001114]:fdiv.h t6, t5, t4, dyn
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x8000bb10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001134]:fdiv.h t6, t5, t4, dyn
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x8000bb18]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001154]:fdiv.h t6, t5, t4, dyn
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x8000bb20]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001174]:fdiv.h t6, t5, t4, dyn
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x8000bb28]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001194]:fdiv.h t6, t5, t4, dyn
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x8000bb30]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011b4]:fdiv.h t6, t5, t4, dyn
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x8000bb38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011d4]:fdiv.h t6, t5, t4, dyn
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x8000bb40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011f4]:fdiv.h t6, t5, t4, dyn
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x8000bb48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001214]:fdiv.h t6, t5, t4, dyn
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x8000bb50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001234]:fdiv.h t6, t5, t4, dyn
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x8000bb58]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001254]:fdiv.h t6, t5, t4, dyn
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x8000bb60]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001274]:fdiv.h t6, t5, t4, dyn
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x8000bb68]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001294]:fdiv.h t6, t5, t4, dyn
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x8000bb70]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012b4]:fdiv.h t6, t5, t4, dyn
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x8000bb78]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012d4]:fdiv.h t6, t5, t4, dyn
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x8000bb80]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012f4]:fdiv.h t6, t5, t4, dyn
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x8000bb88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001314]:fdiv.h t6, t5, t4, dyn
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x8000bb90]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001334]:fdiv.h t6, t5, t4, dyn
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x8000bb98]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001354]:fdiv.h t6, t5, t4, dyn
	-[0x80001358]:csrrs a2, fcsr, zero
	-[0x8000135c]:sw t6, 960(fp)
Current Store : [0x80001360] : sw a2, 964(fp) -- Store: [0x8000bba0]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001374]:fdiv.h t6, t5, t4, dyn
	-[0x80001378]:csrrs a2, fcsr, zero
	-[0x8000137c]:sw t6, 968(fp)
Current Store : [0x80001380] : sw a2, 972(fp) -- Store: [0x8000bba8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001394]:fdiv.h t6, t5, t4, dyn
	-[0x80001398]:csrrs a2, fcsr, zero
	-[0x8000139c]:sw t6, 976(fp)
Current Store : [0x800013a0] : sw a2, 980(fp) -- Store: [0x8000bbb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013b4]:fdiv.h t6, t5, t4, dyn
	-[0x800013b8]:csrrs a2, fcsr, zero
	-[0x800013bc]:sw t6, 984(fp)
Current Store : [0x800013c0] : sw a2, 988(fp) -- Store: [0x8000bbb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013d4]:fdiv.h t6, t5, t4, dyn
	-[0x800013d8]:csrrs a2, fcsr, zero
	-[0x800013dc]:sw t6, 992(fp)
Current Store : [0x800013e0] : sw a2, 996(fp) -- Store: [0x8000bbc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013f4]:fdiv.h t6, t5, t4, dyn
	-[0x800013f8]:csrrs a2, fcsr, zero
	-[0x800013fc]:sw t6, 1000(fp)
Current Store : [0x80001400] : sw a2, 1004(fp) -- Store: [0x8000bbc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001414]:fdiv.h t6, t5, t4, dyn
	-[0x80001418]:csrrs a2, fcsr, zero
	-[0x8000141c]:sw t6, 1008(fp)
Current Store : [0x80001420] : sw a2, 1012(fp) -- Store: [0x8000bbd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001434]:fdiv.h t6, t5, t4, dyn
	-[0x80001438]:csrrs a2, fcsr, zero
	-[0x8000143c]:sw t6, 1016(fp)
Current Store : [0x80001440] : sw a2, 1020(fp) -- Store: [0x8000bbd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000145c]:fdiv.h t6, t5, t4, dyn
	-[0x80001460]:csrrs a2, fcsr, zero
	-[0x80001464]:sw t6, 0(fp)
Current Store : [0x80001468] : sw a2, 4(fp) -- Store: [0x8000bbe0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000147c]:fdiv.h t6, t5, t4, dyn
	-[0x80001480]:csrrs a2, fcsr, zero
	-[0x80001484]:sw t6, 8(fp)
Current Store : [0x80001488] : sw a2, 12(fp) -- Store: [0x8000bbe8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000149c]:fdiv.h t6, t5, t4, dyn
	-[0x800014a0]:csrrs a2, fcsr, zero
	-[0x800014a4]:sw t6, 16(fp)
Current Store : [0x800014a8] : sw a2, 20(fp) -- Store: [0x8000bbf0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014bc]:fdiv.h t6, t5, t4, dyn
	-[0x800014c0]:csrrs a2, fcsr, zero
	-[0x800014c4]:sw t6, 24(fp)
Current Store : [0x800014c8] : sw a2, 28(fp) -- Store: [0x8000bbf8]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014dc]:fdiv.h t6, t5, t4, dyn
	-[0x800014e0]:csrrs a2, fcsr, zero
	-[0x800014e4]:sw t6, 32(fp)
Current Store : [0x800014e8] : sw a2, 36(fp) -- Store: [0x8000bc00]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001500]:csrrs a2, fcsr, zero
	-[0x80001504]:sw t6, 40(fp)
Current Store : [0x80001508] : sw a2, 44(fp) -- Store: [0x8000bc08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000151c]:fdiv.h t6, t5, t4, dyn
	-[0x80001520]:csrrs a2, fcsr, zero
	-[0x80001524]:sw t6, 48(fp)
Current Store : [0x80001528] : sw a2, 52(fp) -- Store: [0x8000bc10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000153c]:fdiv.h t6, t5, t4, dyn
	-[0x80001540]:csrrs a2, fcsr, zero
	-[0x80001544]:sw t6, 56(fp)
Current Store : [0x80001548] : sw a2, 60(fp) -- Store: [0x8000bc18]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000155c]:fdiv.h t6, t5, t4, dyn
	-[0x80001560]:csrrs a2, fcsr, zero
	-[0x80001564]:sw t6, 64(fp)
Current Store : [0x80001568] : sw a2, 68(fp) -- Store: [0x8000bc20]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000157c]:fdiv.h t6, t5, t4, dyn
	-[0x80001580]:csrrs a2, fcsr, zero
	-[0x80001584]:sw t6, 72(fp)
Current Store : [0x80001588] : sw a2, 76(fp) -- Store: [0x8000bc28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000159c]:fdiv.h t6, t5, t4, dyn
	-[0x800015a0]:csrrs a2, fcsr, zero
	-[0x800015a4]:sw t6, 80(fp)
Current Store : [0x800015a8] : sw a2, 84(fp) -- Store: [0x8000bc30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015bc]:fdiv.h t6, t5, t4, dyn
	-[0x800015c0]:csrrs a2, fcsr, zero
	-[0x800015c4]:sw t6, 88(fp)
Current Store : [0x800015c8] : sw a2, 92(fp) -- Store: [0x8000bc38]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015dc]:fdiv.h t6, t5, t4, dyn
	-[0x800015e0]:csrrs a2, fcsr, zero
	-[0x800015e4]:sw t6, 96(fp)
Current Store : [0x800015e8] : sw a2, 100(fp) -- Store: [0x8000bc40]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001600]:csrrs a2, fcsr, zero
	-[0x80001604]:sw t6, 104(fp)
Current Store : [0x80001608] : sw a2, 108(fp) -- Store: [0x8000bc48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000161c]:fdiv.h t6, t5, t4, dyn
	-[0x80001620]:csrrs a2, fcsr, zero
	-[0x80001624]:sw t6, 112(fp)
Current Store : [0x80001628] : sw a2, 116(fp) -- Store: [0x8000bc50]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000163c]:fdiv.h t6, t5, t4, dyn
	-[0x80001640]:csrrs a2, fcsr, zero
	-[0x80001644]:sw t6, 120(fp)
Current Store : [0x80001648] : sw a2, 124(fp) -- Store: [0x8000bc58]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000165c]:fdiv.h t6, t5, t4, dyn
	-[0x80001660]:csrrs a2, fcsr, zero
	-[0x80001664]:sw t6, 128(fp)
Current Store : [0x80001668] : sw a2, 132(fp) -- Store: [0x8000bc60]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000167c]:fdiv.h t6, t5, t4, dyn
	-[0x80001680]:csrrs a2, fcsr, zero
	-[0x80001684]:sw t6, 136(fp)
Current Store : [0x80001688] : sw a2, 140(fp) -- Store: [0x8000bc68]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000169c]:fdiv.h t6, t5, t4, dyn
	-[0x800016a0]:csrrs a2, fcsr, zero
	-[0x800016a4]:sw t6, 144(fp)
Current Store : [0x800016a8] : sw a2, 148(fp) -- Store: [0x8000bc70]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016bc]:fdiv.h t6, t5, t4, dyn
	-[0x800016c0]:csrrs a2, fcsr, zero
	-[0x800016c4]:sw t6, 152(fp)
Current Store : [0x800016c8] : sw a2, 156(fp) -- Store: [0x8000bc78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016dc]:fdiv.h t6, t5, t4, dyn
	-[0x800016e0]:csrrs a2, fcsr, zero
	-[0x800016e4]:sw t6, 160(fp)
Current Store : [0x800016e8] : sw a2, 164(fp) -- Store: [0x8000bc80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001700]:csrrs a2, fcsr, zero
	-[0x80001704]:sw t6, 168(fp)
Current Store : [0x80001708] : sw a2, 172(fp) -- Store: [0x8000bc88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000171c]:fdiv.h t6, t5, t4, dyn
	-[0x80001720]:csrrs a2, fcsr, zero
	-[0x80001724]:sw t6, 176(fp)
Current Store : [0x80001728] : sw a2, 180(fp) -- Store: [0x8000bc90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000173c]:fdiv.h t6, t5, t4, dyn
	-[0x80001740]:csrrs a2, fcsr, zero
	-[0x80001744]:sw t6, 184(fp)
Current Store : [0x80001748] : sw a2, 188(fp) -- Store: [0x8000bc98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000175c]:fdiv.h t6, t5, t4, dyn
	-[0x80001760]:csrrs a2, fcsr, zero
	-[0x80001764]:sw t6, 192(fp)
Current Store : [0x80001768] : sw a2, 196(fp) -- Store: [0x8000bca0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000177c]:fdiv.h t6, t5, t4, dyn
	-[0x80001780]:csrrs a2, fcsr, zero
	-[0x80001784]:sw t6, 200(fp)
Current Store : [0x80001788] : sw a2, 204(fp) -- Store: [0x8000bca8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000179c]:fdiv.h t6, t5, t4, dyn
	-[0x800017a0]:csrrs a2, fcsr, zero
	-[0x800017a4]:sw t6, 208(fp)
Current Store : [0x800017a8] : sw a2, 212(fp) -- Store: [0x8000bcb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017bc]:fdiv.h t6, t5, t4, dyn
	-[0x800017c0]:csrrs a2, fcsr, zero
	-[0x800017c4]:sw t6, 216(fp)
Current Store : [0x800017c8] : sw a2, 220(fp) -- Store: [0x8000bcb8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017dc]:fdiv.h t6, t5, t4, dyn
	-[0x800017e0]:csrrs a2, fcsr, zero
	-[0x800017e4]:sw t6, 224(fp)
Current Store : [0x800017e8] : sw a2, 228(fp) -- Store: [0x8000bcc0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001800]:csrrs a2, fcsr, zero
	-[0x80001804]:sw t6, 232(fp)
Current Store : [0x80001808] : sw a2, 236(fp) -- Store: [0x8000bcc8]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000181c]:fdiv.h t6, t5, t4, dyn
	-[0x80001820]:csrrs a2, fcsr, zero
	-[0x80001824]:sw t6, 240(fp)
Current Store : [0x80001828] : sw a2, 244(fp) -- Store: [0x8000bcd0]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000183c]:fdiv.h t6, t5, t4, dyn
	-[0x80001840]:csrrs a2, fcsr, zero
	-[0x80001844]:sw t6, 248(fp)
Current Store : [0x80001848] : sw a2, 252(fp) -- Store: [0x8000bcd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000185c]:fdiv.h t6, t5, t4, dyn
	-[0x80001860]:csrrs a2, fcsr, zero
	-[0x80001864]:sw t6, 256(fp)
Current Store : [0x80001868] : sw a2, 260(fp) -- Store: [0x8000bce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000187c]:fdiv.h t6, t5, t4, dyn
	-[0x80001880]:csrrs a2, fcsr, zero
	-[0x80001884]:sw t6, 264(fp)
Current Store : [0x80001888] : sw a2, 268(fp) -- Store: [0x8000bce8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000189c]:fdiv.h t6, t5, t4, dyn
	-[0x800018a0]:csrrs a2, fcsr, zero
	-[0x800018a4]:sw t6, 272(fp)
Current Store : [0x800018a8] : sw a2, 276(fp) -- Store: [0x8000bcf0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018bc]:fdiv.h t6, t5, t4, dyn
	-[0x800018c0]:csrrs a2, fcsr, zero
	-[0x800018c4]:sw t6, 280(fp)
Current Store : [0x800018c8] : sw a2, 284(fp) -- Store: [0x8000bcf8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018dc]:fdiv.h t6, t5, t4, dyn
	-[0x800018e0]:csrrs a2, fcsr, zero
	-[0x800018e4]:sw t6, 288(fp)
Current Store : [0x800018e8] : sw a2, 292(fp) -- Store: [0x8000bd00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001900]:csrrs a2, fcsr, zero
	-[0x80001904]:sw t6, 296(fp)
Current Store : [0x80001908] : sw a2, 300(fp) -- Store: [0x8000bd08]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000191c]:fdiv.h t6, t5, t4, dyn
	-[0x80001920]:csrrs a2, fcsr, zero
	-[0x80001924]:sw t6, 304(fp)
Current Store : [0x80001928] : sw a2, 308(fp) -- Store: [0x8000bd10]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000193c]:fdiv.h t6, t5, t4, dyn
	-[0x80001940]:csrrs a2, fcsr, zero
	-[0x80001944]:sw t6, 312(fp)
Current Store : [0x80001948] : sw a2, 316(fp) -- Store: [0x8000bd18]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000195c]:fdiv.h t6, t5, t4, dyn
	-[0x80001960]:csrrs a2, fcsr, zero
	-[0x80001964]:sw t6, 320(fp)
Current Store : [0x80001968] : sw a2, 324(fp) -- Store: [0x8000bd20]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000197c]:fdiv.h t6, t5, t4, dyn
	-[0x80001980]:csrrs a2, fcsr, zero
	-[0x80001984]:sw t6, 328(fp)
Current Store : [0x80001988] : sw a2, 332(fp) -- Store: [0x8000bd28]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000199c]:fdiv.h t6, t5, t4, dyn
	-[0x800019a0]:csrrs a2, fcsr, zero
	-[0x800019a4]:sw t6, 336(fp)
Current Store : [0x800019a8] : sw a2, 340(fp) -- Store: [0x8000bd30]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019bc]:fdiv.h t6, t5, t4, dyn
	-[0x800019c0]:csrrs a2, fcsr, zero
	-[0x800019c4]:sw t6, 344(fp)
Current Store : [0x800019c8] : sw a2, 348(fp) -- Store: [0x8000bd38]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019dc]:fdiv.h t6, t5, t4, dyn
	-[0x800019e0]:csrrs a2, fcsr, zero
	-[0x800019e4]:sw t6, 352(fp)
Current Store : [0x800019e8] : sw a2, 356(fp) -- Store: [0x8000bd40]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019fc]:fdiv.h t6, t5, t4, dyn
	-[0x80001a00]:csrrs a2, fcsr, zero
	-[0x80001a04]:sw t6, 360(fp)
Current Store : [0x80001a08] : sw a2, 364(fp) -- Store: [0x8000bd48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001a20]:csrrs a2, fcsr, zero
	-[0x80001a24]:sw t6, 368(fp)
Current Store : [0x80001a28] : sw a2, 372(fp) -- Store: [0x8000bd50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001a40]:csrrs a2, fcsr, zero
	-[0x80001a44]:sw t6, 376(fp)
Current Store : [0x80001a48] : sw a2, 380(fp) -- Store: [0x8000bd58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001a60]:csrrs a2, fcsr, zero
	-[0x80001a64]:sw t6, 384(fp)
Current Store : [0x80001a68] : sw a2, 388(fp) -- Store: [0x8000bd60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001a80]:csrrs a2, fcsr, zero
	-[0x80001a84]:sw t6, 392(fp)
Current Store : [0x80001a88] : sw a2, 396(fp) -- Store: [0x8000bd68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001aa0]:csrrs a2, fcsr, zero
	-[0x80001aa4]:sw t6, 400(fp)
Current Store : [0x80001aa8] : sw a2, 404(fp) -- Store: [0x8000bd70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001abc]:fdiv.h t6, t5, t4, dyn
	-[0x80001ac0]:csrrs a2, fcsr, zero
	-[0x80001ac4]:sw t6, 408(fp)
Current Store : [0x80001ac8] : sw a2, 412(fp) -- Store: [0x8000bd78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001adc]:fdiv.h t6, t5, t4, dyn
	-[0x80001ae0]:csrrs a2, fcsr, zero
	-[0x80001ae4]:sw t6, 416(fp)
Current Store : [0x80001ae8] : sw a2, 420(fp) -- Store: [0x8000bd80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001afc]:fdiv.h t6, t5, t4, dyn
	-[0x80001b00]:csrrs a2, fcsr, zero
	-[0x80001b04]:sw t6, 424(fp)
Current Store : [0x80001b08] : sw a2, 428(fp) -- Store: [0x8000bd88]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001b20]:csrrs a2, fcsr, zero
	-[0x80001b24]:sw t6, 432(fp)
Current Store : [0x80001b28] : sw a2, 436(fp) -- Store: [0x8000bd90]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001b40]:csrrs a2, fcsr, zero
	-[0x80001b44]:sw t6, 440(fp)
Current Store : [0x80001b48] : sw a2, 444(fp) -- Store: [0x8000bd98]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001b60]:csrrs a2, fcsr, zero
	-[0x80001b64]:sw t6, 448(fp)
Current Store : [0x80001b68] : sw a2, 452(fp) -- Store: [0x8000bda0]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001b80]:csrrs a2, fcsr, zero
	-[0x80001b84]:sw t6, 456(fp)
Current Store : [0x80001b88] : sw a2, 460(fp) -- Store: [0x8000bda8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001ba0]:csrrs a2, fcsr, zero
	-[0x80001ba4]:sw t6, 464(fp)
Current Store : [0x80001ba8] : sw a2, 468(fp) -- Store: [0x8000bdb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bbc]:fdiv.h t6, t5, t4, dyn
	-[0x80001bc0]:csrrs a2, fcsr, zero
	-[0x80001bc4]:sw t6, 472(fp)
Current Store : [0x80001bc8] : sw a2, 476(fp) -- Store: [0x8000bdb8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bdc]:fdiv.h t6, t5, t4, dyn
	-[0x80001be0]:csrrs a2, fcsr, zero
	-[0x80001be4]:sw t6, 480(fp)
Current Store : [0x80001be8] : sw a2, 484(fp) -- Store: [0x8000bdc0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bfc]:fdiv.h t6, t5, t4, dyn
	-[0x80001c00]:csrrs a2, fcsr, zero
	-[0x80001c04]:sw t6, 488(fp)
Current Store : [0x80001c08] : sw a2, 492(fp) -- Store: [0x8000bdc8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001c20]:csrrs a2, fcsr, zero
	-[0x80001c24]:sw t6, 496(fp)
Current Store : [0x80001c28] : sw a2, 500(fp) -- Store: [0x8000bdd0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001c40]:csrrs a2, fcsr, zero
	-[0x80001c44]:sw t6, 504(fp)
Current Store : [0x80001c48] : sw a2, 508(fp) -- Store: [0x8000bdd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001c60]:csrrs a2, fcsr, zero
	-[0x80001c64]:sw t6, 512(fp)
Current Store : [0x80001c68] : sw a2, 516(fp) -- Store: [0x8000bde0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001c80]:csrrs a2, fcsr, zero
	-[0x80001c84]:sw t6, 520(fp)
Current Store : [0x80001c88] : sw a2, 524(fp) -- Store: [0x8000bde8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001ca0]:csrrs a2, fcsr, zero
	-[0x80001ca4]:sw t6, 528(fp)
Current Store : [0x80001ca8] : sw a2, 532(fp) -- Store: [0x8000bdf0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cbc]:fdiv.h t6, t5, t4, dyn
	-[0x80001cc0]:csrrs a2, fcsr, zero
	-[0x80001cc4]:sw t6, 536(fp)
Current Store : [0x80001cc8] : sw a2, 540(fp) -- Store: [0x8000bdf8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fdiv.h t6, t5, t4, dyn
	-[0x80001ce0]:csrrs a2, fcsr, zero
	-[0x80001ce4]:sw t6, 544(fp)
Current Store : [0x80001ce8] : sw a2, 548(fp) -- Store: [0x8000be00]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cfc]:fdiv.h t6, t5, t4, dyn
	-[0x80001d00]:csrrs a2, fcsr, zero
	-[0x80001d04]:sw t6, 552(fp)
Current Store : [0x80001d08] : sw a2, 556(fp) -- Store: [0x8000be08]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001d20]:csrrs a2, fcsr, zero
	-[0x80001d24]:sw t6, 560(fp)
Current Store : [0x80001d28] : sw a2, 564(fp) -- Store: [0x8000be10]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001d40]:csrrs a2, fcsr, zero
	-[0x80001d44]:sw t6, 568(fp)
Current Store : [0x80001d48] : sw a2, 572(fp) -- Store: [0x8000be18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001d60]:csrrs a2, fcsr, zero
	-[0x80001d64]:sw t6, 576(fp)
Current Store : [0x80001d68] : sw a2, 580(fp) -- Store: [0x8000be20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001d80]:csrrs a2, fcsr, zero
	-[0x80001d84]:sw t6, 584(fp)
Current Store : [0x80001d88] : sw a2, 588(fp) -- Store: [0x8000be28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001da0]:csrrs a2, fcsr, zero
	-[0x80001da4]:sw t6, 592(fp)
Current Store : [0x80001da8] : sw a2, 596(fp) -- Store: [0x8000be30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dbc]:fdiv.h t6, t5, t4, dyn
	-[0x80001dc0]:csrrs a2, fcsr, zero
	-[0x80001dc4]:sw t6, 600(fp)
Current Store : [0x80001dc8] : sw a2, 604(fp) -- Store: [0x8000be38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ddc]:fdiv.h t6, t5, t4, dyn
	-[0x80001de0]:csrrs a2, fcsr, zero
	-[0x80001de4]:sw t6, 608(fp)
Current Store : [0x80001de8] : sw a2, 612(fp) -- Store: [0x8000be40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dfc]:fdiv.h t6, t5, t4, dyn
	-[0x80001e00]:csrrs a2, fcsr, zero
	-[0x80001e04]:sw t6, 616(fp)
Current Store : [0x80001e08] : sw a2, 620(fp) -- Store: [0x8000be48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001e20]:csrrs a2, fcsr, zero
	-[0x80001e24]:sw t6, 624(fp)
Current Store : [0x80001e28] : sw a2, 628(fp) -- Store: [0x8000be50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001e40]:csrrs a2, fcsr, zero
	-[0x80001e44]:sw t6, 632(fp)
Current Store : [0x80001e48] : sw a2, 636(fp) -- Store: [0x8000be58]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001e60]:csrrs a2, fcsr, zero
	-[0x80001e64]:sw t6, 640(fp)
Current Store : [0x80001e68] : sw a2, 644(fp) -- Store: [0x8000be60]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001e80]:csrrs a2, fcsr, zero
	-[0x80001e84]:sw t6, 648(fp)
Current Store : [0x80001e88] : sw a2, 652(fp) -- Store: [0x8000be68]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001ea0]:csrrs a2, fcsr, zero
	-[0x80001ea4]:sw t6, 656(fp)
Current Store : [0x80001ea8] : sw a2, 660(fp) -- Store: [0x8000be70]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ebc]:fdiv.h t6, t5, t4, dyn
	-[0x80001ec0]:csrrs a2, fcsr, zero
	-[0x80001ec4]:sw t6, 664(fp)
Current Store : [0x80001ec8] : sw a2, 668(fp) -- Store: [0x8000be78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001edc]:fdiv.h t6, t5, t4, dyn
	-[0x80001ee0]:csrrs a2, fcsr, zero
	-[0x80001ee4]:sw t6, 672(fp)
Current Store : [0x80001ee8] : sw a2, 676(fp) -- Store: [0x8000be80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001efc]:fdiv.h t6, t5, t4, dyn
	-[0x80001f00]:csrrs a2, fcsr, zero
	-[0x80001f04]:sw t6, 680(fp)
Current Store : [0x80001f08] : sw a2, 684(fp) -- Store: [0x8000be88]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f1c]:fdiv.h t6, t5, t4, dyn
	-[0x80001f20]:csrrs a2, fcsr, zero
	-[0x80001f24]:sw t6, 688(fp)
Current Store : [0x80001f28] : sw a2, 692(fp) -- Store: [0x8000be90]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f3c]:fdiv.h t6, t5, t4, dyn
	-[0x80001f40]:csrrs a2, fcsr, zero
	-[0x80001f44]:sw t6, 696(fp)
Current Store : [0x80001f48] : sw a2, 700(fp) -- Store: [0x8000be98]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f5c]:fdiv.h t6, t5, t4, dyn
	-[0x80001f60]:csrrs a2, fcsr, zero
	-[0x80001f64]:sw t6, 704(fp)
Current Store : [0x80001f68] : sw a2, 708(fp) -- Store: [0x8000bea0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f7c]:fdiv.h t6, t5, t4, dyn
	-[0x80001f80]:csrrs a2, fcsr, zero
	-[0x80001f84]:sw t6, 712(fp)
Current Store : [0x80001f88] : sw a2, 716(fp) -- Store: [0x8000bea8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f9c]:fdiv.h t6, t5, t4, dyn
	-[0x80001fa0]:csrrs a2, fcsr, zero
	-[0x80001fa4]:sw t6, 720(fp)
Current Store : [0x80001fa8] : sw a2, 724(fp) -- Store: [0x8000beb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fbc]:fdiv.h t6, t5, t4, dyn
	-[0x80001fc0]:csrrs a2, fcsr, zero
	-[0x80001fc4]:sw t6, 728(fp)
Current Store : [0x80001fc8] : sw a2, 732(fp) -- Store: [0x8000beb8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fdc]:fdiv.h t6, t5, t4, dyn
	-[0x80001fe0]:csrrs a2, fcsr, zero
	-[0x80001fe4]:sw t6, 736(fp)
Current Store : [0x80001fe8] : sw a2, 740(fp) -- Store: [0x8000bec0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ffc]:fdiv.h t6, t5, t4, dyn
	-[0x80002000]:csrrs a2, fcsr, zero
	-[0x80002004]:sw t6, 744(fp)
Current Store : [0x80002008] : sw a2, 748(fp) -- Store: [0x8000bec8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000201c]:fdiv.h t6, t5, t4, dyn
	-[0x80002020]:csrrs a2, fcsr, zero
	-[0x80002024]:sw t6, 752(fp)
Current Store : [0x80002028] : sw a2, 756(fp) -- Store: [0x8000bed0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000203c]:fdiv.h t6, t5, t4, dyn
	-[0x80002040]:csrrs a2, fcsr, zero
	-[0x80002044]:sw t6, 760(fp)
Current Store : [0x80002048] : sw a2, 764(fp) -- Store: [0x8000bed8]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000205c]:fdiv.h t6, t5, t4, dyn
	-[0x80002060]:csrrs a2, fcsr, zero
	-[0x80002064]:sw t6, 768(fp)
Current Store : [0x80002068] : sw a2, 772(fp) -- Store: [0x8000bee0]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000207c]:fdiv.h t6, t5, t4, dyn
	-[0x80002080]:csrrs a2, fcsr, zero
	-[0x80002084]:sw t6, 776(fp)
Current Store : [0x80002088] : sw a2, 780(fp) -- Store: [0x8000bee8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000209c]:fdiv.h t6, t5, t4, dyn
	-[0x800020a0]:csrrs a2, fcsr, zero
	-[0x800020a4]:sw t6, 784(fp)
Current Store : [0x800020a8] : sw a2, 788(fp) -- Store: [0x8000bef0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020bc]:fdiv.h t6, t5, t4, dyn
	-[0x800020c0]:csrrs a2, fcsr, zero
	-[0x800020c4]:sw t6, 792(fp)
Current Store : [0x800020c8] : sw a2, 796(fp) -- Store: [0x8000bef8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020dc]:fdiv.h t6, t5, t4, dyn
	-[0x800020e0]:csrrs a2, fcsr, zero
	-[0x800020e4]:sw t6, 800(fp)
Current Store : [0x800020e8] : sw a2, 804(fp) -- Store: [0x8000bf00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020fc]:fdiv.h t6, t5, t4, dyn
	-[0x80002100]:csrrs a2, fcsr, zero
	-[0x80002104]:sw t6, 808(fp)
Current Store : [0x80002108] : sw a2, 812(fp) -- Store: [0x8000bf08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000211c]:fdiv.h t6, t5, t4, dyn
	-[0x80002120]:csrrs a2, fcsr, zero
	-[0x80002124]:sw t6, 816(fp)
Current Store : [0x80002128] : sw a2, 820(fp) -- Store: [0x8000bf10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000213c]:fdiv.h t6, t5, t4, dyn
	-[0x80002140]:csrrs a2, fcsr, zero
	-[0x80002144]:sw t6, 824(fp)
Current Store : [0x80002148] : sw a2, 828(fp) -- Store: [0x8000bf18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000215c]:fdiv.h t6, t5, t4, dyn
	-[0x80002160]:csrrs a2, fcsr, zero
	-[0x80002164]:sw t6, 832(fp)
Current Store : [0x80002168] : sw a2, 836(fp) -- Store: [0x8000bf20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000217c]:fdiv.h t6, t5, t4, dyn
	-[0x80002180]:csrrs a2, fcsr, zero
	-[0x80002184]:sw t6, 840(fp)
Current Store : [0x80002188] : sw a2, 844(fp) -- Store: [0x8000bf28]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000219c]:fdiv.h t6, t5, t4, dyn
	-[0x800021a0]:csrrs a2, fcsr, zero
	-[0x800021a4]:sw t6, 848(fp)
Current Store : [0x800021a8] : sw a2, 852(fp) -- Store: [0x8000bf30]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021bc]:fdiv.h t6, t5, t4, dyn
	-[0x800021c0]:csrrs a2, fcsr, zero
	-[0x800021c4]:sw t6, 856(fp)
Current Store : [0x800021c8] : sw a2, 860(fp) -- Store: [0x8000bf38]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021dc]:fdiv.h t6, t5, t4, dyn
	-[0x800021e0]:csrrs a2, fcsr, zero
	-[0x800021e4]:sw t6, 864(fp)
Current Store : [0x800021e8] : sw a2, 868(fp) -- Store: [0x8000bf40]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021fc]:fdiv.h t6, t5, t4, dyn
	-[0x80002200]:csrrs a2, fcsr, zero
	-[0x80002204]:sw t6, 872(fp)
Current Store : [0x80002208] : sw a2, 876(fp) -- Store: [0x8000bf48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000221c]:fdiv.h t6, t5, t4, dyn
	-[0x80002220]:csrrs a2, fcsr, zero
	-[0x80002224]:sw t6, 880(fp)
Current Store : [0x80002228] : sw a2, 884(fp) -- Store: [0x8000bf50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000223c]:fdiv.h t6, t5, t4, dyn
	-[0x80002240]:csrrs a2, fcsr, zero
	-[0x80002244]:sw t6, 888(fp)
Current Store : [0x80002248] : sw a2, 892(fp) -- Store: [0x8000bf58]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000225c]:fdiv.h t6, t5, t4, dyn
	-[0x80002260]:csrrs a2, fcsr, zero
	-[0x80002264]:sw t6, 896(fp)
Current Store : [0x80002268] : sw a2, 900(fp) -- Store: [0x8000bf60]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000227c]:fdiv.h t6, t5, t4, dyn
	-[0x80002280]:csrrs a2, fcsr, zero
	-[0x80002284]:sw t6, 904(fp)
Current Store : [0x80002288] : sw a2, 908(fp) -- Store: [0x8000bf68]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000229c]:fdiv.h t6, t5, t4, dyn
	-[0x800022a0]:csrrs a2, fcsr, zero
	-[0x800022a4]:sw t6, 912(fp)
Current Store : [0x800022a8] : sw a2, 916(fp) -- Store: [0x8000bf70]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022bc]:fdiv.h t6, t5, t4, dyn
	-[0x800022c0]:csrrs a2, fcsr, zero
	-[0x800022c4]:sw t6, 920(fp)
Current Store : [0x800022c8] : sw a2, 924(fp) -- Store: [0x8000bf78]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022dc]:fdiv.h t6, t5, t4, dyn
	-[0x800022e0]:csrrs a2, fcsr, zero
	-[0x800022e4]:sw t6, 928(fp)
Current Store : [0x800022e8] : sw a2, 932(fp) -- Store: [0x8000bf80]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022fc]:fdiv.h t6, t5, t4, dyn
	-[0x80002300]:csrrs a2, fcsr, zero
	-[0x80002304]:sw t6, 936(fp)
Current Store : [0x80002308] : sw a2, 940(fp) -- Store: [0x8000bf88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000231c]:fdiv.h t6, t5, t4, dyn
	-[0x80002320]:csrrs a2, fcsr, zero
	-[0x80002324]:sw t6, 944(fp)
Current Store : [0x80002328] : sw a2, 948(fp) -- Store: [0x8000bf90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000233c]:fdiv.h t6, t5, t4, dyn
	-[0x80002340]:csrrs a2, fcsr, zero
	-[0x80002344]:sw t6, 952(fp)
Current Store : [0x80002348] : sw a2, 956(fp) -- Store: [0x8000bf98]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000235c]:fdiv.h t6, t5, t4, dyn
	-[0x80002360]:csrrs a2, fcsr, zero
	-[0x80002364]:sw t6, 960(fp)
Current Store : [0x80002368] : sw a2, 964(fp) -- Store: [0x8000bfa0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000237c]:fdiv.h t6, t5, t4, dyn
	-[0x80002380]:csrrs a2, fcsr, zero
	-[0x80002384]:sw t6, 968(fp)
Current Store : [0x80002388] : sw a2, 972(fp) -- Store: [0x8000bfa8]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000239c]:fdiv.h t6, t5, t4, dyn
	-[0x800023a0]:csrrs a2, fcsr, zero
	-[0x800023a4]:sw t6, 976(fp)
Current Store : [0x800023a8] : sw a2, 980(fp) -- Store: [0x8000bfb0]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023bc]:fdiv.h t6, t5, t4, dyn
	-[0x800023c0]:csrrs a2, fcsr, zero
	-[0x800023c4]:sw t6, 984(fp)
Current Store : [0x800023c8] : sw a2, 988(fp) -- Store: [0x8000bfb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023dc]:fdiv.h t6, t5, t4, dyn
	-[0x800023e0]:csrrs a2, fcsr, zero
	-[0x800023e4]:sw t6, 992(fp)
Current Store : [0x800023e8] : sw a2, 996(fp) -- Store: [0x8000bfc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000241c]:fdiv.h t6, t5, t4, dyn
	-[0x80002420]:csrrs a2, fcsr, zero
	-[0x80002424]:sw t6, 1000(fp)
Current Store : [0x80002428] : sw a2, 1004(fp) -- Store: [0x8000bfc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000245c]:fdiv.h t6, t5, t4, dyn
	-[0x80002460]:csrrs a2, fcsr, zero
	-[0x80002464]:sw t6, 1008(fp)
Current Store : [0x80002468] : sw a2, 1012(fp) -- Store: [0x8000bfd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000249c]:fdiv.h t6, t5, t4, dyn
	-[0x800024a0]:csrrs a2, fcsr, zero
	-[0x800024a4]:sw t6, 1016(fp)
Current Store : [0x800024a8] : sw a2, 1020(fp) -- Store: [0x8000bfd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800024e4]:fdiv.h t6, t5, t4, dyn
	-[0x800024e8]:csrrs a2, fcsr, zero
	-[0x800024ec]:sw t6, 0(fp)
Current Store : [0x800024f0] : sw a2, 4(fp) -- Store: [0x8000bfe0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002524]:fdiv.h t6, t5, t4, dyn
	-[0x80002528]:csrrs a2, fcsr, zero
	-[0x8000252c]:sw t6, 8(fp)
Current Store : [0x80002530] : sw a2, 12(fp) -- Store: [0x8000bfe8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002564]:fdiv.h t6, t5, t4, dyn
	-[0x80002568]:csrrs a2, fcsr, zero
	-[0x8000256c]:sw t6, 16(fp)
Current Store : [0x80002570] : sw a2, 20(fp) -- Store: [0x8000bff0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025a4]:fdiv.h t6, t5, t4, dyn
	-[0x800025a8]:csrrs a2, fcsr, zero
	-[0x800025ac]:sw t6, 24(fp)
Current Store : [0x800025b0] : sw a2, 28(fp) -- Store: [0x8000bff8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025e4]:fdiv.h t6, t5, t4, dyn
	-[0x800025e8]:csrrs a2, fcsr, zero
	-[0x800025ec]:sw t6, 32(fp)
Current Store : [0x800025f0] : sw a2, 36(fp) -- Store: [0x8000c000]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002624]:fdiv.h t6, t5, t4, dyn
	-[0x80002628]:csrrs a2, fcsr, zero
	-[0x8000262c]:sw t6, 40(fp)
Current Store : [0x80002630] : sw a2, 44(fp) -- Store: [0x8000c008]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002664]:fdiv.h t6, t5, t4, dyn
	-[0x80002668]:csrrs a2, fcsr, zero
	-[0x8000266c]:sw t6, 48(fp)
Current Store : [0x80002670] : sw a2, 52(fp) -- Store: [0x8000c010]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026a4]:fdiv.h t6, t5, t4, dyn
	-[0x800026a8]:csrrs a2, fcsr, zero
	-[0x800026ac]:sw t6, 56(fp)
Current Store : [0x800026b0] : sw a2, 60(fp) -- Store: [0x8000c018]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026e4]:fdiv.h t6, t5, t4, dyn
	-[0x800026e8]:csrrs a2, fcsr, zero
	-[0x800026ec]:sw t6, 64(fp)
Current Store : [0x800026f0] : sw a2, 68(fp) -- Store: [0x8000c020]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002724]:fdiv.h t6, t5, t4, dyn
	-[0x80002728]:csrrs a2, fcsr, zero
	-[0x8000272c]:sw t6, 72(fp)
Current Store : [0x80002730] : sw a2, 76(fp) -- Store: [0x8000c028]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002764]:fdiv.h t6, t5, t4, dyn
	-[0x80002768]:csrrs a2, fcsr, zero
	-[0x8000276c]:sw t6, 80(fp)
Current Store : [0x80002770] : sw a2, 84(fp) -- Store: [0x8000c030]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027a4]:fdiv.h t6, t5, t4, dyn
	-[0x800027a8]:csrrs a2, fcsr, zero
	-[0x800027ac]:sw t6, 88(fp)
Current Store : [0x800027b0] : sw a2, 92(fp) -- Store: [0x8000c038]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027e4]:fdiv.h t6, t5, t4, dyn
	-[0x800027e8]:csrrs a2, fcsr, zero
	-[0x800027ec]:sw t6, 96(fp)
Current Store : [0x800027f0] : sw a2, 100(fp) -- Store: [0x8000c040]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002824]:fdiv.h t6, t5, t4, dyn
	-[0x80002828]:csrrs a2, fcsr, zero
	-[0x8000282c]:sw t6, 104(fp)
Current Store : [0x80002830] : sw a2, 108(fp) -- Store: [0x8000c048]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002864]:fdiv.h t6, t5, t4, dyn
	-[0x80002868]:csrrs a2, fcsr, zero
	-[0x8000286c]:sw t6, 112(fp)
Current Store : [0x80002870] : sw a2, 116(fp) -- Store: [0x8000c050]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028a4]:fdiv.h t6, t5, t4, dyn
	-[0x800028a8]:csrrs a2, fcsr, zero
	-[0x800028ac]:sw t6, 120(fp)
Current Store : [0x800028b0] : sw a2, 124(fp) -- Store: [0x8000c058]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028e4]:fdiv.h t6, t5, t4, dyn
	-[0x800028e8]:csrrs a2, fcsr, zero
	-[0x800028ec]:sw t6, 128(fp)
Current Store : [0x800028f0] : sw a2, 132(fp) -- Store: [0x8000c060]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002924]:fdiv.h t6, t5, t4, dyn
	-[0x80002928]:csrrs a2, fcsr, zero
	-[0x8000292c]:sw t6, 136(fp)
Current Store : [0x80002930] : sw a2, 140(fp) -- Store: [0x8000c068]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002964]:fdiv.h t6, t5, t4, dyn
	-[0x80002968]:csrrs a2, fcsr, zero
	-[0x8000296c]:sw t6, 144(fp)
Current Store : [0x80002970] : sw a2, 148(fp) -- Store: [0x8000c070]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029a4]:fdiv.h t6, t5, t4, dyn
	-[0x800029a8]:csrrs a2, fcsr, zero
	-[0x800029ac]:sw t6, 152(fp)
Current Store : [0x800029b0] : sw a2, 156(fp) -- Store: [0x8000c078]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029e4]:fdiv.h t6, t5, t4, dyn
	-[0x800029e8]:csrrs a2, fcsr, zero
	-[0x800029ec]:sw t6, 160(fp)
Current Store : [0x800029f0] : sw a2, 164(fp) -- Store: [0x8000c080]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a24]:fdiv.h t6, t5, t4, dyn
	-[0x80002a28]:csrrs a2, fcsr, zero
	-[0x80002a2c]:sw t6, 168(fp)
Current Store : [0x80002a30] : sw a2, 172(fp) -- Store: [0x8000c088]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a64]:fdiv.h t6, t5, t4, dyn
	-[0x80002a68]:csrrs a2, fcsr, zero
	-[0x80002a6c]:sw t6, 176(fp)
Current Store : [0x80002a70] : sw a2, 180(fp) -- Store: [0x8000c090]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:fdiv.h t6, t5, t4, dyn
	-[0x80002aa8]:csrrs a2, fcsr, zero
	-[0x80002aac]:sw t6, 184(fp)
Current Store : [0x80002ab0] : sw a2, 188(fp) -- Store: [0x8000c098]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ae4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ae8]:csrrs a2, fcsr, zero
	-[0x80002aec]:sw t6, 192(fp)
Current Store : [0x80002af0] : sw a2, 196(fp) -- Store: [0x8000c0a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b24]:fdiv.h t6, t5, t4, dyn
	-[0x80002b28]:csrrs a2, fcsr, zero
	-[0x80002b2c]:sw t6, 200(fp)
Current Store : [0x80002b30] : sw a2, 204(fp) -- Store: [0x8000c0a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b64]:fdiv.h t6, t5, t4, dyn
	-[0x80002b68]:csrrs a2, fcsr, zero
	-[0x80002b6c]:sw t6, 208(fp)
Current Store : [0x80002b70] : sw a2, 212(fp) -- Store: [0x8000c0b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ba4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ba8]:csrrs a2, fcsr, zero
	-[0x80002bac]:sw t6, 216(fp)
Current Store : [0x80002bb0] : sw a2, 220(fp) -- Store: [0x8000c0b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002be4]:fdiv.h t6, t5, t4, dyn
	-[0x80002be8]:csrrs a2, fcsr, zero
	-[0x80002bec]:sw t6, 224(fp)
Current Store : [0x80002bf0] : sw a2, 228(fp) -- Store: [0x8000c0c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c24]:fdiv.h t6, t5, t4, dyn
	-[0x80002c28]:csrrs a2, fcsr, zero
	-[0x80002c2c]:sw t6, 232(fp)
Current Store : [0x80002c30] : sw a2, 236(fp) -- Store: [0x8000c0c8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c64]:fdiv.h t6, t5, t4, dyn
	-[0x80002c68]:csrrs a2, fcsr, zero
	-[0x80002c6c]:sw t6, 240(fp)
Current Store : [0x80002c70] : sw a2, 244(fp) -- Store: [0x8000c0d0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ca4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ca8]:csrrs a2, fcsr, zero
	-[0x80002cac]:sw t6, 248(fp)
Current Store : [0x80002cb0] : sw a2, 252(fp) -- Store: [0x8000c0d8]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ce4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ce8]:csrrs a2, fcsr, zero
	-[0x80002cec]:sw t6, 256(fp)
Current Store : [0x80002cf0] : sw a2, 260(fp) -- Store: [0x8000c0e0]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d24]:fdiv.h t6, t5, t4, dyn
	-[0x80002d28]:csrrs a2, fcsr, zero
	-[0x80002d2c]:sw t6, 264(fp)
Current Store : [0x80002d30] : sw a2, 268(fp) -- Store: [0x8000c0e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d64]:fdiv.h t6, t5, t4, dyn
	-[0x80002d68]:csrrs a2, fcsr, zero
	-[0x80002d6c]:sw t6, 272(fp)
Current Store : [0x80002d70] : sw a2, 276(fp) -- Store: [0x8000c0f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002da4]:fdiv.h t6, t5, t4, dyn
	-[0x80002da8]:csrrs a2, fcsr, zero
	-[0x80002dac]:sw t6, 280(fp)
Current Store : [0x80002db0] : sw a2, 284(fp) -- Store: [0x8000c0f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002de4]:fdiv.h t6, t5, t4, dyn
	-[0x80002de8]:csrrs a2, fcsr, zero
	-[0x80002dec]:sw t6, 288(fp)
Current Store : [0x80002df0] : sw a2, 292(fp) -- Store: [0x8000c100]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e24]:fdiv.h t6, t5, t4, dyn
	-[0x80002e28]:csrrs a2, fcsr, zero
	-[0x80002e2c]:sw t6, 296(fp)
Current Store : [0x80002e30] : sw a2, 300(fp) -- Store: [0x8000c108]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e64]:fdiv.h t6, t5, t4, dyn
	-[0x80002e68]:csrrs a2, fcsr, zero
	-[0x80002e6c]:sw t6, 304(fp)
Current Store : [0x80002e70] : sw a2, 308(fp) -- Store: [0x8000c110]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ea4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ea8]:csrrs a2, fcsr, zero
	-[0x80002eac]:sw t6, 312(fp)
Current Store : [0x80002eb0] : sw a2, 316(fp) -- Store: [0x8000c118]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ee4]:fdiv.h t6, t5, t4, dyn
	-[0x80002ee8]:csrrs a2, fcsr, zero
	-[0x80002eec]:sw t6, 320(fp)
Current Store : [0x80002ef0] : sw a2, 324(fp) -- Store: [0x8000c120]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f24]:fdiv.h t6, t5, t4, dyn
	-[0x80002f28]:csrrs a2, fcsr, zero
	-[0x80002f2c]:sw t6, 328(fp)
Current Store : [0x80002f30] : sw a2, 332(fp) -- Store: [0x8000c128]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f64]:fdiv.h t6, t5, t4, dyn
	-[0x80002f68]:csrrs a2, fcsr, zero
	-[0x80002f6c]:sw t6, 336(fp)
Current Store : [0x80002f70] : sw a2, 340(fp) -- Store: [0x8000c130]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fa4]:fdiv.h t6, t5, t4, dyn
	-[0x80002fa8]:csrrs a2, fcsr, zero
	-[0x80002fac]:sw t6, 344(fp)
Current Store : [0x80002fb0] : sw a2, 348(fp) -- Store: [0x8000c138]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fe4]:fdiv.h t6, t5, t4, dyn
	-[0x80002fe8]:csrrs a2, fcsr, zero
	-[0x80002fec]:sw t6, 352(fp)
Current Store : [0x80002ff0] : sw a2, 356(fp) -- Store: [0x8000c140]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003024]:fdiv.h t6, t5, t4, dyn
	-[0x80003028]:csrrs a2, fcsr, zero
	-[0x8000302c]:sw t6, 360(fp)
Current Store : [0x80003030] : sw a2, 364(fp) -- Store: [0x8000c148]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003064]:fdiv.h t6, t5, t4, dyn
	-[0x80003068]:csrrs a2, fcsr, zero
	-[0x8000306c]:sw t6, 368(fp)
Current Store : [0x80003070] : sw a2, 372(fp) -- Store: [0x8000c150]:0x00000003




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030a4]:fdiv.h t6, t5, t4, dyn
	-[0x800030a8]:csrrs a2, fcsr, zero
	-[0x800030ac]:sw t6, 376(fp)
Current Store : [0x800030b0] : sw a2, 380(fp) -- Store: [0x8000c158]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030e4]:fdiv.h t6, t5, t4, dyn
	-[0x800030e8]:csrrs a2, fcsr, zero
	-[0x800030ec]:sw t6, 384(fp)
Current Store : [0x800030f0] : sw a2, 388(fp) -- Store: [0x8000c160]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003124]:fdiv.h t6, t5, t4, dyn
	-[0x80003128]:csrrs a2, fcsr, zero
	-[0x8000312c]:sw t6, 392(fp)
Current Store : [0x80003130] : sw a2, 396(fp) -- Store: [0x8000c168]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003164]:fdiv.h t6, t5, t4, dyn
	-[0x80003168]:csrrs a2, fcsr, zero
	-[0x8000316c]:sw t6, 400(fp)
Current Store : [0x80003170] : sw a2, 404(fp) -- Store: [0x8000c170]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031a4]:fdiv.h t6, t5, t4, dyn
	-[0x800031a8]:csrrs a2, fcsr, zero
	-[0x800031ac]:sw t6, 408(fp)
Current Store : [0x800031b0] : sw a2, 412(fp) -- Store: [0x8000c178]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031e4]:fdiv.h t6, t5, t4, dyn
	-[0x800031e8]:csrrs a2, fcsr, zero
	-[0x800031ec]:sw t6, 416(fp)
Current Store : [0x800031f0] : sw a2, 420(fp) -- Store: [0x8000c180]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003224]:fdiv.h t6, t5, t4, dyn
	-[0x80003228]:csrrs a2, fcsr, zero
	-[0x8000322c]:sw t6, 424(fp)
Current Store : [0x80003230] : sw a2, 428(fp) -- Store: [0x8000c188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003264]:fdiv.h t6, t5, t4, dyn
	-[0x80003268]:csrrs a2, fcsr, zero
	-[0x8000326c]:sw t6, 432(fp)
Current Store : [0x80003270] : sw a2, 436(fp) -- Store: [0x8000c190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032a4]:fdiv.h t6, t5, t4, dyn
	-[0x800032a8]:csrrs a2, fcsr, zero
	-[0x800032ac]:sw t6, 440(fp)
Current Store : [0x800032b0] : sw a2, 444(fp) -- Store: [0x8000c198]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032e4]:fdiv.h t6, t5, t4, dyn
	-[0x800032e8]:csrrs a2, fcsr, zero
	-[0x800032ec]:sw t6, 448(fp)
Current Store : [0x800032f0] : sw a2, 452(fp) -- Store: [0x8000c1a0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003324]:fdiv.h t6, t5, t4, dyn
	-[0x80003328]:csrrs a2, fcsr, zero
	-[0x8000332c]:sw t6, 456(fp)
Current Store : [0x80003330] : sw a2, 460(fp) -- Store: [0x8000c1a8]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003364]:fdiv.h t6, t5, t4, dyn
	-[0x80003368]:csrrs a2, fcsr, zero
	-[0x8000336c]:sw t6, 464(fp)
Current Store : [0x80003370] : sw a2, 468(fp) -- Store: [0x8000c1b0]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033a4]:fdiv.h t6, t5, t4, dyn
	-[0x800033a8]:csrrs a2, fcsr, zero
	-[0x800033ac]:sw t6, 472(fp)
Current Store : [0x800033b0] : sw a2, 476(fp) -- Store: [0x8000c1b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033e4]:fdiv.h t6, t5, t4, dyn
	-[0x800033e8]:csrrs a2, fcsr, zero
	-[0x800033ec]:sw t6, 480(fp)
Current Store : [0x800033f0] : sw a2, 484(fp) -- Store: [0x8000c1c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003424]:fdiv.h t6, t5, t4, dyn
	-[0x80003428]:csrrs a2, fcsr, zero
	-[0x8000342c]:sw t6, 488(fp)
Current Store : [0x80003430] : sw a2, 492(fp) -- Store: [0x8000c1c8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003464]:fdiv.h t6, t5, t4, dyn
	-[0x80003468]:csrrs a2, fcsr, zero
	-[0x8000346c]:sw t6, 496(fp)
Current Store : [0x80003470] : sw a2, 500(fp) -- Store: [0x8000c1d0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034a4]:fdiv.h t6, t5, t4, dyn
	-[0x800034a8]:csrrs a2, fcsr, zero
	-[0x800034ac]:sw t6, 504(fp)
Current Store : [0x800034b0] : sw a2, 508(fp) -- Store: [0x8000c1d8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034e4]:fdiv.h t6, t5, t4, dyn
	-[0x800034e8]:csrrs a2, fcsr, zero
	-[0x800034ec]:sw t6, 512(fp)
Current Store : [0x800034f0] : sw a2, 516(fp) -- Store: [0x8000c1e0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003524]:fdiv.h t6, t5, t4, dyn
	-[0x80003528]:csrrs a2, fcsr, zero
	-[0x8000352c]:sw t6, 520(fp)
Current Store : [0x80003530] : sw a2, 524(fp) -- Store: [0x8000c1e8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003564]:fdiv.h t6, t5, t4, dyn
	-[0x80003568]:csrrs a2, fcsr, zero
	-[0x8000356c]:sw t6, 528(fp)
Current Store : [0x80003570] : sw a2, 532(fp) -- Store: [0x8000c1f0]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035a4]:fdiv.h t6, t5, t4, dyn
	-[0x800035a8]:csrrs a2, fcsr, zero
	-[0x800035ac]:sw t6, 536(fp)
Current Store : [0x800035b0] : sw a2, 540(fp) -- Store: [0x8000c1f8]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035e4]:fdiv.h t6, t5, t4, dyn
	-[0x800035e8]:csrrs a2, fcsr, zero
	-[0x800035ec]:sw t6, 544(fp)
Current Store : [0x800035f0] : sw a2, 548(fp) -- Store: [0x8000c200]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003624]:fdiv.h t6, t5, t4, dyn
	-[0x80003628]:csrrs a2, fcsr, zero
	-[0x8000362c]:sw t6, 552(fp)
Current Store : [0x80003630] : sw a2, 556(fp) -- Store: [0x8000c208]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003664]:fdiv.h t6, t5, t4, dyn
	-[0x80003668]:csrrs a2, fcsr, zero
	-[0x8000366c]:sw t6, 560(fp)
Current Store : [0x80003670] : sw a2, 564(fp) -- Store: [0x8000c210]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036a4]:fdiv.h t6, t5, t4, dyn
	-[0x800036a8]:csrrs a2, fcsr, zero
	-[0x800036ac]:sw t6, 568(fp)
Current Store : [0x800036b0] : sw a2, 572(fp) -- Store: [0x8000c218]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036e4]:fdiv.h t6, t5, t4, dyn
	-[0x800036e8]:csrrs a2, fcsr, zero
	-[0x800036ec]:sw t6, 576(fp)
Current Store : [0x800036f0] : sw a2, 580(fp) -- Store: [0x8000c220]:0x00000003




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003724]:fdiv.h t6, t5, t4, dyn
	-[0x80003728]:csrrs a2, fcsr, zero
	-[0x8000372c]:sw t6, 584(fp)
Current Store : [0x80003730] : sw a2, 588(fp) -- Store: [0x8000c228]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003764]:fdiv.h t6, t5, t4, dyn
	-[0x80003768]:csrrs a2, fcsr, zero
	-[0x8000376c]:sw t6, 592(fp)
Current Store : [0x80003770] : sw a2, 596(fp) -- Store: [0x8000c230]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037a4]:fdiv.h t6, t5, t4, dyn
	-[0x800037a8]:csrrs a2, fcsr, zero
	-[0x800037ac]:sw t6, 600(fp)
Current Store : [0x800037b0] : sw a2, 604(fp) -- Store: [0x8000c238]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037e4]:fdiv.h t6, t5, t4, dyn
	-[0x800037e8]:csrrs a2, fcsr, zero
	-[0x800037ec]:sw t6, 608(fp)
Current Store : [0x800037f0] : sw a2, 612(fp) -- Store: [0x8000c240]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003824]:fdiv.h t6, t5, t4, dyn
	-[0x80003828]:csrrs a2, fcsr, zero
	-[0x8000382c]:sw t6, 616(fp)
Current Store : [0x80003830] : sw a2, 620(fp) -- Store: [0x8000c248]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003864]:fdiv.h t6, t5, t4, dyn
	-[0x80003868]:csrrs a2, fcsr, zero
	-[0x8000386c]:sw t6, 624(fp)
Current Store : [0x80003870] : sw a2, 628(fp) -- Store: [0x8000c250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038a4]:fdiv.h t6, t5, t4, dyn
	-[0x800038a8]:csrrs a2, fcsr, zero
	-[0x800038ac]:sw t6, 632(fp)
Current Store : [0x800038b0] : sw a2, 636(fp) -- Store: [0x8000c258]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038e4]:fdiv.h t6, t5, t4, dyn
	-[0x800038e8]:csrrs a2, fcsr, zero
	-[0x800038ec]:sw t6, 640(fp)
Current Store : [0x800038f0] : sw a2, 644(fp) -- Store: [0x8000c260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003924]:fdiv.h t6, t5, t4, dyn
	-[0x80003928]:csrrs a2, fcsr, zero
	-[0x8000392c]:sw t6, 648(fp)
Current Store : [0x80003930] : sw a2, 652(fp) -- Store: [0x8000c268]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003964]:fdiv.h t6, t5, t4, dyn
	-[0x80003968]:csrrs a2, fcsr, zero
	-[0x8000396c]:sw t6, 656(fp)
Current Store : [0x80003970] : sw a2, 660(fp) -- Store: [0x8000c270]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039a4]:fdiv.h t6, t5, t4, dyn
	-[0x800039a8]:csrrs a2, fcsr, zero
	-[0x800039ac]:sw t6, 664(fp)
Current Store : [0x800039b0] : sw a2, 668(fp) -- Store: [0x8000c278]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039e4]:fdiv.h t6, t5, t4, dyn
	-[0x800039e8]:csrrs a2, fcsr, zero
	-[0x800039ec]:sw t6, 672(fp)
Current Store : [0x800039f0] : sw a2, 676(fp) -- Store: [0x8000c280]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a24]:fdiv.h t6, t5, t4, dyn
	-[0x80003a28]:csrrs a2, fcsr, zero
	-[0x80003a2c]:sw t6, 680(fp)
Current Store : [0x80003a30] : sw a2, 684(fp) -- Store: [0x8000c288]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a64]:fdiv.h t6, t5, t4, dyn
	-[0x80003a68]:csrrs a2, fcsr, zero
	-[0x80003a6c]:sw t6, 688(fp)
Current Store : [0x80003a70] : sw a2, 692(fp) -- Store: [0x8000c290]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003aa4]:fdiv.h t6, t5, t4, dyn
	-[0x80003aa8]:csrrs a2, fcsr, zero
	-[0x80003aac]:sw t6, 696(fp)
Current Store : [0x80003ab0] : sw a2, 700(fp) -- Store: [0x8000c298]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ae4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ae8]:csrrs a2, fcsr, zero
	-[0x80003aec]:sw t6, 704(fp)
Current Store : [0x80003af0] : sw a2, 708(fp) -- Store: [0x8000c2a0]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b24]:fdiv.h t6, t5, t4, dyn
	-[0x80003b28]:csrrs a2, fcsr, zero
	-[0x80003b2c]:sw t6, 712(fp)
Current Store : [0x80003b30] : sw a2, 716(fp) -- Store: [0x8000c2a8]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b64]:fdiv.h t6, t5, t4, dyn
	-[0x80003b68]:csrrs a2, fcsr, zero
	-[0x80003b6c]:sw t6, 720(fp)
Current Store : [0x80003b70] : sw a2, 724(fp) -- Store: [0x8000c2b0]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ba4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ba8]:csrrs a2, fcsr, zero
	-[0x80003bac]:sw t6, 728(fp)
Current Store : [0x80003bb0] : sw a2, 732(fp) -- Store: [0x8000c2b8]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003be4]:fdiv.h t6, t5, t4, dyn
	-[0x80003be8]:csrrs a2, fcsr, zero
	-[0x80003bec]:sw t6, 736(fp)
Current Store : [0x80003bf0] : sw a2, 740(fp) -- Store: [0x8000c2c0]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c24]:fdiv.h t6, t5, t4, dyn
	-[0x80003c28]:csrrs a2, fcsr, zero
	-[0x80003c2c]:sw t6, 744(fp)
Current Store : [0x80003c30] : sw a2, 748(fp) -- Store: [0x8000c2c8]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c64]:fdiv.h t6, t5, t4, dyn
	-[0x80003c68]:csrrs a2, fcsr, zero
	-[0x80003c6c]:sw t6, 752(fp)
Current Store : [0x80003c70] : sw a2, 756(fp) -- Store: [0x8000c2d0]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ca4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ca8]:csrrs a2, fcsr, zero
	-[0x80003cac]:sw t6, 760(fp)
Current Store : [0x80003cb0] : sw a2, 764(fp) -- Store: [0x8000c2d8]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ce4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ce8]:csrrs a2, fcsr, zero
	-[0x80003cec]:sw t6, 768(fp)
Current Store : [0x80003cf0] : sw a2, 772(fp) -- Store: [0x8000c2e0]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d24]:fdiv.h t6, t5, t4, dyn
	-[0x80003d28]:csrrs a2, fcsr, zero
	-[0x80003d2c]:sw t6, 776(fp)
Current Store : [0x80003d30] : sw a2, 780(fp) -- Store: [0x8000c2e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d64]:fdiv.h t6, t5, t4, dyn
	-[0x80003d68]:csrrs a2, fcsr, zero
	-[0x80003d6c]:sw t6, 784(fp)
Current Store : [0x80003d70] : sw a2, 788(fp) -- Store: [0x8000c2f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003da4]:fdiv.h t6, t5, t4, dyn
	-[0x80003da8]:csrrs a2, fcsr, zero
	-[0x80003dac]:sw t6, 792(fp)
Current Store : [0x80003db0] : sw a2, 796(fp) -- Store: [0x8000c2f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003de4]:fdiv.h t6, t5, t4, dyn
	-[0x80003de8]:csrrs a2, fcsr, zero
	-[0x80003dec]:sw t6, 800(fp)
Current Store : [0x80003df0] : sw a2, 804(fp) -- Store: [0x8000c300]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e24]:fdiv.h t6, t5, t4, dyn
	-[0x80003e28]:csrrs a2, fcsr, zero
	-[0x80003e2c]:sw t6, 808(fp)
Current Store : [0x80003e30] : sw a2, 812(fp) -- Store: [0x8000c308]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e64]:fdiv.h t6, t5, t4, dyn
	-[0x80003e68]:csrrs a2, fcsr, zero
	-[0x80003e6c]:sw t6, 816(fp)
Current Store : [0x80003e70] : sw a2, 820(fp) -- Store: [0x8000c310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ea4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ea8]:csrrs a2, fcsr, zero
	-[0x80003eac]:sw t6, 824(fp)
Current Store : [0x80003eb0] : sw a2, 828(fp) -- Store: [0x8000c318]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ee4]:fdiv.h t6, t5, t4, dyn
	-[0x80003ee8]:csrrs a2, fcsr, zero
	-[0x80003eec]:sw t6, 832(fp)
Current Store : [0x80003ef0] : sw a2, 836(fp) -- Store: [0x8000c320]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f24]:fdiv.h t6, t5, t4, dyn
	-[0x80003f28]:csrrs a2, fcsr, zero
	-[0x80003f2c]:sw t6, 840(fp)
Current Store : [0x80003f30] : sw a2, 844(fp) -- Store: [0x8000c328]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f64]:fdiv.h t6, t5, t4, dyn
	-[0x80003f68]:csrrs a2, fcsr, zero
	-[0x80003f6c]:sw t6, 848(fp)
Current Store : [0x80003f70] : sw a2, 852(fp) -- Store: [0x8000c330]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fa4]:fdiv.h t6, t5, t4, dyn
	-[0x80003fa8]:csrrs a2, fcsr, zero
	-[0x80003fac]:sw t6, 856(fp)
Current Store : [0x80003fb0] : sw a2, 860(fp) -- Store: [0x8000c338]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fe4]:fdiv.h t6, t5, t4, dyn
	-[0x80003fe8]:csrrs a2, fcsr, zero
	-[0x80003fec]:sw t6, 864(fp)
Current Store : [0x80003ff0] : sw a2, 868(fp) -- Store: [0x8000c340]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004024]:fdiv.h t6, t5, t4, dyn
	-[0x80004028]:csrrs a2, fcsr, zero
	-[0x8000402c]:sw t6, 872(fp)
Current Store : [0x80004030] : sw a2, 876(fp) -- Store: [0x8000c348]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004064]:fdiv.h t6, t5, t4, dyn
	-[0x80004068]:csrrs a2, fcsr, zero
	-[0x8000406c]:sw t6, 880(fp)
Current Store : [0x80004070] : sw a2, 884(fp) -- Store: [0x8000c350]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040a4]:fdiv.h t6, t5, t4, dyn
	-[0x800040a8]:csrrs a2, fcsr, zero
	-[0x800040ac]:sw t6, 888(fp)
Current Store : [0x800040b0] : sw a2, 892(fp) -- Store: [0x8000c358]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040e4]:fdiv.h t6, t5, t4, dyn
	-[0x800040e8]:csrrs a2, fcsr, zero
	-[0x800040ec]:sw t6, 896(fp)
Current Store : [0x800040f0] : sw a2, 900(fp) -- Store: [0x8000c360]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004124]:fdiv.h t6, t5, t4, dyn
	-[0x80004128]:csrrs a2, fcsr, zero
	-[0x8000412c]:sw t6, 904(fp)
Current Store : [0x80004130] : sw a2, 908(fp) -- Store: [0x8000c368]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004164]:fdiv.h t6, t5, t4, dyn
	-[0x80004168]:csrrs a2, fcsr, zero
	-[0x8000416c]:sw t6, 912(fp)
Current Store : [0x80004170] : sw a2, 916(fp) -- Store: [0x8000c370]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041a4]:fdiv.h t6, t5, t4, dyn
	-[0x800041a8]:csrrs a2, fcsr, zero
	-[0x800041ac]:sw t6, 920(fp)
Current Store : [0x800041b0] : sw a2, 924(fp) -- Store: [0x8000c378]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041e4]:fdiv.h t6, t5, t4, dyn
	-[0x800041e8]:csrrs a2, fcsr, zero
	-[0x800041ec]:sw t6, 928(fp)
Current Store : [0x800041f0] : sw a2, 932(fp) -- Store: [0x8000c380]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004224]:fdiv.h t6, t5, t4, dyn
	-[0x80004228]:csrrs a2, fcsr, zero
	-[0x8000422c]:sw t6, 936(fp)
Current Store : [0x80004230] : sw a2, 940(fp) -- Store: [0x8000c388]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004264]:fdiv.h t6, t5, t4, dyn
	-[0x80004268]:csrrs a2, fcsr, zero
	-[0x8000426c]:sw t6, 944(fp)
Current Store : [0x80004270] : sw a2, 948(fp) -- Store: [0x8000c390]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042a4]:fdiv.h t6, t5, t4, dyn
	-[0x800042a8]:csrrs a2, fcsr, zero
	-[0x800042ac]:sw t6, 952(fp)
Current Store : [0x800042b0] : sw a2, 956(fp) -- Store: [0x8000c398]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042e4]:fdiv.h t6, t5, t4, dyn
	-[0x800042e8]:csrrs a2, fcsr, zero
	-[0x800042ec]:sw t6, 960(fp)
Current Store : [0x800042f0] : sw a2, 964(fp) -- Store: [0x8000c3a0]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004324]:fdiv.h t6, t5, t4, dyn
	-[0x80004328]:csrrs a2, fcsr, zero
	-[0x8000432c]:sw t6, 968(fp)
Current Store : [0x80004330] : sw a2, 972(fp) -- Store: [0x8000c3a8]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004364]:fdiv.h t6, t5, t4, dyn
	-[0x80004368]:csrrs a2, fcsr, zero
	-[0x8000436c]:sw t6, 976(fp)
Current Store : [0x80004370] : sw a2, 980(fp) -- Store: [0x8000c3b0]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043a4]:fdiv.h t6, t5, t4, dyn
	-[0x800043a8]:csrrs a2, fcsr, zero
	-[0x800043ac]:sw t6, 984(fp)
Current Store : [0x800043b0] : sw a2, 988(fp) -- Store: [0x8000c3b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043e4]:fdiv.h t6, t5, t4, dyn
	-[0x800043e8]:csrrs a2, fcsr, zero
	-[0x800043ec]:sw t6, 992(fp)
Current Store : [0x800043f0] : sw a2, 996(fp) -- Store: [0x8000c3c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004424]:fdiv.h t6, t5, t4, dyn
	-[0x80004428]:csrrs a2, fcsr, zero
	-[0x8000442c]:sw t6, 1000(fp)
Current Store : [0x80004430] : sw a2, 1004(fp) -- Store: [0x8000c3c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004464]:fdiv.h t6, t5, t4, dyn
	-[0x80004468]:csrrs a2, fcsr, zero
	-[0x8000446c]:sw t6, 1008(fp)
Current Store : [0x80004470] : sw a2, 1012(fp) -- Store: [0x8000c3d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044a4]:fdiv.h t6, t5, t4, dyn
	-[0x800044a8]:csrrs a2, fcsr, zero
	-[0x800044ac]:sw t6, 1016(fp)
Current Store : [0x800044b0] : sw a2, 1020(fp) -- Store: [0x8000c3d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044ec]:fdiv.h t6, t5, t4, dyn
	-[0x800044f0]:csrrs a2, fcsr, zero
	-[0x800044f4]:sw t6, 0(fp)
Current Store : [0x800044f8] : sw a2, 4(fp) -- Store: [0x8000c3e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000452c]:fdiv.h t6, t5, t4, dyn
	-[0x80004530]:csrrs a2, fcsr, zero
	-[0x80004534]:sw t6, 8(fp)
Current Store : [0x80004538] : sw a2, 12(fp) -- Store: [0x8000c3e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000456c]:fdiv.h t6, t5, t4, dyn
	-[0x80004570]:csrrs a2, fcsr, zero
	-[0x80004574]:sw t6, 16(fp)
Current Store : [0x80004578] : sw a2, 20(fp) -- Store: [0x8000c3f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045ac]:fdiv.h t6, t5, t4, dyn
	-[0x800045b0]:csrrs a2, fcsr, zero
	-[0x800045b4]:sw t6, 24(fp)
Current Store : [0x800045b8] : sw a2, 28(fp) -- Store: [0x8000c3f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045ec]:fdiv.h t6, t5, t4, dyn
	-[0x800045f0]:csrrs a2, fcsr, zero
	-[0x800045f4]:sw t6, 32(fp)
Current Store : [0x800045f8] : sw a2, 36(fp) -- Store: [0x8000c400]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000462c]:fdiv.h t6, t5, t4, dyn
	-[0x80004630]:csrrs a2, fcsr, zero
	-[0x80004634]:sw t6, 40(fp)
Current Store : [0x80004638] : sw a2, 44(fp) -- Store: [0x8000c408]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000466c]:fdiv.h t6, t5, t4, dyn
	-[0x80004670]:csrrs a2, fcsr, zero
	-[0x80004674]:sw t6, 48(fp)
Current Store : [0x80004678] : sw a2, 52(fp) -- Store: [0x8000c410]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046ac]:fdiv.h t6, t5, t4, dyn
	-[0x800046b0]:csrrs a2, fcsr, zero
	-[0x800046b4]:sw t6, 56(fp)
Current Store : [0x800046b8] : sw a2, 60(fp) -- Store: [0x8000c418]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046ec]:fdiv.h t6, t5, t4, dyn
	-[0x800046f0]:csrrs a2, fcsr, zero
	-[0x800046f4]:sw t6, 64(fp)
Current Store : [0x800046f8] : sw a2, 68(fp) -- Store: [0x8000c420]:0x00000008




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000472c]:fdiv.h t6, t5, t4, dyn
	-[0x80004730]:csrrs a2, fcsr, zero
	-[0x80004734]:sw t6, 72(fp)
Current Store : [0x80004738] : sw a2, 76(fp) -- Store: [0x8000c428]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000476c]:fdiv.h t6, t5, t4, dyn
	-[0x80004770]:csrrs a2, fcsr, zero
	-[0x80004774]:sw t6, 80(fp)
Current Store : [0x80004778] : sw a2, 84(fp) -- Store: [0x8000c430]:0x00000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047ac]:fdiv.h t6, t5, t4, dyn
	-[0x800047b0]:csrrs a2, fcsr, zero
	-[0x800047b4]:sw t6, 88(fp)
Current Store : [0x800047b8] : sw a2, 92(fp) -- Store: [0x8000c438]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047ec]:fdiv.h t6, t5, t4, dyn
	-[0x800047f0]:csrrs a2, fcsr, zero
	-[0x800047f4]:sw t6, 96(fp)
Current Store : [0x800047f8] : sw a2, 100(fp) -- Store: [0x8000c440]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000482c]:fdiv.h t6, t5, t4, dyn
	-[0x80004830]:csrrs a2, fcsr, zero
	-[0x80004834]:sw t6, 104(fp)
Current Store : [0x80004838] : sw a2, 108(fp) -- Store: [0x8000c448]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000486c]:fdiv.h t6, t5, t4, dyn
	-[0x80004870]:csrrs a2, fcsr, zero
	-[0x80004874]:sw t6, 112(fp)
Current Store : [0x80004878] : sw a2, 116(fp) -- Store: [0x8000c450]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048ac]:fdiv.h t6, t5, t4, dyn
	-[0x800048b0]:csrrs a2, fcsr, zero
	-[0x800048b4]:sw t6, 120(fp)
Current Store : [0x800048b8] : sw a2, 124(fp) -- Store: [0x8000c458]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048ec]:fdiv.h t6, t5, t4, dyn
	-[0x800048f0]:csrrs a2, fcsr, zero
	-[0x800048f4]:sw t6, 128(fp)
Current Store : [0x800048f8] : sw a2, 132(fp) -- Store: [0x8000c460]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000492c]:fdiv.h t6, t5, t4, dyn
	-[0x80004930]:csrrs a2, fcsr, zero
	-[0x80004934]:sw t6, 136(fp)
Current Store : [0x80004938] : sw a2, 140(fp) -- Store: [0x8000c468]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000496c]:fdiv.h t6, t5, t4, dyn
	-[0x80004970]:csrrs a2, fcsr, zero
	-[0x80004974]:sw t6, 144(fp)
Current Store : [0x80004978] : sw a2, 148(fp) -- Store: [0x8000c470]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049ac]:fdiv.h t6, t5, t4, dyn
	-[0x800049b0]:csrrs a2, fcsr, zero
	-[0x800049b4]:sw t6, 152(fp)
Current Store : [0x800049b8] : sw a2, 156(fp) -- Store: [0x8000c478]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049ec]:fdiv.h t6, t5, t4, dyn
	-[0x800049f0]:csrrs a2, fcsr, zero
	-[0x800049f4]:sw t6, 160(fp)
Current Store : [0x800049f8] : sw a2, 164(fp) -- Store: [0x8000c480]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004a30]:csrrs a2, fcsr, zero
	-[0x80004a34]:sw t6, 168(fp)
Current Store : [0x80004a38] : sw a2, 172(fp) -- Store: [0x8000c488]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004a70]:csrrs a2, fcsr, zero
	-[0x80004a74]:sw t6, 176(fp)
Current Store : [0x80004a78] : sw a2, 180(fp) -- Store: [0x8000c490]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004aac]:fdiv.h t6, t5, t4, dyn
	-[0x80004ab0]:csrrs a2, fcsr, zero
	-[0x80004ab4]:sw t6, 184(fp)
Current Store : [0x80004ab8] : sw a2, 188(fp) -- Store: [0x8000c498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004aec]:fdiv.h t6, t5, t4, dyn
	-[0x80004af0]:csrrs a2, fcsr, zero
	-[0x80004af4]:sw t6, 192(fp)
Current Store : [0x80004af8] : sw a2, 196(fp) -- Store: [0x8000c4a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004b30]:csrrs a2, fcsr, zero
	-[0x80004b34]:sw t6, 200(fp)
Current Store : [0x80004b38] : sw a2, 204(fp) -- Store: [0x8000c4a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004b70]:csrrs a2, fcsr, zero
	-[0x80004b74]:sw t6, 208(fp)
Current Store : [0x80004b78] : sw a2, 212(fp) -- Store: [0x8000c4b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bac]:fdiv.h t6, t5, t4, dyn
	-[0x80004bb0]:csrrs a2, fcsr, zero
	-[0x80004bb4]:sw t6, 216(fp)
Current Store : [0x80004bb8] : sw a2, 220(fp) -- Store: [0x8000c4b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bec]:fdiv.h t6, t5, t4, dyn
	-[0x80004bf0]:csrrs a2, fcsr, zero
	-[0x80004bf4]:sw t6, 224(fp)
Current Store : [0x80004bf8] : sw a2, 228(fp) -- Store: [0x8000c4c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004c30]:csrrs a2, fcsr, zero
	-[0x80004c34]:sw t6, 232(fp)
Current Store : [0x80004c38] : sw a2, 236(fp) -- Store: [0x8000c4c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004c70]:csrrs a2, fcsr, zero
	-[0x80004c74]:sw t6, 240(fp)
Current Store : [0x80004c78] : sw a2, 244(fp) -- Store: [0x8000c4d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004cac]:fdiv.h t6, t5, t4, dyn
	-[0x80004cb0]:csrrs a2, fcsr, zero
	-[0x80004cb4]:sw t6, 248(fp)
Current Store : [0x80004cb8] : sw a2, 252(fp) -- Store: [0x8000c4d8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004cec]:fdiv.h t6, t5, t4, dyn
	-[0x80004cf0]:csrrs a2, fcsr, zero
	-[0x80004cf4]:sw t6, 256(fp)
Current Store : [0x80004cf8] : sw a2, 260(fp) -- Store: [0x8000c4e0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004d30]:csrrs a2, fcsr, zero
	-[0x80004d34]:sw t6, 264(fp)
Current Store : [0x80004d38] : sw a2, 268(fp) -- Store: [0x8000c4e8]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004d70]:csrrs a2, fcsr, zero
	-[0x80004d74]:sw t6, 272(fp)
Current Store : [0x80004d78] : sw a2, 276(fp) -- Store: [0x8000c4f0]:0x00000008




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004dac]:fdiv.h t6, t5, t4, dyn
	-[0x80004db0]:csrrs a2, fcsr, zero
	-[0x80004db4]:sw t6, 280(fp)
Current Store : [0x80004db8] : sw a2, 284(fp) -- Store: [0x8000c4f8]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004dec]:fdiv.h t6, t5, t4, dyn
	-[0x80004df0]:csrrs a2, fcsr, zero
	-[0x80004df4]:sw t6, 288(fp)
Current Store : [0x80004df8] : sw a2, 292(fp) -- Store: [0x8000c500]:0x00000005




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004e30]:csrrs a2, fcsr, zero
	-[0x80004e34]:sw t6, 296(fp)
Current Store : [0x80004e38] : sw a2, 300(fp) -- Store: [0x8000c508]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004e70]:csrrs a2, fcsr, zero
	-[0x80004e74]:sw t6, 304(fp)
Current Store : [0x80004e78] : sw a2, 308(fp) -- Store: [0x8000c510]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004eac]:fdiv.h t6, t5, t4, dyn
	-[0x80004eb0]:csrrs a2, fcsr, zero
	-[0x80004eb4]:sw t6, 312(fp)
Current Store : [0x80004eb8] : sw a2, 316(fp) -- Store: [0x8000c518]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004eec]:fdiv.h t6, t5, t4, dyn
	-[0x80004ef0]:csrrs a2, fcsr, zero
	-[0x80004ef4]:sw t6, 320(fp)
Current Store : [0x80004ef8] : sw a2, 324(fp) -- Store: [0x8000c520]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f2c]:fdiv.h t6, t5, t4, dyn
	-[0x80004f30]:csrrs a2, fcsr, zero
	-[0x80004f34]:sw t6, 328(fp)
Current Store : [0x80004f38] : sw a2, 332(fp) -- Store: [0x8000c528]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fdiv.h t6, t5, t4, dyn
	-[0x80004f70]:csrrs a2, fcsr, zero
	-[0x80004f74]:sw t6, 336(fp)
Current Store : [0x80004f78] : sw a2, 340(fp) -- Store: [0x8000c530]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fac]:fdiv.h t6, t5, t4, dyn
	-[0x80004fb0]:csrrs a2, fcsr, zero
	-[0x80004fb4]:sw t6, 344(fp)
Current Store : [0x80004fb8] : sw a2, 348(fp) -- Store: [0x8000c538]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fec]:fdiv.h t6, t5, t4, dyn
	-[0x80004ff0]:csrrs a2, fcsr, zero
	-[0x80004ff4]:sw t6, 352(fp)
Current Store : [0x80004ff8] : sw a2, 356(fp) -- Store: [0x8000c540]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000502c]:fdiv.h t6, t5, t4, dyn
	-[0x80005030]:csrrs a2, fcsr, zero
	-[0x80005034]:sw t6, 360(fp)
Current Store : [0x80005038] : sw a2, 364(fp) -- Store: [0x8000c548]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000506c]:fdiv.h t6, t5, t4, dyn
	-[0x80005070]:csrrs a2, fcsr, zero
	-[0x80005074]:sw t6, 368(fp)
Current Store : [0x80005078] : sw a2, 372(fp) -- Store: [0x8000c550]:0x00000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050ac]:fdiv.h t6, t5, t4, dyn
	-[0x800050b0]:csrrs a2, fcsr, zero
	-[0x800050b4]:sw t6, 376(fp)
Current Store : [0x800050b8] : sw a2, 380(fp) -- Store: [0x8000c558]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050ec]:fdiv.h t6, t5, t4, dyn
	-[0x800050f0]:csrrs a2, fcsr, zero
	-[0x800050f4]:sw t6, 384(fp)
Current Store : [0x800050f8] : sw a2, 388(fp) -- Store: [0x8000c560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000512c]:fdiv.h t6, t5, t4, dyn
	-[0x80005130]:csrrs a2, fcsr, zero
	-[0x80005134]:sw t6, 392(fp)
Current Store : [0x80005138] : sw a2, 396(fp) -- Store: [0x8000c568]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000516c]:fdiv.h t6, t5, t4, dyn
	-[0x80005170]:csrrs a2, fcsr, zero
	-[0x80005174]:sw t6, 400(fp)
Current Store : [0x80005178] : sw a2, 404(fp) -- Store: [0x8000c570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051ac]:fdiv.h t6, t5, t4, dyn
	-[0x800051b0]:csrrs a2, fcsr, zero
	-[0x800051b4]:sw t6, 408(fp)
Current Store : [0x800051b8] : sw a2, 412(fp) -- Store: [0x8000c578]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051ec]:fdiv.h t6, t5, t4, dyn
	-[0x800051f0]:csrrs a2, fcsr, zero
	-[0x800051f4]:sw t6, 416(fp)
Current Store : [0x800051f8] : sw a2, 420(fp) -- Store: [0x8000c580]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000522c]:fdiv.h t6, t5, t4, dyn
	-[0x80005230]:csrrs a2, fcsr, zero
	-[0x80005234]:sw t6, 424(fp)
Current Store : [0x80005238] : sw a2, 428(fp) -- Store: [0x8000c588]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000526c]:fdiv.h t6, t5, t4, dyn
	-[0x80005270]:csrrs a2, fcsr, zero
	-[0x80005274]:sw t6, 432(fp)
Current Store : [0x80005278] : sw a2, 436(fp) -- Store: [0x8000c590]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052ac]:fdiv.h t6, t5, t4, dyn
	-[0x800052b0]:csrrs a2, fcsr, zero
	-[0x800052b4]:sw t6, 440(fp)
Current Store : [0x800052b8] : sw a2, 444(fp) -- Store: [0x8000c598]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052ec]:fdiv.h t6, t5, t4, dyn
	-[0x800052f0]:csrrs a2, fcsr, zero
	-[0x800052f4]:sw t6, 448(fp)
Current Store : [0x800052f8] : sw a2, 452(fp) -- Store: [0x8000c5a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000532c]:fdiv.h t6, t5, t4, dyn
	-[0x80005330]:csrrs a2, fcsr, zero
	-[0x80005334]:sw t6, 456(fp)
Current Store : [0x80005338] : sw a2, 460(fp) -- Store: [0x8000c5a8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000536c]:fdiv.h t6, t5, t4, dyn
	-[0x80005370]:csrrs a2, fcsr, zero
	-[0x80005374]:sw t6, 464(fp)
Current Store : [0x80005378] : sw a2, 468(fp) -- Store: [0x8000c5b0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053ac]:fdiv.h t6, t5, t4, dyn
	-[0x800053b0]:csrrs a2, fcsr, zero
	-[0x800053b4]:sw t6, 472(fp)
Current Store : [0x800053b8] : sw a2, 476(fp) -- Store: [0x8000c5b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053ec]:fdiv.h t6, t5, t4, dyn
	-[0x800053f0]:csrrs a2, fcsr, zero
	-[0x800053f4]:sw t6, 480(fp)
Current Store : [0x800053f8] : sw a2, 484(fp) -- Store: [0x8000c5c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000542c]:fdiv.h t6, t5, t4, dyn
	-[0x80005430]:csrrs a2, fcsr, zero
	-[0x80005434]:sw t6, 488(fp)
Current Store : [0x80005438] : sw a2, 492(fp) -- Store: [0x8000c5c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000546c]:fdiv.h t6, t5, t4, dyn
	-[0x80005470]:csrrs a2, fcsr, zero
	-[0x80005474]:sw t6, 496(fp)
Current Store : [0x80005478] : sw a2, 500(fp) -- Store: [0x8000c5d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054ac]:fdiv.h t6, t5, t4, dyn
	-[0x800054b0]:csrrs a2, fcsr, zero
	-[0x800054b4]:sw t6, 504(fp)
Current Store : [0x800054b8] : sw a2, 508(fp) -- Store: [0x8000c5d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054ec]:fdiv.h t6, t5, t4, dyn
	-[0x800054f0]:csrrs a2, fcsr, zero
	-[0x800054f4]:sw t6, 512(fp)
Current Store : [0x800054f8] : sw a2, 516(fp) -- Store: [0x8000c5e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000552c]:fdiv.h t6, t5, t4, dyn
	-[0x80005530]:csrrs a2, fcsr, zero
	-[0x80005534]:sw t6, 520(fp)
Current Store : [0x80005538] : sw a2, 524(fp) -- Store: [0x8000c5e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000556c]:fdiv.h t6, t5, t4, dyn
	-[0x80005570]:csrrs a2, fcsr, zero
	-[0x80005574]:sw t6, 528(fp)
Current Store : [0x80005578] : sw a2, 532(fp) -- Store: [0x8000c5f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055ac]:fdiv.h t6, t5, t4, dyn
	-[0x800055b0]:csrrs a2, fcsr, zero
	-[0x800055b4]:sw t6, 536(fp)
Current Store : [0x800055b8] : sw a2, 540(fp) -- Store: [0x8000c5f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055ec]:fdiv.h t6, t5, t4, dyn
	-[0x800055f0]:csrrs a2, fcsr, zero
	-[0x800055f4]:sw t6, 544(fp)
Current Store : [0x800055f8] : sw a2, 548(fp) -- Store: [0x8000c600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000562c]:fdiv.h t6, t5, t4, dyn
	-[0x80005630]:csrrs a2, fcsr, zero
	-[0x80005634]:sw t6, 552(fp)
Current Store : [0x80005638] : sw a2, 556(fp) -- Store: [0x8000c608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000566c]:fdiv.h t6, t5, t4, dyn
	-[0x80005670]:csrrs a2, fcsr, zero
	-[0x80005674]:sw t6, 560(fp)
Current Store : [0x80005678] : sw a2, 564(fp) -- Store: [0x8000c610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056ac]:fdiv.h t6, t5, t4, dyn
	-[0x800056b0]:csrrs a2, fcsr, zero
	-[0x800056b4]:sw t6, 568(fp)
Current Store : [0x800056b8] : sw a2, 572(fp) -- Store: [0x8000c618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056ec]:fdiv.h t6, t5, t4, dyn
	-[0x800056f0]:csrrs a2, fcsr, zero
	-[0x800056f4]:sw t6, 576(fp)
Current Store : [0x800056f8] : sw a2, 580(fp) -- Store: [0x8000c620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000572c]:fdiv.h t6, t5, t4, dyn
	-[0x80005730]:csrrs a2, fcsr, zero
	-[0x80005734]:sw t6, 584(fp)
Current Store : [0x80005738] : sw a2, 588(fp) -- Store: [0x8000c628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000576c]:fdiv.h t6, t5, t4, dyn
	-[0x80005770]:csrrs a2, fcsr, zero
	-[0x80005774]:sw t6, 592(fp)
Current Store : [0x80005778] : sw a2, 596(fp) -- Store: [0x8000c630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057ac]:fdiv.h t6, t5, t4, dyn
	-[0x800057b0]:csrrs a2, fcsr, zero
	-[0x800057b4]:sw t6, 600(fp)
Current Store : [0x800057b8] : sw a2, 604(fp) -- Store: [0x8000c638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057ec]:fdiv.h t6, t5, t4, dyn
	-[0x800057f0]:csrrs a2, fcsr, zero
	-[0x800057f4]:sw t6, 608(fp)
Current Store : [0x800057f8] : sw a2, 612(fp) -- Store: [0x8000c640]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000582c]:fdiv.h t6, t5, t4, dyn
	-[0x80005830]:csrrs a2, fcsr, zero
	-[0x80005834]:sw t6, 616(fp)
Current Store : [0x80005838] : sw a2, 620(fp) -- Store: [0x8000c648]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000586c]:fdiv.h t6, t5, t4, dyn
	-[0x80005870]:csrrs a2, fcsr, zero
	-[0x80005874]:sw t6, 624(fp)
Current Store : [0x80005878] : sw a2, 628(fp) -- Store: [0x8000c650]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058ac]:fdiv.h t6, t5, t4, dyn
	-[0x800058b0]:csrrs a2, fcsr, zero
	-[0x800058b4]:sw t6, 632(fp)
Current Store : [0x800058b8] : sw a2, 636(fp) -- Store: [0x8000c658]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058ec]:fdiv.h t6, t5, t4, dyn
	-[0x800058f0]:csrrs a2, fcsr, zero
	-[0x800058f4]:sw t6, 640(fp)
Current Store : [0x800058f8] : sw a2, 644(fp) -- Store: [0x8000c660]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000592c]:fdiv.h t6, t5, t4, dyn
	-[0x80005930]:csrrs a2, fcsr, zero
	-[0x80005934]:sw t6, 648(fp)
Current Store : [0x80005938] : sw a2, 652(fp) -- Store: [0x8000c668]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000596c]:fdiv.h t6, t5, t4, dyn
	-[0x80005970]:csrrs a2, fcsr, zero
	-[0x80005974]:sw t6, 656(fp)
Current Store : [0x80005978] : sw a2, 660(fp) -- Store: [0x8000c670]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059ac]:fdiv.h t6, t5, t4, dyn
	-[0x800059b0]:csrrs a2, fcsr, zero
	-[0x800059b4]:sw t6, 664(fp)
Current Store : [0x800059b8] : sw a2, 668(fp) -- Store: [0x8000c678]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059ec]:fdiv.h t6, t5, t4, dyn
	-[0x800059f0]:csrrs a2, fcsr, zero
	-[0x800059f4]:sw t6, 672(fp)
Current Store : [0x800059f8] : sw a2, 676(fp) -- Store: [0x8000c680]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005a30]:csrrs a2, fcsr, zero
	-[0x80005a34]:sw t6, 680(fp)
Current Store : [0x80005a38] : sw a2, 684(fp) -- Store: [0x8000c688]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005a70]:csrrs a2, fcsr, zero
	-[0x80005a74]:sw t6, 688(fp)
Current Store : [0x80005a78] : sw a2, 692(fp) -- Store: [0x8000c690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005aac]:fdiv.h t6, t5, t4, dyn
	-[0x80005ab0]:csrrs a2, fcsr, zero
	-[0x80005ab4]:sw t6, 696(fp)
Current Store : [0x80005ab8] : sw a2, 700(fp) -- Store: [0x8000c698]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005aec]:fdiv.h t6, t5, t4, dyn
	-[0x80005af0]:csrrs a2, fcsr, zero
	-[0x80005af4]:sw t6, 704(fp)
Current Store : [0x80005af8] : sw a2, 708(fp) -- Store: [0x8000c6a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005b30]:csrrs a2, fcsr, zero
	-[0x80005b34]:sw t6, 712(fp)
Current Store : [0x80005b38] : sw a2, 716(fp) -- Store: [0x8000c6a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005b70]:csrrs a2, fcsr, zero
	-[0x80005b74]:sw t6, 720(fp)
Current Store : [0x80005b78] : sw a2, 724(fp) -- Store: [0x8000c6b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bac]:fdiv.h t6, t5, t4, dyn
	-[0x80005bb0]:csrrs a2, fcsr, zero
	-[0x80005bb4]:sw t6, 728(fp)
Current Store : [0x80005bb8] : sw a2, 732(fp) -- Store: [0x8000c6b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bec]:fdiv.h t6, t5, t4, dyn
	-[0x80005bf0]:csrrs a2, fcsr, zero
	-[0x80005bf4]:sw t6, 736(fp)
Current Store : [0x80005bf8] : sw a2, 740(fp) -- Store: [0x8000c6c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005c30]:csrrs a2, fcsr, zero
	-[0x80005c34]:sw t6, 744(fp)
Current Store : [0x80005c38] : sw a2, 748(fp) -- Store: [0x8000c6c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005c70]:csrrs a2, fcsr, zero
	-[0x80005c74]:sw t6, 752(fp)
Current Store : [0x80005c78] : sw a2, 756(fp) -- Store: [0x8000c6d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005cac]:fdiv.h t6, t5, t4, dyn
	-[0x80005cb0]:csrrs a2, fcsr, zero
	-[0x80005cb4]:sw t6, 760(fp)
Current Store : [0x80005cb8] : sw a2, 764(fp) -- Store: [0x8000c6d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005cec]:fdiv.h t6, t5, t4, dyn
	-[0x80005cf0]:csrrs a2, fcsr, zero
	-[0x80005cf4]:sw t6, 768(fp)
Current Store : [0x80005cf8] : sw a2, 772(fp) -- Store: [0x8000c6e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005d30]:csrrs a2, fcsr, zero
	-[0x80005d34]:sw t6, 776(fp)
Current Store : [0x80005d38] : sw a2, 780(fp) -- Store: [0x8000c6e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005d70]:csrrs a2, fcsr, zero
	-[0x80005d74]:sw t6, 784(fp)
Current Store : [0x80005d78] : sw a2, 788(fp) -- Store: [0x8000c6f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005dac]:fdiv.h t6, t5, t4, dyn
	-[0x80005db0]:csrrs a2, fcsr, zero
	-[0x80005db4]:sw t6, 792(fp)
Current Store : [0x80005db8] : sw a2, 796(fp) -- Store: [0x8000c6f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005dec]:fdiv.h t6, t5, t4, dyn
	-[0x80005df0]:csrrs a2, fcsr, zero
	-[0x80005df4]:sw t6, 800(fp)
Current Store : [0x80005df8] : sw a2, 804(fp) -- Store: [0x8000c700]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005e30]:csrrs a2, fcsr, zero
	-[0x80005e34]:sw t6, 808(fp)
Current Store : [0x80005e38] : sw a2, 812(fp) -- Store: [0x8000c708]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005e70]:csrrs a2, fcsr, zero
	-[0x80005e74]:sw t6, 816(fp)
Current Store : [0x80005e78] : sw a2, 820(fp) -- Store: [0x8000c710]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005eac]:fdiv.h t6, t5, t4, dyn
	-[0x80005eb0]:csrrs a2, fcsr, zero
	-[0x80005eb4]:sw t6, 824(fp)
Current Store : [0x80005eb8] : sw a2, 828(fp) -- Store: [0x8000c718]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005eec]:fdiv.h t6, t5, t4, dyn
	-[0x80005ef0]:csrrs a2, fcsr, zero
	-[0x80005ef4]:sw t6, 832(fp)
Current Store : [0x80005ef8] : sw a2, 836(fp) -- Store: [0x8000c720]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f2c]:fdiv.h t6, t5, t4, dyn
	-[0x80005f30]:csrrs a2, fcsr, zero
	-[0x80005f34]:sw t6, 840(fp)
Current Store : [0x80005f38] : sw a2, 844(fp) -- Store: [0x8000c728]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f6c]:fdiv.h t6, t5, t4, dyn
	-[0x80005f70]:csrrs a2, fcsr, zero
	-[0x80005f74]:sw t6, 848(fp)
Current Store : [0x80005f78] : sw a2, 852(fp) -- Store: [0x8000c730]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fac]:fdiv.h t6, t5, t4, dyn
	-[0x80005fb0]:csrrs a2, fcsr, zero
	-[0x80005fb4]:sw t6, 856(fp)
Current Store : [0x80005fb8] : sw a2, 860(fp) -- Store: [0x8000c738]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fec]:fdiv.h t6, t5, t4, dyn
	-[0x80005ff0]:csrrs a2, fcsr, zero
	-[0x80005ff4]:sw t6, 864(fp)
Current Store : [0x80005ff8] : sw a2, 868(fp) -- Store: [0x8000c740]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000602c]:fdiv.h t6, t5, t4, dyn
	-[0x80006030]:csrrs a2, fcsr, zero
	-[0x80006034]:sw t6, 872(fp)
Current Store : [0x80006038] : sw a2, 876(fp) -- Store: [0x8000c748]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000606c]:fdiv.h t6, t5, t4, dyn
	-[0x80006070]:csrrs a2, fcsr, zero
	-[0x80006074]:sw t6, 880(fp)
Current Store : [0x80006078] : sw a2, 884(fp) -- Store: [0x8000c750]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060ac]:fdiv.h t6, t5, t4, dyn
	-[0x800060b0]:csrrs a2, fcsr, zero
	-[0x800060b4]:sw t6, 888(fp)
Current Store : [0x800060b8] : sw a2, 892(fp) -- Store: [0x8000c758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060ec]:fdiv.h t6, t5, t4, dyn
	-[0x800060f0]:csrrs a2, fcsr, zero
	-[0x800060f4]:sw t6, 896(fp)
Current Store : [0x800060f8] : sw a2, 900(fp) -- Store: [0x8000c760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000612c]:fdiv.h t6, t5, t4, dyn
	-[0x80006130]:csrrs a2, fcsr, zero
	-[0x80006134]:sw t6, 904(fp)
Current Store : [0x80006138] : sw a2, 908(fp) -- Store: [0x8000c768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000616c]:fdiv.h t6, t5, t4, dyn
	-[0x80006170]:csrrs a2, fcsr, zero
	-[0x80006174]:sw t6, 912(fp)
Current Store : [0x80006178] : sw a2, 916(fp) -- Store: [0x8000c770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061ac]:fdiv.h t6, t5, t4, dyn
	-[0x800061b0]:csrrs a2, fcsr, zero
	-[0x800061b4]:sw t6, 920(fp)
Current Store : [0x800061b8] : sw a2, 924(fp) -- Store: [0x8000c778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061ec]:fdiv.h t6, t5, t4, dyn
	-[0x800061f0]:csrrs a2, fcsr, zero
	-[0x800061f4]:sw t6, 928(fp)
Current Store : [0x800061f8] : sw a2, 932(fp) -- Store: [0x8000c780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000622c]:fdiv.h t6, t5, t4, dyn
	-[0x80006230]:csrrs a2, fcsr, zero
	-[0x80006234]:sw t6, 936(fp)
Current Store : [0x80006238] : sw a2, 940(fp) -- Store: [0x8000c788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000626c]:fdiv.h t6, t5, t4, dyn
	-[0x80006270]:csrrs a2, fcsr, zero
	-[0x80006274]:sw t6, 944(fp)
Current Store : [0x80006278] : sw a2, 948(fp) -- Store: [0x8000c790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062ac]:fdiv.h t6, t5, t4, dyn
	-[0x800062b0]:csrrs a2, fcsr, zero
	-[0x800062b4]:sw t6, 952(fp)
Current Store : [0x800062b8] : sw a2, 956(fp) -- Store: [0x8000c798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062ec]:fdiv.h t6, t5, t4, dyn
	-[0x800062f0]:csrrs a2, fcsr, zero
	-[0x800062f4]:sw t6, 960(fp)
Current Store : [0x800062f8] : sw a2, 964(fp) -- Store: [0x8000c7a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000632c]:fdiv.h t6, t5, t4, dyn
	-[0x80006330]:csrrs a2, fcsr, zero
	-[0x80006334]:sw t6, 968(fp)
Current Store : [0x80006338] : sw a2, 972(fp) -- Store: [0x8000c7a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000636c]:fdiv.h t6, t5, t4, dyn
	-[0x80006370]:csrrs a2, fcsr, zero
	-[0x80006374]:sw t6, 976(fp)
Current Store : [0x80006378] : sw a2, 980(fp) -- Store: [0x8000c7b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063ac]:fdiv.h t6, t5, t4, dyn
	-[0x800063b0]:csrrs a2, fcsr, zero
	-[0x800063b4]:sw t6, 984(fp)
Current Store : [0x800063b8] : sw a2, 988(fp) -- Store: [0x8000c7b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063ec]:fdiv.h t6, t5, t4, dyn
	-[0x800063f0]:csrrs a2, fcsr, zero
	-[0x800063f4]:sw t6, 992(fp)
Current Store : [0x800063f8] : sw a2, 996(fp) -- Store: [0x8000c7c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006424]:fdiv.h t6, t5, t4, dyn
	-[0x80006428]:csrrs a2, fcsr, zero
	-[0x8000642c]:sw t6, 1000(fp)
Current Store : [0x80006430] : sw a2, 1004(fp) -- Store: [0x8000c7c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000645c]:fdiv.h t6, t5, t4, dyn
	-[0x80006460]:csrrs a2, fcsr, zero
	-[0x80006464]:sw t6, 1008(fp)
Current Store : [0x80006468] : sw a2, 1012(fp) -- Store: [0x8000c7d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006494]:fdiv.h t6, t5, t4, dyn
	-[0x80006498]:csrrs a2, fcsr, zero
	-[0x8000649c]:sw t6, 1016(fp)
Current Store : [0x800064a0] : sw a2, 1020(fp) -- Store: [0x8000c7d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064d4]:fdiv.h t6, t5, t4, dyn
	-[0x800064d8]:csrrs a2, fcsr, zero
	-[0x800064dc]:sw t6, 0(fp)
Current Store : [0x800064e0] : sw a2, 4(fp) -- Store: [0x8000c7e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000650c]:fdiv.h t6, t5, t4, dyn
	-[0x80006510]:csrrs a2, fcsr, zero
	-[0x80006514]:sw t6, 8(fp)
Current Store : [0x80006518] : sw a2, 12(fp) -- Store: [0x8000c7e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006544]:fdiv.h t6, t5, t4, dyn
	-[0x80006548]:csrrs a2, fcsr, zero
	-[0x8000654c]:sw t6, 16(fp)
Current Store : [0x80006550] : sw a2, 20(fp) -- Store: [0x8000c7f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000657c]:fdiv.h t6, t5, t4, dyn
	-[0x80006580]:csrrs a2, fcsr, zero
	-[0x80006584]:sw t6, 24(fp)
Current Store : [0x80006588] : sw a2, 28(fp) -- Store: [0x8000c7f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065b4]:fdiv.h t6, t5, t4, dyn
	-[0x800065b8]:csrrs a2, fcsr, zero
	-[0x800065bc]:sw t6, 32(fp)
Current Store : [0x800065c0] : sw a2, 36(fp) -- Store: [0x8000c800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065ec]:fdiv.h t6, t5, t4, dyn
	-[0x800065f0]:csrrs a2, fcsr, zero
	-[0x800065f4]:sw t6, 40(fp)
Current Store : [0x800065f8] : sw a2, 44(fp) -- Store: [0x8000c808]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006624]:fdiv.h t6, t5, t4, dyn
	-[0x80006628]:csrrs a2, fcsr, zero
	-[0x8000662c]:sw t6, 48(fp)
Current Store : [0x80006630] : sw a2, 52(fp) -- Store: [0x8000c810]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000665c]:fdiv.h t6, t5, t4, dyn
	-[0x80006660]:csrrs a2, fcsr, zero
	-[0x80006664]:sw t6, 56(fp)
Current Store : [0x80006668] : sw a2, 60(fp) -- Store: [0x8000c818]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006694]:fdiv.h t6, t5, t4, dyn
	-[0x80006698]:csrrs a2, fcsr, zero
	-[0x8000669c]:sw t6, 64(fp)
Current Store : [0x800066a0] : sw a2, 68(fp) -- Store: [0x8000c820]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800066cc]:fdiv.h t6, t5, t4, dyn
	-[0x800066d0]:csrrs a2, fcsr, zero
	-[0x800066d4]:sw t6, 72(fp)
Current Store : [0x800066d8] : sw a2, 76(fp) -- Store: [0x8000c828]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006704]:fdiv.h t6, t5, t4, dyn
	-[0x80006708]:csrrs a2, fcsr, zero
	-[0x8000670c]:sw t6, 80(fp)
Current Store : [0x80006710] : sw a2, 84(fp) -- Store: [0x8000c830]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000673c]:fdiv.h t6, t5, t4, dyn
	-[0x80006740]:csrrs a2, fcsr, zero
	-[0x80006744]:sw t6, 88(fp)
Current Store : [0x80006748] : sw a2, 92(fp) -- Store: [0x8000c838]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006774]:fdiv.h t6, t5, t4, dyn
	-[0x80006778]:csrrs a2, fcsr, zero
	-[0x8000677c]:sw t6, 96(fp)
Current Store : [0x80006780] : sw a2, 100(fp) -- Store: [0x8000c840]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067ac]:fdiv.h t6, t5, t4, dyn
	-[0x800067b0]:csrrs a2, fcsr, zero
	-[0x800067b4]:sw t6, 104(fp)
Current Store : [0x800067b8] : sw a2, 108(fp) -- Store: [0x8000c848]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067e4]:fdiv.h t6, t5, t4, dyn
	-[0x800067e8]:csrrs a2, fcsr, zero
	-[0x800067ec]:sw t6, 112(fp)
Current Store : [0x800067f0] : sw a2, 116(fp) -- Store: [0x8000c850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000681c]:fdiv.h t6, t5, t4, dyn
	-[0x80006820]:csrrs a2, fcsr, zero
	-[0x80006824]:sw t6, 120(fp)
Current Store : [0x80006828] : sw a2, 124(fp) -- Store: [0x8000c858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006854]:fdiv.h t6, t5, t4, dyn
	-[0x80006858]:csrrs a2, fcsr, zero
	-[0x8000685c]:sw t6, 128(fp)
Current Store : [0x80006860] : sw a2, 132(fp) -- Store: [0x8000c860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000688c]:fdiv.h t6, t5, t4, dyn
	-[0x80006890]:csrrs a2, fcsr, zero
	-[0x80006894]:sw t6, 136(fp)
Current Store : [0x80006898] : sw a2, 140(fp) -- Store: [0x8000c868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068c4]:fdiv.h t6, t5, t4, dyn
	-[0x800068c8]:csrrs a2, fcsr, zero
	-[0x800068cc]:sw t6, 144(fp)
Current Store : [0x800068d0] : sw a2, 148(fp) -- Store: [0x8000c870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068fc]:fdiv.h t6, t5, t4, dyn
	-[0x80006900]:csrrs a2, fcsr, zero
	-[0x80006904]:sw t6, 152(fp)
Current Store : [0x80006908] : sw a2, 156(fp) -- Store: [0x8000c878]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006934]:fdiv.h t6, t5, t4, dyn
	-[0x80006938]:csrrs a2, fcsr, zero
	-[0x8000693c]:sw t6, 160(fp)
Current Store : [0x80006940] : sw a2, 164(fp) -- Store: [0x8000c880]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000696c]:fdiv.h t6, t5, t4, dyn
	-[0x80006970]:csrrs a2, fcsr, zero
	-[0x80006974]:sw t6, 168(fp)
Current Store : [0x80006978] : sw a2, 172(fp) -- Store: [0x8000c888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069a4]:fdiv.h t6, t5, t4, dyn
	-[0x800069a8]:csrrs a2, fcsr, zero
	-[0x800069ac]:sw t6, 176(fp)
Current Store : [0x800069b0] : sw a2, 180(fp) -- Store: [0x8000c890]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069dc]:fdiv.h t6, t5, t4, dyn
	-[0x800069e0]:csrrs a2, fcsr, zero
	-[0x800069e4]:sw t6, 184(fp)
Current Store : [0x800069e8] : sw a2, 188(fp) -- Store: [0x8000c898]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a14]:fdiv.h t6, t5, t4, dyn
	-[0x80006a18]:csrrs a2, fcsr, zero
	-[0x80006a1c]:sw t6, 192(fp)
Current Store : [0x80006a20] : sw a2, 196(fp) -- Store: [0x8000c8a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a4c]:fdiv.h t6, t5, t4, dyn
	-[0x80006a50]:csrrs a2, fcsr, zero
	-[0x80006a54]:sw t6, 200(fp)
Current Store : [0x80006a58] : sw a2, 204(fp) -- Store: [0x8000c8a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a84]:fdiv.h t6, t5, t4, dyn
	-[0x80006a88]:csrrs a2, fcsr, zero
	-[0x80006a8c]:sw t6, 208(fp)
Current Store : [0x80006a90] : sw a2, 212(fp) -- Store: [0x8000c8b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006abc]:fdiv.h t6, t5, t4, dyn
	-[0x80006ac0]:csrrs a2, fcsr, zero
	-[0x80006ac4]:sw t6, 216(fp)
Current Store : [0x80006ac8] : sw a2, 220(fp) -- Store: [0x8000c8b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006af4]:fdiv.h t6, t5, t4, dyn
	-[0x80006af8]:csrrs a2, fcsr, zero
	-[0x80006afc]:sw t6, 224(fp)
Current Store : [0x80006b00] : sw a2, 228(fp) -- Store: [0x8000c8c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b2c]:fdiv.h t6, t5, t4, dyn
	-[0x80006b30]:csrrs a2, fcsr, zero
	-[0x80006b34]:sw t6, 232(fp)
Current Store : [0x80006b38] : sw a2, 236(fp) -- Store: [0x8000c8c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b64]:fdiv.h t6, t5, t4, dyn
	-[0x80006b68]:csrrs a2, fcsr, zero
	-[0x80006b6c]:sw t6, 240(fp)
Current Store : [0x80006b70] : sw a2, 244(fp) -- Store: [0x8000c8d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b9c]:fdiv.h t6, t5, t4, dyn
	-[0x80006ba0]:csrrs a2, fcsr, zero
	-[0x80006ba4]:sw t6, 248(fp)
Current Store : [0x80006ba8] : sw a2, 252(fp) -- Store: [0x8000c8d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006bd4]:fdiv.h t6, t5, t4, dyn
	-[0x80006bd8]:csrrs a2, fcsr, zero
	-[0x80006bdc]:sw t6, 256(fp)
Current Store : [0x80006be0] : sw a2, 260(fp) -- Store: [0x8000c8e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c0c]:fdiv.h t6, t5, t4, dyn
	-[0x80006c10]:csrrs a2, fcsr, zero
	-[0x80006c14]:sw t6, 264(fp)
Current Store : [0x80006c18] : sw a2, 268(fp) -- Store: [0x8000c8e8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fdiv.h t6, t5, t4, dyn
	-[0x80006c48]:csrrs a2, fcsr, zero
	-[0x80006c4c]:sw t6, 272(fp)
Current Store : [0x80006c50] : sw a2, 276(fp) -- Store: [0x8000c8f0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c7c]:fdiv.h t6, t5, t4, dyn
	-[0x80006c80]:csrrs a2, fcsr, zero
	-[0x80006c84]:sw t6, 280(fp)
Current Store : [0x80006c88] : sw a2, 284(fp) -- Store: [0x8000c8f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cb4]:fdiv.h t6, t5, t4, dyn
	-[0x80006cb8]:csrrs a2, fcsr, zero
	-[0x80006cbc]:sw t6, 288(fp)
Current Store : [0x80006cc0] : sw a2, 292(fp) -- Store: [0x8000c900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cec]:fdiv.h t6, t5, t4, dyn
	-[0x80006cf0]:csrrs a2, fcsr, zero
	-[0x80006cf4]:sw t6, 296(fp)
Current Store : [0x80006cf8] : sw a2, 300(fp) -- Store: [0x8000c908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d24]:fdiv.h t6, t5, t4, dyn
	-[0x80006d28]:csrrs a2, fcsr, zero
	-[0x80006d2c]:sw t6, 304(fp)
Current Store : [0x80006d30] : sw a2, 308(fp) -- Store: [0x8000c910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d5c]:fdiv.h t6, t5, t4, dyn
	-[0x80006d60]:csrrs a2, fcsr, zero
	-[0x80006d64]:sw t6, 312(fp)
Current Store : [0x80006d68] : sw a2, 316(fp) -- Store: [0x8000c918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d94]:fdiv.h t6, t5, t4, dyn
	-[0x80006d98]:csrrs a2, fcsr, zero
	-[0x80006d9c]:sw t6, 320(fp)
Current Store : [0x80006da0] : sw a2, 324(fp) -- Store: [0x8000c920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006dcc]:fdiv.h t6, t5, t4, dyn
	-[0x80006dd0]:csrrs a2, fcsr, zero
	-[0x80006dd4]:sw t6, 328(fp)
Current Store : [0x80006dd8] : sw a2, 332(fp) -- Store: [0x8000c928]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e04]:fdiv.h t6, t5, t4, dyn
	-[0x80006e08]:csrrs a2, fcsr, zero
	-[0x80006e0c]:sw t6, 336(fp)
Current Store : [0x80006e10] : sw a2, 340(fp) -- Store: [0x8000c930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e3c]:fdiv.h t6, t5, t4, dyn
	-[0x80006e40]:csrrs a2, fcsr, zero
	-[0x80006e44]:sw t6, 344(fp)
Current Store : [0x80006e48] : sw a2, 348(fp) -- Store: [0x8000c938]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e74]:fdiv.h t6, t5, t4, dyn
	-[0x80006e78]:csrrs a2, fcsr, zero
	-[0x80006e7c]:sw t6, 352(fp)
Current Store : [0x80006e80] : sw a2, 356(fp) -- Store: [0x8000c940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006eac]:fdiv.h t6, t5, t4, dyn
	-[0x80006eb0]:csrrs a2, fcsr, zero
	-[0x80006eb4]:sw t6, 360(fp)
Current Store : [0x80006eb8] : sw a2, 364(fp) -- Store: [0x8000c948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ee4]:fdiv.h t6, t5, t4, dyn
	-[0x80006ee8]:csrrs a2, fcsr, zero
	-[0x80006eec]:sw t6, 368(fp)
Current Store : [0x80006ef0] : sw a2, 372(fp) -- Store: [0x8000c950]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f1c]:fdiv.h t6, t5, t4, dyn
	-[0x80006f20]:csrrs a2, fcsr, zero
	-[0x80006f24]:sw t6, 376(fp)
Current Store : [0x80006f28] : sw a2, 380(fp) -- Store: [0x8000c958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f54]:fdiv.h t6, t5, t4, dyn
	-[0x80006f58]:csrrs a2, fcsr, zero
	-[0x80006f5c]:sw t6, 384(fp)
Current Store : [0x80006f60] : sw a2, 388(fp) -- Store: [0x8000c960]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f8c]:fdiv.h t6, t5, t4, dyn
	-[0x80006f90]:csrrs a2, fcsr, zero
	-[0x80006f94]:sw t6, 392(fp)
Current Store : [0x80006f98] : sw a2, 396(fp) -- Store: [0x8000c968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006fc4]:fdiv.h t6, t5, t4, dyn
	-[0x80006fc8]:csrrs a2, fcsr, zero
	-[0x80006fcc]:sw t6, 400(fp)
Current Store : [0x80006fd0] : sw a2, 404(fp) -- Store: [0x8000c970]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ffc]:fdiv.h t6, t5, t4, dyn
	-[0x80007000]:csrrs a2, fcsr, zero
	-[0x80007004]:sw t6, 408(fp)
Current Store : [0x80007008] : sw a2, 412(fp) -- Store: [0x8000c978]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007034]:fdiv.h t6, t5, t4, dyn
	-[0x80007038]:csrrs a2, fcsr, zero
	-[0x8000703c]:sw t6, 416(fp)
Current Store : [0x80007040] : sw a2, 420(fp) -- Store: [0x8000c980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000706c]:fdiv.h t6, t5, t4, dyn
	-[0x80007070]:csrrs a2, fcsr, zero
	-[0x80007074]:sw t6, 424(fp)
Current Store : [0x80007078] : sw a2, 428(fp) -- Store: [0x8000c988]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800070a4]:fdiv.h t6, t5, t4, dyn
	-[0x800070a8]:csrrs a2, fcsr, zero
	-[0x800070ac]:sw t6, 432(fp)
Current Store : [0x800070b0] : sw a2, 436(fp) -- Store: [0x8000c990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800070dc]:fdiv.h t6, t5, t4, dyn
	-[0x800070e0]:csrrs a2, fcsr, zero
	-[0x800070e4]:sw t6, 440(fp)
Current Store : [0x800070e8] : sw a2, 444(fp) -- Store: [0x8000c998]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007114]:fdiv.h t6, t5, t4, dyn
	-[0x80007118]:csrrs a2, fcsr, zero
	-[0x8000711c]:sw t6, 448(fp)
Current Store : [0x80007120] : sw a2, 452(fp) -- Store: [0x8000c9a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000714c]:fdiv.h t6, t5, t4, dyn
	-[0x80007150]:csrrs a2, fcsr, zero
	-[0x80007154]:sw t6, 456(fp)
Current Store : [0x80007158] : sw a2, 460(fp) -- Store: [0x8000c9a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007184]:fdiv.h t6, t5, t4, dyn
	-[0x80007188]:csrrs a2, fcsr, zero
	-[0x8000718c]:sw t6, 464(fp)
Current Store : [0x80007190] : sw a2, 468(fp) -- Store: [0x8000c9b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800071bc]:fdiv.h t6, t5, t4, dyn
	-[0x800071c0]:csrrs a2, fcsr, zero
	-[0x800071c4]:sw t6, 472(fp)
Current Store : [0x800071c8] : sw a2, 476(fp) -- Store: [0x8000c9b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800071f4]:fdiv.h t6, t5, t4, dyn
	-[0x800071f8]:csrrs a2, fcsr, zero
	-[0x800071fc]:sw t6, 480(fp)
Current Store : [0x80007200] : sw a2, 484(fp) -- Store: [0x8000c9c0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000722c]:fdiv.h t6, t5, t4, dyn
	-[0x80007230]:csrrs a2, fcsr, zero
	-[0x80007234]:sw t6, 488(fp)
Current Store : [0x80007238] : sw a2, 492(fp) -- Store: [0x8000c9c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007264]:fdiv.h t6, t5, t4, dyn
	-[0x80007268]:csrrs a2, fcsr, zero
	-[0x8000726c]:sw t6, 496(fp)
Current Store : [0x80007270] : sw a2, 500(fp) -- Store: [0x8000c9d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000729c]:fdiv.h t6, t5, t4, dyn
	-[0x800072a0]:csrrs a2, fcsr, zero
	-[0x800072a4]:sw t6, 504(fp)
Current Store : [0x800072a8] : sw a2, 508(fp) -- Store: [0x8000c9d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800072d4]:fdiv.h t6, t5, t4, dyn
	-[0x800072d8]:csrrs a2, fcsr, zero
	-[0x800072dc]:sw t6, 512(fp)
Current Store : [0x800072e0] : sw a2, 516(fp) -- Store: [0x8000c9e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000730c]:fdiv.h t6, t5, t4, dyn
	-[0x80007310]:csrrs a2, fcsr, zero
	-[0x80007314]:sw t6, 520(fp)
Current Store : [0x80007318] : sw a2, 524(fp) -- Store: [0x8000c9e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007344]:fdiv.h t6, t5, t4, dyn
	-[0x80007348]:csrrs a2, fcsr, zero
	-[0x8000734c]:sw t6, 528(fp)
Current Store : [0x80007350] : sw a2, 532(fp) -- Store: [0x8000c9f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000737c]:fdiv.h t6, t5, t4, dyn
	-[0x80007380]:csrrs a2, fcsr, zero
	-[0x80007384]:sw t6, 536(fp)
Current Store : [0x80007388] : sw a2, 540(fp) -- Store: [0x8000c9f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800073b4]:fdiv.h t6, t5, t4, dyn
	-[0x800073b8]:csrrs a2, fcsr, zero
	-[0x800073bc]:sw t6, 544(fp)
Current Store : [0x800073c0] : sw a2, 548(fp) -- Store: [0x8000ca00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800073ec]:fdiv.h t6, t5, t4, dyn
	-[0x800073f0]:csrrs a2, fcsr, zero
	-[0x800073f4]:sw t6, 552(fp)
Current Store : [0x800073f8] : sw a2, 556(fp) -- Store: [0x8000ca08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007424]:fdiv.h t6, t5, t4, dyn
	-[0x80007428]:csrrs a2, fcsr, zero
	-[0x8000742c]:sw t6, 560(fp)
Current Store : [0x80007430] : sw a2, 564(fp) -- Store: [0x8000ca10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000745c]:fdiv.h t6, t5, t4, dyn
	-[0x80007460]:csrrs a2, fcsr, zero
	-[0x80007464]:sw t6, 568(fp)
Current Store : [0x80007468] : sw a2, 572(fp) -- Store: [0x8000ca18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007494]:fdiv.h t6, t5, t4, dyn
	-[0x80007498]:csrrs a2, fcsr, zero
	-[0x8000749c]:sw t6, 576(fp)
Current Store : [0x800074a0] : sw a2, 580(fp) -- Store: [0x8000ca20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800074cc]:fdiv.h t6, t5, t4, dyn
	-[0x800074d0]:csrrs a2, fcsr, zero
	-[0x800074d4]:sw t6, 584(fp)
Current Store : [0x800074d8] : sw a2, 588(fp) -- Store: [0x8000ca28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007504]:fdiv.h t6, t5, t4, dyn
	-[0x80007508]:csrrs a2, fcsr, zero
	-[0x8000750c]:sw t6, 592(fp)
Current Store : [0x80007510] : sw a2, 596(fp) -- Store: [0x8000ca30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000753c]:fdiv.h t6, t5, t4, dyn
	-[0x80007540]:csrrs a2, fcsr, zero
	-[0x80007544]:sw t6, 600(fp)
Current Store : [0x80007548] : sw a2, 604(fp) -- Store: [0x8000ca38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007574]:fdiv.h t6, t5, t4, dyn
	-[0x80007578]:csrrs a2, fcsr, zero
	-[0x8000757c]:sw t6, 608(fp)
Current Store : [0x80007580] : sw a2, 612(fp) -- Store: [0x8000ca40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800075ac]:fdiv.h t6, t5, t4, dyn
	-[0x800075b0]:csrrs a2, fcsr, zero
	-[0x800075b4]:sw t6, 616(fp)
Current Store : [0x800075b8] : sw a2, 620(fp) -- Store: [0x8000ca48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800075e4]:fdiv.h t6, t5, t4, dyn
	-[0x800075e8]:csrrs a2, fcsr, zero
	-[0x800075ec]:sw t6, 624(fp)
Current Store : [0x800075f0] : sw a2, 628(fp) -- Store: [0x8000ca50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000761c]:fdiv.h t6, t5, t4, dyn
	-[0x80007620]:csrrs a2, fcsr, zero
	-[0x80007624]:sw t6, 632(fp)
Current Store : [0x80007628] : sw a2, 636(fp) -- Store: [0x8000ca58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007654]:fdiv.h t6, t5, t4, dyn
	-[0x80007658]:csrrs a2, fcsr, zero
	-[0x8000765c]:sw t6, 640(fp)
Current Store : [0x80007660] : sw a2, 644(fp) -- Store: [0x8000ca60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000768c]:fdiv.h t6, t5, t4, dyn
	-[0x80007690]:csrrs a2, fcsr, zero
	-[0x80007694]:sw t6, 648(fp)
Current Store : [0x80007698] : sw a2, 652(fp) -- Store: [0x8000ca68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800076c4]:fdiv.h t6, t5, t4, dyn
	-[0x800076c8]:csrrs a2, fcsr, zero
	-[0x800076cc]:sw t6, 656(fp)
Current Store : [0x800076d0] : sw a2, 660(fp) -- Store: [0x8000ca70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800076fc]:fdiv.h t6, t5, t4, dyn
	-[0x80007700]:csrrs a2, fcsr, zero
	-[0x80007704]:sw t6, 664(fp)
Current Store : [0x80007708] : sw a2, 668(fp) -- Store: [0x8000ca78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007734]:fdiv.h t6, t5, t4, dyn
	-[0x80007738]:csrrs a2, fcsr, zero
	-[0x8000773c]:sw t6, 672(fp)
Current Store : [0x80007740] : sw a2, 676(fp) -- Store: [0x8000ca80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000776c]:fdiv.h t6, t5, t4, dyn
	-[0x80007770]:csrrs a2, fcsr, zero
	-[0x80007774]:sw t6, 680(fp)
Current Store : [0x80007778] : sw a2, 684(fp) -- Store: [0x8000ca88]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800077a4]:fdiv.h t6, t5, t4, dyn
	-[0x800077a8]:csrrs a2, fcsr, zero
	-[0x800077ac]:sw t6, 688(fp)
Current Store : [0x800077b0] : sw a2, 692(fp) -- Store: [0x8000ca90]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800077dc]:fdiv.h t6, t5, t4, dyn
	-[0x800077e0]:csrrs a2, fcsr, zero
	-[0x800077e4]:sw t6, 696(fp)
Current Store : [0x800077e8] : sw a2, 700(fp) -- Store: [0x8000ca98]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007814]:fdiv.h t6, t5, t4, dyn
	-[0x80007818]:csrrs a2, fcsr, zero
	-[0x8000781c]:sw t6, 704(fp)
Current Store : [0x80007820] : sw a2, 708(fp) -- Store: [0x8000caa0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000784c]:fdiv.h t6, t5, t4, dyn
	-[0x80007850]:csrrs a2, fcsr, zero
	-[0x80007854]:sw t6, 712(fp)
Current Store : [0x80007858] : sw a2, 716(fp) -- Store: [0x8000caa8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007884]:fdiv.h t6, t5, t4, dyn
	-[0x80007888]:csrrs a2, fcsr, zero
	-[0x8000788c]:sw t6, 720(fp)
Current Store : [0x80007890] : sw a2, 724(fp) -- Store: [0x8000cab0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800078bc]:fdiv.h t6, t5, t4, dyn
	-[0x800078c0]:csrrs a2, fcsr, zero
	-[0x800078c4]:sw t6, 728(fp)
Current Store : [0x800078c8] : sw a2, 732(fp) -- Store: [0x8000cab8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800078f4]:fdiv.h t6, t5, t4, dyn
	-[0x800078f8]:csrrs a2, fcsr, zero
	-[0x800078fc]:sw t6, 736(fp)
Current Store : [0x80007900] : sw a2, 740(fp) -- Store: [0x8000cac0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000792c]:fdiv.h t6, t5, t4, dyn
	-[0x80007930]:csrrs a2, fcsr, zero
	-[0x80007934]:sw t6, 744(fp)
Current Store : [0x80007938] : sw a2, 748(fp) -- Store: [0x8000cac8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007964]:fdiv.h t6, t5, t4, dyn
	-[0x80007968]:csrrs a2, fcsr, zero
	-[0x8000796c]:sw t6, 752(fp)
Current Store : [0x80007970] : sw a2, 756(fp) -- Store: [0x8000cad0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000799c]:fdiv.h t6, t5, t4, dyn
	-[0x800079a0]:csrrs a2, fcsr, zero
	-[0x800079a4]:sw t6, 760(fp)
Current Store : [0x800079a8] : sw a2, 764(fp) -- Store: [0x8000cad8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800079d4]:fdiv.h t6, t5, t4, dyn
	-[0x800079d8]:csrrs a2, fcsr, zero
	-[0x800079dc]:sw t6, 768(fp)
Current Store : [0x800079e0] : sw a2, 772(fp) -- Store: [0x8000cae0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a0c]:fdiv.h t6, t5, t4, dyn
	-[0x80007a10]:csrrs a2, fcsr, zero
	-[0x80007a14]:sw t6, 776(fp)
Current Store : [0x80007a18] : sw a2, 780(fp) -- Store: [0x8000cae8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a44]:fdiv.h t6, t5, t4, dyn
	-[0x80007a48]:csrrs a2, fcsr, zero
	-[0x80007a4c]:sw t6, 784(fp)
Current Store : [0x80007a50] : sw a2, 788(fp) -- Store: [0x8000caf0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a7c]:fdiv.h t6, t5, t4, dyn
	-[0x80007a80]:csrrs a2, fcsr, zero
	-[0x80007a84]:sw t6, 792(fp)
Current Store : [0x80007a88] : sw a2, 796(fp) -- Store: [0x8000caf8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ab4]:fdiv.h t6, t5, t4, dyn
	-[0x80007ab8]:csrrs a2, fcsr, zero
	-[0x80007abc]:sw t6, 800(fp)
Current Store : [0x80007ac0] : sw a2, 804(fp) -- Store: [0x8000cb00]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007aec]:fdiv.h t6, t5, t4, dyn
	-[0x80007af0]:csrrs a2, fcsr, zero
	-[0x80007af4]:sw t6, 808(fp)
Current Store : [0x80007af8] : sw a2, 812(fp) -- Store: [0x8000cb08]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b24]:fdiv.h t6, t5, t4, dyn
	-[0x80007b28]:csrrs a2, fcsr, zero
	-[0x80007b2c]:sw t6, 816(fp)
Current Store : [0x80007b30] : sw a2, 820(fp) -- Store: [0x8000cb10]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b5c]:fdiv.h t6, t5, t4, dyn
	-[0x80007b60]:csrrs a2, fcsr, zero
	-[0x80007b64]:sw t6, 824(fp)
Current Store : [0x80007b68] : sw a2, 828(fp) -- Store: [0x8000cb18]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b94]:fdiv.h t6, t5, t4, dyn
	-[0x80007b98]:csrrs a2, fcsr, zero
	-[0x80007b9c]:sw t6, 832(fp)
Current Store : [0x80007ba0] : sw a2, 836(fp) -- Store: [0x8000cb20]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007bcc]:fdiv.h t6, t5, t4, dyn
	-[0x80007bd0]:csrrs a2, fcsr, zero
	-[0x80007bd4]:sw t6, 840(fp)
Current Store : [0x80007bd8] : sw a2, 844(fp) -- Store: [0x8000cb28]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c04]:fdiv.h t6, t5, t4, dyn
	-[0x80007c08]:csrrs a2, fcsr, zero
	-[0x80007c0c]:sw t6, 848(fp)
Current Store : [0x80007c10] : sw a2, 852(fp) -- Store: [0x8000cb30]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c3c]:fdiv.h t6, t5, t4, dyn
	-[0x80007c40]:csrrs a2, fcsr, zero
	-[0x80007c44]:sw t6, 856(fp)
Current Store : [0x80007c48] : sw a2, 860(fp) -- Store: [0x8000cb38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c74]:fdiv.h t6, t5, t4, dyn
	-[0x80007c78]:csrrs a2, fcsr, zero
	-[0x80007c7c]:sw t6, 864(fp)
Current Store : [0x80007c80] : sw a2, 868(fp) -- Store: [0x8000cb40]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007cac]:fdiv.h t6, t5, t4, dyn
	-[0x80007cb0]:csrrs a2, fcsr, zero
	-[0x80007cb4]:sw t6, 872(fp)
Current Store : [0x80007cb8] : sw a2, 876(fp) -- Store: [0x8000cb48]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ce4]:fdiv.h t6, t5, t4, dyn
	-[0x80007ce8]:csrrs a2, fcsr, zero
	-[0x80007cec]:sw t6, 880(fp)
Current Store : [0x80007cf0] : sw a2, 884(fp) -- Store: [0x8000cb50]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007d1c]:fdiv.h t6, t5, t4, dyn
	-[0x80007d20]:csrrs a2, fcsr, zero
	-[0x80007d24]:sw t6, 888(fp)
Current Store : [0x80007d28] : sw a2, 892(fp) -- Store: [0x8000cb58]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007d54]:fdiv.h t6, t5, t4, dyn
	-[0x80007d58]:csrrs a2, fcsr, zero
	-[0x80007d5c]:sw t6, 896(fp)
Current Store : [0x80007d60] : sw a2, 900(fp) -- Store: [0x8000cb60]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007d8c]:fdiv.h t6, t5, t4, dyn
	-[0x80007d90]:csrrs a2, fcsr, zero
	-[0x80007d94]:sw t6, 904(fp)
Current Store : [0x80007d98] : sw a2, 908(fp) -- Store: [0x8000cb68]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007dc4]:fdiv.h t6, t5, t4, dyn
	-[0x80007dc8]:csrrs a2, fcsr, zero
	-[0x80007dcc]:sw t6, 912(fp)
Current Store : [0x80007dd0] : sw a2, 916(fp) -- Store: [0x8000cb70]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007dfc]:fdiv.h t6, t5, t4, dyn
	-[0x80007e00]:csrrs a2, fcsr, zero
	-[0x80007e04]:sw t6, 920(fp)
Current Store : [0x80007e08] : sw a2, 924(fp) -- Store: [0x8000cb78]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007e34]:fdiv.h t6, t5, t4, dyn
	-[0x80007e38]:csrrs a2, fcsr, zero
	-[0x80007e3c]:sw t6, 928(fp)
Current Store : [0x80007e40] : sw a2, 932(fp) -- Store: [0x8000cb80]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007e6c]:fdiv.h t6, t5, t4, dyn
	-[0x80007e70]:csrrs a2, fcsr, zero
	-[0x80007e74]:sw t6, 936(fp)
Current Store : [0x80007e78] : sw a2, 940(fp) -- Store: [0x8000cb88]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ea4]:fdiv.h t6, t5, t4, dyn
	-[0x80007ea8]:csrrs a2, fcsr, zero
	-[0x80007eac]:sw t6, 944(fp)
Current Store : [0x80007eb0] : sw a2, 948(fp) -- Store: [0x8000cb90]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007edc]:fdiv.h t6, t5, t4, dyn
	-[0x80007ee0]:csrrs a2, fcsr, zero
	-[0x80007ee4]:sw t6, 952(fp)
Current Store : [0x80007ee8] : sw a2, 956(fp) -- Store: [0x8000cb98]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f14]:fdiv.h t6, t5, t4, dyn
	-[0x80007f18]:csrrs a2, fcsr, zero
	-[0x80007f1c]:sw t6, 960(fp)
Current Store : [0x80007f20] : sw a2, 964(fp) -- Store: [0x8000cba0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f4c]:fdiv.h t6, t5, t4, dyn
	-[0x80007f50]:csrrs a2, fcsr, zero
	-[0x80007f54]:sw t6, 968(fp)
Current Store : [0x80007f58] : sw a2, 972(fp) -- Store: [0x8000cba8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f84]:fdiv.h t6, t5, t4, dyn
	-[0x80007f88]:csrrs a2, fcsr, zero
	-[0x80007f8c]:sw t6, 976(fp)
Current Store : [0x80007f90] : sw a2, 980(fp) -- Store: [0x8000cbb0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007fbc]:fdiv.h t6, t5, t4, dyn
	-[0x80007fc0]:csrrs a2, fcsr, zero
	-[0x80007fc4]:sw t6, 984(fp)
Current Store : [0x80007fc8] : sw a2, 988(fp) -- Store: [0x8000cbb8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ff4]:fdiv.h t6, t5, t4, dyn
	-[0x80007ff8]:csrrs a2, fcsr, zero
	-[0x80007ffc]:sw t6, 992(fp)
Current Store : [0x80008000] : sw a2, 996(fp) -- Store: [0x8000cbc0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000802c]:fdiv.h t6, t5, t4, dyn
	-[0x80008030]:csrrs a2, fcsr, zero
	-[0x80008034]:sw t6, 1000(fp)
Current Store : [0x80008038] : sw a2, 1004(fp) -- Store: [0x8000cbc8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008064]:fdiv.h t6, t5, t4, dyn
	-[0x80008068]:csrrs a2, fcsr, zero
	-[0x8000806c]:sw t6, 1008(fp)
Current Store : [0x80008070] : sw a2, 1012(fp) -- Store: [0x8000cbd0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000809c]:fdiv.h t6, t5, t4, dyn
	-[0x800080a0]:csrrs a2, fcsr, zero
	-[0x800080a4]:sw t6, 1016(fp)
Current Store : [0x800080a8] : sw a2, 1020(fp) -- Store: [0x8000cbd8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800080dc]:fdiv.h t6, t5, t4, dyn
	-[0x800080e0]:csrrs a2, fcsr, zero
	-[0x800080e4]:sw t6, 0(fp)
Current Store : [0x800080e8] : sw a2, 4(fp) -- Store: [0x8000cbe0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008114]:fdiv.h t6, t5, t4, dyn
	-[0x80008118]:csrrs a2, fcsr, zero
	-[0x8000811c]:sw t6, 8(fp)
Current Store : [0x80008120] : sw a2, 12(fp) -- Store: [0x8000cbe8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000814c]:fdiv.h t6, t5, t4, dyn
	-[0x80008150]:csrrs a2, fcsr, zero
	-[0x80008154]:sw t6, 16(fp)
Current Store : [0x80008158] : sw a2, 20(fp) -- Store: [0x8000cbf0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008184]:fdiv.h t6, t5, t4, dyn
	-[0x80008188]:csrrs a2, fcsr, zero
	-[0x8000818c]:sw t6, 24(fp)
Current Store : [0x80008190] : sw a2, 28(fp) -- Store: [0x8000cbf8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800081bc]:fdiv.h t6, t5, t4, dyn
	-[0x800081c0]:csrrs a2, fcsr, zero
	-[0x800081c4]:sw t6, 32(fp)
Current Store : [0x800081c8] : sw a2, 36(fp) -- Store: [0x8000cc00]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800081f4]:fdiv.h t6, t5, t4, dyn
	-[0x800081f8]:csrrs a2, fcsr, zero
	-[0x800081fc]:sw t6, 40(fp)
Current Store : [0x80008200] : sw a2, 44(fp) -- Store: [0x8000cc08]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000822c]:fdiv.h t6, t5, t4, dyn
	-[0x80008230]:csrrs a2, fcsr, zero
	-[0x80008234]:sw t6, 48(fp)
Current Store : [0x80008238] : sw a2, 52(fp) -- Store: [0x8000cc10]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008264]:fdiv.h t6, t5, t4, dyn
	-[0x80008268]:csrrs a2, fcsr, zero
	-[0x8000826c]:sw t6, 56(fp)
Current Store : [0x80008270] : sw a2, 60(fp) -- Store: [0x8000cc18]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000829c]:fdiv.h t6, t5, t4, dyn
	-[0x800082a0]:csrrs a2, fcsr, zero
	-[0x800082a4]:sw t6, 64(fp)
Current Store : [0x800082a8] : sw a2, 68(fp) -- Store: [0x8000cc20]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800082d4]:fdiv.h t6, t5, t4, dyn
	-[0x800082d8]:csrrs a2, fcsr, zero
	-[0x800082dc]:sw t6, 72(fp)
Current Store : [0x800082e0] : sw a2, 76(fp) -- Store: [0x8000cc28]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000830c]:fdiv.h t6, t5, t4, dyn
	-[0x80008310]:csrrs a2, fcsr, zero
	-[0x80008314]:sw t6, 80(fp)
Current Store : [0x80008318] : sw a2, 84(fp) -- Store: [0x8000cc30]:0x00000010




Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008344]:fdiv.h t6, t5, t4, dyn
	-[0x80008348]:csrrs a2, fcsr, zero
	-[0x8000834c]:sw t6, 88(fp)
Current Store : [0x80008350] : sw a2, 92(fp) -- Store: [0x8000cc38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000837c]:fdiv.h t6, t5, t4, dyn
	-[0x80008380]:csrrs a2, fcsr, zero
	-[0x80008384]:sw t6, 96(fp)
Current Store : [0x80008388] : sw a2, 100(fp) -- Store: [0x8000cc40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800083b4]:fdiv.h t6, t5, t4, dyn
	-[0x800083b8]:csrrs a2, fcsr, zero
	-[0x800083bc]:sw t6, 104(fp)
Current Store : [0x800083c0] : sw a2, 108(fp) -- Store: [0x8000cc48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800083ec]:fdiv.h t6, t5, t4, dyn
	-[0x800083f0]:csrrs a2, fcsr, zero
	-[0x800083f4]:sw t6, 112(fp)
Current Store : [0x800083f8] : sw a2, 116(fp) -- Store: [0x8000cc50]:0x00000000




Last Coverpoint : ['mnemonic : fdiv.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008424]:fdiv.h t6, t5, t4, dyn
	-[0x80008428]:csrrs a2, fcsr, zero
	-[0x8000842c]:sw t6, 120(fp)
Current Store : [0x80008430] : sw a2, 124(fp) -- Store: [0x8000cc58]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                           |                                                      code                                                       |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x8000b714]<br>0x00007E00<br> |- mnemonic : fdiv.h<br> - rs1 : x31<br> - rs2 : x31<br> - rd : x31<br> - rs1 == rs2 == rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fdiv.h t6, t6, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000b71c]<br>0x00007E00<br> |- rs1 : x29<br> - rs2 : x28<br> - rd : x30<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>  |[0x80000144]:fdiv.h t5, t4, t3, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 8(ra)<br>      |
|   3|[0x8000b724]<br>0x00007E00<br> |- rs1 : x30<br> - rs2 : x30<br> - rd : x29<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                           |[0x80000164]:fdiv.h t4, t5, t5, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t4, 16(ra)<br>     |
|   4|[0x8000b72c]<br>0xFFFF8000<br> |- rs1 : x28<br> - rs2 : x29<br> - rd : x28<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x80000184]:fdiv.h t3, t3, t4, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>     |
|   5|[0x8000b734]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x27<br> - rd : x27<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                         |[0x800001a4]:fdiv.h s11, s10, s11, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s11, 32(ra)<br> |
|   6|[0x8000b73c]<br>0xFFFF8000<br> |- rs1 : x27<br> - rs2 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fdiv.h s10, s11, s9, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>  |
|   7|[0x8000b744]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001e4]:fdiv.h s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8000b74c]<br>0xFFFF8000<br> |- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000204]:fdiv.h s8, s9, s7, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x8000b754]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000224]:fdiv.h s7, s6, s8, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000b75c]<br>0xFFFF8000<br> |- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000244]:fdiv.h s6, s7, s5, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x8000b764]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fdiv.h s5, s4, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000b76c]<br>0xFFFF8000<br> |- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000284]:fdiv.h s4, s5, s3, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x8000b774]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002a4]:fdiv.h s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000b77c]<br>0xFFFF8000<br> |- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002c4]:fdiv.h s2, s3, a7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x8000b784]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800002e4]:fdiv.h a7, a6, s2, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000b78c]<br>0xFFFF8000<br> |- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fdiv.h a6, a7, a5, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x8000b794]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000324]:fdiv.h a5, a4, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000b79c]<br>0xFFFF8000<br> |- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000344]:fdiv.h a4, a5, a3, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x8000b7a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000364]:fdiv.h a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x8000b7ac]<br>0xFFFF8000<br> |- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000384]:fdiv.h a2, a3, a1, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x8000b7b4]<br>0x00007E00<br> |- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003a4]:fdiv.h a1, a0, a2, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x8000b7bc]<br>0x00007E00<br> |- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                 |[0x800003c4]:fdiv.h a0, a1, s1, dyn<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x8000b7c4]<br>0x00007E00<br> |- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800003ec]:fdiv.h s1, fp, a0, dyn<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x8000b7cc]<br>0x00007E00<br> |- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x8000040c]:fdiv.h fp, s1, t2, dyn<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x8000b7d4]<br>0x00007E00<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x8000042c]:fdiv.h t2, t1, fp, dyn<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x8000b7dc]<br>0x00007E00<br> |- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000454]:fdiv.h t1, t2, t0, dyn<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x8000b7e4]<br>0x00007E00<br> |- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000474]:fdiv.h t0, tp, t1, dyn<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x8000b7ec]<br>0x00007E00<br> |- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000494]:fdiv.h tp, t0, gp, dyn<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x8000b7f4]<br>0xFFFF8000<br> |- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004b4]:fdiv.h gp, sp, tp, dyn<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x8000b7fc]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x800004d4]:fdiv.h sp, gp, ra, dyn<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x8000b804]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                     |[0x800004f4]:fdiv.h ra, zero, sp, dyn<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw ra, 40(fp)<br>   |
|  32|[0x8000b80c]<br>0x00000000<br> |- rs1 : x1<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                |[0x80000514]:fdiv.h t6, ra, t5, dyn<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>     |
|  33|[0x8000b814]<br>0x00007E00<br> |- rs2 : x0<br>                                                                                                                                                                                                                                                                                  |[0x80000534]:fdiv.h t6, t5, zero, dyn<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>   |
|  34|[0x8000b81c]<br>0x00000000<br> |- rd : x0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                 |[0x80000554]:fdiv.h zero, t6, t5, dyn<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw zero, 64(fp)<br> |
|  35|[0x8000b824]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000574]:fdiv.h t6, t5, t4, dyn<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw t6, 72(fp)<br>     |
|  36|[0x8000b82c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000594]:fdiv.h t6, t5, t4, dyn<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw t6, 80(fp)<br>     |
|  37|[0x8000b834]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005b4]:fdiv.h t6, t5, t4, dyn<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x8000b83c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005d4]:fdiv.h t6, t5, t4, dyn<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x8000b844]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800005f4]:fdiv.h t6, t5, t4, dyn<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x8000b84c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000614]:fdiv.h t6, t5, t4, dyn<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x8000b854]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000634]:fdiv.h t6, t5, t4, dyn<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x8000b85c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000654]:fdiv.h t6, t5, t4, dyn<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x8000b864]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000674]:fdiv.h t6, t5, t4, dyn<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x8000b86c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000694]:fdiv.h t6, t5, t4, dyn<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x8000b874]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006b4]:fdiv.h t6, t5, t4, dyn<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x8000b87c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006d4]:fdiv.h t6, t5, t4, dyn<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x8000b884]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006f4]:fdiv.h t6, t5, t4, dyn<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x8000b88c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000714]:fdiv.h t6, t5, t4, dyn<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x8000b894]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000734]:fdiv.h t6, t5, t4, dyn<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x8000b89c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000754]:fdiv.h t6, t5, t4, dyn<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x8000b8a4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000774]:fdiv.h t6, t5, t4, dyn<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x8000b8ac]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000794]:fdiv.h t6, t5, t4, dyn<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x8000b8b4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007b4]:fdiv.h t6, t5, t4, dyn<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x8000b8bc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007d4]:fdiv.h t6, t5, t4, dyn<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x8000b8c4]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800007f4]:fdiv.h t6, t5, t4, dyn<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x8000b8cc]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000814]:fdiv.h t6, t5, t4, dyn<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x8000b8d4]<br>0x00001802<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000834]:fdiv.h t6, t5, t4, dyn<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x8000b8dc]<br>0xFFFF9802<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000854]:fdiv.h t6, t5, t4, dyn<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x8000b8e4]<br>0x00001A02<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000874]:fdiv.h t6, t5, t4, dyn<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x8000b8ec]<br>0xFFFF9A02<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000894]:fdiv.h t6, t5, t4, dyn<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x8000b8f4]<br>0x000017FE<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008b4]:fdiv.h t6, t5, t4, dyn<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x8000b8fc]<br>0xFFFF97FE<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008d4]:fdiv.h t6, t5, t4, dyn<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x8000b904]<br>0x00001763<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008f4]:fdiv.h t6, t5, t4, dyn<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x8000b90c]<br>0xFFFF9763<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000914]:fdiv.h t6, t5, t4, dyn<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x8000b914]<br>0x000016DC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000934]:fdiv.h t6, t5, t4, dyn<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x8000b91c]<br>0xFFFF96DC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000954]:fdiv.h t6, t5, t4, dyn<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x8000b924]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000974]:fdiv.h t6, t5, t4, dyn<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x8000b92c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000994]:fdiv.h t6, t5, t4, dyn<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x8000b934]<br>0x00000040<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009b4]:fdiv.h t6, t5, t4, dyn<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x8000b93c]<br>0xFFFF8040<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009d4]:fdiv.h t6, t5, t4, dyn<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x8000b944]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009f4]:fdiv.h t6, t5, t4, dyn<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x8000b94c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a14]:fdiv.h t6, t5, t4, dyn<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x8000b954]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a34]:fdiv.h t6, t5, t4, dyn<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x8000b95c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a54]:fdiv.h t6, t5, t4, dyn<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x8000b964]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a74]:fdiv.h t6, t5, t4, dyn<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x8000b96c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a94]:fdiv.h t6, t5, t4, dyn<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x8000b974]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ab4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x8000b97c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ad4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x8000b984]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000af4]:fdiv.h t6, t5, t4, dyn<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x8000b98c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b14]:fdiv.h t6, t5, t4, dyn<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x8000b994]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b34]:fdiv.h t6, t5, t4, dyn<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x8000b99c]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b54]:fdiv.h t6, t5, t4, dyn<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x8000b9a4]<br>0xFFFF9802<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b74]:fdiv.h t6, t5, t4, dyn<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x8000b9ac]<br>0x00001802<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b94]:fdiv.h t6, t5, t4, dyn<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x8000b9b4]<br>0xFFFF9A02<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x8000b9bc]<br>0x00001A02<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x8000b9c4]<br>0xFFFF97FE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bf4]:fdiv.h t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x8000b9cc]<br>0x000017FE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c14]:fdiv.h t6, t5, t4, dyn<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x8000b9d4]<br>0xFFFF9763<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c34]:fdiv.h t6, t5, t4, dyn<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x8000b9dc]<br>0x00001763<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c54]:fdiv.h t6, t5, t4, dyn<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x8000b9e4]<br>0xFFFF96DC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c74]:fdiv.h t6, t5, t4, dyn<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x8000b9ec]<br>0x000016DC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c94]:fdiv.h t6, t5, t4, dyn<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x8000b9f4]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000cb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x8000b9fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000cd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x8000ba04]<br>0xFFFF8040<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000cf4]:fdiv.h t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x8000ba0c]<br>0x00000040<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d14]:fdiv.h t6, t5, t4, dyn<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x8000ba14]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d34]:fdiv.h t6, t5, t4, dyn<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x8000ba1c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d54]:fdiv.h t6, t5, t4, dyn<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x8000ba24]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d74]:fdiv.h t6, t5, t4, dyn<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x8000ba2c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d94]:fdiv.h t6, t5, t4, dyn<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x8000ba34]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000db4]:fdiv.h t6, t5, t4, dyn<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x8000ba3c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000dd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x8000ba44]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000df4]:fdiv.h t6, t5, t4, dyn<br> [0x80000df8]:csrrs a2, fcsr, zero<br> [0x80000dfc]:sw t6, 616(fp)<br>    |
| 104|[0x8000ba4c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e14]:fdiv.h t6, t5, t4, dyn<br> [0x80000e18]:csrrs a2, fcsr, zero<br> [0x80000e1c]:sw t6, 624(fp)<br>    |
| 105|[0x8000ba54]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e34]:fdiv.h t6, t5, t4, dyn<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 106|[0x8000ba5c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e54]:fdiv.h t6, t5, t4, dyn<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 107|[0x8000ba64]<br>0x00005FFC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e74]:fdiv.h t6, t5, t4, dyn<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 108|[0x8000ba6c]<br>0xFFFFDFFC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e94]:fdiv.h t6, t5, t4, dyn<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
| 109|[0x8000ba74]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000eb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a2, fcsr, zero<br> [0x80000ebc]:sw t6, 664(fp)<br>    |
| 110|[0x8000ba7c]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ed4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a2, fcsr, zero<br> [0x80000edc]:sw t6, 672(fp)<br>    |
| 111|[0x8000ba84]<br>0x00003DFE<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ef4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a2, fcsr, zero<br> [0x80000efc]:sw t6, 680(fp)<br>    |
| 112|[0x8000ba8c]<br>0xFFFFBDFE<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f14]:fdiv.h t6, t5, t4, dyn<br> [0x80000f18]:csrrs a2, fcsr, zero<br> [0x80000f1c]:sw t6, 688(fp)<br>    |
| 113|[0x8000ba94]<br>0x00003BFA<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f34]:fdiv.h t6, t5, t4, dyn<br> [0x80000f38]:csrrs a2, fcsr, zero<br> [0x80000f3c]:sw t6, 696(fp)<br>    |
| 114|[0x8000ba9c]<br>0xFFFFBBFA<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f54]:fdiv.h t6, t5, t4, dyn<br> [0x80000f58]:csrrs a2, fcsr, zero<br> [0x80000f5c]:sw t6, 704(fp)<br>    |
| 115|[0x8000baa4]<br>0x00003B5F<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f74]:fdiv.h t6, t5, t4, dyn<br> [0x80000f78]:csrrs a2, fcsr, zero<br> [0x80000f7c]:sw t6, 712(fp)<br>    |
| 116|[0x8000baac]<br>0xFFFFBB5F<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f94]:fdiv.h t6, t5, t4, dyn<br> [0x80000f98]:csrrs a2, fcsr, zero<br> [0x80000f9c]:sw t6, 720(fp)<br>    |
| 117|[0x8000bab4]<br>0x00003AD9<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000fb4]:fdiv.h t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a2, fcsr, zero<br> [0x80000fbc]:sw t6, 728(fp)<br>    |
| 118|[0x8000babc]<br>0xFFFFBAD9<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000fd4]:fdiv.h t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a2, fcsr, zero<br> [0x80000fdc]:sw t6, 736(fp)<br>    |
| 119|[0x8000bac4]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ff4]:fdiv.h t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a2, fcsr, zero<br> [0x80000ffc]:sw t6, 744(fp)<br>    |
| 120|[0x8000bacc]<br>0xFFFF8010<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001014]:fdiv.h t6, t5, t4, dyn<br> [0x80001018]:csrrs a2, fcsr, zero<br> [0x8000101c]:sw t6, 752(fp)<br>    |
| 121|[0x8000bad4]<br>0x000017FC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001034]:fdiv.h t6, t5, t4, dyn<br> [0x80001038]:csrrs a2, fcsr, zero<br> [0x8000103c]:sw t6, 760(fp)<br>    |
| 122|[0x8000badc]<br>0xFFFF97FC<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001054]:fdiv.h t6, t5, t4, dyn<br> [0x80001058]:csrrs a2, fcsr, zero<br> [0x8000105c]:sw t6, 768(fp)<br>    |
| 123|[0x8000bae4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001074]:fdiv.h t6, t5, t4, dyn<br> [0x80001078]:csrrs a2, fcsr, zero<br> [0x8000107c]:sw t6, 776(fp)<br>    |
| 124|[0x8000baec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001094]:fdiv.h t6, t5, t4, dyn<br> [0x80001098]:csrrs a2, fcsr, zero<br> [0x8000109c]:sw t6, 784(fp)<br>    |
| 125|[0x8000baf4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010b4]:fdiv.h t6, t5, t4, dyn<br> [0x800010b8]:csrrs a2, fcsr, zero<br> [0x800010bc]:sw t6, 792(fp)<br>    |
| 126|[0x8000bafc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010d4]:fdiv.h t6, t5, t4, dyn<br> [0x800010d8]:csrrs a2, fcsr, zero<br> [0x800010dc]:sw t6, 800(fp)<br>    |
| 127|[0x8000bb04]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010f4]:fdiv.h t6, t5, t4, dyn<br> [0x800010f8]:csrrs a2, fcsr, zero<br> [0x800010fc]:sw t6, 808(fp)<br>    |
| 128|[0x8000bb0c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001114]:fdiv.h t6, t5, t4, dyn<br> [0x80001118]:csrrs a2, fcsr, zero<br> [0x8000111c]:sw t6, 816(fp)<br>    |
| 129|[0x8000bb14]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001134]:fdiv.h t6, t5, t4, dyn<br> [0x80001138]:csrrs a2, fcsr, zero<br> [0x8000113c]:sw t6, 824(fp)<br>    |
| 130|[0x8000bb1c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001154]:fdiv.h t6, t5, t4, dyn<br> [0x80001158]:csrrs a2, fcsr, zero<br> [0x8000115c]:sw t6, 832(fp)<br>    |
| 131|[0x8000bb24]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001174]:fdiv.h t6, t5, t4, dyn<br> [0x80001178]:csrrs a2, fcsr, zero<br> [0x8000117c]:sw t6, 840(fp)<br>    |
| 132|[0x8000bb2c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001194]:fdiv.h t6, t5, t4, dyn<br> [0x80001198]:csrrs a2, fcsr, zero<br> [0x8000119c]:sw t6, 848(fp)<br>    |
| 133|[0x8000bb34]<br>0xFFFFDFFC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011b4]:fdiv.h t6, t5, t4, dyn<br> [0x800011b8]:csrrs a2, fcsr, zero<br> [0x800011bc]:sw t6, 856(fp)<br>    |
| 134|[0x8000bb3c]<br>0x00005FFC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011d4]:fdiv.h t6, t5, t4, dyn<br> [0x800011d8]:csrrs a2, fcsr, zero<br> [0x800011dc]:sw t6, 864(fp)<br>    |
| 135|[0x8000bb44]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800011f4]:fdiv.h t6, t5, t4, dyn<br> [0x800011f8]:csrrs a2, fcsr, zero<br> [0x800011fc]:sw t6, 872(fp)<br>    |
| 136|[0x8000bb4c]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001214]:fdiv.h t6, t5, t4, dyn<br> [0x80001218]:csrrs a2, fcsr, zero<br> [0x8000121c]:sw t6, 880(fp)<br>    |
| 137|[0x8000bb54]<br>0xFFFFBDFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001234]:fdiv.h t6, t5, t4, dyn<br> [0x80001238]:csrrs a2, fcsr, zero<br> [0x8000123c]:sw t6, 888(fp)<br>    |
| 138|[0x8000bb5c]<br>0x00003DFE<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001254]:fdiv.h t6, t5, t4, dyn<br> [0x80001258]:csrrs a2, fcsr, zero<br> [0x8000125c]:sw t6, 896(fp)<br>    |
| 139|[0x8000bb64]<br>0xFFFFBBFA<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001274]:fdiv.h t6, t5, t4, dyn<br> [0x80001278]:csrrs a2, fcsr, zero<br> [0x8000127c]:sw t6, 904(fp)<br>    |
| 140|[0x8000bb6c]<br>0x00003BFA<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001294]:fdiv.h t6, t5, t4, dyn<br> [0x80001298]:csrrs a2, fcsr, zero<br> [0x8000129c]:sw t6, 912(fp)<br>    |
| 141|[0x8000bb74]<br>0xFFFFBB5F<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012b4]:fdiv.h t6, t5, t4, dyn<br> [0x800012b8]:csrrs a2, fcsr, zero<br> [0x800012bc]:sw t6, 920(fp)<br>    |
| 142|[0x8000bb7c]<br>0x00003B5F<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012d4]:fdiv.h t6, t5, t4, dyn<br> [0x800012d8]:csrrs a2, fcsr, zero<br> [0x800012dc]:sw t6, 928(fp)<br>    |
| 143|[0x8000bb84]<br>0xFFFFBAD9<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012f4]:fdiv.h t6, t5, t4, dyn<br> [0x800012f8]:csrrs a2, fcsr, zero<br> [0x800012fc]:sw t6, 936(fp)<br>    |
| 144|[0x8000bb8c]<br>0x00003AD9<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001314]:fdiv.h t6, t5, t4, dyn<br> [0x80001318]:csrrs a2, fcsr, zero<br> [0x8000131c]:sw t6, 944(fp)<br>    |
| 145|[0x8000bb94]<br>0xFFFF8010<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001334]:fdiv.h t6, t5, t4, dyn<br> [0x80001338]:csrrs a2, fcsr, zero<br> [0x8000133c]:sw t6, 952(fp)<br>    |
| 146|[0x8000bb9c]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001354]:fdiv.h t6, t5, t4, dyn<br> [0x80001358]:csrrs a2, fcsr, zero<br> [0x8000135c]:sw t6, 960(fp)<br>    |
| 147|[0x8000bba4]<br>0xFFFF97FC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001374]:fdiv.h t6, t5, t4, dyn<br> [0x80001378]:csrrs a2, fcsr, zero<br> [0x8000137c]:sw t6, 968(fp)<br>    |
| 148|[0x8000bbac]<br>0x000017FC<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001394]:fdiv.h t6, t5, t4, dyn<br> [0x80001398]:csrrs a2, fcsr, zero<br> [0x8000139c]:sw t6, 976(fp)<br>    |
| 149|[0x8000bbb4]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013b4]:fdiv.h t6, t5, t4, dyn<br> [0x800013b8]:csrrs a2, fcsr, zero<br> [0x800013bc]:sw t6, 984(fp)<br>    |
| 150|[0x8000bbbc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013d4]:fdiv.h t6, t5, t4, dyn<br> [0x800013d8]:csrrs a2, fcsr, zero<br> [0x800013dc]:sw t6, 992(fp)<br>    |
| 151|[0x8000bbc4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013f4]:fdiv.h t6, t5, t4, dyn<br> [0x800013f8]:csrrs a2, fcsr, zero<br> [0x800013fc]:sw t6, 1000(fp)<br>   |
| 152|[0x8000bbcc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001414]:fdiv.h t6, t5, t4, dyn<br> [0x80001418]:csrrs a2, fcsr, zero<br> [0x8000141c]:sw t6, 1008(fp)<br>   |
| 153|[0x8000bbd4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001434]:fdiv.h t6, t5, t4, dyn<br> [0x80001438]:csrrs a2, fcsr, zero<br> [0x8000143c]:sw t6, 1016(fp)<br>   |
| 154|[0x8000bbdc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000145c]:fdiv.h t6, t5, t4, dyn<br> [0x80001460]:csrrs a2, fcsr, zero<br> [0x80001464]:sw t6, 0(fp)<br>      |
| 155|[0x8000bbe4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000147c]:fdiv.h t6, t5, t4, dyn<br> [0x80001480]:csrrs a2, fcsr, zero<br> [0x80001484]:sw t6, 8(fp)<br>      |
| 156|[0x8000bbec]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000149c]:fdiv.h t6, t5, t4, dyn<br> [0x800014a0]:csrrs a2, fcsr, zero<br> [0x800014a4]:sw t6, 16(fp)<br>     |
| 157|[0x8000bbf4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014bc]:fdiv.h t6, t5, t4, dyn<br> [0x800014c0]:csrrs a2, fcsr, zero<br> [0x800014c4]:sw t6, 24(fp)<br>     |
| 158|[0x8000bbfc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014dc]:fdiv.h t6, t5, t4, dyn<br> [0x800014e0]:csrrs a2, fcsr, zero<br> [0x800014e4]:sw t6, 32(fp)<br>     |
| 159|[0x8000bc04]<br>0x00005D54<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800014fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001500]:csrrs a2, fcsr, zero<br> [0x80001504]:sw t6, 40(fp)<br>     |
| 160|[0x8000bc0c]<br>0xFFFFDD54<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000151c]:fdiv.h t6, t5, t4, dyn<br> [0x80001520]:csrrs a2, fcsr, zero<br> [0x80001524]:sw t6, 48(fp)<br>     |
| 161|[0x8000bc14]<br>0x00003957<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000153c]:fdiv.h t6, t5, t4, dyn<br> [0x80001540]:csrrs a2, fcsr, zero<br> [0x80001544]:sw t6, 56(fp)<br>     |
| 162|[0x8000bc1c]<br>0xFFFFB957<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000155c]:fdiv.h t6, t5, t4, dyn<br> [0x80001560]:csrrs a2, fcsr, zero<br> [0x80001564]:sw t6, 64(fp)<br>     |
| 163|[0x8000bc24]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000157c]:fdiv.h t6, t5, t4, dyn<br> [0x80001580]:csrrs a2, fcsr, zero<br> [0x80001584]:sw t6, 72(fp)<br>     |
| 164|[0x8000bc2c]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000159c]:fdiv.h t6, t5, t4, dyn<br> [0x800015a0]:csrrs a2, fcsr, zero<br> [0x800015a4]:sw t6, 80(fp)<br>     |
| 165|[0x8000bc34]<br>0x00003953<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015bc]:fdiv.h t6, t5, t4, dyn<br> [0x800015c0]:csrrs a2, fcsr, zero<br> [0x800015c4]:sw t6, 88(fp)<br>     |
| 166|[0x8000bc3c]<br>0xFFFFB953<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015dc]:fdiv.h t6, t5, t4, dyn<br> [0x800015e0]:csrrs a2, fcsr, zero<br> [0x800015e4]:sw t6, 96(fp)<br>     |
| 167|[0x8000bc44]<br>0x000038EB<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001600]:csrrs a2, fcsr, zero<br> [0x80001604]:sw t6, 104(fp)<br>    |
| 168|[0x8000bc4c]<br>0xFFFFB8EB<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000161c]:fdiv.h t6, t5, t4, dyn<br> [0x80001620]:csrrs a2, fcsr, zero<br> [0x80001624]:sw t6, 112(fp)<br>    |
| 169|[0x8000bc54]<br>0x00003892<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000163c]:fdiv.h t6, t5, t4, dyn<br> [0x80001640]:csrrs a2, fcsr, zero<br> [0x80001644]:sw t6, 120(fp)<br>    |
| 170|[0x8000bc5c]<br>0xFFFFB892<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000165c]:fdiv.h t6, t5, t4, dyn<br> [0x80001660]:csrrs a2, fcsr, zero<br> [0x80001664]:sw t6, 128(fp)<br>    |
| 171|[0x8000bc64]<br>0x0000000B<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000167c]:fdiv.h t6, t5, t4, dyn<br> [0x80001680]:csrrs a2, fcsr, zero<br> [0x80001684]:sw t6, 136(fp)<br>    |
| 172|[0x8000bc6c]<br>0xFFFF800B<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000169c]:fdiv.h t6, t5, t4, dyn<br> [0x800016a0]:csrrs a2, fcsr, zero<br> [0x800016a4]:sw t6, 144(fp)<br>    |
| 173|[0x8000bc74]<br>0x00001554<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016bc]:fdiv.h t6, t5, t4, dyn<br> [0x800016c0]:csrrs a2, fcsr, zero<br> [0x800016c4]:sw t6, 152(fp)<br>    |
| 174|[0x8000bc7c]<br>0xFFFF9554<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016dc]:fdiv.h t6, t5, t4, dyn<br> [0x800016e0]:csrrs a2, fcsr, zero<br> [0x800016e4]:sw t6, 160(fp)<br>    |
| 175|[0x8000bc84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800016fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001700]:csrrs a2, fcsr, zero<br> [0x80001704]:sw t6, 168(fp)<br>    |
| 176|[0x8000bc8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000171c]:fdiv.h t6, t5, t4, dyn<br> [0x80001720]:csrrs a2, fcsr, zero<br> [0x80001724]:sw t6, 176(fp)<br>    |
| 177|[0x8000bc94]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000173c]:fdiv.h t6, t5, t4, dyn<br> [0x80001740]:csrrs a2, fcsr, zero<br> [0x80001744]:sw t6, 184(fp)<br>    |
| 178|[0x8000bc9c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000175c]:fdiv.h t6, t5, t4, dyn<br> [0x80001760]:csrrs a2, fcsr, zero<br> [0x80001764]:sw t6, 192(fp)<br>    |
| 179|[0x8000bca4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000177c]:fdiv.h t6, t5, t4, dyn<br> [0x80001780]:csrrs a2, fcsr, zero<br> [0x80001784]:sw t6, 200(fp)<br>    |
| 180|[0x8000bcac]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000179c]:fdiv.h t6, t5, t4, dyn<br> [0x800017a0]:csrrs a2, fcsr, zero<br> [0x800017a4]:sw t6, 208(fp)<br>    |
| 181|[0x8000bcb4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017bc]:fdiv.h t6, t5, t4, dyn<br> [0x800017c0]:csrrs a2, fcsr, zero<br> [0x800017c4]:sw t6, 216(fp)<br>    |
| 182|[0x8000bcbc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017dc]:fdiv.h t6, t5, t4, dyn<br> [0x800017e0]:csrrs a2, fcsr, zero<br> [0x800017e4]:sw t6, 224(fp)<br>    |
| 183|[0x8000bcc4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001800]:csrrs a2, fcsr, zero<br> [0x80001804]:sw t6, 232(fp)<br>    |
| 184|[0x8000bccc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000181c]:fdiv.h t6, t5, t4, dyn<br> [0x80001820]:csrrs a2, fcsr, zero<br> [0x80001824]:sw t6, 240(fp)<br>    |
| 185|[0x8000bcd4]<br>0xFFFFDD54<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000183c]:fdiv.h t6, t5, t4, dyn<br> [0x80001840]:csrrs a2, fcsr, zero<br> [0x80001844]:sw t6, 248(fp)<br>    |
| 186|[0x8000bcdc]<br>0x00005D54<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000185c]:fdiv.h t6, t5, t4, dyn<br> [0x80001860]:csrrs a2, fcsr, zero<br> [0x80001864]:sw t6, 256(fp)<br>    |
| 187|[0x8000bce4]<br>0xFFFFB957<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000187c]:fdiv.h t6, t5, t4, dyn<br> [0x80001880]:csrrs a2, fcsr, zero<br> [0x80001884]:sw t6, 264(fp)<br>    |
| 188|[0x8000bcec]<br>0x00003957<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000189c]:fdiv.h t6, t5, t4, dyn<br> [0x800018a0]:csrrs a2, fcsr, zero<br> [0x800018a4]:sw t6, 272(fp)<br>    |
| 189|[0x8000bcf4]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018bc]:fdiv.h t6, t5, t4, dyn<br> [0x800018c0]:csrrs a2, fcsr, zero<br> [0x800018c4]:sw t6, 280(fp)<br>    |
| 190|[0x8000bcfc]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018dc]:fdiv.h t6, t5, t4, dyn<br> [0x800018e0]:csrrs a2, fcsr, zero<br> [0x800018e4]:sw t6, 288(fp)<br>    |
| 191|[0x8000bd04]<br>0xFFFFB953<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001900]:csrrs a2, fcsr, zero<br> [0x80001904]:sw t6, 296(fp)<br>    |
| 192|[0x8000bd0c]<br>0x00003953<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000191c]:fdiv.h t6, t5, t4, dyn<br> [0x80001920]:csrrs a2, fcsr, zero<br> [0x80001924]:sw t6, 304(fp)<br>    |
| 193|[0x8000bd14]<br>0xFFFFB8EB<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000193c]:fdiv.h t6, t5, t4, dyn<br> [0x80001940]:csrrs a2, fcsr, zero<br> [0x80001944]:sw t6, 312(fp)<br>    |
| 194|[0x8000bd1c]<br>0x000038EB<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000195c]:fdiv.h t6, t5, t4, dyn<br> [0x80001960]:csrrs a2, fcsr, zero<br> [0x80001964]:sw t6, 320(fp)<br>    |
| 195|[0x8000bd24]<br>0xFFFFB892<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000197c]:fdiv.h t6, t5, t4, dyn<br> [0x80001980]:csrrs a2, fcsr, zero<br> [0x80001984]:sw t6, 328(fp)<br>    |
| 196|[0x8000bd2c]<br>0x00003892<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000199c]:fdiv.h t6, t5, t4, dyn<br> [0x800019a0]:csrrs a2, fcsr, zero<br> [0x800019a4]:sw t6, 336(fp)<br>    |
| 197|[0x8000bd34]<br>0xFFFF800B<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019bc]:fdiv.h t6, t5, t4, dyn<br> [0x800019c0]:csrrs a2, fcsr, zero<br> [0x800019c4]:sw t6, 344(fp)<br>    |
| 198|[0x8000bd3c]<br>0x0000000B<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019dc]:fdiv.h t6, t5, t4, dyn<br> [0x800019e0]:csrrs a2, fcsr, zero<br> [0x800019e4]:sw t6, 352(fp)<br>    |
| 199|[0x8000bd44]<br>0xFFFF9554<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800019fc]:fdiv.h t6, t5, t4, dyn<br> [0x80001a00]:csrrs a2, fcsr, zero<br> [0x80001a04]:sw t6, 360(fp)<br>    |
| 200|[0x8000bd4c]<br>0x00001554<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001a20]:csrrs a2, fcsr, zero<br> [0x80001a24]:sw t6, 368(fp)<br>    |
| 201|[0x8000bd54]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001a40]:csrrs a2, fcsr, zero<br> [0x80001a44]:sw t6, 376(fp)<br>    |
| 202|[0x8000bd5c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001a60]:csrrs a2, fcsr, zero<br> [0x80001a64]:sw t6, 384(fp)<br>    |
| 203|[0x8000bd64]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001a80]:csrrs a2, fcsr, zero<br> [0x80001a84]:sw t6, 392(fp)<br>    |
| 204|[0x8000bd6c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001aa0]:csrrs a2, fcsr, zero<br> [0x80001aa4]:sw t6, 400(fp)<br>    |
| 205|[0x8000bd74]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001abc]:fdiv.h t6, t5, t4, dyn<br> [0x80001ac0]:csrrs a2, fcsr, zero<br> [0x80001ac4]:sw t6, 408(fp)<br>    |
| 206|[0x8000bd7c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001adc]:fdiv.h t6, t5, t4, dyn<br> [0x80001ae0]:csrrs a2, fcsr, zero<br> [0x80001ae4]:sw t6, 416(fp)<br>    |
| 207|[0x8000bd84]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001afc]:fdiv.h t6, t5, t4, dyn<br> [0x80001b00]:csrrs a2, fcsr, zero<br> [0x80001b04]:sw t6, 424(fp)<br>    |
| 208|[0x8000bd8c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x2aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001b20]:csrrs a2, fcsr, zero<br> [0x80001b24]:sw t6, 432(fp)<br>    |
| 209|[0x8000bd94]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001b40]:csrrs a2, fcsr, zero<br> [0x80001b44]:sw t6, 440(fp)<br>    |
| 210|[0x8000bd9c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001b60]:csrrs a2, fcsr, zero<br> [0x80001b64]:sw t6, 448(fp)<br>    |
| 211|[0x8000bda4]<br>0x00006001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001b80]:csrrs a2, fcsr, zero<br> [0x80001b84]:sw t6, 456(fp)<br>    |
| 212|[0x8000bdac]<br>0xFFFFE001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001ba0]:csrrs a2, fcsr, zero<br> [0x80001ba4]:sw t6, 464(fp)<br>    |
| 213|[0x8000bdb4]<br>0x00003C03<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bbc]:fdiv.h t6, t5, t4, dyn<br> [0x80001bc0]:csrrs a2, fcsr, zero<br> [0x80001bc4]:sw t6, 472(fp)<br>    |
| 214|[0x8000bdbc]<br>0xFFFFBC03<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bdc]:fdiv.h t6, t5, t4, dyn<br> [0x80001be0]:csrrs a2, fcsr, zero<br> [0x80001be4]:sw t6, 480(fp)<br>    |
| 215|[0x8000bdc4]<br>0x00003E03<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001bfc]:fdiv.h t6, t5, t4, dyn<br> [0x80001c00]:csrrs a2, fcsr, zero<br> [0x80001c04]:sw t6, 488(fp)<br>    |
| 216|[0x8000bdcc]<br>0xFFFFBE03<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001c20]:csrrs a2, fcsr, zero<br> [0x80001c24]:sw t6, 496(fp)<br>    |
| 217|[0x8000bdd4]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001c40]:csrrs a2, fcsr, zero<br> [0x80001c44]:sw t6, 504(fp)<br>    |
| 218|[0x8000bddc]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001c60]:csrrs a2, fcsr, zero<br> [0x80001c64]:sw t6, 512(fp)<br>    |
| 219|[0x8000bde4]<br>0x00003B65<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001c80]:csrrs a2, fcsr, zero<br> [0x80001c84]:sw t6, 520(fp)<br>    |
| 220|[0x8000bdec]<br>0xFFFFBB65<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001ca0]:csrrs a2, fcsr, zero<br> [0x80001ca4]:sw t6, 528(fp)<br>    |
| 221|[0x8000bdf4]<br>0x00003ADE<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cbc]:fdiv.h t6, t5, t4, dyn<br> [0x80001cc0]:csrrs a2, fcsr, zero<br> [0x80001cc4]:sw t6, 536(fp)<br>    |
| 222|[0x8000bdfc]<br>0xFFFFBADE<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cdc]:fdiv.h t6, t5, t4, dyn<br> [0x80001ce0]:csrrs a2, fcsr, zero<br> [0x80001ce4]:sw t6, 544(fp)<br>    |
| 223|[0x8000be04]<br>0x00000010<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cfc]:fdiv.h t6, t5, t4, dyn<br> [0x80001d00]:csrrs a2, fcsr, zero<br> [0x80001d04]:sw t6, 552(fp)<br>    |
| 224|[0x8000be0c]<br>0xFFFF8010<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001d20]:csrrs a2, fcsr, zero<br> [0x80001d24]:sw t6, 560(fp)<br>    |
| 225|[0x8000be14]<br>0x00001801<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001d40]:csrrs a2, fcsr, zero<br> [0x80001d44]:sw t6, 568(fp)<br>    |
| 226|[0x8000be1c]<br>0xFFFF9801<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001d60]:csrrs a2, fcsr, zero<br> [0x80001d64]:sw t6, 576(fp)<br>    |
| 227|[0x8000be24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001d80]:csrrs a2, fcsr, zero<br> [0x80001d84]:sw t6, 584(fp)<br>    |
| 228|[0x8000be2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001da0]:csrrs a2, fcsr, zero<br> [0x80001da4]:sw t6, 592(fp)<br>    |
| 229|[0x8000be34]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dbc]:fdiv.h t6, t5, t4, dyn<br> [0x80001dc0]:csrrs a2, fcsr, zero<br> [0x80001dc4]:sw t6, 600(fp)<br>    |
| 230|[0x8000be3c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001ddc]:fdiv.h t6, t5, t4, dyn<br> [0x80001de0]:csrrs a2, fcsr, zero<br> [0x80001de4]:sw t6, 608(fp)<br>    |
| 231|[0x8000be44]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dfc]:fdiv.h t6, t5, t4, dyn<br> [0x80001e00]:csrrs a2, fcsr, zero<br> [0x80001e04]:sw t6, 616(fp)<br>    |
| 232|[0x8000be4c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001e20]:csrrs a2, fcsr, zero<br> [0x80001e24]:sw t6, 624(fp)<br>    |
| 233|[0x8000be54]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001e40]:csrrs a2, fcsr, zero<br> [0x80001e44]:sw t6, 632(fp)<br>    |
| 234|[0x8000be5c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001e60]:csrrs a2, fcsr, zero<br> [0x80001e64]:sw t6, 640(fp)<br>    |
| 235|[0x8000be64]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001e80]:csrrs a2, fcsr, zero<br> [0x80001e84]:sw t6, 648(fp)<br>    |
| 236|[0x8000be6c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001ea0]:csrrs a2, fcsr, zero<br> [0x80001ea4]:sw t6, 656(fp)<br>    |
| 237|[0x8000be74]<br>0xFFFFE001<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001ebc]:fdiv.h t6, t5, t4, dyn<br> [0x80001ec0]:csrrs a2, fcsr, zero<br> [0x80001ec4]:sw t6, 664(fp)<br>    |
| 238|[0x8000be7c]<br>0x00006001<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001edc]:fdiv.h t6, t5, t4, dyn<br> [0x80001ee0]:csrrs a2, fcsr, zero<br> [0x80001ee4]:sw t6, 672(fp)<br>    |
| 239|[0x8000be84]<br>0xFFFFBC03<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001efc]:fdiv.h t6, t5, t4, dyn<br> [0x80001f00]:csrrs a2, fcsr, zero<br> [0x80001f04]:sw t6, 680(fp)<br>    |
| 240|[0x8000be8c]<br>0x00003C03<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f1c]:fdiv.h t6, t5, t4, dyn<br> [0x80001f20]:csrrs a2, fcsr, zero<br> [0x80001f24]:sw t6, 688(fp)<br>    |
| 241|[0x8000be94]<br>0xFFFFBE03<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f3c]:fdiv.h t6, t5, t4, dyn<br> [0x80001f40]:csrrs a2, fcsr, zero<br> [0x80001f44]:sw t6, 696(fp)<br>    |
| 242|[0x8000be9c]<br>0x00003E03<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f5c]:fdiv.h t6, t5, t4, dyn<br> [0x80001f60]:csrrs a2, fcsr, zero<br> [0x80001f64]:sw t6, 704(fp)<br>    |
| 243|[0x8000bea4]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f7c]:fdiv.h t6, t5, t4, dyn<br> [0x80001f80]:csrrs a2, fcsr, zero<br> [0x80001f84]:sw t6, 712(fp)<br>    |
| 244|[0x8000beac]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f9c]:fdiv.h t6, t5, t4, dyn<br> [0x80001fa0]:csrrs a2, fcsr, zero<br> [0x80001fa4]:sw t6, 720(fp)<br>    |
| 245|[0x8000beb4]<br>0xFFFFBB65<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fbc]:fdiv.h t6, t5, t4, dyn<br> [0x80001fc0]:csrrs a2, fcsr, zero<br> [0x80001fc4]:sw t6, 728(fp)<br>    |
| 246|[0x8000bebc]<br>0x00003B65<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fdc]:fdiv.h t6, t5, t4, dyn<br> [0x80001fe0]:csrrs a2, fcsr, zero<br> [0x80001fe4]:sw t6, 736(fp)<br>    |
| 247|[0x8000bec4]<br>0xFFFFBADE<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001ffc]:fdiv.h t6, t5, t4, dyn<br> [0x80002000]:csrrs a2, fcsr, zero<br> [0x80002004]:sw t6, 744(fp)<br>    |
| 248|[0x8000becc]<br>0x00003ADE<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000201c]:fdiv.h t6, t5, t4, dyn<br> [0x80002020]:csrrs a2, fcsr, zero<br> [0x80002024]:sw t6, 752(fp)<br>    |
| 249|[0x8000bed4]<br>0xFFFF8010<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000203c]:fdiv.h t6, t5, t4, dyn<br> [0x80002040]:csrrs a2, fcsr, zero<br> [0x80002044]:sw t6, 760(fp)<br>    |
| 250|[0x8000bedc]<br>0x00000010<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000205c]:fdiv.h t6, t5, t4, dyn<br> [0x80002060]:csrrs a2, fcsr, zero<br> [0x80002064]:sw t6, 768(fp)<br>    |
| 251|[0x8000bee4]<br>0xFFFF9801<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000207c]:fdiv.h t6, t5, t4, dyn<br> [0x80002080]:csrrs a2, fcsr, zero<br> [0x80002084]:sw t6, 776(fp)<br>    |
| 252|[0x8000beec]<br>0x00001801<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000209c]:fdiv.h t6, t5, t4, dyn<br> [0x800020a0]:csrrs a2, fcsr, zero<br> [0x800020a4]:sw t6, 784(fp)<br>    |
| 253|[0x8000bef4]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020bc]:fdiv.h t6, t5, t4, dyn<br> [0x800020c0]:csrrs a2, fcsr, zero<br> [0x800020c4]:sw t6, 792(fp)<br>    |
| 254|[0x8000befc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020dc]:fdiv.h t6, t5, t4, dyn<br> [0x800020e0]:csrrs a2, fcsr, zero<br> [0x800020e4]:sw t6, 800(fp)<br>    |
| 255|[0x8000bf04]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800020fc]:fdiv.h t6, t5, t4, dyn<br> [0x80002100]:csrrs a2, fcsr, zero<br> [0x80002104]:sw t6, 808(fp)<br>    |
| 256|[0x8000bf0c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000211c]:fdiv.h t6, t5, t4, dyn<br> [0x80002120]:csrrs a2, fcsr, zero<br> [0x80002124]:sw t6, 816(fp)<br>    |
| 257|[0x8000bf14]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000213c]:fdiv.h t6, t5, t4, dyn<br> [0x80002140]:csrrs a2, fcsr, zero<br> [0x80002144]:sw t6, 824(fp)<br>    |
| 258|[0x8000bf1c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000215c]:fdiv.h t6, t5, t4, dyn<br> [0x80002160]:csrrs a2, fcsr, zero<br> [0x80002164]:sw t6, 832(fp)<br>    |
| 259|[0x8000bf24]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000217c]:fdiv.h t6, t5, t4, dyn<br> [0x80002180]:csrrs a2, fcsr, zero<br> [0x80002184]:sw t6, 840(fp)<br>    |
| 260|[0x8000bf2c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000219c]:fdiv.h t6, t5, t4, dyn<br> [0x800021a0]:csrrs a2, fcsr, zero<br> [0x800021a4]:sw t6, 848(fp)<br>    |
| 261|[0x8000bf34]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800021bc]:fdiv.h t6, t5, t4, dyn<br> [0x800021c0]:csrrs a2, fcsr, zero<br> [0x800021c4]:sw t6, 856(fp)<br>    |
| 262|[0x8000bf3c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800021dc]:fdiv.h t6, t5, t4, dyn<br> [0x800021e0]:csrrs a2, fcsr, zero<br> [0x800021e4]:sw t6, 864(fp)<br>    |
| 263|[0x8000bf44]<br>0x00006055<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800021fc]:fdiv.h t6, t5, t4, dyn<br> [0x80002200]:csrrs a2, fcsr, zero<br> [0x80002204]:sw t6, 872(fp)<br>    |
| 264|[0x8000bf4c]<br>0xFFFFE055<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000221c]:fdiv.h t6, t5, t4, dyn<br> [0x80002220]:csrrs a2, fcsr, zero<br> [0x80002224]:sw t6, 880(fp)<br>    |
| 265|[0x8000bf54]<br>0x00003C57<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000223c]:fdiv.h t6, t5, t4, dyn<br> [0x80002240]:csrrs a2, fcsr, zero<br> [0x80002244]:sw t6, 888(fp)<br>    |
| 266|[0x8000bf5c]<br>0xFFFFBC57<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000225c]:fdiv.h t6, t5, t4, dyn<br> [0x80002260]:csrrs a2, fcsr, zero<br> [0x80002264]:sw t6, 896(fp)<br>    |
| 267|[0x8000bf64]<br>0x00003E81<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000227c]:fdiv.h t6, t5, t4, dyn<br> [0x80002280]:csrrs a2, fcsr, zero<br> [0x80002284]:sw t6, 904(fp)<br>    |
| 268|[0x8000bf6c]<br>0xFFFFBE81<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000229c]:fdiv.h t6, t5, t4, dyn<br> [0x800022a0]:csrrs a2, fcsr, zero<br> [0x800022a4]:sw t6, 912(fp)<br>    |
| 269|[0x8000bf74]<br>0x00003C54<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800022bc]:fdiv.h t6, t5, t4, dyn<br> [0x800022c0]:csrrs a2, fcsr, zero<br> [0x800022c4]:sw t6, 920(fp)<br>    |
| 270|[0x8000bf7c]<br>0xFFFFBC54<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800022dc]:fdiv.h t6, t5, t4, dyn<br> [0x800022e0]:csrrs a2, fcsr, zero<br> [0x800022e4]:sw t6, 928(fp)<br>    |
| 271|[0x8000bf84]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800022fc]:fdiv.h t6, t5, t4, dyn<br> [0x80002300]:csrrs a2, fcsr, zero<br> [0x80002304]:sw t6, 936(fp)<br>    |
| 272|[0x8000bf8c]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000231c]:fdiv.h t6, t5, t4, dyn<br> [0x80002320]:csrrs a2, fcsr, zero<br> [0x80002324]:sw t6, 944(fp)<br>    |
| 273|[0x8000bf94]<br>0x00003B6E<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000233c]:fdiv.h t6, t5, t4, dyn<br> [0x80002340]:csrrs a2, fcsr, zero<br> [0x80002344]:sw t6, 952(fp)<br>    |
| 274|[0x8000bf9c]<br>0xFFFFBB6E<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000235c]:fdiv.h t6, t5, t4, dyn<br> [0x80002360]:csrrs a2, fcsr, zero<br> [0x80002364]:sw t6, 960(fp)<br>    |
| 275|[0x8000bfa4]<br>0x00000011<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000237c]:fdiv.h t6, t5, t4, dyn<br> [0x80002380]:csrrs a2, fcsr, zero<br> [0x80002384]:sw t6, 968(fp)<br>    |
| 276|[0x8000bfac]<br>0xFFFF8011<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000239c]:fdiv.h t6, t5, t4, dyn<br> [0x800023a0]:csrrs a2, fcsr, zero<br> [0x800023a4]:sw t6, 976(fp)<br>    |
| 277|[0x8000bfb4]<br>0x00001855<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800023bc]:fdiv.h t6, t5, t4, dyn<br> [0x800023c0]:csrrs a2, fcsr, zero<br> [0x800023c4]:sw t6, 984(fp)<br>    |
| 278|[0x8000bfbc]<br>0xFFFF9855<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800023dc]:fdiv.h t6, t5, t4, dyn<br> [0x800023e0]:csrrs a2, fcsr, zero<br> [0x800023e4]:sw t6, 992(fp)<br>    |
| 279|[0x8000bfc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000241c]:fdiv.h t6, t5, t4, dyn<br> [0x80002420]:csrrs a2, fcsr, zero<br> [0x80002424]:sw t6, 1000(fp)<br>   |
| 280|[0x8000bfcc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000245c]:fdiv.h t6, t5, t4, dyn<br> [0x80002460]:csrrs a2, fcsr, zero<br> [0x80002464]:sw t6, 1008(fp)<br>   |
| 281|[0x8000bfd4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000249c]:fdiv.h t6, t5, t4, dyn<br> [0x800024a0]:csrrs a2, fcsr, zero<br> [0x800024a4]:sw t6, 1016(fp)<br>   |
| 282|[0x8000bfdc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800024e4]:fdiv.h t6, t5, t4, dyn<br> [0x800024e8]:csrrs a2, fcsr, zero<br> [0x800024ec]:sw t6, 0(fp)<br>      |
| 283|[0x8000bfe4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002524]:fdiv.h t6, t5, t4, dyn<br> [0x80002528]:csrrs a2, fcsr, zero<br> [0x8000252c]:sw t6, 8(fp)<br>      |
| 284|[0x8000bfec]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002564]:fdiv.h t6, t5, t4, dyn<br> [0x80002568]:csrrs a2, fcsr, zero<br> [0x8000256c]:sw t6, 16(fp)<br>     |
| 285|[0x8000bff4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800025a4]:fdiv.h t6, t5, t4, dyn<br> [0x800025a8]:csrrs a2, fcsr, zero<br> [0x800025ac]:sw t6, 24(fp)<br>     |
| 286|[0x8000bffc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800025e4]:fdiv.h t6, t5, t4, dyn<br> [0x800025e8]:csrrs a2, fcsr, zero<br> [0x800025ec]:sw t6, 32(fp)<br>     |
| 287|[0x8000c004]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002624]:fdiv.h t6, t5, t4, dyn<br> [0x80002628]:csrrs a2, fcsr, zero<br> [0x8000262c]:sw t6, 40(fp)<br>     |
| 288|[0x8000c00c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002664]:fdiv.h t6, t5, t4, dyn<br> [0x80002668]:csrrs a2, fcsr, zero<br> [0x8000266c]:sw t6, 48(fp)<br>     |
| 289|[0x8000c014]<br>0xFFFFE055<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800026a4]:fdiv.h t6, t5, t4, dyn<br> [0x800026a8]:csrrs a2, fcsr, zero<br> [0x800026ac]:sw t6, 56(fp)<br>     |
| 290|[0x8000c01c]<br>0x00006055<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800026e4]:fdiv.h t6, t5, t4, dyn<br> [0x800026e8]:csrrs a2, fcsr, zero<br> [0x800026ec]:sw t6, 64(fp)<br>     |
| 291|[0x8000c024]<br>0xFFFFBC57<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002724]:fdiv.h t6, t5, t4, dyn<br> [0x80002728]:csrrs a2, fcsr, zero<br> [0x8000272c]:sw t6, 72(fp)<br>     |
| 292|[0x8000c02c]<br>0x00003C57<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002764]:fdiv.h t6, t5, t4, dyn<br> [0x80002768]:csrrs a2, fcsr, zero<br> [0x8000276c]:sw t6, 80(fp)<br>     |
| 293|[0x8000c034]<br>0xFFFFBE81<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800027a4]:fdiv.h t6, t5, t4, dyn<br> [0x800027a8]:csrrs a2, fcsr, zero<br> [0x800027ac]:sw t6, 88(fp)<br>     |
| 294|[0x8000c03c]<br>0x00003E81<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800027e4]:fdiv.h t6, t5, t4, dyn<br> [0x800027e8]:csrrs a2, fcsr, zero<br> [0x800027ec]:sw t6, 96(fp)<br>     |
| 295|[0x8000c044]<br>0xFFFFBC54<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002824]:fdiv.h t6, t5, t4, dyn<br> [0x80002828]:csrrs a2, fcsr, zero<br> [0x8000282c]:sw t6, 104(fp)<br>    |
| 296|[0x8000c04c]<br>0x00003C54<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002864]:fdiv.h t6, t5, t4, dyn<br> [0x80002868]:csrrs a2, fcsr, zero<br> [0x8000286c]:sw t6, 112(fp)<br>    |
| 297|[0x8000c054]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800028a4]:fdiv.h t6, t5, t4, dyn<br> [0x800028a8]:csrrs a2, fcsr, zero<br> [0x800028ac]:sw t6, 120(fp)<br>    |
| 298|[0x8000c05c]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800028e4]:fdiv.h t6, t5, t4, dyn<br> [0x800028e8]:csrrs a2, fcsr, zero<br> [0x800028ec]:sw t6, 128(fp)<br>    |
| 299|[0x8000c064]<br>0xFFFFBB6E<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002924]:fdiv.h t6, t5, t4, dyn<br> [0x80002928]:csrrs a2, fcsr, zero<br> [0x8000292c]:sw t6, 136(fp)<br>    |
| 300|[0x8000c06c]<br>0x00003B6E<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002964]:fdiv.h t6, t5, t4, dyn<br> [0x80002968]:csrrs a2, fcsr, zero<br> [0x8000296c]:sw t6, 144(fp)<br>    |
| 301|[0x8000c074]<br>0xFFFF8011<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800029a4]:fdiv.h t6, t5, t4, dyn<br> [0x800029a8]:csrrs a2, fcsr, zero<br> [0x800029ac]:sw t6, 152(fp)<br>    |
| 302|[0x8000c07c]<br>0x00000011<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800029e4]:fdiv.h t6, t5, t4, dyn<br> [0x800029e8]:csrrs a2, fcsr, zero<br> [0x800029ec]:sw t6, 160(fp)<br>    |
| 303|[0x8000c084]<br>0xFFFF9855<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002a24]:fdiv.h t6, t5, t4, dyn<br> [0x80002a28]:csrrs a2, fcsr, zero<br> [0x80002a2c]:sw t6, 168(fp)<br>    |
| 304|[0x8000c08c]<br>0x00001855<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002a64]:fdiv.h t6, t5, t4, dyn<br> [0x80002a68]:csrrs a2, fcsr, zero<br> [0x80002a6c]:sw t6, 176(fp)<br>    |
| 305|[0x8000c094]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002aa4]:fdiv.h t6, t5, t4, dyn<br> [0x80002aa8]:csrrs a2, fcsr, zero<br> [0x80002aac]:sw t6, 184(fp)<br>    |
| 306|[0x8000c09c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ae4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ae8]:csrrs a2, fcsr, zero<br> [0x80002aec]:sw t6, 192(fp)<br>    |
| 307|[0x8000c0a4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b24]:fdiv.h t6, t5, t4, dyn<br> [0x80002b28]:csrrs a2, fcsr, zero<br> [0x80002b2c]:sw t6, 200(fp)<br>    |
| 308|[0x8000c0ac]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b64]:fdiv.h t6, t5, t4, dyn<br> [0x80002b68]:csrrs a2, fcsr, zero<br> [0x80002b6c]:sw t6, 208(fp)<br>    |
| 309|[0x8000c0b4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ba4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ba8]:csrrs a2, fcsr, zero<br> [0x80002bac]:sw t6, 216(fp)<br>    |
| 310|[0x8000c0bc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002be4]:fdiv.h t6, t5, t4, dyn<br> [0x80002be8]:csrrs a2, fcsr, zero<br> [0x80002bec]:sw t6, 224(fp)<br>    |
| 311|[0x8000c0c4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c24]:fdiv.h t6, t5, t4, dyn<br> [0x80002c28]:csrrs a2, fcsr, zero<br> [0x80002c2c]:sw t6, 232(fp)<br>    |
| 312|[0x8000c0cc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c64]:fdiv.h t6, t5, t4, dyn<br> [0x80002c68]:csrrs a2, fcsr, zero<br> [0x80002c6c]:sw t6, 240(fp)<br>    |
| 313|[0x8000c0d4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ca4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ca8]:csrrs a2, fcsr, zero<br> [0x80002cac]:sw t6, 248(fp)<br>    |
| 314|[0x8000c0dc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ce4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ce8]:csrrs a2, fcsr, zero<br> [0x80002cec]:sw t6, 256(fp)<br>    |
| 315|[0x8000c0e4]<br>0x000060AA<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d24]:fdiv.h t6, t5, t4, dyn<br> [0x80002d28]:csrrs a2, fcsr, zero<br> [0x80002d2c]:sw t6, 264(fp)<br>    |
| 316|[0x8000c0ec]<br>0xFFFFE0AA<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d64]:fdiv.h t6, t5, t4, dyn<br> [0x80002d68]:csrrs a2, fcsr, zero<br> [0x80002d6c]:sw t6, 272(fp)<br>    |
| 317|[0x8000c0f4]<br>0x00003CAC<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002da4]:fdiv.h t6, t5, t4, dyn<br> [0x80002da8]:csrrs a2, fcsr, zero<br> [0x80002dac]:sw t6, 280(fp)<br>    |
| 318|[0x8000c0fc]<br>0xFFFFBCAC<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002de4]:fdiv.h t6, t5, t4, dyn<br> [0x80002de8]:csrrs a2, fcsr, zero<br> [0x80002dec]:sw t6, 288(fp)<br>    |
| 319|[0x8000c104]<br>0x00003F01<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002e24]:fdiv.h t6, t5, t4, dyn<br> [0x80002e28]:csrrs a2, fcsr, zero<br> [0x80002e2c]:sw t6, 296(fp)<br>    |
| 320|[0x8000c10c]<br>0xFFFFBF01<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002e64]:fdiv.h t6, t5, t4, dyn<br> [0x80002e68]:csrrs a2, fcsr, zero<br> [0x80002e6c]:sw t6, 304(fp)<br>    |
| 321|[0x8000c114]<br>0x00003CA9<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ea4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ea8]:csrrs a2, fcsr, zero<br> [0x80002eac]:sw t6, 312(fp)<br>    |
| 322|[0x8000c11c]<br>0xFFFFBCA9<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ee4]:fdiv.h t6, t5, t4, dyn<br> [0x80002ee8]:csrrs a2, fcsr, zero<br> [0x80002eec]:sw t6, 320(fp)<br>    |
| 323|[0x8000c124]<br>0x00003C4E<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002f24]:fdiv.h t6, t5, t4, dyn<br> [0x80002f28]:csrrs a2, fcsr, zero<br> [0x80002f2c]:sw t6, 328(fp)<br>    |
| 324|[0x8000c12c]<br>0xFFFFBC4E<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002f64]:fdiv.h t6, t5, t4, dyn<br> [0x80002f68]:csrrs a2, fcsr, zero<br> [0x80002f6c]:sw t6, 336(fp)<br>    |
| 325|[0x8000c134]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002fa4]:fdiv.h t6, t5, t4, dyn<br> [0x80002fa8]:csrrs a2, fcsr, zero<br> [0x80002fac]:sw t6, 344(fp)<br>    |
| 326|[0x8000c13c]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002fe4]:fdiv.h t6, t5, t4, dyn<br> [0x80002fe8]:csrrs a2, fcsr, zero<br> [0x80002fec]:sw t6, 352(fp)<br>    |
| 327|[0x8000c144]<br>0x00000013<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003024]:fdiv.h t6, t5, t4, dyn<br> [0x80003028]:csrrs a2, fcsr, zero<br> [0x8000302c]:sw t6, 360(fp)<br>    |
| 328|[0x8000c14c]<br>0xFFFF8013<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003064]:fdiv.h t6, t5, t4, dyn<br> [0x80003068]:csrrs a2, fcsr, zero<br> [0x8000306c]:sw t6, 368(fp)<br>    |
| 329|[0x8000c154]<br>0x000018AA<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800030a4]:fdiv.h t6, t5, t4, dyn<br> [0x800030a8]:csrrs a2, fcsr, zero<br> [0x800030ac]:sw t6, 376(fp)<br>    |
| 330|[0x8000c15c]<br>0xFFFF98AA<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800030e4]:fdiv.h t6, t5, t4, dyn<br> [0x800030e8]:csrrs a2, fcsr, zero<br> [0x800030ec]:sw t6, 384(fp)<br>    |
| 331|[0x8000c164]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003124]:fdiv.h t6, t5, t4, dyn<br> [0x80003128]:csrrs a2, fcsr, zero<br> [0x8000312c]:sw t6, 392(fp)<br>    |
| 332|[0x8000c16c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003164]:fdiv.h t6, t5, t4, dyn<br> [0x80003168]:csrrs a2, fcsr, zero<br> [0x8000316c]:sw t6, 400(fp)<br>    |
| 333|[0x8000c174]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800031a4]:fdiv.h t6, t5, t4, dyn<br> [0x800031a8]:csrrs a2, fcsr, zero<br> [0x800031ac]:sw t6, 408(fp)<br>    |
| 334|[0x8000c17c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800031e4]:fdiv.h t6, t5, t4, dyn<br> [0x800031e8]:csrrs a2, fcsr, zero<br> [0x800031ec]:sw t6, 416(fp)<br>    |
| 335|[0x8000c184]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003224]:fdiv.h t6, t5, t4, dyn<br> [0x80003228]:csrrs a2, fcsr, zero<br> [0x8000322c]:sw t6, 424(fp)<br>    |
| 336|[0x8000c18c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003264]:fdiv.h t6, t5, t4, dyn<br> [0x80003268]:csrrs a2, fcsr, zero<br> [0x8000326c]:sw t6, 432(fp)<br>    |
| 337|[0x8000c194]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800032a4]:fdiv.h t6, t5, t4, dyn<br> [0x800032a8]:csrrs a2, fcsr, zero<br> [0x800032ac]:sw t6, 440(fp)<br>    |
| 338|[0x8000c19c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800032e4]:fdiv.h t6, t5, t4, dyn<br> [0x800032e8]:csrrs a2, fcsr, zero<br> [0x800032ec]:sw t6, 448(fp)<br>    |
| 339|[0x8000c1a4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003324]:fdiv.h t6, t5, t4, dyn<br> [0x80003328]:csrrs a2, fcsr, zero<br> [0x8000332c]:sw t6, 456(fp)<br>    |
| 340|[0x8000c1ac]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003364]:fdiv.h t6, t5, t4, dyn<br> [0x80003368]:csrrs a2, fcsr, zero<br> [0x8000336c]:sw t6, 464(fp)<br>    |
| 341|[0x8000c1b4]<br>0xFFFFE0AA<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800033a4]:fdiv.h t6, t5, t4, dyn<br> [0x800033a8]:csrrs a2, fcsr, zero<br> [0x800033ac]:sw t6, 472(fp)<br>    |
| 342|[0x8000c1bc]<br>0x000060AA<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800033e4]:fdiv.h t6, t5, t4, dyn<br> [0x800033e8]:csrrs a2, fcsr, zero<br> [0x800033ec]:sw t6, 480(fp)<br>    |
| 343|[0x8000c1c4]<br>0xFFFFBCAC<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003424]:fdiv.h t6, t5, t4, dyn<br> [0x80003428]:csrrs a2, fcsr, zero<br> [0x8000342c]:sw t6, 488(fp)<br>    |
| 344|[0x8000c1cc]<br>0x00003CAC<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003464]:fdiv.h t6, t5, t4, dyn<br> [0x80003468]:csrrs a2, fcsr, zero<br> [0x8000346c]:sw t6, 496(fp)<br>    |
| 345|[0x8000c1d4]<br>0xFFFFBF01<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800034a4]:fdiv.h t6, t5, t4, dyn<br> [0x800034a8]:csrrs a2, fcsr, zero<br> [0x800034ac]:sw t6, 504(fp)<br>    |
| 346|[0x8000c1dc]<br>0x00003F01<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800034e4]:fdiv.h t6, t5, t4, dyn<br> [0x800034e8]:csrrs a2, fcsr, zero<br> [0x800034ec]:sw t6, 512(fp)<br>    |
| 347|[0x8000c1e4]<br>0xFFFFBCA9<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003524]:fdiv.h t6, t5, t4, dyn<br> [0x80003528]:csrrs a2, fcsr, zero<br> [0x8000352c]:sw t6, 520(fp)<br>    |
| 348|[0x8000c1ec]<br>0x00003CA9<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003564]:fdiv.h t6, t5, t4, dyn<br> [0x80003568]:csrrs a2, fcsr, zero<br> [0x8000356c]:sw t6, 528(fp)<br>    |
| 349|[0x8000c1f4]<br>0xFFFFBC4E<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800035a4]:fdiv.h t6, t5, t4, dyn<br> [0x800035a8]:csrrs a2, fcsr, zero<br> [0x800035ac]:sw t6, 536(fp)<br>    |
| 350|[0x8000c1fc]<br>0x00003C4E<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800035e4]:fdiv.h t6, t5, t4, dyn<br> [0x800035e8]:csrrs a2, fcsr, zero<br> [0x800035ec]:sw t6, 544(fp)<br>    |
| 351|[0x8000c204]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003624]:fdiv.h t6, t5, t4, dyn<br> [0x80003628]:csrrs a2, fcsr, zero<br> [0x8000362c]:sw t6, 552(fp)<br>    |
| 352|[0x8000c20c]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003664]:fdiv.h t6, t5, t4, dyn<br> [0x80003668]:csrrs a2, fcsr, zero<br> [0x8000366c]:sw t6, 560(fp)<br>    |
| 353|[0x8000c214]<br>0xFFFF8013<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800036a4]:fdiv.h t6, t5, t4, dyn<br> [0x800036a8]:csrrs a2, fcsr, zero<br> [0x800036ac]:sw t6, 568(fp)<br>    |
| 354|[0x8000c21c]<br>0x00000013<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800036e4]:fdiv.h t6, t5, t4, dyn<br> [0x800036e8]:csrrs a2, fcsr, zero<br> [0x800036ec]:sw t6, 576(fp)<br>    |
| 355|[0x8000c224]<br>0xFFFF98AA<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003724]:fdiv.h t6, t5, t4, dyn<br> [0x80003728]:csrrs a2, fcsr, zero<br> [0x8000372c]:sw t6, 584(fp)<br>    |
| 356|[0x8000c22c]<br>0x000018AA<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003764]:fdiv.h t6, t5, t4, dyn<br> [0x80003768]:csrrs a2, fcsr, zero<br> [0x8000376c]:sw t6, 592(fp)<br>    |
| 357|[0x8000c234]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800037a4]:fdiv.h t6, t5, t4, dyn<br> [0x800037a8]:csrrs a2, fcsr, zero<br> [0x800037ac]:sw t6, 600(fp)<br>    |
| 358|[0x8000c23c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800037e4]:fdiv.h t6, t5, t4, dyn<br> [0x800037e8]:csrrs a2, fcsr, zero<br> [0x800037ec]:sw t6, 608(fp)<br>    |
| 359|[0x8000c244]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003824]:fdiv.h t6, t5, t4, dyn<br> [0x80003828]:csrrs a2, fcsr, zero<br> [0x8000382c]:sw t6, 616(fp)<br>    |
| 360|[0x8000c24c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003864]:fdiv.h t6, t5, t4, dyn<br> [0x80003868]:csrrs a2, fcsr, zero<br> [0x8000386c]:sw t6, 624(fp)<br>    |
| 361|[0x8000c254]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800038a4]:fdiv.h t6, t5, t4, dyn<br> [0x800038a8]:csrrs a2, fcsr, zero<br> [0x800038ac]:sw t6, 632(fp)<br>    |
| 362|[0x8000c25c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800038e4]:fdiv.h t6, t5, t4, dyn<br> [0x800038e8]:csrrs a2, fcsr, zero<br> [0x800038ec]:sw t6, 640(fp)<br>    |
| 363|[0x8000c264]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003924]:fdiv.h t6, t5, t4, dyn<br> [0x80003928]:csrrs a2, fcsr, zero<br> [0x8000392c]:sw t6, 648(fp)<br>    |
| 364|[0x8000c26c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x0aa and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003964]:fdiv.h t6, t5, t4, dyn<br> [0x80003968]:csrrs a2, fcsr, zero<br> [0x8000396c]:sw t6, 656(fp)<br>    |
| 365|[0x8000c274]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800039a4]:fdiv.h t6, t5, t4, dyn<br> [0x800039a8]:csrrs a2, fcsr, zero<br> [0x800039ac]:sw t6, 664(fp)<br>    |
| 366|[0x8000c27c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800039e4]:fdiv.h t6, t5, t4, dyn<br> [0x800039e8]:csrrs a2, fcsr, zero<br> [0x800039ec]:sw t6, 672(fp)<br>    |
| 367|[0x8000c284]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a24]:fdiv.h t6, t5, t4, dyn<br> [0x80003a28]:csrrs a2, fcsr, zero<br> [0x80003a2c]:sw t6, 680(fp)<br>    |
| 368|[0x8000c28c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a64]:fdiv.h t6, t5, t4, dyn<br> [0x80003a68]:csrrs a2, fcsr, zero<br> [0x80003a6c]:sw t6, 688(fp)<br>    |
| 369|[0x8000c294]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003aa4]:fdiv.h t6, t5, t4, dyn<br> [0x80003aa8]:csrrs a2, fcsr, zero<br> [0x80003aac]:sw t6, 696(fp)<br>    |
| 370|[0x8000c29c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ae4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ae8]:csrrs a2, fcsr, zero<br> [0x80003aec]:sw t6, 704(fp)<br>    |
| 371|[0x8000c2a4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b24]:fdiv.h t6, t5, t4, dyn<br> [0x80003b28]:csrrs a2, fcsr, zero<br> [0x80003b2c]:sw t6, 712(fp)<br>    |
| 372|[0x8000c2ac]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b64]:fdiv.h t6, t5, t4, dyn<br> [0x80003b68]:csrrs a2, fcsr, zero<br> [0x80003b6c]:sw t6, 720(fp)<br>    |
| 373|[0x8000c2b4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ba4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ba8]:csrrs a2, fcsr, zero<br> [0x80003bac]:sw t6, 728(fp)<br>    |
| 374|[0x8000c2bc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003be4]:fdiv.h t6, t5, t4, dyn<br> [0x80003be8]:csrrs a2, fcsr, zero<br> [0x80003bec]:sw t6, 736(fp)<br>    |
| 375|[0x8000c2c4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c24]:fdiv.h t6, t5, t4, dyn<br> [0x80003c28]:csrrs a2, fcsr, zero<br> [0x80003c2c]:sw t6, 744(fp)<br>    |
| 376|[0x8000c2cc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c64]:fdiv.h t6, t5, t4, dyn<br> [0x80003c68]:csrrs a2, fcsr, zero<br> [0x80003c6c]:sw t6, 752(fp)<br>    |
| 377|[0x8000c2d4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ca4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ca8]:csrrs a2, fcsr, zero<br> [0x80003cac]:sw t6, 760(fp)<br>    |
| 378|[0x8000c2dc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ce4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ce8]:csrrs a2, fcsr, zero<br> [0x80003cec]:sw t6, 768(fp)<br>    |
| 379|[0x8000c2e4]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003d24]:fdiv.h t6, t5, t4, dyn<br> [0x80003d28]:csrrs a2, fcsr, zero<br> [0x80003d2c]:sw t6, 776(fp)<br>    |
| 380|[0x8000c2ec]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003d64]:fdiv.h t6, t5, t4, dyn<br> [0x80003d68]:csrrs a2, fcsr, zero<br> [0x80003d6c]:sw t6, 784(fp)<br>    |
| 381|[0x8000c2f4]<br>0x00006800<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003da4]:fdiv.h t6, t5, t4, dyn<br> [0x80003da8]:csrrs a2, fcsr, zero<br> [0x80003dac]:sw t6, 792(fp)<br>    |
| 382|[0x8000c2fc]<br>0xFFFFE800<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003de4]:fdiv.h t6, t5, t4, dyn<br> [0x80003de8]:csrrs a2, fcsr, zero<br> [0x80003dec]:sw t6, 800(fp)<br>    |
| 383|[0x8000c304]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003e24]:fdiv.h t6, t5, t4, dyn<br> [0x80003e28]:csrrs a2, fcsr, zero<br> [0x80003e2c]:sw t6, 808(fp)<br>    |
| 384|[0x8000c30c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003e64]:fdiv.h t6, t5, t4, dyn<br> [0x80003e68]:csrrs a2, fcsr, zero<br> [0x80003e6c]:sw t6, 816(fp)<br>    |
| 385|[0x8000c314]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ea4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ea8]:csrrs a2, fcsr, zero<br> [0x80003eac]:sw t6, 824(fp)<br>    |
| 386|[0x8000c31c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003ee4]:fdiv.h t6, t5, t4, dyn<br> [0x80003ee8]:csrrs a2, fcsr, zero<br> [0x80003eec]:sw t6, 832(fp)<br>    |
| 387|[0x8000c324]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f24]:fdiv.h t6, t5, t4, dyn<br> [0x80003f28]:csrrs a2, fcsr, zero<br> [0x80003f2c]:sw t6, 840(fp)<br>    |
| 388|[0x8000c32c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f64]:fdiv.h t6, t5, t4, dyn<br> [0x80003f68]:csrrs a2, fcsr, zero<br> [0x80003f6c]:sw t6, 848(fp)<br>    |
| 389|[0x8000c334]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003fa4]:fdiv.h t6, t5, t4, dyn<br> [0x80003fa8]:csrrs a2, fcsr, zero<br> [0x80003fac]:sw t6, 856(fp)<br>    |
| 390|[0x8000c33c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003fe4]:fdiv.h t6, t5, t4, dyn<br> [0x80003fe8]:csrrs a2, fcsr, zero<br> [0x80003fec]:sw t6, 864(fp)<br>    |
| 391|[0x8000c344]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004024]:fdiv.h t6, t5, t4, dyn<br> [0x80004028]:csrrs a2, fcsr, zero<br> [0x8000402c]:sw t6, 872(fp)<br>    |
| 392|[0x8000c34c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004064]:fdiv.h t6, t5, t4, dyn<br> [0x80004068]:csrrs a2, fcsr, zero<br> [0x8000406c]:sw t6, 880(fp)<br>    |
| 393|[0x8000c354]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800040a4]:fdiv.h t6, t5, t4, dyn<br> [0x800040a8]:csrrs a2, fcsr, zero<br> [0x800040ac]:sw t6, 888(fp)<br>    |
| 394|[0x8000c35c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800040e4]:fdiv.h t6, t5, t4, dyn<br> [0x800040e8]:csrrs a2, fcsr, zero<br> [0x800040ec]:sw t6, 896(fp)<br>    |
| 395|[0x8000c364]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004124]:fdiv.h t6, t5, t4, dyn<br> [0x80004128]:csrrs a2, fcsr, zero<br> [0x8000412c]:sw t6, 904(fp)<br>    |
| 396|[0x8000c36c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004164]:fdiv.h t6, t5, t4, dyn<br> [0x80004168]:csrrs a2, fcsr, zero<br> [0x8000416c]:sw t6, 912(fp)<br>    |
| 397|[0x8000c374]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800041a4]:fdiv.h t6, t5, t4, dyn<br> [0x800041a8]:csrrs a2, fcsr, zero<br> [0x800041ac]:sw t6, 920(fp)<br>    |
| 398|[0x8000c37c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800041e4]:fdiv.h t6, t5, t4, dyn<br> [0x800041e8]:csrrs a2, fcsr, zero<br> [0x800041ec]:sw t6, 928(fp)<br>    |
| 399|[0x8000c384]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004224]:fdiv.h t6, t5, t4, dyn<br> [0x80004228]:csrrs a2, fcsr, zero<br> [0x8000422c]:sw t6, 936(fp)<br>    |
| 400|[0x8000c38c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004264]:fdiv.h t6, t5, t4, dyn<br> [0x80004268]:csrrs a2, fcsr, zero<br> [0x8000426c]:sw t6, 944(fp)<br>    |
| 401|[0x8000c394]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800042a4]:fdiv.h t6, t5, t4, dyn<br> [0x800042a8]:csrrs a2, fcsr, zero<br> [0x800042ac]:sw t6, 952(fp)<br>    |
| 402|[0x8000c39c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800042e4]:fdiv.h t6, t5, t4, dyn<br> [0x800042e8]:csrrs a2, fcsr, zero<br> [0x800042ec]:sw t6, 960(fp)<br>    |
| 403|[0x8000c3a4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004324]:fdiv.h t6, t5, t4, dyn<br> [0x80004328]:csrrs a2, fcsr, zero<br> [0x8000432c]:sw t6, 968(fp)<br>    |
| 404|[0x8000c3ac]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004364]:fdiv.h t6, t5, t4, dyn<br> [0x80004368]:csrrs a2, fcsr, zero<br> [0x8000436c]:sw t6, 976(fp)<br>    |
| 405|[0x8000c3b4]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800043a4]:fdiv.h t6, t5, t4, dyn<br> [0x800043a8]:csrrs a2, fcsr, zero<br> [0x800043ac]:sw t6, 984(fp)<br>    |
| 406|[0x8000c3bc]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800043e4]:fdiv.h t6, t5, t4, dyn<br> [0x800043e8]:csrrs a2, fcsr, zero<br> [0x800043ec]:sw t6, 992(fp)<br>    |
| 407|[0x8000c3c4]<br>0xFFFFE800<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004424]:fdiv.h t6, t5, t4, dyn<br> [0x80004428]:csrrs a2, fcsr, zero<br> [0x8000442c]:sw t6, 1000(fp)<br>   |
| 408|[0x8000c3cc]<br>0x00006800<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004464]:fdiv.h t6, t5, t4, dyn<br> [0x80004468]:csrrs a2, fcsr, zero<br> [0x8000446c]:sw t6, 1008(fp)<br>   |
| 409|[0x8000c3d4]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800044a4]:fdiv.h t6, t5, t4, dyn<br> [0x800044a8]:csrrs a2, fcsr, zero<br> [0x800044ac]:sw t6, 1016(fp)<br>   |
| 410|[0x8000c3dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800044ec]:fdiv.h t6, t5, t4, dyn<br> [0x800044f0]:csrrs a2, fcsr, zero<br> [0x800044f4]:sw t6, 0(fp)<br>      |
| 411|[0x8000c3e4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000452c]:fdiv.h t6, t5, t4, dyn<br> [0x80004530]:csrrs a2, fcsr, zero<br> [0x80004534]:sw t6, 8(fp)<br>      |
| 412|[0x8000c3ec]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000456c]:fdiv.h t6, t5, t4, dyn<br> [0x80004570]:csrrs a2, fcsr, zero<br> [0x80004574]:sw t6, 16(fp)<br>     |
| 413|[0x8000c3f4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800045ac]:fdiv.h t6, t5, t4, dyn<br> [0x800045b0]:csrrs a2, fcsr, zero<br> [0x800045b4]:sw t6, 24(fp)<br>     |
| 414|[0x8000c3fc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800045ec]:fdiv.h t6, t5, t4, dyn<br> [0x800045f0]:csrrs a2, fcsr, zero<br> [0x800045f4]:sw t6, 32(fp)<br>     |
| 415|[0x8000c404]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000462c]:fdiv.h t6, t5, t4, dyn<br> [0x80004630]:csrrs a2, fcsr, zero<br> [0x80004634]:sw t6, 40(fp)<br>     |
| 416|[0x8000c40c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x15 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000466c]:fdiv.h t6, t5, t4, dyn<br> [0x80004670]:csrrs a2, fcsr, zero<br> [0x80004674]:sw t6, 48(fp)<br>     |
| 417|[0x8000c414]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800046ac]:fdiv.h t6, t5, t4, dyn<br> [0x800046b0]:csrrs a2, fcsr, zero<br> [0x800046b4]:sw t6, 56(fp)<br>     |
| 418|[0x8000c41c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800046ec]:fdiv.h t6, t5, t4, dyn<br> [0x800046f0]:csrrs a2, fcsr, zero<br> [0x800046f4]:sw t6, 64(fp)<br>     |
| 419|[0x8000c424]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000472c]:fdiv.h t6, t5, t4, dyn<br> [0x80004730]:csrrs a2, fcsr, zero<br> [0x80004734]:sw t6, 72(fp)<br>     |
| 420|[0x8000c42c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000476c]:fdiv.h t6, t5, t4, dyn<br> [0x80004770]:csrrs a2, fcsr, zero<br> [0x80004774]:sw t6, 80(fp)<br>     |
| 421|[0x8000c434]<br>0x00006002<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800047ac]:fdiv.h t6, t5, t4, dyn<br> [0x800047b0]:csrrs a2, fcsr, zero<br> [0x800047b4]:sw t6, 88(fp)<br>     |
| 422|[0x8000c43c]<br>0xFFFFE002<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800047ec]:fdiv.h t6, t5, t4, dyn<br> [0x800047f0]:csrrs a2, fcsr, zero<br> [0x800047f4]:sw t6, 96(fp)<br>     |
| 423|[0x8000c444]<br>0x00006202<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000482c]:fdiv.h t6, t5, t4, dyn<br> [0x80004830]:csrrs a2, fcsr, zero<br> [0x80004834]:sw t6, 104(fp)<br>    |
| 424|[0x8000c44c]<br>0xFFFFE202<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000486c]:fdiv.h t6, t5, t4, dyn<br> [0x80004870]:csrrs a2, fcsr, zero<br> [0x80004874]:sw t6, 112(fp)<br>    |
| 425|[0x8000c454]<br>0x00005FFE<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800048ac]:fdiv.h t6, t5, t4, dyn<br> [0x800048b0]:csrrs a2, fcsr, zero<br> [0x800048b4]:sw t6, 120(fp)<br>    |
| 426|[0x8000c45c]<br>0xFFFFDFFE<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800048ec]:fdiv.h t6, t5, t4, dyn<br> [0x800048f0]:csrrs a2, fcsr, zero<br> [0x800048f4]:sw t6, 128(fp)<br>    |
| 427|[0x8000c464]<br>0x00005F63<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000492c]:fdiv.h t6, t5, t4, dyn<br> [0x80004930]:csrrs a2, fcsr, zero<br> [0x80004934]:sw t6, 136(fp)<br>    |
| 428|[0x8000c46c]<br>0xFFFFDF63<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000496c]:fdiv.h t6, t5, t4, dyn<br> [0x80004970]:csrrs a2, fcsr, zero<br> [0x80004974]:sw t6, 144(fp)<br>    |
| 429|[0x8000c474]<br>0x00005EDC<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800049ac]:fdiv.h t6, t5, t4, dyn<br> [0x800049b0]:csrrs a2, fcsr, zero<br> [0x800049b4]:sw t6, 152(fp)<br>    |
| 430|[0x8000c47c]<br>0xFFFFDEDC<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800049ec]:fdiv.h t6, t5, t4, dyn<br> [0x800049f0]:csrrs a2, fcsr, zero<br> [0x800049f4]:sw t6, 160(fp)<br>    |
| 431|[0x8000c484]<br>0x00001000<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004a30]:csrrs a2, fcsr, zero<br> [0x80004a34]:sw t6, 168(fp)<br>    |
| 432|[0x8000c48c]<br>0xFFFF9000<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004a70]:csrrs a2, fcsr, zero<br> [0x80004a74]:sw t6, 176(fp)<br>    |
| 433|[0x8000c494]<br>0x00003C00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004aac]:fdiv.h t6, t5, t4, dyn<br> [0x80004ab0]:csrrs a2, fcsr, zero<br> [0x80004ab4]:sw t6, 184(fp)<br>    |
| 434|[0x8000c49c]<br>0xFFFFBC00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004aec]:fdiv.h t6, t5, t4, dyn<br> [0x80004af0]:csrrs a2, fcsr, zero<br> [0x80004af4]:sw t6, 192(fp)<br>    |
| 435|[0x8000c4a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004b30]:csrrs a2, fcsr, zero<br> [0x80004b34]:sw t6, 200(fp)<br>    |
| 436|[0x8000c4ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004b70]:csrrs a2, fcsr, zero<br> [0x80004b74]:sw t6, 208(fp)<br>    |
| 437|[0x8000c4b4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004bac]:fdiv.h t6, t5, t4, dyn<br> [0x80004bb0]:csrrs a2, fcsr, zero<br> [0x80004bb4]:sw t6, 216(fp)<br>    |
| 438|[0x8000c4bc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004bec]:fdiv.h t6, t5, t4, dyn<br> [0x80004bf0]:csrrs a2, fcsr, zero<br> [0x80004bf4]:sw t6, 224(fp)<br>    |
| 439|[0x8000c4c4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004c2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004c30]:csrrs a2, fcsr, zero<br> [0x80004c34]:sw t6, 232(fp)<br>    |
| 440|[0x8000c4cc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004c6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004c70]:csrrs a2, fcsr, zero<br> [0x80004c74]:sw t6, 240(fp)<br>    |
| 441|[0x8000c4d4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004cac]:fdiv.h t6, t5, t4, dyn<br> [0x80004cb0]:csrrs a2, fcsr, zero<br> [0x80004cb4]:sw t6, 248(fp)<br>    |
| 442|[0x8000c4dc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004cec]:fdiv.h t6, t5, t4, dyn<br> [0x80004cf0]:csrrs a2, fcsr, zero<br> [0x80004cf4]:sw t6, 256(fp)<br>    |
| 443|[0x8000c4e4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004d2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004d30]:csrrs a2, fcsr, zero<br> [0x80004d34]:sw t6, 264(fp)<br>    |
| 444|[0x8000c4ec]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004d6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004d70]:csrrs a2, fcsr, zero<br> [0x80004d74]:sw t6, 272(fp)<br>    |
| 445|[0x8000c4f4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004dac]:fdiv.h t6, t5, t4, dyn<br> [0x80004db0]:csrrs a2, fcsr, zero<br> [0x80004db4]:sw t6, 280(fp)<br>    |
| 446|[0x8000c4fc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004dec]:fdiv.h t6, t5, t4, dyn<br> [0x80004df0]:csrrs a2, fcsr, zero<br> [0x80004df4]:sw t6, 288(fp)<br>    |
| 447|[0x8000c504]<br>0xFFFFE002<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004e30]:csrrs a2, fcsr, zero<br> [0x80004e34]:sw t6, 296(fp)<br>    |
| 448|[0x8000c50c]<br>0x00006002<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004e70]:csrrs a2, fcsr, zero<br> [0x80004e74]:sw t6, 304(fp)<br>    |
| 449|[0x8000c514]<br>0xFFFFE202<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004eac]:fdiv.h t6, t5, t4, dyn<br> [0x80004eb0]:csrrs a2, fcsr, zero<br> [0x80004eb4]:sw t6, 312(fp)<br>    |
| 450|[0x8000c51c]<br>0x00006202<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004eec]:fdiv.h t6, t5, t4, dyn<br> [0x80004ef0]:csrrs a2, fcsr, zero<br> [0x80004ef4]:sw t6, 320(fp)<br>    |
| 451|[0x8000c524]<br>0xFFFFDFFE<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f2c]:fdiv.h t6, t5, t4, dyn<br> [0x80004f30]:csrrs a2, fcsr, zero<br> [0x80004f34]:sw t6, 328(fp)<br>    |
| 452|[0x8000c52c]<br>0x00005FFE<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f6c]:fdiv.h t6, t5, t4, dyn<br> [0x80004f70]:csrrs a2, fcsr, zero<br> [0x80004f74]:sw t6, 336(fp)<br>    |
| 453|[0x8000c534]<br>0xFFFFDF63<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004fac]:fdiv.h t6, t5, t4, dyn<br> [0x80004fb0]:csrrs a2, fcsr, zero<br> [0x80004fb4]:sw t6, 344(fp)<br>    |
| 454|[0x8000c53c]<br>0x00005F63<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004fec]:fdiv.h t6, t5, t4, dyn<br> [0x80004ff0]:csrrs a2, fcsr, zero<br> [0x80004ff4]:sw t6, 352(fp)<br>    |
| 455|[0x8000c544]<br>0xFFFFDEDC<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000502c]:fdiv.h t6, t5, t4, dyn<br> [0x80005030]:csrrs a2, fcsr, zero<br> [0x80005034]:sw t6, 360(fp)<br>    |
| 456|[0x8000c54c]<br>0x00005EDC<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000506c]:fdiv.h t6, t5, t4, dyn<br> [0x80005070]:csrrs a2, fcsr, zero<br> [0x80005074]:sw t6, 368(fp)<br>    |
| 457|[0x8000c554]<br>0xFFFF9000<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800050ac]:fdiv.h t6, t5, t4, dyn<br> [0x800050b0]:csrrs a2, fcsr, zero<br> [0x800050b4]:sw t6, 376(fp)<br>    |
| 458|[0x8000c55c]<br>0x00001000<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800050ec]:fdiv.h t6, t5, t4, dyn<br> [0x800050f0]:csrrs a2, fcsr, zero<br> [0x800050f4]:sw t6, 384(fp)<br>    |
| 459|[0x8000c564]<br>0xFFFFBC00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000512c]:fdiv.h t6, t5, t4, dyn<br> [0x80005130]:csrrs a2, fcsr, zero<br> [0x80005134]:sw t6, 392(fp)<br>    |
| 460|[0x8000c56c]<br>0x00003C00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000516c]:fdiv.h t6, t5, t4, dyn<br> [0x80005170]:csrrs a2, fcsr, zero<br> [0x80005174]:sw t6, 400(fp)<br>    |
| 461|[0x8000c574]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800051ac]:fdiv.h t6, t5, t4, dyn<br> [0x800051b0]:csrrs a2, fcsr, zero<br> [0x800051b4]:sw t6, 408(fp)<br>    |
| 462|[0x8000c57c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800051ec]:fdiv.h t6, t5, t4, dyn<br> [0x800051f0]:csrrs a2, fcsr, zero<br> [0x800051f4]:sw t6, 416(fp)<br>    |
| 463|[0x8000c584]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000522c]:fdiv.h t6, t5, t4, dyn<br> [0x80005230]:csrrs a2, fcsr, zero<br> [0x80005234]:sw t6, 424(fp)<br>    |
| 464|[0x8000c58c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000526c]:fdiv.h t6, t5, t4, dyn<br> [0x80005270]:csrrs a2, fcsr, zero<br> [0x80005274]:sw t6, 432(fp)<br>    |
| 465|[0x8000c594]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800052ac]:fdiv.h t6, t5, t4, dyn<br> [0x800052b0]:csrrs a2, fcsr, zero<br> [0x800052b4]:sw t6, 440(fp)<br>    |
| 466|[0x8000c59c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800052ec]:fdiv.h t6, t5, t4, dyn<br> [0x800052f0]:csrrs a2, fcsr, zero<br> [0x800052f4]:sw t6, 448(fp)<br>    |
| 467|[0x8000c5a4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000532c]:fdiv.h t6, t5, t4, dyn<br> [0x80005330]:csrrs a2, fcsr, zero<br> [0x80005334]:sw t6, 456(fp)<br>    |
| 468|[0x8000c5ac]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x0a and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000536c]:fdiv.h t6, t5, t4, dyn<br> [0x80005370]:csrrs a2, fcsr, zero<br> [0x80005374]:sw t6, 464(fp)<br>    |
| 469|[0x8000c5b4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800053ac]:fdiv.h t6, t5, t4, dyn<br> [0x800053b0]:csrrs a2, fcsr, zero<br> [0x800053b4]:sw t6, 472(fp)<br>    |
| 470|[0x8000c5bc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800053ec]:fdiv.h t6, t5, t4, dyn<br> [0x800053f0]:csrrs a2, fcsr, zero<br> [0x800053f4]:sw t6, 480(fp)<br>    |
| 471|[0x8000c5c4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000542c]:fdiv.h t6, t5, t4, dyn<br> [0x80005430]:csrrs a2, fcsr, zero<br> [0x80005434]:sw t6, 488(fp)<br>    |
| 472|[0x8000c5cc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000546c]:fdiv.h t6, t5, t4, dyn<br> [0x80005470]:csrrs a2, fcsr, zero<br> [0x80005474]:sw t6, 496(fp)<br>    |
| 473|[0x8000c5d4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800054ac]:fdiv.h t6, t5, t4, dyn<br> [0x800054b0]:csrrs a2, fcsr, zero<br> [0x800054b4]:sw t6, 504(fp)<br>    |
| 474|[0x8000c5dc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800054ec]:fdiv.h t6, t5, t4, dyn<br> [0x800054f0]:csrrs a2, fcsr, zero<br> [0x800054f4]:sw t6, 512(fp)<br>    |
| 475|[0x8000c5e4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000552c]:fdiv.h t6, t5, t4, dyn<br> [0x80005530]:csrrs a2, fcsr, zero<br> [0x80005534]:sw t6, 520(fp)<br>    |
| 476|[0x8000c5ec]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000556c]:fdiv.h t6, t5, t4, dyn<br> [0x80005570]:csrrs a2, fcsr, zero<br> [0x80005574]:sw t6, 528(fp)<br>    |
| 477|[0x8000c5f4]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800055ac]:fdiv.h t6, t5, t4, dyn<br> [0x800055b0]:csrrs a2, fcsr, zero<br> [0x800055b4]:sw t6, 536(fp)<br>    |
| 478|[0x8000c5fc]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800055ec]:fdiv.h t6, t5, t4, dyn<br> [0x800055f0]:csrrs a2, fcsr, zero<br> [0x800055f4]:sw t6, 544(fp)<br>    |
| 479|[0x8000c604]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000562c]:fdiv.h t6, t5, t4, dyn<br> [0x80005630]:csrrs a2, fcsr, zero<br> [0x80005634]:sw t6, 552(fp)<br>    |
| 480|[0x8000c60c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000566c]:fdiv.h t6, t5, t4, dyn<br> [0x80005670]:csrrs a2, fcsr, zero<br> [0x80005674]:sw t6, 560(fp)<br>    |
| 481|[0x8000c614]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800056ac]:fdiv.h t6, t5, t4, dyn<br> [0x800056b0]:csrrs a2, fcsr, zero<br> [0x800056b4]:sw t6, 568(fp)<br>    |
| 482|[0x8000c61c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800056ec]:fdiv.h t6, t5, t4, dyn<br> [0x800056f0]:csrrs a2, fcsr, zero<br> [0x800056f4]:sw t6, 576(fp)<br>    |
| 483|[0x8000c624]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000572c]:fdiv.h t6, t5, t4, dyn<br> [0x80005730]:csrrs a2, fcsr, zero<br> [0x80005734]:sw t6, 584(fp)<br>    |
| 484|[0x8000c62c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000576c]:fdiv.h t6, t5, t4, dyn<br> [0x80005770]:csrrs a2, fcsr, zero<br> [0x80005774]:sw t6, 592(fp)<br>    |
| 485|[0x8000c634]<br>0x00007C00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800057ac]:fdiv.h t6, t5, t4, dyn<br> [0x800057b0]:csrrs a2, fcsr, zero<br> [0x800057b4]:sw t6, 600(fp)<br>    |
| 486|[0x8000c63c]<br>0xFFFFFC00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800057ec]:fdiv.h t6, t5, t4, dyn<br> [0x800057f0]:csrrs a2, fcsr, zero<br> [0x800057f4]:sw t6, 608(fp)<br>    |
| 487|[0x8000c644]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000582c]:fdiv.h t6, t5, t4, dyn<br> [0x80005830]:csrrs a2, fcsr, zero<br> [0x80005834]:sw t6, 616(fp)<br>    |
| 488|[0x8000c64c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000586c]:fdiv.h t6, t5, t4, dyn<br> [0x80005870]:csrrs a2, fcsr, zero<br> [0x80005874]:sw t6, 624(fp)<br>    |
| 489|[0x8000c654]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800058ac]:fdiv.h t6, t5, t4, dyn<br> [0x800058b0]:csrrs a2, fcsr, zero<br> [0x800058b4]:sw t6, 632(fp)<br>    |
| 490|[0x8000c65c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800058ec]:fdiv.h t6, t5, t4, dyn<br> [0x800058f0]:csrrs a2, fcsr, zero<br> [0x800058f4]:sw t6, 640(fp)<br>    |
| 491|[0x8000c664]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000592c]:fdiv.h t6, t5, t4, dyn<br> [0x80005930]:csrrs a2, fcsr, zero<br> [0x80005934]:sw t6, 648(fp)<br>    |
| 492|[0x8000c66c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000596c]:fdiv.h t6, t5, t4, dyn<br> [0x80005970]:csrrs a2, fcsr, zero<br> [0x80005974]:sw t6, 656(fp)<br>    |
| 493|[0x8000c674]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800059ac]:fdiv.h t6, t5, t4, dyn<br> [0x800059b0]:csrrs a2, fcsr, zero<br> [0x800059b4]:sw t6, 664(fp)<br>    |
| 494|[0x8000c67c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800059ec]:fdiv.h t6, t5, t4, dyn<br> [0x800059f0]:csrrs a2, fcsr, zero<br> [0x800059f4]:sw t6, 672(fp)<br>    |
| 495|[0x8000c684]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005a30]:csrrs a2, fcsr, zero<br> [0x80005a34]:sw t6, 680(fp)<br>    |
| 496|[0x8000c68c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005a70]:csrrs a2, fcsr, zero<br> [0x80005a74]:sw t6, 688(fp)<br>    |
| 497|[0x8000c694]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005aac]:fdiv.h t6, t5, t4, dyn<br> [0x80005ab0]:csrrs a2, fcsr, zero<br> [0x80005ab4]:sw t6, 696(fp)<br>    |
| 498|[0x8000c69c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005aec]:fdiv.h t6, t5, t4, dyn<br> [0x80005af0]:csrrs a2, fcsr, zero<br> [0x80005af4]:sw t6, 704(fp)<br>    |
| 499|[0x8000c6a4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005b2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005b30]:csrrs a2, fcsr, zero<br> [0x80005b34]:sw t6, 712(fp)<br>    |
| 500|[0x8000c6ac]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005b6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005b70]:csrrs a2, fcsr, zero<br> [0x80005b74]:sw t6, 720(fp)<br>    |
| 501|[0x8000c6b4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005bac]:fdiv.h t6, t5, t4, dyn<br> [0x80005bb0]:csrrs a2, fcsr, zero<br> [0x80005bb4]:sw t6, 728(fp)<br>    |
| 502|[0x8000c6bc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005bec]:fdiv.h t6, t5, t4, dyn<br> [0x80005bf0]:csrrs a2, fcsr, zero<br> [0x80005bf4]:sw t6, 736(fp)<br>    |
| 503|[0x8000c6c4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005c2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005c30]:csrrs a2, fcsr, zero<br> [0x80005c34]:sw t6, 744(fp)<br>    |
| 504|[0x8000c6cc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005c6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005c70]:csrrs a2, fcsr, zero<br> [0x80005c74]:sw t6, 752(fp)<br>    |
| 505|[0x8000c6d4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005cac]:fdiv.h t6, t5, t4, dyn<br> [0x80005cb0]:csrrs a2, fcsr, zero<br> [0x80005cb4]:sw t6, 760(fp)<br>    |
| 506|[0x8000c6dc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005cec]:fdiv.h t6, t5, t4, dyn<br> [0x80005cf0]:csrrs a2, fcsr, zero<br> [0x80005cf4]:sw t6, 768(fp)<br>    |
| 507|[0x8000c6e4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005d30]:csrrs a2, fcsr, zero<br> [0x80005d34]:sw t6, 776(fp)<br>    |
| 508|[0x8000c6ec]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005d70]:csrrs a2, fcsr, zero<br> [0x80005d74]:sw t6, 784(fp)<br>    |
| 509|[0x8000c6f4]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005dac]:fdiv.h t6, t5, t4, dyn<br> [0x80005db0]:csrrs a2, fcsr, zero<br> [0x80005db4]:sw t6, 792(fp)<br>    |
| 510|[0x8000c6fc]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005dec]:fdiv.h t6, t5, t4, dyn<br> [0x80005df0]:csrrs a2, fcsr, zero<br> [0x80005df4]:sw t6, 800(fp)<br>    |
| 511|[0x8000c704]<br>0xFFFFFC00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005e30]:csrrs a2, fcsr, zero<br> [0x80005e34]:sw t6, 808(fp)<br>    |
| 512|[0x8000c70c]<br>0x00007C00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005e70]:csrrs a2, fcsr, zero<br> [0x80005e74]:sw t6, 816(fp)<br>    |
| 513|[0x8000c714]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005eac]:fdiv.h t6, t5, t4, dyn<br> [0x80005eb0]:csrrs a2, fcsr, zero<br> [0x80005eb4]:sw t6, 824(fp)<br>    |
| 514|[0x8000c71c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005eec]:fdiv.h t6, t5, t4, dyn<br> [0x80005ef0]:csrrs a2, fcsr, zero<br> [0x80005ef4]:sw t6, 832(fp)<br>    |
| 515|[0x8000c724]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f2c]:fdiv.h t6, t5, t4, dyn<br> [0x80005f30]:csrrs a2, fcsr, zero<br> [0x80005f34]:sw t6, 840(fp)<br>    |
| 516|[0x8000c72c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f6c]:fdiv.h t6, t5, t4, dyn<br> [0x80005f70]:csrrs a2, fcsr, zero<br> [0x80005f74]:sw t6, 848(fp)<br>    |
| 517|[0x8000c734]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005fac]:fdiv.h t6, t5, t4, dyn<br> [0x80005fb0]:csrrs a2, fcsr, zero<br> [0x80005fb4]:sw t6, 856(fp)<br>    |
| 518|[0x8000c73c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005fec]:fdiv.h t6, t5, t4, dyn<br> [0x80005ff0]:csrrs a2, fcsr, zero<br> [0x80005ff4]:sw t6, 864(fp)<br>    |
| 519|[0x8000c744]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000602c]:fdiv.h t6, t5, t4, dyn<br> [0x80006030]:csrrs a2, fcsr, zero<br> [0x80006034]:sw t6, 872(fp)<br>    |
| 520|[0x8000c74c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000606c]:fdiv.h t6, t5, t4, dyn<br> [0x80006070]:csrrs a2, fcsr, zero<br> [0x80006074]:sw t6, 880(fp)<br>    |
| 521|[0x8000c754]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800060ac]:fdiv.h t6, t5, t4, dyn<br> [0x800060b0]:csrrs a2, fcsr, zero<br> [0x800060b4]:sw t6, 888(fp)<br>    |
| 522|[0x8000c75c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800060ec]:fdiv.h t6, t5, t4, dyn<br> [0x800060f0]:csrrs a2, fcsr, zero<br> [0x800060f4]:sw t6, 896(fp)<br>    |
| 523|[0x8000c764]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000612c]:fdiv.h t6, t5, t4, dyn<br> [0x80006130]:csrrs a2, fcsr, zero<br> [0x80006134]:sw t6, 904(fp)<br>    |
| 524|[0x8000c76c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000616c]:fdiv.h t6, t5, t4, dyn<br> [0x80006170]:csrrs a2, fcsr, zero<br> [0x80006174]:sw t6, 912(fp)<br>    |
| 525|[0x8000c774]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800061ac]:fdiv.h t6, t5, t4, dyn<br> [0x800061b0]:csrrs a2, fcsr, zero<br> [0x800061b4]:sw t6, 920(fp)<br>    |
| 526|[0x8000c77c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800061ec]:fdiv.h t6, t5, t4, dyn<br> [0x800061f0]:csrrs a2, fcsr, zero<br> [0x800061f4]:sw t6, 928(fp)<br>    |
| 527|[0x8000c784]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000622c]:fdiv.h t6, t5, t4, dyn<br> [0x80006230]:csrrs a2, fcsr, zero<br> [0x80006234]:sw t6, 936(fp)<br>    |
| 528|[0x8000c78c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000626c]:fdiv.h t6, t5, t4, dyn<br> [0x80006270]:csrrs a2, fcsr, zero<br> [0x80006274]:sw t6, 944(fp)<br>    |
| 529|[0x8000c794]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800062ac]:fdiv.h t6, t5, t4, dyn<br> [0x800062b0]:csrrs a2, fcsr, zero<br> [0x800062b4]:sw t6, 952(fp)<br>    |
| 530|[0x8000c79c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800062ec]:fdiv.h t6, t5, t4, dyn<br> [0x800062f0]:csrrs a2, fcsr, zero<br> [0x800062f4]:sw t6, 960(fp)<br>    |
| 531|[0x8000c7a4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000632c]:fdiv.h t6, t5, t4, dyn<br> [0x80006330]:csrrs a2, fcsr, zero<br> [0x80006334]:sw t6, 968(fp)<br>    |
| 532|[0x8000c7ac]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000636c]:fdiv.h t6, t5, t4, dyn<br> [0x80006370]:csrrs a2, fcsr, zero<br> [0x80006374]:sw t6, 976(fp)<br>    |
| 533|[0x8000c7b4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800063ac]:fdiv.h t6, t5, t4, dyn<br> [0x800063b0]:csrrs a2, fcsr, zero<br> [0x800063b4]:sw t6, 984(fp)<br>    |
| 534|[0x8000c7bc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800063ec]:fdiv.h t6, t5, t4, dyn<br> [0x800063f0]:csrrs a2, fcsr, zero<br> [0x800063f4]:sw t6, 992(fp)<br>    |
| 535|[0x8000c7c4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006424]:fdiv.h t6, t5, t4, dyn<br> [0x80006428]:csrrs a2, fcsr, zero<br> [0x8000642c]:sw t6, 1000(fp)<br>   |
| 536|[0x8000c7cc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000645c]:fdiv.h t6, t5, t4, dyn<br> [0x80006460]:csrrs a2, fcsr, zero<br> [0x80006464]:sw t6, 1008(fp)<br>   |
| 537|[0x8000c7d4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006494]:fdiv.h t6, t5, t4, dyn<br> [0x80006498]:csrrs a2, fcsr, zero<br> [0x8000649c]:sw t6, 1016(fp)<br>   |
| 538|[0x8000c7dc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800064d4]:fdiv.h t6, t5, t4, dyn<br> [0x800064d8]:csrrs a2, fcsr, zero<br> [0x800064dc]:sw t6, 0(fp)<br>      |
| 539|[0x8000c7e4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000650c]:fdiv.h t6, t5, t4, dyn<br> [0x80006510]:csrrs a2, fcsr, zero<br> [0x80006514]:sw t6, 8(fp)<br>      |
| 540|[0x8000c7ec]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006544]:fdiv.h t6, t5, t4, dyn<br> [0x80006548]:csrrs a2, fcsr, zero<br> [0x8000654c]:sw t6, 16(fp)<br>     |
| 541|[0x8000c7f4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000657c]:fdiv.h t6, t5, t4, dyn<br> [0x80006580]:csrrs a2, fcsr, zero<br> [0x80006584]:sw t6, 24(fp)<br>     |
| 542|[0x8000c7fc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800065b4]:fdiv.h t6, t5, t4, dyn<br> [0x800065b8]:csrrs a2, fcsr, zero<br> [0x800065bc]:sw t6, 32(fp)<br>     |
| 543|[0x8000c804]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800065ec]:fdiv.h t6, t5, t4, dyn<br> [0x800065f0]:csrrs a2, fcsr, zero<br> [0x800065f4]:sw t6, 40(fp)<br>     |
| 544|[0x8000c80c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006624]:fdiv.h t6, t5, t4, dyn<br> [0x80006628]:csrrs a2, fcsr, zero<br> [0x8000662c]:sw t6, 48(fp)<br>     |
| 545|[0x8000c814]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000665c]:fdiv.h t6, t5, t4, dyn<br> [0x80006660]:csrrs a2, fcsr, zero<br> [0x80006664]:sw t6, 56(fp)<br>     |
| 546|[0x8000c81c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006694]:fdiv.h t6, t5, t4, dyn<br> [0x80006698]:csrrs a2, fcsr, zero<br> [0x8000669c]:sw t6, 64(fp)<br>     |
| 547|[0x8000c824]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800066cc]:fdiv.h t6, t5, t4, dyn<br> [0x800066d0]:csrrs a2, fcsr, zero<br> [0x800066d4]:sw t6, 72(fp)<br>     |
| 548|[0x8000c82c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006704]:fdiv.h t6, t5, t4, dyn<br> [0x80006708]:csrrs a2, fcsr, zero<br> [0x8000670c]:sw t6, 80(fp)<br>     |
| 549|[0x8000c834]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000673c]:fdiv.h t6, t5, t4, dyn<br> [0x80006740]:csrrs a2, fcsr, zero<br> [0x80006744]:sw t6, 88(fp)<br>     |
| 550|[0x8000c83c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006774]:fdiv.h t6, t5, t4, dyn<br> [0x80006778]:csrrs a2, fcsr, zero<br> [0x8000677c]:sw t6, 96(fp)<br>     |
| 551|[0x8000c844]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800067ac]:fdiv.h t6, t5, t4, dyn<br> [0x800067b0]:csrrs a2, fcsr, zero<br> [0x800067b4]:sw t6, 104(fp)<br>    |
| 552|[0x8000c84c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800067e4]:fdiv.h t6, t5, t4, dyn<br> [0x800067e8]:csrrs a2, fcsr, zero<br> [0x800067ec]:sw t6, 112(fp)<br>    |
| 553|[0x8000c854]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000681c]:fdiv.h t6, t5, t4, dyn<br> [0x80006820]:csrrs a2, fcsr, zero<br> [0x80006824]:sw t6, 120(fp)<br>    |
| 554|[0x8000c85c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006854]:fdiv.h t6, t5, t4, dyn<br> [0x80006858]:csrrs a2, fcsr, zero<br> [0x8000685c]:sw t6, 128(fp)<br>    |
| 555|[0x8000c864]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000688c]:fdiv.h t6, t5, t4, dyn<br> [0x80006890]:csrrs a2, fcsr, zero<br> [0x80006894]:sw t6, 136(fp)<br>    |
| 556|[0x8000c86c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800068c4]:fdiv.h t6, t5, t4, dyn<br> [0x800068c8]:csrrs a2, fcsr, zero<br> [0x800068cc]:sw t6, 144(fp)<br>    |
| 557|[0x8000c874]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800068fc]:fdiv.h t6, t5, t4, dyn<br> [0x80006900]:csrrs a2, fcsr, zero<br> [0x80006904]:sw t6, 152(fp)<br>    |
| 558|[0x8000c87c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006934]:fdiv.h t6, t5, t4, dyn<br> [0x80006938]:csrrs a2, fcsr, zero<br> [0x8000693c]:sw t6, 160(fp)<br>    |
| 559|[0x8000c884]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000696c]:fdiv.h t6, t5, t4, dyn<br> [0x80006970]:csrrs a2, fcsr, zero<br> [0x80006974]:sw t6, 168(fp)<br>    |
| 560|[0x8000c88c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800069a4]:fdiv.h t6, t5, t4, dyn<br> [0x800069a8]:csrrs a2, fcsr, zero<br> [0x800069ac]:sw t6, 176(fp)<br>    |
| 561|[0x8000c894]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800069dc]:fdiv.h t6, t5, t4, dyn<br> [0x800069e0]:csrrs a2, fcsr, zero<br> [0x800069e4]:sw t6, 184(fp)<br>    |
| 562|[0x8000c89c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a14]:fdiv.h t6, t5, t4, dyn<br> [0x80006a18]:csrrs a2, fcsr, zero<br> [0x80006a1c]:sw t6, 192(fp)<br>    |
| 563|[0x8000c8a4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a4c]:fdiv.h t6, t5, t4, dyn<br> [0x80006a50]:csrrs a2, fcsr, zero<br> [0x80006a54]:sw t6, 200(fp)<br>    |
| 564|[0x8000c8ac]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006a84]:fdiv.h t6, t5, t4, dyn<br> [0x80006a88]:csrrs a2, fcsr, zero<br> [0x80006a8c]:sw t6, 208(fp)<br>    |
| 565|[0x8000c8b4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006abc]:fdiv.h t6, t5, t4, dyn<br> [0x80006ac0]:csrrs a2, fcsr, zero<br> [0x80006ac4]:sw t6, 216(fp)<br>    |
| 566|[0x8000c8bc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006af4]:fdiv.h t6, t5, t4, dyn<br> [0x80006af8]:csrrs a2, fcsr, zero<br> [0x80006afc]:sw t6, 224(fp)<br>    |
| 567|[0x8000c8c4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b2c]:fdiv.h t6, t5, t4, dyn<br> [0x80006b30]:csrrs a2, fcsr, zero<br> [0x80006b34]:sw t6, 232(fp)<br>    |
| 568|[0x8000c8cc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b64]:fdiv.h t6, t5, t4, dyn<br> [0x80006b68]:csrrs a2, fcsr, zero<br> [0x80006b6c]:sw t6, 240(fp)<br>    |
| 569|[0x8000c8d4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b9c]:fdiv.h t6, t5, t4, dyn<br> [0x80006ba0]:csrrs a2, fcsr, zero<br> [0x80006ba4]:sw t6, 248(fp)<br>    |
| 570|[0x8000c8dc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006bd4]:fdiv.h t6, t5, t4, dyn<br> [0x80006bd8]:csrrs a2, fcsr, zero<br> [0x80006bdc]:sw t6, 256(fp)<br>    |
| 571|[0x8000c8e4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c0c]:fdiv.h t6, t5, t4, dyn<br> [0x80006c10]:csrrs a2, fcsr, zero<br> [0x80006c14]:sw t6, 264(fp)<br>    |
| 572|[0x8000c8ec]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c44]:fdiv.h t6, t5, t4, dyn<br> [0x80006c48]:csrrs a2, fcsr, zero<br> [0x80006c4c]:sw t6, 272(fp)<br>    |
| 573|[0x8000c8f4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c7c]:fdiv.h t6, t5, t4, dyn<br> [0x80006c80]:csrrs a2, fcsr, zero<br> [0x80006c84]:sw t6, 280(fp)<br>    |
| 574|[0x8000c8fc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006cb4]:fdiv.h t6, t5, t4, dyn<br> [0x80006cb8]:csrrs a2, fcsr, zero<br> [0x80006cbc]:sw t6, 288(fp)<br>    |
| 575|[0x8000c904]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006cec]:fdiv.h t6, t5, t4, dyn<br> [0x80006cf0]:csrrs a2, fcsr, zero<br> [0x80006cf4]:sw t6, 296(fp)<br>    |
| 576|[0x8000c90c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d24]:fdiv.h t6, t5, t4, dyn<br> [0x80006d28]:csrrs a2, fcsr, zero<br> [0x80006d2c]:sw t6, 304(fp)<br>    |
| 577|[0x8000c914]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d5c]:fdiv.h t6, t5, t4, dyn<br> [0x80006d60]:csrrs a2, fcsr, zero<br> [0x80006d64]:sw t6, 312(fp)<br>    |
| 578|[0x8000c91c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d94]:fdiv.h t6, t5, t4, dyn<br> [0x80006d98]:csrrs a2, fcsr, zero<br> [0x80006d9c]:sw t6, 320(fp)<br>    |
| 579|[0x8000c924]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006dcc]:fdiv.h t6, t5, t4, dyn<br> [0x80006dd0]:csrrs a2, fcsr, zero<br> [0x80006dd4]:sw t6, 328(fp)<br>    |
| 580|[0x8000c92c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e04]:fdiv.h t6, t5, t4, dyn<br> [0x80006e08]:csrrs a2, fcsr, zero<br> [0x80006e0c]:sw t6, 336(fp)<br>    |
| 581|[0x8000c934]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e3c]:fdiv.h t6, t5, t4, dyn<br> [0x80006e40]:csrrs a2, fcsr, zero<br> [0x80006e44]:sw t6, 344(fp)<br>    |
| 582|[0x8000c93c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e74]:fdiv.h t6, t5, t4, dyn<br> [0x80006e78]:csrrs a2, fcsr, zero<br> [0x80006e7c]:sw t6, 352(fp)<br>    |
| 583|[0x8000c944]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006eac]:fdiv.h t6, t5, t4, dyn<br> [0x80006eb0]:csrrs a2, fcsr, zero<br> [0x80006eb4]:sw t6, 360(fp)<br>    |
| 584|[0x8000c94c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006ee4]:fdiv.h t6, t5, t4, dyn<br> [0x80006ee8]:csrrs a2, fcsr, zero<br> [0x80006eec]:sw t6, 368(fp)<br>    |
| 585|[0x8000c954]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f1c]:fdiv.h t6, t5, t4, dyn<br> [0x80006f20]:csrrs a2, fcsr, zero<br> [0x80006f24]:sw t6, 376(fp)<br>    |
| 586|[0x8000c95c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f54]:fdiv.h t6, t5, t4, dyn<br> [0x80006f58]:csrrs a2, fcsr, zero<br> [0x80006f5c]:sw t6, 384(fp)<br>    |
| 587|[0x8000c964]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f8c]:fdiv.h t6, t5, t4, dyn<br> [0x80006f90]:csrrs a2, fcsr, zero<br> [0x80006f94]:sw t6, 392(fp)<br>    |
| 588|[0x8000c96c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006fc4]:fdiv.h t6, t5, t4, dyn<br> [0x80006fc8]:csrrs a2, fcsr, zero<br> [0x80006fcc]:sw t6, 400(fp)<br>    |
| 589|[0x8000c974]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006ffc]:fdiv.h t6, t5, t4, dyn<br> [0x80007000]:csrrs a2, fcsr, zero<br> [0x80007004]:sw t6, 408(fp)<br>    |
| 590|[0x8000c97c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007034]:fdiv.h t6, t5, t4, dyn<br> [0x80007038]:csrrs a2, fcsr, zero<br> [0x8000703c]:sw t6, 416(fp)<br>    |
| 591|[0x8000c984]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000706c]:fdiv.h t6, t5, t4, dyn<br> [0x80007070]:csrrs a2, fcsr, zero<br> [0x80007074]:sw t6, 424(fp)<br>    |
| 592|[0x8000c98c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800070a4]:fdiv.h t6, t5, t4, dyn<br> [0x800070a8]:csrrs a2, fcsr, zero<br> [0x800070ac]:sw t6, 432(fp)<br>    |
| 593|[0x8000c994]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800070dc]:fdiv.h t6, t5, t4, dyn<br> [0x800070e0]:csrrs a2, fcsr, zero<br> [0x800070e4]:sw t6, 440(fp)<br>    |
| 594|[0x8000c99c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007114]:fdiv.h t6, t5, t4, dyn<br> [0x80007118]:csrrs a2, fcsr, zero<br> [0x8000711c]:sw t6, 448(fp)<br>    |
| 595|[0x8000c9a4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000714c]:fdiv.h t6, t5, t4, dyn<br> [0x80007150]:csrrs a2, fcsr, zero<br> [0x80007154]:sw t6, 456(fp)<br>    |
| 596|[0x8000c9ac]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007184]:fdiv.h t6, t5, t4, dyn<br> [0x80007188]:csrrs a2, fcsr, zero<br> [0x8000718c]:sw t6, 464(fp)<br>    |
| 597|[0x8000c9b4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800071bc]:fdiv.h t6, t5, t4, dyn<br> [0x800071c0]:csrrs a2, fcsr, zero<br> [0x800071c4]:sw t6, 472(fp)<br>    |
| 598|[0x8000c9bc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800071f4]:fdiv.h t6, t5, t4, dyn<br> [0x800071f8]:csrrs a2, fcsr, zero<br> [0x800071fc]:sw t6, 480(fp)<br>    |
| 599|[0x8000c9c4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000722c]:fdiv.h t6, t5, t4, dyn<br> [0x80007230]:csrrs a2, fcsr, zero<br> [0x80007234]:sw t6, 488(fp)<br>    |
| 600|[0x8000c9cc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007264]:fdiv.h t6, t5, t4, dyn<br> [0x80007268]:csrrs a2, fcsr, zero<br> [0x8000726c]:sw t6, 496(fp)<br>    |
| 601|[0x8000c9d4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000729c]:fdiv.h t6, t5, t4, dyn<br> [0x800072a0]:csrrs a2, fcsr, zero<br> [0x800072a4]:sw t6, 504(fp)<br>    |
| 602|[0x8000c9dc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800072d4]:fdiv.h t6, t5, t4, dyn<br> [0x800072d8]:csrrs a2, fcsr, zero<br> [0x800072dc]:sw t6, 512(fp)<br>    |
| 603|[0x8000c9e4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000730c]:fdiv.h t6, t5, t4, dyn<br> [0x80007310]:csrrs a2, fcsr, zero<br> [0x80007314]:sw t6, 520(fp)<br>    |
| 604|[0x8000c9ec]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007344]:fdiv.h t6, t5, t4, dyn<br> [0x80007348]:csrrs a2, fcsr, zero<br> [0x8000734c]:sw t6, 528(fp)<br>    |
| 605|[0x8000c9f4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000737c]:fdiv.h t6, t5, t4, dyn<br> [0x80007380]:csrrs a2, fcsr, zero<br> [0x80007384]:sw t6, 536(fp)<br>    |
| 606|[0x8000c9fc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800073b4]:fdiv.h t6, t5, t4, dyn<br> [0x800073b8]:csrrs a2, fcsr, zero<br> [0x800073bc]:sw t6, 544(fp)<br>    |
| 607|[0x8000ca04]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800073ec]:fdiv.h t6, t5, t4, dyn<br> [0x800073f0]:csrrs a2, fcsr, zero<br> [0x800073f4]:sw t6, 552(fp)<br>    |
| 608|[0x8000ca0c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007424]:fdiv.h t6, t5, t4, dyn<br> [0x80007428]:csrrs a2, fcsr, zero<br> [0x8000742c]:sw t6, 560(fp)<br>    |
| 609|[0x8000ca14]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000745c]:fdiv.h t6, t5, t4, dyn<br> [0x80007460]:csrrs a2, fcsr, zero<br> [0x80007464]:sw t6, 568(fp)<br>    |
| 610|[0x8000ca1c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007494]:fdiv.h t6, t5, t4, dyn<br> [0x80007498]:csrrs a2, fcsr, zero<br> [0x8000749c]:sw t6, 576(fp)<br>    |
| 611|[0x8000ca24]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800074cc]:fdiv.h t6, t5, t4, dyn<br> [0x800074d0]:csrrs a2, fcsr, zero<br> [0x800074d4]:sw t6, 584(fp)<br>    |
| 612|[0x8000ca2c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007504]:fdiv.h t6, t5, t4, dyn<br> [0x80007508]:csrrs a2, fcsr, zero<br> [0x8000750c]:sw t6, 592(fp)<br>    |
| 613|[0x8000ca34]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000753c]:fdiv.h t6, t5, t4, dyn<br> [0x80007540]:csrrs a2, fcsr, zero<br> [0x80007544]:sw t6, 600(fp)<br>    |
| 614|[0x8000ca3c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007574]:fdiv.h t6, t5, t4, dyn<br> [0x80007578]:csrrs a2, fcsr, zero<br> [0x8000757c]:sw t6, 608(fp)<br>    |
| 615|[0x8000ca44]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800075ac]:fdiv.h t6, t5, t4, dyn<br> [0x800075b0]:csrrs a2, fcsr, zero<br> [0x800075b4]:sw t6, 616(fp)<br>    |
| 616|[0x8000ca4c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800075e4]:fdiv.h t6, t5, t4, dyn<br> [0x800075e8]:csrrs a2, fcsr, zero<br> [0x800075ec]:sw t6, 624(fp)<br>    |
| 617|[0x8000ca54]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000761c]:fdiv.h t6, t5, t4, dyn<br> [0x80007620]:csrrs a2, fcsr, zero<br> [0x80007624]:sw t6, 632(fp)<br>    |
| 618|[0x8000ca5c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007654]:fdiv.h t6, t5, t4, dyn<br> [0x80007658]:csrrs a2, fcsr, zero<br> [0x8000765c]:sw t6, 640(fp)<br>    |
| 619|[0x8000ca64]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000768c]:fdiv.h t6, t5, t4, dyn<br> [0x80007690]:csrrs a2, fcsr, zero<br> [0x80007694]:sw t6, 648(fp)<br>    |
| 620|[0x8000ca6c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800076c4]:fdiv.h t6, t5, t4, dyn<br> [0x800076c8]:csrrs a2, fcsr, zero<br> [0x800076cc]:sw t6, 656(fp)<br>    |
| 621|[0x8000ca74]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800076fc]:fdiv.h t6, t5, t4, dyn<br> [0x80007700]:csrrs a2, fcsr, zero<br> [0x80007704]:sw t6, 664(fp)<br>    |
| 622|[0x8000ca7c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007734]:fdiv.h t6, t5, t4, dyn<br> [0x80007738]:csrrs a2, fcsr, zero<br> [0x8000773c]:sw t6, 672(fp)<br>    |
| 623|[0x8000ca84]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000776c]:fdiv.h t6, t5, t4, dyn<br> [0x80007770]:csrrs a2, fcsr, zero<br> [0x80007774]:sw t6, 680(fp)<br>    |
| 624|[0x8000ca8c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800077a4]:fdiv.h t6, t5, t4, dyn<br> [0x800077a8]:csrrs a2, fcsr, zero<br> [0x800077ac]:sw t6, 688(fp)<br>    |
| 625|[0x8000ca94]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800077dc]:fdiv.h t6, t5, t4, dyn<br> [0x800077e0]:csrrs a2, fcsr, zero<br> [0x800077e4]:sw t6, 696(fp)<br>    |
| 626|[0x8000ca9c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007814]:fdiv.h t6, t5, t4, dyn<br> [0x80007818]:csrrs a2, fcsr, zero<br> [0x8000781c]:sw t6, 704(fp)<br>    |
| 627|[0x8000caa4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000784c]:fdiv.h t6, t5, t4, dyn<br> [0x80007850]:csrrs a2, fcsr, zero<br> [0x80007854]:sw t6, 712(fp)<br>    |
| 628|[0x8000caac]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007884]:fdiv.h t6, t5, t4, dyn<br> [0x80007888]:csrrs a2, fcsr, zero<br> [0x8000788c]:sw t6, 720(fp)<br>    |
| 629|[0x8000cab4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800078bc]:fdiv.h t6, t5, t4, dyn<br> [0x800078c0]:csrrs a2, fcsr, zero<br> [0x800078c4]:sw t6, 728(fp)<br>    |
| 630|[0x8000cabc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800078f4]:fdiv.h t6, t5, t4, dyn<br> [0x800078f8]:csrrs a2, fcsr, zero<br> [0x800078fc]:sw t6, 736(fp)<br>    |
| 631|[0x8000cac4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000792c]:fdiv.h t6, t5, t4, dyn<br> [0x80007930]:csrrs a2, fcsr, zero<br> [0x80007934]:sw t6, 744(fp)<br>    |
| 632|[0x8000cacc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007964]:fdiv.h t6, t5, t4, dyn<br> [0x80007968]:csrrs a2, fcsr, zero<br> [0x8000796c]:sw t6, 752(fp)<br>    |
| 633|[0x8000cad4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000799c]:fdiv.h t6, t5, t4, dyn<br> [0x800079a0]:csrrs a2, fcsr, zero<br> [0x800079a4]:sw t6, 760(fp)<br>    |
| 634|[0x8000cadc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800079d4]:fdiv.h t6, t5, t4, dyn<br> [0x800079d8]:csrrs a2, fcsr, zero<br> [0x800079dc]:sw t6, 768(fp)<br>    |
| 635|[0x8000cae4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007a0c]:fdiv.h t6, t5, t4, dyn<br> [0x80007a10]:csrrs a2, fcsr, zero<br> [0x80007a14]:sw t6, 776(fp)<br>    |
| 636|[0x8000caec]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007a44]:fdiv.h t6, t5, t4, dyn<br> [0x80007a48]:csrrs a2, fcsr, zero<br> [0x80007a4c]:sw t6, 784(fp)<br>    |
| 637|[0x8000caf4]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007a7c]:fdiv.h t6, t5, t4, dyn<br> [0x80007a80]:csrrs a2, fcsr, zero<br> [0x80007a84]:sw t6, 792(fp)<br>    |
| 638|[0x8000cafc]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007ab4]:fdiv.h t6, t5, t4, dyn<br> [0x80007ab8]:csrrs a2, fcsr, zero<br> [0x80007abc]:sw t6, 800(fp)<br>    |
| 639|[0x8000cb04]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007aec]:fdiv.h t6, t5, t4, dyn<br> [0x80007af0]:csrrs a2, fcsr, zero<br> [0x80007af4]:sw t6, 808(fp)<br>    |
| 640|[0x8000cb0c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007b24]:fdiv.h t6, t5, t4, dyn<br> [0x80007b28]:csrrs a2, fcsr, zero<br> [0x80007b2c]:sw t6, 816(fp)<br>    |
| 641|[0x8000cb14]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007b5c]:fdiv.h t6, t5, t4, dyn<br> [0x80007b60]:csrrs a2, fcsr, zero<br> [0x80007b64]:sw t6, 824(fp)<br>    |
| 642|[0x8000cb1c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007b94]:fdiv.h t6, t5, t4, dyn<br> [0x80007b98]:csrrs a2, fcsr, zero<br> [0x80007b9c]:sw t6, 832(fp)<br>    |
| 643|[0x8000cb24]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007bcc]:fdiv.h t6, t5, t4, dyn<br> [0x80007bd0]:csrrs a2, fcsr, zero<br> [0x80007bd4]:sw t6, 840(fp)<br>    |
| 644|[0x8000cb2c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007c04]:fdiv.h t6, t5, t4, dyn<br> [0x80007c08]:csrrs a2, fcsr, zero<br> [0x80007c0c]:sw t6, 848(fp)<br>    |
| 645|[0x8000cb34]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007c3c]:fdiv.h t6, t5, t4, dyn<br> [0x80007c40]:csrrs a2, fcsr, zero<br> [0x80007c44]:sw t6, 856(fp)<br>    |
| 646|[0x8000cb3c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007c74]:fdiv.h t6, t5, t4, dyn<br> [0x80007c78]:csrrs a2, fcsr, zero<br> [0x80007c7c]:sw t6, 864(fp)<br>    |
| 647|[0x8000cb44]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007cac]:fdiv.h t6, t5, t4, dyn<br> [0x80007cb0]:csrrs a2, fcsr, zero<br> [0x80007cb4]:sw t6, 872(fp)<br>    |
| 648|[0x8000cb4c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007ce4]:fdiv.h t6, t5, t4, dyn<br> [0x80007ce8]:csrrs a2, fcsr, zero<br> [0x80007cec]:sw t6, 880(fp)<br>    |
| 649|[0x8000cb54]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007d1c]:fdiv.h t6, t5, t4, dyn<br> [0x80007d20]:csrrs a2, fcsr, zero<br> [0x80007d24]:sw t6, 888(fp)<br>    |
| 650|[0x8000cb5c]<br>0x00007E00<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007d54]:fdiv.h t6, t5, t4, dyn<br> [0x80007d58]:csrrs a2, fcsr, zero<br> [0x80007d5c]:sw t6, 896(fp)<br>    |
| 651|[0x8000cb64]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007d8c]:fdiv.h t6, t5, t4, dyn<br> [0x80007d90]:csrrs a2, fcsr, zero<br> [0x80007d94]:sw t6, 904(fp)<br>    |
| 652|[0x8000cb6c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007dc4]:fdiv.h t6, t5, t4, dyn<br> [0x80007dc8]:csrrs a2, fcsr, zero<br> [0x80007dcc]:sw t6, 912(fp)<br>    |
| 653|[0x8000cb74]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007dfc]:fdiv.h t6, t5, t4, dyn<br> [0x80007e00]:csrrs a2, fcsr, zero<br> [0x80007e04]:sw t6, 920(fp)<br>    |
| 654|[0x8000cb7c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007e34]:fdiv.h t6, t5, t4, dyn<br> [0x80007e38]:csrrs a2, fcsr, zero<br> [0x80007e3c]:sw t6, 928(fp)<br>    |
| 655|[0x8000cb84]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007e6c]:fdiv.h t6, t5, t4, dyn<br> [0x80007e70]:csrrs a2, fcsr, zero<br> [0x80007e74]:sw t6, 936(fp)<br>    |
| 656|[0x8000cb8c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007ea4]:fdiv.h t6, t5, t4, dyn<br> [0x80007ea8]:csrrs a2, fcsr, zero<br> [0x80007eac]:sw t6, 944(fp)<br>    |
| 657|[0x8000cb94]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007edc]:fdiv.h t6, t5, t4, dyn<br> [0x80007ee0]:csrrs a2, fcsr, zero<br> [0x80007ee4]:sw t6, 952(fp)<br>    |
| 658|[0x8000cb9c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007f14]:fdiv.h t6, t5, t4, dyn<br> [0x80007f18]:csrrs a2, fcsr, zero<br> [0x80007f1c]:sw t6, 960(fp)<br>    |
| 659|[0x8000cba4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007f4c]:fdiv.h t6, t5, t4, dyn<br> [0x80007f50]:csrrs a2, fcsr, zero<br> [0x80007f54]:sw t6, 968(fp)<br>    |
| 660|[0x8000cbac]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007f84]:fdiv.h t6, t5, t4, dyn<br> [0x80007f88]:csrrs a2, fcsr, zero<br> [0x80007f8c]:sw t6, 976(fp)<br>    |
| 661|[0x8000cbb4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007fbc]:fdiv.h t6, t5, t4, dyn<br> [0x80007fc0]:csrrs a2, fcsr, zero<br> [0x80007fc4]:sw t6, 984(fp)<br>    |
| 662|[0x8000cbbc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007ff4]:fdiv.h t6, t5, t4, dyn<br> [0x80007ff8]:csrrs a2, fcsr, zero<br> [0x80007ffc]:sw t6, 992(fp)<br>    |
| 663|[0x8000cbc4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000802c]:fdiv.h t6, t5, t4, dyn<br> [0x80008030]:csrrs a2, fcsr, zero<br> [0x80008034]:sw t6, 1000(fp)<br>   |
| 664|[0x8000cbcc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x0aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008064]:fdiv.h t6, t5, t4, dyn<br> [0x80008068]:csrrs a2, fcsr, zero<br> [0x8000806c]:sw t6, 1008(fp)<br>   |
| 665|[0x8000cbd4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000809c]:fdiv.h t6, t5, t4, dyn<br> [0x800080a0]:csrrs a2, fcsr, zero<br> [0x800080a4]:sw t6, 1016(fp)<br>   |
| 666|[0x8000cbdc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x15 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800080dc]:fdiv.h t6, t5, t4, dyn<br> [0x800080e0]:csrrs a2, fcsr, zero<br> [0x800080e4]:sw t6, 0(fp)<br>      |
| 667|[0x8000cbe4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008114]:fdiv.h t6, t5, t4, dyn<br> [0x80008118]:csrrs a2, fcsr, zero<br> [0x8000811c]:sw t6, 8(fp)<br>      |
| 668|[0x8000cbec]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0a and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000814c]:fdiv.h t6, t5, t4, dyn<br> [0x80008150]:csrrs a2, fcsr, zero<br> [0x80008154]:sw t6, 16(fp)<br>     |
| 669|[0x8000cbf4]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008184]:fdiv.h t6, t5, t4, dyn<br> [0x80008188]:csrrs a2, fcsr, zero<br> [0x8000818c]:sw t6, 24(fp)<br>     |
| 670|[0x8000cbfc]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800081bc]:fdiv.h t6, t5, t4, dyn<br> [0x800081c0]:csrrs a2, fcsr, zero<br> [0x800081c4]:sw t6, 32(fp)<br>     |
| 671|[0x8000cc04]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800081f4]:fdiv.h t6, t5, t4, dyn<br> [0x800081f8]:csrrs a2, fcsr, zero<br> [0x800081fc]:sw t6, 40(fp)<br>     |
| 672|[0x8000cc0c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000822c]:fdiv.h t6, t5, t4, dyn<br> [0x80008230]:csrrs a2, fcsr, zero<br> [0x80008234]:sw t6, 48(fp)<br>     |
| 673|[0x8000cc14]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008264]:fdiv.h t6, t5, t4, dyn<br> [0x80008268]:csrrs a2, fcsr, zero<br> [0x8000826c]:sw t6, 56(fp)<br>     |
| 674|[0x8000cc1c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000829c]:fdiv.h t6, t5, t4, dyn<br> [0x800082a0]:csrrs a2, fcsr, zero<br> [0x800082a4]:sw t6, 64(fp)<br>     |
| 675|[0x8000cc24]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800082d4]:fdiv.h t6, t5, t4, dyn<br> [0x800082d8]:csrrs a2, fcsr, zero<br> [0x800082dc]:sw t6, 72(fp)<br>     |
| 676|[0x8000cc2c]<br>0x00007E00<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000830c]:fdiv.h t6, t5, t4, dyn<br> [0x80008310]:csrrs a2, fcsr, zero<br> [0x80008314]:sw t6, 80(fp)<br>     |
| 677|[0x8000cc3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000837c]:fdiv.h t6, t5, t4, dyn<br> [0x80008380]:csrrs a2, fcsr, zero<br> [0x80008384]:sw t6, 96(fp)<br>     |
| 678|[0x8000cc44]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800083b4]:fdiv.h t6, t5, t4, dyn<br> [0x800083b8]:csrrs a2, fcsr, zero<br> [0x800083bc]:sw t6, 104(fp)<br>    |
| 679|[0x8000cc4c]<br>0xFFFF8000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x2aa and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800083ec]:fdiv.h t6, t5, t4, dyn<br> [0x800083f0]:csrrs a2, fcsr, zero<br> [0x800083f4]:sw t6, 112(fp)<br>    |
