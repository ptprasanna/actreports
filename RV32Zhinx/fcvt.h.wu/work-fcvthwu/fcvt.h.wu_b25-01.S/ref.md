
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x800004b0')]      |
| SIG_REGION                | [('0x80002210', '0x80002320', '68 words')]      |
| COV_LABELS                | fcvt.h.wu_b25      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-fcvthwu/fcvt.h.wu_b25-01.S/ref.S    |
| Total Number of coverpoints| 69     |
| Total Coverpoints Hit     | 69      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.h.wu', 'rs1 : x30', 'rd : x31', 'rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x80000120]:fcvt.h.wu t6, t5, dyn
	-[0x80000124]:csrrs tp, fcsr, zero
	-[0x80000128]:sw t6, 0(ra)
Current Store : [0x8000012c] : sw tp, 4(ra) -- Store: [0x80002218]:0x00000000




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x8000013c]:fcvt.h.wu t5, t6, dyn
	-[0x80000140]:csrrs tp, fcsr, zero
	-[0x80000144]:sw t5, 8(ra)
Current Store : [0x80000148] : sw tp, 12(ra) -- Store: [0x80002220]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rd : x29', 'rs1_val == 4294967295 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x80000158]:fcvt.h.wu t4, t3, dyn
	-[0x8000015c]:csrrs tp, fcsr, zero
	-[0x80000160]:sw t4, 16(ra)
Current Store : [0x80000164] : sw tp, 20(ra) -- Store: [0x80002228]:0x00000005




Last Coverpoint : ['rs1 : x29', 'rd : x28', 'rs1_val == 2454155456 and  fcsr == 0 and rm_val == 7  #nosat']
Last Code Sequence : 
	-[0x80000174]:fcvt.h.wu t3, t4, dyn
	-[0x80000178]:csrrs tp, fcsr, zero
	-[0x8000017c]:sw t3, 24(ra)
Current Store : [0x80000180] : sw tp, 28(ra) -- Store: [0x80002230]:0x00000005




Last Coverpoint : ['rs1 : x26', 'rd : x27']
Last Code Sequence : 
	-[0x80000190]:fcvt.h.wu s11, s10, dyn
	-[0x80000194]:csrrs tp, fcsr, zero
	-[0x80000198]:sw s11, 32(ra)
Current Store : [0x8000019c] : sw tp, 36(ra) -- Store: [0x80002238]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rd : x26']
Last Code Sequence : 
	-[0x800001ac]:fcvt.h.wu s10, s11, dyn
	-[0x800001b0]:csrrs tp, fcsr, zero
	-[0x800001b4]:sw s10, 40(ra)
Current Store : [0x800001b8] : sw tp, 44(ra) -- Store: [0x80002240]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rd : x25']
Last Code Sequence : 
	-[0x800001c8]:fcvt.h.wu s9, s8, dyn
	-[0x800001cc]:csrrs tp, fcsr, zero
	-[0x800001d0]:sw s9, 48(ra)
Current Store : [0x800001d4] : sw tp, 52(ra) -- Store: [0x80002248]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rd : x24']
Last Code Sequence : 
	-[0x800001e4]:fcvt.h.wu s8, s9, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s8, 56(ra)
Current Store : [0x800001f0] : sw tp, 60(ra) -- Store: [0x80002250]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rd : x23']
Last Code Sequence : 
	-[0x80000200]:fcvt.h.wu s7, s6, dyn
	-[0x80000204]:csrrs tp, fcsr, zero
	-[0x80000208]:sw s7, 64(ra)
Current Store : [0x8000020c] : sw tp, 68(ra) -- Store: [0x80002258]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rd : x22']
Last Code Sequence : 
	-[0x8000021c]:fcvt.h.wu s6, s7, dyn
	-[0x80000220]:csrrs tp, fcsr, zero
	-[0x80000224]:sw s6, 72(ra)
Current Store : [0x80000228] : sw tp, 76(ra) -- Store: [0x80002260]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rd : x21']
Last Code Sequence : 
	-[0x80000238]:fcvt.h.wu s5, s4, dyn
	-[0x8000023c]:csrrs tp, fcsr, zero
	-[0x80000240]:sw s5, 80(ra)
Current Store : [0x80000244] : sw tp, 84(ra) -- Store: [0x80002268]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rd : x20']
Last Code Sequence : 
	-[0x80000254]:fcvt.h.wu s4, s5, dyn
	-[0x80000258]:csrrs tp, fcsr, zero
	-[0x8000025c]:sw s4, 88(ra)
Current Store : [0x80000260] : sw tp, 92(ra) -- Store: [0x80002270]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rd : x19']
Last Code Sequence : 
	-[0x80000270]:fcvt.h.wu s3, s2, dyn
	-[0x80000274]:csrrs tp, fcsr, zero
	-[0x80000278]:sw s3, 96(ra)
Current Store : [0x8000027c] : sw tp, 100(ra) -- Store: [0x80002278]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rd : x18']
Last Code Sequence : 
	-[0x8000028c]:fcvt.h.wu s2, s3, dyn
	-[0x80000290]:csrrs tp, fcsr, zero
	-[0x80000294]:sw s2, 104(ra)
Current Store : [0x80000298] : sw tp, 108(ra) -- Store: [0x80002280]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rd : x17']
Last Code Sequence : 
	-[0x800002a8]:fcvt.h.wu a7, a6, dyn
	-[0x800002ac]:csrrs tp, fcsr, zero
	-[0x800002b0]:sw a7, 112(ra)
Current Store : [0x800002b4] : sw tp, 116(ra) -- Store: [0x80002288]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rd : x16']
Last Code Sequence : 
	-[0x800002c4]:fcvt.h.wu a6, a7, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw a6, 120(ra)
Current Store : [0x800002d0] : sw tp, 124(ra) -- Store: [0x80002290]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rd : x15']
Last Code Sequence : 
	-[0x800002e0]:fcvt.h.wu a5, a4, dyn
	-[0x800002e4]:csrrs tp, fcsr, zero
	-[0x800002e8]:sw a5, 128(ra)
Current Store : [0x800002ec] : sw tp, 132(ra) -- Store: [0x80002298]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rd : x14']
Last Code Sequence : 
	-[0x800002fc]:fcvt.h.wu a4, a5, dyn
	-[0x80000300]:csrrs tp, fcsr, zero
	-[0x80000304]:sw a4, 136(ra)
Current Store : [0x80000308] : sw tp, 140(ra) -- Store: [0x800022a0]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rd : x13']
Last Code Sequence : 
	-[0x80000318]:fcvt.h.wu a3, a2, dyn
	-[0x8000031c]:csrrs tp, fcsr, zero
	-[0x80000320]:sw a3, 144(ra)
Current Store : [0x80000324] : sw tp, 148(ra) -- Store: [0x800022a8]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rd : x12']
Last Code Sequence : 
	-[0x80000334]:fcvt.h.wu a2, a3, dyn
	-[0x80000338]:csrrs tp, fcsr, zero
	-[0x8000033c]:sw a2, 152(ra)
Current Store : [0x80000340] : sw tp, 156(ra) -- Store: [0x800022b0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rd : x11']
Last Code Sequence : 
	-[0x80000350]:fcvt.h.wu a1, a0, dyn
	-[0x80000354]:csrrs tp, fcsr, zero
	-[0x80000358]:sw a1, 160(ra)
Current Store : [0x8000035c] : sw tp, 164(ra) -- Store: [0x800022b8]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rd : x10']
Last Code Sequence : 
	-[0x8000036c]:fcvt.h.wu a0, a1, dyn
	-[0x80000370]:csrrs tp, fcsr, zero
	-[0x80000374]:sw a0, 168(ra)
Current Store : [0x80000378] : sw tp, 172(ra) -- Store: [0x800022c0]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rd : x9']
Last Code Sequence : 
	-[0x80000388]:fcvt.h.wu s1, fp, dyn
	-[0x8000038c]:csrrs tp, fcsr, zero
	-[0x80000390]:sw s1, 176(ra)
Current Store : [0x80000394] : sw tp, 180(ra) -- Store: [0x800022c8]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rd : x8']
Last Code Sequence : 
	-[0x800003ac]:fcvt.h.wu fp, s1, dyn
	-[0x800003b0]:csrrs a1, fcsr, zero
	-[0x800003b4]:sw fp, 184(ra)
Current Store : [0x800003b8] : sw a1, 188(ra) -- Store: [0x800022d0]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rd : x7']
Last Code Sequence : 
	-[0x800003c8]:fcvt.h.wu t2, t1, dyn
	-[0x800003cc]:csrrs a1, fcsr, zero
	-[0x800003d0]:sw t2, 192(ra)
Current Store : [0x800003d4] : sw a1, 196(ra) -- Store: [0x800022d8]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rd : x6']
Last Code Sequence : 
	-[0x800003e4]:fcvt.h.wu t1, t2, dyn
	-[0x800003e8]:csrrs a1, fcsr, zero
	-[0x800003ec]:sw t1, 200(ra)
Current Store : [0x800003f0] : sw a1, 204(ra) -- Store: [0x800022e0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rd : x5']
Last Code Sequence : 
	-[0x80000400]:fcvt.h.wu t0, tp, dyn
	-[0x80000404]:csrrs a1, fcsr, zero
	-[0x80000408]:sw t0, 208(ra)
Current Store : [0x8000040c] : sw a1, 212(ra) -- Store: [0x800022e8]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rd : x4']
Last Code Sequence : 
	-[0x80000424]:fcvt.h.wu tp, t0, dyn
	-[0x80000428]:csrrs a1, fcsr, zero
	-[0x8000042c]:sw tp, 0(t1)
Current Store : [0x80000430] : sw a1, 4(t1) -- Store: [0x800022f0]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rd : x3']
Last Code Sequence : 
	-[0x80000440]:fcvt.h.wu gp, sp, dyn
	-[0x80000444]:csrrs a1, fcsr, zero
	-[0x80000448]:sw gp, 8(t1)
Current Store : [0x8000044c] : sw a1, 12(t1) -- Store: [0x800022f8]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rd : x2']
Last Code Sequence : 
	-[0x8000045c]:fcvt.h.wu sp, gp, dyn
	-[0x80000460]:csrrs a1, fcsr, zero
	-[0x80000464]:sw sp, 16(t1)
Current Store : [0x80000468] : sw a1, 20(t1) -- Store: [0x80002300]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x80000478]:fcvt.h.wu ra, zero, dyn
	-[0x8000047c]:csrrs a1, fcsr, zero
	-[0x80000480]:sw ra, 24(t1)
Current Store : [0x80000484] : sw a1, 28(t1) -- Store: [0x80002308]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rd : x0']
Last Code Sequence : 
	-[0x80000494]:fcvt.h.wu zero, ra, dyn
	-[0x80000498]:csrrs a1, fcsr, zero
	-[0x8000049c]:sw zero, 32(t1)
Current Store : [0x800004a0] : sw a1, 36(t1) -- Store: [0x80002310]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                    coverpoints                                                     |                                                      code                                                      |
|---:|-------------------------------|--------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002214]<br>0x00000000<br> |- mnemonic : fcvt.h.wu<br> - rs1 : x30<br> - rd : x31<br> - rs1_val == 0 and  fcsr == 0 and rm_val == 7  #nosat<br> |[0x80000120]:fcvt.h.wu t6, t5, dyn<br> [0x80000124]:csrrs tp, fcsr, zero<br> [0x80000128]:sw t6, 0(ra)<br>      |
|   2|[0x8000221c]<br>0x00003C00<br> |- rs1 : x31<br> - rd : x30<br> - rs1_val == 1 and  fcsr == 0 and rm_val == 7  #nosat<br>                            |[0x8000013c]:fcvt.h.wu t5, t6, dyn<br> [0x80000140]:csrrs tp, fcsr, zero<br> [0x80000144]:sw t5, 8(ra)<br>      |
|   3|[0x80002224]<br>0x00007C00<br> |- rs1 : x28<br> - rd : x29<br> - rs1_val == 4294967295 and  fcsr == 0 and rm_val == 7  #nosat<br>                   |[0x80000158]:fcvt.h.wu t4, t3, dyn<br> [0x8000015c]:csrrs tp, fcsr, zero<br> [0x80000160]:sw t4, 16(ra)<br>     |
|   4|[0x8000222c]<br>0x00007C00<br> |- rs1 : x29<br> - rd : x28<br> - rs1_val == 2454155456 and  fcsr == 0 and rm_val == 7  #nosat<br>                   |[0x80000174]:fcvt.h.wu t3, t4, dyn<br> [0x80000178]:csrrs tp, fcsr, zero<br> [0x8000017c]:sw t3, 24(ra)<br>     |
|   5|[0x80002234]<br>0x00000000<br> |- rs1 : x26<br> - rd : x27<br>                                                                                      |[0x80000190]:fcvt.h.wu s11, s10, dyn<br> [0x80000194]:csrrs tp, fcsr, zero<br> [0x80000198]:sw s11, 32(ra)<br>  |
|   6|[0x8000223c]<br>0x00000000<br> |- rs1 : x27<br> - rd : x26<br>                                                                                      |[0x800001ac]:fcvt.h.wu s10, s11, dyn<br> [0x800001b0]:csrrs tp, fcsr, zero<br> [0x800001b4]:sw s10, 40(ra)<br>  |
|   7|[0x80002244]<br>0x00000000<br> |- rs1 : x24<br> - rd : x25<br>                                                                                      |[0x800001c8]:fcvt.h.wu s9, s8, dyn<br> [0x800001cc]:csrrs tp, fcsr, zero<br> [0x800001d0]:sw s9, 48(ra)<br>     |
|   8|[0x8000224c]<br>0x00000000<br> |- rs1 : x25<br> - rd : x24<br>                                                                                      |[0x800001e4]:fcvt.h.wu s8, s9, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s8, 56(ra)<br>     |
|   9|[0x80002254]<br>0x00000000<br> |- rs1 : x22<br> - rd : x23<br>                                                                                      |[0x80000200]:fcvt.h.wu s7, s6, dyn<br> [0x80000204]:csrrs tp, fcsr, zero<br> [0x80000208]:sw s7, 64(ra)<br>     |
|  10|[0x8000225c]<br>0x00000000<br> |- rs1 : x23<br> - rd : x22<br>                                                                                      |[0x8000021c]:fcvt.h.wu s6, s7, dyn<br> [0x80000220]:csrrs tp, fcsr, zero<br> [0x80000224]:sw s6, 72(ra)<br>     |
|  11|[0x80002264]<br>0x00000000<br> |- rs1 : x20<br> - rd : x21<br>                                                                                      |[0x80000238]:fcvt.h.wu s5, s4, dyn<br> [0x8000023c]:csrrs tp, fcsr, zero<br> [0x80000240]:sw s5, 80(ra)<br>     |
|  12|[0x8000226c]<br>0x00000000<br> |- rs1 : x21<br> - rd : x20<br>                                                                                      |[0x80000254]:fcvt.h.wu s4, s5, dyn<br> [0x80000258]:csrrs tp, fcsr, zero<br> [0x8000025c]:sw s4, 88(ra)<br>     |
|  13|[0x80002274]<br>0x00000000<br> |- rs1 : x18<br> - rd : x19<br>                                                                                      |[0x80000270]:fcvt.h.wu s3, s2, dyn<br> [0x80000274]:csrrs tp, fcsr, zero<br> [0x80000278]:sw s3, 96(ra)<br>     |
|  14|[0x8000227c]<br>0x00000000<br> |- rs1 : x19<br> - rd : x18<br>                                                                                      |[0x8000028c]:fcvt.h.wu s2, s3, dyn<br> [0x80000290]:csrrs tp, fcsr, zero<br> [0x80000294]:sw s2, 104(ra)<br>    |
|  15|[0x80002284]<br>0x00000000<br> |- rs1 : x16<br> - rd : x17<br>                                                                                      |[0x800002a8]:fcvt.h.wu a7, a6, dyn<br> [0x800002ac]:csrrs tp, fcsr, zero<br> [0x800002b0]:sw a7, 112(ra)<br>    |
|  16|[0x8000228c]<br>0x00000000<br> |- rs1 : x17<br> - rd : x16<br>                                                                                      |[0x800002c4]:fcvt.h.wu a6, a7, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw a6, 120(ra)<br>    |
|  17|[0x80002294]<br>0x00000000<br> |- rs1 : x14<br> - rd : x15<br>                                                                                      |[0x800002e0]:fcvt.h.wu a5, a4, dyn<br> [0x800002e4]:csrrs tp, fcsr, zero<br> [0x800002e8]:sw a5, 128(ra)<br>    |
|  18|[0x8000229c]<br>0x00000000<br> |- rs1 : x15<br> - rd : x14<br>                                                                                      |[0x800002fc]:fcvt.h.wu a4, a5, dyn<br> [0x80000300]:csrrs tp, fcsr, zero<br> [0x80000304]:sw a4, 136(ra)<br>    |
|  19|[0x800022a4]<br>0x00000000<br> |- rs1 : x12<br> - rd : x13<br>                                                                                      |[0x80000318]:fcvt.h.wu a3, a2, dyn<br> [0x8000031c]:csrrs tp, fcsr, zero<br> [0x80000320]:sw a3, 144(ra)<br>    |
|  20|[0x800022ac]<br>0x00000000<br> |- rs1 : x13<br> - rd : x12<br>                                                                                      |[0x80000334]:fcvt.h.wu a2, a3, dyn<br> [0x80000338]:csrrs tp, fcsr, zero<br> [0x8000033c]:sw a2, 152(ra)<br>    |
|  21|[0x800022b4]<br>0x00000000<br> |- rs1 : x10<br> - rd : x11<br>                                                                                      |[0x80000350]:fcvt.h.wu a1, a0, dyn<br> [0x80000354]:csrrs tp, fcsr, zero<br> [0x80000358]:sw a1, 160(ra)<br>    |
|  22|[0x800022bc]<br>0x00000000<br> |- rs1 : x11<br> - rd : x10<br>                                                                                      |[0x8000036c]:fcvt.h.wu a0, a1, dyn<br> [0x80000370]:csrrs tp, fcsr, zero<br> [0x80000374]:sw a0, 168(ra)<br>    |
|  23|[0x800022c4]<br>0x00000000<br> |- rs1 : x8<br> - rd : x9<br>                                                                                        |[0x80000388]:fcvt.h.wu s1, fp, dyn<br> [0x8000038c]:csrrs tp, fcsr, zero<br> [0x80000390]:sw s1, 176(ra)<br>    |
|  24|[0x800022cc]<br>0x00000000<br> |- rs1 : x9<br> - rd : x8<br>                                                                                        |[0x800003ac]:fcvt.h.wu fp, s1, dyn<br> [0x800003b0]:csrrs a1, fcsr, zero<br> [0x800003b4]:sw fp, 184(ra)<br>    |
|  25|[0x800022d4]<br>0x00000000<br> |- rs1 : x6<br> - rd : x7<br>                                                                                        |[0x800003c8]:fcvt.h.wu t2, t1, dyn<br> [0x800003cc]:csrrs a1, fcsr, zero<br> [0x800003d0]:sw t2, 192(ra)<br>    |
|  26|[0x800022dc]<br>0x00000000<br> |- rs1 : x7<br> - rd : x6<br>                                                                                        |[0x800003e4]:fcvt.h.wu t1, t2, dyn<br> [0x800003e8]:csrrs a1, fcsr, zero<br> [0x800003ec]:sw t1, 200(ra)<br>    |
|  27|[0x800022e4]<br>0x00000000<br> |- rs1 : x4<br> - rd : x5<br>                                                                                        |[0x80000400]:fcvt.h.wu t0, tp, dyn<br> [0x80000404]:csrrs a1, fcsr, zero<br> [0x80000408]:sw t0, 208(ra)<br>    |
|  28|[0x800022ec]<br>0x00000000<br> |- rs1 : x5<br> - rd : x4<br>                                                                                        |[0x80000424]:fcvt.h.wu tp, t0, dyn<br> [0x80000428]:csrrs a1, fcsr, zero<br> [0x8000042c]:sw tp, 0(t1)<br>      |
|  29|[0x800022f4]<br>0x00000000<br> |- rs1 : x2<br> - rd : x3<br>                                                                                        |[0x80000440]:fcvt.h.wu gp, sp, dyn<br> [0x80000444]:csrrs a1, fcsr, zero<br> [0x80000448]:sw gp, 8(t1)<br>      |
|  30|[0x800022fc]<br>0x00000000<br> |- rs1 : x3<br> - rd : x2<br>                                                                                        |[0x8000045c]:fcvt.h.wu sp, gp, dyn<br> [0x80000460]:csrrs a1, fcsr, zero<br> [0x80000464]:sw sp, 16(t1)<br>     |
|  31|[0x80002304]<br>0x00000000<br> |- rs1 : x0<br> - rd : x1<br>                                                                                        |[0x80000478]:fcvt.h.wu ra, zero, dyn<br> [0x8000047c]:csrrs a1, fcsr, zero<br> [0x80000480]:sw ra, 24(t1)<br>   |
|  32|[0x8000230c]<br>0x00000000<br> |- rs1 : x1<br> - rd : x0<br>                                                                                        |[0x80000494]:fcvt.h.wu zero, ra, dyn<br> [0x80000498]:csrrs a1, fcsr, zero<br> [0x8000049c]:sw zero, 32(t1)<br> |
