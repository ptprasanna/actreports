
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80000570')]      |
| SIG_REGION                | [('0x80002310', '0x80002430', '72 words')]      |
| COV_LABELS                | fmul_b6      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-fmul/fmul_b6-01.S/ref.S    |
| Total Number of coverpoints| 112     |
| Total Coverpoints Hit     | 112      |
| Total Signature Updates   | 68      |
| STAT1                     | 34      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 34     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fmul.h t6, t5, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80002318]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.h t4, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80002320]:0x00000020




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x30', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fmul.h t5, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80002328]:0x00000040




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fmul.h t3, t6, t4, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80002330]:0x00000060




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x27', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001a4]:fmul.h s11, s11, s11, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80002338]:0x00000080




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fmul.h s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80002340]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fmul.h s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80002348]:0x00000020




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fmul.h s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80002350]:0x00000040




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fmul.h s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80002358]:0x00000060




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fmul.h s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80002360]:0x00000080




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21']
Last Code Sequence : 
	-[0x80000264]:fmul.h s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80002368]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20']
Last Code Sequence : 
	-[0x80000284]:fmul.h s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80002370]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19']
Last Code Sequence : 
	-[0x800002a4]:fmul.h s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80002378]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18']
Last Code Sequence : 
	-[0x800002c4]:fmul.h s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80002380]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17']
Last Code Sequence : 
	-[0x800002e4]:fmul.h a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80002388]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16']
Last Code Sequence : 
	-[0x80000304]:fmul.h a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80002390]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15']
Last Code Sequence : 
	-[0x80000324]:fmul.h a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80002398]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14']
Last Code Sequence : 
	-[0x80000344]:fmul.h a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800023a0]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13']
Last Code Sequence : 
	-[0x80000364]:fmul.h a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800023a8]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12']
Last Code Sequence : 
	-[0x80000384]:fmul.h a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800023b0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11']
Last Code Sequence : 
	-[0x800003a4]:fmul.h a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800023b8]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10']
Last Code Sequence : 
	-[0x800003cc]:fmul.h a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800023c0]:0x00000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9']
Last Code Sequence : 
	-[0x800003ec]:fmul.h s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800023c8]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8']
Last Code Sequence : 
	-[0x8000040c]:fmul.h fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800023d0]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7']
Last Code Sequence : 
	-[0x80000434]:fmul.h t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800023d8]:0x00000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6']
Last Code Sequence : 
	-[0x80000454]:fmul.h t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800023e0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5']
Last Code Sequence : 
	-[0x80000474]:fmul.h t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800023e8]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4']
Last Code Sequence : 
	-[0x80000494]:fmul.h tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800023f0]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3']
Last Code Sequence : 
	-[0x800004b4]:fmul.h gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800023f8]:0x00000000




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2']
Last Code Sequence : 
	-[0x800004d4]:fmul.h sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x80002400]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fmul.h ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80002408]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0']
Last Code Sequence : 
	-[0x80000514]:fmul.h zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x80002410]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000534]:fmul.h t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80002418]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.h t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x80002420]:0x00000080





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                           |                                                      code                                                       |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80002314]<br>0xFFFF8000<br> |- mnemonic : fmul.h<br> - rs1 : x30<br> - rs2 : x31<br> - rd : x31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fmul.h t6, t5, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000231c]<br>0xFFFF8000<br> |- rs1 : x29<br> - rs2 : x30<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000144]:fmul.h t4, t4, t5, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80002324]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x30<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                           |[0x80000164]:fmul.h t5, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x8000232c]<br>0xFFFF8000<br> |- rs1 : x31<br> - rs2 : x29<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000184]:fmul.h t3, t6, t4, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>     |
|   5|[0x80002334]<br>0x00000000<br> |- rs1 : x27<br> - rs2 : x27<br> - rd : x27<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                           |[0x800001a4]:fmul.h s11, s11, s11, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s11, 32(ra)<br> |
|   6|[0x8000233c]<br>0x00000000<br> |- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fmul.h s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80002344]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800001e4]:fmul.h s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8000234c]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000204]:fmul.h s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80002354]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000224]:fmul.h s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000235c]<br>0x00000000<br> |- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000244]:fmul.h s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80002364]<br>0x00000000<br> |- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br>                                                                                                                                                                                                                                                  |[0x80000264]:fmul.h s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000236c]<br>0x00000000<br> |- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br>                                                                                                                                                                                                                                                  |[0x80000284]:fmul.h s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80002374]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br>                                                                                                                                                                                                                                                  |[0x800002a4]:fmul.h s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000237c]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br>                                                                                                                                                                                                                                                  |[0x800002c4]:fmul.h s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80002384]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br>                                                                                                                                                                                                                                                  |[0x800002e4]:fmul.h a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000238c]<br>0x00000000<br> |- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br>                                                                                                                                                                                                                                                  |[0x80000304]:fmul.h a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80002394]<br>0x00000000<br> |- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br>                                                                                                                                                                                                                                                  |[0x80000324]:fmul.h a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000239c]<br>0x00000000<br> |- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br>                                                                                                                                                                                                                                                  |[0x80000344]:fmul.h a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800023a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br>                                                                                                                                                                                                                                                  |[0x80000364]:fmul.h a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800023ac]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br>                                                                                                                                                                                                                                                  |[0x80000384]:fmul.h a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800023b4]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br>                                                                                                                                                                                                                                                   |[0x800003a4]:fmul.h a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800023bc]<br>0x00000000<br> |- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br>                                                                                                                                                                                                                                                   |[0x800003cc]:fmul.h a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800023c4]<br>0x00000000<br> |- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br>                                                                                                                                                                                                                                                   |[0x800003ec]:fmul.h s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800023cc]<br>0x00000000<br> |- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br>                                                                                                                                                                                                                                                     |[0x8000040c]:fmul.h fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800023d4]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br>                                                                                                                                                                                                                                                     |[0x80000434]:fmul.h t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800023dc]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br>                                                                                                                                                                                                                                                     |[0x80000454]:fmul.h t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800023e4]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br>                                                                                                                                                                                                                                                     |[0x80000474]:fmul.h t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800023ec]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br>                                                                                                                                                                                                                                                     |[0x80000494]:fmul.h tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800023f4]<br>0x00000000<br> |- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br>                                                                                                                                                                                                                                                     |[0x800004b4]:fmul.h gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800023fc]<br>0x00000000<br> |- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br>                                                                                                                                                                                                                                                     |[0x800004d4]:fmul.h sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80002404]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                     |[0x800004f4]:fmul.h ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x8000240c]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br>                                                                                                                                                                                                                                                     |[0x80000514]:fmul.h zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80002414]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000534]:fmul.h t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x8000241c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000554]:fmul.h t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
