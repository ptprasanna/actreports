
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x8000ea40')]      |
| SIG_REGION                | [('0x80012410', '0x800146d0', '2224 words')]      |
| COV_LABELS                | fmul_b3      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-fmul/fmul_b3-01.S/ref.S    |
| Total Number of coverpoints| 1207     |
| Total Coverpoints Hit     | 1207      |
| Total Signature Updates   | 2220      |
| STAT1                     | 1108      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 1110     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000e9bc]:fmul.h t6, t5, t4, dyn
      [0x8000e9c0]:csrrs a3, fcsr, zero
      [0x8000e9c4]:sw t6, 472(s1)
 -- Signature Addresses:
      Address: 0x800146ac Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000ea2c]:fmul.h t6, t5, t4, dyn
      [0x8000ea30]:csrrs a3, fcsr, zero
      [0x8000ea34]:sw t6, 488(s1)
 -- Signature Addresses:
      Address: 0x800146bc Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmul.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:fmul.h t6, t5, t6, dyn
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80012418]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x30', 'rd : x29', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:fmul.h t4, t4, t5, dyn
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t4, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80012420]:0x00000020




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x30', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x80000164]:fmul.h t5, t3, t3, dyn
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t5, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80012428]:0x00000045




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x28', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:fmul.h t3, t6, t4, dyn
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80012430]:0x00000060




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x27', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800001a4]:fmul.h s11, s11, s11, dyn
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80012438]:0x00000085




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x26', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:fmul.h s10, s9, s8, dyn
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80012440]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:fmul.h s9, s8, s10, dyn
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80012448]:0x00000020




Last Coverpoint : ['rs1 : x26', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:fmul.h s8, s10, s9, dyn
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80012450]:0x00000040




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x23', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:fmul.h s7, s6, s5, dyn
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80012458]:0x00000060




Last Coverpoint : ['rs1 : x21', 'rs2 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:fmul.h s6, s5, s7, dyn
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80012460]:0x00000080




Last Coverpoint : ['rs1 : x23', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:fmul.h s5, s7, s6, dyn
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80012468]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x20', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:fmul.h s4, s3, s2, dyn
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80012470]:0x00000020




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:fmul.h s3, s2, s4, dyn
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80012478]:0x00000040




Last Coverpoint : ['rs1 : x20', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:fmul.h s2, s4, s3, dyn
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80012480]:0x00000060




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x17', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:fmul.h a7, a6, a5, dyn
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80012488]:0x00000080




Last Coverpoint : ['rs1 : x15', 'rs2 : x17', 'rd : x16', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:fmul.h a6, a5, a7, dyn
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80012490]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:fmul.h a5, a7, a6, dyn
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80012498]:0x00000020




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x14', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:fmul.h a4, a3, a2, dyn
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800124a0]:0x00000040




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:fmul.h a3, a2, a4, dyn
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800124a8]:0x00000060




Last Coverpoint : ['rs1 : x14', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:fmul.h a2, a4, a3, dyn
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800124b0]:0x00000080




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x11', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:fmul.h a1, a0, s1, dyn
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800124b8]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fmul.h a0, s1, a1, dyn
	-[0x800003d0]:csrrs a3, fcsr, zero
	-[0x800003d4]:sw a0, 168(ra)
Current Store : [0x800003d8] : sw a3, 172(ra) -- Store: [0x800124c0]:0x00000020




Last Coverpoint : ['rs1 : x11', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:fmul.h s1, a1, a0, dyn
	-[0x800003f0]:csrrs a3, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a3, 180(ra) -- Store: [0x800124c8]:0x00000040




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x8', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fmul.h fp, t2, t1, dyn
	-[0x80000410]:csrrs a3, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a3, 188(ra) -- Store: [0x800124d0]:0x00000060




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000434]:fmul.h t2, t1, fp, dyn
	-[0x80000438]:csrrs a3, fcsr, zero
	-[0x8000043c]:sw t2, 0(s1)
Current Store : [0x80000440] : sw a3, 4(s1) -- Store: [0x800124d8]:0x00000080




Last Coverpoint : ['rs1 : x8', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:fmul.h t1, fp, t2, dyn
	-[0x80000458]:csrrs a3, fcsr, zero
	-[0x8000045c]:sw t1, 8(s1)
Current Store : [0x80000460] : sw a3, 12(s1) -- Store: [0x800124e0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x5', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:fmul.h t0, tp, gp, dyn
	-[0x80000478]:csrrs a3, fcsr, zero
	-[0x8000047c]:sw t0, 16(s1)
Current Store : [0x80000480] : sw a3, 20(s1) -- Store: [0x800124e8]:0x00000020




Last Coverpoint : ['rs1 : x3', 'rs2 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:fmul.h tp, gp, t0, dyn
	-[0x80000498]:csrrs a3, fcsr, zero
	-[0x8000049c]:sw tp, 24(s1)
Current Store : [0x800004a0] : sw a3, 28(s1) -- Store: [0x800124f0]:0x00000040




Last Coverpoint : ['rs1 : x5', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fmul.h gp, t0, tp, dyn
	-[0x800004b8]:csrrs a3, fcsr, zero
	-[0x800004bc]:sw gp, 32(s1)
Current Store : [0x800004c0] : sw a3, 36(s1) -- Store: [0x800124f8]:0x00000060




Last Coverpoint : ['rs1 : x1', 'rs2 : x0', 'rd : x2', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fmul.h sp, ra, zero, dyn
	-[0x800004d8]:csrrs a3, fcsr, zero
	-[0x800004dc]:sw sp, 40(s1)
Current Store : [0x800004e0] : sw a3, 44(s1) -- Store: [0x80012500]:0x00000080




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:fmul.h ra, zero, sp, dyn
	-[0x800004f8]:csrrs a3, fcsr, zero
	-[0x800004fc]:sw ra, 48(s1)
Current Store : [0x80000500] : sw a3, 52(s1) -- Store: [0x80012508]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x1', 'rd : x0', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000514]:fmul.h zero, sp, ra, dyn
	-[0x80000518]:csrrs a3, fcsr, zero
	-[0x8000051c]:sw zero, 56(s1)
Current Store : [0x80000520] : sw a3, 60(s1) -- Store: [0x80012510]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000534]:fmul.h t6, t5, t4, dyn
	-[0x80000538]:csrrs a3, fcsr, zero
	-[0x8000053c]:sw t6, 64(s1)
Current Store : [0x80000540] : sw a3, 68(s1) -- Store: [0x80012518]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:fmul.h t6, t5, t4, dyn
	-[0x80000558]:csrrs a3, fcsr, zero
	-[0x8000055c]:sw t6, 72(s1)
Current Store : [0x80000560] : sw a3, 76(s1) -- Store: [0x80012520]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:fmul.h t6, t5, t4, dyn
	-[0x80000578]:csrrs a3, fcsr, zero
	-[0x8000057c]:sw t6, 80(s1)
Current Store : [0x80000580] : sw a3, 84(s1) -- Store: [0x80012528]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fmul.h t6, t5, t4, dyn
	-[0x80000598]:csrrs a3, fcsr, zero
	-[0x8000059c]:sw t6, 88(s1)
Current Store : [0x800005a0] : sw a3, 92(s1) -- Store: [0x80012530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:fmul.h t6, t5, t4, dyn
	-[0x800005b8]:csrrs a3, fcsr, zero
	-[0x800005bc]:sw t6, 96(s1)
Current Store : [0x800005c0] : sw a3, 100(s1) -- Store: [0x80012538]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:fmul.h t6, t5, t4, dyn
	-[0x800005d8]:csrrs a3, fcsr, zero
	-[0x800005dc]:sw t6, 104(s1)
Current Store : [0x800005e0] : sw a3, 108(s1) -- Store: [0x80012540]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fmul.h t6, t5, t4, dyn
	-[0x800005f8]:csrrs a3, fcsr, zero
	-[0x800005fc]:sw t6, 112(s1)
Current Store : [0x80000600] : sw a3, 116(s1) -- Store: [0x80012548]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:fmul.h t6, t5, t4, dyn
	-[0x80000618]:csrrs a3, fcsr, zero
	-[0x8000061c]:sw t6, 120(s1)
Current Store : [0x80000620] : sw a3, 124(s1) -- Store: [0x80012550]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:fmul.h t6, t5, t4, dyn
	-[0x80000638]:csrrs a3, fcsr, zero
	-[0x8000063c]:sw t6, 128(s1)
Current Store : [0x80000640] : sw a3, 132(s1) -- Store: [0x80012558]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:fmul.h t6, t5, t4, dyn
	-[0x80000658]:csrrs a3, fcsr, zero
	-[0x8000065c]:sw t6, 136(s1)
Current Store : [0x80000660] : sw a3, 140(s1) -- Store: [0x80012560]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:fmul.h t6, t5, t4, dyn
	-[0x80000678]:csrrs a3, fcsr, zero
	-[0x8000067c]:sw t6, 144(s1)
Current Store : [0x80000680] : sw a3, 148(s1) -- Store: [0x80012568]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:fmul.h t6, t5, t4, dyn
	-[0x80000698]:csrrs a3, fcsr, zero
	-[0x8000069c]:sw t6, 152(s1)
Current Store : [0x800006a0] : sw a3, 156(s1) -- Store: [0x80012570]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:fmul.h t6, t5, t4, dyn
	-[0x800006b8]:csrrs a3, fcsr, zero
	-[0x800006bc]:sw t6, 160(s1)
Current Store : [0x800006c0] : sw a3, 164(s1) -- Store: [0x80012578]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:fmul.h t6, t5, t4, dyn
	-[0x800006d8]:csrrs a3, fcsr, zero
	-[0x800006dc]:sw t6, 168(s1)
Current Store : [0x800006e0] : sw a3, 172(s1) -- Store: [0x80012580]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fmul.h t6, t5, t4, dyn
	-[0x800006f8]:csrrs a3, fcsr, zero
	-[0x800006fc]:sw t6, 176(s1)
Current Store : [0x80000700] : sw a3, 180(s1) -- Store: [0x80012588]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:fmul.h t6, t5, t4, dyn
	-[0x80000718]:csrrs a3, fcsr, zero
	-[0x8000071c]:sw t6, 184(s1)
Current Store : [0x80000720] : sw a3, 188(s1) -- Store: [0x80012590]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:fmul.h t6, t5, t4, dyn
	-[0x80000738]:csrrs a3, fcsr, zero
	-[0x8000073c]:sw t6, 192(s1)
Current Store : [0x80000740] : sw a3, 196(s1) -- Store: [0x80012598]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:fmul.h t6, t5, t4, dyn
	-[0x80000758]:csrrs a3, fcsr, zero
	-[0x8000075c]:sw t6, 200(s1)
Current Store : [0x80000760] : sw a3, 204(s1) -- Store: [0x800125a0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:fmul.h t6, t5, t4, dyn
	-[0x80000778]:csrrs a3, fcsr, zero
	-[0x8000077c]:sw t6, 208(s1)
Current Store : [0x80000780] : sw a3, 212(s1) -- Store: [0x800125a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:fmul.h t6, t5, t4, dyn
	-[0x80000798]:csrrs a3, fcsr, zero
	-[0x8000079c]:sw t6, 216(s1)
Current Store : [0x800007a0] : sw a3, 220(s1) -- Store: [0x800125b0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:fmul.h t6, t5, t4, dyn
	-[0x800007b8]:csrrs a3, fcsr, zero
	-[0x800007bc]:sw t6, 224(s1)
Current Store : [0x800007c0] : sw a3, 228(s1) -- Store: [0x800125b8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fmul.h t6, t5, t4, dyn
	-[0x800007d8]:csrrs a3, fcsr, zero
	-[0x800007dc]:sw t6, 232(s1)
Current Store : [0x800007e0] : sw a3, 236(s1) -- Store: [0x800125c0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:fmul.h t6, t5, t4, dyn
	-[0x800007f8]:csrrs a3, fcsr, zero
	-[0x800007fc]:sw t6, 240(s1)
Current Store : [0x80000800] : sw a3, 244(s1) -- Store: [0x800125c8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:fmul.h t6, t5, t4, dyn
	-[0x80000818]:csrrs a3, fcsr, zero
	-[0x8000081c]:sw t6, 248(s1)
Current Store : [0x80000820] : sw a3, 252(s1) -- Store: [0x800125d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:fmul.h t6, t5, t4, dyn
	-[0x80000838]:csrrs a3, fcsr, zero
	-[0x8000083c]:sw t6, 256(s1)
Current Store : [0x80000840] : sw a3, 260(s1) -- Store: [0x800125d8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:fmul.h t6, t5, t4, dyn
	-[0x80000858]:csrrs a3, fcsr, zero
	-[0x8000085c]:sw t6, 264(s1)
Current Store : [0x80000860] : sw a3, 268(s1) -- Store: [0x800125e0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:fmul.h t6, t5, t4, dyn
	-[0x80000878]:csrrs a3, fcsr, zero
	-[0x8000087c]:sw t6, 272(s1)
Current Store : [0x80000880] : sw a3, 276(s1) -- Store: [0x800125e8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:fmul.h t6, t5, t4, dyn
	-[0x80000898]:csrrs a3, fcsr, zero
	-[0x8000089c]:sw t6, 280(s1)
Current Store : [0x800008a0] : sw a3, 284(s1) -- Store: [0x800125f0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fmul.h t6, t5, t4, dyn
	-[0x800008b8]:csrrs a3, fcsr, zero
	-[0x800008bc]:sw t6, 288(s1)
Current Store : [0x800008c0] : sw a3, 292(s1) -- Store: [0x800125f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d4]:fmul.h t6, t5, t4, dyn
	-[0x800008d8]:csrrs a3, fcsr, zero
	-[0x800008dc]:sw t6, 296(s1)
Current Store : [0x800008e0] : sw a3, 300(s1) -- Store: [0x80012600]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fmul.h t6, t5, t4, dyn
	-[0x800008f8]:csrrs a3, fcsr, zero
	-[0x800008fc]:sw t6, 304(s1)
Current Store : [0x80000900] : sw a3, 308(s1) -- Store: [0x80012608]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000914]:fmul.h t6, t5, t4, dyn
	-[0x80000918]:csrrs a3, fcsr, zero
	-[0x8000091c]:sw t6, 312(s1)
Current Store : [0x80000920] : sw a3, 316(s1) -- Store: [0x80012610]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000934]:fmul.h t6, t5, t4, dyn
	-[0x80000938]:csrrs a3, fcsr, zero
	-[0x8000093c]:sw t6, 320(s1)
Current Store : [0x80000940] : sw a3, 324(s1) -- Store: [0x80012618]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000954]:fmul.h t6, t5, t4, dyn
	-[0x80000958]:csrrs a3, fcsr, zero
	-[0x8000095c]:sw t6, 328(s1)
Current Store : [0x80000960] : sw a3, 332(s1) -- Store: [0x80012620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:fmul.h t6, t5, t4, dyn
	-[0x80000978]:csrrs a3, fcsr, zero
	-[0x8000097c]:sw t6, 336(s1)
Current Store : [0x80000980] : sw a3, 340(s1) -- Store: [0x80012628]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fmul.h t6, t5, t4, dyn
	-[0x80000998]:csrrs a3, fcsr, zero
	-[0x8000099c]:sw t6, 344(s1)
Current Store : [0x800009a0] : sw a3, 348(s1) -- Store: [0x80012630]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fmul.h t6, t5, t4, dyn
	-[0x800009b8]:csrrs a3, fcsr, zero
	-[0x800009bc]:sw t6, 352(s1)
Current Store : [0x800009c0] : sw a3, 356(s1) -- Store: [0x80012638]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009d4]:fmul.h t6, t5, t4, dyn
	-[0x800009d8]:csrrs a3, fcsr, zero
	-[0x800009dc]:sw t6, 360(s1)
Current Store : [0x800009e0] : sw a3, 364(s1) -- Store: [0x80012640]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009f4]:fmul.h t6, t5, t4, dyn
	-[0x800009f8]:csrrs a3, fcsr, zero
	-[0x800009fc]:sw t6, 368(s1)
Current Store : [0x80000a00] : sw a3, 372(s1) -- Store: [0x80012648]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fmul.h t6, t5, t4, dyn
	-[0x80000a18]:csrrs a3, fcsr, zero
	-[0x80000a1c]:sw t6, 376(s1)
Current Store : [0x80000a20] : sw a3, 380(s1) -- Store: [0x80012650]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a34]:fmul.h t6, t5, t4, dyn
	-[0x80000a38]:csrrs a3, fcsr, zero
	-[0x80000a3c]:sw t6, 384(s1)
Current Store : [0x80000a40] : sw a3, 388(s1) -- Store: [0x80012658]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a54]:fmul.h t6, t5, t4, dyn
	-[0x80000a58]:csrrs a3, fcsr, zero
	-[0x80000a5c]:sw t6, 392(s1)
Current Store : [0x80000a60] : sw a3, 396(s1) -- Store: [0x80012660]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fmul.h t6, t5, t4, dyn
	-[0x80000a78]:csrrs a3, fcsr, zero
	-[0x80000a7c]:sw t6, 400(s1)
Current Store : [0x80000a80] : sw a3, 404(s1) -- Store: [0x80012668]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:fmul.h t6, t5, t4, dyn
	-[0x80000a98]:csrrs a3, fcsr, zero
	-[0x80000a9c]:sw t6, 408(s1)
Current Store : [0x80000aa0] : sw a3, 412(s1) -- Store: [0x80012670]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:fmul.h t6, t5, t4, dyn
	-[0x80000ab8]:csrrs a3, fcsr, zero
	-[0x80000abc]:sw t6, 416(s1)
Current Store : [0x80000ac0] : sw a3, 420(s1) -- Store: [0x80012678]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ad4]:fmul.h t6, t5, t4, dyn
	-[0x80000ad8]:csrrs a3, fcsr, zero
	-[0x80000adc]:sw t6, 424(s1)
Current Store : [0x80000ae0] : sw a3, 428(s1) -- Store: [0x80012680]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000af4]:fmul.h t6, t5, t4, dyn
	-[0x80000af8]:csrrs a3, fcsr, zero
	-[0x80000afc]:sw t6, 432(s1)
Current Store : [0x80000b00] : sw a3, 436(s1) -- Store: [0x80012688]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b14]:fmul.h t6, t5, t4, dyn
	-[0x80000b18]:csrrs a3, fcsr, zero
	-[0x80000b1c]:sw t6, 440(s1)
Current Store : [0x80000b20] : sw a3, 444(s1) -- Store: [0x80012690]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b34]:fmul.h t6, t5, t4, dyn
	-[0x80000b38]:csrrs a3, fcsr, zero
	-[0x80000b3c]:sw t6, 448(s1)
Current Store : [0x80000b40] : sw a3, 452(s1) -- Store: [0x80012698]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fmul.h t6, t5, t4, dyn
	-[0x80000b58]:csrrs a3, fcsr, zero
	-[0x80000b5c]:sw t6, 456(s1)
Current Store : [0x80000b60] : sw a3, 460(s1) -- Store: [0x800126a0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b74]:fmul.h t6, t5, t4, dyn
	-[0x80000b78]:csrrs a3, fcsr, zero
	-[0x80000b7c]:sw t6, 464(s1)
Current Store : [0x80000b80] : sw a3, 468(s1) -- Store: [0x800126a8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b94]:fmul.h t6, t5, t4, dyn
	-[0x80000b98]:csrrs a3, fcsr, zero
	-[0x80000b9c]:sw t6, 472(s1)
Current Store : [0x80000ba0] : sw a3, 476(s1) -- Store: [0x800126b0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:fmul.h t6, t5, t4, dyn
	-[0x80000bb8]:csrrs a3, fcsr, zero
	-[0x80000bbc]:sw t6, 480(s1)
Current Store : [0x80000bc0] : sw a3, 484(s1) -- Store: [0x800126b8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd4]:fmul.h t6, t5, t4, dyn
	-[0x80000bd8]:csrrs a3, fcsr, zero
	-[0x80000bdc]:sw t6, 488(s1)
Current Store : [0x80000be0] : sw a3, 492(s1) -- Store: [0x800126c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:fmul.h t6, t5, t4, dyn
	-[0x80000bf8]:csrrs a3, fcsr, zero
	-[0x80000bfc]:sw t6, 496(s1)
Current Store : [0x80000c00] : sw a3, 500(s1) -- Store: [0x800126c8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c14]:fmul.h t6, t5, t4, dyn
	-[0x80000c18]:csrrs a3, fcsr, zero
	-[0x80000c1c]:sw t6, 504(s1)
Current Store : [0x80000c20] : sw a3, 508(s1) -- Store: [0x800126d0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fmul.h t6, t5, t4, dyn
	-[0x80000c38]:csrrs a3, fcsr, zero
	-[0x80000c3c]:sw t6, 512(s1)
Current Store : [0x80000c40] : sw a3, 516(s1) -- Store: [0x800126d8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c54]:fmul.h t6, t5, t4, dyn
	-[0x80000c58]:csrrs a3, fcsr, zero
	-[0x80000c5c]:sw t6, 520(s1)
Current Store : [0x80000c60] : sw a3, 524(s1) -- Store: [0x800126e0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c74]:fmul.h t6, t5, t4, dyn
	-[0x80000c78]:csrrs a3, fcsr, zero
	-[0x80000c7c]:sw t6, 528(s1)
Current Store : [0x80000c80] : sw a3, 532(s1) -- Store: [0x800126e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c94]:fmul.h t6, t5, t4, dyn
	-[0x80000c98]:csrrs a3, fcsr, zero
	-[0x80000c9c]:sw t6, 536(s1)
Current Store : [0x80000ca0] : sw a3, 540(s1) -- Store: [0x800126f0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:fmul.h t6, t5, t4, dyn
	-[0x80000cb8]:csrrs a3, fcsr, zero
	-[0x80000cbc]:sw t6, 544(s1)
Current Store : [0x80000cc0] : sw a3, 548(s1) -- Store: [0x800126f8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:fmul.h t6, t5, t4, dyn
	-[0x80000cd8]:csrrs a3, fcsr, zero
	-[0x80000cdc]:sw t6, 552(s1)
Current Store : [0x80000ce0] : sw a3, 556(s1) -- Store: [0x80012700]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:fmul.h t6, t5, t4, dyn
	-[0x80000cf8]:csrrs a3, fcsr, zero
	-[0x80000cfc]:sw t6, 560(s1)
Current Store : [0x80000d00] : sw a3, 564(s1) -- Store: [0x80012708]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fmul.h t6, t5, t4, dyn
	-[0x80000d18]:csrrs a3, fcsr, zero
	-[0x80000d1c]:sw t6, 568(s1)
Current Store : [0x80000d20] : sw a3, 572(s1) -- Store: [0x80012710]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d34]:fmul.h t6, t5, t4, dyn
	-[0x80000d38]:csrrs a3, fcsr, zero
	-[0x80000d3c]:sw t6, 576(s1)
Current Store : [0x80000d40] : sw a3, 580(s1) -- Store: [0x80012718]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d54]:fmul.h t6, t5, t4, dyn
	-[0x80000d58]:csrrs a3, fcsr, zero
	-[0x80000d5c]:sw t6, 584(s1)
Current Store : [0x80000d60] : sw a3, 588(s1) -- Store: [0x80012720]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d74]:fmul.h t6, t5, t4, dyn
	-[0x80000d78]:csrrs a3, fcsr, zero
	-[0x80000d7c]:sw t6, 592(s1)
Current Store : [0x80000d80] : sw a3, 596(s1) -- Store: [0x80012728]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d94]:fmul.h t6, t5, t4, dyn
	-[0x80000d98]:csrrs a3, fcsr, zero
	-[0x80000d9c]:sw t6, 600(s1)
Current Store : [0x80000da0] : sw a3, 604(s1) -- Store: [0x80012730]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000db4]:fmul.h t6, t5, t4, dyn
	-[0x80000db8]:csrrs a3, fcsr, zero
	-[0x80000dbc]:sw t6, 608(s1)
Current Store : [0x80000dc0] : sw a3, 612(s1) -- Store: [0x80012738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:fmul.h t6, t5, t4, dyn
	-[0x80000dd8]:csrrs a3, fcsr, zero
	-[0x80000ddc]:sw t6, 616(s1)
Current Store : [0x80000de0] : sw a3, 620(s1) -- Store: [0x80012740]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fmul.h t6, t5, t4, dyn
	-[0x80000df8]:csrrs a3, fcsr, zero
	-[0x80000dfc]:sw t6, 624(s1)
Current Store : [0x80000e00] : sw a3, 628(s1) -- Store: [0x80012748]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e14]:fmul.h t6, t5, t4, dyn
	-[0x80000e18]:csrrs a3, fcsr, zero
	-[0x80000e1c]:sw t6, 632(s1)
Current Store : [0x80000e20] : sw a3, 636(s1) -- Store: [0x80012750]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e34]:fmul.h t6, t5, t4, dyn
	-[0x80000e38]:csrrs a3, fcsr, zero
	-[0x80000e3c]:sw t6, 640(s1)
Current Store : [0x80000e40] : sw a3, 644(s1) -- Store: [0x80012758]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e54]:fmul.h t6, t5, t4, dyn
	-[0x80000e58]:csrrs a3, fcsr, zero
	-[0x80000e5c]:sw t6, 648(s1)
Current Store : [0x80000e60] : sw a3, 652(s1) -- Store: [0x80012760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e74]:fmul.h t6, t5, t4, dyn
	-[0x80000e78]:csrrs a3, fcsr, zero
	-[0x80000e7c]:sw t6, 656(s1)
Current Store : [0x80000e80] : sw a3, 660(s1) -- Store: [0x80012768]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e94]:fmul.h t6, t5, t4, dyn
	-[0x80000e98]:csrrs a3, fcsr, zero
	-[0x80000e9c]:sw t6, 664(s1)
Current Store : [0x80000ea0] : sw a3, 668(s1) -- Store: [0x80012770]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:fmul.h t6, t5, t4, dyn
	-[0x80000eb8]:csrrs a3, fcsr, zero
	-[0x80000ebc]:sw t6, 672(s1)
Current Store : [0x80000ec0] : sw a3, 676(s1) -- Store: [0x80012778]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fmul.h t6, t5, t4, dyn
	-[0x80000ed8]:csrrs a3, fcsr, zero
	-[0x80000edc]:sw t6, 680(s1)
Current Store : [0x80000ee0] : sw a3, 684(s1) -- Store: [0x80012780]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ef4]:fmul.h t6, t5, t4, dyn
	-[0x80000ef8]:csrrs a3, fcsr, zero
	-[0x80000efc]:sw t6, 688(s1)
Current Store : [0x80000f00] : sw a3, 692(s1) -- Store: [0x80012788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f14]:fmul.h t6, t5, t4, dyn
	-[0x80000f18]:csrrs a3, fcsr, zero
	-[0x80000f1c]:sw t6, 696(s1)
Current Store : [0x80000f20] : sw a3, 700(s1) -- Store: [0x80012790]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f34]:fmul.h t6, t5, t4, dyn
	-[0x80000f38]:csrrs a3, fcsr, zero
	-[0x80000f3c]:sw t6, 704(s1)
Current Store : [0x80000f40] : sw a3, 708(s1) -- Store: [0x80012798]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f54]:fmul.h t6, t5, t4, dyn
	-[0x80000f58]:csrrs a3, fcsr, zero
	-[0x80000f5c]:sw t6, 712(s1)
Current Store : [0x80000f60] : sw a3, 716(s1) -- Store: [0x800127a0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f74]:fmul.h t6, t5, t4, dyn
	-[0x80000f78]:csrrs a3, fcsr, zero
	-[0x80000f7c]:sw t6, 720(s1)
Current Store : [0x80000f80] : sw a3, 724(s1) -- Store: [0x800127a8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f94]:fmul.h t6, t5, t4, dyn
	-[0x80000f98]:csrrs a3, fcsr, zero
	-[0x80000f9c]:sw t6, 728(s1)
Current Store : [0x80000fa0] : sw a3, 732(s1) -- Store: [0x800127b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fb4]:fmul.h t6, t5, t4, dyn
	-[0x80000fb8]:csrrs a3, fcsr, zero
	-[0x80000fbc]:sw t6, 736(s1)
Current Store : [0x80000fc0] : sw a3, 740(s1) -- Store: [0x800127b8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fd4]:fmul.h t6, t5, t4, dyn
	-[0x80000fd8]:csrrs a3, fcsr, zero
	-[0x80000fdc]:sw t6, 744(s1)
Current Store : [0x80000fe0] : sw a3, 748(s1) -- Store: [0x800127c0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ff4]:fmul.h t6, t5, t4, dyn
	-[0x80000ff8]:csrrs a3, fcsr, zero
	-[0x80000ffc]:sw t6, 752(s1)
Current Store : [0x80001000] : sw a3, 756(s1) -- Store: [0x800127c8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001014]:fmul.h t6, t5, t4, dyn
	-[0x80001018]:csrrs a3, fcsr, zero
	-[0x8000101c]:sw t6, 760(s1)
Current Store : [0x80001020] : sw a3, 764(s1) -- Store: [0x800127d0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001034]:fmul.h t6, t5, t4, dyn
	-[0x80001038]:csrrs a3, fcsr, zero
	-[0x8000103c]:sw t6, 768(s1)
Current Store : [0x80001040] : sw a3, 772(s1) -- Store: [0x800127d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001054]:fmul.h t6, t5, t4, dyn
	-[0x80001058]:csrrs a3, fcsr, zero
	-[0x8000105c]:sw t6, 776(s1)
Current Store : [0x80001060] : sw a3, 780(s1) -- Store: [0x800127e0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001074]:fmul.h t6, t5, t4, dyn
	-[0x80001078]:csrrs a3, fcsr, zero
	-[0x8000107c]:sw t6, 784(s1)
Current Store : [0x80001080] : sw a3, 788(s1) -- Store: [0x800127e8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001094]:fmul.h t6, t5, t4, dyn
	-[0x80001098]:csrrs a3, fcsr, zero
	-[0x8000109c]:sw t6, 792(s1)
Current Store : [0x800010a0] : sw a3, 796(s1) -- Store: [0x800127f0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010b4]:fmul.h t6, t5, t4, dyn
	-[0x800010b8]:csrrs a3, fcsr, zero
	-[0x800010bc]:sw t6, 800(s1)
Current Store : [0x800010c0] : sw a3, 804(s1) -- Store: [0x800127f8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010d4]:fmul.h t6, t5, t4, dyn
	-[0x800010d8]:csrrs a3, fcsr, zero
	-[0x800010dc]:sw t6, 808(s1)
Current Store : [0x800010e0] : sw a3, 812(s1) -- Store: [0x80012800]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010f4]:fmul.h t6, t5, t4, dyn
	-[0x800010f8]:csrrs a3, fcsr, zero
	-[0x800010fc]:sw t6, 816(s1)
Current Store : [0x80001100] : sw a3, 820(s1) -- Store: [0x80012808]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001114]:fmul.h t6, t5, t4, dyn
	-[0x80001118]:csrrs a3, fcsr, zero
	-[0x8000111c]:sw t6, 824(s1)
Current Store : [0x80001120] : sw a3, 828(s1) -- Store: [0x80012810]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001134]:fmul.h t6, t5, t4, dyn
	-[0x80001138]:csrrs a3, fcsr, zero
	-[0x8000113c]:sw t6, 832(s1)
Current Store : [0x80001140] : sw a3, 836(s1) -- Store: [0x80012818]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001154]:fmul.h t6, t5, t4, dyn
	-[0x80001158]:csrrs a3, fcsr, zero
	-[0x8000115c]:sw t6, 840(s1)
Current Store : [0x80001160] : sw a3, 844(s1) -- Store: [0x80012820]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001174]:fmul.h t6, t5, t4, dyn
	-[0x80001178]:csrrs a3, fcsr, zero
	-[0x8000117c]:sw t6, 848(s1)
Current Store : [0x80001180] : sw a3, 852(s1) -- Store: [0x80012828]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001194]:fmul.h t6, t5, t4, dyn
	-[0x80001198]:csrrs a3, fcsr, zero
	-[0x8000119c]:sw t6, 856(s1)
Current Store : [0x800011a0] : sw a3, 860(s1) -- Store: [0x80012830]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011b4]:fmul.h t6, t5, t4, dyn
	-[0x800011b8]:csrrs a3, fcsr, zero
	-[0x800011bc]:sw t6, 864(s1)
Current Store : [0x800011c0] : sw a3, 868(s1) -- Store: [0x80012838]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011d4]:fmul.h t6, t5, t4, dyn
	-[0x800011d8]:csrrs a3, fcsr, zero
	-[0x800011dc]:sw t6, 872(s1)
Current Store : [0x800011e0] : sw a3, 876(s1) -- Store: [0x80012840]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011f4]:fmul.h t6, t5, t4, dyn
	-[0x800011f8]:csrrs a3, fcsr, zero
	-[0x800011fc]:sw t6, 880(s1)
Current Store : [0x80001200] : sw a3, 884(s1) -- Store: [0x80012848]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001214]:fmul.h t6, t5, t4, dyn
	-[0x80001218]:csrrs a3, fcsr, zero
	-[0x8000121c]:sw t6, 888(s1)
Current Store : [0x80001220] : sw a3, 892(s1) -- Store: [0x80012850]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001234]:fmul.h t6, t5, t4, dyn
	-[0x80001238]:csrrs a3, fcsr, zero
	-[0x8000123c]:sw t6, 896(s1)
Current Store : [0x80001240] : sw a3, 900(s1) -- Store: [0x80012858]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001254]:fmul.h t6, t5, t4, dyn
	-[0x80001258]:csrrs a3, fcsr, zero
	-[0x8000125c]:sw t6, 904(s1)
Current Store : [0x80001260] : sw a3, 908(s1) -- Store: [0x80012860]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001274]:fmul.h t6, t5, t4, dyn
	-[0x80001278]:csrrs a3, fcsr, zero
	-[0x8000127c]:sw t6, 912(s1)
Current Store : [0x80001280] : sw a3, 916(s1) -- Store: [0x80012868]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001294]:fmul.h t6, t5, t4, dyn
	-[0x80001298]:csrrs a3, fcsr, zero
	-[0x8000129c]:sw t6, 920(s1)
Current Store : [0x800012a0] : sw a3, 924(s1) -- Store: [0x80012870]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012b4]:fmul.h t6, t5, t4, dyn
	-[0x800012b8]:csrrs a3, fcsr, zero
	-[0x800012bc]:sw t6, 928(s1)
Current Store : [0x800012c0] : sw a3, 932(s1) -- Store: [0x80012878]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012d4]:fmul.h t6, t5, t4, dyn
	-[0x800012d8]:csrrs a3, fcsr, zero
	-[0x800012dc]:sw t6, 936(s1)
Current Store : [0x800012e0] : sw a3, 940(s1) -- Store: [0x80012880]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012f4]:fmul.h t6, t5, t4, dyn
	-[0x800012f8]:csrrs a3, fcsr, zero
	-[0x800012fc]:sw t6, 944(s1)
Current Store : [0x80001300] : sw a3, 948(s1) -- Store: [0x80012888]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001314]:fmul.h t6, t5, t4, dyn
	-[0x80001318]:csrrs a3, fcsr, zero
	-[0x8000131c]:sw t6, 952(s1)
Current Store : [0x80001320] : sw a3, 956(s1) -- Store: [0x80012890]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001334]:fmul.h t6, t5, t4, dyn
	-[0x80001338]:csrrs a3, fcsr, zero
	-[0x8000133c]:sw t6, 960(s1)
Current Store : [0x80001340] : sw a3, 964(s1) -- Store: [0x80012898]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001354]:fmul.h t6, t5, t4, dyn
	-[0x80001358]:csrrs a3, fcsr, zero
	-[0x8000135c]:sw t6, 968(s1)
Current Store : [0x80001360] : sw a3, 972(s1) -- Store: [0x800128a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001374]:fmul.h t6, t5, t4, dyn
	-[0x80001378]:csrrs a3, fcsr, zero
	-[0x8000137c]:sw t6, 976(s1)
Current Store : [0x80001380] : sw a3, 980(s1) -- Store: [0x800128a8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001394]:fmul.h t6, t5, t4, dyn
	-[0x80001398]:csrrs a3, fcsr, zero
	-[0x8000139c]:sw t6, 984(s1)
Current Store : [0x800013a0] : sw a3, 988(s1) -- Store: [0x800128b0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013b4]:fmul.h t6, t5, t4, dyn
	-[0x800013b8]:csrrs a3, fcsr, zero
	-[0x800013bc]:sw t6, 992(s1)
Current Store : [0x800013c0] : sw a3, 996(s1) -- Store: [0x800128b8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013d4]:fmul.h t6, t5, t4, dyn
	-[0x800013d8]:csrrs a3, fcsr, zero
	-[0x800013dc]:sw t6, 1000(s1)
Current Store : [0x800013e0] : sw a3, 1004(s1) -- Store: [0x800128c0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013f4]:fmul.h t6, t5, t4, dyn
	-[0x800013f8]:csrrs a3, fcsr, zero
	-[0x800013fc]:sw t6, 1008(s1)
Current Store : [0x80001400] : sw a3, 1012(s1) -- Store: [0x800128c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001414]:fmul.h t6, t5, t4, dyn
	-[0x80001418]:csrrs a3, fcsr, zero
	-[0x8000141c]:sw t6, 1016(s1)
Current Store : [0x80001420] : sw a3, 1020(s1) -- Store: [0x800128d0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000143c]:fmul.h t6, t5, t4, dyn
	-[0x80001440]:csrrs a3, fcsr, zero
	-[0x80001444]:sw t6, 0(s1)
Current Store : [0x80001448] : sw a3, 4(s1) -- Store: [0x800128d8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000145c]:fmul.h t6, t5, t4, dyn
	-[0x80001460]:csrrs a3, fcsr, zero
	-[0x80001464]:sw t6, 8(s1)
Current Store : [0x80001468] : sw a3, 12(s1) -- Store: [0x800128e0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000147c]:fmul.h t6, t5, t4, dyn
	-[0x80001480]:csrrs a3, fcsr, zero
	-[0x80001484]:sw t6, 16(s1)
Current Store : [0x80001488] : sw a3, 20(s1) -- Store: [0x800128e8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000149c]:fmul.h t6, t5, t4, dyn
	-[0x800014a0]:csrrs a3, fcsr, zero
	-[0x800014a4]:sw t6, 24(s1)
Current Store : [0x800014a8] : sw a3, 28(s1) -- Store: [0x800128f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014bc]:fmul.h t6, t5, t4, dyn
	-[0x800014c0]:csrrs a3, fcsr, zero
	-[0x800014c4]:sw t6, 32(s1)
Current Store : [0x800014c8] : sw a3, 36(s1) -- Store: [0x800128f8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014dc]:fmul.h t6, t5, t4, dyn
	-[0x800014e0]:csrrs a3, fcsr, zero
	-[0x800014e4]:sw t6, 40(s1)
Current Store : [0x800014e8] : sw a3, 44(s1) -- Store: [0x80012900]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014fc]:fmul.h t6, t5, t4, dyn
	-[0x80001500]:csrrs a3, fcsr, zero
	-[0x80001504]:sw t6, 48(s1)
Current Store : [0x80001508] : sw a3, 52(s1) -- Store: [0x80012908]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000151c]:fmul.h t6, t5, t4, dyn
	-[0x80001520]:csrrs a3, fcsr, zero
	-[0x80001524]:sw t6, 56(s1)
Current Store : [0x80001528] : sw a3, 60(s1) -- Store: [0x80012910]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000153c]:fmul.h t6, t5, t4, dyn
	-[0x80001540]:csrrs a3, fcsr, zero
	-[0x80001544]:sw t6, 64(s1)
Current Store : [0x80001548] : sw a3, 68(s1) -- Store: [0x80012918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000155c]:fmul.h t6, t5, t4, dyn
	-[0x80001560]:csrrs a3, fcsr, zero
	-[0x80001564]:sw t6, 72(s1)
Current Store : [0x80001568] : sw a3, 76(s1) -- Store: [0x80012920]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000157c]:fmul.h t6, t5, t4, dyn
	-[0x80001580]:csrrs a3, fcsr, zero
	-[0x80001584]:sw t6, 80(s1)
Current Store : [0x80001588] : sw a3, 84(s1) -- Store: [0x80012928]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000159c]:fmul.h t6, t5, t4, dyn
	-[0x800015a0]:csrrs a3, fcsr, zero
	-[0x800015a4]:sw t6, 88(s1)
Current Store : [0x800015a8] : sw a3, 92(s1) -- Store: [0x80012930]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015bc]:fmul.h t6, t5, t4, dyn
	-[0x800015c0]:csrrs a3, fcsr, zero
	-[0x800015c4]:sw t6, 96(s1)
Current Store : [0x800015c8] : sw a3, 100(s1) -- Store: [0x80012938]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015dc]:fmul.h t6, t5, t4, dyn
	-[0x800015e0]:csrrs a3, fcsr, zero
	-[0x800015e4]:sw t6, 104(s1)
Current Store : [0x800015e8] : sw a3, 108(s1) -- Store: [0x80012940]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015fc]:fmul.h t6, t5, t4, dyn
	-[0x80001600]:csrrs a3, fcsr, zero
	-[0x80001604]:sw t6, 112(s1)
Current Store : [0x80001608] : sw a3, 116(s1) -- Store: [0x80012948]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000161c]:fmul.h t6, t5, t4, dyn
	-[0x80001620]:csrrs a3, fcsr, zero
	-[0x80001624]:sw t6, 120(s1)
Current Store : [0x80001628] : sw a3, 124(s1) -- Store: [0x80012950]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000163c]:fmul.h t6, t5, t4, dyn
	-[0x80001640]:csrrs a3, fcsr, zero
	-[0x80001644]:sw t6, 128(s1)
Current Store : [0x80001648] : sw a3, 132(s1) -- Store: [0x80012958]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000165c]:fmul.h t6, t5, t4, dyn
	-[0x80001660]:csrrs a3, fcsr, zero
	-[0x80001664]:sw t6, 136(s1)
Current Store : [0x80001668] : sw a3, 140(s1) -- Store: [0x80012960]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000167c]:fmul.h t6, t5, t4, dyn
	-[0x80001680]:csrrs a3, fcsr, zero
	-[0x80001684]:sw t6, 144(s1)
Current Store : [0x80001688] : sw a3, 148(s1) -- Store: [0x80012968]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000169c]:fmul.h t6, t5, t4, dyn
	-[0x800016a0]:csrrs a3, fcsr, zero
	-[0x800016a4]:sw t6, 152(s1)
Current Store : [0x800016a8] : sw a3, 156(s1) -- Store: [0x80012970]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016bc]:fmul.h t6, t5, t4, dyn
	-[0x800016c0]:csrrs a3, fcsr, zero
	-[0x800016c4]:sw t6, 160(s1)
Current Store : [0x800016c8] : sw a3, 164(s1) -- Store: [0x80012978]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016dc]:fmul.h t6, t5, t4, dyn
	-[0x800016e0]:csrrs a3, fcsr, zero
	-[0x800016e4]:sw t6, 168(s1)
Current Store : [0x800016e8] : sw a3, 172(s1) -- Store: [0x80012980]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016fc]:fmul.h t6, t5, t4, dyn
	-[0x80001700]:csrrs a3, fcsr, zero
	-[0x80001704]:sw t6, 176(s1)
Current Store : [0x80001708] : sw a3, 180(s1) -- Store: [0x80012988]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000171c]:fmul.h t6, t5, t4, dyn
	-[0x80001720]:csrrs a3, fcsr, zero
	-[0x80001724]:sw t6, 184(s1)
Current Store : [0x80001728] : sw a3, 188(s1) -- Store: [0x80012990]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000173c]:fmul.h t6, t5, t4, dyn
	-[0x80001740]:csrrs a3, fcsr, zero
	-[0x80001744]:sw t6, 192(s1)
Current Store : [0x80001748] : sw a3, 196(s1) -- Store: [0x80012998]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000175c]:fmul.h t6, t5, t4, dyn
	-[0x80001760]:csrrs a3, fcsr, zero
	-[0x80001764]:sw t6, 200(s1)
Current Store : [0x80001768] : sw a3, 204(s1) -- Store: [0x800129a0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000177c]:fmul.h t6, t5, t4, dyn
	-[0x80001780]:csrrs a3, fcsr, zero
	-[0x80001784]:sw t6, 208(s1)
Current Store : [0x80001788] : sw a3, 212(s1) -- Store: [0x800129a8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000179c]:fmul.h t6, t5, t4, dyn
	-[0x800017a0]:csrrs a3, fcsr, zero
	-[0x800017a4]:sw t6, 216(s1)
Current Store : [0x800017a8] : sw a3, 220(s1) -- Store: [0x800129b0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017bc]:fmul.h t6, t5, t4, dyn
	-[0x800017c0]:csrrs a3, fcsr, zero
	-[0x800017c4]:sw t6, 224(s1)
Current Store : [0x800017c8] : sw a3, 228(s1) -- Store: [0x800129b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017dc]:fmul.h t6, t5, t4, dyn
	-[0x800017e0]:csrrs a3, fcsr, zero
	-[0x800017e4]:sw t6, 232(s1)
Current Store : [0x800017e8] : sw a3, 236(s1) -- Store: [0x800129c0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017fc]:fmul.h t6, t5, t4, dyn
	-[0x80001800]:csrrs a3, fcsr, zero
	-[0x80001804]:sw t6, 240(s1)
Current Store : [0x80001808] : sw a3, 244(s1) -- Store: [0x800129c8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000181c]:fmul.h t6, t5, t4, dyn
	-[0x80001820]:csrrs a3, fcsr, zero
	-[0x80001824]:sw t6, 248(s1)
Current Store : [0x80001828] : sw a3, 252(s1) -- Store: [0x800129d0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000183c]:fmul.h t6, t5, t4, dyn
	-[0x80001840]:csrrs a3, fcsr, zero
	-[0x80001844]:sw t6, 256(s1)
Current Store : [0x80001848] : sw a3, 260(s1) -- Store: [0x800129d8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000185c]:fmul.h t6, t5, t4, dyn
	-[0x80001860]:csrrs a3, fcsr, zero
	-[0x80001864]:sw t6, 264(s1)
Current Store : [0x80001868] : sw a3, 268(s1) -- Store: [0x800129e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000187c]:fmul.h t6, t5, t4, dyn
	-[0x80001880]:csrrs a3, fcsr, zero
	-[0x80001884]:sw t6, 272(s1)
Current Store : [0x80001888] : sw a3, 276(s1) -- Store: [0x800129e8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000189c]:fmul.h t6, t5, t4, dyn
	-[0x800018a0]:csrrs a3, fcsr, zero
	-[0x800018a4]:sw t6, 280(s1)
Current Store : [0x800018a8] : sw a3, 284(s1) -- Store: [0x800129f0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018bc]:fmul.h t6, t5, t4, dyn
	-[0x800018c0]:csrrs a3, fcsr, zero
	-[0x800018c4]:sw t6, 288(s1)
Current Store : [0x800018c8] : sw a3, 292(s1) -- Store: [0x800129f8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018dc]:fmul.h t6, t5, t4, dyn
	-[0x800018e0]:csrrs a3, fcsr, zero
	-[0x800018e4]:sw t6, 296(s1)
Current Store : [0x800018e8] : sw a3, 300(s1) -- Store: [0x80012a00]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018fc]:fmul.h t6, t5, t4, dyn
	-[0x80001900]:csrrs a3, fcsr, zero
	-[0x80001904]:sw t6, 304(s1)
Current Store : [0x80001908] : sw a3, 308(s1) -- Store: [0x80012a08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000191c]:fmul.h t6, t5, t4, dyn
	-[0x80001920]:csrrs a3, fcsr, zero
	-[0x80001924]:sw t6, 312(s1)
Current Store : [0x80001928] : sw a3, 316(s1) -- Store: [0x80012a10]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000193c]:fmul.h t6, t5, t4, dyn
	-[0x80001940]:csrrs a3, fcsr, zero
	-[0x80001944]:sw t6, 320(s1)
Current Store : [0x80001948] : sw a3, 324(s1) -- Store: [0x80012a18]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000195c]:fmul.h t6, t5, t4, dyn
	-[0x80001960]:csrrs a3, fcsr, zero
	-[0x80001964]:sw t6, 328(s1)
Current Store : [0x80001968] : sw a3, 332(s1) -- Store: [0x80012a20]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000197c]:fmul.h t6, t5, t4, dyn
	-[0x80001980]:csrrs a3, fcsr, zero
	-[0x80001984]:sw t6, 336(s1)
Current Store : [0x80001988] : sw a3, 340(s1) -- Store: [0x80012a28]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000199c]:fmul.h t6, t5, t4, dyn
	-[0x800019a0]:csrrs a3, fcsr, zero
	-[0x800019a4]:sw t6, 344(s1)
Current Store : [0x800019a8] : sw a3, 348(s1) -- Store: [0x80012a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019bc]:fmul.h t6, t5, t4, dyn
	-[0x800019c0]:csrrs a3, fcsr, zero
	-[0x800019c4]:sw t6, 352(s1)
Current Store : [0x800019c8] : sw a3, 356(s1) -- Store: [0x80012a38]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019dc]:fmul.h t6, t5, t4, dyn
	-[0x800019e0]:csrrs a3, fcsr, zero
	-[0x800019e4]:sw t6, 360(s1)
Current Store : [0x800019e8] : sw a3, 364(s1) -- Store: [0x80012a40]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019fc]:fmul.h t6, t5, t4, dyn
	-[0x80001a00]:csrrs a3, fcsr, zero
	-[0x80001a04]:sw t6, 368(s1)
Current Store : [0x80001a08] : sw a3, 372(s1) -- Store: [0x80012a48]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a1c]:fmul.h t6, t5, t4, dyn
	-[0x80001a20]:csrrs a3, fcsr, zero
	-[0x80001a24]:sw t6, 376(s1)
Current Store : [0x80001a28] : sw a3, 380(s1) -- Store: [0x80012a50]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a3c]:fmul.h t6, t5, t4, dyn
	-[0x80001a40]:csrrs a3, fcsr, zero
	-[0x80001a44]:sw t6, 384(s1)
Current Store : [0x80001a48] : sw a3, 388(s1) -- Store: [0x80012a58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a5c]:fmul.h t6, t5, t4, dyn
	-[0x80001a60]:csrrs a3, fcsr, zero
	-[0x80001a64]:sw t6, 392(s1)
Current Store : [0x80001a68] : sw a3, 396(s1) -- Store: [0x80012a60]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a7c]:fmul.h t6, t5, t4, dyn
	-[0x80001a80]:csrrs a3, fcsr, zero
	-[0x80001a84]:sw t6, 400(s1)
Current Store : [0x80001a88] : sw a3, 404(s1) -- Store: [0x80012a68]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a9c]:fmul.h t6, t5, t4, dyn
	-[0x80001aa0]:csrrs a3, fcsr, zero
	-[0x80001aa4]:sw t6, 408(s1)
Current Store : [0x80001aa8] : sw a3, 412(s1) -- Store: [0x80012a70]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001abc]:fmul.h t6, t5, t4, dyn
	-[0x80001ac0]:csrrs a3, fcsr, zero
	-[0x80001ac4]:sw t6, 416(s1)
Current Store : [0x80001ac8] : sw a3, 420(s1) -- Store: [0x80012a78]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001adc]:fmul.h t6, t5, t4, dyn
	-[0x80001ae0]:csrrs a3, fcsr, zero
	-[0x80001ae4]:sw t6, 424(s1)
Current Store : [0x80001ae8] : sw a3, 428(s1) -- Store: [0x80012a80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001afc]:fmul.h t6, t5, t4, dyn
	-[0x80001b00]:csrrs a3, fcsr, zero
	-[0x80001b04]:sw t6, 432(s1)
Current Store : [0x80001b08] : sw a3, 436(s1) -- Store: [0x80012a88]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:fmul.h t6, t5, t4, dyn
	-[0x80001b20]:csrrs a3, fcsr, zero
	-[0x80001b24]:sw t6, 440(s1)
Current Store : [0x80001b28] : sw a3, 444(s1) -- Store: [0x80012a90]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b3c]:fmul.h t6, t5, t4, dyn
	-[0x80001b40]:csrrs a3, fcsr, zero
	-[0x80001b44]:sw t6, 448(s1)
Current Store : [0x80001b48] : sw a3, 452(s1) -- Store: [0x80012a98]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b5c]:fmul.h t6, t5, t4, dyn
	-[0x80001b60]:csrrs a3, fcsr, zero
	-[0x80001b64]:sw t6, 456(s1)
Current Store : [0x80001b68] : sw a3, 460(s1) -- Store: [0x80012aa0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b7c]:fmul.h t6, t5, t4, dyn
	-[0x80001b80]:csrrs a3, fcsr, zero
	-[0x80001b84]:sw t6, 464(s1)
Current Store : [0x80001b88] : sw a3, 468(s1) -- Store: [0x80012aa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b9c]:fmul.h t6, t5, t4, dyn
	-[0x80001ba0]:csrrs a3, fcsr, zero
	-[0x80001ba4]:sw t6, 472(s1)
Current Store : [0x80001ba8] : sw a3, 476(s1) -- Store: [0x80012ab0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bbc]:fmul.h t6, t5, t4, dyn
	-[0x80001bc0]:csrrs a3, fcsr, zero
	-[0x80001bc4]:sw t6, 480(s1)
Current Store : [0x80001bc8] : sw a3, 484(s1) -- Store: [0x80012ab8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bdc]:fmul.h t6, t5, t4, dyn
	-[0x80001be0]:csrrs a3, fcsr, zero
	-[0x80001be4]:sw t6, 488(s1)
Current Store : [0x80001be8] : sw a3, 492(s1) -- Store: [0x80012ac0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bfc]:fmul.h t6, t5, t4, dyn
	-[0x80001c00]:csrrs a3, fcsr, zero
	-[0x80001c04]:sw t6, 496(s1)
Current Store : [0x80001c08] : sw a3, 500(s1) -- Store: [0x80012ac8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c1c]:fmul.h t6, t5, t4, dyn
	-[0x80001c20]:csrrs a3, fcsr, zero
	-[0x80001c24]:sw t6, 504(s1)
Current Store : [0x80001c28] : sw a3, 508(s1) -- Store: [0x80012ad0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c3c]:fmul.h t6, t5, t4, dyn
	-[0x80001c40]:csrrs a3, fcsr, zero
	-[0x80001c44]:sw t6, 512(s1)
Current Store : [0x80001c48] : sw a3, 516(s1) -- Store: [0x80012ad8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c5c]:fmul.h t6, t5, t4, dyn
	-[0x80001c60]:csrrs a3, fcsr, zero
	-[0x80001c64]:sw t6, 520(s1)
Current Store : [0x80001c68] : sw a3, 524(s1) -- Store: [0x80012ae0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c7c]:fmul.h t6, t5, t4, dyn
	-[0x80001c80]:csrrs a3, fcsr, zero
	-[0x80001c84]:sw t6, 528(s1)
Current Store : [0x80001c88] : sw a3, 532(s1) -- Store: [0x80012ae8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c9c]:fmul.h t6, t5, t4, dyn
	-[0x80001ca0]:csrrs a3, fcsr, zero
	-[0x80001ca4]:sw t6, 536(s1)
Current Store : [0x80001ca8] : sw a3, 540(s1) -- Store: [0x80012af0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cbc]:fmul.h t6, t5, t4, dyn
	-[0x80001cc0]:csrrs a3, fcsr, zero
	-[0x80001cc4]:sw t6, 544(s1)
Current Store : [0x80001cc8] : sw a3, 548(s1) -- Store: [0x80012af8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cdc]:fmul.h t6, t5, t4, dyn
	-[0x80001ce0]:csrrs a3, fcsr, zero
	-[0x80001ce4]:sw t6, 552(s1)
Current Store : [0x80001ce8] : sw a3, 556(s1) -- Store: [0x80012b00]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cfc]:fmul.h t6, t5, t4, dyn
	-[0x80001d00]:csrrs a3, fcsr, zero
	-[0x80001d04]:sw t6, 560(s1)
Current Store : [0x80001d08] : sw a3, 564(s1) -- Store: [0x80012b08]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d1c]:fmul.h t6, t5, t4, dyn
	-[0x80001d20]:csrrs a3, fcsr, zero
	-[0x80001d24]:sw t6, 568(s1)
Current Store : [0x80001d28] : sw a3, 572(s1) -- Store: [0x80012b10]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d3c]:fmul.h t6, t5, t4, dyn
	-[0x80001d40]:csrrs a3, fcsr, zero
	-[0x80001d44]:sw t6, 576(s1)
Current Store : [0x80001d48] : sw a3, 580(s1) -- Store: [0x80012b18]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d5c]:fmul.h t6, t5, t4, dyn
	-[0x80001d60]:csrrs a3, fcsr, zero
	-[0x80001d64]:sw t6, 584(s1)
Current Store : [0x80001d68] : sw a3, 588(s1) -- Store: [0x80012b20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d7c]:fmul.h t6, t5, t4, dyn
	-[0x80001d80]:csrrs a3, fcsr, zero
	-[0x80001d84]:sw t6, 592(s1)
Current Store : [0x80001d88] : sw a3, 596(s1) -- Store: [0x80012b28]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d9c]:fmul.h t6, t5, t4, dyn
	-[0x80001da0]:csrrs a3, fcsr, zero
	-[0x80001da4]:sw t6, 600(s1)
Current Store : [0x80001da8] : sw a3, 604(s1) -- Store: [0x80012b30]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dbc]:fmul.h t6, t5, t4, dyn
	-[0x80001dc0]:csrrs a3, fcsr, zero
	-[0x80001dc4]:sw t6, 608(s1)
Current Store : [0x80001dc8] : sw a3, 612(s1) -- Store: [0x80012b38]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ddc]:fmul.h t6, t5, t4, dyn
	-[0x80001de0]:csrrs a3, fcsr, zero
	-[0x80001de4]:sw t6, 616(s1)
Current Store : [0x80001de8] : sw a3, 620(s1) -- Store: [0x80012b40]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dfc]:fmul.h t6, t5, t4, dyn
	-[0x80001e00]:csrrs a3, fcsr, zero
	-[0x80001e04]:sw t6, 624(s1)
Current Store : [0x80001e08] : sw a3, 628(s1) -- Store: [0x80012b48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e1c]:fmul.h t6, t5, t4, dyn
	-[0x80001e20]:csrrs a3, fcsr, zero
	-[0x80001e24]:sw t6, 632(s1)
Current Store : [0x80001e28] : sw a3, 636(s1) -- Store: [0x80012b50]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e3c]:fmul.h t6, t5, t4, dyn
	-[0x80001e40]:csrrs a3, fcsr, zero
	-[0x80001e44]:sw t6, 640(s1)
Current Store : [0x80001e48] : sw a3, 644(s1) -- Store: [0x80012b58]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e5c]:fmul.h t6, t5, t4, dyn
	-[0x80001e60]:csrrs a3, fcsr, zero
	-[0x80001e64]:sw t6, 648(s1)
Current Store : [0x80001e68] : sw a3, 652(s1) -- Store: [0x80012b60]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e7c]:fmul.h t6, t5, t4, dyn
	-[0x80001e80]:csrrs a3, fcsr, zero
	-[0x80001e84]:sw t6, 656(s1)
Current Store : [0x80001e88] : sw a3, 660(s1) -- Store: [0x80012b68]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e9c]:fmul.h t6, t5, t4, dyn
	-[0x80001ea0]:csrrs a3, fcsr, zero
	-[0x80001ea4]:sw t6, 664(s1)
Current Store : [0x80001ea8] : sw a3, 668(s1) -- Store: [0x80012b70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ebc]:fmul.h t6, t5, t4, dyn
	-[0x80001ec0]:csrrs a3, fcsr, zero
	-[0x80001ec4]:sw t6, 672(s1)
Current Store : [0x80001ec8] : sw a3, 676(s1) -- Store: [0x80012b78]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001edc]:fmul.h t6, t5, t4, dyn
	-[0x80001ee0]:csrrs a3, fcsr, zero
	-[0x80001ee4]:sw t6, 680(s1)
Current Store : [0x80001ee8] : sw a3, 684(s1) -- Store: [0x80012b80]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001efc]:fmul.h t6, t5, t4, dyn
	-[0x80001f00]:csrrs a3, fcsr, zero
	-[0x80001f04]:sw t6, 688(s1)
Current Store : [0x80001f08] : sw a3, 692(s1) -- Store: [0x80012b88]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f1c]:fmul.h t6, t5, t4, dyn
	-[0x80001f20]:csrrs a3, fcsr, zero
	-[0x80001f24]:sw t6, 696(s1)
Current Store : [0x80001f28] : sw a3, 700(s1) -- Store: [0x80012b90]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f3c]:fmul.h t6, t5, t4, dyn
	-[0x80001f40]:csrrs a3, fcsr, zero
	-[0x80001f44]:sw t6, 704(s1)
Current Store : [0x80001f48] : sw a3, 708(s1) -- Store: [0x80012b98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f5c]:fmul.h t6, t5, t4, dyn
	-[0x80001f60]:csrrs a3, fcsr, zero
	-[0x80001f64]:sw t6, 712(s1)
Current Store : [0x80001f68] : sw a3, 716(s1) -- Store: [0x80012ba0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f7c]:fmul.h t6, t5, t4, dyn
	-[0x80001f80]:csrrs a3, fcsr, zero
	-[0x80001f84]:sw t6, 720(s1)
Current Store : [0x80001f88] : sw a3, 724(s1) -- Store: [0x80012ba8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f9c]:fmul.h t6, t5, t4, dyn
	-[0x80001fa0]:csrrs a3, fcsr, zero
	-[0x80001fa4]:sw t6, 728(s1)
Current Store : [0x80001fa8] : sw a3, 732(s1) -- Store: [0x80012bb0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fbc]:fmul.h t6, t5, t4, dyn
	-[0x80001fc0]:csrrs a3, fcsr, zero
	-[0x80001fc4]:sw t6, 736(s1)
Current Store : [0x80001fc8] : sw a3, 740(s1) -- Store: [0x80012bb8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fdc]:fmul.h t6, t5, t4, dyn
	-[0x80001fe0]:csrrs a3, fcsr, zero
	-[0x80001fe4]:sw t6, 744(s1)
Current Store : [0x80001fe8] : sw a3, 748(s1) -- Store: [0x80012bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ffc]:fmul.h t6, t5, t4, dyn
	-[0x80002000]:csrrs a3, fcsr, zero
	-[0x80002004]:sw t6, 752(s1)
Current Store : [0x80002008] : sw a3, 756(s1) -- Store: [0x80012bc8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000201c]:fmul.h t6, t5, t4, dyn
	-[0x80002020]:csrrs a3, fcsr, zero
	-[0x80002024]:sw t6, 760(s1)
Current Store : [0x80002028] : sw a3, 764(s1) -- Store: [0x80012bd0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000203c]:fmul.h t6, t5, t4, dyn
	-[0x80002040]:csrrs a3, fcsr, zero
	-[0x80002044]:sw t6, 768(s1)
Current Store : [0x80002048] : sw a3, 772(s1) -- Store: [0x80012bd8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000205c]:fmul.h t6, t5, t4, dyn
	-[0x80002060]:csrrs a3, fcsr, zero
	-[0x80002064]:sw t6, 776(s1)
Current Store : [0x80002068] : sw a3, 780(s1) -- Store: [0x80012be0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000207c]:fmul.h t6, t5, t4, dyn
	-[0x80002080]:csrrs a3, fcsr, zero
	-[0x80002084]:sw t6, 784(s1)
Current Store : [0x80002088] : sw a3, 788(s1) -- Store: [0x80012be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000209c]:fmul.h t6, t5, t4, dyn
	-[0x800020a0]:csrrs a3, fcsr, zero
	-[0x800020a4]:sw t6, 792(s1)
Current Store : [0x800020a8] : sw a3, 796(s1) -- Store: [0x80012bf0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020bc]:fmul.h t6, t5, t4, dyn
	-[0x800020c0]:csrrs a3, fcsr, zero
	-[0x800020c4]:sw t6, 800(s1)
Current Store : [0x800020c8] : sw a3, 804(s1) -- Store: [0x80012bf8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020dc]:fmul.h t6, t5, t4, dyn
	-[0x800020e0]:csrrs a3, fcsr, zero
	-[0x800020e4]:sw t6, 808(s1)
Current Store : [0x800020e8] : sw a3, 812(s1) -- Store: [0x80012c00]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020fc]:fmul.h t6, t5, t4, dyn
	-[0x80002100]:csrrs a3, fcsr, zero
	-[0x80002104]:sw t6, 816(s1)
Current Store : [0x80002108] : sw a3, 820(s1) -- Store: [0x80012c08]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000211c]:fmul.h t6, t5, t4, dyn
	-[0x80002120]:csrrs a3, fcsr, zero
	-[0x80002124]:sw t6, 824(s1)
Current Store : [0x80002128] : sw a3, 828(s1) -- Store: [0x80012c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000213c]:fmul.h t6, t5, t4, dyn
	-[0x80002140]:csrrs a3, fcsr, zero
	-[0x80002144]:sw t6, 832(s1)
Current Store : [0x80002148] : sw a3, 836(s1) -- Store: [0x80012c18]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000215c]:fmul.h t6, t5, t4, dyn
	-[0x80002160]:csrrs a3, fcsr, zero
	-[0x80002164]:sw t6, 840(s1)
Current Store : [0x80002168] : sw a3, 844(s1) -- Store: [0x80012c20]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000217c]:fmul.h t6, t5, t4, dyn
	-[0x80002180]:csrrs a3, fcsr, zero
	-[0x80002184]:sw t6, 848(s1)
Current Store : [0x80002188] : sw a3, 852(s1) -- Store: [0x80012c28]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000219c]:fmul.h t6, t5, t4, dyn
	-[0x800021a0]:csrrs a3, fcsr, zero
	-[0x800021a4]:sw t6, 856(s1)
Current Store : [0x800021a8] : sw a3, 860(s1) -- Store: [0x80012c30]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021bc]:fmul.h t6, t5, t4, dyn
	-[0x800021c0]:csrrs a3, fcsr, zero
	-[0x800021c4]:sw t6, 864(s1)
Current Store : [0x800021c8] : sw a3, 868(s1) -- Store: [0x80012c38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021dc]:fmul.h t6, t5, t4, dyn
	-[0x800021e0]:csrrs a3, fcsr, zero
	-[0x800021e4]:sw t6, 872(s1)
Current Store : [0x800021e8] : sw a3, 876(s1) -- Store: [0x80012c40]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021fc]:fmul.h t6, t5, t4, dyn
	-[0x80002200]:csrrs a3, fcsr, zero
	-[0x80002204]:sw t6, 880(s1)
Current Store : [0x80002208] : sw a3, 884(s1) -- Store: [0x80012c48]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000221c]:fmul.h t6, t5, t4, dyn
	-[0x80002220]:csrrs a3, fcsr, zero
	-[0x80002224]:sw t6, 888(s1)
Current Store : [0x80002228] : sw a3, 892(s1) -- Store: [0x80012c50]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000223c]:fmul.h t6, t5, t4, dyn
	-[0x80002240]:csrrs a3, fcsr, zero
	-[0x80002244]:sw t6, 896(s1)
Current Store : [0x80002248] : sw a3, 900(s1) -- Store: [0x80012c58]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000225c]:fmul.h t6, t5, t4, dyn
	-[0x80002260]:csrrs a3, fcsr, zero
	-[0x80002264]:sw t6, 904(s1)
Current Store : [0x80002268] : sw a3, 908(s1) -- Store: [0x80012c60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000227c]:fmul.h t6, t5, t4, dyn
	-[0x80002280]:csrrs a3, fcsr, zero
	-[0x80002284]:sw t6, 912(s1)
Current Store : [0x80002288] : sw a3, 916(s1) -- Store: [0x80012c68]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000229c]:fmul.h t6, t5, t4, dyn
	-[0x800022a0]:csrrs a3, fcsr, zero
	-[0x800022a4]:sw t6, 920(s1)
Current Store : [0x800022a8] : sw a3, 924(s1) -- Store: [0x80012c70]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022bc]:fmul.h t6, t5, t4, dyn
	-[0x800022c0]:csrrs a3, fcsr, zero
	-[0x800022c4]:sw t6, 928(s1)
Current Store : [0x800022c8] : sw a3, 932(s1) -- Store: [0x80012c78]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022dc]:fmul.h t6, t5, t4, dyn
	-[0x800022e0]:csrrs a3, fcsr, zero
	-[0x800022e4]:sw t6, 936(s1)
Current Store : [0x800022e8] : sw a3, 940(s1) -- Store: [0x80012c80]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022fc]:fmul.h t6, t5, t4, dyn
	-[0x80002300]:csrrs a3, fcsr, zero
	-[0x80002304]:sw t6, 944(s1)
Current Store : [0x80002308] : sw a3, 948(s1) -- Store: [0x80012c88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000231c]:fmul.h t6, t5, t4, dyn
	-[0x80002320]:csrrs a3, fcsr, zero
	-[0x80002324]:sw t6, 952(s1)
Current Store : [0x80002328] : sw a3, 956(s1) -- Store: [0x80012c90]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000233c]:fmul.h t6, t5, t4, dyn
	-[0x80002340]:csrrs a3, fcsr, zero
	-[0x80002344]:sw t6, 960(s1)
Current Store : [0x80002348] : sw a3, 964(s1) -- Store: [0x80012c98]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000235c]:fmul.h t6, t5, t4, dyn
	-[0x80002360]:csrrs a3, fcsr, zero
	-[0x80002364]:sw t6, 968(s1)
Current Store : [0x80002368] : sw a3, 972(s1) -- Store: [0x80012ca0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000237c]:fmul.h t6, t5, t4, dyn
	-[0x80002380]:csrrs a3, fcsr, zero
	-[0x80002384]:sw t6, 976(s1)
Current Store : [0x80002388] : sw a3, 980(s1) -- Store: [0x80012ca8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000239c]:fmul.h t6, t5, t4, dyn
	-[0x800023a0]:csrrs a3, fcsr, zero
	-[0x800023a4]:sw t6, 984(s1)
Current Store : [0x800023a8] : sw a3, 988(s1) -- Store: [0x80012cb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023bc]:fmul.h t6, t5, t4, dyn
	-[0x800023c0]:csrrs a3, fcsr, zero
	-[0x800023c4]:sw t6, 992(s1)
Current Store : [0x800023c8] : sw a3, 996(s1) -- Store: [0x80012cb8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023fc]:fmul.h t6, t5, t4, dyn
	-[0x80002400]:csrrs a3, fcsr, zero
	-[0x80002404]:sw t6, 1000(s1)
Current Store : [0x80002408] : sw a3, 1004(s1) -- Store: [0x80012cc0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000243c]:fmul.h t6, t5, t4, dyn
	-[0x80002440]:csrrs a3, fcsr, zero
	-[0x80002444]:sw t6, 1008(s1)
Current Store : [0x80002448] : sw a3, 1012(s1) -- Store: [0x80012cc8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000247c]:fmul.h t6, t5, t4, dyn
	-[0x80002480]:csrrs a3, fcsr, zero
	-[0x80002484]:sw t6, 1016(s1)
Current Store : [0x80002488] : sw a3, 1020(s1) -- Store: [0x80012cd0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800024c4]:fmul.h t6, t5, t4, dyn
	-[0x800024c8]:csrrs a3, fcsr, zero
	-[0x800024cc]:sw t6, 0(s1)
Current Store : [0x800024d0] : sw a3, 4(s1) -- Store: [0x80012cd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002504]:fmul.h t6, t5, t4, dyn
	-[0x80002508]:csrrs a3, fcsr, zero
	-[0x8000250c]:sw t6, 8(s1)
Current Store : [0x80002510] : sw a3, 12(s1) -- Store: [0x80012ce0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002544]:fmul.h t6, t5, t4, dyn
	-[0x80002548]:csrrs a3, fcsr, zero
	-[0x8000254c]:sw t6, 16(s1)
Current Store : [0x80002550] : sw a3, 20(s1) -- Store: [0x80012ce8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002584]:fmul.h t6, t5, t4, dyn
	-[0x80002588]:csrrs a3, fcsr, zero
	-[0x8000258c]:sw t6, 24(s1)
Current Store : [0x80002590] : sw a3, 28(s1) -- Store: [0x80012cf0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025c4]:fmul.h t6, t5, t4, dyn
	-[0x800025c8]:csrrs a3, fcsr, zero
	-[0x800025cc]:sw t6, 32(s1)
Current Store : [0x800025d0] : sw a3, 36(s1) -- Store: [0x80012cf8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002604]:fmul.h t6, t5, t4, dyn
	-[0x80002608]:csrrs a3, fcsr, zero
	-[0x8000260c]:sw t6, 40(s1)
Current Store : [0x80002610] : sw a3, 44(s1) -- Store: [0x80012d00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002644]:fmul.h t6, t5, t4, dyn
	-[0x80002648]:csrrs a3, fcsr, zero
	-[0x8000264c]:sw t6, 48(s1)
Current Store : [0x80002650] : sw a3, 52(s1) -- Store: [0x80012d08]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002684]:fmul.h t6, t5, t4, dyn
	-[0x80002688]:csrrs a3, fcsr, zero
	-[0x8000268c]:sw t6, 56(s1)
Current Store : [0x80002690] : sw a3, 60(s1) -- Store: [0x80012d10]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026c4]:fmul.h t6, t5, t4, dyn
	-[0x800026c8]:csrrs a3, fcsr, zero
	-[0x800026cc]:sw t6, 64(s1)
Current Store : [0x800026d0] : sw a3, 68(s1) -- Store: [0x80012d18]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002704]:fmul.h t6, t5, t4, dyn
	-[0x80002708]:csrrs a3, fcsr, zero
	-[0x8000270c]:sw t6, 72(s1)
Current Store : [0x80002710] : sw a3, 76(s1) -- Store: [0x80012d20]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002744]:fmul.h t6, t5, t4, dyn
	-[0x80002748]:csrrs a3, fcsr, zero
	-[0x8000274c]:sw t6, 80(s1)
Current Store : [0x80002750] : sw a3, 84(s1) -- Store: [0x80012d28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002784]:fmul.h t6, t5, t4, dyn
	-[0x80002788]:csrrs a3, fcsr, zero
	-[0x8000278c]:sw t6, 88(s1)
Current Store : [0x80002790] : sw a3, 92(s1) -- Store: [0x80012d30]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027c4]:fmul.h t6, t5, t4, dyn
	-[0x800027c8]:csrrs a3, fcsr, zero
	-[0x800027cc]:sw t6, 96(s1)
Current Store : [0x800027d0] : sw a3, 100(s1) -- Store: [0x80012d38]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002804]:fmul.h t6, t5, t4, dyn
	-[0x80002808]:csrrs a3, fcsr, zero
	-[0x8000280c]:sw t6, 104(s1)
Current Store : [0x80002810] : sw a3, 108(s1) -- Store: [0x80012d40]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002844]:fmul.h t6, t5, t4, dyn
	-[0x80002848]:csrrs a3, fcsr, zero
	-[0x8000284c]:sw t6, 112(s1)
Current Store : [0x80002850] : sw a3, 116(s1) -- Store: [0x80012d48]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002884]:fmul.h t6, t5, t4, dyn
	-[0x80002888]:csrrs a3, fcsr, zero
	-[0x8000288c]:sw t6, 120(s1)
Current Store : [0x80002890] : sw a3, 124(s1) -- Store: [0x80012d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028c4]:fmul.h t6, t5, t4, dyn
	-[0x800028c8]:csrrs a3, fcsr, zero
	-[0x800028cc]:sw t6, 128(s1)
Current Store : [0x800028d0] : sw a3, 132(s1) -- Store: [0x80012d58]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002904]:fmul.h t6, t5, t4, dyn
	-[0x80002908]:csrrs a3, fcsr, zero
	-[0x8000290c]:sw t6, 136(s1)
Current Store : [0x80002910] : sw a3, 140(s1) -- Store: [0x80012d60]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002944]:fmul.h t6, t5, t4, dyn
	-[0x80002948]:csrrs a3, fcsr, zero
	-[0x8000294c]:sw t6, 144(s1)
Current Store : [0x80002950] : sw a3, 148(s1) -- Store: [0x80012d68]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002984]:fmul.h t6, t5, t4, dyn
	-[0x80002988]:csrrs a3, fcsr, zero
	-[0x8000298c]:sw t6, 152(s1)
Current Store : [0x80002990] : sw a3, 156(s1) -- Store: [0x80012d70]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029c4]:fmul.h t6, t5, t4, dyn
	-[0x800029c8]:csrrs a3, fcsr, zero
	-[0x800029cc]:sw t6, 160(s1)
Current Store : [0x800029d0] : sw a3, 164(s1) -- Store: [0x80012d78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a04]:fmul.h t6, t5, t4, dyn
	-[0x80002a08]:csrrs a3, fcsr, zero
	-[0x80002a0c]:sw t6, 168(s1)
Current Store : [0x80002a10] : sw a3, 172(s1) -- Store: [0x80012d80]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a44]:fmul.h t6, t5, t4, dyn
	-[0x80002a48]:csrrs a3, fcsr, zero
	-[0x80002a4c]:sw t6, 176(s1)
Current Store : [0x80002a50] : sw a3, 180(s1) -- Store: [0x80012d88]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a84]:fmul.h t6, t5, t4, dyn
	-[0x80002a88]:csrrs a3, fcsr, zero
	-[0x80002a8c]:sw t6, 184(s1)
Current Store : [0x80002a90] : sw a3, 188(s1) -- Store: [0x80012d90]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ac4]:fmul.h t6, t5, t4, dyn
	-[0x80002ac8]:csrrs a3, fcsr, zero
	-[0x80002acc]:sw t6, 192(s1)
Current Store : [0x80002ad0] : sw a3, 196(s1) -- Store: [0x80012d98]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b04]:fmul.h t6, t5, t4, dyn
	-[0x80002b08]:csrrs a3, fcsr, zero
	-[0x80002b0c]:sw t6, 200(s1)
Current Store : [0x80002b10] : sw a3, 204(s1) -- Store: [0x80012da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b44]:fmul.h t6, t5, t4, dyn
	-[0x80002b48]:csrrs a3, fcsr, zero
	-[0x80002b4c]:sw t6, 208(s1)
Current Store : [0x80002b50] : sw a3, 212(s1) -- Store: [0x80012da8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b84]:fmul.h t6, t5, t4, dyn
	-[0x80002b88]:csrrs a3, fcsr, zero
	-[0x80002b8c]:sw t6, 216(s1)
Current Store : [0x80002b90] : sw a3, 220(s1) -- Store: [0x80012db0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002bc4]:fmul.h t6, t5, t4, dyn
	-[0x80002bc8]:csrrs a3, fcsr, zero
	-[0x80002bcc]:sw t6, 224(s1)
Current Store : [0x80002bd0] : sw a3, 228(s1) -- Store: [0x80012db8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c04]:fmul.h t6, t5, t4, dyn
	-[0x80002c08]:csrrs a3, fcsr, zero
	-[0x80002c0c]:sw t6, 232(s1)
Current Store : [0x80002c10] : sw a3, 236(s1) -- Store: [0x80012dc0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c44]:fmul.h t6, t5, t4, dyn
	-[0x80002c48]:csrrs a3, fcsr, zero
	-[0x80002c4c]:sw t6, 240(s1)
Current Store : [0x80002c50] : sw a3, 244(s1) -- Store: [0x80012dc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c84]:fmul.h t6, t5, t4, dyn
	-[0x80002c88]:csrrs a3, fcsr, zero
	-[0x80002c8c]:sw t6, 248(s1)
Current Store : [0x80002c90] : sw a3, 252(s1) -- Store: [0x80012dd0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002cc4]:fmul.h t6, t5, t4, dyn
	-[0x80002cc8]:csrrs a3, fcsr, zero
	-[0x80002ccc]:sw t6, 256(s1)
Current Store : [0x80002cd0] : sw a3, 260(s1) -- Store: [0x80012dd8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d04]:fmul.h t6, t5, t4, dyn
	-[0x80002d08]:csrrs a3, fcsr, zero
	-[0x80002d0c]:sw t6, 264(s1)
Current Store : [0x80002d10] : sw a3, 268(s1) -- Store: [0x80012de0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d44]:fmul.h t6, t5, t4, dyn
	-[0x80002d48]:csrrs a3, fcsr, zero
	-[0x80002d4c]:sw t6, 272(s1)
Current Store : [0x80002d50] : sw a3, 276(s1) -- Store: [0x80012de8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d84]:fmul.h t6, t5, t4, dyn
	-[0x80002d88]:csrrs a3, fcsr, zero
	-[0x80002d8c]:sw t6, 280(s1)
Current Store : [0x80002d90] : sw a3, 284(s1) -- Store: [0x80012df0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002dc4]:fmul.h t6, t5, t4, dyn
	-[0x80002dc8]:csrrs a3, fcsr, zero
	-[0x80002dcc]:sw t6, 288(s1)
Current Store : [0x80002dd0] : sw a3, 292(s1) -- Store: [0x80012df8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e04]:fmul.h t6, t5, t4, dyn
	-[0x80002e08]:csrrs a3, fcsr, zero
	-[0x80002e0c]:sw t6, 296(s1)
Current Store : [0x80002e10] : sw a3, 300(s1) -- Store: [0x80012e00]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e44]:fmul.h t6, t5, t4, dyn
	-[0x80002e48]:csrrs a3, fcsr, zero
	-[0x80002e4c]:sw t6, 304(s1)
Current Store : [0x80002e50] : sw a3, 308(s1) -- Store: [0x80012e08]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e84]:fmul.h t6, t5, t4, dyn
	-[0x80002e88]:csrrs a3, fcsr, zero
	-[0x80002e8c]:sw t6, 312(s1)
Current Store : [0x80002e90] : sw a3, 316(s1) -- Store: [0x80012e10]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ec4]:fmul.h t6, t5, t4, dyn
	-[0x80002ec8]:csrrs a3, fcsr, zero
	-[0x80002ecc]:sw t6, 320(s1)
Current Store : [0x80002ed0] : sw a3, 324(s1) -- Store: [0x80012e18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f04]:fmul.h t6, t5, t4, dyn
	-[0x80002f08]:csrrs a3, fcsr, zero
	-[0x80002f0c]:sw t6, 328(s1)
Current Store : [0x80002f10] : sw a3, 332(s1) -- Store: [0x80012e20]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f44]:fmul.h t6, t5, t4, dyn
	-[0x80002f48]:csrrs a3, fcsr, zero
	-[0x80002f4c]:sw t6, 336(s1)
Current Store : [0x80002f50] : sw a3, 340(s1) -- Store: [0x80012e28]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f84]:fmul.h t6, t5, t4, dyn
	-[0x80002f88]:csrrs a3, fcsr, zero
	-[0x80002f8c]:sw t6, 344(s1)
Current Store : [0x80002f90] : sw a3, 348(s1) -- Store: [0x80012e30]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fc4]:fmul.h t6, t5, t4, dyn
	-[0x80002fc8]:csrrs a3, fcsr, zero
	-[0x80002fcc]:sw t6, 352(s1)
Current Store : [0x80002fd0] : sw a3, 356(s1) -- Store: [0x80012e38]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003004]:fmul.h t6, t5, t4, dyn
	-[0x80003008]:csrrs a3, fcsr, zero
	-[0x8000300c]:sw t6, 360(s1)
Current Store : [0x80003010] : sw a3, 364(s1) -- Store: [0x80012e40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003044]:fmul.h t6, t5, t4, dyn
	-[0x80003048]:csrrs a3, fcsr, zero
	-[0x8000304c]:sw t6, 368(s1)
Current Store : [0x80003050] : sw a3, 372(s1) -- Store: [0x80012e48]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003084]:fmul.h t6, t5, t4, dyn
	-[0x80003088]:csrrs a3, fcsr, zero
	-[0x8000308c]:sw t6, 376(s1)
Current Store : [0x80003090] : sw a3, 380(s1) -- Store: [0x80012e50]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030c4]:fmul.h t6, t5, t4, dyn
	-[0x800030c8]:csrrs a3, fcsr, zero
	-[0x800030cc]:sw t6, 384(s1)
Current Store : [0x800030d0] : sw a3, 388(s1) -- Store: [0x80012e58]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003104]:fmul.h t6, t5, t4, dyn
	-[0x80003108]:csrrs a3, fcsr, zero
	-[0x8000310c]:sw t6, 392(s1)
Current Store : [0x80003110] : sw a3, 396(s1) -- Store: [0x80012e60]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003144]:fmul.h t6, t5, t4, dyn
	-[0x80003148]:csrrs a3, fcsr, zero
	-[0x8000314c]:sw t6, 400(s1)
Current Store : [0x80003150] : sw a3, 404(s1) -- Store: [0x80012e68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003184]:fmul.h t6, t5, t4, dyn
	-[0x80003188]:csrrs a3, fcsr, zero
	-[0x8000318c]:sw t6, 408(s1)
Current Store : [0x80003190] : sw a3, 412(s1) -- Store: [0x80012e70]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031c4]:fmul.h t6, t5, t4, dyn
	-[0x800031c8]:csrrs a3, fcsr, zero
	-[0x800031cc]:sw t6, 416(s1)
Current Store : [0x800031d0] : sw a3, 420(s1) -- Store: [0x80012e78]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003204]:fmul.h t6, t5, t4, dyn
	-[0x80003208]:csrrs a3, fcsr, zero
	-[0x8000320c]:sw t6, 424(s1)
Current Store : [0x80003210] : sw a3, 428(s1) -- Store: [0x80012e80]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003244]:fmul.h t6, t5, t4, dyn
	-[0x80003248]:csrrs a3, fcsr, zero
	-[0x8000324c]:sw t6, 432(s1)
Current Store : [0x80003250] : sw a3, 436(s1) -- Store: [0x80012e88]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003284]:fmul.h t6, t5, t4, dyn
	-[0x80003288]:csrrs a3, fcsr, zero
	-[0x8000328c]:sw t6, 440(s1)
Current Store : [0x80003290] : sw a3, 444(s1) -- Store: [0x80012e90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032c4]:fmul.h t6, t5, t4, dyn
	-[0x800032c8]:csrrs a3, fcsr, zero
	-[0x800032cc]:sw t6, 448(s1)
Current Store : [0x800032d0] : sw a3, 452(s1) -- Store: [0x80012e98]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003304]:fmul.h t6, t5, t4, dyn
	-[0x80003308]:csrrs a3, fcsr, zero
	-[0x8000330c]:sw t6, 456(s1)
Current Store : [0x80003310] : sw a3, 460(s1) -- Store: [0x80012ea0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003344]:fmul.h t6, t5, t4, dyn
	-[0x80003348]:csrrs a3, fcsr, zero
	-[0x8000334c]:sw t6, 464(s1)
Current Store : [0x80003350] : sw a3, 468(s1) -- Store: [0x80012ea8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003384]:fmul.h t6, t5, t4, dyn
	-[0x80003388]:csrrs a3, fcsr, zero
	-[0x8000338c]:sw t6, 472(s1)
Current Store : [0x80003390] : sw a3, 476(s1) -- Store: [0x80012eb0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033c4]:fmul.h t6, t5, t4, dyn
	-[0x800033c8]:csrrs a3, fcsr, zero
	-[0x800033cc]:sw t6, 480(s1)
Current Store : [0x800033d0] : sw a3, 484(s1) -- Store: [0x80012eb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003404]:fmul.h t6, t5, t4, dyn
	-[0x80003408]:csrrs a3, fcsr, zero
	-[0x8000340c]:sw t6, 488(s1)
Current Store : [0x80003410] : sw a3, 492(s1) -- Store: [0x80012ec0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003444]:fmul.h t6, t5, t4, dyn
	-[0x80003448]:csrrs a3, fcsr, zero
	-[0x8000344c]:sw t6, 496(s1)
Current Store : [0x80003450] : sw a3, 500(s1) -- Store: [0x80012ec8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003484]:fmul.h t6, t5, t4, dyn
	-[0x80003488]:csrrs a3, fcsr, zero
	-[0x8000348c]:sw t6, 504(s1)
Current Store : [0x80003490] : sw a3, 508(s1) -- Store: [0x80012ed0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034c4]:fmul.h t6, t5, t4, dyn
	-[0x800034c8]:csrrs a3, fcsr, zero
	-[0x800034cc]:sw t6, 512(s1)
Current Store : [0x800034d0] : sw a3, 516(s1) -- Store: [0x80012ed8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003504]:fmul.h t6, t5, t4, dyn
	-[0x80003508]:csrrs a3, fcsr, zero
	-[0x8000350c]:sw t6, 520(s1)
Current Store : [0x80003510] : sw a3, 524(s1) -- Store: [0x80012ee0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003544]:fmul.h t6, t5, t4, dyn
	-[0x80003548]:csrrs a3, fcsr, zero
	-[0x8000354c]:sw t6, 528(s1)
Current Store : [0x80003550] : sw a3, 532(s1) -- Store: [0x80012ee8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003584]:fmul.h t6, t5, t4, dyn
	-[0x80003588]:csrrs a3, fcsr, zero
	-[0x8000358c]:sw t6, 536(s1)
Current Store : [0x80003590] : sw a3, 540(s1) -- Store: [0x80012ef0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035c4]:fmul.h t6, t5, t4, dyn
	-[0x800035c8]:csrrs a3, fcsr, zero
	-[0x800035cc]:sw t6, 544(s1)
Current Store : [0x800035d0] : sw a3, 548(s1) -- Store: [0x80012ef8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003604]:fmul.h t6, t5, t4, dyn
	-[0x80003608]:csrrs a3, fcsr, zero
	-[0x8000360c]:sw t6, 552(s1)
Current Store : [0x80003610] : sw a3, 556(s1) -- Store: [0x80012f00]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003644]:fmul.h t6, t5, t4, dyn
	-[0x80003648]:csrrs a3, fcsr, zero
	-[0x8000364c]:sw t6, 560(s1)
Current Store : [0x80003650] : sw a3, 564(s1) -- Store: [0x80012f08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003684]:fmul.h t6, t5, t4, dyn
	-[0x80003688]:csrrs a3, fcsr, zero
	-[0x8000368c]:sw t6, 568(s1)
Current Store : [0x80003690] : sw a3, 572(s1) -- Store: [0x80012f10]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036c4]:fmul.h t6, t5, t4, dyn
	-[0x800036c8]:csrrs a3, fcsr, zero
	-[0x800036cc]:sw t6, 576(s1)
Current Store : [0x800036d0] : sw a3, 580(s1) -- Store: [0x80012f18]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003704]:fmul.h t6, t5, t4, dyn
	-[0x80003708]:csrrs a3, fcsr, zero
	-[0x8000370c]:sw t6, 584(s1)
Current Store : [0x80003710] : sw a3, 588(s1) -- Store: [0x80012f20]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003744]:fmul.h t6, t5, t4, dyn
	-[0x80003748]:csrrs a3, fcsr, zero
	-[0x8000374c]:sw t6, 592(s1)
Current Store : [0x80003750] : sw a3, 596(s1) -- Store: [0x80012f28]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003784]:fmul.h t6, t5, t4, dyn
	-[0x80003788]:csrrs a3, fcsr, zero
	-[0x8000378c]:sw t6, 600(s1)
Current Store : [0x80003790] : sw a3, 604(s1) -- Store: [0x80012f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037c4]:fmul.h t6, t5, t4, dyn
	-[0x800037c8]:csrrs a3, fcsr, zero
	-[0x800037cc]:sw t6, 608(s1)
Current Store : [0x800037d0] : sw a3, 612(s1) -- Store: [0x80012f38]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003804]:fmul.h t6, t5, t4, dyn
	-[0x80003808]:csrrs a3, fcsr, zero
	-[0x8000380c]:sw t6, 616(s1)
Current Store : [0x80003810] : sw a3, 620(s1) -- Store: [0x80012f40]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003844]:fmul.h t6, t5, t4, dyn
	-[0x80003848]:csrrs a3, fcsr, zero
	-[0x8000384c]:sw t6, 624(s1)
Current Store : [0x80003850] : sw a3, 628(s1) -- Store: [0x80012f48]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003884]:fmul.h t6, t5, t4, dyn
	-[0x80003888]:csrrs a3, fcsr, zero
	-[0x8000388c]:sw t6, 632(s1)
Current Store : [0x80003890] : sw a3, 636(s1) -- Store: [0x80012f50]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038c4]:fmul.h t6, t5, t4, dyn
	-[0x800038c8]:csrrs a3, fcsr, zero
	-[0x800038cc]:sw t6, 640(s1)
Current Store : [0x800038d0] : sw a3, 644(s1) -- Store: [0x80012f58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003904]:fmul.h t6, t5, t4, dyn
	-[0x80003908]:csrrs a3, fcsr, zero
	-[0x8000390c]:sw t6, 648(s1)
Current Store : [0x80003910] : sw a3, 652(s1) -- Store: [0x80012f60]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003944]:fmul.h t6, t5, t4, dyn
	-[0x80003948]:csrrs a3, fcsr, zero
	-[0x8000394c]:sw t6, 656(s1)
Current Store : [0x80003950] : sw a3, 660(s1) -- Store: [0x80012f68]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003984]:fmul.h t6, t5, t4, dyn
	-[0x80003988]:csrrs a3, fcsr, zero
	-[0x8000398c]:sw t6, 664(s1)
Current Store : [0x80003990] : sw a3, 668(s1) -- Store: [0x80012f70]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039c4]:fmul.h t6, t5, t4, dyn
	-[0x800039c8]:csrrs a3, fcsr, zero
	-[0x800039cc]:sw t6, 672(s1)
Current Store : [0x800039d0] : sw a3, 676(s1) -- Store: [0x80012f78]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a04]:fmul.h t6, t5, t4, dyn
	-[0x80003a08]:csrrs a3, fcsr, zero
	-[0x80003a0c]:sw t6, 680(s1)
Current Store : [0x80003a10] : sw a3, 684(s1) -- Store: [0x80012f80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a44]:fmul.h t6, t5, t4, dyn
	-[0x80003a48]:csrrs a3, fcsr, zero
	-[0x80003a4c]:sw t6, 688(s1)
Current Store : [0x80003a50] : sw a3, 692(s1) -- Store: [0x80012f88]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a84]:fmul.h t6, t5, t4, dyn
	-[0x80003a88]:csrrs a3, fcsr, zero
	-[0x80003a8c]:sw t6, 696(s1)
Current Store : [0x80003a90] : sw a3, 700(s1) -- Store: [0x80012f90]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ac4]:fmul.h t6, t5, t4, dyn
	-[0x80003ac8]:csrrs a3, fcsr, zero
	-[0x80003acc]:sw t6, 704(s1)
Current Store : [0x80003ad0] : sw a3, 708(s1) -- Store: [0x80012f98]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b04]:fmul.h t6, t5, t4, dyn
	-[0x80003b08]:csrrs a3, fcsr, zero
	-[0x80003b0c]:sw t6, 712(s1)
Current Store : [0x80003b10] : sw a3, 716(s1) -- Store: [0x80012fa0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b44]:fmul.h t6, t5, t4, dyn
	-[0x80003b48]:csrrs a3, fcsr, zero
	-[0x80003b4c]:sw t6, 720(s1)
Current Store : [0x80003b50] : sw a3, 724(s1) -- Store: [0x80012fa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b84]:fmul.h t6, t5, t4, dyn
	-[0x80003b88]:csrrs a3, fcsr, zero
	-[0x80003b8c]:sw t6, 728(s1)
Current Store : [0x80003b90] : sw a3, 732(s1) -- Store: [0x80012fb0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003bc4]:fmul.h t6, t5, t4, dyn
	-[0x80003bc8]:csrrs a3, fcsr, zero
	-[0x80003bcc]:sw t6, 736(s1)
Current Store : [0x80003bd0] : sw a3, 740(s1) -- Store: [0x80012fb8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c04]:fmul.h t6, t5, t4, dyn
	-[0x80003c08]:csrrs a3, fcsr, zero
	-[0x80003c0c]:sw t6, 744(s1)
Current Store : [0x80003c10] : sw a3, 748(s1) -- Store: [0x80012fc0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c44]:fmul.h t6, t5, t4, dyn
	-[0x80003c48]:csrrs a3, fcsr, zero
	-[0x80003c4c]:sw t6, 752(s1)
Current Store : [0x80003c50] : sw a3, 756(s1) -- Store: [0x80012fc8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c84]:fmul.h t6, t5, t4, dyn
	-[0x80003c88]:csrrs a3, fcsr, zero
	-[0x80003c8c]:sw t6, 760(s1)
Current Store : [0x80003c90] : sw a3, 764(s1) -- Store: [0x80012fd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003cc4]:fmul.h t6, t5, t4, dyn
	-[0x80003cc8]:csrrs a3, fcsr, zero
	-[0x80003ccc]:sw t6, 768(s1)
Current Store : [0x80003cd0] : sw a3, 772(s1) -- Store: [0x80012fd8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d04]:fmul.h t6, t5, t4, dyn
	-[0x80003d08]:csrrs a3, fcsr, zero
	-[0x80003d0c]:sw t6, 776(s1)
Current Store : [0x80003d10] : sw a3, 780(s1) -- Store: [0x80012fe0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d44]:fmul.h t6, t5, t4, dyn
	-[0x80003d48]:csrrs a3, fcsr, zero
	-[0x80003d4c]:sw t6, 784(s1)
Current Store : [0x80003d50] : sw a3, 788(s1) -- Store: [0x80012fe8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d84]:fmul.h t6, t5, t4, dyn
	-[0x80003d88]:csrrs a3, fcsr, zero
	-[0x80003d8c]:sw t6, 792(s1)
Current Store : [0x80003d90] : sw a3, 796(s1) -- Store: [0x80012ff0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003dc4]:fmul.h t6, t5, t4, dyn
	-[0x80003dc8]:csrrs a3, fcsr, zero
	-[0x80003dcc]:sw t6, 800(s1)
Current Store : [0x80003dd0] : sw a3, 804(s1) -- Store: [0x80012ff8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e04]:fmul.h t6, t5, t4, dyn
	-[0x80003e08]:csrrs a3, fcsr, zero
	-[0x80003e0c]:sw t6, 808(s1)
Current Store : [0x80003e10] : sw a3, 812(s1) -- Store: [0x80013000]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e44]:fmul.h t6, t5, t4, dyn
	-[0x80003e48]:csrrs a3, fcsr, zero
	-[0x80003e4c]:sw t6, 816(s1)
Current Store : [0x80003e50] : sw a3, 820(s1) -- Store: [0x80013008]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e84]:fmul.h t6, t5, t4, dyn
	-[0x80003e88]:csrrs a3, fcsr, zero
	-[0x80003e8c]:sw t6, 824(s1)
Current Store : [0x80003e90] : sw a3, 828(s1) -- Store: [0x80013010]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ec4]:fmul.h t6, t5, t4, dyn
	-[0x80003ec8]:csrrs a3, fcsr, zero
	-[0x80003ecc]:sw t6, 832(s1)
Current Store : [0x80003ed0] : sw a3, 836(s1) -- Store: [0x80013018]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f04]:fmul.h t6, t5, t4, dyn
	-[0x80003f08]:csrrs a3, fcsr, zero
	-[0x80003f0c]:sw t6, 840(s1)
Current Store : [0x80003f10] : sw a3, 844(s1) -- Store: [0x80013020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f44]:fmul.h t6, t5, t4, dyn
	-[0x80003f48]:csrrs a3, fcsr, zero
	-[0x80003f4c]:sw t6, 848(s1)
Current Store : [0x80003f50] : sw a3, 852(s1) -- Store: [0x80013028]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f84]:fmul.h t6, t5, t4, dyn
	-[0x80003f88]:csrrs a3, fcsr, zero
	-[0x80003f8c]:sw t6, 856(s1)
Current Store : [0x80003f90] : sw a3, 860(s1) -- Store: [0x80013030]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fc4]:fmul.h t6, t5, t4, dyn
	-[0x80003fc8]:csrrs a3, fcsr, zero
	-[0x80003fcc]:sw t6, 864(s1)
Current Store : [0x80003fd0] : sw a3, 868(s1) -- Store: [0x80013038]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004004]:fmul.h t6, t5, t4, dyn
	-[0x80004008]:csrrs a3, fcsr, zero
	-[0x8000400c]:sw t6, 872(s1)
Current Store : [0x80004010] : sw a3, 876(s1) -- Store: [0x80013040]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004044]:fmul.h t6, t5, t4, dyn
	-[0x80004048]:csrrs a3, fcsr, zero
	-[0x8000404c]:sw t6, 880(s1)
Current Store : [0x80004050] : sw a3, 884(s1) -- Store: [0x80013048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004084]:fmul.h t6, t5, t4, dyn
	-[0x80004088]:csrrs a3, fcsr, zero
	-[0x8000408c]:sw t6, 888(s1)
Current Store : [0x80004090] : sw a3, 892(s1) -- Store: [0x80013050]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040c4]:fmul.h t6, t5, t4, dyn
	-[0x800040c8]:csrrs a3, fcsr, zero
	-[0x800040cc]:sw t6, 896(s1)
Current Store : [0x800040d0] : sw a3, 900(s1) -- Store: [0x80013058]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004104]:fmul.h t6, t5, t4, dyn
	-[0x80004108]:csrrs a3, fcsr, zero
	-[0x8000410c]:sw t6, 904(s1)
Current Store : [0x80004110] : sw a3, 908(s1) -- Store: [0x80013060]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004144]:fmul.h t6, t5, t4, dyn
	-[0x80004148]:csrrs a3, fcsr, zero
	-[0x8000414c]:sw t6, 912(s1)
Current Store : [0x80004150] : sw a3, 916(s1) -- Store: [0x80013068]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004184]:fmul.h t6, t5, t4, dyn
	-[0x80004188]:csrrs a3, fcsr, zero
	-[0x8000418c]:sw t6, 920(s1)
Current Store : [0x80004190] : sw a3, 924(s1) -- Store: [0x80013070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041c4]:fmul.h t6, t5, t4, dyn
	-[0x800041c8]:csrrs a3, fcsr, zero
	-[0x800041cc]:sw t6, 928(s1)
Current Store : [0x800041d0] : sw a3, 932(s1) -- Store: [0x80013078]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004204]:fmul.h t6, t5, t4, dyn
	-[0x80004208]:csrrs a3, fcsr, zero
	-[0x8000420c]:sw t6, 936(s1)
Current Store : [0x80004210] : sw a3, 940(s1) -- Store: [0x80013080]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004244]:fmul.h t6, t5, t4, dyn
	-[0x80004248]:csrrs a3, fcsr, zero
	-[0x8000424c]:sw t6, 944(s1)
Current Store : [0x80004250] : sw a3, 948(s1) -- Store: [0x80013088]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004284]:fmul.h t6, t5, t4, dyn
	-[0x80004288]:csrrs a3, fcsr, zero
	-[0x8000428c]:sw t6, 952(s1)
Current Store : [0x80004290] : sw a3, 956(s1) -- Store: [0x80013090]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042c4]:fmul.h t6, t5, t4, dyn
	-[0x800042c8]:csrrs a3, fcsr, zero
	-[0x800042cc]:sw t6, 960(s1)
Current Store : [0x800042d0] : sw a3, 964(s1) -- Store: [0x80013098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004304]:fmul.h t6, t5, t4, dyn
	-[0x80004308]:csrrs a3, fcsr, zero
	-[0x8000430c]:sw t6, 968(s1)
Current Store : [0x80004310] : sw a3, 972(s1) -- Store: [0x800130a0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004344]:fmul.h t6, t5, t4, dyn
	-[0x80004348]:csrrs a3, fcsr, zero
	-[0x8000434c]:sw t6, 976(s1)
Current Store : [0x80004350] : sw a3, 980(s1) -- Store: [0x800130a8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004384]:fmul.h t6, t5, t4, dyn
	-[0x80004388]:csrrs a3, fcsr, zero
	-[0x8000438c]:sw t6, 984(s1)
Current Store : [0x80004390] : sw a3, 988(s1) -- Store: [0x800130b0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043c4]:fmul.h t6, t5, t4, dyn
	-[0x800043c8]:csrrs a3, fcsr, zero
	-[0x800043cc]:sw t6, 992(s1)
Current Store : [0x800043d0] : sw a3, 996(s1) -- Store: [0x800130b8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004404]:fmul.h t6, t5, t4, dyn
	-[0x80004408]:csrrs a3, fcsr, zero
	-[0x8000440c]:sw t6, 1000(s1)
Current Store : [0x80004410] : sw a3, 1004(s1) -- Store: [0x800130c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004444]:fmul.h t6, t5, t4, dyn
	-[0x80004448]:csrrs a3, fcsr, zero
	-[0x8000444c]:sw t6, 1008(s1)
Current Store : [0x80004450] : sw a3, 1012(s1) -- Store: [0x800130c8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004484]:fmul.h t6, t5, t4, dyn
	-[0x80004488]:csrrs a3, fcsr, zero
	-[0x8000448c]:sw t6, 1016(s1)
Current Store : [0x80004490] : sw a3, 1020(s1) -- Store: [0x800130d0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044cc]:fmul.h t6, t5, t4, dyn
	-[0x800044d0]:csrrs a3, fcsr, zero
	-[0x800044d4]:sw t6, 0(s1)
Current Store : [0x800044d8] : sw a3, 4(s1) -- Store: [0x800130d8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000450c]:fmul.h t6, t5, t4, dyn
	-[0x80004510]:csrrs a3, fcsr, zero
	-[0x80004514]:sw t6, 8(s1)
Current Store : [0x80004518] : sw a3, 12(s1) -- Store: [0x800130e0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000454c]:fmul.h t6, t5, t4, dyn
	-[0x80004550]:csrrs a3, fcsr, zero
	-[0x80004554]:sw t6, 16(s1)
Current Store : [0x80004558] : sw a3, 20(s1) -- Store: [0x800130e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000458c]:fmul.h t6, t5, t4, dyn
	-[0x80004590]:csrrs a3, fcsr, zero
	-[0x80004594]:sw t6, 24(s1)
Current Store : [0x80004598] : sw a3, 28(s1) -- Store: [0x800130f0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045cc]:fmul.h t6, t5, t4, dyn
	-[0x800045d0]:csrrs a3, fcsr, zero
	-[0x800045d4]:sw t6, 32(s1)
Current Store : [0x800045d8] : sw a3, 36(s1) -- Store: [0x800130f8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000460c]:fmul.h t6, t5, t4, dyn
	-[0x80004610]:csrrs a3, fcsr, zero
	-[0x80004614]:sw t6, 40(s1)
Current Store : [0x80004618] : sw a3, 44(s1) -- Store: [0x80013100]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000464c]:fmul.h t6, t5, t4, dyn
	-[0x80004650]:csrrs a3, fcsr, zero
	-[0x80004654]:sw t6, 48(s1)
Current Store : [0x80004658] : sw a3, 52(s1) -- Store: [0x80013108]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000468c]:fmul.h t6, t5, t4, dyn
	-[0x80004690]:csrrs a3, fcsr, zero
	-[0x80004694]:sw t6, 56(s1)
Current Store : [0x80004698] : sw a3, 60(s1) -- Store: [0x80013110]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046cc]:fmul.h t6, t5, t4, dyn
	-[0x800046d0]:csrrs a3, fcsr, zero
	-[0x800046d4]:sw t6, 64(s1)
Current Store : [0x800046d8] : sw a3, 68(s1) -- Store: [0x80013118]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000470c]:fmul.h t6, t5, t4, dyn
	-[0x80004710]:csrrs a3, fcsr, zero
	-[0x80004714]:sw t6, 72(s1)
Current Store : [0x80004718] : sw a3, 76(s1) -- Store: [0x80013120]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000474c]:fmul.h t6, t5, t4, dyn
	-[0x80004750]:csrrs a3, fcsr, zero
	-[0x80004754]:sw t6, 80(s1)
Current Store : [0x80004758] : sw a3, 84(s1) -- Store: [0x80013128]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000478c]:fmul.h t6, t5, t4, dyn
	-[0x80004790]:csrrs a3, fcsr, zero
	-[0x80004794]:sw t6, 88(s1)
Current Store : [0x80004798] : sw a3, 92(s1) -- Store: [0x80013130]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047cc]:fmul.h t6, t5, t4, dyn
	-[0x800047d0]:csrrs a3, fcsr, zero
	-[0x800047d4]:sw t6, 96(s1)
Current Store : [0x800047d8] : sw a3, 100(s1) -- Store: [0x80013138]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000480c]:fmul.h t6, t5, t4, dyn
	-[0x80004810]:csrrs a3, fcsr, zero
	-[0x80004814]:sw t6, 104(s1)
Current Store : [0x80004818] : sw a3, 108(s1) -- Store: [0x80013140]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000484c]:fmul.h t6, t5, t4, dyn
	-[0x80004850]:csrrs a3, fcsr, zero
	-[0x80004854]:sw t6, 112(s1)
Current Store : [0x80004858] : sw a3, 116(s1) -- Store: [0x80013148]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000488c]:fmul.h t6, t5, t4, dyn
	-[0x80004890]:csrrs a3, fcsr, zero
	-[0x80004894]:sw t6, 120(s1)
Current Store : [0x80004898] : sw a3, 124(s1) -- Store: [0x80013150]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048cc]:fmul.h t6, t5, t4, dyn
	-[0x800048d0]:csrrs a3, fcsr, zero
	-[0x800048d4]:sw t6, 128(s1)
Current Store : [0x800048d8] : sw a3, 132(s1) -- Store: [0x80013158]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000490c]:fmul.h t6, t5, t4, dyn
	-[0x80004910]:csrrs a3, fcsr, zero
	-[0x80004914]:sw t6, 136(s1)
Current Store : [0x80004918] : sw a3, 140(s1) -- Store: [0x80013160]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000494c]:fmul.h t6, t5, t4, dyn
	-[0x80004950]:csrrs a3, fcsr, zero
	-[0x80004954]:sw t6, 144(s1)
Current Store : [0x80004958] : sw a3, 148(s1) -- Store: [0x80013168]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000498c]:fmul.h t6, t5, t4, dyn
	-[0x80004990]:csrrs a3, fcsr, zero
	-[0x80004994]:sw t6, 152(s1)
Current Store : [0x80004998] : sw a3, 156(s1) -- Store: [0x80013170]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049cc]:fmul.h t6, t5, t4, dyn
	-[0x800049d0]:csrrs a3, fcsr, zero
	-[0x800049d4]:sw t6, 160(s1)
Current Store : [0x800049d8] : sw a3, 164(s1) -- Store: [0x80013178]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a0c]:fmul.h t6, t5, t4, dyn
	-[0x80004a10]:csrrs a3, fcsr, zero
	-[0x80004a14]:sw t6, 168(s1)
Current Store : [0x80004a18] : sw a3, 172(s1) -- Store: [0x80013180]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a4c]:fmul.h t6, t5, t4, dyn
	-[0x80004a50]:csrrs a3, fcsr, zero
	-[0x80004a54]:sw t6, 176(s1)
Current Store : [0x80004a58] : sw a3, 180(s1) -- Store: [0x80013188]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a8c]:fmul.h t6, t5, t4, dyn
	-[0x80004a90]:csrrs a3, fcsr, zero
	-[0x80004a94]:sw t6, 184(s1)
Current Store : [0x80004a98] : sw a3, 188(s1) -- Store: [0x80013190]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004acc]:fmul.h t6, t5, t4, dyn
	-[0x80004ad0]:csrrs a3, fcsr, zero
	-[0x80004ad4]:sw t6, 192(s1)
Current Store : [0x80004ad8] : sw a3, 196(s1) -- Store: [0x80013198]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b0c]:fmul.h t6, t5, t4, dyn
	-[0x80004b10]:csrrs a3, fcsr, zero
	-[0x80004b14]:sw t6, 200(s1)
Current Store : [0x80004b18] : sw a3, 204(s1) -- Store: [0x800131a0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b4c]:fmul.h t6, t5, t4, dyn
	-[0x80004b50]:csrrs a3, fcsr, zero
	-[0x80004b54]:sw t6, 208(s1)
Current Store : [0x80004b58] : sw a3, 212(s1) -- Store: [0x800131a8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b8c]:fmul.h t6, t5, t4, dyn
	-[0x80004b90]:csrrs a3, fcsr, zero
	-[0x80004b94]:sw t6, 216(s1)
Current Store : [0x80004b98] : sw a3, 220(s1) -- Store: [0x800131b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bcc]:fmul.h t6, t5, t4, dyn
	-[0x80004bd0]:csrrs a3, fcsr, zero
	-[0x80004bd4]:sw t6, 224(s1)
Current Store : [0x80004bd8] : sw a3, 228(s1) -- Store: [0x800131b8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c0c]:fmul.h t6, t5, t4, dyn
	-[0x80004c10]:csrrs a3, fcsr, zero
	-[0x80004c14]:sw t6, 232(s1)
Current Store : [0x80004c18] : sw a3, 236(s1) -- Store: [0x800131c0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c4c]:fmul.h t6, t5, t4, dyn
	-[0x80004c50]:csrrs a3, fcsr, zero
	-[0x80004c54]:sw t6, 240(s1)
Current Store : [0x80004c58] : sw a3, 244(s1) -- Store: [0x800131c8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c8c]:fmul.h t6, t5, t4, dyn
	-[0x80004c90]:csrrs a3, fcsr, zero
	-[0x80004c94]:sw t6, 248(s1)
Current Store : [0x80004c98] : sw a3, 252(s1) -- Store: [0x800131d0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004ccc]:fmul.h t6, t5, t4, dyn
	-[0x80004cd0]:csrrs a3, fcsr, zero
	-[0x80004cd4]:sw t6, 256(s1)
Current Store : [0x80004cd8] : sw a3, 260(s1) -- Store: [0x800131d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d0c]:fmul.h t6, t5, t4, dyn
	-[0x80004d10]:csrrs a3, fcsr, zero
	-[0x80004d14]:sw t6, 264(s1)
Current Store : [0x80004d18] : sw a3, 268(s1) -- Store: [0x800131e0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d4c]:fmul.h t6, t5, t4, dyn
	-[0x80004d50]:csrrs a3, fcsr, zero
	-[0x80004d54]:sw t6, 272(s1)
Current Store : [0x80004d58] : sw a3, 276(s1) -- Store: [0x800131e8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d8c]:fmul.h t6, t5, t4, dyn
	-[0x80004d90]:csrrs a3, fcsr, zero
	-[0x80004d94]:sw t6, 280(s1)
Current Store : [0x80004d98] : sw a3, 284(s1) -- Store: [0x800131f0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004dcc]:fmul.h t6, t5, t4, dyn
	-[0x80004dd0]:csrrs a3, fcsr, zero
	-[0x80004dd4]:sw t6, 288(s1)
Current Store : [0x80004dd8] : sw a3, 292(s1) -- Store: [0x800131f8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e0c]:fmul.h t6, t5, t4, dyn
	-[0x80004e10]:csrrs a3, fcsr, zero
	-[0x80004e14]:sw t6, 296(s1)
Current Store : [0x80004e18] : sw a3, 300(s1) -- Store: [0x80013200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e4c]:fmul.h t6, t5, t4, dyn
	-[0x80004e50]:csrrs a3, fcsr, zero
	-[0x80004e54]:sw t6, 304(s1)
Current Store : [0x80004e58] : sw a3, 308(s1) -- Store: [0x80013208]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e8c]:fmul.h t6, t5, t4, dyn
	-[0x80004e90]:csrrs a3, fcsr, zero
	-[0x80004e94]:sw t6, 312(s1)
Current Store : [0x80004e98] : sw a3, 316(s1) -- Store: [0x80013210]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004ecc]:fmul.h t6, t5, t4, dyn
	-[0x80004ed0]:csrrs a3, fcsr, zero
	-[0x80004ed4]:sw t6, 320(s1)
Current Store : [0x80004ed8] : sw a3, 324(s1) -- Store: [0x80013218]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f0c]:fmul.h t6, t5, t4, dyn
	-[0x80004f10]:csrrs a3, fcsr, zero
	-[0x80004f14]:sw t6, 328(s1)
Current Store : [0x80004f18] : sw a3, 332(s1) -- Store: [0x80013220]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f4c]:fmul.h t6, t5, t4, dyn
	-[0x80004f50]:csrrs a3, fcsr, zero
	-[0x80004f54]:sw t6, 336(s1)
Current Store : [0x80004f58] : sw a3, 340(s1) -- Store: [0x80013228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f8c]:fmul.h t6, t5, t4, dyn
	-[0x80004f90]:csrrs a3, fcsr, zero
	-[0x80004f94]:sw t6, 344(s1)
Current Store : [0x80004f98] : sw a3, 348(s1) -- Store: [0x80013230]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fcc]:fmul.h t6, t5, t4, dyn
	-[0x80004fd0]:csrrs a3, fcsr, zero
	-[0x80004fd4]:sw t6, 352(s1)
Current Store : [0x80004fd8] : sw a3, 356(s1) -- Store: [0x80013238]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000500c]:fmul.h t6, t5, t4, dyn
	-[0x80005010]:csrrs a3, fcsr, zero
	-[0x80005014]:sw t6, 360(s1)
Current Store : [0x80005018] : sw a3, 364(s1) -- Store: [0x80013240]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000504c]:fmul.h t6, t5, t4, dyn
	-[0x80005050]:csrrs a3, fcsr, zero
	-[0x80005054]:sw t6, 368(s1)
Current Store : [0x80005058] : sw a3, 372(s1) -- Store: [0x80013248]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000508c]:fmul.h t6, t5, t4, dyn
	-[0x80005090]:csrrs a3, fcsr, zero
	-[0x80005094]:sw t6, 376(s1)
Current Store : [0x80005098] : sw a3, 380(s1) -- Store: [0x80013250]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050cc]:fmul.h t6, t5, t4, dyn
	-[0x800050d0]:csrrs a3, fcsr, zero
	-[0x800050d4]:sw t6, 384(s1)
Current Store : [0x800050d8] : sw a3, 388(s1) -- Store: [0x80013258]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000510c]:fmul.h t6, t5, t4, dyn
	-[0x80005110]:csrrs a3, fcsr, zero
	-[0x80005114]:sw t6, 392(s1)
Current Store : [0x80005118] : sw a3, 396(s1) -- Store: [0x80013260]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000514c]:fmul.h t6, t5, t4, dyn
	-[0x80005150]:csrrs a3, fcsr, zero
	-[0x80005154]:sw t6, 400(s1)
Current Store : [0x80005158] : sw a3, 404(s1) -- Store: [0x80013268]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000518c]:fmul.h t6, t5, t4, dyn
	-[0x80005190]:csrrs a3, fcsr, zero
	-[0x80005194]:sw t6, 408(s1)
Current Store : [0x80005198] : sw a3, 412(s1) -- Store: [0x80013270]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051cc]:fmul.h t6, t5, t4, dyn
	-[0x800051d0]:csrrs a3, fcsr, zero
	-[0x800051d4]:sw t6, 416(s1)
Current Store : [0x800051d8] : sw a3, 420(s1) -- Store: [0x80013278]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000520c]:fmul.h t6, t5, t4, dyn
	-[0x80005210]:csrrs a3, fcsr, zero
	-[0x80005214]:sw t6, 424(s1)
Current Store : [0x80005218] : sw a3, 428(s1) -- Store: [0x80013280]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000524c]:fmul.h t6, t5, t4, dyn
	-[0x80005250]:csrrs a3, fcsr, zero
	-[0x80005254]:sw t6, 432(s1)
Current Store : [0x80005258] : sw a3, 436(s1) -- Store: [0x80013288]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000528c]:fmul.h t6, t5, t4, dyn
	-[0x80005290]:csrrs a3, fcsr, zero
	-[0x80005294]:sw t6, 440(s1)
Current Store : [0x80005298] : sw a3, 444(s1) -- Store: [0x80013290]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052cc]:fmul.h t6, t5, t4, dyn
	-[0x800052d0]:csrrs a3, fcsr, zero
	-[0x800052d4]:sw t6, 448(s1)
Current Store : [0x800052d8] : sw a3, 452(s1) -- Store: [0x80013298]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000530c]:fmul.h t6, t5, t4, dyn
	-[0x80005310]:csrrs a3, fcsr, zero
	-[0x80005314]:sw t6, 456(s1)
Current Store : [0x80005318] : sw a3, 460(s1) -- Store: [0x800132a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000534c]:fmul.h t6, t5, t4, dyn
	-[0x80005350]:csrrs a3, fcsr, zero
	-[0x80005354]:sw t6, 464(s1)
Current Store : [0x80005358] : sw a3, 468(s1) -- Store: [0x800132a8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000538c]:fmul.h t6, t5, t4, dyn
	-[0x80005390]:csrrs a3, fcsr, zero
	-[0x80005394]:sw t6, 472(s1)
Current Store : [0x80005398] : sw a3, 476(s1) -- Store: [0x800132b0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053cc]:fmul.h t6, t5, t4, dyn
	-[0x800053d0]:csrrs a3, fcsr, zero
	-[0x800053d4]:sw t6, 480(s1)
Current Store : [0x800053d8] : sw a3, 484(s1) -- Store: [0x800132b8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000540c]:fmul.h t6, t5, t4, dyn
	-[0x80005410]:csrrs a3, fcsr, zero
	-[0x80005414]:sw t6, 488(s1)
Current Store : [0x80005418] : sw a3, 492(s1) -- Store: [0x800132c0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000544c]:fmul.h t6, t5, t4, dyn
	-[0x80005450]:csrrs a3, fcsr, zero
	-[0x80005454]:sw t6, 496(s1)
Current Store : [0x80005458] : sw a3, 500(s1) -- Store: [0x800132c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000548c]:fmul.h t6, t5, t4, dyn
	-[0x80005490]:csrrs a3, fcsr, zero
	-[0x80005494]:sw t6, 504(s1)
Current Store : [0x80005498] : sw a3, 508(s1) -- Store: [0x800132d0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054cc]:fmul.h t6, t5, t4, dyn
	-[0x800054d0]:csrrs a3, fcsr, zero
	-[0x800054d4]:sw t6, 512(s1)
Current Store : [0x800054d8] : sw a3, 516(s1) -- Store: [0x800132d8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000550c]:fmul.h t6, t5, t4, dyn
	-[0x80005510]:csrrs a3, fcsr, zero
	-[0x80005514]:sw t6, 520(s1)
Current Store : [0x80005518] : sw a3, 524(s1) -- Store: [0x800132e0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000554c]:fmul.h t6, t5, t4, dyn
	-[0x80005550]:csrrs a3, fcsr, zero
	-[0x80005554]:sw t6, 528(s1)
Current Store : [0x80005558] : sw a3, 532(s1) -- Store: [0x800132e8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000558c]:fmul.h t6, t5, t4, dyn
	-[0x80005590]:csrrs a3, fcsr, zero
	-[0x80005594]:sw t6, 536(s1)
Current Store : [0x80005598] : sw a3, 540(s1) -- Store: [0x800132f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055cc]:fmul.h t6, t5, t4, dyn
	-[0x800055d0]:csrrs a3, fcsr, zero
	-[0x800055d4]:sw t6, 544(s1)
Current Store : [0x800055d8] : sw a3, 548(s1) -- Store: [0x800132f8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000560c]:fmul.h t6, t5, t4, dyn
	-[0x80005610]:csrrs a3, fcsr, zero
	-[0x80005614]:sw t6, 552(s1)
Current Store : [0x80005618] : sw a3, 556(s1) -- Store: [0x80013300]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000564c]:fmul.h t6, t5, t4, dyn
	-[0x80005650]:csrrs a3, fcsr, zero
	-[0x80005654]:sw t6, 560(s1)
Current Store : [0x80005658] : sw a3, 564(s1) -- Store: [0x80013308]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000568c]:fmul.h t6, t5, t4, dyn
	-[0x80005690]:csrrs a3, fcsr, zero
	-[0x80005694]:sw t6, 568(s1)
Current Store : [0x80005698] : sw a3, 572(s1) -- Store: [0x80013310]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056cc]:fmul.h t6, t5, t4, dyn
	-[0x800056d0]:csrrs a3, fcsr, zero
	-[0x800056d4]:sw t6, 576(s1)
Current Store : [0x800056d8] : sw a3, 580(s1) -- Store: [0x80013318]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000570c]:fmul.h t6, t5, t4, dyn
	-[0x80005710]:csrrs a3, fcsr, zero
	-[0x80005714]:sw t6, 584(s1)
Current Store : [0x80005718] : sw a3, 588(s1) -- Store: [0x80013320]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000574c]:fmul.h t6, t5, t4, dyn
	-[0x80005750]:csrrs a3, fcsr, zero
	-[0x80005754]:sw t6, 592(s1)
Current Store : [0x80005758] : sw a3, 596(s1) -- Store: [0x80013328]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000578c]:fmul.h t6, t5, t4, dyn
	-[0x80005790]:csrrs a3, fcsr, zero
	-[0x80005794]:sw t6, 600(s1)
Current Store : [0x80005798] : sw a3, 604(s1) -- Store: [0x80013330]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057cc]:fmul.h t6, t5, t4, dyn
	-[0x800057d0]:csrrs a3, fcsr, zero
	-[0x800057d4]:sw t6, 608(s1)
Current Store : [0x800057d8] : sw a3, 612(s1) -- Store: [0x80013338]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000580c]:fmul.h t6, t5, t4, dyn
	-[0x80005810]:csrrs a3, fcsr, zero
	-[0x80005814]:sw t6, 616(s1)
Current Store : [0x80005818] : sw a3, 620(s1) -- Store: [0x80013340]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000584c]:fmul.h t6, t5, t4, dyn
	-[0x80005850]:csrrs a3, fcsr, zero
	-[0x80005854]:sw t6, 624(s1)
Current Store : [0x80005858] : sw a3, 628(s1) -- Store: [0x80013348]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000588c]:fmul.h t6, t5, t4, dyn
	-[0x80005890]:csrrs a3, fcsr, zero
	-[0x80005894]:sw t6, 632(s1)
Current Store : [0x80005898] : sw a3, 636(s1) -- Store: [0x80013350]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058cc]:fmul.h t6, t5, t4, dyn
	-[0x800058d0]:csrrs a3, fcsr, zero
	-[0x800058d4]:sw t6, 640(s1)
Current Store : [0x800058d8] : sw a3, 644(s1) -- Store: [0x80013358]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000590c]:fmul.h t6, t5, t4, dyn
	-[0x80005910]:csrrs a3, fcsr, zero
	-[0x80005914]:sw t6, 648(s1)
Current Store : [0x80005918] : sw a3, 652(s1) -- Store: [0x80013360]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000594c]:fmul.h t6, t5, t4, dyn
	-[0x80005950]:csrrs a3, fcsr, zero
	-[0x80005954]:sw t6, 656(s1)
Current Store : [0x80005958] : sw a3, 660(s1) -- Store: [0x80013368]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000598c]:fmul.h t6, t5, t4, dyn
	-[0x80005990]:csrrs a3, fcsr, zero
	-[0x80005994]:sw t6, 664(s1)
Current Store : [0x80005998] : sw a3, 668(s1) -- Store: [0x80013370]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059cc]:fmul.h t6, t5, t4, dyn
	-[0x800059d0]:csrrs a3, fcsr, zero
	-[0x800059d4]:sw t6, 672(s1)
Current Store : [0x800059d8] : sw a3, 676(s1) -- Store: [0x80013378]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a0c]:fmul.h t6, t5, t4, dyn
	-[0x80005a10]:csrrs a3, fcsr, zero
	-[0x80005a14]:sw t6, 680(s1)
Current Store : [0x80005a18] : sw a3, 684(s1) -- Store: [0x80013380]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a4c]:fmul.h t6, t5, t4, dyn
	-[0x80005a50]:csrrs a3, fcsr, zero
	-[0x80005a54]:sw t6, 688(s1)
Current Store : [0x80005a58] : sw a3, 692(s1) -- Store: [0x80013388]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a8c]:fmul.h t6, t5, t4, dyn
	-[0x80005a90]:csrrs a3, fcsr, zero
	-[0x80005a94]:sw t6, 696(s1)
Current Store : [0x80005a98] : sw a3, 700(s1) -- Store: [0x80013390]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005acc]:fmul.h t6, t5, t4, dyn
	-[0x80005ad0]:csrrs a3, fcsr, zero
	-[0x80005ad4]:sw t6, 704(s1)
Current Store : [0x80005ad8] : sw a3, 708(s1) -- Store: [0x80013398]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b0c]:fmul.h t6, t5, t4, dyn
	-[0x80005b10]:csrrs a3, fcsr, zero
	-[0x80005b14]:sw t6, 712(s1)
Current Store : [0x80005b18] : sw a3, 716(s1) -- Store: [0x800133a0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b4c]:fmul.h t6, t5, t4, dyn
	-[0x80005b50]:csrrs a3, fcsr, zero
	-[0x80005b54]:sw t6, 720(s1)
Current Store : [0x80005b58] : sw a3, 724(s1) -- Store: [0x800133a8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b8c]:fmul.h t6, t5, t4, dyn
	-[0x80005b90]:csrrs a3, fcsr, zero
	-[0x80005b94]:sw t6, 728(s1)
Current Store : [0x80005b98] : sw a3, 732(s1) -- Store: [0x800133b0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bcc]:fmul.h t6, t5, t4, dyn
	-[0x80005bd0]:csrrs a3, fcsr, zero
	-[0x80005bd4]:sw t6, 736(s1)
Current Store : [0x80005bd8] : sw a3, 740(s1) -- Store: [0x800133b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c0c]:fmul.h t6, t5, t4, dyn
	-[0x80005c10]:csrrs a3, fcsr, zero
	-[0x80005c14]:sw t6, 744(s1)
Current Store : [0x80005c18] : sw a3, 748(s1) -- Store: [0x800133c0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c4c]:fmul.h t6, t5, t4, dyn
	-[0x80005c50]:csrrs a3, fcsr, zero
	-[0x80005c54]:sw t6, 752(s1)
Current Store : [0x80005c58] : sw a3, 756(s1) -- Store: [0x800133c8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c8c]:fmul.h t6, t5, t4, dyn
	-[0x80005c90]:csrrs a3, fcsr, zero
	-[0x80005c94]:sw t6, 760(s1)
Current Store : [0x80005c98] : sw a3, 764(s1) -- Store: [0x800133d0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005ccc]:fmul.h t6, t5, t4, dyn
	-[0x80005cd0]:csrrs a3, fcsr, zero
	-[0x80005cd4]:sw t6, 768(s1)
Current Store : [0x80005cd8] : sw a3, 772(s1) -- Store: [0x800133d8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d0c]:fmul.h t6, t5, t4, dyn
	-[0x80005d10]:csrrs a3, fcsr, zero
	-[0x80005d14]:sw t6, 776(s1)
Current Store : [0x80005d18] : sw a3, 780(s1) -- Store: [0x800133e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d4c]:fmul.h t6, t5, t4, dyn
	-[0x80005d50]:csrrs a3, fcsr, zero
	-[0x80005d54]:sw t6, 784(s1)
Current Store : [0x80005d58] : sw a3, 788(s1) -- Store: [0x800133e8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d8c]:fmul.h t6, t5, t4, dyn
	-[0x80005d90]:csrrs a3, fcsr, zero
	-[0x80005d94]:sw t6, 792(s1)
Current Store : [0x80005d98] : sw a3, 796(s1) -- Store: [0x800133f0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005dcc]:fmul.h t6, t5, t4, dyn
	-[0x80005dd0]:csrrs a3, fcsr, zero
	-[0x80005dd4]:sw t6, 800(s1)
Current Store : [0x80005dd8] : sw a3, 804(s1) -- Store: [0x800133f8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e0c]:fmul.h t6, t5, t4, dyn
	-[0x80005e10]:csrrs a3, fcsr, zero
	-[0x80005e14]:sw t6, 808(s1)
Current Store : [0x80005e18] : sw a3, 812(s1) -- Store: [0x80013400]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e4c]:fmul.h t6, t5, t4, dyn
	-[0x80005e50]:csrrs a3, fcsr, zero
	-[0x80005e54]:sw t6, 816(s1)
Current Store : [0x80005e58] : sw a3, 820(s1) -- Store: [0x80013408]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e8c]:fmul.h t6, t5, t4, dyn
	-[0x80005e90]:csrrs a3, fcsr, zero
	-[0x80005e94]:sw t6, 824(s1)
Current Store : [0x80005e98] : sw a3, 828(s1) -- Store: [0x80013410]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005ecc]:fmul.h t6, t5, t4, dyn
	-[0x80005ed0]:csrrs a3, fcsr, zero
	-[0x80005ed4]:sw t6, 832(s1)
Current Store : [0x80005ed8] : sw a3, 836(s1) -- Store: [0x80013418]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f0c]:fmul.h t6, t5, t4, dyn
	-[0x80005f10]:csrrs a3, fcsr, zero
	-[0x80005f14]:sw t6, 840(s1)
Current Store : [0x80005f18] : sw a3, 844(s1) -- Store: [0x80013420]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f4c]:fmul.h t6, t5, t4, dyn
	-[0x80005f50]:csrrs a3, fcsr, zero
	-[0x80005f54]:sw t6, 848(s1)
Current Store : [0x80005f58] : sw a3, 852(s1) -- Store: [0x80013428]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f8c]:fmul.h t6, t5, t4, dyn
	-[0x80005f90]:csrrs a3, fcsr, zero
	-[0x80005f94]:sw t6, 856(s1)
Current Store : [0x80005f98] : sw a3, 860(s1) -- Store: [0x80013430]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fcc]:fmul.h t6, t5, t4, dyn
	-[0x80005fd0]:csrrs a3, fcsr, zero
	-[0x80005fd4]:sw t6, 864(s1)
Current Store : [0x80005fd8] : sw a3, 868(s1) -- Store: [0x80013438]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000600c]:fmul.h t6, t5, t4, dyn
	-[0x80006010]:csrrs a3, fcsr, zero
	-[0x80006014]:sw t6, 872(s1)
Current Store : [0x80006018] : sw a3, 876(s1) -- Store: [0x80013440]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000604c]:fmul.h t6, t5, t4, dyn
	-[0x80006050]:csrrs a3, fcsr, zero
	-[0x80006054]:sw t6, 880(s1)
Current Store : [0x80006058] : sw a3, 884(s1) -- Store: [0x80013448]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000608c]:fmul.h t6, t5, t4, dyn
	-[0x80006090]:csrrs a3, fcsr, zero
	-[0x80006094]:sw t6, 888(s1)
Current Store : [0x80006098] : sw a3, 892(s1) -- Store: [0x80013450]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060cc]:fmul.h t6, t5, t4, dyn
	-[0x800060d0]:csrrs a3, fcsr, zero
	-[0x800060d4]:sw t6, 896(s1)
Current Store : [0x800060d8] : sw a3, 900(s1) -- Store: [0x80013458]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000610c]:fmul.h t6, t5, t4, dyn
	-[0x80006110]:csrrs a3, fcsr, zero
	-[0x80006114]:sw t6, 904(s1)
Current Store : [0x80006118] : sw a3, 908(s1) -- Store: [0x80013460]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000614c]:fmul.h t6, t5, t4, dyn
	-[0x80006150]:csrrs a3, fcsr, zero
	-[0x80006154]:sw t6, 912(s1)
Current Store : [0x80006158] : sw a3, 916(s1) -- Store: [0x80013468]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000618c]:fmul.h t6, t5, t4, dyn
	-[0x80006190]:csrrs a3, fcsr, zero
	-[0x80006194]:sw t6, 920(s1)
Current Store : [0x80006198] : sw a3, 924(s1) -- Store: [0x80013470]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061cc]:fmul.h t6, t5, t4, dyn
	-[0x800061d0]:csrrs a3, fcsr, zero
	-[0x800061d4]:sw t6, 928(s1)
Current Store : [0x800061d8] : sw a3, 932(s1) -- Store: [0x80013478]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000620c]:fmul.h t6, t5, t4, dyn
	-[0x80006210]:csrrs a3, fcsr, zero
	-[0x80006214]:sw t6, 936(s1)
Current Store : [0x80006218] : sw a3, 940(s1) -- Store: [0x80013480]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000624c]:fmul.h t6, t5, t4, dyn
	-[0x80006250]:csrrs a3, fcsr, zero
	-[0x80006254]:sw t6, 944(s1)
Current Store : [0x80006258] : sw a3, 948(s1) -- Store: [0x80013488]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000628c]:fmul.h t6, t5, t4, dyn
	-[0x80006290]:csrrs a3, fcsr, zero
	-[0x80006294]:sw t6, 952(s1)
Current Store : [0x80006298] : sw a3, 956(s1) -- Store: [0x80013490]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062cc]:fmul.h t6, t5, t4, dyn
	-[0x800062d0]:csrrs a3, fcsr, zero
	-[0x800062d4]:sw t6, 960(s1)
Current Store : [0x800062d8] : sw a3, 964(s1) -- Store: [0x80013498]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000630c]:fmul.h t6, t5, t4, dyn
	-[0x80006310]:csrrs a3, fcsr, zero
	-[0x80006314]:sw t6, 968(s1)
Current Store : [0x80006318] : sw a3, 972(s1) -- Store: [0x800134a0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000634c]:fmul.h t6, t5, t4, dyn
	-[0x80006350]:csrrs a3, fcsr, zero
	-[0x80006354]:sw t6, 976(s1)
Current Store : [0x80006358] : sw a3, 980(s1) -- Store: [0x800134a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000638c]:fmul.h t6, t5, t4, dyn
	-[0x80006390]:csrrs a3, fcsr, zero
	-[0x80006394]:sw t6, 984(s1)
Current Store : [0x80006398] : sw a3, 988(s1) -- Store: [0x800134b0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063cc]:fmul.h t6, t5, t4, dyn
	-[0x800063d0]:csrrs a3, fcsr, zero
	-[0x800063d4]:sw t6, 992(s1)
Current Store : [0x800063d8] : sw a3, 996(s1) -- Store: [0x800134b8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006404]:fmul.h t6, t5, t4, dyn
	-[0x80006408]:csrrs a3, fcsr, zero
	-[0x8000640c]:sw t6, 1000(s1)
Current Store : [0x80006410] : sw a3, 1004(s1) -- Store: [0x800134c0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000643c]:fmul.h t6, t5, t4, dyn
	-[0x80006440]:csrrs a3, fcsr, zero
	-[0x80006444]:sw t6, 1008(s1)
Current Store : [0x80006448] : sw a3, 1012(s1) -- Store: [0x800134c8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006474]:fmul.h t6, t5, t4, dyn
	-[0x80006478]:csrrs a3, fcsr, zero
	-[0x8000647c]:sw t6, 1016(s1)
Current Store : [0x80006480] : sw a3, 1020(s1) -- Store: [0x800134d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064b4]:fmul.h t6, t5, t4, dyn
	-[0x800064b8]:csrrs a3, fcsr, zero
	-[0x800064bc]:sw t6, 0(s1)
Current Store : [0x800064c0] : sw a3, 4(s1) -- Store: [0x800134d8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064ec]:fmul.h t6, t5, t4, dyn
	-[0x800064f0]:csrrs a3, fcsr, zero
	-[0x800064f4]:sw t6, 8(s1)
Current Store : [0x800064f8] : sw a3, 12(s1) -- Store: [0x800134e0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006524]:fmul.h t6, t5, t4, dyn
	-[0x80006528]:csrrs a3, fcsr, zero
	-[0x8000652c]:sw t6, 16(s1)
Current Store : [0x80006530] : sw a3, 20(s1) -- Store: [0x800134e8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000655c]:fmul.h t6, t5, t4, dyn
	-[0x80006560]:csrrs a3, fcsr, zero
	-[0x80006564]:sw t6, 24(s1)
Current Store : [0x80006568] : sw a3, 28(s1) -- Store: [0x800134f0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006594]:fmul.h t6, t5, t4, dyn
	-[0x80006598]:csrrs a3, fcsr, zero
	-[0x8000659c]:sw t6, 32(s1)
Current Store : [0x800065a0] : sw a3, 36(s1) -- Store: [0x800134f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065cc]:fmul.h t6, t5, t4, dyn
	-[0x800065d0]:csrrs a3, fcsr, zero
	-[0x800065d4]:sw t6, 40(s1)
Current Store : [0x800065d8] : sw a3, 44(s1) -- Store: [0x80013500]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006604]:fmul.h t6, t5, t4, dyn
	-[0x80006608]:csrrs a3, fcsr, zero
	-[0x8000660c]:sw t6, 48(s1)
Current Store : [0x80006610] : sw a3, 52(s1) -- Store: [0x80013508]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000663c]:fmul.h t6, t5, t4, dyn
	-[0x80006640]:csrrs a3, fcsr, zero
	-[0x80006644]:sw t6, 56(s1)
Current Store : [0x80006648] : sw a3, 60(s1) -- Store: [0x80013510]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006674]:fmul.h t6, t5, t4, dyn
	-[0x80006678]:csrrs a3, fcsr, zero
	-[0x8000667c]:sw t6, 64(s1)
Current Store : [0x80006680] : sw a3, 68(s1) -- Store: [0x80013518]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800066ac]:fmul.h t6, t5, t4, dyn
	-[0x800066b0]:csrrs a3, fcsr, zero
	-[0x800066b4]:sw t6, 72(s1)
Current Store : [0x800066b8] : sw a3, 76(s1) -- Store: [0x80013520]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800066e4]:fmul.h t6, t5, t4, dyn
	-[0x800066e8]:csrrs a3, fcsr, zero
	-[0x800066ec]:sw t6, 80(s1)
Current Store : [0x800066f0] : sw a3, 84(s1) -- Store: [0x80013528]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000671c]:fmul.h t6, t5, t4, dyn
	-[0x80006720]:csrrs a3, fcsr, zero
	-[0x80006724]:sw t6, 88(s1)
Current Store : [0x80006728] : sw a3, 92(s1) -- Store: [0x80013530]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006754]:fmul.h t6, t5, t4, dyn
	-[0x80006758]:csrrs a3, fcsr, zero
	-[0x8000675c]:sw t6, 96(s1)
Current Store : [0x80006760] : sw a3, 100(s1) -- Store: [0x80013538]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000678c]:fmul.h t6, t5, t4, dyn
	-[0x80006790]:csrrs a3, fcsr, zero
	-[0x80006794]:sw t6, 104(s1)
Current Store : [0x80006798] : sw a3, 108(s1) -- Store: [0x80013540]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067c4]:fmul.h t6, t5, t4, dyn
	-[0x800067c8]:csrrs a3, fcsr, zero
	-[0x800067cc]:sw t6, 112(s1)
Current Store : [0x800067d0] : sw a3, 116(s1) -- Store: [0x80013548]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067fc]:fmul.h t6, t5, t4, dyn
	-[0x80006800]:csrrs a3, fcsr, zero
	-[0x80006804]:sw t6, 120(s1)
Current Store : [0x80006808] : sw a3, 124(s1) -- Store: [0x80013550]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006834]:fmul.h t6, t5, t4, dyn
	-[0x80006838]:csrrs a3, fcsr, zero
	-[0x8000683c]:sw t6, 128(s1)
Current Store : [0x80006840] : sw a3, 132(s1) -- Store: [0x80013558]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000686c]:fmul.h t6, t5, t4, dyn
	-[0x80006870]:csrrs a3, fcsr, zero
	-[0x80006874]:sw t6, 136(s1)
Current Store : [0x80006878] : sw a3, 140(s1) -- Store: [0x80013560]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068a4]:fmul.h t6, t5, t4, dyn
	-[0x800068a8]:csrrs a3, fcsr, zero
	-[0x800068ac]:sw t6, 144(s1)
Current Store : [0x800068b0] : sw a3, 148(s1) -- Store: [0x80013568]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068dc]:fmul.h t6, t5, t4, dyn
	-[0x800068e0]:csrrs a3, fcsr, zero
	-[0x800068e4]:sw t6, 152(s1)
Current Store : [0x800068e8] : sw a3, 156(s1) -- Store: [0x80013570]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006914]:fmul.h t6, t5, t4, dyn
	-[0x80006918]:csrrs a3, fcsr, zero
	-[0x8000691c]:sw t6, 160(s1)
Current Store : [0x80006920] : sw a3, 164(s1) -- Store: [0x80013578]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000694c]:fmul.h t6, t5, t4, dyn
	-[0x80006950]:csrrs a3, fcsr, zero
	-[0x80006954]:sw t6, 168(s1)
Current Store : [0x80006958] : sw a3, 172(s1) -- Store: [0x80013580]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006984]:fmul.h t6, t5, t4, dyn
	-[0x80006988]:csrrs a3, fcsr, zero
	-[0x8000698c]:sw t6, 176(s1)
Current Store : [0x80006990] : sw a3, 180(s1) -- Store: [0x80013588]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069bc]:fmul.h t6, t5, t4, dyn
	-[0x800069c0]:csrrs a3, fcsr, zero
	-[0x800069c4]:sw t6, 184(s1)
Current Store : [0x800069c8] : sw a3, 188(s1) -- Store: [0x80013590]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069f4]:fmul.h t6, t5, t4, dyn
	-[0x800069f8]:csrrs a3, fcsr, zero
	-[0x800069fc]:sw t6, 192(s1)
Current Store : [0x80006a00] : sw a3, 196(s1) -- Store: [0x80013598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a2c]:fmul.h t6, t5, t4, dyn
	-[0x80006a30]:csrrs a3, fcsr, zero
	-[0x80006a34]:sw t6, 200(s1)
Current Store : [0x80006a38] : sw a3, 204(s1) -- Store: [0x800135a0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a64]:fmul.h t6, t5, t4, dyn
	-[0x80006a68]:csrrs a3, fcsr, zero
	-[0x80006a6c]:sw t6, 208(s1)
Current Store : [0x80006a70] : sw a3, 212(s1) -- Store: [0x800135a8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a9c]:fmul.h t6, t5, t4, dyn
	-[0x80006aa0]:csrrs a3, fcsr, zero
	-[0x80006aa4]:sw t6, 216(s1)
Current Store : [0x80006aa8] : sw a3, 220(s1) -- Store: [0x800135b0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ad4]:fmul.h t6, t5, t4, dyn
	-[0x80006ad8]:csrrs a3, fcsr, zero
	-[0x80006adc]:sw t6, 224(s1)
Current Store : [0x80006ae0] : sw a3, 228(s1) -- Store: [0x800135b8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b0c]:fmul.h t6, t5, t4, dyn
	-[0x80006b10]:csrrs a3, fcsr, zero
	-[0x80006b14]:sw t6, 232(s1)
Current Store : [0x80006b18] : sw a3, 236(s1) -- Store: [0x800135c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b44]:fmul.h t6, t5, t4, dyn
	-[0x80006b48]:csrrs a3, fcsr, zero
	-[0x80006b4c]:sw t6, 240(s1)
Current Store : [0x80006b50] : sw a3, 244(s1) -- Store: [0x800135c8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b7c]:fmul.h t6, t5, t4, dyn
	-[0x80006b80]:csrrs a3, fcsr, zero
	-[0x80006b84]:sw t6, 248(s1)
Current Store : [0x80006b88] : sw a3, 252(s1) -- Store: [0x800135d0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006bb4]:fmul.h t6, t5, t4, dyn
	-[0x80006bb8]:csrrs a3, fcsr, zero
	-[0x80006bbc]:sw t6, 256(s1)
Current Store : [0x80006bc0] : sw a3, 260(s1) -- Store: [0x800135d8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006bec]:fmul.h t6, t5, t4, dyn
	-[0x80006bf0]:csrrs a3, fcsr, zero
	-[0x80006bf4]:sw t6, 264(s1)
Current Store : [0x80006bf8] : sw a3, 268(s1) -- Store: [0x800135e0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c24]:fmul.h t6, t5, t4, dyn
	-[0x80006c28]:csrrs a3, fcsr, zero
	-[0x80006c2c]:sw t6, 272(s1)
Current Store : [0x80006c30] : sw a3, 276(s1) -- Store: [0x800135e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c5c]:fmul.h t6, t5, t4, dyn
	-[0x80006c60]:csrrs a3, fcsr, zero
	-[0x80006c64]:sw t6, 280(s1)
Current Store : [0x80006c68] : sw a3, 284(s1) -- Store: [0x800135f0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c94]:fmul.h t6, t5, t4, dyn
	-[0x80006c98]:csrrs a3, fcsr, zero
	-[0x80006c9c]:sw t6, 288(s1)
Current Store : [0x80006ca0] : sw a3, 292(s1) -- Store: [0x800135f8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ccc]:fmul.h t6, t5, t4, dyn
	-[0x80006cd0]:csrrs a3, fcsr, zero
	-[0x80006cd4]:sw t6, 296(s1)
Current Store : [0x80006cd8] : sw a3, 300(s1) -- Store: [0x80013600]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d04]:fmul.h t6, t5, t4, dyn
	-[0x80006d08]:csrrs a3, fcsr, zero
	-[0x80006d0c]:sw t6, 304(s1)
Current Store : [0x80006d10] : sw a3, 308(s1) -- Store: [0x80013608]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d3c]:fmul.h t6, t5, t4, dyn
	-[0x80006d40]:csrrs a3, fcsr, zero
	-[0x80006d44]:sw t6, 312(s1)
Current Store : [0x80006d48] : sw a3, 316(s1) -- Store: [0x80013610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d74]:fmul.h t6, t5, t4, dyn
	-[0x80006d78]:csrrs a3, fcsr, zero
	-[0x80006d7c]:sw t6, 320(s1)
Current Store : [0x80006d80] : sw a3, 324(s1) -- Store: [0x80013618]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006dac]:fmul.h t6, t5, t4, dyn
	-[0x80006db0]:csrrs a3, fcsr, zero
	-[0x80006db4]:sw t6, 328(s1)
Current Store : [0x80006db8] : sw a3, 332(s1) -- Store: [0x80013620]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006de4]:fmul.h t6, t5, t4, dyn
	-[0x80006de8]:csrrs a3, fcsr, zero
	-[0x80006dec]:sw t6, 336(s1)
Current Store : [0x80006df0] : sw a3, 340(s1) -- Store: [0x80013628]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e1c]:fmul.h t6, t5, t4, dyn
	-[0x80006e20]:csrrs a3, fcsr, zero
	-[0x80006e24]:sw t6, 344(s1)
Current Store : [0x80006e28] : sw a3, 348(s1) -- Store: [0x80013630]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e54]:fmul.h t6, t5, t4, dyn
	-[0x80006e58]:csrrs a3, fcsr, zero
	-[0x80006e5c]:sw t6, 352(s1)
Current Store : [0x80006e60] : sw a3, 356(s1) -- Store: [0x80013638]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e8c]:fmul.h t6, t5, t4, dyn
	-[0x80006e90]:csrrs a3, fcsr, zero
	-[0x80006e94]:sw t6, 360(s1)
Current Store : [0x80006e98] : sw a3, 364(s1) -- Store: [0x80013640]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006ec4]:fmul.h t6, t5, t4, dyn
	-[0x80006ec8]:csrrs a3, fcsr, zero
	-[0x80006ecc]:sw t6, 368(s1)
Current Store : [0x80006ed0] : sw a3, 372(s1) -- Store: [0x80013648]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006efc]:fmul.h t6, t5, t4, dyn
	-[0x80006f00]:csrrs a3, fcsr, zero
	-[0x80006f04]:sw t6, 376(s1)
Current Store : [0x80006f08] : sw a3, 380(s1) -- Store: [0x80013650]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f34]:fmul.h t6, t5, t4, dyn
	-[0x80006f38]:csrrs a3, fcsr, zero
	-[0x80006f3c]:sw t6, 384(s1)
Current Store : [0x80006f40] : sw a3, 388(s1) -- Store: [0x80013658]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006f6c]:fmul.h t6, t5, t4, dyn
	-[0x80006f70]:csrrs a3, fcsr, zero
	-[0x80006f74]:sw t6, 392(s1)
Current Store : [0x80006f78] : sw a3, 396(s1) -- Store: [0x80013660]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006fa4]:fmul.h t6, t5, t4, dyn
	-[0x80006fa8]:csrrs a3, fcsr, zero
	-[0x80006fac]:sw t6, 400(s1)
Current Store : [0x80006fb0] : sw a3, 404(s1) -- Store: [0x80013668]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006fdc]:fmul.h t6, t5, t4, dyn
	-[0x80006fe0]:csrrs a3, fcsr, zero
	-[0x80006fe4]:sw t6, 408(s1)
Current Store : [0x80006fe8] : sw a3, 412(s1) -- Store: [0x80013670]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007014]:fmul.h t6, t5, t4, dyn
	-[0x80007018]:csrrs a3, fcsr, zero
	-[0x8000701c]:sw t6, 416(s1)
Current Store : [0x80007020] : sw a3, 420(s1) -- Store: [0x80013678]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000704c]:fmul.h t6, t5, t4, dyn
	-[0x80007050]:csrrs a3, fcsr, zero
	-[0x80007054]:sw t6, 424(s1)
Current Store : [0x80007058] : sw a3, 428(s1) -- Store: [0x80013680]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007084]:fmul.h t6, t5, t4, dyn
	-[0x80007088]:csrrs a3, fcsr, zero
	-[0x8000708c]:sw t6, 432(s1)
Current Store : [0x80007090] : sw a3, 436(s1) -- Store: [0x80013688]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800070bc]:fmul.h t6, t5, t4, dyn
	-[0x800070c0]:csrrs a3, fcsr, zero
	-[0x800070c4]:sw t6, 440(s1)
Current Store : [0x800070c8] : sw a3, 444(s1) -- Store: [0x80013690]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800070f4]:fmul.h t6, t5, t4, dyn
	-[0x800070f8]:csrrs a3, fcsr, zero
	-[0x800070fc]:sw t6, 448(s1)
Current Store : [0x80007100] : sw a3, 452(s1) -- Store: [0x80013698]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000712c]:fmul.h t6, t5, t4, dyn
	-[0x80007130]:csrrs a3, fcsr, zero
	-[0x80007134]:sw t6, 456(s1)
Current Store : [0x80007138] : sw a3, 460(s1) -- Store: [0x800136a0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007164]:fmul.h t6, t5, t4, dyn
	-[0x80007168]:csrrs a3, fcsr, zero
	-[0x8000716c]:sw t6, 464(s1)
Current Store : [0x80007170] : sw a3, 468(s1) -- Store: [0x800136a8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000719c]:fmul.h t6, t5, t4, dyn
	-[0x800071a0]:csrrs a3, fcsr, zero
	-[0x800071a4]:sw t6, 472(s1)
Current Store : [0x800071a8] : sw a3, 476(s1) -- Store: [0x800136b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800071d4]:fmul.h t6, t5, t4, dyn
	-[0x800071d8]:csrrs a3, fcsr, zero
	-[0x800071dc]:sw t6, 480(s1)
Current Store : [0x800071e0] : sw a3, 484(s1) -- Store: [0x800136b8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000720c]:fmul.h t6, t5, t4, dyn
	-[0x80007210]:csrrs a3, fcsr, zero
	-[0x80007214]:sw t6, 488(s1)
Current Store : [0x80007218] : sw a3, 492(s1) -- Store: [0x800136c0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007244]:fmul.h t6, t5, t4, dyn
	-[0x80007248]:csrrs a3, fcsr, zero
	-[0x8000724c]:sw t6, 496(s1)
Current Store : [0x80007250] : sw a3, 500(s1) -- Store: [0x800136c8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000727c]:fmul.h t6, t5, t4, dyn
	-[0x80007280]:csrrs a3, fcsr, zero
	-[0x80007284]:sw t6, 504(s1)
Current Store : [0x80007288] : sw a3, 508(s1) -- Store: [0x800136d0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800072b4]:fmul.h t6, t5, t4, dyn
	-[0x800072b8]:csrrs a3, fcsr, zero
	-[0x800072bc]:sw t6, 512(s1)
Current Store : [0x800072c0] : sw a3, 516(s1) -- Store: [0x800136d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800072ec]:fmul.h t6, t5, t4, dyn
	-[0x800072f0]:csrrs a3, fcsr, zero
	-[0x800072f4]:sw t6, 520(s1)
Current Store : [0x800072f8] : sw a3, 524(s1) -- Store: [0x800136e0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007324]:fmul.h t6, t5, t4, dyn
	-[0x80007328]:csrrs a3, fcsr, zero
	-[0x8000732c]:sw t6, 528(s1)
Current Store : [0x80007330] : sw a3, 532(s1) -- Store: [0x800136e8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000735c]:fmul.h t6, t5, t4, dyn
	-[0x80007360]:csrrs a3, fcsr, zero
	-[0x80007364]:sw t6, 536(s1)
Current Store : [0x80007368] : sw a3, 540(s1) -- Store: [0x800136f0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007394]:fmul.h t6, t5, t4, dyn
	-[0x80007398]:csrrs a3, fcsr, zero
	-[0x8000739c]:sw t6, 544(s1)
Current Store : [0x800073a0] : sw a3, 548(s1) -- Store: [0x800136f8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800073cc]:fmul.h t6, t5, t4, dyn
	-[0x800073d0]:csrrs a3, fcsr, zero
	-[0x800073d4]:sw t6, 552(s1)
Current Store : [0x800073d8] : sw a3, 556(s1) -- Store: [0x80013700]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007404]:fmul.h t6, t5, t4, dyn
	-[0x80007408]:csrrs a3, fcsr, zero
	-[0x8000740c]:sw t6, 560(s1)
Current Store : [0x80007410] : sw a3, 564(s1) -- Store: [0x80013708]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000743c]:fmul.h t6, t5, t4, dyn
	-[0x80007440]:csrrs a3, fcsr, zero
	-[0x80007444]:sw t6, 568(s1)
Current Store : [0x80007448] : sw a3, 572(s1) -- Store: [0x80013710]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007474]:fmul.h t6, t5, t4, dyn
	-[0x80007478]:csrrs a3, fcsr, zero
	-[0x8000747c]:sw t6, 576(s1)
Current Store : [0x80007480] : sw a3, 580(s1) -- Store: [0x80013718]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800074ac]:fmul.h t6, t5, t4, dyn
	-[0x800074b0]:csrrs a3, fcsr, zero
	-[0x800074b4]:sw t6, 584(s1)
Current Store : [0x800074b8] : sw a3, 588(s1) -- Store: [0x80013720]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800074e4]:fmul.h t6, t5, t4, dyn
	-[0x800074e8]:csrrs a3, fcsr, zero
	-[0x800074ec]:sw t6, 592(s1)
Current Store : [0x800074f0] : sw a3, 596(s1) -- Store: [0x80013728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000751c]:fmul.h t6, t5, t4, dyn
	-[0x80007520]:csrrs a3, fcsr, zero
	-[0x80007524]:sw t6, 600(s1)
Current Store : [0x80007528] : sw a3, 604(s1) -- Store: [0x80013730]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007554]:fmul.h t6, t5, t4, dyn
	-[0x80007558]:csrrs a3, fcsr, zero
	-[0x8000755c]:sw t6, 608(s1)
Current Store : [0x80007560] : sw a3, 612(s1) -- Store: [0x80013738]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000758c]:fmul.h t6, t5, t4, dyn
	-[0x80007590]:csrrs a3, fcsr, zero
	-[0x80007594]:sw t6, 616(s1)
Current Store : [0x80007598] : sw a3, 620(s1) -- Store: [0x80013740]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800075c4]:fmul.h t6, t5, t4, dyn
	-[0x800075c8]:csrrs a3, fcsr, zero
	-[0x800075cc]:sw t6, 624(s1)
Current Store : [0x800075d0] : sw a3, 628(s1) -- Store: [0x80013748]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800075fc]:fmul.h t6, t5, t4, dyn
	-[0x80007600]:csrrs a3, fcsr, zero
	-[0x80007604]:sw t6, 632(s1)
Current Store : [0x80007608] : sw a3, 636(s1) -- Store: [0x80013750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007634]:fmul.h t6, t5, t4, dyn
	-[0x80007638]:csrrs a3, fcsr, zero
	-[0x8000763c]:sw t6, 640(s1)
Current Store : [0x80007640] : sw a3, 644(s1) -- Store: [0x80013758]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000766c]:fmul.h t6, t5, t4, dyn
	-[0x80007670]:csrrs a3, fcsr, zero
	-[0x80007674]:sw t6, 648(s1)
Current Store : [0x80007678] : sw a3, 652(s1) -- Store: [0x80013760]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800076a4]:fmul.h t6, t5, t4, dyn
	-[0x800076a8]:csrrs a3, fcsr, zero
	-[0x800076ac]:sw t6, 656(s1)
Current Store : [0x800076b0] : sw a3, 660(s1) -- Store: [0x80013768]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800076dc]:fmul.h t6, t5, t4, dyn
	-[0x800076e0]:csrrs a3, fcsr, zero
	-[0x800076e4]:sw t6, 664(s1)
Current Store : [0x800076e8] : sw a3, 668(s1) -- Store: [0x80013770]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007714]:fmul.h t6, t5, t4, dyn
	-[0x80007718]:csrrs a3, fcsr, zero
	-[0x8000771c]:sw t6, 672(s1)
Current Store : [0x80007720] : sw a3, 676(s1) -- Store: [0x80013778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000774c]:fmul.h t6, t5, t4, dyn
	-[0x80007750]:csrrs a3, fcsr, zero
	-[0x80007754]:sw t6, 680(s1)
Current Store : [0x80007758] : sw a3, 684(s1) -- Store: [0x80013780]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007784]:fmul.h t6, t5, t4, dyn
	-[0x80007788]:csrrs a3, fcsr, zero
	-[0x8000778c]:sw t6, 688(s1)
Current Store : [0x80007790] : sw a3, 692(s1) -- Store: [0x80013788]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800077bc]:fmul.h t6, t5, t4, dyn
	-[0x800077c0]:csrrs a3, fcsr, zero
	-[0x800077c4]:sw t6, 696(s1)
Current Store : [0x800077c8] : sw a3, 700(s1) -- Store: [0x80013790]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800077f4]:fmul.h t6, t5, t4, dyn
	-[0x800077f8]:csrrs a3, fcsr, zero
	-[0x800077fc]:sw t6, 704(s1)
Current Store : [0x80007800] : sw a3, 708(s1) -- Store: [0x80013798]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000782c]:fmul.h t6, t5, t4, dyn
	-[0x80007830]:csrrs a3, fcsr, zero
	-[0x80007834]:sw t6, 712(s1)
Current Store : [0x80007838] : sw a3, 716(s1) -- Store: [0x800137a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007864]:fmul.h t6, t5, t4, dyn
	-[0x80007868]:csrrs a3, fcsr, zero
	-[0x8000786c]:sw t6, 720(s1)
Current Store : [0x80007870] : sw a3, 724(s1) -- Store: [0x800137a8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000789c]:fmul.h t6, t5, t4, dyn
	-[0x800078a0]:csrrs a3, fcsr, zero
	-[0x800078a4]:sw t6, 728(s1)
Current Store : [0x800078a8] : sw a3, 732(s1) -- Store: [0x800137b0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800078d4]:fmul.h t6, t5, t4, dyn
	-[0x800078d8]:csrrs a3, fcsr, zero
	-[0x800078dc]:sw t6, 736(s1)
Current Store : [0x800078e0] : sw a3, 740(s1) -- Store: [0x800137b8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000790c]:fmul.h t6, t5, t4, dyn
	-[0x80007910]:csrrs a3, fcsr, zero
	-[0x80007914]:sw t6, 744(s1)
Current Store : [0x80007918] : sw a3, 748(s1) -- Store: [0x800137c0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007944]:fmul.h t6, t5, t4, dyn
	-[0x80007948]:csrrs a3, fcsr, zero
	-[0x8000794c]:sw t6, 752(s1)
Current Store : [0x80007950] : sw a3, 756(s1) -- Store: [0x800137c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000797c]:fmul.h t6, t5, t4, dyn
	-[0x80007980]:csrrs a3, fcsr, zero
	-[0x80007984]:sw t6, 760(s1)
Current Store : [0x80007988] : sw a3, 764(s1) -- Store: [0x800137d0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800079b4]:fmul.h t6, t5, t4, dyn
	-[0x800079b8]:csrrs a3, fcsr, zero
	-[0x800079bc]:sw t6, 768(s1)
Current Store : [0x800079c0] : sw a3, 772(s1) -- Store: [0x800137d8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800079ec]:fmul.h t6, t5, t4, dyn
	-[0x800079f0]:csrrs a3, fcsr, zero
	-[0x800079f4]:sw t6, 776(s1)
Current Store : [0x800079f8] : sw a3, 780(s1) -- Store: [0x800137e0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a24]:fmul.h t6, t5, t4, dyn
	-[0x80007a28]:csrrs a3, fcsr, zero
	-[0x80007a2c]:sw t6, 784(s1)
Current Store : [0x80007a30] : sw a3, 788(s1) -- Store: [0x800137e8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a5c]:fmul.h t6, t5, t4, dyn
	-[0x80007a60]:csrrs a3, fcsr, zero
	-[0x80007a64]:sw t6, 792(s1)
Current Store : [0x80007a68] : sw a3, 796(s1) -- Store: [0x800137f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007a94]:fmul.h t6, t5, t4, dyn
	-[0x80007a98]:csrrs a3, fcsr, zero
	-[0x80007a9c]:sw t6, 800(s1)
Current Store : [0x80007aa0] : sw a3, 804(s1) -- Store: [0x800137f8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007acc]:fmul.h t6, t5, t4, dyn
	-[0x80007ad0]:csrrs a3, fcsr, zero
	-[0x80007ad4]:sw t6, 808(s1)
Current Store : [0x80007ad8] : sw a3, 812(s1) -- Store: [0x80013800]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b04]:fmul.h t6, t5, t4, dyn
	-[0x80007b08]:csrrs a3, fcsr, zero
	-[0x80007b0c]:sw t6, 816(s1)
Current Store : [0x80007b10] : sw a3, 820(s1) -- Store: [0x80013808]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b3c]:fmul.h t6, t5, t4, dyn
	-[0x80007b40]:csrrs a3, fcsr, zero
	-[0x80007b44]:sw t6, 824(s1)
Current Store : [0x80007b48] : sw a3, 828(s1) -- Store: [0x80013810]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007b74]:fmul.h t6, t5, t4, dyn
	-[0x80007b78]:csrrs a3, fcsr, zero
	-[0x80007b7c]:sw t6, 832(s1)
Current Store : [0x80007b80] : sw a3, 836(s1) -- Store: [0x80013818]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007bac]:fmul.h t6, t5, t4, dyn
	-[0x80007bb0]:csrrs a3, fcsr, zero
	-[0x80007bb4]:sw t6, 840(s1)
Current Store : [0x80007bb8] : sw a3, 844(s1) -- Store: [0x80013820]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007be4]:fmul.h t6, t5, t4, dyn
	-[0x80007be8]:csrrs a3, fcsr, zero
	-[0x80007bec]:sw t6, 848(s1)
Current Store : [0x80007bf0] : sw a3, 852(s1) -- Store: [0x80013828]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c1c]:fmul.h t6, t5, t4, dyn
	-[0x80007c20]:csrrs a3, fcsr, zero
	-[0x80007c24]:sw t6, 856(s1)
Current Store : [0x80007c28] : sw a3, 860(s1) -- Store: [0x80013830]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c54]:fmul.h t6, t5, t4, dyn
	-[0x80007c58]:csrrs a3, fcsr, zero
	-[0x80007c5c]:sw t6, 864(s1)
Current Store : [0x80007c60] : sw a3, 868(s1) -- Store: [0x80013838]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007c8c]:fmul.h t6, t5, t4, dyn
	-[0x80007c90]:csrrs a3, fcsr, zero
	-[0x80007c94]:sw t6, 872(s1)
Current Store : [0x80007c98] : sw a3, 876(s1) -- Store: [0x80013840]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007cc4]:fmul.h t6, t5, t4, dyn
	-[0x80007cc8]:csrrs a3, fcsr, zero
	-[0x80007ccc]:sw t6, 880(s1)
Current Store : [0x80007cd0] : sw a3, 884(s1) -- Store: [0x80013848]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007cfc]:fmul.h t6, t5, t4, dyn
	-[0x80007d00]:csrrs a3, fcsr, zero
	-[0x80007d04]:sw t6, 888(s1)
Current Store : [0x80007d08] : sw a3, 892(s1) -- Store: [0x80013850]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007d34]:fmul.h t6, t5, t4, dyn
	-[0x80007d38]:csrrs a3, fcsr, zero
	-[0x80007d3c]:sw t6, 896(s1)
Current Store : [0x80007d40] : sw a3, 900(s1) -- Store: [0x80013858]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007d6c]:fmul.h t6, t5, t4, dyn
	-[0x80007d70]:csrrs a3, fcsr, zero
	-[0x80007d74]:sw t6, 904(s1)
Current Store : [0x80007d78] : sw a3, 908(s1) -- Store: [0x80013860]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007da4]:fmul.h t6, t5, t4, dyn
	-[0x80007da8]:csrrs a3, fcsr, zero
	-[0x80007dac]:sw t6, 912(s1)
Current Store : [0x80007db0] : sw a3, 916(s1) -- Store: [0x80013868]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ddc]:fmul.h t6, t5, t4, dyn
	-[0x80007de0]:csrrs a3, fcsr, zero
	-[0x80007de4]:sw t6, 920(s1)
Current Store : [0x80007de8] : sw a3, 924(s1) -- Store: [0x80013870]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007e14]:fmul.h t6, t5, t4, dyn
	-[0x80007e18]:csrrs a3, fcsr, zero
	-[0x80007e1c]:sw t6, 928(s1)
Current Store : [0x80007e20] : sw a3, 932(s1) -- Store: [0x80013878]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007e4c]:fmul.h t6, t5, t4, dyn
	-[0x80007e50]:csrrs a3, fcsr, zero
	-[0x80007e54]:sw t6, 936(s1)
Current Store : [0x80007e58] : sw a3, 940(s1) -- Store: [0x80013880]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007e84]:fmul.h t6, t5, t4, dyn
	-[0x80007e88]:csrrs a3, fcsr, zero
	-[0x80007e8c]:sw t6, 944(s1)
Current Store : [0x80007e90] : sw a3, 948(s1) -- Store: [0x80013888]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ebc]:fmul.h t6, t5, t4, dyn
	-[0x80007ec0]:csrrs a3, fcsr, zero
	-[0x80007ec4]:sw t6, 952(s1)
Current Store : [0x80007ec8] : sw a3, 956(s1) -- Store: [0x80013890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007ef4]:fmul.h t6, t5, t4, dyn
	-[0x80007ef8]:csrrs a3, fcsr, zero
	-[0x80007efc]:sw t6, 960(s1)
Current Store : [0x80007f00] : sw a3, 964(s1) -- Store: [0x80013898]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f2c]:fmul.h t6, t5, t4, dyn
	-[0x80007f30]:csrrs a3, fcsr, zero
	-[0x80007f34]:sw t6, 968(s1)
Current Store : [0x80007f38] : sw a3, 972(s1) -- Store: [0x800138a0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f64]:fmul.h t6, t5, t4, dyn
	-[0x80007f68]:csrrs a3, fcsr, zero
	-[0x80007f6c]:sw t6, 976(s1)
Current Store : [0x80007f70] : sw a3, 980(s1) -- Store: [0x800138a8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007f9c]:fmul.h t6, t5, t4, dyn
	-[0x80007fa0]:csrrs a3, fcsr, zero
	-[0x80007fa4]:sw t6, 984(s1)
Current Store : [0x80007fa8] : sw a3, 988(s1) -- Store: [0x800138b0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80007fd4]:fmul.h t6, t5, t4, dyn
	-[0x80007fd8]:csrrs a3, fcsr, zero
	-[0x80007fdc]:sw t6, 992(s1)
Current Store : [0x80007fe0] : sw a3, 996(s1) -- Store: [0x800138b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000800c]:fmul.h t6, t5, t4, dyn
	-[0x80008010]:csrrs a3, fcsr, zero
	-[0x80008014]:sw t6, 1000(s1)
Current Store : [0x80008018] : sw a3, 1004(s1) -- Store: [0x800138c0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008044]:fmul.h t6, t5, t4, dyn
	-[0x80008048]:csrrs a3, fcsr, zero
	-[0x8000804c]:sw t6, 1008(s1)
Current Store : [0x80008050] : sw a3, 1012(s1) -- Store: [0x800138c8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000807c]:fmul.h t6, t5, t4, dyn
	-[0x80008080]:csrrs a3, fcsr, zero
	-[0x80008084]:sw t6, 1016(s1)
Current Store : [0x80008088] : sw a3, 1020(s1) -- Store: [0x800138d0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800080bc]:fmul.h t6, t5, t4, dyn
	-[0x800080c0]:csrrs a3, fcsr, zero
	-[0x800080c4]:sw t6, 0(s1)
Current Store : [0x800080c8] : sw a3, 4(s1) -- Store: [0x800138d8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800080f4]:fmul.h t6, t5, t4, dyn
	-[0x800080f8]:csrrs a3, fcsr, zero
	-[0x800080fc]:sw t6, 8(s1)
Current Store : [0x80008100] : sw a3, 12(s1) -- Store: [0x800138e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000812c]:fmul.h t6, t5, t4, dyn
	-[0x80008130]:csrrs a3, fcsr, zero
	-[0x80008134]:sw t6, 16(s1)
Current Store : [0x80008138] : sw a3, 20(s1) -- Store: [0x800138e8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008164]:fmul.h t6, t5, t4, dyn
	-[0x80008168]:csrrs a3, fcsr, zero
	-[0x8000816c]:sw t6, 24(s1)
Current Store : [0x80008170] : sw a3, 28(s1) -- Store: [0x800138f0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000819c]:fmul.h t6, t5, t4, dyn
	-[0x800081a0]:csrrs a3, fcsr, zero
	-[0x800081a4]:sw t6, 32(s1)
Current Store : [0x800081a8] : sw a3, 36(s1) -- Store: [0x800138f8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800081d4]:fmul.h t6, t5, t4, dyn
	-[0x800081d8]:csrrs a3, fcsr, zero
	-[0x800081dc]:sw t6, 40(s1)
Current Store : [0x800081e0] : sw a3, 44(s1) -- Store: [0x80013900]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000820c]:fmul.h t6, t5, t4, dyn
	-[0x80008210]:csrrs a3, fcsr, zero
	-[0x80008214]:sw t6, 48(s1)
Current Store : [0x80008218] : sw a3, 52(s1) -- Store: [0x80013908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008244]:fmul.h t6, t5, t4, dyn
	-[0x80008248]:csrrs a3, fcsr, zero
	-[0x8000824c]:sw t6, 56(s1)
Current Store : [0x80008250] : sw a3, 60(s1) -- Store: [0x80013910]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000827c]:fmul.h t6, t5, t4, dyn
	-[0x80008280]:csrrs a3, fcsr, zero
	-[0x80008284]:sw t6, 64(s1)
Current Store : [0x80008288] : sw a3, 68(s1) -- Store: [0x80013918]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800082b4]:fmul.h t6, t5, t4, dyn
	-[0x800082b8]:csrrs a3, fcsr, zero
	-[0x800082bc]:sw t6, 72(s1)
Current Store : [0x800082c0] : sw a3, 76(s1) -- Store: [0x80013920]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800082ec]:fmul.h t6, t5, t4, dyn
	-[0x800082f0]:csrrs a3, fcsr, zero
	-[0x800082f4]:sw t6, 80(s1)
Current Store : [0x800082f8] : sw a3, 84(s1) -- Store: [0x80013928]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008324]:fmul.h t6, t5, t4, dyn
	-[0x80008328]:csrrs a3, fcsr, zero
	-[0x8000832c]:sw t6, 88(s1)
Current Store : [0x80008330] : sw a3, 92(s1) -- Store: [0x80013930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000835c]:fmul.h t6, t5, t4, dyn
	-[0x80008360]:csrrs a3, fcsr, zero
	-[0x80008364]:sw t6, 96(s1)
Current Store : [0x80008368] : sw a3, 100(s1) -- Store: [0x80013938]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008394]:fmul.h t6, t5, t4, dyn
	-[0x80008398]:csrrs a3, fcsr, zero
	-[0x8000839c]:sw t6, 104(s1)
Current Store : [0x800083a0] : sw a3, 108(s1) -- Store: [0x80013940]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800083cc]:fmul.h t6, t5, t4, dyn
	-[0x800083d0]:csrrs a3, fcsr, zero
	-[0x800083d4]:sw t6, 112(s1)
Current Store : [0x800083d8] : sw a3, 116(s1) -- Store: [0x80013948]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008404]:fmul.h t6, t5, t4, dyn
	-[0x80008408]:csrrs a3, fcsr, zero
	-[0x8000840c]:sw t6, 120(s1)
Current Store : [0x80008410] : sw a3, 124(s1) -- Store: [0x80013950]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000843c]:fmul.h t6, t5, t4, dyn
	-[0x80008440]:csrrs a3, fcsr, zero
	-[0x80008444]:sw t6, 128(s1)
Current Store : [0x80008448] : sw a3, 132(s1) -- Store: [0x80013958]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008474]:fmul.h t6, t5, t4, dyn
	-[0x80008478]:csrrs a3, fcsr, zero
	-[0x8000847c]:sw t6, 136(s1)
Current Store : [0x80008480] : sw a3, 140(s1) -- Store: [0x80013960]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800084ac]:fmul.h t6, t5, t4, dyn
	-[0x800084b0]:csrrs a3, fcsr, zero
	-[0x800084b4]:sw t6, 144(s1)
Current Store : [0x800084b8] : sw a3, 148(s1) -- Store: [0x80013968]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800084e4]:fmul.h t6, t5, t4, dyn
	-[0x800084e8]:csrrs a3, fcsr, zero
	-[0x800084ec]:sw t6, 152(s1)
Current Store : [0x800084f0] : sw a3, 156(s1) -- Store: [0x80013970]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000851c]:fmul.h t6, t5, t4, dyn
	-[0x80008520]:csrrs a3, fcsr, zero
	-[0x80008524]:sw t6, 160(s1)
Current Store : [0x80008528] : sw a3, 164(s1) -- Store: [0x80013978]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008554]:fmul.h t6, t5, t4, dyn
	-[0x80008558]:csrrs a3, fcsr, zero
	-[0x8000855c]:sw t6, 168(s1)
Current Store : [0x80008560] : sw a3, 172(s1) -- Store: [0x80013980]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000858c]:fmul.h t6, t5, t4, dyn
	-[0x80008590]:csrrs a3, fcsr, zero
	-[0x80008594]:sw t6, 176(s1)
Current Store : [0x80008598] : sw a3, 180(s1) -- Store: [0x80013988]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800085c4]:fmul.h t6, t5, t4, dyn
	-[0x800085c8]:csrrs a3, fcsr, zero
	-[0x800085cc]:sw t6, 184(s1)
Current Store : [0x800085d0] : sw a3, 188(s1) -- Store: [0x80013990]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800085fc]:fmul.h t6, t5, t4, dyn
	-[0x80008600]:csrrs a3, fcsr, zero
	-[0x80008604]:sw t6, 192(s1)
Current Store : [0x80008608] : sw a3, 196(s1) -- Store: [0x80013998]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008634]:fmul.h t6, t5, t4, dyn
	-[0x80008638]:csrrs a3, fcsr, zero
	-[0x8000863c]:sw t6, 200(s1)
Current Store : [0x80008640] : sw a3, 204(s1) -- Store: [0x800139a0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000866c]:fmul.h t6, t5, t4, dyn
	-[0x80008670]:csrrs a3, fcsr, zero
	-[0x80008674]:sw t6, 208(s1)
Current Store : [0x80008678] : sw a3, 212(s1) -- Store: [0x800139a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800086a4]:fmul.h t6, t5, t4, dyn
	-[0x800086a8]:csrrs a3, fcsr, zero
	-[0x800086ac]:sw t6, 216(s1)
Current Store : [0x800086b0] : sw a3, 220(s1) -- Store: [0x800139b0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800086dc]:fmul.h t6, t5, t4, dyn
	-[0x800086e0]:csrrs a3, fcsr, zero
	-[0x800086e4]:sw t6, 224(s1)
Current Store : [0x800086e8] : sw a3, 228(s1) -- Store: [0x800139b8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008714]:fmul.h t6, t5, t4, dyn
	-[0x80008718]:csrrs a3, fcsr, zero
	-[0x8000871c]:sw t6, 232(s1)
Current Store : [0x80008720] : sw a3, 236(s1) -- Store: [0x800139c0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000874c]:fmul.h t6, t5, t4, dyn
	-[0x80008750]:csrrs a3, fcsr, zero
	-[0x80008754]:sw t6, 240(s1)
Current Store : [0x80008758] : sw a3, 244(s1) -- Store: [0x800139c8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008784]:fmul.h t6, t5, t4, dyn
	-[0x80008788]:csrrs a3, fcsr, zero
	-[0x8000878c]:sw t6, 248(s1)
Current Store : [0x80008790] : sw a3, 252(s1) -- Store: [0x800139d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800087bc]:fmul.h t6, t5, t4, dyn
	-[0x800087c0]:csrrs a3, fcsr, zero
	-[0x800087c4]:sw t6, 256(s1)
Current Store : [0x800087c8] : sw a3, 260(s1) -- Store: [0x800139d8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800087f4]:fmul.h t6, t5, t4, dyn
	-[0x800087f8]:csrrs a3, fcsr, zero
	-[0x800087fc]:sw t6, 264(s1)
Current Store : [0x80008800] : sw a3, 268(s1) -- Store: [0x800139e0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000882c]:fmul.h t6, t5, t4, dyn
	-[0x80008830]:csrrs a3, fcsr, zero
	-[0x80008834]:sw t6, 272(s1)
Current Store : [0x80008838] : sw a3, 276(s1) -- Store: [0x800139e8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008864]:fmul.h t6, t5, t4, dyn
	-[0x80008868]:csrrs a3, fcsr, zero
	-[0x8000886c]:sw t6, 280(s1)
Current Store : [0x80008870] : sw a3, 284(s1) -- Store: [0x800139f0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000889c]:fmul.h t6, t5, t4, dyn
	-[0x800088a0]:csrrs a3, fcsr, zero
	-[0x800088a4]:sw t6, 288(s1)
Current Store : [0x800088a8] : sw a3, 292(s1) -- Store: [0x800139f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800088d4]:fmul.h t6, t5, t4, dyn
	-[0x800088d8]:csrrs a3, fcsr, zero
	-[0x800088dc]:sw t6, 296(s1)
Current Store : [0x800088e0] : sw a3, 300(s1) -- Store: [0x80013a00]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000890c]:fmul.h t6, t5, t4, dyn
	-[0x80008910]:csrrs a3, fcsr, zero
	-[0x80008914]:sw t6, 304(s1)
Current Store : [0x80008918] : sw a3, 308(s1) -- Store: [0x80013a08]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008944]:fmul.h t6, t5, t4, dyn
	-[0x80008948]:csrrs a3, fcsr, zero
	-[0x8000894c]:sw t6, 312(s1)
Current Store : [0x80008950] : sw a3, 316(s1) -- Store: [0x80013a10]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000897c]:fmul.h t6, t5, t4, dyn
	-[0x80008980]:csrrs a3, fcsr, zero
	-[0x80008984]:sw t6, 320(s1)
Current Store : [0x80008988] : sw a3, 324(s1) -- Store: [0x80013a18]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800089b4]:fmul.h t6, t5, t4, dyn
	-[0x800089b8]:csrrs a3, fcsr, zero
	-[0x800089bc]:sw t6, 328(s1)
Current Store : [0x800089c0] : sw a3, 332(s1) -- Store: [0x80013a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800089ec]:fmul.h t6, t5, t4, dyn
	-[0x800089f0]:csrrs a3, fcsr, zero
	-[0x800089f4]:sw t6, 336(s1)
Current Store : [0x800089f8] : sw a3, 340(s1) -- Store: [0x80013a28]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008a24]:fmul.h t6, t5, t4, dyn
	-[0x80008a28]:csrrs a3, fcsr, zero
	-[0x80008a2c]:sw t6, 344(s1)
Current Store : [0x80008a30] : sw a3, 348(s1) -- Store: [0x80013a30]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008a5c]:fmul.h t6, t5, t4, dyn
	-[0x80008a60]:csrrs a3, fcsr, zero
	-[0x80008a64]:sw t6, 352(s1)
Current Store : [0x80008a68] : sw a3, 356(s1) -- Store: [0x80013a38]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008a94]:fmul.h t6, t5, t4, dyn
	-[0x80008a98]:csrrs a3, fcsr, zero
	-[0x80008a9c]:sw t6, 360(s1)
Current Store : [0x80008aa0] : sw a3, 364(s1) -- Store: [0x80013a40]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008acc]:fmul.h t6, t5, t4, dyn
	-[0x80008ad0]:csrrs a3, fcsr, zero
	-[0x80008ad4]:sw t6, 368(s1)
Current Store : [0x80008ad8] : sw a3, 372(s1) -- Store: [0x80013a48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008b04]:fmul.h t6, t5, t4, dyn
	-[0x80008b08]:csrrs a3, fcsr, zero
	-[0x80008b0c]:sw t6, 376(s1)
Current Store : [0x80008b10] : sw a3, 380(s1) -- Store: [0x80013a50]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008b3c]:fmul.h t6, t5, t4, dyn
	-[0x80008b40]:csrrs a3, fcsr, zero
	-[0x80008b44]:sw t6, 384(s1)
Current Store : [0x80008b48] : sw a3, 388(s1) -- Store: [0x80013a58]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008b74]:fmul.h t6, t5, t4, dyn
	-[0x80008b78]:csrrs a3, fcsr, zero
	-[0x80008b7c]:sw t6, 392(s1)
Current Store : [0x80008b80] : sw a3, 396(s1) -- Store: [0x80013a60]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008bac]:fmul.h t6, t5, t4, dyn
	-[0x80008bb0]:csrrs a3, fcsr, zero
	-[0x80008bb4]:sw t6, 400(s1)
Current Store : [0x80008bb8] : sw a3, 404(s1) -- Store: [0x80013a68]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008be4]:fmul.h t6, t5, t4, dyn
	-[0x80008be8]:csrrs a3, fcsr, zero
	-[0x80008bec]:sw t6, 408(s1)
Current Store : [0x80008bf0] : sw a3, 412(s1) -- Store: [0x80013a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008c1c]:fmul.h t6, t5, t4, dyn
	-[0x80008c20]:csrrs a3, fcsr, zero
	-[0x80008c24]:sw t6, 416(s1)
Current Store : [0x80008c28] : sw a3, 420(s1) -- Store: [0x80013a78]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008c54]:fmul.h t6, t5, t4, dyn
	-[0x80008c58]:csrrs a3, fcsr, zero
	-[0x80008c5c]:sw t6, 424(s1)
Current Store : [0x80008c60] : sw a3, 428(s1) -- Store: [0x80013a80]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008c8c]:fmul.h t6, t5, t4, dyn
	-[0x80008c90]:csrrs a3, fcsr, zero
	-[0x80008c94]:sw t6, 432(s1)
Current Store : [0x80008c98] : sw a3, 436(s1) -- Store: [0x80013a88]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008cc4]:fmul.h t6, t5, t4, dyn
	-[0x80008cc8]:csrrs a3, fcsr, zero
	-[0x80008ccc]:sw t6, 440(s1)
Current Store : [0x80008cd0] : sw a3, 444(s1) -- Store: [0x80013a90]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008cfc]:fmul.h t6, t5, t4, dyn
	-[0x80008d00]:csrrs a3, fcsr, zero
	-[0x80008d04]:sw t6, 448(s1)
Current Store : [0x80008d08] : sw a3, 452(s1) -- Store: [0x80013a98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008d34]:fmul.h t6, t5, t4, dyn
	-[0x80008d38]:csrrs a3, fcsr, zero
	-[0x80008d3c]:sw t6, 456(s1)
Current Store : [0x80008d40] : sw a3, 460(s1) -- Store: [0x80013aa0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008d6c]:fmul.h t6, t5, t4, dyn
	-[0x80008d70]:csrrs a3, fcsr, zero
	-[0x80008d74]:sw t6, 464(s1)
Current Store : [0x80008d78] : sw a3, 468(s1) -- Store: [0x80013aa8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008da4]:fmul.h t6, t5, t4, dyn
	-[0x80008da8]:csrrs a3, fcsr, zero
	-[0x80008dac]:sw t6, 472(s1)
Current Store : [0x80008db0] : sw a3, 476(s1) -- Store: [0x80013ab0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008ddc]:fmul.h t6, t5, t4, dyn
	-[0x80008de0]:csrrs a3, fcsr, zero
	-[0x80008de4]:sw t6, 480(s1)
Current Store : [0x80008de8] : sw a3, 484(s1) -- Store: [0x80013ab8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008e14]:fmul.h t6, t5, t4, dyn
	-[0x80008e18]:csrrs a3, fcsr, zero
	-[0x80008e1c]:sw t6, 488(s1)
Current Store : [0x80008e20] : sw a3, 492(s1) -- Store: [0x80013ac0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008e4c]:fmul.h t6, t5, t4, dyn
	-[0x80008e50]:csrrs a3, fcsr, zero
	-[0x80008e54]:sw t6, 496(s1)
Current Store : [0x80008e58] : sw a3, 500(s1) -- Store: [0x80013ac8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008e84]:fmul.h t6, t5, t4, dyn
	-[0x80008e88]:csrrs a3, fcsr, zero
	-[0x80008e8c]:sw t6, 504(s1)
Current Store : [0x80008e90] : sw a3, 508(s1) -- Store: [0x80013ad0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008ebc]:fmul.h t6, t5, t4, dyn
	-[0x80008ec0]:csrrs a3, fcsr, zero
	-[0x80008ec4]:sw t6, 512(s1)
Current Store : [0x80008ec8] : sw a3, 516(s1) -- Store: [0x80013ad8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008ef4]:fmul.h t6, t5, t4, dyn
	-[0x80008ef8]:csrrs a3, fcsr, zero
	-[0x80008efc]:sw t6, 520(s1)
Current Store : [0x80008f00] : sw a3, 524(s1) -- Store: [0x80013ae0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008f2c]:fmul.h t6, t5, t4, dyn
	-[0x80008f30]:csrrs a3, fcsr, zero
	-[0x80008f34]:sw t6, 528(s1)
Current Store : [0x80008f38] : sw a3, 532(s1) -- Store: [0x80013ae8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008f64]:fmul.h t6, t5, t4, dyn
	-[0x80008f68]:csrrs a3, fcsr, zero
	-[0x80008f6c]:sw t6, 536(s1)
Current Store : [0x80008f70] : sw a3, 540(s1) -- Store: [0x80013af0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008f9c]:fmul.h t6, t5, t4, dyn
	-[0x80008fa0]:csrrs a3, fcsr, zero
	-[0x80008fa4]:sw t6, 544(s1)
Current Store : [0x80008fa8] : sw a3, 548(s1) -- Store: [0x80013af8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80008fd4]:fmul.h t6, t5, t4, dyn
	-[0x80008fd8]:csrrs a3, fcsr, zero
	-[0x80008fdc]:sw t6, 552(s1)
Current Store : [0x80008fe0] : sw a3, 556(s1) -- Store: [0x80013b00]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000900c]:fmul.h t6, t5, t4, dyn
	-[0x80009010]:csrrs a3, fcsr, zero
	-[0x80009014]:sw t6, 560(s1)
Current Store : [0x80009018] : sw a3, 564(s1) -- Store: [0x80013b08]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009044]:fmul.h t6, t5, t4, dyn
	-[0x80009048]:csrrs a3, fcsr, zero
	-[0x8000904c]:sw t6, 568(s1)
Current Store : [0x80009050] : sw a3, 572(s1) -- Store: [0x80013b10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000907c]:fmul.h t6, t5, t4, dyn
	-[0x80009080]:csrrs a3, fcsr, zero
	-[0x80009084]:sw t6, 576(s1)
Current Store : [0x80009088] : sw a3, 580(s1) -- Store: [0x80013b18]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800090b4]:fmul.h t6, t5, t4, dyn
	-[0x800090b8]:csrrs a3, fcsr, zero
	-[0x800090bc]:sw t6, 584(s1)
Current Store : [0x800090c0] : sw a3, 588(s1) -- Store: [0x80013b20]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800090ec]:fmul.h t6, t5, t4, dyn
	-[0x800090f0]:csrrs a3, fcsr, zero
	-[0x800090f4]:sw t6, 592(s1)
Current Store : [0x800090f8] : sw a3, 596(s1) -- Store: [0x80013b28]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009124]:fmul.h t6, t5, t4, dyn
	-[0x80009128]:csrrs a3, fcsr, zero
	-[0x8000912c]:sw t6, 600(s1)
Current Store : [0x80009130] : sw a3, 604(s1) -- Store: [0x80013b30]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000915c]:fmul.h t6, t5, t4, dyn
	-[0x80009160]:csrrs a3, fcsr, zero
	-[0x80009164]:sw t6, 608(s1)
Current Store : [0x80009168] : sw a3, 612(s1) -- Store: [0x80013b38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009194]:fmul.h t6, t5, t4, dyn
	-[0x80009198]:csrrs a3, fcsr, zero
	-[0x8000919c]:sw t6, 616(s1)
Current Store : [0x800091a0] : sw a3, 620(s1) -- Store: [0x80013b40]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800091cc]:fmul.h t6, t5, t4, dyn
	-[0x800091d0]:csrrs a3, fcsr, zero
	-[0x800091d4]:sw t6, 624(s1)
Current Store : [0x800091d8] : sw a3, 628(s1) -- Store: [0x80013b48]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009204]:fmul.h t6, t5, t4, dyn
	-[0x80009208]:csrrs a3, fcsr, zero
	-[0x8000920c]:sw t6, 632(s1)
Current Store : [0x80009210] : sw a3, 636(s1) -- Store: [0x80013b50]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000923c]:fmul.h t6, t5, t4, dyn
	-[0x80009240]:csrrs a3, fcsr, zero
	-[0x80009244]:sw t6, 640(s1)
Current Store : [0x80009248] : sw a3, 644(s1) -- Store: [0x80013b58]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009274]:fmul.h t6, t5, t4, dyn
	-[0x80009278]:csrrs a3, fcsr, zero
	-[0x8000927c]:sw t6, 648(s1)
Current Store : [0x80009280] : sw a3, 652(s1) -- Store: [0x80013b60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800092ac]:fmul.h t6, t5, t4, dyn
	-[0x800092b0]:csrrs a3, fcsr, zero
	-[0x800092b4]:sw t6, 656(s1)
Current Store : [0x800092b8] : sw a3, 660(s1) -- Store: [0x80013b68]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800092e4]:fmul.h t6, t5, t4, dyn
	-[0x800092e8]:csrrs a3, fcsr, zero
	-[0x800092ec]:sw t6, 664(s1)
Current Store : [0x800092f0] : sw a3, 668(s1) -- Store: [0x80013b70]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000931c]:fmul.h t6, t5, t4, dyn
	-[0x80009320]:csrrs a3, fcsr, zero
	-[0x80009324]:sw t6, 672(s1)
Current Store : [0x80009328] : sw a3, 676(s1) -- Store: [0x80013b78]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009354]:fmul.h t6, t5, t4, dyn
	-[0x80009358]:csrrs a3, fcsr, zero
	-[0x8000935c]:sw t6, 680(s1)
Current Store : [0x80009360] : sw a3, 684(s1) -- Store: [0x80013b80]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000938c]:fmul.h t6, t5, t4, dyn
	-[0x80009390]:csrrs a3, fcsr, zero
	-[0x80009394]:sw t6, 688(s1)
Current Store : [0x80009398] : sw a3, 692(s1) -- Store: [0x80013b88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800093c4]:fmul.h t6, t5, t4, dyn
	-[0x800093c8]:csrrs a3, fcsr, zero
	-[0x800093cc]:sw t6, 696(s1)
Current Store : [0x800093d0] : sw a3, 700(s1) -- Store: [0x80013b90]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800093fc]:fmul.h t6, t5, t4, dyn
	-[0x80009400]:csrrs a3, fcsr, zero
	-[0x80009404]:sw t6, 704(s1)
Current Store : [0x80009408] : sw a3, 708(s1) -- Store: [0x80013b98]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009434]:fmul.h t6, t5, t4, dyn
	-[0x80009438]:csrrs a3, fcsr, zero
	-[0x8000943c]:sw t6, 712(s1)
Current Store : [0x80009440] : sw a3, 716(s1) -- Store: [0x80013ba0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000946c]:fmul.h t6, t5, t4, dyn
	-[0x80009470]:csrrs a3, fcsr, zero
	-[0x80009474]:sw t6, 720(s1)
Current Store : [0x80009478] : sw a3, 724(s1) -- Store: [0x80013ba8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800094a4]:fmul.h t6, t5, t4, dyn
	-[0x800094a8]:csrrs a3, fcsr, zero
	-[0x800094ac]:sw t6, 728(s1)
Current Store : [0x800094b0] : sw a3, 732(s1) -- Store: [0x80013bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800094dc]:fmul.h t6, t5, t4, dyn
	-[0x800094e0]:csrrs a3, fcsr, zero
	-[0x800094e4]:sw t6, 736(s1)
Current Store : [0x800094e8] : sw a3, 740(s1) -- Store: [0x80013bb8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009514]:fmul.h t6, t5, t4, dyn
	-[0x80009518]:csrrs a3, fcsr, zero
	-[0x8000951c]:sw t6, 744(s1)
Current Store : [0x80009520] : sw a3, 748(s1) -- Store: [0x80013bc0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000954c]:fmul.h t6, t5, t4, dyn
	-[0x80009550]:csrrs a3, fcsr, zero
	-[0x80009554]:sw t6, 752(s1)
Current Store : [0x80009558] : sw a3, 756(s1) -- Store: [0x80013bc8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009584]:fmul.h t6, t5, t4, dyn
	-[0x80009588]:csrrs a3, fcsr, zero
	-[0x8000958c]:sw t6, 760(s1)
Current Store : [0x80009590] : sw a3, 764(s1) -- Store: [0x80013bd0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800095bc]:fmul.h t6, t5, t4, dyn
	-[0x800095c0]:csrrs a3, fcsr, zero
	-[0x800095c4]:sw t6, 768(s1)
Current Store : [0x800095c8] : sw a3, 772(s1) -- Store: [0x80013bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800095f4]:fmul.h t6, t5, t4, dyn
	-[0x800095f8]:csrrs a3, fcsr, zero
	-[0x800095fc]:sw t6, 776(s1)
Current Store : [0x80009600] : sw a3, 780(s1) -- Store: [0x80013be0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000962c]:fmul.h t6, t5, t4, dyn
	-[0x80009630]:csrrs a3, fcsr, zero
	-[0x80009634]:sw t6, 784(s1)
Current Store : [0x80009638] : sw a3, 788(s1) -- Store: [0x80013be8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009664]:fmul.h t6, t5, t4, dyn
	-[0x80009668]:csrrs a3, fcsr, zero
	-[0x8000966c]:sw t6, 792(s1)
Current Store : [0x80009670] : sw a3, 796(s1) -- Store: [0x80013bf0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000969c]:fmul.h t6, t5, t4, dyn
	-[0x800096a0]:csrrs a3, fcsr, zero
	-[0x800096a4]:sw t6, 800(s1)
Current Store : [0x800096a8] : sw a3, 804(s1) -- Store: [0x80013bf8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800096d4]:fmul.h t6, t5, t4, dyn
	-[0x800096d8]:csrrs a3, fcsr, zero
	-[0x800096dc]:sw t6, 808(s1)
Current Store : [0x800096e0] : sw a3, 812(s1) -- Store: [0x80013c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000970c]:fmul.h t6, t5, t4, dyn
	-[0x80009710]:csrrs a3, fcsr, zero
	-[0x80009714]:sw t6, 816(s1)
Current Store : [0x80009718] : sw a3, 820(s1) -- Store: [0x80013c08]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009744]:fmul.h t6, t5, t4, dyn
	-[0x80009748]:csrrs a3, fcsr, zero
	-[0x8000974c]:sw t6, 824(s1)
Current Store : [0x80009750] : sw a3, 828(s1) -- Store: [0x80013c10]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000977c]:fmul.h t6, t5, t4, dyn
	-[0x80009780]:csrrs a3, fcsr, zero
	-[0x80009784]:sw t6, 832(s1)
Current Store : [0x80009788] : sw a3, 836(s1) -- Store: [0x80013c18]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800097b4]:fmul.h t6, t5, t4, dyn
	-[0x800097b8]:csrrs a3, fcsr, zero
	-[0x800097bc]:sw t6, 840(s1)
Current Store : [0x800097c0] : sw a3, 844(s1) -- Store: [0x80013c20]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800097ec]:fmul.h t6, t5, t4, dyn
	-[0x800097f0]:csrrs a3, fcsr, zero
	-[0x800097f4]:sw t6, 848(s1)
Current Store : [0x800097f8] : sw a3, 852(s1) -- Store: [0x80013c28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009824]:fmul.h t6, t5, t4, dyn
	-[0x80009828]:csrrs a3, fcsr, zero
	-[0x8000982c]:sw t6, 856(s1)
Current Store : [0x80009830] : sw a3, 860(s1) -- Store: [0x80013c30]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000985c]:fmul.h t6, t5, t4, dyn
	-[0x80009860]:csrrs a3, fcsr, zero
	-[0x80009864]:sw t6, 864(s1)
Current Store : [0x80009868] : sw a3, 868(s1) -- Store: [0x80013c38]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009894]:fmul.h t6, t5, t4, dyn
	-[0x80009898]:csrrs a3, fcsr, zero
	-[0x8000989c]:sw t6, 872(s1)
Current Store : [0x800098a0] : sw a3, 876(s1) -- Store: [0x80013c40]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800098cc]:fmul.h t6, t5, t4, dyn
	-[0x800098d0]:csrrs a3, fcsr, zero
	-[0x800098d4]:sw t6, 880(s1)
Current Store : [0x800098d8] : sw a3, 884(s1) -- Store: [0x80013c48]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009904]:fmul.h t6, t5, t4, dyn
	-[0x80009908]:csrrs a3, fcsr, zero
	-[0x8000990c]:sw t6, 888(s1)
Current Store : [0x80009910] : sw a3, 892(s1) -- Store: [0x80013c50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000993c]:fmul.h t6, t5, t4, dyn
	-[0x80009940]:csrrs a3, fcsr, zero
	-[0x80009944]:sw t6, 896(s1)
Current Store : [0x80009948] : sw a3, 900(s1) -- Store: [0x80013c58]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009974]:fmul.h t6, t5, t4, dyn
	-[0x80009978]:csrrs a3, fcsr, zero
	-[0x8000997c]:sw t6, 904(s1)
Current Store : [0x80009980] : sw a3, 908(s1) -- Store: [0x80013c60]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800099ac]:fmul.h t6, t5, t4, dyn
	-[0x800099b0]:csrrs a3, fcsr, zero
	-[0x800099b4]:sw t6, 912(s1)
Current Store : [0x800099b8] : sw a3, 916(s1) -- Store: [0x80013c68]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800099e4]:fmul.h t6, t5, t4, dyn
	-[0x800099e8]:csrrs a3, fcsr, zero
	-[0x800099ec]:sw t6, 920(s1)
Current Store : [0x800099f0] : sw a3, 924(s1) -- Store: [0x80013c70]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009a1c]:fmul.h t6, t5, t4, dyn
	-[0x80009a20]:csrrs a3, fcsr, zero
	-[0x80009a24]:sw t6, 928(s1)
Current Store : [0x80009a28] : sw a3, 932(s1) -- Store: [0x80013c78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009a54]:fmul.h t6, t5, t4, dyn
	-[0x80009a58]:csrrs a3, fcsr, zero
	-[0x80009a5c]:sw t6, 936(s1)
Current Store : [0x80009a60] : sw a3, 940(s1) -- Store: [0x80013c80]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009a8c]:fmul.h t6, t5, t4, dyn
	-[0x80009a90]:csrrs a3, fcsr, zero
	-[0x80009a94]:sw t6, 944(s1)
Current Store : [0x80009a98] : sw a3, 948(s1) -- Store: [0x80013c88]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009ac4]:fmul.h t6, t5, t4, dyn
	-[0x80009ac8]:csrrs a3, fcsr, zero
	-[0x80009acc]:sw t6, 952(s1)
Current Store : [0x80009ad0] : sw a3, 956(s1) -- Store: [0x80013c90]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009afc]:fmul.h t6, t5, t4, dyn
	-[0x80009b00]:csrrs a3, fcsr, zero
	-[0x80009b04]:sw t6, 960(s1)
Current Store : [0x80009b08] : sw a3, 964(s1) -- Store: [0x80013c98]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009b34]:fmul.h t6, t5, t4, dyn
	-[0x80009b38]:csrrs a3, fcsr, zero
	-[0x80009b3c]:sw t6, 968(s1)
Current Store : [0x80009b40] : sw a3, 972(s1) -- Store: [0x80013ca0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009b6c]:fmul.h t6, t5, t4, dyn
	-[0x80009b70]:csrrs a3, fcsr, zero
	-[0x80009b74]:sw t6, 976(s1)
Current Store : [0x80009b78] : sw a3, 980(s1) -- Store: [0x80013ca8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009ba4]:fmul.h t6, t5, t4, dyn
	-[0x80009ba8]:csrrs a3, fcsr, zero
	-[0x80009bac]:sw t6, 984(s1)
Current Store : [0x80009bb0] : sw a3, 988(s1) -- Store: [0x80013cb0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009bdc]:fmul.h t6, t5, t4, dyn
	-[0x80009be0]:csrrs a3, fcsr, zero
	-[0x80009be4]:sw t6, 992(s1)
Current Store : [0x80009be8] : sw a3, 996(s1) -- Store: [0x80013cb8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009c1c]:fmul.h t6, t5, t4, dyn
	-[0x80009c20]:csrrs a3, fcsr, zero
	-[0x80009c24]:sw t6, 1000(s1)
Current Store : [0x80009c28] : sw a3, 1004(s1) -- Store: [0x80013cc0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009c5c]:fmul.h t6, t5, t4, dyn
	-[0x80009c60]:csrrs a3, fcsr, zero
	-[0x80009c64]:sw t6, 1008(s1)
Current Store : [0x80009c68] : sw a3, 1012(s1) -- Store: [0x80013cc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009c9c]:fmul.h t6, t5, t4, dyn
	-[0x80009ca0]:csrrs a3, fcsr, zero
	-[0x80009ca4]:sw t6, 1016(s1)
Current Store : [0x80009ca8] : sw a3, 1020(s1) -- Store: [0x80013cd0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009ce4]:fmul.h t6, t5, t4, dyn
	-[0x80009ce8]:csrrs a3, fcsr, zero
	-[0x80009cec]:sw t6, 0(s1)
Current Store : [0x80009cf0] : sw a3, 4(s1) -- Store: [0x80013cd8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009d24]:fmul.h t6, t5, t4, dyn
	-[0x80009d28]:csrrs a3, fcsr, zero
	-[0x80009d2c]:sw t6, 8(s1)
Current Store : [0x80009d30] : sw a3, 12(s1) -- Store: [0x80013ce0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009d64]:fmul.h t6, t5, t4, dyn
	-[0x80009d68]:csrrs a3, fcsr, zero
	-[0x80009d6c]:sw t6, 16(s1)
Current Store : [0x80009d70] : sw a3, 20(s1) -- Store: [0x80013ce8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009da4]:fmul.h t6, t5, t4, dyn
	-[0x80009da8]:csrrs a3, fcsr, zero
	-[0x80009dac]:sw t6, 24(s1)
Current Store : [0x80009db0] : sw a3, 28(s1) -- Store: [0x80013cf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009de4]:fmul.h t6, t5, t4, dyn
	-[0x80009de8]:csrrs a3, fcsr, zero
	-[0x80009dec]:sw t6, 32(s1)
Current Store : [0x80009df0] : sw a3, 36(s1) -- Store: [0x80013cf8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009e24]:fmul.h t6, t5, t4, dyn
	-[0x80009e28]:csrrs a3, fcsr, zero
	-[0x80009e2c]:sw t6, 40(s1)
Current Store : [0x80009e30] : sw a3, 44(s1) -- Store: [0x80013d00]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009e64]:fmul.h t6, t5, t4, dyn
	-[0x80009e68]:csrrs a3, fcsr, zero
	-[0x80009e6c]:sw t6, 48(s1)
Current Store : [0x80009e70] : sw a3, 52(s1) -- Store: [0x80013d08]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009ea4]:fmul.h t6, t5, t4, dyn
	-[0x80009ea8]:csrrs a3, fcsr, zero
	-[0x80009eac]:sw t6, 56(s1)
Current Store : [0x80009eb0] : sw a3, 60(s1) -- Store: [0x80013d10]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009ee4]:fmul.h t6, t5, t4, dyn
	-[0x80009ee8]:csrrs a3, fcsr, zero
	-[0x80009eec]:sw t6, 64(s1)
Current Store : [0x80009ef0] : sw a3, 68(s1) -- Store: [0x80013d18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009f24]:fmul.h t6, t5, t4, dyn
	-[0x80009f28]:csrrs a3, fcsr, zero
	-[0x80009f2c]:sw t6, 72(s1)
Current Store : [0x80009f30] : sw a3, 76(s1) -- Store: [0x80013d20]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009f64]:fmul.h t6, t5, t4, dyn
	-[0x80009f68]:csrrs a3, fcsr, zero
	-[0x80009f6c]:sw t6, 80(s1)
Current Store : [0x80009f70] : sw a3, 84(s1) -- Store: [0x80013d28]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009fa4]:fmul.h t6, t5, t4, dyn
	-[0x80009fa8]:csrrs a3, fcsr, zero
	-[0x80009fac]:sw t6, 88(s1)
Current Store : [0x80009fb0] : sw a3, 92(s1) -- Store: [0x80013d30]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80009fe4]:fmul.h t6, t5, t4, dyn
	-[0x80009fe8]:csrrs a3, fcsr, zero
	-[0x80009fec]:sw t6, 96(s1)
Current Store : [0x80009ff0] : sw a3, 100(s1) -- Store: [0x80013d38]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a024]:fmul.h t6, t5, t4, dyn
	-[0x8000a028]:csrrs a3, fcsr, zero
	-[0x8000a02c]:sw t6, 104(s1)
Current Store : [0x8000a030] : sw a3, 108(s1) -- Store: [0x80013d40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a064]:fmul.h t6, t5, t4, dyn
	-[0x8000a068]:csrrs a3, fcsr, zero
	-[0x8000a06c]:sw t6, 112(s1)
Current Store : [0x8000a070] : sw a3, 116(s1) -- Store: [0x80013d48]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a0a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a0a8]:csrrs a3, fcsr, zero
	-[0x8000a0ac]:sw t6, 120(s1)
Current Store : [0x8000a0b0] : sw a3, 124(s1) -- Store: [0x80013d50]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a0e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a0e8]:csrrs a3, fcsr, zero
	-[0x8000a0ec]:sw t6, 128(s1)
Current Store : [0x8000a0f0] : sw a3, 132(s1) -- Store: [0x80013d58]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a124]:fmul.h t6, t5, t4, dyn
	-[0x8000a128]:csrrs a3, fcsr, zero
	-[0x8000a12c]:sw t6, 136(s1)
Current Store : [0x8000a130] : sw a3, 140(s1) -- Store: [0x80013d60]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a164]:fmul.h t6, t5, t4, dyn
	-[0x8000a168]:csrrs a3, fcsr, zero
	-[0x8000a16c]:sw t6, 144(s1)
Current Store : [0x8000a170] : sw a3, 148(s1) -- Store: [0x80013d68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a1a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a1a8]:csrrs a3, fcsr, zero
	-[0x8000a1ac]:sw t6, 152(s1)
Current Store : [0x8000a1b0] : sw a3, 156(s1) -- Store: [0x80013d70]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a1e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a1e8]:csrrs a3, fcsr, zero
	-[0x8000a1ec]:sw t6, 160(s1)
Current Store : [0x8000a1f0] : sw a3, 164(s1) -- Store: [0x80013d78]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a224]:fmul.h t6, t5, t4, dyn
	-[0x8000a228]:csrrs a3, fcsr, zero
	-[0x8000a22c]:sw t6, 168(s1)
Current Store : [0x8000a230] : sw a3, 172(s1) -- Store: [0x80013d80]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a264]:fmul.h t6, t5, t4, dyn
	-[0x8000a268]:csrrs a3, fcsr, zero
	-[0x8000a26c]:sw t6, 176(s1)
Current Store : [0x8000a270] : sw a3, 180(s1) -- Store: [0x80013d88]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a2a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a2a8]:csrrs a3, fcsr, zero
	-[0x8000a2ac]:sw t6, 184(s1)
Current Store : [0x8000a2b0] : sw a3, 188(s1) -- Store: [0x80013d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a2e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a2e8]:csrrs a3, fcsr, zero
	-[0x8000a2ec]:sw t6, 192(s1)
Current Store : [0x8000a2f0] : sw a3, 196(s1) -- Store: [0x80013d98]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a324]:fmul.h t6, t5, t4, dyn
	-[0x8000a328]:csrrs a3, fcsr, zero
	-[0x8000a32c]:sw t6, 200(s1)
Current Store : [0x8000a330] : sw a3, 204(s1) -- Store: [0x80013da0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a364]:fmul.h t6, t5, t4, dyn
	-[0x8000a368]:csrrs a3, fcsr, zero
	-[0x8000a36c]:sw t6, 208(s1)
Current Store : [0x8000a370] : sw a3, 212(s1) -- Store: [0x80013da8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a3a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a3a8]:csrrs a3, fcsr, zero
	-[0x8000a3ac]:sw t6, 216(s1)
Current Store : [0x8000a3b0] : sw a3, 220(s1) -- Store: [0x80013db0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a3e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a3e8]:csrrs a3, fcsr, zero
	-[0x8000a3ec]:sw t6, 224(s1)
Current Store : [0x8000a3f0] : sw a3, 228(s1) -- Store: [0x80013db8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a424]:fmul.h t6, t5, t4, dyn
	-[0x8000a428]:csrrs a3, fcsr, zero
	-[0x8000a42c]:sw t6, 232(s1)
Current Store : [0x8000a430] : sw a3, 236(s1) -- Store: [0x80013dc0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a464]:fmul.h t6, t5, t4, dyn
	-[0x8000a468]:csrrs a3, fcsr, zero
	-[0x8000a46c]:sw t6, 240(s1)
Current Store : [0x8000a470] : sw a3, 244(s1) -- Store: [0x80013dc8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a4a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a4a8]:csrrs a3, fcsr, zero
	-[0x8000a4ac]:sw t6, 248(s1)
Current Store : [0x8000a4b0] : sw a3, 252(s1) -- Store: [0x80013dd0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a4e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a4e8]:csrrs a3, fcsr, zero
	-[0x8000a4ec]:sw t6, 256(s1)
Current Store : [0x8000a4f0] : sw a3, 260(s1) -- Store: [0x80013dd8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a524]:fmul.h t6, t5, t4, dyn
	-[0x8000a528]:csrrs a3, fcsr, zero
	-[0x8000a52c]:sw t6, 264(s1)
Current Store : [0x8000a530] : sw a3, 268(s1) -- Store: [0x80013de0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a564]:fmul.h t6, t5, t4, dyn
	-[0x8000a568]:csrrs a3, fcsr, zero
	-[0x8000a56c]:sw t6, 272(s1)
Current Store : [0x8000a570] : sw a3, 276(s1) -- Store: [0x80013de8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a5a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a5a8]:csrrs a3, fcsr, zero
	-[0x8000a5ac]:sw t6, 280(s1)
Current Store : [0x8000a5b0] : sw a3, 284(s1) -- Store: [0x80013df0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a5e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a5e8]:csrrs a3, fcsr, zero
	-[0x8000a5ec]:sw t6, 288(s1)
Current Store : [0x8000a5f0] : sw a3, 292(s1) -- Store: [0x80013df8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a624]:fmul.h t6, t5, t4, dyn
	-[0x8000a628]:csrrs a3, fcsr, zero
	-[0x8000a62c]:sw t6, 296(s1)
Current Store : [0x8000a630] : sw a3, 300(s1) -- Store: [0x80013e00]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a664]:fmul.h t6, t5, t4, dyn
	-[0x8000a668]:csrrs a3, fcsr, zero
	-[0x8000a66c]:sw t6, 304(s1)
Current Store : [0x8000a670] : sw a3, 308(s1) -- Store: [0x80013e08]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a6a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a6a8]:csrrs a3, fcsr, zero
	-[0x8000a6ac]:sw t6, 312(s1)
Current Store : [0x8000a6b0] : sw a3, 316(s1) -- Store: [0x80013e10]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a6e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a6e8]:csrrs a3, fcsr, zero
	-[0x8000a6ec]:sw t6, 320(s1)
Current Store : [0x8000a6f0] : sw a3, 324(s1) -- Store: [0x80013e18]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a724]:fmul.h t6, t5, t4, dyn
	-[0x8000a728]:csrrs a3, fcsr, zero
	-[0x8000a72c]:sw t6, 328(s1)
Current Store : [0x8000a730] : sw a3, 332(s1) -- Store: [0x80013e20]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a764]:fmul.h t6, t5, t4, dyn
	-[0x8000a768]:csrrs a3, fcsr, zero
	-[0x8000a76c]:sw t6, 336(s1)
Current Store : [0x8000a770] : sw a3, 340(s1) -- Store: [0x80013e28]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a7a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a7a8]:csrrs a3, fcsr, zero
	-[0x8000a7ac]:sw t6, 344(s1)
Current Store : [0x8000a7b0] : sw a3, 348(s1) -- Store: [0x80013e30]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a7e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a7e8]:csrrs a3, fcsr, zero
	-[0x8000a7ec]:sw t6, 352(s1)
Current Store : [0x8000a7f0] : sw a3, 356(s1) -- Store: [0x80013e38]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a824]:fmul.h t6, t5, t4, dyn
	-[0x8000a828]:csrrs a3, fcsr, zero
	-[0x8000a82c]:sw t6, 360(s1)
Current Store : [0x8000a830] : sw a3, 364(s1) -- Store: [0x80013e40]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a864]:fmul.h t6, t5, t4, dyn
	-[0x8000a868]:csrrs a3, fcsr, zero
	-[0x8000a86c]:sw t6, 368(s1)
Current Store : [0x8000a870] : sw a3, 372(s1) -- Store: [0x80013e48]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a8a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a8a8]:csrrs a3, fcsr, zero
	-[0x8000a8ac]:sw t6, 376(s1)
Current Store : [0x8000a8b0] : sw a3, 380(s1) -- Store: [0x80013e50]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a8e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a8e8]:csrrs a3, fcsr, zero
	-[0x8000a8ec]:sw t6, 384(s1)
Current Store : [0x8000a8f0] : sw a3, 388(s1) -- Store: [0x80013e58]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a924]:fmul.h t6, t5, t4, dyn
	-[0x8000a928]:csrrs a3, fcsr, zero
	-[0x8000a92c]:sw t6, 392(s1)
Current Store : [0x8000a930] : sw a3, 396(s1) -- Store: [0x80013e60]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a964]:fmul.h t6, t5, t4, dyn
	-[0x8000a968]:csrrs a3, fcsr, zero
	-[0x8000a96c]:sw t6, 400(s1)
Current Store : [0x8000a970] : sw a3, 404(s1) -- Store: [0x80013e68]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a9a4]:fmul.h t6, t5, t4, dyn
	-[0x8000a9a8]:csrrs a3, fcsr, zero
	-[0x8000a9ac]:sw t6, 408(s1)
Current Store : [0x8000a9b0] : sw a3, 412(s1) -- Store: [0x80013e70]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000a9e4]:fmul.h t6, t5, t4, dyn
	-[0x8000a9e8]:csrrs a3, fcsr, zero
	-[0x8000a9ec]:sw t6, 416(s1)
Current Store : [0x8000a9f0] : sw a3, 420(s1) -- Store: [0x80013e78]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aa24]:fmul.h t6, t5, t4, dyn
	-[0x8000aa28]:csrrs a3, fcsr, zero
	-[0x8000aa2c]:sw t6, 424(s1)
Current Store : [0x8000aa30] : sw a3, 428(s1) -- Store: [0x80013e80]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aa64]:fmul.h t6, t5, t4, dyn
	-[0x8000aa68]:csrrs a3, fcsr, zero
	-[0x8000aa6c]:sw t6, 432(s1)
Current Store : [0x8000aa70] : sw a3, 436(s1) -- Store: [0x80013e88]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aaa4]:fmul.h t6, t5, t4, dyn
	-[0x8000aaa8]:csrrs a3, fcsr, zero
	-[0x8000aaac]:sw t6, 440(s1)
Current Store : [0x8000aab0] : sw a3, 444(s1) -- Store: [0x80013e90]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aae4]:fmul.h t6, t5, t4, dyn
	-[0x8000aae8]:csrrs a3, fcsr, zero
	-[0x8000aaec]:sw t6, 448(s1)
Current Store : [0x8000aaf0] : sw a3, 452(s1) -- Store: [0x80013e98]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ab24]:fmul.h t6, t5, t4, dyn
	-[0x8000ab28]:csrrs a3, fcsr, zero
	-[0x8000ab2c]:sw t6, 456(s1)
Current Store : [0x8000ab30] : sw a3, 460(s1) -- Store: [0x80013ea0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ab64]:fmul.h t6, t5, t4, dyn
	-[0x8000ab68]:csrrs a3, fcsr, zero
	-[0x8000ab6c]:sw t6, 464(s1)
Current Store : [0x8000ab70] : sw a3, 468(s1) -- Store: [0x80013ea8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aba4]:fmul.h t6, t5, t4, dyn
	-[0x8000aba8]:csrrs a3, fcsr, zero
	-[0x8000abac]:sw t6, 472(s1)
Current Store : [0x8000abb0] : sw a3, 476(s1) -- Store: [0x80013eb0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000abe4]:fmul.h t6, t5, t4, dyn
	-[0x8000abe8]:csrrs a3, fcsr, zero
	-[0x8000abec]:sw t6, 480(s1)
Current Store : [0x8000abf0] : sw a3, 484(s1) -- Store: [0x80013eb8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ac24]:fmul.h t6, t5, t4, dyn
	-[0x8000ac28]:csrrs a3, fcsr, zero
	-[0x8000ac2c]:sw t6, 488(s1)
Current Store : [0x8000ac30] : sw a3, 492(s1) -- Store: [0x80013ec0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ac64]:fmul.h t6, t5, t4, dyn
	-[0x8000ac68]:csrrs a3, fcsr, zero
	-[0x8000ac6c]:sw t6, 496(s1)
Current Store : [0x8000ac70] : sw a3, 500(s1) -- Store: [0x80013ec8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aca4]:fmul.h t6, t5, t4, dyn
	-[0x8000aca8]:csrrs a3, fcsr, zero
	-[0x8000acac]:sw t6, 504(s1)
Current Store : [0x8000acb0] : sw a3, 508(s1) -- Store: [0x80013ed0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ace4]:fmul.h t6, t5, t4, dyn
	-[0x8000ace8]:csrrs a3, fcsr, zero
	-[0x8000acec]:sw t6, 512(s1)
Current Store : [0x8000acf0] : sw a3, 516(s1) -- Store: [0x80013ed8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ad24]:fmul.h t6, t5, t4, dyn
	-[0x8000ad28]:csrrs a3, fcsr, zero
	-[0x8000ad2c]:sw t6, 520(s1)
Current Store : [0x8000ad30] : sw a3, 524(s1) -- Store: [0x80013ee0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ad64]:fmul.h t6, t5, t4, dyn
	-[0x8000ad68]:csrrs a3, fcsr, zero
	-[0x8000ad6c]:sw t6, 528(s1)
Current Store : [0x8000ad70] : sw a3, 532(s1) -- Store: [0x80013ee8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ada4]:fmul.h t6, t5, t4, dyn
	-[0x8000ada8]:csrrs a3, fcsr, zero
	-[0x8000adac]:sw t6, 536(s1)
Current Store : [0x8000adb0] : sw a3, 540(s1) -- Store: [0x80013ef0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ade4]:fmul.h t6, t5, t4, dyn
	-[0x8000ade8]:csrrs a3, fcsr, zero
	-[0x8000adec]:sw t6, 544(s1)
Current Store : [0x8000adf0] : sw a3, 548(s1) -- Store: [0x80013ef8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ae24]:fmul.h t6, t5, t4, dyn
	-[0x8000ae28]:csrrs a3, fcsr, zero
	-[0x8000ae2c]:sw t6, 552(s1)
Current Store : [0x8000ae30] : sw a3, 556(s1) -- Store: [0x80013f00]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ae64]:fmul.h t6, t5, t4, dyn
	-[0x8000ae68]:csrrs a3, fcsr, zero
	-[0x8000ae6c]:sw t6, 560(s1)
Current Store : [0x8000ae70] : sw a3, 564(s1) -- Store: [0x80013f08]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aea4]:fmul.h t6, t5, t4, dyn
	-[0x8000aea8]:csrrs a3, fcsr, zero
	-[0x8000aeac]:sw t6, 568(s1)
Current Store : [0x8000aeb0] : sw a3, 572(s1) -- Store: [0x80013f10]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000aee4]:fmul.h t6, t5, t4, dyn
	-[0x8000aee8]:csrrs a3, fcsr, zero
	-[0x8000aeec]:sw t6, 576(s1)
Current Store : [0x8000aef0] : sw a3, 580(s1) -- Store: [0x80013f18]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000af24]:fmul.h t6, t5, t4, dyn
	-[0x8000af28]:csrrs a3, fcsr, zero
	-[0x8000af2c]:sw t6, 584(s1)
Current Store : [0x8000af30] : sw a3, 588(s1) -- Store: [0x80013f20]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000af64]:fmul.h t6, t5, t4, dyn
	-[0x8000af68]:csrrs a3, fcsr, zero
	-[0x8000af6c]:sw t6, 592(s1)
Current Store : [0x8000af70] : sw a3, 596(s1) -- Store: [0x80013f28]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000afa4]:fmul.h t6, t5, t4, dyn
	-[0x8000afa8]:csrrs a3, fcsr, zero
	-[0x8000afac]:sw t6, 600(s1)
Current Store : [0x8000afb0] : sw a3, 604(s1) -- Store: [0x80013f30]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000afe4]:fmul.h t6, t5, t4, dyn
	-[0x8000afe8]:csrrs a3, fcsr, zero
	-[0x8000afec]:sw t6, 608(s1)
Current Store : [0x8000aff0] : sw a3, 612(s1) -- Store: [0x80013f38]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b024]:fmul.h t6, t5, t4, dyn
	-[0x8000b028]:csrrs a3, fcsr, zero
	-[0x8000b02c]:sw t6, 616(s1)
Current Store : [0x8000b030] : sw a3, 620(s1) -- Store: [0x80013f40]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b064]:fmul.h t6, t5, t4, dyn
	-[0x8000b068]:csrrs a3, fcsr, zero
	-[0x8000b06c]:sw t6, 624(s1)
Current Store : [0x8000b070] : sw a3, 628(s1) -- Store: [0x80013f48]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b0a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b0a8]:csrrs a3, fcsr, zero
	-[0x8000b0ac]:sw t6, 632(s1)
Current Store : [0x8000b0b0] : sw a3, 636(s1) -- Store: [0x80013f50]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b0e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b0e8]:csrrs a3, fcsr, zero
	-[0x8000b0ec]:sw t6, 640(s1)
Current Store : [0x8000b0f0] : sw a3, 644(s1) -- Store: [0x80013f58]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b124]:fmul.h t6, t5, t4, dyn
	-[0x8000b128]:csrrs a3, fcsr, zero
	-[0x8000b12c]:sw t6, 648(s1)
Current Store : [0x8000b130] : sw a3, 652(s1) -- Store: [0x80013f60]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b164]:fmul.h t6, t5, t4, dyn
	-[0x8000b168]:csrrs a3, fcsr, zero
	-[0x8000b16c]:sw t6, 656(s1)
Current Store : [0x8000b170] : sw a3, 660(s1) -- Store: [0x80013f68]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b1a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b1a8]:csrrs a3, fcsr, zero
	-[0x8000b1ac]:sw t6, 664(s1)
Current Store : [0x8000b1b0] : sw a3, 668(s1) -- Store: [0x80013f70]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b1e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b1e8]:csrrs a3, fcsr, zero
	-[0x8000b1ec]:sw t6, 672(s1)
Current Store : [0x8000b1f0] : sw a3, 676(s1) -- Store: [0x80013f78]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b224]:fmul.h t6, t5, t4, dyn
	-[0x8000b228]:csrrs a3, fcsr, zero
	-[0x8000b22c]:sw t6, 680(s1)
Current Store : [0x8000b230] : sw a3, 684(s1) -- Store: [0x80013f80]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b264]:fmul.h t6, t5, t4, dyn
	-[0x8000b268]:csrrs a3, fcsr, zero
	-[0x8000b26c]:sw t6, 688(s1)
Current Store : [0x8000b270] : sw a3, 692(s1) -- Store: [0x80013f88]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b2a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b2a8]:csrrs a3, fcsr, zero
	-[0x8000b2ac]:sw t6, 696(s1)
Current Store : [0x8000b2b0] : sw a3, 700(s1) -- Store: [0x80013f90]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b2e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b2e8]:csrrs a3, fcsr, zero
	-[0x8000b2ec]:sw t6, 704(s1)
Current Store : [0x8000b2f0] : sw a3, 708(s1) -- Store: [0x80013f98]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b324]:fmul.h t6, t5, t4, dyn
	-[0x8000b328]:csrrs a3, fcsr, zero
	-[0x8000b32c]:sw t6, 712(s1)
Current Store : [0x8000b330] : sw a3, 716(s1) -- Store: [0x80013fa0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b364]:fmul.h t6, t5, t4, dyn
	-[0x8000b368]:csrrs a3, fcsr, zero
	-[0x8000b36c]:sw t6, 720(s1)
Current Store : [0x8000b370] : sw a3, 724(s1) -- Store: [0x80013fa8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b3a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b3a8]:csrrs a3, fcsr, zero
	-[0x8000b3ac]:sw t6, 728(s1)
Current Store : [0x8000b3b0] : sw a3, 732(s1) -- Store: [0x80013fb0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b3e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b3e8]:csrrs a3, fcsr, zero
	-[0x8000b3ec]:sw t6, 736(s1)
Current Store : [0x8000b3f0] : sw a3, 740(s1) -- Store: [0x80013fb8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b424]:fmul.h t6, t5, t4, dyn
	-[0x8000b428]:csrrs a3, fcsr, zero
	-[0x8000b42c]:sw t6, 744(s1)
Current Store : [0x8000b430] : sw a3, 748(s1) -- Store: [0x80013fc0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b464]:fmul.h t6, t5, t4, dyn
	-[0x8000b468]:csrrs a3, fcsr, zero
	-[0x8000b46c]:sw t6, 752(s1)
Current Store : [0x8000b470] : sw a3, 756(s1) -- Store: [0x80013fc8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b4a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b4a8]:csrrs a3, fcsr, zero
	-[0x8000b4ac]:sw t6, 760(s1)
Current Store : [0x8000b4b0] : sw a3, 764(s1) -- Store: [0x80013fd0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b4e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b4e8]:csrrs a3, fcsr, zero
	-[0x8000b4ec]:sw t6, 768(s1)
Current Store : [0x8000b4f0] : sw a3, 772(s1) -- Store: [0x80013fd8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b524]:fmul.h t6, t5, t4, dyn
	-[0x8000b528]:csrrs a3, fcsr, zero
	-[0x8000b52c]:sw t6, 776(s1)
Current Store : [0x8000b530] : sw a3, 780(s1) -- Store: [0x80013fe0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b564]:fmul.h t6, t5, t4, dyn
	-[0x8000b568]:csrrs a3, fcsr, zero
	-[0x8000b56c]:sw t6, 784(s1)
Current Store : [0x8000b570] : sw a3, 788(s1) -- Store: [0x80013fe8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b5a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b5a8]:csrrs a3, fcsr, zero
	-[0x8000b5ac]:sw t6, 792(s1)
Current Store : [0x8000b5b0] : sw a3, 796(s1) -- Store: [0x80013ff0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b5e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b5e8]:csrrs a3, fcsr, zero
	-[0x8000b5ec]:sw t6, 800(s1)
Current Store : [0x8000b5f0] : sw a3, 804(s1) -- Store: [0x80013ff8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b624]:fmul.h t6, t5, t4, dyn
	-[0x8000b628]:csrrs a3, fcsr, zero
	-[0x8000b62c]:sw t6, 808(s1)
Current Store : [0x8000b630] : sw a3, 812(s1) -- Store: [0x80014000]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b664]:fmul.h t6, t5, t4, dyn
	-[0x8000b668]:csrrs a3, fcsr, zero
	-[0x8000b66c]:sw t6, 816(s1)
Current Store : [0x8000b670] : sw a3, 820(s1) -- Store: [0x80014008]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b6a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b6a8]:csrrs a3, fcsr, zero
	-[0x8000b6ac]:sw t6, 824(s1)
Current Store : [0x8000b6b0] : sw a3, 828(s1) -- Store: [0x80014010]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b6e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b6e8]:csrrs a3, fcsr, zero
	-[0x8000b6ec]:sw t6, 832(s1)
Current Store : [0x8000b6f0] : sw a3, 836(s1) -- Store: [0x80014018]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b724]:fmul.h t6, t5, t4, dyn
	-[0x8000b728]:csrrs a3, fcsr, zero
	-[0x8000b72c]:sw t6, 840(s1)
Current Store : [0x8000b730] : sw a3, 844(s1) -- Store: [0x80014020]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b764]:fmul.h t6, t5, t4, dyn
	-[0x8000b768]:csrrs a3, fcsr, zero
	-[0x8000b76c]:sw t6, 848(s1)
Current Store : [0x8000b770] : sw a3, 852(s1) -- Store: [0x80014028]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b7a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b7a8]:csrrs a3, fcsr, zero
	-[0x8000b7ac]:sw t6, 856(s1)
Current Store : [0x8000b7b0] : sw a3, 860(s1) -- Store: [0x80014030]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b7e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b7e8]:csrrs a3, fcsr, zero
	-[0x8000b7ec]:sw t6, 864(s1)
Current Store : [0x8000b7f0] : sw a3, 868(s1) -- Store: [0x80014038]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b824]:fmul.h t6, t5, t4, dyn
	-[0x8000b828]:csrrs a3, fcsr, zero
	-[0x8000b82c]:sw t6, 872(s1)
Current Store : [0x8000b830] : sw a3, 876(s1) -- Store: [0x80014040]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b864]:fmul.h t6, t5, t4, dyn
	-[0x8000b868]:csrrs a3, fcsr, zero
	-[0x8000b86c]:sw t6, 880(s1)
Current Store : [0x8000b870] : sw a3, 884(s1) -- Store: [0x80014048]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b8a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b8a8]:csrrs a3, fcsr, zero
	-[0x8000b8ac]:sw t6, 888(s1)
Current Store : [0x8000b8b0] : sw a3, 892(s1) -- Store: [0x80014050]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b8e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b8e8]:csrrs a3, fcsr, zero
	-[0x8000b8ec]:sw t6, 896(s1)
Current Store : [0x8000b8f0] : sw a3, 900(s1) -- Store: [0x80014058]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b924]:fmul.h t6, t5, t4, dyn
	-[0x8000b928]:csrrs a3, fcsr, zero
	-[0x8000b92c]:sw t6, 904(s1)
Current Store : [0x8000b930] : sw a3, 908(s1) -- Store: [0x80014060]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b964]:fmul.h t6, t5, t4, dyn
	-[0x8000b968]:csrrs a3, fcsr, zero
	-[0x8000b96c]:sw t6, 912(s1)
Current Store : [0x8000b970] : sw a3, 916(s1) -- Store: [0x80014068]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b9a4]:fmul.h t6, t5, t4, dyn
	-[0x8000b9a8]:csrrs a3, fcsr, zero
	-[0x8000b9ac]:sw t6, 920(s1)
Current Store : [0x8000b9b0] : sw a3, 924(s1) -- Store: [0x80014070]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000b9e4]:fmul.h t6, t5, t4, dyn
	-[0x8000b9e8]:csrrs a3, fcsr, zero
	-[0x8000b9ec]:sw t6, 928(s1)
Current Store : [0x8000b9f0] : sw a3, 932(s1) -- Store: [0x80014078]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ba24]:fmul.h t6, t5, t4, dyn
	-[0x8000ba28]:csrrs a3, fcsr, zero
	-[0x8000ba2c]:sw t6, 936(s1)
Current Store : [0x8000ba30] : sw a3, 940(s1) -- Store: [0x80014080]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ba64]:fmul.h t6, t5, t4, dyn
	-[0x8000ba68]:csrrs a3, fcsr, zero
	-[0x8000ba6c]:sw t6, 944(s1)
Current Store : [0x8000ba70] : sw a3, 948(s1) -- Store: [0x80014088]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000baa4]:fmul.h t6, t5, t4, dyn
	-[0x8000baa8]:csrrs a3, fcsr, zero
	-[0x8000baac]:sw t6, 952(s1)
Current Store : [0x8000bab0] : sw a3, 956(s1) -- Store: [0x80014090]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bae4]:fmul.h t6, t5, t4, dyn
	-[0x8000bae8]:csrrs a3, fcsr, zero
	-[0x8000baec]:sw t6, 960(s1)
Current Store : [0x8000baf0] : sw a3, 964(s1) -- Store: [0x80014098]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bb24]:fmul.h t6, t5, t4, dyn
	-[0x8000bb28]:csrrs a3, fcsr, zero
	-[0x8000bb2c]:sw t6, 968(s1)
Current Store : [0x8000bb30] : sw a3, 972(s1) -- Store: [0x800140a0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bb64]:fmul.h t6, t5, t4, dyn
	-[0x8000bb68]:csrrs a3, fcsr, zero
	-[0x8000bb6c]:sw t6, 976(s1)
Current Store : [0x8000bb70] : sw a3, 980(s1) -- Store: [0x800140a8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bba4]:fmul.h t6, t5, t4, dyn
	-[0x8000bba8]:csrrs a3, fcsr, zero
	-[0x8000bbac]:sw t6, 984(s1)
Current Store : [0x8000bbb0] : sw a3, 988(s1) -- Store: [0x800140b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bbe4]:fmul.h t6, t5, t4, dyn
	-[0x8000bbe8]:csrrs a3, fcsr, zero
	-[0x8000bbec]:sw t6, 992(s1)
Current Store : [0x8000bbf0] : sw a3, 996(s1) -- Store: [0x800140b8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bc24]:fmul.h t6, t5, t4, dyn
	-[0x8000bc28]:csrrs a3, fcsr, zero
	-[0x8000bc2c]:sw t6, 1000(s1)
Current Store : [0x8000bc30] : sw a3, 1004(s1) -- Store: [0x800140c0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bc64]:fmul.h t6, t5, t4, dyn
	-[0x8000bc68]:csrrs a3, fcsr, zero
	-[0x8000bc6c]:sw t6, 1008(s1)
Current Store : [0x8000bc70] : sw a3, 1012(s1) -- Store: [0x800140c8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bca4]:fmul.h t6, t5, t4, dyn
	-[0x8000bca8]:csrrs a3, fcsr, zero
	-[0x8000bcac]:sw t6, 1016(s1)
Current Store : [0x8000bcb0] : sw a3, 1020(s1) -- Store: [0x800140d0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bcec]:fmul.h t6, t5, t4, dyn
	-[0x8000bcf0]:csrrs a3, fcsr, zero
	-[0x8000bcf4]:sw t6, 0(s1)
Current Store : [0x8000bcf8] : sw a3, 4(s1) -- Store: [0x800140d8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bd2c]:fmul.h t6, t5, t4, dyn
	-[0x8000bd30]:csrrs a3, fcsr, zero
	-[0x8000bd34]:sw t6, 8(s1)
Current Store : [0x8000bd38] : sw a3, 12(s1) -- Store: [0x800140e0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bd6c]:fmul.h t6, t5, t4, dyn
	-[0x8000bd70]:csrrs a3, fcsr, zero
	-[0x8000bd74]:sw t6, 16(s1)
Current Store : [0x8000bd78] : sw a3, 20(s1) -- Store: [0x800140e8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bdac]:fmul.h t6, t5, t4, dyn
	-[0x8000bdb0]:csrrs a3, fcsr, zero
	-[0x8000bdb4]:sw t6, 24(s1)
Current Store : [0x8000bdb8] : sw a3, 28(s1) -- Store: [0x800140f0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bdec]:fmul.h t6, t5, t4, dyn
	-[0x8000bdf0]:csrrs a3, fcsr, zero
	-[0x8000bdf4]:sw t6, 32(s1)
Current Store : [0x8000bdf8] : sw a3, 36(s1) -- Store: [0x800140f8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000be2c]:fmul.h t6, t5, t4, dyn
	-[0x8000be30]:csrrs a3, fcsr, zero
	-[0x8000be34]:sw t6, 40(s1)
Current Store : [0x8000be38] : sw a3, 44(s1) -- Store: [0x80014100]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000be6c]:fmul.h t6, t5, t4, dyn
	-[0x8000be70]:csrrs a3, fcsr, zero
	-[0x8000be74]:sw t6, 48(s1)
Current Store : [0x8000be78] : sw a3, 52(s1) -- Store: [0x80014108]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000beac]:fmul.h t6, t5, t4, dyn
	-[0x8000beb0]:csrrs a3, fcsr, zero
	-[0x8000beb4]:sw t6, 56(s1)
Current Store : [0x8000beb8] : sw a3, 60(s1) -- Store: [0x80014110]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000beec]:fmul.h t6, t5, t4, dyn
	-[0x8000bef0]:csrrs a3, fcsr, zero
	-[0x8000bef4]:sw t6, 64(s1)
Current Store : [0x8000bef8] : sw a3, 68(s1) -- Store: [0x80014118]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bf2c]:fmul.h t6, t5, t4, dyn
	-[0x8000bf30]:csrrs a3, fcsr, zero
	-[0x8000bf34]:sw t6, 72(s1)
Current Store : [0x8000bf38] : sw a3, 76(s1) -- Store: [0x80014120]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bf6c]:fmul.h t6, t5, t4, dyn
	-[0x8000bf70]:csrrs a3, fcsr, zero
	-[0x8000bf74]:sw t6, 80(s1)
Current Store : [0x8000bf78] : sw a3, 84(s1) -- Store: [0x80014128]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bfac]:fmul.h t6, t5, t4, dyn
	-[0x8000bfb0]:csrrs a3, fcsr, zero
	-[0x8000bfb4]:sw t6, 88(s1)
Current Store : [0x8000bfb8] : sw a3, 92(s1) -- Store: [0x80014130]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000bfec]:fmul.h t6, t5, t4, dyn
	-[0x8000bff0]:csrrs a3, fcsr, zero
	-[0x8000bff4]:sw t6, 96(s1)
Current Store : [0x8000bff8] : sw a3, 100(s1) -- Store: [0x80014138]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c02c]:fmul.h t6, t5, t4, dyn
	-[0x8000c030]:csrrs a3, fcsr, zero
	-[0x8000c034]:sw t6, 104(s1)
Current Store : [0x8000c038] : sw a3, 108(s1) -- Store: [0x80014140]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c06c]:fmul.h t6, t5, t4, dyn
	-[0x8000c070]:csrrs a3, fcsr, zero
	-[0x8000c074]:sw t6, 112(s1)
Current Store : [0x8000c078] : sw a3, 116(s1) -- Store: [0x80014148]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c0ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c0b0]:csrrs a3, fcsr, zero
	-[0x8000c0b4]:sw t6, 120(s1)
Current Store : [0x8000c0b8] : sw a3, 124(s1) -- Store: [0x80014150]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c0ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c0f0]:csrrs a3, fcsr, zero
	-[0x8000c0f4]:sw t6, 128(s1)
Current Store : [0x8000c0f8] : sw a3, 132(s1) -- Store: [0x80014158]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c12c]:fmul.h t6, t5, t4, dyn
	-[0x8000c130]:csrrs a3, fcsr, zero
	-[0x8000c134]:sw t6, 136(s1)
Current Store : [0x8000c138] : sw a3, 140(s1) -- Store: [0x80014160]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c16c]:fmul.h t6, t5, t4, dyn
	-[0x8000c170]:csrrs a3, fcsr, zero
	-[0x8000c174]:sw t6, 144(s1)
Current Store : [0x8000c178] : sw a3, 148(s1) -- Store: [0x80014168]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c1ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c1b0]:csrrs a3, fcsr, zero
	-[0x8000c1b4]:sw t6, 152(s1)
Current Store : [0x8000c1b8] : sw a3, 156(s1) -- Store: [0x80014170]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c1ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c1f0]:csrrs a3, fcsr, zero
	-[0x8000c1f4]:sw t6, 160(s1)
Current Store : [0x8000c1f8] : sw a3, 164(s1) -- Store: [0x80014178]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c22c]:fmul.h t6, t5, t4, dyn
	-[0x8000c230]:csrrs a3, fcsr, zero
	-[0x8000c234]:sw t6, 168(s1)
Current Store : [0x8000c238] : sw a3, 172(s1) -- Store: [0x80014180]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c26c]:fmul.h t6, t5, t4, dyn
	-[0x8000c270]:csrrs a3, fcsr, zero
	-[0x8000c274]:sw t6, 176(s1)
Current Store : [0x8000c278] : sw a3, 180(s1) -- Store: [0x80014188]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c2ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c2b0]:csrrs a3, fcsr, zero
	-[0x8000c2b4]:sw t6, 184(s1)
Current Store : [0x8000c2b8] : sw a3, 188(s1) -- Store: [0x80014190]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c2ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c2f0]:csrrs a3, fcsr, zero
	-[0x8000c2f4]:sw t6, 192(s1)
Current Store : [0x8000c2f8] : sw a3, 196(s1) -- Store: [0x80014198]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c32c]:fmul.h t6, t5, t4, dyn
	-[0x8000c330]:csrrs a3, fcsr, zero
	-[0x8000c334]:sw t6, 200(s1)
Current Store : [0x8000c338] : sw a3, 204(s1) -- Store: [0x800141a0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c36c]:fmul.h t6, t5, t4, dyn
	-[0x8000c370]:csrrs a3, fcsr, zero
	-[0x8000c374]:sw t6, 208(s1)
Current Store : [0x8000c378] : sw a3, 212(s1) -- Store: [0x800141a8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c3ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c3b0]:csrrs a3, fcsr, zero
	-[0x8000c3b4]:sw t6, 216(s1)
Current Store : [0x8000c3b8] : sw a3, 220(s1) -- Store: [0x800141b0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c3ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c3f0]:csrrs a3, fcsr, zero
	-[0x8000c3f4]:sw t6, 224(s1)
Current Store : [0x8000c3f8] : sw a3, 228(s1) -- Store: [0x800141b8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c42c]:fmul.h t6, t5, t4, dyn
	-[0x8000c430]:csrrs a3, fcsr, zero
	-[0x8000c434]:sw t6, 232(s1)
Current Store : [0x8000c438] : sw a3, 236(s1) -- Store: [0x800141c0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c46c]:fmul.h t6, t5, t4, dyn
	-[0x8000c470]:csrrs a3, fcsr, zero
	-[0x8000c474]:sw t6, 240(s1)
Current Store : [0x8000c478] : sw a3, 244(s1) -- Store: [0x800141c8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c4ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c4b0]:csrrs a3, fcsr, zero
	-[0x8000c4b4]:sw t6, 248(s1)
Current Store : [0x8000c4b8] : sw a3, 252(s1) -- Store: [0x800141d0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c4ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c4f0]:csrrs a3, fcsr, zero
	-[0x8000c4f4]:sw t6, 256(s1)
Current Store : [0x8000c4f8] : sw a3, 260(s1) -- Store: [0x800141d8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c52c]:fmul.h t6, t5, t4, dyn
	-[0x8000c530]:csrrs a3, fcsr, zero
	-[0x8000c534]:sw t6, 264(s1)
Current Store : [0x8000c538] : sw a3, 268(s1) -- Store: [0x800141e0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c56c]:fmul.h t6, t5, t4, dyn
	-[0x8000c570]:csrrs a3, fcsr, zero
	-[0x8000c574]:sw t6, 272(s1)
Current Store : [0x8000c578] : sw a3, 276(s1) -- Store: [0x800141e8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c5ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c5b0]:csrrs a3, fcsr, zero
	-[0x8000c5b4]:sw t6, 280(s1)
Current Store : [0x8000c5b8] : sw a3, 284(s1) -- Store: [0x800141f0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c5ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c5f0]:csrrs a3, fcsr, zero
	-[0x8000c5f4]:sw t6, 288(s1)
Current Store : [0x8000c5f8] : sw a3, 292(s1) -- Store: [0x800141f8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c62c]:fmul.h t6, t5, t4, dyn
	-[0x8000c630]:csrrs a3, fcsr, zero
	-[0x8000c634]:sw t6, 296(s1)
Current Store : [0x8000c638] : sw a3, 300(s1) -- Store: [0x80014200]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c66c]:fmul.h t6, t5, t4, dyn
	-[0x8000c670]:csrrs a3, fcsr, zero
	-[0x8000c674]:sw t6, 304(s1)
Current Store : [0x8000c678] : sw a3, 308(s1) -- Store: [0x80014208]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c6ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c6b0]:csrrs a3, fcsr, zero
	-[0x8000c6b4]:sw t6, 312(s1)
Current Store : [0x8000c6b8] : sw a3, 316(s1) -- Store: [0x80014210]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c6ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c6f0]:csrrs a3, fcsr, zero
	-[0x8000c6f4]:sw t6, 320(s1)
Current Store : [0x8000c6f8] : sw a3, 324(s1) -- Store: [0x80014218]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c72c]:fmul.h t6, t5, t4, dyn
	-[0x8000c730]:csrrs a3, fcsr, zero
	-[0x8000c734]:sw t6, 328(s1)
Current Store : [0x8000c738] : sw a3, 332(s1) -- Store: [0x80014220]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c76c]:fmul.h t6, t5, t4, dyn
	-[0x8000c770]:csrrs a3, fcsr, zero
	-[0x8000c774]:sw t6, 336(s1)
Current Store : [0x8000c778] : sw a3, 340(s1) -- Store: [0x80014228]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c7ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c7b0]:csrrs a3, fcsr, zero
	-[0x8000c7b4]:sw t6, 344(s1)
Current Store : [0x8000c7b8] : sw a3, 348(s1) -- Store: [0x80014230]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c7ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c7f0]:csrrs a3, fcsr, zero
	-[0x8000c7f4]:sw t6, 352(s1)
Current Store : [0x8000c7f8] : sw a3, 356(s1) -- Store: [0x80014238]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c82c]:fmul.h t6, t5, t4, dyn
	-[0x8000c830]:csrrs a3, fcsr, zero
	-[0x8000c834]:sw t6, 360(s1)
Current Store : [0x8000c838] : sw a3, 364(s1) -- Store: [0x80014240]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c86c]:fmul.h t6, t5, t4, dyn
	-[0x8000c870]:csrrs a3, fcsr, zero
	-[0x8000c874]:sw t6, 368(s1)
Current Store : [0x8000c878] : sw a3, 372(s1) -- Store: [0x80014248]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c8ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c8b0]:csrrs a3, fcsr, zero
	-[0x8000c8b4]:sw t6, 376(s1)
Current Store : [0x8000c8b8] : sw a3, 380(s1) -- Store: [0x80014250]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c8ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c8f0]:csrrs a3, fcsr, zero
	-[0x8000c8f4]:sw t6, 384(s1)
Current Store : [0x8000c8f8] : sw a3, 388(s1) -- Store: [0x80014258]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c92c]:fmul.h t6, t5, t4, dyn
	-[0x8000c930]:csrrs a3, fcsr, zero
	-[0x8000c934]:sw t6, 392(s1)
Current Store : [0x8000c938] : sw a3, 396(s1) -- Store: [0x80014260]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c96c]:fmul.h t6, t5, t4, dyn
	-[0x8000c970]:csrrs a3, fcsr, zero
	-[0x8000c974]:sw t6, 400(s1)
Current Store : [0x8000c978] : sw a3, 404(s1) -- Store: [0x80014268]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c9ac]:fmul.h t6, t5, t4, dyn
	-[0x8000c9b0]:csrrs a3, fcsr, zero
	-[0x8000c9b4]:sw t6, 408(s1)
Current Store : [0x8000c9b8] : sw a3, 412(s1) -- Store: [0x80014270]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000c9ec]:fmul.h t6, t5, t4, dyn
	-[0x8000c9f0]:csrrs a3, fcsr, zero
	-[0x8000c9f4]:sw t6, 416(s1)
Current Store : [0x8000c9f8] : sw a3, 420(s1) -- Store: [0x80014278]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ca2c]:fmul.h t6, t5, t4, dyn
	-[0x8000ca30]:csrrs a3, fcsr, zero
	-[0x8000ca34]:sw t6, 424(s1)
Current Store : [0x8000ca38] : sw a3, 428(s1) -- Store: [0x80014280]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ca6c]:fmul.h t6, t5, t4, dyn
	-[0x8000ca70]:csrrs a3, fcsr, zero
	-[0x8000ca74]:sw t6, 432(s1)
Current Store : [0x8000ca78] : sw a3, 436(s1) -- Store: [0x80014288]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000caac]:fmul.h t6, t5, t4, dyn
	-[0x8000cab0]:csrrs a3, fcsr, zero
	-[0x8000cab4]:sw t6, 440(s1)
Current Store : [0x8000cab8] : sw a3, 444(s1) -- Store: [0x80014290]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000caec]:fmul.h t6, t5, t4, dyn
	-[0x8000caf0]:csrrs a3, fcsr, zero
	-[0x8000caf4]:sw t6, 448(s1)
Current Store : [0x8000caf8] : sw a3, 452(s1) -- Store: [0x80014298]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cb2c]:fmul.h t6, t5, t4, dyn
	-[0x8000cb30]:csrrs a3, fcsr, zero
	-[0x8000cb34]:sw t6, 456(s1)
Current Store : [0x8000cb38] : sw a3, 460(s1) -- Store: [0x800142a0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cb6c]:fmul.h t6, t5, t4, dyn
	-[0x8000cb70]:csrrs a3, fcsr, zero
	-[0x8000cb74]:sw t6, 464(s1)
Current Store : [0x8000cb78] : sw a3, 468(s1) -- Store: [0x800142a8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cbac]:fmul.h t6, t5, t4, dyn
	-[0x8000cbb0]:csrrs a3, fcsr, zero
	-[0x8000cbb4]:sw t6, 472(s1)
Current Store : [0x8000cbb8] : sw a3, 476(s1) -- Store: [0x800142b0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cbec]:fmul.h t6, t5, t4, dyn
	-[0x8000cbf0]:csrrs a3, fcsr, zero
	-[0x8000cbf4]:sw t6, 480(s1)
Current Store : [0x8000cbf8] : sw a3, 484(s1) -- Store: [0x800142b8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cc2c]:fmul.h t6, t5, t4, dyn
	-[0x8000cc30]:csrrs a3, fcsr, zero
	-[0x8000cc34]:sw t6, 488(s1)
Current Store : [0x8000cc38] : sw a3, 492(s1) -- Store: [0x800142c0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cc6c]:fmul.h t6, t5, t4, dyn
	-[0x8000cc70]:csrrs a3, fcsr, zero
	-[0x8000cc74]:sw t6, 496(s1)
Current Store : [0x8000cc78] : sw a3, 500(s1) -- Store: [0x800142c8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ccac]:fmul.h t6, t5, t4, dyn
	-[0x8000ccb0]:csrrs a3, fcsr, zero
	-[0x8000ccb4]:sw t6, 504(s1)
Current Store : [0x8000ccb8] : sw a3, 508(s1) -- Store: [0x800142d0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ccec]:fmul.h t6, t5, t4, dyn
	-[0x8000ccf0]:csrrs a3, fcsr, zero
	-[0x8000ccf4]:sw t6, 512(s1)
Current Store : [0x8000ccf8] : sw a3, 516(s1) -- Store: [0x800142d8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cd2c]:fmul.h t6, t5, t4, dyn
	-[0x8000cd30]:csrrs a3, fcsr, zero
	-[0x8000cd34]:sw t6, 520(s1)
Current Store : [0x8000cd38] : sw a3, 524(s1) -- Store: [0x800142e0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cd6c]:fmul.h t6, t5, t4, dyn
	-[0x8000cd70]:csrrs a3, fcsr, zero
	-[0x8000cd74]:sw t6, 528(s1)
Current Store : [0x8000cd78] : sw a3, 532(s1) -- Store: [0x800142e8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cdac]:fmul.h t6, t5, t4, dyn
	-[0x8000cdb0]:csrrs a3, fcsr, zero
	-[0x8000cdb4]:sw t6, 536(s1)
Current Store : [0x8000cdb8] : sw a3, 540(s1) -- Store: [0x800142f0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cdec]:fmul.h t6, t5, t4, dyn
	-[0x8000cdf0]:csrrs a3, fcsr, zero
	-[0x8000cdf4]:sw t6, 544(s1)
Current Store : [0x8000cdf8] : sw a3, 548(s1) -- Store: [0x800142f8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ce2c]:fmul.h t6, t5, t4, dyn
	-[0x8000ce30]:csrrs a3, fcsr, zero
	-[0x8000ce34]:sw t6, 552(s1)
Current Store : [0x8000ce38] : sw a3, 556(s1) -- Store: [0x80014300]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ce6c]:fmul.h t6, t5, t4, dyn
	-[0x8000ce70]:csrrs a3, fcsr, zero
	-[0x8000ce74]:sw t6, 560(s1)
Current Store : [0x8000ce78] : sw a3, 564(s1) -- Store: [0x80014308]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ceac]:fmul.h t6, t5, t4, dyn
	-[0x8000ceb0]:csrrs a3, fcsr, zero
	-[0x8000ceb4]:sw t6, 568(s1)
Current Store : [0x8000ceb8] : sw a3, 572(s1) -- Store: [0x80014310]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ceec]:fmul.h t6, t5, t4, dyn
	-[0x8000cef0]:csrrs a3, fcsr, zero
	-[0x8000cef4]:sw t6, 576(s1)
Current Store : [0x8000cef8] : sw a3, 580(s1) -- Store: [0x80014318]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cf2c]:fmul.h t6, t5, t4, dyn
	-[0x8000cf30]:csrrs a3, fcsr, zero
	-[0x8000cf34]:sw t6, 584(s1)
Current Store : [0x8000cf38] : sw a3, 588(s1) -- Store: [0x80014320]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cf6c]:fmul.h t6, t5, t4, dyn
	-[0x8000cf70]:csrrs a3, fcsr, zero
	-[0x8000cf74]:sw t6, 592(s1)
Current Store : [0x8000cf78] : sw a3, 596(s1) -- Store: [0x80014328]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cfac]:fmul.h t6, t5, t4, dyn
	-[0x8000cfb0]:csrrs a3, fcsr, zero
	-[0x8000cfb4]:sw t6, 600(s1)
Current Store : [0x8000cfb8] : sw a3, 604(s1) -- Store: [0x80014330]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000cfec]:fmul.h t6, t5, t4, dyn
	-[0x8000cff0]:csrrs a3, fcsr, zero
	-[0x8000cff4]:sw t6, 608(s1)
Current Store : [0x8000cff8] : sw a3, 612(s1) -- Store: [0x80014338]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d02c]:fmul.h t6, t5, t4, dyn
	-[0x8000d030]:csrrs a3, fcsr, zero
	-[0x8000d034]:sw t6, 616(s1)
Current Store : [0x8000d038] : sw a3, 620(s1) -- Store: [0x80014340]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d06c]:fmul.h t6, t5, t4, dyn
	-[0x8000d070]:csrrs a3, fcsr, zero
	-[0x8000d074]:sw t6, 624(s1)
Current Store : [0x8000d078] : sw a3, 628(s1) -- Store: [0x80014348]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d0ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d0b0]:csrrs a3, fcsr, zero
	-[0x8000d0b4]:sw t6, 632(s1)
Current Store : [0x8000d0b8] : sw a3, 636(s1) -- Store: [0x80014350]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d0ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d0f0]:csrrs a3, fcsr, zero
	-[0x8000d0f4]:sw t6, 640(s1)
Current Store : [0x8000d0f8] : sw a3, 644(s1) -- Store: [0x80014358]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d12c]:fmul.h t6, t5, t4, dyn
	-[0x8000d130]:csrrs a3, fcsr, zero
	-[0x8000d134]:sw t6, 648(s1)
Current Store : [0x8000d138] : sw a3, 652(s1) -- Store: [0x80014360]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d16c]:fmul.h t6, t5, t4, dyn
	-[0x8000d170]:csrrs a3, fcsr, zero
	-[0x8000d174]:sw t6, 656(s1)
Current Store : [0x8000d178] : sw a3, 660(s1) -- Store: [0x80014368]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d1ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d1b0]:csrrs a3, fcsr, zero
	-[0x8000d1b4]:sw t6, 664(s1)
Current Store : [0x8000d1b8] : sw a3, 668(s1) -- Store: [0x80014370]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d1ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d1f0]:csrrs a3, fcsr, zero
	-[0x8000d1f4]:sw t6, 672(s1)
Current Store : [0x8000d1f8] : sw a3, 676(s1) -- Store: [0x80014378]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d22c]:fmul.h t6, t5, t4, dyn
	-[0x8000d230]:csrrs a3, fcsr, zero
	-[0x8000d234]:sw t6, 680(s1)
Current Store : [0x8000d238] : sw a3, 684(s1) -- Store: [0x80014380]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d26c]:fmul.h t6, t5, t4, dyn
	-[0x8000d270]:csrrs a3, fcsr, zero
	-[0x8000d274]:sw t6, 688(s1)
Current Store : [0x8000d278] : sw a3, 692(s1) -- Store: [0x80014388]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d2ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d2b0]:csrrs a3, fcsr, zero
	-[0x8000d2b4]:sw t6, 696(s1)
Current Store : [0x8000d2b8] : sw a3, 700(s1) -- Store: [0x80014390]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d2ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d2f0]:csrrs a3, fcsr, zero
	-[0x8000d2f4]:sw t6, 704(s1)
Current Store : [0x8000d2f8] : sw a3, 708(s1) -- Store: [0x80014398]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d32c]:fmul.h t6, t5, t4, dyn
	-[0x8000d330]:csrrs a3, fcsr, zero
	-[0x8000d334]:sw t6, 712(s1)
Current Store : [0x8000d338] : sw a3, 716(s1) -- Store: [0x800143a0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d36c]:fmul.h t6, t5, t4, dyn
	-[0x8000d370]:csrrs a3, fcsr, zero
	-[0x8000d374]:sw t6, 720(s1)
Current Store : [0x8000d378] : sw a3, 724(s1) -- Store: [0x800143a8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d3ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d3b0]:csrrs a3, fcsr, zero
	-[0x8000d3b4]:sw t6, 728(s1)
Current Store : [0x8000d3b8] : sw a3, 732(s1) -- Store: [0x800143b0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d3ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d3f0]:csrrs a3, fcsr, zero
	-[0x8000d3f4]:sw t6, 736(s1)
Current Store : [0x8000d3f8] : sw a3, 740(s1) -- Store: [0x800143b8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d42c]:fmul.h t6, t5, t4, dyn
	-[0x8000d430]:csrrs a3, fcsr, zero
	-[0x8000d434]:sw t6, 744(s1)
Current Store : [0x8000d438] : sw a3, 748(s1) -- Store: [0x800143c0]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d46c]:fmul.h t6, t5, t4, dyn
	-[0x8000d470]:csrrs a3, fcsr, zero
	-[0x8000d474]:sw t6, 752(s1)
Current Store : [0x8000d478] : sw a3, 756(s1) -- Store: [0x800143c8]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d4ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d4b0]:csrrs a3, fcsr, zero
	-[0x8000d4b4]:sw t6, 760(s1)
Current Store : [0x8000d4b8] : sw a3, 764(s1) -- Store: [0x800143d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d4ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d4f0]:csrrs a3, fcsr, zero
	-[0x8000d4f4]:sw t6, 768(s1)
Current Store : [0x8000d4f8] : sw a3, 772(s1) -- Store: [0x800143d8]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d52c]:fmul.h t6, t5, t4, dyn
	-[0x8000d530]:csrrs a3, fcsr, zero
	-[0x8000d534]:sw t6, 776(s1)
Current Store : [0x8000d538] : sw a3, 780(s1) -- Store: [0x800143e0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d56c]:fmul.h t6, t5, t4, dyn
	-[0x8000d570]:csrrs a3, fcsr, zero
	-[0x8000d574]:sw t6, 784(s1)
Current Store : [0x8000d578] : sw a3, 788(s1) -- Store: [0x800143e8]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d5ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d5b0]:csrrs a3, fcsr, zero
	-[0x8000d5b4]:sw t6, 792(s1)
Current Store : [0x8000d5b8] : sw a3, 796(s1) -- Store: [0x800143f0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d5ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d5f0]:csrrs a3, fcsr, zero
	-[0x8000d5f4]:sw t6, 800(s1)
Current Store : [0x8000d5f8] : sw a3, 804(s1) -- Store: [0x800143f8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d62c]:fmul.h t6, t5, t4, dyn
	-[0x8000d630]:csrrs a3, fcsr, zero
	-[0x8000d634]:sw t6, 808(s1)
Current Store : [0x8000d638] : sw a3, 812(s1) -- Store: [0x80014400]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d66c]:fmul.h t6, t5, t4, dyn
	-[0x8000d670]:csrrs a3, fcsr, zero
	-[0x8000d674]:sw t6, 816(s1)
Current Store : [0x8000d678] : sw a3, 820(s1) -- Store: [0x80014408]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d6ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d6b0]:csrrs a3, fcsr, zero
	-[0x8000d6b4]:sw t6, 824(s1)
Current Store : [0x8000d6b8] : sw a3, 828(s1) -- Store: [0x80014410]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d6ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d6f0]:csrrs a3, fcsr, zero
	-[0x8000d6f4]:sw t6, 832(s1)
Current Store : [0x8000d6f8] : sw a3, 836(s1) -- Store: [0x80014418]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d72c]:fmul.h t6, t5, t4, dyn
	-[0x8000d730]:csrrs a3, fcsr, zero
	-[0x8000d734]:sw t6, 840(s1)
Current Store : [0x8000d738] : sw a3, 844(s1) -- Store: [0x80014420]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d76c]:fmul.h t6, t5, t4, dyn
	-[0x8000d770]:csrrs a3, fcsr, zero
	-[0x8000d774]:sw t6, 848(s1)
Current Store : [0x8000d778] : sw a3, 852(s1) -- Store: [0x80014428]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d7ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d7b0]:csrrs a3, fcsr, zero
	-[0x8000d7b4]:sw t6, 856(s1)
Current Store : [0x8000d7b8] : sw a3, 860(s1) -- Store: [0x80014430]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d7ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d7f0]:csrrs a3, fcsr, zero
	-[0x8000d7f4]:sw t6, 864(s1)
Current Store : [0x8000d7f8] : sw a3, 868(s1) -- Store: [0x80014438]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d82c]:fmul.h t6, t5, t4, dyn
	-[0x8000d830]:csrrs a3, fcsr, zero
	-[0x8000d834]:sw t6, 872(s1)
Current Store : [0x8000d838] : sw a3, 876(s1) -- Store: [0x80014440]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d86c]:fmul.h t6, t5, t4, dyn
	-[0x8000d870]:csrrs a3, fcsr, zero
	-[0x8000d874]:sw t6, 880(s1)
Current Store : [0x8000d878] : sw a3, 884(s1) -- Store: [0x80014448]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d8ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d8b0]:csrrs a3, fcsr, zero
	-[0x8000d8b4]:sw t6, 888(s1)
Current Store : [0x8000d8b8] : sw a3, 892(s1) -- Store: [0x80014450]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d8ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d8f0]:csrrs a3, fcsr, zero
	-[0x8000d8f4]:sw t6, 896(s1)
Current Store : [0x8000d8f8] : sw a3, 900(s1) -- Store: [0x80014458]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d92c]:fmul.h t6, t5, t4, dyn
	-[0x8000d930]:csrrs a3, fcsr, zero
	-[0x8000d934]:sw t6, 904(s1)
Current Store : [0x8000d938] : sw a3, 908(s1) -- Store: [0x80014460]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d96c]:fmul.h t6, t5, t4, dyn
	-[0x8000d970]:csrrs a3, fcsr, zero
	-[0x8000d974]:sw t6, 912(s1)
Current Store : [0x8000d978] : sw a3, 916(s1) -- Store: [0x80014468]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d9ac]:fmul.h t6, t5, t4, dyn
	-[0x8000d9b0]:csrrs a3, fcsr, zero
	-[0x8000d9b4]:sw t6, 920(s1)
Current Store : [0x8000d9b8] : sw a3, 924(s1) -- Store: [0x80014470]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000d9ec]:fmul.h t6, t5, t4, dyn
	-[0x8000d9f0]:csrrs a3, fcsr, zero
	-[0x8000d9f4]:sw t6, 928(s1)
Current Store : [0x8000d9f8] : sw a3, 932(s1) -- Store: [0x80014478]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000da2c]:fmul.h t6, t5, t4, dyn
	-[0x8000da30]:csrrs a3, fcsr, zero
	-[0x8000da34]:sw t6, 936(s1)
Current Store : [0x8000da38] : sw a3, 940(s1) -- Store: [0x80014480]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000da6c]:fmul.h t6, t5, t4, dyn
	-[0x8000da70]:csrrs a3, fcsr, zero
	-[0x8000da74]:sw t6, 944(s1)
Current Store : [0x8000da78] : sw a3, 948(s1) -- Store: [0x80014488]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000daac]:fmul.h t6, t5, t4, dyn
	-[0x8000dab0]:csrrs a3, fcsr, zero
	-[0x8000dab4]:sw t6, 952(s1)
Current Store : [0x8000dab8] : sw a3, 956(s1) -- Store: [0x80014490]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000daec]:fmul.h t6, t5, t4, dyn
	-[0x8000daf0]:csrrs a3, fcsr, zero
	-[0x8000daf4]:sw t6, 960(s1)
Current Store : [0x8000daf8] : sw a3, 964(s1) -- Store: [0x80014498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000db2c]:fmul.h t6, t5, t4, dyn
	-[0x8000db30]:csrrs a3, fcsr, zero
	-[0x8000db34]:sw t6, 968(s1)
Current Store : [0x8000db38] : sw a3, 972(s1) -- Store: [0x800144a0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000db6c]:fmul.h t6, t5, t4, dyn
	-[0x8000db70]:csrrs a3, fcsr, zero
	-[0x8000db74]:sw t6, 976(s1)
Current Store : [0x8000db78] : sw a3, 980(s1) -- Store: [0x800144a8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dbac]:fmul.h t6, t5, t4, dyn
	-[0x8000dbb0]:csrrs a3, fcsr, zero
	-[0x8000dbb4]:sw t6, 984(s1)
Current Store : [0x8000dbb8] : sw a3, 988(s1) -- Store: [0x800144b0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dbec]:fmul.h t6, t5, t4, dyn
	-[0x8000dbf0]:csrrs a3, fcsr, zero
	-[0x8000dbf4]:sw t6, 992(s1)
Current Store : [0x8000dbf8] : sw a3, 996(s1) -- Store: [0x800144b8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dc24]:fmul.h t6, t5, t4, dyn
	-[0x8000dc28]:csrrs a3, fcsr, zero
	-[0x8000dc2c]:sw t6, 1000(s1)
Current Store : [0x8000dc30] : sw a3, 1004(s1) -- Store: [0x800144c0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dc5c]:fmul.h t6, t5, t4, dyn
	-[0x8000dc60]:csrrs a3, fcsr, zero
	-[0x8000dc64]:sw t6, 1008(s1)
Current Store : [0x8000dc68] : sw a3, 1012(s1) -- Store: [0x800144c8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dc94]:fmul.h t6, t5, t4, dyn
	-[0x8000dc98]:csrrs a3, fcsr, zero
	-[0x8000dc9c]:sw t6, 1016(s1)
Current Store : [0x8000dca0] : sw a3, 1020(s1) -- Store: [0x800144d0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dcd4]:fmul.h t6, t5, t4, dyn
	-[0x8000dcd8]:csrrs a3, fcsr, zero
	-[0x8000dcdc]:sw t6, 0(s1)
Current Store : [0x8000dce0] : sw a3, 4(s1) -- Store: [0x800144d8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dd0c]:fmul.h t6, t5, t4, dyn
	-[0x8000dd10]:csrrs a3, fcsr, zero
	-[0x8000dd14]:sw t6, 8(s1)
Current Store : [0x8000dd18] : sw a3, 12(s1) -- Store: [0x800144e0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dd44]:fmul.h t6, t5, t4, dyn
	-[0x8000dd48]:csrrs a3, fcsr, zero
	-[0x8000dd4c]:sw t6, 16(s1)
Current Store : [0x8000dd50] : sw a3, 20(s1) -- Store: [0x800144e8]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dd7c]:fmul.h t6, t5, t4, dyn
	-[0x8000dd80]:csrrs a3, fcsr, zero
	-[0x8000dd84]:sw t6, 24(s1)
Current Store : [0x8000dd88] : sw a3, 28(s1) -- Store: [0x800144f0]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ddb4]:fmul.h t6, t5, t4, dyn
	-[0x8000ddb8]:csrrs a3, fcsr, zero
	-[0x8000ddbc]:sw t6, 32(s1)
Current Store : [0x8000ddc0] : sw a3, 36(s1) -- Store: [0x800144f8]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ddec]:fmul.h t6, t5, t4, dyn
	-[0x8000ddf0]:csrrs a3, fcsr, zero
	-[0x8000ddf4]:sw t6, 40(s1)
Current Store : [0x8000ddf8] : sw a3, 44(s1) -- Store: [0x80014500]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000de24]:fmul.h t6, t5, t4, dyn
	-[0x8000de28]:csrrs a3, fcsr, zero
	-[0x8000de2c]:sw t6, 48(s1)
Current Store : [0x8000de30] : sw a3, 52(s1) -- Store: [0x80014508]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000de5c]:fmul.h t6, t5, t4, dyn
	-[0x8000de60]:csrrs a3, fcsr, zero
	-[0x8000de64]:sw t6, 56(s1)
Current Store : [0x8000de68] : sw a3, 60(s1) -- Store: [0x80014510]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000de94]:fmul.h t6, t5, t4, dyn
	-[0x8000de98]:csrrs a3, fcsr, zero
	-[0x8000de9c]:sw t6, 64(s1)
Current Store : [0x8000dea0] : sw a3, 68(s1) -- Store: [0x80014518]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000decc]:fmul.h t6, t5, t4, dyn
	-[0x8000ded0]:csrrs a3, fcsr, zero
	-[0x8000ded4]:sw t6, 72(s1)
Current Store : [0x8000ded8] : sw a3, 76(s1) -- Store: [0x80014520]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000df04]:fmul.h t6, t5, t4, dyn
	-[0x8000df08]:csrrs a3, fcsr, zero
	-[0x8000df0c]:sw t6, 80(s1)
Current Store : [0x8000df10] : sw a3, 84(s1) -- Store: [0x80014528]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000df3c]:fmul.h t6, t5, t4, dyn
	-[0x8000df40]:csrrs a3, fcsr, zero
	-[0x8000df44]:sw t6, 88(s1)
Current Store : [0x8000df48] : sw a3, 92(s1) -- Store: [0x80014530]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000df74]:fmul.h t6, t5, t4, dyn
	-[0x8000df78]:csrrs a3, fcsr, zero
	-[0x8000df7c]:sw t6, 96(s1)
Current Store : [0x8000df80] : sw a3, 100(s1) -- Store: [0x80014538]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dfac]:fmul.h t6, t5, t4, dyn
	-[0x8000dfb0]:csrrs a3, fcsr, zero
	-[0x8000dfb4]:sw t6, 104(s1)
Current Store : [0x8000dfb8] : sw a3, 108(s1) -- Store: [0x80014540]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000dfe4]:fmul.h t6, t5, t4, dyn
	-[0x8000dfe8]:csrrs a3, fcsr, zero
	-[0x8000dfec]:sw t6, 112(s1)
Current Store : [0x8000dff0] : sw a3, 116(s1) -- Store: [0x80014548]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e01c]:fmul.h t6, t5, t4, dyn
	-[0x8000e020]:csrrs a3, fcsr, zero
	-[0x8000e024]:sw t6, 120(s1)
Current Store : [0x8000e028] : sw a3, 124(s1) -- Store: [0x80014550]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e054]:fmul.h t6, t5, t4, dyn
	-[0x8000e058]:csrrs a3, fcsr, zero
	-[0x8000e05c]:sw t6, 128(s1)
Current Store : [0x8000e060] : sw a3, 132(s1) -- Store: [0x80014558]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e08c]:fmul.h t6, t5, t4, dyn
	-[0x8000e090]:csrrs a3, fcsr, zero
	-[0x8000e094]:sw t6, 136(s1)
Current Store : [0x8000e098] : sw a3, 140(s1) -- Store: [0x80014560]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e0c4]:fmul.h t6, t5, t4, dyn
	-[0x8000e0c8]:csrrs a3, fcsr, zero
	-[0x8000e0cc]:sw t6, 144(s1)
Current Store : [0x8000e0d0] : sw a3, 148(s1) -- Store: [0x80014568]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e0fc]:fmul.h t6, t5, t4, dyn
	-[0x8000e100]:csrrs a3, fcsr, zero
	-[0x8000e104]:sw t6, 152(s1)
Current Store : [0x8000e108] : sw a3, 156(s1) -- Store: [0x80014570]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e134]:fmul.h t6, t5, t4, dyn
	-[0x8000e138]:csrrs a3, fcsr, zero
	-[0x8000e13c]:sw t6, 160(s1)
Current Store : [0x8000e140] : sw a3, 164(s1) -- Store: [0x80014578]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e16c]:fmul.h t6, t5, t4, dyn
	-[0x8000e170]:csrrs a3, fcsr, zero
	-[0x8000e174]:sw t6, 168(s1)
Current Store : [0x8000e178] : sw a3, 172(s1) -- Store: [0x80014580]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e1a4]:fmul.h t6, t5, t4, dyn
	-[0x8000e1a8]:csrrs a3, fcsr, zero
	-[0x8000e1ac]:sw t6, 176(s1)
Current Store : [0x8000e1b0] : sw a3, 180(s1) -- Store: [0x80014588]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e1dc]:fmul.h t6, t5, t4, dyn
	-[0x8000e1e0]:csrrs a3, fcsr, zero
	-[0x8000e1e4]:sw t6, 184(s1)
Current Store : [0x8000e1e8] : sw a3, 188(s1) -- Store: [0x80014590]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e214]:fmul.h t6, t5, t4, dyn
	-[0x8000e218]:csrrs a3, fcsr, zero
	-[0x8000e21c]:sw t6, 192(s1)
Current Store : [0x8000e220] : sw a3, 196(s1) -- Store: [0x80014598]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e24c]:fmul.h t6, t5, t4, dyn
	-[0x8000e250]:csrrs a3, fcsr, zero
	-[0x8000e254]:sw t6, 200(s1)
Current Store : [0x8000e258] : sw a3, 204(s1) -- Store: [0x800145a0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e284]:fmul.h t6, t5, t4, dyn
	-[0x8000e288]:csrrs a3, fcsr, zero
	-[0x8000e28c]:sw t6, 208(s1)
Current Store : [0x8000e290] : sw a3, 212(s1) -- Store: [0x800145a8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e2bc]:fmul.h t6, t5, t4, dyn
	-[0x8000e2c0]:csrrs a3, fcsr, zero
	-[0x8000e2c4]:sw t6, 216(s1)
Current Store : [0x8000e2c8] : sw a3, 220(s1) -- Store: [0x800145b0]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e2f4]:fmul.h t6, t5, t4, dyn
	-[0x8000e2f8]:csrrs a3, fcsr, zero
	-[0x8000e2fc]:sw t6, 224(s1)
Current Store : [0x8000e300] : sw a3, 228(s1) -- Store: [0x800145b8]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e32c]:fmul.h t6, t5, t4, dyn
	-[0x8000e330]:csrrs a3, fcsr, zero
	-[0x8000e334]:sw t6, 232(s1)
Current Store : [0x8000e338] : sw a3, 236(s1) -- Store: [0x800145c0]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e364]:fmul.h t6, t5, t4, dyn
	-[0x8000e368]:csrrs a3, fcsr, zero
	-[0x8000e36c]:sw t6, 240(s1)
Current Store : [0x8000e370] : sw a3, 244(s1) -- Store: [0x800145c8]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e39c]:fmul.h t6, t5, t4, dyn
	-[0x8000e3a0]:csrrs a3, fcsr, zero
	-[0x8000e3a4]:sw t6, 248(s1)
Current Store : [0x8000e3a8] : sw a3, 252(s1) -- Store: [0x800145d0]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e3d4]:fmul.h t6, t5, t4, dyn
	-[0x8000e3d8]:csrrs a3, fcsr, zero
	-[0x8000e3dc]:sw t6, 256(s1)
Current Store : [0x8000e3e0] : sw a3, 260(s1) -- Store: [0x800145d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e40c]:fmul.h t6, t5, t4, dyn
	-[0x8000e410]:csrrs a3, fcsr, zero
	-[0x8000e414]:sw t6, 264(s1)
Current Store : [0x8000e418] : sw a3, 268(s1) -- Store: [0x800145e0]:0x00000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e444]:fmul.h t6, t5, t4, dyn
	-[0x8000e448]:csrrs a3, fcsr, zero
	-[0x8000e44c]:sw t6, 272(s1)
Current Store : [0x8000e450] : sw a3, 276(s1) -- Store: [0x800145e8]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e47c]:fmul.h t6, t5, t4, dyn
	-[0x8000e480]:csrrs a3, fcsr, zero
	-[0x8000e484]:sw t6, 280(s1)
Current Store : [0x8000e488] : sw a3, 284(s1) -- Store: [0x800145f0]:0x00000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e4b4]:fmul.h t6, t5, t4, dyn
	-[0x8000e4b8]:csrrs a3, fcsr, zero
	-[0x8000e4bc]:sw t6, 288(s1)
Current Store : [0x8000e4c0] : sw a3, 292(s1) -- Store: [0x800145f8]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e4ec]:fmul.h t6, t5, t4, dyn
	-[0x8000e4f0]:csrrs a3, fcsr, zero
	-[0x8000e4f4]:sw t6, 296(s1)
Current Store : [0x8000e4f8] : sw a3, 300(s1) -- Store: [0x80014600]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e524]:fmul.h t6, t5, t4, dyn
	-[0x8000e528]:csrrs a3, fcsr, zero
	-[0x8000e52c]:sw t6, 304(s1)
Current Store : [0x8000e530] : sw a3, 308(s1) -- Store: [0x80014608]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e55c]:fmul.h t6, t5, t4, dyn
	-[0x8000e560]:csrrs a3, fcsr, zero
	-[0x8000e564]:sw t6, 312(s1)
Current Store : [0x8000e568] : sw a3, 316(s1) -- Store: [0x80014610]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e594]:fmul.h t6, t5, t4, dyn
	-[0x8000e598]:csrrs a3, fcsr, zero
	-[0x8000e59c]:sw t6, 320(s1)
Current Store : [0x8000e5a0] : sw a3, 324(s1) -- Store: [0x80014618]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e5cc]:fmul.h t6, t5, t4, dyn
	-[0x8000e5d0]:csrrs a3, fcsr, zero
	-[0x8000e5d4]:sw t6, 328(s1)
Current Store : [0x8000e5d8] : sw a3, 332(s1) -- Store: [0x80014620]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e604]:fmul.h t6, t5, t4, dyn
	-[0x8000e608]:csrrs a3, fcsr, zero
	-[0x8000e60c]:sw t6, 336(s1)
Current Store : [0x8000e610] : sw a3, 340(s1) -- Store: [0x80014628]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e63c]:fmul.h t6, t5, t4, dyn
	-[0x8000e640]:csrrs a3, fcsr, zero
	-[0x8000e644]:sw t6, 344(s1)
Current Store : [0x8000e648] : sw a3, 348(s1) -- Store: [0x80014630]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e674]:fmul.h t6, t5, t4, dyn
	-[0x8000e678]:csrrs a3, fcsr, zero
	-[0x8000e67c]:sw t6, 352(s1)
Current Store : [0x8000e680] : sw a3, 356(s1) -- Store: [0x80014638]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e6ac]:fmul.h t6, t5, t4, dyn
	-[0x8000e6b0]:csrrs a3, fcsr, zero
	-[0x8000e6b4]:sw t6, 360(s1)
Current Store : [0x8000e6b8] : sw a3, 364(s1) -- Store: [0x80014640]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e6e4]:fmul.h t6, t5, t4, dyn
	-[0x8000e6e8]:csrrs a3, fcsr, zero
	-[0x8000e6ec]:sw t6, 368(s1)
Current Store : [0x8000e6f0] : sw a3, 372(s1) -- Store: [0x80014648]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e71c]:fmul.h t6, t5, t4, dyn
	-[0x8000e720]:csrrs a3, fcsr, zero
	-[0x8000e724]:sw t6, 376(s1)
Current Store : [0x8000e728] : sw a3, 380(s1) -- Store: [0x80014650]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e754]:fmul.h t6, t5, t4, dyn
	-[0x8000e758]:csrrs a3, fcsr, zero
	-[0x8000e75c]:sw t6, 384(s1)
Current Store : [0x8000e760] : sw a3, 388(s1) -- Store: [0x80014658]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e78c]:fmul.h t6, t5, t4, dyn
	-[0x8000e790]:csrrs a3, fcsr, zero
	-[0x8000e794]:sw t6, 392(s1)
Current Store : [0x8000e798] : sw a3, 396(s1) -- Store: [0x80014660]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e7c4]:fmul.h t6, t5, t4, dyn
	-[0x8000e7c8]:csrrs a3, fcsr, zero
	-[0x8000e7cc]:sw t6, 400(s1)
Current Store : [0x8000e7d0] : sw a3, 404(s1) -- Store: [0x80014668]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e7fc]:fmul.h t6, t5, t4, dyn
	-[0x8000e800]:csrrs a3, fcsr, zero
	-[0x8000e804]:sw t6, 408(s1)
Current Store : [0x8000e808] : sw a3, 412(s1) -- Store: [0x80014670]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e834]:fmul.h t6, t5, t4, dyn
	-[0x8000e838]:csrrs a3, fcsr, zero
	-[0x8000e83c]:sw t6, 416(s1)
Current Store : [0x8000e840] : sw a3, 420(s1) -- Store: [0x80014678]:0x00000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e86c]:fmul.h t6, t5, t4, dyn
	-[0x8000e870]:csrrs a3, fcsr, zero
	-[0x8000e874]:sw t6, 424(s1)
Current Store : [0x8000e878] : sw a3, 428(s1) -- Store: [0x80014680]:0x00000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e8a4]:fmul.h t6, t5, t4, dyn
	-[0x8000e8a8]:csrrs a3, fcsr, zero
	-[0x8000e8ac]:sw t6, 432(s1)
Current Store : [0x8000e8b0] : sw a3, 436(s1) -- Store: [0x80014688]:0x00000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e8dc]:fmul.h t6, t5, t4, dyn
	-[0x8000e8e0]:csrrs a3, fcsr, zero
	-[0x8000e8e4]:sw t6, 440(s1)
Current Store : [0x8000e8e8] : sw a3, 444(s1) -- Store: [0x80014690]:0x00000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e914]:fmul.h t6, t5, t4, dyn
	-[0x8000e918]:csrrs a3, fcsr, zero
	-[0x8000e91c]:sw t6, 448(s1)
Current Store : [0x8000e920] : sw a3, 452(s1) -- Store: [0x80014698]:0x00000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e94c]:fmul.h t6, t5, t4, dyn
	-[0x8000e950]:csrrs a3, fcsr, zero
	-[0x8000e954]:sw t6, 456(s1)
Current Store : [0x8000e958] : sw a3, 460(s1) -- Store: [0x800146a0]:0x00000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e984]:fmul.h t6, t5, t4, dyn
	-[0x8000e988]:csrrs a3, fcsr, zero
	-[0x8000e98c]:sw t6, 464(s1)
Current Store : [0x8000e990] : sw a3, 468(s1) -- Store: [0x800146a8]:0x00000080




Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e9bc]:fmul.h t6, t5, t4, dyn
	-[0x8000e9c0]:csrrs a3, fcsr, zero
	-[0x8000e9c4]:sw t6, 472(s1)
Current Store : [0x8000e9c8] : sw a3, 476(s1) -- Store: [0x800146b0]:0x00000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000e9f4]:fmul.h t6, t5, t4, dyn
	-[0x8000e9f8]:csrrs a3, fcsr, zero
	-[0x8000e9fc]:sw t6, 480(s1)
Current Store : [0x8000ea00] : sw a3, 484(s1) -- Store: [0x800146b8]:0x00000000




Last Coverpoint : ['mnemonic : fmul.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000ea2c]:fmul.h t6, t5, t4, dyn
	-[0x8000ea30]:csrrs a3, fcsr, zero
	-[0x8000ea34]:sw t6, 488(s1)
Current Store : [0x8000ea38] : sw a3, 492(s1) -- Store: [0x800146c0]:0x00000020





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                                          coverpoints                                                                                                                                           |                                                      code                                                       |
|---:|-------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
|   1|[0x80012414]<br>0x00000000<br> |- mnemonic : fmul.h<br> - rs1 : x30<br> - rs2 : x31<br> - rd : x31<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:fmul.h t6, t5, t6, dyn<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8001241c]<br>0x00000000<br> |- rs1 : x29<br> - rs2 : x30<br> - rd : x29<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000144]:fmul.h t4, t4, t5, dyn<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t4, 8(ra)<br>      |
|   3|[0x80012424]<br>0x00007BFF<br> |- rs1 : x28<br> - rs2 : x28<br> - rd : x30<br> - rs1 == rs2 != rd<br>                                                                                                                                                                                                                           |[0x80000164]:fmul.h t5, t3, t3, dyn<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t5, 16(ra)<br>     |
|   4|[0x8001242c]<br>0x00000000<br> |- rs1 : x31<br> - rs2 : x29<br> - rd : x28<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000184]:fmul.h t3, t6, t4, dyn<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>     |
|   5|[0x80012434]<br>0x00007C00<br> |- rs1 : x27<br> - rs2 : x27<br> - rd : x27<br> - rs1 == rs2 == rd<br>                                                                                                                                                                                                                           |[0x800001a4]:fmul.h s11, s11, s11, dyn<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s11, 32(ra)<br> |
|   6|[0x8001243c]<br>0x00000000<br> |- rs1 : x25<br> - rs2 : x24<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800001c4]:fmul.h s10, s9, s8, dyn<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>   |
|   7|[0x80012444]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800001e4]:fmul.h s9, s8, s10, dyn<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8001244c]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x25<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000204]:fmul.h s8, s10, s9, dyn<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>    |
|   9|[0x80012454]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x21<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000224]:fmul.h s7, s6, s5, dyn<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8001245c]<br>0x00000000<br> |- rs1 : x21<br> - rs2 : x23<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x1a and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000244]:fmul.h s6, s5, s7, dyn<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80012464]<br>0x00000000<br> |- rs1 : x23<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000264]:fmul.h s5, s7, s6, dyn<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8001246c]<br>0x00000000<br> |- rs1 : x19<br> - rs2 : x18<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000284]:fmul.h s4, s3, s2, dyn<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80012474]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002a4]:fmul.h s3, s2, s4, dyn<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8001247c]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x19<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002c4]:fmul.h s2, s4, s3, dyn<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80012484]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x15<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x2ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x800002e4]:fmul.h a7, a6, a5, dyn<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8001248c]<br>0x00000000<br> |- rs1 : x15<br> - rs2 : x17<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x80000304]:fmul.h a6, a5, a7, dyn<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80012494]<br>0x00000000<br> |- rs1 : x17<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000324]:fmul.h a5, a7, a6, dyn<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8001249c]<br>0x00000000<br> |- rs1 : x13<br> - rs2 : x12<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000344]:fmul.h a4, a3, a2, dyn<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800124a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000364]:fmul.h a3, a2, a4, dyn<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800124ac]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x13<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x15a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                               |[0x80000384]:fmul.h a2, a4, a3, dyn<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800124b4]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x9<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                 |[0x800003a4]:fmul.h a1, a0, s1, dyn<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800124bc]<br>0x00000000<br> |- rs1 : x9<br> - rs2 : x11<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003cc]:fmul.h a0, s1, a1, dyn<br> [0x800003d0]:csrrs a3, fcsr, zero<br> [0x800003d4]:sw a0, 168(ra)<br>    |
|  23|[0x800124c4]<br>0x00000000<br> |- rs1 : x11<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                |[0x800003ec]:fmul.h s1, a1, a0, dyn<br> [0x800003f0]:csrrs a3, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800124cc]<br>0x00000000<br> |- rs1 : x7<br> - rs2 : x6<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x8000040c]:fmul.h fp, t2, t1, dyn<br> [0x80000410]:csrrs a3, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800124d4]<br>0x00000000<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000434]:fmul.h t2, t1, fp, dyn<br> [0x80000438]:csrrs a3, fcsr, zero<br> [0x8000043c]:sw t2, 0(s1)<br>      |
|  26|[0x800124dc]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x7<br> - rd : x6<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                   |[0x80000454]:fmul.h t1, fp, t2, dyn<br> [0x80000458]:csrrs a3, fcsr, zero<br> [0x8000045c]:sw t1, 8(s1)<br>      |
|  27|[0x800124e4]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x3<br> - rd : x5<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000474]:fmul.h t0, tp, gp, dyn<br> [0x80000478]:csrrs a3, fcsr, zero<br> [0x8000047c]:sw t0, 16(s1)<br>     |
|  28|[0x800124ec]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x5<br> - rd : x4<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000494]:fmul.h tp, gp, t0, dyn<br> [0x80000498]:csrrs a3, fcsr, zero<br> [0x8000049c]:sw tp, 24(s1)<br>     |
|  29|[0x800124f4]<br>0x00000000<br> |- rs1 : x5<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004b4]:fmul.h gp, t0, tp, dyn<br> [0x800004b8]:csrrs a3, fcsr, zero<br> [0x800004bc]:sw gp, 32(s1)<br>     |
|  30|[0x800124fc]<br>0x00000000<br> |- rs1 : x1<br> - rs2 : x0<br> - rd : x2<br> - fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x800004d4]:fmul.h sp, ra, zero, dyn<br> [0x800004d8]:csrrs a3, fcsr, zero<br> [0x800004dc]:sw sp, 40(s1)<br>   |
|  31|[0x80012504]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                                                     |[0x800004f4]:fmul.h ra, zero, sp, dyn<br> [0x800004f8]:csrrs a3, fcsr, zero<br> [0x800004fc]:sw ra, 48(s1)<br>   |
|  32|[0x8001250c]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x1<br> - rd : x0<br> - fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                  |[0x80000514]:fmul.h zero, sp, ra, dyn<br> [0x80000518]:csrrs a3, fcsr, zero<br> [0x8000051c]:sw zero, 56(s1)<br> |
|  33|[0x80012514]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000534]:fmul.h t6, t5, t4, dyn<br> [0x80000538]:csrrs a3, fcsr, zero<br> [0x8000053c]:sw t6, 64(s1)<br>     |
|  34|[0x8001251c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000554]:fmul.h t6, t5, t4, dyn<br> [0x80000558]:csrrs a3, fcsr, zero<br> [0x8000055c]:sw t6, 72(s1)<br>     |
|  35|[0x80012524]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000574]:fmul.h t6, t5, t4, dyn<br> [0x80000578]:csrrs a3, fcsr, zero<br> [0x8000057c]:sw t6, 80(s1)<br>     |
|  36|[0x8001252c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000594]:fmul.h t6, t5, t4, dyn<br> [0x80000598]:csrrs a3, fcsr, zero<br> [0x8000059c]:sw t6, 88(s1)<br>     |
|  37|[0x80012534]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005b4]:fmul.h t6, t5, t4, dyn<br> [0x800005b8]:csrrs a3, fcsr, zero<br> [0x800005bc]:sw t6, 96(s1)<br>     |
|  38|[0x8001253c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005d4]:fmul.h t6, t5, t4, dyn<br> [0x800005d8]:csrrs a3, fcsr, zero<br> [0x800005dc]:sw t6, 104(s1)<br>    |
|  39|[0x80012544]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800005f4]:fmul.h t6, t5, t4, dyn<br> [0x800005f8]:csrrs a3, fcsr, zero<br> [0x800005fc]:sw t6, 112(s1)<br>    |
|  40|[0x8001254c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000614]:fmul.h t6, t5, t4, dyn<br> [0x80000618]:csrrs a3, fcsr, zero<br> [0x8000061c]:sw t6, 120(s1)<br>    |
|  41|[0x80012554]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000634]:fmul.h t6, t5, t4, dyn<br> [0x80000638]:csrrs a3, fcsr, zero<br> [0x8000063c]:sw t6, 128(s1)<br>    |
|  42|[0x8001255c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000654]:fmul.h t6, t5, t4, dyn<br> [0x80000658]:csrrs a3, fcsr, zero<br> [0x8000065c]:sw t6, 136(s1)<br>    |
|  43|[0x80012564]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000674]:fmul.h t6, t5, t4, dyn<br> [0x80000678]:csrrs a3, fcsr, zero<br> [0x8000067c]:sw t6, 144(s1)<br>    |
|  44|[0x8001256c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000694]:fmul.h t6, t5, t4, dyn<br> [0x80000698]:csrrs a3, fcsr, zero<br> [0x8000069c]:sw t6, 152(s1)<br>    |
|  45|[0x80012574]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x397 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006b4]:fmul.h t6, t5, t4, dyn<br> [0x800006b8]:csrrs a3, fcsr, zero<br> [0x800006bc]:sw t6, 160(s1)<br>    |
|  46|[0x8001257c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800006d4]:fmul.h t6, t5, t4, dyn<br> [0x800006d8]:csrrs a3, fcsr, zero<br> [0x800006dc]:sw t6, 168(s1)<br>    |
|  47|[0x80012584]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800006f4]:fmul.h t6, t5, t4, dyn<br> [0x800006f8]:csrrs a3, fcsr, zero<br> [0x800006fc]:sw t6, 176(s1)<br>    |
|  48|[0x8001258c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000714]:fmul.h t6, t5, t4, dyn<br> [0x80000718]:csrrs a3, fcsr, zero<br> [0x8000071c]:sw t6, 184(s1)<br>    |
|  49|[0x80012594]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000734]:fmul.h t6, t5, t4, dyn<br> [0x80000738]:csrrs a3, fcsr, zero<br> [0x8000073c]:sw t6, 192(s1)<br>    |
|  50|[0x8001259c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x31d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000754]:fmul.h t6, t5, t4, dyn<br> [0x80000758]:csrrs a3, fcsr, zero<br> [0x8000075c]:sw t6, 200(s1)<br>    |
|  51|[0x800125a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000774]:fmul.h t6, t5, t4, dyn<br> [0x80000778]:csrrs a3, fcsr, zero<br> [0x8000077c]:sw t6, 208(s1)<br>    |
|  52|[0x800125ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000794]:fmul.h t6, t5, t4, dyn<br> [0x80000798]:csrrs a3, fcsr, zero<br> [0x8000079c]:sw t6, 216(s1)<br>    |
|  53|[0x800125b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007b4]:fmul.h t6, t5, t4, dyn<br> [0x800007b8]:csrrs a3, fcsr, zero<br> [0x800007bc]:sw t6, 224(s1)<br>    |
|  54|[0x800125bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007d4]:fmul.h t6, t5, t4, dyn<br> [0x800007d8]:csrrs a3, fcsr, zero<br> [0x800007dc]:sw t6, 232(s1)<br>    |
|  55|[0x800125c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x099 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800007f4]:fmul.h t6, t5, t4, dyn<br> [0x800007f8]:csrrs a3, fcsr, zero<br> [0x800007fc]:sw t6, 240(s1)<br>    |
|  56|[0x800125cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000814]:fmul.h t6, t5, t4, dyn<br> [0x80000818]:csrrs a3, fcsr, zero<br> [0x8000081c]:sw t6, 248(s1)<br>    |
|  57|[0x800125d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000834]:fmul.h t6, t5, t4, dyn<br> [0x80000838]:csrrs a3, fcsr, zero<br> [0x8000083c]:sw t6, 256(s1)<br>    |
|  58|[0x800125dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000854]:fmul.h t6, t5, t4, dyn<br> [0x80000858]:csrrs a3, fcsr, zero<br> [0x8000085c]:sw t6, 264(s1)<br>    |
|  59|[0x800125e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000874]:fmul.h t6, t5, t4, dyn<br> [0x80000878]:csrrs a3, fcsr, zero<br> [0x8000087c]:sw t6, 272(s1)<br>    |
|  60|[0x800125ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000894]:fmul.h t6, t5, t4, dyn<br> [0x80000898]:csrrs a3, fcsr, zero<br> [0x8000089c]:sw t6, 280(s1)<br>    |
|  61|[0x800125f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800008b4]:fmul.h t6, t5, t4, dyn<br> [0x800008b8]:csrrs a3, fcsr, zero<br> [0x800008bc]:sw t6, 288(s1)<br>    |
|  62|[0x800125fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008d4]:fmul.h t6, t5, t4, dyn<br> [0x800008d8]:csrrs a3, fcsr, zero<br> [0x800008dc]:sw t6, 296(s1)<br>    |
|  63|[0x80012604]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800008f4]:fmul.h t6, t5, t4, dyn<br> [0x800008f8]:csrrs a3, fcsr, zero<br> [0x800008fc]:sw t6, 304(s1)<br>    |
|  64|[0x8001260c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000914]:fmul.h t6, t5, t4, dyn<br> [0x80000918]:csrrs a3, fcsr, zero<br> [0x8000091c]:sw t6, 312(s1)<br>    |
|  65|[0x80012614]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x213 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000934]:fmul.h t6, t5, t4, dyn<br> [0x80000938]:csrrs a3, fcsr, zero<br> [0x8000093c]:sw t6, 320(s1)<br>    |
|  66|[0x8001261c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000954]:fmul.h t6, t5, t4, dyn<br> [0x80000958]:csrrs a3, fcsr, zero<br> [0x8000095c]:sw t6, 328(s1)<br>    |
|  67|[0x80012624]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000974]:fmul.h t6, t5, t4, dyn<br> [0x80000978]:csrrs a3, fcsr, zero<br> [0x8000097c]:sw t6, 336(s1)<br>    |
|  68|[0x8001262c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000994]:fmul.h t6, t5, t4, dyn<br> [0x80000998]:csrrs a3, fcsr, zero<br> [0x8000099c]:sw t6, 344(s1)<br>    |
|  69|[0x80012634]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009b4]:fmul.h t6, t5, t4, dyn<br> [0x800009b8]:csrrs a3, fcsr, zero<br> [0x800009bc]:sw t6, 352(s1)<br>    |
|  70|[0x8001263c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x034 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800009d4]:fmul.h t6, t5, t4, dyn<br> [0x800009d8]:csrrs a3, fcsr, zero<br> [0x800009dc]:sw t6, 360(s1)<br>    |
|  71|[0x80012644]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800009f4]:fmul.h t6, t5, t4, dyn<br> [0x800009f8]:csrrs a3, fcsr, zero<br> [0x800009fc]:sw t6, 368(s1)<br>    |
|  72|[0x8001264c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a14]:fmul.h t6, t5, t4, dyn<br> [0x80000a18]:csrrs a3, fcsr, zero<br> [0x80000a1c]:sw t6, 376(s1)<br>    |
|  73|[0x80012654]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a34]:fmul.h t6, t5, t4, dyn<br> [0x80000a38]:csrrs a3, fcsr, zero<br> [0x80000a3c]:sw t6, 384(s1)<br>    |
|  74|[0x8001265c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a54]:fmul.h t6, t5, t4, dyn<br> [0x80000a58]:csrrs a3, fcsr, zero<br> [0x80000a5c]:sw t6, 392(s1)<br>    |
|  75|[0x80012664]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x38d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000a74]:fmul.h t6, t5, t4, dyn<br> [0x80000a78]:csrrs a3, fcsr, zero<br> [0x80000a7c]:sw t6, 400(s1)<br>    |
|  76|[0x8001266c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000a94]:fmul.h t6, t5, t4, dyn<br> [0x80000a98]:csrrs a3, fcsr, zero<br> [0x80000a9c]:sw t6, 408(s1)<br>    |
|  77|[0x80012674]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ab4]:fmul.h t6, t5, t4, dyn<br> [0x80000ab8]:csrrs a3, fcsr, zero<br> [0x80000abc]:sw t6, 416(s1)<br>    |
|  78|[0x8001267c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ad4]:fmul.h t6, t5, t4, dyn<br> [0x80000ad8]:csrrs a3, fcsr, zero<br> [0x80000adc]:sw t6, 424(s1)<br>    |
|  79|[0x80012684]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000af4]:fmul.h t6, t5, t4, dyn<br> [0x80000af8]:csrrs a3, fcsr, zero<br> [0x80000afc]:sw t6, 432(s1)<br>    |
|  80|[0x8001268c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x133 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b14]:fmul.h t6, t5, t4, dyn<br> [0x80000b18]:csrrs a3, fcsr, zero<br> [0x80000b1c]:sw t6, 440(s1)<br>    |
|  81|[0x80012694]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000b34]:fmul.h t6, t5, t4, dyn<br> [0x80000b38]:csrrs a3, fcsr, zero<br> [0x80000b3c]:sw t6, 448(s1)<br>    |
|  82|[0x8001269c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b54]:fmul.h t6, t5, t4, dyn<br> [0x80000b58]:csrrs a3, fcsr, zero<br> [0x80000b5c]:sw t6, 456(s1)<br>    |
|  83|[0x800126a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b74]:fmul.h t6, t5, t4, dyn<br> [0x80000b78]:csrrs a3, fcsr, zero<br> [0x80000b7c]:sw t6, 464(s1)<br>    |
|  84|[0x800126ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000b94]:fmul.h t6, t5, t4, dyn<br> [0x80000b98]:csrrs a3, fcsr, zero<br> [0x80000b9c]:sw t6, 472(s1)<br>    |
|  85|[0x800126b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x014 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bb4]:fmul.h t6, t5, t4, dyn<br> [0x80000bb8]:csrrs a3, fcsr, zero<br> [0x80000bbc]:sw t6, 480(s1)<br>    |
|  86|[0x800126bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000bd4]:fmul.h t6, t5, t4, dyn<br> [0x80000bd8]:csrrs a3, fcsr, zero<br> [0x80000bdc]:sw t6, 488(s1)<br>    |
|  87|[0x800126c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000bf4]:fmul.h t6, t5, t4, dyn<br> [0x80000bf8]:csrrs a3, fcsr, zero<br> [0x80000bfc]:sw t6, 496(s1)<br>    |
|  88|[0x800126cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c14]:fmul.h t6, t5, t4, dyn<br> [0x80000c18]:csrrs a3, fcsr, zero<br> [0x80000c1c]:sw t6, 504(s1)<br>    |
|  89|[0x800126d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c34]:fmul.h t6, t5, t4, dyn<br> [0x80000c38]:csrrs a3, fcsr, zero<br> [0x80000c3c]:sw t6, 512(s1)<br>    |
|  90|[0x800126dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x164 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c54]:fmul.h t6, t5, t4, dyn<br> [0x80000c58]:csrrs a3, fcsr, zero<br> [0x80000c5c]:sw t6, 520(s1)<br>    |
|  91|[0x800126e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000c74]:fmul.h t6, t5, t4, dyn<br> [0x80000c78]:csrrs a3, fcsr, zero<br> [0x80000c7c]:sw t6, 528(s1)<br>    |
|  92|[0x800126ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000c94]:fmul.h t6, t5, t4, dyn<br> [0x80000c98]:csrrs a3, fcsr, zero<br> [0x80000c9c]:sw t6, 536(s1)<br>    |
|  93|[0x800126f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cb4]:fmul.h t6, t5, t4, dyn<br> [0x80000cb8]:csrrs a3, fcsr, zero<br> [0x80000cbc]:sw t6, 544(s1)<br>    |
|  94|[0x800126fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cd4]:fmul.h t6, t5, t4, dyn<br> [0x80000cd8]:csrrs a3, fcsr, zero<br> [0x80000cdc]:sw t6, 552(s1)<br>    |
|  95|[0x80012704]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x325 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000cf4]:fmul.h t6, t5, t4, dyn<br> [0x80000cf8]:csrrs a3, fcsr, zero<br> [0x80000cfc]:sw t6, 560(s1)<br>    |
|  96|[0x8001270c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000d14]:fmul.h t6, t5, t4, dyn<br> [0x80000d18]:csrrs a3, fcsr, zero<br> [0x80000d1c]:sw t6, 568(s1)<br>    |
|  97|[0x80012714]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d34]:fmul.h t6, t5, t4, dyn<br> [0x80000d38]:csrrs a3, fcsr, zero<br> [0x80000d3c]:sw t6, 576(s1)<br>    |
|  98|[0x8001271c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d54]:fmul.h t6, t5, t4, dyn<br> [0x80000d58]:csrrs a3, fcsr, zero<br> [0x80000d5c]:sw t6, 584(s1)<br>    |
|  99|[0x80012724]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d74]:fmul.h t6, t5, t4, dyn<br> [0x80000d78]:csrrs a3, fcsr, zero<br> [0x80000d7c]:sw t6, 592(s1)<br>    |
| 100|[0x8001272c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000d94]:fmul.h t6, t5, t4, dyn<br> [0x80000d98]:csrrs a3, fcsr, zero<br> [0x80000d9c]:sw t6, 600(s1)<br>    |
| 101|[0x80012734]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000db4]:fmul.h t6, t5, t4, dyn<br> [0x80000db8]:csrrs a3, fcsr, zero<br> [0x80000dbc]:sw t6, 608(s1)<br>    |
| 102|[0x8001273c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000dd4]:fmul.h t6, t5, t4, dyn<br> [0x80000dd8]:csrrs a3, fcsr, zero<br> [0x80000ddc]:sw t6, 616(s1)<br>    |
| 103|[0x80012744]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000df4]:fmul.h t6, t5, t4, dyn<br> [0x80000df8]:csrrs a3, fcsr, zero<br> [0x80000dfc]:sw t6, 624(s1)<br>    |
| 104|[0x8001274c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e14]:fmul.h t6, t5, t4, dyn<br> [0x80000e18]:csrrs a3, fcsr, zero<br> [0x80000e1c]:sw t6, 632(s1)<br>    |
| 105|[0x80012754]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e34]:fmul.h t6, t5, t4, dyn<br> [0x80000e38]:csrrs a3, fcsr, zero<br> [0x80000e3c]:sw t6, 640(s1)<br>    |
| 106|[0x8001275c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000e54]:fmul.h t6, t5, t4, dyn<br> [0x80000e58]:csrrs a3, fcsr, zero<br> [0x80000e5c]:sw t6, 648(s1)<br>    |
| 107|[0x80012764]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e74]:fmul.h t6, t5, t4, dyn<br> [0x80000e78]:csrrs a3, fcsr, zero<br> [0x80000e7c]:sw t6, 656(s1)<br>    |
| 108|[0x8001276c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000e94]:fmul.h t6, t5, t4, dyn<br> [0x80000e98]:csrrs a3, fcsr, zero<br> [0x80000e9c]:sw t6, 664(s1)<br>    |
| 109|[0x80012774]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000eb4]:fmul.h t6, t5, t4, dyn<br> [0x80000eb8]:csrrs a3, fcsr, zero<br> [0x80000ebc]:sw t6, 672(s1)<br>    |
| 110|[0x8001277c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ed4]:fmul.h t6, t5, t4, dyn<br> [0x80000ed8]:csrrs a3, fcsr, zero<br> [0x80000edc]:sw t6, 680(s1)<br>    |
| 111|[0x80012784]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000ef4]:fmul.h t6, t5, t4, dyn<br> [0x80000ef8]:csrrs a3, fcsr, zero<br> [0x80000efc]:sw t6, 688(s1)<br>    |
| 112|[0x8001278c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f14]:fmul.h t6, t5, t4, dyn<br> [0x80000f18]:csrrs a3, fcsr, zero<br> [0x80000f1c]:sw t6, 696(s1)<br>    |
| 113|[0x80012794]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f34]:fmul.h t6, t5, t4, dyn<br> [0x80000f38]:csrrs a3, fcsr, zero<br> [0x80000f3c]:sw t6, 704(s1)<br>    |
| 114|[0x8001279c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f54]:fmul.h t6, t5, t4, dyn<br> [0x80000f58]:csrrs a3, fcsr, zero<br> [0x80000f5c]:sw t6, 712(s1)<br>    |
| 115|[0x800127a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000f74]:fmul.h t6, t5, t4, dyn<br> [0x80000f78]:csrrs a3, fcsr, zero<br> [0x80000f7c]:sw t6, 720(s1)<br>    |
| 116|[0x800127ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80000f94]:fmul.h t6, t5, t4, dyn<br> [0x80000f98]:csrrs a3, fcsr, zero<br> [0x80000f9c]:sw t6, 728(s1)<br>    |
| 117|[0x800127b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000fb4]:fmul.h t6, t5, t4, dyn<br> [0x80000fb8]:csrrs a3, fcsr, zero<br> [0x80000fbc]:sw t6, 736(s1)<br>    |
| 118|[0x800127bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000fd4]:fmul.h t6, t5, t4, dyn<br> [0x80000fd8]:csrrs a3, fcsr, zero<br> [0x80000fdc]:sw t6, 744(s1)<br>    |
| 119|[0x800127c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80000ff4]:fmul.h t6, t5, t4, dyn<br> [0x80000ff8]:csrrs a3, fcsr, zero<br> [0x80000ffc]:sw t6, 752(s1)<br>    |
| 120|[0x800127cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x207 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001014]:fmul.h t6, t5, t4, dyn<br> [0x80001018]:csrrs a3, fcsr, zero<br> [0x8000101c]:sw t6, 760(s1)<br>    |
| 121|[0x800127d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001034]:fmul.h t6, t5, t4, dyn<br> [0x80001038]:csrrs a3, fcsr, zero<br> [0x8000103c]:sw t6, 768(s1)<br>    |
| 122|[0x800127dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001054]:fmul.h t6, t5, t4, dyn<br> [0x80001058]:csrrs a3, fcsr, zero<br> [0x8000105c]:sw t6, 776(s1)<br>    |
| 123|[0x800127e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001074]:fmul.h t6, t5, t4, dyn<br> [0x80001078]:csrrs a3, fcsr, zero<br> [0x8000107c]:sw t6, 784(s1)<br>    |
| 124|[0x800127ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001094]:fmul.h t6, t5, t4, dyn<br> [0x80001098]:csrrs a3, fcsr, zero<br> [0x8000109c]:sw t6, 792(s1)<br>    |
| 125|[0x800127f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x361 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800010b4]:fmul.h t6, t5, t4, dyn<br> [0x800010b8]:csrrs a3, fcsr, zero<br> [0x800010bc]:sw t6, 800(s1)<br>    |
| 126|[0x800127fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800010d4]:fmul.h t6, t5, t4, dyn<br> [0x800010d8]:csrrs a3, fcsr, zero<br> [0x800010dc]:sw t6, 808(s1)<br>    |
| 127|[0x80012804]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800010f4]:fmul.h t6, t5, t4, dyn<br> [0x800010f8]:csrrs a3, fcsr, zero<br> [0x800010fc]:sw t6, 816(s1)<br>    |
| 128|[0x8001280c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001114]:fmul.h t6, t5, t4, dyn<br> [0x80001118]:csrrs a3, fcsr, zero<br> [0x8000111c]:sw t6, 824(s1)<br>    |
| 129|[0x80012814]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001134]:fmul.h t6, t5, t4, dyn<br> [0x80001138]:csrrs a3, fcsr, zero<br> [0x8000113c]:sw t6, 832(s1)<br>    |
| 130|[0x8001281c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001154]:fmul.h t6, t5, t4, dyn<br> [0x80001158]:csrrs a3, fcsr, zero<br> [0x8000115c]:sw t6, 840(s1)<br>    |
| 131|[0x80012824]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001174]:fmul.h t6, t5, t4, dyn<br> [0x80001178]:csrrs a3, fcsr, zero<br> [0x8000117c]:sw t6, 848(s1)<br>    |
| 132|[0x8001282c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001194]:fmul.h t6, t5, t4, dyn<br> [0x80001198]:csrrs a3, fcsr, zero<br> [0x8000119c]:sw t6, 856(s1)<br>    |
| 133|[0x80012834]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011b4]:fmul.h t6, t5, t4, dyn<br> [0x800011b8]:csrrs a3, fcsr, zero<br> [0x800011bc]:sw t6, 864(s1)<br>    |
| 134|[0x8001283c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011d4]:fmul.h t6, t5, t4, dyn<br> [0x800011d8]:csrrs a3, fcsr, zero<br> [0x800011dc]:sw t6, 872(s1)<br>    |
| 135|[0x80012844]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800011f4]:fmul.h t6, t5, t4, dyn<br> [0x800011f8]:csrrs a3, fcsr, zero<br> [0x800011fc]:sw t6, 880(s1)<br>    |
| 136|[0x8001284c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001214]:fmul.h t6, t5, t4, dyn<br> [0x80001218]:csrrs a3, fcsr, zero<br> [0x8000121c]:sw t6, 888(s1)<br>    |
| 137|[0x80012854]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001234]:fmul.h t6, t5, t4, dyn<br> [0x80001238]:csrrs a3, fcsr, zero<br> [0x8000123c]:sw t6, 896(s1)<br>    |
| 138|[0x8001285c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001254]:fmul.h t6, t5, t4, dyn<br> [0x80001258]:csrrs a3, fcsr, zero<br> [0x8000125c]:sw t6, 904(s1)<br>    |
| 139|[0x80012864]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001274]:fmul.h t6, t5, t4, dyn<br> [0x80001278]:csrrs a3, fcsr, zero<br> [0x8000127c]:sw t6, 912(s1)<br>    |
| 140|[0x8001286c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001294]:fmul.h t6, t5, t4, dyn<br> [0x80001298]:csrrs a3, fcsr, zero<br> [0x8000129c]:sw t6, 920(s1)<br>    |
| 141|[0x80012874]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800012b4]:fmul.h t6, t5, t4, dyn<br> [0x800012b8]:csrrs a3, fcsr, zero<br> [0x800012bc]:sw t6, 928(s1)<br>    |
| 142|[0x8001287c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800012d4]:fmul.h t6, t5, t4, dyn<br> [0x800012d8]:csrrs a3, fcsr, zero<br> [0x800012dc]:sw t6, 936(s1)<br>    |
| 143|[0x80012884]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800012f4]:fmul.h t6, t5, t4, dyn<br> [0x800012f8]:csrrs a3, fcsr, zero<br> [0x800012fc]:sw t6, 944(s1)<br>    |
| 144|[0x8001288c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001314]:fmul.h t6, t5, t4, dyn<br> [0x80001318]:csrrs a3, fcsr, zero<br> [0x8000131c]:sw t6, 952(s1)<br>    |
| 145|[0x80012894]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x331 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001334]:fmul.h t6, t5, t4, dyn<br> [0x80001338]:csrrs a3, fcsr, zero<br> [0x8000133c]:sw t6, 960(s1)<br>    |
| 146|[0x8001289c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001354]:fmul.h t6, t5, t4, dyn<br> [0x80001358]:csrrs a3, fcsr, zero<br> [0x8000135c]:sw t6, 968(s1)<br>    |
| 147|[0x800128a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001374]:fmul.h t6, t5, t4, dyn<br> [0x80001378]:csrrs a3, fcsr, zero<br> [0x8000137c]:sw t6, 976(s1)<br>    |
| 148|[0x800128ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001394]:fmul.h t6, t5, t4, dyn<br> [0x80001398]:csrrs a3, fcsr, zero<br> [0x8000139c]:sw t6, 984(s1)<br>    |
| 149|[0x800128b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800013b4]:fmul.h t6, t5, t4, dyn<br> [0x800013b8]:csrrs a3, fcsr, zero<br> [0x800013bc]:sw t6, 992(s1)<br>    |
| 150|[0x800128bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x08a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800013d4]:fmul.h t6, t5, t4, dyn<br> [0x800013d8]:csrrs a3, fcsr, zero<br> [0x800013dc]:sw t6, 1000(s1)<br>   |
| 151|[0x800128c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800013f4]:fmul.h t6, t5, t4, dyn<br> [0x800013f8]:csrrs a3, fcsr, zero<br> [0x800013fc]:sw t6, 1008(s1)<br>   |
| 152|[0x800128cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001414]:fmul.h t6, t5, t4, dyn<br> [0x80001418]:csrrs a3, fcsr, zero<br> [0x8000141c]:sw t6, 1016(s1)<br>   |
| 153|[0x800128d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000143c]:fmul.h t6, t5, t4, dyn<br> [0x80001440]:csrrs a3, fcsr, zero<br> [0x80001444]:sw t6, 0(s1)<br>      |
| 154|[0x800128dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000145c]:fmul.h t6, t5, t4, dyn<br> [0x80001460]:csrrs a3, fcsr, zero<br> [0x80001464]:sw t6, 8(s1)<br>      |
| 155|[0x800128e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000147c]:fmul.h t6, t5, t4, dyn<br> [0x80001480]:csrrs a3, fcsr, zero<br> [0x80001484]:sw t6, 16(s1)<br>     |
| 156|[0x800128ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000149c]:fmul.h t6, t5, t4, dyn<br> [0x800014a0]:csrrs a3, fcsr, zero<br> [0x800014a4]:sw t6, 24(s1)<br>     |
| 157|[0x800128f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800014bc]:fmul.h t6, t5, t4, dyn<br> [0x800014c0]:csrrs a3, fcsr, zero<br> [0x800014c4]:sw t6, 32(s1)<br>     |
| 158|[0x800128fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800014dc]:fmul.h t6, t5, t4, dyn<br> [0x800014e0]:csrrs a3, fcsr, zero<br> [0x800014e4]:sw t6, 40(s1)<br>     |
| 159|[0x80012904]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800014fc]:fmul.h t6, t5, t4, dyn<br> [0x80001500]:csrrs a3, fcsr, zero<br> [0x80001504]:sw t6, 48(s1)<br>     |
| 160|[0x8001290c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x318 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000151c]:fmul.h t6, t5, t4, dyn<br> [0x80001520]:csrrs a3, fcsr, zero<br> [0x80001524]:sw t6, 56(s1)<br>     |
| 161|[0x80012914]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000153c]:fmul.h t6, t5, t4, dyn<br> [0x80001540]:csrrs a3, fcsr, zero<br> [0x80001544]:sw t6, 64(s1)<br>     |
| 162|[0x8001291c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000155c]:fmul.h t6, t5, t4, dyn<br> [0x80001560]:csrrs a3, fcsr, zero<br> [0x80001564]:sw t6, 72(s1)<br>     |
| 163|[0x80012924]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000157c]:fmul.h t6, t5, t4, dyn<br> [0x80001580]:csrrs a3, fcsr, zero<br> [0x80001584]:sw t6, 80(s1)<br>     |
| 164|[0x8001292c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000159c]:fmul.h t6, t5, t4, dyn<br> [0x800015a0]:csrrs a3, fcsr, zero<br> [0x800015a4]:sw t6, 88(s1)<br>     |
| 165|[0x80012934]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x198 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800015bc]:fmul.h t6, t5, t4, dyn<br> [0x800015c0]:csrrs a3, fcsr, zero<br> [0x800015c4]:sw t6, 96(s1)<br>     |
| 166|[0x8001293c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800015dc]:fmul.h t6, t5, t4, dyn<br> [0x800015e0]:csrrs a3, fcsr, zero<br> [0x800015e4]:sw t6, 104(s1)<br>    |
| 167|[0x80012944]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800015fc]:fmul.h t6, t5, t4, dyn<br> [0x80001600]:csrrs a3, fcsr, zero<br> [0x80001604]:sw t6, 112(s1)<br>    |
| 168|[0x8001294c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000161c]:fmul.h t6, t5, t4, dyn<br> [0x80001620]:csrrs a3, fcsr, zero<br> [0x80001624]:sw t6, 120(s1)<br>    |
| 169|[0x80012954]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000163c]:fmul.h t6, t5, t4, dyn<br> [0x80001640]:csrrs a3, fcsr, zero<br> [0x80001644]:sw t6, 128(s1)<br>    |
| 170|[0x8001295c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x342 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000165c]:fmul.h t6, t5, t4, dyn<br> [0x80001660]:csrrs a3, fcsr, zero<br> [0x80001664]:sw t6, 136(s1)<br>    |
| 171|[0x80012964]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000167c]:fmul.h t6, t5, t4, dyn<br> [0x80001680]:csrrs a3, fcsr, zero<br> [0x80001684]:sw t6, 144(s1)<br>    |
| 172|[0x8001296c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000169c]:fmul.h t6, t5, t4, dyn<br> [0x800016a0]:csrrs a3, fcsr, zero<br> [0x800016a4]:sw t6, 152(s1)<br>    |
| 173|[0x80012974]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800016bc]:fmul.h t6, t5, t4, dyn<br> [0x800016c0]:csrrs a3, fcsr, zero<br> [0x800016c4]:sw t6, 160(s1)<br>    |
| 174|[0x8001297c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800016dc]:fmul.h t6, t5, t4, dyn<br> [0x800016e0]:csrrs a3, fcsr, zero<br> [0x800016e4]:sw t6, 168(s1)<br>    |
| 175|[0x80012984]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x349 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800016fc]:fmul.h t6, t5, t4, dyn<br> [0x80001700]:csrrs a3, fcsr, zero<br> [0x80001704]:sw t6, 176(s1)<br>    |
| 176|[0x8001298c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000171c]:fmul.h t6, t5, t4, dyn<br> [0x80001720]:csrrs a3, fcsr, zero<br> [0x80001724]:sw t6, 184(s1)<br>    |
| 177|[0x80012994]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000173c]:fmul.h t6, t5, t4, dyn<br> [0x80001740]:csrrs a3, fcsr, zero<br> [0x80001744]:sw t6, 192(s1)<br>    |
| 178|[0x8001299c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000175c]:fmul.h t6, t5, t4, dyn<br> [0x80001760]:csrrs a3, fcsr, zero<br> [0x80001764]:sw t6, 200(s1)<br>    |
| 179|[0x800129a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000177c]:fmul.h t6, t5, t4, dyn<br> [0x80001780]:csrrs a3, fcsr, zero<br> [0x80001784]:sw t6, 208(s1)<br>    |
| 180|[0x800129ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000179c]:fmul.h t6, t5, t4, dyn<br> [0x800017a0]:csrrs a3, fcsr, zero<br> [0x800017a4]:sw t6, 216(s1)<br>    |
| 181|[0x800129b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800017bc]:fmul.h t6, t5, t4, dyn<br> [0x800017c0]:csrrs a3, fcsr, zero<br> [0x800017c4]:sw t6, 224(s1)<br>    |
| 182|[0x800129bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800017dc]:fmul.h t6, t5, t4, dyn<br> [0x800017e0]:csrrs a3, fcsr, zero<br> [0x800017e4]:sw t6, 232(s1)<br>    |
| 183|[0x800129c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800017fc]:fmul.h t6, t5, t4, dyn<br> [0x80001800]:csrrs a3, fcsr, zero<br> [0x80001804]:sw t6, 240(s1)<br>    |
| 184|[0x800129cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000181c]:fmul.h t6, t5, t4, dyn<br> [0x80001820]:csrrs a3, fcsr, zero<br> [0x80001824]:sw t6, 248(s1)<br>    |
| 185|[0x800129d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x008 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000183c]:fmul.h t6, t5, t4, dyn<br> [0x80001840]:csrrs a3, fcsr, zero<br> [0x80001844]:sw t6, 256(s1)<br>    |
| 186|[0x800129dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000185c]:fmul.h t6, t5, t4, dyn<br> [0x80001860]:csrrs a3, fcsr, zero<br> [0x80001864]:sw t6, 264(s1)<br>    |
| 187|[0x800129e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000187c]:fmul.h t6, t5, t4, dyn<br> [0x80001880]:csrrs a3, fcsr, zero<br> [0x80001884]:sw t6, 272(s1)<br>    |
| 188|[0x800129ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000189c]:fmul.h t6, t5, t4, dyn<br> [0x800018a0]:csrrs a3, fcsr, zero<br> [0x800018a4]:sw t6, 280(s1)<br>    |
| 189|[0x800129f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800018bc]:fmul.h t6, t5, t4, dyn<br> [0x800018c0]:csrrs a3, fcsr, zero<br> [0x800018c4]:sw t6, 288(s1)<br>    |
| 190|[0x800129fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x135 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800018dc]:fmul.h t6, t5, t4, dyn<br> [0x800018e0]:csrrs a3, fcsr, zero<br> [0x800018e4]:sw t6, 296(s1)<br>    |
| 191|[0x80012a04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800018fc]:fmul.h t6, t5, t4, dyn<br> [0x80001900]:csrrs a3, fcsr, zero<br> [0x80001904]:sw t6, 304(s1)<br>    |
| 192|[0x80012a0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000191c]:fmul.h t6, t5, t4, dyn<br> [0x80001920]:csrrs a3, fcsr, zero<br> [0x80001924]:sw t6, 312(s1)<br>    |
| 193|[0x80012a14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000193c]:fmul.h t6, t5, t4, dyn<br> [0x80001940]:csrrs a3, fcsr, zero<br> [0x80001944]:sw t6, 320(s1)<br>    |
| 194|[0x80012a1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000195c]:fmul.h t6, t5, t4, dyn<br> [0x80001960]:csrrs a3, fcsr, zero<br> [0x80001964]:sw t6, 328(s1)<br>    |
| 195|[0x80012a24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000197c]:fmul.h t6, t5, t4, dyn<br> [0x80001980]:csrrs a3, fcsr, zero<br> [0x80001984]:sw t6, 336(s1)<br>    |
| 196|[0x80012a2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000199c]:fmul.h t6, t5, t4, dyn<br> [0x800019a0]:csrrs a3, fcsr, zero<br> [0x800019a4]:sw t6, 344(s1)<br>    |
| 197|[0x80012a34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800019bc]:fmul.h t6, t5, t4, dyn<br> [0x800019c0]:csrrs a3, fcsr, zero<br> [0x800019c4]:sw t6, 352(s1)<br>    |
| 198|[0x80012a3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800019dc]:fmul.h t6, t5, t4, dyn<br> [0x800019e0]:csrrs a3, fcsr, zero<br> [0x800019e4]:sw t6, 360(s1)<br>    |
| 199|[0x80012a44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800019fc]:fmul.h t6, t5, t4, dyn<br> [0x80001a00]:csrrs a3, fcsr, zero<br> [0x80001a04]:sw t6, 368(s1)<br>    |
| 200|[0x80012a4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x017 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001a1c]:fmul.h t6, t5, t4, dyn<br> [0x80001a20]:csrrs a3, fcsr, zero<br> [0x80001a24]:sw t6, 376(s1)<br>    |
| 201|[0x80012a54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001a3c]:fmul.h t6, t5, t4, dyn<br> [0x80001a40]:csrrs a3, fcsr, zero<br> [0x80001a44]:sw t6, 384(s1)<br>    |
| 202|[0x80012a5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001a5c]:fmul.h t6, t5, t4, dyn<br> [0x80001a60]:csrrs a3, fcsr, zero<br> [0x80001a64]:sw t6, 392(s1)<br>    |
| 203|[0x80012a64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001a7c]:fmul.h t6, t5, t4, dyn<br> [0x80001a80]:csrrs a3, fcsr, zero<br> [0x80001a84]:sw t6, 400(s1)<br>    |
| 204|[0x80012a6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001a9c]:fmul.h t6, t5, t4, dyn<br> [0x80001aa0]:csrrs a3, fcsr, zero<br> [0x80001aa4]:sw t6, 408(s1)<br>    |
| 205|[0x80012a74]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001abc]:fmul.h t6, t5, t4, dyn<br> [0x80001ac0]:csrrs a3, fcsr, zero<br> [0x80001ac4]:sw t6, 416(s1)<br>    |
| 206|[0x80012a7c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001adc]:fmul.h t6, t5, t4, dyn<br> [0x80001ae0]:csrrs a3, fcsr, zero<br> [0x80001ae4]:sw t6, 424(s1)<br>    |
| 207|[0x80012a84]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001afc]:fmul.h t6, t5, t4, dyn<br> [0x80001b00]:csrrs a3, fcsr, zero<br> [0x80001b04]:sw t6, 432(s1)<br>    |
| 208|[0x80012a8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001b1c]:fmul.h t6, t5, t4, dyn<br> [0x80001b20]:csrrs a3, fcsr, zero<br> [0x80001b24]:sw t6, 440(s1)<br>    |
| 209|[0x80012a94]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001b3c]:fmul.h t6, t5, t4, dyn<br> [0x80001b40]:csrrs a3, fcsr, zero<br> [0x80001b44]:sw t6, 448(s1)<br>    |
| 210|[0x80012a9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001b5c]:fmul.h t6, t5, t4, dyn<br> [0x80001b60]:csrrs a3, fcsr, zero<br> [0x80001b64]:sw t6, 456(s1)<br>    |
| 211|[0x80012aa4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001b7c]:fmul.h t6, t5, t4, dyn<br> [0x80001b80]:csrrs a3, fcsr, zero<br> [0x80001b84]:sw t6, 464(s1)<br>    |
| 212|[0x80012aac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001b9c]:fmul.h t6, t5, t4, dyn<br> [0x80001ba0]:csrrs a3, fcsr, zero<br> [0x80001ba4]:sw t6, 472(s1)<br>    |
| 213|[0x80012ab4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001bbc]:fmul.h t6, t5, t4, dyn<br> [0x80001bc0]:csrrs a3, fcsr, zero<br> [0x80001bc4]:sw t6, 480(s1)<br>    |
| 214|[0x80012abc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001bdc]:fmul.h t6, t5, t4, dyn<br> [0x80001be0]:csrrs a3, fcsr, zero<br> [0x80001be4]:sw t6, 488(s1)<br>    |
| 215|[0x80012ac4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x341 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001bfc]:fmul.h t6, t5, t4, dyn<br> [0x80001c00]:csrrs a3, fcsr, zero<br> [0x80001c04]:sw t6, 496(s1)<br>    |
| 216|[0x80012acc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001c1c]:fmul.h t6, t5, t4, dyn<br> [0x80001c20]:csrrs a3, fcsr, zero<br> [0x80001c24]:sw t6, 504(s1)<br>    |
| 217|[0x80012ad4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001c3c]:fmul.h t6, t5, t4, dyn<br> [0x80001c40]:csrrs a3, fcsr, zero<br> [0x80001c44]:sw t6, 512(s1)<br>    |
| 218|[0x80012adc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001c5c]:fmul.h t6, t5, t4, dyn<br> [0x80001c60]:csrrs a3, fcsr, zero<br> [0x80001c64]:sw t6, 520(s1)<br>    |
| 219|[0x80012ae4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001c7c]:fmul.h t6, t5, t4, dyn<br> [0x80001c80]:csrrs a3, fcsr, zero<br> [0x80001c84]:sw t6, 528(s1)<br>    |
| 220|[0x80012aec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001c9c]:fmul.h t6, t5, t4, dyn<br> [0x80001ca0]:csrrs a3, fcsr, zero<br> [0x80001ca4]:sw t6, 536(s1)<br>    |
| 221|[0x80012af4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001cbc]:fmul.h t6, t5, t4, dyn<br> [0x80001cc0]:csrrs a3, fcsr, zero<br> [0x80001cc4]:sw t6, 544(s1)<br>    |
| 222|[0x80012afc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001cdc]:fmul.h t6, t5, t4, dyn<br> [0x80001ce0]:csrrs a3, fcsr, zero<br> [0x80001ce4]:sw t6, 552(s1)<br>    |
| 223|[0x80012b04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001cfc]:fmul.h t6, t5, t4, dyn<br> [0x80001d00]:csrrs a3, fcsr, zero<br> [0x80001d04]:sw t6, 560(s1)<br>    |
| 224|[0x80012b0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001d1c]:fmul.h t6, t5, t4, dyn<br> [0x80001d20]:csrrs a3, fcsr, zero<br> [0x80001d24]:sw t6, 568(s1)<br>    |
| 225|[0x80012b14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x138 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001d3c]:fmul.h t6, t5, t4, dyn<br> [0x80001d40]:csrrs a3, fcsr, zero<br> [0x80001d44]:sw t6, 576(s1)<br>    |
| 226|[0x80012b1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001d5c]:fmul.h t6, t5, t4, dyn<br> [0x80001d60]:csrrs a3, fcsr, zero<br> [0x80001d64]:sw t6, 584(s1)<br>    |
| 227|[0x80012b24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001d7c]:fmul.h t6, t5, t4, dyn<br> [0x80001d80]:csrrs a3, fcsr, zero<br> [0x80001d84]:sw t6, 592(s1)<br>    |
| 228|[0x80012b2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001d9c]:fmul.h t6, t5, t4, dyn<br> [0x80001da0]:csrrs a3, fcsr, zero<br> [0x80001da4]:sw t6, 600(s1)<br>    |
| 229|[0x80012b34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001dbc]:fmul.h t6, t5, t4, dyn<br> [0x80001dc0]:csrrs a3, fcsr, zero<br> [0x80001dc4]:sw t6, 608(s1)<br>    |
| 230|[0x80012b3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x33f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001ddc]:fmul.h t6, t5, t4, dyn<br> [0x80001de0]:csrrs a3, fcsr, zero<br> [0x80001de4]:sw t6, 616(s1)<br>    |
| 231|[0x80012b44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001dfc]:fmul.h t6, t5, t4, dyn<br> [0x80001e00]:csrrs a3, fcsr, zero<br> [0x80001e04]:sw t6, 624(s1)<br>    |
| 232|[0x80012b4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001e1c]:fmul.h t6, t5, t4, dyn<br> [0x80001e20]:csrrs a3, fcsr, zero<br> [0x80001e24]:sw t6, 632(s1)<br>    |
| 233|[0x80012b54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001e3c]:fmul.h t6, t5, t4, dyn<br> [0x80001e40]:csrrs a3, fcsr, zero<br> [0x80001e44]:sw t6, 640(s1)<br>    |
| 234|[0x80012b5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001e5c]:fmul.h t6, t5, t4, dyn<br> [0x80001e60]:csrrs a3, fcsr, zero<br> [0x80001e64]:sw t6, 648(s1)<br>    |
| 235|[0x80012b64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x2cc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001e7c]:fmul.h t6, t5, t4, dyn<br> [0x80001e80]:csrrs a3, fcsr, zero<br> [0x80001e84]:sw t6, 656(s1)<br>    |
| 236|[0x80012b6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001e9c]:fmul.h t6, t5, t4, dyn<br> [0x80001ea0]:csrrs a3, fcsr, zero<br> [0x80001ea4]:sw t6, 664(s1)<br>    |
| 237|[0x80012b74]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001ebc]:fmul.h t6, t5, t4, dyn<br> [0x80001ec0]:csrrs a3, fcsr, zero<br> [0x80001ec4]:sw t6, 672(s1)<br>    |
| 238|[0x80012b7c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001edc]:fmul.h t6, t5, t4, dyn<br> [0x80001ee0]:csrrs a3, fcsr, zero<br> [0x80001ee4]:sw t6, 680(s1)<br>    |
| 239|[0x80012b84]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001efc]:fmul.h t6, t5, t4, dyn<br> [0x80001f00]:csrrs a3, fcsr, zero<br> [0x80001f04]:sw t6, 688(s1)<br>    |
| 240|[0x80012b8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2d4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001f1c]:fmul.h t6, t5, t4, dyn<br> [0x80001f20]:csrrs a3, fcsr, zero<br> [0x80001f24]:sw t6, 696(s1)<br>    |
| 241|[0x80012b94]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001f3c]:fmul.h t6, t5, t4, dyn<br> [0x80001f40]:csrrs a3, fcsr, zero<br> [0x80001f44]:sw t6, 704(s1)<br>    |
| 242|[0x80012b9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001f5c]:fmul.h t6, t5, t4, dyn<br> [0x80001f60]:csrrs a3, fcsr, zero<br> [0x80001f64]:sw t6, 712(s1)<br>    |
| 243|[0x80012ba4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001f7c]:fmul.h t6, t5, t4, dyn<br> [0x80001f80]:csrrs a3, fcsr, zero<br> [0x80001f84]:sw t6, 720(s1)<br>    |
| 244|[0x80012bac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001f9c]:fmul.h t6, t5, t4, dyn<br> [0x80001fa0]:csrrs a3, fcsr, zero<br> [0x80001fa4]:sw t6, 728(s1)<br>    |
| 245|[0x80012bb4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2bb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001fbc]:fmul.h t6, t5, t4, dyn<br> [0x80001fc0]:csrrs a3, fcsr, zero<br> [0x80001fc4]:sw t6, 736(s1)<br>    |
| 246|[0x80012bbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80001fdc]:fmul.h t6, t5, t4, dyn<br> [0x80001fe0]:csrrs a3, fcsr, zero<br> [0x80001fe4]:sw t6, 744(s1)<br>    |
| 247|[0x80012bc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80001ffc]:fmul.h t6, t5, t4, dyn<br> [0x80002000]:csrrs a3, fcsr, zero<br> [0x80002004]:sw t6, 752(s1)<br>    |
| 248|[0x80012bcc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000201c]:fmul.h t6, t5, t4, dyn<br> [0x80002020]:csrrs a3, fcsr, zero<br> [0x80002024]:sw t6, 760(s1)<br>    |
| 249|[0x80012bd4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000203c]:fmul.h t6, t5, t4, dyn<br> [0x80002040]:csrrs a3, fcsr, zero<br> [0x80002044]:sw t6, 768(s1)<br>    |
| 250|[0x80012bdc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2c3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000205c]:fmul.h t6, t5, t4, dyn<br> [0x80002060]:csrrs a3, fcsr, zero<br> [0x80002064]:sw t6, 776(s1)<br>    |
| 251|[0x80012be4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000207c]:fmul.h t6, t5, t4, dyn<br> [0x80002080]:csrrs a3, fcsr, zero<br> [0x80002084]:sw t6, 784(s1)<br>    |
| 252|[0x80012bec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000209c]:fmul.h t6, t5, t4, dyn<br> [0x800020a0]:csrrs a3, fcsr, zero<br> [0x800020a4]:sw t6, 792(s1)<br>    |
| 253|[0x80012bf4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800020bc]:fmul.h t6, t5, t4, dyn<br> [0x800020c0]:csrrs a3, fcsr, zero<br> [0x800020c4]:sw t6, 800(s1)<br>    |
| 254|[0x80012bfc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800020dc]:fmul.h t6, t5, t4, dyn<br> [0x800020e0]:csrrs a3, fcsr, zero<br> [0x800020e4]:sw t6, 808(s1)<br>    |
| 255|[0x80012c04]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x014 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800020fc]:fmul.h t6, t5, t4, dyn<br> [0x80002100]:csrrs a3, fcsr, zero<br> [0x80002104]:sw t6, 816(s1)<br>    |
| 256|[0x80012c0c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000211c]:fmul.h t6, t5, t4, dyn<br> [0x80002120]:csrrs a3, fcsr, zero<br> [0x80002124]:sw t6, 824(s1)<br>    |
| 257|[0x80012c14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000213c]:fmul.h t6, t5, t4, dyn<br> [0x80002140]:csrrs a3, fcsr, zero<br> [0x80002144]:sw t6, 832(s1)<br>    |
| 258|[0x80012c1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000215c]:fmul.h t6, t5, t4, dyn<br> [0x80002160]:csrrs a3, fcsr, zero<br> [0x80002164]:sw t6, 840(s1)<br>    |
| 259|[0x80012c24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000217c]:fmul.h t6, t5, t4, dyn<br> [0x80002180]:csrrs a3, fcsr, zero<br> [0x80002184]:sw t6, 848(s1)<br>    |
| 260|[0x80012c2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x17f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000219c]:fmul.h t6, t5, t4, dyn<br> [0x800021a0]:csrrs a3, fcsr, zero<br> [0x800021a4]:sw t6, 856(s1)<br>    |
| 261|[0x80012c34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800021bc]:fmul.h t6, t5, t4, dyn<br> [0x800021c0]:csrrs a3, fcsr, zero<br> [0x800021c4]:sw t6, 864(s1)<br>    |
| 262|[0x80012c3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800021dc]:fmul.h t6, t5, t4, dyn<br> [0x800021e0]:csrrs a3, fcsr, zero<br> [0x800021e4]:sw t6, 872(s1)<br>    |
| 263|[0x80012c44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800021fc]:fmul.h t6, t5, t4, dyn<br> [0x80002200]:csrrs a3, fcsr, zero<br> [0x80002204]:sw t6, 880(s1)<br>    |
| 264|[0x80012c4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000221c]:fmul.h t6, t5, t4, dyn<br> [0x80002220]:csrrs a3, fcsr, zero<br> [0x80002224]:sw t6, 888(s1)<br>    |
| 265|[0x80012c54]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x14d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000223c]:fmul.h t6, t5, t4, dyn<br> [0x80002240]:csrrs a3, fcsr, zero<br> [0x80002244]:sw t6, 896(s1)<br>    |
| 266|[0x80012c5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000225c]:fmul.h t6, t5, t4, dyn<br> [0x80002260]:csrrs a3, fcsr, zero<br> [0x80002264]:sw t6, 904(s1)<br>    |
| 267|[0x80012c64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000227c]:fmul.h t6, t5, t4, dyn<br> [0x80002280]:csrrs a3, fcsr, zero<br> [0x80002284]:sw t6, 912(s1)<br>    |
| 268|[0x80012c6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000229c]:fmul.h t6, t5, t4, dyn<br> [0x800022a0]:csrrs a3, fcsr, zero<br> [0x800022a4]:sw t6, 920(s1)<br>    |
| 269|[0x80012c74]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800022bc]:fmul.h t6, t5, t4, dyn<br> [0x800022c0]:csrrs a3, fcsr, zero<br> [0x800022c4]:sw t6, 928(s1)<br>    |
| 270|[0x80012c7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x27d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800022dc]:fmul.h t6, t5, t4, dyn<br> [0x800022e0]:csrrs a3, fcsr, zero<br> [0x800022e4]:sw t6, 936(s1)<br>    |
| 271|[0x80012c84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800022fc]:fmul.h t6, t5, t4, dyn<br> [0x80002300]:csrrs a3, fcsr, zero<br> [0x80002304]:sw t6, 944(s1)<br>    |
| 272|[0x80012c8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000231c]:fmul.h t6, t5, t4, dyn<br> [0x80002320]:csrrs a3, fcsr, zero<br> [0x80002324]:sw t6, 952(s1)<br>    |
| 273|[0x80012c94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000233c]:fmul.h t6, t5, t4, dyn<br> [0x80002340]:csrrs a3, fcsr, zero<br> [0x80002344]:sw t6, 960(s1)<br>    |
| 274|[0x80012c9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000235c]:fmul.h t6, t5, t4, dyn<br> [0x80002360]:csrrs a3, fcsr, zero<br> [0x80002364]:sw t6, 968(s1)<br>    |
| 275|[0x80012ca4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x16a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000237c]:fmul.h t6, t5, t4, dyn<br> [0x80002380]:csrrs a3, fcsr, zero<br> [0x80002384]:sw t6, 976(s1)<br>    |
| 276|[0x80012cac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000239c]:fmul.h t6, t5, t4, dyn<br> [0x800023a0]:csrrs a3, fcsr, zero<br> [0x800023a4]:sw t6, 984(s1)<br>    |
| 277|[0x80012cb4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800023bc]:fmul.h t6, t5, t4, dyn<br> [0x800023c0]:csrrs a3, fcsr, zero<br> [0x800023c4]:sw t6, 992(s1)<br>    |
| 278|[0x80012cbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800023fc]:fmul.h t6, t5, t4, dyn<br> [0x80002400]:csrrs a3, fcsr, zero<br> [0x80002404]:sw t6, 1000(s1)<br>   |
| 279|[0x80012cc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000243c]:fmul.h t6, t5, t4, dyn<br> [0x80002440]:csrrs a3, fcsr, zero<br> [0x80002444]:sw t6, 1008(s1)<br>   |
| 280|[0x80012ccc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x280 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000247c]:fmul.h t6, t5, t4, dyn<br> [0x80002480]:csrrs a3, fcsr, zero<br> [0x80002484]:sw t6, 1016(s1)<br>   |
| 281|[0x80012cd4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800024c4]:fmul.h t6, t5, t4, dyn<br> [0x800024c8]:csrrs a3, fcsr, zero<br> [0x800024cc]:sw t6, 0(s1)<br>      |
| 282|[0x80012cdc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002504]:fmul.h t6, t5, t4, dyn<br> [0x80002508]:csrrs a3, fcsr, zero<br> [0x8000250c]:sw t6, 8(s1)<br>      |
| 283|[0x80012ce4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002544]:fmul.h t6, t5, t4, dyn<br> [0x80002548]:csrrs a3, fcsr, zero<br> [0x8000254c]:sw t6, 16(s1)<br>     |
| 284|[0x80012cec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002584]:fmul.h t6, t5, t4, dyn<br> [0x80002588]:csrrs a3, fcsr, zero<br> [0x8000258c]:sw t6, 24(s1)<br>     |
| 285|[0x80012cf4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1e0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800025c4]:fmul.h t6, t5, t4, dyn<br> [0x800025c8]:csrrs a3, fcsr, zero<br> [0x800025cc]:sw t6, 32(s1)<br>     |
| 286|[0x80012cfc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002604]:fmul.h t6, t5, t4, dyn<br> [0x80002608]:csrrs a3, fcsr, zero<br> [0x8000260c]:sw t6, 40(s1)<br>     |
| 287|[0x80012d04]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002644]:fmul.h t6, t5, t4, dyn<br> [0x80002648]:csrrs a3, fcsr, zero<br> [0x8000264c]:sw t6, 48(s1)<br>     |
| 288|[0x80012d0c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002684]:fmul.h t6, t5, t4, dyn<br> [0x80002688]:csrrs a3, fcsr, zero<br> [0x8000268c]:sw t6, 56(s1)<br>     |
| 289|[0x80012d14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800026c4]:fmul.h t6, t5, t4, dyn<br> [0x800026c8]:csrrs a3, fcsr, zero<br> [0x800026cc]:sw t6, 64(s1)<br>     |
| 290|[0x80012d1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x22a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002704]:fmul.h t6, t5, t4, dyn<br> [0x80002708]:csrrs a3, fcsr, zero<br> [0x8000270c]:sw t6, 72(s1)<br>     |
| 291|[0x80012d24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002744]:fmul.h t6, t5, t4, dyn<br> [0x80002748]:csrrs a3, fcsr, zero<br> [0x8000274c]:sw t6, 80(s1)<br>     |
| 292|[0x80012d2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002784]:fmul.h t6, t5, t4, dyn<br> [0x80002788]:csrrs a3, fcsr, zero<br> [0x8000278c]:sw t6, 88(s1)<br>     |
| 293|[0x80012d34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800027c4]:fmul.h t6, t5, t4, dyn<br> [0x800027c8]:csrrs a3, fcsr, zero<br> [0x800027cc]:sw t6, 96(s1)<br>     |
| 294|[0x80012d3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002804]:fmul.h t6, t5, t4, dyn<br> [0x80002808]:csrrs a3, fcsr, zero<br> [0x8000280c]:sw t6, 104(s1)<br>    |
| 295|[0x80012d44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ea and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002844]:fmul.h t6, t5, t4, dyn<br> [0x80002848]:csrrs a3, fcsr, zero<br> [0x8000284c]:sw t6, 112(s1)<br>    |
| 296|[0x80012d4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002884]:fmul.h t6, t5, t4, dyn<br> [0x80002888]:csrrs a3, fcsr, zero<br> [0x8000288c]:sw t6, 120(s1)<br>    |
| 297|[0x80012d54]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800028c4]:fmul.h t6, t5, t4, dyn<br> [0x800028c8]:csrrs a3, fcsr, zero<br> [0x800028cc]:sw t6, 128(s1)<br>    |
| 298|[0x80012d5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002904]:fmul.h t6, t5, t4, dyn<br> [0x80002908]:csrrs a3, fcsr, zero<br> [0x8000290c]:sw t6, 136(s1)<br>    |
| 299|[0x80012d64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002944]:fmul.h t6, t5, t4, dyn<br> [0x80002948]:csrrs a3, fcsr, zero<br> [0x8000294c]:sw t6, 144(s1)<br>    |
| 300|[0x80012d6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x0a4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002984]:fmul.h t6, t5, t4, dyn<br> [0x80002988]:csrrs a3, fcsr, zero<br> [0x8000298c]:sw t6, 152(s1)<br>    |
| 301|[0x80012d74]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800029c4]:fmul.h t6, t5, t4, dyn<br> [0x800029c8]:csrrs a3, fcsr, zero<br> [0x800029cc]:sw t6, 160(s1)<br>    |
| 302|[0x80012d7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002a04]:fmul.h t6, t5, t4, dyn<br> [0x80002a08]:csrrs a3, fcsr, zero<br> [0x80002a0c]:sw t6, 168(s1)<br>    |
| 303|[0x80012d84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002a44]:fmul.h t6, t5, t4, dyn<br> [0x80002a48]:csrrs a3, fcsr, zero<br> [0x80002a4c]:sw t6, 176(s1)<br>    |
| 304|[0x80012d8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002a84]:fmul.h t6, t5, t4, dyn<br> [0x80002a88]:csrrs a3, fcsr, zero<br> [0x80002a8c]:sw t6, 184(s1)<br>    |
| 305|[0x80012d94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0eb and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002ac4]:fmul.h t6, t5, t4, dyn<br> [0x80002ac8]:csrrs a3, fcsr, zero<br> [0x80002acc]:sw t6, 192(s1)<br>    |
| 306|[0x80012d9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002b04]:fmul.h t6, t5, t4, dyn<br> [0x80002b08]:csrrs a3, fcsr, zero<br> [0x80002b0c]:sw t6, 200(s1)<br>    |
| 307|[0x80012da4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002b44]:fmul.h t6, t5, t4, dyn<br> [0x80002b48]:csrrs a3, fcsr, zero<br> [0x80002b4c]:sw t6, 208(s1)<br>    |
| 308|[0x80012dac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002b84]:fmul.h t6, t5, t4, dyn<br> [0x80002b88]:csrrs a3, fcsr, zero<br> [0x80002b8c]:sw t6, 216(s1)<br>    |
| 309|[0x80012db4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002bc4]:fmul.h t6, t5, t4, dyn<br> [0x80002bc8]:csrrs a3, fcsr, zero<br> [0x80002bcc]:sw t6, 224(s1)<br>    |
| 310|[0x80012dbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x33c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002c04]:fmul.h t6, t5, t4, dyn<br> [0x80002c08]:csrrs a3, fcsr, zero<br> [0x80002c0c]:sw t6, 232(s1)<br>    |
| 311|[0x80012dc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002c44]:fmul.h t6, t5, t4, dyn<br> [0x80002c48]:csrrs a3, fcsr, zero<br> [0x80002c4c]:sw t6, 240(s1)<br>    |
| 312|[0x80012dcc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002c84]:fmul.h t6, t5, t4, dyn<br> [0x80002c88]:csrrs a3, fcsr, zero<br> [0x80002c8c]:sw t6, 248(s1)<br>    |
| 313|[0x80012dd4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002cc4]:fmul.h t6, t5, t4, dyn<br> [0x80002cc8]:csrrs a3, fcsr, zero<br> [0x80002ccc]:sw t6, 256(s1)<br>    |
| 314|[0x80012ddc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002d04]:fmul.h t6, t5, t4, dyn<br> [0x80002d08]:csrrs a3, fcsr, zero<br> [0x80002d0c]:sw t6, 264(s1)<br>    |
| 315|[0x80012de4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x3e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002d44]:fmul.h t6, t5, t4, dyn<br> [0x80002d48]:csrrs a3, fcsr, zero<br> [0x80002d4c]:sw t6, 272(s1)<br>    |
| 316|[0x80012dec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002d84]:fmul.h t6, t5, t4, dyn<br> [0x80002d88]:csrrs a3, fcsr, zero<br> [0x80002d8c]:sw t6, 280(s1)<br>    |
| 317|[0x80012df4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002dc4]:fmul.h t6, t5, t4, dyn<br> [0x80002dc8]:csrrs a3, fcsr, zero<br> [0x80002dcc]:sw t6, 288(s1)<br>    |
| 318|[0x80012dfc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002e04]:fmul.h t6, t5, t4, dyn<br> [0x80002e08]:csrrs a3, fcsr, zero<br> [0x80002e0c]:sw t6, 296(s1)<br>    |
| 319|[0x80012e04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002e44]:fmul.h t6, t5, t4, dyn<br> [0x80002e48]:csrrs a3, fcsr, zero<br> [0x80002e4c]:sw t6, 304(s1)<br>    |
| 320|[0x80012e0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x21f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002e84]:fmul.h t6, t5, t4, dyn<br> [0x80002e88]:csrrs a3, fcsr, zero<br> [0x80002e8c]:sw t6, 312(s1)<br>    |
| 321|[0x80012e14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80002ec4]:fmul.h t6, t5, t4, dyn<br> [0x80002ec8]:csrrs a3, fcsr, zero<br> [0x80002ecc]:sw t6, 320(s1)<br>    |
| 322|[0x80012e1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002f04]:fmul.h t6, t5, t4, dyn<br> [0x80002f08]:csrrs a3, fcsr, zero<br> [0x80002f0c]:sw t6, 328(s1)<br>    |
| 323|[0x80012e24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002f44]:fmul.h t6, t5, t4, dyn<br> [0x80002f48]:csrrs a3, fcsr, zero<br> [0x80002f4c]:sw t6, 336(s1)<br>    |
| 324|[0x80012e2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002f84]:fmul.h t6, t5, t4, dyn<br> [0x80002f88]:csrrs a3, fcsr, zero<br> [0x80002f8c]:sw t6, 344(s1)<br>    |
| 325|[0x80012e34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x336 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80002fc4]:fmul.h t6, t5, t4, dyn<br> [0x80002fc8]:csrrs a3, fcsr, zero<br> [0x80002fcc]:sw t6, 352(s1)<br>    |
| 326|[0x80012e3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003004]:fmul.h t6, t5, t4, dyn<br> [0x80003008]:csrrs a3, fcsr, zero<br> [0x8000300c]:sw t6, 360(s1)<br>    |
| 327|[0x80012e44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003044]:fmul.h t6, t5, t4, dyn<br> [0x80003048]:csrrs a3, fcsr, zero<br> [0x8000304c]:sw t6, 368(s1)<br>    |
| 328|[0x80012e4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003084]:fmul.h t6, t5, t4, dyn<br> [0x80003088]:csrrs a3, fcsr, zero<br> [0x8000308c]:sw t6, 376(s1)<br>    |
| 329|[0x80012e54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800030c4]:fmul.h t6, t5, t4, dyn<br> [0x800030c8]:csrrs a3, fcsr, zero<br> [0x800030cc]:sw t6, 384(s1)<br>    |
| 330|[0x80012e5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003104]:fmul.h t6, t5, t4, dyn<br> [0x80003108]:csrrs a3, fcsr, zero<br> [0x8000310c]:sw t6, 392(s1)<br>    |
| 331|[0x80012e64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003144]:fmul.h t6, t5, t4, dyn<br> [0x80003148]:csrrs a3, fcsr, zero<br> [0x8000314c]:sw t6, 400(s1)<br>    |
| 332|[0x80012e6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003184]:fmul.h t6, t5, t4, dyn<br> [0x80003188]:csrrs a3, fcsr, zero<br> [0x8000318c]:sw t6, 408(s1)<br>    |
| 333|[0x80012e74]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800031c4]:fmul.h t6, t5, t4, dyn<br> [0x800031c8]:csrrs a3, fcsr, zero<br> [0x800031cc]:sw t6, 416(s1)<br>    |
| 334|[0x80012e7c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003204]:fmul.h t6, t5, t4, dyn<br> [0x80003208]:csrrs a3, fcsr, zero<br> [0x8000320c]:sw t6, 424(s1)<br>    |
| 335|[0x80012e84]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x38f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003244]:fmul.h t6, t5, t4, dyn<br> [0x80003248]:csrrs a3, fcsr, zero<br> [0x8000324c]:sw t6, 432(s1)<br>    |
| 336|[0x80012e8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003284]:fmul.h t6, t5, t4, dyn<br> [0x80003288]:csrrs a3, fcsr, zero<br> [0x8000328c]:sw t6, 440(s1)<br>    |
| 337|[0x80012e94]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800032c4]:fmul.h t6, t5, t4, dyn<br> [0x800032c8]:csrrs a3, fcsr, zero<br> [0x800032cc]:sw t6, 448(s1)<br>    |
| 338|[0x80012e9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003304]:fmul.h t6, t5, t4, dyn<br> [0x80003308]:csrrs a3, fcsr, zero<br> [0x8000330c]:sw t6, 456(s1)<br>    |
| 339|[0x80012ea4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003344]:fmul.h t6, t5, t4, dyn<br> [0x80003348]:csrrs a3, fcsr, zero<br> [0x8000334c]:sw t6, 464(s1)<br>    |
| 340|[0x80012eac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x148 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003384]:fmul.h t6, t5, t4, dyn<br> [0x80003388]:csrrs a3, fcsr, zero<br> [0x8000338c]:sw t6, 472(s1)<br>    |
| 341|[0x80012eb4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800033c4]:fmul.h t6, t5, t4, dyn<br> [0x800033c8]:csrrs a3, fcsr, zero<br> [0x800033cc]:sw t6, 480(s1)<br>    |
| 342|[0x80012ebc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003404]:fmul.h t6, t5, t4, dyn<br> [0x80003408]:csrrs a3, fcsr, zero<br> [0x8000340c]:sw t6, 488(s1)<br>    |
| 343|[0x80012ec4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003444]:fmul.h t6, t5, t4, dyn<br> [0x80003448]:csrrs a3, fcsr, zero<br> [0x8000344c]:sw t6, 496(s1)<br>    |
| 344|[0x80012ecc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003484]:fmul.h t6, t5, t4, dyn<br> [0x80003488]:csrrs a3, fcsr, zero<br> [0x8000348c]:sw t6, 504(s1)<br>    |
| 345|[0x80012ed4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x287 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800034c4]:fmul.h t6, t5, t4, dyn<br> [0x800034c8]:csrrs a3, fcsr, zero<br> [0x800034cc]:sw t6, 512(s1)<br>    |
| 346|[0x80012edc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003504]:fmul.h t6, t5, t4, dyn<br> [0x80003508]:csrrs a3, fcsr, zero<br> [0x8000350c]:sw t6, 520(s1)<br>    |
| 347|[0x80012ee4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003544]:fmul.h t6, t5, t4, dyn<br> [0x80003548]:csrrs a3, fcsr, zero<br> [0x8000354c]:sw t6, 528(s1)<br>    |
| 348|[0x80012eec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003584]:fmul.h t6, t5, t4, dyn<br> [0x80003588]:csrrs a3, fcsr, zero<br> [0x8000358c]:sw t6, 536(s1)<br>    |
| 349|[0x80012ef4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800035c4]:fmul.h t6, t5, t4, dyn<br> [0x800035c8]:csrrs a3, fcsr, zero<br> [0x800035cc]:sw t6, 544(s1)<br>    |
| 350|[0x80012efc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003604]:fmul.h t6, t5, t4, dyn<br> [0x80003608]:csrrs a3, fcsr, zero<br> [0x8000360c]:sw t6, 552(s1)<br>    |
| 351|[0x80012f04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003644]:fmul.h t6, t5, t4, dyn<br> [0x80003648]:csrrs a3, fcsr, zero<br> [0x8000364c]:sw t6, 560(s1)<br>    |
| 352|[0x80012f0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003684]:fmul.h t6, t5, t4, dyn<br> [0x80003688]:csrrs a3, fcsr, zero<br> [0x8000368c]:sw t6, 568(s1)<br>    |
| 353|[0x80012f14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800036c4]:fmul.h t6, t5, t4, dyn<br> [0x800036c8]:csrrs a3, fcsr, zero<br> [0x800036cc]:sw t6, 576(s1)<br>    |
| 354|[0x80012f1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003704]:fmul.h t6, t5, t4, dyn<br> [0x80003708]:csrrs a3, fcsr, zero<br> [0x8000370c]:sw t6, 584(s1)<br>    |
| 355|[0x80012f24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0ad and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003744]:fmul.h t6, t5, t4, dyn<br> [0x80003748]:csrrs a3, fcsr, zero<br> [0x8000374c]:sw t6, 592(s1)<br>    |
| 356|[0x80012f2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003784]:fmul.h t6, t5, t4, dyn<br> [0x80003788]:csrrs a3, fcsr, zero<br> [0x8000378c]:sw t6, 600(s1)<br>    |
| 357|[0x80012f34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800037c4]:fmul.h t6, t5, t4, dyn<br> [0x800037c8]:csrrs a3, fcsr, zero<br> [0x800037cc]:sw t6, 608(s1)<br>    |
| 358|[0x80012f3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003804]:fmul.h t6, t5, t4, dyn<br> [0x80003808]:csrrs a3, fcsr, zero<br> [0x8000380c]:sw t6, 616(s1)<br>    |
| 359|[0x80012f44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003844]:fmul.h t6, t5, t4, dyn<br> [0x80003848]:csrrs a3, fcsr, zero<br> [0x8000384c]:sw t6, 624(s1)<br>    |
| 360|[0x80012f4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x01d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003884]:fmul.h t6, t5, t4, dyn<br> [0x80003888]:csrrs a3, fcsr, zero<br> [0x8000388c]:sw t6, 632(s1)<br>    |
| 361|[0x80012f54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800038c4]:fmul.h t6, t5, t4, dyn<br> [0x800038c8]:csrrs a3, fcsr, zero<br> [0x800038cc]:sw t6, 640(s1)<br>    |
| 362|[0x80012f5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003904]:fmul.h t6, t5, t4, dyn<br> [0x80003908]:csrrs a3, fcsr, zero<br> [0x8000390c]:sw t6, 648(s1)<br>    |
| 363|[0x80012f64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003944]:fmul.h t6, t5, t4, dyn<br> [0x80003948]:csrrs a3, fcsr, zero<br> [0x8000394c]:sw t6, 656(s1)<br>    |
| 364|[0x80012f6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003984]:fmul.h t6, t5, t4, dyn<br> [0x80003988]:csrrs a3, fcsr, zero<br> [0x8000398c]:sw t6, 664(s1)<br>    |
| 365|[0x80012f74]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ac and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800039c4]:fmul.h t6, t5, t4, dyn<br> [0x800039c8]:csrrs a3, fcsr, zero<br> [0x800039cc]:sw t6, 672(s1)<br>    |
| 366|[0x80012f7c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003a04]:fmul.h t6, t5, t4, dyn<br> [0x80003a08]:csrrs a3, fcsr, zero<br> [0x80003a0c]:sw t6, 680(s1)<br>    |
| 367|[0x80012f84]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003a44]:fmul.h t6, t5, t4, dyn<br> [0x80003a48]:csrrs a3, fcsr, zero<br> [0x80003a4c]:sw t6, 688(s1)<br>    |
| 368|[0x80012f8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003a84]:fmul.h t6, t5, t4, dyn<br> [0x80003a88]:csrrs a3, fcsr, zero<br> [0x80003a8c]:sw t6, 696(s1)<br>    |
| 369|[0x80012f94]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003ac4]:fmul.h t6, t5, t4, dyn<br> [0x80003ac8]:csrrs a3, fcsr, zero<br> [0x80003acc]:sw t6, 704(s1)<br>    |
| 370|[0x80012f9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0d8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003b04]:fmul.h t6, t5, t4, dyn<br> [0x80003b08]:csrrs a3, fcsr, zero<br> [0x80003b0c]:sw t6, 712(s1)<br>    |
| 371|[0x80012fa4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003b44]:fmul.h t6, t5, t4, dyn<br> [0x80003b48]:csrrs a3, fcsr, zero<br> [0x80003b4c]:sw t6, 720(s1)<br>    |
| 372|[0x80012fac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003b84]:fmul.h t6, t5, t4, dyn<br> [0x80003b88]:csrrs a3, fcsr, zero<br> [0x80003b8c]:sw t6, 728(s1)<br>    |
| 373|[0x80012fb4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003bc4]:fmul.h t6, t5, t4, dyn<br> [0x80003bc8]:csrrs a3, fcsr, zero<br> [0x80003bcc]:sw t6, 736(s1)<br>    |
| 374|[0x80012fbc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003c04]:fmul.h t6, t5, t4, dyn<br> [0x80003c08]:csrrs a3, fcsr, zero<br> [0x80003c0c]:sw t6, 744(s1)<br>    |
| 375|[0x80012fc4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x09e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003c44]:fmul.h t6, t5, t4, dyn<br> [0x80003c48]:csrrs a3, fcsr, zero<br> [0x80003c4c]:sw t6, 752(s1)<br>    |
| 376|[0x80012fcc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003c84]:fmul.h t6, t5, t4, dyn<br> [0x80003c88]:csrrs a3, fcsr, zero<br> [0x80003c8c]:sw t6, 760(s1)<br>    |
| 377|[0x80012fd4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003cc4]:fmul.h t6, t5, t4, dyn<br> [0x80003cc8]:csrrs a3, fcsr, zero<br> [0x80003ccc]:sw t6, 768(s1)<br>    |
| 378|[0x80012fdc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003d04]:fmul.h t6, t5, t4, dyn<br> [0x80003d08]:csrrs a3, fcsr, zero<br> [0x80003d0c]:sw t6, 776(s1)<br>    |
| 379|[0x80012fe4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003d44]:fmul.h t6, t5, t4, dyn<br> [0x80003d48]:csrrs a3, fcsr, zero<br> [0x80003d4c]:sw t6, 784(s1)<br>    |
| 380|[0x80012fec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x07f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003d84]:fmul.h t6, t5, t4, dyn<br> [0x80003d88]:csrrs a3, fcsr, zero<br> [0x80003d8c]:sw t6, 792(s1)<br>    |
| 381|[0x80012ff4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003dc4]:fmul.h t6, t5, t4, dyn<br> [0x80003dc8]:csrrs a3, fcsr, zero<br> [0x80003dcc]:sw t6, 800(s1)<br>    |
| 382|[0x80012ffc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003e04]:fmul.h t6, t5, t4, dyn<br> [0x80003e08]:csrrs a3, fcsr, zero<br> [0x80003e0c]:sw t6, 808(s1)<br>    |
| 383|[0x80013004]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003e44]:fmul.h t6, t5, t4, dyn<br> [0x80003e48]:csrrs a3, fcsr, zero<br> [0x80003e4c]:sw t6, 816(s1)<br>    |
| 384|[0x8001300c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003e84]:fmul.h t6, t5, t4, dyn<br> [0x80003e88]:csrrs a3, fcsr, zero<br> [0x80003e8c]:sw t6, 824(s1)<br>    |
| 385|[0x80013014]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1a and fm1 == 0x04b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003ec4]:fmul.h t6, t5, t4, dyn<br> [0x80003ec8]:csrrs a3, fcsr, zero<br> [0x80003ecc]:sw t6, 832(s1)<br>    |
| 386|[0x8001301c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80003f04]:fmul.h t6, t5, t4, dyn<br> [0x80003f08]:csrrs a3, fcsr, zero<br> [0x80003f0c]:sw t6, 840(s1)<br>    |
| 387|[0x80013024]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003f44]:fmul.h t6, t5, t4, dyn<br> [0x80003f48]:csrrs a3, fcsr, zero<br> [0x80003f4c]:sw t6, 848(s1)<br>    |
| 388|[0x8001302c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003f84]:fmul.h t6, t5, t4, dyn<br> [0x80003f88]:csrrs a3, fcsr, zero<br> [0x80003f8c]:sw t6, 856(s1)<br>    |
| 389|[0x80013034]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80003fc4]:fmul.h t6, t5, t4, dyn<br> [0x80003fc8]:csrrs a3, fcsr, zero<br> [0x80003fcc]:sw t6, 864(s1)<br>    |
| 390|[0x8001303c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004004]:fmul.h t6, t5, t4, dyn<br> [0x80004008]:csrrs a3, fcsr, zero<br> [0x8000400c]:sw t6, 872(s1)<br>    |
| 391|[0x80013044]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004044]:fmul.h t6, t5, t4, dyn<br> [0x80004048]:csrrs a3, fcsr, zero<br> [0x8000404c]:sw t6, 880(s1)<br>    |
| 392|[0x8001304c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004084]:fmul.h t6, t5, t4, dyn<br> [0x80004088]:csrrs a3, fcsr, zero<br> [0x8000408c]:sw t6, 888(s1)<br>    |
| 393|[0x80013054]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800040c4]:fmul.h t6, t5, t4, dyn<br> [0x800040c8]:csrrs a3, fcsr, zero<br> [0x800040cc]:sw t6, 896(s1)<br>    |
| 394|[0x8001305c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004104]:fmul.h t6, t5, t4, dyn<br> [0x80004108]:csrrs a3, fcsr, zero<br> [0x8000410c]:sw t6, 904(s1)<br>    |
| 395|[0x80013064]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x010 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004144]:fmul.h t6, t5, t4, dyn<br> [0x80004148]:csrrs a3, fcsr, zero<br> [0x8000414c]:sw t6, 912(s1)<br>    |
| 396|[0x8001306c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004184]:fmul.h t6, t5, t4, dyn<br> [0x80004188]:csrrs a3, fcsr, zero<br> [0x8000418c]:sw t6, 920(s1)<br>    |
| 397|[0x80013074]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800041c4]:fmul.h t6, t5, t4, dyn<br> [0x800041c8]:csrrs a3, fcsr, zero<br> [0x800041cc]:sw t6, 928(s1)<br>    |
| 398|[0x8001307c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004204]:fmul.h t6, t5, t4, dyn<br> [0x80004208]:csrrs a3, fcsr, zero<br> [0x8000420c]:sw t6, 936(s1)<br>    |
| 399|[0x80013084]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004244]:fmul.h t6, t5, t4, dyn<br> [0x80004248]:csrrs a3, fcsr, zero<br> [0x8000424c]:sw t6, 944(s1)<br>    |
| 400|[0x8001308c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x378 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004284]:fmul.h t6, t5, t4, dyn<br> [0x80004288]:csrrs a3, fcsr, zero<br> [0x8000428c]:sw t6, 952(s1)<br>    |
| 401|[0x80013094]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800042c4]:fmul.h t6, t5, t4, dyn<br> [0x800042c8]:csrrs a3, fcsr, zero<br> [0x800042cc]:sw t6, 960(s1)<br>    |
| 402|[0x8001309c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004304]:fmul.h t6, t5, t4, dyn<br> [0x80004308]:csrrs a3, fcsr, zero<br> [0x8000430c]:sw t6, 968(s1)<br>    |
| 403|[0x800130a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004344]:fmul.h t6, t5, t4, dyn<br> [0x80004348]:csrrs a3, fcsr, zero<br> [0x8000434c]:sw t6, 976(s1)<br>    |
| 404|[0x800130ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004384]:fmul.h t6, t5, t4, dyn<br> [0x80004388]:csrrs a3, fcsr, zero<br> [0x8000438c]:sw t6, 984(s1)<br>    |
| 405|[0x800130b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x36f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800043c4]:fmul.h t6, t5, t4, dyn<br> [0x800043c8]:csrrs a3, fcsr, zero<br> [0x800043cc]:sw t6, 992(s1)<br>    |
| 406|[0x800130bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004404]:fmul.h t6, t5, t4, dyn<br> [0x80004408]:csrrs a3, fcsr, zero<br> [0x8000440c]:sw t6, 1000(s1)<br>   |
| 407|[0x800130c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004444]:fmul.h t6, t5, t4, dyn<br> [0x80004448]:csrrs a3, fcsr, zero<br> [0x8000444c]:sw t6, 1008(s1)<br>   |
| 408|[0x800130cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004484]:fmul.h t6, t5, t4, dyn<br> [0x80004488]:csrrs a3, fcsr, zero<br> [0x8000448c]:sw t6, 1016(s1)<br>   |
| 409|[0x800130d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800044cc]:fmul.h t6, t5, t4, dyn<br> [0x800044d0]:csrrs a3, fcsr, zero<br> [0x800044d4]:sw t6, 0(s1)<br>      |
| 410|[0x800130dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000450c]:fmul.h t6, t5, t4, dyn<br> [0x80004510]:csrrs a3, fcsr, zero<br> [0x80004514]:sw t6, 8(s1)<br>      |
| 411|[0x800130e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000454c]:fmul.h t6, t5, t4, dyn<br> [0x80004550]:csrrs a3, fcsr, zero<br> [0x80004554]:sw t6, 16(s1)<br>     |
| 412|[0x800130ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000458c]:fmul.h t6, t5, t4, dyn<br> [0x80004590]:csrrs a3, fcsr, zero<br> [0x80004594]:sw t6, 24(s1)<br>     |
| 413|[0x800130f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800045cc]:fmul.h t6, t5, t4, dyn<br> [0x800045d0]:csrrs a3, fcsr, zero<br> [0x800045d4]:sw t6, 32(s1)<br>     |
| 414|[0x800130fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000460c]:fmul.h t6, t5, t4, dyn<br> [0x80004610]:csrrs a3, fcsr, zero<br> [0x80004614]:sw t6, 40(s1)<br>     |
| 415|[0x80013104]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0fd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000464c]:fmul.h t6, t5, t4, dyn<br> [0x80004650]:csrrs a3, fcsr, zero<br> [0x80004654]:sw t6, 48(s1)<br>     |
| 416|[0x8001310c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000468c]:fmul.h t6, t5, t4, dyn<br> [0x80004690]:csrrs a3, fcsr, zero<br> [0x80004694]:sw t6, 56(s1)<br>     |
| 417|[0x80013114]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800046cc]:fmul.h t6, t5, t4, dyn<br> [0x800046d0]:csrrs a3, fcsr, zero<br> [0x800046d4]:sw t6, 64(s1)<br>     |
| 418|[0x8001311c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000470c]:fmul.h t6, t5, t4, dyn<br> [0x80004710]:csrrs a3, fcsr, zero<br> [0x80004714]:sw t6, 72(s1)<br>     |
| 419|[0x80013124]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000474c]:fmul.h t6, t5, t4, dyn<br> [0x80004750]:csrrs a3, fcsr, zero<br> [0x80004754]:sw t6, 80(s1)<br>     |
| 420|[0x8001312c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ac and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000478c]:fmul.h t6, t5, t4, dyn<br> [0x80004790]:csrrs a3, fcsr, zero<br> [0x80004794]:sw t6, 88(s1)<br>     |
| 421|[0x80013134]<br>0x000003B7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800047cc]:fmul.h t6, t5, t4, dyn<br> [0x800047d0]:csrrs a3, fcsr, zero<br> [0x800047d4]:sw t6, 96(s1)<br>     |
| 422|[0x8001313c]<br>0x000003B7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000480c]:fmul.h t6, t5, t4, dyn<br> [0x80004810]:csrrs a3, fcsr, zero<br> [0x80004814]:sw t6, 104(s1)<br>    |
| 423|[0x80013144]<br>0x000003B7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000484c]:fmul.h t6, t5, t4, dyn<br> [0x80004850]:csrrs a3, fcsr, zero<br> [0x80004854]:sw t6, 112(s1)<br>    |
| 424|[0x8001314c]<br>0x000003B7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000488c]:fmul.h t6, t5, t4, dyn<br> [0x80004890]:csrrs a3, fcsr, zero<br> [0x80004894]:sw t6, 120(s1)<br>    |
| 425|[0x80013154]<br>0x000003B7<br> |- fs1 == 0 and fe1 == 0x17 and fm1 == 0x0f4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x003 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800048cc]:fmul.h t6, t5, t4, dyn<br> [0x800048d0]:csrrs a3, fcsr, zero<br> [0x800048d4]:sw t6, 128(s1)<br>    |
| 426|[0x8001315c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000490c]:fmul.h t6, t5, t4, dyn<br> [0x80004910]:csrrs a3, fcsr, zero<br> [0x80004914]:sw t6, 136(s1)<br>    |
| 427|[0x80013164]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000494c]:fmul.h t6, t5, t4, dyn<br> [0x80004950]:csrrs a3, fcsr, zero<br> [0x80004954]:sw t6, 144(s1)<br>    |
| 428|[0x8001316c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000498c]:fmul.h t6, t5, t4, dyn<br> [0x80004990]:csrrs a3, fcsr, zero<br> [0x80004994]:sw t6, 152(s1)<br>    |
| 429|[0x80013174]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800049cc]:fmul.h t6, t5, t4, dyn<br> [0x800049d0]:csrrs a3, fcsr, zero<br> [0x800049d4]:sw t6, 160(s1)<br>    |
| 430|[0x8001317c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x289 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004a0c]:fmul.h t6, t5, t4, dyn<br> [0x80004a10]:csrrs a3, fcsr, zero<br> [0x80004a14]:sw t6, 168(s1)<br>    |
| 431|[0x80013184]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004a4c]:fmul.h t6, t5, t4, dyn<br> [0x80004a50]:csrrs a3, fcsr, zero<br> [0x80004a54]:sw t6, 176(s1)<br>    |
| 432|[0x8001318c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004a8c]:fmul.h t6, t5, t4, dyn<br> [0x80004a90]:csrrs a3, fcsr, zero<br> [0x80004a94]:sw t6, 184(s1)<br>    |
| 433|[0x80013194]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004acc]:fmul.h t6, t5, t4, dyn<br> [0x80004ad0]:csrrs a3, fcsr, zero<br> [0x80004ad4]:sw t6, 192(s1)<br>    |
| 434|[0x8001319c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004b0c]:fmul.h t6, t5, t4, dyn<br> [0x80004b10]:csrrs a3, fcsr, zero<br> [0x80004b14]:sw t6, 200(s1)<br>    |
| 435|[0x800131a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x262 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004b4c]:fmul.h t6, t5, t4, dyn<br> [0x80004b50]:csrrs a3, fcsr, zero<br> [0x80004b54]:sw t6, 208(s1)<br>    |
| 436|[0x800131ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004b8c]:fmul.h t6, t5, t4, dyn<br> [0x80004b90]:csrrs a3, fcsr, zero<br> [0x80004b94]:sw t6, 216(s1)<br>    |
| 437|[0x800131b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004bcc]:fmul.h t6, t5, t4, dyn<br> [0x80004bd0]:csrrs a3, fcsr, zero<br> [0x80004bd4]:sw t6, 224(s1)<br>    |
| 438|[0x800131bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004c0c]:fmul.h t6, t5, t4, dyn<br> [0x80004c10]:csrrs a3, fcsr, zero<br> [0x80004c14]:sw t6, 232(s1)<br>    |
| 439|[0x800131c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004c4c]:fmul.h t6, t5, t4, dyn<br> [0x80004c50]:csrrs a3, fcsr, zero<br> [0x80004c54]:sw t6, 240(s1)<br>    |
| 440|[0x800131cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004c8c]:fmul.h t6, t5, t4, dyn<br> [0x80004c90]:csrrs a3, fcsr, zero<br> [0x80004c94]:sw t6, 248(s1)<br>    |
| 441|[0x800131d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004ccc]:fmul.h t6, t5, t4, dyn<br> [0x80004cd0]:csrrs a3, fcsr, zero<br> [0x80004cd4]:sw t6, 256(s1)<br>    |
| 442|[0x800131dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004d0c]:fmul.h t6, t5, t4, dyn<br> [0x80004d10]:csrrs a3, fcsr, zero<br> [0x80004d14]:sw t6, 264(s1)<br>    |
| 443|[0x800131e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004d4c]:fmul.h t6, t5, t4, dyn<br> [0x80004d50]:csrrs a3, fcsr, zero<br> [0x80004d54]:sw t6, 272(s1)<br>    |
| 444|[0x800131ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004d8c]:fmul.h t6, t5, t4, dyn<br> [0x80004d90]:csrrs a3, fcsr, zero<br> [0x80004d94]:sw t6, 280(s1)<br>    |
| 445|[0x800131f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x029 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004dcc]:fmul.h t6, t5, t4, dyn<br> [0x80004dd0]:csrrs a3, fcsr, zero<br> [0x80004dd4]:sw t6, 288(s1)<br>    |
| 446|[0x800131fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004e0c]:fmul.h t6, t5, t4, dyn<br> [0x80004e10]:csrrs a3, fcsr, zero<br> [0x80004e14]:sw t6, 296(s1)<br>    |
| 447|[0x80013204]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004e4c]:fmul.h t6, t5, t4, dyn<br> [0x80004e50]:csrrs a3, fcsr, zero<br> [0x80004e54]:sw t6, 304(s1)<br>    |
| 448|[0x8001320c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004e8c]:fmul.h t6, t5, t4, dyn<br> [0x80004e90]:csrrs a3, fcsr, zero<br> [0x80004e94]:sw t6, 312(s1)<br>    |
| 449|[0x80013214]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004ecc]:fmul.h t6, t5, t4, dyn<br> [0x80004ed0]:csrrs a3, fcsr, zero<br> [0x80004ed4]:sw t6, 320(s1)<br>    |
| 450|[0x8001321c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0f4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004f0c]:fmul.h t6, t5, t4, dyn<br> [0x80004f10]:csrrs a3, fcsr, zero<br> [0x80004f14]:sw t6, 328(s1)<br>    |
| 451|[0x80013224]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80004f4c]:fmul.h t6, t5, t4, dyn<br> [0x80004f50]:csrrs a3, fcsr, zero<br> [0x80004f54]:sw t6, 336(s1)<br>    |
| 452|[0x8001322c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004f8c]:fmul.h t6, t5, t4, dyn<br> [0x80004f90]:csrrs a3, fcsr, zero<br> [0x80004f94]:sw t6, 344(s1)<br>    |
| 453|[0x80013234]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80004fcc]:fmul.h t6, t5, t4, dyn<br> [0x80004fd0]:csrrs a3, fcsr, zero<br> [0x80004fd4]:sw t6, 352(s1)<br>    |
| 454|[0x8001323c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000500c]:fmul.h t6, t5, t4, dyn<br> [0x80005010]:csrrs a3, fcsr, zero<br> [0x80005014]:sw t6, 360(s1)<br>    |
| 455|[0x80013244]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000504c]:fmul.h t6, t5, t4, dyn<br> [0x80005050]:csrrs a3, fcsr, zero<br> [0x80005054]:sw t6, 368(s1)<br>    |
| 456|[0x8001324c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000508c]:fmul.h t6, t5, t4, dyn<br> [0x80005090]:csrrs a3, fcsr, zero<br> [0x80005094]:sw t6, 376(s1)<br>    |
| 457|[0x80013254]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800050cc]:fmul.h t6, t5, t4, dyn<br> [0x800050d0]:csrrs a3, fcsr, zero<br> [0x800050d4]:sw t6, 384(s1)<br>    |
| 458|[0x8001325c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000510c]:fmul.h t6, t5, t4, dyn<br> [0x80005110]:csrrs a3, fcsr, zero<br> [0x80005114]:sw t6, 392(s1)<br>    |
| 459|[0x80013264]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000514c]:fmul.h t6, t5, t4, dyn<br> [0x80005150]:csrrs a3, fcsr, zero<br> [0x80005154]:sw t6, 400(s1)<br>    |
| 460|[0x8001326c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000518c]:fmul.h t6, t5, t4, dyn<br> [0x80005190]:csrrs a3, fcsr, zero<br> [0x80005194]:sw t6, 408(s1)<br>    |
| 461|[0x80013274]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800051cc]:fmul.h t6, t5, t4, dyn<br> [0x800051d0]:csrrs a3, fcsr, zero<br> [0x800051d4]:sw t6, 416(s1)<br>    |
| 462|[0x8001327c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000520c]:fmul.h t6, t5, t4, dyn<br> [0x80005210]:csrrs a3, fcsr, zero<br> [0x80005214]:sw t6, 424(s1)<br>    |
| 463|[0x80013284]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000524c]:fmul.h t6, t5, t4, dyn<br> [0x80005250]:csrrs a3, fcsr, zero<br> [0x80005254]:sw t6, 432(s1)<br>    |
| 464|[0x8001328c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000528c]:fmul.h t6, t5, t4, dyn<br> [0x80005290]:csrrs a3, fcsr, zero<br> [0x80005294]:sw t6, 440(s1)<br>    |
| 465|[0x80013294]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2ef and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800052cc]:fmul.h t6, t5, t4, dyn<br> [0x800052d0]:csrrs a3, fcsr, zero<br> [0x800052d4]:sw t6, 448(s1)<br>    |
| 466|[0x8001329c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000530c]:fmul.h t6, t5, t4, dyn<br> [0x80005310]:csrrs a3, fcsr, zero<br> [0x80005314]:sw t6, 456(s1)<br>    |
| 467|[0x800132a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000534c]:fmul.h t6, t5, t4, dyn<br> [0x80005350]:csrrs a3, fcsr, zero<br> [0x80005354]:sw t6, 464(s1)<br>    |
| 468|[0x800132ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000538c]:fmul.h t6, t5, t4, dyn<br> [0x80005390]:csrrs a3, fcsr, zero<br> [0x80005394]:sw t6, 472(s1)<br>    |
| 469|[0x800132b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800053cc]:fmul.h t6, t5, t4, dyn<br> [0x800053d0]:csrrs a3, fcsr, zero<br> [0x800053d4]:sw t6, 480(s1)<br>    |
| 470|[0x800132bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0a5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000540c]:fmul.h t6, t5, t4, dyn<br> [0x80005410]:csrrs a3, fcsr, zero<br> [0x80005414]:sw t6, 488(s1)<br>    |
| 471|[0x800132c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000544c]:fmul.h t6, t5, t4, dyn<br> [0x80005450]:csrrs a3, fcsr, zero<br> [0x80005454]:sw t6, 496(s1)<br>    |
| 472|[0x800132cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000548c]:fmul.h t6, t5, t4, dyn<br> [0x80005490]:csrrs a3, fcsr, zero<br> [0x80005494]:sw t6, 504(s1)<br>    |
| 473|[0x800132d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800054cc]:fmul.h t6, t5, t4, dyn<br> [0x800054d0]:csrrs a3, fcsr, zero<br> [0x800054d4]:sw t6, 512(s1)<br>    |
| 474|[0x800132dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000550c]:fmul.h t6, t5, t4, dyn<br> [0x80005510]:csrrs a3, fcsr, zero<br> [0x80005514]:sw t6, 520(s1)<br>    |
| 475|[0x800132e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000554c]:fmul.h t6, t5, t4, dyn<br> [0x80005550]:csrrs a3, fcsr, zero<br> [0x80005554]:sw t6, 528(s1)<br>    |
| 476|[0x800132ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000558c]:fmul.h t6, t5, t4, dyn<br> [0x80005590]:csrrs a3, fcsr, zero<br> [0x80005594]:sw t6, 536(s1)<br>    |
| 477|[0x800132f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800055cc]:fmul.h t6, t5, t4, dyn<br> [0x800055d0]:csrrs a3, fcsr, zero<br> [0x800055d4]:sw t6, 544(s1)<br>    |
| 478|[0x800132fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000560c]:fmul.h t6, t5, t4, dyn<br> [0x80005610]:csrrs a3, fcsr, zero<br> [0x80005614]:sw t6, 552(s1)<br>    |
| 479|[0x80013304]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000564c]:fmul.h t6, t5, t4, dyn<br> [0x80005650]:csrrs a3, fcsr, zero<br> [0x80005654]:sw t6, 560(s1)<br>    |
| 480|[0x8001330c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1cb and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000568c]:fmul.h t6, t5, t4, dyn<br> [0x80005690]:csrrs a3, fcsr, zero<br> [0x80005694]:sw t6, 568(s1)<br>    |
| 481|[0x80013314]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800056cc]:fmul.h t6, t5, t4, dyn<br> [0x800056d0]:csrrs a3, fcsr, zero<br> [0x800056d4]:sw t6, 576(s1)<br>    |
| 482|[0x8001331c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000570c]:fmul.h t6, t5, t4, dyn<br> [0x80005710]:csrrs a3, fcsr, zero<br> [0x80005714]:sw t6, 584(s1)<br>    |
| 483|[0x80013324]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000574c]:fmul.h t6, t5, t4, dyn<br> [0x80005750]:csrrs a3, fcsr, zero<br> [0x80005754]:sw t6, 592(s1)<br>    |
| 484|[0x8001332c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000578c]:fmul.h t6, t5, t4, dyn<br> [0x80005790]:csrrs a3, fcsr, zero<br> [0x80005794]:sw t6, 600(s1)<br>    |
| 485|[0x80013334]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x26b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800057cc]:fmul.h t6, t5, t4, dyn<br> [0x800057d0]:csrrs a3, fcsr, zero<br> [0x800057d4]:sw t6, 608(s1)<br>    |
| 486|[0x8001333c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000580c]:fmul.h t6, t5, t4, dyn<br> [0x80005810]:csrrs a3, fcsr, zero<br> [0x80005814]:sw t6, 616(s1)<br>    |
| 487|[0x80013344]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000584c]:fmul.h t6, t5, t4, dyn<br> [0x80005850]:csrrs a3, fcsr, zero<br> [0x80005854]:sw t6, 624(s1)<br>    |
| 488|[0x8001334c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000588c]:fmul.h t6, t5, t4, dyn<br> [0x80005890]:csrrs a3, fcsr, zero<br> [0x80005894]:sw t6, 632(s1)<br>    |
| 489|[0x80013354]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800058cc]:fmul.h t6, t5, t4, dyn<br> [0x800058d0]:csrrs a3, fcsr, zero<br> [0x800058d4]:sw t6, 640(s1)<br>    |
| 490|[0x8001335c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x026 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000590c]:fmul.h t6, t5, t4, dyn<br> [0x80005910]:csrrs a3, fcsr, zero<br> [0x80005914]:sw t6, 648(s1)<br>    |
| 491|[0x80013364]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000594c]:fmul.h t6, t5, t4, dyn<br> [0x80005950]:csrrs a3, fcsr, zero<br> [0x80005954]:sw t6, 656(s1)<br>    |
| 492|[0x8001336c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000598c]:fmul.h t6, t5, t4, dyn<br> [0x80005990]:csrrs a3, fcsr, zero<br> [0x80005994]:sw t6, 664(s1)<br>    |
| 493|[0x80013374]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800059cc]:fmul.h t6, t5, t4, dyn<br> [0x800059d0]:csrrs a3, fcsr, zero<br> [0x800059d4]:sw t6, 672(s1)<br>    |
| 494|[0x8001337c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005a0c]:fmul.h t6, t5, t4, dyn<br> [0x80005a10]:csrrs a3, fcsr, zero<br> [0x80005a14]:sw t6, 680(s1)<br>    |
| 495|[0x80013384]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005a4c]:fmul.h t6, t5, t4, dyn<br> [0x80005a50]:csrrs a3, fcsr, zero<br> [0x80005a54]:sw t6, 688(s1)<br>    |
| 496|[0x8001338c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005a8c]:fmul.h t6, t5, t4, dyn<br> [0x80005a90]:csrrs a3, fcsr, zero<br> [0x80005a94]:sw t6, 696(s1)<br>    |
| 497|[0x80013394]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005acc]:fmul.h t6, t5, t4, dyn<br> [0x80005ad0]:csrrs a3, fcsr, zero<br> [0x80005ad4]:sw t6, 704(s1)<br>    |
| 498|[0x8001339c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005b0c]:fmul.h t6, t5, t4, dyn<br> [0x80005b10]:csrrs a3, fcsr, zero<br> [0x80005b14]:sw t6, 712(s1)<br>    |
| 499|[0x800133a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005b4c]:fmul.h t6, t5, t4, dyn<br> [0x80005b50]:csrrs a3, fcsr, zero<br> [0x80005b54]:sw t6, 720(s1)<br>    |
| 500|[0x800133ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3dd and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005b8c]:fmul.h t6, t5, t4, dyn<br> [0x80005b90]:csrrs a3, fcsr, zero<br> [0x80005b94]:sw t6, 728(s1)<br>    |
| 501|[0x800133b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005bcc]:fmul.h t6, t5, t4, dyn<br> [0x80005bd0]:csrrs a3, fcsr, zero<br> [0x80005bd4]:sw t6, 736(s1)<br>    |
| 502|[0x800133bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005c0c]:fmul.h t6, t5, t4, dyn<br> [0x80005c10]:csrrs a3, fcsr, zero<br> [0x80005c14]:sw t6, 744(s1)<br>    |
| 503|[0x800133c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005c4c]:fmul.h t6, t5, t4, dyn<br> [0x80005c50]:csrrs a3, fcsr, zero<br> [0x80005c54]:sw t6, 752(s1)<br>    |
| 504|[0x800133cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005c8c]:fmul.h t6, t5, t4, dyn<br> [0x80005c90]:csrrs a3, fcsr, zero<br> [0x80005c94]:sw t6, 760(s1)<br>    |
| 505|[0x800133d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x0e1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005ccc]:fmul.h t6, t5, t4, dyn<br> [0x80005cd0]:csrrs a3, fcsr, zero<br> [0x80005cd4]:sw t6, 768(s1)<br>    |
| 506|[0x800133dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005d0c]:fmul.h t6, t5, t4, dyn<br> [0x80005d10]:csrrs a3, fcsr, zero<br> [0x80005d14]:sw t6, 776(s1)<br>    |
| 507|[0x800133e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005d4c]:fmul.h t6, t5, t4, dyn<br> [0x80005d50]:csrrs a3, fcsr, zero<br> [0x80005d54]:sw t6, 784(s1)<br>    |
| 508|[0x800133ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005d8c]:fmul.h t6, t5, t4, dyn<br> [0x80005d90]:csrrs a3, fcsr, zero<br> [0x80005d94]:sw t6, 792(s1)<br>    |
| 509|[0x800133f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005dcc]:fmul.h t6, t5, t4, dyn<br> [0x80005dd0]:csrrs a3, fcsr, zero<br> [0x80005dd4]:sw t6, 800(s1)<br>    |
| 510|[0x800133fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x278 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005e0c]:fmul.h t6, t5, t4, dyn<br> [0x80005e10]:csrrs a3, fcsr, zero<br> [0x80005e14]:sw t6, 808(s1)<br>    |
| 511|[0x80013404]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005e4c]:fmul.h t6, t5, t4, dyn<br> [0x80005e50]:csrrs a3, fcsr, zero<br> [0x80005e54]:sw t6, 816(s1)<br>    |
| 512|[0x8001340c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005e8c]:fmul.h t6, t5, t4, dyn<br> [0x80005e90]:csrrs a3, fcsr, zero<br> [0x80005e94]:sw t6, 824(s1)<br>    |
| 513|[0x80013414]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005ecc]:fmul.h t6, t5, t4, dyn<br> [0x80005ed0]:csrrs a3, fcsr, zero<br> [0x80005ed4]:sw t6, 832(s1)<br>    |
| 514|[0x8001341c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005f0c]:fmul.h t6, t5, t4, dyn<br> [0x80005f10]:csrrs a3, fcsr, zero<br> [0x80005f14]:sw t6, 840(s1)<br>    |
| 515|[0x80013424]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005f4c]:fmul.h t6, t5, t4, dyn<br> [0x80005f50]:csrrs a3, fcsr, zero<br> [0x80005f54]:sw t6, 848(s1)<br>    |
| 516|[0x8001342c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80005f8c]:fmul.h t6, t5, t4, dyn<br> [0x80005f90]:csrrs a3, fcsr, zero<br> [0x80005f94]:sw t6, 856(s1)<br>    |
| 517|[0x80013434]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80005fcc]:fmul.h t6, t5, t4, dyn<br> [0x80005fd0]:csrrs a3, fcsr, zero<br> [0x80005fd4]:sw t6, 864(s1)<br>    |
| 518|[0x8001343c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000600c]:fmul.h t6, t5, t4, dyn<br> [0x80006010]:csrrs a3, fcsr, zero<br> [0x80006014]:sw t6, 872(s1)<br>    |
| 519|[0x80013444]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000604c]:fmul.h t6, t5, t4, dyn<br> [0x80006050]:csrrs a3, fcsr, zero<br> [0x80006054]:sw t6, 880(s1)<br>    |
| 520|[0x8001344c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x006 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000608c]:fmul.h t6, t5, t4, dyn<br> [0x80006090]:csrrs a3, fcsr, zero<br> [0x80006094]:sw t6, 888(s1)<br>    |
| 521|[0x80013454]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800060cc]:fmul.h t6, t5, t4, dyn<br> [0x800060d0]:csrrs a3, fcsr, zero<br> [0x800060d4]:sw t6, 896(s1)<br>    |
| 522|[0x8001345c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000610c]:fmul.h t6, t5, t4, dyn<br> [0x80006110]:csrrs a3, fcsr, zero<br> [0x80006114]:sw t6, 904(s1)<br>    |
| 523|[0x80013464]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000614c]:fmul.h t6, t5, t4, dyn<br> [0x80006150]:csrrs a3, fcsr, zero<br> [0x80006154]:sw t6, 912(s1)<br>    |
| 524|[0x8001346c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000618c]:fmul.h t6, t5, t4, dyn<br> [0x80006190]:csrrs a3, fcsr, zero<br> [0x80006194]:sw t6, 920(s1)<br>    |
| 525|[0x80013474]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x291 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800061cc]:fmul.h t6, t5, t4, dyn<br> [0x800061d0]:csrrs a3, fcsr, zero<br> [0x800061d4]:sw t6, 928(s1)<br>    |
| 526|[0x8001347c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000620c]:fmul.h t6, t5, t4, dyn<br> [0x80006210]:csrrs a3, fcsr, zero<br> [0x80006214]:sw t6, 936(s1)<br>    |
| 527|[0x80013484]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000624c]:fmul.h t6, t5, t4, dyn<br> [0x80006250]:csrrs a3, fcsr, zero<br> [0x80006254]:sw t6, 944(s1)<br>    |
| 528|[0x8001348c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000628c]:fmul.h t6, t5, t4, dyn<br> [0x80006290]:csrrs a3, fcsr, zero<br> [0x80006294]:sw t6, 952(s1)<br>    |
| 529|[0x80013494]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800062cc]:fmul.h t6, t5, t4, dyn<br> [0x800062d0]:csrrs a3, fcsr, zero<br> [0x800062d4]:sw t6, 960(s1)<br>    |
| 530|[0x8001349c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x2a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000630c]:fmul.h t6, t5, t4, dyn<br> [0x80006310]:csrrs a3, fcsr, zero<br> [0x80006314]:sw t6, 968(s1)<br>    |
| 531|[0x800134a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000634c]:fmul.h t6, t5, t4, dyn<br> [0x80006350]:csrrs a3, fcsr, zero<br> [0x80006354]:sw t6, 976(s1)<br>    |
| 532|[0x800134ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000638c]:fmul.h t6, t5, t4, dyn<br> [0x80006390]:csrrs a3, fcsr, zero<br> [0x80006394]:sw t6, 984(s1)<br>    |
| 533|[0x800134b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800063cc]:fmul.h t6, t5, t4, dyn<br> [0x800063d0]:csrrs a3, fcsr, zero<br> [0x800063d4]:sw t6, 992(s1)<br>    |
| 534|[0x800134bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006404]:fmul.h t6, t5, t4, dyn<br> [0x80006408]:csrrs a3, fcsr, zero<br> [0x8000640c]:sw t6, 1000(s1)<br>   |
| 535|[0x800134c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000643c]:fmul.h t6, t5, t4, dyn<br> [0x80006440]:csrrs a3, fcsr, zero<br> [0x80006444]:sw t6, 1008(s1)<br>   |
| 536|[0x800134cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006474]:fmul.h t6, t5, t4, dyn<br> [0x80006478]:csrrs a3, fcsr, zero<br> [0x8000647c]:sw t6, 1016(s1)<br>   |
| 537|[0x800134d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800064b4]:fmul.h t6, t5, t4, dyn<br> [0x800064b8]:csrrs a3, fcsr, zero<br> [0x800064bc]:sw t6, 0(s1)<br>      |
| 538|[0x800134dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800064ec]:fmul.h t6, t5, t4, dyn<br> [0x800064f0]:csrrs a3, fcsr, zero<br> [0x800064f4]:sw t6, 8(s1)<br>      |
| 539|[0x800134e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006524]:fmul.h t6, t5, t4, dyn<br> [0x80006528]:csrrs a3, fcsr, zero<br> [0x8000652c]:sw t6, 16(s1)<br>     |
| 540|[0x800134ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1f6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000655c]:fmul.h t6, t5, t4, dyn<br> [0x80006560]:csrrs a3, fcsr, zero<br> [0x80006564]:sw t6, 24(s1)<br>     |
| 541|[0x800134f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006594]:fmul.h t6, t5, t4, dyn<br> [0x80006598]:csrrs a3, fcsr, zero<br> [0x8000659c]:sw t6, 32(s1)<br>     |
| 542|[0x800134fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800065cc]:fmul.h t6, t5, t4, dyn<br> [0x800065d0]:csrrs a3, fcsr, zero<br> [0x800065d4]:sw t6, 40(s1)<br>     |
| 543|[0x80013504]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006604]:fmul.h t6, t5, t4, dyn<br> [0x80006608]:csrrs a3, fcsr, zero<br> [0x8000660c]:sw t6, 48(s1)<br>     |
| 544|[0x8001350c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000663c]:fmul.h t6, t5, t4, dyn<br> [0x80006640]:csrrs a3, fcsr, zero<br> [0x80006644]:sw t6, 56(s1)<br>     |
| 545|[0x80013514]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006674]:fmul.h t6, t5, t4, dyn<br> [0x80006678]:csrrs a3, fcsr, zero<br> [0x8000667c]:sw t6, 64(s1)<br>     |
| 546|[0x8001351c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800066ac]:fmul.h t6, t5, t4, dyn<br> [0x800066b0]:csrrs a3, fcsr, zero<br> [0x800066b4]:sw t6, 72(s1)<br>     |
| 547|[0x80013524]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800066e4]:fmul.h t6, t5, t4, dyn<br> [0x800066e8]:csrrs a3, fcsr, zero<br> [0x800066ec]:sw t6, 80(s1)<br>     |
| 548|[0x8001352c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000671c]:fmul.h t6, t5, t4, dyn<br> [0x80006720]:csrrs a3, fcsr, zero<br> [0x80006724]:sw t6, 88(s1)<br>     |
| 549|[0x80013534]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006754]:fmul.h t6, t5, t4, dyn<br> [0x80006758]:csrrs a3, fcsr, zero<br> [0x8000675c]:sw t6, 96(s1)<br>     |
| 550|[0x8001353c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x227 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000678c]:fmul.h t6, t5, t4, dyn<br> [0x80006790]:csrrs a3, fcsr, zero<br> [0x80006794]:sw t6, 104(s1)<br>    |
| 551|[0x80013544]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800067c4]:fmul.h t6, t5, t4, dyn<br> [0x800067c8]:csrrs a3, fcsr, zero<br> [0x800067cc]:sw t6, 112(s1)<br>    |
| 552|[0x8001354c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800067fc]:fmul.h t6, t5, t4, dyn<br> [0x80006800]:csrrs a3, fcsr, zero<br> [0x80006804]:sw t6, 120(s1)<br>    |
| 553|[0x80013554]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006834]:fmul.h t6, t5, t4, dyn<br> [0x80006838]:csrrs a3, fcsr, zero<br> [0x8000683c]:sw t6, 128(s1)<br>    |
| 554|[0x8001355c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000686c]:fmul.h t6, t5, t4, dyn<br> [0x80006870]:csrrs a3, fcsr, zero<br> [0x80006874]:sw t6, 136(s1)<br>    |
| 555|[0x80013564]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x243 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800068a4]:fmul.h t6, t5, t4, dyn<br> [0x800068a8]:csrrs a3, fcsr, zero<br> [0x800068ac]:sw t6, 144(s1)<br>    |
| 556|[0x8001356c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800068dc]:fmul.h t6, t5, t4, dyn<br> [0x800068e0]:csrrs a3, fcsr, zero<br> [0x800068e4]:sw t6, 152(s1)<br>    |
| 557|[0x80013574]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006914]:fmul.h t6, t5, t4, dyn<br> [0x80006918]:csrrs a3, fcsr, zero<br> [0x8000691c]:sw t6, 160(s1)<br>    |
| 558|[0x8001357c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000694c]:fmul.h t6, t5, t4, dyn<br> [0x80006950]:csrrs a3, fcsr, zero<br> [0x80006954]:sw t6, 168(s1)<br>    |
| 559|[0x80013584]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006984]:fmul.h t6, t5, t4, dyn<br> [0x80006988]:csrrs a3, fcsr, zero<br> [0x8000698c]:sw t6, 176(s1)<br>    |
| 560|[0x8001358c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800069bc]:fmul.h t6, t5, t4, dyn<br> [0x800069c0]:csrrs a3, fcsr, zero<br> [0x800069c4]:sw t6, 184(s1)<br>    |
| 561|[0x80013594]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800069f4]:fmul.h t6, t5, t4, dyn<br> [0x800069f8]:csrrs a3, fcsr, zero<br> [0x800069fc]:sw t6, 192(s1)<br>    |
| 562|[0x8001359c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006a2c]:fmul.h t6, t5, t4, dyn<br> [0x80006a30]:csrrs a3, fcsr, zero<br> [0x80006a34]:sw t6, 200(s1)<br>    |
| 563|[0x800135a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006a64]:fmul.h t6, t5, t4, dyn<br> [0x80006a68]:csrrs a3, fcsr, zero<br> [0x80006a6c]:sw t6, 208(s1)<br>    |
| 564|[0x800135ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006a9c]:fmul.h t6, t5, t4, dyn<br> [0x80006aa0]:csrrs a3, fcsr, zero<br> [0x80006aa4]:sw t6, 216(s1)<br>    |
| 565|[0x800135b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x3c9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006ad4]:fmul.h t6, t5, t4, dyn<br> [0x80006ad8]:csrrs a3, fcsr, zero<br> [0x80006adc]:sw t6, 224(s1)<br>    |
| 566|[0x800135bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006b0c]:fmul.h t6, t5, t4, dyn<br> [0x80006b10]:csrrs a3, fcsr, zero<br> [0x80006b14]:sw t6, 232(s1)<br>    |
| 567|[0x800135c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006b44]:fmul.h t6, t5, t4, dyn<br> [0x80006b48]:csrrs a3, fcsr, zero<br> [0x80006b4c]:sw t6, 240(s1)<br>    |
| 568|[0x800135cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006b7c]:fmul.h t6, t5, t4, dyn<br> [0x80006b80]:csrrs a3, fcsr, zero<br> [0x80006b84]:sw t6, 248(s1)<br>    |
| 569|[0x800135d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006bb4]:fmul.h t6, t5, t4, dyn<br> [0x80006bb8]:csrrs a3, fcsr, zero<br> [0x80006bbc]:sw t6, 256(s1)<br>    |
| 570|[0x800135dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x126 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006bec]:fmul.h t6, t5, t4, dyn<br> [0x80006bf0]:csrrs a3, fcsr, zero<br> [0x80006bf4]:sw t6, 264(s1)<br>    |
| 571|[0x800135e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006c24]:fmul.h t6, t5, t4, dyn<br> [0x80006c28]:csrrs a3, fcsr, zero<br> [0x80006c2c]:sw t6, 272(s1)<br>    |
| 572|[0x800135ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006c5c]:fmul.h t6, t5, t4, dyn<br> [0x80006c60]:csrrs a3, fcsr, zero<br> [0x80006c64]:sw t6, 280(s1)<br>    |
| 573|[0x800135f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006c94]:fmul.h t6, t5, t4, dyn<br> [0x80006c98]:csrrs a3, fcsr, zero<br> [0x80006c9c]:sw t6, 288(s1)<br>    |
| 574|[0x800135fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006ccc]:fmul.h t6, t5, t4, dyn<br> [0x80006cd0]:csrrs a3, fcsr, zero<br> [0x80006cd4]:sw t6, 296(s1)<br>    |
| 575|[0x80013604]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x120 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006d04]:fmul.h t6, t5, t4, dyn<br> [0x80006d08]:csrrs a3, fcsr, zero<br> [0x80006d0c]:sw t6, 304(s1)<br>    |
| 576|[0x8001360c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006d3c]:fmul.h t6, t5, t4, dyn<br> [0x80006d40]:csrrs a3, fcsr, zero<br> [0x80006d44]:sw t6, 312(s1)<br>    |
| 577|[0x80013614]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006d74]:fmul.h t6, t5, t4, dyn<br> [0x80006d78]:csrrs a3, fcsr, zero<br> [0x80006d7c]:sw t6, 320(s1)<br>    |
| 578|[0x8001361c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006dac]:fmul.h t6, t5, t4, dyn<br> [0x80006db0]:csrrs a3, fcsr, zero<br> [0x80006db4]:sw t6, 328(s1)<br>    |
| 579|[0x80013624]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006de4]:fmul.h t6, t5, t4, dyn<br> [0x80006de8]:csrrs a3, fcsr, zero<br> [0x80006dec]:sw t6, 336(s1)<br>    |
| 580|[0x8001362c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x189 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006e1c]:fmul.h t6, t5, t4, dyn<br> [0x80006e20]:csrrs a3, fcsr, zero<br> [0x80006e24]:sw t6, 344(s1)<br>    |
| 581|[0x80013634]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006e54]:fmul.h t6, t5, t4, dyn<br> [0x80006e58]:csrrs a3, fcsr, zero<br> [0x80006e5c]:sw t6, 352(s1)<br>    |
| 582|[0x8001363c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006e8c]:fmul.h t6, t5, t4, dyn<br> [0x80006e90]:csrrs a3, fcsr, zero<br> [0x80006e94]:sw t6, 360(s1)<br>    |
| 583|[0x80013644]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006ec4]:fmul.h t6, t5, t4, dyn<br> [0x80006ec8]:csrrs a3, fcsr, zero<br> [0x80006ecc]:sw t6, 368(s1)<br>    |
| 584|[0x8001364c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006efc]:fmul.h t6, t5, t4, dyn<br> [0x80006f00]:csrrs a3, fcsr, zero<br> [0x80006f04]:sw t6, 376(s1)<br>    |
| 585|[0x80013654]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x145 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006f34]:fmul.h t6, t5, t4, dyn<br> [0x80006f38]:csrrs a3, fcsr, zero<br> [0x80006f3c]:sw t6, 384(s1)<br>    |
| 586|[0x8001365c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80006f6c]:fmul.h t6, t5, t4, dyn<br> [0x80006f70]:csrrs a3, fcsr, zero<br> [0x80006f74]:sw t6, 392(s1)<br>    |
| 587|[0x80013664]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006fa4]:fmul.h t6, t5, t4, dyn<br> [0x80006fa8]:csrrs a3, fcsr, zero<br> [0x80006fac]:sw t6, 400(s1)<br>    |
| 588|[0x8001366c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80006fdc]:fmul.h t6, t5, t4, dyn<br> [0x80006fe0]:csrrs a3, fcsr, zero<br> [0x80006fe4]:sw t6, 408(s1)<br>    |
| 589|[0x80013674]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007014]:fmul.h t6, t5, t4, dyn<br> [0x80007018]:csrrs a3, fcsr, zero<br> [0x8000701c]:sw t6, 416(s1)<br>    |
| 590|[0x8001367c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1c9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000704c]:fmul.h t6, t5, t4, dyn<br> [0x80007050]:csrrs a3, fcsr, zero<br> [0x80007054]:sw t6, 424(s1)<br>    |
| 591|[0x80013684]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007084]:fmul.h t6, t5, t4, dyn<br> [0x80007088]:csrrs a3, fcsr, zero<br> [0x8000708c]:sw t6, 432(s1)<br>    |
| 592|[0x8001368c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800070bc]:fmul.h t6, t5, t4, dyn<br> [0x800070c0]:csrrs a3, fcsr, zero<br> [0x800070c4]:sw t6, 440(s1)<br>    |
| 593|[0x80013694]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800070f4]:fmul.h t6, t5, t4, dyn<br> [0x800070f8]:csrrs a3, fcsr, zero<br> [0x800070fc]:sw t6, 448(s1)<br>    |
| 594|[0x8001369c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000712c]:fmul.h t6, t5, t4, dyn<br> [0x80007130]:csrrs a3, fcsr, zero<br> [0x80007134]:sw t6, 456(s1)<br>    |
| 595|[0x800136a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007164]:fmul.h t6, t5, t4, dyn<br> [0x80007168]:csrrs a3, fcsr, zero<br> [0x8000716c]:sw t6, 464(s1)<br>    |
| 596|[0x800136ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000719c]:fmul.h t6, t5, t4, dyn<br> [0x800071a0]:csrrs a3, fcsr, zero<br> [0x800071a4]:sw t6, 472(s1)<br>    |
| 597|[0x800136b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800071d4]:fmul.h t6, t5, t4, dyn<br> [0x800071d8]:csrrs a3, fcsr, zero<br> [0x800071dc]:sw t6, 480(s1)<br>    |
| 598|[0x800136bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000720c]:fmul.h t6, t5, t4, dyn<br> [0x80007210]:csrrs a3, fcsr, zero<br> [0x80007214]:sw t6, 488(s1)<br>    |
| 599|[0x800136c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007244]:fmul.h t6, t5, t4, dyn<br> [0x80007248]:csrrs a3, fcsr, zero<br> [0x8000724c]:sw t6, 496(s1)<br>    |
| 600|[0x800136cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x262 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000727c]:fmul.h t6, t5, t4, dyn<br> [0x80007280]:csrrs a3, fcsr, zero<br> [0x80007284]:sw t6, 504(s1)<br>    |
| 601|[0x800136d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800072b4]:fmul.h t6, t5, t4, dyn<br> [0x800072b8]:csrrs a3, fcsr, zero<br> [0x800072bc]:sw t6, 512(s1)<br>    |
| 602|[0x800136dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800072ec]:fmul.h t6, t5, t4, dyn<br> [0x800072f0]:csrrs a3, fcsr, zero<br> [0x800072f4]:sw t6, 520(s1)<br>    |
| 603|[0x800136e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007324]:fmul.h t6, t5, t4, dyn<br> [0x80007328]:csrrs a3, fcsr, zero<br> [0x8000732c]:sw t6, 528(s1)<br>    |
| 604|[0x800136ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000735c]:fmul.h t6, t5, t4, dyn<br> [0x80007360]:csrrs a3, fcsr, zero<br> [0x80007364]:sw t6, 536(s1)<br>    |
| 605|[0x800136f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x035 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007394]:fmul.h t6, t5, t4, dyn<br> [0x80007398]:csrrs a3, fcsr, zero<br> [0x8000739c]:sw t6, 544(s1)<br>    |
| 606|[0x800136fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800073cc]:fmul.h t6, t5, t4, dyn<br> [0x800073d0]:csrrs a3, fcsr, zero<br> [0x800073d4]:sw t6, 552(s1)<br>    |
| 607|[0x80013704]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007404]:fmul.h t6, t5, t4, dyn<br> [0x80007408]:csrrs a3, fcsr, zero<br> [0x8000740c]:sw t6, 560(s1)<br>    |
| 608|[0x8001370c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000743c]:fmul.h t6, t5, t4, dyn<br> [0x80007440]:csrrs a3, fcsr, zero<br> [0x80007444]:sw t6, 568(s1)<br>    |
| 609|[0x80013714]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007474]:fmul.h t6, t5, t4, dyn<br> [0x80007478]:csrrs a3, fcsr, zero<br> [0x8000747c]:sw t6, 576(s1)<br>    |
| 610|[0x8001371c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800074ac]:fmul.h t6, t5, t4, dyn<br> [0x800074b0]:csrrs a3, fcsr, zero<br> [0x800074b4]:sw t6, 584(s1)<br>    |
| 611|[0x80013724]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800074e4]:fmul.h t6, t5, t4, dyn<br> [0x800074e8]:csrrs a3, fcsr, zero<br> [0x800074ec]:sw t6, 592(s1)<br>    |
| 612|[0x8001372c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000751c]:fmul.h t6, t5, t4, dyn<br> [0x80007520]:csrrs a3, fcsr, zero<br> [0x80007524]:sw t6, 600(s1)<br>    |
| 613|[0x80013734]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007554]:fmul.h t6, t5, t4, dyn<br> [0x80007558]:csrrs a3, fcsr, zero<br> [0x8000755c]:sw t6, 608(s1)<br>    |
| 614|[0x8001373c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000758c]:fmul.h t6, t5, t4, dyn<br> [0x80007590]:csrrs a3, fcsr, zero<br> [0x80007594]:sw t6, 616(s1)<br>    |
| 615|[0x80013744]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x373 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800075c4]:fmul.h t6, t5, t4, dyn<br> [0x800075c8]:csrrs a3, fcsr, zero<br> [0x800075cc]:sw t6, 624(s1)<br>    |
| 616|[0x8001374c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800075fc]:fmul.h t6, t5, t4, dyn<br> [0x80007600]:csrrs a3, fcsr, zero<br> [0x80007604]:sw t6, 632(s1)<br>    |
| 617|[0x80013754]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007634]:fmul.h t6, t5, t4, dyn<br> [0x80007638]:csrrs a3, fcsr, zero<br> [0x8000763c]:sw t6, 640(s1)<br>    |
| 618|[0x8001375c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000766c]:fmul.h t6, t5, t4, dyn<br> [0x80007670]:csrrs a3, fcsr, zero<br> [0x80007674]:sw t6, 648(s1)<br>    |
| 619|[0x80013764]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800076a4]:fmul.h t6, t5, t4, dyn<br> [0x800076a8]:csrrs a3, fcsr, zero<br> [0x800076ac]:sw t6, 656(s1)<br>    |
| 620|[0x8001376c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0be and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800076dc]:fmul.h t6, t5, t4, dyn<br> [0x800076e0]:csrrs a3, fcsr, zero<br> [0x800076e4]:sw t6, 664(s1)<br>    |
| 621|[0x80013774]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007714]:fmul.h t6, t5, t4, dyn<br> [0x80007718]:csrrs a3, fcsr, zero<br> [0x8000771c]:sw t6, 672(s1)<br>    |
| 622|[0x8001377c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000774c]:fmul.h t6, t5, t4, dyn<br> [0x80007750]:csrrs a3, fcsr, zero<br> [0x80007754]:sw t6, 680(s1)<br>    |
| 623|[0x80013784]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007784]:fmul.h t6, t5, t4, dyn<br> [0x80007788]:csrrs a3, fcsr, zero<br> [0x8000778c]:sw t6, 688(s1)<br>    |
| 624|[0x8001378c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800077bc]:fmul.h t6, t5, t4, dyn<br> [0x800077c0]:csrrs a3, fcsr, zero<br> [0x800077c4]:sw t6, 696(s1)<br>    |
| 625|[0x80013794]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3d1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800077f4]:fmul.h t6, t5, t4, dyn<br> [0x800077f8]:csrrs a3, fcsr, zero<br> [0x800077fc]:sw t6, 704(s1)<br>    |
| 626|[0x8001379c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000782c]:fmul.h t6, t5, t4, dyn<br> [0x80007830]:csrrs a3, fcsr, zero<br> [0x80007834]:sw t6, 712(s1)<br>    |
| 627|[0x800137a4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007864]:fmul.h t6, t5, t4, dyn<br> [0x80007868]:csrrs a3, fcsr, zero<br> [0x8000786c]:sw t6, 720(s1)<br>    |
| 628|[0x800137ac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000789c]:fmul.h t6, t5, t4, dyn<br> [0x800078a0]:csrrs a3, fcsr, zero<br> [0x800078a4]:sw t6, 728(s1)<br>    |
| 629|[0x800137b4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800078d4]:fmul.h t6, t5, t4, dyn<br> [0x800078d8]:csrrs a3, fcsr, zero<br> [0x800078dc]:sw t6, 736(s1)<br>    |
| 630|[0x800137bc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2d6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000790c]:fmul.h t6, t5, t4, dyn<br> [0x80007910]:csrrs a3, fcsr, zero<br> [0x80007914]:sw t6, 744(s1)<br>    |
| 631|[0x800137c4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007944]:fmul.h t6, t5, t4, dyn<br> [0x80007948]:csrrs a3, fcsr, zero<br> [0x8000794c]:sw t6, 752(s1)<br>    |
| 632|[0x800137cc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000797c]:fmul.h t6, t5, t4, dyn<br> [0x80007980]:csrrs a3, fcsr, zero<br> [0x80007984]:sw t6, 760(s1)<br>    |
| 633|[0x800137d4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800079b4]:fmul.h t6, t5, t4, dyn<br> [0x800079b8]:csrrs a3, fcsr, zero<br> [0x800079bc]:sw t6, 768(s1)<br>    |
| 634|[0x800137dc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800079ec]:fmul.h t6, t5, t4, dyn<br> [0x800079f0]:csrrs a3, fcsr, zero<br> [0x800079f4]:sw t6, 776(s1)<br>    |
| 635|[0x800137e4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x358 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007a24]:fmul.h t6, t5, t4, dyn<br> [0x80007a28]:csrrs a3, fcsr, zero<br> [0x80007a2c]:sw t6, 784(s1)<br>    |
| 636|[0x800137ec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007a5c]:fmul.h t6, t5, t4, dyn<br> [0x80007a60]:csrrs a3, fcsr, zero<br> [0x80007a64]:sw t6, 792(s1)<br>    |
| 637|[0x800137f4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007a94]:fmul.h t6, t5, t4, dyn<br> [0x80007a98]:csrrs a3, fcsr, zero<br> [0x80007a9c]:sw t6, 800(s1)<br>    |
| 638|[0x800137fc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007acc]:fmul.h t6, t5, t4, dyn<br> [0x80007ad0]:csrrs a3, fcsr, zero<br> [0x80007ad4]:sw t6, 808(s1)<br>    |
| 639|[0x80013804]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007b04]:fmul.h t6, t5, t4, dyn<br> [0x80007b08]:csrrs a3, fcsr, zero<br> [0x80007b0c]:sw t6, 816(s1)<br>    |
| 640|[0x8001380c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x28a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007b3c]:fmul.h t6, t5, t4, dyn<br> [0x80007b40]:csrrs a3, fcsr, zero<br> [0x80007b44]:sw t6, 824(s1)<br>    |
| 641|[0x80013814]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007b74]:fmul.h t6, t5, t4, dyn<br> [0x80007b78]:csrrs a3, fcsr, zero<br> [0x80007b7c]:sw t6, 832(s1)<br>    |
| 642|[0x8001381c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007bac]:fmul.h t6, t5, t4, dyn<br> [0x80007bb0]:csrrs a3, fcsr, zero<br> [0x80007bb4]:sw t6, 840(s1)<br>    |
| 643|[0x80013824]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007be4]:fmul.h t6, t5, t4, dyn<br> [0x80007be8]:csrrs a3, fcsr, zero<br> [0x80007bec]:sw t6, 848(s1)<br>    |
| 644|[0x8001382c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007c1c]:fmul.h t6, t5, t4, dyn<br> [0x80007c20]:csrrs a3, fcsr, zero<br> [0x80007c24]:sw t6, 856(s1)<br>    |
| 645|[0x80013834]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007c54]:fmul.h t6, t5, t4, dyn<br> [0x80007c58]:csrrs a3, fcsr, zero<br> [0x80007c5c]:sw t6, 864(s1)<br>    |
| 646|[0x8001383c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007c8c]:fmul.h t6, t5, t4, dyn<br> [0x80007c90]:csrrs a3, fcsr, zero<br> [0x80007c94]:sw t6, 872(s1)<br>    |
| 647|[0x80013844]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007cc4]:fmul.h t6, t5, t4, dyn<br> [0x80007cc8]:csrrs a3, fcsr, zero<br> [0x80007ccc]:sw t6, 880(s1)<br>    |
| 648|[0x8001384c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007cfc]:fmul.h t6, t5, t4, dyn<br> [0x80007d00]:csrrs a3, fcsr, zero<br> [0x80007d04]:sw t6, 888(s1)<br>    |
| 649|[0x80013854]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007d34]:fmul.h t6, t5, t4, dyn<br> [0x80007d38]:csrrs a3, fcsr, zero<br> [0x80007d3c]:sw t6, 896(s1)<br>    |
| 650|[0x8001385c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x0af and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007d6c]:fmul.h t6, t5, t4, dyn<br> [0x80007d70]:csrrs a3, fcsr, zero<br> [0x80007d74]:sw t6, 904(s1)<br>    |
| 651|[0x80013864]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007da4]:fmul.h t6, t5, t4, dyn<br> [0x80007da8]:csrrs a3, fcsr, zero<br> [0x80007dac]:sw t6, 912(s1)<br>    |
| 652|[0x8001386c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007ddc]:fmul.h t6, t5, t4, dyn<br> [0x80007de0]:csrrs a3, fcsr, zero<br> [0x80007de4]:sw t6, 920(s1)<br>    |
| 653|[0x80013874]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007e14]:fmul.h t6, t5, t4, dyn<br> [0x80007e18]:csrrs a3, fcsr, zero<br> [0x80007e1c]:sw t6, 928(s1)<br>    |
| 654|[0x8001387c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007e4c]:fmul.h t6, t5, t4, dyn<br> [0x80007e50]:csrrs a3, fcsr, zero<br> [0x80007e54]:sw t6, 936(s1)<br>    |
| 655|[0x80013884]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x046 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007e84]:fmul.h t6, t5, t4, dyn<br> [0x80007e88]:csrrs a3, fcsr, zero<br> [0x80007e8c]:sw t6, 944(s1)<br>    |
| 656|[0x8001388c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007ebc]:fmul.h t6, t5, t4, dyn<br> [0x80007ec0]:csrrs a3, fcsr, zero<br> [0x80007ec4]:sw t6, 952(s1)<br>    |
| 657|[0x80013894]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007ef4]:fmul.h t6, t5, t4, dyn<br> [0x80007ef8]:csrrs a3, fcsr, zero<br> [0x80007efc]:sw t6, 960(s1)<br>    |
| 658|[0x8001389c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007f2c]:fmul.h t6, t5, t4, dyn<br> [0x80007f30]:csrrs a3, fcsr, zero<br> [0x80007f34]:sw t6, 968(s1)<br>    |
| 659|[0x800138a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007f64]:fmul.h t6, t5, t4, dyn<br> [0x80007f68]:csrrs a3, fcsr, zero<br> [0x80007f6c]:sw t6, 976(s1)<br>    |
| 660|[0x800138ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80007f9c]:fmul.h t6, t5, t4, dyn<br> [0x80007fa0]:csrrs a3, fcsr, zero<br> [0x80007fa4]:sw t6, 984(s1)<br>    |
| 661|[0x800138b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80007fd4]:fmul.h t6, t5, t4, dyn<br> [0x80007fd8]:csrrs a3, fcsr, zero<br> [0x80007fdc]:sw t6, 992(s1)<br>    |
| 662|[0x800138bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000800c]:fmul.h t6, t5, t4, dyn<br> [0x80008010]:csrrs a3, fcsr, zero<br> [0x80008014]:sw t6, 1000(s1)<br>   |
| 663|[0x800138c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008044]:fmul.h t6, t5, t4, dyn<br> [0x80008048]:csrrs a3, fcsr, zero<br> [0x8000804c]:sw t6, 1008(s1)<br>   |
| 664|[0x800138cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000807c]:fmul.h t6, t5, t4, dyn<br> [0x80008080]:csrrs a3, fcsr, zero<br> [0x80008084]:sw t6, 1016(s1)<br>   |
| 665|[0x800138d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800080bc]:fmul.h t6, t5, t4, dyn<br> [0x800080c0]:csrrs a3, fcsr, zero<br> [0x800080c4]:sw t6, 0(s1)<br>      |
| 666|[0x800138dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800080f4]:fmul.h t6, t5, t4, dyn<br> [0x800080f8]:csrrs a3, fcsr, zero<br> [0x800080fc]:sw t6, 8(s1)<br>      |
| 667|[0x800138e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000812c]:fmul.h t6, t5, t4, dyn<br> [0x80008130]:csrrs a3, fcsr, zero<br> [0x80008134]:sw t6, 16(s1)<br>     |
| 668|[0x800138ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008164]:fmul.h t6, t5, t4, dyn<br> [0x80008168]:csrrs a3, fcsr, zero<br> [0x8000816c]:sw t6, 24(s1)<br>     |
| 669|[0x800138f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000819c]:fmul.h t6, t5, t4, dyn<br> [0x800081a0]:csrrs a3, fcsr, zero<br> [0x800081a4]:sw t6, 32(s1)<br>     |
| 670|[0x800138fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0bd and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800081d4]:fmul.h t6, t5, t4, dyn<br> [0x800081d8]:csrrs a3, fcsr, zero<br> [0x800081dc]:sw t6, 40(s1)<br>     |
| 671|[0x80013904]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000820c]:fmul.h t6, t5, t4, dyn<br> [0x80008210]:csrrs a3, fcsr, zero<br> [0x80008214]:sw t6, 48(s1)<br>     |
| 672|[0x8001390c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008244]:fmul.h t6, t5, t4, dyn<br> [0x80008248]:csrrs a3, fcsr, zero<br> [0x8000824c]:sw t6, 56(s1)<br>     |
| 673|[0x80013914]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000827c]:fmul.h t6, t5, t4, dyn<br> [0x80008280]:csrrs a3, fcsr, zero<br> [0x80008284]:sw t6, 64(s1)<br>     |
| 674|[0x8001391c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800082b4]:fmul.h t6, t5, t4, dyn<br> [0x800082b8]:csrrs a3, fcsr, zero<br> [0x800082bc]:sw t6, 72(s1)<br>     |
| 675|[0x80013924]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800082ec]:fmul.h t6, t5, t4, dyn<br> [0x800082f0]:csrrs a3, fcsr, zero<br> [0x800082f4]:sw t6, 80(s1)<br>     |
| 676|[0x8001392c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008324]:fmul.h t6, t5, t4, dyn<br> [0x80008328]:csrrs a3, fcsr, zero<br> [0x8000832c]:sw t6, 88(s1)<br>     |
| 677|[0x80013934]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000835c]:fmul.h t6, t5, t4, dyn<br> [0x80008360]:csrrs a3, fcsr, zero<br> [0x80008364]:sw t6, 96(s1)<br>     |
| 678|[0x8001393c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008394]:fmul.h t6, t5, t4, dyn<br> [0x80008398]:csrrs a3, fcsr, zero<br> [0x8000839c]:sw t6, 104(s1)<br>    |
| 679|[0x80013944]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800083cc]:fmul.h t6, t5, t4, dyn<br> [0x800083d0]:csrrs a3, fcsr, zero<br> [0x800083d4]:sw t6, 112(s1)<br>    |
| 680|[0x8001394c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x304 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008404]:fmul.h t6, t5, t4, dyn<br> [0x80008408]:csrrs a3, fcsr, zero<br> [0x8000840c]:sw t6, 120(s1)<br>    |
| 681|[0x80013954]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000843c]:fmul.h t6, t5, t4, dyn<br> [0x80008440]:csrrs a3, fcsr, zero<br> [0x80008444]:sw t6, 128(s1)<br>    |
| 682|[0x8001395c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008474]:fmul.h t6, t5, t4, dyn<br> [0x80008478]:csrrs a3, fcsr, zero<br> [0x8000847c]:sw t6, 136(s1)<br>    |
| 683|[0x80013964]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800084ac]:fmul.h t6, t5, t4, dyn<br> [0x800084b0]:csrrs a3, fcsr, zero<br> [0x800084b4]:sw t6, 144(s1)<br>    |
| 684|[0x8001396c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800084e4]:fmul.h t6, t5, t4, dyn<br> [0x800084e8]:csrrs a3, fcsr, zero<br> [0x800084ec]:sw t6, 152(s1)<br>    |
| 685|[0x80013974]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x32b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000851c]:fmul.h t6, t5, t4, dyn<br> [0x80008520]:csrrs a3, fcsr, zero<br> [0x80008524]:sw t6, 160(s1)<br>    |
| 686|[0x8001397c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008554]:fmul.h t6, t5, t4, dyn<br> [0x80008558]:csrrs a3, fcsr, zero<br> [0x8000855c]:sw t6, 168(s1)<br>    |
| 687|[0x80013984]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000858c]:fmul.h t6, t5, t4, dyn<br> [0x80008590]:csrrs a3, fcsr, zero<br> [0x80008594]:sw t6, 176(s1)<br>    |
| 688|[0x8001398c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800085c4]:fmul.h t6, t5, t4, dyn<br> [0x800085c8]:csrrs a3, fcsr, zero<br> [0x800085cc]:sw t6, 184(s1)<br>    |
| 689|[0x80013994]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800085fc]:fmul.h t6, t5, t4, dyn<br> [0x80008600]:csrrs a3, fcsr, zero<br> [0x80008604]:sw t6, 192(s1)<br>    |
| 690|[0x8001399c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x398 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008634]:fmul.h t6, t5, t4, dyn<br> [0x80008638]:csrrs a3, fcsr, zero<br> [0x8000863c]:sw t6, 200(s1)<br>    |
| 691|[0x800139a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000866c]:fmul.h t6, t5, t4, dyn<br> [0x80008670]:csrrs a3, fcsr, zero<br> [0x80008674]:sw t6, 208(s1)<br>    |
| 692|[0x800139ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800086a4]:fmul.h t6, t5, t4, dyn<br> [0x800086a8]:csrrs a3, fcsr, zero<br> [0x800086ac]:sw t6, 216(s1)<br>    |
| 693|[0x800139b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800086dc]:fmul.h t6, t5, t4, dyn<br> [0x800086e0]:csrrs a3, fcsr, zero<br> [0x800086e4]:sw t6, 224(s1)<br>    |
| 694|[0x800139bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008714]:fmul.h t6, t5, t4, dyn<br> [0x80008718]:csrrs a3, fcsr, zero<br> [0x8000871c]:sw t6, 232(s1)<br>    |
| 695|[0x800139c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x226 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000874c]:fmul.h t6, t5, t4, dyn<br> [0x80008750]:csrrs a3, fcsr, zero<br> [0x80008754]:sw t6, 240(s1)<br>    |
| 696|[0x800139cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008784]:fmul.h t6, t5, t4, dyn<br> [0x80008788]:csrrs a3, fcsr, zero<br> [0x8000878c]:sw t6, 248(s1)<br>    |
| 697|[0x800139d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800087bc]:fmul.h t6, t5, t4, dyn<br> [0x800087c0]:csrrs a3, fcsr, zero<br> [0x800087c4]:sw t6, 256(s1)<br>    |
| 698|[0x800139dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800087f4]:fmul.h t6, t5, t4, dyn<br> [0x800087f8]:csrrs a3, fcsr, zero<br> [0x800087fc]:sw t6, 264(s1)<br>    |
| 699|[0x800139e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000882c]:fmul.h t6, t5, t4, dyn<br> [0x80008830]:csrrs a3, fcsr, zero<br> [0x80008834]:sw t6, 272(s1)<br>    |
| 700|[0x800139ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008864]:fmul.h t6, t5, t4, dyn<br> [0x80008868]:csrrs a3, fcsr, zero<br> [0x8000886c]:sw t6, 280(s1)<br>    |
| 701|[0x800139f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000889c]:fmul.h t6, t5, t4, dyn<br> [0x800088a0]:csrrs a3, fcsr, zero<br> [0x800088a4]:sw t6, 288(s1)<br>    |
| 702|[0x800139fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800088d4]:fmul.h t6, t5, t4, dyn<br> [0x800088d8]:csrrs a3, fcsr, zero<br> [0x800088dc]:sw t6, 296(s1)<br>    |
| 703|[0x80013a04]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000890c]:fmul.h t6, t5, t4, dyn<br> [0x80008910]:csrrs a3, fcsr, zero<br> [0x80008914]:sw t6, 304(s1)<br>    |
| 704|[0x80013a0c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008944]:fmul.h t6, t5, t4, dyn<br> [0x80008948]:csrrs a3, fcsr, zero<br> [0x8000894c]:sw t6, 312(s1)<br>    |
| 705|[0x80013a14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000897c]:fmul.h t6, t5, t4, dyn<br> [0x80008980]:csrrs a3, fcsr, zero<br> [0x80008984]:sw t6, 320(s1)<br>    |
| 706|[0x80013a1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800089b4]:fmul.h t6, t5, t4, dyn<br> [0x800089b8]:csrrs a3, fcsr, zero<br> [0x800089bc]:sw t6, 328(s1)<br>    |
| 707|[0x80013a24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800089ec]:fmul.h t6, t5, t4, dyn<br> [0x800089f0]:csrrs a3, fcsr, zero<br> [0x800089f4]:sw t6, 336(s1)<br>    |
| 708|[0x80013a2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008a24]:fmul.h t6, t5, t4, dyn<br> [0x80008a28]:csrrs a3, fcsr, zero<br> [0x80008a2c]:sw t6, 344(s1)<br>    |
| 709|[0x80013a34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008a5c]:fmul.h t6, t5, t4, dyn<br> [0x80008a60]:csrrs a3, fcsr, zero<br> [0x80008a64]:sw t6, 352(s1)<br>    |
| 710|[0x80013a3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x19f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008a94]:fmul.h t6, t5, t4, dyn<br> [0x80008a98]:csrrs a3, fcsr, zero<br> [0x80008a9c]:sw t6, 360(s1)<br>    |
| 711|[0x80013a44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008acc]:fmul.h t6, t5, t4, dyn<br> [0x80008ad0]:csrrs a3, fcsr, zero<br> [0x80008ad4]:sw t6, 368(s1)<br>    |
| 712|[0x80013a4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008b04]:fmul.h t6, t5, t4, dyn<br> [0x80008b08]:csrrs a3, fcsr, zero<br> [0x80008b0c]:sw t6, 376(s1)<br>    |
| 713|[0x80013a54]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008b3c]:fmul.h t6, t5, t4, dyn<br> [0x80008b40]:csrrs a3, fcsr, zero<br> [0x80008b44]:sw t6, 384(s1)<br>    |
| 714|[0x80013a5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008b74]:fmul.h t6, t5, t4, dyn<br> [0x80008b78]:csrrs a3, fcsr, zero<br> [0x80008b7c]:sw t6, 392(s1)<br>    |
| 715|[0x80013a64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3f5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008bac]:fmul.h t6, t5, t4, dyn<br> [0x80008bb0]:csrrs a3, fcsr, zero<br> [0x80008bb4]:sw t6, 400(s1)<br>    |
| 716|[0x80013a6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008be4]:fmul.h t6, t5, t4, dyn<br> [0x80008be8]:csrrs a3, fcsr, zero<br> [0x80008bec]:sw t6, 408(s1)<br>    |
| 717|[0x80013a74]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008c1c]:fmul.h t6, t5, t4, dyn<br> [0x80008c20]:csrrs a3, fcsr, zero<br> [0x80008c24]:sw t6, 416(s1)<br>    |
| 718|[0x80013a7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008c54]:fmul.h t6, t5, t4, dyn<br> [0x80008c58]:csrrs a3, fcsr, zero<br> [0x80008c5c]:sw t6, 424(s1)<br>    |
| 719|[0x80013a84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008c8c]:fmul.h t6, t5, t4, dyn<br> [0x80008c90]:csrrs a3, fcsr, zero<br> [0x80008c94]:sw t6, 432(s1)<br>    |
| 720|[0x80013a8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x095 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008cc4]:fmul.h t6, t5, t4, dyn<br> [0x80008cc8]:csrrs a3, fcsr, zero<br> [0x80008ccc]:sw t6, 440(s1)<br>    |
| 721|[0x80013a94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008cfc]:fmul.h t6, t5, t4, dyn<br> [0x80008d00]:csrrs a3, fcsr, zero<br> [0x80008d04]:sw t6, 448(s1)<br>    |
| 722|[0x80013a9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008d34]:fmul.h t6, t5, t4, dyn<br> [0x80008d38]:csrrs a3, fcsr, zero<br> [0x80008d3c]:sw t6, 456(s1)<br>    |
| 723|[0x80013aa4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008d6c]:fmul.h t6, t5, t4, dyn<br> [0x80008d70]:csrrs a3, fcsr, zero<br> [0x80008d74]:sw t6, 464(s1)<br>    |
| 724|[0x80013aac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008da4]:fmul.h t6, t5, t4, dyn<br> [0x80008da8]:csrrs a3, fcsr, zero<br> [0x80008dac]:sw t6, 472(s1)<br>    |
| 725|[0x80013ab4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x30e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008ddc]:fmul.h t6, t5, t4, dyn<br> [0x80008de0]:csrrs a3, fcsr, zero<br> [0x80008de4]:sw t6, 480(s1)<br>    |
| 726|[0x80013abc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008e14]:fmul.h t6, t5, t4, dyn<br> [0x80008e18]:csrrs a3, fcsr, zero<br> [0x80008e1c]:sw t6, 488(s1)<br>    |
| 727|[0x80013ac4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008e4c]:fmul.h t6, t5, t4, dyn<br> [0x80008e50]:csrrs a3, fcsr, zero<br> [0x80008e54]:sw t6, 496(s1)<br>    |
| 728|[0x80013acc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008e84]:fmul.h t6, t5, t4, dyn<br> [0x80008e88]:csrrs a3, fcsr, zero<br> [0x80008e8c]:sw t6, 504(s1)<br>    |
| 729|[0x80013ad4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008ebc]:fmul.h t6, t5, t4, dyn<br> [0x80008ec0]:csrrs a3, fcsr, zero<br> [0x80008ec4]:sw t6, 512(s1)<br>    |
| 730|[0x80013adc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x0c2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008ef4]:fmul.h t6, t5, t4, dyn<br> [0x80008ef8]:csrrs a3, fcsr, zero<br> [0x80008efc]:sw t6, 520(s1)<br>    |
| 731|[0x80013ae4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80008f2c]:fmul.h t6, t5, t4, dyn<br> [0x80008f30]:csrrs a3, fcsr, zero<br> [0x80008f34]:sw t6, 528(s1)<br>    |
| 732|[0x80013aec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008f64]:fmul.h t6, t5, t4, dyn<br> [0x80008f68]:csrrs a3, fcsr, zero<br> [0x80008f6c]:sw t6, 536(s1)<br>    |
| 733|[0x80013af4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008f9c]:fmul.h t6, t5, t4, dyn<br> [0x80008fa0]:csrrs a3, fcsr, zero<br> [0x80008fa4]:sw t6, 544(s1)<br>    |
| 734|[0x80013afc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80008fd4]:fmul.h t6, t5, t4, dyn<br> [0x80008fd8]:csrrs a3, fcsr, zero<br> [0x80008fdc]:sw t6, 552(s1)<br>    |
| 735|[0x80013b04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x1c5 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000900c]:fmul.h t6, t5, t4, dyn<br> [0x80009010]:csrrs a3, fcsr, zero<br> [0x80009014]:sw t6, 560(s1)<br>    |
| 736|[0x80013b0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009044]:fmul.h t6, t5, t4, dyn<br> [0x80009048]:csrrs a3, fcsr, zero<br> [0x8000904c]:sw t6, 568(s1)<br>    |
| 737|[0x80013b14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000907c]:fmul.h t6, t5, t4, dyn<br> [0x80009080]:csrrs a3, fcsr, zero<br> [0x80009084]:sw t6, 576(s1)<br>    |
| 738|[0x80013b1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800090b4]:fmul.h t6, t5, t4, dyn<br> [0x800090b8]:csrrs a3, fcsr, zero<br> [0x800090bc]:sw t6, 584(s1)<br>    |
| 739|[0x80013b24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800090ec]:fmul.h t6, t5, t4, dyn<br> [0x800090f0]:csrrs a3, fcsr, zero<br> [0x800090f4]:sw t6, 592(s1)<br>    |
| 740|[0x80013b2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3b7 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009124]:fmul.h t6, t5, t4, dyn<br> [0x80009128]:csrrs a3, fcsr, zero<br> [0x8000912c]:sw t6, 600(s1)<br>    |
| 741|[0x80013b34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000915c]:fmul.h t6, t5, t4, dyn<br> [0x80009160]:csrrs a3, fcsr, zero<br> [0x80009164]:sw t6, 608(s1)<br>    |
| 742|[0x80013b3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009194]:fmul.h t6, t5, t4, dyn<br> [0x80009198]:csrrs a3, fcsr, zero<br> [0x8000919c]:sw t6, 616(s1)<br>    |
| 743|[0x80013b44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800091cc]:fmul.h t6, t5, t4, dyn<br> [0x800091d0]:csrrs a3, fcsr, zero<br> [0x800091d4]:sw t6, 624(s1)<br>    |
| 744|[0x80013b4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009204]:fmul.h t6, t5, t4, dyn<br> [0x80009208]:csrrs a3, fcsr, zero<br> [0x8000920c]:sw t6, 632(s1)<br>    |
| 745|[0x80013b54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000923c]:fmul.h t6, t5, t4, dyn<br> [0x80009240]:csrrs a3, fcsr, zero<br> [0x80009244]:sw t6, 640(s1)<br>    |
| 746|[0x80013b5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009274]:fmul.h t6, t5, t4, dyn<br> [0x80009278]:csrrs a3, fcsr, zero<br> [0x8000927c]:sw t6, 648(s1)<br>    |
| 747|[0x80013b64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800092ac]:fmul.h t6, t5, t4, dyn<br> [0x800092b0]:csrrs a3, fcsr, zero<br> [0x800092b4]:sw t6, 656(s1)<br>    |
| 748|[0x80013b6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800092e4]:fmul.h t6, t5, t4, dyn<br> [0x800092e8]:csrrs a3, fcsr, zero<br> [0x800092ec]:sw t6, 664(s1)<br>    |
| 749|[0x80013b74]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000931c]:fmul.h t6, t5, t4, dyn<br> [0x80009320]:csrrs a3, fcsr, zero<br> [0x80009324]:sw t6, 672(s1)<br>    |
| 750|[0x80013b7c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1a3 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009354]:fmul.h t6, t5, t4, dyn<br> [0x80009358]:csrrs a3, fcsr, zero<br> [0x8000935c]:sw t6, 680(s1)<br>    |
| 751|[0x80013b84]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000938c]:fmul.h t6, t5, t4, dyn<br> [0x80009390]:csrrs a3, fcsr, zero<br> [0x80009394]:sw t6, 688(s1)<br>    |
| 752|[0x80013b8c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800093c4]:fmul.h t6, t5, t4, dyn<br> [0x800093c8]:csrrs a3, fcsr, zero<br> [0x800093cc]:sw t6, 696(s1)<br>    |
| 753|[0x80013b94]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800093fc]:fmul.h t6, t5, t4, dyn<br> [0x80009400]:csrrs a3, fcsr, zero<br> [0x80009404]:sw t6, 704(s1)<br>    |
| 754|[0x80013b9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009434]:fmul.h t6, t5, t4, dyn<br> [0x80009438]:csrrs a3, fcsr, zero<br> [0x8000943c]:sw t6, 712(s1)<br>    |
| 755|[0x80013ba4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3b1 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000946c]:fmul.h t6, t5, t4, dyn<br> [0x80009470]:csrrs a3, fcsr, zero<br> [0x80009474]:sw t6, 720(s1)<br>    |
| 756|[0x80013bac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800094a4]:fmul.h t6, t5, t4, dyn<br> [0x800094a8]:csrrs a3, fcsr, zero<br> [0x800094ac]:sw t6, 728(s1)<br>    |
| 757|[0x80013bb4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800094dc]:fmul.h t6, t5, t4, dyn<br> [0x800094e0]:csrrs a3, fcsr, zero<br> [0x800094e4]:sw t6, 736(s1)<br>    |
| 758|[0x80013bbc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009514]:fmul.h t6, t5, t4, dyn<br> [0x80009518]:csrrs a3, fcsr, zero<br> [0x8000951c]:sw t6, 744(s1)<br>    |
| 759|[0x80013bc4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000954c]:fmul.h t6, t5, t4, dyn<br> [0x80009550]:csrrs a3, fcsr, zero<br> [0x80009554]:sw t6, 752(s1)<br>    |
| 760|[0x80013bcc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009584]:fmul.h t6, t5, t4, dyn<br> [0x80009588]:csrrs a3, fcsr, zero<br> [0x8000958c]:sw t6, 760(s1)<br>    |
| 761|[0x80013bd4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800095bc]:fmul.h t6, t5, t4, dyn<br> [0x800095c0]:csrrs a3, fcsr, zero<br> [0x800095c4]:sw t6, 768(s1)<br>    |
| 762|[0x80013bdc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800095f4]:fmul.h t6, t5, t4, dyn<br> [0x800095f8]:csrrs a3, fcsr, zero<br> [0x800095fc]:sw t6, 776(s1)<br>    |
| 763|[0x80013be4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000962c]:fmul.h t6, t5, t4, dyn<br> [0x80009630]:csrrs a3, fcsr, zero<br> [0x80009634]:sw t6, 784(s1)<br>    |
| 764|[0x80013bec]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009664]:fmul.h t6, t5, t4, dyn<br> [0x80009668]:csrrs a3, fcsr, zero<br> [0x8000966c]:sw t6, 792(s1)<br>    |
| 765|[0x80013bf4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x217 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000969c]:fmul.h t6, t5, t4, dyn<br> [0x800096a0]:csrrs a3, fcsr, zero<br> [0x800096a4]:sw t6, 800(s1)<br>    |
| 766|[0x80013bfc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800096d4]:fmul.h t6, t5, t4, dyn<br> [0x800096d8]:csrrs a3, fcsr, zero<br> [0x800096dc]:sw t6, 808(s1)<br>    |
| 767|[0x80013c04]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000970c]:fmul.h t6, t5, t4, dyn<br> [0x80009710]:csrrs a3, fcsr, zero<br> [0x80009714]:sw t6, 816(s1)<br>    |
| 768|[0x80013c0c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009744]:fmul.h t6, t5, t4, dyn<br> [0x80009748]:csrrs a3, fcsr, zero<br> [0x8000974c]:sw t6, 824(s1)<br>    |
| 769|[0x80013c14]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000977c]:fmul.h t6, t5, t4, dyn<br> [0x80009780]:csrrs a3, fcsr, zero<br> [0x80009784]:sw t6, 832(s1)<br>    |
| 770|[0x80013c1c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x332 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800097b4]:fmul.h t6, t5, t4, dyn<br> [0x800097b8]:csrrs a3, fcsr, zero<br> [0x800097bc]:sw t6, 840(s1)<br>    |
| 771|[0x80013c24]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x800097ec]:fmul.h t6, t5, t4, dyn<br> [0x800097f0]:csrrs a3, fcsr, zero<br> [0x800097f4]:sw t6, 848(s1)<br>    |
| 772|[0x80013c2c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009824]:fmul.h t6, t5, t4, dyn<br> [0x80009828]:csrrs a3, fcsr, zero<br> [0x8000982c]:sw t6, 856(s1)<br>    |
| 773|[0x80013c34]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000985c]:fmul.h t6, t5, t4, dyn<br> [0x80009860]:csrrs a3, fcsr, zero<br> [0x80009864]:sw t6, 864(s1)<br>    |
| 774|[0x80013c3c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009894]:fmul.h t6, t5, t4, dyn<br> [0x80009898]:csrrs a3, fcsr, zero<br> [0x8000989c]:sw t6, 872(s1)<br>    |
| 775|[0x80013c44]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x270 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800098cc]:fmul.h t6, t5, t4, dyn<br> [0x800098d0]:csrrs a3, fcsr, zero<br> [0x800098d4]:sw t6, 880(s1)<br>    |
| 776|[0x80013c4c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009904]:fmul.h t6, t5, t4, dyn<br> [0x80009908]:csrrs a3, fcsr, zero<br> [0x8000990c]:sw t6, 888(s1)<br>    |
| 777|[0x80013c54]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000993c]:fmul.h t6, t5, t4, dyn<br> [0x80009940]:csrrs a3, fcsr, zero<br> [0x80009944]:sw t6, 896(s1)<br>    |
| 778|[0x80013c5c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009974]:fmul.h t6, t5, t4, dyn<br> [0x80009978]:csrrs a3, fcsr, zero<br> [0x8000997c]:sw t6, 904(s1)<br>    |
| 779|[0x80013c64]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800099ac]:fmul.h t6, t5, t4, dyn<br> [0x800099b0]:csrrs a3, fcsr, zero<br> [0x800099b4]:sw t6, 912(s1)<br>    |
| 780|[0x80013c6c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3d0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x800099e4]:fmul.h t6, t5, t4, dyn<br> [0x800099e8]:csrrs a3, fcsr, zero<br> [0x800099ec]:sw t6, 920(s1)<br>    |
| 781|[0x80013c74]<br>0xFFFF82F9<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009a1c]:fmul.h t6, t5, t4, dyn<br> [0x80009a20]:csrrs a3, fcsr, zero<br> [0x80009a24]:sw t6, 928(s1)<br>    |
| 782|[0x80013c7c]<br>0xFFFF82F9<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009a54]:fmul.h t6, t5, t4, dyn<br> [0x80009a58]:csrrs a3, fcsr, zero<br> [0x80009a5c]:sw t6, 936(s1)<br>    |
| 783|[0x80013c84]<br>0xFFFF82F9<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009a8c]:fmul.h t6, t5, t4, dyn<br> [0x80009a90]:csrrs a3, fcsr, zero<br> [0x80009a94]:sw t6, 944(s1)<br>    |
| 784|[0x80013c8c]<br>0xFFFF82F9<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009ac4]:fmul.h t6, t5, t4, dyn<br> [0x80009ac8]:csrrs a3, fcsr, zero<br> [0x80009acc]:sw t6, 952(s1)<br>    |
| 785|[0x80013c94]<br>0xFFFF82F9<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x1f2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009afc]:fmul.h t6, t5, t4, dyn<br> [0x80009b00]:csrrs a3, fcsr, zero<br> [0x80009b04]:sw t6, 960(s1)<br>    |
| 786|[0x80013c9c]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009b34]:fmul.h t6, t5, t4, dyn<br> [0x80009b38]:csrrs a3, fcsr, zero<br> [0x80009b3c]:sw t6, 968(s1)<br>    |
| 787|[0x80013ca4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009b6c]:fmul.h t6, t5, t4, dyn<br> [0x80009b70]:csrrs a3, fcsr, zero<br> [0x80009b74]:sw t6, 976(s1)<br>    |
| 788|[0x80013cac]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009ba4]:fmul.h t6, t5, t4, dyn<br> [0x80009ba8]:csrrs a3, fcsr, zero<br> [0x80009bac]:sw t6, 984(s1)<br>    |
| 789|[0x80013cb4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009bdc]:fmul.h t6, t5, t4, dyn<br> [0x80009be0]:csrrs a3, fcsr, zero<br> [0x80009be4]:sw t6, 992(s1)<br>    |
| 790|[0x80013cbc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1b0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009c1c]:fmul.h t6, t5, t4, dyn<br> [0x80009c20]:csrrs a3, fcsr, zero<br> [0x80009c24]:sw t6, 1000(s1)<br>   |
| 791|[0x80013cc4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009c5c]:fmul.h t6, t5, t4, dyn<br> [0x80009c60]:csrrs a3, fcsr, zero<br> [0x80009c64]:sw t6, 1008(s1)<br>   |
| 792|[0x80013ccc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009c9c]:fmul.h t6, t5, t4, dyn<br> [0x80009ca0]:csrrs a3, fcsr, zero<br> [0x80009ca4]:sw t6, 1016(s1)<br>   |
| 793|[0x80013cd4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009ce4]:fmul.h t6, t5, t4, dyn<br> [0x80009ce8]:csrrs a3, fcsr, zero<br> [0x80009cec]:sw t6, 0(s1)<br>      |
| 794|[0x80013cdc]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009d24]:fmul.h t6, t5, t4, dyn<br> [0x80009d28]:csrrs a3, fcsr, zero<br> [0x80009d2c]:sw t6, 8(s1)<br>      |
| 795|[0x80013ce4]<br>0xFFFF8000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x132 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009d64]:fmul.h t6, t5, t4, dyn<br> [0x80009d68]:csrrs a3, fcsr, zero<br> [0x80009d6c]:sw t6, 16(s1)<br>     |
| 796|[0x80013cec]<br>0x00000463<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009da4]:fmul.h t6, t5, t4, dyn<br> [0x80009da8]:csrrs a3, fcsr, zero<br> [0x80009dac]:sw t6, 24(s1)<br>     |
| 797|[0x80013cf4]<br>0x00000463<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009de4]:fmul.h t6, t5, t4, dyn<br> [0x80009de8]:csrrs a3, fcsr, zero<br> [0x80009dec]:sw t6, 32(s1)<br>     |
| 798|[0x80013cfc]<br>0x00000463<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009e24]:fmul.h t6, t5, t4, dyn<br> [0x80009e28]:csrrs a3, fcsr, zero<br> [0x80009e2c]:sw t6, 40(s1)<br>     |
| 799|[0x80013d04]<br>0x00000463<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009e64]:fmul.h t6, t5, t4, dyn<br> [0x80009e68]:csrrs a3, fcsr, zero<br> [0x80009e6c]:sw t6, 48(s1)<br>     |
| 800|[0x80013d0c]<br>0x00000463<br> |- fs1 == 0 and fe1 == 0x19 and fm1 == 0x063 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009ea4]:fmul.h t6, t5, t4, dyn<br> [0x80009ea8]:csrrs a3, fcsr, zero<br> [0x80009eac]:sw t6, 56(s1)<br>     |
| 801|[0x80013d14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x80009ee4]:fmul.h t6, t5, t4, dyn<br> [0x80009ee8]:csrrs a3, fcsr, zero<br> [0x80009eec]:sw t6, 64(s1)<br>     |
| 802|[0x80013d1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009f24]:fmul.h t6, t5, t4, dyn<br> [0x80009f28]:csrrs a3, fcsr, zero<br> [0x80009f2c]:sw t6, 72(s1)<br>     |
| 803|[0x80013d24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009f64]:fmul.h t6, t5, t4, dyn<br> [0x80009f68]:csrrs a3, fcsr, zero<br> [0x80009f6c]:sw t6, 80(s1)<br>     |
| 804|[0x80013d2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009fa4]:fmul.h t6, t5, t4, dyn<br> [0x80009fa8]:csrrs a3, fcsr, zero<br> [0x80009fac]:sw t6, 88(s1)<br>     |
| 805|[0x80013d34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1d5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x80009fe4]:fmul.h t6, t5, t4, dyn<br> [0x80009fe8]:csrrs a3, fcsr, zero<br> [0x80009fec]:sw t6, 96(s1)<br>     |
| 806|[0x80013d3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a024]:fmul.h t6, t5, t4, dyn<br> [0x8000a028]:csrrs a3, fcsr, zero<br> [0x8000a02c]:sw t6, 104(s1)<br>    |
| 807|[0x80013d44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a064]:fmul.h t6, t5, t4, dyn<br> [0x8000a068]:csrrs a3, fcsr, zero<br> [0x8000a06c]:sw t6, 112(s1)<br>    |
| 808|[0x80013d4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a0a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a0a8]:csrrs a3, fcsr, zero<br> [0x8000a0ac]:sw t6, 120(s1)<br>    |
| 809|[0x80013d54]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a0e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a0e8]:csrrs a3, fcsr, zero<br> [0x8000a0ec]:sw t6, 128(s1)<br>    |
| 810|[0x80013d5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x33d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a124]:fmul.h t6, t5, t4, dyn<br> [0x8000a128]:csrrs a3, fcsr, zero<br> [0x8000a12c]:sw t6, 136(s1)<br>    |
| 811|[0x80013d64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a164]:fmul.h t6, t5, t4, dyn<br> [0x8000a168]:csrrs a3, fcsr, zero<br> [0x8000a16c]:sw t6, 144(s1)<br>    |
| 812|[0x80013d6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a1a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a1a8]:csrrs a3, fcsr, zero<br> [0x8000a1ac]:sw t6, 152(s1)<br>    |
| 813|[0x80013d74]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a1e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a1e8]:csrrs a3, fcsr, zero<br> [0x8000a1ec]:sw t6, 160(s1)<br>    |
| 814|[0x80013d7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a224]:fmul.h t6, t5, t4, dyn<br> [0x8000a228]:csrrs a3, fcsr, zero<br> [0x8000a22c]:sw t6, 168(s1)<br>    |
| 815|[0x80013d84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x26d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a264]:fmul.h t6, t5, t4, dyn<br> [0x8000a268]:csrrs a3, fcsr, zero<br> [0x8000a26c]:sw t6, 176(s1)<br>    |
| 816|[0x80013d8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a2a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a2a8]:csrrs a3, fcsr, zero<br> [0x8000a2ac]:sw t6, 184(s1)<br>    |
| 817|[0x80013d94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a2e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a2e8]:csrrs a3, fcsr, zero<br> [0x8000a2ec]:sw t6, 192(s1)<br>    |
| 818|[0x80013d9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a324]:fmul.h t6, t5, t4, dyn<br> [0x8000a328]:csrrs a3, fcsr, zero<br> [0x8000a32c]:sw t6, 200(s1)<br>    |
| 819|[0x80013da4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a364]:fmul.h t6, t5, t4, dyn<br> [0x8000a368]:csrrs a3, fcsr, zero<br> [0x8000a36c]:sw t6, 208(s1)<br>    |
| 820|[0x80013dac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x222 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a3a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a3a8]:csrrs a3, fcsr, zero<br> [0x8000a3ac]:sw t6, 216(s1)<br>    |
| 821|[0x80013db4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a3e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a3e8]:csrrs a3, fcsr, zero<br> [0x8000a3ec]:sw t6, 224(s1)<br>    |
| 822|[0x80013dbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a424]:fmul.h t6, t5, t4, dyn<br> [0x8000a428]:csrrs a3, fcsr, zero<br> [0x8000a42c]:sw t6, 232(s1)<br>    |
| 823|[0x80013dc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a464]:fmul.h t6, t5, t4, dyn<br> [0x8000a468]:csrrs a3, fcsr, zero<br> [0x8000a46c]:sw t6, 240(s1)<br>    |
| 824|[0x80013dcc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a4a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a4a8]:csrrs a3, fcsr, zero<br> [0x8000a4ac]:sw t6, 248(s1)<br>    |
| 825|[0x80013dd4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a4e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a4e8]:csrrs a3, fcsr, zero<br> [0x8000a4ec]:sw t6, 256(s1)<br>    |
| 826|[0x80013ddc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a524]:fmul.h t6, t5, t4, dyn<br> [0x8000a528]:csrrs a3, fcsr, zero<br> [0x8000a52c]:sw t6, 264(s1)<br>    |
| 827|[0x80013de4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a564]:fmul.h t6, t5, t4, dyn<br> [0x8000a568]:csrrs a3, fcsr, zero<br> [0x8000a56c]:sw t6, 272(s1)<br>    |
| 828|[0x80013dec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a5a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a5a8]:csrrs a3, fcsr, zero<br> [0x8000a5ac]:sw t6, 280(s1)<br>    |
| 829|[0x80013df4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a5e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a5e8]:csrrs a3, fcsr, zero<br> [0x8000a5ec]:sw t6, 288(s1)<br>    |
| 830|[0x80013dfc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x220 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a624]:fmul.h t6, t5, t4, dyn<br> [0x8000a628]:csrrs a3, fcsr, zero<br> [0x8000a62c]:sw t6, 296(s1)<br>    |
| 831|[0x80013e04]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a664]:fmul.h t6, t5, t4, dyn<br> [0x8000a668]:csrrs a3, fcsr, zero<br> [0x8000a66c]:sw t6, 304(s1)<br>    |
| 832|[0x80013e0c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a6a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a6a8]:csrrs a3, fcsr, zero<br> [0x8000a6ac]:sw t6, 312(s1)<br>    |
| 833|[0x80013e14]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a6e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a6e8]:csrrs a3, fcsr, zero<br> [0x8000a6ec]:sw t6, 320(s1)<br>    |
| 834|[0x80013e1c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a724]:fmul.h t6, t5, t4, dyn<br> [0x8000a728]:csrrs a3, fcsr, zero<br> [0x8000a72c]:sw t6, 328(s1)<br>    |
| 835|[0x80013e24]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x069 and fs2 == 0 and fe2 == 0x07 and fm2 == 0x341 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a764]:fmul.h t6, t5, t4, dyn<br> [0x8000a768]:csrrs a3, fcsr, zero<br> [0x8000a76c]:sw t6, 336(s1)<br>    |
| 836|[0x80013e2c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a7a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a7a8]:csrrs a3, fcsr, zero<br> [0x8000a7ac]:sw t6, 344(s1)<br>    |
| 837|[0x80013e34]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a7e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a7e8]:csrrs a3, fcsr, zero<br> [0x8000a7ec]:sw t6, 352(s1)<br>    |
| 838|[0x80013e3c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a824]:fmul.h t6, t5, t4, dyn<br> [0x8000a828]:csrrs a3, fcsr, zero<br> [0x8000a82c]:sw t6, 360(s1)<br>    |
| 839|[0x80013e44]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a864]:fmul.h t6, t5, t4, dyn<br> [0x8000a868]:csrrs a3, fcsr, zero<br> [0x8000a86c]:sw t6, 368(s1)<br>    |
| 840|[0x80013e4c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2f2 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x09b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a8a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a8a8]:csrrs a3, fcsr, zero<br> [0x8000a8ac]:sw t6, 376(s1)<br>    |
| 841|[0x80013e54]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000a8e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a8e8]:csrrs a3, fcsr, zero<br> [0x8000a8ec]:sw t6, 384(s1)<br>    |
| 842|[0x80013e5c]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a924]:fmul.h t6, t5, t4, dyn<br> [0x8000a928]:csrrs a3, fcsr, zero<br> [0x8000a92c]:sw t6, 392(s1)<br>    |
| 843|[0x80013e64]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a964]:fmul.h t6, t5, t4, dyn<br> [0x8000a968]:csrrs a3, fcsr, zero<br> [0x8000a96c]:sw t6, 400(s1)<br>    |
| 844|[0x80013e6c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a9a4]:fmul.h t6, t5, t4, dyn<br> [0x8000a9a8]:csrrs a3, fcsr, zero<br> [0x8000a9ac]:sw t6, 408(s1)<br>    |
| 845|[0x80013e74]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x086 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x311 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000a9e4]:fmul.h t6, t5, t4, dyn<br> [0x8000a9e8]:csrrs a3, fcsr, zero<br> [0x8000a9ec]:sw t6, 416(s1)<br>    |
| 846|[0x80013e7c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000aa24]:fmul.h t6, t5, t4, dyn<br> [0x8000aa28]:csrrs a3, fcsr, zero<br> [0x8000aa2c]:sw t6, 424(s1)<br>    |
| 847|[0x80013e84]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aa64]:fmul.h t6, t5, t4, dyn<br> [0x8000aa68]:csrrs a3, fcsr, zero<br> [0x8000aa6c]:sw t6, 432(s1)<br>    |
| 848|[0x80013e8c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aaa4]:fmul.h t6, t5, t4, dyn<br> [0x8000aaa8]:csrrs a3, fcsr, zero<br> [0x8000aaac]:sw t6, 440(s1)<br>    |
| 849|[0x80013e94]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aae4]:fmul.h t6, t5, t4, dyn<br> [0x8000aae8]:csrrs a3, fcsr, zero<br> [0x8000aaec]:sw t6, 448(s1)<br>    |
| 850|[0x80013e9c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x28e and fs2 == 0 and fe2 == 0x07 and fm2 == 0x0e1 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ab24]:fmul.h t6, t5, t4, dyn<br> [0x8000ab28]:csrrs a3, fcsr, zero<br> [0x8000ab2c]:sw t6, 456(s1)<br>    |
| 851|[0x80013ea4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000ab64]:fmul.h t6, t5, t4, dyn<br> [0x8000ab68]:csrrs a3, fcsr, zero<br> [0x8000ab6c]:sw t6, 464(s1)<br>    |
| 852|[0x80013eac]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aba4]:fmul.h t6, t5, t4, dyn<br> [0x8000aba8]:csrrs a3, fcsr, zero<br> [0x8000abac]:sw t6, 472(s1)<br>    |
| 853|[0x80013eb4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000abe4]:fmul.h t6, t5, t4, dyn<br> [0x8000abe8]:csrrs a3, fcsr, zero<br> [0x8000abec]:sw t6, 480(s1)<br>    |
| 854|[0x80013ebc]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ac24]:fmul.h t6, t5, t4, dyn<br> [0x8000ac28]:csrrs a3, fcsr, zero<br> [0x8000ac2c]:sw t6, 488(s1)<br>    |
| 855|[0x80013ec4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x0a8 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x2de and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ac64]:fmul.h t6, t5, t4, dyn<br> [0x8000ac68]:csrrs a3, fcsr, zero<br> [0x8000ac6c]:sw t6, 496(s1)<br>    |
| 856|[0x80013ecc]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000aca4]:fmul.h t6, t5, t4, dyn<br> [0x8000aca8]:csrrs a3, fcsr, zero<br> [0x8000acac]:sw t6, 504(s1)<br>    |
| 857|[0x80013ed4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ace4]:fmul.h t6, t5, t4, dyn<br> [0x8000ace8]:csrrs a3, fcsr, zero<br> [0x8000acec]:sw t6, 512(s1)<br>    |
| 858|[0x80013edc]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ad24]:fmul.h t6, t5, t4, dyn<br> [0x8000ad28]:csrrs a3, fcsr, zero<br> [0x8000ad2c]:sw t6, 520(s1)<br>    |
| 859|[0x80013ee4]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ad64]:fmul.h t6, t5, t4, dyn<br> [0x8000ad68]:csrrs a3, fcsr, zero<br> [0x8000ad6c]:sw t6, 528(s1)<br>    |
| 860|[0x80013eec]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c3 and fs2 == 0 and fe2 == 0x06 and fm2 == 0x01f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ada4]:fmul.h t6, t5, t4, dyn<br> [0x8000ada8]:csrrs a3, fcsr, zero<br> [0x8000adac]:sw t6, 536(s1)<br>    |
| 861|[0x80013ef4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000ade4]:fmul.h t6, t5, t4, dyn<br> [0x8000ade8]:csrrs a3, fcsr, zero<br> [0x8000adec]:sw t6, 544(s1)<br>    |
| 862|[0x80013efc]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ae24]:fmul.h t6, t5, t4, dyn<br> [0x8000ae28]:csrrs a3, fcsr, zero<br> [0x8000ae2c]:sw t6, 552(s1)<br>    |
| 863|[0x80013f04]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ae64]:fmul.h t6, t5, t4, dyn<br> [0x8000ae68]:csrrs a3, fcsr, zero<br> [0x8000ae6c]:sw t6, 560(s1)<br>    |
| 864|[0x80013f0c]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aea4]:fmul.h t6, t5, t4, dyn<br> [0x8000aea8]:csrrs a3, fcsr, zero<br> [0x8000aeac]:sw t6, 568(s1)<br>    |
| 865|[0x80013f14]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x143 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x214 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000aee4]:fmul.h t6, t5, t4, dyn<br> [0x8000aee8]:csrrs a3, fcsr, zero<br> [0x8000aeec]:sw t6, 576(s1)<br>    |
| 866|[0x80013f1c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000af24]:fmul.h t6, t5, t4, dyn<br> [0x8000af28]:csrrs a3, fcsr, zero<br> [0x8000af2c]:sw t6, 584(s1)<br>    |
| 867|[0x80013f24]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000af64]:fmul.h t6, t5, t4, dyn<br> [0x8000af68]:csrrs a3, fcsr, zero<br> [0x8000af6c]:sw t6, 592(s1)<br>    |
| 868|[0x80013f2c]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000afa4]:fmul.h t6, t5, t4, dyn<br> [0x8000afa8]:csrrs a3, fcsr, zero<br> [0x8000afac]:sw t6, 600(s1)<br>    |
| 869|[0x80013f34]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000afe4]:fmul.h t6, t5, t4, dyn<br> [0x8000afe8]:csrrs a3, fcsr, zero<br> [0x8000afec]:sw t6, 608(s1)<br>    |
| 870|[0x80013f3c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3c1 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x020 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b024]:fmul.h t6, t5, t4, dyn<br> [0x8000b028]:csrrs a3, fcsr, zero<br> [0x8000b02c]:sw t6, 616(s1)<br>    |
| 871|[0x80013f44]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b064]:fmul.h t6, t5, t4, dyn<br> [0x8000b068]:csrrs a3, fcsr, zero<br> [0x8000b06c]:sw t6, 624(s1)<br>    |
| 872|[0x80013f4c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b0a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b0a8]:csrrs a3, fcsr, zero<br> [0x8000b0ac]:sw t6, 632(s1)<br>    |
| 873|[0x80013f54]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b0e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b0e8]:csrrs a3, fcsr, zero<br> [0x8000b0ec]:sw t6, 640(s1)<br>    |
| 874|[0x80013f5c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b124]:fmul.h t6, t5, t4, dyn<br> [0x8000b128]:csrrs a3, fcsr, zero<br> [0x8000b12c]:sw t6, 648(s1)<br>    |
| 875|[0x80013f64]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x15b and fs2 == 1 and fe2 == 0x06 and fm2 == 0x1f9 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b164]:fmul.h t6, t5, t4, dyn<br> [0x8000b168]:csrrs a3, fcsr, zero<br> [0x8000b16c]:sw t6, 656(s1)<br>    |
| 876|[0x80013f6c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b1a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b1a8]:csrrs a3, fcsr, zero<br> [0x8000b1ac]:sw t6, 664(s1)<br>    |
| 877|[0x80013f74]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b1e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b1e8]:csrrs a3, fcsr, zero<br> [0x8000b1ec]:sw t6, 672(s1)<br>    |
| 878|[0x80013f7c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b224]:fmul.h t6, t5, t4, dyn<br> [0x8000b228]:csrrs a3, fcsr, zero<br> [0x8000b22c]:sw t6, 680(s1)<br>    |
| 879|[0x80013f84]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b264]:fmul.h t6, t5, t4, dyn<br> [0x8000b268]:csrrs a3, fcsr, zero<br> [0x8000b26c]:sw t6, 688(s1)<br>    |
| 880|[0x80013f8c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x005 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x3f5 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b2a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b2a8]:csrrs a3, fcsr, zero<br> [0x8000b2ac]:sw t6, 696(s1)<br>    |
| 881|[0x80013f94]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b2e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b2e8]:csrrs a3, fcsr, zero<br> [0x8000b2ec]:sw t6, 704(s1)<br>    |
| 882|[0x80013f9c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b324]:fmul.h t6, t5, t4, dyn<br> [0x8000b328]:csrrs a3, fcsr, zero<br> [0x8000b32c]:sw t6, 712(s1)<br>    |
| 883|[0x80013fa4]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b364]:fmul.h t6, t5, t4, dyn<br> [0x8000b368]:csrrs a3, fcsr, zero<br> [0x8000b36c]:sw t6, 720(s1)<br>    |
| 884|[0x80013fac]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b3a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b3a8]:csrrs a3, fcsr, zero<br> [0x8000b3ac]:sw t6, 728(s1)<br>    |
| 885|[0x80013fb4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x11b and fs2 == 1 and fe2 == 0x08 and fm2 == 0x244 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b3e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b3e8]:csrrs a3, fcsr, zero<br> [0x8000b3ec]:sw t6, 736(s1)<br>    |
| 886|[0x80013fbc]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b424]:fmul.h t6, t5, t4, dyn<br> [0x8000b428]:csrrs a3, fcsr, zero<br> [0x8000b42c]:sw t6, 744(s1)<br>    |
| 887|[0x80013fc4]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b464]:fmul.h t6, t5, t4, dyn<br> [0x8000b468]:csrrs a3, fcsr, zero<br> [0x8000b46c]:sw t6, 752(s1)<br>    |
| 888|[0x80013fcc]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b4a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b4a8]:csrrs a3, fcsr, zero<br> [0x8000b4ac]:sw t6, 760(s1)<br>    |
| 889|[0x80013fd4]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b4e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b4e8]:csrrs a3, fcsr, zero<br> [0x8000b4ec]:sw t6, 768(s1)<br>    |
| 890|[0x80013fdc]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x18e and fs2 == 1 and fe2 == 0x07 and fm2 == 0x1c2 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b524]:fmul.h t6, t5, t4, dyn<br> [0x8000b528]:csrrs a3, fcsr, zero<br> [0x8000b52c]:sw t6, 776(s1)<br>    |
| 891|[0x80013fe4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b564]:fmul.h t6, t5, t4, dyn<br> [0x8000b568]:csrrs a3, fcsr, zero<br> [0x8000b56c]:sw t6, 784(s1)<br>    |
| 892|[0x80013fec]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b5a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b5a8]:csrrs a3, fcsr, zero<br> [0x8000b5ac]:sw t6, 792(s1)<br>    |
| 893|[0x80013ff4]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b5e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b5e8]:csrrs a3, fcsr, zero<br> [0x8000b5ec]:sw t6, 800(s1)<br>    |
| 894|[0x80013ffc]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b624]:fmul.h t6, t5, t4, dyn<br> [0x8000b628]:csrrs a3, fcsr, zero<br> [0x8000b62c]:sw t6, 808(s1)<br>    |
| 895|[0x80014004]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x245 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x11a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b664]:fmul.h t6, t5, t4, dyn<br> [0x8000b668]:csrrs a3, fcsr, zero<br> [0x8000b66c]:sw t6, 816(s1)<br>    |
| 896|[0x8001400c]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b6a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b6a8]:csrrs a3, fcsr, zero<br> [0x8000b6ac]:sw t6, 824(s1)<br>    |
| 897|[0x80014014]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b6e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b6e8]:csrrs a3, fcsr, zero<br> [0x8000b6ec]:sw t6, 832(s1)<br>    |
| 898|[0x8001401c]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b724]:fmul.h t6, t5, t4, dyn<br> [0x8000b728]:csrrs a3, fcsr, zero<br> [0x8000b72c]:sw t6, 840(s1)<br>    |
| 899|[0x80014024]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b764]:fmul.h t6, t5, t4, dyn<br> [0x8000b768]:csrrs a3, fcsr, zero<br> [0x8000b76c]:sw t6, 848(s1)<br>    |
| 900|[0x8001402c]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x165 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x1ed and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b7a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b7a8]:csrrs a3, fcsr, zero<br> [0x8000b7ac]:sw t6, 856(s1)<br>    |
| 901|[0x80014034]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b7e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b7e8]:csrrs a3, fcsr, zero<br> [0x8000b7ec]:sw t6, 864(s1)<br>    |
| 902|[0x8001403c]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b824]:fmul.h t6, t5, t4, dyn<br> [0x8000b828]:csrrs a3, fcsr, zero<br> [0x8000b82c]:sw t6, 872(s1)<br>    |
| 903|[0x80014044]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b864]:fmul.h t6, t5, t4, dyn<br> [0x8000b868]:csrrs a3, fcsr, zero<br> [0x8000b86c]:sw t6, 880(s1)<br>    |
| 904|[0x8001404c]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b8a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b8a8]:csrrs a3, fcsr, zero<br> [0x8000b8ac]:sw t6, 888(s1)<br>    |
| 905|[0x80014054]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2e8 and fs2 == 1 and fe2 == 0x05 and fm2 == 0x0a1 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b8e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b8e8]:csrrs a3, fcsr, zero<br> [0x8000b8ec]:sw t6, 896(s1)<br>    |
| 906|[0x8001405c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000b924]:fmul.h t6, t5, t4, dyn<br> [0x8000b928]:csrrs a3, fcsr, zero<br> [0x8000b92c]:sw t6, 904(s1)<br>    |
| 907|[0x80014064]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b964]:fmul.h t6, t5, t4, dyn<br> [0x8000b968]:csrrs a3, fcsr, zero<br> [0x8000b96c]:sw t6, 912(s1)<br>    |
| 908|[0x8001406c]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b9a4]:fmul.h t6, t5, t4, dyn<br> [0x8000b9a8]:csrrs a3, fcsr, zero<br> [0x8000b9ac]:sw t6, 920(s1)<br>    |
| 909|[0x80014074]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000b9e4]:fmul.h t6, t5, t4, dyn<br> [0x8000b9e8]:csrrs a3, fcsr, zero<br> [0x8000b9ec]:sw t6, 928(s1)<br>    |
| 910|[0x8001407c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x133 and fs2 == 1 and fe2 == 0x07 and fm2 == 0x227 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ba24]:fmul.h t6, t5, t4, dyn<br> [0x8000ba28]:csrrs a3, fcsr, zero<br> [0x8000ba2c]:sw t6, 936(s1)<br>    |
| 911|[0x80014084]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000ba64]:fmul.h t6, t5, t4, dyn<br> [0x8000ba68]:csrrs a3, fcsr, zero<br> [0x8000ba6c]:sw t6, 944(s1)<br>    |
| 912|[0x8001408c]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000baa4]:fmul.h t6, t5, t4, dyn<br> [0x8000baa8]:csrrs a3, fcsr, zero<br> [0x8000baac]:sw t6, 952(s1)<br>    |
| 913|[0x80014094]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bae4]:fmul.h t6, t5, t4, dyn<br> [0x8000bae8]:csrrs a3, fcsr, zero<br> [0x8000baec]:sw t6, 960(s1)<br>    |
| 914|[0x8001409c]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bb24]:fmul.h t6, t5, t4, dyn<br> [0x8000bb28]:csrrs a3, fcsr, zero<br> [0x8000bb2c]:sw t6, 968(s1)<br>    |
| 915|[0x800140a4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x36e and fs2 == 1 and fe2 == 0x06 and fm2 == 0x04e and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bb64]:fmul.h t6, t5, t4, dyn<br> [0x8000bb68]:csrrs a3, fcsr, zero<br> [0x8000bb6c]:sw t6, 976(s1)<br>    |
| 916|[0x800140ac]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000bba4]:fmul.h t6, t5, t4, dyn<br> [0x8000bba8]:csrrs a3, fcsr, zero<br> [0x8000bbac]:sw t6, 984(s1)<br>    |
| 917|[0x800140b4]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bbe4]:fmul.h t6, t5, t4, dyn<br> [0x8000bbe8]:csrrs a3, fcsr, zero<br> [0x8000bbec]:sw t6, 992(s1)<br>    |
| 918|[0x800140bc]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bc24]:fmul.h t6, t5, t4, dyn<br> [0x8000bc28]:csrrs a3, fcsr, zero<br> [0x8000bc2c]:sw t6, 1000(s1)<br>   |
| 919|[0x800140c4]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bc64]:fmul.h t6, t5, t4, dyn<br> [0x8000bc68]:csrrs a3, fcsr, zero<br> [0x8000bc6c]:sw t6, 1008(s1)<br>   |
| 920|[0x800140cc]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x18 and fm1 == 0x154 and fs2 == 1 and fe2 == 0x0b and fm2 == 0x200 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bca4]:fmul.h t6, t5, t4, dyn<br> [0x8000bca8]:csrrs a3, fcsr, zero<br> [0x8000bcac]:sw t6, 1016(s1)<br>   |
| 921|[0x800140d4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000bcec]:fmul.h t6, t5, t4, dyn<br> [0x8000bcf0]:csrrs a3, fcsr, zero<br> [0x8000bcf4]:sw t6, 0(s1)<br>      |
| 922|[0x800140dc]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bd2c]:fmul.h t6, t5, t4, dyn<br> [0x8000bd30]:csrrs a3, fcsr, zero<br> [0x8000bd34]:sw t6, 8(s1)<br>      |
| 923|[0x800140e4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bd6c]:fmul.h t6, t5, t4, dyn<br> [0x8000bd70]:csrrs a3, fcsr, zero<br> [0x8000bd74]:sw t6, 16(s1)<br>     |
| 924|[0x800140ec]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bdac]:fmul.h t6, t5, t4, dyn<br> [0x8000bdb0]:csrrs a3, fcsr, zero<br> [0x8000bdb4]:sw t6, 24(s1)<br>     |
| 925|[0x800140f4]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3bb and fs2 == 1 and fe2 == 0x05 and fm2 == 0x023 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bdec]:fmul.h t6, t5, t4, dyn<br> [0x8000bdf0]:csrrs a3, fcsr, zero<br> [0x8000bdf4]:sw t6, 32(s1)<br>     |
| 926|[0x800140fc]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000be2c]:fmul.h t6, t5, t4, dyn<br> [0x8000be30]:csrrs a3, fcsr, zero<br> [0x8000be34]:sw t6, 40(s1)<br>     |
| 927|[0x80014104]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000be6c]:fmul.h t6, t5, t4, dyn<br> [0x8000be70]:csrrs a3, fcsr, zero<br> [0x8000be74]:sw t6, 48(s1)<br>     |
| 928|[0x8001410c]<br>0xFFFFD400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000beac]:fmul.h t6, t5, t4, dyn<br> [0x8000beb0]:csrrs a3, fcsr, zero<br> [0x8000beb4]:sw t6, 56(s1)<br>     |
| 929|[0x80014114]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000beec]:fmul.h t6, t5, t4, dyn<br> [0x8000bef0]:csrrs a3, fcsr, zero<br> [0x8000bef4]:sw t6, 64(s1)<br>     |
| 930|[0x8001411c]<br>0xFFFFD3FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x04d and fs2 == 1 and fe2 == 0x05 and fm2 == 0x370 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bf2c]:fmul.h t6, t5, t4, dyn<br> [0x8000bf30]:csrrs a3, fcsr, zero<br> [0x8000bf34]:sw t6, 72(s1)<br>     |
| 931|[0x80014124]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000bf6c]:fmul.h t6, t5, t4, dyn<br> [0x8000bf70]:csrrs a3, fcsr, zero<br> [0x8000bf74]:sw t6, 80(s1)<br>     |
| 932|[0x8001412c]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bfac]:fmul.h t6, t5, t4, dyn<br> [0x8000bfb0]:csrrs a3, fcsr, zero<br> [0x8000bfb4]:sw t6, 88(s1)<br>     |
| 933|[0x80014134]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000bfec]:fmul.h t6, t5, t4, dyn<br> [0x8000bff0]:csrrs a3, fcsr, zero<br> [0x8000bff4]:sw t6, 96(s1)<br>     |
| 934|[0x8001413c]<br>0xFFFFD3FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c02c]:fmul.h t6, t5, t4, dyn<br> [0x8000c030]:csrrs a3, fcsr, zero<br> [0x8000c034]:sw t6, 104(s1)<br>    |
| 935|[0x80014144]<br>0xFFFFD3FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x314 and fs2 == 1 and fe2 == 0x06 and fm2 == 0x084 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c06c]:fmul.h t6, t5, t4, dyn<br> [0x8000c070]:csrrs a3, fcsr, zero<br> [0x8000c074]:sw t6, 112(s1)<br>    |
| 936|[0x8001414c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c0ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c0b0]:csrrs a3, fcsr, zero<br> [0x8000c0b4]:sw t6, 120(s1)<br>    |
| 937|[0x80014154]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c0ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c0f0]:csrrs a3, fcsr, zero<br> [0x8000c0f4]:sw t6, 128(s1)<br>    |
| 938|[0x8001415c]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c12c]:fmul.h t6, t5, t4, dyn<br> [0x8000c130]:csrrs a3, fcsr, zero<br> [0x8000c134]:sw t6, 136(s1)<br>    |
| 939|[0x80014164]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c16c]:fmul.h t6, t5, t4, dyn<br> [0x8000c170]:csrrs a3, fcsr, zero<br> [0x8000c174]:sw t6, 144(s1)<br>    |
| 940|[0x8001416c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ee and fs2 == 0 and fe2 == 0x06 and fm2 == 0x27c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c1ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c1b0]:csrrs a3, fcsr, zero<br> [0x8000c1b4]:sw t6, 152(s1)<br>    |
| 941|[0x80014174]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c1ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c1f0]:csrrs a3, fcsr, zero<br> [0x8000c1f4]:sw t6, 160(s1)<br>    |
| 942|[0x8001417c]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c22c]:fmul.h t6, t5, t4, dyn<br> [0x8000c230]:csrrs a3, fcsr, zero<br> [0x8000c234]:sw t6, 168(s1)<br>    |
| 943|[0x80014184]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c26c]:fmul.h t6, t5, t4, dyn<br> [0x8000c270]:csrrs a3, fcsr, zero<br> [0x8000c274]:sw t6, 176(s1)<br>    |
| 944|[0x8001418c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c2ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c2b0]:csrrs a3, fcsr, zero<br> [0x8000c2b4]:sw t6, 184(s1)<br>    |
| 945|[0x80014194]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x3bc and fs2 == 0 and fe2 == 0x06 and fm2 == 0x022 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c2ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c2f0]:csrrs a3, fcsr, zero<br> [0x8000c2f4]:sw t6, 192(s1)<br>    |
| 946|[0x8001419c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c32c]:fmul.h t6, t5, t4, dyn<br> [0x8000c330]:csrrs a3, fcsr, zero<br> [0x8000c334]:sw t6, 200(s1)<br>    |
| 947|[0x800141a4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c36c]:fmul.h t6, t5, t4, dyn<br> [0x8000c370]:csrrs a3, fcsr, zero<br> [0x8000c374]:sw t6, 208(s1)<br>    |
| 948|[0x800141ac]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c3ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c3b0]:csrrs a3, fcsr, zero<br> [0x8000c3b4]:sw t6, 216(s1)<br>    |
| 949|[0x800141b4]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c3ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c3f0]:csrrs a3, fcsr, zero<br> [0x8000c3f4]:sw t6, 224(s1)<br>    |
| 950|[0x800141bc]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x350 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x060 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c42c]:fmul.h t6, t5, t4, dyn<br> [0x8000c430]:csrrs a3, fcsr, zero<br> [0x8000c434]:sw t6, 232(s1)<br>    |
| 951|[0x800141c4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c46c]:fmul.h t6, t5, t4, dyn<br> [0x8000c470]:csrrs a3, fcsr, zero<br> [0x8000c474]:sw t6, 240(s1)<br>    |
| 952|[0x800141cc]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c4ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c4b0]:csrrs a3, fcsr, zero<br> [0x8000c4b4]:sw t6, 248(s1)<br>    |
| 953|[0x800141d4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c4ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c4f0]:csrrs a3, fcsr, zero<br> [0x8000c4f4]:sw t6, 256(s1)<br>    |
| 954|[0x800141dc]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c52c]:fmul.h t6, t5, t4, dyn<br> [0x8000c530]:csrrs a3, fcsr, zero<br> [0x8000c534]:sw t6, 264(s1)<br>    |
| 955|[0x800141e4]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x297 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0da and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c56c]:fmul.h t6, t5, t4, dyn<br> [0x8000c570]:csrrs a3, fcsr, zero<br> [0x8000c574]:sw t6, 272(s1)<br>    |
| 956|[0x800141ec]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c5ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c5b0]:csrrs a3, fcsr, zero<br> [0x8000c5b4]:sw t6, 280(s1)<br>    |
| 957|[0x800141f4]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c5ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c5f0]:csrrs a3, fcsr, zero<br> [0x8000c5f4]:sw t6, 288(s1)<br>    |
| 958|[0x800141fc]<br>0x000053FF<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c62c]:fmul.h t6, t5, t4, dyn<br> [0x8000c630]:csrrs a3, fcsr, zero<br> [0x8000c634]:sw t6, 296(s1)<br>    |
| 959|[0x80014204]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c66c]:fmul.h t6, t5, t4, dyn<br> [0x8000c670]:csrrs a3, fcsr, zero<br> [0x8000c674]:sw t6, 304(s1)<br>    |
| 960|[0x8001420c]<br>0x00005400<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x12b and fs2 == 0 and fe2 == 0x05 and fm2 == 0x231 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c6ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c6b0]:csrrs a3, fcsr, zero<br> [0x8000c6b4]:sw t6, 312(s1)<br>    |
| 961|[0x80014214]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c6ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c6f0]:csrrs a3, fcsr, zero<br> [0x8000c6f4]:sw t6, 320(s1)<br>    |
| 962|[0x8001421c]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c72c]:fmul.h t6, t5, t4, dyn<br> [0x8000c730]:csrrs a3, fcsr, zero<br> [0x8000c734]:sw t6, 328(s1)<br>    |
| 963|[0x80014224]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c76c]:fmul.h t6, t5, t4, dyn<br> [0x8000c770]:csrrs a3, fcsr, zero<br> [0x8000c774]:sw t6, 336(s1)<br>    |
| 964|[0x8001422c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c7ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c7b0]:csrrs a3, fcsr, zero<br> [0x8000c7b4]:sw t6, 344(s1)<br>    |
| 965|[0x80014234]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x139 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x21f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c7ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c7f0]:csrrs a3, fcsr, zero<br> [0x8000c7f4]:sw t6, 352(s1)<br>    |
| 966|[0x8001423c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c82c]:fmul.h t6, t5, t4, dyn<br> [0x8000c830]:csrrs a3, fcsr, zero<br> [0x8000c834]:sw t6, 360(s1)<br>    |
| 967|[0x80014244]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c86c]:fmul.h t6, t5, t4, dyn<br> [0x8000c870]:csrrs a3, fcsr, zero<br> [0x8000c874]:sw t6, 368(s1)<br>    |
| 968|[0x8001424c]<br>0x000053FD<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c8ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c8b0]:csrrs a3, fcsr, zero<br> [0x8000c8b4]:sw t6, 376(s1)<br>    |
| 969|[0x80014254]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c8ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c8f0]:csrrs a3, fcsr, zero<br> [0x8000c8f4]:sw t6, 384(s1)<br>    |
| 970|[0x8001425c]<br>0x000053FE<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2b8 and fs2 == 0 and fe2 == 0x05 and fm2 == 0x0c2 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c92c]:fmul.h t6, t5, t4, dyn<br> [0x8000c930]:csrrs a3, fcsr, zero<br> [0x8000c934]:sw t6, 392(s1)<br>    |
| 971|[0x80014264]<br>0x00002798<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000c96c]:fmul.h t6, t5, t4, dyn<br> [0x8000c970]:csrrs a3, fcsr, zero<br> [0x8000c974]:sw t6, 400(s1)<br>    |
| 972|[0x8001426c]<br>0x00002798<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c9ac]:fmul.h t6, t5, t4, dyn<br> [0x8000c9b0]:csrrs a3, fcsr, zero<br> [0x8000c9b4]:sw t6, 408(s1)<br>    |
| 973|[0x80014274]<br>0x00002798<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000c9ec]:fmul.h t6, t5, t4, dyn<br> [0x8000c9f0]:csrrs a3, fcsr, zero<br> [0x8000c9f4]:sw t6, 416(s1)<br>    |
| 974|[0x8001427c]<br>0x00002798<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ca2c]:fmul.h t6, t5, t4, dyn<br> [0x8000ca30]:csrrs a3, fcsr, zero<br> [0x8000ca34]:sw t6, 424(s1)<br>    |
| 975|[0x80014284]<br>0x00002798<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x110 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ca6c]:fmul.h t6, t5, t4, dyn<br> [0x8000ca70]:csrrs a3, fcsr, zero<br> [0x8000ca74]:sw t6, 432(s1)<br>    |
| 976|[0x8001428c]<br>0x000027F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000caac]:fmul.h t6, t5, t4, dyn<br> [0x8000cab0]:csrrs a3, fcsr, zero<br> [0x8000cab4]:sw t6, 440(s1)<br>    |
| 977|[0x80014294]<br>0x000027F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000caec]:fmul.h t6, t5, t4, dyn<br> [0x8000caf0]:csrrs a3, fcsr, zero<br> [0x8000caf4]:sw t6, 448(s1)<br>    |
| 978|[0x8001429c]<br>0x000027F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cb2c]:fmul.h t6, t5, t4, dyn<br> [0x8000cb30]:csrrs a3, fcsr, zero<br> [0x8000cb34]:sw t6, 456(s1)<br>    |
| 979|[0x800142a4]<br>0x000027F3<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cb6c]:fmul.h t6, t5, t4, dyn<br> [0x8000cb70]:csrrs a3, fcsr, zero<br> [0x8000cb74]:sw t6, 464(s1)<br>    |
| 980|[0x800142ac]<br>0x000027F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x03d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00f and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cbac]:fmul.h t6, t5, t4, dyn<br> [0x8000cbb0]:csrrs a3, fcsr, zero<br> [0x8000cbb4]:sw t6, 472(s1)<br>    |
| 981|[0x800142b4]<br>0x000027F9<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000cbec]:fmul.h t6, t5, t4, dyn<br> [0x8000cbf0]:csrrs a3, fcsr, zero<br> [0x8000cbf4]:sw t6, 480(s1)<br>    |
| 982|[0x800142bc]<br>0x000027F9<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cc2c]:fmul.h t6, t5, t4, dyn<br> [0x8000cc30]:csrrs a3, fcsr, zero<br> [0x8000cc34]:sw t6, 488(s1)<br>    |
| 983|[0x800142c4]<br>0x000027F9<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cc6c]:fmul.h t6, t5, t4, dyn<br> [0x8000cc70]:csrrs a3, fcsr, zero<br> [0x8000cc74]:sw t6, 496(s1)<br>    |
| 984|[0x800142cc]<br>0x000027FA<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ccac]:fmul.h t6, t5, t4, dyn<br> [0x8000ccb0]:csrrs a3, fcsr, zero<br> [0x8000ccb4]:sw t6, 504(s1)<br>    |
| 985|[0x800142d4]<br>0x000027F9<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x261 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ccec]:fmul.h t6, t5, t4, dyn<br> [0x8000ccf0]:csrrs a3, fcsr, zero<br> [0x8000ccf4]:sw t6, 512(s1)<br>    |
| 986|[0x800142dc]<br>0x000027B2<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000cd2c]:fmul.h t6, t5, t4, dyn<br> [0x8000cd30]:csrrs a3, fcsr, zero<br> [0x8000cd34]:sw t6, 520(s1)<br>    |
| 987|[0x800142e4]<br>0x000027B2<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cd6c]:fmul.h t6, t5, t4, dyn<br> [0x8000cd70]:csrrs a3, fcsr, zero<br> [0x8000cd74]:sw t6, 528(s1)<br>    |
| 988|[0x800142ec]<br>0x000027B2<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cdac]:fmul.h t6, t5, t4, dyn<br> [0x8000cdb0]:csrrs a3, fcsr, zero<br> [0x8000cdb4]:sw t6, 536(s1)<br>    |
| 989|[0x800142f4]<br>0x000027B3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cdec]:fmul.h t6, t5, t4, dyn<br> [0x8000cdf0]:csrrs a3, fcsr, zero<br> [0x8000cdf4]:sw t6, 544(s1)<br>    |
| 990|[0x800142fc]<br>0x000027B2<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0ed and fs2 == 0 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ce2c]:fmul.h t6, t5, t4, dyn<br> [0x8000ce30]:csrrs a3, fcsr, zero<br> [0x8000ce34]:sw t6, 552(s1)<br>    |
| 991|[0x80014304]<br>0x000027C6<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000ce6c]:fmul.h t6, t5, t4, dyn<br> [0x8000ce70]:csrrs a3, fcsr, zero<br> [0x8000ce74]:sw t6, 560(s1)<br>    |
| 992|[0x8001430c]<br>0x000027C6<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ceac]:fmul.h t6, t5, t4, dyn<br> [0x8000ceb0]:csrrs a3, fcsr, zero<br> [0x8000ceb4]:sw t6, 568(s1)<br>    |
| 993|[0x80014314]<br>0x000027C6<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ceec]:fmul.h t6, t5, t4, dyn<br> [0x8000cef0]:csrrs a3, fcsr, zero<br> [0x8000cef4]:sw t6, 576(s1)<br>    |
| 994|[0x8001431c]<br>0x000027C7<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cf2c]:fmul.h t6, t5, t4, dyn<br> [0x8000cf30]:csrrs a3, fcsr, zero<br> [0x8000cf34]:sw t6, 584(s1)<br>    |
| 995|[0x80014324]<br>0x000027C6<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x351 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cf6c]:fmul.h t6, t5, t4, dyn<br> [0x8000cf70]:csrrs a3, fcsr, zero<br> [0x8000cf74]:sw t6, 592(s1)<br>    |
| 996|[0x8001432c]<br>0x000027C4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000cfac]:fmul.h t6, t5, t4, dyn<br> [0x8000cfb0]:csrrs a3, fcsr, zero<br> [0x8000cfb4]:sw t6, 600(s1)<br>    |
| 997|[0x80014334]<br>0x000027C4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000cfec]:fmul.h t6, t5, t4, dyn<br> [0x8000cff0]:csrrs a3, fcsr, zero<br> [0x8000cff4]:sw t6, 608(s1)<br>    |
| 998|[0x8001433c]<br>0x000027C4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d02c]:fmul.h t6, t5, t4, dyn<br> [0x8000d030]:csrrs a3, fcsr, zero<br> [0x8000d034]:sw t6, 616(s1)<br>    |
| 999|[0x80014344]<br>0x000027C4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d06c]:fmul.h t6, t5, t4, dyn<br> [0x8000d070]:csrrs a3, fcsr, zero<br> [0x8000d074]:sw t6, 624(s1)<br>    |
|1000|[0x8001434c]<br>0x000027C4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x070 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01c and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d0ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d0b0]:csrrs a3, fcsr, zero<br> [0x8000d0b4]:sw t6, 632(s1)<br>    |
|1001|[0x80014354]<br>0x000027F1<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d0ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d0f0]:csrrs a3, fcsr, zero<br> [0x8000d0f4]:sw t6, 640(s1)<br>    |
|1002|[0x8001435c]<br>0x000027F1<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d12c]:fmul.h t6, t5, t4, dyn<br> [0x8000d130]:csrrs a3, fcsr, zero<br> [0x8000d134]:sw t6, 648(s1)<br>    |
|1003|[0x80014364]<br>0x000027F1<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d16c]:fmul.h t6, t5, t4, dyn<br> [0x8000d170]:csrrs a3, fcsr, zero<br> [0x8000d174]:sw t6, 656(s1)<br>    |
|1004|[0x8001436c]<br>0x000027F2<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d1ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d1b0]:csrrs a3, fcsr, zero<br> [0x8000d1b4]:sw t6, 664(s1)<br>    |
|1005|[0x80014374]<br>0x000027F1<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x329 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x047 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d1ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d1f0]:csrrs a3, fcsr, zero<br> [0x8000d1f4]:sw t6, 672(s1)<br>    |
|1006|[0x8001437c]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d22c]:fmul.h t6, t5, t4, dyn<br> [0x8000d230]:csrrs a3, fcsr, zero<br> [0x8000d234]:sw t6, 680(s1)<br>    |
|1007|[0x80014384]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d26c]:fmul.h t6, t5, t4, dyn<br> [0x8000d270]:csrrs a3, fcsr, zero<br> [0x8000d274]:sw t6, 688(s1)<br>    |
|1008|[0x8001438c]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d2ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d2b0]:csrrs a3, fcsr, zero<br> [0x8000d2b4]:sw t6, 696(s1)<br>    |
|1009|[0x80014394]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d2ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d2f0]:csrrs a3, fcsr, zero<br> [0x8000d2f4]:sw t6, 704(s1)<br>    |
|1010|[0x8001439c]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d32c]:fmul.h t6, t5, t4, dyn<br> [0x8000d330]:csrrs a3, fcsr, zero<br> [0x8000d334]:sw t6, 712(s1)<br>    |
|1011|[0x800143a4]<br>0xFFFFA7F4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d36c]:fmul.h t6, t5, t4, dyn<br> [0x8000d370]:csrrs a3, fcsr, zero<br> [0x8000d374]:sw t6, 720(s1)<br>    |
|1012|[0x800143ac]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d3ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d3b0]:csrrs a3, fcsr, zero<br> [0x8000d3b4]:sw t6, 728(s1)<br>    |
|1013|[0x800143b4]<br>0xFFFFA7F4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d3ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d3f0]:csrrs a3, fcsr, zero<br> [0x8000d3f4]:sw t6, 736(s1)<br>    |
|1014|[0x800143bc]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d42c]:fmul.h t6, t5, t4, dyn<br> [0x8000d430]:csrrs a3, fcsr, zero<br> [0x8000d434]:sw t6, 744(s1)<br>    |
|1015|[0x800143c4]<br>0xFFFFA7F4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x117 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x019 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d46c]:fmul.h t6, t5, t4, dyn<br> [0x8000d470]:csrrs a3, fcsr, zero<br> [0x8000d474]:sw t6, 752(s1)<br>    |
|1016|[0x800143cc]<br>0xFFFFA7C5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d4ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d4b0]:csrrs a3, fcsr, zero<br> [0x8000d4b4]:sw t6, 760(s1)<br>    |
|1017|[0x800143d4]<br>0xFFFFA7C5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d4ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d4f0]:csrrs a3, fcsr, zero<br> [0x8000d4f4]:sw t6, 768(s1)<br>    |
|1018|[0x800143dc]<br>0xFFFFA7C5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d52c]:fmul.h t6, t5, t4, dyn<br> [0x8000d530]:csrrs a3, fcsr, zero<br> [0x8000d534]:sw t6, 776(s1)<br>    |
|1019|[0x800143e4]<br>0xFFFFA7C5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d56c]:fmul.h t6, t5, t4, dyn<br> [0x8000d570]:csrrs a3, fcsr, zero<br> [0x8000d574]:sw t6, 784(s1)<br>    |
|1020|[0x800143ec]<br>0xFFFFA7C5<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x350 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x011 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d5ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d5b0]:csrrs a3, fcsr, zero<br> [0x8000d5b4]:sw t6, 792(s1)<br>    |
|1021|[0x800143f4]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d5ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d5f0]:csrrs a3, fcsr, zero<br> [0x8000d5f4]:sw t6, 800(s1)<br>    |
|1022|[0x800143fc]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d62c]:fmul.h t6, t5, t4, dyn<br> [0x8000d630]:csrrs a3, fcsr, zero<br> [0x8000d634]:sw t6, 808(s1)<br>    |
|1023|[0x80014404]<br>0xFFFFA7F4<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d66c]:fmul.h t6, t5, t4, dyn<br> [0x8000d670]:csrrs a3, fcsr, zero<br> [0x8000d674]:sw t6, 816(s1)<br>    |
|1024|[0x8001440c]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d6ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d6b0]:csrrs a3, fcsr, zero<br> [0x8000d6b4]:sw t6, 824(s1)<br>    |
|1025|[0x80014414]<br>0xFFFFA7F3<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x311 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x012 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d6ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d6f0]:csrrs a3, fcsr, zero<br> [0x8000d6f4]:sw t6, 832(s1)<br>    |
|1026|[0x8001441c]<br>0xFFFFA727<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d72c]:fmul.h t6, t5, t4, dyn<br> [0x8000d730]:csrrs a3, fcsr, zero<br> [0x8000d734]:sw t6, 840(s1)<br>    |
|1027|[0x80014424]<br>0xFFFFA727<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d76c]:fmul.h t6, t5, t4, dyn<br> [0x8000d770]:csrrs a3, fcsr, zero<br> [0x8000d774]:sw t6, 848(s1)<br>    |
|1028|[0x8001442c]<br>0xFFFFA727<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d7ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d7b0]:csrrs a3, fcsr, zero<br> [0x8000d7b4]:sw t6, 856(s1)<br>    |
|1029|[0x80014434]<br>0xFFFFA727<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d7ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d7f0]:csrrs a3, fcsr, zero<br> [0x8000d7f4]:sw t6, 864(s1)<br>    |
|1030|[0x8001443c]<br>0xFFFFA727<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x327 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d82c]:fmul.h t6, t5, t4, dyn<br> [0x8000d830]:csrrs a3, fcsr, zero<br> [0x8000d834]:sw t6, 872(s1)<br>    |
|1031|[0x80014444]<br>0xFFFFA76E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d86c]:fmul.h t6, t5, t4, dyn<br> [0x8000d870]:csrrs a3, fcsr, zero<br> [0x8000d874]:sw t6, 880(s1)<br>    |
|1032|[0x8001444c]<br>0xFFFFA76E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d8ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d8b0]:csrrs a3, fcsr, zero<br> [0x8000d8b4]:sw t6, 888(s1)<br>    |
|1033|[0x80014454]<br>0xFFFFA76E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d8ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d8f0]:csrrs a3, fcsr, zero<br> [0x8000d8f4]:sw t6, 896(s1)<br>    |
|1034|[0x8001445c]<br>0xFFFFA76E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d92c]:fmul.h t6, t5, t4, dyn<br> [0x8000d930]:csrrs a3, fcsr, zero<br> [0x8000d934]:sw t6, 904(s1)<br>    |
|1035|[0x80014464]<br>0xFFFFA76E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x36e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d96c]:fmul.h t6, t5, t4, dyn<br> [0x8000d970]:csrrs a3, fcsr, zero<br> [0x8000d974]:sw t6, 912(s1)<br>    |
|1036|[0x8001446c]<br>0xFFFFA7F6<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000d9ac]:fmul.h t6, t5, t4, dyn<br> [0x8000d9b0]:csrrs a3, fcsr, zero<br> [0x8000d9b4]:sw t6, 920(s1)<br>    |
|1037|[0x80014474]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000d9ec]:fmul.h t6, t5, t4, dyn<br> [0x8000d9f0]:csrrs a3, fcsr, zero<br> [0x8000d9f4]:sw t6, 928(s1)<br>    |
|1038|[0x8001447c]<br>0xFFFFA7F6<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000da2c]:fmul.h t6, t5, t4, dyn<br> [0x8000da30]:csrrs a3, fcsr, zero<br> [0x8000da34]:sw t6, 936(s1)<br>    |
|1039|[0x80014484]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000da6c]:fmul.h t6, t5, t4, dyn<br> [0x8000da70]:csrrs a3, fcsr, zero<br> [0x8000da74]:sw t6, 944(s1)<br>    |
|1040|[0x8001448c]<br>0xFFFFA7F6<br> |- fs1 == 0 and fe1 == 0x1b and fm1 == 0x25e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x050 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000daac]:fmul.h t6, t5, t4, dyn<br> [0x8000dab0]:csrrs a3, fcsr, zero<br> [0x8000dab4]:sw t6, 952(s1)<br>    |
|1041|[0x80014494]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000daec]:fmul.h t6, t5, t4, dyn<br> [0x8000daf0]:csrrs a3, fcsr, zero<br> [0x8000daf4]:sw t6, 960(s1)<br>    |
|1042|[0x8001449c]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000db2c]:fmul.h t6, t5, t4, dyn<br> [0x8000db30]:csrrs a3, fcsr, zero<br> [0x8000db34]:sw t6, 968(s1)<br>    |
|1043|[0x800144a4]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000db6c]:fmul.h t6, t5, t4, dyn<br> [0x8000db70]:csrrs a3, fcsr, zero<br> [0x8000db74]:sw t6, 976(s1)<br>    |
|1044|[0x800144ac]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dbac]:fmul.h t6, t5, t4, dyn<br> [0x8000dbb0]:csrrs a3, fcsr, zero<br> [0x8000dbb4]:sw t6, 984(s1)<br>    |
|1045|[0x800144b4]<br>0xFFFFA7F5<br> |- fs1 == 0 and fe1 == 0x1c and fm1 == 0x210 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x02a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dbec]:fmul.h t6, t5, t4, dyn<br> [0x8000dbf0]:csrrs a3, fcsr, zero<br> [0x8000dbf4]:sw t6, 992(s1)<br>    |
|1046|[0x800144bc]<br>0xFFFFA7D8<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000dc24]:fmul.h t6, t5, t4, dyn<br> [0x8000dc28]:csrrs a3, fcsr, zero<br> [0x8000dc2c]:sw t6, 1000(s1)<br>   |
|1047|[0x800144c4]<br>0xFFFFA7D8<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dc5c]:fmul.h t6, t5, t4, dyn<br> [0x8000dc60]:csrrs a3, fcsr, zero<br> [0x8000dc64]:sw t6, 1008(s1)<br>   |
|1048|[0x800144cc]<br>0xFFFFA7D9<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dc94]:fmul.h t6, t5, t4, dyn<br> [0x8000dc98]:csrrs a3, fcsr, zero<br> [0x8000dc9c]:sw t6, 1016(s1)<br>   |
|1049|[0x800144d4]<br>0xFFFFA7D8<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dcd4]:fmul.h t6, t5, t4, dyn<br> [0x8000dcd8]:csrrs a3, fcsr, zero<br> [0x8000dcdc]:sw t6, 0(s1)<br>      |
|1050|[0x800144dc]<br>0xFFFFA7D8<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x1fa and fs2 == 1 and fe2 == 0x00 and fm2 == 0x015 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dd0c]:fmul.h t6, t5, t4, dyn<br> [0x8000dd10]:csrrs a3, fcsr, zero<br> [0x8000dd14]:sw t6, 8(s1)<br>      |
|1051|[0x800144e4]<br>0xFFFFA7F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000dd44]:fmul.h t6, t5, t4, dyn<br> [0x8000dd48]:csrrs a3, fcsr, zero<br> [0x8000dd4c]:sw t6, 16(s1)<br>     |
|1052|[0x800144ec]<br>0xFFFFA7F1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dd7c]:fmul.h t6, t5, t4, dyn<br> [0x8000dd80]:csrrs a3, fcsr, zero<br> [0x8000dd84]:sw t6, 24(s1)<br>     |
|1053|[0x800144f4]<br>0xFFFFA7F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ddb4]:fmul.h t6, t5, t4, dyn<br> [0x8000ddb8]:csrrs a3, fcsr, zero<br> [0x8000ddbc]:sw t6, 32(s1)<br>     |
|1054|[0x800144fc]<br>0xFFFFA7F1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000ddec]:fmul.h t6, t5, t4, dyn<br> [0x8000ddf0]:csrrs a3, fcsr, zero<br> [0x8000ddf4]:sw t6, 40(s1)<br>     |
|1055|[0x80014504]<br>0xFFFFA7F2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x25b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000de24]:fmul.h t6, t5, t4, dyn<br> [0x8000de28]:csrrs a3, fcsr, zero<br> [0x8000de2c]:sw t6, 48(s1)<br>     |
|1056|[0x8001450c]<br>0xFFFFA746<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000de5c]:fmul.h t6, t5, t4, dyn<br> [0x8000de60]:csrrs a3, fcsr, zero<br> [0x8000de64]:sw t6, 56(s1)<br>     |
|1057|[0x80014514]<br>0xFFFFA745<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000de94]:fmul.h t6, t5, t4, dyn<br> [0x8000de98]:csrrs a3, fcsr, zero<br> [0x8000de9c]:sw t6, 64(s1)<br>     |
|1058|[0x8001451c]<br>0xFFFFA746<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000decc]:fmul.h t6, t5, t4, dyn<br> [0x8000ded0]:csrrs a3, fcsr, zero<br> [0x8000ded4]:sw t6, 72(s1)<br>     |
|1059|[0x80014524]<br>0xFFFFA745<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000df04]:fmul.h t6, t5, t4, dyn<br> [0x8000df08]:csrrs a3, fcsr, zero<br> [0x8000df0c]:sw t6, 80(s1)<br>     |
|1060|[0x8001452c]<br>0xFFFFA746<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x277 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000df3c]:fmul.h t6, t5, t4, dyn<br> [0x8000df40]:csrrs a3, fcsr, zero<br> [0x8000df44]:sw t6, 88(s1)<br>     |
|1061|[0x80014534]<br>0xFFFFA799<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000df74]:fmul.h t6, t5, t4, dyn<br> [0x8000df78]:csrrs a3, fcsr, zero<br> [0x8000df7c]:sw t6, 96(s1)<br>     |
|1062|[0x8001453c]<br>0xFFFFA799<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dfac]:fmul.h t6, t5, t4, dyn<br> [0x8000dfb0]:csrrs a3, fcsr, zero<br> [0x8000dfb4]:sw t6, 104(s1)<br>    |
|1063|[0x80014544]<br>0xFFFFA79A<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000dfe4]:fmul.h t6, t5, t4, dyn<br> [0x8000dfe8]:csrrs a3, fcsr, zero<br> [0x8000dfec]:sw t6, 112(s1)<br>    |
|1064|[0x8001454c]<br>0xFFFFA799<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e01c]:fmul.h t6, t5, t4, dyn<br> [0x8000e020]:csrrs a3, fcsr, zero<br> [0x8000e024]:sw t6, 120(s1)<br>    |
|1065|[0x80014554]<br>0xFFFFA799<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x266 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x013 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e054]:fmul.h t6, t5, t4, dyn<br> [0x8000e058]:csrrs a3, fcsr, zero<br> [0x8000e05c]:sw t6, 128(s1)<br>    |
|1066|[0x8001455c]<br>0xFFFFA7DE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e08c]:fmul.h t6, t5, t4, dyn<br> [0x8000e090]:csrrs a3, fcsr, zero<br> [0x8000e094]:sw t6, 136(s1)<br>    |
|1067|[0x80014564]<br>0xFFFFA7DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e0c4]:fmul.h t6, t5, t4, dyn<br> [0x8000e0c8]:csrrs a3, fcsr, zero<br> [0x8000e0cc]:sw t6, 144(s1)<br>    |
|1068|[0x8001456c]<br>0xFFFFA7DE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e0fc]:fmul.h t6, t5, t4, dyn<br> [0x8000e100]:csrrs a3, fcsr, zero<br> [0x8000e104]:sw t6, 152(s1)<br>    |
|1069|[0x80014574]<br>0xFFFFA7DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e134]:fmul.h t6, t5, t4, dyn<br> [0x8000e138]:csrrs a3, fcsr, zero<br> [0x8000e13c]:sw t6, 160(s1)<br>    |
|1070|[0x8001457c]<br>0xFFFFA7DE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x179 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x017 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e16c]:fmul.h t6, t5, t4, dyn<br> [0x8000e170]:csrrs a3, fcsr, zero<br> [0x8000e174]:sw t6, 168(s1)<br>    |
|1071|[0x80014584]<br>0x00002767<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e1a4]:fmul.h t6, t5, t4, dyn<br> [0x8000e1a8]:csrrs a3, fcsr, zero<br> [0x8000e1ac]:sw t6, 176(s1)<br>    |
|1072|[0x8001458c]<br>0x00002767<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e1dc]:fmul.h t6, t5, t4, dyn<br> [0x8000e1e0]:csrrs a3, fcsr, zero<br> [0x8000e1e4]:sw t6, 184(s1)<br>    |
|1073|[0x80014594]<br>0x00002767<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e214]:fmul.h t6, t5, t4, dyn<br> [0x8000e218]:csrrs a3, fcsr, zero<br> [0x8000e21c]:sw t6, 192(s1)<br>    |
|1074|[0x8001459c]<br>0x00002767<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e24c]:fmul.h t6, t5, t4, dyn<br> [0x8000e250]:csrrs a3, fcsr, zero<br> [0x8000e254]:sw t6, 200(s1)<br>    |
|1075|[0x800145a4]<br>0x00002767<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x367 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x008 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e284]:fmul.h t6, t5, t4, dyn<br> [0x8000e288]:csrrs a3, fcsr, zero<br> [0x8000e28c]:sw t6, 208(s1)<br>    |
|1076|[0x800145ac]<br>0x00002796<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e2bc]:fmul.h t6, t5, t4, dyn<br> [0x8000e2c0]:csrrs a3, fcsr, zero<br> [0x8000e2c4]:sw t6, 216(s1)<br>    |
|1077|[0x800145b4]<br>0x00002795<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e2f4]:fmul.h t6, t5, t4, dyn<br> [0x8000e2f8]:csrrs a3, fcsr, zero<br> [0x8000e2fc]:sw t6, 224(s1)<br>    |
|1078|[0x800145bc]<br>0x00002795<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e32c]:fmul.h t6, t5, t4, dyn<br> [0x8000e330]:csrrs a3, fcsr, zero<br> [0x8000e334]:sw t6, 232(s1)<br>    |
|1079|[0x800145c4]<br>0x00002796<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e364]:fmul.h t6, t5, t4, dyn<br> [0x8000e368]:csrrs a3, fcsr, zero<br> [0x8000e36c]:sw t6, 240(s1)<br>    |
|1080|[0x800145cc]<br>0x00002796<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x184 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e39c]:fmul.h t6, t5, t4, dyn<br> [0x8000e3a0]:csrrs a3, fcsr, zero<br> [0x8000e3a4]:sw t6, 248(s1)<br>    |
|1081|[0x800145d4]<br>0x0000276C<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e3d4]:fmul.h t6, t5, t4, dyn<br> [0x8000e3d8]:csrrs a3, fcsr, zero<br> [0x8000e3dc]:sw t6, 256(s1)<br>    |
|1082|[0x800145dc]<br>0x0000276C<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e40c]:fmul.h t6, t5, t4, dyn<br> [0x8000e410]:csrrs a3, fcsr, zero<br> [0x8000e414]:sw t6, 264(s1)<br>    |
|1083|[0x800145e4]<br>0x0000276C<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e444]:fmul.h t6, t5, t4, dyn<br> [0x8000e448]:csrrs a3, fcsr, zero<br> [0x8000e44c]:sw t6, 272(s1)<br>    |
|1084|[0x800145ec]<br>0x0000276C<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e47c]:fmul.h t6, t5, t4, dyn<br> [0x8000e480]:csrrs a3, fcsr, zero<br> [0x8000e484]:sw t6, 280(s1)<br>    |
|1085|[0x800145f4]<br>0x0000276C<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x1f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e4b4]:fmul.h t6, t5, t4, dyn<br> [0x8000e4b8]:csrrs a3, fcsr, zero<br> [0x8000e4bc]:sw t6, 288(s1)<br>    |
|1086|[0x800145fc]<br>0x000027D1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e4ec]:fmul.h t6, t5, t4, dyn<br> [0x8000e4f0]:csrrs a3, fcsr, zero<br> [0x8000e4f4]:sw t6, 296(s1)<br>    |
|1087|[0x80014604]<br>0x000027D1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e524]:fmul.h t6, t5, t4, dyn<br> [0x8000e528]:csrrs a3, fcsr, zero<br> [0x8000e52c]:sw t6, 304(s1)<br>    |
|1088|[0x8001460c]<br>0x000027D1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e55c]:fmul.h t6, t5, t4, dyn<br> [0x8000e560]:csrrs a3, fcsr, zero<br> [0x8000e564]:sw t6, 312(s1)<br>    |
|1089|[0x80014614]<br>0x000027D2<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e594]:fmul.h t6, t5, t4, dyn<br> [0x8000e598]:csrrs a3, fcsr, zero<br> [0x8000e59c]:sw t6, 320(s1)<br>    |
|1090|[0x8001461c]<br>0x000027D1<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x2f3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x009 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e5cc]:fmul.h t6, t5, t4, dyn<br> [0x8000e5d0]:csrrs a3, fcsr, zero<br> [0x8000e5d4]:sw t6, 328(s1)<br>    |
|1091|[0x80014624]<br>0x00002788<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e604]:fmul.h t6, t5, t4, dyn<br> [0x8000e608]:csrrs a3, fcsr, zero<br> [0x8000e60c]:sw t6, 336(s1)<br>    |
|1092|[0x8001462c]<br>0x00002787<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e63c]:fmul.h t6, t5, t4, dyn<br> [0x8000e640]:csrrs a3, fcsr, zero<br> [0x8000e644]:sw t6, 344(s1)<br>    |
|1093|[0x80014634]<br>0x00002787<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e674]:fmul.h t6, t5, t4, dyn<br> [0x8000e678]:csrrs a3, fcsr, zero<br> [0x8000e67c]:sw t6, 352(s1)<br>    |
|1094|[0x8001463c]<br>0x00002788<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e6ac]:fmul.h t6, t5, t4, dyn<br> [0x8000e6b0]:csrrs a3, fcsr, zero<br> [0x8000e6b4]:sw t6, 360(s1)<br>    |
|1095|[0x80014644]<br>0x00002788<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x206 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00a and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e6e4]:fmul.h t6, t5, t4, dyn<br> [0x8000e6e8]:csrrs a3, fcsr, zero<br> [0x8000e6ec]:sw t6, 368(s1)<br>    |
|1096|[0x8001464c]<br>0x000027DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e71c]:fmul.h t6, t5, t4, dyn<br> [0x8000e720]:csrrs a3, fcsr, zero<br> [0x8000e724]:sw t6, 376(s1)<br>    |
|1097|[0x80014654]<br>0x000027DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e754]:fmul.h t6, t5, t4, dyn<br> [0x8000e758]:csrrs a3, fcsr, zero<br> [0x8000e75c]:sw t6, 384(s1)<br>    |
|1098|[0x8001465c]<br>0x000027DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e78c]:fmul.h t6, t5, t4, dyn<br> [0x8000e790]:csrrs a3, fcsr, zero<br> [0x8000e794]:sw t6, 392(s1)<br>    |
|1099|[0x80014664]<br>0x000027DE<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e7c4]:fmul.h t6, t5, t4, dyn<br> [0x8000e7c8]:csrrs a3, fcsr, zero<br> [0x8000e7cc]:sw t6, 400(s1)<br>    |
|1100|[0x8001466c]<br>0x000027DD<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x0a9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x01b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e7fc]:fmul.h t6, t5, t4, dyn<br> [0x8000e800]:csrrs a3, fcsr, zero<br> [0x8000e804]:sw t6, 408(s1)<br>    |
|1101|[0x80014674]<br>0x0000275E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e834]:fmul.h t6, t5, t4, dyn<br> [0x8000e838]:csrrs a3, fcsr, zero<br> [0x8000e83c]:sw t6, 416(s1)<br>    |
|1102|[0x8001467c]<br>0x0000275E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x20 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e86c]:fmul.h t6, t5, t4, dyn<br> [0x8000e870]:csrrs a3, fcsr, zero<br> [0x8000e874]:sw t6, 424(s1)<br>    |
|1103|[0x80014684]<br>0x0000275E<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e8a4]:fmul.h t6, t5, t4, dyn<br> [0x8000e8a8]:csrrs a3, fcsr, zero<br> [0x8000e8ac]:sw t6, 432(s1)<br>    |
|1104|[0x8001468c]<br>0x0000275F<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x60 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e8dc]:fmul.h t6, t5, t4, dyn<br> [0x8000e8e0]:csrrs a3, fcsr, zero<br> [0x8000e8e4]:sw t6, 440(s1)<br>    |
|1105|[0x80014694]<br>0x0000275F<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x15c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x00b and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e914]:fmul.h t6, t5, t4, dyn<br> [0x8000e918]:csrrs a3, fcsr, zero<br> [0x8000e91c]:sw t6, 448(s1)<br>    |
|1106|[0x8001469c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x40 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e94c]:fmul.h t6, t5, t4, dyn<br> [0x8000e950]:csrrs a3, fcsr, zero<br> [0x8000e954]:sw t6, 456(s1)<br>    |
|1107|[0x800146a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3a5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x80 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                              |[0x8000e984]:fmul.h t6, t5, t4, dyn<br> [0x8000e988]:csrrs a3, fcsr, zero<br> [0x8000e98c]:sw t6, 464(s1)<br>    |
|1108|[0x800146b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1d and fm1 == 0x2e3 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0x0 and rm_val == 7  and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                               |[0x8000e9f4]:fmul.h t6, t5, t4, dyn<br> [0x8000e9f8]:csrrs a3, fcsr, zero<br> [0x8000e9fc]:sw t6, 480(s1)<br>    |
