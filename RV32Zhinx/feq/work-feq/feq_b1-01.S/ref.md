
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 32      |
| TEST_REGION               | [('0x800000f8', '0x80006e20')]      |
| SIG_REGION                | [('0x80009410', '0x8000a640', '1164 words')]      |
| COV_LABELS                | feq_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV32Zhinx/work-feq/feq_b1-01.S/ref.S    |
| Total Number of coverpoints| 675     |
| Total Coverpoints Hit     | 675      |
| Total Signature Updates   | 1160      |
| STAT1                     | 578      |
| STAT2                     | 2      |
| STAT3                     | 0     |
| STAT4                     | 580     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80006d5c]:feq.h t6, t5, t4
      [0x80006d60]:csrrs a2, fcsr, zero
      [0x80006d64]:sw t6, 312(fp)
 -- Signature Addresses:
      Address: 0x8000a614 Data: 0x00000001
 -- Redundant Coverpoints hit by the op
      - mnemonic : feq.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80006e04]:feq.h t6, t5, t4
      [0x80006e08]:csrrs a2, fcsr, zero
      [0x80006e0c]:sw t6, 336(fp)
 -- Signature Addresses:
      Address: 0x8000a62c Data: 0x00000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : feq.h
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2
      - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : feq.h', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000124]:feq.h t6, t6, t6
	-[0x80000128]:csrrs tp, fcsr, zero
	-[0x8000012c]:sw t6, 0(ra)
Current Store : [0x80000130] : sw tp, 4(ra) -- Store: [0x80009418]:0x00000000




Last Coverpoint : ['rs1 : x30', 'rs2 : x29', 'rd : x30', 'rs1 != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000144]:feq.h t5, t5, t4
	-[0x80000148]:csrrs tp, fcsr, zero
	-[0x8000014c]:sw t5, 8(ra)
Current Store : [0x80000150] : sw tp, 12(ra) -- Store: [0x80009420]:0x00000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x30', 'rd : x29', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000164]:feq.h t4, t3, t5
	-[0x80000168]:csrrs tp, fcsr, zero
	-[0x8000016c]:sw t4, 16(ra)
Current Store : [0x80000170] : sw tp, 20(ra) -- Store: [0x80009428]:0x00000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000184]:feq.h t3, t4, s11
	-[0x80000188]:csrrs tp, fcsr, zero
	-[0x8000018c]:sw t3, 24(ra)
Current Store : [0x80000190] : sw tp, 28(ra) -- Store: [0x80009430]:0x00000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001a4]:feq.h s11, s10, t3
	-[0x800001a8]:csrrs tp, fcsr, zero
	-[0x800001ac]:sw s11, 32(ra)
Current Store : [0x800001b0] : sw tp, 36(ra) -- Store: [0x80009438]:0x00000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001c4]:feq.h s10, s11, s9
	-[0x800001c8]:csrrs tp, fcsr, zero
	-[0x800001cc]:sw s10, 40(ra)
Current Store : [0x800001d0] : sw tp, 44(ra) -- Store: [0x80009440]:0x00000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800001e4]:feq.h s9, s8, s10
	-[0x800001e8]:csrrs tp, fcsr, zero
	-[0x800001ec]:sw s9, 48(ra)
Current Store : [0x800001f0] : sw tp, 52(ra) -- Store: [0x80009448]:0x00000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000204]:feq.h s8, s9, s7
	-[0x80000208]:csrrs tp, fcsr, zero
	-[0x8000020c]:sw s8, 56(ra)
Current Store : [0x80000210] : sw tp, 60(ra) -- Store: [0x80009450]:0x00000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000224]:feq.h s7, s6, s8
	-[0x80000228]:csrrs tp, fcsr, zero
	-[0x8000022c]:sw s7, 64(ra)
Current Store : [0x80000230] : sw tp, 68(ra) -- Store: [0x80009458]:0x00000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000244]:feq.h s6, s7, s5
	-[0x80000248]:csrrs tp, fcsr, zero
	-[0x8000024c]:sw s6, 72(ra)
Current Store : [0x80000250] : sw tp, 76(ra) -- Store: [0x80009460]:0x00000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000264]:feq.h s5, s4, s6
	-[0x80000268]:csrrs tp, fcsr, zero
	-[0x8000026c]:sw s5, 80(ra)
Current Store : [0x80000270] : sw tp, 84(ra) -- Store: [0x80009468]:0x00000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000284]:feq.h s4, s5, s3
	-[0x80000288]:csrrs tp, fcsr, zero
	-[0x8000028c]:sw s4, 88(ra)
Current Store : [0x80000290] : sw tp, 92(ra) -- Store: [0x80009470]:0x00000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002a4]:feq.h s3, s2, s4
	-[0x800002a8]:csrrs tp, fcsr, zero
	-[0x800002ac]:sw s3, 96(ra)
Current Store : [0x800002b0] : sw tp, 100(ra) -- Store: [0x80009478]:0x00000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002c4]:feq.h s2, s3, a7
	-[0x800002c8]:csrrs tp, fcsr, zero
	-[0x800002cc]:sw s2, 104(ra)
Current Store : [0x800002d0] : sw tp, 108(ra) -- Store: [0x80009480]:0x00000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800002e4]:feq.h a7, a6, s2
	-[0x800002e8]:csrrs tp, fcsr, zero
	-[0x800002ec]:sw a7, 112(ra)
Current Store : [0x800002f0] : sw tp, 116(ra) -- Store: [0x80009488]:0x00000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000304]:feq.h a6, a7, a5
	-[0x80000308]:csrrs tp, fcsr, zero
	-[0x8000030c]:sw a6, 120(ra)
Current Store : [0x80000310] : sw tp, 124(ra) -- Store: [0x80009490]:0x00000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000324]:feq.h a5, a4, a6
	-[0x80000328]:csrrs tp, fcsr, zero
	-[0x8000032c]:sw a5, 128(ra)
Current Store : [0x80000330] : sw tp, 132(ra) -- Store: [0x80009498]:0x00000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000344]:feq.h a4, a5, a3
	-[0x80000348]:csrrs tp, fcsr, zero
	-[0x8000034c]:sw a4, 136(ra)
Current Store : [0x80000350] : sw tp, 140(ra) -- Store: [0x800094a0]:0x00000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000364]:feq.h a3, a2, a4
	-[0x80000368]:csrrs tp, fcsr, zero
	-[0x8000036c]:sw a3, 144(ra)
Current Store : [0x80000370] : sw tp, 148(ra) -- Store: [0x800094a8]:0x00000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000384]:feq.h a2, a3, a1
	-[0x80000388]:csrrs tp, fcsr, zero
	-[0x8000038c]:sw a2, 152(ra)
Current Store : [0x80000390] : sw tp, 156(ra) -- Store: [0x800094b0]:0x00000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003a4]:feq.h a1, a0, a2
	-[0x800003a8]:csrrs tp, fcsr, zero
	-[0x800003ac]:sw a1, 160(ra)
Current Store : [0x800003b0] : sw tp, 164(ra) -- Store: [0x800094b8]:0x00000010




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003c4]:feq.h a0, a1, s1
	-[0x800003c8]:csrrs tp, fcsr, zero
	-[0x800003cc]:sw a0, 168(ra)
Current Store : [0x800003d0] : sw tp, 172(ra) -- Store: [0x800094c0]:0x00000010




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800003ec]:feq.h s1, fp, a0
	-[0x800003f0]:csrrs a2, fcsr, zero
	-[0x800003f4]:sw s1, 176(ra)
Current Store : [0x800003f8] : sw a2, 180(ra) -- Store: [0x800094c8]:0x00000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:feq.h fp, s1, t2
	-[0x80000410]:csrrs a2, fcsr, zero
	-[0x80000414]:sw fp, 184(ra)
Current Store : [0x80000418] : sw a2, 188(ra) -- Store: [0x800094d0]:0x00000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000042c]:feq.h t2, t1, fp
	-[0x80000430]:csrrs a2, fcsr, zero
	-[0x80000434]:sw t2, 192(ra)
Current Store : [0x80000438] : sw a2, 196(ra) -- Store: [0x800094d8]:0x00000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000454]:feq.h t1, t2, t0
	-[0x80000458]:csrrs a2, fcsr, zero
	-[0x8000045c]:sw t1, 0(fp)
Current Store : [0x80000460] : sw a2, 4(fp) -- Store: [0x800094e0]:0x00000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000474]:feq.h t0, tp, t1
	-[0x80000478]:csrrs a2, fcsr, zero
	-[0x8000047c]:sw t0, 8(fp)
Current Store : [0x80000480] : sw a2, 12(fp) -- Store: [0x800094e8]:0x00000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000494]:feq.h tp, t0, gp
	-[0x80000498]:csrrs a2, fcsr, zero
	-[0x8000049c]:sw tp, 16(fp)
Current Store : [0x800004a0] : sw a2, 20(fp) -- Store: [0x800094f0]:0x00000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:feq.h gp, sp, tp
	-[0x800004b8]:csrrs a2, fcsr, zero
	-[0x800004bc]:sw gp, 24(fp)
Current Store : [0x800004c0] : sw a2, 28(fp) -- Store: [0x800094f8]:0x00000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800004d4]:feq.h sp, gp, ra
	-[0x800004d8]:csrrs a2, fcsr, zero
	-[0x800004dc]:sw sp, 32(fp)
Current Store : [0x800004e0] : sw a2, 36(fp) -- Store: [0x80009500]:0x00000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x800004f4]:feq.h ra, zero, sp
	-[0x800004f8]:csrrs a2, fcsr, zero
	-[0x800004fc]:sw ra, 40(fp)
Current Store : [0x80000500] : sw a2, 44(fp) -- Store: [0x80009508]:0x00000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000514]:feq.h t6, ra, t5
	-[0x80000518]:csrrs a2, fcsr, zero
	-[0x8000051c]:sw t6, 48(fp)
Current Store : [0x80000520] : sw a2, 52(fp) -- Store: [0x80009510]:0x00000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x80000534]:feq.h t6, t5, zero
	-[0x80000538]:csrrs a2, fcsr, zero
	-[0x8000053c]:sw t6, 56(fp)
Current Store : [0x80000540] : sw a2, 60(fp) -- Store: [0x80009518]:0x00000000




Last Coverpoint : ['rd : x0', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000554]:feq.h zero, t6, t5
	-[0x80000558]:csrrs a2, fcsr, zero
	-[0x8000055c]:sw zero, 64(fp)
Current Store : [0x80000560] : sw a2, 68(fp) -- Store: [0x80009520]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000574]:feq.h t6, t5, t4
	-[0x80000578]:csrrs a2, fcsr, zero
	-[0x8000057c]:sw t6, 72(fp)
Current Store : [0x80000580] : sw a2, 76(fp) -- Store: [0x80009528]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:feq.h t6, t5, t4
	-[0x80000598]:csrrs a2, fcsr, zero
	-[0x8000059c]:sw t6, 80(fp)
Current Store : [0x800005a0] : sw a2, 84(fp) -- Store: [0x80009530]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005b4]:feq.h t6, t5, t4
	-[0x800005b8]:csrrs a2, fcsr, zero
	-[0x800005bc]:sw t6, 88(fp)
Current Store : [0x800005c0] : sw a2, 92(fp) -- Store: [0x80009538]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005d4]:feq.h t6, t5, t4
	-[0x800005d8]:csrrs a2, fcsr, zero
	-[0x800005dc]:sw t6, 96(fp)
Current Store : [0x800005e0] : sw a2, 100(fp) -- Store: [0x80009540]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800005f4]:feq.h t6, t5, t4
	-[0x800005f8]:csrrs a2, fcsr, zero
	-[0x800005fc]:sw t6, 104(fp)
Current Store : [0x80000600] : sw a2, 108(fp) -- Store: [0x80009548]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000614]:feq.h t6, t5, t4
	-[0x80000618]:csrrs a2, fcsr, zero
	-[0x8000061c]:sw t6, 112(fp)
Current Store : [0x80000620] : sw a2, 116(fp) -- Store: [0x80009550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000634]:feq.h t6, t5, t4
	-[0x80000638]:csrrs a2, fcsr, zero
	-[0x8000063c]:sw t6, 120(fp)
Current Store : [0x80000640] : sw a2, 124(fp) -- Store: [0x80009558]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000654]:feq.h t6, t5, t4
	-[0x80000658]:csrrs a2, fcsr, zero
	-[0x8000065c]:sw t6, 128(fp)
Current Store : [0x80000660] : sw a2, 132(fp) -- Store: [0x80009560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000674]:feq.h t6, t5, t4
	-[0x80000678]:csrrs a2, fcsr, zero
	-[0x8000067c]:sw t6, 136(fp)
Current Store : [0x80000680] : sw a2, 140(fp) -- Store: [0x80009568]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000694]:feq.h t6, t5, t4
	-[0x80000698]:csrrs a2, fcsr, zero
	-[0x8000069c]:sw t6, 144(fp)
Current Store : [0x800006a0] : sw a2, 148(fp) -- Store: [0x80009570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006b4]:feq.h t6, t5, t4
	-[0x800006b8]:csrrs a2, fcsr, zero
	-[0x800006bc]:sw t6, 152(fp)
Current Store : [0x800006c0] : sw a2, 156(fp) -- Store: [0x80009578]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006d4]:feq.h t6, t5, t4
	-[0x800006d8]:csrrs a2, fcsr, zero
	-[0x800006dc]:sw t6, 160(fp)
Current Store : [0x800006e0] : sw a2, 164(fp) -- Store: [0x80009580]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800006f4]:feq.h t6, t5, t4
	-[0x800006f8]:csrrs a2, fcsr, zero
	-[0x800006fc]:sw t6, 168(fp)
Current Store : [0x80000700] : sw a2, 172(fp) -- Store: [0x80009588]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000714]:feq.h t6, t5, t4
	-[0x80000718]:csrrs a2, fcsr, zero
	-[0x8000071c]:sw t6, 176(fp)
Current Store : [0x80000720] : sw a2, 180(fp) -- Store: [0x80009590]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000734]:feq.h t6, t5, t4
	-[0x80000738]:csrrs a2, fcsr, zero
	-[0x8000073c]:sw t6, 184(fp)
Current Store : [0x80000740] : sw a2, 188(fp) -- Store: [0x80009598]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000754]:feq.h t6, t5, t4
	-[0x80000758]:csrrs a2, fcsr, zero
	-[0x8000075c]:sw t6, 192(fp)
Current Store : [0x80000760] : sw a2, 196(fp) -- Store: [0x800095a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000774]:feq.h t6, t5, t4
	-[0x80000778]:csrrs a2, fcsr, zero
	-[0x8000077c]:sw t6, 200(fp)
Current Store : [0x80000780] : sw a2, 204(fp) -- Store: [0x800095a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000794]:feq.h t6, t5, t4
	-[0x80000798]:csrrs a2, fcsr, zero
	-[0x8000079c]:sw t6, 208(fp)
Current Store : [0x800007a0] : sw a2, 212(fp) -- Store: [0x800095b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007b4]:feq.h t6, t5, t4
	-[0x800007b8]:csrrs a2, fcsr, zero
	-[0x800007bc]:sw t6, 216(fp)
Current Store : [0x800007c0] : sw a2, 220(fp) -- Store: [0x800095b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007d4]:feq.h t6, t5, t4
	-[0x800007d8]:csrrs a2, fcsr, zero
	-[0x800007dc]:sw t6, 224(fp)
Current Store : [0x800007e0] : sw a2, 228(fp) -- Store: [0x800095c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800007f4]:feq.h t6, t5, t4
	-[0x800007f8]:csrrs a2, fcsr, zero
	-[0x800007fc]:sw t6, 232(fp)
Current Store : [0x80000800] : sw a2, 236(fp) -- Store: [0x800095c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000814]:feq.h t6, t5, t4
	-[0x80000818]:csrrs a2, fcsr, zero
	-[0x8000081c]:sw t6, 240(fp)
Current Store : [0x80000820] : sw a2, 244(fp) -- Store: [0x800095d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000834]:feq.h t6, t5, t4
	-[0x80000838]:csrrs a2, fcsr, zero
	-[0x8000083c]:sw t6, 248(fp)
Current Store : [0x80000840] : sw a2, 252(fp) -- Store: [0x800095d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000854]:feq.h t6, t5, t4
	-[0x80000858]:csrrs a2, fcsr, zero
	-[0x8000085c]:sw t6, 256(fp)
Current Store : [0x80000860] : sw a2, 260(fp) -- Store: [0x800095e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000874]:feq.h t6, t5, t4
	-[0x80000878]:csrrs a2, fcsr, zero
	-[0x8000087c]:sw t6, 264(fp)
Current Store : [0x80000880] : sw a2, 268(fp) -- Store: [0x800095e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000894]:feq.h t6, t5, t4
	-[0x80000898]:csrrs a2, fcsr, zero
	-[0x8000089c]:sw t6, 272(fp)
Current Store : [0x800008a0] : sw a2, 276(fp) -- Store: [0x800095f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008b4]:feq.h t6, t5, t4
	-[0x800008b8]:csrrs a2, fcsr, zero
	-[0x800008bc]:sw t6, 280(fp)
Current Store : [0x800008c0] : sw a2, 284(fp) -- Store: [0x800095f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008d4]:feq.h t6, t5, t4
	-[0x800008d8]:csrrs a2, fcsr, zero
	-[0x800008dc]:sw t6, 288(fp)
Current Store : [0x800008e0] : sw a2, 292(fp) -- Store: [0x80009600]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800008f4]:feq.h t6, t5, t4
	-[0x800008f8]:csrrs a2, fcsr, zero
	-[0x800008fc]:sw t6, 296(fp)
Current Store : [0x80000900] : sw a2, 300(fp) -- Store: [0x80009608]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000914]:feq.h t6, t5, t4
	-[0x80000918]:csrrs a2, fcsr, zero
	-[0x8000091c]:sw t6, 304(fp)
Current Store : [0x80000920] : sw a2, 308(fp) -- Store: [0x80009610]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000934]:feq.h t6, t5, t4
	-[0x80000938]:csrrs a2, fcsr, zero
	-[0x8000093c]:sw t6, 312(fp)
Current Store : [0x80000940] : sw a2, 316(fp) -- Store: [0x80009618]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000954]:feq.h t6, t5, t4
	-[0x80000958]:csrrs a2, fcsr, zero
	-[0x8000095c]:sw t6, 320(fp)
Current Store : [0x80000960] : sw a2, 324(fp) -- Store: [0x80009620]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000974]:feq.h t6, t5, t4
	-[0x80000978]:csrrs a2, fcsr, zero
	-[0x8000097c]:sw t6, 328(fp)
Current Store : [0x80000980] : sw a2, 332(fp) -- Store: [0x80009628]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:feq.h t6, t5, t4
	-[0x80000998]:csrrs a2, fcsr, zero
	-[0x8000099c]:sw t6, 336(fp)
Current Store : [0x800009a0] : sw a2, 340(fp) -- Store: [0x80009630]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009b4]:feq.h t6, t5, t4
	-[0x800009b8]:csrrs a2, fcsr, zero
	-[0x800009bc]:sw t6, 344(fp)
Current Store : [0x800009c0] : sw a2, 348(fp) -- Store: [0x80009638]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009d4]:feq.h t6, t5, t4
	-[0x800009d8]:csrrs a2, fcsr, zero
	-[0x800009dc]:sw t6, 352(fp)
Current Store : [0x800009e0] : sw a2, 356(fp) -- Store: [0x80009640]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800009f4]:feq.h t6, t5, t4
	-[0x800009f8]:csrrs a2, fcsr, zero
	-[0x800009fc]:sw t6, 360(fp)
Current Store : [0x80000a00] : sw a2, 364(fp) -- Store: [0x80009648]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a14]:feq.h t6, t5, t4
	-[0x80000a18]:csrrs a2, fcsr, zero
	-[0x80000a1c]:sw t6, 368(fp)
Current Store : [0x80000a20] : sw a2, 372(fp) -- Store: [0x80009650]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a34]:feq.h t6, t5, t4
	-[0x80000a38]:csrrs a2, fcsr, zero
	-[0x80000a3c]:sw t6, 376(fp)
Current Store : [0x80000a40] : sw a2, 380(fp) -- Store: [0x80009658]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a54]:feq.h t6, t5, t4
	-[0x80000a58]:csrrs a2, fcsr, zero
	-[0x80000a5c]:sw t6, 384(fp)
Current Store : [0x80000a60] : sw a2, 388(fp) -- Store: [0x80009660]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:feq.h t6, t5, t4
	-[0x80000a78]:csrrs a2, fcsr, zero
	-[0x80000a7c]:sw t6, 392(fp)
Current Store : [0x80000a80] : sw a2, 396(fp) -- Store: [0x80009668]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000a94]:feq.h t6, t5, t4
	-[0x80000a98]:csrrs a2, fcsr, zero
	-[0x80000a9c]:sw t6, 400(fp)
Current Store : [0x80000aa0] : sw a2, 404(fp) -- Store: [0x80009670]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ab4]:feq.h t6, t5, t4
	-[0x80000ab8]:csrrs a2, fcsr, zero
	-[0x80000abc]:sw t6, 408(fp)
Current Store : [0x80000ac0] : sw a2, 412(fp) -- Store: [0x80009678]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ad4]:feq.h t6, t5, t4
	-[0x80000ad8]:csrrs a2, fcsr, zero
	-[0x80000adc]:sw t6, 416(fp)
Current Store : [0x80000ae0] : sw a2, 420(fp) -- Store: [0x80009680]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000af4]:feq.h t6, t5, t4
	-[0x80000af8]:csrrs a2, fcsr, zero
	-[0x80000afc]:sw t6, 424(fp)
Current Store : [0x80000b00] : sw a2, 428(fp) -- Store: [0x80009688]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b14]:feq.h t6, t5, t4
	-[0x80000b18]:csrrs a2, fcsr, zero
	-[0x80000b1c]:sw t6, 432(fp)
Current Store : [0x80000b20] : sw a2, 436(fp) -- Store: [0x80009690]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b34]:feq.h t6, t5, t4
	-[0x80000b38]:csrrs a2, fcsr, zero
	-[0x80000b3c]:sw t6, 440(fp)
Current Store : [0x80000b40] : sw a2, 444(fp) -- Store: [0x80009698]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:feq.h t6, t5, t4
	-[0x80000b58]:csrrs a2, fcsr, zero
	-[0x80000b5c]:sw t6, 448(fp)
Current Store : [0x80000b60] : sw a2, 452(fp) -- Store: [0x800096a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b74]:feq.h t6, t5, t4
	-[0x80000b78]:csrrs a2, fcsr, zero
	-[0x80000b7c]:sw t6, 456(fp)
Current Store : [0x80000b80] : sw a2, 460(fp) -- Store: [0x800096a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000b94]:feq.h t6, t5, t4
	-[0x80000b98]:csrrs a2, fcsr, zero
	-[0x80000b9c]:sw t6, 464(fp)
Current Store : [0x80000ba0] : sw a2, 468(fp) -- Store: [0x800096b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bb4]:feq.h t6, t5, t4
	-[0x80000bb8]:csrrs a2, fcsr, zero
	-[0x80000bbc]:sw t6, 472(fp)
Current Store : [0x80000bc0] : sw a2, 476(fp) -- Store: [0x800096b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bd4]:feq.h t6, t5, t4
	-[0x80000bd8]:csrrs a2, fcsr, zero
	-[0x80000bdc]:sw t6, 480(fp)
Current Store : [0x80000be0] : sw a2, 484(fp) -- Store: [0x800096c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000bf4]:feq.h t6, t5, t4
	-[0x80000bf8]:csrrs a2, fcsr, zero
	-[0x80000bfc]:sw t6, 488(fp)
Current Store : [0x80000c00] : sw a2, 492(fp) -- Store: [0x800096c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c14]:feq.h t6, t5, t4
	-[0x80000c18]:csrrs a2, fcsr, zero
	-[0x80000c1c]:sw t6, 496(fp)
Current Store : [0x80000c20] : sw a2, 500(fp) -- Store: [0x800096d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:feq.h t6, t5, t4
	-[0x80000c38]:csrrs a2, fcsr, zero
	-[0x80000c3c]:sw t6, 504(fp)
Current Store : [0x80000c40] : sw a2, 508(fp) -- Store: [0x800096d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c54]:feq.h t6, t5, t4
	-[0x80000c58]:csrrs a2, fcsr, zero
	-[0x80000c5c]:sw t6, 512(fp)
Current Store : [0x80000c60] : sw a2, 516(fp) -- Store: [0x800096e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c74]:feq.h t6, t5, t4
	-[0x80000c78]:csrrs a2, fcsr, zero
	-[0x80000c7c]:sw t6, 520(fp)
Current Store : [0x80000c80] : sw a2, 524(fp) -- Store: [0x800096e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000c94]:feq.h t6, t5, t4
	-[0x80000c98]:csrrs a2, fcsr, zero
	-[0x80000c9c]:sw t6, 528(fp)
Current Store : [0x80000ca0] : sw a2, 532(fp) -- Store: [0x800096f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cb4]:feq.h t6, t5, t4
	-[0x80000cb8]:csrrs a2, fcsr, zero
	-[0x80000cbc]:sw t6, 536(fp)
Current Store : [0x80000cc0] : sw a2, 540(fp) -- Store: [0x800096f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cd4]:feq.h t6, t5, t4
	-[0x80000cd8]:csrrs a2, fcsr, zero
	-[0x80000cdc]:sw t6, 544(fp)
Current Store : [0x80000ce0] : sw a2, 548(fp) -- Store: [0x80009700]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000cf4]:feq.h t6, t5, t4
	-[0x80000cf8]:csrrs a2, fcsr, zero
	-[0x80000cfc]:sw t6, 552(fp)
Current Store : [0x80000d00] : sw a2, 556(fp) -- Store: [0x80009708]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d14]:feq.h t6, t5, t4
	-[0x80000d18]:csrrs a2, fcsr, zero
	-[0x80000d1c]:sw t6, 560(fp)
Current Store : [0x80000d20] : sw a2, 564(fp) -- Store: [0x80009710]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d34]:feq.h t6, t5, t4
	-[0x80000d38]:csrrs a2, fcsr, zero
	-[0x80000d3c]:sw t6, 568(fp)
Current Store : [0x80000d40] : sw a2, 572(fp) -- Store: [0x80009718]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d54]:feq.h t6, t5, t4
	-[0x80000d58]:csrrs a2, fcsr, zero
	-[0x80000d5c]:sw t6, 576(fp)
Current Store : [0x80000d60] : sw a2, 580(fp) -- Store: [0x80009720]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d74]:feq.h t6, t5, t4
	-[0x80000d78]:csrrs a2, fcsr, zero
	-[0x80000d7c]:sw t6, 584(fp)
Current Store : [0x80000d80] : sw a2, 588(fp) -- Store: [0x80009728]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000d94]:feq.h t6, t5, t4
	-[0x80000d98]:csrrs a2, fcsr, zero
	-[0x80000d9c]:sw t6, 592(fp)
Current Store : [0x80000da0] : sw a2, 596(fp) -- Store: [0x80009730]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000db4]:feq.h t6, t5, t4
	-[0x80000db8]:csrrs a2, fcsr, zero
	-[0x80000dbc]:sw t6, 600(fp)
Current Store : [0x80000dc0] : sw a2, 604(fp) -- Store: [0x80009738]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000dd4]:feq.h t6, t5, t4
	-[0x80000dd8]:csrrs a2, fcsr, zero
	-[0x80000ddc]:sw t6, 608(fp)
Current Store : [0x80000de0] : sw a2, 612(fp) -- Store: [0x80009740]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000df4]:feq.h t6, t5, t4
	-[0x80000df8]:csrrs a2, fcsr, zero
	-[0x80000dfc]:sw t6, 616(fp)
Current Store : [0x80000e00] : sw a2, 620(fp) -- Store: [0x80009748]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e14]:feq.h t6, t5, t4
	-[0x80000e18]:csrrs a2, fcsr, zero
	-[0x80000e1c]:sw t6, 624(fp)
Current Store : [0x80000e20] : sw a2, 628(fp) -- Store: [0x80009750]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e34]:feq.h t6, t5, t4
	-[0x80000e38]:csrrs a2, fcsr, zero
	-[0x80000e3c]:sw t6, 632(fp)
Current Store : [0x80000e40] : sw a2, 636(fp) -- Store: [0x80009758]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e54]:feq.h t6, t5, t4
	-[0x80000e58]:csrrs a2, fcsr, zero
	-[0x80000e5c]:sw t6, 640(fp)
Current Store : [0x80000e60] : sw a2, 644(fp) -- Store: [0x80009760]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e74]:feq.h t6, t5, t4
	-[0x80000e78]:csrrs a2, fcsr, zero
	-[0x80000e7c]:sw t6, 648(fp)
Current Store : [0x80000e80] : sw a2, 652(fp) -- Store: [0x80009768]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000e94]:feq.h t6, t5, t4
	-[0x80000e98]:csrrs a2, fcsr, zero
	-[0x80000e9c]:sw t6, 656(fp)
Current Store : [0x80000ea0] : sw a2, 660(fp) -- Store: [0x80009770]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000eb4]:feq.h t6, t5, t4
	-[0x80000eb8]:csrrs a2, fcsr, zero
	-[0x80000ebc]:sw t6, 664(fp)
Current Store : [0x80000ec0] : sw a2, 668(fp) -- Store: [0x80009778]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:feq.h t6, t5, t4
	-[0x80000ed8]:csrrs a2, fcsr, zero
	-[0x80000edc]:sw t6, 672(fp)
Current Store : [0x80000ee0] : sw a2, 676(fp) -- Store: [0x80009780]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ef4]:feq.h t6, t5, t4
	-[0x80000ef8]:csrrs a2, fcsr, zero
	-[0x80000efc]:sw t6, 680(fp)
Current Store : [0x80000f00] : sw a2, 684(fp) -- Store: [0x80009788]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f14]:feq.h t6, t5, t4
	-[0x80000f18]:csrrs a2, fcsr, zero
	-[0x80000f1c]:sw t6, 688(fp)
Current Store : [0x80000f20] : sw a2, 692(fp) -- Store: [0x80009790]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f34]:feq.h t6, t5, t4
	-[0x80000f38]:csrrs a2, fcsr, zero
	-[0x80000f3c]:sw t6, 696(fp)
Current Store : [0x80000f40] : sw a2, 700(fp) -- Store: [0x80009798]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f54]:feq.h t6, t5, t4
	-[0x80000f58]:csrrs a2, fcsr, zero
	-[0x80000f5c]:sw t6, 704(fp)
Current Store : [0x80000f60] : sw a2, 708(fp) -- Store: [0x800097a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f74]:feq.h t6, t5, t4
	-[0x80000f78]:csrrs a2, fcsr, zero
	-[0x80000f7c]:sw t6, 712(fp)
Current Store : [0x80000f80] : sw a2, 716(fp) -- Store: [0x800097a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000f94]:feq.h t6, t5, t4
	-[0x80000f98]:csrrs a2, fcsr, zero
	-[0x80000f9c]:sw t6, 720(fp)
Current Store : [0x80000fa0] : sw a2, 724(fp) -- Store: [0x800097b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fb4]:feq.h t6, t5, t4
	-[0x80000fb8]:csrrs a2, fcsr, zero
	-[0x80000fbc]:sw t6, 728(fp)
Current Store : [0x80000fc0] : sw a2, 732(fp) -- Store: [0x800097b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000fd4]:feq.h t6, t5, t4
	-[0x80000fd8]:csrrs a2, fcsr, zero
	-[0x80000fdc]:sw t6, 736(fp)
Current Store : [0x80000fe0] : sw a2, 740(fp) -- Store: [0x800097c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80000ff4]:feq.h t6, t5, t4
	-[0x80000ff8]:csrrs a2, fcsr, zero
	-[0x80000ffc]:sw t6, 744(fp)
Current Store : [0x80001000] : sw a2, 748(fp) -- Store: [0x800097c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001014]:feq.h t6, t5, t4
	-[0x80001018]:csrrs a2, fcsr, zero
	-[0x8000101c]:sw t6, 752(fp)
Current Store : [0x80001020] : sw a2, 756(fp) -- Store: [0x800097d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001034]:feq.h t6, t5, t4
	-[0x80001038]:csrrs a2, fcsr, zero
	-[0x8000103c]:sw t6, 760(fp)
Current Store : [0x80001040] : sw a2, 764(fp) -- Store: [0x800097d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001054]:feq.h t6, t5, t4
	-[0x80001058]:csrrs a2, fcsr, zero
	-[0x8000105c]:sw t6, 768(fp)
Current Store : [0x80001060] : sw a2, 772(fp) -- Store: [0x800097e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001074]:feq.h t6, t5, t4
	-[0x80001078]:csrrs a2, fcsr, zero
	-[0x8000107c]:sw t6, 776(fp)
Current Store : [0x80001080] : sw a2, 780(fp) -- Store: [0x800097e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001094]:feq.h t6, t5, t4
	-[0x80001098]:csrrs a2, fcsr, zero
	-[0x8000109c]:sw t6, 784(fp)
Current Store : [0x800010a0] : sw a2, 788(fp) -- Store: [0x800097f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010b4]:feq.h t6, t5, t4
	-[0x800010b8]:csrrs a2, fcsr, zero
	-[0x800010bc]:sw t6, 792(fp)
Current Store : [0x800010c0] : sw a2, 796(fp) -- Store: [0x800097f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010d4]:feq.h t6, t5, t4
	-[0x800010d8]:csrrs a2, fcsr, zero
	-[0x800010dc]:sw t6, 800(fp)
Current Store : [0x800010e0] : sw a2, 804(fp) -- Store: [0x80009800]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800010f4]:feq.h t6, t5, t4
	-[0x800010f8]:csrrs a2, fcsr, zero
	-[0x800010fc]:sw t6, 808(fp)
Current Store : [0x80001100] : sw a2, 812(fp) -- Store: [0x80009808]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001114]:feq.h t6, t5, t4
	-[0x80001118]:csrrs a2, fcsr, zero
	-[0x8000111c]:sw t6, 816(fp)
Current Store : [0x80001120] : sw a2, 820(fp) -- Store: [0x80009810]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001134]:feq.h t6, t5, t4
	-[0x80001138]:csrrs a2, fcsr, zero
	-[0x8000113c]:sw t6, 824(fp)
Current Store : [0x80001140] : sw a2, 828(fp) -- Store: [0x80009818]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001154]:feq.h t6, t5, t4
	-[0x80001158]:csrrs a2, fcsr, zero
	-[0x8000115c]:sw t6, 832(fp)
Current Store : [0x80001160] : sw a2, 836(fp) -- Store: [0x80009820]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001174]:feq.h t6, t5, t4
	-[0x80001178]:csrrs a2, fcsr, zero
	-[0x8000117c]:sw t6, 840(fp)
Current Store : [0x80001180] : sw a2, 844(fp) -- Store: [0x80009828]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001194]:feq.h t6, t5, t4
	-[0x80001198]:csrrs a2, fcsr, zero
	-[0x8000119c]:sw t6, 848(fp)
Current Store : [0x800011a0] : sw a2, 852(fp) -- Store: [0x80009830]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011b4]:feq.h t6, t5, t4
	-[0x800011b8]:csrrs a2, fcsr, zero
	-[0x800011bc]:sw t6, 856(fp)
Current Store : [0x800011c0] : sw a2, 860(fp) -- Store: [0x80009838]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011d4]:feq.h t6, t5, t4
	-[0x800011d8]:csrrs a2, fcsr, zero
	-[0x800011dc]:sw t6, 864(fp)
Current Store : [0x800011e0] : sw a2, 868(fp) -- Store: [0x80009840]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800011f4]:feq.h t6, t5, t4
	-[0x800011f8]:csrrs a2, fcsr, zero
	-[0x800011fc]:sw t6, 872(fp)
Current Store : [0x80001200] : sw a2, 876(fp) -- Store: [0x80009848]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001214]:feq.h t6, t5, t4
	-[0x80001218]:csrrs a2, fcsr, zero
	-[0x8000121c]:sw t6, 880(fp)
Current Store : [0x80001220] : sw a2, 884(fp) -- Store: [0x80009850]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001234]:feq.h t6, t5, t4
	-[0x80001238]:csrrs a2, fcsr, zero
	-[0x8000123c]:sw t6, 888(fp)
Current Store : [0x80001240] : sw a2, 892(fp) -- Store: [0x80009858]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001254]:feq.h t6, t5, t4
	-[0x80001258]:csrrs a2, fcsr, zero
	-[0x8000125c]:sw t6, 896(fp)
Current Store : [0x80001260] : sw a2, 900(fp) -- Store: [0x80009860]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001274]:feq.h t6, t5, t4
	-[0x80001278]:csrrs a2, fcsr, zero
	-[0x8000127c]:sw t6, 904(fp)
Current Store : [0x80001280] : sw a2, 908(fp) -- Store: [0x80009868]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001294]:feq.h t6, t5, t4
	-[0x80001298]:csrrs a2, fcsr, zero
	-[0x8000129c]:sw t6, 912(fp)
Current Store : [0x800012a0] : sw a2, 916(fp) -- Store: [0x80009870]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012b4]:feq.h t6, t5, t4
	-[0x800012b8]:csrrs a2, fcsr, zero
	-[0x800012bc]:sw t6, 920(fp)
Current Store : [0x800012c0] : sw a2, 924(fp) -- Store: [0x80009878]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012d4]:feq.h t6, t5, t4
	-[0x800012d8]:csrrs a2, fcsr, zero
	-[0x800012dc]:sw t6, 928(fp)
Current Store : [0x800012e0] : sw a2, 932(fp) -- Store: [0x80009880]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800012f4]:feq.h t6, t5, t4
	-[0x800012f8]:csrrs a2, fcsr, zero
	-[0x800012fc]:sw t6, 936(fp)
Current Store : [0x80001300] : sw a2, 940(fp) -- Store: [0x80009888]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001314]:feq.h t6, t5, t4
	-[0x80001318]:csrrs a2, fcsr, zero
	-[0x8000131c]:sw t6, 944(fp)
Current Store : [0x80001320] : sw a2, 948(fp) -- Store: [0x80009890]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001334]:feq.h t6, t5, t4
	-[0x80001338]:csrrs a2, fcsr, zero
	-[0x8000133c]:sw t6, 952(fp)
Current Store : [0x80001340] : sw a2, 956(fp) -- Store: [0x80009898]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001354]:feq.h t6, t5, t4
	-[0x80001358]:csrrs a2, fcsr, zero
	-[0x8000135c]:sw t6, 960(fp)
Current Store : [0x80001360] : sw a2, 964(fp) -- Store: [0x800098a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001374]:feq.h t6, t5, t4
	-[0x80001378]:csrrs a2, fcsr, zero
	-[0x8000137c]:sw t6, 968(fp)
Current Store : [0x80001380] : sw a2, 972(fp) -- Store: [0x800098a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001394]:feq.h t6, t5, t4
	-[0x80001398]:csrrs a2, fcsr, zero
	-[0x8000139c]:sw t6, 976(fp)
Current Store : [0x800013a0] : sw a2, 980(fp) -- Store: [0x800098b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013b4]:feq.h t6, t5, t4
	-[0x800013b8]:csrrs a2, fcsr, zero
	-[0x800013bc]:sw t6, 984(fp)
Current Store : [0x800013c0] : sw a2, 988(fp) -- Store: [0x800098b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013d4]:feq.h t6, t5, t4
	-[0x800013d8]:csrrs a2, fcsr, zero
	-[0x800013dc]:sw t6, 992(fp)
Current Store : [0x800013e0] : sw a2, 996(fp) -- Store: [0x800098c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800013f4]:feq.h t6, t5, t4
	-[0x800013f8]:csrrs a2, fcsr, zero
	-[0x800013fc]:sw t6, 1000(fp)
Current Store : [0x80001400] : sw a2, 1004(fp) -- Store: [0x800098c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001414]:feq.h t6, t5, t4
	-[0x80001418]:csrrs a2, fcsr, zero
	-[0x8000141c]:sw t6, 1008(fp)
Current Store : [0x80001420] : sw a2, 1012(fp) -- Store: [0x800098d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001434]:feq.h t6, t5, t4
	-[0x80001438]:csrrs a2, fcsr, zero
	-[0x8000143c]:sw t6, 1016(fp)
Current Store : [0x80001440] : sw a2, 1020(fp) -- Store: [0x800098d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000145c]:feq.h t6, t5, t4
	-[0x80001460]:csrrs a2, fcsr, zero
	-[0x80001464]:sw t6, 0(fp)
Current Store : [0x80001468] : sw a2, 4(fp) -- Store: [0x800098e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000147c]:feq.h t6, t5, t4
	-[0x80001480]:csrrs a2, fcsr, zero
	-[0x80001484]:sw t6, 8(fp)
Current Store : [0x80001488] : sw a2, 12(fp) -- Store: [0x800098e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000149c]:feq.h t6, t5, t4
	-[0x800014a0]:csrrs a2, fcsr, zero
	-[0x800014a4]:sw t6, 16(fp)
Current Store : [0x800014a8] : sw a2, 20(fp) -- Store: [0x800098f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014bc]:feq.h t6, t5, t4
	-[0x800014c0]:csrrs a2, fcsr, zero
	-[0x800014c4]:sw t6, 24(fp)
Current Store : [0x800014c8] : sw a2, 28(fp) -- Store: [0x800098f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014dc]:feq.h t6, t5, t4
	-[0x800014e0]:csrrs a2, fcsr, zero
	-[0x800014e4]:sw t6, 32(fp)
Current Store : [0x800014e8] : sw a2, 36(fp) -- Store: [0x80009900]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800014fc]:feq.h t6, t5, t4
	-[0x80001500]:csrrs a2, fcsr, zero
	-[0x80001504]:sw t6, 40(fp)
Current Store : [0x80001508] : sw a2, 44(fp) -- Store: [0x80009908]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000151c]:feq.h t6, t5, t4
	-[0x80001520]:csrrs a2, fcsr, zero
	-[0x80001524]:sw t6, 48(fp)
Current Store : [0x80001528] : sw a2, 52(fp) -- Store: [0x80009910]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000153c]:feq.h t6, t5, t4
	-[0x80001540]:csrrs a2, fcsr, zero
	-[0x80001544]:sw t6, 56(fp)
Current Store : [0x80001548] : sw a2, 60(fp) -- Store: [0x80009918]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000155c]:feq.h t6, t5, t4
	-[0x80001560]:csrrs a2, fcsr, zero
	-[0x80001564]:sw t6, 64(fp)
Current Store : [0x80001568] : sw a2, 68(fp) -- Store: [0x80009920]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000157c]:feq.h t6, t5, t4
	-[0x80001580]:csrrs a2, fcsr, zero
	-[0x80001584]:sw t6, 72(fp)
Current Store : [0x80001588] : sw a2, 76(fp) -- Store: [0x80009928]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000159c]:feq.h t6, t5, t4
	-[0x800015a0]:csrrs a2, fcsr, zero
	-[0x800015a4]:sw t6, 80(fp)
Current Store : [0x800015a8] : sw a2, 84(fp) -- Store: [0x80009930]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015bc]:feq.h t6, t5, t4
	-[0x800015c0]:csrrs a2, fcsr, zero
	-[0x800015c4]:sw t6, 88(fp)
Current Store : [0x800015c8] : sw a2, 92(fp) -- Store: [0x80009938]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015dc]:feq.h t6, t5, t4
	-[0x800015e0]:csrrs a2, fcsr, zero
	-[0x800015e4]:sw t6, 96(fp)
Current Store : [0x800015e8] : sw a2, 100(fp) -- Store: [0x80009940]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800015fc]:feq.h t6, t5, t4
	-[0x80001600]:csrrs a2, fcsr, zero
	-[0x80001604]:sw t6, 104(fp)
Current Store : [0x80001608] : sw a2, 108(fp) -- Store: [0x80009948]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000161c]:feq.h t6, t5, t4
	-[0x80001620]:csrrs a2, fcsr, zero
	-[0x80001624]:sw t6, 112(fp)
Current Store : [0x80001628] : sw a2, 116(fp) -- Store: [0x80009950]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000163c]:feq.h t6, t5, t4
	-[0x80001640]:csrrs a2, fcsr, zero
	-[0x80001644]:sw t6, 120(fp)
Current Store : [0x80001648] : sw a2, 124(fp) -- Store: [0x80009958]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000165c]:feq.h t6, t5, t4
	-[0x80001660]:csrrs a2, fcsr, zero
	-[0x80001664]:sw t6, 128(fp)
Current Store : [0x80001668] : sw a2, 132(fp) -- Store: [0x80009960]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000167c]:feq.h t6, t5, t4
	-[0x80001680]:csrrs a2, fcsr, zero
	-[0x80001684]:sw t6, 136(fp)
Current Store : [0x80001688] : sw a2, 140(fp) -- Store: [0x80009968]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000169c]:feq.h t6, t5, t4
	-[0x800016a0]:csrrs a2, fcsr, zero
	-[0x800016a4]:sw t6, 144(fp)
Current Store : [0x800016a8] : sw a2, 148(fp) -- Store: [0x80009970]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016bc]:feq.h t6, t5, t4
	-[0x800016c0]:csrrs a2, fcsr, zero
	-[0x800016c4]:sw t6, 152(fp)
Current Store : [0x800016c8] : sw a2, 156(fp) -- Store: [0x80009978]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016dc]:feq.h t6, t5, t4
	-[0x800016e0]:csrrs a2, fcsr, zero
	-[0x800016e4]:sw t6, 160(fp)
Current Store : [0x800016e8] : sw a2, 164(fp) -- Store: [0x80009980]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800016fc]:feq.h t6, t5, t4
	-[0x80001700]:csrrs a2, fcsr, zero
	-[0x80001704]:sw t6, 168(fp)
Current Store : [0x80001708] : sw a2, 172(fp) -- Store: [0x80009988]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000171c]:feq.h t6, t5, t4
	-[0x80001720]:csrrs a2, fcsr, zero
	-[0x80001724]:sw t6, 176(fp)
Current Store : [0x80001728] : sw a2, 180(fp) -- Store: [0x80009990]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000173c]:feq.h t6, t5, t4
	-[0x80001740]:csrrs a2, fcsr, zero
	-[0x80001744]:sw t6, 184(fp)
Current Store : [0x80001748] : sw a2, 188(fp) -- Store: [0x80009998]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000175c]:feq.h t6, t5, t4
	-[0x80001760]:csrrs a2, fcsr, zero
	-[0x80001764]:sw t6, 192(fp)
Current Store : [0x80001768] : sw a2, 196(fp) -- Store: [0x800099a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000177c]:feq.h t6, t5, t4
	-[0x80001780]:csrrs a2, fcsr, zero
	-[0x80001784]:sw t6, 200(fp)
Current Store : [0x80001788] : sw a2, 204(fp) -- Store: [0x800099a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000179c]:feq.h t6, t5, t4
	-[0x800017a0]:csrrs a2, fcsr, zero
	-[0x800017a4]:sw t6, 208(fp)
Current Store : [0x800017a8] : sw a2, 212(fp) -- Store: [0x800099b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017bc]:feq.h t6, t5, t4
	-[0x800017c0]:csrrs a2, fcsr, zero
	-[0x800017c4]:sw t6, 216(fp)
Current Store : [0x800017c8] : sw a2, 220(fp) -- Store: [0x800099b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017dc]:feq.h t6, t5, t4
	-[0x800017e0]:csrrs a2, fcsr, zero
	-[0x800017e4]:sw t6, 224(fp)
Current Store : [0x800017e8] : sw a2, 228(fp) -- Store: [0x800099c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800017fc]:feq.h t6, t5, t4
	-[0x80001800]:csrrs a2, fcsr, zero
	-[0x80001804]:sw t6, 232(fp)
Current Store : [0x80001808] : sw a2, 236(fp) -- Store: [0x800099c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000181c]:feq.h t6, t5, t4
	-[0x80001820]:csrrs a2, fcsr, zero
	-[0x80001824]:sw t6, 240(fp)
Current Store : [0x80001828] : sw a2, 244(fp) -- Store: [0x800099d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000183c]:feq.h t6, t5, t4
	-[0x80001840]:csrrs a2, fcsr, zero
	-[0x80001844]:sw t6, 248(fp)
Current Store : [0x80001848] : sw a2, 252(fp) -- Store: [0x800099d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000185c]:feq.h t6, t5, t4
	-[0x80001860]:csrrs a2, fcsr, zero
	-[0x80001864]:sw t6, 256(fp)
Current Store : [0x80001868] : sw a2, 260(fp) -- Store: [0x800099e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000187c]:feq.h t6, t5, t4
	-[0x80001880]:csrrs a2, fcsr, zero
	-[0x80001884]:sw t6, 264(fp)
Current Store : [0x80001888] : sw a2, 268(fp) -- Store: [0x800099e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000189c]:feq.h t6, t5, t4
	-[0x800018a0]:csrrs a2, fcsr, zero
	-[0x800018a4]:sw t6, 272(fp)
Current Store : [0x800018a8] : sw a2, 276(fp) -- Store: [0x800099f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018bc]:feq.h t6, t5, t4
	-[0x800018c0]:csrrs a2, fcsr, zero
	-[0x800018c4]:sw t6, 280(fp)
Current Store : [0x800018c8] : sw a2, 284(fp) -- Store: [0x800099f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018dc]:feq.h t6, t5, t4
	-[0x800018e0]:csrrs a2, fcsr, zero
	-[0x800018e4]:sw t6, 288(fp)
Current Store : [0x800018e8] : sw a2, 292(fp) -- Store: [0x80009a00]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800018fc]:feq.h t6, t5, t4
	-[0x80001900]:csrrs a2, fcsr, zero
	-[0x80001904]:sw t6, 296(fp)
Current Store : [0x80001908] : sw a2, 300(fp) -- Store: [0x80009a08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000191c]:feq.h t6, t5, t4
	-[0x80001920]:csrrs a2, fcsr, zero
	-[0x80001924]:sw t6, 304(fp)
Current Store : [0x80001928] : sw a2, 308(fp) -- Store: [0x80009a10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000193c]:feq.h t6, t5, t4
	-[0x80001940]:csrrs a2, fcsr, zero
	-[0x80001944]:sw t6, 312(fp)
Current Store : [0x80001948] : sw a2, 316(fp) -- Store: [0x80009a18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000195c]:feq.h t6, t5, t4
	-[0x80001960]:csrrs a2, fcsr, zero
	-[0x80001964]:sw t6, 320(fp)
Current Store : [0x80001968] : sw a2, 324(fp) -- Store: [0x80009a20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000197c]:feq.h t6, t5, t4
	-[0x80001980]:csrrs a2, fcsr, zero
	-[0x80001984]:sw t6, 328(fp)
Current Store : [0x80001988] : sw a2, 332(fp) -- Store: [0x80009a28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000199c]:feq.h t6, t5, t4
	-[0x800019a0]:csrrs a2, fcsr, zero
	-[0x800019a4]:sw t6, 336(fp)
Current Store : [0x800019a8] : sw a2, 340(fp) -- Store: [0x80009a30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019bc]:feq.h t6, t5, t4
	-[0x800019c0]:csrrs a2, fcsr, zero
	-[0x800019c4]:sw t6, 344(fp)
Current Store : [0x800019c8] : sw a2, 348(fp) -- Store: [0x80009a38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019dc]:feq.h t6, t5, t4
	-[0x800019e0]:csrrs a2, fcsr, zero
	-[0x800019e4]:sw t6, 352(fp)
Current Store : [0x800019e8] : sw a2, 356(fp) -- Store: [0x80009a40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800019fc]:feq.h t6, t5, t4
	-[0x80001a00]:csrrs a2, fcsr, zero
	-[0x80001a04]:sw t6, 360(fp)
Current Store : [0x80001a08] : sw a2, 364(fp) -- Store: [0x80009a48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a1c]:feq.h t6, t5, t4
	-[0x80001a20]:csrrs a2, fcsr, zero
	-[0x80001a24]:sw t6, 368(fp)
Current Store : [0x80001a28] : sw a2, 372(fp) -- Store: [0x80009a50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a3c]:feq.h t6, t5, t4
	-[0x80001a40]:csrrs a2, fcsr, zero
	-[0x80001a44]:sw t6, 376(fp)
Current Store : [0x80001a48] : sw a2, 380(fp) -- Store: [0x80009a58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a5c]:feq.h t6, t5, t4
	-[0x80001a60]:csrrs a2, fcsr, zero
	-[0x80001a64]:sw t6, 384(fp)
Current Store : [0x80001a68] : sw a2, 388(fp) -- Store: [0x80009a60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a7c]:feq.h t6, t5, t4
	-[0x80001a80]:csrrs a2, fcsr, zero
	-[0x80001a84]:sw t6, 392(fp)
Current Store : [0x80001a88] : sw a2, 396(fp) -- Store: [0x80009a68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001a9c]:feq.h t6, t5, t4
	-[0x80001aa0]:csrrs a2, fcsr, zero
	-[0x80001aa4]:sw t6, 400(fp)
Current Store : [0x80001aa8] : sw a2, 404(fp) -- Store: [0x80009a70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001abc]:feq.h t6, t5, t4
	-[0x80001ac0]:csrrs a2, fcsr, zero
	-[0x80001ac4]:sw t6, 408(fp)
Current Store : [0x80001ac8] : sw a2, 412(fp) -- Store: [0x80009a78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001adc]:feq.h t6, t5, t4
	-[0x80001ae0]:csrrs a2, fcsr, zero
	-[0x80001ae4]:sw t6, 416(fp)
Current Store : [0x80001ae8] : sw a2, 420(fp) -- Store: [0x80009a80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001afc]:feq.h t6, t5, t4
	-[0x80001b00]:csrrs a2, fcsr, zero
	-[0x80001b04]:sw t6, 424(fp)
Current Store : [0x80001b08] : sw a2, 428(fp) -- Store: [0x80009a88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b1c]:feq.h t6, t5, t4
	-[0x80001b20]:csrrs a2, fcsr, zero
	-[0x80001b24]:sw t6, 432(fp)
Current Store : [0x80001b28] : sw a2, 436(fp) -- Store: [0x80009a90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b3c]:feq.h t6, t5, t4
	-[0x80001b40]:csrrs a2, fcsr, zero
	-[0x80001b44]:sw t6, 440(fp)
Current Store : [0x80001b48] : sw a2, 444(fp) -- Store: [0x80009a98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b5c]:feq.h t6, t5, t4
	-[0x80001b60]:csrrs a2, fcsr, zero
	-[0x80001b64]:sw t6, 448(fp)
Current Store : [0x80001b68] : sw a2, 452(fp) -- Store: [0x80009aa0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b7c]:feq.h t6, t5, t4
	-[0x80001b80]:csrrs a2, fcsr, zero
	-[0x80001b84]:sw t6, 456(fp)
Current Store : [0x80001b88] : sw a2, 460(fp) -- Store: [0x80009aa8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001b9c]:feq.h t6, t5, t4
	-[0x80001ba0]:csrrs a2, fcsr, zero
	-[0x80001ba4]:sw t6, 464(fp)
Current Store : [0x80001ba8] : sw a2, 468(fp) -- Store: [0x80009ab0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bbc]:feq.h t6, t5, t4
	-[0x80001bc0]:csrrs a2, fcsr, zero
	-[0x80001bc4]:sw t6, 472(fp)
Current Store : [0x80001bc8] : sw a2, 476(fp) -- Store: [0x80009ab8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bdc]:feq.h t6, t5, t4
	-[0x80001be0]:csrrs a2, fcsr, zero
	-[0x80001be4]:sw t6, 480(fp)
Current Store : [0x80001be8] : sw a2, 484(fp) -- Store: [0x80009ac0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001bfc]:feq.h t6, t5, t4
	-[0x80001c00]:csrrs a2, fcsr, zero
	-[0x80001c04]:sw t6, 488(fp)
Current Store : [0x80001c08] : sw a2, 492(fp) -- Store: [0x80009ac8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c1c]:feq.h t6, t5, t4
	-[0x80001c20]:csrrs a2, fcsr, zero
	-[0x80001c24]:sw t6, 496(fp)
Current Store : [0x80001c28] : sw a2, 500(fp) -- Store: [0x80009ad0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c3c]:feq.h t6, t5, t4
	-[0x80001c40]:csrrs a2, fcsr, zero
	-[0x80001c44]:sw t6, 504(fp)
Current Store : [0x80001c48] : sw a2, 508(fp) -- Store: [0x80009ad8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c5c]:feq.h t6, t5, t4
	-[0x80001c60]:csrrs a2, fcsr, zero
	-[0x80001c64]:sw t6, 512(fp)
Current Store : [0x80001c68] : sw a2, 516(fp) -- Store: [0x80009ae0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c7c]:feq.h t6, t5, t4
	-[0x80001c80]:csrrs a2, fcsr, zero
	-[0x80001c84]:sw t6, 520(fp)
Current Store : [0x80001c88] : sw a2, 524(fp) -- Store: [0x80009ae8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001c9c]:feq.h t6, t5, t4
	-[0x80001ca0]:csrrs a2, fcsr, zero
	-[0x80001ca4]:sw t6, 528(fp)
Current Store : [0x80001ca8] : sw a2, 532(fp) -- Store: [0x80009af0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cbc]:feq.h t6, t5, t4
	-[0x80001cc0]:csrrs a2, fcsr, zero
	-[0x80001cc4]:sw t6, 536(fp)
Current Store : [0x80001cc8] : sw a2, 540(fp) -- Store: [0x80009af8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cdc]:feq.h t6, t5, t4
	-[0x80001ce0]:csrrs a2, fcsr, zero
	-[0x80001ce4]:sw t6, 544(fp)
Current Store : [0x80001ce8] : sw a2, 548(fp) -- Store: [0x80009b00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001cfc]:feq.h t6, t5, t4
	-[0x80001d00]:csrrs a2, fcsr, zero
	-[0x80001d04]:sw t6, 552(fp)
Current Store : [0x80001d08] : sw a2, 556(fp) -- Store: [0x80009b08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d1c]:feq.h t6, t5, t4
	-[0x80001d20]:csrrs a2, fcsr, zero
	-[0x80001d24]:sw t6, 560(fp)
Current Store : [0x80001d28] : sw a2, 564(fp) -- Store: [0x80009b10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d3c]:feq.h t6, t5, t4
	-[0x80001d40]:csrrs a2, fcsr, zero
	-[0x80001d44]:sw t6, 568(fp)
Current Store : [0x80001d48] : sw a2, 572(fp) -- Store: [0x80009b18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d5c]:feq.h t6, t5, t4
	-[0x80001d60]:csrrs a2, fcsr, zero
	-[0x80001d64]:sw t6, 576(fp)
Current Store : [0x80001d68] : sw a2, 580(fp) -- Store: [0x80009b20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d7c]:feq.h t6, t5, t4
	-[0x80001d80]:csrrs a2, fcsr, zero
	-[0x80001d84]:sw t6, 584(fp)
Current Store : [0x80001d88] : sw a2, 588(fp) -- Store: [0x80009b28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001d9c]:feq.h t6, t5, t4
	-[0x80001da0]:csrrs a2, fcsr, zero
	-[0x80001da4]:sw t6, 592(fp)
Current Store : [0x80001da8] : sw a2, 596(fp) -- Store: [0x80009b30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dbc]:feq.h t6, t5, t4
	-[0x80001dc0]:csrrs a2, fcsr, zero
	-[0x80001dc4]:sw t6, 600(fp)
Current Store : [0x80001dc8] : sw a2, 604(fp) -- Store: [0x80009b38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ddc]:feq.h t6, t5, t4
	-[0x80001de0]:csrrs a2, fcsr, zero
	-[0x80001de4]:sw t6, 608(fp)
Current Store : [0x80001de8] : sw a2, 612(fp) -- Store: [0x80009b40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001dfc]:feq.h t6, t5, t4
	-[0x80001e00]:csrrs a2, fcsr, zero
	-[0x80001e04]:sw t6, 616(fp)
Current Store : [0x80001e08] : sw a2, 620(fp) -- Store: [0x80009b48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e1c]:feq.h t6, t5, t4
	-[0x80001e20]:csrrs a2, fcsr, zero
	-[0x80001e24]:sw t6, 624(fp)
Current Store : [0x80001e28] : sw a2, 628(fp) -- Store: [0x80009b50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e3c]:feq.h t6, t5, t4
	-[0x80001e40]:csrrs a2, fcsr, zero
	-[0x80001e44]:sw t6, 632(fp)
Current Store : [0x80001e48] : sw a2, 636(fp) -- Store: [0x80009b58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e5c]:feq.h t6, t5, t4
	-[0x80001e60]:csrrs a2, fcsr, zero
	-[0x80001e64]:sw t6, 640(fp)
Current Store : [0x80001e68] : sw a2, 644(fp) -- Store: [0x80009b60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e7c]:feq.h t6, t5, t4
	-[0x80001e80]:csrrs a2, fcsr, zero
	-[0x80001e84]:sw t6, 648(fp)
Current Store : [0x80001e88] : sw a2, 652(fp) -- Store: [0x80009b68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001e9c]:feq.h t6, t5, t4
	-[0x80001ea0]:csrrs a2, fcsr, zero
	-[0x80001ea4]:sw t6, 656(fp)
Current Store : [0x80001ea8] : sw a2, 660(fp) -- Store: [0x80009b70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ebc]:feq.h t6, t5, t4
	-[0x80001ec0]:csrrs a2, fcsr, zero
	-[0x80001ec4]:sw t6, 664(fp)
Current Store : [0x80001ec8] : sw a2, 668(fp) -- Store: [0x80009b78]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001edc]:feq.h t6, t5, t4
	-[0x80001ee0]:csrrs a2, fcsr, zero
	-[0x80001ee4]:sw t6, 672(fp)
Current Store : [0x80001ee8] : sw a2, 676(fp) -- Store: [0x80009b80]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001efc]:feq.h t6, t5, t4
	-[0x80001f00]:csrrs a2, fcsr, zero
	-[0x80001f04]:sw t6, 680(fp)
Current Store : [0x80001f08] : sw a2, 684(fp) -- Store: [0x80009b88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f1c]:feq.h t6, t5, t4
	-[0x80001f20]:csrrs a2, fcsr, zero
	-[0x80001f24]:sw t6, 688(fp)
Current Store : [0x80001f28] : sw a2, 692(fp) -- Store: [0x80009b90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f3c]:feq.h t6, t5, t4
	-[0x80001f40]:csrrs a2, fcsr, zero
	-[0x80001f44]:sw t6, 696(fp)
Current Store : [0x80001f48] : sw a2, 700(fp) -- Store: [0x80009b98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f5c]:feq.h t6, t5, t4
	-[0x80001f60]:csrrs a2, fcsr, zero
	-[0x80001f64]:sw t6, 704(fp)
Current Store : [0x80001f68] : sw a2, 708(fp) -- Store: [0x80009ba0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f7c]:feq.h t6, t5, t4
	-[0x80001f80]:csrrs a2, fcsr, zero
	-[0x80001f84]:sw t6, 712(fp)
Current Store : [0x80001f88] : sw a2, 716(fp) -- Store: [0x80009ba8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001f9c]:feq.h t6, t5, t4
	-[0x80001fa0]:csrrs a2, fcsr, zero
	-[0x80001fa4]:sw t6, 720(fp)
Current Store : [0x80001fa8] : sw a2, 724(fp) -- Store: [0x80009bb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fbc]:feq.h t6, t5, t4
	-[0x80001fc0]:csrrs a2, fcsr, zero
	-[0x80001fc4]:sw t6, 728(fp)
Current Store : [0x80001fc8] : sw a2, 732(fp) -- Store: [0x80009bb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001fdc]:feq.h t6, t5, t4
	-[0x80001fe0]:csrrs a2, fcsr, zero
	-[0x80001fe4]:sw t6, 736(fp)
Current Store : [0x80001fe8] : sw a2, 740(fp) -- Store: [0x80009bc0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80001ffc]:feq.h t6, t5, t4
	-[0x80002000]:csrrs a2, fcsr, zero
	-[0x80002004]:sw t6, 744(fp)
Current Store : [0x80002008] : sw a2, 748(fp) -- Store: [0x80009bc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000201c]:feq.h t6, t5, t4
	-[0x80002020]:csrrs a2, fcsr, zero
	-[0x80002024]:sw t6, 752(fp)
Current Store : [0x80002028] : sw a2, 756(fp) -- Store: [0x80009bd0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000203c]:feq.h t6, t5, t4
	-[0x80002040]:csrrs a2, fcsr, zero
	-[0x80002044]:sw t6, 760(fp)
Current Store : [0x80002048] : sw a2, 764(fp) -- Store: [0x80009bd8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000205c]:feq.h t6, t5, t4
	-[0x80002060]:csrrs a2, fcsr, zero
	-[0x80002064]:sw t6, 768(fp)
Current Store : [0x80002068] : sw a2, 772(fp) -- Store: [0x80009be0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000207c]:feq.h t6, t5, t4
	-[0x80002080]:csrrs a2, fcsr, zero
	-[0x80002084]:sw t6, 776(fp)
Current Store : [0x80002088] : sw a2, 780(fp) -- Store: [0x80009be8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000209c]:feq.h t6, t5, t4
	-[0x800020a0]:csrrs a2, fcsr, zero
	-[0x800020a4]:sw t6, 784(fp)
Current Store : [0x800020a8] : sw a2, 788(fp) -- Store: [0x80009bf0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020bc]:feq.h t6, t5, t4
	-[0x800020c0]:csrrs a2, fcsr, zero
	-[0x800020c4]:sw t6, 792(fp)
Current Store : [0x800020c8] : sw a2, 796(fp) -- Store: [0x80009bf8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020dc]:feq.h t6, t5, t4
	-[0x800020e0]:csrrs a2, fcsr, zero
	-[0x800020e4]:sw t6, 800(fp)
Current Store : [0x800020e8] : sw a2, 804(fp) -- Store: [0x80009c00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800020fc]:feq.h t6, t5, t4
	-[0x80002100]:csrrs a2, fcsr, zero
	-[0x80002104]:sw t6, 808(fp)
Current Store : [0x80002108] : sw a2, 812(fp) -- Store: [0x80009c08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000211c]:feq.h t6, t5, t4
	-[0x80002120]:csrrs a2, fcsr, zero
	-[0x80002124]:sw t6, 816(fp)
Current Store : [0x80002128] : sw a2, 820(fp) -- Store: [0x80009c10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000213c]:feq.h t6, t5, t4
	-[0x80002140]:csrrs a2, fcsr, zero
	-[0x80002144]:sw t6, 824(fp)
Current Store : [0x80002148] : sw a2, 828(fp) -- Store: [0x80009c18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000215c]:feq.h t6, t5, t4
	-[0x80002160]:csrrs a2, fcsr, zero
	-[0x80002164]:sw t6, 832(fp)
Current Store : [0x80002168] : sw a2, 836(fp) -- Store: [0x80009c20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000217c]:feq.h t6, t5, t4
	-[0x80002180]:csrrs a2, fcsr, zero
	-[0x80002184]:sw t6, 840(fp)
Current Store : [0x80002188] : sw a2, 844(fp) -- Store: [0x80009c28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000219c]:feq.h t6, t5, t4
	-[0x800021a0]:csrrs a2, fcsr, zero
	-[0x800021a4]:sw t6, 848(fp)
Current Store : [0x800021a8] : sw a2, 852(fp) -- Store: [0x80009c30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021bc]:feq.h t6, t5, t4
	-[0x800021c0]:csrrs a2, fcsr, zero
	-[0x800021c4]:sw t6, 856(fp)
Current Store : [0x800021c8] : sw a2, 860(fp) -- Store: [0x80009c38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021dc]:feq.h t6, t5, t4
	-[0x800021e0]:csrrs a2, fcsr, zero
	-[0x800021e4]:sw t6, 864(fp)
Current Store : [0x800021e8] : sw a2, 868(fp) -- Store: [0x80009c40]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800021fc]:feq.h t6, t5, t4
	-[0x80002200]:csrrs a2, fcsr, zero
	-[0x80002204]:sw t6, 872(fp)
Current Store : [0x80002208] : sw a2, 876(fp) -- Store: [0x80009c48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000221c]:feq.h t6, t5, t4
	-[0x80002220]:csrrs a2, fcsr, zero
	-[0x80002224]:sw t6, 880(fp)
Current Store : [0x80002228] : sw a2, 884(fp) -- Store: [0x80009c50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000223c]:feq.h t6, t5, t4
	-[0x80002240]:csrrs a2, fcsr, zero
	-[0x80002244]:sw t6, 888(fp)
Current Store : [0x80002248] : sw a2, 892(fp) -- Store: [0x80009c58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000225c]:feq.h t6, t5, t4
	-[0x80002260]:csrrs a2, fcsr, zero
	-[0x80002264]:sw t6, 896(fp)
Current Store : [0x80002268] : sw a2, 900(fp) -- Store: [0x80009c60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000227c]:feq.h t6, t5, t4
	-[0x80002280]:csrrs a2, fcsr, zero
	-[0x80002284]:sw t6, 904(fp)
Current Store : [0x80002288] : sw a2, 908(fp) -- Store: [0x80009c68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000229c]:feq.h t6, t5, t4
	-[0x800022a0]:csrrs a2, fcsr, zero
	-[0x800022a4]:sw t6, 912(fp)
Current Store : [0x800022a8] : sw a2, 916(fp) -- Store: [0x80009c70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022bc]:feq.h t6, t5, t4
	-[0x800022c0]:csrrs a2, fcsr, zero
	-[0x800022c4]:sw t6, 920(fp)
Current Store : [0x800022c8] : sw a2, 924(fp) -- Store: [0x80009c78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022dc]:feq.h t6, t5, t4
	-[0x800022e0]:csrrs a2, fcsr, zero
	-[0x800022e4]:sw t6, 928(fp)
Current Store : [0x800022e8] : sw a2, 932(fp) -- Store: [0x80009c80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800022fc]:feq.h t6, t5, t4
	-[0x80002300]:csrrs a2, fcsr, zero
	-[0x80002304]:sw t6, 936(fp)
Current Store : [0x80002308] : sw a2, 940(fp) -- Store: [0x80009c88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000231c]:feq.h t6, t5, t4
	-[0x80002320]:csrrs a2, fcsr, zero
	-[0x80002324]:sw t6, 944(fp)
Current Store : [0x80002328] : sw a2, 948(fp) -- Store: [0x80009c90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000233c]:feq.h t6, t5, t4
	-[0x80002340]:csrrs a2, fcsr, zero
	-[0x80002344]:sw t6, 952(fp)
Current Store : [0x80002348] : sw a2, 956(fp) -- Store: [0x80009c98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000235c]:feq.h t6, t5, t4
	-[0x80002360]:csrrs a2, fcsr, zero
	-[0x80002364]:sw t6, 960(fp)
Current Store : [0x80002368] : sw a2, 964(fp) -- Store: [0x80009ca0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000237c]:feq.h t6, t5, t4
	-[0x80002380]:csrrs a2, fcsr, zero
	-[0x80002384]:sw t6, 968(fp)
Current Store : [0x80002388] : sw a2, 972(fp) -- Store: [0x80009ca8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000239c]:feq.h t6, t5, t4
	-[0x800023a0]:csrrs a2, fcsr, zero
	-[0x800023a4]:sw t6, 976(fp)
Current Store : [0x800023a8] : sw a2, 980(fp) -- Store: [0x80009cb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023bc]:feq.h t6, t5, t4
	-[0x800023c0]:csrrs a2, fcsr, zero
	-[0x800023c4]:sw t6, 984(fp)
Current Store : [0x800023c8] : sw a2, 988(fp) -- Store: [0x80009cb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800023dc]:feq.h t6, t5, t4
	-[0x800023e0]:csrrs a2, fcsr, zero
	-[0x800023e4]:sw t6, 992(fp)
Current Store : [0x800023e8] : sw a2, 996(fp) -- Store: [0x80009cc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000241c]:feq.h t6, t5, t4
	-[0x80002420]:csrrs a2, fcsr, zero
	-[0x80002424]:sw t6, 1000(fp)
Current Store : [0x80002428] : sw a2, 1004(fp) -- Store: [0x80009cc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000245c]:feq.h t6, t5, t4
	-[0x80002460]:csrrs a2, fcsr, zero
	-[0x80002464]:sw t6, 1008(fp)
Current Store : [0x80002468] : sw a2, 1012(fp) -- Store: [0x80009cd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000249c]:feq.h t6, t5, t4
	-[0x800024a0]:csrrs a2, fcsr, zero
	-[0x800024a4]:sw t6, 1016(fp)
Current Store : [0x800024a8] : sw a2, 1020(fp) -- Store: [0x80009cd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800024e4]:feq.h t6, t5, t4
	-[0x800024e8]:csrrs a2, fcsr, zero
	-[0x800024ec]:sw t6, 0(fp)
Current Store : [0x800024f0] : sw a2, 4(fp) -- Store: [0x80009ce0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002524]:feq.h t6, t5, t4
	-[0x80002528]:csrrs a2, fcsr, zero
	-[0x8000252c]:sw t6, 8(fp)
Current Store : [0x80002530] : sw a2, 12(fp) -- Store: [0x80009ce8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002564]:feq.h t6, t5, t4
	-[0x80002568]:csrrs a2, fcsr, zero
	-[0x8000256c]:sw t6, 16(fp)
Current Store : [0x80002570] : sw a2, 20(fp) -- Store: [0x80009cf0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025a4]:feq.h t6, t5, t4
	-[0x800025a8]:csrrs a2, fcsr, zero
	-[0x800025ac]:sw t6, 24(fp)
Current Store : [0x800025b0] : sw a2, 28(fp) -- Store: [0x80009cf8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800025e4]:feq.h t6, t5, t4
	-[0x800025e8]:csrrs a2, fcsr, zero
	-[0x800025ec]:sw t6, 32(fp)
Current Store : [0x800025f0] : sw a2, 36(fp) -- Store: [0x80009d00]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002624]:feq.h t6, t5, t4
	-[0x80002628]:csrrs a2, fcsr, zero
	-[0x8000262c]:sw t6, 40(fp)
Current Store : [0x80002630] : sw a2, 44(fp) -- Store: [0x80009d08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002664]:feq.h t6, t5, t4
	-[0x80002668]:csrrs a2, fcsr, zero
	-[0x8000266c]:sw t6, 48(fp)
Current Store : [0x80002670] : sw a2, 52(fp) -- Store: [0x80009d10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026a4]:feq.h t6, t5, t4
	-[0x800026a8]:csrrs a2, fcsr, zero
	-[0x800026ac]:sw t6, 56(fp)
Current Store : [0x800026b0] : sw a2, 60(fp) -- Store: [0x80009d18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800026e4]:feq.h t6, t5, t4
	-[0x800026e8]:csrrs a2, fcsr, zero
	-[0x800026ec]:sw t6, 64(fp)
Current Store : [0x800026f0] : sw a2, 68(fp) -- Store: [0x80009d20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002724]:feq.h t6, t5, t4
	-[0x80002728]:csrrs a2, fcsr, zero
	-[0x8000272c]:sw t6, 72(fp)
Current Store : [0x80002730] : sw a2, 76(fp) -- Store: [0x80009d28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002764]:feq.h t6, t5, t4
	-[0x80002768]:csrrs a2, fcsr, zero
	-[0x8000276c]:sw t6, 80(fp)
Current Store : [0x80002770] : sw a2, 84(fp) -- Store: [0x80009d30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027a4]:feq.h t6, t5, t4
	-[0x800027a8]:csrrs a2, fcsr, zero
	-[0x800027ac]:sw t6, 88(fp)
Current Store : [0x800027b0] : sw a2, 92(fp) -- Store: [0x80009d38]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800027e4]:feq.h t6, t5, t4
	-[0x800027e8]:csrrs a2, fcsr, zero
	-[0x800027ec]:sw t6, 96(fp)
Current Store : [0x800027f0] : sw a2, 100(fp) -- Store: [0x80009d40]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002824]:feq.h t6, t5, t4
	-[0x80002828]:csrrs a2, fcsr, zero
	-[0x8000282c]:sw t6, 104(fp)
Current Store : [0x80002830] : sw a2, 108(fp) -- Store: [0x80009d48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002864]:feq.h t6, t5, t4
	-[0x80002868]:csrrs a2, fcsr, zero
	-[0x8000286c]:sw t6, 112(fp)
Current Store : [0x80002870] : sw a2, 116(fp) -- Store: [0x80009d50]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028a4]:feq.h t6, t5, t4
	-[0x800028a8]:csrrs a2, fcsr, zero
	-[0x800028ac]:sw t6, 120(fp)
Current Store : [0x800028b0] : sw a2, 124(fp) -- Store: [0x80009d58]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800028e4]:feq.h t6, t5, t4
	-[0x800028e8]:csrrs a2, fcsr, zero
	-[0x800028ec]:sw t6, 128(fp)
Current Store : [0x800028f0] : sw a2, 132(fp) -- Store: [0x80009d60]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002924]:feq.h t6, t5, t4
	-[0x80002928]:csrrs a2, fcsr, zero
	-[0x8000292c]:sw t6, 136(fp)
Current Store : [0x80002930] : sw a2, 140(fp) -- Store: [0x80009d68]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002964]:feq.h t6, t5, t4
	-[0x80002968]:csrrs a2, fcsr, zero
	-[0x8000296c]:sw t6, 144(fp)
Current Store : [0x80002970] : sw a2, 148(fp) -- Store: [0x80009d70]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029a4]:feq.h t6, t5, t4
	-[0x800029a8]:csrrs a2, fcsr, zero
	-[0x800029ac]:sw t6, 152(fp)
Current Store : [0x800029b0] : sw a2, 156(fp) -- Store: [0x80009d78]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800029e4]:feq.h t6, t5, t4
	-[0x800029e8]:csrrs a2, fcsr, zero
	-[0x800029ec]:sw t6, 160(fp)
Current Store : [0x800029f0] : sw a2, 164(fp) -- Store: [0x80009d80]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a24]:feq.h t6, t5, t4
	-[0x80002a28]:csrrs a2, fcsr, zero
	-[0x80002a2c]:sw t6, 168(fp)
Current Store : [0x80002a30] : sw a2, 172(fp) -- Store: [0x80009d88]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002a64]:feq.h t6, t5, t4
	-[0x80002a68]:csrrs a2, fcsr, zero
	-[0x80002a6c]:sw t6, 176(fp)
Current Store : [0x80002a70] : sw a2, 180(fp) -- Store: [0x80009d90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002aa4]:feq.h t6, t5, t4
	-[0x80002aa8]:csrrs a2, fcsr, zero
	-[0x80002aac]:sw t6, 184(fp)
Current Store : [0x80002ab0] : sw a2, 188(fp) -- Store: [0x80009d98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ae4]:feq.h t6, t5, t4
	-[0x80002ae8]:csrrs a2, fcsr, zero
	-[0x80002aec]:sw t6, 192(fp)
Current Store : [0x80002af0] : sw a2, 196(fp) -- Store: [0x80009da0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b24]:feq.h t6, t5, t4
	-[0x80002b28]:csrrs a2, fcsr, zero
	-[0x80002b2c]:sw t6, 200(fp)
Current Store : [0x80002b30] : sw a2, 204(fp) -- Store: [0x80009da8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002b64]:feq.h t6, t5, t4
	-[0x80002b68]:csrrs a2, fcsr, zero
	-[0x80002b6c]:sw t6, 208(fp)
Current Store : [0x80002b70] : sw a2, 212(fp) -- Store: [0x80009db0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ba4]:feq.h t6, t5, t4
	-[0x80002ba8]:csrrs a2, fcsr, zero
	-[0x80002bac]:sw t6, 216(fp)
Current Store : [0x80002bb0] : sw a2, 220(fp) -- Store: [0x80009db8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002be4]:feq.h t6, t5, t4
	-[0x80002be8]:csrrs a2, fcsr, zero
	-[0x80002bec]:sw t6, 224(fp)
Current Store : [0x80002bf0] : sw a2, 228(fp) -- Store: [0x80009dc0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c24]:feq.h t6, t5, t4
	-[0x80002c28]:csrrs a2, fcsr, zero
	-[0x80002c2c]:sw t6, 232(fp)
Current Store : [0x80002c30] : sw a2, 236(fp) -- Store: [0x80009dc8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002c64]:feq.h t6, t5, t4
	-[0x80002c68]:csrrs a2, fcsr, zero
	-[0x80002c6c]:sw t6, 240(fp)
Current Store : [0x80002c70] : sw a2, 244(fp) -- Store: [0x80009dd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ca4]:feq.h t6, t5, t4
	-[0x80002ca8]:csrrs a2, fcsr, zero
	-[0x80002cac]:sw t6, 248(fp)
Current Store : [0x80002cb0] : sw a2, 252(fp) -- Store: [0x80009dd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ce4]:feq.h t6, t5, t4
	-[0x80002ce8]:csrrs a2, fcsr, zero
	-[0x80002cec]:sw t6, 256(fp)
Current Store : [0x80002cf0] : sw a2, 260(fp) -- Store: [0x80009de0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d24]:feq.h t6, t5, t4
	-[0x80002d28]:csrrs a2, fcsr, zero
	-[0x80002d2c]:sw t6, 264(fp)
Current Store : [0x80002d30] : sw a2, 268(fp) -- Store: [0x80009de8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002d64]:feq.h t6, t5, t4
	-[0x80002d68]:csrrs a2, fcsr, zero
	-[0x80002d6c]:sw t6, 272(fp)
Current Store : [0x80002d70] : sw a2, 276(fp) -- Store: [0x80009df0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002da4]:feq.h t6, t5, t4
	-[0x80002da8]:csrrs a2, fcsr, zero
	-[0x80002dac]:sw t6, 280(fp)
Current Store : [0x80002db0] : sw a2, 284(fp) -- Store: [0x80009df8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002de4]:feq.h t6, t5, t4
	-[0x80002de8]:csrrs a2, fcsr, zero
	-[0x80002dec]:sw t6, 288(fp)
Current Store : [0x80002df0] : sw a2, 292(fp) -- Store: [0x80009e00]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e24]:feq.h t6, t5, t4
	-[0x80002e28]:csrrs a2, fcsr, zero
	-[0x80002e2c]:sw t6, 296(fp)
Current Store : [0x80002e30] : sw a2, 300(fp) -- Store: [0x80009e08]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002e64]:feq.h t6, t5, t4
	-[0x80002e68]:csrrs a2, fcsr, zero
	-[0x80002e6c]:sw t6, 304(fp)
Current Store : [0x80002e70] : sw a2, 308(fp) -- Store: [0x80009e10]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ea4]:feq.h t6, t5, t4
	-[0x80002ea8]:csrrs a2, fcsr, zero
	-[0x80002eac]:sw t6, 312(fp)
Current Store : [0x80002eb0] : sw a2, 316(fp) -- Store: [0x80009e18]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002ee4]:feq.h t6, t5, t4
	-[0x80002ee8]:csrrs a2, fcsr, zero
	-[0x80002eec]:sw t6, 320(fp)
Current Store : [0x80002ef0] : sw a2, 324(fp) -- Store: [0x80009e20]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f24]:feq.h t6, t5, t4
	-[0x80002f28]:csrrs a2, fcsr, zero
	-[0x80002f2c]:sw t6, 328(fp)
Current Store : [0x80002f30] : sw a2, 332(fp) -- Store: [0x80009e28]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002f64]:feq.h t6, t5, t4
	-[0x80002f68]:csrrs a2, fcsr, zero
	-[0x80002f6c]:sw t6, 336(fp)
Current Store : [0x80002f70] : sw a2, 340(fp) -- Store: [0x80009e30]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fa4]:feq.h t6, t5, t4
	-[0x80002fa8]:csrrs a2, fcsr, zero
	-[0x80002fac]:sw t6, 344(fp)
Current Store : [0x80002fb0] : sw a2, 348(fp) -- Store: [0x80009e38]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80002fe4]:feq.h t6, t5, t4
	-[0x80002fe8]:csrrs a2, fcsr, zero
	-[0x80002fec]:sw t6, 352(fp)
Current Store : [0x80002ff0] : sw a2, 356(fp) -- Store: [0x80009e40]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003024]:feq.h t6, t5, t4
	-[0x80003028]:csrrs a2, fcsr, zero
	-[0x8000302c]:sw t6, 360(fp)
Current Store : [0x80003030] : sw a2, 364(fp) -- Store: [0x80009e48]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003064]:feq.h t6, t5, t4
	-[0x80003068]:csrrs a2, fcsr, zero
	-[0x8000306c]:sw t6, 368(fp)
Current Store : [0x80003070] : sw a2, 372(fp) -- Store: [0x80009e50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030a4]:feq.h t6, t5, t4
	-[0x800030a8]:csrrs a2, fcsr, zero
	-[0x800030ac]:sw t6, 376(fp)
Current Store : [0x800030b0] : sw a2, 380(fp) -- Store: [0x80009e58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800030e4]:feq.h t6, t5, t4
	-[0x800030e8]:csrrs a2, fcsr, zero
	-[0x800030ec]:sw t6, 384(fp)
Current Store : [0x800030f0] : sw a2, 388(fp) -- Store: [0x80009e60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003124]:feq.h t6, t5, t4
	-[0x80003128]:csrrs a2, fcsr, zero
	-[0x8000312c]:sw t6, 392(fp)
Current Store : [0x80003130] : sw a2, 396(fp) -- Store: [0x80009e68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003164]:feq.h t6, t5, t4
	-[0x80003168]:csrrs a2, fcsr, zero
	-[0x8000316c]:sw t6, 400(fp)
Current Store : [0x80003170] : sw a2, 404(fp) -- Store: [0x80009e70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031a4]:feq.h t6, t5, t4
	-[0x800031a8]:csrrs a2, fcsr, zero
	-[0x800031ac]:sw t6, 408(fp)
Current Store : [0x800031b0] : sw a2, 412(fp) -- Store: [0x80009e78]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800031e4]:feq.h t6, t5, t4
	-[0x800031e8]:csrrs a2, fcsr, zero
	-[0x800031ec]:sw t6, 416(fp)
Current Store : [0x800031f0] : sw a2, 420(fp) -- Store: [0x80009e80]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003224]:feq.h t6, t5, t4
	-[0x80003228]:csrrs a2, fcsr, zero
	-[0x8000322c]:sw t6, 424(fp)
Current Store : [0x80003230] : sw a2, 428(fp) -- Store: [0x80009e88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003264]:feq.h t6, t5, t4
	-[0x80003268]:csrrs a2, fcsr, zero
	-[0x8000326c]:sw t6, 432(fp)
Current Store : [0x80003270] : sw a2, 436(fp) -- Store: [0x80009e90]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032a4]:feq.h t6, t5, t4
	-[0x800032a8]:csrrs a2, fcsr, zero
	-[0x800032ac]:sw t6, 440(fp)
Current Store : [0x800032b0] : sw a2, 444(fp) -- Store: [0x80009e98]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800032e4]:feq.h t6, t5, t4
	-[0x800032e8]:csrrs a2, fcsr, zero
	-[0x800032ec]:sw t6, 448(fp)
Current Store : [0x800032f0] : sw a2, 452(fp) -- Store: [0x80009ea0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003324]:feq.h t6, t5, t4
	-[0x80003328]:csrrs a2, fcsr, zero
	-[0x8000332c]:sw t6, 456(fp)
Current Store : [0x80003330] : sw a2, 460(fp) -- Store: [0x80009ea8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003364]:feq.h t6, t5, t4
	-[0x80003368]:csrrs a2, fcsr, zero
	-[0x8000336c]:sw t6, 464(fp)
Current Store : [0x80003370] : sw a2, 468(fp) -- Store: [0x80009eb0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033a4]:feq.h t6, t5, t4
	-[0x800033a8]:csrrs a2, fcsr, zero
	-[0x800033ac]:sw t6, 472(fp)
Current Store : [0x800033b0] : sw a2, 476(fp) -- Store: [0x80009eb8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800033e4]:feq.h t6, t5, t4
	-[0x800033e8]:csrrs a2, fcsr, zero
	-[0x800033ec]:sw t6, 480(fp)
Current Store : [0x800033f0] : sw a2, 484(fp) -- Store: [0x80009ec0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003424]:feq.h t6, t5, t4
	-[0x80003428]:csrrs a2, fcsr, zero
	-[0x8000342c]:sw t6, 488(fp)
Current Store : [0x80003430] : sw a2, 492(fp) -- Store: [0x80009ec8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003464]:feq.h t6, t5, t4
	-[0x80003468]:csrrs a2, fcsr, zero
	-[0x8000346c]:sw t6, 496(fp)
Current Store : [0x80003470] : sw a2, 500(fp) -- Store: [0x80009ed0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034a4]:feq.h t6, t5, t4
	-[0x800034a8]:csrrs a2, fcsr, zero
	-[0x800034ac]:sw t6, 504(fp)
Current Store : [0x800034b0] : sw a2, 508(fp) -- Store: [0x80009ed8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800034e4]:feq.h t6, t5, t4
	-[0x800034e8]:csrrs a2, fcsr, zero
	-[0x800034ec]:sw t6, 512(fp)
Current Store : [0x800034f0] : sw a2, 516(fp) -- Store: [0x80009ee0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003524]:feq.h t6, t5, t4
	-[0x80003528]:csrrs a2, fcsr, zero
	-[0x8000352c]:sw t6, 520(fp)
Current Store : [0x80003530] : sw a2, 524(fp) -- Store: [0x80009ee8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003564]:feq.h t6, t5, t4
	-[0x80003568]:csrrs a2, fcsr, zero
	-[0x8000356c]:sw t6, 528(fp)
Current Store : [0x80003570] : sw a2, 532(fp) -- Store: [0x80009ef0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035a4]:feq.h t6, t5, t4
	-[0x800035a8]:csrrs a2, fcsr, zero
	-[0x800035ac]:sw t6, 536(fp)
Current Store : [0x800035b0] : sw a2, 540(fp) -- Store: [0x80009ef8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800035e4]:feq.h t6, t5, t4
	-[0x800035e8]:csrrs a2, fcsr, zero
	-[0x800035ec]:sw t6, 544(fp)
Current Store : [0x800035f0] : sw a2, 548(fp) -- Store: [0x80009f00]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003624]:feq.h t6, t5, t4
	-[0x80003628]:csrrs a2, fcsr, zero
	-[0x8000362c]:sw t6, 552(fp)
Current Store : [0x80003630] : sw a2, 556(fp) -- Store: [0x80009f08]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003664]:feq.h t6, t5, t4
	-[0x80003668]:csrrs a2, fcsr, zero
	-[0x8000366c]:sw t6, 560(fp)
Current Store : [0x80003670] : sw a2, 564(fp) -- Store: [0x80009f10]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036a4]:feq.h t6, t5, t4
	-[0x800036a8]:csrrs a2, fcsr, zero
	-[0x800036ac]:sw t6, 568(fp)
Current Store : [0x800036b0] : sw a2, 572(fp) -- Store: [0x80009f18]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800036e4]:feq.h t6, t5, t4
	-[0x800036e8]:csrrs a2, fcsr, zero
	-[0x800036ec]:sw t6, 576(fp)
Current Store : [0x800036f0] : sw a2, 580(fp) -- Store: [0x80009f20]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003724]:feq.h t6, t5, t4
	-[0x80003728]:csrrs a2, fcsr, zero
	-[0x8000372c]:sw t6, 584(fp)
Current Store : [0x80003730] : sw a2, 588(fp) -- Store: [0x80009f28]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003764]:feq.h t6, t5, t4
	-[0x80003768]:csrrs a2, fcsr, zero
	-[0x8000376c]:sw t6, 592(fp)
Current Store : [0x80003770] : sw a2, 596(fp) -- Store: [0x80009f30]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037a4]:feq.h t6, t5, t4
	-[0x800037a8]:csrrs a2, fcsr, zero
	-[0x800037ac]:sw t6, 600(fp)
Current Store : [0x800037b0] : sw a2, 604(fp) -- Store: [0x80009f38]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800037e4]:feq.h t6, t5, t4
	-[0x800037e8]:csrrs a2, fcsr, zero
	-[0x800037ec]:sw t6, 608(fp)
Current Store : [0x800037f0] : sw a2, 612(fp) -- Store: [0x80009f40]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003824]:feq.h t6, t5, t4
	-[0x80003828]:csrrs a2, fcsr, zero
	-[0x8000382c]:sw t6, 616(fp)
Current Store : [0x80003830] : sw a2, 620(fp) -- Store: [0x80009f48]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003864]:feq.h t6, t5, t4
	-[0x80003868]:csrrs a2, fcsr, zero
	-[0x8000386c]:sw t6, 624(fp)
Current Store : [0x80003870] : sw a2, 628(fp) -- Store: [0x80009f50]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038a4]:feq.h t6, t5, t4
	-[0x800038a8]:csrrs a2, fcsr, zero
	-[0x800038ac]:sw t6, 632(fp)
Current Store : [0x800038b0] : sw a2, 636(fp) -- Store: [0x80009f58]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800038e4]:feq.h t6, t5, t4
	-[0x800038e8]:csrrs a2, fcsr, zero
	-[0x800038ec]:sw t6, 640(fp)
Current Store : [0x800038f0] : sw a2, 644(fp) -- Store: [0x80009f60]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003924]:feq.h t6, t5, t4
	-[0x80003928]:csrrs a2, fcsr, zero
	-[0x8000392c]:sw t6, 648(fp)
Current Store : [0x80003930] : sw a2, 652(fp) -- Store: [0x80009f68]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003964]:feq.h t6, t5, t4
	-[0x80003968]:csrrs a2, fcsr, zero
	-[0x8000396c]:sw t6, 656(fp)
Current Store : [0x80003970] : sw a2, 660(fp) -- Store: [0x80009f70]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039a4]:feq.h t6, t5, t4
	-[0x800039a8]:csrrs a2, fcsr, zero
	-[0x800039ac]:sw t6, 664(fp)
Current Store : [0x800039b0] : sw a2, 668(fp) -- Store: [0x80009f78]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800039e4]:feq.h t6, t5, t4
	-[0x800039e8]:csrrs a2, fcsr, zero
	-[0x800039ec]:sw t6, 672(fp)
Current Store : [0x800039f0] : sw a2, 676(fp) -- Store: [0x80009f80]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a24]:feq.h t6, t5, t4
	-[0x80003a28]:csrrs a2, fcsr, zero
	-[0x80003a2c]:sw t6, 680(fp)
Current Store : [0x80003a30] : sw a2, 684(fp) -- Store: [0x80009f88]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003a64]:feq.h t6, t5, t4
	-[0x80003a68]:csrrs a2, fcsr, zero
	-[0x80003a6c]:sw t6, 688(fp)
Current Store : [0x80003a70] : sw a2, 692(fp) -- Store: [0x80009f90]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003aa4]:feq.h t6, t5, t4
	-[0x80003aa8]:csrrs a2, fcsr, zero
	-[0x80003aac]:sw t6, 696(fp)
Current Store : [0x80003ab0] : sw a2, 700(fp) -- Store: [0x80009f98]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ae4]:feq.h t6, t5, t4
	-[0x80003ae8]:csrrs a2, fcsr, zero
	-[0x80003aec]:sw t6, 704(fp)
Current Store : [0x80003af0] : sw a2, 708(fp) -- Store: [0x80009fa0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b24]:feq.h t6, t5, t4
	-[0x80003b28]:csrrs a2, fcsr, zero
	-[0x80003b2c]:sw t6, 712(fp)
Current Store : [0x80003b30] : sw a2, 716(fp) -- Store: [0x80009fa8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003b64]:feq.h t6, t5, t4
	-[0x80003b68]:csrrs a2, fcsr, zero
	-[0x80003b6c]:sw t6, 720(fp)
Current Store : [0x80003b70] : sw a2, 724(fp) -- Store: [0x80009fb0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ba4]:feq.h t6, t5, t4
	-[0x80003ba8]:csrrs a2, fcsr, zero
	-[0x80003bac]:sw t6, 728(fp)
Current Store : [0x80003bb0] : sw a2, 732(fp) -- Store: [0x80009fb8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003be4]:feq.h t6, t5, t4
	-[0x80003be8]:csrrs a2, fcsr, zero
	-[0x80003bec]:sw t6, 736(fp)
Current Store : [0x80003bf0] : sw a2, 740(fp) -- Store: [0x80009fc0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c24]:feq.h t6, t5, t4
	-[0x80003c28]:csrrs a2, fcsr, zero
	-[0x80003c2c]:sw t6, 744(fp)
Current Store : [0x80003c30] : sw a2, 748(fp) -- Store: [0x80009fc8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003c64]:feq.h t6, t5, t4
	-[0x80003c68]:csrrs a2, fcsr, zero
	-[0x80003c6c]:sw t6, 752(fp)
Current Store : [0x80003c70] : sw a2, 756(fp) -- Store: [0x80009fd0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ca4]:feq.h t6, t5, t4
	-[0x80003ca8]:csrrs a2, fcsr, zero
	-[0x80003cac]:sw t6, 760(fp)
Current Store : [0x80003cb0] : sw a2, 764(fp) -- Store: [0x80009fd8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ce4]:feq.h t6, t5, t4
	-[0x80003ce8]:csrrs a2, fcsr, zero
	-[0x80003cec]:sw t6, 768(fp)
Current Store : [0x80003cf0] : sw a2, 772(fp) -- Store: [0x80009fe0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d24]:feq.h t6, t5, t4
	-[0x80003d28]:csrrs a2, fcsr, zero
	-[0x80003d2c]:sw t6, 776(fp)
Current Store : [0x80003d30] : sw a2, 780(fp) -- Store: [0x80009fe8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003d64]:feq.h t6, t5, t4
	-[0x80003d68]:csrrs a2, fcsr, zero
	-[0x80003d6c]:sw t6, 784(fp)
Current Store : [0x80003d70] : sw a2, 788(fp) -- Store: [0x80009ff0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003da4]:feq.h t6, t5, t4
	-[0x80003da8]:csrrs a2, fcsr, zero
	-[0x80003dac]:sw t6, 792(fp)
Current Store : [0x80003db0] : sw a2, 796(fp) -- Store: [0x80009ff8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003de4]:feq.h t6, t5, t4
	-[0x80003de8]:csrrs a2, fcsr, zero
	-[0x80003dec]:sw t6, 800(fp)
Current Store : [0x80003df0] : sw a2, 804(fp) -- Store: [0x8000a000]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e24]:feq.h t6, t5, t4
	-[0x80003e28]:csrrs a2, fcsr, zero
	-[0x80003e2c]:sw t6, 808(fp)
Current Store : [0x80003e30] : sw a2, 812(fp) -- Store: [0x8000a008]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003e64]:feq.h t6, t5, t4
	-[0x80003e68]:csrrs a2, fcsr, zero
	-[0x80003e6c]:sw t6, 816(fp)
Current Store : [0x80003e70] : sw a2, 820(fp) -- Store: [0x8000a010]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ea4]:feq.h t6, t5, t4
	-[0x80003ea8]:csrrs a2, fcsr, zero
	-[0x80003eac]:sw t6, 824(fp)
Current Store : [0x80003eb0] : sw a2, 828(fp) -- Store: [0x8000a018]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003ee4]:feq.h t6, t5, t4
	-[0x80003ee8]:csrrs a2, fcsr, zero
	-[0x80003eec]:sw t6, 832(fp)
Current Store : [0x80003ef0] : sw a2, 836(fp) -- Store: [0x8000a020]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f24]:feq.h t6, t5, t4
	-[0x80003f28]:csrrs a2, fcsr, zero
	-[0x80003f2c]:sw t6, 840(fp)
Current Store : [0x80003f30] : sw a2, 844(fp) -- Store: [0x8000a028]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003f64]:feq.h t6, t5, t4
	-[0x80003f68]:csrrs a2, fcsr, zero
	-[0x80003f6c]:sw t6, 848(fp)
Current Store : [0x80003f70] : sw a2, 852(fp) -- Store: [0x8000a030]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fa4]:feq.h t6, t5, t4
	-[0x80003fa8]:csrrs a2, fcsr, zero
	-[0x80003fac]:sw t6, 856(fp)
Current Store : [0x80003fb0] : sw a2, 860(fp) -- Store: [0x8000a038]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80003fe4]:feq.h t6, t5, t4
	-[0x80003fe8]:csrrs a2, fcsr, zero
	-[0x80003fec]:sw t6, 864(fp)
Current Store : [0x80003ff0] : sw a2, 868(fp) -- Store: [0x8000a040]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004024]:feq.h t6, t5, t4
	-[0x80004028]:csrrs a2, fcsr, zero
	-[0x8000402c]:sw t6, 872(fp)
Current Store : [0x80004030] : sw a2, 876(fp) -- Store: [0x8000a048]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004064]:feq.h t6, t5, t4
	-[0x80004068]:csrrs a2, fcsr, zero
	-[0x8000406c]:sw t6, 880(fp)
Current Store : [0x80004070] : sw a2, 884(fp) -- Store: [0x8000a050]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040a4]:feq.h t6, t5, t4
	-[0x800040a8]:csrrs a2, fcsr, zero
	-[0x800040ac]:sw t6, 888(fp)
Current Store : [0x800040b0] : sw a2, 892(fp) -- Store: [0x8000a058]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800040e4]:feq.h t6, t5, t4
	-[0x800040e8]:csrrs a2, fcsr, zero
	-[0x800040ec]:sw t6, 896(fp)
Current Store : [0x800040f0] : sw a2, 900(fp) -- Store: [0x8000a060]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004124]:feq.h t6, t5, t4
	-[0x80004128]:csrrs a2, fcsr, zero
	-[0x8000412c]:sw t6, 904(fp)
Current Store : [0x80004130] : sw a2, 908(fp) -- Store: [0x8000a068]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004164]:feq.h t6, t5, t4
	-[0x80004168]:csrrs a2, fcsr, zero
	-[0x8000416c]:sw t6, 912(fp)
Current Store : [0x80004170] : sw a2, 916(fp) -- Store: [0x8000a070]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041a4]:feq.h t6, t5, t4
	-[0x800041a8]:csrrs a2, fcsr, zero
	-[0x800041ac]:sw t6, 920(fp)
Current Store : [0x800041b0] : sw a2, 924(fp) -- Store: [0x8000a078]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800041e4]:feq.h t6, t5, t4
	-[0x800041e8]:csrrs a2, fcsr, zero
	-[0x800041ec]:sw t6, 928(fp)
Current Store : [0x800041f0] : sw a2, 932(fp) -- Store: [0x8000a080]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004224]:feq.h t6, t5, t4
	-[0x80004228]:csrrs a2, fcsr, zero
	-[0x8000422c]:sw t6, 936(fp)
Current Store : [0x80004230] : sw a2, 940(fp) -- Store: [0x8000a088]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004264]:feq.h t6, t5, t4
	-[0x80004268]:csrrs a2, fcsr, zero
	-[0x8000426c]:sw t6, 944(fp)
Current Store : [0x80004270] : sw a2, 948(fp) -- Store: [0x8000a090]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042a4]:feq.h t6, t5, t4
	-[0x800042a8]:csrrs a2, fcsr, zero
	-[0x800042ac]:sw t6, 952(fp)
Current Store : [0x800042b0] : sw a2, 956(fp) -- Store: [0x8000a098]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800042e4]:feq.h t6, t5, t4
	-[0x800042e8]:csrrs a2, fcsr, zero
	-[0x800042ec]:sw t6, 960(fp)
Current Store : [0x800042f0] : sw a2, 964(fp) -- Store: [0x8000a0a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004324]:feq.h t6, t5, t4
	-[0x80004328]:csrrs a2, fcsr, zero
	-[0x8000432c]:sw t6, 968(fp)
Current Store : [0x80004330] : sw a2, 972(fp) -- Store: [0x8000a0a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004364]:feq.h t6, t5, t4
	-[0x80004368]:csrrs a2, fcsr, zero
	-[0x8000436c]:sw t6, 976(fp)
Current Store : [0x80004370] : sw a2, 980(fp) -- Store: [0x8000a0b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043a4]:feq.h t6, t5, t4
	-[0x800043a8]:csrrs a2, fcsr, zero
	-[0x800043ac]:sw t6, 984(fp)
Current Store : [0x800043b0] : sw a2, 988(fp) -- Store: [0x8000a0b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800043e4]:feq.h t6, t5, t4
	-[0x800043e8]:csrrs a2, fcsr, zero
	-[0x800043ec]:sw t6, 992(fp)
Current Store : [0x800043f0] : sw a2, 996(fp) -- Store: [0x8000a0c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004424]:feq.h t6, t5, t4
	-[0x80004428]:csrrs a2, fcsr, zero
	-[0x8000442c]:sw t6, 1000(fp)
Current Store : [0x80004430] : sw a2, 1004(fp) -- Store: [0x8000a0c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004464]:feq.h t6, t5, t4
	-[0x80004468]:csrrs a2, fcsr, zero
	-[0x8000446c]:sw t6, 1008(fp)
Current Store : [0x80004470] : sw a2, 1012(fp) -- Store: [0x8000a0d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044a4]:feq.h t6, t5, t4
	-[0x800044a8]:csrrs a2, fcsr, zero
	-[0x800044ac]:sw t6, 1016(fp)
Current Store : [0x800044b0] : sw a2, 1020(fp) -- Store: [0x8000a0d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800044ec]:feq.h t6, t5, t4
	-[0x800044f0]:csrrs a2, fcsr, zero
	-[0x800044f4]:sw t6, 0(fp)
Current Store : [0x800044f8] : sw a2, 4(fp) -- Store: [0x8000a0e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000452c]:feq.h t6, t5, t4
	-[0x80004530]:csrrs a2, fcsr, zero
	-[0x80004534]:sw t6, 8(fp)
Current Store : [0x80004538] : sw a2, 12(fp) -- Store: [0x8000a0e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000456c]:feq.h t6, t5, t4
	-[0x80004570]:csrrs a2, fcsr, zero
	-[0x80004574]:sw t6, 16(fp)
Current Store : [0x80004578] : sw a2, 20(fp) -- Store: [0x8000a0f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045ac]:feq.h t6, t5, t4
	-[0x800045b0]:csrrs a2, fcsr, zero
	-[0x800045b4]:sw t6, 24(fp)
Current Store : [0x800045b8] : sw a2, 28(fp) -- Store: [0x8000a0f8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800045ec]:feq.h t6, t5, t4
	-[0x800045f0]:csrrs a2, fcsr, zero
	-[0x800045f4]:sw t6, 32(fp)
Current Store : [0x800045f8] : sw a2, 36(fp) -- Store: [0x8000a100]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000462c]:feq.h t6, t5, t4
	-[0x80004630]:csrrs a2, fcsr, zero
	-[0x80004634]:sw t6, 40(fp)
Current Store : [0x80004638] : sw a2, 44(fp) -- Store: [0x8000a108]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000466c]:feq.h t6, t5, t4
	-[0x80004670]:csrrs a2, fcsr, zero
	-[0x80004674]:sw t6, 48(fp)
Current Store : [0x80004678] : sw a2, 52(fp) -- Store: [0x8000a110]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046ac]:feq.h t6, t5, t4
	-[0x800046b0]:csrrs a2, fcsr, zero
	-[0x800046b4]:sw t6, 56(fp)
Current Store : [0x800046b8] : sw a2, 60(fp) -- Store: [0x8000a118]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800046ec]:feq.h t6, t5, t4
	-[0x800046f0]:csrrs a2, fcsr, zero
	-[0x800046f4]:sw t6, 64(fp)
Current Store : [0x800046f8] : sw a2, 68(fp) -- Store: [0x8000a120]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000472c]:feq.h t6, t5, t4
	-[0x80004730]:csrrs a2, fcsr, zero
	-[0x80004734]:sw t6, 72(fp)
Current Store : [0x80004738] : sw a2, 76(fp) -- Store: [0x8000a128]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000476c]:feq.h t6, t5, t4
	-[0x80004770]:csrrs a2, fcsr, zero
	-[0x80004774]:sw t6, 80(fp)
Current Store : [0x80004778] : sw a2, 84(fp) -- Store: [0x8000a130]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047ac]:feq.h t6, t5, t4
	-[0x800047b0]:csrrs a2, fcsr, zero
	-[0x800047b4]:sw t6, 88(fp)
Current Store : [0x800047b8] : sw a2, 92(fp) -- Store: [0x8000a138]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800047ec]:feq.h t6, t5, t4
	-[0x800047f0]:csrrs a2, fcsr, zero
	-[0x800047f4]:sw t6, 96(fp)
Current Store : [0x800047f8] : sw a2, 100(fp) -- Store: [0x8000a140]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000482c]:feq.h t6, t5, t4
	-[0x80004830]:csrrs a2, fcsr, zero
	-[0x80004834]:sw t6, 104(fp)
Current Store : [0x80004838] : sw a2, 108(fp) -- Store: [0x8000a148]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000486c]:feq.h t6, t5, t4
	-[0x80004870]:csrrs a2, fcsr, zero
	-[0x80004874]:sw t6, 112(fp)
Current Store : [0x80004878] : sw a2, 116(fp) -- Store: [0x8000a150]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048ac]:feq.h t6, t5, t4
	-[0x800048b0]:csrrs a2, fcsr, zero
	-[0x800048b4]:sw t6, 120(fp)
Current Store : [0x800048b8] : sw a2, 124(fp) -- Store: [0x8000a158]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800048ec]:feq.h t6, t5, t4
	-[0x800048f0]:csrrs a2, fcsr, zero
	-[0x800048f4]:sw t6, 128(fp)
Current Store : [0x800048f8] : sw a2, 132(fp) -- Store: [0x8000a160]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000492c]:feq.h t6, t5, t4
	-[0x80004930]:csrrs a2, fcsr, zero
	-[0x80004934]:sw t6, 136(fp)
Current Store : [0x80004938] : sw a2, 140(fp) -- Store: [0x8000a168]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000496c]:feq.h t6, t5, t4
	-[0x80004970]:csrrs a2, fcsr, zero
	-[0x80004974]:sw t6, 144(fp)
Current Store : [0x80004978] : sw a2, 148(fp) -- Store: [0x8000a170]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049ac]:feq.h t6, t5, t4
	-[0x800049b0]:csrrs a2, fcsr, zero
	-[0x800049b4]:sw t6, 152(fp)
Current Store : [0x800049b8] : sw a2, 156(fp) -- Store: [0x8000a178]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800049ec]:feq.h t6, t5, t4
	-[0x800049f0]:csrrs a2, fcsr, zero
	-[0x800049f4]:sw t6, 160(fp)
Current Store : [0x800049f8] : sw a2, 164(fp) -- Store: [0x8000a180]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a2c]:feq.h t6, t5, t4
	-[0x80004a30]:csrrs a2, fcsr, zero
	-[0x80004a34]:sw t6, 168(fp)
Current Store : [0x80004a38] : sw a2, 172(fp) -- Store: [0x8000a188]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004a6c]:feq.h t6, t5, t4
	-[0x80004a70]:csrrs a2, fcsr, zero
	-[0x80004a74]:sw t6, 176(fp)
Current Store : [0x80004a78] : sw a2, 180(fp) -- Store: [0x8000a190]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004aac]:feq.h t6, t5, t4
	-[0x80004ab0]:csrrs a2, fcsr, zero
	-[0x80004ab4]:sw t6, 184(fp)
Current Store : [0x80004ab8] : sw a2, 188(fp) -- Store: [0x8000a198]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004aec]:feq.h t6, t5, t4
	-[0x80004af0]:csrrs a2, fcsr, zero
	-[0x80004af4]:sw t6, 192(fp)
Current Store : [0x80004af8] : sw a2, 196(fp) -- Store: [0x8000a1a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b2c]:feq.h t6, t5, t4
	-[0x80004b30]:csrrs a2, fcsr, zero
	-[0x80004b34]:sw t6, 200(fp)
Current Store : [0x80004b38] : sw a2, 204(fp) -- Store: [0x8000a1a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004b6c]:feq.h t6, t5, t4
	-[0x80004b70]:csrrs a2, fcsr, zero
	-[0x80004b74]:sw t6, 208(fp)
Current Store : [0x80004b78] : sw a2, 212(fp) -- Store: [0x8000a1b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bac]:feq.h t6, t5, t4
	-[0x80004bb0]:csrrs a2, fcsr, zero
	-[0x80004bb4]:sw t6, 216(fp)
Current Store : [0x80004bb8] : sw a2, 220(fp) -- Store: [0x8000a1b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004bec]:feq.h t6, t5, t4
	-[0x80004bf0]:csrrs a2, fcsr, zero
	-[0x80004bf4]:sw t6, 224(fp)
Current Store : [0x80004bf8] : sw a2, 228(fp) -- Store: [0x8000a1c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c2c]:feq.h t6, t5, t4
	-[0x80004c30]:csrrs a2, fcsr, zero
	-[0x80004c34]:sw t6, 232(fp)
Current Store : [0x80004c38] : sw a2, 236(fp) -- Store: [0x8000a1c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004c6c]:feq.h t6, t5, t4
	-[0x80004c70]:csrrs a2, fcsr, zero
	-[0x80004c74]:sw t6, 240(fp)
Current Store : [0x80004c78] : sw a2, 244(fp) -- Store: [0x8000a1d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004cac]:feq.h t6, t5, t4
	-[0x80004cb0]:csrrs a2, fcsr, zero
	-[0x80004cb4]:sw t6, 248(fp)
Current Store : [0x80004cb8] : sw a2, 252(fp) -- Store: [0x8000a1d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004cec]:feq.h t6, t5, t4
	-[0x80004cf0]:csrrs a2, fcsr, zero
	-[0x80004cf4]:sw t6, 256(fp)
Current Store : [0x80004cf8] : sw a2, 260(fp) -- Store: [0x8000a1e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d2c]:feq.h t6, t5, t4
	-[0x80004d30]:csrrs a2, fcsr, zero
	-[0x80004d34]:sw t6, 264(fp)
Current Store : [0x80004d38] : sw a2, 268(fp) -- Store: [0x8000a1e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004d6c]:feq.h t6, t5, t4
	-[0x80004d70]:csrrs a2, fcsr, zero
	-[0x80004d74]:sw t6, 272(fp)
Current Store : [0x80004d78] : sw a2, 276(fp) -- Store: [0x8000a1f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004dac]:feq.h t6, t5, t4
	-[0x80004db0]:csrrs a2, fcsr, zero
	-[0x80004db4]:sw t6, 280(fp)
Current Store : [0x80004db8] : sw a2, 284(fp) -- Store: [0x8000a1f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004dec]:feq.h t6, t5, t4
	-[0x80004df0]:csrrs a2, fcsr, zero
	-[0x80004df4]:sw t6, 288(fp)
Current Store : [0x80004df8] : sw a2, 292(fp) -- Store: [0x8000a200]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e2c]:feq.h t6, t5, t4
	-[0x80004e30]:csrrs a2, fcsr, zero
	-[0x80004e34]:sw t6, 296(fp)
Current Store : [0x80004e38] : sw a2, 300(fp) -- Store: [0x8000a208]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004e6c]:feq.h t6, t5, t4
	-[0x80004e70]:csrrs a2, fcsr, zero
	-[0x80004e74]:sw t6, 304(fp)
Current Store : [0x80004e78] : sw a2, 308(fp) -- Store: [0x8000a210]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004eac]:feq.h t6, t5, t4
	-[0x80004eb0]:csrrs a2, fcsr, zero
	-[0x80004eb4]:sw t6, 312(fp)
Current Store : [0x80004eb8] : sw a2, 316(fp) -- Store: [0x8000a218]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004eec]:feq.h t6, t5, t4
	-[0x80004ef0]:csrrs a2, fcsr, zero
	-[0x80004ef4]:sw t6, 320(fp)
Current Store : [0x80004ef8] : sw a2, 324(fp) -- Store: [0x8000a220]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f2c]:feq.h t6, t5, t4
	-[0x80004f30]:csrrs a2, fcsr, zero
	-[0x80004f34]:sw t6, 328(fp)
Current Store : [0x80004f38] : sw a2, 332(fp) -- Store: [0x8000a228]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004f6c]:feq.h t6, t5, t4
	-[0x80004f70]:csrrs a2, fcsr, zero
	-[0x80004f74]:sw t6, 336(fp)
Current Store : [0x80004f78] : sw a2, 340(fp) -- Store: [0x8000a230]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fac]:feq.h t6, t5, t4
	-[0x80004fb0]:csrrs a2, fcsr, zero
	-[0x80004fb4]:sw t6, 344(fp)
Current Store : [0x80004fb8] : sw a2, 348(fp) -- Store: [0x8000a238]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80004fec]:feq.h t6, t5, t4
	-[0x80004ff0]:csrrs a2, fcsr, zero
	-[0x80004ff4]:sw t6, 352(fp)
Current Store : [0x80004ff8] : sw a2, 356(fp) -- Store: [0x8000a240]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000502c]:feq.h t6, t5, t4
	-[0x80005030]:csrrs a2, fcsr, zero
	-[0x80005034]:sw t6, 360(fp)
Current Store : [0x80005038] : sw a2, 364(fp) -- Store: [0x8000a248]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000506c]:feq.h t6, t5, t4
	-[0x80005070]:csrrs a2, fcsr, zero
	-[0x80005074]:sw t6, 368(fp)
Current Store : [0x80005078] : sw a2, 372(fp) -- Store: [0x8000a250]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050ac]:feq.h t6, t5, t4
	-[0x800050b0]:csrrs a2, fcsr, zero
	-[0x800050b4]:sw t6, 376(fp)
Current Store : [0x800050b8] : sw a2, 380(fp) -- Store: [0x8000a258]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800050ec]:feq.h t6, t5, t4
	-[0x800050f0]:csrrs a2, fcsr, zero
	-[0x800050f4]:sw t6, 384(fp)
Current Store : [0x800050f8] : sw a2, 388(fp) -- Store: [0x8000a260]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000512c]:feq.h t6, t5, t4
	-[0x80005130]:csrrs a2, fcsr, zero
	-[0x80005134]:sw t6, 392(fp)
Current Store : [0x80005138] : sw a2, 396(fp) -- Store: [0x8000a268]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000516c]:feq.h t6, t5, t4
	-[0x80005170]:csrrs a2, fcsr, zero
	-[0x80005174]:sw t6, 400(fp)
Current Store : [0x80005178] : sw a2, 404(fp) -- Store: [0x8000a270]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051ac]:feq.h t6, t5, t4
	-[0x800051b0]:csrrs a2, fcsr, zero
	-[0x800051b4]:sw t6, 408(fp)
Current Store : [0x800051b8] : sw a2, 412(fp) -- Store: [0x8000a278]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800051ec]:feq.h t6, t5, t4
	-[0x800051f0]:csrrs a2, fcsr, zero
	-[0x800051f4]:sw t6, 416(fp)
Current Store : [0x800051f8] : sw a2, 420(fp) -- Store: [0x8000a280]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000522c]:feq.h t6, t5, t4
	-[0x80005230]:csrrs a2, fcsr, zero
	-[0x80005234]:sw t6, 424(fp)
Current Store : [0x80005238] : sw a2, 428(fp) -- Store: [0x8000a288]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000526c]:feq.h t6, t5, t4
	-[0x80005270]:csrrs a2, fcsr, zero
	-[0x80005274]:sw t6, 432(fp)
Current Store : [0x80005278] : sw a2, 436(fp) -- Store: [0x8000a290]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052ac]:feq.h t6, t5, t4
	-[0x800052b0]:csrrs a2, fcsr, zero
	-[0x800052b4]:sw t6, 440(fp)
Current Store : [0x800052b8] : sw a2, 444(fp) -- Store: [0x8000a298]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800052ec]:feq.h t6, t5, t4
	-[0x800052f0]:csrrs a2, fcsr, zero
	-[0x800052f4]:sw t6, 448(fp)
Current Store : [0x800052f8] : sw a2, 452(fp) -- Store: [0x8000a2a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000532c]:feq.h t6, t5, t4
	-[0x80005330]:csrrs a2, fcsr, zero
	-[0x80005334]:sw t6, 456(fp)
Current Store : [0x80005338] : sw a2, 460(fp) -- Store: [0x8000a2a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000536c]:feq.h t6, t5, t4
	-[0x80005370]:csrrs a2, fcsr, zero
	-[0x80005374]:sw t6, 464(fp)
Current Store : [0x80005378] : sw a2, 468(fp) -- Store: [0x8000a2b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053ac]:feq.h t6, t5, t4
	-[0x800053b0]:csrrs a2, fcsr, zero
	-[0x800053b4]:sw t6, 472(fp)
Current Store : [0x800053b8] : sw a2, 476(fp) -- Store: [0x8000a2b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800053ec]:feq.h t6, t5, t4
	-[0x800053f0]:csrrs a2, fcsr, zero
	-[0x800053f4]:sw t6, 480(fp)
Current Store : [0x800053f8] : sw a2, 484(fp) -- Store: [0x8000a2c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000542c]:feq.h t6, t5, t4
	-[0x80005430]:csrrs a2, fcsr, zero
	-[0x80005434]:sw t6, 488(fp)
Current Store : [0x80005438] : sw a2, 492(fp) -- Store: [0x8000a2c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000546c]:feq.h t6, t5, t4
	-[0x80005470]:csrrs a2, fcsr, zero
	-[0x80005474]:sw t6, 496(fp)
Current Store : [0x80005478] : sw a2, 500(fp) -- Store: [0x8000a2d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054ac]:feq.h t6, t5, t4
	-[0x800054b0]:csrrs a2, fcsr, zero
	-[0x800054b4]:sw t6, 504(fp)
Current Store : [0x800054b8] : sw a2, 508(fp) -- Store: [0x8000a2d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800054ec]:feq.h t6, t5, t4
	-[0x800054f0]:csrrs a2, fcsr, zero
	-[0x800054f4]:sw t6, 512(fp)
Current Store : [0x800054f8] : sw a2, 516(fp) -- Store: [0x8000a2e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000552c]:feq.h t6, t5, t4
	-[0x80005530]:csrrs a2, fcsr, zero
	-[0x80005534]:sw t6, 520(fp)
Current Store : [0x80005538] : sw a2, 524(fp) -- Store: [0x8000a2e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000556c]:feq.h t6, t5, t4
	-[0x80005570]:csrrs a2, fcsr, zero
	-[0x80005574]:sw t6, 528(fp)
Current Store : [0x80005578] : sw a2, 532(fp) -- Store: [0x8000a2f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055ac]:feq.h t6, t5, t4
	-[0x800055b0]:csrrs a2, fcsr, zero
	-[0x800055b4]:sw t6, 536(fp)
Current Store : [0x800055b8] : sw a2, 540(fp) -- Store: [0x8000a2f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800055ec]:feq.h t6, t5, t4
	-[0x800055f0]:csrrs a2, fcsr, zero
	-[0x800055f4]:sw t6, 544(fp)
Current Store : [0x800055f8] : sw a2, 548(fp) -- Store: [0x8000a300]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000562c]:feq.h t6, t5, t4
	-[0x80005630]:csrrs a2, fcsr, zero
	-[0x80005634]:sw t6, 552(fp)
Current Store : [0x80005638] : sw a2, 556(fp) -- Store: [0x8000a308]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000566c]:feq.h t6, t5, t4
	-[0x80005670]:csrrs a2, fcsr, zero
	-[0x80005674]:sw t6, 560(fp)
Current Store : [0x80005678] : sw a2, 564(fp) -- Store: [0x8000a310]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056ac]:feq.h t6, t5, t4
	-[0x800056b0]:csrrs a2, fcsr, zero
	-[0x800056b4]:sw t6, 568(fp)
Current Store : [0x800056b8] : sw a2, 572(fp) -- Store: [0x8000a318]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800056ec]:feq.h t6, t5, t4
	-[0x800056f0]:csrrs a2, fcsr, zero
	-[0x800056f4]:sw t6, 576(fp)
Current Store : [0x800056f8] : sw a2, 580(fp) -- Store: [0x8000a320]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000572c]:feq.h t6, t5, t4
	-[0x80005730]:csrrs a2, fcsr, zero
	-[0x80005734]:sw t6, 584(fp)
Current Store : [0x80005738] : sw a2, 588(fp) -- Store: [0x8000a328]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000576c]:feq.h t6, t5, t4
	-[0x80005770]:csrrs a2, fcsr, zero
	-[0x80005774]:sw t6, 592(fp)
Current Store : [0x80005778] : sw a2, 596(fp) -- Store: [0x8000a330]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057ac]:feq.h t6, t5, t4
	-[0x800057b0]:csrrs a2, fcsr, zero
	-[0x800057b4]:sw t6, 600(fp)
Current Store : [0x800057b8] : sw a2, 604(fp) -- Store: [0x8000a338]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800057ec]:feq.h t6, t5, t4
	-[0x800057f0]:csrrs a2, fcsr, zero
	-[0x800057f4]:sw t6, 608(fp)
Current Store : [0x800057f8] : sw a2, 612(fp) -- Store: [0x8000a340]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000582c]:feq.h t6, t5, t4
	-[0x80005830]:csrrs a2, fcsr, zero
	-[0x80005834]:sw t6, 616(fp)
Current Store : [0x80005838] : sw a2, 620(fp) -- Store: [0x8000a348]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000586c]:feq.h t6, t5, t4
	-[0x80005870]:csrrs a2, fcsr, zero
	-[0x80005874]:sw t6, 624(fp)
Current Store : [0x80005878] : sw a2, 628(fp) -- Store: [0x8000a350]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058ac]:feq.h t6, t5, t4
	-[0x800058b0]:csrrs a2, fcsr, zero
	-[0x800058b4]:sw t6, 632(fp)
Current Store : [0x800058b8] : sw a2, 636(fp) -- Store: [0x8000a358]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800058ec]:feq.h t6, t5, t4
	-[0x800058f0]:csrrs a2, fcsr, zero
	-[0x800058f4]:sw t6, 640(fp)
Current Store : [0x800058f8] : sw a2, 644(fp) -- Store: [0x8000a360]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000592c]:feq.h t6, t5, t4
	-[0x80005930]:csrrs a2, fcsr, zero
	-[0x80005934]:sw t6, 648(fp)
Current Store : [0x80005938] : sw a2, 652(fp) -- Store: [0x8000a368]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000596c]:feq.h t6, t5, t4
	-[0x80005970]:csrrs a2, fcsr, zero
	-[0x80005974]:sw t6, 656(fp)
Current Store : [0x80005978] : sw a2, 660(fp) -- Store: [0x8000a370]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059ac]:feq.h t6, t5, t4
	-[0x800059b0]:csrrs a2, fcsr, zero
	-[0x800059b4]:sw t6, 664(fp)
Current Store : [0x800059b8] : sw a2, 668(fp) -- Store: [0x8000a378]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800059ec]:feq.h t6, t5, t4
	-[0x800059f0]:csrrs a2, fcsr, zero
	-[0x800059f4]:sw t6, 672(fp)
Current Store : [0x800059f8] : sw a2, 676(fp) -- Store: [0x8000a380]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a2c]:feq.h t6, t5, t4
	-[0x80005a30]:csrrs a2, fcsr, zero
	-[0x80005a34]:sw t6, 680(fp)
Current Store : [0x80005a38] : sw a2, 684(fp) -- Store: [0x8000a388]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005a6c]:feq.h t6, t5, t4
	-[0x80005a70]:csrrs a2, fcsr, zero
	-[0x80005a74]:sw t6, 688(fp)
Current Store : [0x80005a78] : sw a2, 692(fp) -- Store: [0x8000a390]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005aac]:feq.h t6, t5, t4
	-[0x80005ab0]:csrrs a2, fcsr, zero
	-[0x80005ab4]:sw t6, 696(fp)
Current Store : [0x80005ab8] : sw a2, 700(fp) -- Store: [0x8000a398]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005aec]:feq.h t6, t5, t4
	-[0x80005af0]:csrrs a2, fcsr, zero
	-[0x80005af4]:sw t6, 704(fp)
Current Store : [0x80005af8] : sw a2, 708(fp) -- Store: [0x8000a3a0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b2c]:feq.h t6, t5, t4
	-[0x80005b30]:csrrs a2, fcsr, zero
	-[0x80005b34]:sw t6, 712(fp)
Current Store : [0x80005b38] : sw a2, 716(fp) -- Store: [0x8000a3a8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005b6c]:feq.h t6, t5, t4
	-[0x80005b70]:csrrs a2, fcsr, zero
	-[0x80005b74]:sw t6, 720(fp)
Current Store : [0x80005b78] : sw a2, 724(fp) -- Store: [0x8000a3b0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bac]:feq.h t6, t5, t4
	-[0x80005bb0]:csrrs a2, fcsr, zero
	-[0x80005bb4]:sw t6, 728(fp)
Current Store : [0x80005bb8] : sw a2, 732(fp) -- Store: [0x8000a3b8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005bec]:feq.h t6, t5, t4
	-[0x80005bf0]:csrrs a2, fcsr, zero
	-[0x80005bf4]:sw t6, 736(fp)
Current Store : [0x80005bf8] : sw a2, 740(fp) -- Store: [0x8000a3c0]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c2c]:feq.h t6, t5, t4
	-[0x80005c30]:csrrs a2, fcsr, zero
	-[0x80005c34]:sw t6, 744(fp)
Current Store : [0x80005c38] : sw a2, 748(fp) -- Store: [0x8000a3c8]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005c6c]:feq.h t6, t5, t4
	-[0x80005c70]:csrrs a2, fcsr, zero
	-[0x80005c74]:sw t6, 752(fp)
Current Store : [0x80005c78] : sw a2, 756(fp) -- Store: [0x8000a3d0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005cac]:feq.h t6, t5, t4
	-[0x80005cb0]:csrrs a2, fcsr, zero
	-[0x80005cb4]:sw t6, 760(fp)
Current Store : [0x80005cb8] : sw a2, 764(fp) -- Store: [0x8000a3d8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005cec]:feq.h t6, t5, t4
	-[0x80005cf0]:csrrs a2, fcsr, zero
	-[0x80005cf4]:sw t6, 768(fp)
Current Store : [0x80005cf8] : sw a2, 772(fp) -- Store: [0x8000a3e0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d2c]:feq.h t6, t5, t4
	-[0x80005d30]:csrrs a2, fcsr, zero
	-[0x80005d34]:sw t6, 776(fp)
Current Store : [0x80005d38] : sw a2, 780(fp) -- Store: [0x8000a3e8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005d6c]:feq.h t6, t5, t4
	-[0x80005d70]:csrrs a2, fcsr, zero
	-[0x80005d74]:sw t6, 784(fp)
Current Store : [0x80005d78] : sw a2, 788(fp) -- Store: [0x8000a3f0]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005dac]:feq.h t6, t5, t4
	-[0x80005db0]:csrrs a2, fcsr, zero
	-[0x80005db4]:sw t6, 792(fp)
Current Store : [0x80005db8] : sw a2, 796(fp) -- Store: [0x8000a3f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005dec]:feq.h t6, t5, t4
	-[0x80005df0]:csrrs a2, fcsr, zero
	-[0x80005df4]:sw t6, 800(fp)
Current Store : [0x80005df8] : sw a2, 804(fp) -- Store: [0x8000a400]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e2c]:feq.h t6, t5, t4
	-[0x80005e30]:csrrs a2, fcsr, zero
	-[0x80005e34]:sw t6, 808(fp)
Current Store : [0x80005e38] : sw a2, 812(fp) -- Store: [0x8000a408]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005e6c]:feq.h t6, t5, t4
	-[0x80005e70]:csrrs a2, fcsr, zero
	-[0x80005e74]:sw t6, 816(fp)
Current Store : [0x80005e78] : sw a2, 820(fp) -- Store: [0x8000a410]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005eac]:feq.h t6, t5, t4
	-[0x80005eb0]:csrrs a2, fcsr, zero
	-[0x80005eb4]:sw t6, 824(fp)
Current Store : [0x80005eb8] : sw a2, 828(fp) -- Store: [0x8000a418]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005eec]:feq.h t6, t5, t4
	-[0x80005ef0]:csrrs a2, fcsr, zero
	-[0x80005ef4]:sw t6, 832(fp)
Current Store : [0x80005ef8] : sw a2, 836(fp) -- Store: [0x8000a420]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f2c]:feq.h t6, t5, t4
	-[0x80005f30]:csrrs a2, fcsr, zero
	-[0x80005f34]:sw t6, 840(fp)
Current Store : [0x80005f38] : sw a2, 844(fp) -- Store: [0x8000a428]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005f6c]:feq.h t6, t5, t4
	-[0x80005f70]:csrrs a2, fcsr, zero
	-[0x80005f74]:sw t6, 848(fp)
Current Store : [0x80005f78] : sw a2, 852(fp) -- Store: [0x8000a430]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fac]:feq.h t6, t5, t4
	-[0x80005fb0]:csrrs a2, fcsr, zero
	-[0x80005fb4]:sw t6, 856(fp)
Current Store : [0x80005fb8] : sw a2, 860(fp) -- Store: [0x8000a438]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80005fec]:feq.h t6, t5, t4
	-[0x80005ff0]:csrrs a2, fcsr, zero
	-[0x80005ff4]:sw t6, 864(fp)
Current Store : [0x80005ff8] : sw a2, 868(fp) -- Store: [0x8000a440]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000602c]:feq.h t6, t5, t4
	-[0x80006030]:csrrs a2, fcsr, zero
	-[0x80006034]:sw t6, 872(fp)
Current Store : [0x80006038] : sw a2, 876(fp) -- Store: [0x8000a448]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000606c]:feq.h t6, t5, t4
	-[0x80006070]:csrrs a2, fcsr, zero
	-[0x80006074]:sw t6, 880(fp)
Current Store : [0x80006078] : sw a2, 884(fp) -- Store: [0x8000a450]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060ac]:feq.h t6, t5, t4
	-[0x800060b0]:csrrs a2, fcsr, zero
	-[0x800060b4]:sw t6, 888(fp)
Current Store : [0x800060b8] : sw a2, 892(fp) -- Store: [0x8000a458]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800060ec]:feq.h t6, t5, t4
	-[0x800060f0]:csrrs a2, fcsr, zero
	-[0x800060f4]:sw t6, 896(fp)
Current Store : [0x800060f8] : sw a2, 900(fp) -- Store: [0x8000a460]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000612c]:feq.h t6, t5, t4
	-[0x80006130]:csrrs a2, fcsr, zero
	-[0x80006134]:sw t6, 904(fp)
Current Store : [0x80006138] : sw a2, 908(fp) -- Store: [0x8000a468]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000616c]:feq.h t6, t5, t4
	-[0x80006170]:csrrs a2, fcsr, zero
	-[0x80006174]:sw t6, 912(fp)
Current Store : [0x80006178] : sw a2, 916(fp) -- Store: [0x8000a470]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061ac]:feq.h t6, t5, t4
	-[0x800061b0]:csrrs a2, fcsr, zero
	-[0x800061b4]:sw t6, 920(fp)
Current Store : [0x800061b8] : sw a2, 924(fp) -- Store: [0x8000a478]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800061ec]:feq.h t6, t5, t4
	-[0x800061f0]:csrrs a2, fcsr, zero
	-[0x800061f4]:sw t6, 928(fp)
Current Store : [0x800061f8] : sw a2, 932(fp) -- Store: [0x8000a480]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000622c]:feq.h t6, t5, t4
	-[0x80006230]:csrrs a2, fcsr, zero
	-[0x80006234]:sw t6, 936(fp)
Current Store : [0x80006238] : sw a2, 940(fp) -- Store: [0x8000a488]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000626c]:feq.h t6, t5, t4
	-[0x80006270]:csrrs a2, fcsr, zero
	-[0x80006274]:sw t6, 944(fp)
Current Store : [0x80006278] : sw a2, 948(fp) -- Store: [0x8000a490]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062ac]:feq.h t6, t5, t4
	-[0x800062b0]:csrrs a2, fcsr, zero
	-[0x800062b4]:sw t6, 952(fp)
Current Store : [0x800062b8] : sw a2, 956(fp) -- Store: [0x8000a498]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800062ec]:feq.h t6, t5, t4
	-[0x800062f0]:csrrs a2, fcsr, zero
	-[0x800062f4]:sw t6, 960(fp)
Current Store : [0x800062f8] : sw a2, 964(fp) -- Store: [0x8000a4a0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000632c]:feq.h t6, t5, t4
	-[0x80006330]:csrrs a2, fcsr, zero
	-[0x80006334]:sw t6, 968(fp)
Current Store : [0x80006338] : sw a2, 972(fp) -- Store: [0x8000a4a8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000636c]:feq.h t6, t5, t4
	-[0x80006370]:csrrs a2, fcsr, zero
	-[0x80006374]:sw t6, 976(fp)
Current Store : [0x80006378] : sw a2, 980(fp) -- Store: [0x8000a4b0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063ac]:feq.h t6, t5, t4
	-[0x800063b0]:csrrs a2, fcsr, zero
	-[0x800063b4]:sw t6, 984(fp)
Current Store : [0x800063b8] : sw a2, 988(fp) -- Store: [0x8000a4b8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800063ec]:feq.h t6, t5, t4
	-[0x800063f0]:csrrs a2, fcsr, zero
	-[0x800063f4]:sw t6, 992(fp)
Current Store : [0x800063f8] : sw a2, 996(fp) -- Store: [0x8000a4c0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006424]:feq.h t6, t5, t4
	-[0x80006428]:csrrs a2, fcsr, zero
	-[0x8000642c]:sw t6, 1000(fp)
Current Store : [0x80006430] : sw a2, 1004(fp) -- Store: [0x8000a4c8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000645c]:feq.h t6, t5, t4
	-[0x80006460]:csrrs a2, fcsr, zero
	-[0x80006464]:sw t6, 1008(fp)
Current Store : [0x80006468] : sw a2, 1012(fp) -- Store: [0x8000a4d0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006494]:feq.h t6, t5, t4
	-[0x80006498]:csrrs a2, fcsr, zero
	-[0x8000649c]:sw t6, 1016(fp)
Current Store : [0x800064a0] : sw a2, 1020(fp) -- Store: [0x8000a4d8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800064d4]:feq.h t6, t5, t4
	-[0x800064d8]:csrrs a2, fcsr, zero
	-[0x800064dc]:sw t6, 0(fp)
Current Store : [0x800064e0] : sw a2, 4(fp) -- Store: [0x8000a4e0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000650c]:feq.h t6, t5, t4
	-[0x80006510]:csrrs a2, fcsr, zero
	-[0x80006514]:sw t6, 8(fp)
Current Store : [0x80006518] : sw a2, 12(fp) -- Store: [0x8000a4e8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006544]:feq.h t6, t5, t4
	-[0x80006548]:csrrs a2, fcsr, zero
	-[0x8000654c]:sw t6, 16(fp)
Current Store : [0x80006550] : sw a2, 20(fp) -- Store: [0x8000a4f0]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000657c]:feq.h t6, t5, t4
	-[0x80006580]:csrrs a2, fcsr, zero
	-[0x80006584]:sw t6, 24(fp)
Current Store : [0x80006588] : sw a2, 28(fp) -- Store: [0x8000a4f8]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065b4]:feq.h t6, t5, t4
	-[0x800065b8]:csrrs a2, fcsr, zero
	-[0x800065bc]:sw t6, 32(fp)
Current Store : [0x800065c0] : sw a2, 36(fp) -- Store: [0x8000a500]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800065ec]:feq.h t6, t5, t4
	-[0x800065f0]:csrrs a2, fcsr, zero
	-[0x800065f4]:sw t6, 40(fp)
Current Store : [0x800065f8] : sw a2, 44(fp) -- Store: [0x8000a508]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006624]:feq.h t6, t5, t4
	-[0x80006628]:csrrs a2, fcsr, zero
	-[0x8000662c]:sw t6, 48(fp)
Current Store : [0x80006630] : sw a2, 52(fp) -- Store: [0x8000a510]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000665c]:feq.h t6, t5, t4
	-[0x80006660]:csrrs a2, fcsr, zero
	-[0x80006664]:sw t6, 56(fp)
Current Store : [0x80006668] : sw a2, 60(fp) -- Store: [0x8000a518]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006694]:feq.h t6, t5, t4
	-[0x80006698]:csrrs a2, fcsr, zero
	-[0x8000669c]:sw t6, 64(fp)
Current Store : [0x800066a0] : sw a2, 68(fp) -- Store: [0x8000a520]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800066cc]:feq.h t6, t5, t4
	-[0x800066d0]:csrrs a2, fcsr, zero
	-[0x800066d4]:sw t6, 72(fp)
Current Store : [0x800066d8] : sw a2, 76(fp) -- Store: [0x8000a528]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006704]:feq.h t6, t5, t4
	-[0x80006708]:csrrs a2, fcsr, zero
	-[0x8000670c]:sw t6, 80(fp)
Current Store : [0x80006710] : sw a2, 84(fp) -- Store: [0x8000a530]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000673c]:feq.h t6, t5, t4
	-[0x80006740]:csrrs a2, fcsr, zero
	-[0x80006744]:sw t6, 88(fp)
Current Store : [0x80006748] : sw a2, 92(fp) -- Store: [0x8000a538]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006774]:feq.h t6, t5, t4
	-[0x80006778]:csrrs a2, fcsr, zero
	-[0x8000677c]:sw t6, 96(fp)
Current Store : [0x80006780] : sw a2, 100(fp) -- Store: [0x8000a540]:0x00000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067ac]:feq.h t6, t5, t4
	-[0x800067b0]:csrrs a2, fcsr, zero
	-[0x800067b4]:sw t6, 104(fp)
Current Store : [0x800067b8] : sw a2, 108(fp) -- Store: [0x8000a548]:0x00000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800067e4]:feq.h t6, t5, t4
	-[0x800067e8]:csrrs a2, fcsr, zero
	-[0x800067ec]:sw t6, 112(fp)
Current Store : [0x800067f0] : sw a2, 116(fp) -- Store: [0x8000a550]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000681c]:feq.h t6, t5, t4
	-[0x80006820]:csrrs a2, fcsr, zero
	-[0x80006824]:sw t6, 120(fp)
Current Store : [0x80006828] : sw a2, 124(fp) -- Store: [0x8000a558]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006854]:feq.h t6, t5, t4
	-[0x80006858]:csrrs a2, fcsr, zero
	-[0x8000685c]:sw t6, 128(fp)
Current Store : [0x80006860] : sw a2, 132(fp) -- Store: [0x8000a560]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000688c]:feq.h t6, t5, t4
	-[0x80006890]:csrrs a2, fcsr, zero
	-[0x80006894]:sw t6, 136(fp)
Current Store : [0x80006898] : sw a2, 140(fp) -- Store: [0x8000a568]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068c4]:feq.h t6, t5, t4
	-[0x800068c8]:csrrs a2, fcsr, zero
	-[0x800068cc]:sw t6, 144(fp)
Current Store : [0x800068d0] : sw a2, 148(fp) -- Store: [0x8000a570]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800068fc]:feq.h t6, t5, t4
	-[0x80006900]:csrrs a2, fcsr, zero
	-[0x80006904]:sw t6, 152(fp)
Current Store : [0x80006908] : sw a2, 156(fp) -- Store: [0x8000a578]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006934]:feq.h t6, t5, t4
	-[0x80006938]:csrrs a2, fcsr, zero
	-[0x8000693c]:sw t6, 160(fp)
Current Store : [0x80006940] : sw a2, 164(fp) -- Store: [0x8000a580]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x8000696c]:feq.h t6, t5, t4
	-[0x80006970]:csrrs a2, fcsr, zero
	-[0x80006974]:sw t6, 168(fp)
Current Store : [0x80006978] : sw a2, 172(fp) -- Store: [0x8000a588]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069a4]:feq.h t6, t5, t4
	-[0x800069a8]:csrrs a2, fcsr, zero
	-[0x800069ac]:sw t6, 176(fp)
Current Store : [0x800069b0] : sw a2, 180(fp) -- Store: [0x8000a590]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x800069dc]:feq.h t6, t5, t4
	-[0x800069e0]:csrrs a2, fcsr, zero
	-[0x800069e4]:sw t6, 184(fp)
Current Store : [0x800069e8] : sw a2, 188(fp) -- Store: [0x8000a598]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a14]:feq.h t6, t5, t4
	-[0x80006a18]:csrrs a2, fcsr, zero
	-[0x80006a1c]:sw t6, 192(fp)
Current Store : [0x80006a20] : sw a2, 196(fp) -- Store: [0x8000a5a0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a4c]:feq.h t6, t5, t4
	-[0x80006a50]:csrrs a2, fcsr, zero
	-[0x80006a54]:sw t6, 200(fp)
Current Store : [0x80006a58] : sw a2, 204(fp) -- Store: [0x8000a5a8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006a84]:feq.h t6, t5, t4
	-[0x80006a88]:csrrs a2, fcsr, zero
	-[0x80006a8c]:sw t6, 208(fp)
Current Store : [0x80006a90] : sw a2, 212(fp) -- Store: [0x8000a5b0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006abc]:feq.h t6, t5, t4
	-[0x80006ac0]:csrrs a2, fcsr, zero
	-[0x80006ac4]:sw t6, 216(fp)
Current Store : [0x80006ac8] : sw a2, 220(fp) -- Store: [0x8000a5b8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006af4]:feq.h t6, t5, t4
	-[0x80006af8]:csrrs a2, fcsr, zero
	-[0x80006afc]:sw t6, 224(fp)
Current Store : [0x80006b00] : sw a2, 228(fp) -- Store: [0x8000a5c0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b2c]:feq.h t6, t5, t4
	-[0x80006b30]:csrrs a2, fcsr, zero
	-[0x80006b34]:sw t6, 232(fp)
Current Store : [0x80006b38] : sw a2, 236(fp) -- Store: [0x8000a5c8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b64]:feq.h t6, t5, t4
	-[0x80006b68]:csrrs a2, fcsr, zero
	-[0x80006b6c]:sw t6, 240(fp)
Current Store : [0x80006b70] : sw a2, 244(fp) -- Store: [0x8000a5d0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006b9c]:feq.h t6, t5, t4
	-[0x80006ba0]:csrrs a2, fcsr, zero
	-[0x80006ba4]:sw t6, 248(fp)
Current Store : [0x80006ba8] : sw a2, 252(fp) -- Store: [0x8000a5d8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006bd4]:feq.h t6, t5, t4
	-[0x80006bd8]:csrrs a2, fcsr, zero
	-[0x80006bdc]:sw t6, 256(fp)
Current Store : [0x80006be0] : sw a2, 260(fp) -- Store: [0x8000a5e0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c0c]:feq.h t6, t5, t4
	-[0x80006c10]:csrrs a2, fcsr, zero
	-[0x80006c14]:sw t6, 264(fp)
Current Store : [0x80006c18] : sw a2, 268(fp) -- Store: [0x8000a5e8]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c44]:feq.h t6, t5, t4
	-[0x80006c48]:csrrs a2, fcsr, zero
	-[0x80006c4c]:sw t6, 272(fp)
Current Store : [0x80006c50] : sw a2, 276(fp) -- Store: [0x8000a5f0]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006c7c]:feq.h t6, t5, t4
	-[0x80006c80]:csrrs a2, fcsr, zero
	-[0x80006c84]:sw t6, 280(fp)
Current Store : [0x80006c88] : sw a2, 284(fp) -- Store: [0x8000a5f8]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cb4]:feq.h t6, t5, t4
	-[0x80006cb8]:csrrs a2, fcsr, zero
	-[0x80006cbc]:sw t6, 288(fp)
Current Store : [0x80006cc0] : sw a2, 292(fp) -- Store: [0x8000a600]:0x00000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006cec]:feq.h t6, t5, t4
	-[0x80006cf0]:csrrs a2, fcsr, zero
	-[0x80006cf4]:sw t6, 296(fp)
Current Store : [0x80006cf8] : sw a2, 300(fp) -- Store: [0x8000a608]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d24]:feq.h t6, t5, t4
	-[0x80006d28]:csrrs a2, fcsr, zero
	-[0x80006d2c]:sw t6, 304(fp)
Current Store : [0x80006d30] : sw a2, 308(fp) -- Store: [0x8000a610]:0x00000000




Last Coverpoint : ['mnemonic : feq.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d5c]:feq.h t6, t5, t4
	-[0x80006d60]:csrrs a2, fcsr, zero
	-[0x80006d64]:sw t6, 312(fp)
Current Store : [0x80006d68] : sw a2, 316(fp) -- Store: [0x8000a618]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006d94]:feq.h t6, t5, t4
	-[0x80006d98]:csrrs a2, fcsr, zero
	-[0x80006d9c]:sw t6, 320(fp)
Current Store : [0x80006da0] : sw a2, 324(fp) -- Store: [0x8000a620]:0x00000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006dcc]:feq.h t6, t5, t4
	-[0x80006dd0]:csrrs a2, fcsr, zero
	-[0x80006dd4]:sw t6, 328(fp)
Current Store : [0x80006dd8] : sw a2, 332(fp) -- Store: [0x8000a628]:0x00000000




Last Coverpoint : ['mnemonic : feq.h', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat']
Last Code Sequence : 
	-[0x80006e04]:feq.h t6, t5, t4
	-[0x80006e08]:csrrs a2, fcsr, zero
	-[0x80006e0c]:sw t6, 336(fp)
Current Store : [0x80006e10] : sw a2, 340(fp) -- Store: [0x8000a630]:0x00000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|           signature           |                                                                                                                             coverpoints                                                                                                                              |                                                   code                                                    |
|---:|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------|
|   1|[0x80009414]<br>0x00000001<br> |- mnemonic : feq.h<br> - rs1 : x31<br> - rs2 : x31<br> - rd : x31<br> - rs1 == rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br> |[0x80000124]:feq.h t6, t6, t6<br> [0x80000128]:csrrs tp, fcsr, zero<br> [0x8000012c]:sw t6, 0(ra)<br>      |
|   2|[0x8000941c]<br>0x00000001<br> |- rs1 : x30<br> - rs2 : x29<br> - rd : x30<br> - rs1 != rs2<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                        |[0x80000144]:feq.h t5, t5, t4<br> [0x80000148]:csrrs tp, fcsr, zero<br> [0x8000014c]:sw t5, 8(ra)<br>      |
|   3|[0x80009424]<br>0x00000000<br> |- rs1 : x28<br> - rs2 : x30<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000164]:feq.h t4, t3, t5<br> [0x80000168]:csrrs tp, fcsr, zero<br> [0x8000016c]:sw t4, 16(ra)<br>     |
|   4|[0x8000942c]<br>0x00000000<br> |- rs1 : x29<br> - rs2 : x27<br> - rd : x28<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000184]:feq.h t3, t4, s11<br> [0x80000188]:csrrs tp, fcsr, zero<br> [0x8000018c]:sw t3, 24(ra)<br>    |
|   5|[0x80009434]<br>0x00000000<br> |- rs1 : x26<br> - rs2 : x28<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800001a4]:feq.h s11, s10, t3<br> [0x800001a8]:csrrs tp, fcsr, zero<br> [0x800001ac]:sw s11, 32(ra)<br>  |
|   6|[0x8000943c]<br>0x00000000<br> |- rs1 : x27<br> - rs2 : x25<br> - rd : x26<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800001c4]:feq.h s10, s11, s9<br> [0x800001c8]:csrrs tp, fcsr, zero<br> [0x800001cc]:sw s10, 40(ra)<br>  |
|   7|[0x80009444]<br>0x00000000<br> |- rs1 : x24<br> - rs2 : x26<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800001e4]:feq.h s9, s8, s10<br> [0x800001e8]:csrrs tp, fcsr, zero<br> [0x800001ec]:sw s9, 48(ra)<br>    |
|   8|[0x8000944c]<br>0x00000000<br> |- rs1 : x25<br> - rs2 : x23<br> - rd : x24<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000204]:feq.h s8, s9, s7<br> [0x80000208]:csrrs tp, fcsr, zero<br> [0x8000020c]:sw s8, 56(ra)<br>     |
|   9|[0x80009454]<br>0x00000000<br> |- rs1 : x22<br> - rs2 : x24<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000224]:feq.h s7, s6, s8<br> [0x80000228]:csrrs tp, fcsr, zero<br> [0x8000022c]:sw s7, 64(ra)<br>     |
|  10|[0x8000945c]<br>0x00000000<br> |- rs1 : x23<br> - rs2 : x21<br> - rd : x22<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000244]:feq.h s6, s7, s5<br> [0x80000248]:csrrs tp, fcsr, zero<br> [0x8000024c]:sw s6, 72(ra)<br>     |
|  11|[0x80009464]<br>0x00000000<br> |- rs1 : x20<br> - rs2 : x22<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000264]:feq.h s5, s4, s6<br> [0x80000268]:csrrs tp, fcsr, zero<br> [0x8000026c]:sw s5, 80(ra)<br>     |
|  12|[0x8000946c]<br>0x00000000<br> |- rs1 : x21<br> - rs2 : x19<br> - rd : x20<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000284]:feq.h s4, s5, s3<br> [0x80000288]:csrrs tp, fcsr, zero<br> [0x8000028c]:sw s4, 88(ra)<br>     |
|  13|[0x80009474]<br>0x00000000<br> |- rs1 : x18<br> - rs2 : x20<br> - rd : x19<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800002a4]:feq.h s3, s2, s4<br> [0x800002a8]:csrrs tp, fcsr, zero<br> [0x800002ac]:sw s3, 96(ra)<br>     |
|  14|[0x8000947c]<br>0x00000000<br> |- rs1 : x19<br> - rs2 : x17<br> - rd : x18<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800002c4]:feq.h s2, s3, a7<br> [0x800002c8]:csrrs tp, fcsr, zero<br> [0x800002cc]:sw s2, 104(ra)<br>    |
|  15|[0x80009484]<br>0x00000000<br> |- rs1 : x16<br> - rs2 : x18<br> - rd : x17<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800002e4]:feq.h a7, a6, s2<br> [0x800002e8]:csrrs tp, fcsr, zero<br> [0x800002ec]:sw a7, 112(ra)<br>    |
|  16|[0x8000948c]<br>0x00000000<br> |- rs1 : x17<br> - rs2 : x15<br> - rd : x16<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000304]:feq.h a6, a7, a5<br> [0x80000308]:csrrs tp, fcsr, zero<br> [0x8000030c]:sw a6, 120(ra)<br>    |
|  17|[0x80009494]<br>0x00000000<br> |- rs1 : x14<br> - rs2 : x16<br> - rd : x15<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000324]:feq.h a5, a4, a6<br> [0x80000328]:csrrs tp, fcsr, zero<br> [0x8000032c]:sw a5, 128(ra)<br>    |
|  18|[0x8000949c]<br>0x00000000<br> |- rs1 : x15<br> - rs2 : x13<br> - rd : x14<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000344]:feq.h a4, a5, a3<br> [0x80000348]:csrrs tp, fcsr, zero<br> [0x8000034c]:sw a4, 136(ra)<br>    |
|  19|[0x800094a4]<br>0x00000000<br> |- rs1 : x12<br> - rs2 : x14<br> - rd : x13<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000364]:feq.h a3, a2, a4<br> [0x80000368]:csrrs tp, fcsr, zero<br> [0x8000036c]:sw a3, 144(ra)<br>    |
|  20|[0x800094ac]<br>0x00000000<br> |- rs1 : x13<br> - rs2 : x11<br> - rd : x12<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x80000384]:feq.h a2, a3, a1<br> [0x80000388]:csrrs tp, fcsr, zero<br> [0x8000038c]:sw a2, 152(ra)<br>    |
|  21|[0x800094b4]<br>0x00000000<br> |- rs1 : x10<br> - rs2 : x12<br> - rd : x11<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                         |[0x800003a4]:feq.h a1, a0, a2<br> [0x800003a8]:csrrs tp, fcsr, zero<br> [0x800003ac]:sw a1, 160(ra)<br>    |
|  22|[0x800094bc]<br>0x00000000<br> |- rs1 : x11<br> - rs2 : x9<br> - rd : x10<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                          |[0x800003c4]:feq.h a0, a1, s1<br> [0x800003c8]:csrrs tp, fcsr, zero<br> [0x800003cc]:sw a0, 168(ra)<br>    |
|  23|[0x800094c4]<br>0x00000000<br> |- rs1 : x8<br> - rs2 : x10<br> - rd : x9<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                           |[0x800003ec]:feq.h s1, fp, a0<br> [0x800003f0]:csrrs a2, fcsr, zero<br> [0x800003f4]:sw s1, 176(ra)<br>    |
|  24|[0x800094cc]<br>0x00000000<br> |- rs1 : x9<br> - rs2 : x7<br> - rd : x8<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x8000040c]:feq.h fp, s1, t2<br> [0x80000410]:csrrs a2, fcsr, zero<br> [0x80000414]:sw fp, 184(ra)<br>    |
|  25|[0x800094d4]<br>0x00000001<br> |- rs1 : x6<br> - rs2 : x8<br> - rd : x7<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x8000042c]:feq.h t2, t1, fp<br> [0x80000430]:csrrs a2, fcsr, zero<br> [0x80000434]:sw t2, 192(ra)<br>    |
|  26|[0x800094dc]<br>0x00000001<br> |- rs1 : x7<br> - rs2 : x5<br> - rd : x6<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x80000454]:feq.h t1, t2, t0<br> [0x80000458]:csrrs a2, fcsr, zero<br> [0x8000045c]:sw t1, 0(fp)<br>      |
|  27|[0x800094e4]<br>0x00000000<br> |- rs1 : x4<br> - rs2 : x6<br> - rd : x5<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x80000474]:feq.h t0, tp, t1<br> [0x80000478]:csrrs a2, fcsr, zero<br> [0x8000047c]:sw t0, 8(fp)<br>      |
|  28|[0x800094ec]<br>0x00000000<br> |- rs1 : x5<br> - rs2 : x3<br> - rd : x4<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x80000494]:feq.h tp, t0, gp<br> [0x80000498]:csrrs a2, fcsr, zero<br> [0x8000049c]:sw tp, 16(fp)<br>     |
|  29|[0x800094f4]<br>0x00000000<br> |- rs1 : x2<br> - rs2 : x4<br> - rd : x3<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x800004b4]:feq.h gp, sp, tp<br> [0x800004b8]:csrrs a2, fcsr, zero<br> [0x800004bc]:sw gp, 24(fp)<br>     |
|  30|[0x800094fc]<br>0x00000000<br> |- rs1 : x3<br> - rs2 : x1<br> - rd : x2<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                            |[0x800004d4]:feq.h sp, gp, ra<br> [0x800004d8]:csrrs a2, fcsr, zero<br> [0x800004dc]:sw sp, 32(fp)<br>     |
|  31|[0x80009504]<br>0x00000000<br> |- rs1 : x0<br> - rs2 : x2<br> - rd : x1<br>                                                                                                                                                                                                                           |[0x800004f4]:feq.h ra, zero, sp<br> [0x800004f8]:csrrs a2, fcsr, zero<br> [0x800004fc]:sw ra, 40(fp)<br>   |
|  32|[0x8000950c]<br>0x00000000<br> |- rs1 : x1<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                         |[0x80000514]:feq.h t6, ra, t5<br> [0x80000518]:csrrs a2, fcsr, zero<br> [0x8000051c]:sw t6, 48(fp)<br>     |
|  33|[0x80009514]<br>0x00000001<br> |- rs2 : x0<br>                                                                                                                                                                                                                                                        |[0x80000534]:feq.h t6, t5, zero<br> [0x80000538]:csrrs a2, fcsr, zero<br> [0x8000053c]:sw t6, 56(fp)<br>   |
|  34|[0x8000951c]<br>0x00000000<br> |- rd : x0<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                          |[0x80000554]:feq.h zero, t6, t5<br> [0x80000558]:csrrs a2, fcsr, zero<br> [0x8000055c]:sw zero, 64(fp)<br> |
|  35|[0x80009524]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000574]:feq.h t6, t5, t4<br> [0x80000578]:csrrs a2, fcsr, zero<br> [0x8000057c]:sw t6, 72(fp)<br>     |
|  36|[0x8000952c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000594]:feq.h t6, t5, t4<br> [0x80000598]:csrrs a2, fcsr, zero<br> [0x8000059c]:sw t6, 80(fp)<br>     |
|  37|[0x80009534]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800005b4]:feq.h t6, t5, t4<br> [0x800005b8]:csrrs a2, fcsr, zero<br> [0x800005bc]:sw t6, 88(fp)<br>     |
|  38|[0x8000953c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800005d4]:feq.h t6, t5, t4<br> [0x800005d8]:csrrs a2, fcsr, zero<br> [0x800005dc]:sw t6, 96(fp)<br>     |
|  39|[0x80009544]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800005f4]:feq.h t6, t5, t4<br> [0x800005f8]:csrrs a2, fcsr, zero<br> [0x800005fc]:sw t6, 104(fp)<br>    |
|  40|[0x8000954c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000614]:feq.h t6, t5, t4<br> [0x80000618]:csrrs a2, fcsr, zero<br> [0x8000061c]:sw t6, 112(fp)<br>    |
|  41|[0x80009554]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000634]:feq.h t6, t5, t4<br> [0x80000638]:csrrs a2, fcsr, zero<br> [0x8000063c]:sw t6, 120(fp)<br>    |
|  42|[0x8000955c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000654]:feq.h t6, t5, t4<br> [0x80000658]:csrrs a2, fcsr, zero<br> [0x8000065c]:sw t6, 128(fp)<br>    |
|  43|[0x80009564]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000674]:feq.h t6, t5, t4<br> [0x80000678]:csrrs a2, fcsr, zero<br> [0x8000067c]:sw t6, 136(fp)<br>    |
|  44|[0x8000956c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000694]:feq.h t6, t5, t4<br> [0x80000698]:csrrs a2, fcsr, zero<br> [0x8000069c]:sw t6, 144(fp)<br>    |
|  45|[0x80009574]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800006b4]:feq.h t6, t5, t4<br> [0x800006b8]:csrrs a2, fcsr, zero<br> [0x800006bc]:sw t6, 152(fp)<br>    |
|  46|[0x8000957c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800006d4]:feq.h t6, t5, t4<br> [0x800006d8]:csrrs a2, fcsr, zero<br> [0x800006dc]:sw t6, 160(fp)<br>    |
|  47|[0x80009584]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800006f4]:feq.h t6, t5, t4<br> [0x800006f8]:csrrs a2, fcsr, zero<br> [0x800006fc]:sw t6, 168(fp)<br>    |
|  48|[0x8000958c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000714]:feq.h t6, t5, t4<br> [0x80000718]:csrrs a2, fcsr, zero<br> [0x8000071c]:sw t6, 176(fp)<br>    |
|  49|[0x80009594]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000734]:feq.h t6, t5, t4<br> [0x80000738]:csrrs a2, fcsr, zero<br> [0x8000073c]:sw t6, 184(fp)<br>    |
|  50|[0x8000959c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000754]:feq.h t6, t5, t4<br> [0x80000758]:csrrs a2, fcsr, zero<br> [0x8000075c]:sw t6, 192(fp)<br>    |
|  51|[0x800095a4]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000774]:feq.h t6, t5, t4<br> [0x80000778]:csrrs a2, fcsr, zero<br> [0x8000077c]:sw t6, 200(fp)<br>    |
|  52|[0x800095ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000794]:feq.h t6, t5, t4<br> [0x80000798]:csrrs a2, fcsr, zero<br> [0x8000079c]:sw t6, 208(fp)<br>    |
|  53|[0x800095b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800007b4]:feq.h t6, t5, t4<br> [0x800007b8]:csrrs a2, fcsr, zero<br> [0x800007bc]:sw t6, 216(fp)<br>    |
|  54|[0x800095bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800007d4]:feq.h t6, t5, t4<br> [0x800007d8]:csrrs a2, fcsr, zero<br> [0x800007dc]:sw t6, 224(fp)<br>    |
|  55|[0x800095c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800007f4]:feq.h t6, t5, t4<br> [0x800007f8]:csrrs a2, fcsr, zero<br> [0x800007fc]:sw t6, 232(fp)<br>    |
|  56|[0x800095cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000814]:feq.h t6, t5, t4<br> [0x80000818]:csrrs a2, fcsr, zero<br> [0x8000081c]:sw t6, 240(fp)<br>    |
|  57|[0x800095d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000834]:feq.h t6, t5, t4<br> [0x80000838]:csrrs a2, fcsr, zero<br> [0x8000083c]:sw t6, 248(fp)<br>    |
|  58|[0x800095dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000854]:feq.h t6, t5, t4<br> [0x80000858]:csrrs a2, fcsr, zero<br> [0x8000085c]:sw t6, 256(fp)<br>    |
|  59|[0x800095e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000874]:feq.h t6, t5, t4<br> [0x80000878]:csrrs a2, fcsr, zero<br> [0x8000087c]:sw t6, 264(fp)<br>    |
|  60|[0x800095ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000894]:feq.h t6, t5, t4<br> [0x80000898]:csrrs a2, fcsr, zero<br> [0x8000089c]:sw t6, 272(fp)<br>    |
|  61|[0x800095f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800008b4]:feq.h t6, t5, t4<br> [0x800008b8]:csrrs a2, fcsr, zero<br> [0x800008bc]:sw t6, 280(fp)<br>    |
|  62|[0x800095fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800008d4]:feq.h t6, t5, t4<br> [0x800008d8]:csrrs a2, fcsr, zero<br> [0x800008dc]:sw t6, 288(fp)<br>    |
|  63|[0x80009604]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800008f4]:feq.h t6, t5, t4<br> [0x800008f8]:csrrs a2, fcsr, zero<br> [0x800008fc]:sw t6, 296(fp)<br>    |
|  64|[0x8000960c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000914]:feq.h t6, t5, t4<br> [0x80000918]:csrrs a2, fcsr, zero<br> [0x8000091c]:sw t6, 304(fp)<br>    |
|  65|[0x80009614]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000934]:feq.h t6, t5, t4<br> [0x80000938]:csrrs a2, fcsr, zero<br> [0x8000093c]:sw t6, 312(fp)<br>    |
|  66|[0x8000961c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000954]:feq.h t6, t5, t4<br> [0x80000958]:csrrs a2, fcsr, zero<br> [0x8000095c]:sw t6, 320(fp)<br>    |
|  67|[0x80009624]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000974]:feq.h t6, t5, t4<br> [0x80000978]:csrrs a2, fcsr, zero<br> [0x8000097c]:sw t6, 328(fp)<br>    |
|  68|[0x8000962c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000994]:feq.h t6, t5, t4<br> [0x80000998]:csrrs a2, fcsr, zero<br> [0x8000099c]:sw t6, 336(fp)<br>    |
|  69|[0x80009634]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800009b4]:feq.h t6, t5, t4<br> [0x800009b8]:csrrs a2, fcsr, zero<br> [0x800009bc]:sw t6, 344(fp)<br>    |
|  70|[0x8000963c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800009d4]:feq.h t6, t5, t4<br> [0x800009d8]:csrrs a2, fcsr, zero<br> [0x800009dc]:sw t6, 352(fp)<br>    |
|  71|[0x80009644]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800009f4]:feq.h t6, t5, t4<br> [0x800009f8]:csrrs a2, fcsr, zero<br> [0x800009fc]:sw t6, 360(fp)<br>    |
|  72|[0x8000964c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000a14]:feq.h t6, t5, t4<br> [0x80000a18]:csrrs a2, fcsr, zero<br> [0x80000a1c]:sw t6, 368(fp)<br>    |
|  73|[0x80009654]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000a34]:feq.h t6, t5, t4<br> [0x80000a38]:csrrs a2, fcsr, zero<br> [0x80000a3c]:sw t6, 376(fp)<br>    |
|  74|[0x8000965c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000a54]:feq.h t6, t5, t4<br> [0x80000a58]:csrrs a2, fcsr, zero<br> [0x80000a5c]:sw t6, 384(fp)<br>    |
|  75|[0x80009664]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000a74]:feq.h t6, t5, t4<br> [0x80000a78]:csrrs a2, fcsr, zero<br> [0x80000a7c]:sw t6, 392(fp)<br>    |
|  76|[0x8000966c]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000a94]:feq.h t6, t5, t4<br> [0x80000a98]:csrrs a2, fcsr, zero<br> [0x80000a9c]:sw t6, 400(fp)<br>    |
|  77|[0x80009674]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000ab4]:feq.h t6, t5, t4<br> [0x80000ab8]:csrrs a2, fcsr, zero<br> [0x80000abc]:sw t6, 408(fp)<br>    |
|  78|[0x8000967c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000ad4]:feq.h t6, t5, t4<br> [0x80000ad8]:csrrs a2, fcsr, zero<br> [0x80000adc]:sw t6, 416(fp)<br>    |
|  79|[0x80009684]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000af4]:feq.h t6, t5, t4<br> [0x80000af8]:csrrs a2, fcsr, zero<br> [0x80000afc]:sw t6, 424(fp)<br>    |
|  80|[0x8000968c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000b14]:feq.h t6, t5, t4<br> [0x80000b18]:csrrs a2, fcsr, zero<br> [0x80000b1c]:sw t6, 432(fp)<br>    |
|  81|[0x80009694]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000b34]:feq.h t6, t5, t4<br> [0x80000b38]:csrrs a2, fcsr, zero<br> [0x80000b3c]:sw t6, 440(fp)<br>    |
|  82|[0x8000969c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000b54]:feq.h t6, t5, t4<br> [0x80000b58]:csrrs a2, fcsr, zero<br> [0x80000b5c]:sw t6, 448(fp)<br>    |
|  83|[0x800096a4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000b74]:feq.h t6, t5, t4<br> [0x80000b78]:csrrs a2, fcsr, zero<br> [0x80000b7c]:sw t6, 456(fp)<br>    |
|  84|[0x800096ac]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000b94]:feq.h t6, t5, t4<br> [0x80000b98]:csrrs a2, fcsr, zero<br> [0x80000b9c]:sw t6, 464(fp)<br>    |
|  85|[0x800096b4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000bb4]:feq.h t6, t5, t4<br> [0x80000bb8]:csrrs a2, fcsr, zero<br> [0x80000bbc]:sw t6, 472(fp)<br>    |
|  86|[0x800096bc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000bd4]:feq.h t6, t5, t4<br> [0x80000bd8]:csrrs a2, fcsr, zero<br> [0x80000bdc]:sw t6, 480(fp)<br>    |
|  87|[0x800096c4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000bf4]:feq.h t6, t5, t4<br> [0x80000bf8]:csrrs a2, fcsr, zero<br> [0x80000bfc]:sw t6, 488(fp)<br>    |
|  88|[0x800096cc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000c14]:feq.h t6, t5, t4<br> [0x80000c18]:csrrs a2, fcsr, zero<br> [0x80000c1c]:sw t6, 496(fp)<br>    |
|  89|[0x800096d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000c34]:feq.h t6, t5, t4<br> [0x80000c38]:csrrs a2, fcsr, zero<br> [0x80000c3c]:sw t6, 504(fp)<br>    |
|  90|[0x800096dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000c54]:feq.h t6, t5, t4<br> [0x80000c58]:csrrs a2, fcsr, zero<br> [0x80000c5c]:sw t6, 512(fp)<br>    |
|  91|[0x800096e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000c74]:feq.h t6, t5, t4<br> [0x80000c78]:csrrs a2, fcsr, zero<br> [0x80000c7c]:sw t6, 520(fp)<br>    |
|  92|[0x800096ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000c94]:feq.h t6, t5, t4<br> [0x80000c98]:csrrs a2, fcsr, zero<br> [0x80000c9c]:sw t6, 528(fp)<br>    |
|  93|[0x800096f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000cb4]:feq.h t6, t5, t4<br> [0x80000cb8]:csrrs a2, fcsr, zero<br> [0x80000cbc]:sw t6, 536(fp)<br>    |
|  94|[0x800096fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000cd4]:feq.h t6, t5, t4<br> [0x80000cd8]:csrrs a2, fcsr, zero<br> [0x80000cdc]:sw t6, 544(fp)<br>    |
|  95|[0x80009704]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000cf4]:feq.h t6, t5, t4<br> [0x80000cf8]:csrrs a2, fcsr, zero<br> [0x80000cfc]:sw t6, 552(fp)<br>    |
|  96|[0x8000970c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000d14]:feq.h t6, t5, t4<br> [0x80000d18]:csrrs a2, fcsr, zero<br> [0x80000d1c]:sw t6, 560(fp)<br>    |
|  97|[0x80009714]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000d34]:feq.h t6, t5, t4<br> [0x80000d38]:csrrs a2, fcsr, zero<br> [0x80000d3c]:sw t6, 568(fp)<br>    |
|  98|[0x8000971c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000d54]:feq.h t6, t5, t4<br> [0x80000d58]:csrrs a2, fcsr, zero<br> [0x80000d5c]:sw t6, 576(fp)<br>    |
|  99|[0x80009724]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000d74]:feq.h t6, t5, t4<br> [0x80000d78]:csrrs a2, fcsr, zero<br> [0x80000d7c]:sw t6, 584(fp)<br>    |
| 100|[0x8000972c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000d94]:feq.h t6, t5, t4<br> [0x80000d98]:csrrs a2, fcsr, zero<br> [0x80000d9c]:sw t6, 592(fp)<br>    |
| 101|[0x80009734]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000db4]:feq.h t6, t5, t4<br> [0x80000db8]:csrrs a2, fcsr, zero<br> [0x80000dbc]:sw t6, 600(fp)<br>    |
| 102|[0x8000973c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000dd4]:feq.h t6, t5, t4<br> [0x80000dd8]:csrrs a2, fcsr, zero<br> [0x80000ddc]:sw t6, 608(fp)<br>    |
| 103|[0x80009744]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000df4]:feq.h t6, t5, t4<br> [0x80000df8]:csrrs a2, fcsr, zero<br> [0x80000dfc]:sw t6, 616(fp)<br>    |
| 104|[0x8000974c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000e14]:feq.h t6, t5, t4<br> [0x80000e18]:csrrs a2, fcsr, zero<br> [0x80000e1c]:sw t6, 624(fp)<br>    |
| 105|[0x80009754]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000e34]:feq.h t6, t5, t4<br> [0x80000e38]:csrrs a2, fcsr, zero<br> [0x80000e3c]:sw t6, 632(fp)<br>    |
| 106|[0x8000975c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000e54]:feq.h t6, t5, t4<br> [0x80000e58]:csrrs a2, fcsr, zero<br> [0x80000e5c]:sw t6, 640(fp)<br>    |
| 107|[0x80009764]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000e74]:feq.h t6, t5, t4<br> [0x80000e78]:csrrs a2, fcsr, zero<br> [0x80000e7c]:sw t6, 648(fp)<br>    |
| 108|[0x8000976c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000e94]:feq.h t6, t5, t4<br> [0x80000e98]:csrrs a2, fcsr, zero<br> [0x80000e9c]:sw t6, 656(fp)<br>    |
| 109|[0x80009774]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000eb4]:feq.h t6, t5, t4<br> [0x80000eb8]:csrrs a2, fcsr, zero<br> [0x80000ebc]:sw t6, 664(fp)<br>    |
| 110|[0x8000977c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000ed4]:feq.h t6, t5, t4<br> [0x80000ed8]:csrrs a2, fcsr, zero<br> [0x80000edc]:sw t6, 672(fp)<br>    |
| 111|[0x80009784]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000ef4]:feq.h t6, t5, t4<br> [0x80000ef8]:csrrs a2, fcsr, zero<br> [0x80000efc]:sw t6, 680(fp)<br>    |
| 112|[0x8000978c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000f14]:feq.h t6, t5, t4<br> [0x80000f18]:csrrs a2, fcsr, zero<br> [0x80000f1c]:sw t6, 688(fp)<br>    |
| 113|[0x80009794]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000f34]:feq.h t6, t5, t4<br> [0x80000f38]:csrrs a2, fcsr, zero<br> [0x80000f3c]:sw t6, 696(fp)<br>    |
| 114|[0x8000979c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000f54]:feq.h t6, t5, t4<br> [0x80000f58]:csrrs a2, fcsr, zero<br> [0x80000f5c]:sw t6, 704(fp)<br>    |
| 115|[0x800097a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000f74]:feq.h t6, t5, t4<br> [0x80000f78]:csrrs a2, fcsr, zero<br> [0x80000f7c]:sw t6, 712(fp)<br>    |
| 116|[0x800097ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000f94]:feq.h t6, t5, t4<br> [0x80000f98]:csrrs a2, fcsr, zero<br> [0x80000f9c]:sw t6, 720(fp)<br>    |
| 117|[0x800097b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000fb4]:feq.h t6, t5, t4<br> [0x80000fb8]:csrrs a2, fcsr, zero<br> [0x80000fbc]:sw t6, 728(fp)<br>    |
| 118|[0x800097bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000fd4]:feq.h t6, t5, t4<br> [0x80000fd8]:csrrs a2, fcsr, zero<br> [0x80000fdc]:sw t6, 736(fp)<br>    |
| 119|[0x800097c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80000ff4]:feq.h t6, t5, t4<br> [0x80000ff8]:csrrs a2, fcsr, zero<br> [0x80000ffc]:sw t6, 744(fp)<br>    |
| 120|[0x800097cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x002 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001014]:feq.h t6, t5, t4<br> [0x80001018]:csrrs a2, fcsr, zero<br> [0x8000101c]:sw t6, 752(fp)<br>    |
| 121|[0x800097d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001034]:feq.h t6, t5, t4<br> [0x80001038]:csrrs a2, fcsr, zero<br> [0x8000103c]:sw t6, 760(fp)<br>    |
| 122|[0x800097dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001054]:feq.h t6, t5, t4<br> [0x80001058]:csrrs a2, fcsr, zero<br> [0x8000105c]:sw t6, 768(fp)<br>    |
| 123|[0x800097e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001074]:feq.h t6, t5, t4<br> [0x80001078]:csrrs a2, fcsr, zero<br> [0x8000107c]:sw t6, 776(fp)<br>    |
| 124|[0x800097ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001094]:feq.h t6, t5, t4<br> [0x80001098]:csrrs a2, fcsr, zero<br> [0x8000109c]:sw t6, 784(fp)<br>    |
| 125|[0x800097f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800010b4]:feq.h t6, t5, t4<br> [0x800010b8]:csrrs a2, fcsr, zero<br> [0x800010bc]:sw t6, 792(fp)<br>    |
| 126|[0x800097fc]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800010d4]:feq.h t6, t5, t4<br> [0x800010d8]:csrrs a2, fcsr, zero<br> [0x800010dc]:sw t6, 800(fp)<br>    |
| 127|[0x80009804]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800010f4]:feq.h t6, t5, t4<br> [0x800010f8]:csrrs a2, fcsr, zero<br> [0x800010fc]:sw t6, 808(fp)<br>    |
| 128|[0x8000980c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001114]:feq.h t6, t5, t4<br> [0x80001118]:csrrs a2, fcsr, zero<br> [0x8000111c]:sw t6, 816(fp)<br>    |
| 129|[0x80009814]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001134]:feq.h t6, t5, t4<br> [0x80001138]:csrrs a2, fcsr, zero<br> [0x8000113c]:sw t6, 824(fp)<br>    |
| 130|[0x8000981c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001154]:feq.h t6, t5, t4<br> [0x80001158]:csrrs a2, fcsr, zero<br> [0x8000115c]:sw t6, 832(fp)<br>    |
| 131|[0x80009824]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001174]:feq.h t6, t5, t4<br> [0x80001178]:csrrs a2, fcsr, zero<br> [0x8000117c]:sw t6, 840(fp)<br>    |
| 132|[0x8000982c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001194]:feq.h t6, t5, t4<br> [0x80001198]:csrrs a2, fcsr, zero<br> [0x8000119c]:sw t6, 848(fp)<br>    |
| 133|[0x80009834]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800011b4]:feq.h t6, t5, t4<br> [0x800011b8]:csrrs a2, fcsr, zero<br> [0x800011bc]:sw t6, 856(fp)<br>    |
| 134|[0x8000983c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800011d4]:feq.h t6, t5, t4<br> [0x800011d8]:csrrs a2, fcsr, zero<br> [0x800011dc]:sw t6, 864(fp)<br>    |
| 135|[0x80009844]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800011f4]:feq.h t6, t5, t4<br> [0x800011f8]:csrrs a2, fcsr, zero<br> [0x800011fc]:sw t6, 872(fp)<br>    |
| 136|[0x8000984c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001214]:feq.h t6, t5, t4<br> [0x80001218]:csrrs a2, fcsr, zero<br> [0x8000121c]:sw t6, 880(fp)<br>    |
| 137|[0x80009854]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001234]:feq.h t6, t5, t4<br> [0x80001238]:csrrs a2, fcsr, zero<br> [0x8000123c]:sw t6, 888(fp)<br>    |
| 138|[0x8000985c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001254]:feq.h t6, t5, t4<br> [0x80001258]:csrrs a2, fcsr, zero<br> [0x8000125c]:sw t6, 896(fp)<br>    |
| 139|[0x80009864]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001274]:feq.h t6, t5, t4<br> [0x80001278]:csrrs a2, fcsr, zero<br> [0x8000127c]:sw t6, 904(fp)<br>    |
| 140|[0x8000986c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001294]:feq.h t6, t5, t4<br> [0x80001298]:csrrs a2, fcsr, zero<br> [0x8000129c]:sw t6, 912(fp)<br>    |
| 141|[0x80009874]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800012b4]:feq.h t6, t5, t4<br> [0x800012b8]:csrrs a2, fcsr, zero<br> [0x800012bc]:sw t6, 920(fp)<br>    |
| 142|[0x8000987c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800012d4]:feq.h t6, t5, t4<br> [0x800012d8]:csrrs a2, fcsr, zero<br> [0x800012dc]:sw t6, 928(fp)<br>    |
| 143|[0x80009884]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800012f4]:feq.h t6, t5, t4<br> [0x800012f8]:csrrs a2, fcsr, zero<br> [0x800012fc]:sw t6, 936(fp)<br>    |
| 144|[0x8000988c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3fe and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001314]:feq.h t6, t5, t4<br> [0x80001318]:csrrs a2, fcsr, zero<br> [0x8000131c]:sw t6, 944(fp)<br>    |
| 145|[0x80009894]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001334]:feq.h t6, t5, t4<br> [0x80001338]:csrrs a2, fcsr, zero<br> [0x8000133c]:sw t6, 952(fp)<br>    |
| 146|[0x8000989c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001354]:feq.h t6, t5, t4<br> [0x80001358]:csrrs a2, fcsr, zero<br> [0x8000135c]:sw t6, 960(fp)<br>    |
| 147|[0x800098a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001374]:feq.h t6, t5, t4<br> [0x80001378]:csrrs a2, fcsr, zero<br> [0x8000137c]:sw t6, 968(fp)<br>    |
| 148|[0x800098ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001394]:feq.h t6, t5, t4<br> [0x80001398]:csrrs a2, fcsr, zero<br> [0x8000139c]:sw t6, 976(fp)<br>    |
| 149|[0x800098b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800013b4]:feq.h t6, t5, t4<br> [0x800013b8]:csrrs a2, fcsr, zero<br> [0x800013bc]:sw t6, 984(fp)<br>    |
| 150|[0x800098bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800013d4]:feq.h t6, t5, t4<br> [0x800013d8]:csrrs a2, fcsr, zero<br> [0x800013dc]:sw t6, 992(fp)<br>    |
| 151|[0x800098c4]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800013f4]:feq.h t6, t5, t4<br> [0x800013f8]:csrrs a2, fcsr, zero<br> [0x800013fc]:sw t6, 1000(fp)<br>   |
| 152|[0x800098cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001414]:feq.h t6, t5, t4<br> [0x80001418]:csrrs a2, fcsr, zero<br> [0x8000141c]:sw t6, 1008(fp)<br>   |
| 153|[0x800098d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001434]:feq.h t6, t5, t4<br> [0x80001438]:csrrs a2, fcsr, zero<br> [0x8000143c]:sw t6, 1016(fp)<br>   |
| 154|[0x800098dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000145c]:feq.h t6, t5, t4<br> [0x80001460]:csrrs a2, fcsr, zero<br> [0x80001464]:sw t6, 0(fp)<br>      |
| 155|[0x800098e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000147c]:feq.h t6, t5, t4<br> [0x80001480]:csrrs a2, fcsr, zero<br> [0x80001484]:sw t6, 8(fp)<br>      |
| 156|[0x800098ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000149c]:feq.h t6, t5, t4<br> [0x800014a0]:csrrs a2, fcsr, zero<br> [0x800014a4]:sw t6, 16(fp)<br>     |
| 157|[0x800098f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800014bc]:feq.h t6, t5, t4<br> [0x800014c0]:csrrs a2, fcsr, zero<br> [0x800014c4]:sw t6, 24(fp)<br>     |
| 158|[0x800098fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800014dc]:feq.h t6, t5, t4<br> [0x800014e0]:csrrs a2, fcsr, zero<br> [0x800014e4]:sw t6, 32(fp)<br>     |
| 159|[0x80009904]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800014fc]:feq.h t6, t5, t4<br> [0x80001500]:csrrs a2, fcsr, zero<br> [0x80001504]:sw t6, 40(fp)<br>     |
| 160|[0x8000990c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000151c]:feq.h t6, t5, t4<br> [0x80001520]:csrrs a2, fcsr, zero<br> [0x80001524]:sw t6, 48(fp)<br>     |
| 161|[0x80009914]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000153c]:feq.h t6, t5, t4<br> [0x80001540]:csrrs a2, fcsr, zero<br> [0x80001544]:sw t6, 56(fp)<br>     |
| 162|[0x8000991c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000155c]:feq.h t6, t5, t4<br> [0x80001560]:csrrs a2, fcsr, zero<br> [0x80001564]:sw t6, 64(fp)<br>     |
| 163|[0x80009924]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000157c]:feq.h t6, t5, t4<br> [0x80001580]:csrrs a2, fcsr, zero<br> [0x80001584]:sw t6, 72(fp)<br>     |
| 164|[0x8000992c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000159c]:feq.h t6, t5, t4<br> [0x800015a0]:csrrs a2, fcsr, zero<br> [0x800015a4]:sw t6, 80(fp)<br>     |
| 165|[0x80009934]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800015bc]:feq.h t6, t5, t4<br> [0x800015c0]:csrrs a2, fcsr, zero<br> [0x800015c4]:sw t6, 88(fp)<br>     |
| 166|[0x8000993c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800015dc]:feq.h t6, t5, t4<br> [0x800015e0]:csrrs a2, fcsr, zero<br> [0x800015e4]:sw t6, 96(fp)<br>     |
| 167|[0x80009944]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800015fc]:feq.h t6, t5, t4<br> [0x80001600]:csrrs a2, fcsr, zero<br> [0x80001604]:sw t6, 104(fp)<br>    |
| 168|[0x8000994c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000161c]:feq.h t6, t5, t4<br> [0x80001620]:csrrs a2, fcsr, zero<br> [0x80001624]:sw t6, 112(fp)<br>    |
| 169|[0x80009954]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000163c]:feq.h t6, t5, t4<br> [0x80001640]:csrrs a2, fcsr, zero<br> [0x80001644]:sw t6, 120(fp)<br>    |
| 170|[0x8000995c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000165c]:feq.h t6, t5, t4<br> [0x80001660]:csrrs a2, fcsr, zero<br> [0x80001664]:sw t6, 128(fp)<br>    |
| 171|[0x80009964]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000167c]:feq.h t6, t5, t4<br> [0x80001680]:csrrs a2, fcsr, zero<br> [0x80001684]:sw t6, 136(fp)<br>    |
| 172|[0x8000996c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000169c]:feq.h t6, t5, t4<br> [0x800016a0]:csrrs a2, fcsr, zero<br> [0x800016a4]:sw t6, 144(fp)<br>    |
| 173|[0x80009974]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800016bc]:feq.h t6, t5, t4<br> [0x800016c0]:csrrs a2, fcsr, zero<br> [0x800016c4]:sw t6, 152(fp)<br>    |
| 174|[0x8000997c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800016dc]:feq.h t6, t5, t4<br> [0x800016e0]:csrrs a2, fcsr, zero<br> [0x800016e4]:sw t6, 160(fp)<br>    |
| 175|[0x80009984]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800016fc]:feq.h t6, t5, t4<br> [0x80001700]:csrrs a2, fcsr, zero<br> [0x80001704]:sw t6, 168(fp)<br>    |
| 176|[0x8000998c]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000171c]:feq.h t6, t5, t4<br> [0x80001720]:csrrs a2, fcsr, zero<br> [0x80001724]:sw t6, 176(fp)<br>    |
| 177|[0x80009994]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000173c]:feq.h t6, t5, t4<br> [0x80001740]:csrrs a2, fcsr, zero<br> [0x80001744]:sw t6, 184(fp)<br>    |
| 178|[0x8000999c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000175c]:feq.h t6, t5, t4<br> [0x80001760]:csrrs a2, fcsr, zero<br> [0x80001764]:sw t6, 192(fp)<br>    |
| 179|[0x800099a4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000177c]:feq.h t6, t5, t4<br> [0x80001780]:csrrs a2, fcsr, zero<br> [0x80001784]:sw t6, 200(fp)<br>    |
| 180|[0x800099ac]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000179c]:feq.h t6, t5, t4<br> [0x800017a0]:csrrs a2, fcsr, zero<br> [0x800017a4]:sw t6, 208(fp)<br>    |
| 181|[0x800099b4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800017bc]:feq.h t6, t5, t4<br> [0x800017c0]:csrrs a2, fcsr, zero<br> [0x800017c4]:sw t6, 216(fp)<br>    |
| 182|[0x800099bc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800017dc]:feq.h t6, t5, t4<br> [0x800017e0]:csrrs a2, fcsr, zero<br> [0x800017e4]:sw t6, 224(fp)<br>    |
| 183|[0x800099c4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800017fc]:feq.h t6, t5, t4<br> [0x80001800]:csrrs a2, fcsr, zero<br> [0x80001804]:sw t6, 232(fp)<br>    |
| 184|[0x800099cc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000181c]:feq.h t6, t5, t4<br> [0x80001820]:csrrs a2, fcsr, zero<br> [0x80001824]:sw t6, 240(fp)<br>    |
| 185|[0x800099d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000183c]:feq.h t6, t5, t4<br> [0x80001840]:csrrs a2, fcsr, zero<br> [0x80001844]:sw t6, 248(fp)<br>    |
| 186|[0x800099dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000185c]:feq.h t6, t5, t4<br> [0x80001860]:csrrs a2, fcsr, zero<br> [0x80001864]:sw t6, 256(fp)<br>    |
| 187|[0x800099e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000187c]:feq.h t6, t5, t4<br> [0x80001880]:csrrs a2, fcsr, zero<br> [0x80001884]:sw t6, 264(fp)<br>    |
| 188|[0x800099ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000189c]:feq.h t6, t5, t4<br> [0x800018a0]:csrrs a2, fcsr, zero<br> [0x800018a4]:sw t6, 272(fp)<br>    |
| 189|[0x800099f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800018bc]:feq.h t6, t5, t4<br> [0x800018c0]:csrrs a2, fcsr, zero<br> [0x800018c4]:sw t6, 280(fp)<br>    |
| 190|[0x800099fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800018dc]:feq.h t6, t5, t4<br> [0x800018e0]:csrrs a2, fcsr, zero<br> [0x800018e4]:sw t6, 288(fp)<br>    |
| 191|[0x80009a04]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800018fc]:feq.h t6, t5, t4<br> [0x80001900]:csrrs a2, fcsr, zero<br> [0x80001904]:sw t6, 296(fp)<br>    |
| 192|[0x80009a0c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000191c]:feq.h t6, t5, t4<br> [0x80001920]:csrrs a2, fcsr, zero<br> [0x80001924]:sw t6, 304(fp)<br>    |
| 193|[0x80009a14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000193c]:feq.h t6, t5, t4<br> [0x80001940]:csrrs a2, fcsr, zero<br> [0x80001944]:sw t6, 312(fp)<br>    |
| 194|[0x80009a1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000195c]:feq.h t6, t5, t4<br> [0x80001960]:csrrs a2, fcsr, zero<br> [0x80001964]:sw t6, 320(fp)<br>    |
| 195|[0x80009a24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000197c]:feq.h t6, t5, t4<br> [0x80001980]:csrrs a2, fcsr, zero<br> [0x80001984]:sw t6, 328(fp)<br>    |
| 196|[0x80009a2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000199c]:feq.h t6, t5, t4<br> [0x800019a0]:csrrs a2, fcsr, zero<br> [0x800019a4]:sw t6, 336(fp)<br>    |
| 197|[0x80009a34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800019bc]:feq.h t6, t5, t4<br> [0x800019c0]:csrrs a2, fcsr, zero<br> [0x800019c4]:sw t6, 344(fp)<br>    |
| 198|[0x80009a3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800019dc]:feq.h t6, t5, t4<br> [0x800019e0]:csrrs a2, fcsr, zero<br> [0x800019e4]:sw t6, 352(fp)<br>    |
| 199|[0x80009a44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800019fc]:feq.h t6, t5, t4<br> [0x80001a00]:csrrs a2, fcsr, zero<br> [0x80001a04]:sw t6, 360(fp)<br>    |
| 200|[0x80009a4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001a1c]:feq.h t6, t5, t4<br> [0x80001a20]:csrrs a2, fcsr, zero<br> [0x80001a24]:sw t6, 368(fp)<br>    |
| 201|[0x80009a54]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001a3c]:feq.h t6, t5, t4<br> [0x80001a40]:csrrs a2, fcsr, zero<br> [0x80001a44]:sw t6, 376(fp)<br>    |
| 202|[0x80009a5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001a5c]:feq.h t6, t5, t4<br> [0x80001a60]:csrrs a2, fcsr, zero<br> [0x80001a64]:sw t6, 384(fp)<br>    |
| 203|[0x80009a64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001a7c]:feq.h t6, t5, t4<br> [0x80001a80]:csrrs a2, fcsr, zero<br> [0x80001a84]:sw t6, 392(fp)<br>    |
| 204|[0x80009a6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001a9c]:feq.h t6, t5, t4<br> [0x80001aa0]:csrrs a2, fcsr, zero<br> [0x80001aa4]:sw t6, 400(fp)<br>    |
| 205|[0x80009a74]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001abc]:feq.h t6, t5, t4<br> [0x80001ac0]:csrrs a2, fcsr, zero<br> [0x80001ac4]:sw t6, 408(fp)<br>    |
| 206|[0x80009a7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001adc]:feq.h t6, t5, t4<br> [0x80001ae0]:csrrs a2, fcsr, zero<br> [0x80001ae4]:sw t6, 416(fp)<br>    |
| 207|[0x80009a84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001afc]:feq.h t6, t5, t4<br> [0x80001b00]:csrrs a2, fcsr, zero<br> [0x80001b04]:sw t6, 424(fp)<br>    |
| 208|[0x80009a8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001b1c]:feq.h t6, t5, t4<br> [0x80001b20]:csrrs a2, fcsr, zero<br> [0x80001b24]:sw t6, 432(fp)<br>    |
| 209|[0x80009a94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001b3c]:feq.h t6, t5, t4<br> [0x80001b40]:csrrs a2, fcsr, zero<br> [0x80001b44]:sw t6, 440(fp)<br>    |
| 210|[0x80009a9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001b5c]:feq.h t6, t5, t4<br> [0x80001b60]:csrrs a2, fcsr, zero<br> [0x80001b64]:sw t6, 448(fp)<br>    |
| 211|[0x80009aa4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001b7c]:feq.h t6, t5, t4<br> [0x80001b80]:csrrs a2, fcsr, zero<br> [0x80001b84]:sw t6, 456(fp)<br>    |
| 212|[0x80009aac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001b9c]:feq.h t6, t5, t4<br> [0x80001ba0]:csrrs a2, fcsr, zero<br> [0x80001ba4]:sw t6, 464(fp)<br>    |
| 213|[0x80009ab4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001bbc]:feq.h t6, t5, t4<br> [0x80001bc0]:csrrs a2, fcsr, zero<br> [0x80001bc4]:sw t6, 472(fp)<br>    |
| 214|[0x80009abc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001bdc]:feq.h t6, t5, t4<br> [0x80001be0]:csrrs a2, fcsr, zero<br> [0x80001be4]:sw t6, 480(fp)<br>    |
| 215|[0x80009ac4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001bfc]:feq.h t6, t5, t4<br> [0x80001c00]:csrrs a2, fcsr, zero<br> [0x80001c04]:sw t6, 488(fp)<br>    |
| 216|[0x80009acc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001c1c]:feq.h t6, t5, t4<br> [0x80001c20]:csrrs a2, fcsr, zero<br> [0x80001c24]:sw t6, 496(fp)<br>    |
| 217|[0x80009ad4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001c3c]:feq.h t6, t5, t4<br> [0x80001c40]:csrrs a2, fcsr, zero<br> [0x80001c44]:sw t6, 504(fp)<br>    |
| 218|[0x80009adc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001c5c]:feq.h t6, t5, t4<br> [0x80001c60]:csrrs a2, fcsr, zero<br> [0x80001c64]:sw t6, 512(fp)<br>    |
| 219|[0x80009ae4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001c7c]:feq.h t6, t5, t4<br> [0x80001c80]:csrrs a2, fcsr, zero<br> [0x80001c84]:sw t6, 520(fp)<br>    |
| 220|[0x80009aec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001c9c]:feq.h t6, t5, t4<br> [0x80001ca0]:csrrs a2, fcsr, zero<br> [0x80001ca4]:sw t6, 528(fp)<br>    |
| 221|[0x80009af4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001cbc]:feq.h t6, t5, t4<br> [0x80001cc0]:csrrs a2, fcsr, zero<br> [0x80001cc4]:sw t6, 536(fp)<br>    |
| 222|[0x80009afc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001cdc]:feq.h t6, t5, t4<br> [0x80001ce0]:csrrs a2, fcsr, zero<br> [0x80001ce4]:sw t6, 544(fp)<br>    |
| 223|[0x80009b04]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001cfc]:feq.h t6, t5, t4<br> [0x80001d00]:csrrs a2, fcsr, zero<br> [0x80001d04]:sw t6, 552(fp)<br>    |
| 224|[0x80009b0c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001d1c]:feq.h t6, t5, t4<br> [0x80001d20]:csrrs a2, fcsr, zero<br> [0x80001d24]:sw t6, 560(fp)<br>    |
| 225|[0x80009b14]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001d3c]:feq.h t6, t5, t4<br> [0x80001d40]:csrrs a2, fcsr, zero<br> [0x80001d44]:sw t6, 568(fp)<br>    |
| 226|[0x80009b1c]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001d5c]:feq.h t6, t5, t4<br> [0x80001d60]:csrrs a2, fcsr, zero<br> [0x80001d64]:sw t6, 576(fp)<br>    |
| 227|[0x80009b24]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001d7c]:feq.h t6, t5, t4<br> [0x80001d80]:csrrs a2, fcsr, zero<br> [0x80001d84]:sw t6, 584(fp)<br>    |
| 228|[0x80009b2c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001d9c]:feq.h t6, t5, t4<br> [0x80001da0]:csrrs a2, fcsr, zero<br> [0x80001da4]:sw t6, 592(fp)<br>    |
| 229|[0x80009b34]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001dbc]:feq.h t6, t5, t4<br> [0x80001dc0]:csrrs a2, fcsr, zero<br> [0x80001dc4]:sw t6, 600(fp)<br>    |
| 230|[0x80009b3c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001ddc]:feq.h t6, t5, t4<br> [0x80001de0]:csrrs a2, fcsr, zero<br> [0x80001de4]:sw t6, 608(fp)<br>    |
| 231|[0x80009b44]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001dfc]:feq.h t6, t5, t4<br> [0x80001e00]:csrrs a2, fcsr, zero<br> [0x80001e04]:sw t6, 616(fp)<br>    |
| 232|[0x80009b4c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001e1c]:feq.h t6, t5, t4<br> [0x80001e20]:csrrs a2, fcsr, zero<br> [0x80001e24]:sw t6, 624(fp)<br>    |
| 233|[0x80009b54]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001e3c]:feq.h t6, t5, t4<br> [0x80001e40]:csrrs a2, fcsr, zero<br> [0x80001e44]:sw t6, 632(fp)<br>    |
| 234|[0x80009b5c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001e5c]:feq.h t6, t5, t4<br> [0x80001e60]:csrrs a2, fcsr, zero<br> [0x80001e64]:sw t6, 640(fp)<br>    |
| 235|[0x80009b64]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001e7c]:feq.h t6, t5, t4<br> [0x80001e80]:csrrs a2, fcsr, zero<br> [0x80001e84]:sw t6, 648(fp)<br>    |
| 236|[0x80009b6c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001e9c]:feq.h t6, t5, t4<br> [0x80001ea0]:csrrs a2, fcsr, zero<br> [0x80001ea4]:sw t6, 656(fp)<br>    |
| 237|[0x80009b74]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001ebc]:feq.h t6, t5, t4<br> [0x80001ec0]:csrrs a2, fcsr, zero<br> [0x80001ec4]:sw t6, 664(fp)<br>    |
| 238|[0x80009b7c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001edc]:feq.h t6, t5, t4<br> [0x80001ee0]:csrrs a2, fcsr, zero<br> [0x80001ee4]:sw t6, 672(fp)<br>    |
| 239|[0x80009b84]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001efc]:feq.h t6, t5, t4<br> [0x80001f00]:csrrs a2, fcsr, zero<br> [0x80001f04]:sw t6, 680(fp)<br>    |
| 240|[0x80009b8c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001f1c]:feq.h t6, t5, t4<br> [0x80001f20]:csrrs a2, fcsr, zero<br> [0x80001f24]:sw t6, 688(fp)<br>    |
| 241|[0x80009b94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001f3c]:feq.h t6, t5, t4<br> [0x80001f40]:csrrs a2, fcsr, zero<br> [0x80001f44]:sw t6, 696(fp)<br>    |
| 242|[0x80009b9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001f5c]:feq.h t6, t5, t4<br> [0x80001f60]:csrrs a2, fcsr, zero<br> [0x80001f64]:sw t6, 704(fp)<br>    |
| 243|[0x80009ba4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001f7c]:feq.h t6, t5, t4<br> [0x80001f80]:csrrs a2, fcsr, zero<br> [0x80001f84]:sw t6, 712(fp)<br>    |
| 244|[0x80009bac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001f9c]:feq.h t6, t5, t4<br> [0x80001fa0]:csrrs a2, fcsr, zero<br> [0x80001fa4]:sw t6, 720(fp)<br>    |
| 245|[0x80009bb4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001fbc]:feq.h t6, t5, t4<br> [0x80001fc0]:csrrs a2, fcsr, zero<br> [0x80001fc4]:sw t6, 728(fp)<br>    |
| 246|[0x80009bbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001fdc]:feq.h t6, t5, t4<br> [0x80001fe0]:csrrs a2, fcsr, zero<br> [0x80001fe4]:sw t6, 736(fp)<br>    |
| 247|[0x80009bc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80001ffc]:feq.h t6, t5, t4<br> [0x80002000]:csrrs a2, fcsr, zero<br> [0x80002004]:sw t6, 744(fp)<br>    |
| 248|[0x80009bcc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000201c]:feq.h t6, t5, t4<br> [0x80002020]:csrrs a2, fcsr, zero<br> [0x80002024]:sw t6, 752(fp)<br>    |
| 249|[0x80009bd4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000203c]:feq.h t6, t5, t4<br> [0x80002040]:csrrs a2, fcsr, zero<br> [0x80002044]:sw t6, 760(fp)<br>    |
| 250|[0x80009bdc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000205c]:feq.h t6, t5, t4<br> [0x80002060]:csrrs a2, fcsr, zero<br> [0x80002064]:sw t6, 768(fp)<br>    |
| 251|[0x80009be4]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000207c]:feq.h t6, t5, t4<br> [0x80002080]:csrrs a2, fcsr, zero<br> [0x80002084]:sw t6, 776(fp)<br>    |
| 252|[0x80009bec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000209c]:feq.h t6, t5, t4<br> [0x800020a0]:csrrs a2, fcsr, zero<br> [0x800020a4]:sw t6, 784(fp)<br>    |
| 253|[0x80009bf4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800020bc]:feq.h t6, t5, t4<br> [0x800020c0]:csrrs a2, fcsr, zero<br> [0x800020c4]:sw t6, 792(fp)<br>    |
| 254|[0x80009bfc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800020dc]:feq.h t6, t5, t4<br> [0x800020e0]:csrrs a2, fcsr, zero<br> [0x800020e4]:sw t6, 800(fp)<br>    |
| 255|[0x80009c04]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800020fc]:feq.h t6, t5, t4<br> [0x80002100]:csrrs a2, fcsr, zero<br> [0x80002104]:sw t6, 808(fp)<br>    |
| 256|[0x80009c0c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000211c]:feq.h t6, t5, t4<br> [0x80002120]:csrrs a2, fcsr, zero<br> [0x80002124]:sw t6, 816(fp)<br>    |
| 257|[0x80009c14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000213c]:feq.h t6, t5, t4<br> [0x80002140]:csrrs a2, fcsr, zero<br> [0x80002144]:sw t6, 824(fp)<br>    |
| 258|[0x80009c1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000215c]:feq.h t6, t5, t4<br> [0x80002160]:csrrs a2, fcsr, zero<br> [0x80002164]:sw t6, 832(fp)<br>    |
| 259|[0x80009c24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000217c]:feq.h t6, t5, t4<br> [0x80002180]:csrrs a2, fcsr, zero<br> [0x80002184]:sw t6, 840(fp)<br>    |
| 260|[0x80009c2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000219c]:feq.h t6, t5, t4<br> [0x800021a0]:csrrs a2, fcsr, zero<br> [0x800021a4]:sw t6, 848(fp)<br>    |
| 261|[0x80009c34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800021bc]:feq.h t6, t5, t4<br> [0x800021c0]:csrrs a2, fcsr, zero<br> [0x800021c4]:sw t6, 856(fp)<br>    |
| 262|[0x80009c3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800021dc]:feq.h t6, t5, t4<br> [0x800021e0]:csrrs a2, fcsr, zero<br> [0x800021e4]:sw t6, 864(fp)<br>    |
| 263|[0x80009c44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800021fc]:feq.h t6, t5, t4<br> [0x80002200]:csrrs a2, fcsr, zero<br> [0x80002204]:sw t6, 872(fp)<br>    |
| 264|[0x80009c4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x01 and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000221c]:feq.h t6, t5, t4<br> [0x80002220]:csrrs a2, fcsr, zero<br> [0x80002224]:sw t6, 880(fp)<br>    |
| 265|[0x80009c54]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000223c]:feq.h t6, t5, t4<br> [0x80002240]:csrrs a2, fcsr, zero<br> [0x80002244]:sw t6, 888(fp)<br>    |
| 266|[0x80009c5c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000225c]:feq.h t6, t5, t4<br> [0x80002260]:csrrs a2, fcsr, zero<br> [0x80002264]:sw t6, 896(fp)<br>    |
| 267|[0x80009c64]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000227c]:feq.h t6, t5, t4<br> [0x80002280]:csrrs a2, fcsr, zero<br> [0x80002284]:sw t6, 904(fp)<br>    |
| 268|[0x80009c6c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000229c]:feq.h t6, t5, t4<br> [0x800022a0]:csrrs a2, fcsr, zero<br> [0x800022a4]:sw t6, 912(fp)<br>    |
| 269|[0x80009c74]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800022bc]:feq.h t6, t5, t4<br> [0x800022c0]:csrrs a2, fcsr, zero<br> [0x800022c4]:sw t6, 920(fp)<br>    |
| 270|[0x80009c7c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800022dc]:feq.h t6, t5, t4<br> [0x800022e0]:csrrs a2, fcsr, zero<br> [0x800022e4]:sw t6, 928(fp)<br>    |
| 271|[0x80009c84]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800022fc]:feq.h t6, t5, t4<br> [0x80002300]:csrrs a2, fcsr, zero<br> [0x80002304]:sw t6, 936(fp)<br>    |
| 272|[0x80009c8c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000231c]:feq.h t6, t5, t4<br> [0x80002320]:csrrs a2, fcsr, zero<br> [0x80002324]:sw t6, 944(fp)<br>    |
| 273|[0x80009c94]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000233c]:feq.h t6, t5, t4<br> [0x80002340]:csrrs a2, fcsr, zero<br> [0x80002344]:sw t6, 952(fp)<br>    |
| 274|[0x80009c9c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000235c]:feq.h t6, t5, t4<br> [0x80002360]:csrrs a2, fcsr, zero<br> [0x80002364]:sw t6, 960(fp)<br>    |
| 275|[0x80009ca4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000237c]:feq.h t6, t5, t4<br> [0x80002380]:csrrs a2, fcsr, zero<br> [0x80002384]:sw t6, 968(fp)<br>    |
| 276|[0x80009cac]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000239c]:feq.h t6, t5, t4<br> [0x800023a0]:csrrs a2, fcsr, zero<br> [0x800023a4]:sw t6, 976(fp)<br>    |
| 277|[0x80009cb4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800023bc]:feq.h t6, t5, t4<br> [0x800023c0]:csrrs a2, fcsr, zero<br> [0x800023c4]:sw t6, 984(fp)<br>    |
| 278|[0x80009cbc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800023dc]:feq.h t6, t5, t4<br> [0x800023e0]:csrrs a2, fcsr, zero<br> [0x800023e4]:sw t6, 992(fp)<br>    |
| 279|[0x80009cc4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000241c]:feq.h t6, t5, t4<br> [0x80002420]:csrrs a2, fcsr, zero<br> [0x80002424]:sw t6, 1000(fp)<br>   |
| 280|[0x80009ccc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000245c]:feq.h t6, t5, t4<br> [0x80002460]:csrrs a2, fcsr, zero<br> [0x80002464]:sw t6, 1008(fp)<br>   |
| 281|[0x80009cd4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000249c]:feq.h t6, t5, t4<br> [0x800024a0]:csrrs a2, fcsr, zero<br> [0x800024a4]:sw t6, 1016(fp)<br>   |
| 282|[0x80009cdc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800024e4]:feq.h t6, t5, t4<br> [0x800024e8]:csrrs a2, fcsr, zero<br> [0x800024ec]:sw t6, 0(fp)<br>      |
| 283|[0x80009ce4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002524]:feq.h t6, t5, t4<br> [0x80002528]:csrrs a2, fcsr, zero<br> [0x8000252c]:sw t6, 8(fp)<br>      |
| 284|[0x80009cec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002564]:feq.h t6, t5, t4<br> [0x80002568]:csrrs a2, fcsr, zero<br> [0x8000256c]:sw t6, 16(fp)<br>     |
| 285|[0x80009cf4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800025a4]:feq.h t6, t5, t4<br> [0x800025a8]:csrrs a2, fcsr, zero<br> [0x800025ac]:sw t6, 24(fp)<br>     |
| 286|[0x80009cfc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800025e4]:feq.h t6, t5, t4<br> [0x800025e8]:csrrs a2, fcsr, zero<br> [0x800025ec]:sw t6, 32(fp)<br>     |
| 287|[0x80009d04]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002624]:feq.h t6, t5, t4<br> [0x80002628]:csrrs a2, fcsr, zero<br> [0x8000262c]:sw t6, 40(fp)<br>     |
| 288|[0x80009d0c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x01 and fm1 == 0x055 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002664]:feq.h t6, t5, t4<br> [0x80002668]:csrrs a2, fcsr, zero<br> [0x8000266c]:sw t6, 48(fp)<br>     |
| 289|[0x80009d14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800026a4]:feq.h t6, t5, t4<br> [0x800026a8]:csrrs a2, fcsr, zero<br> [0x800026ac]:sw t6, 56(fp)<br>     |
| 290|[0x80009d1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800026e4]:feq.h t6, t5, t4<br> [0x800026e8]:csrrs a2, fcsr, zero<br> [0x800026ec]:sw t6, 64(fp)<br>     |
| 291|[0x80009d24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002724]:feq.h t6, t5, t4<br> [0x80002728]:csrrs a2, fcsr, zero<br> [0x8000272c]:sw t6, 72(fp)<br>     |
| 292|[0x80009d2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002764]:feq.h t6, t5, t4<br> [0x80002768]:csrrs a2, fcsr, zero<br> [0x8000276c]:sw t6, 80(fp)<br>     |
| 293|[0x80009d34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800027a4]:feq.h t6, t5, t4<br> [0x800027a8]:csrrs a2, fcsr, zero<br> [0x800027ac]:sw t6, 88(fp)<br>     |
| 294|[0x80009d3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800027e4]:feq.h t6, t5, t4<br> [0x800027e8]:csrrs a2, fcsr, zero<br> [0x800027ec]:sw t6, 96(fp)<br>     |
| 295|[0x80009d44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002824]:feq.h t6, t5, t4<br> [0x80002828]:csrrs a2, fcsr, zero<br> [0x8000282c]:sw t6, 104(fp)<br>    |
| 296|[0x80009d4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002864]:feq.h t6, t5, t4<br> [0x80002868]:csrrs a2, fcsr, zero<br> [0x8000286c]:sw t6, 112(fp)<br>    |
| 297|[0x80009d54]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800028a4]:feq.h t6, t5, t4<br> [0x800028a8]:csrrs a2, fcsr, zero<br> [0x800028ac]:sw t6, 120(fp)<br>    |
| 298|[0x80009d5c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800028e4]:feq.h t6, t5, t4<br> [0x800028e8]:csrrs a2, fcsr, zero<br> [0x800028ec]:sw t6, 128(fp)<br>    |
| 299|[0x80009d64]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002924]:feq.h t6, t5, t4<br> [0x80002928]:csrrs a2, fcsr, zero<br> [0x8000292c]:sw t6, 136(fp)<br>    |
| 300|[0x80009d6c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002964]:feq.h t6, t5, t4<br> [0x80002968]:csrrs a2, fcsr, zero<br> [0x8000296c]:sw t6, 144(fp)<br>    |
| 301|[0x80009d74]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800029a4]:feq.h t6, t5, t4<br> [0x800029a8]:csrrs a2, fcsr, zero<br> [0x800029ac]:sw t6, 152(fp)<br>    |
| 302|[0x80009d7c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800029e4]:feq.h t6, t5, t4<br> [0x800029e8]:csrrs a2, fcsr, zero<br> [0x800029ec]:sw t6, 160(fp)<br>    |
| 303|[0x80009d84]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002a24]:feq.h t6, t5, t4<br> [0x80002a28]:csrrs a2, fcsr, zero<br> [0x80002a2c]:sw t6, 168(fp)<br>    |
| 304|[0x80009d8c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002a64]:feq.h t6, t5, t4<br> [0x80002a68]:csrrs a2, fcsr, zero<br> [0x80002a6c]:sw t6, 176(fp)<br>    |
| 305|[0x80009d94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002aa4]:feq.h t6, t5, t4<br> [0x80002aa8]:csrrs a2, fcsr, zero<br> [0x80002aac]:sw t6, 184(fp)<br>    |
| 306|[0x80009d9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ae4]:feq.h t6, t5, t4<br> [0x80002ae8]:csrrs a2, fcsr, zero<br> [0x80002aec]:sw t6, 192(fp)<br>    |
| 307|[0x80009da4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002b24]:feq.h t6, t5, t4<br> [0x80002b28]:csrrs a2, fcsr, zero<br> [0x80002b2c]:sw t6, 200(fp)<br>    |
| 308|[0x80009dac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002b64]:feq.h t6, t5, t4<br> [0x80002b68]:csrrs a2, fcsr, zero<br> [0x80002b6c]:sw t6, 208(fp)<br>    |
| 309|[0x80009db4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ba4]:feq.h t6, t5, t4<br> [0x80002ba8]:csrrs a2, fcsr, zero<br> [0x80002bac]:sw t6, 216(fp)<br>    |
| 310|[0x80009dbc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002be4]:feq.h t6, t5, t4<br> [0x80002be8]:csrrs a2, fcsr, zero<br> [0x80002bec]:sw t6, 224(fp)<br>    |
| 311|[0x80009dc4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002c24]:feq.h t6, t5, t4<br> [0x80002c28]:csrrs a2, fcsr, zero<br> [0x80002c2c]:sw t6, 232(fp)<br>    |
| 312|[0x80009dcc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002c64]:feq.h t6, t5, t4<br> [0x80002c68]:csrrs a2, fcsr, zero<br> [0x80002c6c]:sw t6, 240(fp)<br>    |
| 313|[0x80009dd4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ca4]:feq.h t6, t5, t4<br> [0x80002ca8]:csrrs a2, fcsr, zero<br> [0x80002cac]:sw t6, 248(fp)<br>    |
| 314|[0x80009ddc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ce4]:feq.h t6, t5, t4<br> [0x80002ce8]:csrrs a2, fcsr, zero<br> [0x80002cec]:sw t6, 256(fp)<br>    |
| 315|[0x80009de4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002d24]:feq.h t6, t5, t4<br> [0x80002d28]:csrrs a2, fcsr, zero<br> [0x80002d2c]:sw t6, 264(fp)<br>    |
| 316|[0x80009dec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002d64]:feq.h t6, t5, t4<br> [0x80002d68]:csrrs a2, fcsr, zero<br> [0x80002d6c]:sw t6, 272(fp)<br>    |
| 317|[0x80009df4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002da4]:feq.h t6, t5, t4<br> [0x80002da8]:csrrs a2, fcsr, zero<br> [0x80002dac]:sw t6, 280(fp)<br>    |
| 318|[0x80009dfc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002de4]:feq.h t6, t5, t4<br> [0x80002de8]:csrrs a2, fcsr, zero<br> [0x80002dec]:sw t6, 288(fp)<br>    |
| 319|[0x80009e04]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002e24]:feq.h t6, t5, t4<br> [0x80002e28]:csrrs a2, fcsr, zero<br> [0x80002e2c]:sw t6, 296(fp)<br>    |
| 320|[0x80009e0c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002e64]:feq.h t6, t5, t4<br> [0x80002e68]:csrrs a2, fcsr, zero<br> [0x80002e6c]:sw t6, 304(fp)<br>    |
| 321|[0x80009e14]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ea4]:feq.h t6, t5, t4<br> [0x80002ea8]:csrrs a2, fcsr, zero<br> [0x80002eac]:sw t6, 312(fp)<br>    |
| 322|[0x80009e1c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002ee4]:feq.h t6, t5, t4<br> [0x80002ee8]:csrrs a2, fcsr, zero<br> [0x80002eec]:sw t6, 320(fp)<br>    |
| 323|[0x80009e24]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002f24]:feq.h t6, t5, t4<br> [0x80002f28]:csrrs a2, fcsr, zero<br> [0x80002f2c]:sw t6, 328(fp)<br>    |
| 324|[0x80009e2c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002f64]:feq.h t6, t5, t4<br> [0x80002f68]:csrrs a2, fcsr, zero<br> [0x80002f6c]:sw t6, 336(fp)<br>    |
| 325|[0x80009e34]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002fa4]:feq.h t6, t5, t4<br> [0x80002fa8]:csrrs a2, fcsr, zero<br> [0x80002fac]:sw t6, 344(fp)<br>    |
| 326|[0x80009e3c]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80002fe4]:feq.h t6, t5, t4<br> [0x80002fe8]:csrrs a2, fcsr, zero<br> [0x80002fec]:sw t6, 352(fp)<br>    |
| 327|[0x80009e44]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003024]:feq.h t6, t5, t4<br> [0x80003028]:csrrs a2, fcsr, zero<br> [0x8000302c]:sw t6, 360(fp)<br>    |
| 328|[0x80009e4c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003064]:feq.h t6, t5, t4<br> [0x80003068]:csrrs a2, fcsr, zero<br> [0x8000306c]:sw t6, 368(fp)<br>    |
| 329|[0x80009e54]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800030a4]:feq.h t6, t5, t4<br> [0x800030a8]:csrrs a2, fcsr, zero<br> [0x800030ac]:sw t6, 376(fp)<br>    |
| 330|[0x80009e5c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800030e4]:feq.h t6, t5, t4<br> [0x800030e8]:csrrs a2, fcsr, zero<br> [0x800030ec]:sw t6, 384(fp)<br>    |
| 331|[0x80009e64]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003124]:feq.h t6, t5, t4<br> [0x80003128]:csrrs a2, fcsr, zero<br> [0x8000312c]:sw t6, 392(fp)<br>    |
| 332|[0x80009e6c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003164]:feq.h t6, t5, t4<br> [0x80003168]:csrrs a2, fcsr, zero<br> [0x8000316c]:sw t6, 400(fp)<br>    |
| 333|[0x80009e74]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800031a4]:feq.h t6, t5, t4<br> [0x800031a8]:csrrs a2, fcsr, zero<br> [0x800031ac]:sw t6, 408(fp)<br>    |
| 334|[0x80009e7c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800031e4]:feq.h t6, t5, t4<br> [0x800031e8]:csrrs a2, fcsr, zero<br> [0x800031ec]:sw t6, 416(fp)<br>    |
| 335|[0x80009e84]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003224]:feq.h t6, t5, t4<br> [0x80003228]:csrrs a2, fcsr, zero<br> [0x8000322c]:sw t6, 424(fp)<br>    |
| 336|[0x80009e8c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1e and fm1 == 0x3ff and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003264]:feq.h t6, t5, t4<br> [0x80003268]:csrrs a2, fcsr, zero<br> [0x8000326c]:sw t6, 432(fp)<br>    |
| 337|[0x80009e94]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800032a4]:feq.h t6, t5, t4<br> [0x800032a8]:csrrs a2, fcsr, zero<br> [0x800032ac]:sw t6, 440(fp)<br>    |
| 338|[0x80009e9c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800032e4]:feq.h t6, t5, t4<br> [0x800032e8]:csrrs a2, fcsr, zero<br> [0x800032ec]:sw t6, 448(fp)<br>    |
| 339|[0x80009ea4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003324]:feq.h t6, t5, t4<br> [0x80003328]:csrrs a2, fcsr, zero<br> [0x8000332c]:sw t6, 456(fp)<br>    |
| 340|[0x80009eac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003364]:feq.h t6, t5, t4<br> [0x80003368]:csrrs a2, fcsr, zero<br> [0x8000336c]:sw t6, 464(fp)<br>    |
| 341|[0x80009eb4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800033a4]:feq.h t6, t5, t4<br> [0x800033a8]:csrrs a2, fcsr, zero<br> [0x800033ac]:sw t6, 472(fp)<br>    |
| 342|[0x80009ebc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800033e4]:feq.h t6, t5, t4<br> [0x800033e8]:csrrs a2, fcsr, zero<br> [0x800033ec]:sw t6, 480(fp)<br>    |
| 343|[0x80009ec4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003424]:feq.h t6, t5, t4<br> [0x80003428]:csrrs a2, fcsr, zero<br> [0x8000342c]:sw t6, 488(fp)<br>    |
| 344|[0x80009ecc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003464]:feq.h t6, t5, t4<br> [0x80003468]:csrrs a2, fcsr, zero<br> [0x8000346c]:sw t6, 496(fp)<br>    |
| 345|[0x80009ed4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800034a4]:feq.h t6, t5, t4<br> [0x800034a8]:csrrs a2, fcsr, zero<br> [0x800034ac]:sw t6, 504(fp)<br>    |
| 346|[0x80009edc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800034e4]:feq.h t6, t5, t4<br> [0x800034e8]:csrrs a2, fcsr, zero<br> [0x800034ec]:sw t6, 512(fp)<br>    |
| 347|[0x80009ee4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003524]:feq.h t6, t5, t4<br> [0x80003528]:csrrs a2, fcsr, zero<br> [0x8000352c]:sw t6, 520(fp)<br>    |
| 348|[0x80009eec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003564]:feq.h t6, t5, t4<br> [0x80003568]:csrrs a2, fcsr, zero<br> [0x8000356c]:sw t6, 528(fp)<br>    |
| 349|[0x80009ef4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800035a4]:feq.h t6, t5, t4<br> [0x800035a8]:csrrs a2, fcsr, zero<br> [0x800035ac]:sw t6, 536(fp)<br>    |
| 350|[0x80009efc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800035e4]:feq.h t6, t5, t4<br> [0x800035e8]:csrrs a2, fcsr, zero<br> [0x800035ec]:sw t6, 544(fp)<br>    |
| 351|[0x80009f04]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003624]:feq.h t6, t5, t4<br> [0x80003628]:csrrs a2, fcsr, zero<br> [0x8000362c]:sw t6, 552(fp)<br>    |
| 352|[0x80009f0c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003664]:feq.h t6, t5, t4<br> [0x80003668]:csrrs a2, fcsr, zero<br> [0x8000366c]:sw t6, 560(fp)<br>    |
| 353|[0x80009f14]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800036a4]:feq.h t6, t5, t4<br> [0x800036a8]:csrrs a2, fcsr, zero<br> [0x800036ac]:sw t6, 568(fp)<br>    |
| 354|[0x80009f1c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800036e4]:feq.h t6, t5, t4<br> [0x800036e8]:csrrs a2, fcsr, zero<br> [0x800036ec]:sw t6, 576(fp)<br>    |
| 355|[0x80009f24]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003724]:feq.h t6, t5, t4<br> [0x80003728]:csrrs a2, fcsr, zero<br> [0x8000372c]:sw t6, 584(fp)<br>    |
| 356|[0x80009f2c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003764]:feq.h t6, t5, t4<br> [0x80003768]:csrrs a2, fcsr, zero<br> [0x8000376c]:sw t6, 592(fp)<br>    |
| 357|[0x80009f34]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800037a4]:feq.h t6, t5, t4<br> [0x800037a8]:csrrs a2, fcsr, zero<br> [0x800037ac]:sw t6, 600(fp)<br>    |
| 358|[0x80009f3c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800037e4]:feq.h t6, t5, t4<br> [0x800037e8]:csrrs a2, fcsr, zero<br> [0x800037ec]:sw t6, 608(fp)<br>    |
| 359|[0x80009f44]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003824]:feq.h t6, t5, t4<br> [0x80003828]:csrrs a2, fcsr, zero<br> [0x8000382c]:sw t6, 616(fp)<br>    |
| 360|[0x80009f4c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003864]:feq.h t6, t5, t4<br> [0x80003868]:csrrs a2, fcsr, zero<br> [0x8000386c]:sw t6, 624(fp)<br>    |
| 361|[0x80009f54]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800038a4]:feq.h t6, t5, t4<br> [0x800038a8]:csrrs a2, fcsr, zero<br> [0x800038ac]:sw t6, 632(fp)<br>    |
| 362|[0x80009f5c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800038e4]:feq.h t6, t5, t4<br> [0x800038e8]:csrrs a2, fcsr, zero<br> [0x800038ec]:sw t6, 640(fp)<br>    |
| 363|[0x80009f64]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003924]:feq.h t6, t5, t4<br> [0x80003928]:csrrs a2, fcsr, zero<br> [0x8000392c]:sw t6, 648(fp)<br>    |
| 364|[0x80009f6c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003964]:feq.h t6, t5, t4<br> [0x80003968]:csrrs a2, fcsr, zero<br> [0x8000396c]:sw t6, 656(fp)<br>    |
| 365|[0x80009f74]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800039a4]:feq.h t6, t5, t4<br> [0x800039a8]:csrrs a2, fcsr, zero<br> [0x800039ac]:sw t6, 664(fp)<br>    |
| 366|[0x80009f7c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800039e4]:feq.h t6, t5, t4<br> [0x800039e8]:csrrs a2, fcsr, zero<br> [0x800039ec]:sw t6, 672(fp)<br>    |
| 367|[0x80009f84]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003a24]:feq.h t6, t5, t4<br> [0x80003a28]:csrrs a2, fcsr, zero<br> [0x80003a2c]:sw t6, 680(fp)<br>    |
| 368|[0x80009f8c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003a64]:feq.h t6, t5, t4<br> [0x80003a68]:csrrs a2, fcsr, zero<br> [0x80003a6c]:sw t6, 688(fp)<br>    |
| 369|[0x80009f94]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003aa4]:feq.h t6, t5, t4<br> [0x80003aa8]:csrrs a2, fcsr, zero<br> [0x80003aac]:sw t6, 696(fp)<br>    |
| 370|[0x80009f9c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ae4]:feq.h t6, t5, t4<br> [0x80003ae8]:csrrs a2, fcsr, zero<br> [0x80003aec]:sw t6, 704(fp)<br>    |
| 371|[0x80009fa4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003b24]:feq.h t6, t5, t4<br> [0x80003b28]:csrrs a2, fcsr, zero<br> [0x80003b2c]:sw t6, 712(fp)<br>    |
| 372|[0x80009fac]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003b64]:feq.h t6, t5, t4<br> [0x80003b68]:csrrs a2, fcsr, zero<br> [0x80003b6c]:sw t6, 720(fp)<br>    |
| 373|[0x80009fb4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ba4]:feq.h t6, t5, t4<br> [0x80003ba8]:csrrs a2, fcsr, zero<br> [0x80003bac]:sw t6, 728(fp)<br>    |
| 374|[0x80009fbc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003be4]:feq.h t6, t5, t4<br> [0x80003be8]:csrrs a2, fcsr, zero<br> [0x80003bec]:sw t6, 736(fp)<br>    |
| 375|[0x80009fc4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003c24]:feq.h t6, t5, t4<br> [0x80003c28]:csrrs a2, fcsr, zero<br> [0x80003c2c]:sw t6, 744(fp)<br>    |
| 376|[0x80009fcc]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003c64]:feq.h t6, t5, t4<br> [0x80003c68]:csrrs a2, fcsr, zero<br> [0x80003c6c]:sw t6, 752(fp)<br>    |
| 377|[0x80009fd4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ca4]:feq.h t6, t5, t4<br> [0x80003ca8]:csrrs a2, fcsr, zero<br> [0x80003cac]:sw t6, 760(fp)<br>    |
| 378|[0x80009fdc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ce4]:feq.h t6, t5, t4<br> [0x80003ce8]:csrrs a2, fcsr, zero<br> [0x80003cec]:sw t6, 768(fp)<br>    |
| 379|[0x80009fe4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003d24]:feq.h t6, t5, t4<br> [0x80003d28]:csrrs a2, fcsr, zero<br> [0x80003d2c]:sw t6, 776(fp)<br>    |
| 380|[0x80009fec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003d64]:feq.h t6, t5, t4<br> [0x80003d68]:csrrs a2, fcsr, zero<br> [0x80003d6c]:sw t6, 784(fp)<br>    |
| 381|[0x80009ff4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003da4]:feq.h t6, t5, t4<br> [0x80003da8]:csrrs a2, fcsr, zero<br> [0x80003dac]:sw t6, 792(fp)<br>    |
| 382|[0x80009ffc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003de4]:feq.h t6, t5, t4<br> [0x80003de8]:csrrs a2, fcsr, zero<br> [0x80003dec]:sw t6, 800(fp)<br>    |
| 383|[0x8000a004]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003e24]:feq.h t6, t5, t4<br> [0x80003e28]:csrrs a2, fcsr, zero<br> [0x80003e2c]:sw t6, 808(fp)<br>    |
| 384|[0x8000a00c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003e64]:feq.h t6, t5, t4<br> [0x80003e68]:csrrs a2, fcsr, zero<br> [0x80003e6c]:sw t6, 816(fp)<br>    |
| 385|[0x8000a014]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ea4]:feq.h t6, t5, t4<br> [0x80003ea8]:csrrs a2, fcsr, zero<br> [0x80003eac]:sw t6, 824(fp)<br>    |
| 386|[0x8000a01c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003ee4]:feq.h t6, t5, t4<br> [0x80003ee8]:csrrs a2, fcsr, zero<br> [0x80003eec]:sw t6, 832(fp)<br>    |
| 387|[0x8000a024]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003f24]:feq.h t6, t5, t4<br> [0x80003f28]:csrrs a2, fcsr, zero<br> [0x80003f2c]:sw t6, 840(fp)<br>    |
| 388|[0x8000a02c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003f64]:feq.h t6, t5, t4<br> [0x80003f68]:csrrs a2, fcsr, zero<br> [0x80003f6c]:sw t6, 848(fp)<br>    |
| 389|[0x8000a034]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003fa4]:feq.h t6, t5, t4<br> [0x80003fa8]:csrrs a2, fcsr, zero<br> [0x80003fac]:sw t6, 856(fp)<br>    |
| 390|[0x8000a03c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80003fe4]:feq.h t6, t5, t4<br> [0x80003fe8]:csrrs a2, fcsr, zero<br> [0x80003fec]:sw t6, 864(fp)<br>    |
| 391|[0x8000a044]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004024]:feq.h t6, t5, t4<br> [0x80004028]:csrrs a2, fcsr, zero<br> [0x8000402c]:sw t6, 872(fp)<br>    |
| 392|[0x8000a04c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004064]:feq.h t6, t5, t4<br> [0x80004068]:csrrs a2, fcsr, zero<br> [0x8000406c]:sw t6, 880(fp)<br>    |
| 393|[0x8000a054]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800040a4]:feq.h t6, t5, t4<br> [0x800040a8]:csrrs a2, fcsr, zero<br> [0x800040ac]:sw t6, 888(fp)<br>    |
| 394|[0x8000a05c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800040e4]:feq.h t6, t5, t4<br> [0x800040e8]:csrrs a2, fcsr, zero<br> [0x800040ec]:sw t6, 896(fp)<br>    |
| 395|[0x8000a064]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004124]:feq.h t6, t5, t4<br> [0x80004128]:csrrs a2, fcsr, zero<br> [0x8000412c]:sw t6, 904(fp)<br>    |
| 396|[0x8000a06c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004164]:feq.h t6, t5, t4<br> [0x80004168]:csrrs a2, fcsr, zero<br> [0x8000416c]:sw t6, 912(fp)<br>    |
| 397|[0x8000a074]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800041a4]:feq.h t6, t5, t4<br> [0x800041a8]:csrrs a2, fcsr, zero<br> [0x800041ac]:sw t6, 920(fp)<br>    |
| 398|[0x8000a07c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800041e4]:feq.h t6, t5, t4<br> [0x800041e8]:csrrs a2, fcsr, zero<br> [0x800041ec]:sw t6, 928(fp)<br>    |
| 399|[0x8000a084]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004224]:feq.h t6, t5, t4<br> [0x80004228]:csrrs a2, fcsr, zero<br> [0x8000422c]:sw t6, 936(fp)<br>    |
| 400|[0x8000a08c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004264]:feq.h t6, t5, t4<br> [0x80004268]:csrrs a2, fcsr, zero<br> [0x8000426c]:sw t6, 944(fp)<br>    |
| 401|[0x8000a094]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800042a4]:feq.h t6, t5, t4<br> [0x800042a8]:csrrs a2, fcsr, zero<br> [0x800042ac]:sw t6, 952(fp)<br>    |
| 402|[0x8000a09c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800042e4]:feq.h t6, t5, t4<br> [0x800042e8]:csrrs a2, fcsr, zero<br> [0x800042ec]:sw t6, 960(fp)<br>    |
| 403|[0x8000a0a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004324]:feq.h t6, t5, t4<br> [0x80004328]:csrrs a2, fcsr, zero<br> [0x8000432c]:sw t6, 968(fp)<br>    |
| 404|[0x8000a0ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004364]:feq.h t6, t5, t4<br> [0x80004368]:csrrs a2, fcsr, zero<br> [0x8000436c]:sw t6, 976(fp)<br>    |
| 405|[0x8000a0b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800043a4]:feq.h t6, t5, t4<br> [0x800043a8]:csrrs a2, fcsr, zero<br> [0x800043ac]:sw t6, 984(fp)<br>    |
| 406|[0x8000a0bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800043e4]:feq.h t6, t5, t4<br> [0x800043e8]:csrrs a2, fcsr, zero<br> [0x800043ec]:sw t6, 992(fp)<br>    |
| 407|[0x8000a0c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004424]:feq.h t6, t5, t4<br> [0x80004428]:csrrs a2, fcsr, zero<br> [0x8000442c]:sw t6, 1000(fp)<br>   |
| 408|[0x8000a0cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004464]:feq.h t6, t5, t4<br> [0x80004468]:csrrs a2, fcsr, zero<br> [0x8000446c]:sw t6, 1008(fp)<br>   |
| 409|[0x8000a0d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800044a4]:feq.h t6, t5, t4<br> [0x800044a8]:csrrs a2, fcsr, zero<br> [0x800044ac]:sw t6, 1016(fp)<br>   |
| 410|[0x8000a0dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800044ec]:feq.h t6, t5, t4<br> [0x800044f0]:csrrs a2, fcsr, zero<br> [0x800044f4]:sw t6, 0(fp)<br>      |
| 411|[0x8000a0e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000452c]:feq.h t6, t5, t4<br> [0x80004530]:csrrs a2, fcsr, zero<br> [0x80004534]:sw t6, 8(fp)<br>      |
| 412|[0x8000a0ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000456c]:feq.h t6, t5, t4<br> [0x80004570]:csrrs a2, fcsr, zero<br> [0x80004574]:sw t6, 16(fp)<br>     |
| 413|[0x8000a0f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800045ac]:feq.h t6, t5, t4<br> [0x800045b0]:csrrs a2, fcsr, zero<br> [0x800045b4]:sw t6, 24(fp)<br>     |
| 414|[0x8000a0fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800045ec]:feq.h t6, t5, t4<br> [0x800045f0]:csrrs a2, fcsr, zero<br> [0x800045f4]:sw t6, 32(fp)<br>     |
| 415|[0x8000a104]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000462c]:feq.h t6, t5, t4<br> [0x80004630]:csrrs a2, fcsr, zero<br> [0x80004634]:sw t6, 40(fp)<br>     |
| 416|[0x8000a10c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000466c]:feq.h t6, t5, t4<br> [0x80004670]:csrrs a2, fcsr, zero<br> [0x80004674]:sw t6, 48(fp)<br>     |
| 417|[0x8000a114]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800046ac]:feq.h t6, t5, t4<br> [0x800046b0]:csrrs a2, fcsr, zero<br> [0x800046b4]:sw t6, 56(fp)<br>     |
| 418|[0x8000a11c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800046ec]:feq.h t6, t5, t4<br> [0x800046f0]:csrrs a2, fcsr, zero<br> [0x800046f4]:sw t6, 64(fp)<br>     |
| 419|[0x8000a124]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000472c]:feq.h t6, t5, t4<br> [0x80004730]:csrrs a2, fcsr, zero<br> [0x80004734]:sw t6, 72(fp)<br>     |
| 420|[0x8000a12c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000476c]:feq.h t6, t5, t4<br> [0x80004770]:csrrs a2, fcsr, zero<br> [0x80004774]:sw t6, 80(fp)<br>     |
| 421|[0x8000a134]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800047ac]:feq.h t6, t5, t4<br> [0x800047b0]:csrrs a2, fcsr, zero<br> [0x800047b4]:sw t6, 88(fp)<br>     |
| 422|[0x8000a13c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800047ec]:feq.h t6, t5, t4<br> [0x800047f0]:csrrs a2, fcsr, zero<br> [0x800047f4]:sw t6, 96(fp)<br>     |
| 423|[0x8000a144]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000482c]:feq.h t6, t5, t4<br> [0x80004830]:csrrs a2, fcsr, zero<br> [0x80004834]:sw t6, 104(fp)<br>    |
| 424|[0x8000a14c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000486c]:feq.h t6, t5, t4<br> [0x80004870]:csrrs a2, fcsr, zero<br> [0x80004874]:sw t6, 112(fp)<br>    |
| 425|[0x8000a154]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800048ac]:feq.h t6, t5, t4<br> [0x800048b0]:csrrs a2, fcsr, zero<br> [0x800048b4]:sw t6, 120(fp)<br>    |
| 426|[0x8000a15c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800048ec]:feq.h t6, t5, t4<br> [0x800048f0]:csrrs a2, fcsr, zero<br> [0x800048f4]:sw t6, 128(fp)<br>    |
| 427|[0x8000a164]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000492c]:feq.h t6, t5, t4<br> [0x80004930]:csrrs a2, fcsr, zero<br> [0x80004934]:sw t6, 136(fp)<br>    |
| 428|[0x8000a16c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000496c]:feq.h t6, t5, t4<br> [0x80004970]:csrrs a2, fcsr, zero<br> [0x80004974]:sw t6, 144(fp)<br>    |
| 429|[0x8000a174]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800049ac]:feq.h t6, t5, t4<br> [0x800049b0]:csrrs a2, fcsr, zero<br> [0x800049b4]:sw t6, 152(fp)<br>    |
| 430|[0x8000a17c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800049ec]:feq.h t6, t5, t4<br> [0x800049f0]:csrrs a2, fcsr, zero<br> [0x800049f4]:sw t6, 160(fp)<br>    |
| 431|[0x8000a184]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004a2c]:feq.h t6, t5, t4<br> [0x80004a30]:csrrs a2, fcsr, zero<br> [0x80004a34]:sw t6, 168(fp)<br>    |
| 432|[0x8000a18c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x200 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004a6c]:feq.h t6, t5, t4<br> [0x80004a70]:csrrs a2, fcsr, zero<br> [0x80004a74]:sw t6, 176(fp)<br>    |
| 433|[0x8000a194]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004aac]:feq.h t6, t5, t4<br> [0x80004ab0]:csrrs a2, fcsr, zero<br> [0x80004ab4]:sw t6, 184(fp)<br>    |
| 434|[0x8000a19c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004aec]:feq.h t6, t5, t4<br> [0x80004af0]:csrrs a2, fcsr, zero<br> [0x80004af4]:sw t6, 192(fp)<br>    |
| 435|[0x8000a1a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004b2c]:feq.h t6, t5, t4<br> [0x80004b30]:csrrs a2, fcsr, zero<br> [0x80004b34]:sw t6, 200(fp)<br>    |
| 436|[0x8000a1ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004b6c]:feq.h t6, t5, t4<br> [0x80004b70]:csrrs a2, fcsr, zero<br> [0x80004b74]:sw t6, 208(fp)<br>    |
| 437|[0x8000a1b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004bac]:feq.h t6, t5, t4<br> [0x80004bb0]:csrrs a2, fcsr, zero<br> [0x80004bb4]:sw t6, 216(fp)<br>    |
| 438|[0x8000a1bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004bec]:feq.h t6, t5, t4<br> [0x80004bf0]:csrrs a2, fcsr, zero<br> [0x80004bf4]:sw t6, 224(fp)<br>    |
| 439|[0x8000a1c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004c2c]:feq.h t6, t5, t4<br> [0x80004c30]:csrrs a2, fcsr, zero<br> [0x80004c34]:sw t6, 232(fp)<br>    |
| 440|[0x8000a1cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004c6c]:feq.h t6, t5, t4<br> [0x80004c70]:csrrs a2, fcsr, zero<br> [0x80004c74]:sw t6, 240(fp)<br>    |
| 441|[0x8000a1d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004cac]:feq.h t6, t5, t4<br> [0x80004cb0]:csrrs a2, fcsr, zero<br> [0x80004cb4]:sw t6, 248(fp)<br>    |
| 442|[0x8000a1dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004cec]:feq.h t6, t5, t4<br> [0x80004cf0]:csrrs a2, fcsr, zero<br> [0x80004cf4]:sw t6, 256(fp)<br>    |
| 443|[0x8000a1e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004d2c]:feq.h t6, t5, t4<br> [0x80004d30]:csrrs a2, fcsr, zero<br> [0x80004d34]:sw t6, 264(fp)<br>    |
| 444|[0x8000a1ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004d6c]:feq.h t6, t5, t4<br> [0x80004d70]:csrrs a2, fcsr, zero<br> [0x80004d74]:sw t6, 272(fp)<br>    |
| 445|[0x8000a1f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004dac]:feq.h t6, t5, t4<br> [0x80004db0]:csrrs a2, fcsr, zero<br> [0x80004db4]:sw t6, 280(fp)<br>    |
| 446|[0x8000a1fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004dec]:feq.h t6, t5, t4<br> [0x80004df0]:csrrs a2, fcsr, zero<br> [0x80004df4]:sw t6, 288(fp)<br>    |
| 447|[0x8000a204]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004e2c]:feq.h t6, t5, t4<br> [0x80004e30]:csrrs a2, fcsr, zero<br> [0x80004e34]:sw t6, 296(fp)<br>    |
| 448|[0x8000a20c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004e6c]:feq.h t6, t5, t4<br> [0x80004e70]:csrrs a2, fcsr, zero<br> [0x80004e74]:sw t6, 304(fp)<br>    |
| 449|[0x8000a214]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004eac]:feq.h t6, t5, t4<br> [0x80004eb0]:csrrs a2, fcsr, zero<br> [0x80004eb4]:sw t6, 312(fp)<br>    |
| 450|[0x8000a21c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004eec]:feq.h t6, t5, t4<br> [0x80004ef0]:csrrs a2, fcsr, zero<br> [0x80004ef4]:sw t6, 320(fp)<br>    |
| 451|[0x8000a224]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004f2c]:feq.h t6, t5, t4<br> [0x80004f30]:csrrs a2, fcsr, zero<br> [0x80004f34]:sw t6, 328(fp)<br>    |
| 452|[0x8000a22c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004f6c]:feq.h t6, t5, t4<br> [0x80004f70]:csrrs a2, fcsr, zero<br> [0x80004f74]:sw t6, 336(fp)<br>    |
| 453|[0x8000a234]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004fac]:feq.h t6, t5, t4<br> [0x80004fb0]:csrrs a2, fcsr, zero<br> [0x80004fb4]:sw t6, 344(fp)<br>    |
| 454|[0x8000a23c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80004fec]:feq.h t6, t5, t4<br> [0x80004ff0]:csrrs a2, fcsr, zero<br> [0x80004ff4]:sw t6, 352(fp)<br>    |
| 455|[0x8000a244]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000502c]:feq.h t6, t5, t4<br> [0x80005030]:csrrs a2, fcsr, zero<br> [0x80005034]:sw t6, 360(fp)<br>    |
| 456|[0x8000a24c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x201 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000506c]:feq.h t6, t5, t4<br> [0x80005070]:csrrs a2, fcsr, zero<br> [0x80005074]:sw t6, 368(fp)<br>    |
| 457|[0x8000a254]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800050ac]:feq.h t6, t5, t4<br> [0x800050b0]:csrrs a2, fcsr, zero<br> [0x800050b4]:sw t6, 376(fp)<br>    |
| 458|[0x8000a25c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800050ec]:feq.h t6, t5, t4<br> [0x800050f0]:csrrs a2, fcsr, zero<br> [0x800050f4]:sw t6, 384(fp)<br>    |
| 459|[0x8000a264]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000512c]:feq.h t6, t5, t4<br> [0x80005130]:csrrs a2, fcsr, zero<br> [0x80005134]:sw t6, 392(fp)<br>    |
| 460|[0x8000a26c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000516c]:feq.h t6, t5, t4<br> [0x80005170]:csrrs a2, fcsr, zero<br> [0x80005174]:sw t6, 400(fp)<br>    |
| 461|[0x8000a274]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800051ac]:feq.h t6, t5, t4<br> [0x800051b0]:csrrs a2, fcsr, zero<br> [0x800051b4]:sw t6, 408(fp)<br>    |
| 462|[0x8000a27c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800051ec]:feq.h t6, t5, t4<br> [0x800051f0]:csrrs a2, fcsr, zero<br> [0x800051f4]:sw t6, 416(fp)<br>    |
| 463|[0x8000a284]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000522c]:feq.h t6, t5, t4<br> [0x80005230]:csrrs a2, fcsr, zero<br> [0x80005234]:sw t6, 424(fp)<br>    |
| 464|[0x8000a28c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000526c]:feq.h t6, t5, t4<br> [0x80005270]:csrrs a2, fcsr, zero<br> [0x80005274]:sw t6, 432(fp)<br>    |
| 465|[0x8000a294]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800052ac]:feq.h t6, t5, t4<br> [0x800052b0]:csrrs a2, fcsr, zero<br> [0x800052b4]:sw t6, 440(fp)<br>    |
| 466|[0x8000a29c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800052ec]:feq.h t6, t5, t4<br> [0x800052f0]:csrrs a2, fcsr, zero<br> [0x800052f4]:sw t6, 448(fp)<br>    |
| 467|[0x8000a2a4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000532c]:feq.h t6, t5, t4<br> [0x80005330]:csrrs a2, fcsr, zero<br> [0x80005334]:sw t6, 456(fp)<br>    |
| 468|[0x8000a2ac]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000536c]:feq.h t6, t5, t4<br> [0x80005370]:csrrs a2, fcsr, zero<br> [0x80005374]:sw t6, 464(fp)<br>    |
| 469|[0x8000a2b4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800053ac]:feq.h t6, t5, t4<br> [0x800053b0]:csrrs a2, fcsr, zero<br> [0x800053b4]:sw t6, 472(fp)<br>    |
| 470|[0x8000a2bc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800053ec]:feq.h t6, t5, t4<br> [0x800053f0]:csrrs a2, fcsr, zero<br> [0x800053f4]:sw t6, 480(fp)<br>    |
| 471|[0x8000a2c4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000542c]:feq.h t6, t5, t4<br> [0x80005430]:csrrs a2, fcsr, zero<br> [0x80005434]:sw t6, 488(fp)<br>    |
| 472|[0x8000a2cc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000546c]:feq.h t6, t5, t4<br> [0x80005470]:csrrs a2, fcsr, zero<br> [0x80005474]:sw t6, 496(fp)<br>    |
| 473|[0x8000a2d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800054ac]:feq.h t6, t5, t4<br> [0x800054b0]:csrrs a2, fcsr, zero<br> [0x800054b4]:sw t6, 504(fp)<br>    |
| 474|[0x8000a2dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800054ec]:feq.h t6, t5, t4<br> [0x800054f0]:csrrs a2, fcsr, zero<br> [0x800054f4]:sw t6, 512(fp)<br>    |
| 475|[0x8000a2e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000552c]:feq.h t6, t5, t4<br> [0x80005530]:csrrs a2, fcsr, zero<br> [0x80005534]:sw t6, 520(fp)<br>    |
| 476|[0x8000a2ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000556c]:feq.h t6, t5, t4<br> [0x80005570]:csrrs a2, fcsr, zero<br> [0x80005574]:sw t6, 528(fp)<br>    |
| 477|[0x8000a2f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800055ac]:feq.h t6, t5, t4<br> [0x800055b0]:csrrs a2, fcsr, zero<br> [0x800055b4]:sw t6, 536(fp)<br>    |
| 478|[0x8000a2fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800055ec]:feq.h t6, t5, t4<br> [0x800055f0]:csrrs a2, fcsr, zero<br> [0x800055f4]:sw t6, 544(fp)<br>    |
| 479|[0x8000a304]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000562c]:feq.h t6, t5, t4<br> [0x80005630]:csrrs a2, fcsr, zero<br> [0x80005634]:sw t6, 552(fp)<br>    |
| 480|[0x8000a30c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x255 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000566c]:feq.h t6, t5, t4<br> [0x80005670]:csrrs a2, fcsr, zero<br> [0x80005674]:sw t6, 560(fp)<br>    |
| 481|[0x8000a314]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800056ac]:feq.h t6, t5, t4<br> [0x800056b0]:csrrs a2, fcsr, zero<br> [0x800056b4]:sw t6, 568(fp)<br>    |
| 482|[0x8000a31c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800056ec]:feq.h t6, t5, t4<br> [0x800056f0]:csrrs a2, fcsr, zero<br> [0x800056f4]:sw t6, 576(fp)<br>    |
| 483|[0x8000a324]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000572c]:feq.h t6, t5, t4<br> [0x80005730]:csrrs a2, fcsr, zero<br> [0x80005734]:sw t6, 584(fp)<br>    |
| 484|[0x8000a32c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000576c]:feq.h t6, t5, t4<br> [0x80005770]:csrrs a2, fcsr, zero<br> [0x80005774]:sw t6, 592(fp)<br>    |
| 485|[0x8000a334]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800057ac]:feq.h t6, t5, t4<br> [0x800057b0]:csrrs a2, fcsr, zero<br> [0x800057b4]:sw t6, 600(fp)<br>    |
| 486|[0x8000a33c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800057ec]:feq.h t6, t5, t4<br> [0x800057f0]:csrrs a2, fcsr, zero<br> [0x800057f4]:sw t6, 608(fp)<br>    |
| 487|[0x8000a344]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000582c]:feq.h t6, t5, t4<br> [0x80005830]:csrrs a2, fcsr, zero<br> [0x80005834]:sw t6, 616(fp)<br>    |
| 488|[0x8000a34c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000586c]:feq.h t6, t5, t4<br> [0x80005870]:csrrs a2, fcsr, zero<br> [0x80005874]:sw t6, 624(fp)<br>    |
| 489|[0x8000a354]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800058ac]:feq.h t6, t5, t4<br> [0x800058b0]:csrrs a2, fcsr, zero<br> [0x800058b4]:sw t6, 632(fp)<br>    |
| 490|[0x8000a35c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800058ec]:feq.h t6, t5, t4<br> [0x800058f0]:csrrs a2, fcsr, zero<br> [0x800058f4]:sw t6, 640(fp)<br>    |
| 491|[0x8000a364]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000592c]:feq.h t6, t5, t4<br> [0x80005930]:csrrs a2, fcsr, zero<br> [0x80005934]:sw t6, 648(fp)<br>    |
| 492|[0x8000a36c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000596c]:feq.h t6, t5, t4<br> [0x80005970]:csrrs a2, fcsr, zero<br> [0x80005974]:sw t6, 656(fp)<br>    |
| 493|[0x8000a374]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800059ac]:feq.h t6, t5, t4<br> [0x800059b0]:csrrs a2, fcsr, zero<br> [0x800059b4]:sw t6, 664(fp)<br>    |
| 494|[0x8000a37c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800059ec]:feq.h t6, t5, t4<br> [0x800059f0]:csrrs a2, fcsr, zero<br> [0x800059f4]:sw t6, 672(fp)<br>    |
| 495|[0x8000a384]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005a2c]:feq.h t6, t5, t4<br> [0x80005a30]:csrrs a2, fcsr, zero<br> [0x80005a34]:sw t6, 680(fp)<br>    |
| 496|[0x8000a38c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005a6c]:feq.h t6, t5, t4<br> [0x80005a70]:csrrs a2, fcsr, zero<br> [0x80005a74]:sw t6, 688(fp)<br>    |
| 497|[0x8000a394]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005aac]:feq.h t6, t5, t4<br> [0x80005ab0]:csrrs a2, fcsr, zero<br> [0x80005ab4]:sw t6, 696(fp)<br>    |
| 498|[0x8000a39c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005aec]:feq.h t6, t5, t4<br> [0x80005af0]:csrrs a2, fcsr, zero<br> [0x80005af4]:sw t6, 704(fp)<br>    |
| 499|[0x8000a3a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005b2c]:feq.h t6, t5, t4<br> [0x80005b30]:csrrs a2, fcsr, zero<br> [0x80005b34]:sw t6, 712(fp)<br>    |
| 500|[0x8000a3ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005b6c]:feq.h t6, t5, t4<br> [0x80005b70]:csrrs a2, fcsr, zero<br> [0x80005b74]:sw t6, 720(fp)<br>    |
| 501|[0x8000a3b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005bac]:feq.h t6, t5, t4<br> [0x80005bb0]:csrrs a2, fcsr, zero<br> [0x80005bb4]:sw t6, 728(fp)<br>    |
| 502|[0x8000a3bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005bec]:feq.h t6, t5, t4<br> [0x80005bf0]:csrrs a2, fcsr, zero<br> [0x80005bf4]:sw t6, 736(fp)<br>    |
| 503|[0x8000a3c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005c2c]:feq.h t6, t5, t4<br> [0x80005c30]:csrrs a2, fcsr, zero<br> [0x80005c34]:sw t6, 744(fp)<br>    |
| 504|[0x8000a3cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x1f and fm1 == 0x001 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005c6c]:feq.h t6, t5, t4<br> [0x80005c70]:csrrs a2, fcsr, zero<br> [0x80005c74]:sw t6, 752(fp)<br>    |
| 505|[0x8000a3d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005cac]:feq.h t6, t5, t4<br> [0x80005cb0]:csrrs a2, fcsr, zero<br> [0x80005cb4]:sw t6, 760(fp)<br>    |
| 506|[0x8000a3dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005cec]:feq.h t6, t5, t4<br> [0x80005cf0]:csrrs a2, fcsr, zero<br> [0x80005cf4]:sw t6, 768(fp)<br>    |
| 507|[0x8000a3e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005d2c]:feq.h t6, t5, t4<br> [0x80005d30]:csrrs a2, fcsr, zero<br> [0x80005d34]:sw t6, 776(fp)<br>    |
| 508|[0x8000a3ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005d6c]:feq.h t6, t5, t4<br> [0x80005d70]:csrrs a2, fcsr, zero<br> [0x80005d74]:sw t6, 784(fp)<br>    |
| 509|[0x8000a3f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005dac]:feq.h t6, t5, t4<br> [0x80005db0]:csrrs a2, fcsr, zero<br> [0x80005db4]:sw t6, 792(fp)<br>    |
| 510|[0x8000a3fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005dec]:feq.h t6, t5, t4<br> [0x80005df0]:csrrs a2, fcsr, zero<br> [0x80005df4]:sw t6, 800(fp)<br>    |
| 511|[0x8000a404]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005e2c]:feq.h t6, t5, t4<br> [0x80005e30]:csrrs a2, fcsr, zero<br> [0x80005e34]:sw t6, 808(fp)<br>    |
| 512|[0x8000a40c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005e6c]:feq.h t6, t5, t4<br> [0x80005e70]:csrrs a2, fcsr, zero<br> [0x80005e74]:sw t6, 816(fp)<br>    |
| 513|[0x8000a414]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005eac]:feq.h t6, t5, t4<br> [0x80005eb0]:csrrs a2, fcsr, zero<br> [0x80005eb4]:sw t6, 824(fp)<br>    |
| 514|[0x8000a41c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005eec]:feq.h t6, t5, t4<br> [0x80005ef0]:csrrs a2, fcsr, zero<br> [0x80005ef4]:sw t6, 832(fp)<br>    |
| 515|[0x8000a424]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005f2c]:feq.h t6, t5, t4<br> [0x80005f30]:csrrs a2, fcsr, zero<br> [0x80005f34]:sw t6, 840(fp)<br>    |
| 516|[0x8000a42c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005f6c]:feq.h t6, t5, t4<br> [0x80005f70]:csrrs a2, fcsr, zero<br> [0x80005f74]:sw t6, 848(fp)<br>    |
| 517|[0x8000a434]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005fac]:feq.h t6, t5, t4<br> [0x80005fb0]:csrrs a2, fcsr, zero<br> [0x80005fb4]:sw t6, 856(fp)<br>    |
| 518|[0x8000a43c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80005fec]:feq.h t6, t5, t4<br> [0x80005ff0]:csrrs a2, fcsr, zero<br> [0x80005ff4]:sw t6, 864(fp)<br>    |
| 519|[0x8000a444]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000602c]:feq.h t6, t5, t4<br> [0x80006030]:csrrs a2, fcsr, zero<br> [0x80006034]:sw t6, 872(fp)<br>    |
| 520|[0x8000a44c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000606c]:feq.h t6, t5, t4<br> [0x80006070]:csrrs a2, fcsr, zero<br> [0x80006074]:sw t6, 880(fp)<br>    |
| 521|[0x8000a454]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800060ac]:feq.h t6, t5, t4<br> [0x800060b0]:csrrs a2, fcsr, zero<br> [0x800060b4]:sw t6, 888(fp)<br>    |
| 522|[0x8000a45c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800060ec]:feq.h t6, t5, t4<br> [0x800060f0]:csrrs a2, fcsr, zero<br> [0x800060f4]:sw t6, 896(fp)<br>    |
| 523|[0x8000a464]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000612c]:feq.h t6, t5, t4<br> [0x80006130]:csrrs a2, fcsr, zero<br> [0x80006134]:sw t6, 904(fp)<br>    |
| 524|[0x8000a46c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000616c]:feq.h t6, t5, t4<br> [0x80006170]:csrrs a2, fcsr, zero<br> [0x80006174]:sw t6, 912(fp)<br>    |
| 525|[0x8000a474]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800061ac]:feq.h t6, t5, t4<br> [0x800061b0]:csrrs a2, fcsr, zero<br> [0x800061b4]:sw t6, 920(fp)<br>    |
| 526|[0x8000a47c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800061ec]:feq.h t6, t5, t4<br> [0x800061f0]:csrrs a2, fcsr, zero<br> [0x800061f4]:sw t6, 928(fp)<br>    |
| 527|[0x8000a484]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000622c]:feq.h t6, t5, t4<br> [0x80006230]:csrrs a2, fcsr, zero<br> [0x80006234]:sw t6, 936(fp)<br>    |
| 528|[0x8000a48c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x1f and fm1 == 0x155 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000626c]:feq.h t6, t5, t4<br> [0x80006270]:csrrs a2, fcsr, zero<br> [0x80006274]:sw t6, 944(fp)<br>    |
| 529|[0x8000a494]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800062ac]:feq.h t6, t5, t4<br> [0x800062b0]:csrrs a2, fcsr, zero<br> [0x800062b4]:sw t6, 952(fp)<br>    |
| 530|[0x8000a49c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800062ec]:feq.h t6, t5, t4<br> [0x800062f0]:csrrs a2, fcsr, zero<br> [0x800062f4]:sw t6, 960(fp)<br>    |
| 531|[0x8000a4a4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000632c]:feq.h t6, t5, t4<br> [0x80006330]:csrrs a2, fcsr, zero<br> [0x80006334]:sw t6, 968(fp)<br>    |
| 532|[0x8000a4ac]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000636c]:feq.h t6, t5, t4<br> [0x80006370]:csrrs a2, fcsr, zero<br> [0x80006374]:sw t6, 976(fp)<br>    |
| 533|[0x8000a4b4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800063ac]:feq.h t6, t5, t4<br> [0x800063b0]:csrrs a2, fcsr, zero<br> [0x800063b4]:sw t6, 984(fp)<br>    |
| 534|[0x8000a4bc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800063ec]:feq.h t6, t5, t4<br> [0x800063f0]:csrrs a2, fcsr, zero<br> [0x800063f4]:sw t6, 992(fp)<br>    |
| 535|[0x8000a4c4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006424]:feq.h t6, t5, t4<br> [0x80006428]:csrrs a2, fcsr, zero<br> [0x8000642c]:sw t6, 1000(fp)<br>   |
| 536|[0x8000a4cc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000645c]:feq.h t6, t5, t4<br> [0x80006460]:csrrs a2, fcsr, zero<br> [0x80006464]:sw t6, 1008(fp)<br>   |
| 537|[0x8000a4d4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006494]:feq.h t6, t5, t4<br> [0x80006498]:csrrs a2, fcsr, zero<br> [0x8000649c]:sw t6, 1016(fp)<br>   |
| 538|[0x8000a4dc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800064d4]:feq.h t6, t5, t4<br> [0x800064d8]:csrrs a2, fcsr, zero<br> [0x800064dc]:sw t6, 0(fp)<br>      |
| 539|[0x8000a4e4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000650c]:feq.h t6, t5, t4<br> [0x80006510]:csrrs a2, fcsr, zero<br> [0x80006514]:sw t6, 8(fp)<br>      |
| 540|[0x8000a4ec]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006544]:feq.h t6, t5, t4<br> [0x80006548]:csrrs a2, fcsr, zero<br> [0x8000654c]:sw t6, 16(fp)<br>     |
| 541|[0x8000a4f4]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000657c]:feq.h t6, t5, t4<br> [0x80006580]:csrrs a2, fcsr, zero<br> [0x80006584]:sw t6, 24(fp)<br>     |
| 542|[0x8000a4fc]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800065b4]:feq.h t6, t5, t4<br> [0x800065b8]:csrrs a2, fcsr, zero<br> [0x800065bc]:sw t6, 32(fp)<br>     |
| 543|[0x8000a504]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800065ec]:feq.h t6, t5, t4<br> [0x800065f0]:csrrs a2, fcsr, zero<br> [0x800065f4]:sw t6, 40(fp)<br>     |
| 544|[0x8000a50c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006624]:feq.h t6, t5, t4<br> [0x80006628]:csrrs a2, fcsr, zero<br> [0x8000662c]:sw t6, 48(fp)<br>     |
| 545|[0x8000a514]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000665c]:feq.h t6, t5, t4<br> [0x80006660]:csrrs a2, fcsr, zero<br> [0x80006664]:sw t6, 56(fp)<br>     |
| 546|[0x8000a51c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006694]:feq.h t6, t5, t4<br> [0x80006698]:csrrs a2, fcsr, zero<br> [0x8000669c]:sw t6, 64(fp)<br>     |
| 547|[0x8000a524]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800066cc]:feq.h t6, t5, t4<br> [0x800066d0]:csrrs a2, fcsr, zero<br> [0x800066d4]:sw t6, 72(fp)<br>     |
| 548|[0x8000a52c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006704]:feq.h t6, t5, t4<br> [0x80006708]:csrrs a2, fcsr, zero<br> [0x8000670c]:sw t6, 80(fp)<br>     |
| 549|[0x8000a534]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000673c]:feq.h t6, t5, t4<br> [0x80006740]:csrrs a2, fcsr, zero<br> [0x80006744]:sw t6, 88(fp)<br>     |
| 550|[0x8000a53c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006774]:feq.h t6, t5, t4<br> [0x80006778]:csrrs a2, fcsr, zero<br> [0x8000677c]:sw t6, 96(fp)<br>     |
| 551|[0x8000a544]<br>0x00000001<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800067ac]:feq.h t6, t5, t4<br> [0x800067b0]:csrrs a2, fcsr, zero<br> [0x800067b4]:sw t6, 104(fp)<br>    |
| 552|[0x8000a54c]<br>0x00000000<br> |- fs1 == 0 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800067e4]:feq.h t6, t5, t4<br> [0x800067e8]:csrrs a2, fcsr, zero<br> [0x800067ec]:sw t6, 112(fp)<br>    |
| 553|[0x8000a554]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000681c]:feq.h t6, t5, t4<br> [0x80006820]:csrrs a2, fcsr, zero<br> [0x80006824]:sw t6, 120(fp)<br>    |
| 554|[0x8000a55c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006854]:feq.h t6, t5, t4<br> [0x80006858]:csrrs a2, fcsr, zero<br> [0x8000685c]:sw t6, 128(fp)<br>    |
| 555|[0x8000a564]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000688c]:feq.h t6, t5, t4<br> [0x80006890]:csrrs a2, fcsr, zero<br> [0x80006894]:sw t6, 136(fp)<br>    |
| 556|[0x8000a56c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800068c4]:feq.h t6, t5, t4<br> [0x800068c8]:csrrs a2, fcsr, zero<br> [0x800068cc]:sw t6, 144(fp)<br>    |
| 557|[0x8000a574]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x002 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800068fc]:feq.h t6, t5, t4<br> [0x80006900]:csrrs a2, fcsr, zero<br> [0x80006904]:sw t6, 152(fp)<br>    |
| 558|[0x8000a57c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3fe and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006934]:feq.h t6, t5, t4<br> [0x80006938]:csrrs a2, fcsr, zero<br> [0x8000693c]:sw t6, 160(fp)<br>    |
| 559|[0x8000a584]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x8000696c]:feq.h t6, t5, t4<br> [0x80006970]:csrrs a2, fcsr, zero<br> [0x80006974]:sw t6, 168(fp)<br>    |
| 560|[0x8000a58c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800069a4]:feq.h t6, t5, t4<br> [0x800069a8]:csrrs a2, fcsr, zero<br> [0x800069ac]:sw t6, 176(fp)<br>    |
| 561|[0x8000a594]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x800069dc]:feq.h t6, t5, t4<br> [0x800069e0]:csrrs a2, fcsr, zero<br> [0x800069e4]:sw t6, 184(fp)<br>    |
| 562|[0x8000a59c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006a14]:feq.h t6, t5, t4<br> [0x80006a18]:csrrs a2, fcsr, zero<br> [0x80006a1c]:sw t6, 192(fp)<br>    |
| 563|[0x8000a5a4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006a4c]:feq.h t6, t5, t4<br> [0x80006a50]:csrrs a2, fcsr, zero<br> [0x80006a54]:sw t6, 200(fp)<br>    |
| 564|[0x8000a5ac]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x01 and fm2 == 0x055 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006a84]:feq.h t6, t5, t4<br> [0x80006a88]:csrrs a2, fcsr, zero<br> [0x80006a8c]:sw t6, 208(fp)<br>    |
| 565|[0x8000a5b4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006abc]:feq.h t6, t5, t4<br> [0x80006ac0]:csrrs a2, fcsr, zero<br> [0x80006ac4]:sw t6, 216(fp)<br>    |
| 566|[0x8000a5bc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1e and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006af4]:feq.h t6, t5, t4<br> [0x80006af8]:csrrs a2, fcsr, zero<br> [0x80006afc]:sw t6, 224(fp)<br>    |
| 567|[0x8000a5c4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006b2c]:feq.h t6, t5, t4<br> [0x80006b30]:csrrs a2, fcsr, zero<br> [0x80006b34]:sw t6, 232(fp)<br>    |
| 568|[0x8000a5cc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006b64]:feq.h t6, t5, t4<br> [0x80006b68]:csrrs a2, fcsr, zero<br> [0x80006b6c]:sw t6, 240(fp)<br>    |
| 569|[0x8000a5d4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006b9c]:feq.h t6, t5, t4<br> [0x80006ba0]:csrrs a2, fcsr, zero<br> [0x80006ba4]:sw t6, 248(fp)<br>    |
| 570|[0x8000a5dc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x200 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006bd4]:feq.h t6, t5, t4<br> [0x80006bd8]:csrrs a2, fcsr, zero<br> [0x80006bdc]:sw t6, 256(fp)<br>    |
| 571|[0x8000a5e4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x201 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006c0c]:feq.h t6, t5, t4<br> [0x80006c10]:csrrs a2, fcsr, zero<br> [0x80006c14]:sw t6, 264(fp)<br>    |
| 572|[0x8000a5ec]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x255 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006c44]:feq.h t6, t5, t4<br> [0x80006c48]:csrrs a2, fcsr, zero<br> [0x80006c4c]:sw t6, 272(fp)<br>    |
| 573|[0x8000a5f4]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x001 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006c7c]:feq.h t6, t5, t4<br> [0x80006c80]:csrrs a2, fcsr, zero<br> [0x80006c84]:sw t6, 280(fp)<br>    |
| 574|[0x8000a5fc]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x1f and fm2 == 0x155 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006cb4]:feq.h t6, t5, t4<br> [0x80006cb8]:csrrs a2, fcsr, zero<br> [0x80006cbc]:sw t6, 288(fp)<br>    |
| 575|[0x8000a604]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006cec]:feq.h t6, t5, t4<br> [0x80006cf0]:csrrs a2, fcsr, zero<br> [0x80006cf4]:sw t6, 296(fp)<br>    |
| 576|[0x8000a60c]<br>0x00000001<br> |- fs1 == 1 and fe1 == 0x0f and fm1 == 0x000 and fs2 == 1 and fe2 == 0x0f and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006d24]:feq.h t6, t5, t4<br> [0x80006d28]:csrrs a2, fcsr, zero<br> [0x80006d2c]:sw t6, 304(fp)<br>    |
| 577|[0x8000a61c]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3ff and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006d94]:feq.h t6, t5, t4<br> [0x80006d98]:csrrs a2, fcsr, zero<br> [0x80006d9c]:sw t6, 320(fp)<br>    |
| 578|[0x8000a624]<br>0x00000000<br> |- fs1 == 1 and fe1 == 0x00 and fm1 == 0x000 and fs2 == 0 and fe2 == 0x01 and fm2 == 0x000 and  fcsr == 0 and rs1_nan_prefix == 0xffff and rs2_nan_prefix == 0xffff  #nosat<br>                                                                                        |[0x80006dcc]:feq.h t6, t5, t4<br> [0x80006dd0]:csrrs a2, fcsr, zero<br> [0x80006dd4]:sw t6, 328(fp)<br>    |
