
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000700')]      |
| SIG_REGION                | [('0x80002310', '0x80002520', '66 dwords')]      |
| COV_LABELS                | fcvt.d.w_b25      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.d.w/riscof_work/fcvt.d.w_b25-01.S/ref.S    |
| Total Number of coverpoints| 76     |
| Total Coverpoints Hit     | 72      |
| Total Signature Updates   | 33      |
| STAT1                     | 33      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                           coverpoints                                            |                                                                       code                                                                        |
|---:|----------------------------------|--------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000000|- opcode : fcvt.d.w<br> - rs1 : x16<br> - rd : f27<br> - rs1_val == 0 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.d.w fs11, a6, rne<br> [0x800003b8]:csrrs s5, fflags, zero<br> [0x800003bc]:fsd fs11, 0(s3)<br> [0x800003c0]:sd s5, 8(s3)<br>     |
|   2|[0x80002328]<br>0x0000000000000000|- rs1 : x22<br> - rd : f10<br> - rs1_val == -5270258713649211392 and rm_val == 0  #nosat<br>      |[0x800003d8]:fcvt.d.w fa0, s6, rne<br> [0x800003dc]:csrrs a7, fflags, zero<br> [0x800003e0]:fsd fa0, 0(a5)<br> [0x800003e4]:sd a7, 8(a5)<br>       |
|   3|[0x80002338]<br>0x0000000000000000|- rs1 : x20<br> - rd : f19<br> - rs1_val == 5270258713649211392 and rm_val == 0  #nosat<br>       |[0x800003f0]:fcvt.d.w fs3, s4, rne<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:fsd fs3, 16(a5)<br> [0x800003fc]:sd a7, 24(a5)<br>     |
|   4|[0x80002348]<br>0x0000000000000000|- rs1 : x6<br> - rd : f25<br> - rs1_val == -9223372036854775807 and rm_val == 0  #nosat<br>       |[0x80000408]:fcvt.d.w fs9, t1, rne<br> [0x8000040c]:csrrs a7, fflags, zero<br> [0x80000410]:fsd fs9, 32(a5)<br> [0x80000414]:sd a7, 40(a5)<br>     |
|   5|[0x80002358]<br>0x0000000000000000|- rs1 : x10<br> - rd : f23<br> - rs1_val == 9223372036854775807 and rm_val == 0  #nosat<br>       |[0x80000420]:fcvt.d.w fs7, a0, rne<br> [0x80000424]:csrrs a7, fflags, zero<br> [0x80000428]:fsd fs7, 48(a5)<br> [0x8000042c]:sd a7, 56(a5)<br>     |
|   6|[0x80002368]<br>0x0000000000000000|- rs1 : x31<br> - rd : f31<br> - rs1_val == -1 and rm_val == 0  #nosat<br>                        |[0x80000438]:fcvt.d.w ft11, t6, rne<br> [0x8000043c]:csrrs a7, fflags, zero<br> [0x80000440]:fsd ft11, 64(a5)<br> [0x80000444]:sd a7, 72(a5)<br>   |
|   7|[0x80002378]<br>0x0000000000000000|- rs1 : x0<br> - rd : f17<br>                                                                     |[0x80000450]:fcvt.d.w fa7, zero, rne<br> [0x80000454]:csrrs a7, fflags, zero<br> [0x80000458]:fsd fa7, 80(a5)<br> [0x8000045c]:sd a7, 88(a5)<br>   |
|   8|[0x80002388]<br>0x0000000000000000|- rs1 : x17<br> - rd : f13<br>                                                                    |[0x80000474]:fcvt.d.w fa3, a7, rne<br> [0x80000478]:csrrs s5, fflags, zero<br> [0x8000047c]:fsd fa3, 0(s3)<br> [0x80000480]:sd s5, 8(s3)<br>       |
|   9|[0x80002398]<br>0x0000000000000000|- rs1 : x1<br> - rd : f20<br>                                                                     |[0x80000498]:fcvt.d.w fs4, ra, rne<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:fsd fs4, 0(a5)<br> [0x800004a4]:sd a7, 8(a5)<br>       |
|  10|[0x800023a8]<br>0x0000000000000000|- rs1 : x11<br> - rd : f28<br>                                                                    |[0x800004b0]:fcvt.d.w ft8, a1, rne<br> [0x800004b4]:csrrs a7, fflags, zero<br> [0x800004b8]:fsd ft8, 16(a5)<br> [0x800004bc]:sd a7, 24(a5)<br>     |
|  11|[0x800023b8]<br>0x0000000000000000|- rs1 : x28<br> - rd : f11<br>                                                                    |[0x800004c8]:fcvt.d.w fa1, t3, rne<br> [0x800004cc]:csrrs a7, fflags, zero<br> [0x800004d0]:fsd fa1, 32(a5)<br> [0x800004d4]:sd a7, 40(a5)<br>     |
|  12|[0x800023c8]<br>0x0000000000000000|- rs1 : x26<br> - rd : f29<br>                                                                    |[0x800004e0]:fcvt.d.w ft9, s10, rne<br> [0x800004e4]:csrrs a7, fflags, zero<br> [0x800004e8]:fsd ft9, 48(a5)<br> [0x800004ec]:sd a7, 56(a5)<br>    |
|  13|[0x800023d8]<br>0x0000000000000000|- rs1 : x29<br> - rd : f4<br>                                                                     |[0x800004f8]:fcvt.d.w ft4, t4, rne<br> [0x800004fc]:csrrs a7, fflags, zero<br> [0x80000500]:fsd ft4, 64(a5)<br> [0x80000504]:sd a7, 72(a5)<br>     |
|  14|[0x800023e8]<br>0x0000000000000000|- rs1 : x14<br> - rd : f16<br>                                                                    |[0x80000510]:fcvt.d.w fa6, a4, rne<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:fsd fa6, 80(a5)<br> [0x8000051c]:sd a7, 88(a5)<br>     |
|  15|[0x800023f8]<br>0x0000000000000000|- rs1 : x2<br> - rd : f9<br>                                                                      |[0x80000528]:fcvt.d.w fs1, sp, rne<br> [0x8000052c]:csrrs a7, fflags, zero<br> [0x80000530]:fsd fs1, 96(a5)<br> [0x80000534]:sd a7, 104(a5)<br>    |
|  16|[0x80002408]<br>0x0000000000000000|- rs1 : x18<br> - rd : f0<br>                                                                     |[0x80000540]:fcvt.d.w ft0, s2, rne<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:fsd ft0, 112(a5)<br> [0x8000054c]:sd a7, 120(a5)<br>   |
|  17|[0x80002418]<br>0x0000000000000000|- rs1 : x27<br> - rd : f2<br>                                                                     |[0x80000558]:fcvt.d.w ft2, s11, rne<br> [0x8000055c]:csrrs a7, fflags, zero<br> [0x80000560]:fsd ft2, 128(a5)<br> [0x80000564]:sd a7, 136(a5)<br>  |
|  18|[0x80002428]<br>0x0000000000000000|- rs1 : x30<br> - rd : f22<br>                                                                    |[0x80000570]:fcvt.d.w fs6, t5, rne<br> [0x80000574]:csrrs a7, fflags, zero<br> [0x80000578]:fsd fs6, 144(a5)<br> [0x8000057c]:sd a7, 152(a5)<br>   |
|  19|[0x80002438]<br>0x0000000000000000|- rs1 : x5<br> - rd : f21<br>                                                                     |[0x80000588]:fcvt.d.w fs5, t0, rne<br> [0x8000058c]:csrrs a7, fflags, zero<br> [0x80000590]:fsd fs5, 160(a5)<br> [0x80000594]:sd a7, 168(a5)<br>   |
|  20|[0x80002448]<br>0x0000000000000000|- rs1 : x15<br> - rd : f5<br>                                                                     |[0x800005ac]:fcvt.d.w ft5, a5, rne<br> [0x800005b0]:csrrs s5, fflags, zero<br> [0x800005b4]:fsd ft5, 0(s3)<br> [0x800005b8]:sd s5, 8(s3)<br>       |
|  21|[0x80002458]<br>0x0000000000000000|- rs1 : x4<br> - rd : f6<br>                                                                      |[0x800005d0]:fcvt.d.w ft6, tp, rne<br> [0x800005d4]:csrrs a7, fflags, zero<br> [0x800005d8]:fsd ft6, 0(a5)<br> [0x800005dc]:sd a7, 8(a5)<br>       |
|  22|[0x80002468]<br>0x0000000000000000|- rs1 : x24<br> - rd : f18<br>                                                                    |[0x800005e8]:fcvt.d.w fs2, s8, rne<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:fsd fs2, 16(a5)<br> [0x800005f4]:sd a7, 24(a5)<br>     |
|  23|[0x80002478]<br>0x0000000000000000|- rs1 : x19<br> - rd : f1<br>                                                                     |[0x80000600]:fcvt.d.w ft1, s3, rne<br> [0x80000604]:csrrs a7, fflags, zero<br> [0x80000608]:fsd ft1, 32(a5)<br> [0x8000060c]:sd a7, 40(a5)<br>     |
|  24|[0x80002488]<br>0x0000000000000000|- rs1 : x21<br> - rd : f26<br>                                                                    |[0x80000618]:fcvt.d.w fs10, s5, rne<br> [0x8000061c]:csrrs a7, fflags, zero<br> [0x80000620]:fsd fs10, 48(a5)<br> [0x80000624]:sd a7, 56(a5)<br>   |
|  25|[0x80002498]<br>0x0000000000000000|- rs1 : x3<br> - rd : f8<br>                                                                      |[0x80000630]:fcvt.d.w fs0, gp, rne<br> [0x80000634]:csrrs a7, fflags, zero<br> [0x80000638]:fsd fs0, 64(a5)<br> [0x8000063c]:sd a7, 72(a5)<br>     |
|  26|[0x800024a8]<br>0x0000000000000000|- rs1 : x25<br> - rd : f24<br>                                                                    |[0x80000648]:fcvt.d.w fs8, s9, rne<br> [0x8000064c]:csrrs a7, fflags, zero<br> [0x80000650]:fsd fs8, 80(a5)<br> [0x80000654]:sd a7, 88(a5)<br>     |
|  27|[0x800024b8]<br>0x0000000000000000|- rs1 : x12<br> - rd : f12<br>                                                                    |[0x80000660]:fcvt.d.w fa2, a2, rne<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:fsd fa2, 96(a5)<br> [0x8000066c]:sd a7, 104(a5)<br>    |
|  28|[0x800024c8]<br>0x0000000000000000|- rs1 : x7<br> - rd : f14<br>                                                                     |[0x80000678]:fcvt.d.w fa4, t2, rne<br> [0x8000067c]:csrrs a7, fflags, zero<br> [0x80000680]:fsd fa4, 112(a5)<br> [0x80000684]:sd a7, 120(a5)<br>   |
|  29|[0x800024d8]<br>0x0000000000000000|- rs1 : x8<br> - rd : f30<br>                                                                     |[0x80000690]:fcvt.d.w ft10, fp, rne<br> [0x80000694]:csrrs a7, fflags, zero<br> [0x80000698]:fsd ft10, 128(a5)<br> [0x8000069c]:sd a7, 136(a5)<br> |
|  30|[0x800024e8]<br>0x0000000000000000|- rs1 : x9<br> - rd : f3<br>                                                                      |[0x800006a8]:fcvt.d.w ft3, s1, rne<br> [0x800006ac]:csrrs a7, fflags, zero<br> [0x800006b0]:fsd ft3, 144(a5)<br> [0x800006b4]:sd a7, 152(a5)<br>   |
|  31|[0x800024f8]<br>0x0000000000000000|- rs1 : x13<br> - rd : f7<br>                                                                     |[0x800006c0]:fcvt.d.w ft7, a3, rne<br> [0x800006c4]:csrrs a7, fflags, zero<br> [0x800006c8]:fsd ft7, 160(a5)<br> [0x800006cc]:sd a7, 168(a5)<br>   |
|  32|[0x80002508]<br>0x0000000000000000|- rs1 : x23<br> - rd : f15<br>                                                                    |[0x800006d8]:fcvt.d.w fa5, s7, rne<br> [0x800006dc]:csrrs a7, fflags, zero<br> [0x800006e0]:fsd fa5, 176(a5)<br> [0x800006e4]:sd a7, 184(a5)<br>   |
|  33|[0x80002518]<br>0x0000000000000000|- rs1_val == 1 and rm_val == 0  #nosat<br>                                                        |[0x800006f0]:fcvt.d.w ft11, t6, rne<br> [0x800006f4]:csrrs a7, fflags, zero<br> [0x800006f8]:fsd ft11, 192(a5)<br> [0x800006fc]:sd a7, 200(a5)<br> |
