
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000980')]      |
| SIG_REGION                | [('0x80002510', '0x80002860', '106 dwords')]      |
| COV_LABELS                | fsub_b12      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fsub/riscof_work/fsub_b12-01.S/ref.S    |
| Total Number of coverpoints| 158     |
| Total Coverpoints Hit     | 152      |
| Total Signature Updates   | 53      |
| STAT1                     | 52      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000094c]:fsub.d ft11, ft10, ft9, dyn
      [0x80000950]:csrrs a7, fflags, zero
      [0x80000954]:fsd ft11, 816(a5)
      [0x80000958]:sd a7, 824(a5)
 -- Signature Address: 0x80002848 Data: 0x0000000000000001
 -- Redundant Coverpoints hit by the op
      - opcode : fsub.d
      - rs1 : f30
      - rs2 : f29
      - rd : f31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x1506f64179e12 and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                                                              coverpoints                                                                                                               |                                                                          code                                                                          |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002518]<br>0x0000000000000000|- opcode : fsub.d<br> - rs1 : f29<br> - rs2 : f29<br> - rd : f4<br> - rs1 == rs2 != rd<br>                                                                                                                                              |[0x800003b8]:fsub.d ft4, ft9, ft9, dyn<br> [0x800003bc]:csrrs a7, fflags, zero<br> [0x800003c0]:fsd ft4, 0(a5)<br> [0x800003c4]:sd a7, 8(a5)<br>        |
|   2|[0x80002528]<br>0x0000000000000000|- rs1 : f0<br> - rs2 : f30<br> - rd : f0<br> - rs1 == rd != rs2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x1f6a4c4d26ab9 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x1506f64179e12 and rm_val == 0  #nosat<br>                         |[0x800003d4]:fsub.d ft0, ft0, ft10, dyn<br> [0x800003d8]:csrrs a7, fflags, zero<br> [0x800003dc]:fsd ft0, 16(a5)<br> [0x800003e0]:sd a7, 24(a5)<br>     |
|   3|[0x80002538]<br>0x0000000000000000|- rs1 : f13<br> - rs2 : f18<br> - rd : f18<br> - rs2 == rd != rs1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x9e4795c8459f5 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x296ec52d097ea and rm_val == 0  #nosat<br>                       |[0x800003f0]:fsub.d fs2, fa3, fs2, dyn<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:fsd fs2, 32(a5)<br> [0x800003fc]:sd a7, 40(a5)<br>      |
|   4|[0x80002548]<br>0x0000000000000000|- rs1 : f17<br> - rs2 : f8<br> - rd : f21<br> - rs1 != rs2  and rs1 != rd and rs2 != rd<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc5b9547c0fb71 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x2a58446d0baa8 and rm_val == 0  #nosat<br> |[0x8000040c]:fsub.d fs5, fa7, fs0, dyn<br> [0x80000410]:csrrs a7, fflags, zero<br> [0x80000414]:fsd fs5, 48(a5)<br> [0x80000418]:sd a7, 56(a5)<br>      |
|   5|[0x80002558]<br>0x0000000000000000|- rs1 : f9<br> - rs2 : f9<br> - rd : f9<br> - rs1 == rs2 == rd<br>                                                                                                                                                                      |[0x80000428]:fsub.d fs1, fs1, fs1, dyn<br> [0x8000042c]:csrrs a7, fflags, zero<br> [0x80000430]:fsd fs1, 64(a5)<br> [0x80000434]:sd a7, 72(a5)<br>      |
|   6|[0x80002568]<br>0x0000000000000000|- rs1 : f24<br> - rs2 : f11<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xea0b252eae7e0 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xe71ed696201f1 and rm_val == 0  #nosat<br>                                              |[0x80000444]:fsub.d fs3, fs8, fa1, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs3, 80(a5)<br> [0x80000450]:sd a7, 88(a5)<br>      |
|   7|[0x80002578]<br>0x0000000000000000|- rs1 : f30<br> - rs2 : f0<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x13bdffd461269 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1d67f1f990c0b and rm_val == 0  #nosat<br>                                                |[0x80000460]:fsub.d fs0, ft10, ft0, dyn<br> [0x80000464]:csrrs a7, fflags, zero<br> [0x80000468]:fsd fs0, 96(a5)<br> [0x8000046c]:sd a7, 104(a5)<br>    |
|   8|[0x80002588]<br>0x0000000000000000|- rs1 : f28<br> - rs2 : f26<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x27d4b8969c0b2 and fs2 == 0 and fe2 == 0x7fb and fm2 == 0xe60b40e314f0c and rm_val == 0  #nosat<br>                                              |[0x8000047c]:fsub.d fs11, ft8, fs10, dyn<br> [0x80000480]:csrrs a7, fflags, zero<br> [0x80000484]:fsd fs11, 112(a5)<br> [0x80000488]:sd a7, 120(a5)<br> |
|   9|[0x80002598]<br>0x0000000000000000|- rs1 : f23<br> - rs2 : f3<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x070d1456013e3 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xc13973c0771d8 and rm_val == 0  #nosat<br>                                               |[0x80000498]:fsub.d fa4, fs7, ft3, dyn<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:fsd fa4, 128(a5)<br> [0x800004a4]:sd a7, 136(a5)<br>    |
|  10|[0x800025a8]<br>0x0000000000000000|- rs1 : f10<br> - rs2 : f16<br> - rd : f15<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xb877e6e317fa2 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x41981cc935638 and rm_val == 0  #nosat<br>                                              |[0x800004b4]:fsub.d fa5, fa0, fa6, dyn<br> [0x800004b8]:csrrs a7, fflags, zero<br> [0x800004bc]:fsd fa5, 144(a5)<br> [0x800004c0]:sd a7, 152(a5)<br>    |
|  11|[0x800025b8]<br>0x0000000000000000|- rs1 : f19<br> - rs2 : f31<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x8a82024cc4e03 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xd8e5154788b84 and rm_val == 0  #nosat<br>                                               |[0x800004d0]:fsub.d ft2, fs3, ft11, dyn<br> [0x800004d4]:csrrs a7, fflags, zero<br> [0x800004d8]:fsd ft2, 160(a5)<br> [0x800004dc]:sd a7, 168(a5)<br>   |
|  12|[0x800025c8]<br>0x0000000000000000|- rs1 : f15<br> - rs2 : f23<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0125698e86242 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0xeb5aac6486d0c and rm_val == 0  #nosat<br>                                              |[0x800004ec]:fsub.d fs8, fa5, fs7, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsd fs8, 176(a5)<br> [0x800004f8]:sd a7, 184(a5)<br>    |
|  13|[0x800025d8]<br>0x0000000000000000|- rs1 : f6<br> - rs2 : f1<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x930bcbd2d6035 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0xc9378d7a8307f and rm_val == 0  #nosat<br>                                                |[0x80000508]:fsub.d fa1, ft6, ft1, dyn<br> [0x8000050c]:csrrs a7, fflags, zero<br> [0x80000510]:fsd fa1, 192(a5)<br> [0x80000514]:sd a7, 200(a5)<br>    |
|  14|[0x800025e8]<br>0x0000000000000000|- rs1 : f16<br> - rs2 : f10<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xf7646167590ef and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8f5d3484b0730 and rm_val == 0  #nosat<br>                                              |[0x80000524]:fsub.d fs10, fa6, fa0, dyn<br> [0x80000528]:csrrs a7, fflags, zero<br> [0x8000052c]:fsd fs10, 208(a5)<br> [0x80000530]:sd a7, 216(a5)<br>  |
|  15|[0x800025f8]<br>0x0000000000000001|- rs1 : f20<br> - rs2 : f17<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x643f753bef22f and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x51ddbb228ba06 and rm_val == 0  #nosat<br>                                              |[0x80000540]:fsub.d ft9, fs4, fa7, dyn<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:fsd ft9, 224(a5)<br> [0x8000054c]:sd a7, 232(a5)<br>    |
|  16|[0x80002608]<br>0x0000000000000001|- rs1 : f4<br> - rs2 : f6<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0xf57237ddcb451 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd72951a1b8967 and rm_val == 0  #nosat<br>                                                |[0x8000055c]:fsub.d ft11, ft4, ft6, dyn<br> [0x80000560]:csrrs a7, fflags, zero<br> [0x80000564]:fsd ft11, 240(a5)<br> [0x80000568]:sd a7, 248(a5)<br>  |
|  17|[0x80002618]<br>0x0000000000000001|- rs1 : f8<br> - rs2 : f12<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x0ab870b5c1c40 and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x057ed5782c7d6 and rm_val == 0  #nosat<br>                                                |[0x80000578]:fsub.d ft1, fs0, fa2, dyn<br> [0x8000057c]:csrrs a7, fflags, zero<br> [0x80000580]:fsd ft1, 256(a5)<br> [0x80000584]:sd a7, 264(a5)<br>    |
|  18|[0x80002628]<br>0x0000000000000001|- rs1 : f3<br> - rs2 : f22<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x04507a06e8587 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x147f1b87235fc and rm_val == 0  #nosat<br>                                               |[0x80000594]:fsub.d ft10, ft3, fs6, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsd ft10, 272(a5)<br> [0x800005a0]:sd a7, 280(a5)<br>  |
|  19|[0x80002638]<br>0x0000000000000001|- rs1 : f25<br> - rs2 : f21<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x7fb2260b115e9 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x0734092792958 and rm_val == 0  #nosat<br>                                              |[0x800005b0]:fsub.d fa3, fs9, fs5, dyn<br> [0x800005b4]:csrrs a7, fflags, zero<br> [0x800005b8]:fsd fa3, 288(a5)<br> [0x800005bc]:sd a7, 296(a5)<br>    |
|  20|[0x80002648]<br>0x0000000000000001|- rs1 : f21<br> - rs2 : f27<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x67f4f571a752e and fs2 == 0 and fe2 == 0x7f9 and fm2 == 0xd3d8104d0cdc0 and rm_val == 0  #nosat<br>                                              |[0x800005cc]:fsub.d ft8, fs5, fs11, dyn<br> [0x800005d0]:csrrs a7, fflags, zero<br> [0x800005d4]:fsd ft8, 304(a5)<br> [0x800005d8]:sd a7, 312(a5)<br>   |
|  21|[0x80002658]<br>0x0000000000000001|- rs1 : f27<br> - rs2 : f25<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x7fc and fm1 == 0x6251b45dfbd3b and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x80cf7341ff72e and rm_val == 0  #nosat<br>                                               |[0x800005e8]:fsub.d ft7, fs11, fs9, dyn<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:fsd ft7, 320(a5)<br> [0x800005f4]:sd a7, 328(a5)<br>   |
|  22|[0x80002668]<br>0x0000000000000001|- rs1 : f7<br> - rs2 : f5<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x98455e99dfdb1 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x8848cf5ea9657 and rm_val == 0  #nosat<br>                                                |[0x80000604]:fsub.d fs7, ft7, ft5, dyn<br> [0x80000608]:csrrs a7, fflags, zero<br> [0x8000060c]:fsd fs7, 336(a5)<br> [0x80000610]:sd a7, 344(a5)<br>    |
|  23|[0x80002678]<br>0x0000000000000001|- rs1 : f22<br> - rs2 : f4<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x7fa and fm1 == 0x1ad5e9ebc09df and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xaa6c2d4374fa3 and rm_val == 0  #nosat<br>                                               |[0x80000620]:fsub.d fa0, fs6, ft4, dyn<br> [0x80000624]:csrrs a7, fflags, zero<br> [0x80000628]:fsd fa0, 352(a5)<br> [0x8000062c]:sd a7, 360(a5)<br>    |
|  24|[0x80002688]<br>0x0000000000000001|- rs1 : f5<br> - rs2 : f2<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x02b48f992cb49 and fs2 == 1 and fe2 == 0x7fb and fm2 == 0x56e924eb7c838 and rm_val == 0  #nosat<br>                                                |[0x8000063c]:fsub.d fs9, ft5, ft2, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsd fs9, 368(a5)<br> [0x80000648]:sd a7, 376(a5)<br>    |
|  25|[0x80002698]<br>0x0000000000000001|- rs1 : f14<br> - rs2 : f13<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xc3d4499ff58c3 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x2937fe3bd9f20 and rm_val == 0  #nosat<br>                                              |[0x80000658]:fsub.d fs6, fa4, fa3, dyn<br> [0x8000065c]:csrrs a7, fflags, zero<br> [0x80000660]:fsd fs6, 384(a5)<br> [0x80000664]:sd a7, 392(a5)<br>    |
|  26|[0x800026a8]<br>0x0000000000000001|- rs1 : f1<br> - rs2 : f7<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fd and fm1 == 0x36a63c245f557 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x23087ed83ab89 and rm_val == 0  #nosat<br>                                                 |[0x80000674]:fsub.d ft6, ft1, ft7, dyn<br> [0x80000678]:csrrs a7, fflags, zero<br> [0x8000067c]:fsd ft6, 400(a5)<br> [0x80000680]:sd a7, 408(a5)<br>    |
|  27|[0x800026b8]<br>0x0000000000000001|- rs1 : f11<br> - rs2 : f28<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xa8fa703a4078c and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x472096b867e58 and rm_val == 0  #nosat<br>                                              |[0x80000690]:fsub.d fs4, fa1, ft8, dyn<br> [0x80000694]:csrrs a7, fflags, zero<br> [0x80000698]:fsd fs4, 416(a5)<br> [0x8000069c]:sd a7, 424(a5)<br>    |
|  28|[0x800026c8]<br>0x0000000000000001|- rs1 : f31<br> - rs2 : f24<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xdf7523fde6c5d and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x0756bb5d68556 and rm_val == 0  #nosat<br>                                              |[0x800006ac]:fsub.d fa2, ft11, fs8, dyn<br> [0x800006b0]:csrrs a7, fflags, zero<br> [0x800006b4]:fsd fa2, 432(a5)<br> [0x800006b8]:sd a7, 440(a5)<br>   |
|  29|[0x800026d8]<br>0x0000000000000001|- rs1 : f18<br> - rs2 : f20<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x7166677e49c3c and fs2 == 0 and fe2 == 0x7f8 and fm2 == 0x5144e78f2a6c0 and rm_val == 0  #nosat<br>                                              |[0x800006c8]:fsub.d fa7, fs2, fs4, dyn<br> [0x800006cc]:csrrs a7, fflags, zero<br> [0x800006d0]:fsd fa7, 448(a5)<br> [0x800006d4]:sd a7, 456(a5)<br>    |
|  30|[0x800026e8]<br>0x0000000000000001|- rs1 : f12<br> - rs2 : f19<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x7fb and fm1 == 0xef2a4f7c7db7f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xb1c6f0270591a and rm_val == 0  #nosat<br>                                              |[0x800006e4]:fsub.d fa6, fa2, fs3, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd fa6, 464(a5)<br> [0x800006f0]:sd a7, 472(a5)<br>    |
|  31|[0x800026f8]<br>0x0000000000000001|- rs1 : f26<br> - rs2 : f15<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfc2ea66e5019e and fs2 == 0 and fe2 == 0x7fc and fm2 == 0x0f8ef46d602a4 and rm_val == 0  #nosat<br>                                               |[0x80000700]:fsub.d ft5, fs10, fa5, dyn<br> [0x80000704]:csrrs a7, fflags, zero<br> [0x80000708]:fsd ft5, 480(a5)<br> [0x8000070c]:sd a7, 488(a5)<br>   |
|  32|[0x80002708]<br>0x0000000000000001|- rs1 : f2<br> - rs2 : f14<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0x48dace8666677 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0xd35766bc3e2c3 and rm_val == 0  #nosat<br>                                                |[0x8000071c]:fsub.d ft3, ft2, fa4, dyn<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:fsd ft3, 496(a5)<br> [0x80000728]:sd a7, 504(a5)<br>    |
|  33|[0x80002718]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xacd7053aa42a2 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x1fdee0ff3e0e2 and rm_val == 0  #nosat<br>                                                                                             |[0x80000738]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:fsd ft11, 512(a5)<br> [0x80000744]:sd a7, 520(a5)<br> |
|  34|[0x80002728]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0x28bc82f697c4d and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x12bb1d4152629 and rm_val == 0  #nosat<br>                                                                                             |[0x80000754]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000758]:csrrs a7, fflags, zero<br> [0x8000075c]:fsd ft11, 528(a5)<br> [0x80000760]:sd a7, 536(a5)<br> |
|  35|[0x80002738]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xc01045c2cd787 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xed344f30f8d23 and rm_val == 0  #nosat<br>                                                                                             |[0x80000770]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000774]:csrrs a7, fflags, zero<br> [0x80000778]:fsd ft11, 544(a5)<br> [0x8000077c]:sd a7, 552(a5)<br> |
|  36|[0x80002748]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xdd5b61587fd27 and fs2 == 0 and fe2 == 0x7f6 and fm2 == 0x22b4aace78200 and rm_val == 0  #nosat<br>                                                                                             |[0x8000078c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:fsd ft11, 560(a5)<br> [0x80000798]:sd a7, 568(a5)<br> |
|  37|[0x80002758]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc0659af8369fd and fs2 == 1 and fe2 == 0x7fa and fm2 == 0xda84ca746bd30 and rm_val == 0  #nosat<br>                                                                                             |[0x800007a8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007ac]:csrrs a7, fflags, zero<br> [0x800007b0]:fsd ft11, 576(a5)<br> [0x800007b4]:sd a7, 584(a5)<br> |
|  38|[0x80002768]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xdbcde43895c3f and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x8a8c8b3c6f2ff and rm_val == 0  #nosat<br>                                                                                             |[0x800007c4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007c8]:csrrs a7, fflags, zero<br> [0x800007cc]:fsd ft11, 592(a5)<br> [0x800007d0]:sd a7, 600(a5)<br> |
|  39|[0x80002778]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xbb9876f8130c3 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0xa4e630c1be6d7 and rm_val == 0  #nosat<br>                                                                                             |[0x800007e0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800007e4]:csrrs a7, fflags, zero<br> [0x800007e8]:fsd ft11, 608(a5)<br> [0x800007ec]:sd a7, 616(a5)<br> |
|  40|[0x80002788]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xe0d828b86622a and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x1daaf50c76c8b and rm_val == 0  #nosat<br>                                                                                             |[0x800007fc]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000800]:csrrs a7, fflags, zero<br> [0x80000804]:fsd ft11, 624(a5)<br> [0x80000808]:sd a7, 632(a5)<br> |
|  41|[0x80002798]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7f9 and fm1 == 0xa0e7ad32453df and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x082cc69704a64 and rm_val == 0  #nosat<br>                                                                                             |[0x80000818]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000081c]:csrrs a7, fflags, zero<br> [0x80000820]:fsd ft11, 640(a5)<br> [0x80000824]:sd a7, 648(a5)<br> |
|  42|[0x800027a8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xcd87e65450c45 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0xbdde68d2e30aa and rm_val == 0  #nosat<br>                                                                                             |[0x80000834]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:fsd ft11, 656(a5)<br> [0x80000840]:sd a7, 664(a5)<br> |
|  43|[0x800027b8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0xd481499755d4b and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6eda32e0b56e8 and rm_val == 0  #nosat<br>                                                                                             |[0x80000850]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000854]:csrrs a7, fflags, zero<br> [0x80000858]:fsd ft11, 672(a5)<br> [0x8000085c]:sd a7, 680(a5)<br> |
|  44|[0x800027c8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xc81394a2171e9 and fs2 == 1 and fe2 == 0x7fd and fm2 == 0x1ac7cf448b205 and rm_val == 0  #nosat<br>                                                                                             |[0x8000086c]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000870]:csrrs a7, fflags, zero<br> [0x80000874]:fsd ft11, 688(a5)<br> [0x80000878]:sd a7, 696(a5)<br> |
|  45|[0x800027d8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x86499331191c4 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x853587c49095b and rm_val == 0  #nosat<br>                                                                                             |[0x80000888]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000088c]:csrrs a7, fflags, zero<br> [0x80000890]:fsd ft11, 704(a5)<br> [0x80000894]:sd a7, 712(a5)<br> |
|  46|[0x800027e8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0xabe96758f2a09 and fs2 == 0 and fe2 == 0x7fd and fm2 == 0x9cab846424ba1 and rm_val == 0  #nosat<br>                                                                                             |[0x800008a4]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008a8]:csrrs a7, fflags, zero<br> [0x800008ac]:fsd ft11, 720(a5)<br> [0x800008b0]:sd a7, 728(a5)<br> |
|  47|[0x800027f8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fb and fm1 == 0x8072e8f9c858f and fs2 == 0 and fe2 == 0x7fb and fm2 == 0x0c566d30677f7 and rm_val == 0  #nosat<br>                                                                                             |[0x800008c0]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008c4]:csrrs a7, fflags, zero<br> [0x800008c8]:fsd ft11, 736(a5)<br> [0x800008cc]:sd a7, 744(a5)<br> |
|  48|[0x80002808]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x4d9d98184b9d9 and fs2 == 1 and fe2 == 0x7fc and fm2 == 0x18d7cfd491228 and rm_val == 0  #nosat<br>                                                                                             |[0x800008dc]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:fsd ft11, 752(a5)<br> [0x800008e8]:sd a7, 760(a5)<br> |
|  49|[0x80002818]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fd and fm1 == 0xfb5355e167379 and fs2 == 0 and fe2 == 0x7fa and fm2 == 0x785f9927a57c0 and rm_val == 0  #nosat<br>                                                                                             |[0x800008f8]:fsub.d ft11, ft10, ft9, dyn<br> [0x800008fc]:csrrs a7, fflags, zero<br> [0x80000900]:fsd ft11, 768(a5)<br> [0x80000904]:sd a7, 776(a5)<br> |
|  50|[0x80002828]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x870d778409f12 and fs2 == 0 and fe2 == 0x7fe and fm2 == 0x04750f3c7df65 and rm_val == 0  #nosat<br>                                                                                             |[0x80000914]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000918]:csrrs a7, fflags, zero<br> [0x8000091c]:fsd ft11, 784(a5)<br> [0x80000920]:sd a7, 792(a5)<br> |
|  51|[0x80002838]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fc and fm1 == 0x132d8f91b7583 and fs2 == 1 and fe2 == 0x7fe and fm2 == 0x6d1771ceea796 and rm_val == 0  #nosat<br>                                                                                             |[0x80000930]:fsub.d ft11, ft10, ft9, dyn<br> [0x80000934]:csrrs a7, fflags, zero<br> [0x80000938]:fsd ft11, 800(a5)<br> [0x8000093c]:sd a7, 808(a5)<br> |
|  52|[0x80002858]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x7fe and fm1 == 0x913b4236d8411 and fs2 == 1 and fe2 == 0x7fa and fm2 == 0x2db788640aba0 and rm_val == 0  #nosat<br>                                                                                             |[0x80000968]:fsub.d ft11, ft10, ft9, dyn<br> [0x8000096c]:csrrs a7, fflags, zero<br> [0x80000970]:fsd ft11, 832(a5)<br> [0x80000974]:sd a7, 840(a5)<br> |
