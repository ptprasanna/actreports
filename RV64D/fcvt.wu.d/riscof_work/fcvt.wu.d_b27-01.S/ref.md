
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000700')]      |
| SIG_REGION                | [('0x80002310', '0x80002510', '64 dwords')]      |
| COV_LABELS                | fcvt.wu.d_b27      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.wu.d/riscof_work/fcvt.wu.d_b27-01.S/ref.S    |
| Total Number of coverpoints| 77     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.wu.d', 'rd : x12', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.wu.d a2, ft5, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd a2, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002318]:0x0000000000000010




Last Coverpoint : ['rd : x26', 'rs1 : f27', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fcvt.wu.d s10, fs11, dyn
	-[0x800003d0]:csrrs a7, fflags, zero
	-[0x800003d4]:sd s10, 16(a5)
Current Store : [0x800003d8] : sd a7, 24(a5) -- Store: [0x80002328]:0x0000000000000010




Last Coverpoint : ['rd : x1', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fcvt.wu.d ra, fs0, dyn
	-[0x800003e8]:csrrs a7, fflags, zero
	-[0x800003ec]:sd ra, 32(a5)
Current Store : [0x800003f0] : sd a7, 40(a5) -- Store: [0x80002338]:0x0000000000000010




Last Coverpoint : ['rd : x6', 'rs1 : f16', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.wu.d t1, fa6, dyn
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd t1, 48(a5)
Current Store : [0x80000408] : sd a7, 56(a5) -- Store: [0x80002348]:0x0000000000000010




Last Coverpoint : ['rd : x20', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000414]:fcvt.wu.d s4, fa4, dyn
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd s4, 64(a5)
Current Store : [0x80000420] : sd a7, 72(a5) -- Store: [0x80002358]:0x0000000000000010




Last Coverpoint : ['rd : x9', 'rs1 : f18', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.wu.d s1, fs2, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd s1, 80(a5)
Current Store : [0x80000438] : sd a7, 88(a5) -- Store: [0x80002368]:0x0000000000000010




Last Coverpoint : ['rd : x11', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.wu.d a1, fs7, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd a1, 96(a5)
Current Store : [0x80000450] : sd a7, 104(a5) -- Store: [0x80002378]:0x0000000000000010




Last Coverpoint : ['rd : x28', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.wu.d t3, fa7, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd t3, 112(a5)
Current Store : [0x80000468] : sd a7, 120(a5) -- Store: [0x80002388]:0x0000000000000010




Last Coverpoint : ['rd : x2', 'rs1 : f9']
Last Code Sequence : 
	-[0x80000474]:fcvt.wu.d sp, fs1, dyn
	-[0x80000478]:csrrs a7, fflags, zero
	-[0x8000047c]:sd sp, 128(a5)
Current Store : [0x80000480] : sd a7, 136(a5) -- Store: [0x80002398]:0x0000000000000010




Last Coverpoint : ['rd : x25', 'rs1 : f13']
Last Code Sequence : 
	-[0x8000048c]:fcvt.wu.d s9, fa3, dyn
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd s9, 144(a5)
Current Store : [0x80000498] : sd a7, 152(a5) -- Store: [0x800023a8]:0x0000000000000010




Last Coverpoint : ['rd : x21', 'rs1 : f4']
Last Code Sequence : 
	-[0x800004a4]:fcvt.wu.d s5, ft4, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s5, 160(a5)
Current Store : [0x800004b0] : sd a7, 168(a5) -- Store: [0x800023b8]:0x0000000000000010




Last Coverpoint : ['rd : x18', 'rs1 : f29']
Last Code Sequence : 
	-[0x800004bc]:fcvt.wu.d s2, ft9, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd s2, 176(a5)
Current Store : [0x800004c8] : sd a7, 184(a5) -- Store: [0x800023c8]:0x0000000000000010




Last Coverpoint : ['rd : x30', 'rs1 : f21']
Last Code Sequence : 
	-[0x800004d4]:fcvt.wu.d t5, fs5, dyn
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd t5, 192(a5)
Current Store : [0x800004e0] : sd a7, 200(a5) -- Store: [0x800023d8]:0x0000000000000010




Last Coverpoint : ['rd : x24', 'rs1 : f7']
Last Code Sequence : 
	-[0x800004ec]:fcvt.wu.d s8, ft7, dyn
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd s8, 208(a5)
Current Store : [0x800004f8] : sd a7, 216(a5) -- Store: [0x800023e8]:0x0000000000000010




Last Coverpoint : ['rd : x29', 'rs1 : f15']
Last Code Sequence : 
	-[0x80000504]:fcvt.wu.d t4, fa5, dyn
	-[0x80000508]:csrrs a7, fflags, zero
	-[0x8000050c]:sd t4, 224(a5)
Current Store : [0x80000510] : sd a7, 232(a5) -- Store: [0x800023f8]:0x0000000000000010




Last Coverpoint : ['rd : x5', 'rs1 : f3']
Last Code Sequence : 
	-[0x8000051c]:fcvt.wu.d t0, ft3, dyn
	-[0x80000520]:csrrs a7, fflags, zero
	-[0x80000524]:sd t0, 240(a5)
Current Store : [0x80000528] : sd a7, 248(a5) -- Store: [0x80002408]:0x0000000000000010




Last Coverpoint : ['rd : x7', 'rs1 : f0']
Last Code Sequence : 
	-[0x80000534]:fcvt.wu.d t2, ft0, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd t2, 256(a5)
Current Store : [0x80000540] : sd a7, 264(a5) -- Store: [0x80002418]:0x0000000000000010




Last Coverpoint : ['rd : x15', 'rs1 : f12']
Last Code Sequence : 
	-[0x80000558]:fcvt.wu.d a5, fa2, dyn
	-[0x8000055c]:csrrs s5, fflags, zero
	-[0x80000560]:sd a5, 0(s3)
Current Store : [0x80000564] : sd s5, 8(s3) -- Store: [0x80002428]:0x0000000000000010




Last Coverpoint : ['rd : x31', 'rs1 : f22']
Last Code Sequence : 
	-[0x8000057c]:fcvt.wu.d t6, fs6, dyn
	-[0x80000580]:csrrs a7, fflags, zero
	-[0x80000584]:sd t6, 0(a5)
Current Store : [0x80000588] : sd a7, 8(a5) -- Store: [0x80002438]:0x0000000000000010




Last Coverpoint : ['rd : x22', 'rs1 : f24']
Last Code Sequence : 
	-[0x80000594]:fcvt.wu.d s6, fs8, dyn
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd s6, 16(a5)
Current Store : [0x800005a0] : sd a7, 24(a5) -- Store: [0x80002448]:0x0000000000000010




Last Coverpoint : ['rd : x4', 'rs1 : f1']
Last Code Sequence : 
	-[0x800005ac]:fcvt.wu.d tp, ft1, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd tp, 32(a5)
Current Store : [0x800005b8] : sd a7, 40(a5) -- Store: [0x80002458]:0x0000000000000010




Last Coverpoint : ['rd : x3', 'rs1 : f19']
Last Code Sequence : 
	-[0x800005c4]:fcvt.wu.d gp, fs3, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd gp, 48(a5)
Current Store : [0x800005d0] : sd a7, 56(a5) -- Store: [0x80002468]:0x0000000000000010




Last Coverpoint : ['rd : x14', 'rs1 : f26']
Last Code Sequence : 
	-[0x800005dc]:fcvt.wu.d a4, fs10, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd a4, 64(a5)
Current Store : [0x800005e8] : sd a7, 72(a5) -- Store: [0x80002478]:0x0000000000000010




Last Coverpoint : ['rd : x10', 'rs1 : f28']
Last Code Sequence : 
	-[0x800005f4]:fcvt.wu.d a0, ft8, dyn
	-[0x800005f8]:csrrs a7, fflags, zero
	-[0x800005fc]:sd a0, 80(a5)
Current Store : [0x80000600] : sd a7, 88(a5) -- Store: [0x80002488]:0x0000000000000010




Last Coverpoint : ['rd : x16', 'rs1 : f30']
Last Code Sequence : 
	-[0x80000618]:fcvt.wu.d a6, ft10, dyn
	-[0x8000061c]:csrrs s5, fflags, zero
	-[0x80000620]:sd a6, 0(s3)
Current Store : [0x80000624] : sd s5, 8(s3) -- Store: [0x80002498]:0x0000000000000010




Last Coverpoint : ['rd : x8', 'rs1 : f2']
Last Code Sequence : 
	-[0x8000063c]:fcvt.wu.d fp, ft2, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd fp, 0(a5)
Current Store : [0x80000648] : sd a7, 8(a5) -- Store: [0x800024a8]:0x0000000000000010




Last Coverpoint : ['rd : x13', 'rs1 : f10']
Last Code Sequence : 
	-[0x80000654]:fcvt.wu.d a3, fa0, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd a3, 16(a5)
Current Store : [0x80000660] : sd a7, 24(a5) -- Store: [0x800024b8]:0x0000000000000010




Last Coverpoint : ['rd : x17', 'rs1 : f25']
Last Code Sequence : 
	-[0x80000678]:fcvt.wu.d a7, fs9, dyn
	-[0x8000067c]:csrrs s5, fflags, zero
	-[0x80000680]:sd a7, 0(s3)
Current Store : [0x80000684] : sd s5, 8(s3) -- Store: [0x800024c8]:0x0000000000000010




Last Coverpoint : ['rd : x27', 'rs1 : f31']
Last Code Sequence : 
	-[0x8000069c]:fcvt.wu.d s11, ft11, dyn
	-[0x800006a0]:csrrs a7, fflags, zero
	-[0x800006a4]:sd s11, 0(a5)
Current Store : [0x800006a8] : sd a7, 8(a5) -- Store: [0x800024d8]:0x0000000000000010




Last Coverpoint : ['rd : x0', 'rs1 : f6']
Last Code Sequence : 
	-[0x800006b4]:fcvt.wu.d zero, ft6, dyn
	-[0x800006b8]:csrrs a7, fflags, zero
	-[0x800006bc]:sd zero, 16(a5)
Current Store : [0x800006c0] : sd a7, 24(a5) -- Store: [0x800024e8]:0x0000000000000010




Last Coverpoint : ['rd : x19', 'rs1 : f11']
Last Code Sequence : 
	-[0x800006cc]:fcvt.wu.d s3, fa1, dyn
	-[0x800006d0]:csrrs a7, fflags, zero
	-[0x800006d4]:sd s3, 32(a5)
Current Store : [0x800006d8] : sd a7, 40(a5) -- Store: [0x800024f8]:0x0000000000000010




Last Coverpoint : ['rd : x23', 'rs1 : f20']
Last Code Sequence : 
	-[0x800006e4]:fcvt.wu.d s7, fs4, dyn
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd s7, 48(a5)
Current Store : [0x800006f0] : sd a7, 56(a5) -- Store: [0x80002508]:0x0000000000000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                               coverpoints                                                                |                                                       code                                                        |
|---:|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0xFFFFFFFFFFFFFFFF|- opcode : fcvt.wu.d<br> - rd : x12<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.wu.d a2, ft5, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd a2, 0(a5)<br>      |
|   2|[0x80002320]<br>0xFFFFFFFFFFFFFFFF|- rd : x26<br> - rs1 : f27<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat<br>                         |[0x800003cc]:fcvt.wu.d s10, fs11, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:sd s10, 16(a5)<br>  |
|   3|[0x80002330]<br>0xFFFFFFFFFFFFFFFF|- rd : x1<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0xc000000000001 and rm_val == 0  #nosat<br>                           |[0x800003e4]:fcvt.wu.d ra, fs0, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:sd ra, 32(a5)<br>     |
|   4|[0x80002340]<br>0xFFFFFFFFFFFFFFFF|- rd : x6<br> - rs1 : f16<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                          |[0x800003fc]:fcvt.wu.d t1, fa6, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd t1, 48(a5)<br>     |
|   5|[0x80002350]<br>0xFFFFFFFFFFFFFFFF|- rd : x20<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                         |[0x80000414]:fcvt.wu.d s4, fa4, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd s4, 64(a5)<br>     |
|   6|[0x80002360]<br>0xFFFFFFFFFFFFFFFF|- rd : x9<br> - rs1 : f18<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat<br>                          |[0x8000042c]:fcvt.wu.d s1, fs2, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd s1, 80(a5)<br>     |
|   7|[0x80002370]<br>0xFFFFFFFFFFFFFFFF|- rd : x11<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x4aaaaaaaaaaaa and rm_val == 0  #nosat<br>                         |[0x80000444]:fcvt.wu.d a1, fs7, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd a1, 96(a5)<br>     |
|   8|[0x80002380]<br>0xFFFFFFFFFFFFFFFF|- rd : x28<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                         |[0x8000045c]:fcvt.wu.d t3, fa7, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd t3, 112(a5)<br>    |
|   9|[0x80002390]<br>0x0000000000000000|- rd : x2<br> - rs1 : f9<br>                                                                                                              |[0x80000474]:fcvt.wu.d sp, fs1, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:sd sp, 128(a5)<br>    |
|  10|[0x800023a0]<br>0x0000000000000000|- rd : x25<br> - rs1 : f13<br>                                                                                                            |[0x8000048c]:fcvt.wu.d s9, fa3, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd s9, 144(a5)<br>    |
|  11|[0x800023b0]<br>0x0000000000000000|- rd : x21<br> - rs1 : f4<br>                                                                                                             |[0x800004a4]:fcvt.wu.d s5, ft4, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s5, 160(a5)<br>    |
|  12|[0x800023c0]<br>0x0000000000000000|- rd : x18<br> - rs1 : f29<br>                                                                                                            |[0x800004bc]:fcvt.wu.d s2, ft9, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd s2, 176(a5)<br>    |
|  13|[0x800023d0]<br>0x0000000000000000|- rd : x30<br> - rs1 : f21<br>                                                                                                            |[0x800004d4]:fcvt.wu.d t5, fs5, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd t5, 192(a5)<br>    |
|  14|[0x800023e0]<br>0x0000000000000000|- rd : x24<br> - rs1 : f7<br>                                                                                                             |[0x800004ec]:fcvt.wu.d s8, ft7, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd s8, 208(a5)<br>    |
|  15|[0x800023f0]<br>0x0000000000000000|- rd : x29<br> - rs1 : f15<br>                                                                                                            |[0x80000504]:fcvt.wu.d t4, fa5, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:sd t4, 224(a5)<br>    |
|  16|[0x80002400]<br>0x0000000000000000|- rd : x5<br> - rs1 : f3<br>                                                                                                              |[0x8000051c]:fcvt.wu.d t0, ft3, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:sd t0, 240(a5)<br>    |
|  17|[0x80002410]<br>0x0000000000000000|- rd : x7<br> - rs1 : f0<br>                                                                                                              |[0x80000534]:fcvt.wu.d t2, ft0, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd t2, 256(a5)<br>    |
|  18|[0x80002420]<br>0x0000000000000000|- rd : x15<br> - rs1 : f12<br>                                                                                                            |[0x80000558]:fcvt.wu.d a5, fa2, dyn<br> [0x8000055c]:csrrs s5, fflags, zero<br> [0x80000560]:sd a5, 0(s3)<br>      |
|  19|[0x80002430]<br>0x0000000000000000|- rd : x31<br> - rs1 : f22<br>                                                                                                            |[0x8000057c]:fcvt.wu.d t6, fs6, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:sd t6, 0(a5)<br>      |
|  20|[0x80002440]<br>0x0000000000000000|- rd : x22<br> - rs1 : f24<br>                                                                                                            |[0x80000594]:fcvt.wu.d s6, fs8, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd s6, 16(a5)<br>     |
|  21|[0x80002450]<br>0x0000000000000000|- rd : x4<br> - rs1 : f1<br>                                                                                                              |[0x800005ac]:fcvt.wu.d tp, ft1, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd tp, 32(a5)<br>     |
|  22|[0x80002460]<br>0x0000000000000000|- rd : x3<br> - rs1 : f19<br>                                                                                                             |[0x800005c4]:fcvt.wu.d gp, fs3, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd gp, 48(a5)<br>     |
|  23|[0x80002470]<br>0x0000000000000000|- rd : x14<br> - rs1 : f26<br>                                                                                                            |[0x800005dc]:fcvt.wu.d a4, fs10, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd a4, 64(a5)<br>    |
|  24|[0x80002480]<br>0x0000000000000000|- rd : x10<br> - rs1 : f28<br>                                                                                                            |[0x800005f4]:fcvt.wu.d a0, ft8, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:sd a0, 80(a5)<br>     |
|  25|[0x80002490]<br>0x0000000000000000|- rd : x16<br> - rs1 : f30<br>                                                                                                            |[0x80000618]:fcvt.wu.d a6, ft10, dyn<br> [0x8000061c]:csrrs s5, fflags, zero<br> [0x80000620]:sd a6, 0(s3)<br>     |
|  26|[0x800024a0]<br>0x0000000000000000|- rd : x8<br> - rs1 : f2<br>                                                                                                              |[0x8000063c]:fcvt.wu.d fp, ft2, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd fp, 0(a5)<br>      |
|  27|[0x800024b0]<br>0x0000000000000000|- rd : x13<br> - rs1 : f10<br>                                                                                                            |[0x80000654]:fcvt.wu.d a3, fa0, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd a3, 16(a5)<br>     |
|  28|[0x800024c0]<br>0x0000000000000000|- rd : x17<br> - rs1 : f25<br>                                                                                                            |[0x80000678]:fcvt.wu.d a7, fs9, dyn<br> [0x8000067c]:csrrs s5, fflags, zero<br> [0x80000680]:sd a7, 0(s3)<br>      |
|  29|[0x800024d0]<br>0x0000000000000000|- rd : x27<br> - rs1 : f31<br>                                                                                                            |[0x8000069c]:fcvt.wu.d s11, ft11, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:sd s11, 0(a5)<br>   |
|  30|[0x800024e0]<br>0x0000000000000000|- rd : x0<br> - rs1 : f6<br>                                                                                                              |[0x800006b4]:fcvt.wu.d zero, ft6, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:sd zero, 16(a5)<br> |
|  31|[0x800024f0]<br>0x0000000000000000|- rd : x19<br> - rs1 : f11<br>                                                                                                            |[0x800006cc]:fcvt.wu.d s3, fa1, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:sd s3, 32(a5)<br>     |
|  32|[0x80002500]<br>0x0000000000000000|- rd : x23<br> - rs1 : f20<br>                                                                                                            |[0x800006e4]:fcvt.wu.d s7, fs4, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd s7, 48(a5)<br>     |
