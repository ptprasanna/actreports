
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000990')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | fsd-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fsd/riscof_work/fsd-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                  coverpoints                                                   |                                                                                        code                                                                                        |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002210]<br>0x0000000000000000|- opcode : fsd<br> - rs1 : x13<br> - rs2 : f21<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x800003c0]:fsd fs5, 4(a3)<br> [0x800003c4]:addi zero, zero, 0<br> [0x800003c8]:addi zero, zero, 0<br> [0x800003cc]:csrrs a7, fflags, zero<br> [0x800003d0]:sd a7, 0(a5)<br>       |
|   2|[0x80002220]<br>0x0000000000000000|- rs1 : x14<br> - rs2 : f19<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                      |[0x800003f0]:fsd fs3, 5(a4)<br> [0x800003f4]:addi zero, zero, 0<br> [0x800003f8]:addi zero, zero, 0<br> [0x800003fc]:csrrs a7, fflags, zero<br> [0x80000400]:sd a7, 16(a5)<br>      |
|   3|[0x80002230]<br>0x0000000000000000|- rs1 : x29<br> - rs2 : f5<br> - ea_align == 0 and (imm_val % 4) == 2<br>                                       |[0x80000420]:fsd ft5, 2(t4)<br> [0x80000424]:addi zero, zero, 0<br> [0x80000428]:addi zero, zero, 0<br> [0x8000042c]:csrrs a7, fflags, zero<br> [0x80000430]:sd a7, 32(a5)<br>      |
|   4|[0x80002240]<br>0x0000000000000000|- rs1 : x3<br> - rs2 : f25<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                       |[0x80000454]:fsd fs9, 7(gp)<br> [0x80000458]:addi zero, zero, 0<br> [0x8000045c]:addi zero, zero, 0<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd a7, 48(a5)<br>      |
|   5|[0x80002250]<br>0x0000000000000000|- rs1 : x5<br> - rs2 : f16<br> - imm_val < 0<br>                                                                |[0x80000480]:fsd fa6, 4087(t0)<br> [0x80000484]:addi zero, zero, 0<br> [0x80000488]:addi zero, zero, 0<br> [0x8000048c]:csrrs a7, fflags, zero<br> [0x80000490]:sd a7, 64(a5)<br>   |
|   6|[0x80002260]<br>0x0000000000000000|- rs1 : x22<br> - rs2 : f26<br> - imm_val == 0<br>                                                              |[0x800004b0]:fsd fs10, 0(s6)<br> [0x800004b4]:addi zero, zero, 0<br> [0x800004b8]:addi zero, zero, 0<br> [0x800004bc]:csrrs a7, fflags, zero<br> [0x800004c0]:sd a7, 80(a5)<br>     |
|   7|[0x80002270]<br>0x0000000000000000|- rs1 : x26<br> - rs2 : f31<br>                                                                                 |[0x800004dc]:fsd ft11, 2048(s10)<br> [0x800004e0]:addi zero, zero, 0<br> [0x800004e4]:addi zero, zero, 0<br> [0x800004e8]:csrrs a7, fflags, zero<br> [0x800004ec]:sd a7, 96(a5)<br> |
|   8|[0x80002280]<br>0x0000000000000000|- rs1 : x23<br> - rs2 : f28<br>                                                                                 |[0x80000508]:fsd ft8, 2048(s7)<br> [0x8000050c]:addi zero, zero, 0<br> [0x80000510]:addi zero, zero, 0<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:sd a7, 112(a5)<br>  |
|   9|[0x80002290]<br>0x0000000000000000|- rs1 : x31<br> - rs2 : f27<br>                                                                                 |[0x80000534]:fsd fs11, 2048(t6)<br> [0x80000538]:addi zero, zero, 0<br> [0x8000053c]:addi zero, zero, 0<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:sd a7, 128(a5)<br> |
|  10|[0x800022a0]<br>0x0000000000000000|- rs1 : x19<br> - rs2 : f17<br>                                                                                 |[0x80000560]:fsd fa7, 2048(s3)<br> [0x80000564]:addi zero, zero, 0<br> [0x80000568]:addi zero, zero, 0<br> [0x8000056c]:csrrs a7, fflags, zero<br> [0x80000570]:sd a7, 144(a5)<br>  |
|  11|[0x800022b0]<br>0x0000000000000000|- rs1 : x9<br> - rs2 : f11<br>                                                                                  |[0x8000058c]:fsd fa1, 2048(s1)<br> [0x80000590]:addi zero, zero, 0<br> [0x80000594]:addi zero, zero, 0<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd a7, 160(a5)<br>  |
|  12|[0x800022c0]<br>0x0000000000000000|- rs1 : x24<br> - rs2 : f22<br>                                                                                 |[0x800005b8]:fsd fs6, 2048(s8)<br> [0x800005bc]:addi zero, zero, 0<br> [0x800005c0]:addi zero, zero, 0<br> [0x800005c4]:csrrs a7, fflags, zero<br> [0x800005c8]:sd a7, 176(a5)<br>  |
|  13|[0x800022d0]<br>0x0000000000000000|- rs1 : x7<br> - rs2 : f8<br>                                                                                   |[0x800005e4]:fsd fs0, 2048(t2)<br> [0x800005e8]:addi zero, zero, 0<br> [0x800005ec]:addi zero, zero, 0<br> [0x800005f0]:csrrs a7, fflags, zero<br> [0x800005f4]:sd a7, 192(a5)<br>  |
|  14|[0x800022e0]<br>0x0000000000000000|- rs1 : x1<br> - rs2 : f4<br>                                                                                   |[0x80000610]:fsd ft4, 2048(ra)<br> [0x80000614]:addi zero, zero, 0<br> [0x80000618]:addi zero, zero, 0<br> [0x8000061c]:csrrs a7, fflags, zero<br> [0x80000620]:sd a7, 208(a5)<br>  |
|  15|[0x800022f0]<br>0x0000000000000000|- rs1 : x12<br> - rs2 : f23<br>                                                                                 |[0x8000063c]:fsd fs7, 2048(a2)<br> [0x80000640]:addi zero, zero, 0<br> [0x80000644]:addi zero, zero, 0<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:sd a7, 224(a5)<br>  |
|  16|[0x80002300]<br>0x0000000000000000|- rs1 : x30<br> - rs2 : f3<br>                                                                                  |[0x80000668]:fsd ft3, 2048(t5)<br> [0x8000066c]:addi zero, zero, 0<br> [0x80000670]:addi zero, zero, 0<br> [0x80000674]:csrrs a7, fflags, zero<br> [0x80000678]:sd a7, 240(a5)<br>  |
|  17|[0x80002310]<br>0x0000000000000000|- rs1 : x17<br> - rs2 : f0<br>                                                                                  |[0x800006a0]:fsd ft0, 2048(a7)<br> [0x800006a4]:addi zero, zero, 0<br> [0x800006a8]:addi zero, zero, 0<br> [0x800006ac]:csrrs s5, fflags, zero<br> [0x800006b0]:sd s5, 0(s3)<br>    |
|  18|[0x80002320]<br>0x0000000000000000|- rs1 : x11<br> - rs2 : f7<br>                                                                                  |[0x800006d8]:fsd ft7, 2048(a1)<br> [0x800006dc]:addi zero, zero, 0<br> [0x800006e0]:addi zero, zero, 0<br> [0x800006e4]:csrrs a7, fflags, zero<br> [0x800006e8]:sd a7, 0(a5)<br>    |
|  19|[0x80002330]<br>0x0000000000000000|- rs1 : x6<br> - rs2 : f10<br>                                                                                  |[0x80000704]:fsd fa0, 2048(t1)<br> [0x80000708]:addi zero, zero, 0<br> [0x8000070c]:addi zero, zero, 0<br> [0x80000710]:csrrs a7, fflags, zero<br> [0x80000714]:sd a7, 16(a5)<br>   |
|  20|[0x80002340]<br>0x0000000000000000|- rs1 : x8<br> - rs2 : f6<br>                                                                                   |[0x80000730]:fsd ft6, 2048(fp)<br> [0x80000734]:addi zero, zero, 0<br> [0x80000738]:addi zero, zero, 0<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:sd a7, 32(a5)<br>   |
|  21|[0x80002350]<br>0x0000000000000000|- rs1 : x28<br> - rs2 : f15<br>                                                                                 |[0x8000075c]:fsd fa5, 2048(t3)<br> [0x80000760]:addi zero, zero, 0<br> [0x80000764]:addi zero, zero, 0<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:sd a7, 48(a5)<br>   |
|  22|[0x80002360]<br>0x0000000000000000|- rs1 : x15<br> - rs2 : f24<br>                                                                                 |[0x80000794]:fsd fs8, 2048(a5)<br> [0x80000798]:addi zero, zero, 0<br> [0x8000079c]:addi zero, zero, 0<br> [0x800007a0]:csrrs s5, fflags, zero<br> [0x800007a4]:sd s5, 0(s3)<br>    |
|  23|[0x80002370]<br>0x0000000000000000|- rs1 : x21<br> - rs2 : f29<br>                                                                                 |[0x800007cc]:fsd ft9, 2048(s5)<br> [0x800007d0]:addi zero, zero, 0<br> [0x800007d4]:addi zero, zero, 0<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:sd a7, 0(a5)<br>    |
|  24|[0x80002380]<br>0x0000000000000000|- rs1 : x16<br> - rs2 : f20<br>                                                                                 |[0x80000804]:fsd fs4, 2048(a6)<br> [0x80000808]:addi zero, zero, 0<br> [0x8000080c]:addi zero, zero, 0<br> [0x80000810]:csrrs s5, fflags, zero<br> [0x80000814]:sd s5, 0(s3)<br>    |
|  25|[0x80002390]<br>0x0000000000000000|- rs1 : x18<br> - rs2 : f12<br>                                                                                 |[0x8000083c]:fsd fa2, 2048(s2)<br> [0x80000840]:addi zero, zero, 0<br> [0x80000844]:addi zero, zero, 0<br> [0x80000848]:csrrs a7, fflags, zero<br> [0x8000084c]:sd a7, 0(a5)<br>    |
|  26|[0x800023a0]<br>0x0000000000000000|- rs1 : x20<br> - rs2 : f2<br>                                                                                  |[0x80000868]:fsd ft2, 2048(s4)<br> [0x8000086c]:addi zero, zero, 0<br> [0x80000870]:addi zero, zero, 0<br> [0x80000874]:csrrs a7, fflags, zero<br> [0x80000878]:sd a7, 16(a5)<br>   |
|  27|[0x800023b0]<br>0x0000000000000000|- rs1 : x25<br> - rs2 : f13<br>                                                                                 |[0x80000894]:fsd fa3, 2048(s9)<br> [0x80000898]:addi zero, zero, 0<br> [0x8000089c]:addi zero, zero, 0<br> [0x800008a0]:csrrs a7, fflags, zero<br> [0x800008a4]:sd a7, 32(a5)<br>   |
|  28|[0x800023c0]<br>0x0000000000000000|- rs1 : x10<br> - rs2 : f9<br>                                                                                  |[0x800008c0]:fsd fs1, 2048(a0)<br> [0x800008c4]:addi zero, zero, 0<br> [0x800008c8]:addi zero, zero, 0<br> [0x800008cc]:csrrs a7, fflags, zero<br> [0x800008d0]:sd a7, 48(a5)<br>   |
|  29|[0x800023d0]<br>0x0000000000000000|- rs1 : x2<br> - rs2 : f14<br>                                                                                  |[0x800008ec]:fsd fa4, 2048(sp)<br> [0x800008f0]:addi zero, zero, 0<br> [0x800008f4]:addi zero, zero, 0<br> [0x800008f8]:csrrs a7, fflags, zero<br> [0x800008fc]:sd a7, 64(a5)<br>   |
|  30|[0x800023e0]<br>0x0000000000000000|- rs1 : x27<br> - rs2 : f18<br>                                                                                 |[0x80000918]:fsd fs2, 2048(s11)<br> [0x8000091c]:addi zero, zero, 0<br> [0x80000920]:addi zero, zero, 0<br> [0x80000924]:csrrs a7, fflags, zero<br> [0x80000928]:sd a7, 80(a5)<br>  |
|  31|[0x800023f0]<br>0x0000000000000000|- rs1 : x4<br> - rs2 : f1<br>                                                                                   |[0x80000944]:fsd ft1, 2048(tp)<br> [0x80000948]:addi zero, zero, 0<br> [0x8000094c]:addi zero, zero, 0<br> [0x80000950]:csrrs a7, fflags, zero<br> [0x80000954]:sd a7, 96(a5)<br>   |
|  32|[0x80002400]<br>0x0000000000000000|- rs2 : f30<br>                                                                                                 |[0x80000970]:fsd ft10, 2048(a1)<br> [0x80000974]:addi zero, zero, 0<br> [0x80000978]:addi zero, zero, 0<br> [0x8000097c]:csrrs a7, fflags, zero<br> [0x80000980]:sd a7, 112(a5)<br> |
