
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000a10')]      |
| SIG_REGION                | [('0x80002410', '0x80002820', '130 dwords')]      |
| COV_LABELS                | fmv.d.x_b26      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fmv.d.x/riscof_work/fmv.d.x_b26-01.S/ref.S    |
| Total Number of coverpoints| 133     |
| Total Coverpoints Hit     | 129      |
| Total Signature Updates   | 65      |
| STAT1                     | 65      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                          coverpoints                                           |                                                                    code                                                                     |
|---:|----------------------------------|------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002418]<br>0x0000000000000000|- opcode : fmv.d.x<br> - rs1 : x28<br> - rd : f0<br> - rs1_val == 0 and rm_val == 0  #nosat<br> |[0x800003b4]:fmv.d.x ft0, t3<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsd ft0, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>       |
|   2|[0x80002428]<br>0x0000000000000000|- rs1 : x8<br> - rd : f25<br> - rs1_val == 9184267462870993263 and rm_val == 0  #nosat<br>      |[0x800003cc]:fmv.d.x fs9, fp<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsd fs9, 16(a5)<br> [0x800003d8]:sd a7, 24(a5)<br>     |
|   3|[0x80002438]<br>0x0000000000000000|- rs1 : x4<br> - rd : f5<br> - rs1_val == 3035559518675506972 and rm_val == 0  #nosat<br>       |[0x800003e4]:fmv.d.x ft5, tp<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd ft5, 32(a5)<br> [0x800003f0]:sd a7, 40(a5)<br>     |
|   4|[0x80002448]<br>0x0000000000000000|- rs1 : x1<br> - rd : f1<br> - rs1_val == 2086309477244717835 and rm_val == 0  #nosat<br>       |[0x800003fc]:fmv.d.x ft1, ra<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:fsd ft1, 48(a5)<br> [0x80000408]:sd a7, 56(a5)<br>     |
|   5|[0x80002458]<br>0x0000000000000000|- rs1 : x26<br> - rd : f10<br> - rs1_val == 878257878219487117 and rm_val == 0  #nosat<br>      |[0x80000414]:fmv.d.x fa0, s10<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fa0, 64(a5)<br> [0x80000420]:sd a7, 72(a5)<br>    |
|   6|[0x80002468]<br>0x0000000000000000|- rs1 : x21<br> - rd : f6<br> - rs1_val == 428092830716901554 and rm_val == 0  #nosat<br>       |[0x8000042c]:fmv.d.x ft6, s5<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsd ft6, 80(a5)<br> [0x80000438]:sd a7, 88(a5)<br>     |
|   7|[0x80002478]<br>0x0000000000000000|- rs1 : x7<br> - rd : f20<br> - rs1_val == 156703057381110404 and rm_val == 0  #nosat<br>       |[0x80000444]:fmv.d.x fs4, t2<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs4, 96(a5)<br> [0x80000450]:sd a7, 104(a5)<br>    |
|   8|[0x80002488]<br>0x0000000000000000|- rs1 : x6<br> - rd : f21<br> - rs1_val == 104291213792325832 and rm_val == 0  #nosat<br>       |[0x8000045c]:fmv.d.x fs5, t1<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:fsd fs5, 112(a5)<br> [0x80000468]:sd a7, 120(a5)<br>   |
|   9|[0x80002498]<br>0x0000000000000000|- rs1 : x23<br> - rd : f31<br> - rs1_val == 59668294213987868 and rm_val == 0  #nosat<br>       |[0x80000474]:fmv.d.x ft11, s7<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsd ft11, 128(a5)<br> [0x80000480]:sd a7, 136(a5)<br> |
|  10|[0x800024a8]<br>0x0000000000000000|- rs1 : x10<br> - rd : f8<br> - rs1_val == 24358691315317906 and rm_val == 0  #nosat<br>        |[0x8000048c]:fmv.d.x fs0, a0<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsd fs0, 144(a5)<br> [0x80000498]:sd a7, 152(a5)<br>   |
|  11|[0x800024b8]<br>0x0000000000000000|- rs1 : x16<br> - rd : f7<br> - rs1_val == 12147253371253868 and rm_val == 0  #nosat<br>        |[0x800004b0]:fmv.d.x ft7, a6<br> [0x800004b4]:csrrs s5, fflags, zero<br> [0x800004b8]:fsd ft7, 0(s3)<br> [0x800004bc]:sd s5, 8(s3)<br>       |
|  12|[0x800024c8]<br>0x0000000000000000|- rs1 : x12<br> - rd : f11<br> - rs1_val == 7228908657904184 and rm_val == 0  #nosat<br>        |[0x800004d4]:fmv.d.x fa1, a2<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsd fa1, 0(a5)<br> [0x800004e0]:sd a7, 8(a5)<br>       |
|  13|[0x800024d8]<br>0x0000000000000000|- rs1 : x24<br> - rd : f2<br> - rs1_val == 3454382579804098 and rm_val == 0  #nosat<br>         |[0x800004ec]:fmv.d.x ft2, s8<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsd ft2, 16(a5)<br> [0x800004f8]:sd a7, 24(a5)<br>     |
|  14|[0x800024e8]<br>0x0000000000000000|- rs1 : x29<br> - rd : f26<br> - rs1_val == 1449063015970349 and rm_val == 0  #nosat<br>        |[0x80000504]:fmv.d.x fs10, t4<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fs10, 32(a5)<br> [0x80000510]:sd a7, 40(a5)<br>   |
|  15|[0x800024f8]<br>0x0000000000000000|- rs1 : x19<br> - rd : f18<br> - rs1_val == 1064659746632576 and rm_val == 0  #nosat<br>        |[0x8000051c]:fmv.d.x fs2, s3<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsd fs2, 48(a5)<br> [0x80000528]:sd a7, 56(a5)<br>     |
|  16|[0x80002508]<br>0x0000000000000000|- rs1 : x9<br> - rd : f19<br> - rs1_val == 477767642386861 and rm_val == 0  #nosat<br>          |[0x80000534]:fmv.d.x fs3, s1<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsd fs3, 64(a5)<br> [0x80000540]:sd a7, 72(a5)<br>     |
|  17|[0x80002518]<br>0x0000000000000000|- rs1 : x25<br> - rd : f16<br> - rs1_val == 194479587133174 and rm_val == 0  #nosat<br>         |[0x8000054c]:fmv.d.x fa6, s9<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:fsd fa6, 80(a5)<br> [0x80000558]:sd a7, 88(a5)<br>     |
|  18|[0x80002528]<br>0x0000000000000000|- rs1 : x31<br> - rd : f22<br> - rs1_val == 132508745935081 and rm_val == 0  #nosat<br>         |[0x80000564]:fmv.d.x fs6, t6<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd fs6, 96(a5)<br> [0x80000570]:sd a7, 104(a5)<br>    |
|  19|[0x80002538]<br>0x0000000000000000|- rs1 : x14<br> - rd : f14<br> - rs1_val == 45718214482007 and rm_val == 0  #nosat<br>          |[0x8000057c]:fmv.d.x fa4, a4<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:fsd fa4, 112(a5)<br> [0x80000588]:sd a7, 120(a5)<br>   |
|  20|[0x80002548]<br>0x0000000000000000|- rs1 : x20<br> - rd : f4<br> - rs1_val == 31117680965175 and rm_val == 0  #nosat<br>           |[0x80000594]:fmv.d.x ft4, s4<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsd ft4, 128(a5)<br> [0x800005a0]:sd a7, 136(a5)<br>   |
|  21|[0x80002558]<br>0x0000000000000000|- rs1 : x0<br> - rd : f3<br>                                                                    |[0x800005ac]:fmv.d.x ft3, zero<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:fsd ft3, 144(a5)<br> [0x800005b8]:sd a7, 152(a5)<br> |
|  22|[0x80002568]<br>0x0000000000000000|- rs1 : x11<br> - rd : f13<br> - rs1_val == 5032232323694 and rm_val == 0  #nosat<br>           |[0x800005c4]:fmv.d.x fa3, a1<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd fa3, 160(a5)<br> [0x800005d0]:sd a7, 168(a5)<br>   |
|  23|[0x80002578]<br>0x0000000000000000|- rs1 : x18<br> - rd : f30<br> - rs1_val == 3524006078498 and rm_val == 0  #nosat<br>           |[0x800005dc]:fmv.d.x ft10, s2<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:fsd ft10, 176(a5)<br> [0x800005e8]:sd a7, 184(a5)<br> |
|  24|[0x80002588]<br>0x0000000000000000|- rs1 : x15<br> - rd : f15<br> - rs1_val == 1168389695644 and rm_val == 0  #nosat<br>           |[0x80000600]:fmv.d.x fa5, a5<br> [0x80000604]:csrrs s5, fflags, zero<br> [0x80000608]:fsd fa5, 0(s3)<br> [0x8000060c]:sd s5, 8(s3)<br>       |
|  25|[0x80002598]<br>0x0000000000000000|- rs1 : x30<br> - rd : f23<br> - rs1_val == 813522083007 and rm_val == 0  #nosat<br>            |[0x80000624]:fmv.d.x fs7, t5<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd fs7, 0(a5)<br> [0x80000630]:sd a7, 8(a5)<br>       |
|  26|[0x800025a8]<br>0x0000000000000000|- rs1 : x2<br> - rd : f28<br> - rs1_val == 453482173015 and rm_val == 0  #nosat<br>             |[0x8000063c]:fmv.d.x ft8, sp<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsd ft8, 16(a5)<br> [0x80000648]:sd a7, 24(a5)<br>     |
|  27|[0x800025b8]<br>0x0000000000000000|- rs1 : x13<br> - rd : f27<br> - rs1_val == 268160711063 and rm_val == 0  #nosat<br>            |[0x80000654]:fmv.d.x fs11, a3<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:fsd fs11, 32(a5)<br> [0x80000660]:sd a7, 40(a5)<br>   |
|  28|[0x800025c8]<br>0x0000000000000000|- rs1 : x3<br> - rd : f17<br> - rs1_val == 131206879410 and rm_val == 0  #nosat<br>             |[0x8000066c]:fmv.d.x fa7, gp<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:fsd fa7, 48(a5)<br> [0x80000678]:sd a7, 56(a5)<br>     |
|  29|[0x800025d8]<br>0x0000000000000000|- rs1 : x17<br> - rd : f9<br> - rs1_val == 51102363774 and rm_val == 0  #nosat<br>              |[0x80000690]:fmv.d.x fs1, a7<br> [0x80000694]:csrrs s5, fflags, zero<br> [0x80000698]:fsd fs1, 0(s3)<br> [0x8000069c]:sd s5, 8(s3)<br>       |
|  30|[0x800025e8]<br>0x0000000000000000|- rs1 : x22<br> - rd : f24<br> - rs1_val == 22050244097 and rm_val == 0  #nosat<br>             |[0x800006b4]:fmv.d.x fs8, s6<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd fs8, 0(a5)<br> [0x800006c0]:sd a7, 8(a5)<br>       |
|  31|[0x800025f8]<br>0x0000000000000000|- rs1 : x27<br> - rd : f29<br> - rs1_val == 8607351303 and rm_val == 0  #nosat<br>              |[0x800006cc]:fmv.d.x ft9, s11<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:fsd ft9, 16(a5)<br> [0x800006d8]:sd a7, 24(a5)<br>    |
|  32|[0x80002608]<br>0x0000000000000000|- rs1 : x5<br> - rd : f12<br> - rs1_val == 6929185936 and rm_val == 0  #nosat<br>               |[0x800006e4]:fmv.d.x fa2, t0<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd fa2, 32(a5)<br> [0x800006f0]:sd a7, 40(a5)<br>     |
|  33|[0x80002618]<br>0x0000000000000000|- rs1_val == 4035756470 and rm_val == 0  #nosat<br>                                             |[0x800006fc]:fmv.d.x ft11, t6<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:fsd ft11, 48(a5)<br> [0x80000708]:sd a7, 56(a5)<br>   |
|  34|[0x80002628]<br>0x0000000000000000|- rs1_val == 1587807073 and rm_val == 0  #nosat<br>                                             |[0x80000714]:fmv.d.x ft11, t6<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:fsd ft11, 64(a5)<br> [0x80000720]:sd a7, 72(a5)<br>   |
|  35|[0x80002638]<br>0x0000000000000000|- rs1_val == 1027494066 and rm_val == 0  #nosat<br>                                             |[0x8000072c]:fmv.d.x ft11, t6<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:fsd ft11, 80(a5)<br> [0x80000738]:sd a7, 88(a5)<br>   |
|  36|[0x80002648]<br>0x0000000000000000|- rs1_val == 339827553 and rm_val == 0  #nosat<br>                                              |[0x80000744]:fmv.d.x ft11, t6<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 96(a5)<br> [0x80000750]:sd a7, 104(a5)<br>  |
|  37|[0x80002658]<br>0x0000000000000000|- rs1_val == 231549045 and rm_val == 0  #nosat<br>                                              |[0x8000075c]:fmv.d.x ft11, t6<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 112(a5)<br> [0x80000768]:sd a7, 120(a5)<br> |
|  38|[0x80002668]<br>0x0000000000000000|- rs1_val == 107790943 and rm_val == 0  #nosat<br>                                              |[0x80000774]:fmv.d.x ft11, t6<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:fsd ft11, 128(a5)<br> [0x80000780]:sd a7, 136(a5)<br> |
|  39|[0x80002678]<br>0x0000000000000000|- rs1_val == 45276376 and rm_val == 0  #nosat<br>                                               |[0x8000078c]:fmv.d.x ft11, t6<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:fsd ft11, 144(a5)<br> [0x80000798]:sd a7, 152(a5)<br> |
|  40|[0x80002688]<br>0x0000000000000000|- rs1_val == 32105925 and rm_val == 0  #nosat<br>                                               |[0x800007a4]:fmv.d.x ft11, t6<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 160(a5)<br> [0x800007b0]:sd a7, 168(a5)<br> |
|  41|[0x80002698]<br>0x0000000000000000|- rs1_val == 12789625 and rm_val == 0  #nosat<br>                                               |[0x800007bc]:fmv.d.x ft11, t6<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:fsd ft11, 176(a5)<br> [0x800007c8]:sd a7, 184(a5)<br> |
|  42|[0x800026a8]<br>0x0000000000000000|- rs1_val == 6573466 and rm_val == 0  #nosat<br>                                                |[0x800007d4]:fmv.d.x ft11, t6<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:fsd ft11, 192(a5)<br> [0x800007e0]:sd a7, 200(a5)<br> |
|  43|[0x800026b8]<br>0x0000000000000000|- rs1_val == 3864061 and rm_val == 0  #nosat<br>                                                |[0x800007ec]:fmv.d.x ft11, t6<br> [0x800007f0]:csrrs a7, fflags, zero<br> [0x800007f4]:fsd ft11, 208(a5)<br> [0x800007f8]:sd a7, 216(a5)<br> |
|  44|[0x800026c8]<br>0x0000000000000000|- rs1_val == 1848861 and rm_val == 0  #nosat<br>                                                |[0x80000804]:fmv.d.x ft11, t6<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:fsd ft11, 224(a5)<br> [0x80000810]:sd a7, 232(a5)<br> |
|  45|[0x800026d8]<br>0x0000000000000000|- rs1_val == 896618 and rm_val == 0  #nosat<br>                                                 |[0x8000081c]:fmv.d.x ft11, t6<br> [0x80000820]:csrrs a7, fflags, zero<br> [0x80000824]:fsd ft11, 240(a5)<br> [0x80000828]:sd a7, 248(a5)<br> |
|  46|[0x800026e8]<br>0x0000000000000000|- rs1_val == 334857 and rm_val == 0  #nosat<br>                                                 |[0x80000834]:fmv.d.x ft11, t6<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:fsd ft11, 256(a5)<br> [0x80000840]:sd a7, 264(a5)<br> |
|  47|[0x800026f8]<br>0x0000000000000000|- rs1_val == 241276 and rm_val == 0  #nosat<br>                                                 |[0x8000084c]:fmv.d.x ft11, t6<br> [0x80000850]:csrrs a7, fflags, zero<br> [0x80000854]:fsd ft11, 272(a5)<br> [0x80000858]:sd a7, 280(a5)<br> |
|  48|[0x80002708]<br>0x0000000000000000|- rs1_val == 71376 and rm_val == 0  #nosat<br>                                                  |[0x80000864]:fmv.d.x ft11, t6<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:fsd ft11, 288(a5)<br> [0x80000870]:sd a7, 296(a5)<br> |
|  49|[0x80002718]<br>0x0000000000000000|- rs1_val == 56436 and rm_val == 0  #nosat<br>                                                  |[0x8000087c]:fmv.d.x ft11, t6<br> [0x80000880]:csrrs a7, fflags, zero<br> [0x80000884]:fsd ft11, 304(a5)<br> [0x80000888]:sd a7, 312(a5)<br> |
|  50|[0x80002728]<br>0x0000000000000000|- rs1_val == 24575 and rm_val == 0  #nosat<br>                                                  |[0x80000894]:fmv.d.x ft11, t6<br> [0x80000898]:csrrs a7, fflags, zero<br> [0x8000089c]:fsd ft11, 320(a5)<br> [0x800008a0]:sd a7, 328(a5)<br> |
|  51|[0x80002738]<br>0x0000000000000000|- rs1_val == 9438 and rm_val == 0  #nosat<br>                                                   |[0x800008ac]:fmv.d.x ft11, t6<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:fsd ft11, 336(a5)<br> [0x800008b8]:sd a7, 344(a5)<br> |
|  52|[0x80002748]<br>0x0000000000000000|- rs1_val == 6781 and rm_val == 0  #nosat<br>                                                   |[0x800008c4]:fmv.d.x ft11, t6<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:fsd ft11, 352(a5)<br> [0x800008d0]:sd a7, 360(a5)<br> |
|  53|[0x80002758]<br>0x0000000000000000|- rs1_val == 4055 and rm_val == 0  #nosat<br>                                                   |[0x800008dc]:fmv.d.x ft11, t6<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:fsd ft11, 368(a5)<br> [0x800008e8]:sd a7, 376(a5)<br> |
|  54|[0x80002768]<br>0x0000000000000000|- rs1_val == 1094 and rm_val == 0  #nosat<br>                                                   |[0x800008f4]:fmv.d.x ft11, t6<br> [0x800008f8]:csrrs a7, fflags, zero<br> [0x800008fc]:fsd ft11, 384(a5)<br> [0x80000900]:sd a7, 392(a5)<br> |
|  55|[0x80002778]<br>0x0000000000000000|- rs1_val == 676 and rm_val == 0  #nosat<br>                                                    |[0x8000090c]:fmv.d.x ft11, t6<br> [0x80000910]:csrrs a7, fflags, zero<br> [0x80000914]:fsd ft11, 400(a5)<br> [0x80000918]:sd a7, 408(a5)<br> |
|  56|[0x80002788]<br>0x0000000000000000|- rs1_val == 398 and rm_val == 0  #nosat<br>                                                    |[0x80000924]:fmv.d.x ft11, t6<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:fsd ft11, 416(a5)<br> [0x80000930]:sd a7, 424(a5)<br> |
|  57|[0x80002798]<br>0x0000000000000000|- rs1_val == 253 and rm_val == 0  #nosat<br>                                                    |[0x8000093c]:fmv.d.x ft11, t6<br> [0x80000940]:csrrs a7, fflags, zero<br> [0x80000944]:fsd ft11, 432(a5)<br> [0x80000948]:sd a7, 440(a5)<br> |
|  58|[0x800027a8]<br>0x0000000000000000|- rs1_val == 123 and rm_val == 0  #nosat<br>                                                    |[0x80000954]:fmv.d.x ft11, t6<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:fsd ft11, 448(a5)<br> [0x80000960]:sd a7, 456(a5)<br> |
|  59|[0x800027b8]<br>0x0000000000000000|- rs1_val == 45 and rm_val == 0  #nosat<br>                                                     |[0x8000096c]:fmv.d.x ft11, t6<br> [0x80000970]:csrrs a7, fflags, zero<br> [0x80000974]:fsd ft11, 464(a5)<br> [0x80000978]:sd a7, 472(a5)<br> |
|  60|[0x800027c8]<br>0x0000000000000000|- rs1_val == 16 and rm_val == 0  #nosat<br>                                                     |[0x80000984]:fmv.d.x ft11, t6<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:fsd ft11, 480(a5)<br> [0x80000990]:sd a7, 488(a5)<br> |
|  61|[0x800027d8]<br>0x0000000000000000|- rs1_val == 15 and rm_val == 0  #nosat<br>                                                     |[0x8000099c]:fmv.d.x ft11, t6<br> [0x800009a0]:csrrs a7, fflags, zero<br> [0x800009a4]:fsd ft11, 496(a5)<br> [0x800009a8]:sd a7, 504(a5)<br> |
|  62|[0x800027e8]<br>0x0000000000000000|- rs1_val == 7 and rm_val == 0  #nosat<br>                                                      |[0x800009b4]:fmv.d.x ft11, t6<br> [0x800009b8]:csrrs a7, fflags, zero<br> [0x800009bc]:fsd ft11, 512(a5)<br> [0x800009c0]:sd a7, 520(a5)<br> |
|  63|[0x800027f8]<br>0x0000000000000000|- rs1_val == 2 and rm_val == 0  #nosat<br>                                                      |[0x800009cc]:fmv.d.x ft11, t6<br> [0x800009d0]:csrrs a7, fflags, zero<br> [0x800009d4]:fsd ft11, 528(a5)<br> [0x800009d8]:sd a7, 536(a5)<br> |
|  64|[0x80002808]<br>0x0000000000000000|- rs1_val == 1 and rm_val == 0  #nosat<br>                                                      |[0x800009e4]:fmv.d.x ft11, t6<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:fsd ft11, 544(a5)<br> [0x800009f0]:sd a7, 552(a5)<br> |
|  65|[0x80002818]<br>0x0000000000000000|- rs1_val == 10221399934292 and rm_val == 0  #nosat<br>                                         |[0x800009fc]:fmv.d.x ft11, t6<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:fsd ft11, 560(a5)<br> [0x80000a08]:sd a7, 568(a5)<br> |
