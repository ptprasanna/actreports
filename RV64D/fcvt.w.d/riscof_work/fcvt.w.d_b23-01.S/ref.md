
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000840')]      |
| SIG_REGION                | [('0x80002310', '0x800025f0', '92 dwords')]      |
| COV_LABELS                | fcvt.w.d_b23      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.w.d/riscof_work/fcvt.w.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 114     |
| Total Coverpoints Hit     | 110      |
| Total Signature Updates   | 92      |
| STAT1                     | 45      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 46     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000828]:fcvt.w.d t6, ft11, dyn
      [0x8000082c]:csrrs a7, fflags, zero
      [0x80000830]:sd t6, 256(a5)
 -- Signature Address: 0x800025e0 Data: 0x000000007FFFFFFF
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.w.d
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x15', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.w.d a5, fa3, dyn
	-[0x800003b8]:csrrs s5, fflags, zero
	-[0x800003bc]:sd a5, 0(s3)
Current Store : [0x800003c0] : sd s5, 8(s3) -- Store: [0x80002318]:0x0000000000000010




Last Coverpoint : ['rd : x28', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800003d8]:fcvt.w.d t3, fs5, dyn
	-[0x800003dc]:csrrs a7, fflags, zero
	-[0x800003e0]:sd t3, 0(a5)
Current Store : [0x800003e4] : sd a7, 8(a5) -- Store: [0x80002328]:0x0000000000000010




Last Coverpoint : ['rd : x24', 'rs1 : f19', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.w.d s8, fs3, dyn
	-[0x800003f4]:csrrs a7, fflags, zero
	-[0x800003f8]:sd s8, 16(a5)
Current Store : [0x800003fc] : sd a7, 24(a5) -- Store: [0x80002338]:0x0000000000000010




Last Coverpoint : ['rd : x6', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000408]:fcvt.w.d t1, ft11, dyn
	-[0x8000040c]:csrrs a7, fflags, zero
	-[0x80000410]:sd t1, 32(a5)
Current Store : [0x80000414] : sd a7, 40(a5) -- Store: [0x80002348]:0x0000000000000010




Last Coverpoint : ['rd : x27', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000420]:fcvt.w.d s11, fa6, dyn
	-[0x80000424]:csrrs a7, fflags, zero
	-[0x80000428]:sd s11, 48(a5)
Current Store : [0x8000042c] : sd a7, 56(a5) -- Store: [0x80002358]:0x0000000000000010




Last Coverpoint : ['rd : x3', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000438]:fcvt.w.d gp, fa7, dyn
	-[0x8000043c]:csrrs a7, fflags, zero
	-[0x80000440]:sd gp, 64(a5)
Current Store : [0x80000444] : sd a7, 72(a5) -- Store: [0x80002368]:0x0000000000000010




Last Coverpoint : ['rd : x25', 'rs1 : f8', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000450]:fcvt.w.d s9, fs0, dyn
	-[0x80000454]:csrrs a7, fflags, zero
	-[0x80000458]:sd s9, 80(a5)
Current Store : [0x8000045c] : sd a7, 88(a5) -- Store: [0x80002378]:0x0000000000000010




Last Coverpoint : ['rd : x4', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000468]:fcvt.w.d tp, ft6, dyn
	-[0x8000046c]:csrrs a7, fflags, zero
	-[0x80000470]:sd tp, 96(a5)
Current Store : [0x80000474] : sd a7, 104(a5) -- Store: [0x80002388]:0x0000000000000010




Last Coverpoint : ['rd : x9', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000480]:fcvt.w.d s1, fa2, dyn
	-[0x80000484]:csrrs a7, fflags, zero
	-[0x80000488]:sd s1, 112(a5)
Current Store : [0x8000048c] : sd a7, 120(a5) -- Store: [0x80002398]:0x0000000000000010




Last Coverpoint : ['rd : x0', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000498]:fcvt.w.d zero, ft5, dyn
	-[0x8000049c]:csrrs a7, fflags, zero
	-[0x800004a0]:sd zero, 128(a5)
Current Store : [0x800004a4] : sd a7, 136(a5) -- Store: [0x800023a8]:0x0000000000000010




Last Coverpoint : ['rd : x20', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004b0]:fcvt.w.d s4, ft8, dyn
	-[0x800004b4]:csrrs a7, fflags, zero
	-[0x800004b8]:sd s4, 144(a5)
Current Store : [0x800004bc] : sd a7, 152(a5) -- Store: [0x800023b8]:0x0000000000000010




Last Coverpoint : ['rd : x18', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800004c8]:fcvt.w.d s2, fs1, dyn
	-[0x800004cc]:csrrs a7, fflags, zero
	-[0x800004d0]:sd s2, 160(a5)
Current Store : [0x800004d4] : sd a7, 168(a5) -- Store: [0x800023c8]:0x0000000000000010




Last Coverpoint : ['rd : x17', 'rs1 : f26', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.w.d a7, fs10, dyn
	-[0x800004f0]:csrrs s5, fflags, zero
	-[0x800004f4]:sd a7, 0(s3)
Current Store : [0x800004f8] : sd s5, 8(s3) -- Store: [0x800023d8]:0x0000000000000010




Last Coverpoint : ['rd : x30', 'rs1 : f30', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000510]:fcvt.w.d t5, ft10, dyn
	-[0x80000514]:csrrs a7, fflags, zero
	-[0x80000518]:sd t5, 0(a5)
Current Store : [0x8000051c] : sd a7, 8(a5) -- Store: [0x800023e8]:0x0000000000000010




Last Coverpoint : ['rd : x1', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000528]:fcvt.w.d ra, ft2, dyn
	-[0x8000052c]:csrrs a7, fflags, zero
	-[0x80000530]:sd ra, 16(a5)
Current Store : [0x80000534] : sd a7, 24(a5) -- Store: [0x800023f8]:0x0000000000000010




Last Coverpoint : ['rd : x29', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000540]:fcvt.w.d t4, ft1, dyn
	-[0x80000544]:csrrs a7, fflags, zero
	-[0x80000548]:sd t4, 32(a5)
Current Store : [0x8000054c] : sd a7, 40(a5) -- Store: [0x80002408]:0x0000000000000010




Last Coverpoint : ['rd : x2', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000558]:fcvt.w.d sp, fs6, dyn
	-[0x8000055c]:csrrs a7, fflags, zero
	-[0x80000560]:sd sp, 48(a5)
Current Store : [0x80000564] : sd a7, 56(a5) -- Store: [0x80002418]:0x0000000000000010




Last Coverpoint : ['rd : x13', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000570]:fcvt.w.d a3, ft4, dyn
	-[0x80000574]:csrrs a7, fflags, zero
	-[0x80000578]:sd a3, 64(a5)
Current Store : [0x8000057c] : sd a7, 72(a5) -- Store: [0x80002428]:0x0000000000000010




Last Coverpoint : ['rd : x12', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000588]:fcvt.w.d a2, fs7, dyn
	-[0x8000058c]:csrrs a7, fflags, zero
	-[0x80000590]:sd a2, 80(a5)
Current Store : [0x80000594] : sd a7, 88(a5) -- Store: [0x80002438]:0x0000000000000010




Last Coverpoint : ['rd : x11', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800005a0]:fcvt.w.d a1, fs11, dyn
	-[0x800005a4]:csrrs a7, fflags, zero
	-[0x800005a8]:sd a1, 96(a5)
Current Store : [0x800005ac] : sd a7, 104(a5) -- Store: [0x80002448]:0x0000000000000010




Last Coverpoint : ['rd : x19', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005b8]:fcvt.w.d s3, ft3, dyn
	-[0x800005bc]:csrrs a7, fflags, zero
	-[0x800005c0]:sd s3, 112(a5)
Current Store : [0x800005c4] : sd a7, 120(a5) -- Store: [0x80002458]:0x0000000000000010




Last Coverpoint : ['rd : x21', 'rs1 : f29', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800005d0]:fcvt.w.d s5, ft9, dyn
	-[0x800005d4]:csrrs a7, fflags, zero
	-[0x800005d8]:sd s5, 128(a5)
Current Store : [0x800005dc] : sd a7, 136(a5) -- Store: [0x80002468]:0x0000000000000010




Last Coverpoint : ['rd : x7', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800005e8]:fcvt.w.d t2, ft7, dyn
	-[0x800005ec]:csrrs a7, fflags, zero
	-[0x800005f0]:sd t2, 144(a5)
Current Store : [0x800005f4] : sd a7, 152(a5) -- Store: [0x80002478]:0x0000000000000010




Last Coverpoint : ['rd : x10', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000600]:fcvt.w.d a0, fa5, dyn
	-[0x80000604]:csrrs a7, fflags, zero
	-[0x80000608]:sd a0, 160(a5)
Current Store : [0x8000060c] : sd a7, 168(a5) -- Store: [0x80002488]:0x0000000000000010




Last Coverpoint : ['rd : x5', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000618]:fcvt.w.d t0, fs9, dyn
	-[0x8000061c]:csrrs a7, fflags, zero
	-[0x80000620]:sd t0, 176(a5)
Current Store : [0x80000624] : sd a7, 184(a5) -- Store: [0x80002498]:0x0000000000000010




Last Coverpoint : ['rd : x31', 'rs1 : f10', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000630]:fcvt.w.d t6, fa0, dyn
	-[0x80000634]:csrrs a7, fflags, zero
	-[0x80000638]:sd t6, 192(a5)
Current Store : [0x8000063c] : sd a7, 200(a5) -- Store: [0x800024a8]:0x0000000000000010




Last Coverpoint : ['rd : x14', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000648]:fcvt.w.d a4, fa1, dyn
	-[0x8000064c]:csrrs a7, fflags, zero
	-[0x80000650]:sd a4, 208(a5)
Current Store : [0x80000654] : sd a7, 216(a5) -- Store: [0x800024b8]:0x0000000000000010




Last Coverpoint : ['rd : x26', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000660]:fcvt.w.d s10, fs4, dyn
	-[0x80000664]:csrrs a7, fflags, zero
	-[0x80000668]:sd s10, 224(a5)
Current Store : [0x8000066c] : sd a7, 232(a5) -- Store: [0x800024c8]:0x0000000000000010




Last Coverpoint : ['rd : x16', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000684]:fcvt.w.d a6, fa4, dyn
	-[0x80000688]:csrrs s5, fflags, zero
	-[0x8000068c]:sd a6, 0(s3)
Current Store : [0x80000690] : sd s5, 8(s3) -- Store: [0x800024d8]:0x0000000000000010




Last Coverpoint : ['rd : x23', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x800006a8]:fcvt.w.d s7, fs2, dyn
	-[0x800006ac]:csrrs a7, fflags, zero
	-[0x800006b0]:sd s7, 0(a5)
Current Store : [0x800006b4] : sd a7, 8(a5) -- Store: [0x800024e8]:0x0000000000000010




Last Coverpoint : ['rd : x8', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006c0]:fcvt.w.d fp, fs8, dyn
	-[0x800006c4]:csrrs a7, fflags, zero
	-[0x800006c8]:sd fp, 16(a5)
Current Store : [0x800006cc] : sd a7, 24(a5) -- Store: [0x800024f8]:0x0000000000000010




Last Coverpoint : ['rd : x22', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800006d8]:fcvt.w.d s6, ft0, dyn
	-[0x800006dc]:csrrs a7, fflags, zero
	-[0x800006e0]:sd s6, 32(a5)
Current Store : [0x800006e4] : sd a7, 40(a5) -- Store: [0x80002508]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800006f0]:fcvt.w.d t6, ft11, dyn
	-[0x800006f4]:csrrs a7, fflags, zero
	-[0x800006f8]:sd t6, 48(a5)
Current Store : [0x800006fc] : sd a7, 56(a5) -- Store: [0x80002518]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000708]:fcvt.w.d t6, ft11, dyn
	-[0x8000070c]:csrrs a7, fflags, zero
	-[0x80000710]:sd t6, 64(a5)
Current Store : [0x80000714] : sd a7, 72(a5) -- Store: [0x80002528]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000720]:fcvt.w.d t6, ft11, dyn
	-[0x80000724]:csrrs a7, fflags, zero
	-[0x80000728]:sd t6, 80(a5)
Current Store : [0x8000072c] : sd a7, 88(a5) -- Store: [0x80002538]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000738]:fcvt.w.d t6, ft11, dyn
	-[0x8000073c]:csrrs a7, fflags, zero
	-[0x80000740]:sd t6, 96(a5)
Current Store : [0x80000744] : sd a7, 104(a5) -- Store: [0x80002548]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x80000750]:fcvt.w.d t6, ft11, dyn
	-[0x80000754]:csrrs a7, fflags, zero
	-[0x80000758]:sd t6, 112(a5)
Current Store : [0x8000075c] : sd a7, 120(a5) -- Store: [0x80002558]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x80000768]:fcvt.w.d t6, ft11, dyn
	-[0x8000076c]:csrrs a7, fflags, zero
	-[0x80000770]:sd t6, 128(a5)
Current Store : [0x80000774] : sd a7, 136(a5) -- Store: [0x80002568]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x80000780]:fcvt.w.d t6, ft11, dyn
	-[0x80000784]:csrrs a7, fflags, zero
	-[0x80000788]:sd t6, 144(a5)
Current Store : [0x8000078c] : sd a7, 152(a5) -- Store: [0x80002578]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000798]:fcvt.w.d t6, ft11, dyn
	-[0x8000079c]:csrrs a7, fflags, zero
	-[0x800007a0]:sd t6, 160(a5)
Current Store : [0x800007a4] : sd a7, 168(a5) -- Store: [0x80002588]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007b0]:fcvt.w.d t6, ft11, dyn
	-[0x800007b4]:csrrs a7, fflags, zero
	-[0x800007b8]:sd t6, 176(a5)
Current Store : [0x800007bc] : sd a7, 184(a5) -- Store: [0x80002598]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 4  #nosat']
Last Code Sequence : 
	-[0x800007c8]:fcvt.w.d t6, ft11, dyn
	-[0x800007cc]:csrrs a7, fflags, zero
	-[0x800007d0]:sd t6, 192(a5)
Current Store : [0x800007d4] : sd a7, 200(a5) -- Store: [0x800025a8]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 3  #nosat']
Last Code Sequence : 
	-[0x800007e0]:fcvt.w.d t6, ft11, dyn
	-[0x800007e4]:csrrs a7, fflags, zero
	-[0x800007e8]:sd t6, 208(a5)
Current Store : [0x800007ec] : sd a7, 216(a5) -- Store: [0x800025b8]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 2  #nosat']
Last Code Sequence : 
	-[0x800007f8]:fcvt.w.d t6, ft11, dyn
	-[0x800007fc]:csrrs a7, fflags, zero
	-[0x80000800]:sd t6, 224(a5)
Current Store : [0x80000804] : sd a7, 232(a5) -- Store: [0x800025c8]:0x0000000000000010




Last Coverpoint : ['fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000810]:fcvt.w.d t6, ft11, dyn
	-[0x80000814]:csrrs a7, fflags, zero
	-[0x80000818]:sd t6, 240(a5)
Current Store : [0x8000081c] : sd a7, 248(a5) -- Store: [0x800025d8]:0x0000000000000010




Last Coverpoint : ['opcode : fcvt.w.d', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat']
Last Code Sequence : 
	-[0x80000828]:fcvt.w.d t6, ft11, dyn
	-[0x8000082c]:csrrs a7, fflags, zero
	-[0x80000830]:sd t6, 256(a5)
Current Store : [0x80000834] : sd a7, 264(a5) -- Store: [0x800025e8]:0x0000000000000010





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                               coverpoints                                                                |                                                       code                                                        |
|---:|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x000000007FFFFFFF|- opcode : fcvt.w.d<br> - rd : x15<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.w.d a5, fa3, dyn<br> [0x800003b8]:csrrs s5, fflags, zero<br> [0x800003bc]:sd a5, 0(s3)<br>       |
|   2|[0x80002320]<br>0x000000007FFFFFFF|- rd : x28<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat<br>                         |[0x800003d8]:fcvt.w.d t3, fs5, dyn<br> [0x800003dc]:csrrs a7, fflags, zero<br> [0x800003e0]:sd t3, 0(a5)<br>       |
|   3|[0x80002330]<br>0x000000007FFFFFFF|- rd : x24<br> - rs1 : f19<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat<br>                         |[0x800003f0]:fcvt.w.d s8, fs3, dyn<br> [0x800003f4]:csrrs a7, fflags, zero<br> [0x800003f8]:sd s8, 16(a5)<br>      |
|   4|[0x80002340]<br>0x000000007FFFFFFF|- rd : x6<br> - rs1 : f31<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat<br>                          |[0x80000408]:fcvt.w.d t1, ft11, dyn<br> [0x8000040c]:csrrs a7, fflags, zero<br> [0x80000410]:sd t1, 32(a5)<br>     |
|   5|[0x80002350]<br>0x000000007FFFFFFF|- rd : x27<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat<br>                         |[0x80000420]:fcvt.w.d s11, fa6, dyn<br> [0x80000424]:csrrs a7, fflags, zero<br> [0x80000428]:sd s11, 48(a5)<br>    |
|   6|[0x80002360]<br>0x000000007FFFFFFF|- rd : x3<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat<br>                          |[0x80000438]:fcvt.w.d gp, fa7, dyn<br> [0x8000043c]:csrrs a7, fflags, zero<br> [0x80000440]:sd gp, 64(a5)<br>      |
|   7|[0x80002370]<br>0x000000007FFFFFFF|- rd : x25<br> - rs1 : f8<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat<br>                          |[0x80000450]:fcvt.w.d s9, fs0, dyn<br> [0x80000454]:csrrs a7, fflags, zero<br> [0x80000458]:sd s9, 80(a5)<br>      |
|   8|[0x80002380]<br>0x000000007FFFFFFF|- rd : x4<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat<br>                           |[0x80000468]:fcvt.w.d tp, ft6, dyn<br> [0x8000046c]:csrrs a7, fflags, zero<br> [0x80000470]:sd tp, 96(a5)<br>      |
|   9|[0x80002390]<br>0x000000007FFFFFFF|- rd : x9<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat<br>                          |[0x80000480]:fcvt.w.d s1, fa2, dyn<br> [0x80000484]:csrrs a7, fflags, zero<br> [0x80000488]:sd s1, 112(a5)<br>     |
|  10|[0x800023a0]<br>0x0000000000000000|- rd : x0<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat<br>                           |[0x80000498]:fcvt.w.d zero, ft5, dyn<br> [0x8000049c]:csrrs a7, fflags, zero<br> [0x800004a0]:sd zero, 128(a5)<br> |
|  11|[0x800023b0]<br>0x000000007FFFFFFF|- rd : x20<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat<br>                         |[0x800004b0]:fcvt.w.d s4, ft8, dyn<br> [0x800004b4]:csrrs a7, fflags, zero<br> [0x800004b8]:sd s4, 144(a5)<br>     |
|  12|[0x800023c0]<br>0x000000007FFFFFFF|- rd : x18<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat<br>                          |[0x800004c8]:fcvt.w.d s2, fs1, dyn<br> [0x800004cc]:csrrs a7, fflags, zero<br> [0x800004d0]:sd s2, 160(a5)<br>     |
|  13|[0x800023d0]<br>0x000000007FFFFFFF|- rd : x17<br> - rs1 : f26<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat<br>                         |[0x800004ec]:fcvt.w.d a7, fs10, dyn<br> [0x800004f0]:csrrs s5, fflags, zero<br> [0x800004f4]:sd a7, 0(s3)<br>      |
|  14|[0x800023e0]<br>0x000000007FFFFFFF|- rd : x30<br> - rs1 : f30<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat<br>                         |[0x80000510]:fcvt.w.d t5, ft10, dyn<br> [0x80000514]:csrrs a7, fflags, zero<br> [0x80000518]:sd t5, 0(a5)<br>      |
|  15|[0x800023f0]<br>0x000000007FFFFFFF|- rd : x1<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                           |[0x80000528]:fcvt.w.d ra, ft2, dyn<br> [0x8000052c]:csrrs a7, fflags, zero<br> [0x80000530]:sd ra, 16(a5)<br>      |
|  16|[0x80002400]<br>0x000000007FFFFFFF|- rd : x29<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                          |[0x80000540]:fcvt.w.d t4, ft1, dyn<br> [0x80000544]:csrrs a7, fflags, zero<br> [0x80000548]:sd t4, 32(a5)<br>      |
|  17|[0x80002410]<br>0x000000007FFFFFFF|- rd : x2<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat<br>                          |[0x80000558]:fcvt.w.d sp, fs6, dyn<br> [0x8000055c]:csrrs a7, fflags, zero<br> [0x80000560]:sd sp, 48(a5)<br>      |
|  18|[0x80002420]<br>0x000000007FFFFFFF|- rd : x13<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat<br>                          |[0x80000570]:fcvt.w.d a3, ft4, dyn<br> [0x80000574]:csrrs a7, fflags, zero<br> [0x80000578]:sd a3, 64(a5)<br>      |
|  19|[0x80002430]<br>0x000000007FFFFFFF|- rd : x12<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat<br>                         |[0x80000588]:fcvt.w.d a2, fs7, dyn<br> [0x8000058c]:csrrs a7, fflags, zero<br> [0x80000590]:sd a2, 80(a5)<br>      |
|  20|[0x80002440]<br>0x000000007FFFFFFF|- rd : x11<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                         |[0x800005a0]:fcvt.w.d a1, fs11, dyn<br> [0x800005a4]:csrrs a7, fflags, zero<br> [0x800005a8]:sd a1, 96(a5)<br>     |
|  21|[0x80002450]<br>0x000000007FFFFFFF|- rd : x19<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x800005b8]:fcvt.w.d s3, ft3, dyn<br> [0x800005bc]:csrrs a7, fflags, zero<br> [0x800005c0]:sd s3, 112(a5)<br>     |
|  22|[0x80002460]<br>0x000000007FFFFFFF|- rd : x21<br> - rs1 : f29<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                         |[0x800005d0]:fcvt.w.d s5, ft9, dyn<br> [0x800005d4]:csrrs a7, fflags, zero<br> [0x800005d8]:sd s5, 128(a5)<br>     |
|  23|[0x80002470]<br>0x000000007FFFFFFF|- rd : x7<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                           |[0x800005e8]:fcvt.w.d t2, ft7, dyn<br> [0x800005ec]:csrrs a7, fflags, zero<br> [0x800005f0]:sd t2, 144(a5)<br>     |
|  24|[0x80002480]<br>0x000000007FFFFFFF|- rd : x10<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 2  #nosat<br>                         |[0x80000600]:fcvt.w.d a0, fa5, dyn<br> [0x80000604]:csrrs a7, fflags, zero<br> [0x80000608]:sd a0, 160(a5)<br>     |
|  25|[0x80002490]<br>0x000000007FFFFFFF|- rd : x5<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                          |[0x80000618]:fcvt.w.d t0, fs9, dyn<br> [0x8000061c]:csrrs a7, fflags, zero<br> [0x80000620]:sd t0, 176(a5)<br>     |
|  26|[0x800024a0]<br>0x000000007FFFFFFF|- rd : x31<br> - rs1 : f10<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x80000630]:fcvt.w.d t6, fa0, dyn<br> [0x80000634]:csrrs a7, fflags, zero<br> [0x80000638]:sd t6, 192(a5)<br>     |
|  27|[0x800024b0]<br>0x000000007FFFFFFF|- rd : x14<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 4  #nosat<br>                         |[0x80000648]:fcvt.w.d a4, fa1, dyn<br> [0x8000064c]:csrrs a7, fflags, zero<br> [0x80000650]:sd a4, 208(a5)<br>     |
|  28|[0x800024c0]<br>0x000000007FFFFFFF|- rd : x26<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 3  #nosat<br>                         |[0x80000660]:fcvt.w.d s10, fs4, dyn<br> [0x80000664]:csrrs a7, fflags, zero<br> [0x80000668]:sd s10, 224(a5)<br>   |
|  29|[0x800024d0]<br>0x000000007FFFFFFF|- rd : x16<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 2  #nosat<br>                         |[0x80000684]:fcvt.w.d a6, fa4, dyn<br> [0x80000688]:csrrs s5, fflags, zero<br> [0x8000068c]:sd a6, 0(s3)<br>       |
|  30|[0x800024e0]<br>0x000000007FFFFFFF|- rd : x23<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                         |[0x800006a8]:fcvt.w.d s7, fs2, dyn<br> [0x800006ac]:csrrs a7, fflags, zero<br> [0x800006b0]:sd s7, 0(a5)<br>       |
|  31|[0x800024f0]<br>0x000000007FFFFFFF|- rd : x8<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                          |[0x800006c0]:fcvt.w.d fp, fs8, dyn<br> [0x800006c4]:csrrs a7, fflags, zero<br> [0x800006c8]:sd fp, 16(a5)<br>      |
|  32|[0x80002500]<br>0x000000007FFFFFFF|- rd : x22<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 4  #nosat<br>                          |[0x800006d8]:fcvt.w.d s6, ft0, dyn<br> [0x800006dc]:csrrs a7, fflags, zero<br> [0x800006e0]:sd s6, 32(a5)<br>      |
|  33|[0x80002510]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 3  #nosat<br>                                                        |[0x800006f0]:fcvt.w.d t6, ft11, dyn<br> [0x800006f4]:csrrs a7, fflags, zero<br> [0x800006f8]:sd t6, 48(a5)<br>     |
|  34|[0x80002520]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 2  #nosat<br>                                                        |[0x80000708]:fcvt.w.d t6, ft11, dyn<br> [0x8000070c]:csrrs a7, fflags, zero<br> [0x80000710]:sd t6, 64(a5)<br>     |
|  35|[0x80002530]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 1  #nosat<br>                                                        |[0x80000720]:fcvt.w.d t6, ft11, dyn<br> [0x80000724]:csrrs a7, fflags, zero<br> [0x80000728]:sd t6, 80(a5)<br>     |
|  36|[0x80002540]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 0  #nosat<br>                                                        |[0x80000738]:fcvt.w.d t6, ft11, dyn<br> [0x8000073c]:csrrs a7, fflags, zero<br> [0x80000740]:sd t6, 96(a5)<br>     |
|  37|[0x80002550]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 4  #nosat<br>                                                        |[0x80000750]:fcvt.w.d t6, ft11, dyn<br> [0x80000754]:csrrs a7, fflags, zero<br> [0x80000758]:sd t6, 112(a5)<br>    |
|  38|[0x80002560]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 3  #nosat<br>                                                        |[0x80000768]:fcvt.w.d t6, ft11, dyn<br> [0x8000076c]:csrrs a7, fflags, zero<br> [0x80000770]:sd t6, 128(a5)<br>    |
|  39|[0x80002570]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 2  #nosat<br>                                                        |[0x80000780]:fcvt.w.d t6, ft11, dyn<br> [0x80000784]:csrrs a7, fflags, zero<br> [0x80000788]:sd t6, 144(a5)<br>    |
|  40|[0x80002580]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 1  #nosat<br>                                                        |[0x80000798]:fcvt.w.d t6, ft11, dyn<br> [0x8000079c]:csrrs a7, fflags, zero<br> [0x800007a0]:sd t6, 160(a5)<br>    |
|  41|[0x80002590]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 0  #nosat<br>                                                        |[0x800007b0]:fcvt.w.d t6, ft11, dyn<br> [0x800007b4]:csrrs a7, fflags, zero<br> [0x800007b8]:sd t6, 176(a5)<br>    |
|  42|[0x800025a0]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 4  #nosat<br>                                                        |[0x800007c8]:fcvt.w.d t6, ft11, dyn<br> [0x800007cc]:csrrs a7, fflags, zero<br> [0x800007d0]:sd t6, 192(a5)<br>    |
|  43|[0x800025b0]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 3  #nosat<br>                                                        |[0x800007e0]:fcvt.w.d t6, ft11, dyn<br> [0x800007e4]:csrrs a7, fflags, zero<br> [0x800007e8]:sd t6, 208(a5)<br>    |
|  44|[0x800025c0]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 2  #nosat<br>                                                        |[0x800007f8]:fcvt.w.d t6, ft11, dyn<br> [0x800007fc]:csrrs a7, fflags, zero<br> [0x80000800]:sd t6, 224(a5)<br>    |
|  45|[0x800025d0]<br>0x000000007FFFFFFF|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 1  #nosat<br>                                                        |[0x80000810]:fcvt.w.d t6, ft11, dyn<br> [0x80000814]:csrrs a7, fflags, zero<br> [0x80000818]:sd t6, 240(a5)<br>    |
