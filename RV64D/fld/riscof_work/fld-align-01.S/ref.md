
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000870')]      |
| SIG_REGION                | [('0x80002210', '0x80002410', '64 dwords')]      |
| COV_LABELS                | fld-align      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fld/riscof_work/fld-align-01.S/ref.S    |
| Total Number of coverpoints| 75     |
| Total Coverpoints Hit     | 71      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                 coverpoints                                                  |                                                                                                         code                                                                                                          |
|---:|----------------------------------|--------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002218]<br>0x0000000000000000|- opcode : fld<br> - rs1 : x4<br> - rd : f27<br> - ea_align == 0 and (imm_val % 4) == 0<br> - imm_val > 0<br> |[0x800003b4]:fld fs11, 8(tp)<br> [0x800003b8]:addi zero, zero, 0<br> [0x800003bc]:addi zero, zero, 0<br> [0x800003c0]:csrrs a7, fflags, zero<br> [0x800003c4]:fsd fs11, 0(a5)<br> [0x800003c8]:sd a7, 8(a5)<br>        |
|   2|[0x80002228]<br>0x0000000000000000|- rs1 : x27<br> - rd : f5<br> - ea_align == 0 and (imm_val % 4) == 1<br>                                      |[0x800003d8]:fld ft5, 5(s11)<br> [0x800003dc]:addi zero, zero, 0<br> [0x800003e0]:addi zero, zero, 0<br> [0x800003e4]:csrrs a7, fflags, zero<br> [0x800003e8]:fsd ft5, 16(a5)<br> [0x800003ec]:sd a7, 24(a5)<br>       |
|   3|[0x80002238]<br>0x0000000000000000|- rs1 : x3<br> - rd : f20<br> - ea_align == 0 and (imm_val % 4) == 2<br> - imm_val < 0<br>                    |[0x800003fc]:fld fs4, 4086(gp)<br> [0x80000400]:addi zero, zero, 0<br> [0x80000404]:addi zero, zero, 0<br> [0x80000408]:csrrs a7, fflags, zero<br> [0x8000040c]:fsd fs4, 32(a5)<br> [0x80000410]:sd a7, 40(a5)<br>     |
|   4|[0x80002248]<br>0x0000000000000000|- rs1 : x19<br> - rd : f19<br> - ea_align == 0 and (imm_val % 4) == 3<br>                                     |[0x80000420]:fld fs3, 3839(s3)<br> [0x80000424]:addi zero, zero, 0<br> [0x80000428]:addi zero, zero, 0<br> [0x8000042c]:csrrs a7, fflags, zero<br> [0x80000430]:fsd fs3, 48(a5)<br> [0x80000434]:sd a7, 56(a5)<br>     |
|   5|[0x80002258]<br>0x0000000000000000|- rs1 : x22<br> - rd : f16<br> - imm_val == 0<br>                                                             |[0x80000444]:fld fa6, 0(s6)<br> [0x80000448]:addi zero, zero, 0<br> [0x8000044c]:addi zero, zero, 0<br> [0x80000450]:csrrs a7, fflags, zero<br> [0x80000454]:fsd fa6, 64(a5)<br> [0x80000458]:sd a7, 72(a5)<br>        |
|   6|[0x80002268]<br>0x0000000000000000|- rs1 : x11<br> - rd : f8<br>                                                                                 |[0x80000468]:fld fs0, 2048(a1)<br> [0x8000046c]:addi zero, zero, 0<br> [0x80000470]:addi zero, zero, 0<br> [0x80000474]:csrrs a7, fflags, zero<br> [0x80000478]:fsd fs0, 80(a5)<br> [0x8000047c]:sd a7, 88(a5)<br>     |
|   7|[0x80002278]<br>0x0000000000000000|- rs1 : x17<br> - rd : f6<br>                                                                                 |[0x80000498]:fld ft6, 2048(a7)<br> [0x8000049c]:addi zero, zero, 0<br> [0x800004a0]:addi zero, zero, 0<br> [0x800004a4]:csrrs s5, fflags, zero<br> [0x800004a8]:fsd ft6, 0(s3)<br> [0x800004ac]:sd s5, 8(s3)<br>       |
|   8|[0x80002288]<br>0x0000000000000000|- rs1 : x23<br> - rd : f3<br>                                                                                 |[0x800004c8]:fld ft3, 2048(s7)<br> [0x800004cc]:addi zero, zero, 0<br> [0x800004d0]:addi zero, zero, 0<br> [0x800004d4]:csrrs a7, fflags, zero<br> [0x800004d8]:fsd ft3, 0(a5)<br> [0x800004dc]:sd a7, 8(a5)<br>       |
|   9|[0x80002298]<br>0x0000000000000000|- rs1 : x21<br> - rd : f25<br>                                                                                |[0x800004ec]:fld fs9, 2048(s5)<br> [0x800004f0]:addi zero, zero, 0<br> [0x800004f4]:addi zero, zero, 0<br> [0x800004f8]:csrrs a7, fflags, zero<br> [0x800004fc]:fsd fs9, 16(a5)<br> [0x80000500]:sd a7, 24(a5)<br>     |
|  10|[0x800022a8]<br>0x0000000000000000|- rs1 : x31<br> - rd : f24<br>                                                                                |[0x80000510]:fld fs8, 2048(t6)<br> [0x80000514]:addi zero, zero, 0<br> [0x80000518]:addi zero, zero, 0<br> [0x8000051c]:csrrs a7, fflags, zero<br> [0x80000520]:fsd fs8, 32(a5)<br> [0x80000524]:sd a7, 40(a5)<br>     |
|  11|[0x800022b8]<br>0x0000000000000000|- rs1 : x28<br> - rd : f22<br>                                                                                |[0x80000534]:fld fs6, 2048(t3)<br> [0x80000538]:addi zero, zero, 0<br> [0x8000053c]:addi zero, zero, 0<br> [0x80000540]:csrrs a7, fflags, zero<br> [0x80000544]:fsd fs6, 48(a5)<br> [0x80000548]:sd a7, 56(a5)<br>     |
|  12|[0x800022c8]<br>0x0000000000000000|- rs1 : x18<br> - rd : f17<br>                                                                                |[0x80000558]:fld fa7, 2048(s2)<br> [0x8000055c]:addi zero, zero, 0<br> [0x80000560]:addi zero, zero, 0<br> [0x80000564]:csrrs a7, fflags, zero<br> [0x80000568]:fsd fa7, 64(a5)<br> [0x8000056c]:sd a7, 72(a5)<br>     |
|  13|[0x800022d8]<br>0x0000000000000000|- rs1 : x7<br> - rd : f14<br>                                                                                 |[0x8000057c]:fld fa4, 2048(t2)<br> [0x80000580]:addi zero, zero, 0<br> [0x80000584]:addi zero, zero, 0<br> [0x80000588]:csrrs a7, fflags, zero<br> [0x8000058c]:fsd fa4, 80(a5)<br> [0x80000590]:sd a7, 88(a5)<br>     |
|  14|[0x800022e8]<br>0x0000000000000000|- rs1 : x16<br> - rd : f4<br>                                                                                 |[0x800005ac]:fld ft4, 2048(a6)<br> [0x800005b0]:addi zero, zero, 0<br> [0x800005b4]:addi zero, zero, 0<br> [0x800005b8]:csrrs s5, fflags, zero<br> [0x800005bc]:fsd ft4, 0(s3)<br> [0x800005c0]:sd s5, 8(s3)<br>       |
|  15|[0x800022f8]<br>0x0000000000000000|- rs1 : x12<br> - rd : f15<br>                                                                                |[0x800005dc]:fld fa5, 2048(a2)<br> [0x800005e0]:addi zero, zero, 0<br> [0x800005e4]:addi zero, zero, 0<br> [0x800005e8]:csrrs a7, fflags, zero<br> [0x800005ec]:fsd fa5, 0(a5)<br> [0x800005f0]:sd a7, 8(a5)<br>       |
|  16|[0x80002308]<br>0x0000000000000000|- rs1 : x15<br> - rd : f13<br>                                                                                |[0x8000060c]:fld fa3, 2048(a5)<br> [0x80000610]:addi zero, zero, 0<br> [0x80000614]:addi zero, zero, 0<br> [0x80000618]:csrrs s5, fflags, zero<br> [0x8000061c]:fsd fa3, 0(s3)<br> [0x80000620]:sd s5, 8(s3)<br>       |
|  17|[0x80002318]<br>0x0000000000000000|- rs1 : x30<br> - rd : f28<br>                                                                                |[0x8000063c]:fld ft8, 2048(t5)<br> [0x80000640]:addi zero, zero, 0<br> [0x80000644]:addi zero, zero, 0<br> [0x80000648]:csrrs a7, fflags, zero<br> [0x8000064c]:fsd ft8, 0(a5)<br> [0x80000650]:sd a7, 8(a5)<br>       |
|  18|[0x80002328]<br>0x0000000000000000|- rs1 : x13<br> - rd : f29<br>                                                                                |[0x80000660]:fld ft9, 2048(a3)<br> [0x80000664]:addi zero, zero, 0<br> [0x80000668]:addi zero, zero, 0<br> [0x8000066c]:csrrs a7, fflags, zero<br> [0x80000670]:fsd ft9, 16(a5)<br> [0x80000674]:sd a7, 24(a5)<br>     |
|  19|[0x80002338]<br>0x0000000000000000|- rs1 : x25<br> - rd : f7<br>                                                                                 |[0x80000684]:fld ft7, 2048(s9)<br> [0x80000688]:addi zero, zero, 0<br> [0x8000068c]:addi zero, zero, 0<br> [0x80000690]:csrrs a7, fflags, zero<br> [0x80000694]:fsd ft7, 32(a5)<br> [0x80000698]:sd a7, 40(a5)<br>     |
|  20|[0x80002348]<br>0x0000000000000000|- rs1 : x29<br> - rd : f31<br>                                                                                |[0x800006a8]:fld ft11, 2048(t4)<br> [0x800006ac]:addi zero, zero, 0<br> [0x800006b0]:addi zero, zero, 0<br> [0x800006b4]:csrrs a7, fflags, zero<br> [0x800006b8]:fsd ft11, 48(a5)<br> [0x800006bc]:sd a7, 56(a5)<br>   |
|  21|[0x80002358]<br>0x0000000000000000|- rs1 : x26<br> - rd : f26<br>                                                                                |[0x800006cc]:fld fs10, 2048(s10)<br> [0x800006d0]:addi zero, zero, 0<br> [0x800006d4]:addi zero, zero, 0<br> [0x800006d8]:csrrs a7, fflags, zero<br> [0x800006dc]:fsd fs10, 64(a5)<br> [0x800006e0]:sd a7, 72(a5)<br>  |
|  22|[0x80002368]<br>0x0000000000000000|- rs1 : x6<br> - rd : f2<br>                                                                                  |[0x800006f0]:fld ft2, 2048(t1)<br> [0x800006f4]:addi zero, zero, 0<br> [0x800006f8]:addi zero, zero, 0<br> [0x800006fc]:csrrs a7, fflags, zero<br> [0x80000700]:fsd ft2, 80(a5)<br> [0x80000704]:sd a7, 88(a5)<br>     |
|  23|[0x80002378]<br>0x0000000000000000|- rs1 : x14<br> - rd : f12<br>                                                                                |[0x80000714]:fld fa2, 2048(a4)<br> [0x80000718]:addi zero, zero, 0<br> [0x8000071c]:addi zero, zero, 0<br> [0x80000720]:csrrs a7, fflags, zero<br> [0x80000724]:fsd fa2, 96(a5)<br> [0x80000728]:sd a7, 104(a5)<br>    |
|  24|[0x80002388]<br>0x0000000000000000|- rs1 : x8<br> - rd : f18<br>                                                                                 |[0x80000738]:fld fs2, 2048(fp)<br> [0x8000073c]:addi zero, zero, 0<br> [0x80000740]:addi zero, zero, 0<br> [0x80000744]:csrrs a7, fflags, zero<br> [0x80000748]:fsd fs2, 112(a5)<br> [0x8000074c]:sd a7, 120(a5)<br>   |
|  25|[0x80002398]<br>0x0000000000000000|- rs1 : x2<br> - rd : f10<br>                                                                                 |[0x8000075c]:fld fa0, 2048(sp)<br> [0x80000760]:addi zero, zero, 0<br> [0x80000764]:addi zero, zero, 0<br> [0x80000768]:csrrs a7, fflags, zero<br> [0x8000076c]:fsd fa0, 128(a5)<br> [0x80000770]:sd a7, 136(a5)<br>   |
|  26|[0x800023a8]<br>0x0000000000000000|- rs1 : x5<br> - rd : f1<br>                                                                                  |[0x80000780]:fld ft1, 2048(t0)<br> [0x80000784]:addi zero, zero, 0<br> [0x80000788]:addi zero, zero, 0<br> [0x8000078c]:csrrs a7, fflags, zero<br> [0x80000790]:fsd ft1, 144(a5)<br> [0x80000794]:sd a7, 152(a5)<br>   |
|  27|[0x800023b8]<br>0x0000000000000000|- rs1 : x1<br> - rd : f23<br>                                                                                 |[0x800007a4]:fld fs7, 2048(ra)<br> [0x800007a8]:addi zero, zero, 0<br> [0x800007ac]:addi zero, zero, 0<br> [0x800007b0]:csrrs a7, fflags, zero<br> [0x800007b4]:fsd fs7, 160(a5)<br> [0x800007b8]:sd a7, 168(a5)<br>   |
|  28|[0x800023c8]<br>0x0000000000000000|- rs1 : x24<br> - rd : f30<br>                                                                                |[0x800007c8]:fld ft10, 2048(s8)<br> [0x800007cc]:addi zero, zero, 0<br> [0x800007d0]:addi zero, zero, 0<br> [0x800007d4]:csrrs a7, fflags, zero<br> [0x800007d8]:fsd ft10, 176(a5)<br> [0x800007dc]:sd a7, 184(a5)<br> |
|  29|[0x800023d8]<br>0x0000000000000000|- rs1 : x9<br> - rd : f11<br>                                                                                 |[0x800007ec]:fld fa1, 2048(s1)<br> [0x800007f0]:addi zero, zero, 0<br> [0x800007f4]:addi zero, zero, 0<br> [0x800007f8]:csrrs a7, fflags, zero<br> [0x800007fc]:fsd fa1, 192(a5)<br> [0x80000800]:sd a7, 200(a5)<br>   |
|  30|[0x800023e8]<br>0x0000000000000000|- rs1 : x20<br> - rd : f0<br>                                                                                 |[0x80000810]:fld ft0, 2048(s4)<br> [0x80000814]:addi zero, zero, 0<br> [0x80000818]:addi zero, zero, 0<br> [0x8000081c]:csrrs a7, fflags, zero<br> [0x80000820]:fsd ft0, 208(a5)<br> [0x80000824]:sd a7, 216(a5)<br>   |
|  31|[0x800023f8]<br>0x0000000000000000|- rs1 : x10<br> - rd : f21<br>                                                                                |[0x80000834]:fld fs5, 2048(a0)<br> [0x80000838]:addi zero, zero, 0<br> [0x8000083c]:addi zero, zero, 0<br> [0x80000840]:csrrs a7, fflags, zero<br> [0x80000844]:fsd fs5, 224(a5)<br> [0x80000848]:sd a7, 232(a5)<br>   |
|  32|[0x80002408]<br>0x0000000000000000|- rd : f9<br>                                                                                                 |[0x80000858]:fld fs1, 2048(a4)<br> [0x8000085c]:addi zero, zero, 0<br> [0x80000860]:addi zero, zero, 0<br> [0x80000864]:csrrs a7, fflags, zero<br> [0x80000868]:fsd fs1, 240(a5)<br> [0x8000086c]:sd a7, 248(a5)<br>   |
