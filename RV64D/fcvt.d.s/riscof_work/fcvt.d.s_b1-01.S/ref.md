
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800006b0')]      |
| SIG_REGION                | [('0x80002310', '0x80002510', '64 dwords')]      |
| COV_LABELS                | fcvt.d.s_b1      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.d.s/riscof_work/fcvt.d.s_b1-01.S/ref.S    |
| Total Number of coverpoints| 96     |
| Total Coverpoints Hit     | 91      |
| Total Signature Updates   | 32      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                       coverpoints                                                                       |                                                                        code                                                                         |
|---:|----------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000000|- opcode : fcvt.d.s<br> - rs1 : f22<br> - rd : f8<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.d.s fs0, fs6, rne<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsd fs0, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>        |
|   2|[0x80002328]<br>0x0000000000000000|- rs1 : f5<br> - rd : f5<br> - rs1 == rd<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800003cc]:fcvt.d.s ft5, ft5, rne<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsd ft5, 16(a5)<br> [0x800003d8]:sd a7, 24(a5)<br>      |
|   3|[0x80002338]<br>0x0000000000000000|- rs1 : f26<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x800003e4]:fcvt.d.s fa6, fs10, rne<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd fa6, 32(a5)<br> [0x800003f0]:sd a7, 40(a5)<br>     |
|   4|[0x80002348]<br>0x0000000000000000|- rs1 : f13<br> - rd : f4<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                         |[0x800003fc]:fcvt.d.s ft4, fa3, rne<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:fsd ft4, 48(a5)<br> [0x80000408]:sd a7, 56(a5)<br>      |
|   5|[0x80002358]<br>0x0000000000000000|- rs1 : f4<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                         |[0x80000414]:fcvt.d.s fs8, ft4, rne<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fs8, 64(a5)<br> [0x80000420]:sd a7, 72(a5)<br>      |
|   6|[0x80002368]<br>0x0000000000000000|- rs1 : f11<br> - rd : f19<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                                        |[0x8000042c]:fcvt.d.s fs3, fa1, rne<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsd fs3, 80(a5)<br> [0x80000438]:sd a7, 88(a5)<br>      |
|   7|[0x80002378]<br>0x0000000000000000|- rs1 : f23<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                                         |[0x80000444]:fcvt.d.s ft1, fs7, rne<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd ft1, 96(a5)<br> [0x80000450]:sd a7, 104(a5)<br>     |
|   8|[0x80002388]<br>0x0000000000000000|- rs1 : f12<br> - rd : f10<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                                        |[0x8000045c]:fcvt.d.s fa0, fa2, rne<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:fsd fa0, 112(a5)<br> [0x80000468]:sd a7, 120(a5)<br>    |
|   9|[0x80002398]<br>0x0000000000000000|- rs1 : f15<br> - rd : f13<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                                        |[0x80000474]:fcvt.d.s fa3, fa5, rne<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsd fa3, 128(a5)<br> [0x80000480]:sd a7, 136(a5)<br>    |
|  10|[0x800023a8]<br>0x0000000000000000|- rs1 : f20<br> - rd : f11<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                        |[0x8000048c]:fcvt.d.s fa1, fs4, rne<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsd fa1, 144(a5)<br> [0x80000498]:sd a7, 152(a5)<br>    |
|  11|[0x800023b8]<br>0x0000000000000000|- rs1 : f7<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                         |[0x800004a4]:fcvt.d.s ft9, ft7, rne<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd ft9, 160(a5)<br> [0x800004b0]:sd a7, 168(a5)<br>    |
|  12|[0x800023c8]<br>0x0000000000000000|- rs1 : f1<br> - rd : f27<br> - fs1 == 1 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                         |[0x800004bc]:fcvt.d.s fs11, ft1, rne<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd fs11, 176(a5)<br> [0x800004c8]:sd a7, 184(a5)<br>  |
|  13|[0x800023d8]<br>0x0000000000000000|- rs1 : f21<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x7fe and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                         |[0x800004d4]:fcvt.d.s ft6, fs5, rne<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsd ft6, 192(a5)<br> [0x800004e0]:sd a7, 200(a5)<br>    |
|  14|[0x800023e8]<br>0x0000000000000000|- rs1 : f29<br> - rd : f18<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x800004ec]:fcvt.d.s fs2, ft9, rne<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsd fs2, 208(a5)<br> [0x800004f8]:sd a7, 216(a5)<br>    |
|  15|[0x800023f8]<br>0x0000000000000000|- rs1 : f30<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x80000504]:fcvt.d.s ft11, ft10, rne<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd ft11, 224(a5)<br> [0x80000510]:sd a7, 232(a5)<br> |
|  16|[0x80002408]<br>0x0000000000000000|- rs1 : f2<br> - rd : f12<br> - fs1 == 1 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                         |[0x8000051c]:fcvt.d.s fa2, ft2, rne<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsd fa2, 240(a5)<br> [0x80000528]:sd a7, 248(a5)<br>    |
|  17|[0x80002418]<br>0x0000000000000000|- rs1 : f31<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x001 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                         |[0x80000534]:fcvt.d.s fs1, ft11, rne<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsd fs1, 256(a5)<br> [0x80000540]:sd a7, 264(a5)<br>   |
|  18|[0x80002428]<br>0x0000000000000000|- rs1 : f3<br> - rd : f0<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                          |[0x8000054c]:fcvt.d.s ft0, ft3, rne<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:fsd ft0, 272(a5)<br> [0x80000558]:sd a7, 280(a5)<br>    |
|  19|[0x80002438]<br>0x0000000000000000|- rs1 : f28<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                        |[0x80000564]:fcvt.d.s fs5, ft8, rne<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd fs5, 288(a5)<br> [0x80000570]:sd a7, 296(a5)<br>    |
|  20|[0x80002448]<br>0x0000000000000000|- rs1 : f16<br> - rd : f23<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                        |[0x8000057c]:fcvt.d.s fs7, fa6, rne<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:fsd fs7, 304(a5)<br> [0x80000588]:sd a7, 312(a5)<br>    |
|  21|[0x80002458]<br>0x0000000000000000|- rs1 : f0<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                          |[0x80000594]:fcvt.d.s ft3, ft0, rne<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsd ft3, 320(a5)<br> [0x800005a0]:sd a7, 328(a5)<br>    |
|  22|[0x80002468]<br>0x0000000000000000|- rs1 : f17<br> - rd : f7<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                         |[0x800005ac]:fcvt.d.s ft7, fa7, rne<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:fsd ft7, 336(a5)<br> [0x800005b8]:sd a7, 344(a5)<br>    |
|  23|[0x80002478]<br>0x0000000000000000|- rs1 : f6<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                         |[0x800005c4]:fcvt.d.s fa7, ft6, rne<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd fa7, 352(a5)<br> [0x800005d0]:sd a7, 360(a5)<br>    |
|  24|[0x80002488]<br>0x0000000000000000|- rs1 : f25<br> - rd : f2<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                         |[0x800005dc]:fcvt.d.s ft2, fs9, rne<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:fsd ft2, 368(a5)<br> [0x800005e8]:sd a7, 376(a5)<br>    |
|  25|[0x80002498]<br>0x0000000000000000|- rs1 : f18<br> - rd : f26<br>                                                                                                                           |[0x800005f4]:fcvt.d.s fs10, fs2, rne<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:fsd fs10, 384(a5)<br> [0x80000600]:sd a7, 392(a5)<br>  |
|  26|[0x800024a8]<br>0x0000000000000000|- rs1 : f27<br> - rd : f25<br>                                                                                                                           |[0x8000060c]:fcvt.d.s fs9, fs11, rne<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd fs9, 400(a5)<br> [0x80000618]:sd a7, 408(a5)<br>   |
|  27|[0x800024b8]<br>0x0000000000000000|- rs1 : f19<br> - rd : f14<br>                                                                                                                           |[0x80000624]:fcvt.d.s fa4, fs3, rne<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd fa4, 416(a5)<br> [0x80000630]:sd a7, 424(a5)<br>    |
|  28|[0x800024c8]<br>0x0000000000000000|- rs1 : f10<br> - rd : f28<br>                                                                                                                           |[0x8000063c]:fcvt.d.s ft8, fa0, rne<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsd ft8, 432(a5)<br> [0x80000648]:sd a7, 440(a5)<br>    |
|  29|[0x800024d8]<br>0x0000000000000000|- rs1 : f14<br> - rd : f30<br>                                                                                                                           |[0x80000654]:fcvt.d.s ft10, fa4, rne<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:fsd ft10, 448(a5)<br> [0x80000660]:sd a7, 456(a5)<br>  |
|  30|[0x800024e8]<br>0x0000000000000000|- rs1 : f24<br> - rd : f20<br>                                                                                                                           |[0x8000066c]:fcvt.d.s fs4, fs8, rne<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:fsd fs4, 464(a5)<br> [0x80000678]:sd a7, 472(a5)<br>    |
|  31|[0x800024f8]<br>0x0000000000000000|- rs1 : f8<br> - rd : f15<br>                                                                                                                            |[0x80000684]:fcvt.d.s fa5, fs0, rne<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd fa5, 480(a5)<br> [0x80000690]:sd a7, 488(a5)<br>    |
|  32|[0x80002508]<br>0x0000000000000000|- rs1 : f9<br> - rd : f22<br>                                                                                                                            |[0x8000069c]:fcvt.d.s fs6, fs1, rne<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:fsd fs6, 496(a5)<br> [0x800006a8]:sd a7, 504(a5)<br>    |
