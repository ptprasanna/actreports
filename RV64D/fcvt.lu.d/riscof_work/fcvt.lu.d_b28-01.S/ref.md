
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000700')]      |
| SIG_REGION                | [('0x80002310', '0x80002510', '64 dwords')]      |
| COV_LABELS                | fcvt.lu.d_b28      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.lu.d/riscof_work/fcvt.lu.d_b28-01.S/ref.S    |
| Total Number of coverpoints| 97     |
| Total Coverpoints Hit     | 93      |
| Total Signature Updates   | 64      |
| STAT1                     | 32      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.lu.d', 'rd : x30', 'rs1 : f5', 'fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.lu.d t5, ft5, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd t5, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002318]:0x0000000000000000




Last Coverpoint : ['rd : x10', 'rs1 : f21', 'fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fcvt.lu.d a0, fs5, dyn
	-[0x800003d0]:csrrs a7, fflags, zero
	-[0x800003d4]:sd a0, 16(a5)
Current Store : [0x800003d8] : sd a7, 24(a5) -- Store: [0x80002328]:0x0000000000000010




Last Coverpoint : ['rd : x14', 'rs1 : f4', 'fs1 == 1 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fcvt.lu.d a4, ft4, dyn
	-[0x800003e8]:csrrs a7, fflags, zero
	-[0x800003ec]:sd a4, 32(a5)
Current Store : [0x800003f0] : sd a7, 40(a5) -- Store: [0x80002338]:0x0000000000000010




Last Coverpoint : ['rd : x9', 'rs1 : f28', 'fs1 == 1 and fe1 == 0x43d and fm1 == 0x967a4ae26514c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.lu.d s1, ft8, dyn
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd s1, 48(a5)
Current Store : [0x80000408] : sd a7, 56(a5) -- Store: [0x80002348]:0x0000000000000010




Last Coverpoint : ['rd : x27', 'rs1 : f12', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000414]:fcvt.lu.d s11, fa2, dyn
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd s11, 64(a5)
Current Store : [0x80000420] : sd a7, 72(a5) -- Store: [0x80002358]:0x0000000000000010




Last Coverpoint : ['rd : x18', 'rs1 : f22', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.lu.d s2, fs6, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd s2, 80(a5)
Current Store : [0x80000438] : sd a7, 88(a5) -- Store: [0x80002368]:0x0000000000000010




Last Coverpoint : ['rd : x12', 'rs1 : f8', 'fs1 == 1 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.lu.d a2, fs0, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd a2, 96(a5)
Current Store : [0x80000450] : sd a7, 104(a5) -- Store: [0x80002378]:0x0000000000000010




Last Coverpoint : ['rd : x23', 'rs1 : f2', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.lu.d s7, ft2, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd s7, 112(a5)
Current Store : [0x80000468] : sd a7, 120(a5) -- Store: [0x80002388]:0x0000000000000010




Last Coverpoint : ['rd : x24', 'rs1 : f18', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000474]:fcvt.lu.d s8, fs2, dyn
	-[0x80000478]:csrrs a7, fflags, zero
	-[0x8000047c]:sd s8, 128(a5)
Current Store : [0x80000480] : sd a7, 136(a5) -- Store: [0x80002398]:0x0000000000000010




Last Coverpoint : ['rd : x22', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000048c]:fcvt.lu.d s6, fa0, dyn
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd s6, 144(a5)
Current Store : [0x80000498] : sd a7, 152(a5) -- Store: [0x800023a8]:0x0000000000000010




Last Coverpoint : ['rd : x26', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fcvt.lu.d s10, fs3, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s10, 160(a5)
Current Store : [0x800004b0] : sd a7, 168(a5) -- Store: [0x800023b8]:0x0000000000000010




Last Coverpoint : ['rd : x20', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fcvt.lu.d s4, ft11, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd s4, 176(a5)
Current Store : [0x800004c8] : sd a7, 184(a5) -- Store: [0x800023c8]:0x0000000000000011




Last Coverpoint : ['rd : x31', 'rs1 : f15', 'fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fcvt.lu.d t6, fa5, dyn
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd t6, 192(a5)
Current Store : [0x800004e0] : sd a7, 200(a5) -- Store: [0x800023d8]:0x0000000000000011




Last Coverpoint : ['rd : x8', 'rs1 : f11', 'fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.lu.d fp, fa1, dyn
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd fp, 208(a5)
Current Store : [0x800004f8] : sd a7, 216(a5) -- Store: [0x800023e8]:0x0000000000000011




Last Coverpoint : ['rd : x16', 'rs1 : f17', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000510]:fcvt.lu.d a6, fa7, dyn
	-[0x80000514]:csrrs s5, fflags, zero
	-[0x80000518]:sd a6, 0(s3)
Current Store : [0x8000051c] : sd s5, 8(s3) -- Store: [0x800023f8]:0x0000000000000011




Last Coverpoint : ['rd : x2', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000534]:fcvt.lu.d sp, fs4, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd sp, 0(a5)
Current Store : [0x80000540] : sd a7, 8(a5) -- Store: [0x80002408]:0x0000000000000011




Last Coverpoint : ['rd : x4', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000054c]:fcvt.lu.d tp, fa4, dyn
	-[0x80000550]:csrrs a7, fflags, zero
	-[0x80000554]:sd tp, 16(a5)
Current Store : [0x80000558] : sd a7, 24(a5) -- Store: [0x80002418]:0x0000000000000011




Last Coverpoint : ['rd : x17', 'rs1 : f0', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000570]:fcvt.lu.d a7, ft0, dyn
	-[0x80000574]:csrrs s5, fflags, zero
	-[0x80000578]:sd a7, 0(s3)
Current Store : [0x8000057c] : sd s5, 8(s3) -- Store: [0x80002428]:0x0000000000000011




Last Coverpoint : ['rd : x21', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.lu.d s5, fs1, dyn
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd s5, 0(a5)
Current Store : [0x800005a0] : sd a7, 8(a5) -- Store: [0x80002438]:0x0000000000000011




Last Coverpoint : ['rd : x1', 'rs1 : f6', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005ac]:fcvt.lu.d ra, ft6, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd ra, 16(a5)
Current Store : [0x800005b8] : sd a7, 24(a5) -- Store: [0x80002448]:0x0000000000000011




Last Coverpoint : ['rd : x6', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fcvt.lu.d t1, ft3, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd t1, 32(a5)
Current Store : [0x800005d0] : sd a7, 40(a5) -- Store: [0x80002458]:0x0000000000000011




Last Coverpoint : ['rd : x25', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fcvt.lu.d s9, fs7, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd s9, 48(a5)
Current Store : [0x800005e8] : sd a7, 56(a5) -- Store: [0x80002468]:0x0000000000000011




Last Coverpoint : ['rd : x28', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005f4]:fcvt.lu.d t3, fs9, dyn
	-[0x800005f8]:csrrs a7, fflags, zero
	-[0x800005fc]:sd t3, 64(a5)
Current Store : [0x80000600] : sd a7, 72(a5) -- Store: [0x80002478]:0x0000000000000011




Last Coverpoint : ['rd : x11', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000060c]:fcvt.lu.d a1, fa6, dyn
	-[0x80000610]:csrrs a7, fflags, zero
	-[0x80000614]:sd a1, 80(a5)
Current Store : [0x80000618] : sd a7, 88(a5) -- Store: [0x80002488]:0x0000000000000011




Last Coverpoint : ['rd : x19', 'rs1 : f13', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fcvt.lu.d s3, fa3, dyn
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd s3, 96(a5)
Current Store : [0x80000630] : sd a7, 104(a5) -- Store: [0x80002498]:0x0000000000000011




Last Coverpoint : ['rd : x29', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fcvt.lu.d t4, fs8, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd t4, 112(a5)
Current Store : [0x80000648] : sd a7, 120(a5) -- Store: [0x800024a8]:0x0000000000000011




Last Coverpoint : ['rd : x7', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000654]:fcvt.lu.d t2, ft1, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd t2, 128(a5)
Current Store : [0x80000660] : sd a7, 136(a5) -- Store: [0x800024b8]:0x0000000000000011




Last Coverpoint : ['rd : x13', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x3fe and fm1 == 0x248ee18215dfa and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000066c]:fcvt.lu.d a3, ft7, dyn
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd a3, 144(a5)
Current Store : [0x80000678] : sd a7, 152(a5) -- Store: [0x800024c8]:0x0000000000000011




Last Coverpoint : ['rd : x15', 'rs1 : f26']
Last Code Sequence : 
	-[0x80000690]:fcvt.lu.d a5, fs10, dyn
	-[0x80000694]:csrrs s5, fflags, zero
	-[0x80000698]:sd a5, 0(s3)
Current Store : [0x8000069c] : sd s5, 8(s3) -- Store: [0x800024d8]:0x0000000000000011




Last Coverpoint : ['rd : x0', 'rs1 : f29']
Last Code Sequence : 
	-[0x800006b4]:fcvt.lu.d zero, ft9, dyn
	-[0x800006b8]:csrrs a7, fflags, zero
	-[0x800006bc]:sd zero, 0(a5)
Current Store : [0x800006c0] : sd a7, 8(a5) -- Store: [0x800024e8]:0x0000000000000011




Last Coverpoint : ['rd : x3', 'rs1 : f30']
Last Code Sequence : 
	-[0x800006cc]:fcvt.lu.d gp, ft10, dyn
	-[0x800006d0]:csrrs a7, fflags, zero
	-[0x800006d4]:sd gp, 16(a5)
Current Store : [0x800006d8] : sd a7, 24(a5) -- Store: [0x800024f8]:0x0000000000000011




Last Coverpoint : ['rd : x5', 'rs1 : f27']
Last Code Sequence : 
	-[0x800006e4]:fcvt.lu.d t0, fs11, dyn
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd t0, 32(a5)
Current Store : [0x800006f0] : sd a7, 40(a5) -- Store: [0x80002508]:0x0000000000000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                               coverpoints                                                                |                                                       code                                                       |
|---:|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002310]<br>0x0000000000000000|- opcode : fcvt.lu.d<br> - rd : x30<br> - rs1 : f5<br> - fs1 == 0 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.lu.d t5, ft5, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd t5, 0(a5)<br>     |
|   2|[0x80002320]<br>0x0000000000000000|- rd : x10<br> - rs1 : f21<br> - fs1 == 1 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x800003cc]:fcvt.lu.d a0, fs5, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:sd a0, 16(a5)<br>    |
|   3|[0x80002330]<br>0x0000000000000000|- rd : x14<br> - rs1 : f4<br> - fs1 == 1 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800003e4]:fcvt.lu.d a4, ft4, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:sd a4, 32(a5)<br>    |
|   4|[0x80002340]<br>0x0000000000000000|- rd : x9<br> - rs1 : f28<br> - fs1 == 1 and fe1 == 0x43d and fm1 == 0x967a4ae26514c and rm_val == 0  #nosat<br>                          |[0x800003fc]:fcvt.lu.d s1, ft8, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd s1, 48(a5)<br>    |
|   5|[0x80002350]<br>0x0000000000000000|- rd : x27<br> - rs1 : f12<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                         |[0x80000414]:fcvt.lu.d s11, fa2, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd s11, 64(a5)<br>  |
|   6|[0x80002360]<br>0x0000000000000000|- rd : x18<br> - rs1 : f22<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                         |[0x8000042c]:fcvt.lu.d s2, fs6, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd s2, 80(a5)<br>    |
|   7|[0x80002370]<br>0x0000000000000000|- rd : x12<br> - rs1 : f8<br> - fs1 == 1 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat<br>                          |[0x80000444]:fcvt.lu.d a2, fs0, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd a2, 96(a5)<br>    |
|   8|[0x80002380]<br>0x0000000000000000|- rd : x23<br> - rs1 : f2<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x8000045c]:fcvt.lu.d s7, ft2, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd s7, 112(a5)<br>   |
|   9|[0x80002390]<br>0x0000000000000000|- rd : x24<br> - rs1 : f18<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat<br>                         |[0x80000474]:fcvt.lu.d s8, fs2, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:sd s8, 128(a5)<br>   |
|  10|[0x800023a0]<br>0x0000000000000000|- rd : x22<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                         |[0x8000048c]:fcvt.lu.d s6, fa0, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd s6, 144(a5)<br>   |
|  11|[0x800023b0]<br>0x0000000000000000|- rd : x26<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat<br>                         |[0x800004a4]:fcvt.lu.d s10, fs3, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s10, 160(a5)<br> |
|  12|[0x800023c0]<br>0x0000000000000000|- rd : x20<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x3f8 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x800004bc]:fcvt.lu.d s4, ft11, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd s4, 176(a5)<br>  |
|  13|[0x800023d0]<br>0x0000000000000000|- rd : x31<br> - rs1 : f15<br> - fs1 == 1 and fe1 == 0x3fd and fm1 == 0xb008d57e19f88 and rm_val == 0  #nosat<br>                         |[0x800004d4]:fcvt.lu.d t6, fa5, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd t6, 192(a5)<br>   |
|  14|[0x800023e0]<br>0x0000000000000000|- rd : x8<br> - rs1 : f11<br> - fs1 == 1 and fe1 == 0x000 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x800004ec]:fcvt.lu.d fp, fa1, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd fp, 208(a5)<br>   |
|  15|[0x800023f0]<br>0xFFFFFFFFFFFFFFFF|- rd : x16<br> - rs1 : f17<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x8000000000001 and rm_val == 0  #nosat<br>                         |[0x80000510]:fcvt.lu.d a6, fa7, dyn<br> [0x80000514]:csrrs s5, fflags, zero<br> [0x80000518]:sd a6, 0(s3)<br>     |
|  16|[0x80002400]<br>0xFFFFFFFFFFFFFFFF|- rd : x2<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                          |[0x80000534]:fcvt.lu.d sp, fs4, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd sp, 0(a5)<br>     |
|  17|[0x80002410]<br>0xFFFFFFFFFFFFFFFF|- rd : x4<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x7ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x8000054c]:fcvt.lu.d tp, fa4, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:sd tp, 16(a5)<br>    |
|  18|[0x80002420]<br>0x8000000000000000|- rd : x17<br> - rs1 : f0<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                          |[0x80000570]:fcvt.lu.d a7, ft0, dyn<br> [0x80000574]:csrrs s5, fflags, zero<br> [0x80000578]:sd a7, 0(s3)<br>     |
|  19|[0x80002430]<br>0x36E5D627B8929400|- rd : x21<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x43c and fm1 == 0xb72eb13dc494a and rm_val == 0  #nosat<br>                          |[0x80000594]:fcvt.lu.d s5, fs1, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd s5, 0(a5)<br>     |
|  20|[0x80002440]<br>0x0000000000000003|- rd : x1<br> - rs1 : f6<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x6000000000000 and rm_val == 0  #nosat<br>                           |[0x800005ac]:fcvt.lu.d ra, ft6, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd ra, 16(a5)<br>    |
|  21|[0x80002450]<br>0x0000000000000002|- rd : x6<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                           |[0x800005c4]:fcvt.lu.d t1, ft3, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd t1, 32(a5)<br>    |
|  22|[0x80002460]<br>0x0000000000000002|- rd : x25<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x2000000000000 and rm_val == 0  #nosat<br>                         |[0x800005dc]:fcvt.lu.d s9, fs7, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd s9, 48(a5)<br>    |
|  23|[0x80002470]<br>0x0000000000000002|- rd : x28<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x400 and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                         |[0x800005f4]:fcvt.lu.d t3, fs9, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:sd t3, 64(a5)<br>    |
|  24|[0x80002480]<br>0x0000000000000002|- rd : x11<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0xc000000000000 and rm_val == 0  #nosat<br>                         |[0x8000060c]:fcvt.lu.d a1, fa6, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:sd a1, 80(a5)<br>    |
|  25|[0x80002490]<br>0x0000000000000002|- rd : x19<br> - rs1 : f13<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x8000000000000 and rm_val == 0  #nosat<br>                         |[0x80000624]:fcvt.lu.d s3, fa3, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd s3, 96(a5)<br>    |
|  26|[0x800024a0]<br>0x0000000000000001|- rd : x29<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x4000000000000 and rm_val == 0  #nosat<br>                         |[0x8000063c]:fcvt.lu.d t4, fs8, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd t4, 112(a5)<br>   |
|  27|[0x800024b0]<br>0x0000000000000001|- rd : x7<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x3ff and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                           |[0x80000654]:fcvt.lu.d t2, ft1, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd t2, 128(a5)<br>   |
|  28|[0x800024c0]<br>0x0000000000000001|- rd : x13<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x3fe and fm1 == 0x248ee18215dfa and rm_val == 0  #nosat<br>                          |[0x8000066c]:fcvt.lu.d a3, ft7, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd a3, 144(a5)<br>   |
|  29|[0x800024d0]<br>0x0000000000000000|- rd : x15<br> - rs1 : f26<br>                                                                                                            |[0x80000690]:fcvt.lu.d a5, fs10, dyn<br> [0x80000694]:csrrs s5, fflags, zero<br> [0x80000698]:sd a5, 0(s3)<br>    |
|  30|[0x800024e0]<br>0x0000000000000000|- rd : x0<br> - rs1 : f29<br>                                                                                                             |[0x800006b4]:fcvt.lu.d zero, ft9, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:sd zero, 0(a5)<br> |
|  31|[0x800024f0]<br>0x0000000000000000|- rd : x3<br> - rs1 : f30<br>                                                                                                             |[0x800006cc]:fcvt.lu.d gp, ft10, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:sd gp, 16(a5)<br>   |
|  32|[0x80002500]<br>0x0000000000000000|- rd : x5<br> - rs1 : f27<br>                                                                                                             |[0x800006e4]:fcvt.lu.d t0, fs11, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd t0, 32(a5)<br>   |
