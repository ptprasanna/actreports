
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000ad0')]      |
| SIG_REGION                | [('0x80002410', '0x800028a0', '146 dwords')]      |
| COV_LABELS                | fcvt.lu.d_b22      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.lu.d/riscof_work/fcvt.lu.d_b22-01.S/ref.S    |
| Total Number of coverpoints| 141     |
| Total Coverpoints Hit     | 137      |
| Total Signature Updates   | 146      |
| STAT1                     | 72      |
| STAT2                     | 1      |
| STAT3                     | 0     |
| STAT4                     | 73     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000abc]:fcvt.lu.d t6, ft11, dyn
      [0x80000ac0]:csrrs a7, fflags, zero
      [0x80000ac4]:sd t6, 672(a5)
 -- Signature Address: 0x80002890 Data: 0x3674C6FAD39DDE00
 -- Redundant Coverpoints hit by the op
      - opcode : fcvt.lu.d
      - rd : x31
      - rs1 : f31
      - fs1 == 0 and fe1 == 0x43c and fm1 == 0xb3a637d69ceef and rm_val == 0  #nosat






```

## Details of STAT3

```


```

## Details of STAT4:

```
Last Coverpoint : ['opcode : fcvt.lu.d', 'rd : x11', 'rs1 : f11', 'fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003b4]:fcvt.lu.d a1, fa1, dyn
	-[0x800003b8]:csrrs a7, fflags, zero
	-[0x800003bc]:sd a1, 0(a5)
Current Store : [0x800003c0] : sd a7, 8(a5) -- Store: [0x80002418]:0x0000000000000001




Last Coverpoint : ['rd : x5', 'rs1 : f15', 'fs1 == 0 and fe1 == 0x774 and fm1 == 0xd55d1743e7a34 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003cc]:fcvt.lu.d t0, fa5, dyn
	-[0x800003d0]:csrrs a7, fflags, zero
	-[0x800003d4]:sd t0, 16(a5)
Current Store : [0x800003d8] : sd a7, 24(a5) -- Store: [0x80002428]:0x0000000000000011




Last Coverpoint : ['rd : x4', 'rs1 : f8', 'fs1 == 1 and fe1 == 0x06f and fm1 == 0x26ea417be082f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003e4]:fcvt.lu.d tp, fs0, dyn
	-[0x800003e8]:csrrs a7, fflags, zero
	-[0x800003ec]:sd tp, 32(a5)
Current Store : [0x800003f0] : sd a7, 40(a5) -- Store: [0x80002438]:0x0000000000000011




Last Coverpoint : ['rd : x23', 'rs1 : f21', 'fs1 == 0 and fe1 == 0x441 and fm1 == 0x0cf25109ff475 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fcvt.lu.d s7, fs5, dyn
	-[0x80000400]:csrrs a7, fflags, zero
	-[0x80000404]:sd s7, 48(a5)
Current Store : [0x80000408] : sd a7, 56(a5) -- Store: [0x80002448]:0x0000000000000011




Last Coverpoint : ['rd : x3', 'rs1 : f17', 'fs1 == 1 and fe1 == 0x440 and fm1 == 0x7046a00b97ea6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000414]:fcvt.lu.d gp, fa7, dyn
	-[0x80000418]:csrrs a7, fflags, zero
	-[0x8000041c]:sd gp, 64(a5)
Current Store : [0x80000420] : sd a7, 72(a5) -- Store: [0x80002458]:0x0000000000000011




Last Coverpoint : ['rd : x19', 'rs1 : f5', 'fs1 == 1 and fe1 == 0x43f and fm1 == 0x090a9d43db43e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000042c]:fcvt.lu.d s3, ft5, dyn
	-[0x80000430]:csrrs a7, fflags, zero
	-[0x80000434]:sd s3, 80(a5)
Current Store : [0x80000438] : sd a7, 88(a5) -- Store: [0x80002468]:0x0000000000000011




Last Coverpoint : ['rd : x20', 'rs1 : f18', 'fs1 == 0 and fe1 == 0x43e and fm1 == 0xf956ef7fb4b49 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.lu.d s4, fs2, dyn
	-[0x80000448]:csrrs a7, fflags, zero
	-[0x8000044c]:sd s4, 96(a5)
Current Store : [0x80000450] : sd a7, 104(a5) -- Store: [0x80002478]:0x0000000000000011




Last Coverpoint : ['rd : x27', 'rs1 : f12', 'fs1 == 0 and fe1 == 0x43d and fm1 == 0x70fc18f0846a2 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fcvt.lu.d s11, fa2, dyn
	-[0x80000460]:csrrs a7, fflags, zero
	-[0x80000464]:sd s11, 112(a5)
Current Store : [0x80000468] : sd a7, 120(a5) -- Store: [0x80002488]:0x0000000000000011




Last Coverpoint : ['rd : x0', 'rs1 : f20', 'fs1 == 0 and fe1 == 0x43c and fm1 == 0xb3a637d69ceef and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000474]:fcvt.lu.d zero, fs4, dyn
	-[0x80000478]:csrrs a7, fflags, zero
	-[0x8000047c]:sd zero, 128(a5)
Current Store : [0x80000480] : sd a7, 136(a5) -- Store: [0x80002498]:0x0000000000000011




Last Coverpoint : ['rd : x7', 'rs1 : f7', 'fs1 == 0 and fe1 == 0x43b and fm1 == 0x89e6159672bd6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000048c]:fcvt.lu.d t2, ft7, dyn
	-[0x80000490]:csrrs a7, fflags, zero
	-[0x80000494]:sd t2, 144(a5)
Current Store : [0x80000498] : sd a7, 152(a5) -- Store: [0x800024a8]:0x0000000000000011




Last Coverpoint : ['rd : x9', 'rs1 : f13', 'fs1 == 1 and fe1 == 0x43a and fm1 == 0x8a9226ba1a6ca and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004a4]:fcvt.lu.d s1, fa3, dyn
	-[0x800004a8]:csrrs a7, fflags, zero
	-[0x800004ac]:sd s1, 160(a5)
Current Store : [0x800004b0] : sd a7, 168(a5) -- Store: [0x800024b8]:0x0000000000000011




Last Coverpoint : ['rd : x8', 'rs1 : f22', 'fs1 == 0 and fe1 == 0x439 and fm1 == 0x66b07e5e290be and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fcvt.lu.d fp, fs6, dyn
	-[0x800004c0]:csrrs a7, fflags, zero
	-[0x800004c4]:sd fp, 176(a5)
Current Store : [0x800004c8] : sd a7, 184(a5) -- Store: [0x800024c8]:0x0000000000000011




Last Coverpoint : ['rd : x14', 'rs1 : f4', 'fs1 == 0 and fe1 == 0x438 and fm1 == 0xac9e97d486895 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004d4]:fcvt.lu.d a4, ft4, dyn
	-[0x800004d8]:csrrs a7, fflags, zero
	-[0x800004dc]:sd a4, 192(a5)
Current Store : [0x800004e0] : sd a7, 200(a5) -- Store: [0x800024d8]:0x0000000000000011




Last Coverpoint : ['rd : x10', 'rs1 : f27', 'fs1 == 0 and fe1 == 0x437 and fm1 == 0x1011e7ff822ed and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.lu.d a0, fs11, dyn
	-[0x800004f0]:csrrs a7, fflags, zero
	-[0x800004f4]:sd a0, 208(a5)
Current Store : [0x800004f8] : sd a7, 216(a5) -- Store: [0x800024e8]:0x0000000000000011




Last Coverpoint : ['rd : x15', 'rs1 : f29', 'fs1 == 1 and fe1 == 0x436 and fm1 == 0x7b8b58525e8a8 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000510]:fcvt.lu.d a5, ft9, dyn
	-[0x80000514]:csrrs s5, fflags, zero
	-[0x80000518]:sd a5, 0(s3)
Current Store : [0x8000051c] : sd s5, 8(s3) -- Store: [0x800024f8]:0x0000000000000011




Last Coverpoint : ['rd : x13', 'rs1 : f16', 'fs1 == 0 and fe1 == 0x435 and fm1 == 0xe918bfe057dc5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000534]:fcvt.lu.d a3, fa6, dyn
	-[0x80000538]:csrrs a7, fflags, zero
	-[0x8000053c]:sd a3, 0(a5)
Current Store : [0x80000540] : sd a7, 8(a5) -- Store: [0x80002508]:0x0000000000000011




Last Coverpoint : ['rd : x18', 'rs1 : f6', 'fs1 == 1 and fe1 == 0x434 and fm1 == 0xbc7f83dccee2a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000054c]:fcvt.lu.d s2, ft6, dyn
	-[0x80000550]:csrrs a7, fflags, zero
	-[0x80000554]:sd s2, 16(a5)
Current Store : [0x80000558] : sd a7, 24(a5) -- Store: [0x80002518]:0x0000000000000011




Last Coverpoint : ['rd : x26', 'rs1 : f26', 'fs1 == 1 and fe1 == 0x433 and fm1 == 0xac60e7eea2531 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000564]:fcvt.lu.d s10, fs10, dyn
	-[0x80000568]:csrrs a7, fflags, zero
	-[0x8000056c]:sd s10, 32(a5)
Current Store : [0x80000570] : sd a7, 40(a5) -- Store: [0x80002528]:0x0000000000000011




Last Coverpoint : ['rd : x22', 'rs1 : f0', 'fs1 == 1 and fe1 == 0x432 and fm1 == 0xebadc63fd817f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fcvt.lu.d s6, ft0, dyn
	-[0x80000580]:csrrs a7, fflags, zero
	-[0x80000584]:sd s6, 48(a5)
Current Store : [0x80000588] : sd a7, 56(a5) -- Store: [0x80002538]:0x0000000000000011




Last Coverpoint : ['rd : x30', 'rs1 : f25', 'fs1 == 0 and fe1 == 0x431 and fm1 == 0x9535967a07b54 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.lu.d t5, fs9, dyn
	-[0x80000598]:csrrs a7, fflags, zero
	-[0x8000059c]:sd t5, 64(a5)
Current Store : [0x800005a0] : sd a7, 72(a5) -- Store: [0x80002548]:0x0000000000000011




Last Coverpoint : ['rd : x28', 'rs1 : f3', 'fs1 == 0 and fe1 == 0x430 and fm1 == 0x430321475f78d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005ac]:fcvt.lu.d t3, ft3, dyn
	-[0x800005b0]:csrrs a7, fflags, zero
	-[0x800005b4]:sd t3, 80(a5)
Current Store : [0x800005b8] : sd a7, 88(a5) -- Store: [0x80002558]:0x0000000000000011




Last Coverpoint : ['rd : x1', 'rs1 : f2', 'fs1 == 0 and fe1 == 0x42f and fm1 == 0x51c5be59cf78f and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005c4]:fcvt.lu.d ra, ft2, dyn
	-[0x800005c8]:csrrs a7, fflags, zero
	-[0x800005cc]:sd ra, 96(a5)
Current Store : [0x800005d0] : sd a7, 104(a5) -- Store: [0x80002568]:0x0000000000000011




Last Coverpoint : ['rd : x2', 'rs1 : f1', 'fs1 == 0 and fe1 == 0x42e and fm1 == 0x0b2e2b66b3284 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fcvt.lu.d sp, ft1, dyn
	-[0x800005e0]:csrrs a7, fflags, zero
	-[0x800005e4]:sd sp, 112(a5)
Current Store : [0x800005e8] : sd a7, 120(a5) -- Store: [0x80002578]:0x0000000000000011




Last Coverpoint : ['rd : x17', 'rs1 : f10', 'fs1 == 1 and fe1 == 0x42d and fm1 == 0x0148339422745 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000600]:fcvt.lu.d a7, fa0, dyn
	-[0x80000604]:csrrs s5, fflags, zero
	-[0x80000608]:sd a7, 0(s3)
Current Store : [0x8000060c] : sd s5, 8(s3) -- Store: [0x80002588]:0x0000000000000011




Last Coverpoint : ['rd : x24', 'rs1 : f9', 'fs1 == 0 and fe1 == 0x42c and fm1 == 0x53fbf58fa6e1c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000624]:fcvt.lu.d s8, fs1, dyn
	-[0x80000628]:csrrs a7, fflags, zero
	-[0x8000062c]:sd s8, 0(a5)
Current Store : [0x80000630] : sd a7, 8(a5) -- Store: [0x80002598]:0x0000000000000011




Last Coverpoint : ['rd : x12', 'rs1 : f19', 'fs1 == 1 and fe1 == 0x42b and fm1 == 0x7aea02d143295 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fcvt.lu.d a2, fs3, dyn
	-[0x80000640]:csrrs a7, fflags, zero
	-[0x80000644]:sd a2, 16(a5)
Current Store : [0x80000648] : sd a7, 24(a5) -- Store: [0x800025a8]:0x0000000000000011




Last Coverpoint : ['rd : x25', 'rs1 : f30', 'fs1 == 1 and fe1 == 0x42a and fm1 == 0x2c9f7d230b46c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000654]:fcvt.lu.d s9, ft10, dyn
	-[0x80000658]:csrrs a7, fflags, zero
	-[0x8000065c]:sd s9, 32(a5)
Current Store : [0x80000660] : sd a7, 40(a5) -- Store: [0x800025b8]:0x0000000000000011




Last Coverpoint : ['rd : x6', 'rs1 : f31', 'fs1 == 1 and fe1 == 0x429 and fm1 == 0xa612da911655e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000066c]:fcvt.lu.d t1, ft11, dyn
	-[0x80000670]:csrrs a7, fflags, zero
	-[0x80000674]:sd t1, 48(a5)
Current Store : [0x80000678] : sd a7, 56(a5) -- Store: [0x800025c8]:0x0000000000000011




Last Coverpoint : ['rd : x29', 'rs1 : f14', 'fs1 == 0 and fe1 == 0x428 and fm1 == 0x4d75374ab0a38 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000684]:fcvt.lu.d t4, fa4, dyn
	-[0x80000688]:csrrs a7, fflags, zero
	-[0x8000068c]:sd t4, 64(a5)
Current Store : [0x80000690] : sd a7, 72(a5) -- Store: [0x800025d8]:0x0000000000000011




Last Coverpoint : ['rd : x16', 'rs1 : f28', 'fs1 == 0 and fe1 == 0x427 and fm1 == 0x1e966dd68f201 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006a8]:fcvt.lu.d a6, ft8, dyn
	-[0x800006ac]:csrrs s5, fflags, zero
	-[0x800006b0]:sd a6, 0(s3)
Current Store : [0x800006b4] : sd s5, 8(s3) -- Store: [0x800025e8]:0x0000000000000011




Last Coverpoint : ['rd : x31', 'rs1 : f24', 'fs1 == 0 and fe1 == 0x426 and fm1 == 0x859869e044706 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006cc]:fcvt.lu.d t6, fs8, dyn
	-[0x800006d0]:csrrs a7, fflags, zero
	-[0x800006d4]:sd t6, 0(a5)
Current Store : [0x800006d8] : sd a7, 8(a5) -- Store: [0x800025f8]:0x0000000000000011




Last Coverpoint : ['rd : x21', 'rs1 : f23', 'fs1 == 0 and fe1 == 0x425 and fm1 == 0x4652fae4839a1 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006e4]:fcvt.lu.d s5, fs7, dyn
	-[0x800006e8]:csrrs a7, fflags, zero
	-[0x800006ec]:sd s5, 16(a5)
Current Store : [0x800006f0] : sd a7, 24(a5) -- Store: [0x80002608]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x424 and fm1 == 0x721356a013380 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800006fc]:fcvt.lu.d t6, ft11, dyn
	-[0x80000700]:csrrs a7, fflags, zero
	-[0x80000704]:sd t6, 32(a5)
Current Store : [0x80000708] : sd a7, 40(a5) -- Store: [0x80002618]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x423 and fm1 == 0x6ee8459432a19 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000714]:fcvt.lu.d t6, ft11, dyn
	-[0x80000718]:csrrs a7, fflags, zero
	-[0x8000071c]:sd t6, 48(a5)
Current Store : [0x80000720] : sd a7, 56(a5) -- Store: [0x80002628]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x422 and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000730]:csrrs a7, fflags, zero
	-[0x80000734]:sd t6, 64(a5)
Current Store : [0x80000738] : sd a7, 72(a5) -- Store: [0x80002638]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000744]:fcvt.lu.d t6, ft11, dyn
	-[0x80000748]:csrrs a7, fflags, zero
	-[0x8000074c]:sd t6, 80(a5)
Current Store : [0x80000750] : sd a7, 88(a5) -- Store: [0x80002648]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000075c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000760]:csrrs a7, fflags, zero
	-[0x80000764]:sd t6, 96(a5)
Current Store : [0x80000768] : sd a7, 104(a5) -- Store: [0x80002658]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000774]:fcvt.lu.d t6, ft11, dyn
	-[0x80000778]:csrrs a7, fflags, zero
	-[0x8000077c]:sd t6, 112(a5)
Current Store : [0x80000780] : sd a7, 120(a5) -- Store: [0x80002668]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000078c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000790]:csrrs a7, fflags, zero
	-[0x80000794]:sd t6, 128(a5)
Current Store : [0x80000798] : sd a7, 136(a5) -- Store: [0x80002678]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007a4]:fcvt.lu.d t6, ft11, dyn
	-[0x800007a8]:csrrs a7, fflags, zero
	-[0x800007ac]:sd t6, 144(a5)
Current Store : [0x800007b0] : sd a7, 152(a5) -- Store: [0x80002688]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007bc]:fcvt.lu.d t6, ft11, dyn
	-[0x800007c0]:csrrs a7, fflags, zero
	-[0x800007c4]:sd t6, 160(a5)
Current Store : [0x800007c8] : sd a7, 168(a5) -- Store: [0x80002698]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fcvt.lu.d t6, ft11, dyn
	-[0x800007d8]:csrrs a7, fflags, zero
	-[0x800007dc]:sd t6, 176(a5)
Current Store : [0x800007e0] : sd a7, 184(a5) -- Store: [0x800026a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fcvt.lu.d t6, ft11, dyn
	-[0x800007f0]:csrrs a7, fflags, zero
	-[0x800007f4]:sd t6, 192(a5)
Current Store : [0x800007f8] : sd a7, 200(a5) -- Store: [0x800026b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000804]:fcvt.lu.d t6, ft11, dyn
	-[0x80000808]:csrrs a7, fflags, zero
	-[0x8000080c]:sd t6, 208(a5)
Current Store : [0x80000810] : sd a7, 216(a5) -- Store: [0x800026c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000081c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000820]:csrrs a7, fflags, zero
	-[0x80000824]:sd t6, 224(a5)
Current Store : [0x80000828] : sd a7, 232(a5) -- Store: [0x800026d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000834]:fcvt.lu.d t6, ft11, dyn
	-[0x80000838]:csrrs a7, fflags, zero
	-[0x8000083c]:sd t6, 240(a5)
Current Store : [0x80000840] : sd a7, 248(a5) -- Store: [0x800026e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000850]:csrrs a7, fflags, zero
	-[0x80000854]:sd t6, 256(a5)
Current Store : [0x80000858] : sd a7, 264(a5) -- Store: [0x800026f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000864]:fcvt.lu.d t6, ft11, dyn
	-[0x80000868]:csrrs a7, fflags, zero
	-[0x8000086c]:sd t6, 272(a5)
Current Store : [0x80000870] : sd a7, 280(a5) -- Store: [0x80002708]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000087c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000880]:csrrs a7, fflags, zero
	-[0x80000884]:sd t6, 288(a5)
Current Store : [0x80000888] : sd a7, 296(a5) -- Store: [0x80002718]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000894]:fcvt.lu.d t6, ft11, dyn
	-[0x80000898]:csrrs a7, fflags, zero
	-[0x8000089c]:sd t6, 304(a5)
Current Store : [0x800008a0] : sd a7, 312(a5) -- Store: [0x80002728]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fcvt.lu.d t6, ft11, dyn
	-[0x800008b0]:csrrs a7, fflags, zero
	-[0x800008b4]:sd t6, 320(a5)
Current Store : [0x800008b8] : sd a7, 328(a5) -- Store: [0x80002738]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008c4]:fcvt.lu.d t6, ft11, dyn
	-[0x800008c8]:csrrs a7, fflags, zero
	-[0x800008cc]:sd t6, 336(a5)
Current Store : [0x800008d0] : sd a7, 344(a5) -- Store: [0x80002748]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008dc]:fcvt.lu.d t6, ft11, dyn
	-[0x800008e0]:csrrs a7, fflags, zero
	-[0x800008e4]:sd t6, 352(a5)
Current Store : [0x800008e8] : sd a7, 360(a5) -- Store: [0x80002758]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800008f4]:fcvt.lu.d t6, ft11, dyn
	-[0x800008f8]:csrrs a7, fflags, zero
	-[0x800008fc]:sd t6, 368(a5)
Current Store : [0x80000900] : sd a7, 376(a5) -- Store: [0x80002768]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000910]:csrrs a7, fflags, zero
	-[0x80000914]:sd t6, 384(a5)
Current Store : [0x80000918] : sd a7, 392(a5) -- Store: [0x80002778]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000924]:fcvt.lu.d t6, ft11, dyn
	-[0x80000928]:csrrs a7, fflags, zero
	-[0x8000092c]:sd t6, 400(a5)
Current Store : [0x80000930] : sd a7, 408(a5) -- Store: [0x80002788]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000093c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000940]:csrrs a7, fflags, zero
	-[0x80000944]:sd t6, 416(a5)
Current Store : [0x80000948] : sd a7, 424(a5) -- Store: [0x80002798]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000954]:fcvt.lu.d t6, ft11, dyn
	-[0x80000958]:csrrs a7, fflags, zero
	-[0x8000095c]:sd t6, 432(a5)
Current Store : [0x80000960] : sd a7, 440(a5) -- Store: [0x800027a8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000970]:csrrs a7, fflags, zero
	-[0x80000974]:sd t6, 448(a5)
Current Store : [0x80000978] : sd a7, 456(a5) -- Store: [0x800027b8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000984]:fcvt.lu.d t6, ft11, dyn
	-[0x80000988]:csrrs a7, fflags, zero
	-[0x8000098c]:sd t6, 464(a5)
Current Store : [0x80000990] : sd a7, 472(a5) -- Store: [0x800027c8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x408 and fm1 == 0x43277acca7f0d and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x8000099c]:fcvt.lu.d t6, ft11, dyn
	-[0x800009a0]:csrrs a7, fflags, zero
	-[0x800009a4]:sd t6, 480(a5)
Current Store : [0x800009a8] : sd a7, 488(a5) -- Store: [0x800027d8]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x407 and fm1 == 0x489b36bd7f503 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009b4]:fcvt.lu.d t6, ft11, dyn
	-[0x800009b8]:csrrs a7, fflags, zero
	-[0x800009bc]:sd t6, 496(a5)
Current Store : [0x800009c0] : sd a7, 504(a5) -- Store: [0x800027e8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x406 and fm1 == 0x5ae6a9a6ab329 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fcvt.lu.d t6, ft11, dyn
	-[0x800009d0]:csrrs a7, fflags, zero
	-[0x800009d4]:sd t6, 512(a5)
Current Store : [0x800009d8] : sd a7, 520(a5) -- Store: [0x800027f8]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x405 and fm1 == 0xdc3386b9f15c4 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009e4]:fcvt.lu.d t6, ft11, dyn
	-[0x800009e8]:csrrs a7, fflags, zero
	-[0x800009ec]:sd t6, 528(a5)
Current Store : [0x800009f0] : sd a7, 536(a5) -- Store: [0x80002808]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x404 and fm1 == 0x5c74eff1e5bef and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x800009fc]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a00]:csrrs a7, fflags, zero
	-[0x80000a04]:sd t6, 544(a5)
Current Store : [0x80000a08] : sd a7, 552(a5) -- Store: [0x80002818]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x403 and fm1 == 0xf3ebcf3d06f86 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a14]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a18]:csrrs a7, fflags, zero
	-[0x80000a1c]:sd t6, 560(a5)
Current Store : [0x80000a20] : sd a7, 568(a5) -- Store: [0x80002828]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x402 and fm1 == 0x137a953e8eb43 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a30]:csrrs a7, fflags, zero
	-[0x80000a34]:sd t6, 576(a5)
Current Store : [0x80000a38] : sd a7, 584(a5) -- Store: [0x80002838]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x401 and fm1 == 0x854a908ceac39 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a44]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a48]:csrrs a7, fflags, zero
	-[0x80000a4c]:sd t6, 592(a5)
Current Store : [0x80000a50] : sd a7, 600(a5) -- Store: [0x80002848]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x400 and fm1 == 0xcf84ba749f9c5 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a5c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a60]:csrrs a7, fflags, zero
	-[0x80000a64]:sd t6, 608(a5)
Current Store : [0x80000a68] : sd a7, 616(a5) -- Store: [0x80002858]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3ff and fm1 == 0xd2d6b7dc59a3a and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a78]:csrrs a7, fflags, zero
	-[0x80000a7c]:sd t6, 624(a5)
Current Store : [0x80000a80] : sd a7, 632(a5) -- Store: [0x80002868]:0x0000000000000011




Last Coverpoint : ['fs1 == 1 and fe1 == 0x3fe and fm1 == 0x766ba34c2da80 and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fcvt.lu.d t6, ft11, dyn
	-[0x80000a90]:csrrs a7, fflags, zero
	-[0x80000a94]:sd t6, 640(a5)
Current Store : [0x80000a98] : sd a7, 648(a5) -- Store: [0x80002878]:0x0000000000000011




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3fd and fm1 == 0x93fdc7b89296c and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000aa4]:fcvt.lu.d t6, ft11, dyn
	-[0x80000aa8]:csrrs a7, fflags, zero
	-[0x80000aac]:sd t6, 656(a5)
Current Store : [0x80000ab0] : sd a7, 664(a5) -- Store: [0x80002888]:0x0000000000000011




Last Coverpoint : ['opcode : fcvt.lu.d', 'rd : x31', 'rs1 : f31', 'fs1 == 0 and fe1 == 0x43c and fm1 == 0xb3a637d69ceef and rm_val == 0  #nosat']
Last Code Sequence : 
	-[0x80000abc]:fcvt.lu.d t6, ft11, dyn
	-[0x80000ac0]:csrrs a7, fflags, zero
	-[0x80000ac4]:sd t6, 672(a5)
Current Store : [0x80000ac8] : sd a7, 680(a5) -- Store: [0x80002898]:0x0000000000000011





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                coverpoints                                                                |                                                        code                                                        |
|---:|----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002410]<br>0x0000000000000000|- opcode : fcvt.lu.d<br> - rd : x11<br> - rs1 : f11<br> - fs1 == 0 and fe1 == 0x3fc and fm1 == 0x08577924770d3 and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.lu.d a1, fa1, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:sd a1, 0(a5)<br>       |
|   2|[0x80002420]<br>0xFFFFFFFFFFFFFFFF|- rd : x5<br> - rs1 : f15<br> - fs1 == 0 and fe1 == 0x774 and fm1 == 0xd55d1743e7a34 and rm_val == 0  #nosat<br>                           |[0x800003cc]:fcvt.lu.d t0, fa5, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:sd t0, 16(a5)<br>      |
|   3|[0x80002430]<br>0x0000000000000000|- rd : x4<br> - rs1 : f8<br> - fs1 == 1 and fe1 == 0x06f and fm1 == 0x26ea417be082f and rm_val == 0  #nosat<br>                            |[0x800003e4]:fcvt.lu.d tp, fs0, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:sd tp, 32(a5)<br>      |
|   4|[0x80002440]<br>0xFFFFFFFFFFFFFFFF|- rd : x23<br> - rs1 : f21<br> - fs1 == 0 and fe1 == 0x441 and fm1 == 0x0cf25109ff475 and rm_val == 0  #nosat<br>                          |[0x800003fc]:fcvt.lu.d s7, fs5, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:sd s7, 48(a5)<br>      |
|   5|[0x80002450]<br>0x0000000000000000|- rd : x3<br> - rs1 : f17<br> - fs1 == 1 and fe1 == 0x440 and fm1 == 0x7046a00b97ea6 and rm_val == 0  #nosat<br>                           |[0x80000414]:fcvt.lu.d gp, fa7, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:sd gp, 64(a5)<br>      |
|   6|[0x80002460]<br>0x0000000000000000|- rd : x19<br> - rs1 : f5<br> - fs1 == 1 and fe1 == 0x43f and fm1 == 0x090a9d43db43e and rm_val == 0  #nosat<br>                           |[0x8000042c]:fcvt.lu.d s3, ft5, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:sd s3, 80(a5)<br>      |
|   7|[0x80002470]<br>0xFCAB77BFDA5A4800|- rd : x20<br> - rs1 : f18<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0xf956ef7fb4b49 and rm_val == 0  #nosat<br>                          |[0x80000444]:fcvt.lu.d s4, fs2, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:sd s4, 96(a5)<br>      |
|   8|[0x80002480]<br>0x5C3F063C211A8800|- rd : x27<br> - rs1 : f12<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0x70fc18f0846a2 and rm_val == 0  #nosat<br>                          |[0x8000045c]:fcvt.lu.d s11, fa2, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:sd s11, 112(a5)<br>   |
|   9|[0x80002490]<br>0x0000000000000000|- rd : x0<br> - rs1 : f20<br> - fs1 == 0 and fe1 == 0x43c and fm1 == 0xb3a637d69ceef and rm_val == 0  #nosat<br>                           |[0x80000474]:fcvt.lu.d zero, fs4, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:sd zero, 128(a5)<br> |
|  10|[0x800024a0]<br>0x189E6159672BD600|- rd : x7<br> - rs1 : f7<br> - fs1 == 0 and fe1 == 0x43b and fm1 == 0x89e6159672bd6 and rm_val == 0  #nosat<br>                            |[0x8000048c]:fcvt.lu.d t2, ft7, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:sd t2, 144(a5)<br>     |
|  11|[0x800024b0]<br>0x0000000000000000|- rd : x9<br> - rs1 : f13<br> - fs1 == 1 and fe1 == 0x43a and fm1 == 0x8a9226ba1a6ca and rm_val == 0  #nosat<br>                           |[0x800004a4]:fcvt.lu.d s1, fa3, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:sd s1, 160(a5)<br>     |
|  12|[0x800024c0]<br>0x059AC1F978A42F80|- rd : x8<br> - rs1 : f22<br> - fs1 == 0 and fe1 == 0x439 and fm1 == 0x66b07e5e290be and rm_val == 0  #nosat<br>                           |[0x800004bc]:fcvt.lu.d fp, fs6, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:sd fp, 176(a5)<br>     |
|  13|[0x800024d0]<br>0x03593D2FA90D12A0|- rd : x14<br> - rs1 : f4<br> - fs1 == 0 and fe1 == 0x438 and fm1 == 0xac9e97d486895 and rm_val == 0  #nosat<br>                           |[0x800004d4]:fcvt.lu.d a4, ft4, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:sd a4, 192(a5)<br>     |
|  14|[0x800024e0]<br>0x011011E7FF822ED0|- rd : x10<br> - rs1 : f27<br> - fs1 == 0 and fe1 == 0x437 and fm1 == 0x1011e7ff822ed and rm_val == 0  #nosat<br>                          |[0x800004ec]:fcvt.lu.d a0, fs11, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:sd a0, 208(a5)<br>    |
|  15|[0x800024f0]<br>0x0000000000000000|- rd : x15<br> - rs1 : f29<br> - fs1 == 1 and fe1 == 0x436 and fm1 == 0x7b8b58525e8a8 and rm_val == 0  #nosat<br>                          |[0x80000510]:fcvt.lu.d a5, ft9, dyn<br> [0x80000514]:csrrs s5, fflags, zero<br> [0x80000518]:sd a5, 0(s3)<br>       |
|  16|[0x80002500]<br>0x007A462FF815F714|- rd : x13<br> - rs1 : f16<br> - fs1 == 0 and fe1 == 0x435 and fm1 == 0xe918bfe057dc5 and rm_val == 0  #nosat<br>                          |[0x80000534]:fcvt.lu.d a3, fa6, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:sd a3, 0(a5)<br>       |
|  17|[0x80002510]<br>0x0000000000000000|- rd : x18<br> - rs1 : f6<br> - fs1 == 1 and fe1 == 0x434 and fm1 == 0xbc7f83dccee2a and rm_val == 0  #nosat<br>                           |[0x8000054c]:fcvt.lu.d s2, ft6, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:sd s2, 16(a5)<br>      |
|  18|[0x80002520]<br>0x0000000000000000|- rd : x26<br> - rs1 : f26<br> - fs1 == 1 and fe1 == 0x433 and fm1 == 0xac60e7eea2531 and rm_val == 0  #nosat<br>                          |[0x80000564]:fcvt.lu.d s10, fs10, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:sd s10, 32(a5)<br>   |
|  19|[0x80002530]<br>0x0000000000000000|- rd : x22<br> - rs1 : f0<br> - fs1 == 1 and fe1 == 0x432 and fm1 == 0xebadc63fd817f and rm_val == 0  #nosat<br>                           |[0x8000057c]:fcvt.lu.d s6, ft0, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:sd s6, 48(a5)<br>      |
|  20|[0x80002540]<br>0x000654D659E81ED5|- rd : x30<br> - rs1 : f25<br> - fs1 == 0 and fe1 == 0x431 and fm1 == 0x9535967a07b54 and rm_val == 0  #nosat<br>                          |[0x80000594]:fcvt.lu.d t5, fs9, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:sd t5, 64(a5)<br>      |
|  21|[0x80002550]<br>0x00028606428EBEF2|- rd : x28<br> - rs1 : f3<br> - fs1 == 0 and fe1 == 0x430 and fm1 == 0x430321475f78d and rm_val == 0  #nosat<br>                           |[0x800005ac]:fcvt.lu.d t3, ft3, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:sd t3, 80(a5)<br>      |
|  22|[0x80002560]<br>0x000151C5BE59CF79|- rd : x1<br> - rs1 : f2<br> - fs1 == 0 and fe1 == 0x42f and fm1 == 0x51c5be59cf78f and rm_val == 0  #nosat<br>                            |[0x800005c4]:fcvt.lu.d ra, ft2, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:sd ra, 96(a5)<br>      |
|  23|[0x80002570]<br>0x0000859715B35994|- rd : x2<br> - rs1 : f1<br> - fs1 == 0 and fe1 == 0x42e and fm1 == 0x0b2e2b66b3284 and rm_val == 0  #nosat<br>                            |[0x800005dc]:fcvt.lu.d sp, ft1, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:sd sp, 112(a5)<br>     |
|  24|[0x80002580]<br>0x0000000000000000|- rd : x17<br> - rs1 : f10<br> - fs1 == 1 and fe1 == 0x42d and fm1 == 0x0148339422745 and rm_val == 0  #nosat<br>                          |[0x80000600]:fcvt.lu.d a7, fa0, dyn<br> [0x80000604]:csrrs s5, fflags, zero<br> [0x80000608]:sd a7, 0(s3)<br>       |
|  25|[0x80002590]<br>0x00002A7F7EB1F4DC|- rd : x24<br> - rs1 : f9<br> - fs1 == 0 and fe1 == 0x42c and fm1 == 0x53fbf58fa6e1c and rm_val == 0  #nosat<br>                           |[0x80000624]:fcvt.lu.d s8, fs1, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:sd s8, 0(a5)<br>       |
|  26|[0x800025a0]<br>0x0000000000000000|- rd : x12<br> - rs1 : f19<br> - fs1 == 1 and fe1 == 0x42b and fm1 == 0x7aea02d143295 and rm_val == 0  #nosat<br>                          |[0x8000063c]:fcvt.lu.d a2, fs3, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:sd a2, 16(a5)<br>      |
|  27|[0x800025b0]<br>0x0000000000000000|- rd : x25<br> - rs1 : f30<br> - fs1 == 1 and fe1 == 0x42a and fm1 == 0x2c9f7d230b46c and rm_val == 0  #nosat<br>                          |[0x80000654]:fcvt.lu.d s9, ft10, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:sd s9, 32(a5)<br>     |
|  28|[0x800025c0]<br>0x0000000000000000|- rd : x6<br> - rs1 : f31<br> - fs1 == 1 and fe1 == 0x429 and fm1 == 0xa612da911655e and rm_val == 0  #nosat<br>                           |[0x8000066c]:fcvt.lu.d t1, ft11, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:sd t1, 48(a5)<br>     |
|  29|[0x800025d0]<br>0x0000029AEA6E9561|- rd : x29<br> - rs1 : f14<br> - fs1 == 0 and fe1 == 0x428 and fm1 == 0x4d75374ab0a38 and rm_val == 0  #nosat<br>                          |[0x80000684]:fcvt.lu.d t4, fa4, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:sd t4, 64(a5)<br>      |
|  30|[0x800025e0]<br>0x0000011E966DD68F|- rd : x16<br> - rs1 : f28<br> - fs1 == 0 and fe1 == 0x427 and fm1 == 0x1e966dd68f201 and rm_val == 0  #nosat<br>                          |[0x800006a8]:fcvt.lu.d a6, ft8, dyn<br> [0x800006ac]:csrrs s5, fflags, zero<br> [0x800006b0]:sd a6, 0(s3)<br>       |
|  31|[0x800025f0]<br>0x000000C2CC34F022|- rd : x31<br> - rs1 : f24<br> - fs1 == 0 and fe1 == 0x426 and fm1 == 0x859869e044706 and rm_val == 0  #nosat<br>                          |[0x800006cc]:fcvt.lu.d t6, fs8, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:sd t6, 0(a5)<br>       |
|  32|[0x80002600]<br>0x0000005194BEB921|- rd : x21<br> - rs1 : f23<br> - fs1 == 0 and fe1 == 0x425 and fm1 == 0x4652fae4839a1 and rm_val == 0  #nosat<br>                          |[0x800006e4]:fcvt.lu.d s5, fs7, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:sd s5, 16(a5)<br>      |
|  33|[0x80002610]<br>0x0000002E426AD402|- fs1 == 0 and fe1 == 0x424 and fm1 == 0x721356a013380 and rm_val == 0  #nosat<br>                                                         |[0x800006fc]:fcvt.lu.d t6, ft11, dyn<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:sd t6, 32(a5)<br>     |
|  34|[0x80002620]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x423 and fm1 == 0x6ee8459432a19 and rm_val == 0  #nosat<br>                                                         |[0x80000714]:fcvt.lu.d t6, ft11, dyn<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:sd t6, 48(a5)<br>     |
|  35|[0x80002630]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x422 and fm1 == 0x30e08ceb506f6 and rm_val == 0  #nosat<br>                                                         |[0x8000072c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:sd t6, 64(a5)<br>     |
|  36|[0x80002640]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x421 and fm1 == 0x2a96d71097999 and rm_val == 0  #nosat<br>                                                         |[0x80000744]:fcvt.lu.d t6, ft11, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:sd t6, 80(a5)<br>     |
|  37|[0x80002650]<br>0x000000038BD8D8D1|- fs1 == 0 and fe1 == 0x420 and fm1 == 0xc5ec6c6880007 and rm_val == 0  #nosat<br>                                                         |[0x8000075c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:sd t6, 96(a5)<br>     |
|  38|[0x80002660]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x41f and fm1 == 0x1ce80265039f6 and rm_val == 0  #nosat<br>                                                         |[0x80000774]:fcvt.lu.d t6, ft11, dyn<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:sd t6, 112(a5)<br>    |
|  39|[0x80002670]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x41e and fm1 == 0xe9b7e5fc9eba4 and rm_val == 0  #nosat<br>                                                         |[0x8000078c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:sd t6, 128(a5)<br>    |
|  40|[0x80002680]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x41d and fm1 == 0x9136562694646 and rm_val == 0  #nosat<br>                                                         |[0x800007a4]:fcvt.lu.d t6, ft11, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:sd t6, 144(a5)<br>    |
|  41|[0x80002690]<br>0x00000000229723B6|- fs1 == 0 and fe1 == 0x41c and fm1 == 0x14b91dae98554 and rm_val == 0  #nosat<br>                                                         |[0x800007bc]:fcvt.lu.d t6, ft11, dyn<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:sd t6, 160(a5)<br>    |
|  42|[0x800026a0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x41b and fm1 == 0x889261270dee2 and rm_val == 0  #nosat<br>                                                         |[0x800007d4]:fcvt.lu.d t6, ft11, dyn<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:sd t6, 176(a5)<br>    |
|  43|[0x800026b0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x41a and fm1 == 0x9b4f3d167533a and rm_val == 0  #nosat<br>                                                         |[0x800007ec]:fcvt.lu.d t6, ft11, dyn<br> [0x800007f0]:csrrs a7, fflags, zero<br> [0x800007f4]:sd t6, 192(a5)<br>    |
|  44|[0x800026c0]<br>0x0000000005FC8582|- fs1 == 0 and fe1 == 0x419 and fm1 == 0x7f21608208d09 and rm_val == 0  #nosat<br>                                                         |[0x80000804]:fcvt.lu.d t6, ft11, dyn<br> [0x80000808]:csrrs a7, fflags, zero<br> [0x8000080c]:sd t6, 208(a5)<br>    |
|  45|[0x800026d0]<br>0x00000000027A0C2D|- fs1 == 0 and fe1 == 0x418 and fm1 == 0x3d06169b1dcbf and rm_val == 0  #nosat<br>                                                         |[0x8000081c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000820]:csrrs a7, fflags, zero<br> [0x80000824]:sd t6, 224(a5)<br>    |
|  46|[0x800026e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x417 and fm1 == 0x396bad798c9cf and rm_val == 0  #nosat<br>                                                         |[0x80000834]:fcvt.lu.d t6, ft11, dyn<br> [0x80000838]:csrrs a7, fflags, zero<br> [0x8000083c]:sd t6, 240(a5)<br>    |
|  47|[0x800026f0]<br>0x0000000000C03ED7|- fs1 == 0 and fe1 == 0x416 and fm1 == 0x807dad814d575 and rm_val == 0  #nosat<br>                                                         |[0x8000084c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000850]:csrrs a7, fflags, zero<br> [0x80000854]:sd t6, 256(a5)<br>    |
|  48|[0x80002700]<br>0x0000000000656937|- fs1 == 0 and fe1 == 0x415 and fm1 == 0x95a4da7298c66 and rm_val == 0  #nosat<br>                                                         |[0x80000864]:fcvt.lu.d t6, ft11, dyn<br> [0x80000868]:csrrs a7, fflags, zero<br> [0x8000086c]:sd t6, 272(a5)<br>    |
|  49|[0x80002710]<br>0x00000000002F0A07|- fs1 == 0 and fe1 == 0x414 and fm1 == 0x785036f9fb997 and rm_val == 0  #nosat<br>                                                         |[0x8000087c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000880]:csrrs a7, fflags, zero<br> [0x80000884]:sd t6, 288(a5)<br>    |
|  50|[0x80002720]<br>0x000000000018C8A2|- fs1 == 0 and fe1 == 0x413 and fm1 == 0x8c8a1aaac3142 and rm_val == 0  #nosat<br>                                                         |[0x80000894]:fcvt.lu.d t6, ft11, dyn<br> [0x80000898]:csrrs a7, fflags, zero<br> [0x8000089c]:sd t6, 304(a5)<br>    |
|  51|[0x80002730]<br>0x000000000009EBE5|- fs1 == 0 and fe1 == 0x412 and fm1 == 0x3d7c9e5f0307e and rm_val == 0  #nosat<br>                                                         |[0x800008ac]:fcvt.lu.d t6, ft11, dyn<br> [0x800008b0]:csrrs a7, fflags, zero<br> [0x800008b4]:sd t6, 320(a5)<br>    |
|  52|[0x80002740]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x411 and fm1 == 0x5dbbb894deab4 and rm_val == 0  #nosat<br>                                                         |[0x800008c4]:fcvt.lu.d t6, ft11, dyn<br> [0x800008c8]:csrrs a7, fflags, zero<br> [0x800008cc]:sd t6, 336(a5)<br>    |
|  53|[0x80002750]<br>0x000000000003D1B6|- fs1 == 0 and fe1 == 0x410 and fm1 == 0xe8dacf0e58650 and rm_val == 0  #nosat<br>                                                         |[0x800008dc]:fcvt.lu.d t6, ft11, dyn<br> [0x800008e0]:csrrs a7, fflags, zero<br> [0x800008e4]:sd t6, 352(a5)<br>    |
|  54|[0x80002760]<br>0x000000000001224C|- fs1 == 0 and fe1 == 0x40f and fm1 == 0x224c03c53d0e3 and rm_val == 0  #nosat<br>                                                         |[0x800008f4]:fcvt.lu.d t6, ft11, dyn<br> [0x800008f8]:csrrs a7, fflags, zero<br> [0x800008fc]:sd t6, 368(a5)<br>    |
|  55|[0x80002770]<br>0x000000000000CA9E|- fs1 == 0 and fe1 == 0x40e and fm1 == 0x953b00b54aa22 and rm_val == 0  #nosat<br>                                                         |[0x8000090c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000910]:csrrs a7, fflags, zero<br> [0x80000914]:sd t6, 384(a5)<br>    |
|  56|[0x80002780]<br>0x0000000000006741|- fs1 == 0 and fe1 == 0x40d and fm1 == 0x9d02f708cc1b6 and rm_val == 0  #nosat<br>                                                         |[0x80000924]:fcvt.lu.d t6, ft11, dyn<br> [0x80000928]:csrrs a7, fflags, zero<br> [0x8000092c]:sd t6, 400(a5)<br>    |
|  57|[0x80002790]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x40c and fm1 == 0x3d480fb7f6f5d and rm_val == 0  #nosat<br>                                                         |[0x8000093c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000940]:csrrs a7, fflags, zero<br> [0x80000944]:sd t6, 416(a5)<br>    |
|  58|[0x800027a0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x40b and fm1 == 0xc491074f942cb and rm_val == 0  #nosat<br>                                                         |[0x80000954]:fcvt.lu.d t6, ft11, dyn<br> [0x80000958]:csrrs a7, fflags, zero<br> [0x8000095c]:sd t6, 432(a5)<br>    |
|  59|[0x800027b0]<br>0x0000000000000AE7|- fs1 == 0 and fe1 == 0x40a and fm1 == 0x5cd28a96ec2b3 and rm_val == 0  #nosat<br>                                                         |[0x8000096c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000970]:csrrs a7, fflags, zero<br> [0x80000974]:sd t6, 448(a5)<br>    |
|  60|[0x800027c0]<br>0x00000000000006BE|- fs1 == 0 and fe1 == 0x409 and fm1 == 0xaf9492cb7362c and rm_val == 0  #nosat<br>                                                         |[0x80000984]:fcvt.lu.d t6, ft11, dyn<br> [0x80000988]:csrrs a7, fflags, zero<br> [0x8000098c]:sd t6, 464(a5)<br>    |
|  61|[0x800027d0]<br>0x0000000000000286|- fs1 == 0 and fe1 == 0x408 and fm1 == 0x43277acca7f0d and rm_val == 0  #nosat<br>                                                         |[0x8000099c]:fcvt.lu.d t6, ft11, dyn<br> [0x800009a0]:csrrs a7, fflags, zero<br> [0x800009a4]:sd t6, 480(a5)<br>    |
|  62|[0x800027e0]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x407 and fm1 == 0x489b36bd7f503 and rm_val == 0  #nosat<br>                                                         |[0x800009b4]:fcvt.lu.d t6, ft11, dyn<br> [0x800009b8]:csrrs a7, fflags, zero<br> [0x800009bc]:sd t6, 496(a5)<br>    |
|  63|[0x800027f0]<br>0x00000000000000AD|- fs1 == 0 and fe1 == 0x406 and fm1 == 0x5ae6a9a6ab329 and rm_val == 0  #nosat<br>                                                         |[0x800009cc]:fcvt.lu.d t6, ft11, dyn<br> [0x800009d0]:csrrs a7, fflags, zero<br> [0x800009d4]:sd t6, 512(a5)<br>    |
|  64|[0x80002800]<br>0x0000000000000077|- fs1 == 0 and fe1 == 0x405 and fm1 == 0xdc3386b9f15c4 and rm_val == 0  #nosat<br>                                                         |[0x800009e4]:fcvt.lu.d t6, ft11, dyn<br> [0x800009e8]:csrrs a7, fflags, zero<br> [0x800009ec]:sd t6, 528(a5)<br>    |
|  65|[0x80002810]<br>0x000000000000002C|- fs1 == 0 and fe1 == 0x404 and fm1 == 0x5c74eff1e5bef and rm_val == 0  #nosat<br>                                                         |[0x800009fc]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a00]:csrrs a7, fflags, zero<br> [0x80000a04]:sd t6, 544(a5)<br>    |
|  66|[0x80002820]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x403 and fm1 == 0xf3ebcf3d06f86 and rm_val == 0  #nosat<br>                                                         |[0x80000a14]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a18]:csrrs a7, fflags, zero<br> [0x80000a1c]:sd t6, 560(a5)<br>    |
|  67|[0x80002830]<br>0x0000000000000009|- fs1 == 0 and fe1 == 0x402 and fm1 == 0x137a953e8eb43 and rm_val == 0  #nosat<br>                                                         |[0x80000a2c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a30]:csrrs a7, fflags, zero<br> [0x80000a34]:sd t6, 576(a5)<br>    |
|  68|[0x80002840]<br>0x0000000000000006|- fs1 == 0 and fe1 == 0x401 and fm1 == 0x854a908ceac39 and rm_val == 0  #nosat<br>                                                         |[0x80000a44]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a48]:csrrs a7, fflags, zero<br> [0x80000a4c]:sd t6, 592(a5)<br>    |
|  69|[0x80002850]<br>0x0000000000000004|- fs1 == 0 and fe1 == 0x400 and fm1 == 0xcf84ba749f9c5 and rm_val == 0  #nosat<br>                                                         |[0x80000a5c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a60]:csrrs a7, fflags, zero<br> [0x80000a64]:sd t6, 608(a5)<br>    |
|  70|[0x80002860]<br>0x0000000000000002|- fs1 == 0 and fe1 == 0x3ff and fm1 == 0xd2d6b7dc59a3a and rm_val == 0  #nosat<br>                                                         |[0x80000a74]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a78]:csrrs a7, fflags, zero<br> [0x80000a7c]:sd t6, 624(a5)<br>    |
|  71|[0x80002870]<br>0x0000000000000000|- fs1 == 1 and fe1 == 0x3fe and fm1 == 0x766ba34c2da80 and rm_val == 0  #nosat<br>                                                         |[0x80000a8c]:fcvt.lu.d t6, ft11, dyn<br> [0x80000a90]:csrrs a7, fflags, zero<br> [0x80000a94]:sd t6, 640(a5)<br>    |
|  72|[0x80002880]<br>0x0000000000000000|- fs1 == 0 and fe1 == 0x3fd and fm1 == 0x93fdc7b89296c and rm_val == 0  #nosat<br>                                                         |[0x80000aa4]:fcvt.lu.d t6, ft11, dyn<br> [0x80000aa8]:csrrs a7, fflags, zero<br> [0x80000aac]:sd t6, 656(a5)<br>    |
