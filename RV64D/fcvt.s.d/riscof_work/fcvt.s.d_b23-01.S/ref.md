
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature.
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update signature
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800007f0')]      |
| SIG_REGION                | [('0x80002310', '0x800025e0', '90 dwords')]      |
| COV_LABELS                | fcvt.s.d_b23      |
| TEST_NAME                 | /home/riscv/Documents/FloatingResults/Arch13/fcvt.s.d/riscof_work/fcvt.s.d_b23-01.S/ref.S    |
| Total Number of coverpoints| 117     |
| Total Coverpoints Hit     | 112      |
| Total Signature Updates   | 45      |
| STAT1                     | 45      |
| STAT2                     | 0      |
| STAT3                     | 0     |
| STAT4                     | 0     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```


```

## Details of STAT4:

```

```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address and the data at that location in hexadecimal in the following format: 
  ```
  [Address]
  Data
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|            signature             |                                                                       coverpoints                                                                        |                                                                        code                                                                         |
|---:|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000001|- opcode : fcvt.s.d<br> - rs1 : f18<br> - rd : f13<br> - rs1 != rd<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 0  #nosat<br> |[0x800003b4]:fcvt.s.d fa3, fs2, dyn<br> [0x800003b8]:csrrs a7, fflags, zero<br> [0x800003bc]:fsd fa3, 0(a5)<br> [0x800003c0]:sd a7, 8(a5)<br>        |
|   2|[0x80002328]<br>0x0000000000000001|- rs1 : f15<br> - rd : f15<br> - rs1 == rd<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 4  #nosat<br>                         |[0x800003cc]:fcvt.s.d fa5, fa5, dyn<br> [0x800003d0]:csrrs a7, fflags, zero<br> [0x800003d4]:fsd fa5, 16(a5)<br> [0x800003d8]:sd a7, 24(a5)<br>      |
|   3|[0x80002338]<br>0x0000000000000001|- rs1 : f1<br> - rd : f4<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 3  #nosat<br>                                           |[0x800003e4]:fcvt.s.d ft4, ft1, dyn<br> [0x800003e8]:csrrs a7, fflags, zero<br> [0x800003ec]:fsd ft4, 32(a5)<br> [0x800003f0]:sd a7, 40(a5)<br>      |
|   4|[0x80002348]<br>0x0000000000000001|- rs1 : f6<br> - rd : f12<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 2  #nosat<br>                                          |[0x800003fc]:fcvt.s.d fa2, ft6, dyn<br> [0x80000400]:csrrs a7, fflags, zero<br> [0x80000404]:fsd fa2, 48(a5)<br> [0x80000408]:sd a7, 56(a5)<br>      |
|   5|[0x80002358]<br>0x0000000000000001|- rs1 : f11<br> - rd : f10<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 1  #nosat<br>                                         |[0x80000414]:fcvt.s.d fa0, fa1, dyn<br> [0x80000418]:csrrs a7, fflags, zero<br> [0x8000041c]:fsd fa0, 64(a5)<br> [0x80000420]:sd a7, 72(a5)<br>      |
|   6|[0x80002368]<br>0x0000000000000001|- rs1 : f12<br> - rd : f17<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000004 and rm_val == 0  #nosat<br>                                         |[0x8000042c]:fcvt.s.d fa7, fa2, dyn<br> [0x80000430]:csrrs a7, fflags, zero<br> [0x80000434]:fsd fa7, 80(a5)<br> [0x80000438]:sd a7, 88(a5)<br>      |
|   7|[0x80002378]<br>0x0000000000000001|- rs1 : f13<br> - rd : f18<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 4  #nosat<br>                                         |[0x80000444]:fcvt.s.d fs2, fa3, dyn<br> [0x80000448]:csrrs a7, fflags, zero<br> [0x8000044c]:fsd fs2, 96(a5)<br> [0x80000450]:sd a7, 104(a5)<br>     |
|   8|[0x80002388]<br>0x0000000000000001|- rs1 : f0<br> - rd : f23<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 3  #nosat<br>                                          |[0x8000045c]:fcvt.s.d fs7, ft0, dyn<br> [0x80000460]:csrrs a7, fflags, zero<br> [0x80000464]:fsd fs7, 112(a5)<br> [0x80000468]:sd a7, 120(a5)<br>    |
|   9|[0x80002398]<br>0x0000000000000001|- rs1 : f20<br> - rd : f5<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 2  #nosat<br>                                          |[0x80000474]:fcvt.s.d ft5, fs4, dyn<br> [0x80000478]:csrrs a7, fflags, zero<br> [0x8000047c]:fsd ft5, 128(a5)<br> [0x80000480]:sd a7, 136(a5)<br>    |
|  10|[0x800023a8]<br>0x0000000000000001|- rs1 : f22<br> - rd : f29<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 1  #nosat<br>                                         |[0x8000048c]:fcvt.s.d ft9, fs6, dyn<br> [0x80000490]:csrrs a7, fflags, zero<br> [0x80000494]:fsd ft9, 144(a5)<br> [0x80000498]:sd a7, 152(a5)<br>    |
|  11|[0x800023b8]<br>0x0000000000000001|- rs1 : f3<br> - rd : f16<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000003 and rm_val == 0  #nosat<br>                                          |[0x800004a4]:fcvt.s.d fa6, ft3, dyn<br> [0x800004a8]:csrrs a7, fflags, zero<br> [0x800004ac]:fsd fa6, 160(a5)<br> [0x800004b0]:sd a7, 168(a5)<br>    |
|  12|[0x800023c8]<br>0x0000000000000001|- rs1 : f17<br> - rd : f3<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 4  #nosat<br>                                          |[0x800004bc]:fcvt.s.d ft3, fa7, dyn<br> [0x800004c0]:csrrs a7, fflags, zero<br> [0x800004c4]:fsd ft3, 176(a5)<br> [0x800004c8]:sd a7, 184(a5)<br>    |
|  13|[0x800023d8]<br>0x0000000000000001|- rs1 : f2<br> - rd : f20<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 3  #nosat<br>                                          |[0x800004d4]:fcvt.s.d fs4, ft2, dyn<br> [0x800004d8]:csrrs a7, fflags, zero<br> [0x800004dc]:fsd fs4, 192(a5)<br> [0x800004e0]:sd a7, 200(a5)<br>    |
|  14|[0x800023e8]<br>0x0000000000000001|- rs1 : f26<br> - rd : f28<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 2  #nosat<br>                                         |[0x800004ec]:fcvt.s.d ft8, fs10, dyn<br> [0x800004f0]:csrrs a7, fflags, zero<br> [0x800004f4]:fsd ft8, 208(a5)<br> [0x800004f8]:sd a7, 216(a5)<br>   |
|  15|[0x800023f8]<br>0x0000000000000001|- rs1 : f8<br> - rd : f26<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 1  #nosat<br>                                          |[0x80000504]:fcvt.s.d fs10, fs0, dyn<br> [0x80000508]:csrrs a7, fflags, zero<br> [0x8000050c]:fsd fs10, 224(a5)<br> [0x80000510]:sd a7, 232(a5)<br>  |
|  16|[0x80002408]<br>0x0000000000000001|- rs1 : f24<br> - rd : f6<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000002 and rm_val == 0  #nosat<br>                                          |[0x8000051c]:fcvt.s.d ft6, fs8, dyn<br> [0x80000520]:csrrs a7, fflags, zero<br> [0x80000524]:fsd ft6, 240(a5)<br> [0x80000528]:sd a7, 248(a5)<br>    |
|  17|[0x80002418]<br>0x0000000000000001|- rs1 : f19<br> - rd : f24<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 4  #nosat<br>                                         |[0x80000534]:fcvt.s.d fs8, fs3, dyn<br> [0x80000538]:csrrs a7, fflags, zero<br> [0x8000053c]:fsd fs8, 256(a5)<br> [0x80000540]:sd a7, 264(a5)<br>    |
|  18|[0x80002428]<br>0x0000000000000001|- rs1 : f7<br> - rd : f30<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 3  #nosat<br>                                          |[0x8000054c]:fcvt.s.d ft10, ft7, dyn<br> [0x80000550]:csrrs a7, fflags, zero<br> [0x80000554]:fsd ft10, 272(a5)<br> [0x80000558]:sd a7, 280(a5)<br>  |
|  19|[0x80002438]<br>0x0000000000000001|- rs1 : f14<br> - rd : f9<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 2  #nosat<br>                                          |[0x80000564]:fcvt.s.d fs1, fa4, dyn<br> [0x80000568]:csrrs a7, fflags, zero<br> [0x8000056c]:fsd fs1, 288(a5)<br> [0x80000570]:sd a7, 296(a5)<br>    |
|  20|[0x80002448]<br>0x0000000000000001|- rs1 : f29<br> - rd : f1<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 1  #nosat<br>                                          |[0x8000057c]:fcvt.s.d ft1, ft9, dyn<br> [0x80000580]:csrrs a7, fflags, zero<br> [0x80000584]:fsd ft1, 304(a5)<br> [0x80000588]:sd a7, 312(a5)<br>    |
|  21|[0x80002458]<br>0x0000000000000001|- rs1 : f9<br> - rd : f27<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000001 and rm_val == 0  #nosat<br>                                          |[0x80000594]:fcvt.s.d fs11, fs1, dyn<br> [0x80000598]:csrrs a7, fflags, zero<br> [0x8000059c]:fsd fs11, 320(a5)<br> [0x800005a0]:sd a7, 328(a5)<br>  |
|  22|[0x80002468]<br>0x0000000000000001|- rs1 : f27<br> - rd : f21<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 4  #nosat<br>                                         |[0x800005ac]:fcvt.s.d fs5, fs11, dyn<br> [0x800005b0]:csrrs a7, fflags, zero<br> [0x800005b4]:fsd fs5, 336(a5)<br> [0x800005b8]:sd a7, 344(a5)<br>   |
|  23|[0x80002478]<br>0x0000000000000001|- rs1 : f10<br> - rd : f8<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 3  #nosat<br>                                          |[0x800005c4]:fcvt.s.d fs0, fa0, dyn<br> [0x800005c8]:csrrs a7, fflags, zero<br> [0x800005cc]:fsd fs0, 352(a5)<br> [0x800005d0]:sd a7, 360(a5)<br>    |
|  24|[0x80002488]<br>0x0000000000000001|- rs1 : f21<br> - rd : f7<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 2  #nosat<br>                                          |[0x800005dc]:fcvt.s.d ft7, fs5, dyn<br> [0x800005e0]:csrrs a7, fflags, zero<br> [0x800005e4]:fsd ft7, 368(a5)<br> [0x800005e8]:sd a7, 376(a5)<br>    |
|  25|[0x80002498]<br>0x0000000000000001|- rs1 : f28<br> - rd : f31<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 1  #nosat<br>                                         |[0x800005f4]:fcvt.s.d ft11, ft8, dyn<br> [0x800005f8]:csrrs a7, fflags, zero<br> [0x800005fc]:fsd ft11, 384(a5)<br> [0x80000600]:sd a7, 392(a5)<br>  |
|  26|[0x800024a8]<br>0x0000000000000001|- rs1 : f5<br> - rd : f2<br> - fs1 == 0 and fe1 == 0x43e and fm1 == 0x0000000000000 and rm_val == 0  #nosat<br>                                           |[0x8000060c]:fcvt.s.d ft2, ft5, dyn<br> [0x80000610]:csrrs a7, fflags, zero<br> [0x80000614]:fsd ft2, 400(a5)<br> [0x80000618]:sd a7, 408(a5)<br>    |
|  27|[0x800024b8]<br>0x0000000000000001|- rs1 : f30<br> - rd : f0<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 4  #nosat<br>                                          |[0x80000624]:fcvt.s.d ft0, ft10, dyn<br> [0x80000628]:csrrs a7, fflags, zero<br> [0x8000062c]:fsd ft0, 416(a5)<br> [0x80000630]:sd a7, 424(a5)<br>   |
|  28|[0x800024c8]<br>0x0000000000000001|- rs1 : f25<br> - rd : f19<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 3  #nosat<br>                                         |[0x8000063c]:fcvt.s.d fs3, fs9, dyn<br> [0x80000640]:csrrs a7, fflags, zero<br> [0x80000644]:fsd fs3, 432(a5)<br> [0x80000648]:sd a7, 440(a5)<br>    |
|  29|[0x800024d8]<br>0x0000000000000001|- rs1 : f4<br> - rd : f11<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 2  #nosat<br>                                          |[0x80000654]:fcvt.s.d fa1, ft4, dyn<br> [0x80000658]:csrrs a7, fflags, zero<br> [0x8000065c]:fsd fa1, 448(a5)<br> [0x80000660]:sd a7, 456(a5)<br>    |
|  30|[0x800024e8]<br>0x0000000000000001|- rs1 : f31<br> - rd : f22<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 1  #nosat<br>                                         |[0x8000066c]:fcvt.s.d fs6, ft11, dyn<br> [0x80000670]:csrrs a7, fflags, zero<br> [0x80000674]:fsd fs6, 464(a5)<br> [0x80000678]:sd a7, 472(a5)<br>   |
|  31|[0x800024f8]<br>0x0000000000000001|- rs1 : f23<br> - rd : f25<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xfffffffffffff and rm_val == 0  #nosat<br>                                         |[0x80000684]:fcvt.s.d fs9, fs7, dyn<br> [0x80000688]:csrrs a7, fflags, zero<br> [0x8000068c]:fsd fs9, 480(a5)<br> [0x80000690]:sd a7, 488(a5)<br>    |
|  32|[0x80002508]<br>0x0000000000000001|- rs1 : f16<br> - rd : f14<br> - fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 4  #nosat<br>                                         |[0x8000069c]:fcvt.s.d fa4, fa6, dyn<br> [0x800006a0]:csrrs a7, fflags, zero<br> [0x800006a4]:fsd fa4, 496(a5)<br> [0x800006a8]:sd a7, 504(a5)<br>    |
|  33|[0x80002518]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 3  #nosat<br>                                                                        |[0x800006b4]:fcvt.s.d ft11, ft10, dyn<br> [0x800006b8]:csrrs a7, fflags, zero<br> [0x800006bc]:fsd ft11, 512(a5)<br> [0x800006c0]:sd a7, 520(a5)<br> |
|  34|[0x80002528]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 2  #nosat<br>                                                                        |[0x800006cc]:fcvt.s.d ft11, ft10, dyn<br> [0x800006d0]:csrrs a7, fflags, zero<br> [0x800006d4]:fsd ft11, 528(a5)<br> [0x800006d8]:sd a7, 536(a5)<br> |
|  35|[0x80002538]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 1  #nosat<br>                                                                        |[0x800006e4]:fcvt.s.d ft11, ft10, dyn<br> [0x800006e8]:csrrs a7, fflags, zero<br> [0x800006ec]:fsd ft11, 544(a5)<br> [0x800006f0]:sd a7, 552(a5)<br> |
|  36|[0x80002548]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffe and rm_val == 0  #nosat<br>                                                                        |[0x800006fc]:fcvt.s.d ft11, ft10, dyn<br> [0x80000700]:csrrs a7, fflags, zero<br> [0x80000704]:fsd ft11, 560(a5)<br> [0x80000708]:sd a7, 568(a5)<br> |
|  37|[0x80002558]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 4  #nosat<br>                                                                        |[0x80000714]:fcvt.s.d ft11, ft10, dyn<br> [0x80000718]:csrrs a7, fflags, zero<br> [0x8000071c]:fsd ft11, 576(a5)<br> [0x80000720]:sd a7, 584(a5)<br> |
|  38|[0x80002568]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 3  #nosat<br>                                                                        |[0x8000072c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000730]:csrrs a7, fflags, zero<br> [0x80000734]:fsd ft11, 592(a5)<br> [0x80000738]:sd a7, 600(a5)<br> |
|  39|[0x80002578]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 2  #nosat<br>                                                                        |[0x80000744]:fcvt.s.d ft11, ft10, dyn<br> [0x80000748]:csrrs a7, fflags, zero<br> [0x8000074c]:fsd ft11, 608(a5)<br> [0x80000750]:sd a7, 616(a5)<br> |
|  40|[0x80002588]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 1  #nosat<br>                                                                        |[0x8000075c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000760]:csrrs a7, fflags, zero<br> [0x80000764]:fsd ft11, 624(a5)<br> [0x80000768]:sd a7, 632(a5)<br> |
|  41|[0x80002598]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffd and rm_val == 0  #nosat<br>                                                                        |[0x80000774]:fcvt.s.d ft11, ft10, dyn<br> [0x80000778]:csrrs a7, fflags, zero<br> [0x8000077c]:fsd ft11, 640(a5)<br> [0x80000780]:sd a7, 648(a5)<br> |
|  42|[0x800025a8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 4  #nosat<br>                                                                        |[0x8000078c]:fcvt.s.d ft11, ft10, dyn<br> [0x80000790]:csrrs a7, fflags, zero<br> [0x80000794]:fsd ft11, 656(a5)<br> [0x80000798]:sd a7, 664(a5)<br> |
|  43|[0x800025b8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 3  #nosat<br>                                                                        |[0x800007a4]:fcvt.s.d ft11, ft10, dyn<br> [0x800007a8]:csrrs a7, fflags, zero<br> [0x800007ac]:fsd ft11, 672(a5)<br> [0x800007b0]:sd a7, 680(a5)<br> |
|  44|[0x800025c8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 2  #nosat<br>                                                                        |[0x800007bc]:fcvt.s.d ft11, ft10, dyn<br> [0x800007c0]:csrrs a7, fflags, zero<br> [0x800007c4]:fsd ft11, 688(a5)<br> [0x800007c8]:sd a7, 696(a5)<br> |
|  45|[0x800025d8]<br>0x0000000000000001|- fs1 == 0 and fe1 == 0x43d and fm1 == 0xffffffffffffc and rm_val == 1  #nosat<br>                                                                        |[0x800007d4]:fcvt.s.d ft11, ft10, dyn<br> [0x800007d8]:csrrs a7, fflags, zero<br> [0x800007dc]:fsd ft11, 704(a5)<br> [0x800007e0]:sd a7, 712(a5)<br> |
