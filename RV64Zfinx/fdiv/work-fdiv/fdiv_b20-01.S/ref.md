
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80001120')]      |
| SIG_REGION                | [('0x80003810', '0x80003ed0', '216 dwords')]      |
| COV_LABELS                | fdiv_b20      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fdiv/work-fdiv/fdiv_b20-01.S/ref.S    |
| Total Number of coverpoints| 204     |
| Total Coverpoints Hit     | 204      |
| Total Signature Updates   | 214      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 106     |
| STAT4                     | 107     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000110c]:fdiv.s t6, t5, t4, dyn
      [0x80001110]:csrrs a2, fcsr, zero
      [0x80001114]:sd t6, 1296(fp)
      [0x80001118]:sd a2, 1304(fp)
      [0x8000111c]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80003eb8 Data: 0xFFFFFFFFEC3FBFFF
 -- Redundant Coverpoints hit by the op
      - mnemonic : fdiv.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfe and fm1 == 0x3af6ff and fs2 == 1 and fe2 == 0xa4 and fm2 == 0x799c89 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat






```

## Details of STAT3

```
[0x800003bc]:fdiv.s t6, t5, t6, dyn
[0x800003c0]:csrrs tp, fcsr, zero
[0x800003c4]:sd t6, 0(ra)
[0x800003c8]:sd tp, 8(ra)
[0x800003cc]:ld t6, 16(gp)
[0x800003d0]:ld t4, 24(gp)
[0x800003d4]:addi sp, zero, 0
[0x800003d8]:csrrw zero, fcsr, sp
[0x800003dc]:fdiv.s t5, t6, t4, dyn

[0x800003dc]:fdiv.s t5, t6, t4, dyn
[0x800003e0]:csrrs tp, fcsr, zero
[0x800003e4]:sd t5, 16(ra)
[0x800003e8]:sd tp, 24(ra)
[0x800003ec]:ld t3, 32(gp)
[0x800003f0]:ld t3, 40(gp)
[0x800003f4]:addi sp, zero, 0
[0x800003f8]:csrrw zero, fcsr, sp
[0x800003fc]:fdiv.s t3, t3, t3, dyn

[0x800003fc]:fdiv.s t3, t3, t3, dyn
[0x80000400]:csrrs tp, fcsr, zero
[0x80000404]:sd t3, 32(ra)
[0x80000408]:sd tp, 40(ra)
[0x8000040c]:ld s11, 48(gp)
[0x80000410]:ld s11, 56(gp)
[0x80000414]:addi sp, zero, 0
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fdiv.s t4, s11, s11, dyn

[0x8000041c]:fdiv.s t4, s11, s11, dyn
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:sd t4, 48(ra)
[0x80000428]:sd tp, 56(ra)
[0x8000042c]:ld s10, 64(gp)
[0x80000430]:ld t5, 72(gp)
[0x80000434]:addi sp, zero, 0
[0x80000438]:csrrw zero, fcsr, sp
[0x8000043c]:fdiv.s s10, s10, t5, dyn

[0x8000043c]:fdiv.s s10, s10, t5, dyn
[0x80000440]:csrrs tp, fcsr, zero
[0x80000444]:sd s10, 64(ra)
[0x80000448]:sd tp, 72(ra)
[0x8000044c]:ld t4, 80(gp)
[0x80000450]:ld s10, 88(gp)
[0x80000454]:addi sp, zero, 0
[0x80000458]:csrrw zero, fcsr, sp
[0x8000045c]:fdiv.s s11, t4, s10, dyn

[0x8000045c]:fdiv.s s11, t4, s10, dyn
[0x80000460]:csrrs tp, fcsr, zero
[0x80000464]:sd s11, 80(ra)
[0x80000468]:sd tp, 88(ra)
[0x8000046c]:ld s8, 96(gp)
[0x80000470]:ld s7, 104(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fdiv.s s9, s8, s7, dyn

[0x8000047c]:fdiv.s s9, s8, s7, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s9, 96(ra)
[0x80000488]:sd tp, 104(ra)
[0x8000048c]:ld s7, 112(gp)
[0x80000490]:ld s9, 120(gp)
[0x80000494]:addi sp, zero, 0
[0x80000498]:csrrw zero, fcsr, sp
[0x8000049c]:fdiv.s s8, s7, s9, dyn

[0x8000049c]:fdiv.s s8, s7, s9, dyn
[0x800004a0]:csrrs tp, fcsr, zero
[0x800004a4]:sd s8, 112(ra)
[0x800004a8]:sd tp, 120(ra)
[0x800004ac]:ld s9, 128(gp)
[0x800004b0]:ld s8, 136(gp)
[0x800004b4]:addi sp, zero, 0
[0x800004b8]:csrrw zero, fcsr, sp
[0x800004bc]:fdiv.s s7, s9, s8, dyn

[0x800004bc]:fdiv.s s7, s9, s8, dyn
[0x800004c0]:csrrs tp, fcsr, zero
[0x800004c4]:sd s7, 128(ra)
[0x800004c8]:sd tp, 136(ra)
[0x800004cc]:ld s5, 144(gp)
[0x800004d0]:ld s4, 152(gp)
[0x800004d4]:addi sp, zero, 0
[0x800004d8]:csrrw zero, fcsr, sp
[0x800004dc]:fdiv.s s6, s5, s4, dyn

[0x800004dc]:fdiv.s s6, s5, s4, dyn
[0x800004e0]:csrrs tp, fcsr, zero
[0x800004e4]:sd s6, 144(ra)
[0x800004e8]:sd tp, 152(ra)
[0x800004ec]:ld s4, 160(gp)
[0x800004f0]:ld s6, 168(gp)
[0x800004f4]:addi sp, zero, 0
[0x800004f8]:csrrw zero, fcsr, sp
[0x800004fc]:fdiv.s s5, s4, s6, dyn

[0x800004fc]:fdiv.s s5, s4, s6, dyn
[0x80000500]:csrrs tp, fcsr, zero
[0x80000504]:sd s5, 160(ra)
[0x80000508]:sd tp, 168(ra)
[0x8000050c]:ld s6, 176(gp)
[0x80000510]:ld s5, 184(gp)
[0x80000514]:addi sp, zero, 0
[0x80000518]:csrrw zero, fcsr, sp
[0x8000051c]:fdiv.s s4, s6, s5, dyn

[0x8000051c]:fdiv.s s4, s6, s5, dyn
[0x80000520]:csrrs tp, fcsr, zero
[0x80000524]:sd s4, 176(ra)
[0x80000528]:sd tp, 184(ra)
[0x8000052c]:ld s2, 192(gp)
[0x80000530]:ld a7, 200(gp)
[0x80000534]:addi sp, zero, 0
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fdiv.s s3, s2, a7, dyn

[0x8000053c]:fdiv.s s3, s2, a7, dyn
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:sd s3, 192(ra)
[0x80000548]:sd tp, 200(ra)
[0x8000054c]:ld a7, 208(gp)
[0x80000550]:ld s3, 216(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fdiv.s s2, a7, s3, dyn

[0x8000055c]:fdiv.s s2, a7, s3, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd s2, 208(ra)
[0x80000568]:sd tp, 216(ra)
[0x8000056c]:ld s3, 224(gp)
[0x80000570]:ld s2, 232(gp)
[0x80000574]:addi sp, zero, 0
[0x80000578]:csrrw zero, fcsr, sp
[0x8000057c]:fdiv.s a7, s3, s2, dyn

[0x8000057c]:fdiv.s a7, s3, s2, dyn
[0x80000580]:csrrs tp, fcsr, zero
[0x80000584]:sd a7, 224(ra)
[0x80000588]:sd tp, 232(ra)
[0x8000058c]:ld a5, 240(gp)
[0x80000590]:ld a4, 248(gp)
[0x80000594]:addi sp, zero, 0
[0x80000598]:csrrw zero, fcsr, sp
[0x8000059c]:fdiv.s a6, a5, a4, dyn

[0x8000059c]:fdiv.s a6, a5, a4, dyn
[0x800005a0]:csrrs tp, fcsr, zero
[0x800005a4]:sd a6, 240(ra)
[0x800005a8]:sd tp, 248(ra)
[0x800005ac]:ld a4, 256(gp)
[0x800005b0]:ld a6, 264(gp)
[0x800005b4]:addi sp, zero, 0
[0x800005b8]:csrrw zero, fcsr, sp
[0x800005bc]:fdiv.s a5, a4, a6, dyn

[0x800005bc]:fdiv.s a5, a4, a6, dyn
[0x800005c0]:csrrs tp, fcsr, zero
[0x800005c4]:sd a5, 256(ra)
[0x800005c8]:sd tp, 264(ra)
[0x800005cc]:ld a6, 272(gp)
[0x800005d0]:ld a5, 280(gp)
[0x800005d4]:addi sp, zero, 0
[0x800005d8]:csrrw zero, fcsr, sp
[0x800005dc]:fdiv.s a4, a6, a5, dyn

[0x800005dc]:fdiv.s a4, a6, a5, dyn
[0x800005e0]:csrrs tp, fcsr, zero
[0x800005e4]:sd a4, 272(ra)
[0x800005e8]:sd tp, 280(ra)
[0x800005ec]:ld a2, 288(gp)
[0x800005f0]:ld a1, 296(gp)
[0x800005f4]:addi sp, zero, 0
[0x800005f8]:csrrw zero, fcsr, sp
[0x800005fc]:fdiv.s a3, a2, a1, dyn

[0x800005fc]:fdiv.s a3, a2, a1, dyn
[0x80000600]:csrrs tp, fcsr, zero
[0x80000604]:sd a3, 288(ra)
[0x80000608]:sd tp, 296(ra)
[0x8000060c]:ld a1, 304(gp)
[0x80000610]:ld a3, 312(gp)
[0x80000614]:addi sp, zero, 0
[0x80000618]:csrrw zero, fcsr, sp
[0x8000061c]:fdiv.s a2, a1, a3, dyn

[0x8000061c]:fdiv.s a2, a1, a3, dyn
[0x80000620]:csrrs tp, fcsr, zero
[0x80000624]:sd a2, 304(ra)
[0x80000628]:sd tp, 312(ra)
[0x8000062c]:ld a3, 320(gp)
[0x80000630]:ld a2, 328(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fdiv.s a1, a3, a2, dyn

[0x8000063c]:fdiv.s a1, a3, a2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd a1, 320(ra)
[0x80000648]:sd tp, 328(ra)
[0x8000064c]:ld s1, 336(gp)
[0x80000650]:ld fp, 344(gp)
[0x80000654]:addi sp, zero, 0
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fdiv.s a0, s1, fp, dyn

[0x8000065c]:fdiv.s a0, s1, fp, dyn
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:sd a0, 336(ra)
[0x80000668]:sd tp, 344(ra)
[0x8000066c]:auipc a1, 3
[0x80000670]:addi a1, a1, 2820
[0x80000674]:ld fp, 0(a1)
[0x80000678]:ld a0, 8(a1)
[0x8000067c]:addi sp, zero, 0
[0x80000680]:csrrw zero, fcsr, sp
[0x80000684]:fdiv.s s1, fp, a0, dyn

[0x80000684]:fdiv.s s1, fp, a0, dyn
[0x80000688]:csrrs a2, fcsr, zero
[0x8000068c]:sd s1, 352(ra)
[0x80000690]:sd a2, 360(ra)
[0x80000694]:ld a0, 16(a1)
[0x80000698]:ld s1, 24(a1)
[0x8000069c]:addi sp, zero, 0
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fdiv.s fp, a0, s1, dyn

[0x800006a4]:fdiv.s fp, a0, s1, dyn
[0x800006a8]:csrrs a2, fcsr, zero
[0x800006ac]:sd fp, 368(ra)
[0x800006b0]:sd a2, 376(ra)
[0x800006b4]:ld t1, 32(a1)
[0x800006b8]:ld t0, 40(a1)
[0x800006bc]:addi sp, zero, 0
[0x800006c0]:csrrw zero, fcsr, sp
[0x800006c4]:fdiv.s t2, t1, t0, dyn

[0x800006c4]:fdiv.s t2, t1, t0, dyn
[0x800006c8]:csrrs a2, fcsr, zero
[0x800006cc]:sd t2, 384(ra)
[0x800006d0]:sd a2, 392(ra)
[0x800006d4]:auipc fp, 3
[0x800006d8]:addi fp, fp, 724
[0x800006dc]:ld t0, 48(a1)
[0x800006e0]:ld t2, 56(a1)
[0x800006e4]:addi s1, zero, 0
[0x800006e8]:csrrw zero, fcsr, s1
[0x800006ec]:fdiv.s t1, t0, t2, dyn

[0x800006ec]:fdiv.s t1, t0, t2, dyn
[0x800006f0]:csrrs a2, fcsr, zero
[0x800006f4]:sd t1, 0(fp)
[0x800006f8]:sd a2, 8(fp)
[0x800006fc]:ld t2, 64(a1)
[0x80000700]:ld t1, 72(a1)
[0x80000704]:addi s1, zero, 0
[0x80000708]:csrrw zero, fcsr, s1
[0x8000070c]:fdiv.s t0, t2, t1, dyn

[0x8000070c]:fdiv.s t0, t2, t1, dyn
[0x80000710]:csrrs a2, fcsr, zero
[0x80000714]:sd t0, 16(fp)
[0x80000718]:sd a2, 24(fp)
[0x8000071c]:ld gp, 80(a1)
[0x80000720]:ld sp, 88(a1)
[0x80000724]:addi s1, zero, 0
[0x80000728]:csrrw zero, fcsr, s1
[0x8000072c]:fdiv.s tp, gp, sp, dyn

[0x8000072c]:fdiv.s tp, gp, sp, dyn
[0x80000730]:csrrs a2, fcsr, zero
[0x80000734]:sd tp, 32(fp)
[0x80000738]:sd a2, 40(fp)
[0x8000073c]:ld sp, 96(a1)
[0x80000740]:ld tp, 104(a1)
[0x80000744]:addi s1, zero, 0
[0x80000748]:csrrw zero, fcsr, s1
[0x8000074c]:fdiv.s gp, sp, tp, dyn

[0x8000074c]:fdiv.s gp, sp, tp, dyn
[0x80000750]:csrrs a2, fcsr, zero
[0x80000754]:sd gp, 48(fp)
[0x80000758]:sd a2, 56(fp)
[0x8000075c]:ld tp, 112(a1)
[0x80000760]:ld gp, 120(a1)
[0x80000764]:addi s1, zero, 0
[0x80000768]:csrrw zero, fcsr, s1
[0x8000076c]:fdiv.s sp, tp, gp, dyn

[0x8000076c]:fdiv.s sp, tp, gp, dyn
[0x80000770]:csrrs a2, fcsr, zero
[0x80000774]:sd sp, 64(fp)
[0x80000778]:sd a2, 72(fp)
[0x8000077c]:ld ra, 128(a1)
[0x80000780]:ld t5, 136(a1)
[0x80000784]:addi s1, zero, 0
[0x80000788]:csrrw zero, fcsr, s1
[0x8000078c]:fdiv.s t6, ra, t5, dyn

[0x8000078c]:fdiv.s t6, ra, t5, dyn
[0x80000790]:csrrs a2, fcsr, zero
[0x80000794]:sd t6, 80(fp)
[0x80000798]:sd a2, 88(fp)
[0x8000079c]:ld zero, 144(a1)
[0x800007a0]:ld t5, 152(a1)
[0x800007a4]:addi s1, zero, 0
[0x800007a8]:csrrw zero, fcsr, s1
[0x800007ac]:fdiv.s t6, zero, t5, dyn

[0x800007ac]:fdiv.s t6, zero, t5, dyn
[0x800007b0]:csrrs a2, fcsr, zero
[0x800007b4]:sd t6, 96(fp)
[0x800007b8]:sd a2, 104(fp)
[0x800007bc]:ld t5, 160(a1)
[0x800007c0]:ld ra, 168(a1)
[0x800007c4]:addi s1, zero, 0
[0x800007c8]:csrrw zero, fcsr, s1
[0x800007cc]:fdiv.s t6, t5, ra, dyn

[0x800007cc]:fdiv.s t6, t5, ra, dyn
[0x800007d0]:csrrs a2, fcsr, zero
[0x800007d4]:sd t6, 112(fp)
[0x800007d8]:sd a2, 120(fp)
[0x800007dc]:ld t5, 176(a1)
[0x800007e0]:ld zero, 184(a1)
[0x800007e4]:addi s1, zero, 0
[0x800007e8]:csrrw zero, fcsr, s1
[0x800007ec]:fdiv.s t6, t5, zero, dyn

[0x800007ec]:fdiv.s t6, t5, zero, dyn
[0x800007f0]:csrrs a2, fcsr, zero
[0x800007f4]:sd t6, 128(fp)
[0x800007f8]:sd a2, 136(fp)
[0x800007fc]:ld t6, 192(a1)
[0x80000800]:ld t5, 200(a1)
[0x80000804]:addi s1, zero, 0
[0x80000808]:csrrw zero, fcsr, s1
[0x8000080c]:fdiv.s ra, t6, t5, dyn

[0x8000080c]:fdiv.s ra, t6, t5, dyn
[0x80000810]:csrrs a2, fcsr, zero
[0x80000814]:sd ra, 144(fp)
[0x80000818]:sd a2, 152(fp)
[0x8000081c]:ld t6, 208(a1)
[0x80000820]:ld t5, 216(a1)
[0x80000824]:addi s1, zero, 0
[0x80000828]:csrrw zero, fcsr, s1
[0x8000082c]:fdiv.s zero, t6, t5, dyn

[0x8000082c]:fdiv.s zero, t6, t5, dyn
[0x80000830]:csrrs a2, fcsr, zero
[0x80000834]:sd zero, 160(fp)
[0x80000838]:sd a2, 168(fp)
[0x8000083c]:ld t5, 224(a1)
[0x80000840]:ld t4, 232(a1)
[0x80000844]:addi s1, zero, 0
[0x80000848]:csrrw zero, fcsr, s1
[0x8000084c]:fdiv.s t6, t5, t4, dyn

[0x8000084c]:fdiv.s t6, t5, t4, dyn
[0x80000850]:csrrs a2, fcsr, zero
[0x80000854]:sd t6, 176(fp)
[0x80000858]:sd a2, 184(fp)
[0x8000085c]:ld t5, 240(a1)
[0x80000860]:ld t4, 248(a1)
[0x80000864]:addi s1, zero, 0
[0x80000868]:csrrw zero, fcsr, s1
[0x8000086c]:fdiv.s t6, t5, t4, dyn

[0x8000086c]:fdiv.s t6, t5, t4, dyn
[0x80000870]:csrrs a2, fcsr, zero
[0x80000874]:sd t6, 192(fp)
[0x80000878]:sd a2, 200(fp)
[0x8000087c]:ld t5, 256(a1)
[0x80000880]:ld t4, 264(a1)
[0x80000884]:addi s1, zero, 0
[0x80000888]:csrrw zero, fcsr, s1
[0x8000088c]:fdiv.s t6, t5, t4, dyn

[0x8000088c]:fdiv.s t6, t5, t4, dyn
[0x80000890]:csrrs a2, fcsr, zero
[0x80000894]:sd t6, 208(fp)
[0x80000898]:sd a2, 216(fp)
[0x8000089c]:ld t5, 272(a1)
[0x800008a0]:ld t4, 280(a1)
[0x800008a4]:addi s1, zero, 0
[0x800008a8]:csrrw zero, fcsr, s1
[0x800008ac]:fdiv.s t6, t5, t4, dyn

[0x800008ac]:fdiv.s t6, t5, t4, dyn
[0x800008b0]:csrrs a2, fcsr, zero
[0x800008b4]:sd t6, 224(fp)
[0x800008b8]:sd a2, 232(fp)
[0x800008bc]:ld t5, 288(a1)
[0x800008c0]:ld t4, 296(a1)
[0x800008c4]:addi s1, zero, 0
[0x800008c8]:csrrw zero, fcsr, s1
[0x800008cc]:fdiv.s t6, t5, t4, dyn

[0x800008cc]:fdiv.s t6, t5, t4, dyn
[0x800008d0]:csrrs a2, fcsr, zero
[0x800008d4]:sd t6, 240(fp)
[0x800008d8]:sd a2, 248(fp)
[0x800008dc]:ld t5, 304(a1)
[0x800008e0]:ld t4, 312(a1)
[0x800008e4]:addi s1, zero, 0
[0x800008e8]:csrrw zero, fcsr, s1
[0x800008ec]:fdiv.s t6, t5, t4, dyn

[0x800008ec]:fdiv.s t6, t5, t4, dyn
[0x800008f0]:csrrs a2, fcsr, zero
[0x800008f4]:sd t6, 256(fp)
[0x800008f8]:sd a2, 264(fp)
[0x800008fc]:ld t5, 320(a1)
[0x80000900]:ld t4, 328(a1)
[0x80000904]:addi s1, zero, 0
[0x80000908]:csrrw zero, fcsr, s1
[0x8000090c]:fdiv.s t6, t5, t4, dyn

[0x8000090c]:fdiv.s t6, t5, t4, dyn
[0x80000910]:csrrs a2, fcsr, zero
[0x80000914]:sd t6, 272(fp)
[0x80000918]:sd a2, 280(fp)
[0x8000091c]:ld t5, 336(a1)
[0x80000920]:ld t4, 344(a1)
[0x80000924]:addi s1, zero, 0
[0x80000928]:csrrw zero, fcsr, s1
[0x8000092c]:fdiv.s t6, t5, t4, dyn

[0x8000092c]:fdiv.s t6, t5, t4, dyn
[0x80000930]:csrrs a2, fcsr, zero
[0x80000934]:sd t6, 288(fp)
[0x80000938]:sd a2, 296(fp)
[0x8000093c]:ld t5, 352(a1)
[0x80000940]:ld t4, 360(a1)
[0x80000944]:addi s1, zero, 0
[0x80000948]:csrrw zero, fcsr, s1
[0x8000094c]:fdiv.s t6, t5, t4, dyn

[0x8000094c]:fdiv.s t6, t5, t4, dyn
[0x80000950]:csrrs a2, fcsr, zero
[0x80000954]:sd t6, 304(fp)
[0x80000958]:sd a2, 312(fp)
[0x8000095c]:ld t5, 368(a1)
[0x80000960]:ld t4, 376(a1)
[0x80000964]:addi s1, zero, 0
[0x80000968]:csrrw zero, fcsr, s1
[0x8000096c]:fdiv.s t6, t5, t4, dyn

[0x8000096c]:fdiv.s t6, t5, t4, dyn
[0x80000970]:csrrs a2, fcsr, zero
[0x80000974]:sd t6, 320(fp)
[0x80000978]:sd a2, 328(fp)
[0x8000097c]:ld t5, 384(a1)
[0x80000980]:ld t4, 392(a1)
[0x80000984]:addi s1, zero, 0
[0x80000988]:csrrw zero, fcsr, s1
[0x8000098c]:fdiv.s t6, t5, t4, dyn

[0x8000098c]:fdiv.s t6, t5, t4, dyn
[0x80000990]:csrrs a2, fcsr, zero
[0x80000994]:sd t6, 336(fp)
[0x80000998]:sd a2, 344(fp)
[0x8000099c]:ld t5, 400(a1)
[0x800009a0]:ld t4, 408(a1)
[0x800009a4]:addi s1, zero, 0
[0x800009a8]:csrrw zero, fcsr, s1
[0x800009ac]:fdiv.s t6, t5, t4, dyn

[0x800009ac]:fdiv.s t6, t5, t4, dyn
[0x800009b0]:csrrs a2, fcsr, zero
[0x800009b4]:sd t6, 352(fp)
[0x800009b8]:sd a2, 360(fp)
[0x800009bc]:ld t5, 416(a1)
[0x800009c0]:ld t4, 424(a1)
[0x800009c4]:addi s1, zero, 0
[0x800009c8]:csrrw zero, fcsr, s1
[0x800009cc]:fdiv.s t6, t5, t4, dyn

[0x800009cc]:fdiv.s t6, t5, t4, dyn
[0x800009d0]:csrrs a2, fcsr, zero
[0x800009d4]:sd t6, 368(fp)
[0x800009d8]:sd a2, 376(fp)
[0x800009dc]:ld t5, 432(a1)
[0x800009e0]:ld t4, 440(a1)
[0x800009e4]:addi s1, zero, 0
[0x800009e8]:csrrw zero, fcsr, s1
[0x800009ec]:fdiv.s t6, t5, t4, dyn

[0x800009ec]:fdiv.s t6, t5, t4, dyn
[0x800009f0]:csrrs a2, fcsr, zero
[0x800009f4]:sd t6, 384(fp)
[0x800009f8]:sd a2, 392(fp)
[0x800009fc]:ld t5, 448(a1)
[0x80000a00]:ld t4, 456(a1)
[0x80000a04]:addi s1, zero, 0
[0x80000a08]:csrrw zero, fcsr, s1
[0x80000a0c]:fdiv.s t6, t5, t4, dyn

[0x80000a0c]:fdiv.s t6, t5, t4, dyn
[0x80000a10]:csrrs a2, fcsr, zero
[0x80000a14]:sd t6, 400(fp)
[0x80000a18]:sd a2, 408(fp)
[0x80000a1c]:ld t5, 464(a1)
[0x80000a20]:ld t4, 472(a1)
[0x80000a24]:addi s1, zero, 0
[0x80000a28]:csrrw zero, fcsr, s1
[0x80000a2c]:fdiv.s t6, t5, t4, dyn

[0x80000a2c]:fdiv.s t6, t5, t4, dyn
[0x80000a30]:csrrs a2, fcsr, zero
[0x80000a34]:sd t6, 416(fp)
[0x80000a38]:sd a2, 424(fp)
[0x80000a3c]:ld t5, 480(a1)
[0x80000a40]:ld t4, 488(a1)
[0x80000a44]:addi s1, zero, 0
[0x80000a48]:csrrw zero, fcsr, s1
[0x80000a4c]:fdiv.s t6, t5, t4, dyn

[0x80000a4c]:fdiv.s t6, t5, t4, dyn
[0x80000a50]:csrrs a2, fcsr, zero
[0x80000a54]:sd t6, 432(fp)
[0x80000a58]:sd a2, 440(fp)
[0x80000a5c]:ld t5, 496(a1)
[0x80000a60]:ld t4, 504(a1)
[0x80000a64]:addi s1, zero, 0
[0x80000a68]:csrrw zero, fcsr, s1
[0x80000a6c]:fdiv.s t6, t5, t4, dyn

[0x80000a6c]:fdiv.s t6, t5, t4, dyn
[0x80000a70]:csrrs a2, fcsr, zero
[0x80000a74]:sd t6, 448(fp)
[0x80000a78]:sd a2, 456(fp)
[0x80000a7c]:ld t5, 512(a1)
[0x80000a80]:ld t4, 520(a1)
[0x80000a84]:addi s1, zero, 0
[0x80000a88]:csrrw zero, fcsr, s1
[0x80000a8c]:fdiv.s t6, t5, t4, dyn

[0x80000a8c]:fdiv.s t6, t5, t4, dyn
[0x80000a90]:csrrs a2, fcsr, zero
[0x80000a94]:sd t6, 464(fp)
[0x80000a98]:sd a2, 472(fp)
[0x80000a9c]:ld t5, 528(a1)
[0x80000aa0]:ld t4, 536(a1)
[0x80000aa4]:addi s1, zero, 0
[0x80000aa8]:csrrw zero, fcsr, s1
[0x80000aac]:fdiv.s t6, t5, t4, dyn

[0x80000aac]:fdiv.s t6, t5, t4, dyn
[0x80000ab0]:csrrs a2, fcsr, zero
[0x80000ab4]:sd t6, 480(fp)
[0x80000ab8]:sd a2, 488(fp)
[0x80000abc]:ld t5, 544(a1)
[0x80000ac0]:ld t4, 552(a1)
[0x80000ac4]:addi s1, zero, 0
[0x80000ac8]:csrrw zero, fcsr, s1
[0x80000acc]:fdiv.s t6, t5, t4, dyn

[0x80000acc]:fdiv.s t6, t5, t4, dyn
[0x80000ad0]:csrrs a2, fcsr, zero
[0x80000ad4]:sd t6, 496(fp)
[0x80000ad8]:sd a2, 504(fp)
[0x80000adc]:ld t5, 560(a1)
[0x80000ae0]:ld t4, 568(a1)
[0x80000ae4]:addi s1, zero, 0
[0x80000ae8]:csrrw zero, fcsr, s1
[0x80000aec]:fdiv.s t6, t5, t4, dyn

[0x80000aec]:fdiv.s t6, t5, t4, dyn
[0x80000af0]:csrrs a2, fcsr, zero
[0x80000af4]:sd t6, 512(fp)
[0x80000af8]:sd a2, 520(fp)
[0x80000afc]:ld t5, 576(a1)
[0x80000b00]:ld t4, 584(a1)
[0x80000b04]:addi s1, zero, 0
[0x80000b08]:csrrw zero, fcsr, s1
[0x80000b0c]:fdiv.s t6, t5, t4, dyn

[0x80000b0c]:fdiv.s t6, t5, t4, dyn
[0x80000b10]:csrrs a2, fcsr, zero
[0x80000b14]:sd t6, 528(fp)
[0x80000b18]:sd a2, 536(fp)
[0x80000b1c]:ld t5, 592(a1)
[0x80000b20]:ld t4, 600(a1)
[0x80000b24]:addi s1, zero, 0
[0x80000b28]:csrrw zero, fcsr, s1
[0x80000b2c]:fdiv.s t6, t5, t4, dyn

[0x80000b2c]:fdiv.s t6, t5, t4, dyn
[0x80000b30]:csrrs a2, fcsr, zero
[0x80000b34]:sd t6, 544(fp)
[0x80000b38]:sd a2, 552(fp)
[0x80000b3c]:ld t5, 608(a1)
[0x80000b40]:ld t4, 616(a1)
[0x80000b44]:addi s1, zero, 0
[0x80000b48]:csrrw zero, fcsr, s1
[0x80000b4c]:fdiv.s t6, t5, t4, dyn

[0x80000b4c]:fdiv.s t6, t5, t4, dyn
[0x80000b50]:csrrs a2, fcsr, zero
[0x80000b54]:sd t6, 560(fp)
[0x80000b58]:sd a2, 568(fp)
[0x80000b5c]:ld t5, 624(a1)
[0x80000b60]:ld t4, 632(a1)
[0x80000b64]:addi s1, zero, 0
[0x80000b68]:csrrw zero, fcsr, s1
[0x80000b6c]:fdiv.s t6, t5, t4, dyn

[0x80000b6c]:fdiv.s t6, t5, t4, dyn
[0x80000b70]:csrrs a2, fcsr, zero
[0x80000b74]:sd t6, 576(fp)
[0x80000b78]:sd a2, 584(fp)
[0x80000b7c]:ld t5, 640(a1)
[0x80000b80]:ld t4, 648(a1)
[0x80000b84]:addi s1, zero, 0
[0x80000b88]:csrrw zero, fcsr, s1
[0x80000b8c]:fdiv.s t6, t5, t4, dyn

[0x80000b8c]:fdiv.s t6, t5, t4, dyn
[0x80000b90]:csrrs a2, fcsr, zero
[0x80000b94]:sd t6, 592(fp)
[0x80000b98]:sd a2, 600(fp)
[0x80000b9c]:ld t5, 656(a1)
[0x80000ba0]:ld t4, 664(a1)
[0x80000ba4]:addi s1, zero, 0
[0x80000ba8]:csrrw zero, fcsr, s1
[0x80000bac]:fdiv.s t6, t5, t4, dyn

[0x80000bac]:fdiv.s t6, t5, t4, dyn
[0x80000bb0]:csrrs a2, fcsr, zero
[0x80000bb4]:sd t6, 608(fp)
[0x80000bb8]:sd a2, 616(fp)
[0x80000bbc]:ld t5, 672(a1)
[0x80000bc0]:ld t4, 680(a1)
[0x80000bc4]:addi s1, zero, 0
[0x80000bc8]:csrrw zero, fcsr, s1
[0x80000bcc]:fdiv.s t6, t5, t4, dyn

[0x80000bcc]:fdiv.s t6, t5, t4, dyn
[0x80000bd0]:csrrs a2, fcsr, zero
[0x80000bd4]:sd t6, 624(fp)
[0x80000bd8]:sd a2, 632(fp)
[0x80000bdc]:ld t5, 688(a1)
[0x80000be0]:ld t4, 696(a1)
[0x80000be4]:addi s1, zero, 0
[0x80000be8]:csrrw zero, fcsr, s1
[0x80000bec]:fdiv.s t6, t5, t4, dyn

[0x80000bec]:fdiv.s t6, t5, t4, dyn
[0x80000bf0]:csrrs a2, fcsr, zero
[0x80000bf4]:sd t6, 640(fp)
[0x80000bf8]:sd a2, 648(fp)
[0x80000bfc]:ld t5, 704(a1)
[0x80000c00]:ld t4, 712(a1)
[0x80000c04]:addi s1, zero, 0
[0x80000c08]:csrrw zero, fcsr, s1
[0x80000c0c]:fdiv.s t6, t5, t4, dyn

[0x80000c0c]:fdiv.s t6, t5, t4, dyn
[0x80000c10]:csrrs a2, fcsr, zero
[0x80000c14]:sd t6, 656(fp)
[0x80000c18]:sd a2, 664(fp)
[0x80000c1c]:ld t5, 720(a1)
[0x80000c20]:ld t4, 728(a1)
[0x80000c24]:addi s1, zero, 0
[0x80000c28]:csrrw zero, fcsr, s1
[0x80000c2c]:fdiv.s t6, t5, t4, dyn

[0x80000c2c]:fdiv.s t6, t5, t4, dyn
[0x80000c30]:csrrs a2, fcsr, zero
[0x80000c34]:sd t6, 672(fp)
[0x80000c38]:sd a2, 680(fp)
[0x80000c3c]:ld t5, 736(a1)
[0x80000c40]:ld t4, 744(a1)
[0x80000c44]:addi s1, zero, 0
[0x80000c48]:csrrw zero, fcsr, s1
[0x80000c4c]:fdiv.s t6, t5, t4, dyn

[0x80000c4c]:fdiv.s t6, t5, t4, dyn
[0x80000c50]:csrrs a2, fcsr, zero
[0x80000c54]:sd t6, 688(fp)
[0x80000c58]:sd a2, 696(fp)
[0x80000c5c]:ld t5, 752(a1)
[0x80000c60]:ld t4, 760(a1)
[0x80000c64]:addi s1, zero, 0
[0x80000c68]:csrrw zero, fcsr, s1
[0x80000c6c]:fdiv.s t6, t5, t4, dyn

[0x80000c6c]:fdiv.s t6, t5, t4, dyn
[0x80000c70]:csrrs a2, fcsr, zero
[0x80000c74]:sd t6, 704(fp)
[0x80000c78]:sd a2, 712(fp)
[0x80000c7c]:ld t5, 768(a1)
[0x80000c80]:ld t4, 776(a1)
[0x80000c84]:addi s1, zero, 0
[0x80000c88]:csrrw zero, fcsr, s1
[0x80000c8c]:fdiv.s t6, t5, t4, dyn

[0x80000c8c]:fdiv.s t6, t5, t4, dyn
[0x80000c90]:csrrs a2, fcsr, zero
[0x80000c94]:sd t6, 720(fp)
[0x80000c98]:sd a2, 728(fp)
[0x80000c9c]:ld t5, 784(a1)
[0x80000ca0]:ld t4, 792(a1)
[0x80000ca4]:addi s1, zero, 0
[0x80000ca8]:csrrw zero, fcsr, s1
[0x80000cac]:fdiv.s t6, t5, t4, dyn

[0x80000cac]:fdiv.s t6, t5, t4, dyn
[0x80000cb0]:csrrs a2, fcsr, zero
[0x80000cb4]:sd t6, 736(fp)
[0x80000cb8]:sd a2, 744(fp)
[0x80000cbc]:ld t5, 800(a1)
[0x80000cc0]:ld t4, 808(a1)
[0x80000cc4]:addi s1, zero, 0
[0x80000cc8]:csrrw zero, fcsr, s1
[0x80000ccc]:fdiv.s t6, t5, t4, dyn

[0x80000ccc]:fdiv.s t6, t5, t4, dyn
[0x80000cd0]:csrrs a2, fcsr, zero
[0x80000cd4]:sd t6, 752(fp)
[0x80000cd8]:sd a2, 760(fp)
[0x80000cdc]:ld t5, 816(a1)
[0x80000ce0]:ld t4, 824(a1)
[0x80000ce4]:addi s1, zero, 0
[0x80000ce8]:csrrw zero, fcsr, s1
[0x80000cec]:fdiv.s t6, t5, t4, dyn

[0x80000cec]:fdiv.s t6, t5, t4, dyn
[0x80000cf0]:csrrs a2, fcsr, zero
[0x80000cf4]:sd t6, 768(fp)
[0x80000cf8]:sd a2, 776(fp)
[0x80000cfc]:ld t5, 832(a1)
[0x80000d00]:ld t4, 840(a1)
[0x80000d04]:addi s1, zero, 0
[0x80000d08]:csrrw zero, fcsr, s1
[0x80000d0c]:fdiv.s t6, t5, t4, dyn

[0x80000d0c]:fdiv.s t6, t5, t4, dyn
[0x80000d10]:csrrs a2, fcsr, zero
[0x80000d14]:sd t6, 784(fp)
[0x80000d18]:sd a2, 792(fp)
[0x80000d1c]:ld t5, 848(a1)
[0x80000d20]:ld t4, 856(a1)
[0x80000d24]:addi s1, zero, 0
[0x80000d28]:csrrw zero, fcsr, s1
[0x80000d2c]:fdiv.s t6, t5, t4, dyn

[0x80000d2c]:fdiv.s t6, t5, t4, dyn
[0x80000d30]:csrrs a2, fcsr, zero
[0x80000d34]:sd t6, 800(fp)
[0x80000d38]:sd a2, 808(fp)
[0x80000d3c]:ld t5, 864(a1)
[0x80000d40]:ld t4, 872(a1)
[0x80000d44]:addi s1, zero, 0
[0x80000d48]:csrrw zero, fcsr, s1
[0x80000d4c]:fdiv.s t6, t5, t4, dyn

[0x80000d4c]:fdiv.s t6, t5, t4, dyn
[0x80000d50]:csrrs a2, fcsr, zero
[0x80000d54]:sd t6, 816(fp)
[0x80000d58]:sd a2, 824(fp)
[0x80000d5c]:ld t5, 880(a1)
[0x80000d60]:ld t4, 888(a1)
[0x80000d64]:addi s1, zero, 0
[0x80000d68]:csrrw zero, fcsr, s1
[0x80000d6c]:fdiv.s t6, t5, t4, dyn

[0x80000d6c]:fdiv.s t6, t5, t4, dyn
[0x80000d70]:csrrs a2, fcsr, zero
[0x80000d74]:sd t6, 832(fp)
[0x80000d78]:sd a2, 840(fp)
[0x80000d7c]:ld t5, 896(a1)
[0x80000d80]:ld t4, 904(a1)
[0x80000d84]:addi s1, zero, 0
[0x80000d88]:csrrw zero, fcsr, s1
[0x80000d8c]:fdiv.s t6, t5, t4, dyn

[0x80000d8c]:fdiv.s t6, t5, t4, dyn
[0x80000d90]:csrrs a2, fcsr, zero
[0x80000d94]:sd t6, 848(fp)
[0x80000d98]:sd a2, 856(fp)
[0x80000d9c]:ld t5, 912(a1)
[0x80000da0]:ld t4, 920(a1)
[0x80000da4]:addi s1, zero, 0
[0x80000da8]:csrrw zero, fcsr, s1
[0x80000dac]:fdiv.s t6, t5, t4, dyn

[0x80000dac]:fdiv.s t6, t5, t4, dyn
[0x80000db0]:csrrs a2, fcsr, zero
[0x80000db4]:sd t6, 864(fp)
[0x80000db8]:sd a2, 872(fp)
[0x80000dbc]:ld t5, 928(a1)
[0x80000dc0]:ld t4, 936(a1)
[0x80000dc4]:addi s1, zero, 0
[0x80000dc8]:csrrw zero, fcsr, s1
[0x80000dcc]:fdiv.s t6, t5, t4, dyn

[0x80000dcc]:fdiv.s t6, t5, t4, dyn
[0x80000dd0]:csrrs a2, fcsr, zero
[0x80000dd4]:sd t6, 880(fp)
[0x80000dd8]:sd a2, 888(fp)
[0x80000ddc]:ld t5, 944(a1)
[0x80000de0]:ld t4, 952(a1)
[0x80000de4]:addi s1, zero, 0
[0x80000de8]:csrrw zero, fcsr, s1
[0x80000dec]:fdiv.s t6, t5, t4, dyn

[0x80000dec]:fdiv.s t6, t5, t4, dyn
[0x80000df0]:csrrs a2, fcsr, zero
[0x80000df4]:sd t6, 896(fp)
[0x80000df8]:sd a2, 904(fp)
[0x80000dfc]:ld t5, 960(a1)
[0x80000e00]:ld t4, 968(a1)
[0x80000e04]:addi s1, zero, 0
[0x80000e08]:csrrw zero, fcsr, s1
[0x80000e0c]:fdiv.s t6, t5, t4, dyn

[0x80000e0c]:fdiv.s t6, t5, t4, dyn
[0x80000e10]:csrrs a2, fcsr, zero
[0x80000e14]:sd t6, 912(fp)
[0x80000e18]:sd a2, 920(fp)
[0x80000e1c]:ld t5, 976(a1)
[0x80000e20]:ld t4, 984(a1)
[0x80000e24]:addi s1, zero, 0
[0x80000e28]:csrrw zero, fcsr, s1
[0x80000e2c]:fdiv.s t6, t5, t4, dyn

[0x80000e2c]:fdiv.s t6, t5, t4, dyn
[0x80000e30]:csrrs a2, fcsr, zero
[0x80000e34]:sd t6, 928(fp)
[0x80000e38]:sd a2, 936(fp)
[0x80000e3c]:ld t5, 992(a1)
[0x80000e40]:ld t4, 1000(a1)
[0x80000e44]:addi s1, zero, 0
[0x80000e48]:csrrw zero, fcsr, s1
[0x80000e4c]:fdiv.s t6, t5, t4, dyn

[0x80000e4c]:fdiv.s t6, t5, t4, dyn
[0x80000e50]:csrrs a2, fcsr, zero
[0x80000e54]:sd t6, 944(fp)
[0x80000e58]:sd a2, 952(fp)
[0x80000e5c]:ld t5, 1008(a1)
[0x80000e60]:ld t4, 1016(a1)
[0x80000e64]:addi s1, zero, 0
[0x80000e68]:csrrw zero, fcsr, s1
[0x80000e6c]:fdiv.s t6, t5, t4, dyn

[0x80000e6c]:fdiv.s t6, t5, t4, dyn
[0x80000e70]:csrrs a2, fcsr, zero
[0x80000e74]:sd t6, 960(fp)
[0x80000e78]:sd a2, 968(fp)
[0x80000e7c]:ld t5, 1024(a1)
[0x80000e80]:ld t4, 1032(a1)
[0x80000e84]:addi s1, zero, 0
[0x80000e88]:csrrw zero, fcsr, s1
[0x80000e8c]:fdiv.s t6, t5, t4, dyn

[0x80000e8c]:fdiv.s t6, t5, t4, dyn
[0x80000e90]:csrrs a2, fcsr, zero
[0x80000e94]:sd t6, 976(fp)
[0x80000e98]:sd a2, 984(fp)
[0x80000e9c]:ld t5, 1040(a1)
[0x80000ea0]:ld t4, 1048(a1)
[0x80000ea4]:addi s1, zero, 0
[0x80000ea8]:csrrw zero, fcsr, s1
[0x80000eac]:fdiv.s t6, t5, t4, dyn

[0x80000eac]:fdiv.s t6, t5, t4, dyn
[0x80000eb0]:csrrs a2, fcsr, zero
[0x80000eb4]:sd t6, 992(fp)
[0x80000eb8]:sd a2, 1000(fp)
[0x80000ebc]:ld t5, 1056(a1)
[0x80000ec0]:ld t4, 1064(a1)
[0x80000ec4]:addi s1, zero, 0
[0x80000ec8]:csrrw zero, fcsr, s1
[0x80000ecc]:fdiv.s t6, t5, t4, dyn

[0x80000ecc]:fdiv.s t6, t5, t4, dyn
[0x80000ed0]:csrrs a2, fcsr, zero
[0x80000ed4]:sd t6, 1008(fp)
[0x80000ed8]:sd a2, 1016(fp)
[0x80000edc]:ld t5, 1072(a1)
[0x80000ee0]:ld t4, 1080(a1)
[0x80000ee4]:addi s1, zero, 0
[0x80000ee8]:csrrw zero, fcsr, s1
[0x80000eec]:fdiv.s t6, t5, t4, dyn

[0x80000eec]:fdiv.s t6, t5, t4, dyn
[0x80000ef0]:csrrs a2, fcsr, zero
[0x80000ef4]:sd t6, 1024(fp)
[0x80000ef8]:sd a2, 1032(fp)
[0x80000efc]:ld t5, 1088(a1)
[0x80000f00]:ld t4, 1096(a1)
[0x80000f04]:addi s1, zero, 0
[0x80000f08]:csrrw zero, fcsr, s1
[0x80000f0c]:fdiv.s t6, t5, t4, dyn

[0x80000f0c]:fdiv.s t6, t5, t4, dyn
[0x80000f10]:csrrs a2, fcsr, zero
[0x80000f14]:sd t6, 1040(fp)
[0x80000f18]:sd a2, 1048(fp)
[0x80000f1c]:ld t5, 1104(a1)
[0x80000f20]:ld t4, 1112(a1)
[0x80000f24]:addi s1, zero, 0
[0x80000f28]:csrrw zero, fcsr, s1
[0x80000f2c]:fdiv.s t6, t5, t4, dyn

[0x80000f2c]:fdiv.s t6, t5, t4, dyn
[0x80000f30]:csrrs a2, fcsr, zero
[0x80000f34]:sd t6, 1056(fp)
[0x80000f38]:sd a2, 1064(fp)
[0x80000f3c]:ld t5, 1120(a1)
[0x80000f40]:ld t4, 1128(a1)
[0x80000f44]:addi s1, zero, 0
[0x80000f48]:csrrw zero, fcsr, s1
[0x80000f4c]:fdiv.s t6, t5, t4, dyn

[0x80000f4c]:fdiv.s t6, t5, t4, dyn
[0x80000f50]:csrrs a2, fcsr, zero
[0x80000f54]:sd t6, 1072(fp)
[0x80000f58]:sd a2, 1080(fp)
[0x80000f5c]:ld t5, 1136(a1)
[0x80000f60]:ld t4, 1144(a1)
[0x80000f64]:addi s1, zero, 0
[0x80000f68]:csrrw zero, fcsr, s1
[0x80000f6c]:fdiv.s t6, t5, t4, dyn

[0x80000f6c]:fdiv.s t6, t5, t4, dyn
[0x80000f70]:csrrs a2, fcsr, zero
[0x80000f74]:sd t6, 1088(fp)
[0x80000f78]:sd a2, 1096(fp)
[0x80000f7c]:ld t5, 1152(a1)
[0x80000f80]:ld t4, 1160(a1)
[0x80000f84]:addi s1, zero, 0
[0x80000f88]:csrrw zero, fcsr, s1
[0x80000f8c]:fdiv.s t6, t5, t4, dyn

[0x80000f8c]:fdiv.s t6, t5, t4, dyn
[0x80000f90]:csrrs a2, fcsr, zero
[0x80000f94]:sd t6, 1104(fp)
[0x80000f98]:sd a2, 1112(fp)
[0x80000f9c]:ld t5, 1168(a1)
[0x80000fa0]:ld t4, 1176(a1)
[0x80000fa4]:addi s1, zero, 0
[0x80000fa8]:csrrw zero, fcsr, s1
[0x80000fac]:fdiv.s t6, t5, t4, dyn

[0x80000fac]:fdiv.s t6, t5, t4, dyn
[0x80000fb0]:csrrs a2, fcsr, zero
[0x80000fb4]:sd t6, 1120(fp)
[0x80000fb8]:sd a2, 1128(fp)
[0x80000fbc]:ld t5, 1184(a1)
[0x80000fc0]:ld t4, 1192(a1)
[0x80000fc4]:addi s1, zero, 0
[0x80000fc8]:csrrw zero, fcsr, s1
[0x80000fcc]:fdiv.s t6, t5, t4, dyn

[0x80000fcc]:fdiv.s t6, t5, t4, dyn
[0x80000fd0]:csrrs a2, fcsr, zero
[0x80000fd4]:sd t6, 1136(fp)
[0x80000fd8]:sd a2, 1144(fp)
[0x80000fdc]:ld t5, 1200(a1)
[0x80000fe0]:ld t4, 1208(a1)
[0x80000fe4]:addi s1, zero, 0
[0x80000fe8]:csrrw zero, fcsr, s1
[0x80000fec]:fdiv.s t6, t5, t4, dyn

[0x80000fec]:fdiv.s t6, t5, t4, dyn
[0x80000ff0]:csrrs a2, fcsr, zero
[0x80000ff4]:sd t6, 1152(fp)
[0x80000ff8]:sd a2, 1160(fp)
[0x80000ffc]:ld t5, 1216(a1)
[0x80001000]:ld t4, 1224(a1)
[0x80001004]:addi s1, zero, 0
[0x80001008]:csrrw zero, fcsr, s1
[0x8000100c]:fdiv.s t6, t5, t4, dyn

[0x8000100c]:fdiv.s t6, t5, t4, dyn
[0x80001010]:csrrs a2, fcsr, zero
[0x80001014]:sd t6, 1168(fp)
[0x80001018]:sd a2, 1176(fp)
[0x8000101c]:ld t5, 1232(a1)
[0x80001020]:ld t4, 1240(a1)
[0x80001024]:addi s1, zero, 0
[0x80001028]:csrrw zero, fcsr, s1
[0x8000102c]:fdiv.s t6, t5, t4, dyn

[0x8000102c]:fdiv.s t6, t5, t4, dyn
[0x80001030]:csrrs a2, fcsr, zero
[0x80001034]:sd t6, 1184(fp)
[0x80001038]:sd a2, 1192(fp)
[0x8000103c]:ld t5, 1248(a1)
[0x80001040]:ld t4, 1256(a1)
[0x80001044]:addi s1, zero, 0
[0x80001048]:csrrw zero, fcsr, s1
[0x8000104c]:fdiv.s t6, t5, t4, dyn

[0x8000104c]:fdiv.s t6, t5, t4, dyn
[0x80001050]:csrrs a2, fcsr, zero
[0x80001054]:sd t6, 1200(fp)
[0x80001058]:sd a2, 1208(fp)
[0x8000105c]:ld t5, 1264(a1)
[0x80001060]:ld t4, 1272(a1)
[0x80001064]:addi s1, zero, 0
[0x80001068]:csrrw zero, fcsr, s1
[0x8000106c]:fdiv.s t6, t5, t4, dyn

[0x8000106c]:fdiv.s t6, t5, t4, dyn
[0x80001070]:csrrs a2, fcsr, zero
[0x80001074]:sd t6, 1216(fp)
[0x80001078]:sd a2, 1224(fp)
[0x8000107c]:ld t5, 1280(a1)
[0x80001080]:ld t4, 1288(a1)
[0x80001084]:addi s1, zero, 0
[0x80001088]:csrrw zero, fcsr, s1
[0x8000108c]:fdiv.s t6, t5, t4, dyn

[0x8000108c]:fdiv.s t6, t5, t4, dyn
[0x80001090]:csrrs a2, fcsr, zero
[0x80001094]:sd t6, 1232(fp)
[0x80001098]:sd a2, 1240(fp)
[0x8000109c]:ld t5, 1296(a1)
[0x800010a0]:ld t4, 1304(a1)
[0x800010a4]:addi s1, zero, 0
[0x800010a8]:csrrw zero, fcsr, s1
[0x800010ac]:fdiv.s t6, t5, t4, dyn

[0x800010ac]:fdiv.s t6, t5, t4, dyn
[0x800010b0]:csrrs a2, fcsr, zero
[0x800010b4]:sd t6, 1248(fp)
[0x800010b8]:sd a2, 1256(fp)
[0x800010bc]:ld t5, 1312(a1)
[0x800010c0]:ld t4, 1320(a1)
[0x800010c4]:addi s1, zero, 0
[0x800010c8]:csrrw zero, fcsr, s1
[0x800010cc]:fdiv.s t6, t5, t4, dyn

[0x800010cc]:fdiv.s t6, t5, t4, dyn
[0x800010d0]:csrrs a2, fcsr, zero
[0x800010d4]:sd t6, 1264(fp)
[0x800010d8]:sd a2, 1272(fp)
[0x800010dc]:ld t5, 1328(a1)
[0x800010e0]:ld t4, 1336(a1)
[0x800010e4]:addi s1, zero, 0
[0x800010e8]:csrrw zero, fcsr, s1
[0x800010ec]:fdiv.s t6, t5, t4, dyn

[0x800010ec]:fdiv.s t6, t5, t4, dyn
[0x800010f0]:csrrs a2, fcsr, zero
[0x800010f4]:sd t6, 1280(fp)
[0x800010f8]:sd a2, 1288(fp)
[0x800010fc]:ld t5, 1344(a1)
[0x80001100]:ld t4, 1352(a1)
[0x80001104]:addi s1, zero, 0
[0x80001108]:csrrw zero, fcsr, s1
[0x8000110c]:fdiv.s t6, t5, t4, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x60f718 and fs2 == 1 and fe2 == 0xe9 and fm2 == 0x33f8e0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fdiv.s t6, t5, t6, dyn
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
	-[0x800003c8]:sd tp, 8(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x80003820]:0x0000000000000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x29', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a0433 and fs2 == 0 and fe2 == 0xb9 and fm2 == 0x1dbba8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003dc]:fdiv.s t5, t6, t4, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t5, 16(ra)
	-[0x800003e8]:sd tp, 24(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x80003830]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x28', 'rd : x28', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003fc]:fdiv.s t3, t3, t3, dyn
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t3, 32(ra)
	-[0x80000408]:sd tp, 40(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x80003840]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x27', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000041c]:fdiv.s t4, s11, s11, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t4, 48(ra)
	-[0x80000428]:sd tp, 56(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x80003850]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x30', 'rd : x26', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0235b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000043c]:fdiv.s s10, s10, t5, dyn
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s10, 64(ra)
	-[0x80000448]:sd tp, 72(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x80003860]:0x0000000000000001




Last Coverpoint : ['rs1 : x29', 'rs2 : x26', 'rd : x27', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x512a66 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fdiv.s s11, t4, s10, dyn
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s11, 80(ra)
	-[0x80000468]:sd tp, 88(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x80003870]:0x0000000000000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x23', 'rd : x25', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x296bac and fs2 == 1 and fe2 == 0x97 and fm2 == 0x169899 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fdiv.s s9, s8, s7, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
	-[0x80000488]:sd tp, 104(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x80003880]:0x0000000000000001




Last Coverpoint : ['rs1 : x23', 'rs2 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3b506b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fdiv.s s8, s7, s9, dyn
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
	-[0x800004a8]:sd tp, 120(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x80003890]:0x0000000000000001




Last Coverpoint : ['rs1 : x25', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x192dff and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x236443 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fdiv.s s7, s9, s8, dyn
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
	-[0x800004c8]:sd tp, 136(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x800038a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x21', 'rs2 : x20', 'rd : x22', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x465fcc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fdiv.s s6, s5, s4, dyn
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
	-[0x800004e8]:sd tp, 152(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x800038b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x2b6a13 and fs2 == 0 and fe2 == 0xad and fm2 == 0x4b2862 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fdiv.s s5, s4, s6, dyn
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
	-[0x80000508]:sd tp, 168(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x800038c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x22', 'rs2 : x21', 'rd : x20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7906c5 and fs2 == 0 and fe2 == 0x8b and fm2 == 0x1f607e and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fdiv.s s4, s6, s5, dyn
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
	-[0x80000528]:sd tp, 184(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x800038d0]:0x0000000000000001




Last Coverpoint : ['rs1 : x18', 'rs2 : x17', 'rd : x19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x17a40d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fdiv.s s3, s2, a7, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
	-[0x80000548]:sd tp, 200(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x800038e0]:0x0000000000000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x19', 'rd : x18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5d1ff5 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fdiv.s s2, a7, s3, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
	-[0x80000568]:sd tp, 216(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x800038f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x76b77e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fdiv.s a7, s3, s2, dyn
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
	-[0x80000588]:sd tp, 232(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x80003900]:0x0000000000000001




Last Coverpoint : ['rs1 : x15', 'rs2 : x14', 'rd : x16', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x7248b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fdiv.s a6, a5, a4, dyn
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
	-[0x800005a8]:sd tp, 248(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x80003910]:0x0000000000000001




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x34967e and fs2 == 0 and fe2 == 0x81 and fm2 == 0x142cb6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fdiv.s a5, a4, a6, dyn
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
	-[0x800005c8]:sd tp, 264(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x80003920]:0x0000000000000001




Last Coverpoint : ['rs1 : x16', 'rs2 : x15', 'rd : x14', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x655450 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fdiv.s a4, a6, a5, dyn
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
	-[0x800005e8]:sd tp, 280(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x80003930]:0x0000000000000001




Last Coverpoint : ['rs1 : x12', 'rs2 : x11', 'rd : x13', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x39d661 and fs2 == 0 and fe2 == 0xba and fm2 == 0x187b63 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fdiv.s a3, a2, a1, dyn
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
	-[0x80000608]:sd tp, 296(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x80003940]:0x0000000000000001




Last Coverpoint : ['rs1 : x11', 'rs2 : x13', 'rd : x12', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x281a41 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x2ac557 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fdiv.s a2, a1, a3, dyn
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
	-[0x80000628]:sd tp, 312(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x80003950]:0x0000000000000001




Last Coverpoint : ['rs1 : x13', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x6b4e0e and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fdiv.s a1, a3, a2, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
	-[0x80000648]:sd tp, 328(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x80003960]:0x0000000000000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x8', 'rd : x10', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x24d5b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fdiv.s a0, s1, fp, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:sd a0, 336(ra)
	-[0x80000668]:sd tp, 344(ra)
Current Store : [0x80000668] : sd tp, 344(ra) -- Store: [0x80003970]:0x0000000000000001




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e5bf8 and fs2 == 0 and fe2 == 0x88 and fm2 == 0x24083c and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fdiv.s s1, fp, a0, dyn
	-[0x80000688]:csrrs a2, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
	-[0x80000690]:sd a2, 360(ra)
Current Store : [0x80000690] : sd a2, 360(ra) -- Store: [0x80003980]:0x0000000000000001




Last Coverpoint : ['rs1 : x10', 'rs2 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3457e7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fdiv.s fp, a0, s1, dyn
	-[0x800006a8]:csrrs a2, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
	-[0x800006b0]:sd a2, 376(ra)
Current Store : [0x800006b0] : sd a2, 376(ra) -- Store: [0x80003990]:0x0000000000000001




Last Coverpoint : ['rs1 : x6', 'rs2 : x5', 'rd : x7', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x0ed153 and fs2 == 1 and fe2 == 0x92 and fm2 == 0x1c3eb7 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fdiv.s t2, t1, t0, dyn
	-[0x800006c8]:csrrs a2, fcsr, zero
	-[0x800006cc]:sd t2, 384(ra)
	-[0x800006d0]:sd a2, 392(ra)
Current Store : [0x800006d0] : sd a2, 392(ra) -- Store: [0x800039a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x5', 'rs2 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x1fcf65 and fs2 == 0 and fe2 == 0xd7 and fm2 == 0x25a218 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fdiv.s t1, t0, t2, dyn
	-[0x800006f0]:csrrs a2, fcsr, zero
	-[0x800006f4]:sd t1, 0(fp)
	-[0x800006f8]:sd a2, 8(fp)
Current Store : [0x800006f8] : sd a2, 8(fp) -- Store: [0x800039b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x7', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x41cf9e and fs2 == 1 and fe2 == 0xda and fm2 == 0x244a46 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fdiv.s t0, t2, t1, dyn
	-[0x80000710]:csrrs a2, fcsr, zero
	-[0x80000714]:sd t0, 16(fp)
	-[0x80000718]:sd a2, 24(fp)
Current Store : [0x80000718] : sd a2, 24(fp) -- Store: [0x800039c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x2', 'rd : x4', 'fs1 == 0 and fe1 == 0xf7 and fm1 == 0x506932 and fs2 == 0 and fe2 == 0x99 and fm2 == 0x716acc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fdiv.s tp, gp, sp, dyn
	-[0x80000730]:csrrs a2, fcsr, zero
	-[0x80000734]:sd tp, 32(fp)
	-[0x80000738]:sd a2, 40(fp)
Current Store : [0x80000738] : sd a2, 40(fp) -- Store: [0x800039d0]:0x0000000000000001




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x28b6bd and fs2 == 0 and fe2 == 0xbf and fm2 == 0x17043e and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fdiv.s gp, sp, tp, dyn
	-[0x80000750]:csrrs a2, fcsr, zero
	-[0x80000754]:sd gp, 48(fp)
	-[0x80000758]:sd a2, 56(fp)
Current Store : [0x80000758] : sd a2, 56(fp) -- Store: [0x800039e0]:0x0000000000000001




Last Coverpoint : ['rs1 : x4', 'rs2 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x68f58b and fs2 == 1 and fe2 == 0xbf and fm2 == 0x756c07 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000076c]:fdiv.s sp, tp, gp, dyn
	-[0x80000770]:csrrs a2, fcsr, zero
	-[0x80000774]:sd sp, 64(fp)
	-[0x80000778]:sd a2, 72(fp)
Current Store : [0x80000778] : sd a2, 72(fp) -- Store: [0x800039f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x1d53d7 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x377d30 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000078c]:fdiv.s t6, ra, t5, dyn
	-[0x80000790]:csrrs a2, fcsr, zero
	-[0x80000794]:sd t6, 80(fp)
	-[0x80000798]:sd a2, 88(fp)
Current Store : [0x80000798] : sd a2, 88(fp) -- Store: [0x80003a00]:0x0000000000000001




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x800007ac]:fdiv.s t6, zero, t5, dyn
	-[0x800007b0]:csrrs a2, fcsr, zero
	-[0x800007b4]:sd t6, 96(fp)
	-[0x800007b8]:sd a2, 104(fp)
Current Store : [0x800007b8] : sd a2, 104(fp) -- Store: [0x80003a10]:0x0000000000000000




Last Coverpoint : ['rs2 : x1', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x5afcdb and fs2 == 1 and fe2 == 0x92 and fm2 == 0x6f10c4 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800007cc]:fdiv.s t6, t5, ra, dyn
	-[0x800007d0]:csrrs a2, fcsr, zero
	-[0x800007d4]:sd t6, 112(fp)
	-[0x800007d8]:sd a2, 120(fp)
Current Store : [0x800007d8] : sd a2, 120(fp) -- Store: [0x80003a20]:0x0000000000000001




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x800007ec]:fdiv.s t6, t5, zero, dyn
	-[0x800007f0]:csrrs a2, fcsr, zero
	-[0x800007f4]:sd t6, 128(fp)
	-[0x800007f8]:sd a2, 136(fp)
Current Store : [0x800007f8] : sd a2, 136(fp) -- Store: [0x80003a30]:0x0000000000000008




Last Coverpoint : ['rd : x1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x269d2c and fs2 == 1 and fe2 == 0x83 and fm2 == 0x519910 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fdiv.s ra, t6, t5, dyn
	-[0x80000810]:csrrs a2, fcsr, zero
	-[0x80000814]:sd ra, 144(fp)
	-[0x80000818]:sd a2, 152(fp)
Current Store : [0x80000818] : sd a2, 152(fp) -- Store: [0x80003a40]:0x0000000000000001




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3af6ff and fs2 == 1 and fe2 == 0xa4 and fm2 == 0x799c89 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fdiv.s zero, t6, t5, dyn
	-[0x80000830]:csrrs a2, fcsr, zero
	-[0x80000834]:sd zero, 160(fp)
	-[0x80000838]:sd a2, 168(fp)
Current Store : [0x80000838] : sd a2, 168(fp) -- Store: [0x80003a50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x072c24 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fdiv.s t6, t5, t4, dyn
	-[0x80000850]:csrrs a2, fcsr, zero
	-[0x80000854]:sd t6, 176(fp)
	-[0x80000858]:sd a2, 184(fp)
Current Store : [0x80000858] : sd a2, 184(fp) -- Store: [0x80003a60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x436852 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fdiv.s t6, t5, t4, dyn
	-[0x80000870]:csrrs a2, fcsr, zero
	-[0x80000874]:sd t6, 192(fp)
	-[0x80000878]:sd a2, 200(fp)
Current Store : [0x80000878] : sd a2, 200(fp) -- Store: [0x80003a70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x1123d8 and fs2 == 1 and fe2 == 0xbd and fm2 == 0x695156 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fdiv.s t6, t5, t4, dyn
	-[0x80000890]:csrrs a2, fcsr, zero
	-[0x80000894]:sd t6, 208(fp)
	-[0x80000898]:sd a2, 216(fp)
Current Store : [0x80000898] : sd a2, 216(fp) -- Store: [0x80003a80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0538b1 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fdiv.s t6, t5, t4, dyn
	-[0x800008b0]:csrrs a2, fcsr, zero
	-[0x800008b4]:sd t6, 224(fp)
	-[0x800008b8]:sd a2, 232(fp)
Current Store : [0x800008b8] : sd a2, 232(fp) -- Store: [0x80003a90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1e0667 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fdiv.s t6, t5, t4, dyn
	-[0x800008d0]:csrrs a2, fcsr, zero
	-[0x800008d4]:sd t6, 240(fp)
	-[0x800008d8]:sd a2, 248(fp)
Current Store : [0x800008d8] : sd a2, 248(fp) -- Store: [0x80003aa0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x016ff7 and fs2 == 1 and fe2 == 0xc9 and fm2 == 0x0990a3 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fdiv.s t6, t5, t4, dyn
	-[0x800008f0]:csrrs a2, fcsr, zero
	-[0x800008f4]:sd t6, 256(fp)
	-[0x800008f8]:sd a2, 264(fp)
Current Store : [0x800008f8] : sd a2, 264(fp) -- Store: [0x80003ab0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2640ba and fs2 == 0 and fe2 == 0xbf and fm2 == 0x4688b4 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fdiv.s t6, t5, t4, dyn
	-[0x80000910]:csrrs a2, fcsr, zero
	-[0x80000914]:sd t6, 272(fp)
	-[0x80000918]:sd a2, 280(fp)
Current Store : [0x80000918] : sd a2, 280(fp) -- Store: [0x80003ac0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x151546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fdiv.s t6, t5, t4, dyn
	-[0x80000930]:csrrs a2, fcsr, zero
	-[0x80000934]:sd t6, 288(fp)
	-[0x80000938]:sd a2, 296(fp)
Current Store : [0x80000938] : sd a2, 296(fp) -- Store: [0x80003ad0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x206546 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fdiv.s t6, t5, t4, dyn
	-[0x80000950]:csrrs a2, fcsr, zero
	-[0x80000954]:sd t6, 304(fp)
	-[0x80000958]:sd a2, 312(fp)
Current Store : [0x80000958] : sd a2, 312(fp) -- Store: [0x80003ae0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x7fba49 and fs2 == 1 and fe2 == 0xf7 and fm2 == 0x5bc718 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fdiv.s t6, t5, t4, dyn
	-[0x80000970]:csrrs a2, fcsr, zero
	-[0x80000974]:sd t6, 320(fp)
	-[0x80000978]:sd a2, 328(fp)
Current Store : [0x80000978] : sd a2, 328(fp) -- Store: [0x80003af0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x636240 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fdiv.s t6, t5, t4, dyn
	-[0x80000990]:csrrs a2, fcsr, zero
	-[0x80000994]:sd t6, 336(fp)
	-[0x80000998]:sd a2, 344(fp)
Current Store : [0x80000998] : sd a2, 344(fp) -- Store: [0x80003b00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3fec54 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fdiv.s t6, t5, t4, dyn
	-[0x800009b0]:csrrs a2, fcsr, zero
	-[0x800009b4]:sd t6, 352(fp)
	-[0x800009b8]:sd a2, 360(fp)
Current Store : [0x800009b8] : sd a2, 360(fp) -- Store: [0x80003b10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79dd8e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fdiv.s t6, t5, t4, dyn
	-[0x800009d0]:csrrs a2, fcsr, zero
	-[0x800009d4]:sd t6, 368(fp)
	-[0x800009d8]:sd a2, 376(fp)
Current Store : [0x800009d8] : sd a2, 376(fp) -- Store: [0x80003b20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2759f0 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x351a1b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fdiv.s t6, t5, t4, dyn
	-[0x800009f0]:csrrs a2, fcsr, zero
	-[0x800009f4]:sd t6, 384(fp)
	-[0x800009f8]:sd a2, 392(fp)
Current Store : [0x800009f8] : sd a2, 392(fp) -- Store: [0x80003b30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2cbcfc and fs2 == 0 and fe2 == 0xe8 and fm2 == 0x58de83 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000a10]:csrrs a2, fcsr, zero
	-[0x80000a14]:sd t6, 400(fp)
	-[0x80000a18]:sd a2, 408(fp)
Current Store : [0x80000a18] : sd a2, 408(fp) -- Store: [0x80003b40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf8 and fm1 == 0x5e539a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000a30]:csrrs a2, fcsr, zero
	-[0x80000a34]:sd t6, 416(fp)
	-[0x80000a38]:sd a2, 424(fp)
Current Store : [0x80000a38] : sd a2, 424(fp) -- Store: [0x80003b50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1a414e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000a50]:csrrs a2, fcsr, zero
	-[0x80000a54]:sd t6, 432(fp)
	-[0x80000a58]:sd a2, 440(fp)
Current Store : [0x80000a58] : sd a2, 440(fp) -- Store: [0x80003b60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x7bb471 and fs2 == 0 and fe2 == 0xb6 and fm2 == 0x789bff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000a70]:csrrs a2, fcsr, zero
	-[0x80000a74]:sd t6, 448(fp)
	-[0x80000a78]:sd a2, 456(fp)
Current Store : [0x80000a78] : sd a2, 456(fp) -- Store: [0x80003b70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d8377 and fs2 == 1 and fe2 == 0x91 and fm2 == 0x76277d and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000a90]:csrrs a2, fcsr, zero
	-[0x80000a94]:sd t6, 464(fp)
	-[0x80000a98]:sd a2, 472(fp)
Current Store : [0x80000a98] : sd a2, 472(fp) -- Store: [0x80003b80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x164749 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fdiv.s t6, t5, t4, dyn
	-[0x80000ab0]:csrrs a2, fcsr, zero
	-[0x80000ab4]:sd t6, 480(fp)
	-[0x80000ab8]:sd a2, 488(fp)
Current Store : [0x80000ab8] : sd a2, 488(fp) -- Store: [0x80003b90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x023675 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fdiv.s t6, t5, t4, dyn
	-[0x80000ad0]:csrrs a2, fcsr, zero
	-[0x80000ad4]:sd t6, 496(fp)
	-[0x80000ad8]:sd a2, 504(fp)
Current Store : [0x80000ad8] : sd a2, 504(fp) -- Store: [0x80003ba0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x239b5c and fs2 == 0 and fe2 == 0xe7 and fm2 == 0x311bbd and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fdiv.s t6, t5, t4, dyn
	-[0x80000af0]:csrrs a2, fcsr, zero
	-[0x80000af4]:sd t6, 512(fp)
	-[0x80000af8]:sd a2, 520(fp)
Current Store : [0x80000af8] : sd a2, 520(fp) -- Store: [0x80003bb0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x017ed0 and fs2 == 0 and fe2 == 0x94 and fm2 == 0x317fe2 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000b10]:csrrs a2, fcsr, zero
	-[0x80000b14]:sd t6, 528(fp)
	-[0x80000b18]:sd a2, 536(fp)
Current Store : [0x80000b18] : sd a2, 536(fp) -- Store: [0x80003bc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x20d6ae and fs2 == 1 and fe2 == 0xf0 and fm2 == 0x41dcec and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000b30]:csrrs a2, fcsr, zero
	-[0x80000b34]:sd t6, 544(fp)
	-[0x80000b38]:sd a2, 552(fp)
Current Store : [0x80000b38] : sd a2, 552(fp) -- Store: [0x80003bd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x32fae0 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x36e76b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000b50]:csrrs a2, fcsr, zero
	-[0x80000b54]:sd t6, 560(fp)
	-[0x80000b58]:sd a2, 568(fp)
Current Store : [0x80000b58] : sd a2, 568(fp) -- Store: [0x80003be0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0409cf and fs2 == 1 and fe2 == 0xb3 and fm2 == 0x3ed825 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000b70]:csrrs a2, fcsr, zero
	-[0x80000b74]:sd t6, 576(fp)
	-[0x80000b78]:sd a2, 584(fp)
Current Store : [0x80000b78] : sd a2, 584(fp) -- Store: [0x80003bf0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x296b63 and fs2 == 1 and fe2 == 0x88 and fm2 == 0x3a1fe4 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000b90]:csrrs a2, fcsr, zero
	-[0x80000b94]:sd t6, 592(fp)
	-[0x80000b98]:sd a2, 600(fp)
Current Store : [0x80000b98] : sd a2, 600(fp) -- Store: [0x80003c00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x03ecd6 and fs2 == 1 and fe2 == 0xbf and fm2 == 0x081dd1 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fdiv.s t6, t5, t4, dyn
	-[0x80000bb0]:csrrs a2, fcsr, zero
	-[0x80000bb4]:sd t6, 608(fp)
	-[0x80000bb8]:sd a2, 616(fp)
Current Store : [0x80000bb8] : sd a2, 616(fp) -- Store: [0x80003c10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3a0c29 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fdiv.s t6, t5, t4, dyn
	-[0x80000bd0]:csrrs a2, fcsr, zero
	-[0x80000bd4]:sd t6, 624(fp)
	-[0x80000bd8]:sd a2, 632(fp)
Current Store : [0x80000bd8] : sd a2, 632(fp) -- Store: [0x80003c20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3d37b2 and fs2 == 0 and fe2 == 0xed and fm2 == 0x54eb08 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fdiv.s t6, t5, t4, dyn
	-[0x80000bf0]:csrrs a2, fcsr, zero
	-[0x80000bf4]:sd t6, 640(fp)
	-[0x80000bf8]:sd a2, 648(fp)
Current Store : [0x80000bf8] : sd a2, 648(fp) -- Store: [0x80003c30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x34342f and fs2 == 0 and fe2 == 0x87 and fm2 == 0x7cb837 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000c10]:csrrs a2, fcsr, zero
	-[0x80000c14]:sd t6, 656(fp)
	-[0x80000c18]:sd a2, 664(fp)
Current Store : [0x80000c18] : sd a2, 664(fp) -- Store: [0x80003c40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0b4a10 and fs2 == 0 and fe2 == 0x9e and fm2 == 0x3c1678 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000c30]:csrrs a2, fcsr, zero
	-[0x80000c34]:sd t6, 672(fp)
	-[0x80000c38]:sd a2, 680(fp)
Current Store : [0x80000c38] : sd a2, 680(fp) -- Store: [0x80003c50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x02119e and fs2 == 0 and fe2 == 0xec and fm2 == 0x25b1fa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000c50]:csrrs a2, fcsr, zero
	-[0x80000c54]:sd t6, 688(fp)
	-[0x80000c58]:sd a2, 696(fp)
Current Store : [0x80000c58] : sd a2, 696(fp) -- Store: [0x80003c60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x025339 and fs2 == 0 and fe2 == 0xf9 and fm2 == 0x064930 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000c70]:csrrs a2, fcsr, zero
	-[0x80000c74]:sd t6, 704(fp)
	-[0x80000c78]:sd a2, 712(fp)
Current Store : [0x80000c78] : sd a2, 712(fp) -- Store: [0x80003c70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2ee8de and fs2 == 0 and fe2 == 0xac and fm2 == 0x6b9400 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000c90]:csrrs a2, fcsr, zero
	-[0x80000c94]:sd t6, 720(fp)
	-[0x80000c98]:sd a2, 728(fp)
Current Store : [0x80000c98] : sd a2, 728(fp) -- Store: [0x80003c80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x238f3f and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fdiv.s t6, t5, t4, dyn
	-[0x80000cb0]:csrrs a2, fcsr, zero
	-[0x80000cb4]:sd t6, 736(fp)
	-[0x80000cb8]:sd a2, 744(fp)
Current Store : [0x80000cb8] : sd a2, 744(fp) -- Store: [0x80003c90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x11c013 and fs2 == 1 and fe2 == 0xf2 and fm2 == 0x3d7983 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fdiv.s t6, t5, t4, dyn
	-[0x80000cd0]:csrrs a2, fcsr, zero
	-[0x80000cd4]:sd t6, 752(fp)
	-[0x80000cd8]:sd a2, 760(fp)
Current Store : [0x80000cd8] : sd a2, 760(fp) -- Store: [0x80003ca0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x667aed and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x0103f8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fdiv.s t6, t5, t4, dyn
	-[0x80000cf0]:csrrs a2, fcsr, zero
	-[0x80000cf4]:sd t6, 768(fp)
	-[0x80000cf8]:sd a2, 776(fp)
Current Store : [0x80000cf8] : sd a2, 776(fp) -- Store: [0x80003cb0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x217f53 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x5d82bd and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000d10]:csrrs a2, fcsr, zero
	-[0x80000d14]:sd t6, 784(fp)
	-[0x80000d18]:sd a2, 792(fp)
Current Store : [0x80000d18] : sd a2, 792(fp) -- Store: [0x80003cc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x0d5a7c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000d30]:csrrs a2, fcsr, zero
	-[0x80000d34]:sd t6, 800(fp)
	-[0x80000d38]:sd a2, 808(fp)
Current Store : [0x80000d38] : sd a2, 808(fp) -- Store: [0x80003cd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x02ab65 and fs2 == 1 and fe2 == 0xef and fm2 == 0x5dffc1 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000d50]:csrrs a2, fcsr, zero
	-[0x80000d54]:sd t6, 816(fp)
	-[0x80000d58]:sd a2, 824(fp)
Current Store : [0x80000d58] : sd a2, 824(fp) -- Store: [0x80003ce0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x5fa740 and fs2 == 0 and fe2 == 0x9f and fm2 == 0x3d6968 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000d70]:csrrs a2, fcsr, zero
	-[0x80000d74]:sd t6, 832(fp)
	-[0x80000d78]:sd a2, 840(fp)
Current Store : [0x80000d78] : sd a2, 840(fp) -- Store: [0x80003cf0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x268b6a and fs2 == 1 and fe2 == 0xef and fm2 == 0x3935cd and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000d90]:csrrs a2, fcsr, zero
	-[0x80000d94]:sd t6, 848(fp)
	-[0x80000d98]:sd a2, 856(fp)
Current Store : [0x80000d98] : sd a2, 856(fp) -- Store: [0x80003d00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x67ede5 and fs2 == 1 and fe2 == 0xaa and fm2 == 0x7a8f9b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fdiv.s t6, t5, t4, dyn
	-[0x80000db0]:csrrs a2, fcsr, zero
	-[0x80000db4]:sd t6, 864(fp)
	-[0x80000db8]:sd a2, 872(fp)
Current Store : [0x80000db8] : sd a2, 872(fp) -- Store: [0x80003d10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x373a1e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dcc]:fdiv.s t6, t5, t4, dyn
	-[0x80000dd0]:csrrs a2, fcsr, zero
	-[0x80000dd4]:sd t6, 880(fp)
	-[0x80000dd8]:sd a2, 888(fp)
Current Store : [0x80000dd8] : sd a2, 888(fp) -- Store: [0x80003d20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2a5ada and fs2 == 0 and fe2 == 0xd0 and fm2 == 0x2f4d13 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dec]:fdiv.s t6, t5, t4, dyn
	-[0x80000df0]:csrrs a2, fcsr, zero
	-[0x80000df4]:sd t6, 896(fp)
	-[0x80000df8]:sd a2, 904(fp)
Current Store : [0x80000df8] : sd a2, 904(fp) -- Store: [0x80003d30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4fe433 and fs2 == 0 and fe2 == 0x9c and fm2 == 0x3feae5 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000e10]:csrrs a2, fcsr, zero
	-[0x80000e14]:sd t6, 912(fp)
	-[0x80000e18]:sd a2, 920(fp)
Current Store : [0x80000e18] : sd a2, 920(fp) -- Store: [0x80003d40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x53a642 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000e30]:csrrs a2, fcsr, zero
	-[0x80000e34]:sd t6, 928(fp)
	-[0x80000e38]:sd a2, 936(fp)
Current Store : [0x80000e38] : sd a2, 936(fp) -- Store: [0x80003d50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x202a98 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000e50]:csrrs a2, fcsr, zero
	-[0x80000e54]:sd t6, 944(fp)
	-[0x80000e58]:sd a2, 952(fp)
Current Store : [0x80000e58] : sd a2, 952(fp) -- Store: [0x80003d60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x70766e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000e70]:csrrs a2, fcsr, zero
	-[0x80000e74]:sd t6, 960(fp)
	-[0x80000e78]:sd a2, 968(fp)
Current Store : [0x80000e78] : sd a2, 968(fp) -- Store: [0x80003d70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x60d9a4 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000e90]:csrrs a2, fcsr, zero
	-[0x80000e94]:sd t6, 976(fp)
	-[0x80000e98]:sd a2, 984(fp)
Current Store : [0x80000e98] : sd a2, 984(fp) -- Store: [0x80003d80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x264de7 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000eac]:fdiv.s t6, t5, t4, dyn
	-[0x80000eb0]:csrrs a2, fcsr, zero
	-[0x80000eb4]:sd t6, 992(fp)
	-[0x80000eb8]:sd a2, 1000(fp)
Current Store : [0x80000eb8] : sd a2, 1000(fp) -- Store: [0x80003d90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x17517f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fdiv.s t6, t5, t4, dyn
	-[0x80000ed0]:csrrs a2, fcsr, zero
	-[0x80000ed4]:sd t6, 1008(fp)
	-[0x80000ed8]:sd a2, 1016(fp)
Current Store : [0x80000ed8] : sd a2, 1016(fp) -- Store: [0x80003da0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1f12b9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eec]:fdiv.s t6, t5, t4, dyn
	-[0x80000ef0]:csrrs a2, fcsr, zero
	-[0x80000ef4]:sd t6, 1024(fp)
	-[0x80000ef8]:sd a2, 1032(fp)
Current Store : [0x80000ef8] : sd a2, 1032(fp) -- Store: [0x80003db0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x02e795 and fs2 == 1 and fe2 == 0xd6 and fm2 == 0x4dad56 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fdiv.s t6, t5, t4, dyn
	-[0x80000f10]:csrrs a2, fcsr, zero
	-[0x80000f14]:sd t6, 1040(fp)
	-[0x80000f18]:sd a2, 1048(fp)
Current Store : [0x80000f18] : sd a2, 1048(fp) -- Store: [0x80003dc0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6c3b3e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fdiv.s t6, t5, t4, dyn
	-[0x80000f30]:csrrs a2, fcsr, zero
	-[0x80000f34]:sd t6, 1056(fp)
	-[0x80000f38]:sd a2, 1064(fp)
Current Store : [0x80000f38] : sd a2, 1064(fp) -- Store: [0x80003dd0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x16201f and fs2 == 0 and fe2 == 0xfd and fm2 == 0x631e55 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fdiv.s t6, t5, t4, dyn
	-[0x80000f50]:csrrs a2, fcsr, zero
	-[0x80000f54]:sd t6, 1072(fp)
	-[0x80000f58]:sd a2, 1080(fp)
Current Store : [0x80000f58] : sd a2, 1080(fp) -- Store: [0x80003de0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x112ace and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fdiv.s t6, t5, t4, dyn
	-[0x80000f70]:csrrs a2, fcsr, zero
	-[0x80000f74]:sd t6, 1088(fp)
	-[0x80000f78]:sd a2, 1096(fp)
Current Store : [0x80000f78] : sd a2, 1096(fp) -- Store: [0x80003df0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x5b2e1a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:fdiv.s t6, t5, t4, dyn
	-[0x80000f90]:csrrs a2, fcsr, zero
	-[0x80000f94]:sd t6, 1104(fp)
	-[0x80000f98]:sd a2, 1112(fp)
Current Store : [0x80000f98] : sd a2, 1112(fp) -- Store: [0x80003e00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2bcff9 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fac]:fdiv.s t6, t5, t4, dyn
	-[0x80000fb0]:csrrs a2, fcsr, zero
	-[0x80000fb4]:sd t6, 1120(fp)
	-[0x80000fb8]:sd a2, 1128(fp)
Current Store : [0x80000fb8] : sd a2, 1128(fp) -- Store: [0x80003e10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x7b1d83 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fcc]:fdiv.s t6, t5, t4, dyn
	-[0x80000fd0]:csrrs a2, fcsr, zero
	-[0x80000fd4]:sd t6, 1136(fp)
	-[0x80000fd8]:sd a2, 1144(fp)
Current Store : [0x80000fd8] : sd a2, 1144(fp) -- Store: [0x80003e20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x4cd7ff and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x6ccaee and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fdiv.s t6, t5, t4, dyn
	-[0x80000ff0]:csrrs a2, fcsr, zero
	-[0x80000ff4]:sd t6, 1152(fp)
	-[0x80000ff8]:sd a2, 1160(fp)
Current Store : [0x80000ff8] : sd a2, 1160(fp) -- Store: [0x80003e30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2b0708 and fs2 == 1 and fe2 == 0x86 and fm2 == 0x0d7389 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000100c]:fdiv.s t6, t5, t4, dyn
	-[0x80001010]:csrrs a2, fcsr, zero
	-[0x80001014]:sd t6, 1168(fp)
	-[0x80001018]:sd a2, 1176(fp)
Current Store : [0x80001018] : sd a2, 1176(fp) -- Store: [0x80003e40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x58bf61 and fs2 == 1 and fe2 == 0xab and fm2 == 0x17657f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000102c]:fdiv.s t6, t5, t4, dyn
	-[0x80001030]:csrrs a2, fcsr, zero
	-[0x80001034]:sd t6, 1184(fp)
	-[0x80001038]:sd a2, 1192(fp)
Current Store : [0x80001038] : sd a2, 1192(fp) -- Store: [0x80003e50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x319ce6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000104c]:fdiv.s t6, t5, t4, dyn
	-[0x80001050]:csrrs a2, fcsr, zero
	-[0x80001054]:sd t6, 1200(fp)
	-[0x80001058]:sd a2, 1208(fp)
Current Store : [0x80001058] : sd a2, 1208(fp) -- Store: [0x80003e60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x2fc88c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000106c]:fdiv.s t6, t5, t4, dyn
	-[0x80001070]:csrrs a2, fcsr, zero
	-[0x80001074]:sd t6, 1216(fp)
	-[0x80001078]:sd a2, 1224(fp)
Current Store : [0x80001078] : sd a2, 1224(fp) -- Store: [0x80003e70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x761c0c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000108c]:fdiv.s t6, t5, t4, dyn
	-[0x80001090]:csrrs a2, fcsr, zero
	-[0x80001094]:sd t6, 1232(fp)
	-[0x80001098]:sd a2, 1240(fp)
Current Store : [0x80001098] : sd a2, 1240(fp) -- Store: [0x80003e80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0748c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010ac]:fdiv.s t6, t5, t4, dyn
	-[0x800010b0]:csrrs a2, fcsr, zero
	-[0x800010b4]:sd t6, 1248(fp)
	-[0x800010b8]:sd a2, 1256(fp)
Current Store : [0x800010b8] : sd a2, 1256(fp) -- Store: [0x80003e90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x38be1b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010cc]:fdiv.s t6, t5, t4, dyn
	-[0x800010d0]:csrrs a2, fcsr, zero
	-[0x800010d4]:sd t6, 1264(fp)
	-[0x800010d8]:sd a2, 1272(fp)
Current Store : [0x800010d8] : sd a2, 1272(fp) -- Store: [0x80003ea0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x03b9a1 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010ec]:fdiv.s t6, t5, t4, dyn
	-[0x800010f0]:csrrs a2, fcsr, zero
	-[0x800010f4]:sd t6, 1280(fp)
	-[0x800010f8]:sd a2, 1288(fp)
Current Store : [0x800010f8] : sd a2, 1288(fp) -- Store: [0x80003eb0]:0x0000000000000001




Last Coverpoint : ['mnemonic : fdiv.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x3af6ff and fs2 == 1 and fe2 == 0xa4 and fm2 == 0x799c89 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fdiv.s t6, t5, t4, dyn
	-[0x80001110]:csrrs a2, fcsr, zero
	-[0x80001114]:sd t6, 1296(fp)
	-[0x80001118]:sd a2, 1304(fp)
Current Store : [0x80001118] : sd a2, 1304(fp) -- Store: [0x80003ec0]:0x0000000000000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
