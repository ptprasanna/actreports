
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000f70')]      |
| SIG_REGION                | [('0x80002510', '0x80002bd0', '216 dwords')]      |
| COV_LABELS                | fcvt.wu.s_b24      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fcvt.wu.s/work-fcvtwus/fcvt.wu.s_b24-01.S/ref.S    |
| Total Number of coverpoints| 170     |
| Total Coverpoints Hit     | 170      |
| Total Signature Updates   | 214      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 106     |
| STAT4                     | 107     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000f60]:fcvt.wu.s t6, t5, dyn
      [0x80000f64]:csrrs a1, fcsr, zero
      [0x80000f68]:sd t6, 1264(t1)
      [0x80000f6c]:sd a1, 1272(t1)
 -- Signature Addresses:
      Address: 0x80002bb8 Data: 0x0000000000000000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fcvt.wu.s
      - rs1 : x30
      - rd : x31
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat






```

## Details of STAT3

```
[0x800003b8]:fcvt.wu.s t6, t5, dyn
[0x800003bc]:csrrs tp, fcsr, zero
[0x800003c0]:sd t6, 0(ra)
[0x800003c4]:sd tp, 8(ra)
[0x800003c8]:ld t6, 8(gp)
[0x800003cc]:addi sp, zero, 32
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fcvt.wu.s t5, t6, dyn

[0x800003d4]:fcvt.wu.s t5, t6, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:sd t5, 16(ra)
[0x800003e0]:sd tp, 24(ra)
[0x800003e4]:ld t3, 16(gp)
[0x800003e8]:addi sp, zero, 64
[0x800003ec]:csrrw zero, fcsr, sp
[0x800003f0]:fcvt.wu.s t4, t3, dyn

[0x800003f0]:fcvt.wu.s t4, t3, dyn
[0x800003f4]:csrrs tp, fcsr, zero
[0x800003f8]:sd t4, 32(ra)
[0x800003fc]:sd tp, 40(ra)
[0x80000400]:ld t4, 24(gp)
[0x80000404]:addi sp, zero, 96
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fcvt.wu.s t3, t4, dyn

[0x8000040c]:fcvt.wu.s t3, t4, dyn
[0x80000410]:csrrs tp, fcsr, zero
[0x80000414]:sd t3, 48(ra)
[0x80000418]:sd tp, 56(ra)
[0x8000041c]:ld s10, 32(gp)
[0x80000420]:addi sp, zero, 128
[0x80000424]:csrrw zero, fcsr, sp
[0x80000428]:fcvt.wu.s s11, s10, dyn

[0x80000428]:fcvt.wu.s s11, s10, dyn
[0x8000042c]:csrrs tp, fcsr, zero
[0x80000430]:sd s11, 64(ra)
[0x80000434]:sd tp, 72(ra)
[0x80000438]:ld s11, 40(gp)
[0x8000043c]:addi sp, zero, 0
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fcvt.wu.s s10, s11, dyn

[0x80000444]:fcvt.wu.s s10, s11, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:sd s10, 80(ra)
[0x80000450]:sd tp, 88(ra)
[0x80000454]:ld s8, 48(gp)
[0x80000458]:addi sp, zero, 32
[0x8000045c]:csrrw zero, fcsr, sp
[0x80000460]:fcvt.wu.s s9, s8, dyn

[0x80000460]:fcvt.wu.s s9, s8, dyn
[0x80000464]:csrrs tp, fcsr, zero
[0x80000468]:sd s9, 96(ra)
[0x8000046c]:sd tp, 104(ra)
[0x80000470]:ld s9, 56(gp)
[0x80000474]:addi sp, zero, 64
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fcvt.wu.s s8, s9, dyn

[0x8000047c]:fcvt.wu.s s8, s9, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s8, 112(ra)
[0x80000488]:sd tp, 120(ra)
[0x8000048c]:ld s6, 64(gp)
[0x80000490]:addi sp, zero, 96
[0x80000494]:csrrw zero, fcsr, sp
[0x80000498]:fcvt.wu.s s7, s6, dyn

[0x80000498]:fcvt.wu.s s7, s6, dyn
[0x8000049c]:csrrs tp, fcsr, zero
[0x800004a0]:sd s7, 128(ra)
[0x800004a4]:sd tp, 136(ra)
[0x800004a8]:ld s7, 72(gp)
[0x800004ac]:addi sp, zero, 128
[0x800004b0]:csrrw zero, fcsr, sp
[0x800004b4]:fcvt.wu.s s6, s7, dyn

[0x800004b4]:fcvt.wu.s s6, s7, dyn
[0x800004b8]:csrrs tp, fcsr, zero
[0x800004bc]:sd s6, 144(ra)
[0x800004c0]:sd tp, 152(ra)
[0x800004c4]:ld s4, 80(gp)
[0x800004c8]:addi sp, zero, 0
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fcvt.wu.s s5, s4, dyn

[0x800004d0]:fcvt.wu.s s5, s4, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:sd s5, 160(ra)
[0x800004dc]:sd tp, 168(ra)
[0x800004e0]:ld s5, 88(gp)
[0x800004e4]:addi sp, zero, 32
[0x800004e8]:csrrw zero, fcsr, sp
[0x800004ec]:fcvt.wu.s s4, s5, dyn

[0x800004ec]:fcvt.wu.s s4, s5, dyn
[0x800004f0]:csrrs tp, fcsr, zero
[0x800004f4]:sd s4, 176(ra)
[0x800004f8]:sd tp, 184(ra)
[0x800004fc]:ld s2, 96(gp)
[0x80000500]:addi sp, zero, 64
[0x80000504]:csrrw zero, fcsr, sp
[0x80000508]:fcvt.wu.s s3, s2, dyn

[0x80000508]:fcvt.wu.s s3, s2, dyn
[0x8000050c]:csrrs tp, fcsr, zero
[0x80000510]:sd s3, 192(ra)
[0x80000514]:sd tp, 200(ra)
[0x80000518]:ld s3, 104(gp)
[0x8000051c]:addi sp, zero, 96
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fcvt.wu.s s2, s3, dyn

[0x80000524]:fcvt.wu.s s2, s3, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:sd s2, 208(ra)
[0x80000530]:sd tp, 216(ra)
[0x80000534]:ld a6, 112(gp)
[0x80000538]:addi sp, zero, 128
[0x8000053c]:csrrw zero, fcsr, sp
[0x80000540]:fcvt.wu.s a7, a6, dyn

[0x80000540]:fcvt.wu.s a7, a6, dyn
[0x80000544]:csrrs tp, fcsr, zero
[0x80000548]:sd a7, 224(ra)
[0x8000054c]:sd tp, 232(ra)
[0x80000550]:ld a7, 120(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fcvt.wu.s a6, a7, dyn

[0x8000055c]:fcvt.wu.s a6, a7, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd a6, 240(ra)
[0x80000568]:sd tp, 248(ra)
[0x8000056c]:ld a4, 128(gp)
[0x80000570]:addi sp, zero, 32
[0x80000574]:csrrw zero, fcsr, sp
[0x80000578]:fcvt.wu.s a5, a4, dyn

[0x80000578]:fcvt.wu.s a5, a4, dyn
[0x8000057c]:csrrs tp, fcsr, zero
[0x80000580]:sd a5, 256(ra)
[0x80000584]:sd tp, 264(ra)
[0x80000588]:ld a5, 136(gp)
[0x8000058c]:addi sp, zero, 64
[0x80000590]:csrrw zero, fcsr, sp
[0x80000594]:fcvt.wu.s a4, a5, dyn

[0x80000594]:fcvt.wu.s a4, a5, dyn
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a2, 144(gp)
[0x800005a8]:addi sp, zero, 96
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fcvt.wu.s a3, a2, dyn

[0x800005b0]:fcvt.wu.s a3, a2, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a3, 152(gp)
[0x800005c4]:addi sp, zero, 128
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fcvt.wu.s a2, a3, dyn

[0x800005cc]:fcvt.wu.s a2, a3, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a0, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fcvt.wu.s a1, a0, dyn

[0x800005e8]:fcvt.wu.s a1, a0, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld a1, 168(gp)
[0x800005fc]:addi sp, zero, 32
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fcvt.wu.s a0, a1, dyn

[0x80000604]:fcvt.wu.s a0, a1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 64
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fcvt.wu.s s1, fp, dyn

[0x80000620]:fcvt.wu.s s1, fp, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 96
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fcvt.wu.s fp, s1, dyn

[0x80000644]:fcvt.wu.s fp, s1, dyn
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 128
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fcvt.wu.s t2, t1, dyn

[0x80000660]:fcvt.wu.s t2, t1, dyn
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fcvt.wu.s t1, t2, dyn

[0x8000067c]:fcvt.wu.s t1, t2, dyn
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 32
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fcvt.wu.s t0, tp, dyn

[0x80000698]:fcvt.wu.s t0, tp, dyn
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 32
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 64
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fcvt.wu.s tp, t0, dyn

[0x800006bc]:fcvt.wu.s tp, t0, dyn
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 96
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fcvt.wu.s gp, sp, dyn

[0x800006d8]:fcvt.wu.s gp, sp, dyn
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 128
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fcvt.wu.s sp, gp, dyn

[0x800006f4]:fcvt.wu.s sp, gp, dyn
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fcvt.wu.s ra, zero, dyn

[0x80000710]:fcvt.wu.s ra, zero, dyn
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 32
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fcvt.wu.s zero, ra, dyn

[0x8000072c]:fcvt.wu.s zero, ra, dyn
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:ld t5, 72(a0)
[0x80000740]:addi fp, zero, 64
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fcvt.wu.s t6, t5, dyn

[0x80000748]:fcvt.wu.s t6, t5, dyn
[0x8000074c]:csrrs a1, fcsr, zero
[0x80000750]:sd t6, 80(t1)
[0x80000754]:sd a1, 88(t1)
[0x80000758]:ld t5, 80(a0)
[0x8000075c]:addi fp, zero, 96
[0x80000760]:csrrw zero, fcsr, fp
[0x80000764]:fcvt.wu.s t6, t5, dyn

[0x80000764]:fcvt.wu.s t6, t5, dyn
[0x80000768]:csrrs a1, fcsr, zero
[0x8000076c]:sd t6, 96(t1)
[0x80000770]:sd a1, 104(t1)
[0x80000774]:ld t5, 88(a0)
[0x80000778]:addi fp, zero, 128
[0x8000077c]:csrrw zero, fcsr, fp
[0x80000780]:fcvt.wu.s t6, t5, dyn

[0x80000780]:fcvt.wu.s t6, t5, dyn
[0x80000784]:csrrs a1, fcsr, zero
[0x80000788]:sd t6, 112(t1)
[0x8000078c]:sd a1, 120(t1)
[0x80000790]:ld t5, 96(a0)
[0x80000794]:addi fp, zero, 0
[0x80000798]:csrrw zero, fcsr, fp
[0x8000079c]:fcvt.wu.s t6, t5, dyn

[0x8000079c]:fcvt.wu.s t6, t5, dyn
[0x800007a0]:csrrs a1, fcsr, zero
[0x800007a4]:sd t6, 128(t1)
[0x800007a8]:sd a1, 136(t1)
[0x800007ac]:ld t5, 104(a0)
[0x800007b0]:addi fp, zero, 32
[0x800007b4]:csrrw zero, fcsr, fp
[0x800007b8]:fcvt.wu.s t6, t5, dyn

[0x800007b8]:fcvt.wu.s t6, t5, dyn
[0x800007bc]:csrrs a1, fcsr, zero
[0x800007c0]:sd t6, 144(t1)
[0x800007c4]:sd a1, 152(t1)
[0x800007c8]:ld t5, 112(a0)
[0x800007cc]:addi fp, zero, 64
[0x800007d0]:csrrw zero, fcsr, fp
[0x800007d4]:fcvt.wu.s t6, t5, dyn

[0x800007d4]:fcvt.wu.s t6, t5, dyn
[0x800007d8]:csrrs a1, fcsr, zero
[0x800007dc]:sd t6, 160(t1)
[0x800007e0]:sd a1, 168(t1)
[0x800007e4]:ld t5, 120(a0)
[0x800007e8]:addi fp, zero, 96
[0x800007ec]:csrrw zero, fcsr, fp
[0x800007f0]:fcvt.wu.s t6, t5, dyn

[0x800007f0]:fcvt.wu.s t6, t5, dyn
[0x800007f4]:csrrs a1, fcsr, zero
[0x800007f8]:sd t6, 176(t1)
[0x800007fc]:sd a1, 184(t1)
[0x80000800]:ld t5, 128(a0)
[0x80000804]:addi fp, zero, 128
[0x80000808]:csrrw zero, fcsr, fp
[0x8000080c]:fcvt.wu.s t6, t5, dyn

[0x8000080c]:fcvt.wu.s t6, t5, dyn
[0x80000810]:csrrs a1, fcsr, zero
[0x80000814]:sd t6, 192(t1)
[0x80000818]:sd a1, 200(t1)
[0x8000081c]:ld t5, 136(a0)
[0x80000820]:addi fp, zero, 0
[0x80000824]:csrrw zero, fcsr, fp
[0x80000828]:fcvt.wu.s t6, t5, dyn

[0x80000828]:fcvt.wu.s t6, t5, dyn
[0x8000082c]:csrrs a1, fcsr, zero
[0x80000830]:sd t6, 208(t1)
[0x80000834]:sd a1, 216(t1)
[0x80000838]:ld t5, 144(a0)
[0x8000083c]:addi fp, zero, 32
[0x80000840]:csrrw zero, fcsr, fp
[0x80000844]:fcvt.wu.s t6, t5, dyn

[0x80000844]:fcvt.wu.s t6, t5, dyn
[0x80000848]:csrrs a1, fcsr, zero
[0x8000084c]:sd t6, 224(t1)
[0x80000850]:sd a1, 232(t1)
[0x80000854]:ld t5, 152(a0)
[0x80000858]:addi fp, zero, 64
[0x8000085c]:csrrw zero, fcsr, fp
[0x80000860]:fcvt.wu.s t6, t5, dyn

[0x80000860]:fcvt.wu.s t6, t5, dyn
[0x80000864]:csrrs a1, fcsr, zero
[0x80000868]:sd t6, 240(t1)
[0x8000086c]:sd a1, 248(t1)
[0x80000870]:ld t5, 160(a0)
[0x80000874]:addi fp, zero, 96
[0x80000878]:csrrw zero, fcsr, fp
[0x8000087c]:fcvt.wu.s t6, t5, dyn

[0x8000087c]:fcvt.wu.s t6, t5, dyn
[0x80000880]:csrrs a1, fcsr, zero
[0x80000884]:sd t6, 256(t1)
[0x80000888]:sd a1, 264(t1)
[0x8000088c]:ld t5, 168(a0)
[0x80000890]:addi fp, zero, 128
[0x80000894]:csrrw zero, fcsr, fp
[0x80000898]:fcvt.wu.s t6, t5, dyn

[0x80000898]:fcvt.wu.s t6, t5, dyn
[0x8000089c]:csrrs a1, fcsr, zero
[0x800008a0]:sd t6, 272(t1)
[0x800008a4]:sd a1, 280(t1)
[0x800008a8]:ld t5, 176(a0)
[0x800008ac]:addi fp, zero, 0
[0x800008b0]:csrrw zero, fcsr, fp
[0x800008b4]:fcvt.wu.s t6, t5, dyn

[0x800008b4]:fcvt.wu.s t6, t5, dyn
[0x800008b8]:csrrs a1, fcsr, zero
[0x800008bc]:sd t6, 288(t1)
[0x800008c0]:sd a1, 296(t1)
[0x800008c4]:ld t5, 184(a0)
[0x800008c8]:addi fp, zero, 32
[0x800008cc]:csrrw zero, fcsr, fp
[0x800008d0]:fcvt.wu.s t6, t5, dyn

[0x800008d0]:fcvt.wu.s t6, t5, dyn
[0x800008d4]:csrrs a1, fcsr, zero
[0x800008d8]:sd t6, 304(t1)
[0x800008dc]:sd a1, 312(t1)
[0x800008e0]:ld t5, 192(a0)
[0x800008e4]:addi fp, zero, 64
[0x800008e8]:csrrw zero, fcsr, fp
[0x800008ec]:fcvt.wu.s t6, t5, dyn

[0x800008ec]:fcvt.wu.s t6, t5, dyn
[0x800008f0]:csrrs a1, fcsr, zero
[0x800008f4]:sd t6, 320(t1)
[0x800008f8]:sd a1, 328(t1)
[0x800008fc]:ld t5, 200(a0)
[0x80000900]:addi fp, zero, 96
[0x80000904]:csrrw zero, fcsr, fp
[0x80000908]:fcvt.wu.s t6, t5, dyn

[0x80000908]:fcvt.wu.s t6, t5, dyn
[0x8000090c]:csrrs a1, fcsr, zero
[0x80000910]:sd t6, 336(t1)
[0x80000914]:sd a1, 344(t1)
[0x80000918]:ld t5, 208(a0)
[0x8000091c]:addi fp, zero, 128
[0x80000920]:csrrw zero, fcsr, fp
[0x80000924]:fcvt.wu.s t6, t5, dyn

[0x80000924]:fcvt.wu.s t6, t5, dyn
[0x80000928]:csrrs a1, fcsr, zero
[0x8000092c]:sd t6, 352(t1)
[0x80000930]:sd a1, 360(t1)
[0x80000934]:ld t5, 216(a0)
[0x80000938]:addi fp, zero, 0
[0x8000093c]:csrrw zero, fcsr, fp
[0x80000940]:fcvt.wu.s t6, t5, dyn

[0x80000940]:fcvt.wu.s t6, t5, dyn
[0x80000944]:csrrs a1, fcsr, zero
[0x80000948]:sd t6, 368(t1)
[0x8000094c]:sd a1, 376(t1)
[0x80000950]:ld t5, 224(a0)
[0x80000954]:addi fp, zero, 32
[0x80000958]:csrrw zero, fcsr, fp
[0x8000095c]:fcvt.wu.s t6, t5, dyn

[0x8000095c]:fcvt.wu.s t6, t5, dyn
[0x80000960]:csrrs a1, fcsr, zero
[0x80000964]:sd t6, 384(t1)
[0x80000968]:sd a1, 392(t1)
[0x8000096c]:ld t5, 232(a0)
[0x80000970]:addi fp, zero, 64
[0x80000974]:csrrw zero, fcsr, fp
[0x80000978]:fcvt.wu.s t6, t5, dyn

[0x80000978]:fcvt.wu.s t6, t5, dyn
[0x8000097c]:csrrs a1, fcsr, zero
[0x80000980]:sd t6, 400(t1)
[0x80000984]:sd a1, 408(t1)
[0x80000988]:ld t5, 240(a0)
[0x8000098c]:addi fp, zero, 96
[0x80000990]:csrrw zero, fcsr, fp
[0x80000994]:fcvt.wu.s t6, t5, dyn

[0x80000994]:fcvt.wu.s t6, t5, dyn
[0x80000998]:csrrs a1, fcsr, zero
[0x8000099c]:sd t6, 416(t1)
[0x800009a0]:sd a1, 424(t1)
[0x800009a4]:ld t5, 248(a0)
[0x800009a8]:addi fp, zero, 128
[0x800009ac]:csrrw zero, fcsr, fp
[0x800009b0]:fcvt.wu.s t6, t5, dyn

[0x800009b0]:fcvt.wu.s t6, t5, dyn
[0x800009b4]:csrrs a1, fcsr, zero
[0x800009b8]:sd t6, 432(t1)
[0x800009bc]:sd a1, 440(t1)
[0x800009c0]:ld t5, 256(a0)
[0x800009c4]:addi fp, zero, 0
[0x800009c8]:csrrw zero, fcsr, fp
[0x800009cc]:fcvt.wu.s t6, t5, dyn

[0x800009cc]:fcvt.wu.s t6, t5, dyn
[0x800009d0]:csrrs a1, fcsr, zero
[0x800009d4]:sd t6, 448(t1)
[0x800009d8]:sd a1, 456(t1)
[0x800009dc]:ld t5, 264(a0)
[0x800009e0]:addi fp, zero, 32
[0x800009e4]:csrrw zero, fcsr, fp
[0x800009e8]:fcvt.wu.s t6, t5, dyn

[0x800009e8]:fcvt.wu.s t6, t5, dyn
[0x800009ec]:csrrs a1, fcsr, zero
[0x800009f0]:sd t6, 464(t1)
[0x800009f4]:sd a1, 472(t1)
[0x800009f8]:ld t5, 272(a0)
[0x800009fc]:addi fp, zero, 64
[0x80000a00]:csrrw zero, fcsr, fp
[0x80000a04]:fcvt.wu.s t6, t5, dyn

[0x80000a04]:fcvt.wu.s t6, t5, dyn
[0x80000a08]:csrrs a1, fcsr, zero
[0x80000a0c]:sd t6, 480(t1)
[0x80000a10]:sd a1, 488(t1)
[0x80000a14]:ld t5, 280(a0)
[0x80000a18]:addi fp, zero, 96
[0x80000a1c]:csrrw zero, fcsr, fp
[0x80000a20]:fcvt.wu.s t6, t5, dyn

[0x80000a20]:fcvt.wu.s t6, t5, dyn
[0x80000a24]:csrrs a1, fcsr, zero
[0x80000a28]:sd t6, 496(t1)
[0x80000a2c]:sd a1, 504(t1)
[0x80000a30]:ld t5, 288(a0)
[0x80000a34]:addi fp, zero, 128
[0x80000a38]:csrrw zero, fcsr, fp
[0x80000a3c]:fcvt.wu.s t6, t5, dyn

[0x80000a3c]:fcvt.wu.s t6, t5, dyn
[0x80000a40]:csrrs a1, fcsr, zero
[0x80000a44]:sd t6, 512(t1)
[0x80000a48]:sd a1, 520(t1)
[0x80000a4c]:ld t5, 296(a0)
[0x80000a50]:addi fp, zero, 0
[0x80000a54]:csrrw zero, fcsr, fp
[0x80000a58]:fcvt.wu.s t6, t5, dyn

[0x80000a58]:fcvt.wu.s t6, t5, dyn
[0x80000a5c]:csrrs a1, fcsr, zero
[0x80000a60]:sd t6, 528(t1)
[0x80000a64]:sd a1, 536(t1)
[0x80000a68]:ld t5, 304(a0)
[0x80000a6c]:addi fp, zero, 32
[0x80000a70]:csrrw zero, fcsr, fp
[0x80000a74]:fcvt.wu.s t6, t5, dyn

[0x80000a74]:fcvt.wu.s t6, t5, dyn
[0x80000a78]:csrrs a1, fcsr, zero
[0x80000a7c]:sd t6, 544(t1)
[0x80000a80]:sd a1, 552(t1)
[0x80000a84]:ld t5, 312(a0)
[0x80000a88]:addi fp, zero, 64
[0x80000a8c]:csrrw zero, fcsr, fp
[0x80000a90]:fcvt.wu.s t6, t5, dyn

[0x80000a90]:fcvt.wu.s t6, t5, dyn
[0x80000a94]:csrrs a1, fcsr, zero
[0x80000a98]:sd t6, 560(t1)
[0x80000a9c]:sd a1, 568(t1)
[0x80000aa0]:ld t5, 320(a0)
[0x80000aa4]:addi fp, zero, 96
[0x80000aa8]:csrrw zero, fcsr, fp
[0x80000aac]:fcvt.wu.s t6, t5, dyn

[0x80000aac]:fcvt.wu.s t6, t5, dyn
[0x80000ab0]:csrrs a1, fcsr, zero
[0x80000ab4]:sd t6, 576(t1)
[0x80000ab8]:sd a1, 584(t1)
[0x80000abc]:ld t5, 328(a0)
[0x80000ac0]:addi fp, zero, 128
[0x80000ac4]:csrrw zero, fcsr, fp
[0x80000ac8]:fcvt.wu.s t6, t5, dyn

[0x80000ac8]:fcvt.wu.s t6, t5, dyn
[0x80000acc]:csrrs a1, fcsr, zero
[0x80000ad0]:sd t6, 592(t1)
[0x80000ad4]:sd a1, 600(t1)
[0x80000ad8]:ld t5, 336(a0)
[0x80000adc]:addi fp, zero, 0
[0x80000ae0]:csrrw zero, fcsr, fp
[0x80000ae4]:fcvt.wu.s t6, t5, dyn

[0x80000ae4]:fcvt.wu.s t6, t5, dyn
[0x80000ae8]:csrrs a1, fcsr, zero
[0x80000aec]:sd t6, 608(t1)
[0x80000af0]:sd a1, 616(t1)
[0x80000af4]:ld t5, 344(a0)
[0x80000af8]:addi fp, zero, 32
[0x80000afc]:csrrw zero, fcsr, fp
[0x80000b00]:fcvt.wu.s t6, t5, dyn

[0x80000b00]:fcvt.wu.s t6, t5, dyn
[0x80000b04]:csrrs a1, fcsr, zero
[0x80000b08]:sd t6, 624(t1)
[0x80000b0c]:sd a1, 632(t1)
[0x80000b10]:ld t5, 352(a0)
[0x80000b14]:addi fp, zero, 64
[0x80000b18]:csrrw zero, fcsr, fp
[0x80000b1c]:fcvt.wu.s t6, t5, dyn

[0x80000b1c]:fcvt.wu.s t6, t5, dyn
[0x80000b20]:csrrs a1, fcsr, zero
[0x80000b24]:sd t6, 640(t1)
[0x80000b28]:sd a1, 648(t1)
[0x80000b2c]:ld t5, 360(a0)
[0x80000b30]:addi fp, zero, 96
[0x80000b34]:csrrw zero, fcsr, fp
[0x80000b38]:fcvt.wu.s t6, t5, dyn

[0x80000b38]:fcvt.wu.s t6, t5, dyn
[0x80000b3c]:csrrs a1, fcsr, zero
[0x80000b40]:sd t6, 656(t1)
[0x80000b44]:sd a1, 664(t1)
[0x80000b48]:ld t5, 368(a0)
[0x80000b4c]:addi fp, zero, 128
[0x80000b50]:csrrw zero, fcsr, fp
[0x80000b54]:fcvt.wu.s t6, t5, dyn

[0x80000b54]:fcvt.wu.s t6, t5, dyn
[0x80000b58]:csrrs a1, fcsr, zero
[0x80000b5c]:sd t6, 672(t1)
[0x80000b60]:sd a1, 680(t1)
[0x80000b64]:ld t5, 376(a0)
[0x80000b68]:addi fp, zero, 0
[0x80000b6c]:csrrw zero, fcsr, fp
[0x80000b70]:fcvt.wu.s t6, t5, dyn

[0x80000b70]:fcvt.wu.s t6, t5, dyn
[0x80000b74]:csrrs a1, fcsr, zero
[0x80000b78]:sd t6, 688(t1)
[0x80000b7c]:sd a1, 696(t1)
[0x80000b80]:ld t5, 384(a0)
[0x80000b84]:addi fp, zero, 32
[0x80000b88]:csrrw zero, fcsr, fp
[0x80000b8c]:fcvt.wu.s t6, t5, dyn

[0x80000b8c]:fcvt.wu.s t6, t5, dyn
[0x80000b90]:csrrs a1, fcsr, zero
[0x80000b94]:sd t6, 704(t1)
[0x80000b98]:sd a1, 712(t1)
[0x80000b9c]:ld t5, 392(a0)
[0x80000ba0]:addi fp, zero, 64
[0x80000ba4]:csrrw zero, fcsr, fp
[0x80000ba8]:fcvt.wu.s t6, t5, dyn

[0x80000ba8]:fcvt.wu.s t6, t5, dyn
[0x80000bac]:csrrs a1, fcsr, zero
[0x80000bb0]:sd t6, 720(t1)
[0x80000bb4]:sd a1, 728(t1)
[0x80000bb8]:ld t5, 400(a0)
[0x80000bbc]:addi fp, zero, 96
[0x80000bc0]:csrrw zero, fcsr, fp
[0x80000bc4]:fcvt.wu.s t6, t5, dyn

[0x80000bc4]:fcvt.wu.s t6, t5, dyn
[0x80000bc8]:csrrs a1, fcsr, zero
[0x80000bcc]:sd t6, 736(t1)
[0x80000bd0]:sd a1, 744(t1)
[0x80000bd4]:ld t5, 408(a0)
[0x80000bd8]:addi fp, zero, 128
[0x80000bdc]:csrrw zero, fcsr, fp
[0x80000be0]:fcvt.wu.s t6, t5, dyn

[0x80000be0]:fcvt.wu.s t6, t5, dyn
[0x80000be4]:csrrs a1, fcsr, zero
[0x80000be8]:sd t6, 752(t1)
[0x80000bec]:sd a1, 760(t1)
[0x80000bf0]:ld t5, 416(a0)
[0x80000bf4]:addi fp, zero, 0
[0x80000bf8]:csrrw zero, fcsr, fp
[0x80000bfc]:fcvt.wu.s t6, t5, dyn

[0x80000bfc]:fcvt.wu.s t6, t5, dyn
[0x80000c00]:csrrs a1, fcsr, zero
[0x80000c04]:sd t6, 768(t1)
[0x80000c08]:sd a1, 776(t1)
[0x80000c0c]:ld t5, 424(a0)
[0x80000c10]:addi fp, zero, 32
[0x80000c14]:csrrw zero, fcsr, fp
[0x80000c18]:fcvt.wu.s t6, t5, dyn

[0x80000c18]:fcvt.wu.s t6, t5, dyn
[0x80000c1c]:csrrs a1, fcsr, zero
[0x80000c20]:sd t6, 784(t1)
[0x80000c24]:sd a1, 792(t1)
[0x80000c28]:ld t5, 432(a0)
[0x80000c2c]:addi fp, zero, 64
[0x80000c30]:csrrw zero, fcsr, fp
[0x80000c34]:fcvt.wu.s t6, t5, dyn

[0x80000c34]:fcvt.wu.s t6, t5, dyn
[0x80000c38]:csrrs a1, fcsr, zero
[0x80000c3c]:sd t6, 800(t1)
[0x80000c40]:sd a1, 808(t1)
[0x80000c44]:ld t5, 440(a0)
[0x80000c48]:addi fp, zero, 96
[0x80000c4c]:csrrw zero, fcsr, fp
[0x80000c50]:fcvt.wu.s t6, t5, dyn

[0x80000c50]:fcvt.wu.s t6, t5, dyn
[0x80000c54]:csrrs a1, fcsr, zero
[0x80000c58]:sd t6, 816(t1)
[0x80000c5c]:sd a1, 824(t1)
[0x80000c60]:ld t5, 448(a0)
[0x80000c64]:addi fp, zero, 128
[0x80000c68]:csrrw zero, fcsr, fp
[0x80000c6c]:fcvt.wu.s t6, t5, dyn

[0x80000c6c]:fcvt.wu.s t6, t5, dyn
[0x80000c70]:csrrs a1, fcsr, zero
[0x80000c74]:sd t6, 832(t1)
[0x80000c78]:sd a1, 840(t1)
[0x80000c7c]:ld t5, 456(a0)
[0x80000c80]:addi fp, zero, 0
[0x80000c84]:csrrw zero, fcsr, fp
[0x80000c88]:fcvt.wu.s t6, t5, dyn

[0x80000c88]:fcvt.wu.s t6, t5, dyn
[0x80000c8c]:csrrs a1, fcsr, zero
[0x80000c90]:sd t6, 848(t1)
[0x80000c94]:sd a1, 856(t1)
[0x80000c98]:ld t5, 464(a0)
[0x80000c9c]:addi fp, zero, 32
[0x80000ca0]:csrrw zero, fcsr, fp
[0x80000ca4]:fcvt.wu.s t6, t5, dyn

[0x80000ca4]:fcvt.wu.s t6, t5, dyn
[0x80000ca8]:csrrs a1, fcsr, zero
[0x80000cac]:sd t6, 864(t1)
[0x80000cb0]:sd a1, 872(t1)
[0x80000cb4]:ld t5, 472(a0)
[0x80000cb8]:addi fp, zero, 64
[0x80000cbc]:csrrw zero, fcsr, fp
[0x80000cc0]:fcvt.wu.s t6, t5, dyn

[0x80000cc0]:fcvt.wu.s t6, t5, dyn
[0x80000cc4]:csrrs a1, fcsr, zero
[0x80000cc8]:sd t6, 880(t1)
[0x80000ccc]:sd a1, 888(t1)
[0x80000cd0]:ld t5, 480(a0)
[0x80000cd4]:addi fp, zero, 96
[0x80000cd8]:csrrw zero, fcsr, fp
[0x80000cdc]:fcvt.wu.s t6, t5, dyn

[0x80000cdc]:fcvt.wu.s t6, t5, dyn
[0x80000ce0]:csrrs a1, fcsr, zero
[0x80000ce4]:sd t6, 896(t1)
[0x80000ce8]:sd a1, 904(t1)
[0x80000cec]:ld t5, 488(a0)
[0x80000cf0]:addi fp, zero, 128
[0x80000cf4]:csrrw zero, fcsr, fp
[0x80000cf8]:fcvt.wu.s t6, t5, dyn

[0x80000cf8]:fcvt.wu.s t6, t5, dyn
[0x80000cfc]:csrrs a1, fcsr, zero
[0x80000d00]:sd t6, 912(t1)
[0x80000d04]:sd a1, 920(t1)
[0x80000d08]:ld t5, 496(a0)
[0x80000d0c]:addi fp, zero, 0
[0x80000d10]:csrrw zero, fcsr, fp
[0x80000d14]:fcvt.wu.s t6, t5, dyn

[0x80000d14]:fcvt.wu.s t6, t5, dyn
[0x80000d18]:csrrs a1, fcsr, zero
[0x80000d1c]:sd t6, 928(t1)
[0x80000d20]:sd a1, 936(t1)
[0x80000d24]:ld t5, 504(a0)
[0x80000d28]:addi fp, zero, 32
[0x80000d2c]:csrrw zero, fcsr, fp
[0x80000d30]:fcvt.wu.s t6, t5, dyn

[0x80000d30]:fcvt.wu.s t6, t5, dyn
[0x80000d34]:csrrs a1, fcsr, zero
[0x80000d38]:sd t6, 944(t1)
[0x80000d3c]:sd a1, 952(t1)
[0x80000d40]:ld t5, 512(a0)
[0x80000d44]:addi fp, zero, 64
[0x80000d48]:csrrw zero, fcsr, fp
[0x80000d4c]:fcvt.wu.s t6, t5, dyn

[0x80000d4c]:fcvt.wu.s t6, t5, dyn
[0x80000d50]:csrrs a1, fcsr, zero
[0x80000d54]:sd t6, 960(t1)
[0x80000d58]:sd a1, 968(t1)
[0x80000d5c]:ld t5, 520(a0)
[0x80000d60]:addi fp, zero, 96
[0x80000d64]:csrrw zero, fcsr, fp
[0x80000d68]:fcvt.wu.s t6, t5, dyn

[0x80000d68]:fcvt.wu.s t6, t5, dyn
[0x80000d6c]:csrrs a1, fcsr, zero
[0x80000d70]:sd t6, 976(t1)
[0x80000d74]:sd a1, 984(t1)
[0x80000d78]:ld t5, 528(a0)
[0x80000d7c]:addi fp, zero, 128
[0x80000d80]:csrrw zero, fcsr, fp
[0x80000d84]:fcvt.wu.s t6, t5, dyn

[0x80000d84]:fcvt.wu.s t6, t5, dyn
[0x80000d88]:csrrs a1, fcsr, zero
[0x80000d8c]:sd t6, 992(t1)
[0x80000d90]:sd a1, 1000(t1)
[0x80000d94]:ld t5, 536(a0)
[0x80000d98]:addi fp, zero, 0
[0x80000d9c]:csrrw zero, fcsr, fp
[0x80000da0]:fcvt.wu.s t6, t5, dyn

[0x80000da0]:fcvt.wu.s t6, t5, dyn
[0x80000da4]:csrrs a1, fcsr, zero
[0x80000da8]:sd t6, 1008(t1)
[0x80000dac]:sd a1, 1016(t1)
[0x80000db0]:ld t5, 544(a0)
[0x80000db4]:addi fp, zero, 32
[0x80000db8]:csrrw zero, fcsr, fp
[0x80000dbc]:fcvt.wu.s t6, t5, dyn

[0x80000dbc]:fcvt.wu.s t6, t5, dyn
[0x80000dc0]:csrrs a1, fcsr, zero
[0x80000dc4]:sd t6, 1024(t1)
[0x80000dc8]:sd a1, 1032(t1)
[0x80000dcc]:ld t5, 552(a0)
[0x80000dd0]:addi fp, zero, 64
[0x80000dd4]:csrrw zero, fcsr, fp
[0x80000dd8]:fcvt.wu.s t6, t5, dyn

[0x80000dd8]:fcvt.wu.s t6, t5, dyn
[0x80000ddc]:csrrs a1, fcsr, zero
[0x80000de0]:sd t6, 1040(t1)
[0x80000de4]:sd a1, 1048(t1)
[0x80000de8]:ld t5, 560(a0)
[0x80000dec]:addi fp, zero, 96
[0x80000df0]:csrrw zero, fcsr, fp
[0x80000df4]:fcvt.wu.s t6, t5, dyn

[0x80000df4]:fcvt.wu.s t6, t5, dyn
[0x80000df8]:csrrs a1, fcsr, zero
[0x80000dfc]:sd t6, 1056(t1)
[0x80000e00]:sd a1, 1064(t1)
[0x80000e04]:ld t5, 568(a0)
[0x80000e08]:addi fp, zero, 128
[0x80000e0c]:csrrw zero, fcsr, fp
[0x80000e10]:fcvt.wu.s t6, t5, dyn

[0x80000e10]:fcvt.wu.s t6, t5, dyn
[0x80000e14]:csrrs a1, fcsr, zero
[0x80000e18]:sd t6, 1072(t1)
[0x80000e1c]:sd a1, 1080(t1)
[0x80000e20]:ld t5, 576(a0)
[0x80000e24]:addi fp, zero, 0
[0x80000e28]:csrrw zero, fcsr, fp
[0x80000e2c]:fcvt.wu.s t6, t5, dyn

[0x80000e2c]:fcvt.wu.s t6, t5, dyn
[0x80000e30]:csrrs a1, fcsr, zero
[0x80000e34]:sd t6, 1088(t1)
[0x80000e38]:sd a1, 1096(t1)
[0x80000e3c]:ld t5, 584(a0)
[0x80000e40]:addi fp, zero, 32
[0x80000e44]:csrrw zero, fcsr, fp
[0x80000e48]:fcvt.wu.s t6, t5, dyn

[0x80000e48]:fcvt.wu.s t6, t5, dyn
[0x80000e4c]:csrrs a1, fcsr, zero
[0x80000e50]:sd t6, 1104(t1)
[0x80000e54]:sd a1, 1112(t1)
[0x80000e58]:ld t5, 592(a0)
[0x80000e5c]:addi fp, zero, 64
[0x80000e60]:csrrw zero, fcsr, fp
[0x80000e64]:fcvt.wu.s t6, t5, dyn

[0x80000e64]:fcvt.wu.s t6, t5, dyn
[0x80000e68]:csrrs a1, fcsr, zero
[0x80000e6c]:sd t6, 1120(t1)
[0x80000e70]:sd a1, 1128(t1)
[0x80000e74]:ld t5, 600(a0)
[0x80000e78]:addi fp, zero, 96
[0x80000e7c]:csrrw zero, fcsr, fp
[0x80000e80]:fcvt.wu.s t6, t5, dyn

[0x80000e80]:fcvt.wu.s t6, t5, dyn
[0x80000e84]:csrrs a1, fcsr, zero
[0x80000e88]:sd t6, 1136(t1)
[0x80000e8c]:sd a1, 1144(t1)
[0x80000e90]:ld t5, 608(a0)
[0x80000e94]:addi fp, zero, 128
[0x80000e98]:csrrw zero, fcsr, fp
[0x80000e9c]:fcvt.wu.s t6, t5, dyn

[0x80000e9c]:fcvt.wu.s t6, t5, dyn
[0x80000ea0]:csrrs a1, fcsr, zero
[0x80000ea4]:sd t6, 1152(t1)
[0x80000ea8]:sd a1, 1160(t1)
[0x80000eac]:ld t5, 616(a0)
[0x80000eb0]:addi fp, zero, 0
[0x80000eb4]:csrrw zero, fcsr, fp
[0x80000eb8]:fcvt.wu.s t6, t5, dyn

[0x80000eb8]:fcvt.wu.s t6, t5, dyn
[0x80000ebc]:csrrs a1, fcsr, zero
[0x80000ec0]:sd t6, 1168(t1)
[0x80000ec4]:sd a1, 1176(t1)
[0x80000ec8]:ld t5, 624(a0)
[0x80000ecc]:addi fp, zero, 32
[0x80000ed0]:csrrw zero, fcsr, fp
[0x80000ed4]:fcvt.wu.s t6, t5, dyn

[0x80000ed4]:fcvt.wu.s t6, t5, dyn
[0x80000ed8]:csrrs a1, fcsr, zero
[0x80000edc]:sd t6, 1184(t1)
[0x80000ee0]:sd a1, 1192(t1)
[0x80000ee4]:ld t5, 632(a0)
[0x80000ee8]:addi fp, zero, 64
[0x80000eec]:csrrw zero, fcsr, fp
[0x80000ef0]:fcvt.wu.s t6, t5, dyn

[0x80000ef0]:fcvt.wu.s t6, t5, dyn
[0x80000ef4]:csrrs a1, fcsr, zero
[0x80000ef8]:sd t6, 1200(t1)
[0x80000efc]:sd a1, 1208(t1)
[0x80000f00]:ld t5, 640(a0)
[0x80000f04]:addi fp, zero, 96
[0x80000f08]:csrrw zero, fcsr, fp
[0x80000f0c]:fcvt.wu.s t6, t5, dyn

[0x80000f0c]:fcvt.wu.s t6, t5, dyn
[0x80000f10]:csrrs a1, fcsr, zero
[0x80000f14]:sd t6, 1216(t1)
[0x80000f18]:sd a1, 1224(t1)
[0x80000f1c]:ld t5, 648(a0)
[0x80000f20]:addi fp, zero, 128
[0x80000f24]:csrrw zero, fcsr, fp
[0x80000f28]:fcvt.wu.s t6, t5, dyn

[0x80000f28]:fcvt.wu.s t6, t5, dyn
[0x80000f2c]:csrrs a1, fcsr, zero
[0x80000f30]:sd t6, 1232(t1)
[0x80000f34]:sd a1, 1240(t1)
[0x80000f38]:ld t5, 656(a0)
[0x80000f3c]:addi fp, zero, 0
[0x80000f40]:csrrw zero, fcsr, fp
[0x80000f44]:fcvt.wu.s t6, t5, dyn

[0x80000f44]:fcvt.wu.s t6, t5, dyn
[0x80000f48]:csrrs a1, fcsr, zero
[0x80000f4c]:sd t6, 1248(t1)
[0x80000f50]:sd a1, 1256(t1)
[0x80000f54]:ld t5, 664(a0)
[0x80000f58]:addi fp, zero, 32
[0x80000f5c]:csrrw zero, fcsr, fp
[0x80000f60]:fcvt.wu.s t6, t5, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.wu.s', 'rs1 : x30', 'rd : x31', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.wu.s t6, t5, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
	-[0x800003c4]:sd tp, 8(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002520]:0x0000000000000001




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.wu.s t5, t6, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
	-[0x800003e0]:sd tp, 24(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002530]:0x0000000000000021




Last Coverpoint : ['rs1 : x28', 'rd : x29', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.wu.s t4, t3, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
	-[0x800003fc]:sd tp, 40(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002540]:0x0000000000000041




Last Coverpoint : ['rs1 : x29', 'rd : x28', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fcvt.wu.s t3, t4, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
	-[0x80000418]:sd tp, 56(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002550]:0x0000000000000061




Last Coverpoint : ['rs1 : x26', 'rd : x27', 'fs1 == 0 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000428]:fcvt.wu.s s11, s10, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
	-[0x80000434]:sd tp, 72(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002560]:0x0000000000000081




Last Coverpoint : ['rs1 : x27', 'rd : x26', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.wu.s s10, s11, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
	-[0x80000450]:sd tp, 88(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002570]:0x0000000000000001




Last Coverpoint : ['rs1 : x24', 'rd : x25', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000460]:fcvt.wu.s s9, s8, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
	-[0x8000046c]:sd tp, 104(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002580]:0x0000000000000021




Last Coverpoint : ['rs1 : x25', 'rd : x24', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.wu.s s8, s9, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
	-[0x80000488]:sd tp, 120(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002590]:0x0000000000000041




Last Coverpoint : ['rs1 : x22', 'rd : x23', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000498]:fcvt.wu.s s7, s6, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
	-[0x800004a4]:sd tp, 136(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800025a0]:0x0000000000000061




Last Coverpoint : ['rs1 : x23', 'rd : x22', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fcvt.wu.s s6, s7, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
	-[0x800004c0]:sd tp, 152(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800025b0]:0x0000000000000081




Last Coverpoint : ['rs1 : x20', 'rd : x21', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fcvt.wu.s s5, s4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
	-[0x800004dc]:sd tp, 168(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800025c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x21', 'rd : x20', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fcvt.wu.s s4, s5, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
	-[0x800004f8]:sd tp, 184(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800025d0]:0x0000000000000021




Last Coverpoint : ['rs1 : x18', 'rd : x19', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000508]:fcvt.wu.s s3, s2, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
	-[0x80000514]:sd tp, 200(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800025e0]:0x0000000000000050




Last Coverpoint : ['rs1 : x19', 'rd : x18', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000524]:fcvt.wu.s s2, s3, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
	-[0x80000530]:sd tp, 216(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800025f0]:0x0000000000000061




Last Coverpoint : ['rs1 : x16', 'rd : x17', 'fs1 == 1 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000540]:fcvt.wu.s a7, a6, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
	-[0x8000054c]:sd tp, 232(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002600]:0x0000000000000081




Last Coverpoint : ['rs1 : x17', 'rd : x16', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fcvt.wu.s a6, a7, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
	-[0x80000568]:sd tp, 248(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002610]:0x0000000000000001




Last Coverpoint : ['rs1 : x14', 'rd : x15', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000578]:fcvt.wu.s a5, a4, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
	-[0x80000584]:sd tp, 264(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002620]:0x0000000000000021




Last Coverpoint : ['rs1 : x15', 'rd : x14', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000594]:fcvt.wu.s a4, a5, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
	-[0x800005a0]:sd tp, 280(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002630]:0x0000000000000050




Last Coverpoint : ['rs1 : x12', 'rd : x13', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005b0]:fcvt.wu.s a3, a2, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
	-[0x800005bc]:sd tp, 296(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002640]:0x0000000000000061




Last Coverpoint : ['rs1 : x13', 'rd : x12', 'fs1 == 1 and fe1 == 0x7b and fm1 == 0x4ccccc and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005cc]:fcvt.wu.s a2, a3, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
	-[0x800005d8]:sd tp, 312(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002650]:0x0000000000000081




Last Coverpoint : ['rs1 : x10', 'rd : x11', 'fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005e8]:fcvt.wu.s a1, a0, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
	-[0x800005f4]:sd tp, 328(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002660]:0x0000000000000001




Last Coverpoint : ['rs1 : x11', 'rd : x10', 'fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000604]:fcvt.wu.s a0, a1, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
	-[0x80000610]:sd tp, 344(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002670]:0x0000000000000021




Last Coverpoint : ['rs1 : x8', 'rd : x9', 'fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000620]:fcvt.wu.s s1, fp, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
	-[0x8000062c]:sd tp, 360(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002680]:0x0000000000000041




Last Coverpoint : ['rs1 : x9', 'rd : x8', 'fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000644]:fcvt.wu.s fp, s1, dyn
	-[0x80000648]:csrrs a1, fcsr, zero
	-[0x8000064c]:sd fp, 368(ra)
	-[0x80000650]:sd a1, 376(ra)
Current Store : [0x80000650] : sd a1, 376(ra) -- Store: [0x80002690]:0x0000000000000061




Last Coverpoint : ['rs1 : x6', 'rd : x7', 'fs1 == 0 and fe1 == 0x78 and fm1 == 0x23d70a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000660]:fcvt.wu.s t2, t1, dyn
	-[0x80000664]:csrrs a1, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
	-[0x8000066c]:sd a1, 392(ra)
Current Store : [0x8000066c] : sd a1, 392(ra) -- Store: [0x800026a0]:0x0000000000000081




Last Coverpoint : ['rs1 : x7', 'rd : x6', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000067c]:fcvt.wu.s t1, t2, dyn
	-[0x80000680]:csrrs a1, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
	-[0x80000688]:sd a1, 408(ra)
Current Store : [0x80000688] : sd a1, 408(ra) -- Store: [0x800026b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x4', 'rd : x5', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000698]:fcvt.wu.s t0, tp, dyn
	-[0x8000069c]:csrrs a1, fcsr, zero
	-[0x800006a0]:sd t0, 416(ra)
	-[0x800006a4]:sd a1, 424(ra)
Current Store : [0x800006a4] : sd a1, 424(ra) -- Store: [0x800026c0]:0x0000000000000021




Last Coverpoint : ['rs1 : x5', 'rd : x4', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006bc]:fcvt.wu.s tp, t0, dyn
	-[0x800006c0]:csrrs a1, fcsr, zero
	-[0x800006c4]:sd tp, 0(t1)
	-[0x800006c8]:sd a1, 8(t1)
Current Store : [0x800006c8] : sd a1, 8(t1) -- Store: [0x800026d0]:0x0000000000000041




Last Coverpoint : ['rs1 : x2', 'rd : x3', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006d8]:fcvt.wu.s gp, sp, dyn
	-[0x800006dc]:csrrs a1, fcsr, zero
	-[0x800006e0]:sd gp, 16(t1)
	-[0x800006e4]:sd a1, 24(t1)
Current Store : [0x800006e4] : sd a1, 24(t1) -- Store: [0x800026e0]:0x0000000000000061




Last Coverpoint : ['rs1 : x3', 'rd : x2', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006f4]:fcvt.wu.s sp, gp, dyn
	-[0x800006f8]:csrrs a1, fcsr, zero
	-[0x800006fc]:sd sp, 32(t1)
	-[0x80000700]:sd a1, 40(t1)
Current Store : [0x80000700] : sd a1, 40(t1) -- Store: [0x800026f0]:0x0000000000000081




Last Coverpoint : ['rs1 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x80000710]:fcvt.wu.s ra, zero, dyn
	-[0x80000714]:csrrs a1, fcsr, zero
	-[0x80000718]:sd ra, 48(t1)
	-[0x8000071c]:sd a1, 56(t1)
Current Store : [0x8000071c] : sd a1, 56(t1) -- Store: [0x80002700]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fcvt.wu.s zero, ra, dyn
	-[0x80000730]:csrrs a1, fcsr, zero
	-[0x80000734]:sd zero, 64(t1)
	-[0x80000738]:sd a1, 72(t1)
Current Store : [0x80000738] : sd a1, 72(t1) -- Store: [0x80002710]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000748]:fcvt.wu.s t6, t5, dyn
	-[0x8000074c]:csrrs a1, fcsr, zero
	-[0x80000750]:sd t6, 80(t1)
	-[0x80000754]:sd a1, 88(t1)
Current Store : [0x80000754] : sd a1, 88(t1) -- Store: [0x80002720]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000764]:fcvt.wu.s t6, t5, dyn
	-[0x80000768]:csrrs a1, fcsr, zero
	-[0x8000076c]:sd t6, 96(t1)
	-[0x80000770]:sd a1, 104(t1)
Current Store : [0x80000770] : sd a1, 104(t1) -- Store: [0x80002730]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000780]:fcvt.wu.s t6, t5, dyn
	-[0x80000784]:csrrs a1, fcsr, zero
	-[0x80000788]:sd t6, 112(t1)
	-[0x8000078c]:sd a1, 120(t1)
Current Store : [0x8000078c] : sd a1, 120(t1) -- Store: [0x80002740]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000079c]:fcvt.wu.s t6, t5, dyn
	-[0x800007a0]:csrrs a1, fcsr, zero
	-[0x800007a4]:sd t6, 128(t1)
	-[0x800007a8]:sd a1, 136(t1)
Current Store : [0x800007a8] : sd a1, 136(t1) -- Store: [0x80002750]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007b8]:fcvt.wu.s t6, t5, dyn
	-[0x800007bc]:csrrs a1, fcsr, zero
	-[0x800007c0]:sd t6, 144(t1)
	-[0x800007c4]:sd a1, 152(t1)
Current Store : [0x800007c4] : sd a1, 152(t1) -- Store: [0x80002760]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007d4]:fcvt.wu.s t6, t5, dyn
	-[0x800007d8]:csrrs a1, fcsr, zero
	-[0x800007dc]:sd t6, 160(t1)
	-[0x800007e0]:sd a1, 168(t1)
Current Store : [0x800007e0] : sd a1, 168(t1) -- Store: [0x80002770]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007f0]:fcvt.wu.s t6, t5, dyn
	-[0x800007f4]:csrrs a1, fcsr, zero
	-[0x800007f8]:sd t6, 176(t1)
	-[0x800007fc]:sd a1, 184(t1)
Current Store : [0x800007fc] : sd a1, 184(t1) -- Store: [0x80002780]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fcvt.wu.s t6, t5, dyn
	-[0x80000810]:csrrs a1, fcsr, zero
	-[0x80000814]:sd t6, 192(t1)
	-[0x80000818]:sd a1, 200(t1)
Current Store : [0x80000818] : sd a1, 200(t1) -- Store: [0x80002790]:0x0000000000000081




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000828]:fcvt.wu.s t6, t5, dyn
	-[0x8000082c]:csrrs a1, fcsr, zero
	-[0x80000830]:sd t6, 208(t1)
	-[0x80000834]:sd a1, 216(t1)
Current Store : [0x80000834] : sd a1, 216(t1) -- Store: [0x800027a0]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000844]:fcvt.wu.s t6, t5, dyn
	-[0x80000848]:csrrs a1, fcsr, zero
	-[0x8000084c]:sd t6, 224(t1)
	-[0x80000850]:sd a1, 232(t1)
Current Store : [0x80000850] : sd a1, 232(t1) -- Store: [0x800027b0]:0x0000000000000030




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000860]:fcvt.wu.s t6, t5, dyn
	-[0x80000864]:csrrs a1, fcsr, zero
	-[0x80000868]:sd t6, 240(t1)
	-[0x8000086c]:sd a1, 248(t1)
Current Store : [0x8000086c] : sd a1, 248(t1) -- Store: [0x800027c0]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000087c]:fcvt.wu.s t6, t5, dyn
	-[0x80000880]:csrrs a1, fcsr, zero
	-[0x80000884]:sd t6, 256(t1)
	-[0x80000888]:sd a1, 264(t1)
Current Store : [0x80000888] : sd a1, 264(t1) -- Store: [0x800027d0]:0x0000000000000070




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000898]:fcvt.wu.s t6, t5, dyn
	-[0x8000089c]:csrrs a1, fcsr, zero
	-[0x800008a0]:sd t6, 272(t1)
	-[0x800008a4]:sd a1, 280(t1)
Current Store : [0x800008a4] : sd a1, 280(t1) -- Store: [0x800027e0]:0x0000000000000090




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008b4]:fcvt.wu.s t6, t5, dyn
	-[0x800008b8]:csrrs a1, fcsr, zero
	-[0x800008bc]:sd t6, 288(t1)
	-[0x800008c0]:sd a1, 296(t1)
Current Store : [0x800008c0] : sd a1, 296(t1) -- Store: [0x800027f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008d0]:fcvt.wu.s t6, t5, dyn
	-[0x800008d4]:csrrs a1, fcsr, zero
	-[0x800008d8]:sd t6, 304(t1)
	-[0x800008dc]:sd a1, 312(t1)
Current Store : [0x800008dc] : sd a1, 312(t1) -- Store: [0x80002800]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fcvt.wu.s t6, t5, dyn
	-[0x800008f0]:csrrs a1, fcsr, zero
	-[0x800008f4]:sd t6, 320(t1)
	-[0x800008f8]:sd a1, 328(t1)
Current Store : [0x800008f8] : sd a1, 328(t1) -- Store: [0x80002810]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000908]:fcvt.wu.s t6, t5, dyn
	-[0x8000090c]:csrrs a1, fcsr, zero
	-[0x80000910]:sd t6, 336(t1)
	-[0x80000914]:sd a1, 344(t1)
Current Store : [0x80000914] : sd a1, 344(t1) -- Store: [0x80002820]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000924]:fcvt.wu.s t6, t5, dyn
	-[0x80000928]:csrrs a1, fcsr, zero
	-[0x8000092c]:sd t6, 352(t1)
	-[0x80000930]:sd a1, 360(t1)
Current Store : [0x80000930] : sd a1, 360(t1) -- Store: [0x80002830]:0x0000000000000081




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000940]:fcvt.wu.s t6, t5, dyn
	-[0x80000944]:csrrs a1, fcsr, zero
	-[0x80000948]:sd t6, 368(t1)
	-[0x8000094c]:sd a1, 376(t1)
Current Store : [0x8000094c] : sd a1, 376(t1) -- Store: [0x80002840]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000095c]:fcvt.wu.s t6, t5, dyn
	-[0x80000960]:csrrs a1, fcsr, zero
	-[0x80000964]:sd t6, 384(t1)
	-[0x80000968]:sd a1, 392(t1)
Current Store : [0x80000968] : sd a1, 392(t1) -- Store: [0x80002850]:0x0000000000000030




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000978]:fcvt.wu.s t6, t5, dyn
	-[0x8000097c]:csrrs a1, fcsr, zero
	-[0x80000980]:sd t6, 400(t1)
	-[0x80000984]:sd a1, 408(t1)
Current Store : [0x80000984] : sd a1, 408(t1) -- Store: [0x80002860]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000994]:fcvt.wu.s t6, t5, dyn
	-[0x80000998]:csrrs a1, fcsr, zero
	-[0x8000099c]:sd t6, 416(t1)
	-[0x800009a0]:sd a1, 424(t1)
Current Store : [0x800009a0] : sd a1, 424(t1) -- Store: [0x80002870]:0x0000000000000070




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800009b0]:fcvt.wu.s t6, t5, dyn
	-[0x800009b4]:csrrs a1, fcsr, zero
	-[0x800009b8]:sd t6, 432(t1)
	-[0x800009bc]:sd a1, 440(t1)
Current Store : [0x800009bc] : sd a1, 440(t1) -- Store: [0x80002880]:0x0000000000000090




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fcvt.wu.s t6, t5, dyn
	-[0x800009d0]:csrrs a1, fcsr, zero
	-[0x800009d4]:sd t6, 448(t1)
	-[0x800009d8]:sd a1, 456(t1)
Current Store : [0x800009d8] : sd a1, 456(t1) -- Store: [0x80002890]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009e8]:fcvt.wu.s t6, t5, dyn
	-[0x800009ec]:csrrs a1, fcsr, zero
	-[0x800009f0]:sd t6, 464(t1)
	-[0x800009f4]:sd a1, 472(t1)
Current Store : [0x800009f4] : sd a1, 472(t1) -- Store: [0x800028a0]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a04]:fcvt.wu.s t6, t5, dyn
	-[0x80000a08]:csrrs a1, fcsr, zero
	-[0x80000a0c]:sd t6, 480(t1)
	-[0x80000a10]:sd a1, 488(t1)
Current Store : [0x80000a10] : sd a1, 488(t1) -- Store: [0x800028b0]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a20]:fcvt.wu.s t6, t5, dyn
	-[0x80000a24]:csrrs a1, fcsr, zero
	-[0x80000a28]:sd t6, 496(t1)
	-[0x80000a2c]:sd a1, 504(t1)
Current Store : [0x80000a2c] : sd a1, 504(t1) -- Store: [0x800028c0]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a3c]:fcvt.wu.s t6, t5, dyn
	-[0x80000a40]:csrrs a1, fcsr, zero
	-[0x80000a44]:sd t6, 512(t1)
	-[0x80000a48]:sd a1, 520(t1)
Current Store : [0x80000a48] : sd a1, 520(t1) -- Store: [0x800028d0]:0x0000000000000081




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a58]:fcvt.wu.s t6, t5, dyn
	-[0x80000a5c]:csrrs a1, fcsr, zero
	-[0x80000a60]:sd t6, 528(t1)
	-[0x80000a64]:sd a1, 536(t1)
Current Store : [0x80000a64] : sd a1, 536(t1) -- Store: [0x800028e0]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a74]:fcvt.wu.s t6, t5, dyn
	-[0x80000a78]:csrrs a1, fcsr, zero
	-[0x80000a7c]:sd t6, 544(t1)
	-[0x80000a80]:sd a1, 552(t1)
Current Store : [0x80000a80] : sd a1, 552(t1) -- Store: [0x800028f0]:0x0000000000000021




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a90]:fcvt.wu.s t6, t5, dyn
	-[0x80000a94]:csrrs a1, fcsr, zero
	-[0x80000a98]:sd t6, 560(t1)
	-[0x80000a9c]:sd a1, 568(t1)
Current Store : [0x80000a9c] : sd a1, 568(t1) -- Store: [0x80002900]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fcvt.wu.s t6, t5, dyn
	-[0x80000ab0]:csrrs a1, fcsr, zero
	-[0x80000ab4]:sd t6, 576(t1)
	-[0x80000ab8]:sd a1, 584(t1)
Current Store : [0x80000ab8] : sd a1, 584(t1) -- Store: [0x80002910]:0x0000000000000061




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x666666 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ac8]:fcvt.wu.s t6, t5, dyn
	-[0x80000acc]:csrrs a1, fcsr, zero
	-[0x80000ad0]:sd t6, 592(t1)
	-[0x80000ad4]:sd a1, 600(t1)
Current Store : [0x80000ad4] : sd a1, 600(t1) -- Store: [0x80002920]:0x0000000000000090




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ae4]:fcvt.wu.s t6, t5, dyn
	-[0x80000ae8]:csrrs a1, fcsr, zero
	-[0x80000aec]:sd t6, 608(t1)
	-[0x80000af0]:sd a1, 616(t1)
Current Store : [0x80000af0] : sd a1, 616(t1) -- Store: [0x80002930]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b00]:fcvt.wu.s t6, t5, dyn
	-[0x80000b04]:csrrs a1, fcsr, zero
	-[0x80000b08]:sd t6, 624(t1)
	-[0x80000b0c]:sd a1, 632(t1)
Current Store : [0x80000b0c] : sd a1, 632(t1) -- Store: [0x80002940]:0x0000000000000030




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b1c]:fcvt.wu.s t6, t5, dyn
	-[0x80000b20]:csrrs a1, fcsr, zero
	-[0x80000b24]:sd t6, 640(t1)
	-[0x80000b28]:sd a1, 648(t1)
Current Store : [0x80000b28] : sd a1, 648(t1) -- Store: [0x80002950]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b38]:fcvt.wu.s t6, t5, dyn
	-[0x80000b3c]:csrrs a1, fcsr, zero
	-[0x80000b40]:sd t6, 656(t1)
	-[0x80000b44]:sd a1, 664(t1)
Current Store : [0x80000b44] : sd a1, 664(t1) -- Store: [0x80002960]:0x0000000000000070




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0ccccc and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b54]:fcvt.wu.s t6, t5, dyn
	-[0x80000b58]:csrrs a1, fcsr, zero
	-[0x80000b5c]:sd t6, 672(t1)
	-[0x80000b60]:sd a1, 680(t1)
Current Store : [0x80000b60] : sd a1, 680(t1) -- Store: [0x80002970]:0x0000000000000090




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b70]:fcvt.wu.s t6, t5, dyn
	-[0x80000b74]:csrrs a1, fcsr, zero
	-[0x80000b78]:sd t6, 688(t1)
	-[0x80000b7c]:sd a1, 696(t1)
Current Store : [0x80000b7c] : sd a1, 696(t1) -- Store: [0x80002980]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fcvt.wu.s t6, t5, dyn
	-[0x80000b90]:csrrs a1, fcsr, zero
	-[0x80000b94]:sd t6, 704(t1)
	-[0x80000b98]:sd a1, 712(t1)
Current Store : [0x80000b98] : sd a1, 712(t1) -- Store: [0x80002990]:0x0000000000000021




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ba8]:fcvt.wu.s t6, t5, dyn
	-[0x80000bac]:csrrs a1, fcsr, zero
	-[0x80000bb0]:sd t6, 720(t1)
	-[0x80000bb4]:sd a1, 728(t1)
Current Store : [0x80000bb4] : sd a1, 728(t1) -- Store: [0x800029a0]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bc4]:fcvt.wu.s t6, t5, dyn
	-[0x80000bc8]:csrrs a1, fcsr, zero
	-[0x80000bcc]:sd t6, 736(t1)
	-[0x80000bd0]:sd a1, 744(t1)
Current Store : [0x80000bd0] : sd a1, 744(t1) -- Store: [0x800029b0]:0x0000000000000061




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x63d70a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000be0]:fcvt.wu.s t6, t5, dyn
	-[0x80000be4]:csrrs a1, fcsr, zero
	-[0x80000be8]:sd t6, 752(t1)
	-[0x80000bec]:sd a1, 760(t1)
Current Store : [0x80000bec] : sd a1, 760(t1) -- Store: [0x800029c0]:0x0000000000000090




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bfc]:fcvt.wu.s t6, t5, dyn
	-[0x80000c00]:csrrs a1, fcsr, zero
	-[0x80000c04]:sd t6, 768(t1)
	-[0x80000c08]:sd a1, 776(t1)
Current Store : [0x80000c08] : sd a1, 776(t1) -- Store: [0x800029d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c18]:fcvt.wu.s t6, t5, dyn
	-[0x80000c1c]:csrrs a1, fcsr, zero
	-[0x80000c20]:sd t6, 784(t1)
	-[0x80000c24]:sd a1, 792(t1)
Current Store : [0x80000c24] : sd a1, 792(t1) -- Store: [0x800029e0]:0x0000000000000021




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c34]:fcvt.wu.s t6, t5, dyn
	-[0x80000c38]:csrrs a1, fcsr, zero
	-[0x80000c3c]:sd t6, 800(t1)
	-[0x80000c40]:sd a1, 808(t1)
Current Store : [0x80000c40] : sd a1, 808(t1) -- Store: [0x800029f0]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c50]:fcvt.wu.s t6, t5, dyn
	-[0x80000c54]:csrrs a1, fcsr, zero
	-[0x80000c58]:sd t6, 816(t1)
	-[0x80000c5c]:sd a1, 824(t1)
Current Store : [0x80000c5c] : sd a1, 824(t1) -- Store: [0x80002a00]:0x0000000000000061




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7b and fm1 == 0x6147ae and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fcvt.wu.s t6, t5, dyn
	-[0x80000c70]:csrrs a1, fcsr, zero
	-[0x80000c74]:sd t6, 832(t1)
	-[0x80000c78]:sd a1, 840(t1)
Current Store : [0x80000c78] : sd a1, 840(t1) -- Store: [0x80002a10]:0x0000000000000081




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c88]:fcvt.wu.s t6, t5, dyn
	-[0x80000c8c]:csrrs a1, fcsr, zero
	-[0x80000c90]:sd t6, 848(t1)
	-[0x80000c94]:sd a1, 856(t1)
Current Store : [0x80000c94] : sd a1, 856(t1) -- Store: [0x80002a20]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ca4]:fcvt.wu.s t6, t5, dyn
	-[0x80000ca8]:csrrs a1, fcsr, zero
	-[0x80000cac]:sd t6, 864(t1)
	-[0x80000cb0]:sd a1, 872(t1)
Current Store : [0x80000cb0] : sd a1, 872(t1) -- Store: [0x80002a30]:0x0000000000000021




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cc0]:fcvt.wu.s t6, t5, dyn
	-[0x80000cc4]:csrrs a1, fcsr, zero
	-[0x80000cc8]:sd t6, 880(t1)
	-[0x80000ccc]:sd a1, 888(t1)
Current Store : [0x80000ccc] : sd a1, 888(t1) -- Store: [0x80002a40]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cdc]:fcvt.wu.s t6, t5, dyn
	-[0x80000ce0]:csrrs a1, fcsr, zero
	-[0x80000ce4]:sd t6, 896(t1)
	-[0x80000ce8]:sd a1, 904(t1)
Current Store : [0x80000ce8] : sd a1, 904(t1) -- Store: [0x80002a50]:0x0000000000000061




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cf8]:fcvt.wu.s t6, t5, dyn
	-[0x80000cfc]:csrrs a1, fcsr, zero
	-[0x80000d00]:sd t6, 912(t1)
	-[0x80000d04]:sd a1, 920(t1)
Current Store : [0x80000d04] : sd a1, 920(t1) -- Store: [0x80002a60]:0x0000000000000090




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d14]:fcvt.wu.s t6, t5, dyn
	-[0x80000d18]:csrrs a1, fcsr, zero
	-[0x80000d1c]:sd t6, 928(t1)
	-[0x80000d20]:sd a1, 936(t1)
Current Store : [0x80000d20] : sd a1, 936(t1) -- Store: [0x80002a70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d30]:fcvt.wu.s t6, t5, dyn
	-[0x80000d34]:csrrs a1, fcsr, zero
	-[0x80000d38]:sd t6, 944(t1)
	-[0x80000d3c]:sd a1, 952(t1)
Current Store : [0x80000d3c] : sd a1, 952(t1) -- Store: [0x80002a80]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fcvt.wu.s t6, t5, dyn
	-[0x80000d50]:csrrs a1, fcsr, zero
	-[0x80000d54]:sd t6, 960(t1)
	-[0x80000d58]:sd a1, 968(t1)
Current Store : [0x80000d58] : sd a1, 968(t1) -- Store: [0x80002a90]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d68]:fcvt.wu.s t6, t5, dyn
	-[0x80000d6c]:csrrs a1, fcsr, zero
	-[0x80000d70]:sd t6, 976(t1)
	-[0x80000d74]:sd a1, 984(t1)
Current Store : [0x80000d74] : sd a1, 984(t1) -- Store: [0x80002aa0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x000000 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d84]:fcvt.wu.s t6, t5, dyn
	-[0x80000d88]:csrrs a1, fcsr, zero
	-[0x80000d8c]:sd t6, 992(t1)
	-[0x80000d90]:sd a1, 1000(t1)
Current Store : [0x80000d90] : sd a1, 1000(t1) -- Store: [0x80002ab0]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000da0]:fcvt.wu.s t6, t5, dyn
	-[0x80000da4]:csrrs a1, fcsr, zero
	-[0x80000da8]:sd t6, 1008(t1)
	-[0x80000dac]:sd a1, 1016(t1)
Current Store : [0x80000dac] : sd a1, 1016(t1) -- Store: [0x80002ac0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dbc]:fcvt.wu.s t6, t5, dyn
	-[0x80000dc0]:csrrs a1, fcsr, zero
	-[0x80000dc4]:sd t6, 1024(t1)
	-[0x80000dc8]:sd a1, 1032(t1)
Current Store : [0x80000dc8] : sd a1, 1032(t1) -- Store: [0x80002ad0]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dd8]:fcvt.wu.s t6, t5, dyn
	-[0x80000ddc]:csrrs a1, fcsr, zero
	-[0x80000de0]:sd t6, 1040(t1)
	-[0x80000de4]:sd a1, 1048(t1)
Current Store : [0x80000de4] : sd a1, 1048(t1) -- Store: [0x80002ae0]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000df4]:fcvt.wu.s t6, t5, dyn
	-[0x80000df8]:csrrs a1, fcsr, zero
	-[0x80000dfc]:sd t6, 1056(t1)
	-[0x80000e00]:sd a1, 1064(t1)
Current Store : [0x80000e00] : sd a1, 1064(t1) -- Store: [0x80002af0]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7e and fm1 == 0x7d70a3 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e10]:fcvt.wu.s t6, t5, dyn
	-[0x80000e14]:csrrs a1, fcsr, zero
	-[0x80000e18]:sd t6, 1072(t1)
	-[0x80000e1c]:sd a1, 1080(t1)
Current Store : [0x80000e1c] : sd a1, 1080(t1) -- Store: [0x80002b00]:0x0000000000000081




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fcvt.wu.s t6, t5, dyn
	-[0x80000e30]:csrrs a1, fcsr, zero
	-[0x80000e34]:sd t6, 1088(t1)
	-[0x80000e38]:sd a1, 1096(t1)
Current Store : [0x80000e38] : sd a1, 1096(t1) -- Store: [0x80002b10]:0x0000000000000010




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e48]:fcvt.wu.s t6, t5, dyn
	-[0x80000e4c]:csrrs a1, fcsr, zero
	-[0x80000e50]:sd t6, 1104(t1)
	-[0x80000e54]:sd a1, 1112(t1)
Current Store : [0x80000e54] : sd a1, 1112(t1) -- Store: [0x80002b20]:0x0000000000000030




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e64]:fcvt.wu.s t6, t5, dyn
	-[0x80000e68]:csrrs a1, fcsr, zero
	-[0x80000e6c]:sd t6, 1120(t1)
	-[0x80000e70]:sd a1, 1128(t1)
Current Store : [0x80000e70] : sd a1, 1128(t1) -- Store: [0x80002b30]:0x0000000000000050




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e80]:fcvt.wu.s t6, t5, dyn
	-[0x80000e84]:csrrs a1, fcsr, zero
	-[0x80000e88]:sd t6, 1136(t1)
	-[0x80000e8c]:sd a1, 1144(t1)
Current Store : [0x80000e8c] : sd a1, 1144(t1) -- Store: [0x80002b40]:0x0000000000000070




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x0147ae and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e9c]:fcvt.wu.s t6, t5, dyn
	-[0x80000ea0]:csrrs a1, fcsr, zero
	-[0x80000ea4]:sd t6, 1152(t1)
	-[0x80000ea8]:sd a1, 1160(t1)
Current Store : [0x80000ea8] : sd a1, 1160(t1) -- Store: [0x80002b50]:0x0000000000000090




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000eb8]:fcvt.wu.s t6, t5, dyn
	-[0x80000ebc]:csrrs a1, fcsr, zero
	-[0x80000ec0]:sd t6, 1168(t1)
	-[0x80000ec4]:sd a1, 1176(t1)
Current Store : [0x80000ec4] : sd a1, 1176(t1) -- Store: [0x80002b60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ed4]:fcvt.wu.s t6, t5, dyn
	-[0x80000ed8]:csrrs a1, fcsr, zero
	-[0x80000edc]:sd t6, 1184(t1)
	-[0x80000ee0]:sd a1, 1192(t1)
Current Store : [0x80000ee0] : sd a1, 1192(t1) -- Store: [0x80002b70]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ef0]:fcvt.wu.s t6, t5, dyn
	-[0x80000ef4]:csrrs a1, fcsr, zero
	-[0x80000ef8]:sd t6, 1200(t1)
	-[0x80000efc]:sd a1, 1208(t1)
Current Store : [0x80000efc] : sd a1, 1208(t1) -- Store: [0x80002b80]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fcvt.wu.s t6, t5, dyn
	-[0x80000f10]:csrrs a1, fcsr, zero
	-[0x80000f14]:sd t6, 1216(t1)
	-[0x80000f18]:sd a1, 1224(t1)
Current Store : [0x80000f18] : sd a1, 1224(t1) -- Store: [0x80002b90]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x0e147a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f28]:fcvt.wu.s t6, t5, dyn
	-[0x80000f2c]:csrrs a1, fcsr, zero
	-[0x80000f30]:sd t6, 1232(t1)
	-[0x80000f34]:sd a1, 1240(t1)
Current Store : [0x80000f34] : sd a1, 1240(t1) -- Store: [0x80002ba0]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f44]:fcvt.wu.s t6, t5, dyn
	-[0x80000f48]:csrrs a1, fcsr, zero
	-[0x80000f4c]:sd t6, 1248(t1)
	-[0x80000f50]:sd a1, 1256(t1)
Current Store : [0x80000f50] : sd a1, 1256(t1) -- Store: [0x80002bb0]:0x0000000000000001




Last Coverpoint : ['mnemonic : fcvt.wu.s', 'rs1 : x30', 'rd : x31', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f60]:fcvt.wu.s t6, t5, dyn
	-[0x80000f64]:csrrs a1, fcsr, zero
	-[0x80000f68]:sd t6, 1264(t1)
	-[0x80000f6c]:sd a1, 1272(t1)
Current Store : [0x80000f6c] : sd a1, 1272(t1) -- Store: [0x80002bc0]:0x0000000000000021





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
