
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000740')]      |
| SIG_REGION                | [('0x80002310', '0x80002520', '66 dwords')]      |
| COV_LABELS                | fcvt.wu.s_b27      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fcvt.wu.s/work-fcvtwus/fcvt.wu.s_b27-01.S/ref.S    |
| Total Number of coverpoints| 73     |
| Total Coverpoints Hit     | 73      |
| Total Signature Updates   | 64      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 32     |
| STAT4                     | 32     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003b8]:fcvt.wu.s t6, t5, dyn
[0x800003bc]:csrrs tp, fcsr, zero
[0x800003c0]:sd t6, 0(ra)
[0x800003c4]:sd tp, 8(ra)
[0x800003c8]:ld t6, 8(gp)
[0x800003cc]:addi sp, zero, 0
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fcvt.wu.s t5, t6, dyn

[0x800003d4]:fcvt.wu.s t5, t6, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:sd t5, 16(ra)
[0x800003e0]:sd tp, 24(ra)
[0x800003e4]:ld t3, 16(gp)
[0x800003e8]:addi sp, zero, 0
[0x800003ec]:csrrw zero, fcsr, sp
[0x800003f0]:fcvt.wu.s t4, t3, dyn

[0x800003f0]:fcvt.wu.s t4, t3, dyn
[0x800003f4]:csrrs tp, fcsr, zero
[0x800003f8]:sd t4, 32(ra)
[0x800003fc]:sd tp, 40(ra)
[0x80000400]:ld t4, 24(gp)
[0x80000404]:addi sp, zero, 0
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fcvt.wu.s t3, t4, dyn

[0x8000040c]:fcvt.wu.s t3, t4, dyn
[0x80000410]:csrrs tp, fcsr, zero
[0x80000414]:sd t3, 48(ra)
[0x80000418]:sd tp, 56(ra)
[0x8000041c]:ld s10, 32(gp)
[0x80000420]:addi sp, zero, 0
[0x80000424]:csrrw zero, fcsr, sp
[0x80000428]:fcvt.wu.s s11, s10, dyn

[0x80000428]:fcvt.wu.s s11, s10, dyn
[0x8000042c]:csrrs tp, fcsr, zero
[0x80000430]:sd s11, 64(ra)
[0x80000434]:sd tp, 72(ra)
[0x80000438]:ld s11, 40(gp)
[0x8000043c]:addi sp, zero, 0
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fcvt.wu.s s10, s11, dyn

[0x80000444]:fcvt.wu.s s10, s11, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:sd s10, 80(ra)
[0x80000450]:sd tp, 88(ra)
[0x80000454]:ld s8, 48(gp)
[0x80000458]:addi sp, zero, 0
[0x8000045c]:csrrw zero, fcsr, sp
[0x80000460]:fcvt.wu.s s9, s8, dyn

[0x80000460]:fcvt.wu.s s9, s8, dyn
[0x80000464]:csrrs tp, fcsr, zero
[0x80000468]:sd s9, 96(ra)
[0x8000046c]:sd tp, 104(ra)
[0x80000470]:ld s9, 56(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fcvt.wu.s s8, s9, dyn

[0x8000047c]:fcvt.wu.s s8, s9, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s8, 112(ra)
[0x80000488]:sd tp, 120(ra)
[0x8000048c]:ld s6, 64(gp)
[0x80000490]:addi sp, zero, 0
[0x80000494]:csrrw zero, fcsr, sp
[0x80000498]:fcvt.wu.s s7, s6, dyn

[0x80000498]:fcvt.wu.s s7, s6, dyn
[0x8000049c]:csrrs tp, fcsr, zero
[0x800004a0]:sd s7, 128(ra)
[0x800004a4]:sd tp, 136(ra)
[0x800004a8]:ld s7, 72(gp)
[0x800004ac]:addi sp, zero, 0
[0x800004b0]:csrrw zero, fcsr, sp
[0x800004b4]:fcvt.wu.s s6, s7, dyn

[0x800004b4]:fcvt.wu.s s6, s7, dyn
[0x800004b8]:csrrs tp, fcsr, zero
[0x800004bc]:sd s6, 144(ra)
[0x800004c0]:sd tp, 152(ra)
[0x800004c4]:ld s4, 80(gp)
[0x800004c8]:addi sp, zero, 0
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fcvt.wu.s s5, s4, dyn

[0x800004d0]:fcvt.wu.s s5, s4, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:sd s5, 160(ra)
[0x800004dc]:sd tp, 168(ra)
[0x800004e0]:ld s5, 88(gp)
[0x800004e4]:addi sp, zero, 0
[0x800004e8]:csrrw zero, fcsr, sp
[0x800004ec]:fcvt.wu.s s4, s5, dyn

[0x800004ec]:fcvt.wu.s s4, s5, dyn
[0x800004f0]:csrrs tp, fcsr, zero
[0x800004f4]:sd s4, 176(ra)
[0x800004f8]:sd tp, 184(ra)
[0x800004fc]:ld s2, 96(gp)
[0x80000500]:addi sp, zero, 0
[0x80000504]:csrrw zero, fcsr, sp
[0x80000508]:fcvt.wu.s s3, s2, dyn

[0x80000508]:fcvt.wu.s s3, s2, dyn
[0x8000050c]:csrrs tp, fcsr, zero
[0x80000510]:sd s3, 192(ra)
[0x80000514]:sd tp, 200(ra)
[0x80000518]:ld s3, 104(gp)
[0x8000051c]:addi sp, zero, 0
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fcvt.wu.s s2, s3, dyn

[0x80000524]:fcvt.wu.s s2, s3, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:sd s2, 208(ra)
[0x80000530]:sd tp, 216(ra)
[0x80000534]:ld a6, 112(gp)
[0x80000538]:addi sp, zero, 0
[0x8000053c]:csrrw zero, fcsr, sp
[0x80000540]:fcvt.wu.s a7, a6, dyn

[0x80000540]:fcvt.wu.s a7, a6, dyn
[0x80000544]:csrrs tp, fcsr, zero
[0x80000548]:sd a7, 224(ra)
[0x8000054c]:sd tp, 232(ra)
[0x80000550]:ld a7, 120(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fcvt.wu.s a6, a7, dyn

[0x8000055c]:fcvt.wu.s a6, a7, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd a6, 240(ra)
[0x80000568]:sd tp, 248(ra)
[0x8000056c]:ld a4, 128(gp)
[0x80000570]:addi sp, zero, 0
[0x80000574]:csrrw zero, fcsr, sp
[0x80000578]:fcvt.wu.s a5, a4, dyn

[0x80000578]:fcvt.wu.s a5, a4, dyn
[0x8000057c]:csrrs tp, fcsr, zero
[0x80000580]:sd a5, 256(ra)
[0x80000584]:sd tp, 264(ra)
[0x80000588]:ld a5, 136(gp)
[0x8000058c]:addi sp, zero, 0
[0x80000590]:csrrw zero, fcsr, sp
[0x80000594]:fcvt.wu.s a4, a5, dyn

[0x80000594]:fcvt.wu.s a4, a5, dyn
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a2, 144(gp)
[0x800005a8]:addi sp, zero, 0
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fcvt.wu.s a3, a2, dyn

[0x800005b0]:fcvt.wu.s a3, a2, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a3, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fcvt.wu.s a2, a3, dyn

[0x800005cc]:fcvt.wu.s a2, a3, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a0, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fcvt.wu.s a1, a0, dyn

[0x800005e8]:fcvt.wu.s a1, a0, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld a1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fcvt.wu.s a0, a1, dyn

[0x80000604]:fcvt.wu.s a0, a1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fcvt.wu.s s1, fp, dyn

[0x80000620]:fcvt.wu.s s1, fp, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fcvt.wu.s fp, s1, dyn

[0x80000644]:fcvt.wu.s fp, s1, dyn
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fcvt.wu.s t2, t1, dyn

[0x80000660]:fcvt.wu.s t2, t1, dyn
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fcvt.wu.s t1, t2, dyn

[0x8000067c]:fcvt.wu.s t1, t2, dyn
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fcvt.wu.s t0, tp, dyn

[0x80000698]:fcvt.wu.s t0, tp, dyn
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3616
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fcvt.wu.s tp, t0, dyn

[0x800006bc]:fcvt.wu.s tp, t0, dyn
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fcvt.wu.s gp, sp, dyn

[0x800006d8]:fcvt.wu.s gp, sp, dyn
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fcvt.wu.s sp, gp, dyn

[0x800006f4]:fcvt.wu.s sp, gp, dyn
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fcvt.wu.s ra, zero, dyn

[0x80000710]:fcvt.wu.s ra, zero, dyn
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fcvt.wu.s zero, ra, dyn

[0x8000072c]:fcvt.wu.s zero, ra, dyn
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fcvt.wu.s', 'rs1 : x30', 'rd : x31', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fcvt.wu.s t6, t5, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
	-[0x800003c4]:sd tp, 8(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002320]:0x0000000000000010




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x000001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fcvt.wu.s t5, t6, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
	-[0x800003e0]:sd tp, 24(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002330]:0x0000000000000010




Last Coverpoint : ['rs1 : x28', 'rd : x29', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x2aaaaa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fcvt.wu.s t4, t3, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
	-[0x800003fc]:sd tp, 40(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002340]:0x0000000000000010




Last Coverpoint : ['rs1 : x29', 'rd : x28', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x2aaaaa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fcvt.wu.s t3, t4, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
	-[0x80000418]:sd tp, 56(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002350]:0x0000000000000010




Last Coverpoint : ['rs1 : x26', 'rd : x27', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000428]:fcvt.wu.s s11, s10, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
	-[0x80000434]:sd tp, 72(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002360]:0x0000000000000010




Last Coverpoint : ['rs1 : x27', 'rd : x26', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x400001 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fcvt.wu.s s10, s11, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
	-[0x80000450]:sd tp, 88(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002370]:0x0000000000000010




Last Coverpoint : ['rs1 : x24', 'rd : x25', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x455555 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000460]:fcvt.wu.s s9, s8, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
	-[0x8000046c]:sd tp, 104(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002380]:0x0000000000000010




Last Coverpoint : ['rs1 : x25', 'rd : x24', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x455555 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fcvt.wu.s s8, s9, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
	-[0x80000488]:sd tp, 120(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002390]:0x0000000000000010




Last Coverpoint : ['rs1 : x22', 'rd : x23']
Last Code Sequence : 
	-[0x80000498]:fcvt.wu.s s7, s6, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
	-[0x800004a4]:sd tp, 136(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800023a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rd : x22']
Last Code Sequence : 
	-[0x800004b4]:fcvt.wu.s s6, s7, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
	-[0x800004c0]:sd tp, 152(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rd : x21']
Last Code Sequence : 
	-[0x800004d0]:fcvt.wu.s s5, s4, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
	-[0x800004dc]:sd tp, 168(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rd : x20']
Last Code Sequence : 
	-[0x800004ec]:fcvt.wu.s s4, s5, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
	-[0x800004f8]:sd tp, 184(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rd : x19']
Last Code Sequence : 
	-[0x80000508]:fcvt.wu.s s3, s2, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
	-[0x80000514]:sd tp, 200(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : x18']
Last Code Sequence : 
	-[0x80000524]:fcvt.wu.s s2, s3, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
	-[0x80000530]:sd tp, 216(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : x17']
Last Code Sequence : 
	-[0x80000540]:fcvt.wu.s a7, a6, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
	-[0x8000054c]:sd tp, 232(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rd : x16']
Last Code Sequence : 
	-[0x8000055c]:fcvt.wu.s a6, a7, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
	-[0x80000568]:sd tp, 248(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : x15']
Last Code Sequence : 
	-[0x80000578]:fcvt.wu.s a5, a4, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
	-[0x80000584]:sd tp, 264(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rd : x14']
Last Code Sequence : 
	-[0x80000594]:fcvt.wu.s a4, a5, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
	-[0x800005a0]:sd tp, 280(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002430]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rd : x13']
Last Code Sequence : 
	-[0x800005b0]:fcvt.wu.s a3, a2, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
	-[0x800005bc]:sd tp, 296(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002440]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : x12']
Last Code Sequence : 
	-[0x800005cc]:fcvt.wu.s a2, a3, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
	-[0x800005d8]:sd tp, 312(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002450]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rd : x11']
Last Code Sequence : 
	-[0x800005e8]:fcvt.wu.s a1, a0, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
	-[0x800005f4]:sd tp, 328(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002460]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rd : x10']
Last Code Sequence : 
	-[0x80000604]:fcvt.wu.s a0, a1, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
	-[0x80000610]:sd tp, 344(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002470]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rd : x9']
Last Code Sequence : 
	-[0x80000620]:fcvt.wu.s s1, fp, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
	-[0x8000062c]:sd tp, 360(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002480]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rd : x8']
Last Code Sequence : 
	-[0x80000644]:fcvt.wu.s fp, s1, dyn
	-[0x80000648]:csrrs a1, fcsr, zero
	-[0x8000064c]:sd fp, 368(ra)
	-[0x80000650]:sd a1, 376(ra)
Current Store : [0x80000650] : sd a1, 376(ra) -- Store: [0x80002490]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rd : x7']
Last Code Sequence : 
	-[0x80000660]:fcvt.wu.s t2, t1, dyn
	-[0x80000664]:csrrs a1, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
	-[0x8000066c]:sd a1, 392(ra)
Current Store : [0x8000066c] : sd a1, 392(ra) -- Store: [0x800024a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rd : x6']
Last Code Sequence : 
	-[0x8000067c]:fcvt.wu.s t1, t2, dyn
	-[0x80000680]:csrrs a1, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
	-[0x80000688]:sd a1, 408(ra)
Current Store : [0x80000688] : sd a1, 408(ra) -- Store: [0x800024b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rd : x5']
Last Code Sequence : 
	-[0x80000698]:fcvt.wu.s t0, tp, dyn
	-[0x8000069c]:csrrs a1, fcsr, zero
	-[0x800006a0]:sd t0, 416(ra)
	-[0x800006a4]:sd a1, 424(ra)
Current Store : [0x800006a4] : sd a1, 424(ra) -- Store: [0x800024c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rd : x4']
Last Code Sequence : 
	-[0x800006bc]:fcvt.wu.s tp, t0, dyn
	-[0x800006c0]:csrrs a1, fcsr, zero
	-[0x800006c4]:sd tp, 0(t1)
	-[0x800006c8]:sd a1, 8(t1)
Current Store : [0x800006c8] : sd a1, 8(t1) -- Store: [0x800024d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : x3']
Last Code Sequence : 
	-[0x800006d8]:fcvt.wu.s gp, sp, dyn
	-[0x800006dc]:csrrs a1, fcsr, zero
	-[0x800006e0]:sd gp, 16(t1)
	-[0x800006e4]:sd a1, 24(t1)
Current Store : [0x800006e4] : sd a1, 24(t1) -- Store: [0x800024e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rd : x2']
Last Code Sequence : 
	-[0x800006f4]:fcvt.wu.s sp, gp, dyn
	-[0x800006f8]:csrrs a1, fcsr, zero
	-[0x800006fc]:sd sp, 32(t1)
	-[0x80000700]:sd a1, 40(t1)
Current Store : [0x80000700] : sd a1, 40(t1) -- Store: [0x800024f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rd : x1']
Last Code Sequence : 
	-[0x80000710]:fcvt.wu.s ra, zero, dyn
	-[0x80000714]:csrrs a1, fcsr, zero
	-[0x80000718]:sd ra, 48(t1)
	-[0x8000071c]:sd a1, 56(t1)
Current Store : [0x8000071c] : sd a1, 56(t1) -- Store: [0x80002500]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : x0']
Last Code Sequence : 
	-[0x8000072c]:fcvt.wu.s zero, ra, dyn
	-[0x80000730]:csrrs a1, fcsr, zero
	-[0x80000734]:sd zero, 64(t1)
	-[0x80000738]:sd a1, 72(t1)
Current Store : [0x80000738] : sd a1, 72(t1) -- Store: [0x80002510]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
