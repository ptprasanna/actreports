
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x800015e0')]      |
| SIG_REGION                | [('0x80003b10', '0x80004430', '292 dwords')]      |
| COV_LABELS                | fadd_b4      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fadd/work-fadd/fadd_b4-01.S/ref.S    |
| Total Number of coverpoints| 242     |
| Total Coverpoints Hit     | 242      |
| Total Signature Updates   | 290      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 144     |
| STAT4                     | 145     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x800015cc]:fadd.s t6, t5, t4, dyn
      [0x800015d0]:csrrs a2, fcsr, zero
      [0x800015d4]:sd t6, 1904(fp)
      [0x800015d8]:sd a2, 1912(fp)
      [0x800015dc]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80004418 Data: 0x000000007F400000
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat






```

## Details of STAT3

```
[0x800003bc]:fadd.s t6, t6, t6, dyn
[0x800003c0]:csrrs tp, fcsr, zero
[0x800003c4]:sd t6, 0(ra)
[0x800003c8]:sd tp, 8(ra)
[0x800003cc]:ld t4, 16(gp)
[0x800003d0]:ld t3, 24(gp)
[0x800003d4]:addi sp, zero, 32
[0x800003d8]:csrrw zero, fcsr, sp
[0x800003dc]:fadd.s t5, t4, t3, dyn

[0x800003dc]:fadd.s t5, t4, t3, dyn
[0x800003e0]:csrrs tp, fcsr, zero
[0x800003e4]:sd t5, 16(ra)
[0x800003e8]:sd tp, 24(ra)
[0x800003ec]:ld t5, 32(gp)
[0x800003f0]:ld t5, 40(gp)
[0x800003f4]:addi sp, zero, 64
[0x800003f8]:csrrw zero, fcsr, sp
[0x800003fc]:fadd.s t4, t5, t5, dyn

[0x800003fc]:fadd.s t4, t5, t5, dyn
[0x80000400]:csrrs tp, fcsr, zero
[0x80000404]:sd t4, 32(ra)
[0x80000408]:sd tp, 40(ra)
[0x8000040c]:ld t3, 48(gp)
[0x80000410]:ld t4, 56(gp)
[0x80000414]:addi sp, zero, 96
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fadd.s t3, t3, t4, dyn

[0x8000041c]:fadd.s t3, t3, t4, dyn
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:sd t3, 48(ra)
[0x80000428]:sd tp, 56(ra)
[0x8000042c]:ld s10, 64(gp)
[0x80000430]:ld s11, 72(gp)
[0x80000434]:addi sp, zero, 128
[0x80000438]:csrrw zero, fcsr, sp
[0x8000043c]:fadd.s s11, s10, s11, dyn

[0x8000043c]:fadd.s s11, s10, s11, dyn
[0x80000440]:csrrs tp, fcsr, zero
[0x80000444]:sd s11, 64(ra)
[0x80000448]:sd tp, 72(ra)
[0x8000044c]:ld s11, 80(gp)
[0x80000450]:ld s9, 88(gp)
[0x80000454]:addi sp, zero, 0
[0x80000458]:csrrw zero, fcsr, sp
[0x8000045c]:fadd.s s10, s11, s9, dyn

[0x8000045c]:fadd.s s10, s11, s9, dyn
[0x80000460]:csrrs tp, fcsr, zero
[0x80000464]:sd s10, 80(ra)
[0x80000468]:sd tp, 88(ra)
[0x8000046c]:ld s8, 96(gp)
[0x80000470]:ld s10, 104(gp)
[0x80000474]:addi sp, zero, 32
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fadd.s s9, s8, s10, dyn

[0x8000047c]:fadd.s s9, s8, s10, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s9, 96(ra)
[0x80000488]:sd tp, 104(ra)
[0x8000048c]:ld s9, 112(gp)
[0x80000490]:ld s7, 120(gp)
[0x80000494]:addi sp, zero, 64
[0x80000498]:csrrw zero, fcsr, sp
[0x8000049c]:fadd.s s8, s9, s7, dyn

[0x8000049c]:fadd.s s8, s9, s7, dyn
[0x800004a0]:csrrs tp, fcsr, zero
[0x800004a4]:sd s8, 112(ra)
[0x800004a8]:sd tp, 120(ra)
[0x800004ac]:ld s6, 128(gp)
[0x800004b0]:ld s8, 136(gp)
[0x800004b4]:addi sp, zero, 96
[0x800004b8]:csrrw zero, fcsr, sp
[0x800004bc]:fadd.s s7, s6, s8, dyn

[0x800004bc]:fadd.s s7, s6, s8, dyn
[0x800004c0]:csrrs tp, fcsr, zero
[0x800004c4]:sd s7, 128(ra)
[0x800004c8]:sd tp, 136(ra)
[0x800004cc]:ld s7, 144(gp)
[0x800004d0]:ld s5, 152(gp)
[0x800004d4]:addi sp, zero, 128
[0x800004d8]:csrrw zero, fcsr, sp
[0x800004dc]:fadd.s s6, s7, s5, dyn

[0x800004dc]:fadd.s s6, s7, s5, dyn
[0x800004e0]:csrrs tp, fcsr, zero
[0x800004e4]:sd s6, 144(ra)
[0x800004e8]:sd tp, 152(ra)
[0x800004ec]:ld s4, 160(gp)
[0x800004f0]:ld s6, 168(gp)
[0x800004f4]:addi sp, zero, 0
[0x800004f8]:csrrw zero, fcsr, sp
[0x800004fc]:fadd.s s5, s4, s6, dyn

[0x800004fc]:fadd.s s5, s4, s6, dyn
[0x80000500]:csrrs tp, fcsr, zero
[0x80000504]:sd s5, 160(ra)
[0x80000508]:sd tp, 168(ra)
[0x8000050c]:ld s5, 176(gp)
[0x80000510]:ld s3, 184(gp)
[0x80000514]:addi sp, zero, 32
[0x80000518]:csrrw zero, fcsr, sp
[0x8000051c]:fadd.s s4, s5, s3, dyn

[0x8000051c]:fadd.s s4, s5, s3, dyn
[0x80000520]:csrrs tp, fcsr, zero
[0x80000524]:sd s4, 176(ra)
[0x80000528]:sd tp, 184(ra)
[0x8000052c]:ld s2, 192(gp)
[0x80000530]:ld s4, 200(gp)
[0x80000534]:addi sp, zero, 64
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fadd.s s3, s2, s4, dyn

[0x8000053c]:fadd.s s3, s2, s4, dyn
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:sd s3, 192(ra)
[0x80000548]:sd tp, 200(ra)
[0x8000054c]:ld s3, 208(gp)
[0x80000550]:ld a7, 216(gp)
[0x80000554]:addi sp, zero, 96
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fadd.s s2, s3, a7, dyn

[0x8000055c]:fadd.s s2, s3, a7, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd s2, 208(ra)
[0x80000568]:sd tp, 216(ra)
[0x8000056c]:ld a6, 224(gp)
[0x80000570]:ld s2, 232(gp)
[0x80000574]:addi sp, zero, 128
[0x80000578]:csrrw zero, fcsr, sp
[0x8000057c]:fadd.s a7, a6, s2, dyn

[0x8000057c]:fadd.s a7, a6, s2, dyn
[0x80000580]:csrrs tp, fcsr, zero
[0x80000584]:sd a7, 224(ra)
[0x80000588]:sd tp, 232(ra)
[0x8000058c]:ld a7, 240(gp)
[0x80000590]:ld a5, 248(gp)
[0x80000594]:addi sp, zero, 0
[0x80000598]:csrrw zero, fcsr, sp
[0x8000059c]:fadd.s a6, a7, a5, dyn

[0x8000059c]:fadd.s a6, a7, a5, dyn
[0x800005a0]:csrrs tp, fcsr, zero
[0x800005a4]:sd a6, 240(ra)
[0x800005a8]:sd tp, 248(ra)
[0x800005ac]:ld a4, 256(gp)
[0x800005b0]:ld a6, 264(gp)
[0x800005b4]:addi sp, zero, 32
[0x800005b8]:csrrw zero, fcsr, sp
[0x800005bc]:fadd.s a5, a4, a6, dyn

[0x800005bc]:fadd.s a5, a4, a6, dyn
[0x800005c0]:csrrs tp, fcsr, zero
[0x800005c4]:sd a5, 256(ra)
[0x800005c8]:sd tp, 264(ra)
[0x800005cc]:ld a5, 272(gp)
[0x800005d0]:ld a3, 280(gp)
[0x800005d4]:addi sp, zero, 64
[0x800005d8]:csrrw zero, fcsr, sp
[0x800005dc]:fadd.s a4, a5, a3, dyn

[0x800005dc]:fadd.s a4, a5, a3, dyn
[0x800005e0]:csrrs tp, fcsr, zero
[0x800005e4]:sd a4, 272(ra)
[0x800005e8]:sd tp, 280(ra)
[0x800005ec]:ld a2, 288(gp)
[0x800005f0]:ld a4, 296(gp)
[0x800005f4]:addi sp, zero, 96
[0x800005f8]:csrrw zero, fcsr, sp
[0x800005fc]:fadd.s a3, a2, a4, dyn

[0x800005fc]:fadd.s a3, a2, a4, dyn
[0x80000600]:csrrs tp, fcsr, zero
[0x80000604]:sd a3, 288(ra)
[0x80000608]:sd tp, 296(ra)
[0x8000060c]:ld a3, 304(gp)
[0x80000610]:ld a1, 312(gp)
[0x80000614]:addi sp, zero, 128
[0x80000618]:csrrw zero, fcsr, sp
[0x8000061c]:fadd.s a2, a3, a1, dyn

[0x8000061c]:fadd.s a2, a3, a1, dyn
[0x80000620]:csrrs tp, fcsr, zero
[0x80000624]:sd a2, 304(ra)
[0x80000628]:sd tp, 312(ra)
[0x8000062c]:ld a0, 320(gp)
[0x80000630]:ld a2, 328(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fadd.s a1, a0, a2, dyn

[0x8000063c]:fadd.s a1, a0, a2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd a1, 320(ra)
[0x80000648]:sd tp, 328(ra)
[0x8000064c]:ld a1, 336(gp)
[0x80000650]:ld s1, 344(gp)
[0x80000654]:addi sp, zero, 32
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fadd.s a0, a1, s1, dyn

[0x8000065c]:fadd.s a0, a1, s1, dyn
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:sd a0, 336(ra)
[0x80000668]:sd tp, 344(ra)
[0x8000066c]:auipc a1, 3
[0x80000670]:addi a1, a1, 2820
[0x80000674]:ld fp, 0(a1)
[0x80000678]:ld a0, 8(a1)
[0x8000067c]:addi sp, zero, 64
[0x80000680]:csrrw zero, fcsr, sp
[0x80000684]:fadd.s s1, fp, a0, dyn

[0x80000684]:fadd.s s1, fp, a0, dyn
[0x80000688]:csrrs a2, fcsr, zero
[0x8000068c]:sd s1, 352(ra)
[0x80000690]:sd a2, 360(ra)
[0x80000694]:ld s1, 16(a1)
[0x80000698]:ld t2, 24(a1)
[0x8000069c]:addi sp, zero, 96
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fadd.s fp, s1, t2, dyn

[0x800006a4]:fadd.s fp, s1, t2, dyn
[0x800006a8]:csrrs a2, fcsr, zero
[0x800006ac]:sd fp, 368(ra)
[0x800006b0]:sd a2, 376(ra)
[0x800006b4]:ld t1, 32(a1)
[0x800006b8]:ld fp, 40(a1)
[0x800006bc]:addi s1, zero, 128
[0x800006c0]:csrrw zero, fcsr, s1
[0x800006c4]:fadd.s t2, t1, fp, dyn

[0x800006c4]:fadd.s t2, t1, fp, dyn
[0x800006c8]:csrrs a2, fcsr, zero
[0x800006cc]:sd t2, 384(ra)
[0x800006d0]:sd a2, 392(ra)
[0x800006d4]:auipc fp, 3
[0x800006d8]:addi fp, fp, 1492
[0x800006dc]:ld t2, 48(a1)
[0x800006e0]:ld t0, 56(a1)
[0x800006e4]:addi s1, zero, 0
[0x800006e8]:csrrw zero, fcsr, s1
[0x800006ec]:fadd.s t1, t2, t0, dyn

[0x800006ec]:fadd.s t1, t2, t0, dyn
[0x800006f0]:csrrs a2, fcsr, zero
[0x800006f4]:sd t1, 0(fp)
[0x800006f8]:sd a2, 8(fp)
[0x800006fc]:ld tp, 64(a1)
[0x80000700]:ld t1, 72(a1)
[0x80000704]:addi s1, zero, 32
[0x80000708]:csrrw zero, fcsr, s1
[0x8000070c]:fadd.s t0, tp, t1, dyn

[0x8000070c]:fadd.s t0, tp, t1, dyn
[0x80000710]:csrrs a2, fcsr, zero
[0x80000714]:sd t0, 16(fp)
[0x80000718]:sd a2, 24(fp)
[0x8000071c]:ld t0, 80(a1)
[0x80000720]:ld gp, 88(a1)
[0x80000724]:addi s1, zero, 64
[0x80000728]:csrrw zero, fcsr, s1
[0x8000072c]:fadd.s tp, t0, gp, dyn

[0x8000072c]:fadd.s tp, t0, gp, dyn
[0x80000730]:csrrs a2, fcsr, zero
[0x80000734]:sd tp, 32(fp)
[0x80000738]:sd a2, 40(fp)
[0x8000073c]:ld sp, 96(a1)
[0x80000740]:ld tp, 104(a1)
[0x80000744]:addi s1, zero, 96
[0x80000748]:csrrw zero, fcsr, s1
[0x8000074c]:fadd.s gp, sp, tp, dyn

[0x8000074c]:fadd.s gp, sp, tp, dyn
[0x80000750]:csrrs a2, fcsr, zero
[0x80000754]:sd gp, 48(fp)
[0x80000758]:sd a2, 56(fp)
[0x8000075c]:ld gp, 112(a1)
[0x80000760]:ld ra, 120(a1)
[0x80000764]:addi s1, zero, 128
[0x80000768]:csrrw zero, fcsr, s1
[0x8000076c]:fadd.s sp, gp, ra, dyn

[0x8000076c]:fadd.s sp, gp, ra, dyn
[0x80000770]:csrrs a2, fcsr, zero
[0x80000774]:sd sp, 64(fp)
[0x80000778]:sd a2, 72(fp)
[0x8000077c]:ld zero, 128(a1)
[0x80000780]:ld sp, 136(a1)
[0x80000784]:addi s1, zero, 0
[0x80000788]:csrrw zero, fcsr, s1
[0x8000078c]:fadd.s ra, zero, sp, dyn

[0x8000078c]:fadd.s ra, zero, sp, dyn
[0x80000790]:csrrs a2, fcsr, zero
[0x80000794]:sd ra, 80(fp)
[0x80000798]:sd a2, 88(fp)
[0x8000079c]:ld ra, 144(a1)
[0x800007a0]:ld t5, 152(a1)
[0x800007a4]:addi s1, zero, 32
[0x800007a8]:csrrw zero, fcsr, s1
[0x800007ac]:fadd.s t6, ra, t5, dyn

[0x800007ac]:fadd.s t6, ra, t5, dyn
[0x800007b0]:csrrs a2, fcsr, zero
[0x800007b4]:sd t6, 96(fp)
[0x800007b8]:sd a2, 104(fp)
[0x800007bc]:ld t5, 160(a1)
[0x800007c0]:ld zero, 168(a1)
[0x800007c4]:addi s1, zero, 64
[0x800007c8]:csrrw zero, fcsr, s1
[0x800007cc]:fadd.s t6, t5, zero, dyn

[0x800007cc]:fadd.s t6, t5, zero, dyn
[0x800007d0]:csrrs a2, fcsr, zero
[0x800007d4]:sd t6, 112(fp)
[0x800007d8]:sd a2, 120(fp)
[0x800007dc]:ld t6, 176(a1)
[0x800007e0]:ld t5, 184(a1)
[0x800007e4]:addi s1, zero, 96
[0x800007e8]:csrrw zero, fcsr, s1
[0x800007ec]:fadd.s zero, t6, t5, dyn

[0x800007ec]:fadd.s zero, t6, t5, dyn
[0x800007f0]:csrrs a2, fcsr, zero
[0x800007f4]:sd zero, 128(fp)
[0x800007f8]:sd a2, 136(fp)
[0x800007fc]:ld t5, 192(a1)
[0x80000800]:ld t4, 200(a1)
[0x80000804]:addi s1, zero, 128
[0x80000808]:csrrw zero, fcsr, s1
[0x8000080c]:fadd.s t6, t5, t4, dyn

[0x8000080c]:fadd.s t6, t5, t4, dyn
[0x80000810]:csrrs a2, fcsr, zero
[0x80000814]:sd t6, 144(fp)
[0x80000818]:sd a2, 152(fp)
[0x8000081c]:ld t5, 208(a1)
[0x80000820]:ld t4, 216(a1)
[0x80000824]:addi s1, zero, 0
[0x80000828]:csrrw zero, fcsr, s1
[0x8000082c]:fadd.s t6, t5, t4, dyn

[0x8000082c]:fadd.s t6, t5, t4, dyn
[0x80000830]:csrrs a2, fcsr, zero
[0x80000834]:sd t6, 160(fp)
[0x80000838]:sd a2, 168(fp)
[0x8000083c]:ld t5, 224(a1)
[0x80000840]:ld t4, 232(a1)
[0x80000844]:addi s1, zero, 32
[0x80000848]:csrrw zero, fcsr, s1
[0x8000084c]:fadd.s t6, t5, t4, dyn

[0x8000084c]:fadd.s t6, t5, t4, dyn
[0x80000850]:csrrs a2, fcsr, zero
[0x80000854]:sd t6, 176(fp)
[0x80000858]:sd a2, 184(fp)
[0x8000085c]:ld t5, 240(a1)
[0x80000860]:ld t4, 248(a1)
[0x80000864]:addi s1, zero, 64
[0x80000868]:csrrw zero, fcsr, s1
[0x8000086c]:fadd.s t6, t5, t4, dyn

[0x8000086c]:fadd.s t6, t5, t4, dyn
[0x80000870]:csrrs a2, fcsr, zero
[0x80000874]:sd t6, 192(fp)
[0x80000878]:sd a2, 200(fp)
[0x8000087c]:ld t5, 256(a1)
[0x80000880]:ld t4, 264(a1)
[0x80000884]:addi s1, zero, 96
[0x80000888]:csrrw zero, fcsr, s1
[0x8000088c]:fadd.s t6, t5, t4, dyn

[0x8000088c]:fadd.s t6, t5, t4, dyn
[0x80000890]:csrrs a2, fcsr, zero
[0x80000894]:sd t6, 208(fp)
[0x80000898]:sd a2, 216(fp)
[0x8000089c]:ld t5, 272(a1)
[0x800008a0]:ld t4, 280(a1)
[0x800008a4]:addi s1, zero, 128
[0x800008a8]:csrrw zero, fcsr, s1
[0x800008ac]:fadd.s t6, t5, t4, dyn

[0x800008ac]:fadd.s t6, t5, t4, dyn
[0x800008b0]:csrrs a2, fcsr, zero
[0x800008b4]:sd t6, 224(fp)
[0x800008b8]:sd a2, 232(fp)
[0x800008bc]:ld t5, 288(a1)
[0x800008c0]:ld t4, 296(a1)
[0x800008c4]:addi s1, zero, 0
[0x800008c8]:csrrw zero, fcsr, s1
[0x800008cc]:fadd.s t6, t5, t4, dyn

[0x800008cc]:fadd.s t6, t5, t4, dyn
[0x800008d0]:csrrs a2, fcsr, zero
[0x800008d4]:sd t6, 240(fp)
[0x800008d8]:sd a2, 248(fp)
[0x800008dc]:ld t5, 304(a1)
[0x800008e0]:ld t4, 312(a1)
[0x800008e4]:addi s1, zero, 32
[0x800008e8]:csrrw zero, fcsr, s1
[0x800008ec]:fadd.s t6, t5, t4, dyn

[0x800008ec]:fadd.s t6, t5, t4, dyn
[0x800008f0]:csrrs a2, fcsr, zero
[0x800008f4]:sd t6, 256(fp)
[0x800008f8]:sd a2, 264(fp)
[0x800008fc]:ld t5, 320(a1)
[0x80000900]:ld t4, 328(a1)
[0x80000904]:addi s1, zero, 64
[0x80000908]:csrrw zero, fcsr, s1
[0x8000090c]:fadd.s t6, t5, t4, dyn

[0x8000090c]:fadd.s t6, t5, t4, dyn
[0x80000910]:csrrs a2, fcsr, zero
[0x80000914]:sd t6, 272(fp)
[0x80000918]:sd a2, 280(fp)
[0x8000091c]:ld t5, 336(a1)
[0x80000920]:ld t4, 344(a1)
[0x80000924]:addi s1, zero, 96
[0x80000928]:csrrw zero, fcsr, s1
[0x8000092c]:fadd.s t6, t5, t4, dyn

[0x8000092c]:fadd.s t6, t5, t4, dyn
[0x80000930]:csrrs a2, fcsr, zero
[0x80000934]:sd t6, 288(fp)
[0x80000938]:sd a2, 296(fp)
[0x8000093c]:ld t5, 352(a1)
[0x80000940]:ld t4, 360(a1)
[0x80000944]:addi s1, zero, 128
[0x80000948]:csrrw zero, fcsr, s1
[0x8000094c]:fadd.s t6, t5, t4, dyn

[0x8000094c]:fadd.s t6, t5, t4, dyn
[0x80000950]:csrrs a2, fcsr, zero
[0x80000954]:sd t6, 304(fp)
[0x80000958]:sd a2, 312(fp)
[0x8000095c]:ld t5, 368(a1)
[0x80000960]:ld t4, 376(a1)
[0x80000964]:addi s1, zero, 0
[0x80000968]:csrrw zero, fcsr, s1
[0x8000096c]:fadd.s t6, t5, t4, dyn

[0x8000096c]:fadd.s t6, t5, t4, dyn
[0x80000970]:csrrs a2, fcsr, zero
[0x80000974]:sd t6, 320(fp)
[0x80000978]:sd a2, 328(fp)
[0x8000097c]:ld t5, 384(a1)
[0x80000980]:ld t4, 392(a1)
[0x80000984]:addi s1, zero, 32
[0x80000988]:csrrw zero, fcsr, s1
[0x8000098c]:fadd.s t6, t5, t4, dyn

[0x8000098c]:fadd.s t6, t5, t4, dyn
[0x80000990]:csrrs a2, fcsr, zero
[0x80000994]:sd t6, 336(fp)
[0x80000998]:sd a2, 344(fp)
[0x8000099c]:ld t5, 400(a1)
[0x800009a0]:ld t4, 408(a1)
[0x800009a4]:addi s1, zero, 64
[0x800009a8]:csrrw zero, fcsr, s1
[0x800009ac]:fadd.s t6, t5, t4, dyn

[0x800009ac]:fadd.s t6, t5, t4, dyn
[0x800009b0]:csrrs a2, fcsr, zero
[0x800009b4]:sd t6, 352(fp)
[0x800009b8]:sd a2, 360(fp)
[0x800009bc]:ld t5, 416(a1)
[0x800009c0]:ld t4, 424(a1)
[0x800009c4]:addi s1, zero, 96
[0x800009c8]:csrrw zero, fcsr, s1
[0x800009cc]:fadd.s t6, t5, t4, dyn

[0x800009cc]:fadd.s t6, t5, t4, dyn
[0x800009d0]:csrrs a2, fcsr, zero
[0x800009d4]:sd t6, 368(fp)
[0x800009d8]:sd a2, 376(fp)
[0x800009dc]:ld t5, 432(a1)
[0x800009e0]:ld t4, 440(a1)
[0x800009e4]:addi s1, zero, 128
[0x800009e8]:csrrw zero, fcsr, s1
[0x800009ec]:fadd.s t6, t5, t4, dyn

[0x800009ec]:fadd.s t6, t5, t4, dyn
[0x800009f0]:csrrs a2, fcsr, zero
[0x800009f4]:sd t6, 384(fp)
[0x800009f8]:sd a2, 392(fp)
[0x800009fc]:ld t5, 448(a1)
[0x80000a00]:ld t4, 456(a1)
[0x80000a04]:addi s1, zero, 0
[0x80000a08]:csrrw zero, fcsr, s1
[0x80000a0c]:fadd.s t6, t5, t4, dyn

[0x80000a0c]:fadd.s t6, t5, t4, dyn
[0x80000a10]:csrrs a2, fcsr, zero
[0x80000a14]:sd t6, 400(fp)
[0x80000a18]:sd a2, 408(fp)
[0x80000a1c]:ld t5, 464(a1)
[0x80000a20]:ld t4, 472(a1)
[0x80000a24]:addi s1, zero, 32
[0x80000a28]:csrrw zero, fcsr, s1
[0x80000a2c]:fadd.s t6, t5, t4, dyn

[0x80000a2c]:fadd.s t6, t5, t4, dyn
[0x80000a30]:csrrs a2, fcsr, zero
[0x80000a34]:sd t6, 416(fp)
[0x80000a38]:sd a2, 424(fp)
[0x80000a3c]:ld t5, 480(a1)
[0x80000a40]:ld t4, 488(a1)
[0x80000a44]:addi s1, zero, 64
[0x80000a48]:csrrw zero, fcsr, s1
[0x80000a4c]:fadd.s t6, t5, t4, dyn

[0x80000a4c]:fadd.s t6, t5, t4, dyn
[0x80000a50]:csrrs a2, fcsr, zero
[0x80000a54]:sd t6, 432(fp)
[0x80000a58]:sd a2, 440(fp)
[0x80000a5c]:ld t5, 496(a1)
[0x80000a60]:ld t4, 504(a1)
[0x80000a64]:addi s1, zero, 96
[0x80000a68]:csrrw zero, fcsr, s1
[0x80000a6c]:fadd.s t6, t5, t4, dyn

[0x80000a6c]:fadd.s t6, t5, t4, dyn
[0x80000a70]:csrrs a2, fcsr, zero
[0x80000a74]:sd t6, 448(fp)
[0x80000a78]:sd a2, 456(fp)
[0x80000a7c]:ld t5, 512(a1)
[0x80000a80]:ld t4, 520(a1)
[0x80000a84]:addi s1, zero, 128
[0x80000a88]:csrrw zero, fcsr, s1
[0x80000a8c]:fadd.s t6, t5, t4, dyn

[0x80000a8c]:fadd.s t6, t5, t4, dyn
[0x80000a90]:csrrs a2, fcsr, zero
[0x80000a94]:sd t6, 464(fp)
[0x80000a98]:sd a2, 472(fp)
[0x80000a9c]:ld t5, 528(a1)
[0x80000aa0]:ld t4, 536(a1)
[0x80000aa4]:addi s1, zero, 0
[0x80000aa8]:csrrw zero, fcsr, s1
[0x80000aac]:fadd.s t6, t5, t4, dyn

[0x80000aac]:fadd.s t6, t5, t4, dyn
[0x80000ab0]:csrrs a2, fcsr, zero
[0x80000ab4]:sd t6, 480(fp)
[0x80000ab8]:sd a2, 488(fp)
[0x80000abc]:ld t5, 544(a1)
[0x80000ac0]:ld t4, 552(a1)
[0x80000ac4]:addi s1, zero, 32
[0x80000ac8]:csrrw zero, fcsr, s1
[0x80000acc]:fadd.s t6, t5, t4, dyn

[0x80000acc]:fadd.s t6, t5, t4, dyn
[0x80000ad0]:csrrs a2, fcsr, zero
[0x80000ad4]:sd t6, 496(fp)
[0x80000ad8]:sd a2, 504(fp)
[0x80000adc]:ld t5, 560(a1)
[0x80000ae0]:ld t4, 568(a1)
[0x80000ae4]:addi s1, zero, 64
[0x80000ae8]:csrrw zero, fcsr, s1
[0x80000aec]:fadd.s t6, t5, t4, dyn

[0x80000aec]:fadd.s t6, t5, t4, dyn
[0x80000af0]:csrrs a2, fcsr, zero
[0x80000af4]:sd t6, 512(fp)
[0x80000af8]:sd a2, 520(fp)
[0x80000afc]:ld t5, 576(a1)
[0x80000b00]:ld t4, 584(a1)
[0x80000b04]:addi s1, zero, 96
[0x80000b08]:csrrw zero, fcsr, s1
[0x80000b0c]:fadd.s t6, t5, t4, dyn

[0x80000b0c]:fadd.s t6, t5, t4, dyn
[0x80000b10]:csrrs a2, fcsr, zero
[0x80000b14]:sd t6, 528(fp)
[0x80000b18]:sd a2, 536(fp)
[0x80000b1c]:ld t5, 592(a1)
[0x80000b20]:ld t4, 600(a1)
[0x80000b24]:addi s1, zero, 128
[0x80000b28]:csrrw zero, fcsr, s1
[0x80000b2c]:fadd.s t6, t5, t4, dyn

[0x80000b2c]:fadd.s t6, t5, t4, dyn
[0x80000b30]:csrrs a2, fcsr, zero
[0x80000b34]:sd t6, 544(fp)
[0x80000b38]:sd a2, 552(fp)
[0x80000b3c]:ld t5, 608(a1)
[0x80000b40]:ld t4, 616(a1)
[0x80000b44]:addi s1, zero, 0
[0x80000b48]:csrrw zero, fcsr, s1
[0x80000b4c]:fadd.s t6, t5, t4, dyn

[0x80000b4c]:fadd.s t6, t5, t4, dyn
[0x80000b50]:csrrs a2, fcsr, zero
[0x80000b54]:sd t6, 560(fp)
[0x80000b58]:sd a2, 568(fp)
[0x80000b5c]:ld t5, 624(a1)
[0x80000b60]:ld t4, 632(a1)
[0x80000b64]:addi s1, zero, 32
[0x80000b68]:csrrw zero, fcsr, s1
[0x80000b6c]:fadd.s t6, t5, t4, dyn

[0x80000b6c]:fadd.s t6, t5, t4, dyn
[0x80000b70]:csrrs a2, fcsr, zero
[0x80000b74]:sd t6, 576(fp)
[0x80000b78]:sd a2, 584(fp)
[0x80000b7c]:ld t5, 640(a1)
[0x80000b80]:ld t4, 648(a1)
[0x80000b84]:addi s1, zero, 64
[0x80000b88]:csrrw zero, fcsr, s1
[0x80000b8c]:fadd.s t6, t5, t4, dyn

[0x80000b8c]:fadd.s t6, t5, t4, dyn
[0x80000b90]:csrrs a2, fcsr, zero
[0x80000b94]:sd t6, 592(fp)
[0x80000b98]:sd a2, 600(fp)
[0x80000b9c]:ld t5, 656(a1)
[0x80000ba0]:ld t4, 664(a1)
[0x80000ba4]:addi s1, zero, 96
[0x80000ba8]:csrrw zero, fcsr, s1
[0x80000bac]:fadd.s t6, t5, t4, dyn

[0x80000bac]:fadd.s t6, t5, t4, dyn
[0x80000bb0]:csrrs a2, fcsr, zero
[0x80000bb4]:sd t6, 608(fp)
[0x80000bb8]:sd a2, 616(fp)
[0x80000bbc]:ld t5, 672(a1)
[0x80000bc0]:ld t4, 680(a1)
[0x80000bc4]:addi s1, zero, 128
[0x80000bc8]:csrrw zero, fcsr, s1
[0x80000bcc]:fadd.s t6, t5, t4, dyn

[0x80000bcc]:fadd.s t6, t5, t4, dyn
[0x80000bd0]:csrrs a2, fcsr, zero
[0x80000bd4]:sd t6, 624(fp)
[0x80000bd8]:sd a2, 632(fp)
[0x80000bdc]:ld t5, 688(a1)
[0x80000be0]:ld t4, 696(a1)
[0x80000be4]:addi s1, zero, 0
[0x80000be8]:csrrw zero, fcsr, s1
[0x80000bec]:fadd.s t6, t5, t4, dyn

[0x80000bec]:fadd.s t6, t5, t4, dyn
[0x80000bf0]:csrrs a2, fcsr, zero
[0x80000bf4]:sd t6, 640(fp)
[0x80000bf8]:sd a2, 648(fp)
[0x80000bfc]:ld t5, 704(a1)
[0x80000c00]:ld t4, 712(a1)
[0x80000c04]:addi s1, zero, 32
[0x80000c08]:csrrw zero, fcsr, s1
[0x80000c0c]:fadd.s t6, t5, t4, dyn

[0x80000c0c]:fadd.s t6, t5, t4, dyn
[0x80000c10]:csrrs a2, fcsr, zero
[0x80000c14]:sd t6, 656(fp)
[0x80000c18]:sd a2, 664(fp)
[0x80000c1c]:ld t5, 720(a1)
[0x80000c20]:ld t4, 728(a1)
[0x80000c24]:addi s1, zero, 64
[0x80000c28]:csrrw zero, fcsr, s1
[0x80000c2c]:fadd.s t6, t5, t4, dyn

[0x80000c2c]:fadd.s t6, t5, t4, dyn
[0x80000c30]:csrrs a2, fcsr, zero
[0x80000c34]:sd t6, 672(fp)
[0x80000c38]:sd a2, 680(fp)
[0x80000c3c]:ld t5, 736(a1)
[0x80000c40]:ld t4, 744(a1)
[0x80000c44]:addi s1, zero, 96
[0x80000c48]:csrrw zero, fcsr, s1
[0x80000c4c]:fadd.s t6, t5, t4, dyn

[0x80000c4c]:fadd.s t6, t5, t4, dyn
[0x80000c50]:csrrs a2, fcsr, zero
[0x80000c54]:sd t6, 688(fp)
[0x80000c58]:sd a2, 696(fp)
[0x80000c5c]:ld t5, 752(a1)
[0x80000c60]:ld t4, 760(a1)
[0x80000c64]:addi s1, zero, 128
[0x80000c68]:csrrw zero, fcsr, s1
[0x80000c6c]:fadd.s t6, t5, t4, dyn

[0x80000c6c]:fadd.s t6, t5, t4, dyn
[0x80000c70]:csrrs a2, fcsr, zero
[0x80000c74]:sd t6, 704(fp)
[0x80000c78]:sd a2, 712(fp)
[0x80000c7c]:ld t5, 768(a1)
[0x80000c80]:ld t4, 776(a1)
[0x80000c84]:addi s1, zero, 0
[0x80000c88]:csrrw zero, fcsr, s1
[0x80000c8c]:fadd.s t6, t5, t4, dyn

[0x80000c8c]:fadd.s t6, t5, t4, dyn
[0x80000c90]:csrrs a2, fcsr, zero
[0x80000c94]:sd t6, 720(fp)
[0x80000c98]:sd a2, 728(fp)
[0x80000c9c]:ld t5, 784(a1)
[0x80000ca0]:ld t4, 792(a1)
[0x80000ca4]:addi s1, zero, 32
[0x80000ca8]:csrrw zero, fcsr, s1
[0x80000cac]:fadd.s t6, t5, t4, dyn

[0x80000cac]:fadd.s t6, t5, t4, dyn
[0x80000cb0]:csrrs a2, fcsr, zero
[0x80000cb4]:sd t6, 736(fp)
[0x80000cb8]:sd a2, 744(fp)
[0x80000cbc]:ld t5, 800(a1)
[0x80000cc0]:ld t4, 808(a1)
[0x80000cc4]:addi s1, zero, 64
[0x80000cc8]:csrrw zero, fcsr, s1
[0x80000ccc]:fadd.s t6, t5, t4, dyn

[0x80000ccc]:fadd.s t6, t5, t4, dyn
[0x80000cd0]:csrrs a2, fcsr, zero
[0x80000cd4]:sd t6, 752(fp)
[0x80000cd8]:sd a2, 760(fp)
[0x80000cdc]:ld t5, 816(a1)
[0x80000ce0]:ld t4, 824(a1)
[0x80000ce4]:addi s1, zero, 96
[0x80000ce8]:csrrw zero, fcsr, s1
[0x80000cec]:fadd.s t6, t5, t4, dyn

[0x80000cec]:fadd.s t6, t5, t4, dyn
[0x80000cf0]:csrrs a2, fcsr, zero
[0x80000cf4]:sd t6, 768(fp)
[0x80000cf8]:sd a2, 776(fp)
[0x80000cfc]:ld t5, 832(a1)
[0x80000d00]:ld t4, 840(a1)
[0x80000d04]:addi s1, zero, 128
[0x80000d08]:csrrw zero, fcsr, s1
[0x80000d0c]:fadd.s t6, t5, t4, dyn

[0x80000d0c]:fadd.s t6, t5, t4, dyn
[0x80000d10]:csrrs a2, fcsr, zero
[0x80000d14]:sd t6, 784(fp)
[0x80000d18]:sd a2, 792(fp)
[0x80000d1c]:ld t5, 848(a1)
[0x80000d20]:ld t4, 856(a1)
[0x80000d24]:addi s1, zero, 0
[0x80000d28]:csrrw zero, fcsr, s1
[0x80000d2c]:fadd.s t6, t5, t4, dyn

[0x80000d2c]:fadd.s t6, t5, t4, dyn
[0x80000d30]:csrrs a2, fcsr, zero
[0x80000d34]:sd t6, 800(fp)
[0x80000d38]:sd a2, 808(fp)
[0x80000d3c]:ld t5, 864(a1)
[0x80000d40]:ld t4, 872(a1)
[0x80000d44]:addi s1, zero, 32
[0x80000d48]:csrrw zero, fcsr, s1
[0x80000d4c]:fadd.s t6, t5, t4, dyn

[0x80000d4c]:fadd.s t6, t5, t4, dyn
[0x80000d50]:csrrs a2, fcsr, zero
[0x80000d54]:sd t6, 816(fp)
[0x80000d58]:sd a2, 824(fp)
[0x80000d5c]:ld t5, 880(a1)
[0x80000d60]:ld t4, 888(a1)
[0x80000d64]:addi s1, zero, 64
[0x80000d68]:csrrw zero, fcsr, s1
[0x80000d6c]:fadd.s t6, t5, t4, dyn

[0x80000d6c]:fadd.s t6, t5, t4, dyn
[0x80000d70]:csrrs a2, fcsr, zero
[0x80000d74]:sd t6, 832(fp)
[0x80000d78]:sd a2, 840(fp)
[0x80000d7c]:ld t5, 896(a1)
[0x80000d80]:ld t4, 904(a1)
[0x80000d84]:addi s1, zero, 96
[0x80000d88]:csrrw zero, fcsr, s1
[0x80000d8c]:fadd.s t6, t5, t4, dyn

[0x80000d8c]:fadd.s t6, t5, t4, dyn
[0x80000d90]:csrrs a2, fcsr, zero
[0x80000d94]:sd t6, 848(fp)
[0x80000d98]:sd a2, 856(fp)
[0x80000d9c]:ld t5, 912(a1)
[0x80000da0]:ld t4, 920(a1)
[0x80000da4]:addi s1, zero, 128
[0x80000da8]:csrrw zero, fcsr, s1
[0x80000dac]:fadd.s t6, t5, t4, dyn

[0x80000dac]:fadd.s t6, t5, t4, dyn
[0x80000db0]:csrrs a2, fcsr, zero
[0x80000db4]:sd t6, 864(fp)
[0x80000db8]:sd a2, 872(fp)
[0x80000dbc]:ld t5, 928(a1)
[0x80000dc0]:ld t4, 936(a1)
[0x80000dc4]:addi s1, zero, 0
[0x80000dc8]:csrrw zero, fcsr, s1
[0x80000dcc]:fadd.s t6, t5, t4, dyn

[0x80000dcc]:fadd.s t6, t5, t4, dyn
[0x80000dd0]:csrrs a2, fcsr, zero
[0x80000dd4]:sd t6, 880(fp)
[0x80000dd8]:sd a2, 888(fp)
[0x80000ddc]:ld t5, 944(a1)
[0x80000de0]:ld t4, 952(a1)
[0x80000de4]:addi s1, zero, 32
[0x80000de8]:csrrw zero, fcsr, s1
[0x80000dec]:fadd.s t6, t5, t4, dyn

[0x80000dec]:fadd.s t6, t5, t4, dyn
[0x80000df0]:csrrs a2, fcsr, zero
[0x80000df4]:sd t6, 896(fp)
[0x80000df8]:sd a2, 904(fp)
[0x80000dfc]:ld t5, 960(a1)
[0x80000e00]:ld t4, 968(a1)
[0x80000e04]:addi s1, zero, 64
[0x80000e08]:csrrw zero, fcsr, s1
[0x80000e0c]:fadd.s t6, t5, t4, dyn

[0x80000e0c]:fadd.s t6, t5, t4, dyn
[0x80000e10]:csrrs a2, fcsr, zero
[0x80000e14]:sd t6, 912(fp)
[0x80000e18]:sd a2, 920(fp)
[0x80000e1c]:ld t5, 976(a1)
[0x80000e20]:ld t4, 984(a1)
[0x80000e24]:addi s1, zero, 96
[0x80000e28]:csrrw zero, fcsr, s1
[0x80000e2c]:fadd.s t6, t5, t4, dyn

[0x80000e2c]:fadd.s t6, t5, t4, dyn
[0x80000e30]:csrrs a2, fcsr, zero
[0x80000e34]:sd t6, 928(fp)
[0x80000e38]:sd a2, 936(fp)
[0x80000e3c]:ld t5, 992(a1)
[0x80000e40]:ld t4, 1000(a1)
[0x80000e44]:addi s1, zero, 128
[0x80000e48]:csrrw zero, fcsr, s1
[0x80000e4c]:fadd.s t6, t5, t4, dyn

[0x80000e4c]:fadd.s t6, t5, t4, dyn
[0x80000e50]:csrrs a2, fcsr, zero
[0x80000e54]:sd t6, 944(fp)
[0x80000e58]:sd a2, 952(fp)
[0x80000e5c]:ld t5, 1008(a1)
[0x80000e60]:ld t4, 1016(a1)
[0x80000e64]:addi s1, zero, 0
[0x80000e68]:csrrw zero, fcsr, s1
[0x80000e6c]:fadd.s t6, t5, t4, dyn

[0x80000e6c]:fadd.s t6, t5, t4, dyn
[0x80000e70]:csrrs a2, fcsr, zero
[0x80000e74]:sd t6, 960(fp)
[0x80000e78]:sd a2, 968(fp)
[0x80000e7c]:ld t5, 1024(a1)
[0x80000e80]:ld t4, 1032(a1)
[0x80000e84]:addi s1, zero, 32
[0x80000e88]:csrrw zero, fcsr, s1
[0x80000e8c]:fadd.s t6, t5, t4, dyn

[0x80000e8c]:fadd.s t6, t5, t4, dyn
[0x80000e90]:csrrs a2, fcsr, zero
[0x80000e94]:sd t6, 976(fp)
[0x80000e98]:sd a2, 984(fp)
[0x80000e9c]:ld t5, 1040(a1)
[0x80000ea0]:ld t4, 1048(a1)
[0x80000ea4]:addi s1, zero, 64
[0x80000ea8]:csrrw zero, fcsr, s1
[0x80000eac]:fadd.s t6, t5, t4, dyn

[0x80000eac]:fadd.s t6, t5, t4, dyn
[0x80000eb0]:csrrs a2, fcsr, zero
[0x80000eb4]:sd t6, 992(fp)
[0x80000eb8]:sd a2, 1000(fp)
[0x80000ebc]:ld t5, 1056(a1)
[0x80000ec0]:ld t4, 1064(a1)
[0x80000ec4]:addi s1, zero, 96
[0x80000ec8]:csrrw zero, fcsr, s1
[0x80000ecc]:fadd.s t6, t5, t4, dyn

[0x80000ecc]:fadd.s t6, t5, t4, dyn
[0x80000ed0]:csrrs a2, fcsr, zero
[0x80000ed4]:sd t6, 1008(fp)
[0x80000ed8]:sd a2, 1016(fp)
[0x80000edc]:ld t5, 1072(a1)
[0x80000ee0]:ld t4, 1080(a1)
[0x80000ee4]:addi s1, zero, 128
[0x80000ee8]:csrrw zero, fcsr, s1
[0x80000eec]:fadd.s t6, t5, t4, dyn

[0x80000eec]:fadd.s t6, t5, t4, dyn
[0x80000ef0]:csrrs a2, fcsr, zero
[0x80000ef4]:sd t6, 1024(fp)
[0x80000ef8]:sd a2, 1032(fp)
[0x80000efc]:ld t5, 1088(a1)
[0x80000f00]:ld t4, 1096(a1)
[0x80000f04]:addi s1, zero, 0
[0x80000f08]:csrrw zero, fcsr, s1
[0x80000f0c]:fadd.s t6, t5, t4, dyn

[0x80000f0c]:fadd.s t6, t5, t4, dyn
[0x80000f10]:csrrs a2, fcsr, zero
[0x80000f14]:sd t6, 1040(fp)
[0x80000f18]:sd a2, 1048(fp)
[0x80000f1c]:ld t5, 1104(a1)
[0x80000f20]:ld t4, 1112(a1)
[0x80000f24]:addi s1, zero, 32
[0x80000f28]:csrrw zero, fcsr, s1
[0x80000f2c]:fadd.s t6, t5, t4, dyn

[0x80000f2c]:fadd.s t6, t5, t4, dyn
[0x80000f30]:csrrs a2, fcsr, zero
[0x80000f34]:sd t6, 1056(fp)
[0x80000f38]:sd a2, 1064(fp)
[0x80000f3c]:ld t5, 1120(a1)
[0x80000f40]:ld t4, 1128(a1)
[0x80000f44]:addi s1, zero, 64
[0x80000f48]:csrrw zero, fcsr, s1
[0x80000f4c]:fadd.s t6, t5, t4, dyn

[0x80000f4c]:fadd.s t6, t5, t4, dyn
[0x80000f50]:csrrs a2, fcsr, zero
[0x80000f54]:sd t6, 1072(fp)
[0x80000f58]:sd a2, 1080(fp)
[0x80000f5c]:ld t5, 1136(a1)
[0x80000f60]:ld t4, 1144(a1)
[0x80000f64]:addi s1, zero, 96
[0x80000f68]:csrrw zero, fcsr, s1
[0x80000f6c]:fadd.s t6, t5, t4, dyn

[0x80000f6c]:fadd.s t6, t5, t4, dyn
[0x80000f70]:csrrs a2, fcsr, zero
[0x80000f74]:sd t6, 1088(fp)
[0x80000f78]:sd a2, 1096(fp)
[0x80000f7c]:ld t5, 1152(a1)
[0x80000f80]:ld t4, 1160(a1)
[0x80000f84]:addi s1, zero, 128
[0x80000f88]:csrrw zero, fcsr, s1
[0x80000f8c]:fadd.s t6, t5, t4, dyn

[0x80000f8c]:fadd.s t6, t5, t4, dyn
[0x80000f90]:csrrs a2, fcsr, zero
[0x80000f94]:sd t6, 1104(fp)
[0x80000f98]:sd a2, 1112(fp)
[0x80000f9c]:ld t5, 1168(a1)
[0x80000fa0]:ld t4, 1176(a1)
[0x80000fa4]:addi s1, zero, 0
[0x80000fa8]:csrrw zero, fcsr, s1
[0x80000fac]:fadd.s t6, t5, t4, dyn

[0x80000fac]:fadd.s t6, t5, t4, dyn
[0x80000fb0]:csrrs a2, fcsr, zero
[0x80000fb4]:sd t6, 1120(fp)
[0x80000fb8]:sd a2, 1128(fp)
[0x80000fbc]:ld t5, 1184(a1)
[0x80000fc0]:ld t4, 1192(a1)
[0x80000fc4]:addi s1, zero, 32
[0x80000fc8]:csrrw zero, fcsr, s1
[0x80000fcc]:fadd.s t6, t5, t4, dyn

[0x80000fcc]:fadd.s t6, t5, t4, dyn
[0x80000fd0]:csrrs a2, fcsr, zero
[0x80000fd4]:sd t6, 1136(fp)
[0x80000fd8]:sd a2, 1144(fp)
[0x80000fdc]:ld t5, 1200(a1)
[0x80000fe0]:ld t4, 1208(a1)
[0x80000fe4]:addi s1, zero, 64
[0x80000fe8]:csrrw zero, fcsr, s1
[0x80000fec]:fadd.s t6, t5, t4, dyn

[0x80000fec]:fadd.s t6, t5, t4, dyn
[0x80000ff0]:csrrs a2, fcsr, zero
[0x80000ff4]:sd t6, 1152(fp)
[0x80000ff8]:sd a2, 1160(fp)
[0x80000ffc]:ld t5, 1216(a1)
[0x80001000]:ld t4, 1224(a1)
[0x80001004]:addi s1, zero, 96
[0x80001008]:csrrw zero, fcsr, s1
[0x8000100c]:fadd.s t6, t5, t4, dyn

[0x8000100c]:fadd.s t6, t5, t4, dyn
[0x80001010]:csrrs a2, fcsr, zero
[0x80001014]:sd t6, 1168(fp)
[0x80001018]:sd a2, 1176(fp)
[0x8000101c]:ld t5, 1232(a1)
[0x80001020]:ld t4, 1240(a1)
[0x80001024]:addi s1, zero, 128
[0x80001028]:csrrw zero, fcsr, s1
[0x8000102c]:fadd.s t6, t5, t4, dyn

[0x8000102c]:fadd.s t6, t5, t4, dyn
[0x80001030]:csrrs a2, fcsr, zero
[0x80001034]:sd t6, 1184(fp)
[0x80001038]:sd a2, 1192(fp)
[0x8000103c]:ld t5, 1248(a1)
[0x80001040]:ld t4, 1256(a1)
[0x80001044]:addi s1, zero, 0
[0x80001048]:csrrw zero, fcsr, s1
[0x8000104c]:fadd.s t6, t5, t4, dyn

[0x8000104c]:fadd.s t6, t5, t4, dyn
[0x80001050]:csrrs a2, fcsr, zero
[0x80001054]:sd t6, 1200(fp)
[0x80001058]:sd a2, 1208(fp)
[0x8000105c]:ld t5, 1264(a1)
[0x80001060]:ld t4, 1272(a1)
[0x80001064]:addi s1, zero, 32
[0x80001068]:csrrw zero, fcsr, s1
[0x8000106c]:fadd.s t6, t5, t4, dyn

[0x8000106c]:fadd.s t6, t5, t4, dyn
[0x80001070]:csrrs a2, fcsr, zero
[0x80001074]:sd t6, 1216(fp)
[0x80001078]:sd a2, 1224(fp)
[0x8000107c]:ld t5, 1280(a1)
[0x80001080]:ld t4, 1288(a1)
[0x80001084]:addi s1, zero, 64
[0x80001088]:csrrw zero, fcsr, s1
[0x8000108c]:fadd.s t6, t5, t4, dyn

[0x8000108c]:fadd.s t6, t5, t4, dyn
[0x80001090]:csrrs a2, fcsr, zero
[0x80001094]:sd t6, 1232(fp)
[0x80001098]:sd a2, 1240(fp)
[0x8000109c]:ld t5, 1296(a1)
[0x800010a0]:ld t4, 1304(a1)
[0x800010a4]:addi s1, zero, 96
[0x800010a8]:csrrw zero, fcsr, s1
[0x800010ac]:fadd.s t6, t5, t4, dyn

[0x800010ac]:fadd.s t6, t5, t4, dyn
[0x800010b0]:csrrs a2, fcsr, zero
[0x800010b4]:sd t6, 1248(fp)
[0x800010b8]:sd a2, 1256(fp)
[0x800010bc]:ld t5, 1312(a1)
[0x800010c0]:ld t4, 1320(a1)
[0x800010c4]:addi s1, zero, 128
[0x800010c8]:csrrw zero, fcsr, s1
[0x800010cc]:fadd.s t6, t5, t4, dyn

[0x800010cc]:fadd.s t6, t5, t4, dyn
[0x800010d0]:csrrs a2, fcsr, zero
[0x800010d4]:sd t6, 1264(fp)
[0x800010d8]:sd a2, 1272(fp)
[0x800010dc]:ld t5, 1328(a1)
[0x800010e0]:ld t4, 1336(a1)
[0x800010e4]:addi s1, zero, 0
[0x800010e8]:csrrw zero, fcsr, s1
[0x800010ec]:fadd.s t6, t5, t4, dyn

[0x800010ec]:fadd.s t6, t5, t4, dyn
[0x800010f0]:csrrs a2, fcsr, zero
[0x800010f4]:sd t6, 1280(fp)
[0x800010f8]:sd a2, 1288(fp)
[0x800010fc]:ld t5, 1344(a1)
[0x80001100]:ld t4, 1352(a1)
[0x80001104]:addi s1, zero, 32
[0x80001108]:csrrw zero, fcsr, s1
[0x8000110c]:fadd.s t6, t5, t4, dyn

[0x8000110c]:fadd.s t6, t5, t4, dyn
[0x80001110]:csrrs a2, fcsr, zero
[0x80001114]:sd t6, 1296(fp)
[0x80001118]:sd a2, 1304(fp)
[0x8000111c]:ld t5, 1360(a1)
[0x80001120]:ld t4, 1368(a1)
[0x80001124]:addi s1, zero, 64
[0x80001128]:csrrw zero, fcsr, s1
[0x8000112c]:fadd.s t6, t5, t4, dyn

[0x8000112c]:fadd.s t6, t5, t4, dyn
[0x80001130]:csrrs a2, fcsr, zero
[0x80001134]:sd t6, 1312(fp)
[0x80001138]:sd a2, 1320(fp)
[0x8000113c]:ld t5, 1376(a1)
[0x80001140]:ld t4, 1384(a1)
[0x80001144]:addi s1, zero, 96
[0x80001148]:csrrw zero, fcsr, s1
[0x8000114c]:fadd.s t6, t5, t4, dyn

[0x8000114c]:fadd.s t6, t5, t4, dyn
[0x80001150]:csrrs a2, fcsr, zero
[0x80001154]:sd t6, 1328(fp)
[0x80001158]:sd a2, 1336(fp)
[0x8000115c]:ld t5, 1392(a1)
[0x80001160]:ld t4, 1400(a1)
[0x80001164]:addi s1, zero, 128
[0x80001168]:csrrw zero, fcsr, s1
[0x8000116c]:fadd.s t6, t5, t4, dyn

[0x8000116c]:fadd.s t6, t5, t4, dyn
[0x80001170]:csrrs a2, fcsr, zero
[0x80001174]:sd t6, 1344(fp)
[0x80001178]:sd a2, 1352(fp)
[0x8000117c]:ld t5, 1408(a1)
[0x80001180]:ld t4, 1416(a1)
[0x80001184]:addi s1, zero, 0
[0x80001188]:csrrw zero, fcsr, s1
[0x8000118c]:fadd.s t6, t5, t4, dyn

[0x8000118c]:fadd.s t6, t5, t4, dyn
[0x80001190]:csrrs a2, fcsr, zero
[0x80001194]:sd t6, 1360(fp)
[0x80001198]:sd a2, 1368(fp)
[0x8000119c]:ld t5, 1424(a1)
[0x800011a0]:ld t4, 1432(a1)
[0x800011a4]:addi s1, zero, 32
[0x800011a8]:csrrw zero, fcsr, s1
[0x800011ac]:fadd.s t6, t5, t4, dyn

[0x800011ac]:fadd.s t6, t5, t4, dyn
[0x800011b0]:csrrs a2, fcsr, zero
[0x800011b4]:sd t6, 1376(fp)
[0x800011b8]:sd a2, 1384(fp)
[0x800011bc]:ld t5, 1440(a1)
[0x800011c0]:ld t4, 1448(a1)
[0x800011c4]:addi s1, zero, 64
[0x800011c8]:csrrw zero, fcsr, s1
[0x800011cc]:fadd.s t6, t5, t4, dyn

[0x800011cc]:fadd.s t6, t5, t4, dyn
[0x800011d0]:csrrs a2, fcsr, zero
[0x800011d4]:sd t6, 1392(fp)
[0x800011d8]:sd a2, 1400(fp)
[0x800011dc]:ld t5, 1456(a1)
[0x800011e0]:ld t4, 1464(a1)
[0x800011e4]:addi s1, zero, 96
[0x800011e8]:csrrw zero, fcsr, s1
[0x800011ec]:fadd.s t6, t5, t4, dyn

[0x800011ec]:fadd.s t6, t5, t4, dyn
[0x800011f0]:csrrs a2, fcsr, zero
[0x800011f4]:sd t6, 1408(fp)
[0x800011f8]:sd a2, 1416(fp)
[0x800011fc]:ld t5, 1472(a1)
[0x80001200]:ld t4, 1480(a1)
[0x80001204]:addi s1, zero, 128
[0x80001208]:csrrw zero, fcsr, s1
[0x8000120c]:fadd.s t6, t5, t4, dyn

[0x8000120c]:fadd.s t6, t5, t4, dyn
[0x80001210]:csrrs a2, fcsr, zero
[0x80001214]:sd t6, 1424(fp)
[0x80001218]:sd a2, 1432(fp)
[0x8000121c]:ld t5, 1488(a1)
[0x80001220]:ld t4, 1496(a1)
[0x80001224]:addi s1, zero, 0
[0x80001228]:csrrw zero, fcsr, s1
[0x8000122c]:fadd.s t6, t5, t4, dyn

[0x8000122c]:fadd.s t6, t5, t4, dyn
[0x80001230]:csrrs a2, fcsr, zero
[0x80001234]:sd t6, 1440(fp)
[0x80001238]:sd a2, 1448(fp)
[0x8000123c]:ld t5, 1504(a1)
[0x80001240]:ld t4, 1512(a1)
[0x80001244]:addi s1, zero, 32
[0x80001248]:csrrw zero, fcsr, s1
[0x8000124c]:fadd.s t6, t5, t4, dyn

[0x8000124c]:fadd.s t6, t5, t4, dyn
[0x80001250]:csrrs a2, fcsr, zero
[0x80001254]:sd t6, 1456(fp)
[0x80001258]:sd a2, 1464(fp)
[0x8000125c]:ld t5, 1520(a1)
[0x80001260]:ld t4, 1528(a1)
[0x80001264]:addi s1, zero, 64
[0x80001268]:csrrw zero, fcsr, s1
[0x8000126c]:fadd.s t6, t5, t4, dyn

[0x8000126c]:fadd.s t6, t5, t4, dyn
[0x80001270]:csrrs a2, fcsr, zero
[0x80001274]:sd t6, 1472(fp)
[0x80001278]:sd a2, 1480(fp)
[0x8000127c]:ld t5, 1536(a1)
[0x80001280]:ld t4, 1544(a1)
[0x80001284]:addi s1, zero, 96
[0x80001288]:csrrw zero, fcsr, s1
[0x8000128c]:fadd.s t6, t5, t4, dyn

[0x8000128c]:fadd.s t6, t5, t4, dyn
[0x80001290]:csrrs a2, fcsr, zero
[0x80001294]:sd t6, 1488(fp)
[0x80001298]:sd a2, 1496(fp)
[0x8000129c]:ld t5, 1552(a1)
[0x800012a0]:ld t4, 1560(a1)
[0x800012a4]:addi s1, zero, 128
[0x800012a8]:csrrw zero, fcsr, s1
[0x800012ac]:fadd.s t6, t5, t4, dyn

[0x800012ac]:fadd.s t6, t5, t4, dyn
[0x800012b0]:csrrs a2, fcsr, zero
[0x800012b4]:sd t6, 1504(fp)
[0x800012b8]:sd a2, 1512(fp)
[0x800012bc]:ld t5, 1568(a1)
[0x800012c0]:ld t4, 1576(a1)
[0x800012c4]:addi s1, zero, 0
[0x800012c8]:csrrw zero, fcsr, s1
[0x800012cc]:fadd.s t6, t5, t4, dyn

[0x800012cc]:fadd.s t6, t5, t4, dyn
[0x800012d0]:csrrs a2, fcsr, zero
[0x800012d4]:sd t6, 1520(fp)
[0x800012d8]:sd a2, 1528(fp)
[0x800012dc]:ld t5, 1584(a1)
[0x800012e0]:ld t4, 1592(a1)
[0x800012e4]:addi s1, zero, 32
[0x800012e8]:csrrw zero, fcsr, s1
[0x800012ec]:fadd.s t6, t5, t4, dyn

[0x800012ec]:fadd.s t6, t5, t4, dyn
[0x800012f0]:csrrs a2, fcsr, zero
[0x800012f4]:sd t6, 1536(fp)
[0x800012f8]:sd a2, 1544(fp)
[0x800012fc]:ld t5, 1600(a1)
[0x80001300]:ld t4, 1608(a1)
[0x80001304]:addi s1, zero, 64
[0x80001308]:csrrw zero, fcsr, s1
[0x8000130c]:fadd.s t6, t5, t4, dyn

[0x8000130c]:fadd.s t6, t5, t4, dyn
[0x80001310]:csrrs a2, fcsr, zero
[0x80001314]:sd t6, 1552(fp)
[0x80001318]:sd a2, 1560(fp)
[0x8000131c]:ld t5, 1616(a1)
[0x80001320]:ld t4, 1624(a1)
[0x80001324]:addi s1, zero, 96
[0x80001328]:csrrw zero, fcsr, s1
[0x8000132c]:fadd.s t6, t5, t4, dyn

[0x8000132c]:fadd.s t6, t5, t4, dyn
[0x80001330]:csrrs a2, fcsr, zero
[0x80001334]:sd t6, 1568(fp)
[0x80001338]:sd a2, 1576(fp)
[0x8000133c]:ld t5, 1632(a1)
[0x80001340]:ld t4, 1640(a1)
[0x80001344]:addi s1, zero, 128
[0x80001348]:csrrw zero, fcsr, s1
[0x8000134c]:fadd.s t6, t5, t4, dyn

[0x8000134c]:fadd.s t6, t5, t4, dyn
[0x80001350]:csrrs a2, fcsr, zero
[0x80001354]:sd t6, 1584(fp)
[0x80001358]:sd a2, 1592(fp)
[0x8000135c]:ld t5, 1648(a1)
[0x80001360]:ld t4, 1656(a1)
[0x80001364]:addi s1, zero, 0
[0x80001368]:csrrw zero, fcsr, s1
[0x8000136c]:fadd.s t6, t5, t4, dyn

[0x8000136c]:fadd.s t6, t5, t4, dyn
[0x80001370]:csrrs a2, fcsr, zero
[0x80001374]:sd t6, 1600(fp)
[0x80001378]:sd a2, 1608(fp)
[0x8000137c]:ld t5, 1664(a1)
[0x80001380]:ld t4, 1672(a1)
[0x80001384]:addi s1, zero, 32
[0x80001388]:csrrw zero, fcsr, s1
[0x8000138c]:fadd.s t6, t5, t4, dyn

[0x8000138c]:fadd.s t6, t5, t4, dyn
[0x80001390]:csrrs a2, fcsr, zero
[0x80001394]:sd t6, 1616(fp)
[0x80001398]:sd a2, 1624(fp)
[0x8000139c]:ld t5, 1680(a1)
[0x800013a0]:ld t4, 1688(a1)
[0x800013a4]:addi s1, zero, 64
[0x800013a8]:csrrw zero, fcsr, s1
[0x800013ac]:fadd.s t6, t5, t4, dyn

[0x800013ac]:fadd.s t6, t5, t4, dyn
[0x800013b0]:csrrs a2, fcsr, zero
[0x800013b4]:sd t6, 1632(fp)
[0x800013b8]:sd a2, 1640(fp)
[0x800013bc]:ld t5, 1696(a1)
[0x800013c0]:ld t4, 1704(a1)
[0x800013c4]:addi s1, zero, 96
[0x800013c8]:csrrw zero, fcsr, s1
[0x800013cc]:fadd.s t6, t5, t4, dyn

[0x800013cc]:fadd.s t6, t5, t4, dyn
[0x800013d0]:csrrs a2, fcsr, zero
[0x800013d4]:sd t6, 1648(fp)
[0x800013d8]:sd a2, 1656(fp)
[0x800013dc]:ld t5, 1712(a1)
[0x800013e0]:ld t4, 1720(a1)
[0x800013e4]:addi s1, zero, 128
[0x800013e8]:csrrw zero, fcsr, s1
[0x800013ec]:fadd.s t6, t5, t4, dyn

[0x800013ec]:fadd.s t6, t5, t4, dyn
[0x800013f0]:csrrs a2, fcsr, zero
[0x800013f4]:sd t6, 1664(fp)
[0x800013f8]:sd a2, 1672(fp)
[0x800013fc]:ld t5, 1728(a1)
[0x80001400]:ld t4, 1736(a1)
[0x80001404]:addi s1, zero, 0
[0x80001408]:csrrw zero, fcsr, s1
[0x8000140c]:fadd.s t6, t5, t4, dyn

[0x8000140c]:fadd.s t6, t5, t4, dyn
[0x80001410]:csrrs a2, fcsr, zero
[0x80001414]:sd t6, 1680(fp)
[0x80001418]:sd a2, 1688(fp)
[0x8000141c]:ld t5, 1744(a1)
[0x80001420]:ld t4, 1752(a1)
[0x80001424]:addi s1, zero, 32
[0x80001428]:csrrw zero, fcsr, s1
[0x8000142c]:fadd.s t6, t5, t4, dyn

[0x8000142c]:fadd.s t6, t5, t4, dyn
[0x80001430]:csrrs a2, fcsr, zero
[0x80001434]:sd t6, 1696(fp)
[0x80001438]:sd a2, 1704(fp)
[0x8000143c]:ld t5, 1760(a1)
[0x80001440]:ld t4, 1768(a1)
[0x80001444]:addi s1, zero, 64
[0x80001448]:csrrw zero, fcsr, s1
[0x8000144c]:fadd.s t6, t5, t4, dyn

[0x8000144c]:fadd.s t6, t5, t4, dyn
[0x80001450]:csrrs a2, fcsr, zero
[0x80001454]:sd t6, 1712(fp)
[0x80001458]:sd a2, 1720(fp)
[0x8000145c]:ld t5, 1776(a1)
[0x80001460]:ld t4, 1784(a1)
[0x80001464]:addi s1, zero, 96
[0x80001468]:csrrw zero, fcsr, s1
[0x8000146c]:fadd.s t6, t5, t4, dyn

[0x8000146c]:fadd.s t6, t5, t4, dyn
[0x80001470]:csrrs a2, fcsr, zero
[0x80001474]:sd t6, 1728(fp)
[0x80001478]:sd a2, 1736(fp)
[0x8000147c]:ld t5, 1792(a1)
[0x80001480]:ld t4, 1800(a1)
[0x80001484]:addi s1, zero, 128
[0x80001488]:csrrw zero, fcsr, s1
[0x8000148c]:fadd.s t6, t5, t4, dyn

[0x8000148c]:fadd.s t6, t5, t4, dyn
[0x80001490]:csrrs a2, fcsr, zero
[0x80001494]:sd t6, 1744(fp)
[0x80001498]:sd a2, 1752(fp)
[0x8000149c]:ld t5, 1808(a1)
[0x800014a0]:ld t4, 1816(a1)
[0x800014a4]:addi s1, zero, 0
[0x800014a8]:csrrw zero, fcsr, s1
[0x800014ac]:fadd.s t6, t5, t4, dyn

[0x800014ac]:fadd.s t6, t5, t4, dyn
[0x800014b0]:csrrs a2, fcsr, zero
[0x800014b4]:sd t6, 1760(fp)
[0x800014b8]:sd a2, 1768(fp)
[0x800014bc]:ld t5, 1824(a1)
[0x800014c0]:ld t4, 1832(a1)
[0x800014c4]:addi s1, zero, 32
[0x800014c8]:csrrw zero, fcsr, s1
[0x800014cc]:fadd.s t6, t5, t4, dyn

[0x800014cc]:fadd.s t6, t5, t4, dyn
[0x800014d0]:csrrs a2, fcsr, zero
[0x800014d4]:sd t6, 1776(fp)
[0x800014d8]:sd a2, 1784(fp)
[0x800014dc]:ld t5, 1840(a1)
[0x800014e0]:ld t4, 1848(a1)
[0x800014e4]:addi s1, zero, 64
[0x800014e8]:csrrw zero, fcsr, s1
[0x800014ec]:fadd.s t6, t5, t4, dyn

[0x800014ec]:fadd.s t6, t5, t4, dyn
[0x800014f0]:csrrs a2, fcsr, zero
[0x800014f4]:sd t6, 1792(fp)
[0x800014f8]:sd a2, 1800(fp)
[0x800014fc]:ld t5, 1856(a1)
[0x80001500]:ld t4, 1864(a1)
[0x80001504]:addi s1, zero, 96
[0x80001508]:csrrw zero, fcsr, s1
[0x8000150c]:fadd.s t6, t5, t4, dyn

[0x8000150c]:fadd.s t6, t5, t4, dyn
[0x80001510]:csrrs a2, fcsr, zero
[0x80001514]:sd t6, 1808(fp)
[0x80001518]:sd a2, 1816(fp)
[0x8000151c]:ld t5, 1872(a1)
[0x80001520]:ld t4, 1880(a1)
[0x80001524]:addi s1, zero, 128
[0x80001528]:csrrw zero, fcsr, s1
[0x8000152c]:fadd.s t6, t5, t4, dyn

[0x8000152c]:fadd.s t6, t5, t4, dyn
[0x80001530]:csrrs a2, fcsr, zero
[0x80001534]:sd t6, 1824(fp)
[0x80001538]:sd a2, 1832(fp)
[0x8000153c]:ld t5, 1888(a1)
[0x80001540]:ld t4, 1896(a1)
[0x80001544]:addi s1, zero, 0
[0x80001548]:csrrw zero, fcsr, s1
[0x8000154c]:fadd.s t6, t5, t4, dyn

[0x8000154c]:fadd.s t6, t5, t4, dyn
[0x80001550]:csrrs a2, fcsr, zero
[0x80001554]:sd t6, 1840(fp)
[0x80001558]:sd a2, 1848(fp)
[0x8000155c]:ld t5, 1904(a1)
[0x80001560]:ld t4, 1912(a1)
[0x80001564]:addi s1, zero, 64
[0x80001568]:csrrw zero, fcsr, s1
[0x8000156c]:fadd.s t6, t5, t4, dyn

[0x8000156c]:fadd.s t6, t5, t4, dyn
[0x80001570]:csrrs a2, fcsr, zero
[0x80001574]:sd t6, 1856(fp)
[0x80001578]:sd a2, 1864(fp)
[0x8000157c]:ld t5, 1920(a1)
[0x80001580]:ld t4, 1928(a1)
[0x80001584]:addi s1, zero, 0
[0x80001588]:csrrw zero, fcsr, s1
[0x8000158c]:fadd.s t6, t5, t4, dyn

[0x8000158c]:fadd.s t6, t5, t4, dyn
[0x80001590]:csrrs a2, fcsr, zero
[0x80001594]:sd t6, 1872(fp)
[0x80001598]:sd a2, 1880(fp)
[0x8000159c]:ld t5, 1936(a1)
[0x800015a0]:ld t4, 1944(a1)
[0x800015a4]:addi s1, zero, 64
[0x800015a8]:csrrw zero, fcsr, s1
[0x800015ac]:fadd.s t6, t5, t4, dyn

[0x800015ac]:fadd.s t6, t5, t4, dyn
[0x800015b0]:csrrs a2, fcsr, zero
[0x800015b4]:sd t6, 1888(fp)
[0x800015b8]:sd a2, 1896(fp)
[0x800015bc]:ld t5, 1952(a1)
[0x800015c0]:ld t4, 1960(a1)
[0x800015c4]:addi s1, zero, 96
[0x800015c8]:csrrw zero, fcsr, s1
[0x800015cc]:fadd.s t6, t5, t4, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003bc]:fadd.s t6, t6, t6, dyn
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
	-[0x800003c8]:sd tp, 8(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x80003b20]:0x0000000000000005




Last Coverpoint : ['rs1 : x29', 'rs2 : x28', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x6ef7d1 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003dc]:fadd.s t5, t4, t3, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t5, 16(ra)
	-[0x800003e8]:sd tp, 24(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x80003b30]:0x0000000000000021




Last Coverpoint : ['rs1 : x30', 'rs2 : x30', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800003fc]:fadd.s t4, t5, t5, dyn
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t4, 32(ra)
	-[0x80000408]:sd tp, 40(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x80003b40]:0x0000000000000045




Last Coverpoint : ['rs1 : x28', 'rs2 : x29', 'rd : x28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x6ef7d1 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fadd.s t3, t3, t4, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t3, 48(ra)
	-[0x80000428]:sd tp, 56(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x80003b50]:0x0000000000000061




Last Coverpoint : ['rs1 : x26', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x6ef7d1 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000043c]:fadd.s s11, s10, s11, dyn
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s11, 64(ra)
	-[0x80000448]:sd tp, 72(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x80003b60]:0x0000000000000081




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5e8020 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fadd.s s10, s11, s9, dyn
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s10, 80(ra)
	-[0x80000468]:sd tp, 88(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x80003b70]:0x0000000000000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5e8020 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fadd.s s9, s8, s10, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
	-[0x80000488]:sd tp, 104(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x80003b80]:0x0000000000000021




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5e8020 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fadd.s s8, s9, s7, dyn
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
	-[0x800004a8]:sd tp, 120(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x80003b90]:0x0000000000000041




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5e8020 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fadd.s s7, s6, s8, dyn
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
	-[0x800004c8]:sd tp, 136(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x80003ba0]:0x0000000000000061




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x217fdd and fs2 == 0 and fe2 == 0xfd and fm2 == 0x5e8020 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.s s6, s7, s5, dyn
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
	-[0x800004e8]:sd tp, 152(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x80003bb0]:0x0000000000000081




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3baba6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fadd.s s5, s4, s6, dyn
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
	-[0x80000508]:sd tp, 168(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x80003bc0]:0x0000000000000001




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3baba6 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.s s4, s5, s3, dyn
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
	-[0x80000528]:sd tp, 184(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x80003bd0]:0x0000000000000021




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3baba6 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fadd.s s3, s2, s4, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
	-[0x80000548]:sd tp, 200(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x80003be0]:0x0000000000000041




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3baba6 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fadd.s s2, s3, a7, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
	-[0x80000568]:sd tp, 216(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x80003bf0]:0x0000000000000061




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x445459 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3baba6 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fadd.s a7, a6, s2, dyn
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
	-[0x80000588]:sd tp, 232(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x80003c00]:0x0000000000000081




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x260aeb and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fadd.s a6, a7, a5, dyn
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
	-[0x800005a8]:sd tp, 248(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x80003c10]:0x0000000000000001




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x260aeb and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fadd.s a5, a4, a6, dyn
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
	-[0x800005c8]:sd tp, 264(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x80003c20]:0x0000000000000021




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x260aeb and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fadd.s a4, a5, a3, dyn
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
	-[0x800005e8]:sd tp, 280(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x80003c30]:0x0000000000000041




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x260aeb and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fadd.s a3, a2, a4, dyn
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
	-[0x80000608]:sd tp, 296(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x80003c40]:0x0000000000000061




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x167d44 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x260aeb and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fadd.s a2, a3, a1, dyn
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
	-[0x80000628]:sd tp, 312(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x80003c50]:0x0000000000000081




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x48fc9c and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fadd.s a1, a0, a2, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
	-[0x80000648]:sd tp, 328(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x80003c60]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x48fc9c and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.s a0, a1, s1, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:sd a0, 336(ra)
	-[0x80000668]:sd tp, 344(ra)
Current Store : [0x80000668] : sd tp, 344(ra) -- Store: [0x80003c70]:0x0000000000000020




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x48fc9c and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fadd.s s1, fp, a0, dyn
	-[0x80000688]:csrrs a2, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
	-[0x80000690]:sd a2, 360(ra)
Current Store : [0x80000690] : sd a2, 360(ra) -- Store: [0x80003c80]:0x0000000000000040




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x48fc9c and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fadd.s fp, s1, t2, dyn
	-[0x800006a8]:csrrs a2, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
	-[0x800006b0]:sd a2, 376(ra)
Current Store : [0x800006b0] : sd a2, 376(ra) -- Store: [0x80003c90]:0x0000000000000060




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xfd and fm1 == 0x370362 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x48fc9c and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fadd.s t2, t1, fp, dyn
	-[0x800006c8]:csrrs a2, fcsr, zero
	-[0x800006cc]:sd t2, 384(ra)
	-[0x800006d0]:sd a2, 392(ra)
Current Store : [0x800006d0] : sd a2, 392(ra) -- Store: [0x80003ca0]:0x0000000000000080




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0d2378 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fadd.s t1, t2, t0, dyn
	-[0x800006f0]:csrrs a2, fcsr, zero
	-[0x800006f4]:sd t1, 0(fp)
	-[0x800006f8]:sd a2, 8(fp)
Current Store : [0x800006f8] : sd a2, 8(fp) -- Store: [0x80003cb0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0d2378 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fadd.s t0, tp, t1, dyn
	-[0x80000710]:csrrs a2, fcsr, zero
	-[0x80000714]:sd t0, 16(fp)
	-[0x80000718]:sd a2, 24(fp)
Current Store : [0x80000718] : sd a2, 24(fp) -- Store: [0x80003cc0]:0x0000000000000020




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0d2378 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fadd.s tp, t0, gp, dyn
	-[0x80000730]:csrrs a2, fcsr, zero
	-[0x80000734]:sd tp, 32(fp)
	-[0x80000738]:sd a2, 40(fp)
Current Store : [0x80000738] : sd a2, 40(fp) -- Store: [0x80003cd0]:0x0000000000000040




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0d2378 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fadd.s gp, sp, tp, dyn
	-[0x80000750]:csrrs a2, fcsr, zero
	-[0x80000754]:sd gp, 48(fp)
	-[0x80000758]:sd a2, 56(fp)
Current Store : [0x80000758] : sd a2, 56(fp) -- Store: [0x80003ce0]:0x0000000000000060




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x2e5b90 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0d2378 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000076c]:fadd.s sp, gp, ra, dyn
	-[0x80000770]:csrrs a2, fcsr, zero
	-[0x80000774]:sd sp, 64(fp)
	-[0x80000778]:sd a2, 72(fp)
Current Store : [0x80000778] : sd a2, 72(fp) -- Store: [0x80003cf0]:0x0000000000000080




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x8000078c]:fadd.s ra, zero, sp, dyn
	-[0x80000790]:csrrs a2, fcsr, zero
	-[0x80000794]:sd ra, 80(fp)
	-[0x80000798]:sd a2, 88(fp)
Current Store : [0x80000798] : sd a2, 88(fp) -- Store: [0x80003d00]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fadd.s t6, ra, t5, dyn
	-[0x800007b0]:csrrs a2, fcsr, zero
	-[0x800007b4]:sd t6, 96(fp)
	-[0x800007b8]:sd a2, 104(fp)
Current Store : [0x800007b8] : sd a2, 104(fp) -- Store: [0x80003d10]:0x0000000000000021




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x800007cc]:fadd.s t6, t5, zero, dyn
	-[0x800007d0]:csrrs a2, fcsr, zero
	-[0x800007d4]:sd t6, 112(fp)
	-[0x800007d8]:sd a2, 120(fp)
Current Store : [0x800007d8] : sd a2, 120(fp) -- Store: [0x80003d20]:0x0000000000000040




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fadd.s zero, t6, t5, dyn
	-[0x800007f0]:csrrs a2, fcsr, zero
	-[0x800007f4]:sd zero, 128(fp)
	-[0x800007f8]:sd a2, 136(fp)
Current Store : [0x800007f8] : sd a2, 136(fp) -- Store: [0x80003d30]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fadd.s t6, t5, t4, dyn
	-[0x80000810]:csrrs a2, fcsr, zero
	-[0x80000814]:sd t6, 144(fp)
	-[0x80000818]:sd a2, 152(fp)
Current Store : [0x80000818] : sd a2, 152(fp) -- Store: [0x80003d40]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x128bd9 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fadd.s t6, t5, t4, dyn
	-[0x80000830]:csrrs a2, fcsr, zero
	-[0x80000834]:sd t6, 160(fp)
	-[0x80000838]:sd a2, 168(fp)
Current Store : [0x80000838] : sd a2, 168(fp) -- Store: [0x80003d50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x128bd9 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fadd.s t6, t5, t4, dyn
	-[0x80000850]:csrrs a2, fcsr, zero
	-[0x80000854]:sd t6, 176(fp)
	-[0x80000858]:sd a2, 184(fp)
Current Store : [0x80000858] : sd a2, 184(fp) -- Store: [0x80003d60]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x128bd9 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fadd.s t6, t5, t4, dyn
	-[0x80000870]:csrrs a2, fcsr, zero
	-[0x80000874]:sd t6, 192(fp)
	-[0x80000878]:sd a2, 200(fp)
Current Store : [0x80000878] : sd a2, 200(fp) -- Store: [0x80003d70]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x128bd9 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fadd.s t6, t5, t4, dyn
	-[0x80000890]:csrrs a2, fcsr, zero
	-[0x80000894]:sd t6, 208(fp)
	-[0x80000898]:sd a2, 216(fp)
Current Store : [0x80000898] : sd a2, 216(fp) -- Store: [0x80003d80]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x6d7424 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x128bd9 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fadd.s t6, t5, t4, dyn
	-[0x800008b0]:csrrs a2, fcsr, zero
	-[0x800008b4]:sd t6, 224(fp)
	-[0x800008b8]:sd a2, 232(fp)
Current Store : [0x800008b8] : sd a2, 232(fp) -- Store: [0x80003d90]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0aa137 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fadd.s t6, t5, t4, dyn
	-[0x800008d0]:csrrs a2, fcsr, zero
	-[0x800008d4]:sd t6, 240(fp)
	-[0x800008d8]:sd a2, 248(fp)
Current Store : [0x800008d8] : sd a2, 248(fp) -- Store: [0x80003da0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0aa137 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fadd.s t6, t5, t4, dyn
	-[0x800008f0]:csrrs a2, fcsr, zero
	-[0x800008f4]:sd t6, 256(fp)
	-[0x800008f8]:sd a2, 264(fp)
Current Store : [0x800008f8] : sd a2, 264(fp) -- Store: [0x80003db0]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0aa137 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fadd.s t6, t5, t4, dyn
	-[0x80000910]:csrrs a2, fcsr, zero
	-[0x80000914]:sd t6, 272(fp)
	-[0x80000918]:sd a2, 280(fp)
Current Store : [0x80000918] : sd a2, 280(fp) -- Store: [0x80003dc0]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0aa137 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fadd.s t6, t5, t4, dyn
	-[0x80000930]:csrrs a2, fcsr, zero
	-[0x80000934]:sd t6, 288(fp)
	-[0x80000938]:sd a2, 296(fp)
Current Store : [0x80000938] : sd a2, 296(fp) -- Store: [0x80003dd0]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2eabd8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x0aa137 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fadd.s t6, t5, t4, dyn
	-[0x80000950]:csrrs a2, fcsr, zero
	-[0x80000954]:sd t6, 304(fp)
	-[0x80000958]:sd a2, 312(fp)
Current Store : [0x80000958] : sd a2, 312(fp) -- Store: [0x80003de0]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfc and fm2 == 0x11c0cd and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fadd.s t6, t5, t4, dyn
	-[0x80000970]:csrrs a2, fcsr, zero
	-[0x80000974]:sd t6, 320(fp)
	-[0x80000978]:sd a2, 328(fp)
Current Store : [0x80000978] : sd a2, 328(fp) -- Store: [0x80003df0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfc and fm2 == 0x11c0cd and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fadd.s t6, t5, t4, dyn
	-[0x80000990]:csrrs a2, fcsr, zero
	-[0x80000994]:sd t6, 336(fp)
	-[0x80000998]:sd a2, 344(fp)
Current Store : [0x80000998] : sd a2, 344(fp) -- Store: [0x80003e00]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfc and fm2 == 0x11c0cd and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fadd.s t6, t5, t4, dyn
	-[0x800009b0]:csrrs a2, fcsr, zero
	-[0x800009b4]:sd t6, 352(fp)
	-[0x800009b8]:sd a2, 360(fp)
Current Store : [0x800009b8] : sd a2, 360(fp) -- Store: [0x80003e10]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfc and fm2 == 0x11c0cd and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.s t6, t5, t4, dyn
	-[0x800009d0]:csrrs a2, fcsr, zero
	-[0x800009d4]:sd t6, 368(fp)
	-[0x800009d8]:sd a2, 376(fp)
Current Store : [0x800009d8] : sd a2, 376(fp) -- Store: [0x80003e20]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1b8fcb and fs2 == 0 and fe2 == 0xfc and fm2 == 0x11c0cd and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fadd.s t6, t5, t4, dyn
	-[0x800009f0]:csrrs a2, fcsr, zero
	-[0x800009f4]:sd t6, 384(fp)
	-[0x800009f8]:sd a2, 392(fp)
Current Store : [0x800009f8] : sd a2, 392(fp) -- Store: [0x80003e30]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x0473a0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fadd.s t6, t5, t4, dyn
	-[0x80000a10]:csrrs a2, fcsr, zero
	-[0x80000a14]:sd t6, 400(fp)
	-[0x80000a18]:sd a2, 408(fp)
Current Store : [0x80000a18] : sd a2, 408(fp) -- Store: [0x80003e40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x0473a0 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fadd.s t6, t5, t4, dyn
	-[0x80000a30]:csrrs a2, fcsr, zero
	-[0x80000a34]:sd t6, 416(fp)
	-[0x80000a38]:sd a2, 424(fp)
Current Store : [0x80000a38] : sd a2, 424(fp) -- Store: [0x80003e50]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x0473a0 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fadd.s t6, t5, t4, dyn
	-[0x80000a50]:csrrs a2, fcsr, zero
	-[0x80000a54]:sd t6, 432(fp)
	-[0x80000a58]:sd a2, 440(fp)
Current Store : [0x80000a58] : sd a2, 440(fp) -- Store: [0x80003e60]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x0473a0 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fadd.s t6, t5, t4, dyn
	-[0x80000a70]:csrrs a2, fcsr, zero
	-[0x80000a74]:sd t6, 448(fp)
	-[0x80000a78]:sd a2, 456(fp)
Current Store : [0x80000a78] : sd a2, 456(fp) -- Store: [0x80003e70]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x6e317d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x0473a0 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fadd.s t6, t5, t4, dyn
	-[0x80000a90]:csrrs a2, fcsr, zero
	-[0x80000a94]:sd t6, 464(fp)
	-[0x80000a98]:sd a2, 472(fp)
Current Store : [0x80000a98] : sd a2, 472(fp) -- Store: [0x80003e80]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1b6263 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fadd.s t6, t5, t4, dyn
	-[0x80000ab0]:csrrs a2, fcsr, zero
	-[0x80000ab4]:sd t6, 480(fp)
	-[0x80000ab8]:sd a2, 488(fp)
Current Store : [0x80000ab8] : sd a2, 488(fp) -- Store: [0x80003e90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1b6263 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fadd.s t6, t5, t4, dyn
	-[0x80000ad0]:csrrs a2, fcsr, zero
	-[0x80000ad4]:sd t6, 496(fp)
	-[0x80000ad8]:sd a2, 504(fp)
Current Store : [0x80000ad8] : sd a2, 504(fp) -- Store: [0x80003ea0]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1b6263 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fadd.s t6, t5, t4, dyn
	-[0x80000af0]:csrrs a2, fcsr, zero
	-[0x80000af4]:sd t6, 512(fp)
	-[0x80000af8]:sd a2, 520(fp)
Current Store : [0x80000af8] : sd a2, 520(fp) -- Store: [0x80003eb0]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1b6263 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fadd.s t6, t5, t4, dyn
	-[0x80000b10]:csrrs a2, fcsr, zero
	-[0x80000b14]:sd t6, 528(fp)
	-[0x80000b18]:sd a2, 536(fp)
Current Store : [0x80000b18] : sd a2, 536(fp) -- Store: [0x80003ec0]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2c93b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1b6263 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fadd.s t6, t5, t4, dyn
	-[0x80000b30]:csrrs a2, fcsr, zero
	-[0x80000b34]:sd t6, 544(fp)
	-[0x80000b38]:sd a2, 552(fp)
Current Store : [0x80000b38] : sd a2, 552(fp) -- Store: [0x80003ed0]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4ab27b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fadd.s t6, t5, t4, dyn
	-[0x80000b50]:csrrs a2, fcsr, zero
	-[0x80000b54]:sd t6, 560(fp)
	-[0x80000b58]:sd a2, 568(fp)
Current Store : [0x80000b58] : sd a2, 568(fp) -- Store: [0x80003ee0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4ab27b and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fadd.s t6, t5, t4, dyn
	-[0x80000b70]:csrrs a2, fcsr, zero
	-[0x80000b74]:sd t6, 576(fp)
	-[0x80000b78]:sd a2, 584(fp)
Current Store : [0x80000b78] : sd a2, 584(fp) -- Store: [0x80003ef0]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4ab27b and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fadd.s t6, t5, t4, dyn
	-[0x80000b90]:csrrs a2, fcsr, zero
	-[0x80000b94]:sd t6, 592(fp)
	-[0x80000b98]:sd a2, 600(fp)
Current Store : [0x80000b98] : sd a2, 600(fp) -- Store: [0x80003f00]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4ab27b and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.s t6, t5, t4, dyn
	-[0x80000bb0]:csrrs a2, fcsr, zero
	-[0x80000bb4]:sd t6, 608(fp)
	-[0x80000bb8]:sd a2, 616(fp)
Current Store : [0x80000bb8] : sd a2, 616(fp) -- Store: [0x80003f10]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x354d84 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x4ab27b and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fadd.s t6, t5, t4, dyn
	-[0x80000bd0]:csrrs a2, fcsr, zero
	-[0x80000bd4]:sd t6, 624(fp)
	-[0x80000bd8]:sd a2, 632(fp)
Current Store : [0x80000bd8] : sd a2, 632(fp) -- Store: [0x80003f20]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x59472a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fadd.s t6, t5, t4, dyn
	-[0x80000bf0]:csrrs a2, fcsr, zero
	-[0x80000bf4]:sd t6, 640(fp)
	-[0x80000bf8]:sd a2, 648(fp)
Current Store : [0x80000bf8] : sd a2, 648(fp) -- Store: [0x80003f30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x59472a and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fadd.s t6, t5, t4, dyn
	-[0x80000c10]:csrrs a2, fcsr, zero
	-[0x80000c14]:sd t6, 656(fp)
	-[0x80000c18]:sd a2, 664(fp)
Current Store : [0x80000c18] : sd a2, 664(fp) -- Store: [0x80003f40]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x59472a and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fadd.s t6, t5, t4, dyn
	-[0x80000c30]:csrrs a2, fcsr, zero
	-[0x80000c34]:sd t6, 672(fp)
	-[0x80000c38]:sd a2, 680(fp)
Current Store : [0x80000c38] : sd a2, 680(fp) -- Store: [0x80003f50]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x59472a and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fadd.s t6, t5, t4, dyn
	-[0x80000c50]:csrrs a2, fcsr, zero
	-[0x80000c54]:sd t6, 688(fp)
	-[0x80000c58]:sd a2, 696(fp)
Current Store : [0x80000c58] : sd a2, 696(fp) -- Store: [0x80003f60]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x26b8d3 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x59472a and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fadd.s t6, t5, t4, dyn
	-[0x80000c70]:csrrs a2, fcsr, zero
	-[0x80000c74]:sd t6, 704(fp)
	-[0x80000c78]:sd a2, 712(fp)
Current Store : [0x80000c78] : sd a2, 712(fp) -- Store: [0x80003f70]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x174c51 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fadd.s t6, t5, t4, dyn
	-[0x80000c90]:csrrs a2, fcsr, zero
	-[0x80000c94]:sd t6, 720(fp)
	-[0x80000c98]:sd a2, 728(fp)
Current Store : [0x80000c98] : sd a2, 728(fp) -- Store: [0x80003f80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x174c51 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fadd.s t6, t5, t4, dyn
	-[0x80000cb0]:csrrs a2, fcsr, zero
	-[0x80000cb4]:sd t6, 736(fp)
	-[0x80000cb8]:sd a2, 744(fp)
Current Store : [0x80000cb8] : sd a2, 744(fp) -- Store: [0x80003f90]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x174c51 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fadd.s t6, t5, t4, dyn
	-[0x80000cd0]:csrrs a2, fcsr, zero
	-[0x80000cd4]:sd t6, 752(fp)
	-[0x80000cd8]:sd a2, 760(fp)
Current Store : [0x80000cd8] : sd a2, 760(fp) -- Store: [0x80003fa0]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x174c51 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.s t6, t5, t4, dyn
	-[0x80000cf0]:csrrs a2, fcsr, zero
	-[0x80000cf4]:sd t6, 768(fp)
	-[0x80000cf8]:sd a2, 776(fp)
Current Store : [0x80000cf8] : sd a2, 776(fp) -- Store: [0x80003fb0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x2f4c51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x174c51 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fadd.s t6, t5, t4, dyn
	-[0x80000d10]:csrrs a2, fcsr, zero
	-[0x80000d14]:sd t6, 784(fp)
	-[0x80000d18]:sd a2, 792(fp)
Current Store : [0x80000d18] : sd a2, 792(fp) -- Store: [0x80003fc0]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x672bf7 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fadd.s t6, t5, t4, dyn
	-[0x80000d30]:csrrs a2, fcsr, zero
	-[0x80000d34]:sd t6, 800(fp)
	-[0x80000d38]:sd a2, 808(fp)
Current Store : [0x80000d38] : sd a2, 808(fp) -- Store: [0x80003fd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x672bf7 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fadd.s t6, t5, t4, dyn
	-[0x80000d50]:csrrs a2, fcsr, zero
	-[0x80000d54]:sd t6, 816(fp)
	-[0x80000d58]:sd a2, 824(fp)
Current Store : [0x80000d58] : sd a2, 824(fp) -- Store: [0x80003fe0]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x672bf7 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fadd.s t6, t5, t4, dyn
	-[0x80000d70]:csrrs a2, fcsr, zero
	-[0x80000d74]:sd t6, 832(fp)
	-[0x80000d78]:sd a2, 840(fp)
Current Store : [0x80000d78] : sd a2, 840(fp) -- Store: [0x80003ff0]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x672bf7 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fadd.s t6, t5, t4, dyn
	-[0x80000d90]:csrrs a2, fcsr, zero
	-[0x80000d94]:sd t6, 848(fp)
	-[0x80000d98]:sd a2, 856(fp)
Current Store : [0x80000d98] : sd a2, 856(fp) -- Store: [0x80004000]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x372bf7 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x672bf7 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fadd.s t6, t5, t4, dyn
	-[0x80000db0]:csrrs a2, fcsr, zero
	-[0x80000db4]:sd t6, 864(fp)
	-[0x80000db8]:sd a2, 872(fp)
Current Store : [0x80000db8] : sd a2, 872(fp) -- Store: [0x80004010]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x00edf4 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dcc]:fadd.s t6, t5, t4, dyn
	-[0x80000dd0]:csrrs a2, fcsr, zero
	-[0x80000dd4]:sd t6, 880(fp)
	-[0x80000dd8]:sd a2, 888(fp)
Current Store : [0x80000dd8] : sd a2, 888(fp) -- Store: [0x80004020]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x00edf4 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000dec]:fadd.s t6, t5, t4, dyn
	-[0x80000df0]:csrrs a2, fcsr, zero
	-[0x80000df4]:sd t6, 896(fp)
	-[0x80000df8]:sd a2, 904(fp)
Current Store : [0x80000df8] : sd a2, 904(fp) -- Store: [0x80004030]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x00edf4 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fadd.s t6, t5, t4, dyn
	-[0x80000e10]:csrrs a2, fcsr, zero
	-[0x80000e14]:sd t6, 912(fp)
	-[0x80000e18]:sd a2, 920(fp)
Current Store : [0x80000e18] : sd a2, 920(fp) -- Store: [0x80004040]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x00edf4 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fadd.s t6, t5, t4, dyn
	-[0x80000e30]:csrrs a2, fcsr, zero
	-[0x80000e34]:sd t6, 928(fp)
	-[0x80000e38]:sd a2, 936(fp)
Current Store : [0x80000e38] : sd a2, 936(fp) -- Store: [0x80004050]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x480ede and fs2 == 1 and fe2 == 0xf8 and fm2 == 0x00edf4 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fadd.s t6, t5, t4, dyn
	-[0x80000e50]:csrrs a2, fcsr, zero
	-[0x80000e54]:sd t6, 944(fp)
	-[0x80000e58]:sd a2, 952(fp)
Current Store : [0x80000e58] : sd a2, 952(fp) -- Store: [0x80004060]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1959aa and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fadd.s t6, t5, t4, dyn
	-[0x80000e70]:csrrs a2, fcsr, zero
	-[0x80000e74]:sd t6, 960(fp)
	-[0x80000e78]:sd a2, 968(fp)
Current Store : [0x80000e78] : sd a2, 968(fp) -- Store: [0x80004070]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1959aa and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000e8c]:fadd.s t6, t5, t4, dyn
	-[0x80000e90]:csrrs a2, fcsr, zero
	-[0x80000e94]:sd t6, 976(fp)
	-[0x80000e98]:sd a2, 984(fp)
Current Store : [0x80000e98] : sd a2, 984(fp) -- Store: [0x80004080]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1959aa and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eac]:fadd.s t6, t5, t4, dyn
	-[0x80000eb0]:csrrs a2, fcsr, zero
	-[0x80000eb4]:sd t6, 992(fp)
	-[0x80000eb8]:sd a2, 1000(fp)
Current Store : [0x80000eb8] : sd a2, 1000(fp) -- Store: [0x80004090]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1959aa and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fadd.s t6, t5, t4, dyn
	-[0x80000ed0]:csrrs a2, fcsr, zero
	-[0x80000ed4]:sd t6, 1008(fp)
	-[0x80000ed8]:sd a2, 1016(fp)
Current Store : [0x80000ed8] : sd a2, 1016(fp) -- Store: [0x800040a0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x52b355 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x1959aa and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eec]:fadd.s t6, t5, t4, dyn
	-[0x80000ef0]:csrrs a2, fcsr, zero
	-[0x80000ef4]:sd t6, 1024(fp)
	-[0x80000ef8]:sd a2, 1032(fp)
Current Store : [0x80000ef8] : sd a2, 1032(fp) -- Store: [0x800040b0]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 1 and fe2 == 0xfc and fm2 == 0x28bbb2 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fadd.s t6, t5, t4, dyn
	-[0x80000f10]:csrrs a2, fcsr, zero
	-[0x80000f14]:sd t6, 1040(fp)
	-[0x80000f18]:sd a2, 1048(fp)
Current Store : [0x80000f18] : sd a2, 1048(fp) -- Store: [0x800040c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 1 and fe2 == 0xfc and fm2 == 0x28bbb2 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fadd.s t6, t5, t4, dyn
	-[0x80000f30]:csrrs a2, fcsr, zero
	-[0x80000f34]:sd t6, 1056(fp)
	-[0x80000f38]:sd a2, 1064(fp)
Current Store : [0x80000f38] : sd a2, 1064(fp) -- Store: [0x800040d0]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 1 and fe2 == 0xfc and fm2 == 0x28bbb2 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fadd.s t6, t5, t4, dyn
	-[0x80000f50]:csrrs a2, fcsr, zero
	-[0x80000f54]:sd t6, 1072(fp)
	-[0x80000f58]:sd a2, 1080(fp)
Current Store : [0x80000f58] : sd a2, 1080(fp) -- Store: [0x800040e0]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 1 and fe2 == 0xfc and fm2 == 0x28bbb2 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fadd.s t6, t5, t4, dyn
	-[0x80000f70]:csrrs a2, fcsr, zero
	-[0x80000f74]:sd t6, 1088(fp)
	-[0x80000f78]:sd a2, 1096(fp)
Current Store : [0x80000f78] : sd a2, 1096(fp) -- Store: [0x800040f0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x0a2eec and fs2 == 1 and fe2 == 0xfc and fm2 == 0x28bbb2 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:fadd.s t6, t5, t4, dyn
	-[0x80000f90]:csrrs a2, fcsr, zero
	-[0x80000f94]:sd t6, 1104(fp)
	-[0x80000f98]:sd a2, 1112(fp)
Current Store : [0x80000f98] : sd a2, 1112(fp) -- Store: [0x80004100]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7e5ec6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fac]:fadd.s t6, t5, t4, dyn
	-[0x80000fb0]:csrrs a2, fcsr, zero
	-[0x80000fb4]:sd t6, 1120(fp)
	-[0x80000fb8]:sd a2, 1128(fp)
Current Store : [0x80000fb8] : sd a2, 1128(fp) -- Store: [0x80004110]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7e5ec6 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fcc]:fadd.s t6, t5, t4, dyn
	-[0x80000fd0]:csrrs a2, fcsr, zero
	-[0x80000fd4]:sd t6, 1136(fp)
	-[0x80000fd8]:sd a2, 1144(fp)
Current Store : [0x80000fd8] : sd a2, 1144(fp) -- Store: [0x80004120]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7e5ec6 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fadd.s t6, t5, t4, dyn
	-[0x80000ff0]:csrrs a2, fcsr, zero
	-[0x80000ff4]:sd t6, 1152(fp)
	-[0x80000ff8]:sd a2, 1160(fp)
Current Store : [0x80000ff8] : sd a2, 1160(fp) -- Store: [0x80004130]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7e5ec6 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000100c]:fadd.s t6, t5, t4, dyn
	-[0x80001010]:csrrs a2, fcsr, zero
	-[0x80001014]:sd t6, 1168(fp)
	-[0x80001018]:sd a2, 1176(fp)
Current Store : [0x80001018] : sd a2, 1176(fp) -- Store: [0x80004140]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1e5ec7 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7e5ec6 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000102c]:fadd.s t6, t5, t4, dyn
	-[0x80001030]:csrrs a2, fcsr, zero
	-[0x80001034]:sd t6, 1184(fp)
	-[0x80001038]:sd a2, 1192(fp)
Current Store : [0x80001038] : sd a2, 1192(fp) -- Store: [0x80004150]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3fc7d3 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000104c]:fadd.s t6, t5, t4, dyn
	-[0x80001050]:csrrs a2, fcsr, zero
	-[0x80001054]:sd t6, 1200(fp)
	-[0x80001058]:sd a2, 1208(fp)
Current Store : [0x80001058] : sd a2, 1208(fp) -- Store: [0x80004160]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3fc7d3 and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000106c]:fadd.s t6, t5, t4, dyn
	-[0x80001070]:csrrs a2, fcsr, zero
	-[0x80001074]:sd t6, 1216(fp)
	-[0x80001078]:sd a2, 1224(fp)
Current Store : [0x80001078] : sd a2, 1224(fp) -- Store: [0x80004170]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3fc7d3 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000108c]:fadd.s t6, t5, t4, dyn
	-[0x80001090]:csrrs a2, fcsr, zero
	-[0x80001094]:sd t6, 1232(fp)
	-[0x80001098]:sd a2, 1240(fp)
Current Store : [0x80001098] : sd a2, 1240(fp) -- Store: [0x80004180]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3fc7d3 and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010ac]:fadd.s t6, t5, t4, dyn
	-[0x800010b0]:csrrs a2, fcsr, zero
	-[0x800010b4]:sd t6, 1248(fp)
	-[0x800010b8]:sd a2, 1256(fp)
Current Store : [0x800010b8] : sd a2, 1256(fp) -- Store: [0x80004190]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x60affa and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3fc7d3 and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010cc]:fadd.s t6, t5, t4, dyn
	-[0x800010d0]:csrrs a2, fcsr, zero
	-[0x800010d4]:sd t6, 1264(fp)
	-[0x800010d8]:sd a2, 1272(fp)
Current Store : [0x800010d8] : sd a2, 1272(fp) -- Store: [0x800041a0]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800010ec]:fadd.s t6, t5, t4, dyn
	-[0x800010f0]:csrrs a2, fcsr, zero
	-[0x800010f4]:sd t6, 1280(fp)
	-[0x800010f8]:sd a2, 1288(fp)
Current Store : [0x800010f8] : sd a2, 1288(fp) -- Store: [0x800041b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fadd.s t6, t5, t4, dyn
	-[0x80001110]:csrrs a2, fcsr, zero
	-[0x80001114]:sd t6, 1296(fp)
	-[0x80001118]:sd a2, 1304(fp)
Current Store : [0x80001118] : sd a2, 1304(fp) -- Store: [0x800041c0]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000112c]:fadd.s t6, t5, t4, dyn
	-[0x80001130]:csrrs a2, fcsr, zero
	-[0x80001134]:sd t6, 1312(fp)
	-[0x80001138]:sd a2, 1320(fp)
Current Store : [0x80001138] : sd a2, 1320(fp) -- Store: [0x800041d0]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000114c]:fadd.s t6, t5, t4, dyn
	-[0x80001150]:csrrs a2, fcsr, zero
	-[0x80001154]:sd t6, 1328(fp)
	-[0x80001158]:sd a2, 1336(fp)
Current Store : [0x80001158] : sd a2, 1336(fp) -- Store: [0x800041e0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x269468 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000116c]:fadd.s t6, t5, t4, dyn
	-[0x80001170]:csrrs a2, fcsr, zero
	-[0x80001174]:sd t6, 1344(fp)
	-[0x80001178]:sd a2, 1352(fp)
Current Store : [0x80001178] : sd a2, 1352(fp) -- Store: [0x800041f0]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000118c]:fadd.s t6, t5, t4, dyn
	-[0x80001190]:csrrs a2, fcsr, zero
	-[0x80001194]:sd t6, 1360(fp)
	-[0x80001198]:sd a2, 1368(fp)
Current Store : [0x80001198] : sd a2, 1368(fp) -- Store: [0x80004200]:0x0000000000000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011ac]:fadd.s t6, t5, t4, dyn
	-[0x800011b0]:csrrs a2, fcsr, zero
	-[0x800011b4]:sd t6, 1376(fp)
	-[0x800011b8]:sd a2, 1384(fp)
Current Store : [0x800011b8] : sd a2, 1384(fp) -- Store: [0x80004210]:0x0000000000000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011cc]:fadd.s t6, t5, t4, dyn
	-[0x800011d0]:csrrs a2, fcsr, zero
	-[0x800011d4]:sd t6, 1392(fp)
	-[0x800011d8]:sd a2, 1400(fp)
Current Store : [0x800011d8] : sd a2, 1400(fp) -- Store: [0x80004220]:0x0000000000000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011ec]:fadd.s t6, t5, t4, dyn
	-[0x800011f0]:csrrs a2, fcsr, zero
	-[0x800011f4]:sd t6, 1408(fp)
	-[0x800011f8]:sd a2, 1416(fp)
Current Store : [0x800011f8] : sd a2, 1416(fp) -- Store: [0x80004230]:0x0000000000000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x79c1c6 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000120c]:fadd.s t6, t5, t4, dyn
	-[0x80001210]:csrrs a2, fcsr, zero
	-[0x80001214]:sd t6, 1424(fp)
	-[0x80001218]:sd a2, 1432(fp)
Current Store : [0x80001218] : sd a2, 1432(fp) -- Store: [0x80004240]:0x0000000000000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fadd.s t6, t5, t4, dyn
	-[0x80001230]:csrrs a2, fcsr, zero
	-[0x80001234]:sd t6, 1440(fp)
	-[0x80001238]:sd a2, 1448(fp)
Current Store : [0x80001238] : sd a2, 1448(fp) -- Store: [0x80004250]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000124c]:fadd.s t6, t5, t4, dyn
	-[0x80001250]:csrrs a2, fcsr, zero
	-[0x80001254]:sd t6, 1456(fp)
	-[0x80001258]:sd a2, 1464(fp)
Current Store : [0x80001258] : sd a2, 1464(fp) -- Store: [0x80004260]:0x0000000000000021




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000126c]:fadd.s t6, t5, t4, dyn
	-[0x80001270]:csrrs a2, fcsr, zero
	-[0x80001274]:sd t6, 1472(fp)
	-[0x80001278]:sd a2, 1480(fp)
Current Store : [0x80001278] : sd a2, 1480(fp) -- Store: [0x80004270]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000128c]:fadd.s t6, t5, t4, dyn
	-[0x80001290]:csrrs a2, fcsr, zero
	-[0x80001294]:sd t6, 1488(fp)
	-[0x80001298]:sd a2, 1496(fp)
Current Store : [0x80001298] : sd a2, 1496(fp) -- Store: [0x80004280]:0x0000000000000061




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x12bd51 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ac]:fadd.s t6, t5, t4, dyn
	-[0x800012b0]:csrrs a2, fcsr, zero
	-[0x800012b4]:sd t6, 1504(fp)
	-[0x800012b8]:sd a2, 1512(fp)
Current Store : [0x800012b8] : sd a2, 1512(fp) -- Store: [0x80004290]:0x0000000000000081




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800012cc]:fadd.s t6, t5, t4, dyn
	-[0x800012d0]:csrrs a2, fcsr, zero
	-[0x800012d4]:sd t6, 1520(fp)
	-[0x800012d8]:sd a2, 1528(fp)
Current Store : [0x800012d8] : sd a2, 1528(fp) -- Store: [0x800042a0]:0x0000000000000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800012ec]:fadd.s t6, t5, t4, dyn
	-[0x800012f0]:csrrs a2, fcsr, zero
	-[0x800012f4]:sd t6, 1536(fp)
	-[0x800012f8]:sd a2, 1544(fp)
Current Store : [0x800012f8] : sd a2, 1544(fp) -- Store: [0x800042b0]:0x0000000000000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000130c]:fadd.s t6, t5, t4, dyn
	-[0x80001310]:csrrs a2, fcsr, zero
	-[0x80001314]:sd t6, 1552(fp)
	-[0x80001318]:sd a2, 1560(fp)
Current Store : [0x80001318] : sd a2, 1560(fp) -- Store: [0x800042c0]:0x0000000000000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000132c]:fadd.s t6, t5, t4, dyn
	-[0x80001330]:csrrs a2, fcsr, zero
	-[0x80001334]:sd t6, 1568(fp)
	-[0x80001338]:sd a2, 1576(fp)
Current Store : [0x80001338] : sd a2, 1576(fp) -- Store: [0x800042d0]:0x0000000000000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x3741cc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000134c]:fadd.s t6, t5, t4, dyn
	-[0x80001350]:csrrs a2, fcsr, zero
	-[0x80001354]:sd t6, 1584(fp)
	-[0x80001358]:sd a2, 1592(fp)
Current Store : [0x80001358] : sd a2, 1592(fp) -- Store: [0x800042e0]:0x0000000000000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000136c]:fadd.s t6, t5, t4, dyn
	-[0x80001370]:csrrs a2, fcsr, zero
	-[0x80001374]:sd t6, 1600(fp)
	-[0x80001378]:sd a2, 1608(fp)
Current Store : [0x80001378] : sd a2, 1608(fp) -- Store: [0x800042f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000138c]:fadd.s t6, t5, t4, dyn
	-[0x80001390]:csrrs a2, fcsr, zero
	-[0x80001394]:sd t6, 1616(fp)
	-[0x80001398]:sd a2, 1624(fp)
Current Store : [0x80001398] : sd a2, 1624(fp) -- Store: [0x80004300]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ac]:fadd.s t6, t5, t4, dyn
	-[0x800013b0]:csrrs a2, fcsr, zero
	-[0x800013b4]:sd t6, 1632(fp)
	-[0x800013b8]:sd a2, 1640(fp)
Current Store : [0x800013b8] : sd a2, 1640(fp) -- Store: [0x80004310]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013cc]:fadd.s t6, t5, t4, dyn
	-[0x800013d0]:csrrs a2, fcsr, zero
	-[0x800013d4]:sd t6, 1648(fp)
	-[0x800013d8]:sd a2, 1656(fp)
Current Store : [0x800013d8] : sd a2, 1656(fp) -- Store: [0x80004320]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x1a35e0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ec]:fadd.s t6, t5, t4, dyn
	-[0x800013f0]:csrrs a2, fcsr, zero
	-[0x800013f4]:sd t6, 1664(fp)
	-[0x800013f8]:sd a2, 1672(fp)
Current Store : [0x800013f8] : sd a2, 1672(fp) -- Store: [0x80004330]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000140c]:fadd.s t6, t5, t4, dyn
	-[0x80001410]:csrrs a2, fcsr, zero
	-[0x80001414]:sd t6, 1680(fp)
	-[0x80001418]:sd a2, 1688(fp)
Current Store : [0x80001418] : sd a2, 1688(fp) -- Store: [0x80004340]:0x0000000000000005




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fadd.s t6, t5, t4, dyn
	-[0x80001430]:csrrs a2, fcsr, zero
	-[0x80001434]:sd t6, 1696(fp)
	-[0x80001438]:sd a2, 1704(fp)
Current Store : [0x80001438] : sd a2, 1704(fp) -- Store: [0x80004350]:0x0000000000000025




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000144c]:fadd.s t6, t5, t4, dyn
	-[0x80001450]:csrrs a2, fcsr, zero
	-[0x80001454]:sd t6, 1712(fp)
	-[0x80001458]:sd a2, 1720(fp)
Current Store : [0x80001458] : sd a2, 1720(fp) -- Store: [0x80004360]:0x0000000000000045




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000146c]:fadd.s t6, t5, t4, dyn
	-[0x80001470]:csrrs a2, fcsr, zero
	-[0x80001474]:sd t6, 1728(fp)
	-[0x80001478]:sd a2, 1736(fp)
Current Store : [0x80001478] : sd a2, 1736(fp) -- Store: [0x80004370]:0x0000000000000065




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x772129 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000148c]:fadd.s t6, t5, t4, dyn
	-[0x80001490]:csrrs a2, fcsr, zero
	-[0x80001494]:sd t6, 1744(fp)
	-[0x80001498]:sd a2, 1752(fp)
Current Store : [0x80001498] : sd a2, 1752(fp) -- Store: [0x80004380]:0x0000000000000085




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ac]:fadd.s t6, t5, t4, dyn
	-[0x800014b0]:csrrs a2, fcsr, zero
	-[0x800014b4]:sd t6, 1760(fp)
	-[0x800014b8]:sd a2, 1768(fp)
Current Store : [0x800014b8] : sd a2, 1768(fp) -- Store: [0x80004390]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x20 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014cc]:fadd.s t6, t5, t4, dyn
	-[0x800014d0]:csrrs a2, fcsr, zero
	-[0x800014d4]:sd t6, 1776(fp)
	-[0x800014d8]:sd a2, 1784(fp)
Current Store : [0x800014d8] : sd a2, 1784(fp) -- Store: [0x800043a0]:0x0000000000000020




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800014ec]:fadd.s t6, t5, t4, dyn
	-[0x800014f0]:csrrs a2, fcsr, zero
	-[0x800014f4]:sd t6, 1792(fp)
	-[0x800014f8]:sd a2, 1800(fp)
Current Store : [0x800014f8] : sd a2, 1800(fp) -- Store: [0x800043b0]:0x0000000000000040




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fadd.s t6, t5, t4, dyn
	-[0x80001510]:csrrs a2, fcsr, zero
	-[0x80001514]:sd t6, 1808(fp)
	-[0x80001518]:sd a2, 1816(fp)
Current Store : [0x80001518] : sd a2, 1816(fp) -- Store: [0x800043c0]:0x0000000000000060




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x430c98 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x80 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000152c]:fadd.s t6, t5, t4, dyn
	-[0x80001530]:csrrs a2, fcsr, zero
	-[0x80001534]:sd t6, 1824(fp)
	-[0x80001538]:sd a2, 1832(fp)
Current Store : [0x80001538] : sd a2, 1832(fp) -- Store: [0x800043d0]:0x0000000000000080




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x6ef7d1 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fadd.s t6, t5, t4, dyn
	-[0x80001550]:csrrs a2, fcsr, zero
	-[0x80001554]:sd t6, 1840(fp)
	-[0x80001558]:sd a2, 1848(fp)
Current Store : [0x80001558] : sd a2, 1848(fp) -- Store: [0x800043e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x222105 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x6ef7d1 and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000156c]:fadd.s t6, t5, t4, dyn
	-[0x80001570]:csrrs a2, fcsr, zero
	-[0x80001574]:sd t6, 1856(fp)
	-[0x80001578]:sd a2, 1864(fp)
Current Store : [0x80001578] : sd a2, 1864(fp) -- Store: [0x800043f0]:0x0000000000000041




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000158c]:fadd.s t6, t5, t4, dyn
	-[0x80001590]:csrrs a2, fcsr, zero
	-[0x80001594]:sd t6, 1872(fp)
	-[0x80001598]:sd a2, 1880(fp)
Current Store : [0x80001598] : sd a2, 1880(fp) -- Store: [0x80004400]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x40 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015ac]:fadd.s t6, t5, t4, dyn
	-[0x800015b0]:csrrs a2, fcsr, zero
	-[0x800015b4]:sd t6, 1888(fp)
	-[0x800015b8]:sd a2, 1896(fp)
Current Store : [0x800015b8] : sd a2, 1896(fp) -- Store: [0x80004410]:0x0000000000000041




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x587392 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x09e31b and  fcsr == 0x60 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015cc]:fadd.s t6, t5, t4, dyn
	-[0x800015d0]:csrrs a2, fcsr, zero
	-[0x800015d4]:sd t6, 1904(fp)
	-[0x800015d8]:sd a2, 1912(fp)
Current Store : [0x800015d8] : sd a2, 1912(fp) -- Store: [0x80004420]:0x0000000000000061





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
