
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000da0')]      |
| SIG_REGION                | [('0x80002610', '0x80002b10', '160 dwords')]      |
| COV_LABELS                | fadd_b10      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fadd/work-fadd/fadd_b10-01.S/ref.S    |
| Total Number of coverpoints| 176     |
| Total Coverpoints Hit     | 176      |
| Total Signature Updates   | 158      |
| STAT1                     | 0      |
| STAT2                     | 2      |
| STAT3                     | 77     |
| STAT4                     | 79     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000d0c]:fadd.s t6, t5, t4, dyn
      [0x80000d10]:csrrs a2, fcsr, zero
      [0x80000d14]:sd t6, 784(fp)
      [0x80000d18]:sd a2, 792(fp)
      [0x80000d1c]:ld t5, 848(a1)
      [0x80000d20]:ld t4, 856(a1)
      [0x80000d24]:addi s1, zero, 0
      [0x80000d28]:csrrw zero, fcsr, s1
      [0x80000d2c]:fadd.s t6, t5, t4, dyn
 -- Signature Addresses:
      Address: 0x80002ab8 Data: 0x000000007A260524
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat




Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x80000d8c]:fadd.s t6, t5, t4, dyn
      [0x80000d90]:csrrs a2, fcsr, zero
      [0x80000d94]:sd t6, 848(fp)
      [0x80000d98]:sd a2, 856(fp)
      [0x80000d9c]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80002af8 Data: 0x000000007A260524
 -- Redundant Coverpoints hit by the op
      - mnemonic : fadd.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x4ad5da and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat






```

## Details of STAT3

```
[0x800003bc]:fadd.s t6, t6, t6, dyn
[0x800003c0]:csrrs tp, fcsr, zero
[0x800003c4]:sd t6, 0(ra)
[0x800003c8]:sd tp, 8(ra)
[0x800003cc]:ld t4, 16(gp)
[0x800003d0]:ld t3, 24(gp)
[0x800003d4]:addi sp, zero, 0
[0x800003d8]:csrrw zero, fcsr, sp
[0x800003dc]:fadd.s t5, t4, t3, dyn

[0x800003dc]:fadd.s t5, t4, t3, dyn
[0x800003e0]:csrrs tp, fcsr, zero
[0x800003e4]:sd t5, 16(ra)
[0x800003e8]:sd tp, 24(ra)
[0x800003ec]:ld t5, 32(gp)
[0x800003f0]:ld t5, 40(gp)
[0x800003f4]:addi sp, zero, 0
[0x800003f8]:csrrw zero, fcsr, sp
[0x800003fc]:fadd.s t4, t5, t5, dyn

[0x800003fc]:fadd.s t4, t5, t5, dyn
[0x80000400]:csrrs tp, fcsr, zero
[0x80000404]:sd t4, 32(ra)
[0x80000408]:sd tp, 40(ra)
[0x8000040c]:ld t3, 48(gp)
[0x80000410]:ld t4, 56(gp)
[0x80000414]:addi sp, zero, 0
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fadd.s t3, t3, t4, dyn

[0x8000041c]:fadd.s t3, t3, t4, dyn
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:sd t3, 48(ra)
[0x80000428]:sd tp, 56(ra)
[0x8000042c]:ld s10, 64(gp)
[0x80000430]:ld s11, 72(gp)
[0x80000434]:addi sp, zero, 0
[0x80000438]:csrrw zero, fcsr, sp
[0x8000043c]:fadd.s s11, s10, s11, dyn

[0x8000043c]:fadd.s s11, s10, s11, dyn
[0x80000440]:csrrs tp, fcsr, zero
[0x80000444]:sd s11, 64(ra)
[0x80000448]:sd tp, 72(ra)
[0x8000044c]:ld s11, 80(gp)
[0x80000450]:ld s9, 88(gp)
[0x80000454]:addi sp, zero, 0
[0x80000458]:csrrw zero, fcsr, sp
[0x8000045c]:fadd.s s10, s11, s9, dyn

[0x8000045c]:fadd.s s10, s11, s9, dyn
[0x80000460]:csrrs tp, fcsr, zero
[0x80000464]:sd s10, 80(ra)
[0x80000468]:sd tp, 88(ra)
[0x8000046c]:ld s8, 96(gp)
[0x80000470]:ld s10, 104(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fadd.s s9, s8, s10, dyn

[0x8000047c]:fadd.s s9, s8, s10, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s9, 96(ra)
[0x80000488]:sd tp, 104(ra)
[0x8000048c]:ld s9, 112(gp)
[0x80000490]:ld s7, 120(gp)
[0x80000494]:addi sp, zero, 0
[0x80000498]:csrrw zero, fcsr, sp
[0x8000049c]:fadd.s s8, s9, s7, dyn

[0x8000049c]:fadd.s s8, s9, s7, dyn
[0x800004a0]:csrrs tp, fcsr, zero
[0x800004a4]:sd s8, 112(ra)
[0x800004a8]:sd tp, 120(ra)
[0x800004ac]:ld s6, 128(gp)
[0x800004b0]:ld s8, 136(gp)
[0x800004b4]:addi sp, zero, 0
[0x800004b8]:csrrw zero, fcsr, sp
[0x800004bc]:fadd.s s7, s6, s8, dyn

[0x800004bc]:fadd.s s7, s6, s8, dyn
[0x800004c0]:csrrs tp, fcsr, zero
[0x800004c4]:sd s7, 128(ra)
[0x800004c8]:sd tp, 136(ra)
[0x800004cc]:ld s7, 144(gp)
[0x800004d0]:ld s5, 152(gp)
[0x800004d4]:addi sp, zero, 0
[0x800004d8]:csrrw zero, fcsr, sp
[0x800004dc]:fadd.s s6, s7, s5, dyn

[0x800004dc]:fadd.s s6, s7, s5, dyn
[0x800004e0]:csrrs tp, fcsr, zero
[0x800004e4]:sd s6, 144(ra)
[0x800004e8]:sd tp, 152(ra)
[0x800004ec]:ld s4, 160(gp)
[0x800004f0]:ld s6, 168(gp)
[0x800004f4]:addi sp, zero, 0
[0x800004f8]:csrrw zero, fcsr, sp
[0x800004fc]:fadd.s s5, s4, s6, dyn

[0x800004fc]:fadd.s s5, s4, s6, dyn
[0x80000500]:csrrs tp, fcsr, zero
[0x80000504]:sd s5, 160(ra)
[0x80000508]:sd tp, 168(ra)
[0x8000050c]:ld s5, 176(gp)
[0x80000510]:ld s3, 184(gp)
[0x80000514]:addi sp, zero, 0
[0x80000518]:csrrw zero, fcsr, sp
[0x8000051c]:fadd.s s4, s5, s3, dyn

[0x8000051c]:fadd.s s4, s5, s3, dyn
[0x80000520]:csrrs tp, fcsr, zero
[0x80000524]:sd s4, 176(ra)
[0x80000528]:sd tp, 184(ra)
[0x8000052c]:ld s2, 192(gp)
[0x80000530]:ld s4, 200(gp)
[0x80000534]:addi sp, zero, 0
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fadd.s s3, s2, s4, dyn

[0x8000053c]:fadd.s s3, s2, s4, dyn
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:sd s3, 192(ra)
[0x80000548]:sd tp, 200(ra)
[0x8000054c]:ld s3, 208(gp)
[0x80000550]:ld a7, 216(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fadd.s s2, s3, a7, dyn

[0x8000055c]:fadd.s s2, s3, a7, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd s2, 208(ra)
[0x80000568]:sd tp, 216(ra)
[0x8000056c]:ld a6, 224(gp)
[0x80000570]:ld s2, 232(gp)
[0x80000574]:addi sp, zero, 0
[0x80000578]:csrrw zero, fcsr, sp
[0x8000057c]:fadd.s a7, a6, s2, dyn

[0x8000057c]:fadd.s a7, a6, s2, dyn
[0x80000580]:csrrs tp, fcsr, zero
[0x80000584]:sd a7, 224(ra)
[0x80000588]:sd tp, 232(ra)
[0x8000058c]:ld a7, 240(gp)
[0x80000590]:ld a5, 248(gp)
[0x80000594]:addi sp, zero, 0
[0x80000598]:csrrw zero, fcsr, sp
[0x8000059c]:fadd.s a6, a7, a5, dyn

[0x8000059c]:fadd.s a6, a7, a5, dyn
[0x800005a0]:csrrs tp, fcsr, zero
[0x800005a4]:sd a6, 240(ra)
[0x800005a8]:sd tp, 248(ra)
[0x800005ac]:ld a4, 256(gp)
[0x800005b0]:ld a6, 264(gp)
[0x800005b4]:addi sp, zero, 0
[0x800005b8]:csrrw zero, fcsr, sp
[0x800005bc]:fadd.s a5, a4, a6, dyn

[0x800005bc]:fadd.s a5, a4, a6, dyn
[0x800005c0]:csrrs tp, fcsr, zero
[0x800005c4]:sd a5, 256(ra)
[0x800005c8]:sd tp, 264(ra)
[0x800005cc]:ld a5, 272(gp)
[0x800005d0]:ld a3, 280(gp)
[0x800005d4]:addi sp, zero, 0
[0x800005d8]:csrrw zero, fcsr, sp
[0x800005dc]:fadd.s a4, a5, a3, dyn

[0x800005dc]:fadd.s a4, a5, a3, dyn
[0x800005e0]:csrrs tp, fcsr, zero
[0x800005e4]:sd a4, 272(ra)
[0x800005e8]:sd tp, 280(ra)
[0x800005ec]:ld a2, 288(gp)
[0x800005f0]:ld a4, 296(gp)
[0x800005f4]:addi sp, zero, 0
[0x800005f8]:csrrw zero, fcsr, sp
[0x800005fc]:fadd.s a3, a2, a4, dyn

[0x800005fc]:fadd.s a3, a2, a4, dyn
[0x80000600]:csrrs tp, fcsr, zero
[0x80000604]:sd a3, 288(ra)
[0x80000608]:sd tp, 296(ra)
[0x8000060c]:ld a3, 304(gp)
[0x80000610]:ld a1, 312(gp)
[0x80000614]:addi sp, zero, 0
[0x80000618]:csrrw zero, fcsr, sp
[0x8000061c]:fadd.s a2, a3, a1, dyn

[0x8000061c]:fadd.s a2, a3, a1, dyn
[0x80000620]:csrrs tp, fcsr, zero
[0x80000624]:sd a2, 304(ra)
[0x80000628]:sd tp, 312(ra)
[0x8000062c]:ld a0, 320(gp)
[0x80000630]:ld a2, 328(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fadd.s a1, a0, a2, dyn

[0x8000063c]:fadd.s a1, a0, a2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd a1, 320(ra)
[0x80000648]:sd tp, 328(ra)
[0x8000064c]:ld a1, 336(gp)
[0x80000650]:ld s1, 344(gp)
[0x80000654]:addi sp, zero, 0
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fadd.s a0, a1, s1, dyn

[0x8000065c]:fadd.s a0, a1, s1, dyn
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:sd a0, 336(ra)
[0x80000668]:sd tp, 344(ra)
[0x8000066c]:auipc a1, 2
[0x80000670]:addi a1, a1, 2820
[0x80000674]:ld fp, 0(a1)
[0x80000678]:ld a0, 8(a1)
[0x8000067c]:addi sp, zero, 0
[0x80000680]:csrrw zero, fcsr, sp
[0x80000684]:fadd.s s1, fp, a0, dyn

[0x80000684]:fadd.s s1, fp, a0, dyn
[0x80000688]:csrrs a2, fcsr, zero
[0x8000068c]:sd s1, 352(ra)
[0x80000690]:sd a2, 360(ra)
[0x80000694]:ld s1, 16(a1)
[0x80000698]:ld t2, 24(a1)
[0x8000069c]:addi sp, zero, 0
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fadd.s fp, s1, t2, dyn

[0x800006a4]:fadd.s fp, s1, t2, dyn
[0x800006a8]:csrrs a2, fcsr, zero
[0x800006ac]:sd fp, 368(ra)
[0x800006b0]:sd a2, 376(ra)
[0x800006b4]:ld t1, 32(a1)
[0x800006b8]:ld fp, 40(a1)
[0x800006bc]:addi s1, zero, 0
[0x800006c0]:csrrw zero, fcsr, s1
[0x800006c4]:fadd.s t2, t1, fp, dyn

[0x800006c4]:fadd.s t2, t1, fp, dyn
[0x800006c8]:csrrs a2, fcsr, zero
[0x800006cc]:sd t2, 384(ra)
[0x800006d0]:sd a2, 392(ra)
[0x800006d4]:auipc fp, 2
[0x800006d8]:addi fp, fp, 212
[0x800006dc]:ld t2, 48(a1)
[0x800006e0]:ld t0, 56(a1)
[0x800006e4]:addi s1, zero, 0
[0x800006e8]:csrrw zero, fcsr, s1
[0x800006ec]:fadd.s t1, t2, t0, dyn

[0x800006ec]:fadd.s t1, t2, t0, dyn
[0x800006f0]:csrrs a2, fcsr, zero
[0x800006f4]:sd t1, 0(fp)
[0x800006f8]:sd a2, 8(fp)
[0x800006fc]:ld tp, 64(a1)
[0x80000700]:ld t1, 72(a1)
[0x80000704]:addi s1, zero, 0
[0x80000708]:csrrw zero, fcsr, s1
[0x8000070c]:fadd.s t0, tp, t1, dyn

[0x8000070c]:fadd.s t0, tp, t1, dyn
[0x80000710]:csrrs a2, fcsr, zero
[0x80000714]:sd t0, 16(fp)
[0x80000718]:sd a2, 24(fp)
[0x8000071c]:ld t0, 80(a1)
[0x80000720]:ld gp, 88(a1)
[0x80000724]:addi s1, zero, 0
[0x80000728]:csrrw zero, fcsr, s1
[0x8000072c]:fadd.s tp, t0, gp, dyn

[0x8000072c]:fadd.s tp, t0, gp, dyn
[0x80000730]:csrrs a2, fcsr, zero
[0x80000734]:sd tp, 32(fp)
[0x80000738]:sd a2, 40(fp)
[0x8000073c]:ld sp, 96(a1)
[0x80000740]:ld tp, 104(a1)
[0x80000744]:addi s1, zero, 0
[0x80000748]:csrrw zero, fcsr, s1
[0x8000074c]:fadd.s gp, sp, tp, dyn

[0x8000074c]:fadd.s gp, sp, tp, dyn
[0x80000750]:csrrs a2, fcsr, zero
[0x80000754]:sd gp, 48(fp)
[0x80000758]:sd a2, 56(fp)
[0x8000075c]:ld gp, 112(a1)
[0x80000760]:ld ra, 120(a1)
[0x80000764]:addi s1, zero, 0
[0x80000768]:csrrw zero, fcsr, s1
[0x8000076c]:fadd.s sp, gp, ra, dyn

[0x8000076c]:fadd.s sp, gp, ra, dyn
[0x80000770]:csrrs a2, fcsr, zero
[0x80000774]:sd sp, 64(fp)
[0x80000778]:sd a2, 72(fp)
[0x8000077c]:ld zero, 128(a1)
[0x80000780]:ld sp, 136(a1)
[0x80000784]:addi s1, zero, 0
[0x80000788]:csrrw zero, fcsr, s1
[0x8000078c]:fadd.s ra, zero, sp, dyn

[0x8000078c]:fadd.s ra, zero, sp, dyn
[0x80000790]:csrrs a2, fcsr, zero
[0x80000794]:sd ra, 80(fp)
[0x80000798]:sd a2, 88(fp)
[0x8000079c]:ld ra, 144(a1)
[0x800007a0]:ld t5, 152(a1)
[0x800007a4]:addi s1, zero, 0
[0x800007a8]:csrrw zero, fcsr, s1
[0x800007ac]:fadd.s t6, ra, t5, dyn

[0x800007ac]:fadd.s t6, ra, t5, dyn
[0x800007b0]:csrrs a2, fcsr, zero
[0x800007b4]:sd t6, 96(fp)
[0x800007b8]:sd a2, 104(fp)
[0x800007bc]:ld t5, 160(a1)
[0x800007c0]:ld zero, 168(a1)
[0x800007c4]:addi s1, zero, 0
[0x800007c8]:csrrw zero, fcsr, s1
[0x800007cc]:fadd.s t6, t5, zero, dyn

[0x800007cc]:fadd.s t6, t5, zero, dyn
[0x800007d0]:csrrs a2, fcsr, zero
[0x800007d4]:sd t6, 112(fp)
[0x800007d8]:sd a2, 120(fp)
[0x800007dc]:ld t6, 176(a1)
[0x800007e0]:ld t5, 184(a1)
[0x800007e4]:addi s1, zero, 0
[0x800007e8]:csrrw zero, fcsr, s1
[0x800007ec]:fadd.s zero, t6, t5, dyn

[0x800007ec]:fadd.s zero, t6, t5, dyn
[0x800007f0]:csrrs a2, fcsr, zero
[0x800007f4]:sd zero, 128(fp)
[0x800007f8]:sd a2, 136(fp)
[0x800007fc]:ld t5, 192(a1)
[0x80000800]:ld t4, 200(a1)
[0x80000804]:addi s1, zero, 0
[0x80000808]:csrrw zero, fcsr, s1
[0x8000080c]:fadd.s t6, t5, t4, dyn

[0x8000080c]:fadd.s t6, t5, t4, dyn
[0x80000810]:csrrs a2, fcsr, zero
[0x80000814]:sd t6, 144(fp)
[0x80000818]:sd a2, 152(fp)
[0x8000081c]:ld t5, 208(a1)
[0x80000820]:ld t4, 216(a1)
[0x80000824]:addi s1, zero, 0
[0x80000828]:csrrw zero, fcsr, s1
[0x8000082c]:fadd.s t6, t5, t4, dyn

[0x8000082c]:fadd.s t6, t5, t4, dyn
[0x80000830]:csrrs a2, fcsr, zero
[0x80000834]:sd t6, 160(fp)
[0x80000838]:sd a2, 168(fp)
[0x8000083c]:ld t5, 224(a1)
[0x80000840]:ld t4, 232(a1)
[0x80000844]:addi s1, zero, 0
[0x80000848]:csrrw zero, fcsr, s1
[0x8000084c]:fadd.s t6, t5, t4, dyn

[0x8000084c]:fadd.s t6, t5, t4, dyn
[0x80000850]:csrrs a2, fcsr, zero
[0x80000854]:sd t6, 176(fp)
[0x80000858]:sd a2, 184(fp)
[0x8000085c]:ld t5, 240(a1)
[0x80000860]:ld t4, 248(a1)
[0x80000864]:addi s1, zero, 0
[0x80000868]:csrrw zero, fcsr, s1
[0x8000086c]:fadd.s t6, t5, t4, dyn

[0x8000086c]:fadd.s t6, t5, t4, dyn
[0x80000870]:csrrs a2, fcsr, zero
[0x80000874]:sd t6, 192(fp)
[0x80000878]:sd a2, 200(fp)
[0x8000087c]:ld t5, 256(a1)
[0x80000880]:ld t4, 264(a1)
[0x80000884]:addi s1, zero, 0
[0x80000888]:csrrw zero, fcsr, s1
[0x8000088c]:fadd.s t6, t5, t4, dyn

[0x8000088c]:fadd.s t6, t5, t4, dyn
[0x80000890]:csrrs a2, fcsr, zero
[0x80000894]:sd t6, 208(fp)
[0x80000898]:sd a2, 216(fp)
[0x8000089c]:ld t5, 272(a1)
[0x800008a0]:ld t4, 280(a1)
[0x800008a4]:addi s1, zero, 0
[0x800008a8]:csrrw zero, fcsr, s1
[0x800008ac]:fadd.s t6, t5, t4, dyn

[0x800008ac]:fadd.s t6, t5, t4, dyn
[0x800008b0]:csrrs a2, fcsr, zero
[0x800008b4]:sd t6, 224(fp)
[0x800008b8]:sd a2, 232(fp)
[0x800008bc]:ld t5, 288(a1)
[0x800008c0]:ld t4, 296(a1)
[0x800008c4]:addi s1, zero, 0
[0x800008c8]:csrrw zero, fcsr, s1
[0x800008cc]:fadd.s t6, t5, t4, dyn

[0x800008cc]:fadd.s t6, t5, t4, dyn
[0x800008d0]:csrrs a2, fcsr, zero
[0x800008d4]:sd t6, 240(fp)
[0x800008d8]:sd a2, 248(fp)
[0x800008dc]:ld t5, 304(a1)
[0x800008e0]:ld t4, 312(a1)
[0x800008e4]:addi s1, zero, 0
[0x800008e8]:csrrw zero, fcsr, s1
[0x800008ec]:fadd.s t6, t5, t4, dyn

[0x800008ec]:fadd.s t6, t5, t4, dyn
[0x800008f0]:csrrs a2, fcsr, zero
[0x800008f4]:sd t6, 256(fp)
[0x800008f8]:sd a2, 264(fp)
[0x800008fc]:ld t5, 320(a1)
[0x80000900]:ld t4, 328(a1)
[0x80000904]:addi s1, zero, 0
[0x80000908]:csrrw zero, fcsr, s1
[0x8000090c]:fadd.s t6, t5, t4, dyn

[0x8000090c]:fadd.s t6, t5, t4, dyn
[0x80000910]:csrrs a2, fcsr, zero
[0x80000914]:sd t6, 272(fp)
[0x80000918]:sd a2, 280(fp)
[0x8000091c]:ld t5, 336(a1)
[0x80000920]:ld t4, 344(a1)
[0x80000924]:addi s1, zero, 0
[0x80000928]:csrrw zero, fcsr, s1
[0x8000092c]:fadd.s t6, t5, t4, dyn

[0x8000092c]:fadd.s t6, t5, t4, dyn
[0x80000930]:csrrs a2, fcsr, zero
[0x80000934]:sd t6, 288(fp)
[0x80000938]:sd a2, 296(fp)
[0x8000093c]:ld t5, 352(a1)
[0x80000940]:ld t4, 360(a1)
[0x80000944]:addi s1, zero, 0
[0x80000948]:csrrw zero, fcsr, s1
[0x8000094c]:fadd.s t6, t5, t4, dyn

[0x8000094c]:fadd.s t6, t5, t4, dyn
[0x80000950]:csrrs a2, fcsr, zero
[0x80000954]:sd t6, 304(fp)
[0x80000958]:sd a2, 312(fp)
[0x8000095c]:ld t5, 368(a1)
[0x80000960]:ld t4, 376(a1)
[0x80000964]:addi s1, zero, 0
[0x80000968]:csrrw zero, fcsr, s1
[0x8000096c]:fadd.s t6, t5, t4, dyn

[0x8000096c]:fadd.s t6, t5, t4, dyn
[0x80000970]:csrrs a2, fcsr, zero
[0x80000974]:sd t6, 320(fp)
[0x80000978]:sd a2, 328(fp)
[0x8000097c]:ld t5, 384(a1)
[0x80000980]:ld t4, 392(a1)
[0x80000984]:addi s1, zero, 0
[0x80000988]:csrrw zero, fcsr, s1
[0x8000098c]:fadd.s t6, t5, t4, dyn

[0x8000098c]:fadd.s t6, t5, t4, dyn
[0x80000990]:csrrs a2, fcsr, zero
[0x80000994]:sd t6, 336(fp)
[0x80000998]:sd a2, 344(fp)
[0x8000099c]:ld t5, 400(a1)
[0x800009a0]:ld t4, 408(a1)
[0x800009a4]:addi s1, zero, 0
[0x800009a8]:csrrw zero, fcsr, s1
[0x800009ac]:fadd.s t6, t5, t4, dyn

[0x800009ac]:fadd.s t6, t5, t4, dyn
[0x800009b0]:csrrs a2, fcsr, zero
[0x800009b4]:sd t6, 352(fp)
[0x800009b8]:sd a2, 360(fp)
[0x800009bc]:ld t5, 416(a1)
[0x800009c0]:ld t4, 424(a1)
[0x800009c4]:addi s1, zero, 0
[0x800009c8]:csrrw zero, fcsr, s1
[0x800009cc]:fadd.s t6, t5, t4, dyn

[0x800009cc]:fadd.s t6, t5, t4, dyn
[0x800009d0]:csrrs a2, fcsr, zero
[0x800009d4]:sd t6, 368(fp)
[0x800009d8]:sd a2, 376(fp)
[0x800009dc]:ld t5, 432(a1)
[0x800009e0]:ld t4, 440(a1)
[0x800009e4]:addi s1, zero, 0
[0x800009e8]:csrrw zero, fcsr, s1
[0x800009ec]:fadd.s t6, t5, t4, dyn

[0x800009ec]:fadd.s t6, t5, t4, dyn
[0x800009f0]:csrrs a2, fcsr, zero
[0x800009f4]:sd t6, 384(fp)
[0x800009f8]:sd a2, 392(fp)
[0x800009fc]:ld t5, 448(a1)
[0x80000a00]:ld t4, 456(a1)
[0x80000a04]:addi s1, zero, 0
[0x80000a08]:csrrw zero, fcsr, s1
[0x80000a0c]:fadd.s t6, t5, t4, dyn

[0x80000a0c]:fadd.s t6, t5, t4, dyn
[0x80000a10]:csrrs a2, fcsr, zero
[0x80000a14]:sd t6, 400(fp)
[0x80000a18]:sd a2, 408(fp)
[0x80000a1c]:ld t5, 464(a1)
[0x80000a20]:ld t4, 472(a1)
[0x80000a24]:addi s1, zero, 0
[0x80000a28]:csrrw zero, fcsr, s1
[0x80000a2c]:fadd.s t6, t5, t4, dyn

[0x80000a2c]:fadd.s t6, t5, t4, dyn
[0x80000a30]:csrrs a2, fcsr, zero
[0x80000a34]:sd t6, 416(fp)
[0x80000a38]:sd a2, 424(fp)
[0x80000a3c]:ld t5, 480(a1)
[0x80000a40]:ld t4, 488(a1)
[0x80000a44]:addi s1, zero, 0
[0x80000a48]:csrrw zero, fcsr, s1
[0x80000a4c]:fadd.s t6, t5, t4, dyn

[0x80000a4c]:fadd.s t6, t5, t4, dyn
[0x80000a50]:csrrs a2, fcsr, zero
[0x80000a54]:sd t6, 432(fp)
[0x80000a58]:sd a2, 440(fp)
[0x80000a5c]:ld t5, 496(a1)
[0x80000a60]:ld t4, 504(a1)
[0x80000a64]:addi s1, zero, 0
[0x80000a68]:csrrw zero, fcsr, s1
[0x80000a6c]:fadd.s t6, t5, t4, dyn

[0x80000a6c]:fadd.s t6, t5, t4, dyn
[0x80000a70]:csrrs a2, fcsr, zero
[0x80000a74]:sd t6, 448(fp)
[0x80000a78]:sd a2, 456(fp)
[0x80000a7c]:ld t5, 512(a1)
[0x80000a80]:ld t4, 520(a1)
[0x80000a84]:addi s1, zero, 0
[0x80000a88]:csrrw zero, fcsr, s1
[0x80000a8c]:fadd.s t6, t5, t4, dyn

[0x80000a8c]:fadd.s t6, t5, t4, dyn
[0x80000a90]:csrrs a2, fcsr, zero
[0x80000a94]:sd t6, 464(fp)
[0x80000a98]:sd a2, 472(fp)
[0x80000a9c]:ld t5, 528(a1)
[0x80000aa0]:ld t4, 536(a1)
[0x80000aa4]:addi s1, zero, 0
[0x80000aa8]:csrrw zero, fcsr, s1
[0x80000aac]:fadd.s t6, t5, t4, dyn

[0x80000aac]:fadd.s t6, t5, t4, dyn
[0x80000ab0]:csrrs a2, fcsr, zero
[0x80000ab4]:sd t6, 480(fp)
[0x80000ab8]:sd a2, 488(fp)
[0x80000abc]:ld t5, 544(a1)
[0x80000ac0]:ld t4, 552(a1)
[0x80000ac4]:addi s1, zero, 0
[0x80000ac8]:csrrw zero, fcsr, s1
[0x80000acc]:fadd.s t6, t5, t4, dyn

[0x80000acc]:fadd.s t6, t5, t4, dyn
[0x80000ad0]:csrrs a2, fcsr, zero
[0x80000ad4]:sd t6, 496(fp)
[0x80000ad8]:sd a2, 504(fp)
[0x80000adc]:ld t5, 560(a1)
[0x80000ae0]:ld t4, 568(a1)
[0x80000ae4]:addi s1, zero, 0
[0x80000ae8]:csrrw zero, fcsr, s1
[0x80000aec]:fadd.s t6, t5, t4, dyn

[0x80000aec]:fadd.s t6, t5, t4, dyn
[0x80000af0]:csrrs a2, fcsr, zero
[0x80000af4]:sd t6, 512(fp)
[0x80000af8]:sd a2, 520(fp)
[0x80000afc]:ld t5, 576(a1)
[0x80000b00]:ld t4, 584(a1)
[0x80000b04]:addi s1, zero, 0
[0x80000b08]:csrrw zero, fcsr, s1
[0x80000b0c]:fadd.s t6, t5, t4, dyn

[0x80000b0c]:fadd.s t6, t5, t4, dyn
[0x80000b10]:csrrs a2, fcsr, zero
[0x80000b14]:sd t6, 528(fp)
[0x80000b18]:sd a2, 536(fp)
[0x80000b1c]:ld t5, 592(a1)
[0x80000b20]:ld t4, 600(a1)
[0x80000b24]:addi s1, zero, 0
[0x80000b28]:csrrw zero, fcsr, s1
[0x80000b2c]:fadd.s t6, t5, t4, dyn

[0x80000b2c]:fadd.s t6, t5, t4, dyn
[0x80000b30]:csrrs a2, fcsr, zero
[0x80000b34]:sd t6, 544(fp)
[0x80000b38]:sd a2, 552(fp)
[0x80000b3c]:ld t5, 608(a1)
[0x80000b40]:ld t4, 616(a1)
[0x80000b44]:addi s1, zero, 0
[0x80000b48]:csrrw zero, fcsr, s1
[0x80000b4c]:fadd.s t6, t5, t4, dyn

[0x80000b4c]:fadd.s t6, t5, t4, dyn
[0x80000b50]:csrrs a2, fcsr, zero
[0x80000b54]:sd t6, 560(fp)
[0x80000b58]:sd a2, 568(fp)
[0x80000b5c]:ld t5, 624(a1)
[0x80000b60]:ld t4, 632(a1)
[0x80000b64]:addi s1, zero, 0
[0x80000b68]:csrrw zero, fcsr, s1
[0x80000b6c]:fadd.s t6, t5, t4, dyn

[0x80000b6c]:fadd.s t6, t5, t4, dyn
[0x80000b70]:csrrs a2, fcsr, zero
[0x80000b74]:sd t6, 576(fp)
[0x80000b78]:sd a2, 584(fp)
[0x80000b7c]:ld t5, 640(a1)
[0x80000b80]:ld t4, 648(a1)
[0x80000b84]:addi s1, zero, 0
[0x80000b88]:csrrw zero, fcsr, s1
[0x80000b8c]:fadd.s t6, t5, t4, dyn

[0x80000b8c]:fadd.s t6, t5, t4, dyn
[0x80000b90]:csrrs a2, fcsr, zero
[0x80000b94]:sd t6, 592(fp)
[0x80000b98]:sd a2, 600(fp)
[0x80000b9c]:ld t5, 656(a1)
[0x80000ba0]:ld t4, 664(a1)
[0x80000ba4]:addi s1, zero, 0
[0x80000ba8]:csrrw zero, fcsr, s1
[0x80000bac]:fadd.s t6, t5, t4, dyn

[0x80000bac]:fadd.s t6, t5, t4, dyn
[0x80000bb0]:csrrs a2, fcsr, zero
[0x80000bb4]:sd t6, 608(fp)
[0x80000bb8]:sd a2, 616(fp)
[0x80000bbc]:ld t5, 672(a1)
[0x80000bc0]:ld t4, 680(a1)
[0x80000bc4]:addi s1, zero, 0
[0x80000bc8]:csrrw zero, fcsr, s1
[0x80000bcc]:fadd.s t6, t5, t4, dyn

[0x80000bcc]:fadd.s t6, t5, t4, dyn
[0x80000bd0]:csrrs a2, fcsr, zero
[0x80000bd4]:sd t6, 624(fp)
[0x80000bd8]:sd a2, 632(fp)
[0x80000bdc]:ld t5, 688(a1)
[0x80000be0]:ld t4, 696(a1)
[0x80000be4]:addi s1, zero, 0
[0x80000be8]:csrrw zero, fcsr, s1
[0x80000bec]:fadd.s t6, t5, t4, dyn

[0x80000bec]:fadd.s t6, t5, t4, dyn
[0x80000bf0]:csrrs a2, fcsr, zero
[0x80000bf4]:sd t6, 640(fp)
[0x80000bf8]:sd a2, 648(fp)
[0x80000bfc]:ld t5, 704(a1)
[0x80000c00]:ld t4, 712(a1)
[0x80000c04]:addi s1, zero, 0
[0x80000c08]:csrrw zero, fcsr, s1
[0x80000c0c]:fadd.s t6, t5, t4, dyn

[0x80000c0c]:fadd.s t6, t5, t4, dyn
[0x80000c10]:csrrs a2, fcsr, zero
[0x80000c14]:sd t6, 656(fp)
[0x80000c18]:sd a2, 664(fp)
[0x80000c1c]:ld t5, 720(a1)
[0x80000c20]:ld t4, 728(a1)
[0x80000c24]:addi s1, zero, 0
[0x80000c28]:csrrw zero, fcsr, s1
[0x80000c2c]:fadd.s t6, t5, t4, dyn

[0x80000c2c]:fadd.s t6, t5, t4, dyn
[0x80000c30]:csrrs a2, fcsr, zero
[0x80000c34]:sd t6, 672(fp)
[0x80000c38]:sd a2, 680(fp)
[0x80000c3c]:ld t5, 736(a1)
[0x80000c40]:ld t4, 744(a1)
[0x80000c44]:addi s1, zero, 0
[0x80000c48]:csrrw zero, fcsr, s1
[0x80000c4c]:fadd.s t6, t5, t4, dyn

[0x80000c4c]:fadd.s t6, t5, t4, dyn
[0x80000c50]:csrrs a2, fcsr, zero
[0x80000c54]:sd t6, 688(fp)
[0x80000c58]:sd a2, 696(fp)
[0x80000c5c]:ld t5, 752(a1)
[0x80000c60]:ld t4, 760(a1)
[0x80000c64]:addi s1, zero, 0
[0x80000c68]:csrrw zero, fcsr, s1
[0x80000c6c]:fadd.s t6, t5, t4, dyn

[0x80000c6c]:fadd.s t6, t5, t4, dyn
[0x80000c70]:csrrs a2, fcsr, zero
[0x80000c74]:sd t6, 704(fp)
[0x80000c78]:sd a2, 712(fp)
[0x80000c7c]:ld t5, 768(a1)
[0x80000c80]:ld t4, 776(a1)
[0x80000c84]:addi s1, zero, 0
[0x80000c88]:csrrw zero, fcsr, s1
[0x80000c8c]:fadd.s t6, t5, t4, dyn

[0x80000c8c]:fadd.s t6, t5, t4, dyn
[0x80000c90]:csrrs a2, fcsr, zero
[0x80000c94]:sd t6, 720(fp)
[0x80000c98]:sd a2, 728(fp)
[0x80000c9c]:ld t5, 784(a1)
[0x80000ca0]:ld t4, 792(a1)
[0x80000ca4]:addi s1, zero, 0
[0x80000ca8]:csrrw zero, fcsr, s1
[0x80000cac]:fadd.s t6, t5, t4, dyn

[0x80000cac]:fadd.s t6, t5, t4, dyn
[0x80000cb0]:csrrs a2, fcsr, zero
[0x80000cb4]:sd t6, 736(fp)
[0x80000cb8]:sd a2, 744(fp)
[0x80000cbc]:ld t5, 800(a1)
[0x80000cc0]:ld t4, 808(a1)
[0x80000cc4]:addi s1, zero, 0
[0x80000cc8]:csrrw zero, fcsr, s1
[0x80000ccc]:fadd.s t6, t5, t4, dyn

[0x80000ccc]:fadd.s t6, t5, t4, dyn
[0x80000cd0]:csrrs a2, fcsr, zero
[0x80000cd4]:sd t6, 752(fp)
[0x80000cd8]:sd a2, 760(fp)
[0x80000cdc]:ld t5, 816(a1)
[0x80000ce0]:ld t4, 824(a1)
[0x80000ce4]:addi s1, zero, 0
[0x80000ce8]:csrrw zero, fcsr, s1
[0x80000cec]:fadd.s t6, t5, t4, dyn

[0x80000cec]:fadd.s t6, t5, t4, dyn
[0x80000cf0]:csrrs a2, fcsr, zero
[0x80000cf4]:sd t6, 768(fp)
[0x80000cf8]:sd a2, 776(fp)
[0x80000cfc]:ld t5, 832(a1)
[0x80000d00]:ld t4, 840(a1)
[0x80000d04]:addi s1, zero, 0
[0x80000d08]:csrrw zero, fcsr, s1
[0x80000d0c]:fadd.s t6, t5, t4, dyn

[0x80000d2c]:fadd.s t6, t5, t4, dyn
[0x80000d30]:csrrs a2, fcsr, zero
[0x80000d34]:sd t6, 800(fp)
[0x80000d38]:sd a2, 808(fp)
[0x80000d3c]:ld t5, 864(a1)
[0x80000d40]:ld t4, 872(a1)
[0x80000d44]:addi s1, zero, 0
[0x80000d48]:csrrw zero, fcsr, s1
[0x80000d4c]:fadd.s t6, t5, t4, dyn

[0x80000d4c]:fadd.s t6, t5, t4, dyn
[0x80000d50]:csrrs a2, fcsr, zero
[0x80000d54]:sd t6, 816(fp)
[0x80000d58]:sd a2, 824(fp)
[0x80000d5c]:ld t5, 880(a1)
[0x80000d60]:ld t4, 888(a1)
[0x80000d64]:addi s1, zero, 0
[0x80000d68]:csrrw zero, fcsr, s1
[0x80000d6c]:fadd.s t6, t5, t4, dyn

[0x80000d6c]:fadd.s t6, t5, t4, dyn
[0x80000d70]:csrrs a2, fcsr, zero
[0x80000d74]:sd t6, 832(fp)
[0x80000d78]:sd a2, 840(fp)
[0x80000d7c]:ld t5, 896(a1)
[0x80000d80]:ld t4, 904(a1)
[0x80000d84]:addi s1, zero, 0
[0x80000d88]:csrrw zero, fcsr, s1
[0x80000d8c]:fadd.s t6, t5, t4, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x31', 'rs2 : x31', 'rd : x31', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003bc]:fadd.s t6, t6, t6, dyn
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
	-[0x800003c8]:sd tp, 8(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x80002620]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x28', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x08 and fm2 == 0x248f41 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003dc]:fadd.s t5, t4, t3, dyn
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t5, 16(ra)
	-[0x800003e8]:sd tp, 24(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x80002630]:0x0000000000000001




Last Coverpoint : ['rs1 : x30', 'rs2 : x30', 'rd : x29', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x800003fc]:fadd.s t4, t5, t5, dyn
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t4, 32(ra)
	-[0x80000408]:sd tp, 40(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x80002640]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x29', 'rd : x28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x0f and fm2 == 0x008feb and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fadd.s t3, t3, t4, dyn
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t3, 48(ra)
	-[0x80000428]:sd tp, 56(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x80002650]:0x0000000000000001




Last Coverpoint : ['rs1 : x26', 'rs2 : x27', 'rd : x27', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x12 and fm2 == 0x20b3e6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000043c]:fadd.s s11, s10, s11, dyn
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s11, 64(ra)
	-[0x80000448]:sd tp, 72(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x80002660]:0x0000000000000001




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x15 and fm2 == 0x48e0e0 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fadd.s s10, s11, s9, dyn
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s10, 80(ra)
	-[0x80000468]:sd tp, 88(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x80002670]:0x0000000000000001




Last Coverpoint : ['rs1 : x24', 'rs2 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x18 and fm2 == 0x7b1918 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fadd.s s9, s8, s10, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
	-[0x80000488]:sd tp, 104(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x80002680]:0x0000000000000001




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x1c and fm2 == 0x1cefaf and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fadd.s s8, s9, s7, dyn
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
	-[0x800004a8]:sd tp, 120(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x80002690]:0x0000000000000001




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x1f and fm2 == 0x442b9a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fadd.s s7, s6, s8, dyn
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
	-[0x800004c8]:sd tp, 136(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x800026a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x22 and fm2 == 0x753681 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fadd.s s6, s7, s5, dyn
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
	-[0x800004e8]:sd tp, 152(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x800026b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x26 and fm2 == 0x194210 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fadd.s s5, s4, s6, dyn
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
	-[0x80000508]:sd tp, 168(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x800026c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x29 and fm2 == 0x3f9295 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fadd.s s4, s5, s3, dyn
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
	-[0x80000528]:sd tp, 184(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x800026d0]:0x0000000000000001




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x2c and fm2 == 0x6f773a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fadd.s s3, s2, s4, dyn
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
	-[0x80000548]:sd tp, 200(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x800026e0]:0x0000000000000001




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x30 and fm2 == 0x15aa84 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fadd.s s2, s3, a7, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
	-[0x80000568]:sd tp, 216(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x800026f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x33 and fm2 == 0x3b1525 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fadd.s a7, a6, s2, dyn
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
	-[0x80000588]:sd tp, 232(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x80002700]:0x0000000000000001




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x36 and fm2 == 0x69da6f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fadd.s a6, a7, a5, dyn
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
	-[0x800005a8]:sd tp, 248(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x80002710]:0x0000000000000001




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x3a and fm2 == 0x122885 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fadd.s a5, a4, a6, dyn
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
	-[0x800005c8]:sd tp, 264(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x80002720]:0x0000000000000001




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x3d and fm2 == 0x36b2a6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fadd.s a4, a5, a3, dyn
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
	-[0x800005e8]:sd tp, 280(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x80002730]:0x0000000000000001




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x40 and fm2 == 0x645f50 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fadd.s a3, a2, a4, dyn
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
	-[0x80000608]:sd tp, 296(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x80002740]:0x0000000000000001




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x44 and fm2 == 0x0ebb92 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fadd.s a2, a3, a1, dyn
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
	-[0x80000628]:sd tp, 312(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x80002750]:0x0000000000000001




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x47 and fm2 == 0x326a76 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fadd.s a1, a0, a2, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
	-[0x80000648]:sd tp, 328(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x80002760]:0x0000000000000001




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x4a and fm2 == 0x5f0514 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fadd.s a0, a1, s1, dyn
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:sd a0, 336(ra)
	-[0x80000668]:sd tp, 344(ra)
Current Store : [0x80000668] : sd tp, 344(ra) -- Store: [0x80002770]:0x0000000000000001




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x4e and fm2 == 0x0b632c and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fadd.s s1, fp, a0, dyn
	-[0x80000688]:csrrs a2, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
	-[0x80000690]:sd a2, 360(ra)
Current Store : [0x80000690] : sd a2, 360(ra) -- Store: [0x80002780]:0x0000000000000001




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x51 and fm2 == 0x2e3bf8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fadd.s fp, s1, t2, dyn
	-[0x800006a8]:csrrs a2, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
	-[0x800006b0]:sd a2, 376(ra)
Current Store : [0x800006b0] : sd a2, 376(ra) -- Store: [0x80002790]:0x0000000000000001




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x54 and fm2 == 0x59caf6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fadd.s t2, t1, fp, dyn
	-[0x800006c8]:csrrs a2, fcsr, zero
	-[0x800006cc]:sd t2, 384(ra)
	-[0x800006d0]:sd a2, 392(ra)
Current Store : [0x800006d0] : sd a2, 392(ra) -- Store: [0x800027a0]:0x0000000000000001




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x58 and fm2 == 0x081ed9 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fadd.s t1, t2, t0, dyn
	-[0x800006f0]:csrrs a2, fcsr, zero
	-[0x800006f4]:sd t1, 0(fp)
	-[0x800006f8]:sd a2, 8(fp)
Current Store : [0x800006f8] : sd a2, 8(fp) -- Store: [0x800027b0]:0x0000000000000001




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x5b and fm2 == 0x2a2690 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fadd.s t0, tp, t1, dyn
	-[0x80000710]:csrrs a2, fcsr, zero
	-[0x80000714]:sd t0, 16(fp)
	-[0x80000718]:sd a2, 24(fp)
Current Store : [0x80000718] : sd a2, 24(fp) -- Store: [0x800027c0]:0x0000000000000001




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x5e and fm2 == 0x54b034 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fadd.s tp, t0, gp, dyn
	-[0x80000730]:csrrs a2, fcsr, zero
	-[0x80000734]:sd tp, 32(fp)
	-[0x80000738]:sd a2, 40(fp)
Current Store : [0x80000738] : sd a2, 40(fp) -- Store: [0x800027d0]:0x0000000000000001




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x62 and fm2 == 0x04ee20 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fadd.s gp, sp, tp, dyn
	-[0x80000750]:csrrs a2, fcsr, zero
	-[0x80000754]:sd gp, 48(fp)
	-[0x80000758]:sd a2, 56(fp)
Current Store : [0x80000758] : sd a2, 56(fp) -- Store: [0x800027e0]:0x0000000000000001




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x65 and fm2 == 0x2629a8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000076c]:fadd.s sp, gp, ra, dyn
	-[0x80000770]:csrrs a2, fcsr, zero
	-[0x80000774]:sd sp, 64(fp)
	-[0x80000778]:sd a2, 72(fp)
Current Store : [0x80000778] : sd a2, 72(fp) -- Store: [0x800027f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x8000078c]:fadd.s ra, zero, sp, dyn
	-[0x80000790]:csrrs a2, fcsr, zero
	-[0x80000794]:sd ra, 80(fp)
	-[0x80000798]:sd a2, 88(fp)
Current Store : [0x80000798] : sd a2, 88(fp) -- Store: [0x80002800]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x6c and fm2 == 0x01d08b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fadd.s t6, ra, t5, dyn
	-[0x800007b0]:csrrs a2, fcsr, zero
	-[0x800007b4]:sd t6, 96(fp)
	-[0x800007b8]:sd a2, 104(fp)
Current Store : [0x800007b8] : sd a2, 104(fp) -- Store: [0x80002810]:0x0000000000000001




Last Coverpoint : ['rs2 : x0', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007cc]:fadd.s t6, t5, zero, dyn
	-[0x800007d0]:csrrs a2, fcsr, zero
	-[0x800007d4]:sd t6, 112(fp)
	-[0x800007d8]:sd a2, 120(fp)
Current Store : [0x800007d8] : sd a2, 120(fp) -- Store: [0x80002820]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x4ad5da and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fadd.s zero, t6, t5, dyn
	-[0x800007f0]:csrrs a2, fcsr, zero
	-[0x800007f4]:sd zero, 128(fp)
	-[0x800007f8]:sd a2, 136(fp)
Current Store : [0x800007f8] : sd a2, 136(fp) -- Store: [0x80002830]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x75 and fm2 == 0x7d8b51 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fadd.s t6, t5, t4, dyn
	-[0x80000810]:csrrs a2, fcsr, zero
	-[0x80000814]:sd t6, 144(fp)
	-[0x80000818]:sd a2, 152(fp)
Current Store : [0x80000818] : sd a2, 152(fp) -- Store: [0x80002840]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x79 and fm2 == 0x1e7712 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fadd.s t6, t5, t4, dyn
	-[0x80000830]:csrrs a2, fcsr, zero
	-[0x80000834]:sd t6, 160(fp)
	-[0x80000838]:sd a2, 168(fp)
Current Store : [0x80000838] : sd a2, 168(fp) -- Store: [0x80002850]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x7c and fm2 == 0x4614d7 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fadd.s t6, t5, t4, dyn
	-[0x80000850]:csrrs a2, fcsr, zero
	-[0x80000854]:sd t6, 176(fp)
	-[0x80000858]:sd a2, 184(fp)
Current Store : [0x80000858] : sd a2, 184(fp) -- Store: [0x80002860]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x779a0d and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fadd.s t6, t5, t4, dyn
	-[0x80000870]:csrrs a2, fcsr, zero
	-[0x80000874]:sd t6, 192(fp)
	-[0x80000878]:sd a2, 200(fp)
Current Store : [0x80000878] : sd a2, 200(fp) -- Store: [0x80002870]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x83 and fm2 == 0x1ac048 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fadd.s t6, t5, t4, dyn
	-[0x80000890]:csrrs a2, fcsr, zero
	-[0x80000894]:sd t6, 208(fp)
	-[0x80000898]:sd a2, 216(fp)
Current Store : [0x80000898] : sd a2, 216(fp) -- Store: [0x80002880]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x86 and fm2 == 0x41705a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fadd.s t6, t5, t4, dyn
	-[0x800008b0]:csrrs a2, fcsr, zero
	-[0x800008b4]:sd t6, 224(fp)
	-[0x800008b8]:sd a2, 232(fp)
Current Store : [0x800008b8] : sd a2, 232(fp) -- Store: [0x80002890]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x89 and fm2 == 0x71cc71 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fadd.s t6, t5, t4, dyn
	-[0x800008d0]:csrrs a2, fcsr, zero
	-[0x800008d4]:sd t6, 240(fp)
	-[0x800008d8]:sd a2, 248(fp)
Current Store : [0x800008d8] : sd a2, 248(fp) -- Store: [0x800028a0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x8d and fm2 == 0x171fc6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fadd.s t6, t5, t4, dyn
	-[0x800008f0]:csrrs a2, fcsr, zero
	-[0x800008f4]:sd t6, 256(fp)
	-[0x800008f8]:sd a2, 264(fp)
Current Store : [0x800008f8] : sd a2, 264(fp) -- Store: [0x800028b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x90 and fm2 == 0x3ce7b8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fadd.s t6, t5, t4, dyn
	-[0x80000910]:csrrs a2, fcsr, zero
	-[0x80000914]:sd t6, 272(fp)
	-[0x80000918]:sd a2, 280(fp)
Current Store : [0x80000918] : sd a2, 280(fp) -- Store: [0x800028c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x93 and fm2 == 0x6c21a6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fadd.s t6, t5, t4, dyn
	-[0x80000930]:csrrs a2, fcsr, zero
	-[0x80000934]:sd t6, 288(fp)
	-[0x80000938]:sd a2, 296(fp)
Current Store : [0x80000938] : sd a2, 296(fp) -- Store: [0x800028d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x97 and fm2 == 0x139508 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fadd.s t6, t5, t4, dyn
	-[0x80000950]:csrrs a2, fcsr, zero
	-[0x80000954]:sd t6, 304(fp)
	-[0x80000958]:sd a2, 312(fp)
Current Store : [0x80000958] : sd a2, 312(fp) -- Store: [0x800028e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x9a and fm2 == 0x387a4a and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fadd.s t6, t5, t4, dyn
	-[0x80000970]:csrrs a2, fcsr, zero
	-[0x80000974]:sd t6, 320(fp)
	-[0x80000978]:sd a2, 328(fp)
Current Store : [0x80000978] : sd a2, 328(fp) -- Store: [0x800028f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x9d and fm2 == 0x6698dc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fadd.s t6, t5, t4, dyn
	-[0x80000990]:csrrs a2, fcsr, zero
	-[0x80000994]:sd t6, 336(fp)
	-[0x80000998]:sd a2, 344(fp)
Current Store : [0x80000998] : sd a2, 344(fp) -- Store: [0x80002900]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xa1 and fm2 == 0x101f89 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fadd.s t6, t5, t4, dyn
	-[0x800009b0]:csrrs a2, fcsr, zero
	-[0x800009b4]:sd t6, 352(fp)
	-[0x800009b8]:sd a2, 360(fp)
Current Store : [0x800009b8] : sd a2, 360(fp) -- Store: [0x80002910]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xa4 and fm2 == 0x34276c and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fadd.s t6, t5, t4, dyn
	-[0x800009d0]:csrrs a2, fcsr, zero
	-[0x800009d4]:sd t6, 368(fp)
	-[0x800009d8]:sd a2, 376(fp)
Current Store : [0x800009d8] : sd a2, 376(fp) -- Store: [0x80002920]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xa7 and fm2 == 0x613147 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fadd.s t6, t5, t4, dyn
	-[0x800009f0]:csrrs a2, fcsr, zero
	-[0x800009f4]:sd t6, 384(fp)
	-[0x800009f8]:sd a2, 392(fp)
Current Store : [0x800009f8] : sd a2, 392(fp) -- Store: [0x80002930]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xab and fm2 == 0x0cbecc and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fadd.s t6, t5, t4, dyn
	-[0x80000a10]:csrrs a2, fcsr, zero
	-[0x80000a14]:sd t6, 400(fp)
	-[0x80000a18]:sd a2, 408(fp)
Current Store : [0x80000a18] : sd a2, 408(fp) -- Store: [0x80002940]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xae and fm2 == 0x2fee7f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fadd.s t6, t5, t4, dyn
	-[0x80000a30]:csrrs a2, fcsr, zero
	-[0x80000a34]:sd t6, 416(fp)
	-[0x80000a38]:sd a2, 424(fp)
Current Store : [0x80000a38] : sd a2, 424(fp) -- Store: [0x80002950]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xb1 and fm2 == 0x5bea1f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fadd.s t6, t5, t4, dyn
	-[0x80000a50]:csrrs a2, fcsr, zero
	-[0x80000a54]:sd t6, 432(fp)
	-[0x80000a58]:sd a2, 440(fp)
Current Store : [0x80000a58] : sd a2, 440(fp) -- Store: [0x80002960]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xb5 and fm2 == 0x097253 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fadd.s t6, t5, t4, dyn
	-[0x80000a70]:csrrs a2, fcsr, zero
	-[0x80000a74]:sd t6, 448(fp)
	-[0x80000a78]:sd a2, 456(fp)
Current Store : [0x80000a78] : sd a2, 456(fp) -- Store: [0x80002970]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xb8 and fm2 == 0x2bcee8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fadd.s t6, t5, t4, dyn
	-[0x80000a90]:csrrs a2, fcsr, zero
	-[0x80000a94]:sd t6, 464(fp)
	-[0x80000a98]:sd a2, 472(fp)
Current Store : [0x80000a98] : sd a2, 472(fp) -- Store: [0x80002980]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xbb and fm2 == 0x56c2a2 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fadd.s t6, t5, t4, dyn
	-[0x80000ab0]:csrrs a2, fcsr, zero
	-[0x80000ab4]:sd t6, 480(fp)
	-[0x80000ab8]:sd a2, 488(fp)
Current Store : [0x80000ab8] : sd a2, 488(fp) -- Store: [0x80002990]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xbf and fm2 == 0x0639a5 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fadd.s t6, t5, t4, dyn
	-[0x80000ad0]:csrrs a2, fcsr, zero
	-[0x80000ad4]:sd t6, 496(fp)
	-[0x80000ad8]:sd a2, 504(fp)
Current Store : [0x80000ad8] : sd a2, 504(fp) -- Store: [0x800029a0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xc2 and fm2 == 0x27c80f and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fadd.s t6, t5, t4, dyn
	-[0x80000af0]:csrrs a2, fcsr, zero
	-[0x80000af4]:sd t6, 512(fp)
	-[0x80000af8]:sd a2, 520(fp)
Current Store : [0x80000af8] : sd a2, 520(fp) -- Store: [0x800029b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xc5 and fm2 == 0x51ba13 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fadd.s t6, t5, t4, dyn
	-[0x80000b10]:csrrs a2, fcsr, zero
	-[0x80000b14]:sd t6, 528(fp)
	-[0x80000b18]:sd a2, 536(fp)
Current Store : [0x80000b18] : sd a2, 536(fp) -- Store: [0x800029c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xc9 and fm2 == 0x03144b and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fadd.s t6, t5, t4, dyn
	-[0x80000b30]:csrrs a2, fcsr, zero
	-[0x80000b34]:sd t6, 544(fp)
	-[0x80000b38]:sd a2, 552(fp)
Current Store : [0x80000b38] : sd a2, 552(fp) -- Store: [0x800029d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xcc and fm2 == 0x23d95e and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fadd.s t6, t5, t4, dyn
	-[0x80000b50]:csrrs a2, fcsr, zero
	-[0x80000b54]:sd t6, 560(fp)
	-[0x80000b58]:sd a2, 568(fp)
Current Store : [0x80000b58] : sd a2, 568(fp) -- Store: [0x800029e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xcf and fm2 == 0x4ccfb6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fadd.s t6, t5, t4, dyn
	-[0x80000b70]:csrrs a2, fcsr, zero
	-[0x80000b74]:sd t6, 576(fp)
	-[0x80000b78]:sd a2, 584(fp)
Current Store : [0x80000b78] : sd a2, 584(fp) -- Store: [0x800029f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xd3 and fm2 == 0x0001d2 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fadd.s t6, t5, t4, dyn
	-[0x80000b90]:csrrs a2, fcsr, zero
	-[0x80000b94]:sd t6, 592(fp)
	-[0x80000b98]:sd a2, 600(fp)
Current Store : [0x80000b98] : sd a2, 600(fp) -- Store: [0x80002a00]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xd6 and fm2 == 0x200246 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fadd.s t6, t5, t4, dyn
	-[0x80000bb0]:csrrs a2, fcsr, zero
	-[0x80000bb4]:sd t6, 608(fp)
	-[0x80000bb8]:sd a2, 616(fp)
Current Store : [0x80000bb8] : sd a2, 616(fp) -- Store: [0x80002a10]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xd9 and fm2 == 0x4802d8 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fadd.s t6, t5, t4, dyn
	-[0x80000bd0]:csrrs a2, fcsr, zero
	-[0x80000bd4]:sd t6, 624(fp)
	-[0x80000bd8]:sd a2, 632(fp)
Current Store : [0x80000bd8] : sd a2, 632(fp) -- Store: [0x80002a20]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xdc and fm2 == 0x7a038e and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fadd.s t6, t5, t4, dyn
	-[0x80000bf0]:csrrs a2, fcsr, zero
	-[0x80000bf4]:sd t6, 640(fp)
	-[0x80000bf8]:sd a2, 648(fp)
Current Store : [0x80000bf8] : sd a2, 648(fp) -- Store: [0x80002a30]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xe0 and fm2 == 0x1c4239 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fadd.s t6, t5, t4, dyn
	-[0x80000c10]:csrrs a2, fcsr, zero
	-[0x80000c14]:sd t6, 656(fp)
	-[0x80000c18]:sd a2, 664(fp)
Current Store : [0x80000c18] : sd a2, 664(fp) -- Store: [0x80002a40]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xe3 and fm2 == 0x4352c7 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fadd.s t6, t5, t4, dyn
	-[0x80000c30]:csrrs a2, fcsr, zero
	-[0x80000c34]:sd t6, 672(fp)
	-[0x80000c38]:sd a2, 680(fp)
Current Store : [0x80000c38] : sd a2, 680(fp) -- Store: [0x80002a50]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xe6 and fm2 == 0x742779 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fadd.s t6, t5, t4, dyn
	-[0x80000c50]:csrrs a2, fcsr, zero
	-[0x80000c54]:sd t6, 688(fp)
	-[0x80000c58]:sd a2, 696(fp)
Current Store : [0x80000c58] : sd a2, 696(fp) -- Store: [0x80002a60]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xea and fm2 == 0x1898ab and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fadd.s t6, t5, t4, dyn
	-[0x80000c70]:csrrs a2, fcsr, zero
	-[0x80000c74]:sd t6, 704(fp)
	-[0x80000c78]:sd a2, 712(fp)
Current Store : [0x80000c78] : sd a2, 712(fp) -- Store: [0x80002a70]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xed and fm2 == 0x3ebed6 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fadd.s t6, t5, t4, dyn
	-[0x80000c90]:csrrs a2, fcsr, zero
	-[0x80000c94]:sd t6, 720(fp)
	-[0x80000c98]:sd a2, 728(fp)
Current Store : [0x80000c98] : sd a2, 728(fp) -- Store: [0x80002a80]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xf0 and fm2 == 0x6e6e8c and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fadd.s t6, t5, t4, dyn
	-[0x80000cb0]:csrrs a2, fcsr, zero
	-[0x80000cb4]:sd t6, 736(fp)
	-[0x80000cb8]:sd a2, 744(fp)
Current Store : [0x80000cb8] : sd a2, 744(fp) -- Store: [0x80002a90]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xf4 and fm2 == 0x150517 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fadd.s t6, t5, t4, dyn
	-[0x80000cd0]:csrrs a2, fcsr, zero
	-[0x80000cd4]:sd t6, 752(fp)
	-[0x80000cd8]:sd a2, 760(fp)
Current Store : [0x80000cd8] : sd a2, 760(fp) -- Store: [0x80002aa0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fadd.s t6, t5, t4, dyn
	-[0x80000cf0]:csrrs a2, fcsr, zero
	-[0x80000cf4]:sd t6, 768(fp)
	-[0x80000cf8]:sd a2, 776(fp)
Current Store : [0x80000cf8] : sd a2, 776(fp) -- Store: [0x80002ab0]:0x0000000000000005




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x000000 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fadd.s t6, t5, t4, dyn
	-[0x80000d10]:csrrs a2, fcsr, zero
	-[0x80000d14]:sd t6, 784(fp)
	-[0x80000d18]:sd a2, 792(fp)
Current Store : [0x80000d18] : sd a2, 792(fp) -- Store: [0x80002ac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x0b and fm2 == 0x4db312 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fadd.s t6, t5, t4, dyn
	-[0x80000d30]:csrrs a2, fcsr, zero
	-[0x80000d34]:sd t6, 800(fp)
	-[0x80000d38]:sd a2, 808(fp)
Current Store : [0x80000d38] : sd a2, 808(fp) -- Store: [0x80002ad0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x68 and fm2 == 0x4fb413 and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fadd.s t6, t5, t4, dyn
	-[0x80000d50]:csrrs a2, fcsr, zero
	-[0x80000d54]:sd t6, 816(fp)
	-[0x80000d58]:sd a2, 824(fp)
Current Store : [0x80000d58] : sd a2, 824(fp) -- Store: [0x80002ae0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x6f and fm2 == 0x2244ae and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fadd.s t6, t5, t4, dyn
	-[0x80000d70]:csrrs a2, fcsr, zero
	-[0x80000d74]:sd t6, 832(fp)
	-[0x80000d78]:sd a2, 840(fp)
Current Store : [0x80000d78] : sd a2, 840(fp) -- Store: [0x80002af0]:0x0000000000000001




Last Coverpoint : ['mnemonic : fadd.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x260524 and fs2 == 0 and fe2 == 0x72 and fm2 == 0x4ad5da and  fcsr == 0x0 and rm_val == 7  and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fadd.s t6, t5, t4, dyn
	-[0x80000d90]:csrrs a2, fcsr, zero
	-[0x80000d94]:sd t6, 848(fp)
	-[0x80000d98]:sd a2, 856(fp)
Current Store : [0x80000d98] : sd a2, 856(fp) -- Store: [0x80002b00]:0x0000000000000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
