
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000740')]      |
| SIG_REGION                | [('0x80002310', '0x80002420', '34 dwords')]      |
| COV_LABELS                | fclass_b1      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/work-fclass/fclass_b1-01.S/ref.S    |
| Total Number of coverpoints| 89     |
| Total Coverpoints Hit     | 89      |
| Total Signature Updates   | 41      |
| STAT1                     | 22      |
| STAT2                     | 0      |
| STAT3                     | 10     |
| STAT4                     | 19     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x80000594]:fclass.s a4, a5
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a2, 144(gp)
[0x800005a8]:addi sp, zero, 0
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fclass.s a3, a2
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a3, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fclass.s a2, a3
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a0, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fclass.s a1, a0
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld a1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fclass.s a0, a1
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fclass.s s1, fp
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x800005b0]:fclass.s a3, a2
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a3, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fclass.s a2, a3
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a0, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fclass.s a1, a0
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld a1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fclass.s a0, a1
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fclass.s s1, fp
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x800005cc]:fclass.s a2, a3
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a0, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fclass.s a1, a0
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld a1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fclass.s a0, a1
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fclass.s s1, fp
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x80000604]:fclass.s a0, a1
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld fp, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fclass.s s1, fp
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x80000620]:fclass.s s1, fp
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:auipc a0, 2
[0x80000634]:addi a0, a0, 2712
[0x80000638]:ld s1, 0(a0)
[0x8000063c]:addi sp, zero, 0
[0x80000640]:csrrw zero, fcsr, sp
[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x80000644]:fclass.s fp, s1
[0x80000648]:csrrs a1, fcsr, zero
[0x8000064c]:sd fp, 368(ra)
[0x80000650]:sd a1, 376(ra)
[0x80000654]:ld t1, 8(a0)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x80000660]:fclass.s t2, t1
[0x80000664]:csrrs a1, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a1, 392(ra)
[0x80000670]:ld t2, 16(a0)
[0x80000674]:addi fp, zero, 0
[0x80000678]:csrrw zero, fcsr, fp
[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x8000067c]:fclass.s t1, t2
[0x80000680]:csrrs a1, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a1, 408(ra)
[0x8000068c]:ld tp, 24(a0)
[0x80000690]:addi fp, zero, 0
[0x80000694]:csrrw zero, fcsr, fp
[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x80000698]:fclass.s t0, tp
[0x8000069c]:csrrs a1, fcsr, zero
[0x800006a0]:sd t0, 416(ra)
[0x800006a4]:sd a1, 424(ra)
[0x800006a8]:auipc t1, 2
[0x800006ac]:addi t1, t1, 3400
[0x800006b0]:ld t0, 32(a0)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fclass.s tp, t0
[0x800006c0]:csrrs a1, fcsr, zero
[0x800006c4]:sd tp, 0(t1)
[0x800006c8]:sd a1, 8(t1)
[0x800006cc]:ld sp, 40(a0)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fclass.s gp, sp
[0x800006dc]:csrrs a1, fcsr, zero
[0x800006e0]:sd gp, 16(t1)
[0x800006e4]:sd a1, 24(t1)
[0x800006e8]:ld gp, 48(a0)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fclass.s sp, gp
[0x800006f8]:csrrs a1, fcsr, zero
[0x800006fc]:sd sp, 32(t1)
[0x80000700]:sd a1, 40(t1)
[0x80000704]:ld zero, 56(a0)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fclass.s ra, zero
[0x80000714]:csrrs a1, fcsr, zero
[0x80000718]:sd ra, 48(t1)
[0x8000071c]:sd a1, 56(t1)
[0x80000720]:ld ra, 64(a0)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0

[0x8000072c]:fclass.s zero, ra
[0x80000730]:csrrs a1, fcsr, zero
[0x80000734]:sd zero, 64(t1)
[0x80000738]:sd a1, 72(t1)
[0x8000073c]:addi zero, zero, 0



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fclass.s', 'rs1 : x30', 'rd : x31', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003b8]:fclass.s t6, t5
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002320]:0x0000000000000000




Last Coverpoint : ['rs1 : x31', 'rd : x30', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800003d4]:fclass.s t5, t6
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002330]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rd : x29', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003f0]:fclass.s t4, t3
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002340]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rd : x28', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000040c]:fclass.s t3, t4
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002350]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rd : x27', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000428]:fclass.s s11, s10
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002360]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rd : x26', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000444]:fclass.s s10, s11
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002370]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rd : x25', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000460]:fclass.s s9, s8
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002380]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rd : x24', 'fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fclass.s s8, s9
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002390]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rd : x23', 'fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000498]:fclass.s s7, s6
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800023a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rd : x22', 'fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004b4]:fclass.s s6, s7
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800023b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rd : x21', 'fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004d0]:fclass.s s5, s4
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800023c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rd : x20', 'fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800004ec]:fclass.s s4, s5
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800023d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rd : x19', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000508]:fclass.s s3, s2
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800023e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : x18', 'fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000524]:fclass.s s2, s3
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800023f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : x17', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000540]:fclass.s a7, a6
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002400]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rd : x16', 'fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fclass.s a6, a7
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002410]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : x15', 'fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000578]:fclass.s a5, a4
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002420]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : x3']
Last Code Sequence : 
	-[0x800006d8]:fclass.s gp, sp
	-[0x800006dc]:csrrs a1, fcsr, zero
	-[0x800006e0]:sd gp, 16(t1)
Current Store : [0x800006e4] : sd a1, 24(t1) -- Store: [0x80002408]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rd : x2']
Last Code Sequence : 
	-[0x800006f4]:fclass.s sp, gp
	-[0x800006f8]:csrrs a1, fcsr, zero
	-[0x800006fc]:sd sp, 32(t1)
Current Store : [0x80000700] : sd a1, 40(t1) -- Store: [0x80002418]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|               signature               |                                                                            coverpoints                                                                             |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                code                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|---:|---------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|   1|[0x80002318]<br>0x0000000000000010<br> |- mnemonic : fclass.s<br> - rs1 : x30<br> - rd : x31<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br> |[0x800003b8]:fclass.s t6, t5<br> [0x800003bc]:csrrs tp, fcsr, zero<br> [0x800003c0]:sd t6, 0(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|   2|[0x80002328]<br>0x0000000000000008<br> |- rs1 : x31<br> - rd : x30<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800003d4]:fclass.s t5, t6<br> [0x800003d8]:csrrs tp, fcsr, zero<br> [0x800003dc]:sd t5, 16(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|   3|[0x80002338]<br>0x0000000000000020<br> |- rs1 : x28<br> - rd : x29<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x800003f0]:fclass.s t4, t3<br> [0x800003f4]:csrrs tp, fcsr, zero<br> [0x800003f8]:sd t4, 32(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|   4|[0x80002348]<br>0x0000000000000004<br> |- rs1 : x29<br> - rd : x28<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x8000040c]:fclass.s t3, t4<br> [0x80000410]:csrrs tp, fcsr, zero<br> [0x80000414]:sd t3, 48(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|   5|[0x80002358]<br>0x0000000000000020<br> |- rs1 : x26<br> - rd : x27<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x000002 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000428]:fclass.s s11, s10<br> [0x8000042c]:csrrs tp, fcsr, zero<br> [0x80000430]:sd s11, 64(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|   6|[0x80002368]<br>0x0000000000000004<br> |- rs1 : x27<br> - rd : x26<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x7ffffe and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x80000444]:fclass.s s10, s11<br> [0x80000448]:csrrs tp, fcsr, zero<br> [0x8000044c]:sd s10, 80(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|   7|[0x80002378]<br>0x0000000000000020<br> |- rs1 : x24<br> - rd : x25<br> - fs1 == 0 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000460]:fclass.s s9, s8<br> [0x80000464]:csrrs tp, fcsr, zero<br> [0x80000468]:sd s9, 96(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|   8|[0x80002388]<br>0x0000000000000004<br> |- rs1 : x25<br> - rd : x24<br> - fs1 == 1 and fe1 == 0x00 and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x8000047c]:fclass.s s8, s9<br> [0x80000480]:csrrs tp, fcsr, zero<br> [0x80000484]:sd s8, 112(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|   9|[0x80002398]<br>0x0000000000000040<br> |- rs1 : x22<br> - rd : x23<br> - fs1 == 0 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000498]:fclass.s s7, s6<br> [0x8000049c]:csrrs tp, fcsr, zero<br> [0x800004a0]:sd s7, 128(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  10|[0x800023a8]<br>0x0000000000000002<br> |- rs1 : x23<br> - rd : x22<br> - fs1 == 1 and fe1 == 0x01 and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800004b4]:fclass.s s6, s7<br> [0x800004b8]:csrrs tp, fcsr, zero<br> [0x800004bc]:sd s6, 144(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  11|[0x800023b8]<br>0x0000000000000040<br> |- rs1 : x20<br> - rd : x21<br> - fs1 == 0 and fe1 == 0x01 and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x800004d0]:fclass.s s5, s4<br> [0x800004d4]:csrrs tp, fcsr, zero<br> [0x800004d8]:sd s5, 160(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  12|[0x800023c8]<br>0x0000000000000002<br> |- rs1 : x21<br> - rd : x20<br> - fs1 == 1 and fe1 == 0x01 and fm1 == 0x055555 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x800004ec]:fclass.s s4, s5<br> [0x800004f0]:csrrs tp, fcsr, zero<br> [0x800004f4]:sd s4, 176(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  13|[0x800023d8]<br>0x0000000000000040<br> |- rs1 : x18<br> - rd : x19<br> - fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000508]:fclass.s s3, s2<br> [0x8000050c]:csrrs tp, fcsr, zero<br> [0x80000510]:sd s3, 192(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  14|[0x800023e8]<br>0x0000000000000002<br> |- rs1 : x19<br> - rd : x18<br> - fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x80000524]:fclass.s s2, s3<br> [0x80000528]:csrrs tp, fcsr, zero<br> [0x8000052c]:sd s2, 208(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  15|[0x800023f8]<br>0x0000000000000080<br> |- rs1 : x16<br> - rd : x17<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000540]:fclass.s a7, a6<br> [0x80000544]:csrrs tp, fcsr, zero<br> [0x80000548]:sd a7, 224(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  16|[0x80002408]<br>0x0000000000000001<br> |- rs1 : x17<br> - rd : x16<br> - fs1 == 1 and fe1 == 0xff and fm1 == 0x000000 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff  #nosat<br>                           |[0x8000055c]:fclass.s a6, a7<br> [0x80000560]:csrrs tp, fcsr, zero<br> [0x80000564]:sd a6, 240(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  17|[0x80002418]<br>0x0000000000000200<br> |- rs1 : x14<br> - rd : x15<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x400000 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x80000578]:fclass.s a5, a4<br> [0x8000057c]:csrrs tp, fcsr, zero<br> [0x80000580]:sd a5, 256(ra)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
|  18|[0x800023f0]<br>0x0000000000000010<br> |- rs1 : x5<br> - rd : x4<br>                                                                                                                                        |[0x800006bc]:fclass.s tp, t0<br> [0x800006c0]:csrrs a1, fcsr, zero<br> [0x800006c4]:sd tp, 0(t1)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|  19|[0x800023f8]<br>0x0000000000000000<br> |- rs1 : x10<br> - rd : x11<br> - fs1 == 0 and fe1 == 0xff and fm1 == 0x000001 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000  #nosat<br>                           |[0x800005e8]:fclass.s a1, a0<br> [0x800005ec]:csrrs tp, fcsr, zero<br> [0x800005f0]:sd a1, 320(ra)<br> [0x800005f4]:sd tp, 328(ra)<br> [0x800005f8]:ld a1, 168(gp)<br> [0x800005fc]:addi sp, zero, 0<br> [0x80000600]:csrrw zero, fcsr, sp<br> [0x80000604]:fclass.s a0, a1<br> [0x80000608]:csrrs tp, fcsr, zero<br> [0x8000060c]:sd a0, 336(ra)<br> [0x80000610]:sd tp, 344(ra)<br> [0x80000614]:ld fp, 176(gp)<br> [0x80000618]:addi sp, zero, 0<br> [0x8000061c]:csrrw zero, fcsr, sp<br> [0x80000620]:fclass.s s1, fp<br> [0x80000624]:csrrs tp, fcsr, zero<br> [0x80000628]:sd s1, 352(ra)<br> [0x8000062c]:sd tp, 360(ra)<br> [0x80000630]:auipc a0, 2<br> [0x80000634]:addi a0, a0, 2712<br> [0x80000638]:ld s1, 0(a0)<br> [0x8000063c]:addi sp, zero, 0<br> [0x80000640]:csrrw zero, fcsr, sp<br> [0x80000644]:fclass.s fp, s1<br> [0x80000648]:csrrs a1, fcsr, zero<br> [0x8000064c]:sd fp, 368(ra)<br> [0x80000650]:sd a1, 376(ra)<br> [0x80000654]:ld t1, 8(a0)<br> [0x80000658]:addi sp, zero, 0<br> [0x8000065c]:csrrw zero, fcsr, sp<br> [0x80000660]:fclass.s t2, t1<br> [0x80000664]:csrrs a1, fcsr, zero<br> [0x80000668]:sd t2, 384(ra)<br> [0x8000066c]:sd a1, 392(ra)<br> [0x80000670]:ld t2, 16(a0)<br> [0x80000674]:addi fp, zero, 0<br> [0x80000678]:csrrw zero, fcsr, fp<br> [0x8000067c]:fclass.s t1, t2<br> [0x80000680]:csrrs a1, fcsr, zero<br> [0x80000684]:sd t1, 400(ra)<br> [0x80000688]:sd a1, 408(ra)<br> [0x8000068c]:ld tp, 24(a0)<br> [0x80000690]:addi fp, zero, 0<br> [0x80000694]:csrrw zero, fcsr, fp<br> [0x80000698]:fclass.s t0, tp<br> [0x8000069c]:csrrs a1, fcsr, zero<br> [0x800006a0]:sd t0, 416(ra)<br> [0x800006a4]:sd a1, 424(ra)<br> [0x800006a8]:auipc t1, 2<br> [0x800006ac]:addi t1, t1, 3400<br> [0x800006b0]:ld t0, 32(a0)<br> [0x800006b4]:addi fp, zero, 0<br> [0x800006b8]:csrrw zero, fcsr, fp<br> [0x800006bc]:fclass.s tp, t0<br> [0x800006c0]:csrrs a1, fcsr, zero<br> [0x800006c4]:sd tp, 0(t1)<br> [0x800006c8]:sd a1, 8(t1)<br> |
|  20|[0x80002400]<br>0x0000000000000010<br> |- rs1 : x2<br> - rd : x3<br>                                                                                                                                        |[0x800006d8]:fclass.s gp, sp<br> [0x800006dc]:csrrs a1, fcsr, zero<br> [0x800006e0]:sd gp, 16(t1)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|  21|[0x80002410]<br>0x0000000000000010<br> |- rs1 : x3<br> - rd : x2<br>                                                                                                                                        |[0x800006f4]:fclass.s sp, gp<br> [0x800006f8]:csrrs a1, fcsr, zero<br> [0x800006fc]:sd sp, 32(t1)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|  22|[0x80002420]<br>0x0000000000000010<br> |- rs1 : x0<br> - rd : x1<br>                                                                                                                                        |[0x80000710]:fclass.s ra, zero<br> [0x80000714]:csrrs a1, fcsr, zero<br> [0x80000718]:sd ra, 48(t1)<br>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
