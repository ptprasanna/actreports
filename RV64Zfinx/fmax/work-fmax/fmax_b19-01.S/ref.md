
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x8000d580')]      |
| SIG_REGION                | [('0x80012e10', '0x80016a30', '1924 dwords')]      |
| COV_LABELS                | fmax_b19      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/fmax/work-fmax/fmax_b19-01.S/ref.S    |
| Total Number of coverpoints| 1058     |
| Total Coverpoints Hit     | 1058      |
| Total Signature Updates   | 1922      |
| STAT1                     | 0      |
| STAT2                     | 1      |
| STAT3                     | 960     |
| STAT4                     | 961     |
| STAT5                     | 0     |

## Details for STAT2:

```
Op without unique coverpoint updates Signature
 -- Code Sequence:
      [0x8000d564]:fmax.s t6, t5, t4
      [0x8000d568]:csrrs a2, fcsr, zero
      [0x8000d56c]:sd t6, 624(fp)
      [0x8000d570]:sd a2, 632(fp)
      [0x8000d574]:addi zero, zero, 0
      [0x8000d578]:addi zero, zero, 0
      [0x8000d57c]:addi zero, zero, 0
 -- Signature Addresses:
      Address: 0x80016a18 Data: 0x000000000030E1AE
 -- Redundant Coverpoints hit by the op
      - mnemonic : fmax.s
      - rs1 : x30
      - rs2 : x29
      - rd : x31
      - rs1 != rs2  and rs1 != rd and rs2 != rd
      - fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat






```

## Details of STAT3

```
[0x800003bc]:fmax.s t6, t5, t6
[0x800003c0]:csrrs tp, fcsr, zero
[0x800003c4]:sd t6, 0(ra)
[0x800003c8]:sd tp, 8(ra)
[0x800003cc]:ld t4, 16(gp)
[0x800003d0]:ld t4, 24(gp)
[0x800003d4]:addi sp, zero, 0
[0x800003d8]:csrrw zero, fcsr, sp
[0x800003dc]:fmax.s t4, t4, t4

[0x800003dc]:fmax.s t4, t4, t4
[0x800003e0]:csrrs tp, fcsr, zero
[0x800003e4]:sd t4, 16(ra)
[0x800003e8]:sd tp, 24(ra)
[0x800003ec]:ld t6, 32(gp)
[0x800003f0]:ld t3, 40(gp)
[0x800003f4]:addi sp, zero, 0
[0x800003f8]:csrrw zero, fcsr, sp
[0x800003fc]:fmax.s t5, t6, t3

[0x800003fc]:fmax.s t5, t6, t3
[0x80000400]:csrrs tp, fcsr, zero
[0x80000404]:sd t5, 32(ra)
[0x80000408]:sd tp, 40(ra)
[0x8000040c]:ld t3, 48(gp)
[0x80000410]:ld t5, 56(gp)
[0x80000414]:addi sp, zero, 0
[0x80000418]:csrrw zero, fcsr, sp
[0x8000041c]:fmax.s t3, t3, t5

[0x8000041c]:fmax.s t3, t3, t5
[0x80000420]:csrrs tp, fcsr, zero
[0x80000424]:sd t3, 48(ra)
[0x80000428]:sd tp, 56(ra)
[0x8000042c]:ld s10, 64(gp)
[0x80000430]:ld s10, 72(gp)
[0x80000434]:addi sp, zero, 0
[0x80000438]:csrrw zero, fcsr, sp
[0x8000043c]:fmax.s s11, s10, s10

[0x8000043c]:fmax.s s11, s10, s10
[0x80000440]:csrrs tp, fcsr, zero
[0x80000444]:sd s11, 64(ra)
[0x80000448]:sd tp, 72(ra)
[0x8000044c]:ld s11, 80(gp)
[0x80000450]:ld s9, 88(gp)
[0x80000454]:addi sp, zero, 0
[0x80000458]:csrrw zero, fcsr, sp
[0x8000045c]:fmax.s s10, s11, s9

[0x8000045c]:fmax.s s10, s11, s9
[0x80000460]:csrrs tp, fcsr, zero
[0x80000464]:sd s10, 80(ra)
[0x80000468]:sd tp, 88(ra)
[0x8000046c]:ld s8, 96(gp)
[0x80000470]:ld s11, 104(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fmax.s s9, s8, s11

[0x8000047c]:fmax.s s9, s8, s11
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s9, 96(ra)
[0x80000488]:sd tp, 104(ra)
[0x8000048c]:ld s9, 112(gp)
[0x80000490]:ld s7, 120(gp)
[0x80000494]:addi sp, zero, 0
[0x80000498]:csrrw zero, fcsr, sp
[0x8000049c]:fmax.s s8, s9, s7

[0x8000049c]:fmax.s s8, s9, s7
[0x800004a0]:csrrs tp, fcsr, zero
[0x800004a4]:sd s8, 112(ra)
[0x800004a8]:sd tp, 120(ra)
[0x800004ac]:ld s6, 128(gp)
[0x800004b0]:ld s8, 136(gp)
[0x800004b4]:addi sp, zero, 0
[0x800004b8]:csrrw zero, fcsr, sp
[0x800004bc]:fmax.s s7, s6, s8

[0x800004bc]:fmax.s s7, s6, s8
[0x800004c0]:csrrs tp, fcsr, zero
[0x800004c4]:sd s7, 128(ra)
[0x800004c8]:sd tp, 136(ra)
[0x800004cc]:ld s7, 144(gp)
[0x800004d0]:ld s5, 152(gp)
[0x800004d4]:addi sp, zero, 0
[0x800004d8]:csrrw zero, fcsr, sp
[0x800004dc]:fmax.s s6, s7, s5

[0x800004dc]:fmax.s s6, s7, s5
[0x800004e0]:csrrs tp, fcsr, zero
[0x800004e4]:sd s6, 144(ra)
[0x800004e8]:sd tp, 152(ra)
[0x800004ec]:ld s4, 160(gp)
[0x800004f0]:ld s6, 168(gp)
[0x800004f4]:addi sp, zero, 0
[0x800004f8]:csrrw zero, fcsr, sp
[0x800004fc]:fmax.s s5, s4, s6

[0x800004fc]:fmax.s s5, s4, s6
[0x80000500]:csrrs tp, fcsr, zero
[0x80000504]:sd s5, 160(ra)
[0x80000508]:sd tp, 168(ra)
[0x8000050c]:ld s5, 176(gp)
[0x80000510]:ld s3, 184(gp)
[0x80000514]:addi sp, zero, 0
[0x80000518]:csrrw zero, fcsr, sp
[0x8000051c]:fmax.s s4, s5, s3

[0x8000051c]:fmax.s s4, s5, s3
[0x80000520]:csrrs tp, fcsr, zero
[0x80000524]:sd s4, 176(ra)
[0x80000528]:sd tp, 184(ra)
[0x8000052c]:ld s2, 192(gp)
[0x80000530]:ld s4, 200(gp)
[0x80000534]:addi sp, zero, 0
[0x80000538]:csrrw zero, fcsr, sp
[0x8000053c]:fmax.s s3, s2, s4

[0x8000053c]:fmax.s s3, s2, s4
[0x80000540]:csrrs tp, fcsr, zero
[0x80000544]:sd s3, 192(ra)
[0x80000548]:sd tp, 200(ra)
[0x8000054c]:ld s3, 208(gp)
[0x80000550]:ld a7, 216(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fmax.s s2, s3, a7

[0x8000055c]:fmax.s s2, s3, a7
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd s2, 208(ra)
[0x80000568]:sd tp, 216(ra)
[0x8000056c]:ld a6, 224(gp)
[0x80000570]:ld s2, 232(gp)
[0x80000574]:addi sp, zero, 0
[0x80000578]:csrrw zero, fcsr, sp
[0x8000057c]:fmax.s a7, a6, s2

[0x8000057c]:fmax.s a7, a6, s2
[0x80000580]:csrrs tp, fcsr, zero
[0x80000584]:sd a7, 224(ra)
[0x80000588]:sd tp, 232(ra)
[0x8000058c]:ld a7, 240(gp)
[0x80000590]:ld a5, 248(gp)
[0x80000594]:addi sp, zero, 0
[0x80000598]:csrrw zero, fcsr, sp
[0x8000059c]:fmax.s a6, a7, a5

[0x8000059c]:fmax.s a6, a7, a5
[0x800005a0]:csrrs tp, fcsr, zero
[0x800005a4]:sd a6, 240(ra)
[0x800005a8]:sd tp, 248(ra)
[0x800005ac]:ld a4, 256(gp)
[0x800005b0]:ld a6, 264(gp)
[0x800005b4]:addi sp, zero, 0
[0x800005b8]:csrrw zero, fcsr, sp
[0x800005bc]:fmax.s a5, a4, a6

[0x800005bc]:fmax.s a5, a4, a6
[0x800005c0]:csrrs tp, fcsr, zero
[0x800005c4]:sd a5, 256(ra)
[0x800005c8]:sd tp, 264(ra)
[0x800005cc]:ld a5, 272(gp)
[0x800005d0]:ld a3, 280(gp)
[0x800005d4]:addi sp, zero, 0
[0x800005d8]:csrrw zero, fcsr, sp
[0x800005dc]:fmax.s a4, a5, a3

[0x800005dc]:fmax.s a4, a5, a3
[0x800005e0]:csrrs tp, fcsr, zero
[0x800005e4]:sd a4, 272(ra)
[0x800005e8]:sd tp, 280(ra)
[0x800005ec]:ld a2, 288(gp)
[0x800005f0]:ld a4, 296(gp)
[0x800005f4]:addi sp, zero, 0
[0x800005f8]:csrrw zero, fcsr, sp
[0x800005fc]:fmax.s a3, a2, a4

[0x800005fc]:fmax.s a3, a2, a4
[0x80000600]:csrrs tp, fcsr, zero
[0x80000604]:sd a3, 288(ra)
[0x80000608]:sd tp, 296(ra)
[0x8000060c]:ld a3, 304(gp)
[0x80000610]:ld a1, 312(gp)
[0x80000614]:addi sp, zero, 0
[0x80000618]:csrrw zero, fcsr, sp
[0x8000061c]:fmax.s a2, a3, a1

[0x8000061c]:fmax.s a2, a3, a1
[0x80000620]:csrrs tp, fcsr, zero
[0x80000624]:sd a2, 304(ra)
[0x80000628]:sd tp, 312(ra)
[0x8000062c]:ld a0, 320(gp)
[0x80000630]:ld a2, 328(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fmax.s a1, a0, a2

[0x8000063c]:fmax.s a1, a0, a2
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd a1, 320(ra)
[0x80000648]:sd tp, 328(ra)
[0x8000064c]:ld a1, 336(gp)
[0x80000650]:ld s1, 344(gp)
[0x80000654]:addi sp, zero, 0
[0x80000658]:csrrw zero, fcsr, sp
[0x8000065c]:fmax.s a0, a1, s1

[0x8000065c]:fmax.s a0, a1, s1
[0x80000660]:csrrs tp, fcsr, zero
[0x80000664]:sd a0, 336(ra)
[0x80000668]:sd tp, 344(ra)
[0x8000066c]:auipc a1, 15
[0x80000670]:addi a1, a1, 2820
[0x80000674]:ld fp, 0(a1)
[0x80000678]:ld a0, 8(a1)
[0x8000067c]:addi sp, zero, 0
[0x80000680]:csrrw zero, fcsr, sp
[0x80000684]:fmax.s s1, fp, a0

[0x80000684]:fmax.s s1, fp, a0
[0x80000688]:csrrs a2, fcsr, zero
[0x8000068c]:sd s1, 352(ra)
[0x80000690]:sd a2, 360(ra)
[0x80000694]:ld s1, 16(a1)
[0x80000698]:ld t2, 24(a1)
[0x8000069c]:addi sp, zero, 0
[0x800006a0]:csrrw zero, fcsr, sp
[0x800006a4]:fmax.s fp, s1, t2

[0x800006a4]:fmax.s fp, s1, t2
[0x800006a8]:csrrs a2, fcsr, zero
[0x800006ac]:sd fp, 368(ra)
[0x800006b0]:sd a2, 376(ra)
[0x800006b4]:ld t1, 32(a1)
[0x800006b8]:ld fp, 40(a1)
[0x800006bc]:addi s1, zero, 0
[0x800006c0]:csrrw zero, fcsr, s1
[0x800006c4]:fmax.s t2, t1, fp

[0x800006c4]:fmax.s t2, t1, fp
[0x800006c8]:csrrs a2, fcsr, zero
[0x800006cc]:sd t2, 384(ra)
[0x800006d0]:sd a2, 392(ra)
[0x800006d4]:auipc fp, 19
[0x800006d8]:addi fp, fp, 2260
[0x800006dc]:ld t2, 48(a1)
[0x800006e0]:ld t0, 56(a1)
[0x800006e4]:addi s1, zero, 0
[0x800006e8]:csrrw zero, fcsr, s1
[0x800006ec]:fmax.s t1, t2, t0

[0x800006ec]:fmax.s t1, t2, t0
[0x800006f0]:csrrs a2, fcsr, zero
[0x800006f4]:sd t1, 0(fp)
[0x800006f8]:sd a2, 8(fp)
[0x800006fc]:ld tp, 64(a1)
[0x80000700]:ld t1, 72(a1)
[0x80000704]:addi s1, zero, 0
[0x80000708]:csrrw zero, fcsr, s1
[0x8000070c]:fmax.s t0, tp, t1

[0x8000070c]:fmax.s t0, tp, t1
[0x80000710]:csrrs a2, fcsr, zero
[0x80000714]:sd t0, 16(fp)
[0x80000718]:sd a2, 24(fp)
[0x8000071c]:ld t0, 80(a1)
[0x80000720]:ld gp, 88(a1)
[0x80000724]:addi s1, zero, 0
[0x80000728]:csrrw zero, fcsr, s1
[0x8000072c]:fmax.s tp, t0, gp

[0x8000072c]:fmax.s tp, t0, gp
[0x80000730]:csrrs a2, fcsr, zero
[0x80000734]:sd tp, 32(fp)
[0x80000738]:sd a2, 40(fp)
[0x8000073c]:ld sp, 96(a1)
[0x80000740]:ld tp, 104(a1)
[0x80000744]:addi s1, zero, 0
[0x80000748]:csrrw zero, fcsr, s1
[0x8000074c]:fmax.s gp, sp, tp

[0x8000074c]:fmax.s gp, sp, tp
[0x80000750]:csrrs a2, fcsr, zero
[0x80000754]:sd gp, 48(fp)
[0x80000758]:sd a2, 56(fp)
[0x8000075c]:ld gp, 112(a1)
[0x80000760]:ld ra, 120(a1)
[0x80000764]:addi s1, zero, 0
[0x80000768]:csrrw zero, fcsr, s1
[0x8000076c]:fmax.s sp, gp, ra

[0x8000076c]:fmax.s sp, gp, ra
[0x80000770]:csrrs a2, fcsr, zero
[0x80000774]:sd sp, 64(fp)
[0x80000778]:sd a2, 72(fp)
[0x8000077c]:ld zero, 128(a1)
[0x80000780]:ld sp, 136(a1)
[0x80000784]:addi s1, zero, 0
[0x80000788]:csrrw zero, fcsr, s1
[0x8000078c]:fmax.s ra, zero, sp

[0x8000078c]:fmax.s ra, zero, sp
[0x80000790]:csrrs a2, fcsr, zero
[0x80000794]:sd ra, 80(fp)
[0x80000798]:sd a2, 88(fp)
[0x8000079c]:ld ra, 144(a1)
[0x800007a0]:ld t5, 152(a1)
[0x800007a4]:addi s1, zero, 0
[0x800007a8]:csrrw zero, fcsr, s1
[0x800007ac]:fmax.s t6, ra, t5

[0x800007ac]:fmax.s t6, ra, t5
[0x800007b0]:csrrs a2, fcsr, zero
[0x800007b4]:sd t6, 96(fp)
[0x800007b8]:sd a2, 104(fp)
[0x800007bc]:ld t5, 160(a1)
[0x800007c0]:ld zero, 168(a1)
[0x800007c4]:addi s1, zero, 0
[0x800007c8]:csrrw zero, fcsr, s1
[0x800007cc]:fmax.s t6, t5, zero

[0x800007cc]:fmax.s t6, t5, zero
[0x800007d0]:csrrs a2, fcsr, zero
[0x800007d4]:sd t6, 112(fp)
[0x800007d8]:sd a2, 120(fp)
[0x800007dc]:ld t6, 176(a1)
[0x800007e0]:ld t5, 184(a1)
[0x800007e4]:addi s1, zero, 0
[0x800007e8]:csrrw zero, fcsr, s1
[0x800007ec]:fmax.s zero, t6, t5

[0x800007ec]:fmax.s zero, t6, t5
[0x800007f0]:csrrs a2, fcsr, zero
[0x800007f4]:sd zero, 128(fp)
[0x800007f8]:sd a2, 136(fp)
[0x800007fc]:ld t5, 192(a1)
[0x80000800]:ld t4, 200(a1)
[0x80000804]:addi s1, zero, 0
[0x80000808]:csrrw zero, fcsr, s1
[0x8000080c]:fmax.s t6, t5, t4

[0x8000080c]:fmax.s t6, t5, t4
[0x80000810]:csrrs a2, fcsr, zero
[0x80000814]:sd t6, 144(fp)
[0x80000818]:sd a2, 152(fp)
[0x8000081c]:ld t5, 208(a1)
[0x80000820]:ld t4, 216(a1)
[0x80000824]:addi s1, zero, 0
[0x80000828]:csrrw zero, fcsr, s1
[0x8000082c]:fmax.s t6, t5, t4

[0x8000082c]:fmax.s t6, t5, t4
[0x80000830]:csrrs a2, fcsr, zero
[0x80000834]:sd t6, 160(fp)
[0x80000838]:sd a2, 168(fp)
[0x8000083c]:ld t5, 224(a1)
[0x80000840]:ld t4, 232(a1)
[0x80000844]:addi s1, zero, 0
[0x80000848]:csrrw zero, fcsr, s1
[0x8000084c]:fmax.s t6, t5, t4

[0x8000084c]:fmax.s t6, t5, t4
[0x80000850]:csrrs a2, fcsr, zero
[0x80000854]:sd t6, 176(fp)
[0x80000858]:sd a2, 184(fp)
[0x8000085c]:ld t5, 240(a1)
[0x80000860]:ld t4, 248(a1)
[0x80000864]:addi s1, zero, 0
[0x80000868]:csrrw zero, fcsr, s1
[0x8000086c]:fmax.s t6, t5, t4

[0x8000086c]:fmax.s t6, t5, t4
[0x80000870]:csrrs a2, fcsr, zero
[0x80000874]:sd t6, 192(fp)
[0x80000878]:sd a2, 200(fp)
[0x8000087c]:ld t5, 256(a1)
[0x80000880]:ld t4, 264(a1)
[0x80000884]:addi s1, zero, 0
[0x80000888]:csrrw zero, fcsr, s1
[0x8000088c]:fmax.s t6, t5, t4

[0x8000088c]:fmax.s t6, t5, t4
[0x80000890]:csrrs a2, fcsr, zero
[0x80000894]:sd t6, 208(fp)
[0x80000898]:sd a2, 216(fp)
[0x8000089c]:ld t5, 272(a1)
[0x800008a0]:ld t4, 280(a1)
[0x800008a4]:addi s1, zero, 0
[0x800008a8]:csrrw zero, fcsr, s1
[0x800008ac]:fmax.s t6, t5, t4

[0x800008ac]:fmax.s t6, t5, t4
[0x800008b0]:csrrs a2, fcsr, zero
[0x800008b4]:sd t6, 224(fp)
[0x800008b8]:sd a2, 232(fp)
[0x800008bc]:ld t5, 288(a1)
[0x800008c0]:ld t4, 296(a1)
[0x800008c4]:addi s1, zero, 0
[0x800008c8]:csrrw zero, fcsr, s1
[0x800008cc]:fmax.s t6, t5, t4

[0x800008cc]:fmax.s t6, t5, t4
[0x800008d0]:csrrs a2, fcsr, zero
[0x800008d4]:sd t6, 240(fp)
[0x800008d8]:sd a2, 248(fp)
[0x800008dc]:ld t5, 304(a1)
[0x800008e0]:ld t4, 312(a1)
[0x800008e4]:addi s1, zero, 0
[0x800008e8]:csrrw zero, fcsr, s1
[0x800008ec]:fmax.s t6, t5, t4

[0x800008ec]:fmax.s t6, t5, t4
[0x800008f0]:csrrs a2, fcsr, zero
[0x800008f4]:sd t6, 256(fp)
[0x800008f8]:sd a2, 264(fp)
[0x800008fc]:ld t5, 320(a1)
[0x80000900]:ld t4, 328(a1)
[0x80000904]:addi s1, zero, 0
[0x80000908]:csrrw zero, fcsr, s1
[0x8000090c]:fmax.s t6, t5, t4

[0x8000090c]:fmax.s t6, t5, t4
[0x80000910]:csrrs a2, fcsr, zero
[0x80000914]:sd t6, 272(fp)
[0x80000918]:sd a2, 280(fp)
[0x8000091c]:ld t5, 336(a1)
[0x80000920]:ld t4, 344(a1)
[0x80000924]:addi s1, zero, 0
[0x80000928]:csrrw zero, fcsr, s1
[0x8000092c]:fmax.s t6, t5, t4

[0x8000092c]:fmax.s t6, t5, t4
[0x80000930]:csrrs a2, fcsr, zero
[0x80000934]:sd t6, 288(fp)
[0x80000938]:sd a2, 296(fp)
[0x8000093c]:ld t5, 352(a1)
[0x80000940]:ld t4, 360(a1)
[0x80000944]:addi s1, zero, 0
[0x80000948]:csrrw zero, fcsr, s1
[0x8000094c]:fmax.s t6, t5, t4

[0x8000094c]:fmax.s t6, t5, t4
[0x80000950]:csrrs a2, fcsr, zero
[0x80000954]:sd t6, 304(fp)
[0x80000958]:sd a2, 312(fp)
[0x8000095c]:ld t5, 368(a1)
[0x80000960]:ld t4, 376(a1)
[0x80000964]:addi s1, zero, 0
[0x80000968]:csrrw zero, fcsr, s1
[0x8000096c]:fmax.s t6, t5, t4

[0x8000096c]:fmax.s t6, t5, t4
[0x80000970]:csrrs a2, fcsr, zero
[0x80000974]:sd t6, 320(fp)
[0x80000978]:sd a2, 328(fp)
[0x8000097c]:ld t5, 384(a1)
[0x80000980]:ld t4, 392(a1)
[0x80000984]:addi s1, zero, 0
[0x80000988]:csrrw zero, fcsr, s1
[0x8000098c]:fmax.s t6, t5, t4

[0x8000098c]:fmax.s t6, t5, t4
[0x80000990]:csrrs a2, fcsr, zero
[0x80000994]:sd t6, 336(fp)
[0x80000998]:sd a2, 344(fp)
[0x8000099c]:ld t5, 400(a1)
[0x800009a0]:ld t4, 408(a1)
[0x800009a4]:addi s1, zero, 0
[0x800009a8]:csrrw zero, fcsr, s1
[0x800009ac]:fmax.s t6, t5, t4

[0x800009ac]:fmax.s t6, t5, t4
[0x800009b0]:csrrs a2, fcsr, zero
[0x800009b4]:sd t6, 352(fp)
[0x800009b8]:sd a2, 360(fp)
[0x800009bc]:ld t5, 416(a1)
[0x800009c0]:ld t4, 424(a1)
[0x800009c4]:addi s1, zero, 0
[0x800009c8]:csrrw zero, fcsr, s1
[0x800009cc]:fmax.s t6, t5, t4

[0x800009cc]:fmax.s t6, t5, t4
[0x800009d0]:csrrs a2, fcsr, zero
[0x800009d4]:sd t6, 368(fp)
[0x800009d8]:sd a2, 376(fp)
[0x800009dc]:ld t5, 432(a1)
[0x800009e0]:ld t4, 440(a1)
[0x800009e4]:addi s1, zero, 0
[0x800009e8]:csrrw zero, fcsr, s1
[0x800009ec]:fmax.s t6, t5, t4

[0x800009ec]:fmax.s t6, t5, t4
[0x800009f0]:csrrs a2, fcsr, zero
[0x800009f4]:sd t6, 384(fp)
[0x800009f8]:sd a2, 392(fp)
[0x800009fc]:ld t5, 448(a1)
[0x80000a00]:ld t4, 456(a1)
[0x80000a04]:addi s1, zero, 0
[0x80000a08]:csrrw zero, fcsr, s1
[0x80000a0c]:fmax.s t6, t5, t4

[0x80000a0c]:fmax.s t6, t5, t4
[0x80000a10]:csrrs a2, fcsr, zero
[0x80000a14]:sd t6, 400(fp)
[0x80000a18]:sd a2, 408(fp)
[0x80000a1c]:ld t5, 464(a1)
[0x80000a20]:ld t4, 472(a1)
[0x80000a24]:addi s1, zero, 0
[0x80000a28]:csrrw zero, fcsr, s1
[0x80000a2c]:fmax.s t6, t5, t4

[0x80000a2c]:fmax.s t6, t5, t4
[0x80000a30]:csrrs a2, fcsr, zero
[0x80000a34]:sd t6, 416(fp)
[0x80000a38]:sd a2, 424(fp)
[0x80000a3c]:ld t5, 480(a1)
[0x80000a40]:ld t4, 488(a1)
[0x80000a44]:addi s1, zero, 0
[0x80000a48]:csrrw zero, fcsr, s1
[0x80000a4c]:fmax.s t6, t5, t4

[0x80000a4c]:fmax.s t6, t5, t4
[0x80000a50]:csrrs a2, fcsr, zero
[0x80000a54]:sd t6, 432(fp)
[0x80000a58]:sd a2, 440(fp)
[0x80000a5c]:ld t5, 496(a1)
[0x80000a60]:ld t4, 504(a1)
[0x80000a64]:addi s1, zero, 0
[0x80000a68]:csrrw zero, fcsr, s1
[0x80000a6c]:fmax.s t6, t5, t4

[0x80000a6c]:fmax.s t6, t5, t4
[0x80000a70]:csrrs a2, fcsr, zero
[0x80000a74]:sd t6, 448(fp)
[0x80000a78]:sd a2, 456(fp)
[0x80000a7c]:ld t5, 512(a1)
[0x80000a80]:ld t4, 520(a1)
[0x80000a84]:addi s1, zero, 0
[0x80000a88]:csrrw zero, fcsr, s1
[0x80000a8c]:fmax.s t6, t5, t4

[0x80000a8c]:fmax.s t6, t5, t4
[0x80000a90]:csrrs a2, fcsr, zero
[0x80000a94]:sd t6, 464(fp)
[0x80000a98]:sd a2, 472(fp)
[0x80000a9c]:ld t5, 528(a1)
[0x80000aa0]:ld t4, 536(a1)
[0x80000aa4]:addi s1, zero, 0
[0x80000aa8]:csrrw zero, fcsr, s1
[0x80000aac]:fmax.s t6, t5, t4

[0x80000aac]:fmax.s t6, t5, t4
[0x80000ab0]:csrrs a2, fcsr, zero
[0x80000ab4]:sd t6, 480(fp)
[0x80000ab8]:sd a2, 488(fp)
[0x80000abc]:ld t5, 544(a1)
[0x80000ac0]:ld t4, 552(a1)
[0x80000ac4]:addi s1, zero, 0
[0x80000ac8]:csrrw zero, fcsr, s1
[0x80000acc]:fmax.s t6, t5, t4

[0x80000acc]:fmax.s t6, t5, t4
[0x80000ad0]:csrrs a2, fcsr, zero
[0x80000ad4]:sd t6, 496(fp)
[0x80000ad8]:sd a2, 504(fp)
[0x80000adc]:ld t5, 560(a1)
[0x80000ae0]:ld t4, 568(a1)
[0x80000ae4]:addi s1, zero, 0
[0x80000ae8]:csrrw zero, fcsr, s1
[0x80000aec]:fmax.s t6, t5, t4

[0x80000aec]:fmax.s t6, t5, t4
[0x80000af0]:csrrs a2, fcsr, zero
[0x80000af4]:sd t6, 512(fp)
[0x80000af8]:sd a2, 520(fp)
[0x80000afc]:ld t5, 576(a1)
[0x80000b00]:ld t4, 584(a1)
[0x80000b04]:addi s1, zero, 0
[0x80000b08]:csrrw zero, fcsr, s1
[0x80000b0c]:fmax.s t6, t5, t4

[0x80000b0c]:fmax.s t6, t5, t4
[0x80000b10]:csrrs a2, fcsr, zero
[0x80000b14]:sd t6, 528(fp)
[0x80000b18]:sd a2, 536(fp)
[0x80000b1c]:ld t5, 592(a1)
[0x80000b20]:ld t4, 600(a1)
[0x80000b24]:addi s1, zero, 0
[0x80000b28]:csrrw zero, fcsr, s1
[0x80000b2c]:fmax.s t6, t5, t4

[0x80000b2c]:fmax.s t6, t5, t4
[0x80000b30]:csrrs a2, fcsr, zero
[0x80000b34]:sd t6, 544(fp)
[0x80000b38]:sd a2, 552(fp)
[0x80000b3c]:ld t5, 608(a1)
[0x80000b40]:ld t4, 616(a1)
[0x80000b44]:addi s1, zero, 0
[0x80000b48]:csrrw zero, fcsr, s1
[0x80000b4c]:fmax.s t6, t5, t4

[0x80000b4c]:fmax.s t6, t5, t4
[0x80000b50]:csrrs a2, fcsr, zero
[0x80000b54]:sd t6, 560(fp)
[0x80000b58]:sd a2, 568(fp)
[0x80000b5c]:ld t5, 624(a1)
[0x80000b60]:ld t4, 632(a1)
[0x80000b64]:addi s1, zero, 0
[0x80000b68]:csrrw zero, fcsr, s1
[0x80000b6c]:fmax.s t6, t5, t4

[0x80000b6c]:fmax.s t6, t5, t4
[0x80000b70]:csrrs a2, fcsr, zero
[0x80000b74]:sd t6, 576(fp)
[0x80000b78]:sd a2, 584(fp)
[0x80000b7c]:ld t5, 640(a1)
[0x80000b80]:ld t4, 648(a1)
[0x80000b84]:addi s1, zero, 0
[0x80000b88]:csrrw zero, fcsr, s1
[0x80000b8c]:fmax.s t6, t5, t4

[0x80000b8c]:fmax.s t6, t5, t4
[0x80000b90]:csrrs a2, fcsr, zero
[0x80000b94]:sd t6, 592(fp)
[0x80000b98]:sd a2, 600(fp)
[0x80000b9c]:ld t5, 656(a1)
[0x80000ba0]:ld t4, 664(a1)
[0x80000ba4]:addi s1, zero, 0
[0x80000ba8]:csrrw zero, fcsr, s1
[0x80000bac]:fmax.s t6, t5, t4

[0x80000bac]:fmax.s t6, t5, t4
[0x80000bb0]:csrrs a2, fcsr, zero
[0x80000bb4]:sd t6, 608(fp)
[0x80000bb8]:sd a2, 616(fp)
[0x80000bbc]:ld t5, 672(a1)
[0x80000bc0]:ld t4, 680(a1)
[0x80000bc4]:addi s1, zero, 0
[0x80000bc8]:csrrw zero, fcsr, s1
[0x80000bcc]:fmax.s t6, t5, t4

[0x80000bcc]:fmax.s t6, t5, t4
[0x80000bd0]:csrrs a2, fcsr, zero
[0x80000bd4]:sd t6, 624(fp)
[0x80000bd8]:sd a2, 632(fp)
[0x80000bdc]:ld t5, 688(a1)
[0x80000be0]:ld t4, 696(a1)
[0x80000be4]:addi s1, zero, 0
[0x80000be8]:csrrw zero, fcsr, s1
[0x80000bec]:fmax.s t6, t5, t4

[0x80000bec]:fmax.s t6, t5, t4
[0x80000bf0]:csrrs a2, fcsr, zero
[0x80000bf4]:sd t6, 640(fp)
[0x80000bf8]:sd a2, 648(fp)
[0x80000bfc]:ld t5, 704(a1)
[0x80000c00]:ld t4, 712(a1)
[0x80000c04]:addi s1, zero, 0
[0x80000c08]:csrrw zero, fcsr, s1
[0x80000c0c]:fmax.s t6, t5, t4

[0x80000c0c]:fmax.s t6, t5, t4
[0x80000c10]:csrrs a2, fcsr, zero
[0x80000c14]:sd t6, 656(fp)
[0x80000c18]:sd a2, 664(fp)
[0x80000c1c]:ld t5, 720(a1)
[0x80000c20]:ld t4, 728(a1)
[0x80000c24]:addi s1, zero, 0
[0x80000c28]:csrrw zero, fcsr, s1
[0x80000c2c]:fmax.s t6, t5, t4

[0x80000c2c]:fmax.s t6, t5, t4
[0x80000c30]:csrrs a2, fcsr, zero
[0x80000c34]:sd t6, 672(fp)
[0x80000c38]:sd a2, 680(fp)
[0x80000c3c]:ld t5, 736(a1)
[0x80000c40]:ld t4, 744(a1)
[0x80000c44]:addi s1, zero, 0
[0x80000c48]:csrrw zero, fcsr, s1
[0x80000c4c]:fmax.s t6, t5, t4

[0x80000c4c]:fmax.s t6, t5, t4
[0x80000c50]:csrrs a2, fcsr, zero
[0x80000c54]:sd t6, 688(fp)
[0x80000c58]:sd a2, 696(fp)
[0x80000c5c]:ld t5, 752(a1)
[0x80000c60]:ld t4, 760(a1)
[0x80000c64]:addi s1, zero, 0
[0x80000c68]:csrrw zero, fcsr, s1
[0x80000c6c]:fmax.s t6, t5, t4

[0x80000c6c]:fmax.s t6, t5, t4
[0x80000c70]:csrrs a2, fcsr, zero
[0x80000c74]:sd t6, 704(fp)
[0x80000c78]:sd a2, 712(fp)
[0x80000c7c]:ld t5, 768(a1)
[0x80000c80]:ld t4, 776(a1)
[0x80000c84]:addi s1, zero, 0
[0x80000c88]:csrrw zero, fcsr, s1
[0x80000c8c]:fmax.s t6, t5, t4

[0x80000c8c]:fmax.s t6, t5, t4
[0x80000c90]:csrrs a2, fcsr, zero
[0x80000c94]:sd t6, 720(fp)
[0x80000c98]:sd a2, 728(fp)
[0x80000c9c]:ld t5, 784(a1)
[0x80000ca0]:ld t4, 792(a1)
[0x80000ca4]:addi s1, zero, 0
[0x80000ca8]:csrrw zero, fcsr, s1
[0x80000cac]:fmax.s t6, t5, t4

[0x80000cac]:fmax.s t6, t5, t4
[0x80000cb0]:csrrs a2, fcsr, zero
[0x80000cb4]:sd t6, 736(fp)
[0x80000cb8]:sd a2, 744(fp)
[0x80000cbc]:ld t5, 800(a1)
[0x80000cc0]:ld t4, 808(a1)
[0x80000cc4]:addi s1, zero, 0
[0x80000cc8]:csrrw zero, fcsr, s1
[0x80000ccc]:fmax.s t6, t5, t4

[0x80000ccc]:fmax.s t6, t5, t4
[0x80000cd0]:csrrs a2, fcsr, zero
[0x80000cd4]:sd t6, 752(fp)
[0x80000cd8]:sd a2, 760(fp)
[0x80000cdc]:ld t5, 816(a1)
[0x80000ce0]:ld t4, 824(a1)
[0x80000ce4]:addi s1, zero, 0
[0x80000ce8]:csrrw zero, fcsr, s1
[0x80000cec]:fmax.s t6, t5, t4

[0x80000cec]:fmax.s t6, t5, t4
[0x80000cf0]:csrrs a2, fcsr, zero
[0x80000cf4]:sd t6, 768(fp)
[0x80000cf8]:sd a2, 776(fp)
[0x80000cfc]:ld t5, 832(a1)
[0x80000d00]:ld t4, 840(a1)
[0x80000d04]:addi s1, zero, 0
[0x80000d08]:csrrw zero, fcsr, s1
[0x80000d0c]:fmax.s t6, t5, t4

[0x80000d0c]:fmax.s t6, t5, t4
[0x80000d10]:csrrs a2, fcsr, zero
[0x80000d14]:sd t6, 784(fp)
[0x80000d18]:sd a2, 792(fp)
[0x80000d1c]:ld t5, 848(a1)
[0x80000d20]:ld t4, 856(a1)
[0x80000d24]:addi s1, zero, 0
[0x80000d28]:csrrw zero, fcsr, s1
[0x80000d2c]:fmax.s t6, t5, t4

[0x80000d2c]:fmax.s t6, t5, t4
[0x80000d30]:csrrs a2, fcsr, zero
[0x80000d34]:sd t6, 800(fp)
[0x80000d38]:sd a2, 808(fp)
[0x80000d3c]:ld t5, 864(a1)
[0x80000d40]:ld t4, 872(a1)
[0x80000d44]:addi s1, zero, 0
[0x80000d48]:csrrw zero, fcsr, s1
[0x80000d4c]:fmax.s t6, t5, t4

[0x80000d4c]:fmax.s t6, t5, t4
[0x80000d50]:csrrs a2, fcsr, zero
[0x80000d54]:sd t6, 816(fp)
[0x80000d58]:sd a2, 824(fp)
[0x80000d5c]:ld t5, 880(a1)
[0x80000d60]:ld t4, 888(a1)
[0x80000d64]:addi s1, zero, 0
[0x80000d68]:csrrw zero, fcsr, s1
[0x80000d6c]:fmax.s t6, t5, t4

[0x80000d6c]:fmax.s t6, t5, t4
[0x80000d70]:csrrs a2, fcsr, zero
[0x80000d74]:sd t6, 832(fp)
[0x80000d78]:sd a2, 840(fp)
[0x80000d7c]:ld t5, 896(a1)
[0x80000d80]:ld t4, 904(a1)
[0x80000d84]:addi s1, zero, 0
[0x80000d88]:csrrw zero, fcsr, s1
[0x80000d8c]:fmax.s t6, t5, t4

[0x80000d8c]:fmax.s t6, t5, t4
[0x80000d90]:csrrs a2, fcsr, zero
[0x80000d94]:sd t6, 848(fp)
[0x80000d98]:sd a2, 856(fp)
[0x80000d9c]:ld t5, 912(a1)
[0x80000da0]:ld t4, 920(a1)
[0x80000da4]:addi s1, zero, 0
[0x80000da8]:csrrw zero, fcsr, s1
[0x80000dac]:fmax.s t6, t5, t4

[0x80000dac]:fmax.s t6, t5, t4
[0x80000db0]:csrrs a2, fcsr, zero
[0x80000db4]:sd t6, 864(fp)
[0x80000db8]:sd a2, 872(fp)
[0x80000dbc]:ld t5, 928(a1)
[0x80000dc0]:ld t4, 936(a1)
[0x80000dc4]:addi s1, zero, 0
[0x80000dc8]:csrrw zero, fcsr, s1
[0x80000dcc]:fmax.s t6, t5, t4

[0x80000dcc]:fmax.s t6, t5, t4
[0x80000dd0]:csrrs a2, fcsr, zero
[0x80000dd4]:sd t6, 880(fp)
[0x80000dd8]:sd a2, 888(fp)
[0x80000ddc]:ld t5, 944(a1)
[0x80000de0]:ld t4, 952(a1)
[0x80000de4]:addi s1, zero, 0
[0x80000de8]:csrrw zero, fcsr, s1
[0x80000dec]:fmax.s t6, t5, t4

[0x80000dec]:fmax.s t6, t5, t4
[0x80000df0]:csrrs a2, fcsr, zero
[0x80000df4]:sd t6, 896(fp)
[0x80000df8]:sd a2, 904(fp)
[0x80000dfc]:ld t5, 960(a1)
[0x80000e00]:ld t4, 968(a1)
[0x80000e04]:addi s1, zero, 0
[0x80000e08]:csrrw zero, fcsr, s1
[0x80000e0c]:fmax.s t6, t5, t4

[0x80000e0c]:fmax.s t6, t5, t4
[0x80000e10]:csrrs a2, fcsr, zero
[0x80000e14]:sd t6, 912(fp)
[0x80000e18]:sd a2, 920(fp)
[0x80000e1c]:ld t5, 976(a1)
[0x80000e20]:ld t4, 984(a1)
[0x80000e24]:addi s1, zero, 0
[0x80000e28]:csrrw zero, fcsr, s1
[0x80000e2c]:fmax.s t6, t5, t4

[0x80000e2c]:fmax.s t6, t5, t4
[0x80000e30]:csrrs a2, fcsr, zero
[0x80000e34]:sd t6, 928(fp)
[0x80000e38]:sd a2, 936(fp)
[0x80000e3c]:ld t5, 992(a1)
[0x80000e40]:ld t4, 1000(a1)
[0x80000e44]:addi s1, zero, 0
[0x80000e48]:csrrw zero, fcsr, s1
[0x80000e4c]:fmax.s t6, t5, t4

[0x80000e4c]:fmax.s t6, t5, t4
[0x80000e50]:csrrs a2, fcsr, zero
[0x80000e54]:sd t6, 944(fp)
[0x80000e58]:sd a2, 952(fp)
[0x80000e5c]:ld t5, 1008(a1)
[0x80000e60]:ld t4, 1016(a1)
[0x80000e64]:addi s1, zero, 0
[0x80000e68]:csrrw zero, fcsr, s1
[0x80000e6c]:fmax.s t6, t5, t4

[0x80000e6c]:fmax.s t6, t5, t4
[0x80000e70]:csrrs a2, fcsr, zero
[0x80000e74]:sd t6, 960(fp)
[0x80000e78]:sd a2, 968(fp)
[0x80000e7c]:ld t5, 1024(a1)
[0x80000e80]:ld t4, 1032(a1)
[0x80000e84]:addi s1, zero, 0
[0x80000e88]:csrrw zero, fcsr, s1
[0x80000e8c]:fmax.s t6, t5, t4

[0x80000e8c]:fmax.s t6, t5, t4
[0x80000e90]:csrrs a2, fcsr, zero
[0x80000e94]:sd t6, 976(fp)
[0x80000e98]:sd a2, 984(fp)
[0x80000e9c]:ld t5, 1040(a1)
[0x80000ea0]:ld t4, 1048(a1)
[0x80000ea4]:addi s1, zero, 0
[0x80000ea8]:csrrw zero, fcsr, s1
[0x80000eac]:fmax.s t6, t5, t4

[0x80000eac]:fmax.s t6, t5, t4
[0x80000eb0]:csrrs a2, fcsr, zero
[0x80000eb4]:sd t6, 992(fp)
[0x80000eb8]:sd a2, 1000(fp)
[0x80000ebc]:ld t5, 1056(a1)
[0x80000ec0]:ld t4, 1064(a1)
[0x80000ec4]:addi s1, zero, 0
[0x80000ec8]:csrrw zero, fcsr, s1
[0x80000ecc]:fmax.s t6, t5, t4

[0x80000ecc]:fmax.s t6, t5, t4
[0x80000ed0]:csrrs a2, fcsr, zero
[0x80000ed4]:sd t6, 1008(fp)
[0x80000ed8]:sd a2, 1016(fp)
[0x80000edc]:ld t5, 1072(a1)
[0x80000ee0]:ld t4, 1080(a1)
[0x80000ee4]:addi s1, zero, 0
[0x80000ee8]:csrrw zero, fcsr, s1
[0x80000eec]:fmax.s t6, t5, t4

[0x80000eec]:fmax.s t6, t5, t4
[0x80000ef0]:csrrs a2, fcsr, zero
[0x80000ef4]:sd t6, 1024(fp)
[0x80000ef8]:sd a2, 1032(fp)
[0x80000efc]:ld t5, 1088(a1)
[0x80000f00]:ld t4, 1096(a1)
[0x80000f04]:addi s1, zero, 0
[0x80000f08]:csrrw zero, fcsr, s1
[0x80000f0c]:fmax.s t6, t5, t4

[0x80000f0c]:fmax.s t6, t5, t4
[0x80000f10]:csrrs a2, fcsr, zero
[0x80000f14]:sd t6, 1040(fp)
[0x80000f18]:sd a2, 1048(fp)
[0x80000f1c]:ld t5, 1104(a1)
[0x80000f20]:ld t4, 1112(a1)
[0x80000f24]:addi s1, zero, 0
[0x80000f28]:csrrw zero, fcsr, s1
[0x80000f2c]:fmax.s t6, t5, t4

[0x80000f2c]:fmax.s t6, t5, t4
[0x80000f30]:csrrs a2, fcsr, zero
[0x80000f34]:sd t6, 1056(fp)
[0x80000f38]:sd a2, 1064(fp)
[0x80000f3c]:ld t5, 1120(a1)
[0x80000f40]:ld t4, 1128(a1)
[0x80000f44]:addi s1, zero, 0
[0x80000f48]:csrrw zero, fcsr, s1
[0x80000f4c]:fmax.s t6, t5, t4

[0x80000f4c]:fmax.s t6, t5, t4
[0x80000f50]:csrrs a2, fcsr, zero
[0x80000f54]:sd t6, 1072(fp)
[0x80000f58]:sd a2, 1080(fp)
[0x80000f5c]:ld t5, 1136(a1)
[0x80000f60]:ld t4, 1144(a1)
[0x80000f64]:addi s1, zero, 0
[0x80000f68]:csrrw zero, fcsr, s1
[0x80000f6c]:fmax.s t6, t5, t4

[0x80000f6c]:fmax.s t6, t5, t4
[0x80000f70]:csrrs a2, fcsr, zero
[0x80000f74]:sd t6, 1088(fp)
[0x80000f78]:sd a2, 1096(fp)
[0x80000f7c]:ld t5, 1152(a1)
[0x80000f80]:ld t4, 1160(a1)
[0x80000f84]:addi s1, zero, 0
[0x80000f88]:csrrw zero, fcsr, s1
[0x80000f8c]:fmax.s t6, t5, t4

[0x80000f8c]:fmax.s t6, t5, t4
[0x80000f90]:csrrs a2, fcsr, zero
[0x80000f94]:sd t6, 1104(fp)
[0x80000f98]:sd a2, 1112(fp)
[0x80000f9c]:ld t5, 1168(a1)
[0x80000fa0]:ld t4, 1176(a1)
[0x80000fa4]:addi s1, zero, 0
[0x80000fa8]:csrrw zero, fcsr, s1
[0x80000fac]:fmax.s t6, t5, t4

[0x80000fac]:fmax.s t6, t5, t4
[0x80000fb0]:csrrs a2, fcsr, zero
[0x80000fb4]:sd t6, 1120(fp)
[0x80000fb8]:sd a2, 1128(fp)
[0x80000fbc]:ld t5, 1184(a1)
[0x80000fc0]:ld t4, 1192(a1)
[0x80000fc4]:addi s1, zero, 0
[0x80000fc8]:csrrw zero, fcsr, s1
[0x80000fcc]:fmax.s t6, t5, t4

[0x80000fcc]:fmax.s t6, t5, t4
[0x80000fd0]:csrrs a2, fcsr, zero
[0x80000fd4]:sd t6, 1136(fp)
[0x80000fd8]:sd a2, 1144(fp)
[0x80000fdc]:ld t5, 1200(a1)
[0x80000fe0]:ld t4, 1208(a1)
[0x80000fe4]:addi s1, zero, 0
[0x80000fe8]:csrrw zero, fcsr, s1
[0x80000fec]:fmax.s t6, t5, t4

[0x80000fec]:fmax.s t6, t5, t4
[0x80000ff0]:csrrs a2, fcsr, zero
[0x80000ff4]:sd t6, 1152(fp)
[0x80000ff8]:sd a2, 1160(fp)
[0x80000ffc]:ld t5, 1216(a1)
[0x80001000]:ld t4, 1224(a1)
[0x80001004]:addi s1, zero, 0
[0x80001008]:csrrw zero, fcsr, s1
[0x8000100c]:fmax.s t6, t5, t4

[0x8000100c]:fmax.s t6, t5, t4
[0x80001010]:csrrs a2, fcsr, zero
[0x80001014]:sd t6, 1168(fp)
[0x80001018]:sd a2, 1176(fp)
[0x8000101c]:ld t5, 1232(a1)
[0x80001020]:ld t4, 1240(a1)
[0x80001024]:addi s1, zero, 0
[0x80001028]:csrrw zero, fcsr, s1
[0x8000102c]:fmax.s t6, t5, t4

[0x8000102c]:fmax.s t6, t5, t4
[0x80001030]:csrrs a2, fcsr, zero
[0x80001034]:sd t6, 1184(fp)
[0x80001038]:sd a2, 1192(fp)
[0x8000103c]:ld t5, 1248(a1)
[0x80001040]:ld t4, 1256(a1)
[0x80001044]:addi s1, zero, 0
[0x80001048]:csrrw zero, fcsr, s1
[0x8000104c]:fmax.s t6, t5, t4

[0x8000104c]:fmax.s t6, t5, t4
[0x80001050]:csrrs a2, fcsr, zero
[0x80001054]:sd t6, 1200(fp)
[0x80001058]:sd a2, 1208(fp)
[0x8000105c]:ld t5, 1264(a1)
[0x80001060]:ld t4, 1272(a1)
[0x80001064]:addi s1, zero, 0
[0x80001068]:csrrw zero, fcsr, s1
[0x8000106c]:fmax.s t6, t5, t4

[0x8000106c]:fmax.s t6, t5, t4
[0x80001070]:csrrs a2, fcsr, zero
[0x80001074]:sd t6, 1216(fp)
[0x80001078]:sd a2, 1224(fp)
[0x8000107c]:ld t5, 1280(a1)
[0x80001080]:ld t4, 1288(a1)
[0x80001084]:addi s1, zero, 0
[0x80001088]:csrrw zero, fcsr, s1
[0x8000108c]:fmax.s t6, t5, t4

[0x8000108c]:fmax.s t6, t5, t4
[0x80001090]:csrrs a2, fcsr, zero
[0x80001094]:sd t6, 1232(fp)
[0x80001098]:sd a2, 1240(fp)
[0x8000109c]:ld t5, 1296(a1)
[0x800010a0]:ld t4, 1304(a1)
[0x800010a4]:addi s1, zero, 0
[0x800010a8]:csrrw zero, fcsr, s1
[0x800010ac]:fmax.s t6, t5, t4

[0x800010ac]:fmax.s t6, t5, t4
[0x800010b0]:csrrs a2, fcsr, zero
[0x800010b4]:sd t6, 1248(fp)
[0x800010b8]:sd a2, 1256(fp)
[0x800010bc]:ld t5, 1312(a1)
[0x800010c0]:ld t4, 1320(a1)
[0x800010c4]:addi s1, zero, 0
[0x800010c8]:csrrw zero, fcsr, s1
[0x800010cc]:fmax.s t6, t5, t4

[0x800010cc]:fmax.s t6, t5, t4
[0x800010d0]:csrrs a2, fcsr, zero
[0x800010d4]:sd t6, 1264(fp)
[0x800010d8]:sd a2, 1272(fp)
[0x800010dc]:ld t5, 1328(a1)
[0x800010e0]:ld t4, 1336(a1)
[0x800010e4]:addi s1, zero, 0
[0x800010e8]:csrrw zero, fcsr, s1
[0x800010ec]:fmax.s t6, t5, t4

[0x800010ec]:fmax.s t6, t5, t4
[0x800010f0]:csrrs a2, fcsr, zero
[0x800010f4]:sd t6, 1280(fp)
[0x800010f8]:sd a2, 1288(fp)
[0x800010fc]:ld t5, 1344(a1)
[0x80001100]:ld t4, 1352(a1)
[0x80001104]:addi s1, zero, 0
[0x80001108]:csrrw zero, fcsr, s1
[0x8000110c]:fmax.s t6, t5, t4

[0x8000110c]:fmax.s t6, t5, t4
[0x80001110]:csrrs a2, fcsr, zero
[0x80001114]:sd t6, 1296(fp)
[0x80001118]:sd a2, 1304(fp)
[0x8000111c]:ld t5, 1360(a1)
[0x80001120]:ld t4, 1368(a1)
[0x80001124]:addi s1, zero, 0
[0x80001128]:csrrw zero, fcsr, s1
[0x8000112c]:fmax.s t6, t5, t4

[0x8000112c]:fmax.s t6, t5, t4
[0x80001130]:csrrs a2, fcsr, zero
[0x80001134]:sd t6, 1312(fp)
[0x80001138]:sd a2, 1320(fp)
[0x8000113c]:ld t5, 1376(a1)
[0x80001140]:ld t4, 1384(a1)
[0x80001144]:addi s1, zero, 0
[0x80001148]:csrrw zero, fcsr, s1
[0x8000114c]:fmax.s t6, t5, t4

[0x8000114c]:fmax.s t6, t5, t4
[0x80001150]:csrrs a2, fcsr, zero
[0x80001154]:sd t6, 1328(fp)
[0x80001158]:sd a2, 1336(fp)
[0x8000115c]:ld t5, 1392(a1)
[0x80001160]:ld t4, 1400(a1)
[0x80001164]:addi s1, zero, 0
[0x80001168]:csrrw zero, fcsr, s1
[0x8000116c]:fmax.s t6, t5, t4

[0x8000116c]:fmax.s t6, t5, t4
[0x80001170]:csrrs a2, fcsr, zero
[0x80001174]:sd t6, 1344(fp)
[0x80001178]:sd a2, 1352(fp)
[0x8000117c]:ld t5, 1408(a1)
[0x80001180]:ld t4, 1416(a1)
[0x80001184]:addi s1, zero, 0
[0x80001188]:csrrw zero, fcsr, s1
[0x8000118c]:fmax.s t6, t5, t4

[0x8000118c]:fmax.s t6, t5, t4
[0x80001190]:csrrs a2, fcsr, zero
[0x80001194]:sd t6, 1360(fp)
[0x80001198]:sd a2, 1368(fp)
[0x8000119c]:ld t5, 1424(a1)
[0x800011a0]:ld t4, 1432(a1)
[0x800011a4]:addi s1, zero, 0
[0x800011a8]:csrrw zero, fcsr, s1
[0x800011ac]:fmax.s t6, t5, t4

[0x800011ac]:fmax.s t6, t5, t4
[0x800011b0]:csrrs a2, fcsr, zero
[0x800011b4]:sd t6, 1376(fp)
[0x800011b8]:sd a2, 1384(fp)
[0x800011bc]:ld t5, 1440(a1)
[0x800011c0]:ld t4, 1448(a1)
[0x800011c4]:addi s1, zero, 0
[0x800011c8]:csrrw zero, fcsr, s1
[0x800011cc]:fmax.s t6, t5, t4

[0x800011cc]:fmax.s t6, t5, t4
[0x800011d0]:csrrs a2, fcsr, zero
[0x800011d4]:sd t6, 1392(fp)
[0x800011d8]:sd a2, 1400(fp)
[0x800011dc]:ld t5, 1456(a1)
[0x800011e0]:ld t4, 1464(a1)
[0x800011e4]:addi s1, zero, 0
[0x800011e8]:csrrw zero, fcsr, s1
[0x800011ec]:fmax.s t6, t5, t4

[0x800011ec]:fmax.s t6, t5, t4
[0x800011f0]:csrrs a2, fcsr, zero
[0x800011f4]:sd t6, 1408(fp)
[0x800011f8]:sd a2, 1416(fp)
[0x800011fc]:ld t5, 1472(a1)
[0x80001200]:ld t4, 1480(a1)
[0x80001204]:addi s1, zero, 0
[0x80001208]:csrrw zero, fcsr, s1
[0x8000120c]:fmax.s t6, t5, t4

[0x8000120c]:fmax.s t6, t5, t4
[0x80001210]:csrrs a2, fcsr, zero
[0x80001214]:sd t6, 1424(fp)
[0x80001218]:sd a2, 1432(fp)
[0x8000121c]:ld t5, 1488(a1)
[0x80001220]:ld t4, 1496(a1)
[0x80001224]:addi s1, zero, 0
[0x80001228]:csrrw zero, fcsr, s1
[0x8000122c]:fmax.s t6, t5, t4

[0x8000122c]:fmax.s t6, t5, t4
[0x80001230]:csrrs a2, fcsr, zero
[0x80001234]:sd t6, 1440(fp)
[0x80001238]:sd a2, 1448(fp)
[0x8000123c]:ld t5, 1504(a1)
[0x80001240]:ld t4, 1512(a1)
[0x80001244]:addi s1, zero, 0
[0x80001248]:csrrw zero, fcsr, s1
[0x8000124c]:fmax.s t6, t5, t4

[0x8000124c]:fmax.s t6, t5, t4
[0x80001250]:csrrs a2, fcsr, zero
[0x80001254]:sd t6, 1456(fp)
[0x80001258]:sd a2, 1464(fp)
[0x8000125c]:ld t5, 1520(a1)
[0x80001260]:ld t4, 1528(a1)
[0x80001264]:addi s1, zero, 0
[0x80001268]:csrrw zero, fcsr, s1
[0x8000126c]:fmax.s t6, t5, t4

[0x8000126c]:fmax.s t6, t5, t4
[0x80001270]:csrrs a2, fcsr, zero
[0x80001274]:sd t6, 1472(fp)
[0x80001278]:sd a2, 1480(fp)
[0x8000127c]:ld t5, 1536(a1)
[0x80001280]:ld t4, 1544(a1)
[0x80001284]:addi s1, zero, 0
[0x80001288]:csrrw zero, fcsr, s1
[0x8000128c]:fmax.s t6, t5, t4

[0x8000128c]:fmax.s t6, t5, t4
[0x80001290]:csrrs a2, fcsr, zero
[0x80001294]:sd t6, 1488(fp)
[0x80001298]:sd a2, 1496(fp)
[0x8000129c]:ld t5, 1552(a1)
[0x800012a0]:ld t4, 1560(a1)
[0x800012a4]:addi s1, zero, 0
[0x800012a8]:csrrw zero, fcsr, s1
[0x800012ac]:fmax.s t6, t5, t4

[0x800012ac]:fmax.s t6, t5, t4
[0x800012b0]:csrrs a2, fcsr, zero
[0x800012b4]:sd t6, 1504(fp)
[0x800012b8]:sd a2, 1512(fp)
[0x800012bc]:ld t5, 1568(a1)
[0x800012c0]:ld t4, 1576(a1)
[0x800012c4]:addi s1, zero, 0
[0x800012c8]:csrrw zero, fcsr, s1
[0x800012cc]:fmax.s t6, t5, t4

[0x800012cc]:fmax.s t6, t5, t4
[0x800012d0]:csrrs a2, fcsr, zero
[0x800012d4]:sd t6, 1520(fp)
[0x800012d8]:sd a2, 1528(fp)
[0x800012dc]:ld t5, 1584(a1)
[0x800012e0]:ld t4, 1592(a1)
[0x800012e4]:addi s1, zero, 0
[0x800012e8]:csrrw zero, fcsr, s1
[0x800012ec]:fmax.s t6, t5, t4

[0x800012ec]:fmax.s t6, t5, t4
[0x800012f0]:csrrs a2, fcsr, zero
[0x800012f4]:sd t6, 1536(fp)
[0x800012f8]:sd a2, 1544(fp)
[0x800012fc]:ld t5, 1600(a1)
[0x80001300]:ld t4, 1608(a1)
[0x80001304]:addi s1, zero, 0
[0x80001308]:csrrw zero, fcsr, s1
[0x8000130c]:fmax.s t6, t5, t4

[0x8000130c]:fmax.s t6, t5, t4
[0x80001310]:csrrs a2, fcsr, zero
[0x80001314]:sd t6, 1552(fp)
[0x80001318]:sd a2, 1560(fp)
[0x8000131c]:ld t5, 1616(a1)
[0x80001320]:ld t4, 1624(a1)
[0x80001324]:addi s1, zero, 0
[0x80001328]:csrrw zero, fcsr, s1
[0x8000132c]:fmax.s t6, t5, t4

[0x8000132c]:fmax.s t6, t5, t4
[0x80001330]:csrrs a2, fcsr, zero
[0x80001334]:sd t6, 1568(fp)
[0x80001338]:sd a2, 1576(fp)
[0x8000133c]:ld t5, 1632(a1)
[0x80001340]:ld t4, 1640(a1)
[0x80001344]:addi s1, zero, 0
[0x80001348]:csrrw zero, fcsr, s1
[0x8000134c]:fmax.s t6, t5, t4

[0x8000134c]:fmax.s t6, t5, t4
[0x80001350]:csrrs a2, fcsr, zero
[0x80001354]:sd t6, 1584(fp)
[0x80001358]:sd a2, 1592(fp)
[0x8000135c]:ld t5, 1648(a1)
[0x80001360]:ld t4, 1656(a1)
[0x80001364]:addi s1, zero, 0
[0x80001368]:csrrw zero, fcsr, s1
[0x8000136c]:fmax.s t6, t5, t4

[0x8000136c]:fmax.s t6, t5, t4
[0x80001370]:csrrs a2, fcsr, zero
[0x80001374]:sd t6, 1600(fp)
[0x80001378]:sd a2, 1608(fp)
[0x8000137c]:ld t5, 1664(a1)
[0x80001380]:ld t4, 1672(a1)
[0x80001384]:addi s1, zero, 0
[0x80001388]:csrrw zero, fcsr, s1
[0x8000138c]:fmax.s t6, t5, t4

[0x8000138c]:fmax.s t6, t5, t4
[0x80001390]:csrrs a2, fcsr, zero
[0x80001394]:sd t6, 1616(fp)
[0x80001398]:sd a2, 1624(fp)
[0x8000139c]:ld t5, 1680(a1)
[0x800013a0]:ld t4, 1688(a1)
[0x800013a4]:addi s1, zero, 0
[0x800013a8]:csrrw zero, fcsr, s1
[0x800013ac]:fmax.s t6, t5, t4

[0x800013ac]:fmax.s t6, t5, t4
[0x800013b0]:csrrs a2, fcsr, zero
[0x800013b4]:sd t6, 1632(fp)
[0x800013b8]:sd a2, 1640(fp)
[0x800013bc]:ld t5, 1696(a1)
[0x800013c0]:ld t4, 1704(a1)
[0x800013c4]:addi s1, zero, 0
[0x800013c8]:csrrw zero, fcsr, s1
[0x800013cc]:fmax.s t6, t5, t4

[0x800013cc]:fmax.s t6, t5, t4
[0x800013d0]:csrrs a2, fcsr, zero
[0x800013d4]:sd t6, 1648(fp)
[0x800013d8]:sd a2, 1656(fp)
[0x800013dc]:ld t5, 1712(a1)
[0x800013e0]:ld t4, 1720(a1)
[0x800013e4]:addi s1, zero, 0
[0x800013e8]:csrrw zero, fcsr, s1
[0x800013ec]:fmax.s t6, t5, t4

[0x800013ec]:fmax.s t6, t5, t4
[0x800013f0]:csrrs a2, fcsr, zero
[0x800013f4]:sd t6, 1664(fp)
[0x800013f8]:sd a2, 1672(fp)
[0x800013fc]:ld t5, 1728(a1)
[0x80001400]:ld t4, 1736(a1)
[0x80001404]:addi s1, zero, 0
[0x80001408]:csrrw zero, fcsr, s1
[0x8000140c]:fmax.s t6, t5, t4

[0x8000140c]:fmax.s t6, t5, t4
[0x80001410]:csrrs a2, fcsr, zero
[0x80001414]:sd t6, 1680(fp)
[0x80001418]:sd a2, 1688(fp)
[0x8000141c]:ld t5, 1744(a1)
[0x80001420]:ld t4, 1752(a1)
[0x80001424]:addi s1, zero, 0
[0x80001428]:csrrw zero, fcsr, s1
[0x8000142c]:fmax.s t6, t5, t4

[0x8000142c]:fmax.s t6, t5, t4
[0x80001430]:csrrs a2, fcsr, zero
[0x80001434]:sd t6, 1696(fp)
[0x80001438]:sd a2, 1704(fp)
[0x8000143c]:ld t5, 1760(a1)
[0x80001440]:ld t4, 1768(a1)
[0x80001444]:addi s1, zero, 0
[0x80001448]:csrrw zero, fcsr, s1
[0x8000144c]:fmax.s t6, t5, t4

[0x8000144c]:fmax.s t6, t5, t4
[0x80001450]:csrrs a2, fcsr, zero
[0x80001454]:sd t6, 1712(fp)
[0x80001458]:sd a2, 1720(fp)
[0x8000145c]:ld t5, 1776(a1)
[0x80001460]:ld t4, 1784(a1)
[0x80001464]:addi s1, zero, 0
[0x80001468]:csrrw zero, fcsr, s1
[0x8000146c]:fmax.s t6, t5, t4

[0x8000146c]:fmax.s t6, t5, t4
[0x80001470]:csrrs a2, fcsr, zero
[0x80001474]:sd t6, 1728(fp)
[0x80001478]:sd a2, 1736(fp)
[0x8000147c]:ld t5, 1792(a1)
[0x80001480]:ld t4, 1800(a1)
[0x80001484]:addi s1, zero, 0
[0x80001488]:csrrw zero, fcsr, s1
[0x8000148c]:fmax.s t6, t5, t4

[0x8000148c]:fmax.s t6, t5, t4
[0x80001490]:csrrs a2, fcsr, zero
[0x80001494]:sd t6, 1744(fp)
[0x80001498]:sd a2, 1752(fp)
[0x8000149c]:ld t5, 1808(a1)
[0x800014a0]:ld t4, 1816(a1)
[0x800014a4]:addi s1, zero, 0
[0x800014a8]:csrrw zero, fcsr, s1
[0x800014ac]:fmax.s t6, t5, t4

[0x800014ac]:fmax.s t6, t5, t4
[0x800014b0]:csrrs a2, fcsr, zero
[0x800014b4]:sd t6, 1760(fp)
[0x800014b8]:sd a2, 1768(fp)
[0x800014bc]:ld t5, 1824(a1)
[0x800014c0]:ld t4, 1832(a1)
[0x800014c4]:addi s1, zero, 0
[0x800014c8]:csrrw zero, fcsr, s1
[0x800014cc]:fmax.s t6, t5, t4

[0x800014cc]:fmax.s t6, t5, t4
[0x800014d0]:csrrs a2, fcsr, zero
[0x800014d4]:sd t6, 1776(fp)
[0x800014d8]:sd a2, 1784(fp)
[0x800014dc]:ld t5, 1840(a1)
[0x800014e0]:ld t4, 1848(a1)
[0x800014e4]:addi s1, zero, 0
[0x800014e8]:csrrw zero, fcsr, s1
[0x800014ec]:fmax.s t6, t5, t4

[0x800014ec]:fmax.s t6, t5, t4
[0x800014f0]:csrrs a2, fcsr, zero
[0x800014f4]:sd t6, 1792(fp)
[0x800014f8]:sd a2, 1800(fp)
[0x800014fc]:ld t5, 1856(a1)
[0x80001500]:ld t4, 1864(a1)
[0x80001504]:addi s1, zero, 0
[0x80001508]:csrrw zero, fcsr, s1
[0x8000150c]:fmax.s t6, t5, t4

[0x8000150c]:fmax.s t6, t5, t4
[0x80001510]:csrrs a2, fcsr, zero
[0x80001514]:sd t6, 1808(fp)
[0x80001518]:sd a2, 1816(fp)
[0x8000151c]:ld t5, 1872(a1)
[0x80001520]:ld t4, 1880(a1)
[0x80001524]:addi s1, zero, 0
[0x80001528]:csrrw zero, fcsr, s1
[0x8000152c]:fmax.s t6, t5, t4

[0x8000152c]:fmax.s t6, t5, t4
[0x80001530]:csrrs a2, fcsr, zero
[0x80001534]:sd t6, 1824(fp)
[0x80001538]:sd a2, 1832(fp)
[0x8000153c]:ld t5, 1888(a1)
[0x80001540]:ld t4, 1896(a1)
[0x80001544]:addi s1, zero, 0
[0x80001548]:csrrw zero, fcsr, s1
[0x8000154c]:fmax.s t6, t5, t4

[0x8000154c]:fmax.s t6, t5, t4
[0x80001550]:csrrs a2, fcsr, zero
[0x80001554]:sd t6, 1840(fp)
[0x80001558]:sd a2, 1848(fp)
[0x8000155c]:ld t5, 1904(a1)
[0x80001560]:ld t4, 1912(a1)
[0x80001564]:addi s1, zero, 0
[0x80001568]:csrrw zero, fcsr, s1
[0x8000156c]:fmax.s t6, t5, t4

[0x8000156c]:fmax.s t6, t5, t4
[0x80001570]:csrrs a2, fcsr, zero
[0x80001574]:sd t6, 1856(fp)
[0x80001578]:sd a2, 1864(fp)
[0x8000157c]:ld t5, 1920(a1)
[0x80001580]:ld t4, 1928(a1)
[0x80001584]:addi s1, zero, 0
[0x80001588]:csrrw zero, fcsr, s1
[0x8000158c]:fmax.s t6, t5, t4

[0x8000158c]:fmax.s t6, t5, t4
[0x80001590]:csrrs a2, fcsr, zero
[0x80001594]:sd t6, 1872(fp)
[0x80001598]:sd a2, 1880(fp)
[0x8000159c]:ld t5, 1936(a1)
[0x800015a0]:ld t4, 1944(a1)
[0x800015a4]:addi s1, zero, 0
[0x800015a8]:csrrw zero, fcsr, s1
[0x800015ac]:fmax.s t6, t5, t4

[0x800015ac]:fmax.s t6, t5, t4
[0x800015b0]:csrrs a2, fcsr, zero
[0x800015b4]:sd t6, 1888(fp)
[0x800015b8]:sd a2, 1896(fp)
[0x800015bc]:ld t5, 1952(a1)
[0x800015c0]:ld t4, 1960(a1)
[0x800015c4]:addi s1, zero, 0
[0x800015c8]:csrrw zero, fcsr, s1
[0x800015cc]:fmax.s t6, t5, t4

[0x800015cc]:fmax.s t6, t5, t4
[0x800015d0]:csrrs a2, fcsr, zero
[0x800015d4]:sd t6, 1904(fp)
[0x800015d8]:sd a2, 1912(fp)
[0x800015dc]:ld t5, 1968(a1)
[0x800015e0]:ld t4, 1976(a1)
[0x800015e4]:addi s1, zero, 0
[0x800015e8]:csrrw zero, fcsr, s1
[0x800015ec]:fmax.s t6, t5, t4

[0x800015ec]:fmax.s t6, t5, t4
[0x800015f0]:csrrs a2, fcsr, zero
[0x800015f4]:sd t6, 1920(fp)
[0x800015f8]:sd a2, 1928(fp)
[0x800015fc]:ld t5, 1984(a1)
[0x80001600]:ld t4, 1992(a1)
[0x80001604]:addi s1, zero, 0
[0x80001608]:csrrw zero, fcsr, s1
[0x8000160c]:fmax.s t6, t5, t4

[0x8000160c]:fmax.s t6, t5, t4
[0x80001610]:csrrs a2, fcsr, zero
[0x80001614]:sd t6, 1936(fp)
[0x80001618]:sd a2, 1944(fp)
[0x8000161c]:ld t5, 2000(a1)
[0x80001620]:ld t4, 2008(a1)
[0x80001624]:addi s1, zero, 0
[0x80001628]:csrrw zero, fcsr, s1
[0x8000162c]:fmax.s t6, t5, t4

[0x8000162c]:fmax.s t6, t5, t4
[0x80001630]:csrrs a2, fcsr, zero
[0x80001634]:sd t6, 1952(fp)
[0x80001638]:sd a2, 1960(fp)
[0x8000163c]:ld t5, 2016(a1)
[0x80001640]:ld t4, 2024(a1)
[0x80001644]:addi s1, zero, 0
[0x80001648]:csrrw zero, fcsr, s1
[0x8000164c]:fmax.s t6, t5, t4

[0x8000164c]:fmax.s t6, t5, t4
[0x80001650]:csrrs a2, fcsr, zero
[0x80001654]:sd t6, 1968(fp)
[0x80001658]:sd a2, 1976(fp)
[0x8000165c]:ld t5, 2032(a1)
[0x80001660]:ld t4, 2040(a1)
[0x80001664]:addi s1, zero, 0
[0x80001668]:csrrw zero, fcsr, s1
[0x8000166c]:fmax.s t6, t5, t4

[0x8000166c]:fmax.s t6, t5, t4
[0x80001670]:csrrs a2, fcsr, zero
[0x80001674]:sd t6, 1984(fp)
[0x80001678]:sd a2, 1992(fp)
[0x8000167c]:lui s1, 1
[0x80001680]:addiw s1, s1, 2048
[0x80001684]:add a1, a1, s1
[0x80001688]:ld t5, 0(a1)
[0x8000168c]:sub a1, a1, s1
[0x80001690]:lui s1, 1
[0x80001694]:addiw s1, s1, 2048
[0x80001698]:add a1, a1, s1
[0x8000169c]:ld t4, 8(a1)
[0x800016a0]:sub a1, a1, s1
[0x800016a4]:addi s1, zero, 0
[0x800016a8]:csrrw zero, fcsr, s1
[0x800016ac]:fmax.s t6, t5, t4

[0x800016ac]:fmax.s t6, t5, t4
[0x800016b0]:csrrs a2, fcsr, zero
[0x800016b4]:sd t6, 2000(fp)
[0x800016b8]:sd a2, 2008(fp)
[0x800016bc]:lui s1, 1
[0x800016c0]:addiw s1, s1, 2048
[0x800016c4]:add a1, a1, s1
[0x800016c8]:ld t5, 16(a1)
[0x800016cc]:sub a1, a1, s1
[0x800016d0]:lui s1, 1
[0x800016d4]:addiw s1, s1, 2048
[0x800016d8]:add a1, a1, s1
[0x800016dc]:ld t4, 24(a1)
[0x800016e0]:sub a1, a1, s1
[0x800016e4]:addi s1, zero, 0
[0x800016e8]:csrrw zero, fcsr, s1
[0x800016ec]:fmax.s t6, t5, t4

[0x800016ec]:fmax.s t6, t5, t4
[0x800016f0]:csrrs a2, fcsr, zero
[0x800016f4]:sd t6, 2016(fp)
[0x800016f8]:sd a2, 2024(fp)
[0x800016fc]:lui s1, 1
[0x80001700]:addiw s1, s1, 2048
[0x80001704]:add a1, a1, s1
[0x80001708]:ld t5, 32(a1)
[0x8000170c]:sub a1, a1, s1
[0x80001710]:lui s1, 1
[0x80001714]:addiw s1, s1, 2048
[0x80001718]:add a1, a1, s1
[0x8000171c]:ld t4, 40(a1)
[0x80001720]:sub a1, a1, s1
[0x80001724]:addi s1, zero, 0
[0x80001728]:csrrw zero, fcsr, s1
[0x8000172c]:fmax.s t6, t5, t4

[0x8000172c]:fmax.s t6, t5, t4
[0x80001730]:csrrs a2, fcsr, zero
[0x80001734]:sd t6, 2032(fp)
[0x80001738]:sd a2, 2040(fp)
[0x8000173c]:auipc fp, 18
[0x80001740]:addi fp, fp, 108
[0x80001744]:lui s1, 1
[0x80001748]:addiw s1, s1, 2048
[0x8000174c]:add a1, a1, s1
[0x80001750]:ld t5, 48(a1)
[0x80001754]:sub a1, a1, s1
[0x80001758]:lui s1, 1
[0x8000175c]:addiw s1, s1, 2048
[0x80001760]:add a1, a1, s1
[0x80001764]:ld t4, 56(a1)
[0x80001768]:sub a1, a1, s1
[0x8000176c]:addi s1, zero, 0
[0x80001770]:csrrw zero, fcsr, s1
[0x80001774]:fmax.s t6, t5, t4

[0x80001774]:fmax.s t6, t5, t4
[0x80001778]:csrrs a2, fcsr, zero
[0x8000177c]:sd t6, 0(fp)
[0x80001780]:sd a2, 8(fp)
[0x80001784]:lui s1, 1
[0x80001788]:addiw s1, s1, 2048
[0x8000178c]:add a1, a1, s1
[0x80001790]:ld t5, 64(a1)
[0x80001794]:sub a1, a1, s1
[0x80001798]:lui s1, 1
[0x8000179c]:addiw s1, s1, 2048
[0x800017a0]:add a1, a1, s1
[0x800017a4]:ld t4, 72(a1)
[0x800017a8]:sub a1, a1, s1
[0x800017ac]:addi s1, zero, 0
[0x800017b0]:csrrw zero, fcsr, s1
[0x800017b4]:fmax.s t6, t5, t4

[0x800017b4]:fmax.s t6, t5, t4
[0x800017b8]:csrrs a2, fcsr, zero
[0x800017bc]:sd t6, 16(fp)
[0x800017c0]:sd a2, 24(fp)
[0x800017c4]:lui s1, 1
[0x800017c8]:addiw s1, s1, 2048
[0x800017cc]:add a1, a1, s1
[0x800017d0]:ld t5, 80(a1)
[0x800017d4]:sub a1, a1, s1
[0x800017d8]:lui s1, 1
[0x800017dc]:addiw s1, s1, 2048
[0x800017e0]:add a1, a1, s1
[0x800017e4]:ld t4, 88(a1)
[0x800017e8]:sub a1, a1, s1
[0x800017ec]:addi s1, zero, 0
[0x800017f0]:csrrw zero, fcsr, s1
[0x800017f4]:fmax.s t6, t5, t4

[0x800017f4]:fmax.s t6, t5, t4
[0x800017f8]:csrrs a2, fcsr, zero
[0x800017fc]:sd t6, 32(fp)
[0x80001800]:sd a2, 40(fp)
[0x80001804]:lui s1, 1
[0x80001808]:addiw s1, s1, 2048
[0x8000180c]:add a1, a1, s1
[0x80001810]:ld t5, 96(a1)
[0x80001814]:sub a1, a1, s1
[0x80001818]:lui s1, 1
[0x8000181c]:addiw s1, s1, 2048
[0x80001820]:add a1, a1, s1
[0x80001824]:ld t4, 104(a1)
[0x80001828]:sub a1, a1, s1
[0x8000182c]:addi s1, zero, 0
[0x80001830]:csrrw zero, fcsr, s1
[0x80001834]:fmax.s t6, t5, t4

[0x80001834]:fmax.s t6, t5, t4
[0x80001838]:csrrs a2, fcsr, zero
[0x8000183c]:sd t6, 48(fp)
[0x80001840]:sd a2, 56(fp)
[0x80001844]:lui s1, 1
[0x80001848]:addiw s1, s1, 2048
[0x8000184c]:add a1, a1, s1
[0x80001850]:ld t5, 112(a1)
[0x80001854]:sub a1, a1, s1
[0x80001858]:lui s1, 1
[0x8000185c]:addiw s1, s1, 2048
[0x80001860]:add a1, a1, s1
[0x80001864]:ld t4, 120(a1)
[0x80001868]:sub a1, a1, s1
[0x8000186c]:addi s1, zero, 0
[0x80001870]:csrrw zero, fcsr, s1
[0x80001874]:fmax.s t6, t5, t4

[0x80001874]:fmax.s t6, t5, t4
[0x80001878]:csrrs a2, fcsr, zero
[0x8000187c]:sd t6, 64(fp)
[0x80001880]:sd a2, 72(fp)
[0x80001884]:lui s1, 1
[0x80001888]:addiw s1, s1, 2048
[0x8000188c]:add a1, a1, s1
[0x80001890]:ld t5, 128(a1)
[0x80001894]:sub a1, a1, s1
[0x80001898]:lui s1, 1
[0x8000189c]:addiw s1, s1, 2048
[0x800018a0]:add a1, a1, s1
[0x800018a4]:ld t4, 136(a1)
[0x800018a8]:sub a1, a1, s1
[0x800018ac]:addi s1, zero, 0
[0x800018b0]:csrrw zero, fcsr, s1
[0x800018b4]:fmax.s t6, t5, t4

[0x800018b4]:fmax.s t6, t5, t4
[0x800018b8]:csrrs a2, fcsr, zero
[0x800018bc]:sd t6, 80(fp)
[0x800018c0]:sd a2, 88(fp)
[0x800018c4]:lui s1, 1
[0x800018c8]:addiw s1, s1, 2048
[0x800018cc]:add a1, a1, s1
[0x800018d0]:ld t5, 144(a1)
[0x800018d4]:sub a1, a1, s1
[0x800018d8]:lui s1, 1
[0x800018dc]:addiw s1, s1, 2048
[0x800018e0]:add a1, a1, s1
[0x800018e4]:ld t4, 152(a1)
[0x800018e8]:sub a1, a1, s1
[0x800018ec]:addi s1, zero, 0
[0x800018f0]:csrrw zero, fcsr, s1
[0x800018f4]:fmax.s t6, t5, t4

[0x800018f4]:fmax.s t6, t5, t4
[0x800018f8]:csrrs a2, fcsr, zero
[0x800018fc]:sd t6, 96(fp)
[0x80001900]:sd a2, 104(fp)
[0x80001904]:lui s1, 1
[0x80001908]:addiw s1, s1, 2048
[0x8000190c]:add a1, a1, s1
[0x80001910]:ld t5, 160(a1)
[0x80001914]:sub a1, a1, s1
[0x80001918]:lui s1, 1
[0x8000191c]:addiw s1, s1, 2048
[0x80001920]:add a1, a1, s1
[0x80001924]:ld t4, 168(a1)
[0x80001928]:sub a1, a1, s1
[0x8000192c]:addi s1, zero, 0
[0x80001930]:csrrw zero, fcsr, s1
[0x80001934]:fmax.s t6, t5, t4

[0x80001934]:fmax.s t6, t5, t4
[0x80001938]:csrrs a2, fcsr, zero
[0x8000193c]:sd t6, 112(fp)
[0x80001940]:sd a2, 120(fp)
[0x80001944]:lui s1, 1
[0x80001948]:addiw s1, s1, 2048
[0x8000194c]:add a1, a1, s1
[0x80001950]:ld t5, 176(a1)
[0x80001954]:sub a1, a1, s1
[0x80001958]:lui s1, 1
[0x8000195c]:addiw s1, s1, 2048
[0x80001960]:add a1, a1, s1
[0x80001964]:ld t4, 184(a1)
[0x80001968]:sub a1, a1, s1
[0x8000196c]:addi s1, zero, 0
[0x80001970]:csrrw zero, fcsr, s1
[0x80001974]:fmax.s t6, t5, t4

[0x80001974]:fmax.s t6, t5, t4
[0x80001978]:csrrs a2, fcsr, zero
[0x8000197c]:sd t6, 128(fp)
[0x80001980]:sd a2, 136(fp)
[0x80001984]:lui s1, 1
[0x80001988]:addiw s1, s1, 2048
[0x8000198c]:add a1, a1, s1
[0x80001990]:ld t5, 192(a1)
[0x80001994]:sub a1, a1, s1
[0x80001998]:lui s1, 1
[0x8000199c]:addiw s1, s1, 2048
[0x800019a0]:add a1, a1, s1
[0x800019a4]:ld t4, 200(a1)
[0x800019a8]:sub a1, a1, s1
[0x800019ac]:addi s1, zero, 0
[0x800019b0]:csrrw zero, fcsr, s1
[0x800019b4]:fmax.s t6, t5, t4

[0x800019b4]:fmax.s t6, t5, t4
[0x800019b8]:csrrs a2, fcsr, zero
[0x800019bc]:sd t6, 144(fp)
[0x800019c0]:sd a2, 152(fp)
[0x800019c4]:lui s1, 1
[0x800019c8]:addiw s1, s1, 2048
[0x800019cc]:add a1, a1, s1
[0x800019d0]:ld t5, 208(a1)
[0x800019d4]:sub a1, a1, s1
[0x800019d8]:lui s1, 1
[0x800019dc]:addiw s1, s1, 2048
[0x800019e0]:add a1, a1, s1
[0x800019e4]:ld t4, 216(a1)
[0x800019e8]:sub a1, a1, s1
[0x800019ec]:addi s1, zero, 0
[0x800019f0]:csrrw zero, fcsr, s1
[0x800019f4]:fmax.s t6, t5, t4

[0x800019f4]:fmax.s t6, t5, t4
[0x800019f8]:csrrs a2, fcsr, zero
[0x800019fc]:sd t6, 160(fp)
[0x80001a00]:sd a2, 168(fp)
[0x80001a04]:lui s1, 1
[0x80001a08]:addiw s1, s1, 2048
[0x80001a0c]:add a1, a1, s1
[0x80001a10]:ld t5, 224(a1)
[0x80001a14]:sub a1, a1, s1
[0x80001a18]:lui s1, 1
[0x80001a1c]:addiw s1, s1, 2048
[0x80001a20]:add a1, a1, s1
[0x80001a24]:ld t4, 232(a1)
[0x80001a28]:sub a1, a1, s1
[0x80001a2c]:addi s1, zero, 0
[0x80001a30]:csrrw zero, fcsr, s1
[0x80001a34]:fmax.s t6, t5, t4

[0x80001a34]:fmax.s t6, t5, t4
[0x80001a38]:csrrs a2, fcsr, zero
[0x80001a3c]:sd t6, 176(fp)
[0x80001a40]:sd a2, 184(fp)
[0x80001a44]:lui s1, 1
[0x80001a48]:addiw s1, s1, 2048
[0x80001a4c]:add a1, a1, s1
[0x80001a50]:ld t5, 240(a1)
[0x80001a54]:sub a1, a1, s1
[0x80001a58]:lui s1, 1
[0x80001a5c]:addiw s1, s1, 2048
[0x80001a60]:add a1, a1, s1
[0x80001a64]:ld t4, 248(a1)
[0x80001a68]:sub a1, a1, s1
[0x80001a6c]:addi s1, zero, 0
[0x80001a70]:csrrw zero, fcsr, s1
[0x80001a74]:fmax.s t6, t5, t4

[0x80001a74]:fmax.s t6, t5, t4
[0x80001a78]:csrrs a2, fcsr, zero
[0x80001a7c]:sd t6, 192(fp)
[0x80001a80]:sd a2, 200(fp)
[0x80001a84]:lui s1, 1
[0x80001a88]:addiw s1, s1, 2048
[0x80001a8c]:add a1, a1, s1
[0x80001a90]:ld t5, 256(a1)
[0x80001a94]:sub a1, a1, s1
[0x80001a98]:lui s1, 1
[0x80001a9c]:addiw s1, s1, 2048
[0x80001aa0]:add a1, a1, s1
[0x80001aa4]:ld t4, 264(a1)
[0x80001aa8]:sub a1, a1, s1
[0x80001aac]:addi s1, zero, 0
[0x80001ab0]:csrrw zero, fcsr, s1
[0x80001ab4]:fmax.s t6, t5, t4

[0x80001ab4]:fmax.s t6, t5, t4
[0x80001ab8]:csrrs a2, fcsr, zero
[0x80001abc]:sd t6, 208(fp)
[0x80001ac0]:sd a2, 216(fp)
[0x80001ac4]:lui s1, 1
[0x80001ac8]:addiw s1, s1, 2048
[0x80001acc]:add a1, a1, s1
[0x80001ad0]:ld t5, 272(a1)
[0x80001ad4]:sub a1, a1, s1
[0x80001ad8]:lui s1, 1
[0x80001adc]:addiw s1, s1, 2048
[0x80001ae0]:add a1, a1, s1
[0x80001ae4]:ld t4, 280(a1)
[0x80001ae8]:sub a1, a1, s1
[0x80001aec]:addi s1, zero, 0
[0x80001af0]:csrrw zero, fcsr, s1
[0x80001af4]:fmax.s t6, t5, t4

[0x80001af4]:fmax.s t6, t5, t4
[0x80001af8]:csrrs a2, fcsr, zero
[0x80001afc]:sd t6, 224(fp)
[0x80001b00]:sd a2, 232(fp)
[0x80001b04]:lui s1, 1
[0x80001b08]:addiw s1, s1, 2048
[0x80001b0c]:add a1, a1, s1
[0x80001b10]:ld t5, 288(a1)
[0x80001b14]:sub a1, a1, s1
[0x80001b18]:lui s1, 1
[0x80001b1c]:addiw s1, s1, 2048
[0x80001b20]:add a1, a1, s1
[0x80001b24]:ld t4, 296(a1)
[0x80001b28]:sub a1, a1, s1
[0x80001b2c]:addi s1, zero, 0
[0x80001b30]:csrrw zero, fcsr, s1
[0x80001b34]:fmax.s t6, t5, t4

[0x80001b34]:fmax.s t6, t5, t4
[0x80001b38]:csrrs a2, fcsr, zero
[0x80001b3c]:sd t6, 240(fp)
[0x80001b40]:sd a2, 248(fp)
[0x80001b44]:lui s1, 1
[0x80001b48]:addiw s1, s1, 2048
[0x80001b4c]:add a1, a1, s1
[0x80001b50]:ld t5, 304(a1)
[0x80001b54]:sub a1, a1, s1
[0x80001b58]:lui s1, 1
[0x80001b5c]:addiw s1, s1, 2048
[0x80001b60]:add a1, a1, s1
[0x80001b64]:ld t4, 312(a1)
[0x80001b68]:sub a1, a1, s1
[0x80001b6c]:addi s1, zero, 0
[0x80001b70]:csrrw zero, fcsr, s1
[0x80001b74]:fmax.s t6, t5, t4

[0x80001b74]:fmax.s t6, t5, t4
[0x80001b78]:csrrs a2, fcsr, zero
[0x80001b7c]:sd t6, 256(fp)
[0x80001b80]:sd a2, 264(fp)
[0x80001b84]:lui s1, 1
[0x80001b88]:addiw s1, s1, 2048
[0x80001b8c]:add a1, a1, s1
[0x80001b90]:ld t5, 320(a1)
[0x80001b94]:sub a1, a1, s1
[0x80001b98]:lui s1, 1
[0x80001b9c]:addiw s1, s1, 2048
[0x80001ba0]:add a1, a1, s1
[0x80001ba4]:ld t4, 328(a1)
[0x80001ba8]:sub a1, a1, s1
[0x80001bac]:addi s1, zero, 0
[0x80001bb0]:csrrw zero, fcsr, s1
[0x80001bb4]:fmax.s t6, t5, t4

[0x80001bb4]:fmax.s t6, t5, t4
[0x80001bb8]:csrrs a2, fcsr, zero
[0x80001bbc]:sd t6, 272(fp)
[0x80001bc0]:sd a2, 280(fp)
[0x80001bc4]:lui s1, 1
[0x80001bc8]:addiw s1, s1, 2048
[0x80001bcc]:add a1, a1, s1
[0x80001bd0]:ld t5, 336(a1)
[0x80001bd4]:sub a1, a1, s1
[0x80001bd8]:lui s1, 1
[0x80001bdc]:addiw s1, s1, 2048
[0x80001be0]:add a1, a1, s1
[0x80001be4]:ld t4, 344(a1)
[0x80001be8]:sub a1, a1, s1
[0x80001bec]:addi s1, zero, 0
[0x80001bf0]:csrrw zero, fcsr, s1
[0x80001bf4]:fmax.s t6, t5, t4

[0x80001bf4]:fmax.s t6, t5, t4
[0x80001bf8]:csrrs a2, fcsr, zero
[0x80001bfc]:sd t6, 288(fp)
[0x80001c00]:sd a2, 296(fp)
[0x80001c04]:lui s1, 1
[0x80001c08]:addiw s1, s1, 2048
[0x80001c0c]:add a1, a1, s1
[0x80001c10]:ld t5, 352(a1)
[0x80001c14]:sub a1, a1, s1
[0x80001c18]:lui s1, 1
[0x80001c1c]:addiw s1, s1, 2048
[0x80001c20]:add a1, a1, s1
[0x80001c24]:ld t4, 360(a1)
[0x80001c28]:sub a1, a1, s1
[0x80001c2c]:addi s1, zero, 0
[0x80001c30]:csrrw zero, fcsr, s1
[0x80001c34]:fmax.s t6, t5, t4

[0x80001c34]:fmax.s t6, t5, t4
[0x80001c38]:csrrs a2, fcsr, zero
[0x80001c3c]:sd t6, 304(fp)
[0x80001c40]:sd a2, 312(fp)
[0x80001c44]:lui s1, 1
[0x80001c48]:addiw s1, s1, 2048
[0x80001c4c]:add a1, a1, s1
[0x80001c50]:ld t5, 368(a1)
[0x80001c54]:sub a1, a1, s1
[0x80001c58]:lui s1, 1
[0x80001c5c]:addiw s1, s1, 2048
[0x80001c60]:add a1, a1, s1
[0x80001c64]:ld t4, 376(a1)
[0x80001c68]:sub a1, a1, s1
[0x80001c6c]:addi s1, zero, 0
[0x80001c70]:csrrw zero, fcsr, s1
[0x80001c74]:fmax.s t6, t5, t4

[0x80001c74]:fmax.s t6, t5, t4
[0x80001c78]:csrrs a2, fcsr, zero
[0x80001c7c]:sd t6, 320(fp)
[0x80001c80]:sd a2, 328(fp)
[0x80001c84]:lui s1, 1
[0x80001c88]:addiw s1, s1, 2048
[0x80001c8c]:add a1, a1, s1
[0x80001c90]:ld t5, 384(a1)
[0x80001c94]:sub a1, a1, s1
[0x80001c98]:lui s1, 1
[0x80001c9c]:addiw s1, s1, 2048
[0x80001ca0]:add a1, a1, s1
[0x80001ca4]:ld t4, 392(a1)
[0x80001ca8]:sub a1, a1, s1
[0x80001cac]:addi s1, zero, 0
[0x80001cb0]:csrrw zero, fcsr, s1
[0x80001cb4]:fmax.s t6, t5, t4

[0x80001cb4]:fmax.s t6, t5, t4
[0x80001cb8]:csrrs a2, fcsr, zero
[0x80001cbc]:sd t6, 336(fp)
[0x80001cc0]:sd a2, 344(fp)
[0x80001cc4]:lui s1, 1
[0x80001cc8]:addiw s1, s1, 2048
[0x80001ccc]:add a1, a1, s1
[0x80001cd0]:ld t5, 400(a1)
[0x80001cd4]:sub a1, a1, s1
[0x80001cd8]:lui s1, 1
[0x80001cdc]:addiw s1, s1, 2048
[0x80001ce0]:add a1, a1, s1
[0x80001ce4]:ld t4, 408(a1)
[0x80001ce8]:sub a1, a1, s1
[0x80001cec]:addi s1, zero, 0
[0x80001cf0]:csrrw zero, fcsr, s1
[0x80001cf4]:fmax.s t6, t5, t4

[0x80001cf4]:fmax.s t6, t5, t4
[0x80001cf8]:csrrs a2, fcsr, zero
[0x80001cfc]:sd t6, 352(fp)
[0x80001d00]:sd a2, 360(fp)
[0x80001d04]:lui s1, 1
[0x80001d08]:addiw s1, s1, 2048
[0x80001d0c]:add a1, a1, s1
[0x80001d10]:ld t5, 416(a1)
[0x80001d14]:sub a1, a1, s1
[0x80001d18]:lui s1, 1
[0x80001d1c]:addiw s1, s1, 2048
[0x80001d20]:add a1, a1, s1
[0x80001d24]:ld t4, 424(a1)
[0x80001d28]:sub a1, a1, s1
[0x80001d2c]:addi s1, zero, 0
[0x80001d30]:csrrw zero, fcsr, s1
[0x80001d34]:fmax.s t6, t5, t4

[0x80001d34]:fmax.s t6, t5, t4
[0x80001d38]:csrrs a2, fcsr, zero
[0x80001d3c]:sd t6, 368(fp)
[0x80001d40]:sd a2, 376(fp)
[0x80001d44]:lui s1, 1
[0x80001d48]:addiw s1, s1, 2048
[0x80001d4c]:add a1, a1, s1
[0x80001d50]:ld t5, 432(a1)
[0x80001d54]:sub a1, a1, s1
[0x80001d58]:lui s1, 1
[0x80001d5c]:addiw s1, s1, 2048
[0x80001d60]:add a1, a1, s1
[0x80001d64]:ld t4, 440(a1)
[0x80001d68]:sub a1, a1, s1
[0x80001d6c]:addi s1, zero, 0
[0x80001d70]:csrrw zero, fcsr, s1
[0x80001d74]:fmax.s t6, t5, t4

[0x80001d74]:fmax.s t6, t5, t4
[0x80001d78]:csrrs a2, fcsr, zero
[0x80001d7c]:sd t6, 384(fp)
[0x80001d80]:sd a2, 392(fp)
[0x80001d84]:lui s1, 1
[0x80001d88]:addiw s1, s1, 2048
[0x80001d8c]:add a1, a1, s1
[0x80001d90]:ld t5, 448(a1)
[0x80001d94]:sub a1, a1, s1
[0x80001d98]:lui s1, 1
[0x80001d9c]:addiw s1, s1, 2048
[0x80001da0]:add a1, a1, s1
[0x80001da4]:ld t4, 456(a1)
[0x80001da8]:sub a1, a1, s1
[0x80001dac]:addi s1, zero, 0
[0x80001db0]:csrrw zero, fcsr, s1
[0x80001db4]:fmax.s t6, t5, t4

[0x80001db4]:fmax.s t6, t5, t4
[0x80001db8]:csrrs a2, fcsr, zero
[0x80001dbc]:sd t6, 400(fp)
[0x80001dc0]:sd a2, 408(fp)
[0x80001dc4]:lui s1, 1
[0x80001dc8]:addiw s1, s1, 2048
[0x80001dcc]:add a1, a1, s1
[0x80001dd0]:ld t5, 464(a1)
[0x80001dd4]:sub a1, a1, s1
[0x80001dd8]:lui s1, 1
[0x80001ddc]:addiw s1, s1, 2048
[0x80001de0]:add a1, a1, s1
[0x80001de4]:ld t4, 472(a1)
[0x80001de8]:sub a1, a1, s1
[0x80001dec]:addi s1, zero, 0
[0x80001df0]:csrrw zero, fcsr, s1
[0x80001df4]:fmax.s t6, t5, t4

[0x80001df4]:fmax.s t6, t5, t4
[0x80001df8]:csrrs a2, fcsr, zero
[0x80001dfc]:sd t6, 416(fp)
[0x80001e00]:sd a2, 424(fp)
[0x80001e04]:lui s1, 1
[0x80001e08]:addiw s1, s1, 2048
[0x80001e0c]:add a1, a1, s1
[0x80001e10]:ld t5, 480(a1)
[0x80001e14]:sub a1, a1, s1
[0x80001e18]:lui s1, 1
[0x80001e1c]:addiw s1, s1, 2048
[0x80001e20]:add a1, a1, s1
[0x80001e24]:ld t4, 488(a1)
[0x80001e28]:sub a1, a1, s1
[0x80001e2c]:addi s1, zero, 0
[0x80001e30]:csrrw zero, fcsr, s1
[0x80001e34]:fmax.s t6, t5, t4

[0x80001e34]:fmax.s t6, t5, t4
[0x80001e38]:csrrs a2, fcsr, zero
[0x80001e3c]:sd t6, 432(fp)
[0x80001e40]:sd a2, 440(fp)
[0x80001e44]:lui s1, 1
[0x80001e48]:addiw s1, s1, 2048
[0x80001e4c]:add a1, a1, s1
[0x80001e50]:ld t5, 496(a1)
[0x80001e54]:sub a1, a1, s1
[0x80001e58]:lui s1, 1
[0x80001e5c]:addiw s1, s1, 2048
[0x80001e60]:add a1, a1, s1
[0x80001e64]:ld t4, 504(a1)
[0x80001e68]:sub a1, a1, s1
[0x80001e6c]:addi s1, zero, 0
[0x80001e70]:csrrw zero, fcsr, s1
[0x80001e74]:fmax.s t6, t5, t4

[0x80001e74]:fmax.s t6, t5, t4
[0x80001e78]:csrrs a2, fcsr, zero
[0x80001e7c]:sd t6, 448(fp)
[0x80001e80]:sd a2, 456(fp)
[0x80001e84]:lui s1, 1
[0x80001e88]:addiw s1, s1, 2048
[0x80001e8c]:add a1, a1, s1
[0x80001e90]:ld t5, 512(a1)
[0x80001e94]:sub a1, a1, s1
[0x80001e98]:lui s1, 1
[0x80001e9c]:addiw s1, s1, 2048
[0x80001ea0]:add a1, a1, s1
[0x80001ea4]:ld t4, 520(a1)
[0x80001ea8]:sub a1, a1, s1
[0x80001eac]:addi s1, zero, 0
[0x80001eb0]:csrrw zero, fcsr, s1
[0x80001eb4]:fmax.s t6, t5, t4

[0x80001eb4]:fmax.s t6, t5, t4
[0x80001eb8]:csrrs a2, fcsr, zero
[0x80001ebc]:sd t6, 464(fp)
[0x80001ec0]:sd a2, 472(fp)
[0x80001ec4]:lui s1, 1
[0x80001ec8]:addiw s1, s1, 2048
[0x80001ecc]:add a1, a1, s1
[0x80001ed0]:ld t5, 528(a1)
[0x80001ed4]:sub a1, a1, s1
[0x80001ed8]:lui s1, 1
[0x80001edc]:addiw s1, s1, 2048
[0x80001ee0]:add a1, a1, s1
[0x80001ee4]:ld t4, 536(a1)
[0x80001ee8]:sub a1, a1, s1
[0x80001eec]:addi s1, zero, 0
[0x80001ef0]:csrrw zero, fcsr, s1
[0x80001ef4]:fmax.s t6, t5, t4

[0x80001ef4]:fmax.s t6, t5, t4
[0x80001ef8]:csrrs a2, fcsr, zero
[0x80001efc]:sd t6, 480(fp)
[0x80001f00]:sd a2, 488(fp)
[0x80001f04]:lui s1, 1
[0x80001f08]:addiw s1, s1, 2048
[0x80001f0c]:add a1, a1, s1
[0x80001f10]:ld t5, 544(a1)
[0x80001f14]:sub a1, a1, s1
[0x80001f18]:lui s1, 1
[0x80001f1c]:addiw s1, s1, 2048
[0x80001f20]:add a1, a1, s1
[0x80001f24]:ld t4, 552(a1)
[0x80001f28]:sub a1, a1, s1
[0x80001f2c]:addi s1, zero, 0
[0x80001f30]:csrrw zero, fcsr, s1
[0x80001f34]:fmax.s t6, t5, t4

[0x80001f34]:fmax.s t6, t5, t4
[0x80001f38]:csrrs a2, fcsr, zero
[0x80001f3c]:sd t6, 496(fp)
[0x80001f40]:sd a2, 504(fp)
[0x80001f44]:lui s1, 1
[0x80001f48]:addiw s1, s1, 2048
[0x80001f4c]:add a1, a1, s1
[0x80001f50]:ld t5, 560(a1)
[0x80001f54]:sub a1, a1, s1
[0x80001f58]:lui s1, 1
[0x80001f5c]:addiw s1, s1, 2048
[0x80001f60]:add a1, a1, s1
[0x80001f64]:ld t4, 568(a1)
[0x80001f68]:sub a1, a1, s1
[0x80001f6c]:addi s1, zero, 0
[0x80001f70]:csrrw zero, fcsr, s1
[0x80001f74]:fmax.s t6, t5, t4

[0x80001f74]:fmax.s t6, t5, t4
[0x80001f78]:csrrs a2, fcsr, zero
[0x80001f7c]:sd t6, 512(fp)
[0x80001f80]:sd a2, 520(fp)
[0x80001f84]:lui s1, 1
[0x80001f88]:addiw s1, s1, 2048
[0x80001f8c]:add a1, a1, s1
[0x80001f90]:ld t5, 576(a1)
[0x80001f94]:sub a1, a1, s1
[0x80001f98]:lui s1, 1
[0x80001f9c]:addiw s1, s1, 2048
[0x80001fa0]:add a1, a1, s1
[0x80001fa4]:ld t4, 584(a1)
[0x80001fa8]:sub a1, a1, s1
[0x80001fac]:addi s1, zero, 0
[0x80001fb0]:csrrw zero, fcsr, s1
[0x80001fb4]:fmax.s t6, t5, t4

[0x80001fb4]:fmax.s t6, t5, t4
[0x80001fb8]:csrrs a2, fcsr, zero
[0x80001fbc]:sd t6, 528(fp)
[0x80001fc0]:sd a2, 536(fp)
[0x80001fc4]:lui s1, 1
[0x80001fc8]:addiw s1, s1, 2048
[0x80001fcc]:add a1, a1, s1
[0x80001fd0]:ld t5, 592(a1)
[0x80001fd4]:sub a1, a1, s1
[0x80001fd8]:lui s1, 1
[0x80001fdc]:addiw s1, s1, 2048
[0x80001fe0]:add a1, a1, s1
[0x80001fe4]:ld t4, 600(a1)
[0x80001fe8]:sub a1, a1, s1
[0x80001fec]:addi s1, zero, 0
[0x80001ff0]:csrrw zero, fcsr, s1
[0x80001ff4]:fmax.s t6, t5, t4

[0x80001ff4]:fmax.s t6, t5, t4
[0x80001ff8]:csrrs a2, fcsr, zero
[0x80001ffc]:sd t6, 544(fp)
[0x80002000]:sd a2, 552(fp)
[0x80002004]:lui s1, 1
[0x80002008]:addiw s1, s1, 2048
[0x8000200c]:add a1, a1, s1
[0x80002010]:ld t5, 608(a1)
[0x80002014]:sub a1, a1, s1
[0x80002018]:lui s1, 1
[0x8000201c]:addiw s1, s1, 2048
[0x80002020]:add a1, a1, s1
[0x80002024]:ld t4, 616(a1)
[0x80002028]:sub a1, a1, s1
[0x8000202c]:addi s1, zero, 0
[0x80002030]:csrrw zero, fcsr, s1
[0x80002034]:fmax.s t6, t5, t4

[0x80002034]:fmax.s t6, t5, t4
[0x80002038]:csrrs a2, fcsr, zero
[0x8000203c]:sd t6, 560(fp)
[0x80002040]:sd a2, 568(fp)
[0x80002044]:lui s1, 1
[0x80002048]:addiw s1, s1, 2048
[0x8000204c]:add a1, a1, s1
[0x80002050]:ld t5, 624(a1)
[0x80002054]:sub a1, a1, s1
[0x80002058]:lui s1, 1
[0x8000205c]:addiw s1, s1, 2048
[0x80002060]:add a1, a1, s1
[0x80002064]:ld t4, 632(a1)
[0x80002068]:sub a1, a1, s1
[0x8000206c]:addi s1, zero, 0
[0x80002070]:csrrw zero, fcsr, s1
[0x80002074]:fmax.s t6, t5, t4

[0x80002074]:fmax.s t6, t5, t4
[0x80002078]:csrrs a2, fcsr, zero
[0x8000207c]:sd t6, 576(fp)
[0x80002080]:sd a2, 584(fp)
[0x80002084]:lui s1, 1
[0x80002088]:addiw s1, s1, 2048
[0x8000208c]:add a1, a1, s1
[0x80002090]:ld t5, 640(a1)
[0x80002094]:sub a1, a1, s1
[0x80002098]:lui s1, 1
[0x8000209c]:addiw s1, s1, 2048
[0x800020a0]:add a1, a1, s1
[0x800020a4]:ld t4, 648(a1)
[0x800020a8]:sub a1, a1, s1
[0x800020ac]:addi s1, zero, 0
[0x800020b0]:csrrw zero, fcsr, s1
[0x800020b4]:fmax.s t6, t5, t4

[0x800020b4]:fmax.s t6, t5, t4
[0x800020b8]:csrrs a2, fcsr, zero
[0x800020bc]:sd t6, 592(fp)
[0x800020c0]:sd a2, 600(fp)
[0x800020c4]:lui s1, 1
[0x800020c8]:addiw s1, s1, 2048
[0x800020cc]:add a1, a1, s1
[0x800020d0]:ld t5, 656(a1)
[0x800020d4]:sub a1, a1, s1
[0x800020d8]:lui s1, 1
[0x800020dc]:addiw s1, s1, 2048
[0x800020e0]:add a1, a1, s1
[0x800020e4]:ld t4, 664(a1)
[0x800020e8]:sub a1, a1, s1
[0x800020ec]:addi s1, zero, 0
[0x800020f0]:csrrw zero, fcsr, s1
[0x800020f4]:fmax.s t6, t5, t4

[0x800020f4]:fmax.s t6, t5, t4
[0x800020f8]:csrrs a2, fcsr, zero
[0x800020fc]:sd t6, 608(fp)
[0x80002100]:sd a2, 616(fp)
[0x80002104]:lui s1, 1
[0x80002108]:addiw s1, s1, 2048
[0x8000210c]:add a1, a1, s1
[0x80002110]:ld t5, 672(a1)
[0x80002114]:sub a1, a1, s1
[0x80002118]:lui s1, 1
[0x8000211c]:addiw s1, s1, 2048
[0x80002120]:add a1, a1, s1
[0x80002124]:ld t4, 680(a1)
[0x80002128]:sub a1, a1, s1
[0x8000212c]:addi s1, zero, 0
[0x80002130]:csrrw zero, fcsr, s1
[0x80002134]:fmax.s t6, t5, t4

[0x80002134]:fmax.s t6, t5, t4
[0x80002138]:csrrs a2, fcsr, zero
[0x8000213c]:sd t6, 624(fp)
[0x80002140]:sd a2, 632(fp)
[0x80002144]:lui s1, 1
[0x80002148]:addiw s1, s1, 2048
[0x8000214c]:add a1, a1, s1
[0x80002150]:ld t5, 688(a1)
[0x80002154]:sub a1, a1, s1
[0x80002158]:lui s1, 1
[0x8000215c]:addiw s1, s1, 2048
[0x80002160]:add a1, a1, s1
[0x80002164]:ld t4, 696(a1)
[0x80002168]:sub a1, a1, s1
[0x8000216c]:addi s1, zero, 0
[0x80002170]:csrrw zero, fcsr, s1
[0x80002174]:fmax.s t6, t5, t4

[0x80002174]:fmax.s t6, t5, t4
[0x80002178]:csrrs a2, fcsr, zero
[0x8000217c]:sd t6, 640(fp)
[0x80002180]:sd a2, 648(fp)
[0x80002184]:lui s1, 1
[0x80002188]:addiw s1, s1, 2048
[0x8000218c]:add a1, a1, s1
[0x80002190]:ld t5, 704(a1)
[0x80002194]:sub a1, a1, s1
[0x80002198]:lui s1, 1
[0x8000219c]:addiw s1, s1, 2048
[0x800021a0]:add a1, a1, s1
[0x800021a4]:ld t4, 712(a1)
[0x800021a8]:sub a1, a1, s1
[0x800021ac]:addi s1, zero, 0
[0x800021b0]:csrrw zero, fcsr, s1
[0x800021b4]:fmax.s t6, t5, t4

[0x800021b4]:fmax.s t6, t5, t4
[0x800021b8]:csrrs a2, fcsr, zero
[0x800021bc]:sd t6, 656(fp)
[0x800021c0]:sd a2, 664(fp)
[0x800021c4]:lui s1, 1
[0x800021c8]:addiw s1, s1, 2048
[0x800021cc]:add a1, a1, s1
[0x800021d0]:ld t5, 720(a1)
[0x800021d4]:sub a1, a1, s1
[0x800021d8]:lui s1, 1
[0x800021dc]:addiw s1, s1, 2048
[0x800021e0]:add a1, a1, s1
[0x800021e4]:ld t4, 728(a1)
[0x800021e8]:sub a1, a1, s1
[0x800021ec]:addi s1, zero, 0
[0x800021f0]:csrrw zero, fcsr, s1
[0x800021f4]:fmax.s t6, t5, t4

[0x800021f4]:fmax.s t6, t5, t4
[0x800021f8]:csrrs a2, fcsr, zero
[0x800021fc]:sd t6, 672(fp)
[0x80002200]:sd a2, 680(fp)
[0x80002204]:lui s1, 1
[0x80002208]:addiw s1, s1, 2048
[0x8000220c]:add a1, a1, s1
[0x80002210]:ld t5, 736(a1)
[0x80002214]:sub a1, a1, s1
[0x80002218]:lui s1, 1
[0x8000221c]:addiw s1, s1, 2048
[0x80002220]:add a1, a1, s1
[0x80002224]:ld t4, 744(a1)
[0x80002228]:sub a1, a1, s1
[0x8000222c]:addi s1, zero, 0
[0x80002230]:csrrw zero, fcsr, s1
[0x80002234]:fmax.s t6, t5, t4

[0x80002234]:fmax.s t6, t5, t4
[0x80002238]:csrrs a2, fcsr, zero
[0x8000223c]:sd t6, 688(fp)
[0x80002240]:sd a2, 696(fp)
[0x80002244]:lui s1, 1
[0x80002248]:addiw s1, s1, 2048
[0x8000224c]:add a1, a1, s1
[0x80002250]:ld t5, 752(a1)
[0x80002254]:sub a1, a1, s1
[0x80002258]:lui s1, 1
[0x8000225c]:addiw s1, s1, 2048
[0x80002260]:add a1, a1, s1
[0x80002264]:ld t4, 760(a1)
[0x80002268]:sub a1, a1, s1
[0x8000226c]:addi s1, zero, 0
[0x80002270]:csrrw zero, fcsr, s1
[0x80002274]:fmax.s t6, t5, t4

[0x80002274]:fmax.s t6, t5, t4
[0x80002278]:csrrs a2, fcsr, zero
[0x8000227c]:sd t6, 704(fp)
[0x80002280]:sd a2, 712(fp)
[0x80002284]:lui s1, 1
[0x80002288]:addiw s1, s1, 2048
[0x8000228c]:add a1, a1, s1
[0x80002290]:ld t5, 768(a1)
[0x80002294]:sub a1, a1, s1
[0x80002298]:lui s1, 1
[0x8000229c]:addiw s1, s1, 2048
[0x800022a0]:add a1, a1, s1
[0x800022a4]:ld t4, 776(a1)
[0x800022a8]:sub a1, a1, s1
[0x800022ac]:addi s1, zero, 0
[0x800022b0]:csrrw zero, fcsr, s1
[0x800022b4]:fmax.s t6, t5, t4

[0x800022b4]:fmax.s t6, t5, t4
[0x800022b8]:csrrs a2, fcsr, zero
[0x800022bc]:sd t6, 720(fp)
[0x800022c0]:sd a2, 728(fp)
[0x800022c4]:lui s1, 1
[0x800022c8]:addiw s1, s1, 2048
[0x800022cc]:add a1, a1, s1
[0x800022d0]:ld t5, 784(a1)
[0x800022d4]:sub a1, a1, s1
[0x800022d8]:lui s1, 1
[0x800022dc]:addiw s1, s1, 2048
[0x800022e0]:add a1, a1, s1
[0x800022e4]:ld t4, 792(a1)
[0x800022e8]:sub a1, a1, s1
[0x800022ec]:addi s1, zero, 0
[0x800022f0]:csrrw zero, fcsr, s1
[0x800022f4]:fmax.s t6, t5, t4

[0x800022f4]:fmax.s t6, t5, t4
[0x800022f8]:csrrs a2, fcsr, zero
[0x800022fc]:sd t6, 736(fp)
[0x80002300]:sd a2, 744(fp)
[0x80002304]:lui s1, 1
[0x80002308]:addiw s1, s1, 2048
[0x8000230c]:add a1, a1, s1
[0x80002310]:ld t5, 800(a1)
[0x80002314]:sub a1, a1, s1
[0x80002318]:lui s1, 1
[0x8000231c]:addiw s1, s1, 2048
[0x80002320]:add a1, a1, s1
[0x80002324]:ld t4, 808(a1)
[0x80002328]:sub a1, a1, s1
[0x8000232c]:addi s1, zero, 0
[0x80002330]:csrrw zero, fcsr, s1
[0x80002334]:fmax.s t6, t5, t4

[0x80002334]:fmax.s t6, t5, t4
[0x80002338]:csrrs a2, fcsr, zero
[0x8000233c]:sd t6, 752(fp)
[0x80002340]:sd a2, 760(fp)
[0x80002344]:lui s1, 1
[0x80002348]:addiw s1, s1, 2048
[0x8000234c]:add a1, a1, s1
[0x80002350]:ld t5, 816(a1)
[0x80002354]:sub a1, a1, s1
[0x80002358]:lui s1, 1
[0x8000235c]:addiw s1, s1, 2048
[0x80002360]:add a1, a1, s1
[0x80002364]:ld t4, 824(a1)
[0x80002368]:sub a1, a1, s1
[0x8000236c]:addi s1, zero, 0
[0x80002370]:csrrw zero, fcsr, s1
[0x80002374]:fmax.s t6, t5, t4

[0x80002374]:fmax.s t6, t5, t4
[0x80002378]:csrrs a2, fcsr, zero
[0x8000237c]:sd t6, 768(fp)
[0x80002380]:sd a2, 776(fp)
[0x80002384]:lui s1, 1
[0x80002388]:addiw s1, s1, 2048
[0x8000238c]:add a1, a1, s1
[0x80002390]:ld t5, 832(a1)
[0x80002394]:sub a1, a1, s1
[0x80002398]:lui s1, 1
[0x8000239c]:addiw s1, s1, 2048
[0x800023a0]:add a1, a1, s1
[0x800023a4]:ld t4, 840(a1)
[0x800023a8]:sub a1, a1, s1
[0x800023ac]:addi s1, zero, 0
[0x800023b0]:csrrw zero, fcsr, s1
[0x800023b4]:fmax.s t6, t5, t4

[0x800023b4]:fmax.s t6, t5, t4
[0x800023b8]:csrrs a2, fcsr, zero
[0x800023bc]:sd t6, 784(fp)
[0x800023c0]:sd a2, 792(fp)
[0x800023c4]:lui s1, 1
[0x800023c8]:addiw s1, s1, 2048
[0x800023cc]:add a1, a1, s1
[0x800023d0]:ld t5, 848(a1)
[0x800023d4]:sub a1, a1, s1
[0x800023d8]:lui s1, 1
[0x800023dc]:addiw s1, s1, 2048
[0x800023e0]:add a1, a1, s1
[0x800023e4]:ld t4, 856(a1)
[0x800023e8]:sub a1, a1, s1
[0x800023ec]:addi s1, zero, 0
[0x800023f0]:csrrw zero, fcsr, s1
[0x800023f4]:fmax.s t6, t5, t4

[0x800023f4]:fmax.s t6, t5, t4
[0x800023f8]:csrrs a2, fcsr, zero
[0x800023fc]:sd t6, 800(fp)
[0x80002400]:sd a2, 808(fp)
[0x80002404]:lui s1, 1
[0x80002408]:addiw s1, s1, 2048
[0x8000240c]:add a1, a1, s1
[0x80002410]:ld t5, 864(a1)
[0x80002414]:sub a1, a1, s1
[0x80002418]:lui s1, 1
[0x8000241c]:addiw s1, s1, 2048
[0x80002420]:add a1, a1, s1
[0x80002424]:ld t4, 872(a1)
[0x80002428]:sub a1, a1, s1
[0x8000242c]:addi s1, zero, 0
[0x80002430]:csrrw zero, fcsr, s1
[0x80002434]:fmax.s t6, t5, t4

[0x80002434]:fmax.s t6, t5, t4
[0x80002438]:csrrs a2, fcsr, zero
[0x8000243c]:sd t6, 816(fp)
[0x80002440]:sd a2, 824(fp)
[0x80002444]:lui s1, 1
[0x80002448]:addiw s1, s1, 2048
[0x8000244c]:add a1, a1, s1
[0x80002450]:ld t5, 880(a1)
[0x80002454]:sub a1, a1, s1
[0x80002458]:lui s1, 1
[0x8000245c]:addiw s1, s1, 2048
[0x80002460]:add a1, a1, s1
[0x80002464]:ld t4, 888(a1)
[0x80002468]:sub a1, a1, s1
[0x8000246c]:addi s1, zero, 0
[0x80002470]:csrrw zero, fcsr, s1
[0x80002474]:fmax.s t6, t5, t4

[0x80002474]:fmax.s t6, t5, t4
[0x80002478]:csrrs a2, fcsr, zero
[0x8000247c]:sd t6, 832(fp)
[0x80002480]:sd a2, 840(fp)
[0x80002484]:lui s1, 1
[0x80002488]:addiw s1, s1, 2048
[0x8000248c]:add a1, a1, s1
[0x80002490]:ld t5, 896(a1)
[0x80002494]:sub a1, a1, s1
[0x80002498]:lui s1, 1
[0x8000249c]:addiw s1, s1, 2048
[0x800024a0]:add a1, a1, s1
[0x800024a4]:ld t4, 904(a1)
[0x800024a8]:sub a1, a1, s1
[0x800024ac]:addi s1, zero, 0
[0x800024b0]:csrrw zero, fcsr, s1
[0x800024b4]:fmax.s t6, t5, t4

[0x800024b4]:fmax.s t6, t5, t4
[0x800024b8]:csrrs a2, fcsr, zero
[0x800024bc]:sd t6, 848(fp)
[0x800024c0]:sd a2, 856(fp)
[0x800024c4]:lui s1, 1
[0x800024c8]:addiw s1, s1, 2048
[0x800024cc]:add a1, a1, s1
[0x800024d0]:ld t5, 912(a1)
[0x800024d4]:sub a1, a1, s1
[0x800024d8]:lui s1, 1
[0x800024dc]:addiw s1, s1, 2048
[0x800024e0]:add a1, a1, s1
[0x800024e4]:ld t4, 920(a1)
[0x800024e8]:sub a1, a1, s1
[0x800024ec]:addi s1, zero, 0
[0x800024f0]:csrrw zero, fcsr, s1
[0x800024f4]:fmax.s t6, t5, t4

[0x800024f4]:fmax.s t6, t5, t4
[0x800024f8]:csrrs a2, fcsr, zero
[0x800024fc]:sd t6, 864(fp)
[0x80002500]:sd a2, 872(fp)
[0x80002504]:lui s1, 1
[0x80002508]:addiw s1, s1, 2048
[0x8000250c]:add a1, a1, s1
[0x80002510]:ld t5, 928(a1)
[0x80002514]:sub a1, a1, s1
[0x80002518]:lui s1, 1
[0x8000251c]:addiw s1, s1, 2048
[0x80002520]:add a1, a1, s1
[0x80002524]:ld t4, 936(a1)
[0x80002528]:sub a1, a1, s1
[0x8000252c]:addi s1, zero, 0
[0x80002530]:csrrw zero, fcsr, s1
[0x80002534]:fmax.s t6, t5, t4

[0x80002534]:fmax.s t6, t5, t4
[0x80002538]:csrrs a2, fcsr, zero
[0x8000253c]:sd t6, 880(fp)
[0x80002540]:sd a2, 888(fp)
[0x80002544]:lui s1, 1
[0x80002548]:addiw s1, s1, 2048
[0x8000254c]:add a1, a1, s1
[0x80002550]:ld t5, 944(a1)
[0x80002554]:sub a1, a1, s1
[0x80002558]:lui s1, 1
[0x8000255c]:addiw s1, s1, 2048
[0x80002560]:add a1, a1, s1
[0x80002564]:ld t4, 952(a1)
[0x80002568]:sub a1, a1, s1
[0x8000256c]:addi s1, zero, 0
[0x80002570]:csrrw zero, fcsr, s1
[0x80002574]:fmax.s t6, t5, t4

[0x80002574]:fmax.s t6, t5, t4
[0x80002578]:csrrs a2, fcsr, zero
[0x8000257c]:sd t6, 896(fp)
[0x80002580]:sd a2, 904(fp)
[0x80002584]:lui s1, 1
[0x80002588]:addiw s1, s1, 2048
[0x8000258c]:add a1, a1, s1
[0x80002590]:ld t5, 960(a1)
[0x80002594]:sub a1, a1, s1
[0x80002598]:lui s1, 1
[0x8000259c]:addiw s1, s1, 2048
[0x800025a0]:add a1, a1, s1
[0x800025a4]:ld t4, 968(a1)
[0x800025a8]:sub a1, a1, s1
[0x800025ac]:addi s1, zero, 0
[0x800025b0]:csrrw zero, fcsr, s1
[0x800025b4]:fmax.s t6, t5, t4

[0x800025b4]:fmax.s t6, t5, t4
[0x800025b8]:csrrs a2, fcsr, zero
[0x800025bc]:sd t6, 912(fp)
[0x800025c0]:sd a2, 920(fp)
[0x800025c4]:lui s1, 1
[0x800025c8]:addiw s1, s1, 2048
[0x800025cc]:add a1, a1, s1
[0x800025d0]:ld t5, 976(a1)
[0x800025d4]:sub a1, a1, s1
[0x800025d8]:lui s1, 1
[0x800025dc]:addiw s1, s1, 2048
[0x800025e0]:add a1, a1, s1
[0x800025e4]:ld t4, 984(a1)
[0x800025e8]:sub a1, a1, s1
[0x800025ec]:addi s1, zero, 0
[0x800025f0]:csrrw zero, fcsr, s1
[0x800025f4]:fmax.s t6, t5, t4

[0x800025f4]:fmax.s t6, t5, t4
[0x800025f8]:csrrs a2, fcsr, zero
[0x800025fc]:sd t6, 928(fp)
[0x80002600]:sd a2, 936(fp)
[0x80002604]:lui s1, 1
[0x80002608]:addiw s1, s1, 2048
[0x8000260c]:add a1, a1, s1
[0x80002610]:ld t5, 992(a1)
[0x80002614]:sub a1, a1, s1
[0x80002618]:lui s1, 1
[0x8000261c]:addiw s1, s1, 2048
[0x80002620]:add a1, a1, s1
[0x80002624]:ld t4, 1000(a1)
[0x80002628]:sub a1, a1, s1
[0x8000262c]:addi s1, zero, 0
[0x80002630]:csrrw zero, fcsr, s1
[0x80002634]:fmax.s t6, t5, t4

[0x80002634]:fmax.s t6, t5, t4
[0x80002638]:csrrs a2, fcsr, zero
[0x8000263c]:sd t6, 944(fp)
[0x80002640]:sd a2, 952(fp)
[0x80002644]:lui s1, 1
[0x80002648]:addiw s1, s1, 2048
[0x8000264c]:add a1, a1, s1
[0x80002650]:ld t5, 1008(a1)
[0x80002654]:sub a1, a1, s1
[0x80002658]:lui s1, 1
[0x8000265c]:addiw s1, s1, 2048
[0x80002660]:add a1, a1, s1
[0x80002664]:ld t4, 1016(a1)
[0x80002668]:sub a1, a1, s1
[0x8000266c]:addi s1, zero, 0
[0x80002670]:csrrw zero, fcsr, s1
[0x80002674]:fmax.s t6, t5, t4

[0x80002674]:fmax.s t6, t5, t4
[0x80002678]:csrrs a2, fcsr, zero
[0x8000267c]:sd t6, 960(fp)
[0x80002680]:sd a2, 968(fp)
[0x80002684]:lui s1, 1
[0x80002688]:addiw s1, s1, 2048
[0x8000268c]:add a1, a1, s1
[0x80002690]:ld t5, 1024(a1)
[0x80002694]:sub a1, a1, s1
[0x80002698]:lui s1, 1
[0x8000269c]:addiw s1, s1, 2048
[0x800026a0]:add a1, a1, s1
[0x800026a4]:ld t4, 1032(a1)
[0x800026a8]:sub a1, a1, s1
[0x800026ac]:addi s1, zero, 0
[0x800026b0]:csrrw zero, fcsr, s1
[0x800026b4]:fmax.s t6, t5, t4

[0x800026b4]:fmax.s t6, t5, t4
[0x800026b8]:csrrs a2, fcsr, zero
[0x800026bc]:sd t6, 976(fp)
[0x800026c0]:sd a2, 984(fp)
[0x800026c4]:lui s1, 1
[0x800026c8]:addiw s1, s1, 2048
[0x800026cc]:add a1, a1, s1
[0x800026d0]:ld t5, 1040(a1)
[0x800026d4]:sub a1, a1, s1
[0x800026d8]:lui s1, 1
[0x800026dc]:addiw s1, s1, 2048
[0x800026e0]:add a1, a1, s1
[0x800026e4]:ld t4, 1048(a1)
[0x800026e8]:sub a1, a1, s1
[0x800026ec]:addi s1, zero, 0
[0x800026f0]:csrrw zero, fcsr, s1
[0x800026f4]:fmax.s t6, t5, t4

[0x800026f4]:fmax.s t6, t5, t4
[0x800026f8]:csrrs a2, fcsr, zero
[0x800026fc]:sd t6, 992(fp)
[0x80002700]:sd a2, 1000(fp)
[0x80002704]:lui s1, 1
[0x80002708]:addiw s1, s1, 2048
[0x8000270c]:add a1, a1, s1
[0x80002710]:ld t5, 1056(a1)
[0x80002714]:sub a1, a1, s1
[0x80002718]:lui s1, 1
[0x8000271c]:addiw s1, s1, 2048
[0x80002720]:add a1, a1, s1
[0x80002724]:ld t4, 1064(a1)
[0x80002728]:sub a1, a1, s1
[0x8000272c]:addi s1, zero, 0
[0x80002730]:csrrw zero, fcsr, s1
[0x80002734]:fmax.s t6, t5, t4

[0x80002734]:fmax.s t6, t5, t4
[0x80002738]:csrrs a2, fcsr, zero
[0x8000273c]:sd t6, 1008(fp)
[0x80002740]:sd a2, 1016(fp)
[0x80002744]:lui s1, 1
[0x80002748]:addiw s1, s1, 2048
[0x8000274c]:add a1, a1, s1
[0x80002750]:ld t5, 1072(a1)
[0x80002754]:sub a1, a1, s1
[0x80002758]:lui s1, 1
[0x8000275c]:addiw s1, s1, 2048
[0x80002760]:add a1, a1, s1
[0x80002764]:ld t4, 1080(a1)
[0x80002768]:sub a1, a1, s1
[0x8000276c]:addi s1, zero, 0
[0x80002770]:csrrw zero, fcsr, s1
[0x80002774]:fmax.s t6, t5, t4

[0x80002774]:fmax.s t6, t5, t4
[0x80002778]:csrrs a2, fcsr, zero
[0x8000277c]:sd t6, 1024(fp)
[0x80002780]:sd a2, 1032(fp)
[0x80002784]:lui s1, 1
[0x80002788]:addiw s1, s1, 2048
[0x8000278c]:add a1, a1, s1
[0x80002790]:ld t5, 1088(a1)
[0x80002794]:sub a1, a1, s1
[0x80002798]:lui s1, 1
[0x8000279c]:addiw s1, s1, 2048
[0x800027a0]:add a1, a1, s1
[0x800027a4]:ld t4, 1096(a1)
[0x800027a8]:sub a1, a1, s1
[0x800027ac]:addi s1, zero, 0
[0x800027b0]:csrrw zero, fcsr, s1
[0x800027b4]:fmax.s t6, t5, t4

[0x800027b4]:fmax.s t6, t5, t4
[0x800027b8]:csrrs a2, fcsr, zero
[0x800027bc]:sd t6, 1040(fp)
[0x800027c0]:sd a2, 1048(fp)
[0x800027c4]:lui s1, 1
[0x800027c8]:addiw s1, s1, 2048
[0x800027cc]:add a1, a1, s1
[0x800027d0]:ld t5, 1104(a1)
[0x800027d4]:sub a1, a1, s1
[0x800027d8]:lui s1, 1
[0x800027dc]:addiw s1, s1, 2048
[0x800027e0]:add a1, a1, s1
[0x800027e4]:ld t4, 1112(a1)
[0x800027e8]:sub a1, a1, s1
[0x800027ec]:addi s1, zero, 0
[0x800027f0]:csrrw zero, fcsr, s1
[0x800027f4]:fmax.s t6, t5, t4

[0x800027f4]:fmax.s t6, t5, t4
[0x800027f8]:csrrs a2, fcsr, zero
[0x800027fc]:sd t6, 1056(fp)
[0x80002800]:sd a2, 1064(fp)
[0x80002804]:lui s1, 1
[0x80002808]:addiw s1, s1, 2048
[0x8000280c]:add a1, a1, s1
[0x80002810]:ld t5, 1120(a1)
[0x80002814]:sub a1, a1, s1
[0x80002818]:lui s1, 1
[0x8000281c]:addiw s1, s1, 2048
[0x80002820]:add a1, a1, s1
[0x80002824]:ld t4, 1128(a1)
[0x80002828]:sub a1, a1, s1
[0x8000282c]:addi s1, zero, 0
[0x80002830]:csrrw zero, fcsr, s1
[0x80002834]:fmax.s t6, t5, t4

[0x80002834]:fmax.s t6, t5, t4
[0x80002838]:csrrs a2, fcsr, zero
[0x8000283c]:sd t6, 1072(fp)
[0x80002840]:sd a2, 1080(fp)
[0x80002844]:lui s1, 1
[0x80002848]:addiw s1, s1, 2048
[0x8000284c]:add a1, a1, s1
[0x80002850]:ld t5, 1136(a1)
[0x80002854]:sub a1, a1, s1
[0x80002858]:lui s1, 1
[0x8000285c]:addiw s1, s1, 2048
[0x80002860]:add a1, a1, s1
[0x80002864]:ld t4, 1144(a1)
[0x80002868]:sub a1, a1, s1
[0x8000286c]:addi s1, zero, 0
[0x80002870]:csrrw zero, fcsr, s1
[0x80002874]:fmax.s t6, t5, t4

[0x80002874]:fmax.s t6, t5, t4
[0x80002878]:csrrs a2, fcsr, zero
[0x8000287c]:sd t6, 1088(fp)
[0x80002880]:sd a2, 1096(fp)
[0x80002884]:lui s1, 1
[0x80002888]:addiw s1, s1, 2048
[0x8000288c]:add a1, a1, s1
[0x80002890]:ld t5, 1152(a1)
[0x80002894]:sub a1, a1, s1
[0x80002898]:lui s1, 1
[0x8000289c]:addiw s1, s1, 2048
[0x800028a0]:add a1, a1, s1
[0x800028a4]:ld t4, 1160(a1)
[0x800028a8]:sub a1, a1, s1
[0x800028ac]:addi s1, zero, 0
[0x800028b0]:csrrw zero, fcsr, s1
[0x800028b4]:fmax.s t6, t5, t4

[0x800028b4]:fmax.s t6, t5, t4
[0x800028b8]:csrrs a2, fcsr, zero
[0x800028bc]:sd t6, 1104(fp)
[0x800028c0]:sd a2, 1112(fp)
[0x800028c4]:lui s1, 1
[0x800028c8]:addiw s1, s1, 2048
[0x800028cc]:add a1, a1, s1
[0x800028d0]:ld t5, 1168(a1)
[0x800028d4]:sub a1, a1, s1
[0x800028d8]:lui s1, 1
[0x800028dc]:addiw s1, s1, 2048
[0x800028e0]:add a1, a1, s1
[0x800028e4]:ld t4, 1176(a1)
[0x800028e8]:sub a1, a1, s1
[0x800028ec]:addi s1, zero, 0
[0x800028f0]:csrrw zero, fcsr, s1
[0x800028f4]:fmax.s t6, t5, t4

[0x800028f4]:fmax.s t6, t5, t4
[0x800028f8]:csrrs a2, fcsr, zero
[0x800028fc]:sd t6, 1120(fp)
[0x80002900]:sd a2, 1128(fp)
[0x80002904]:lui s1, 1
[0x80002908]:addiw s1, s1, 2048
[0x8000290c]:add a1, a1, s1
[0x80002910]:ld t5, 1184(a1)
[0x80002914]:sub a1, a1, s1
[0x80002918]:lui s1, 1
[0x8000291c]:addiw s1, s1, 2048
[0x80002920]:add a1, a1, s1
[0x80002924]:ld t4, 1192(a1)
[0x80002928]:sub a1, a1, s1
[0x8000292c]:addi s1, zero, 0
[0x80002930]:csrrw zero, fcsr, s1
[0x80002934]:fmax.s t6, t5, t4

[0x80002934]:fmax.s t6, t5, t4
[0x80002938]:csrrs a2, fcsr, zero
[0x8000293c]:sd t6, 1136(fp)
[0x80002940]:sd a2, 1144(fp)
[0x80002944]:lui s1, 1
[0x80002948]:addiw s1, s1, 2048
[0x8000294c]:add a1, a1, s1
[0x80002950]:ld t5, 1200(a1)
[0x80002954]:sub a1, a1, s1
[0x80002958]:lui s1, 1
[0x8000295c]:addiw s1, s1, 2048
[0x80002960]:add a1, a1, s1
[0x80002964]:ld t4, 1208(a1)
[0x80002968]:sub a1, a1, s1
[0x8000296c]:addi s1, zero, 0
[0x80002970]:csrrw zero, fcsr, s1
[0x80002974]:fmax.s t6, t5, t4

[0x80002974]:fmax.s t6, t5, t4
[0x80002978]:csrrs a2, fcsr, zero
[0x8000297c]:sd t6, 1152(fp)
[0x80002980]:sd a2, 1160(fp)
[0x80002984]:lui s1, 1
[0x80002988]:addiw s1, s1, 2048
[0x8000298c]:add a1, a1, s1
[0x80002990]:ld t5, 1216(a1)
[0x80002994]:sub a1, a1, s1
[0x80002998]:lui s1, 1
[0x8000299c]:addiw s1, s1, 2048
[0x800029a0]:add a1, a1, s1
[0x800029a4]:ld t4, 1224(a1)
[0x800029a8]:sub a1, a1, s1
[0x800029ac]:addi s1, zero, 0
[0x800029b0]:csrrw zero, fcsr, s1
[0x800029b4]:fmax.s t6, t5, t4

[0x800029b4]:fmax.s t6, t5, t4
[0x800029b8]:csrrs a2, fcsr, zero
[0x800029bc]:sd t6, 1168(fp)
[0x800029c0]:sd a2, 1176(fp)
[0x800029c4]:lui s1, 1
[0x800029c8]:addiw s1, s1, 2048
[0x800029cc]:add a1, a1, s1
[0x800029d0]:ld t5, 1232(a1)
[0x800029d4]:sub a1, a1, s1
[0x800029d8]:lui s1, 1
[0x800029dc]:addiw s1, s1, 2048
[0x800029e0]:add a1, a1, s1
[0x800029e4]:ld t4, 1240(a1)
[0x800029e8]:sub a1, a1, s1
[0x800029ec]:addi s1, zero, 0
[0x800029f0]:csrrw zero, fcsr, s1
[0x800029f4]:fmax.s t6, t5, t4

[0x800029f4]:fmax.s t6, t5, t4
[0x800029f8]:csrrs a2, fcsr, zero
[0x800029fc]:sd t6, 1184(fp)
[0x80002a00]:sd a2, 1192(fp)
[0x80002a04]:lui s1, 1
[0x80002a08]:addiw s1, s1, 2048
[0x80002a0c]:add a1, a1, s1
[0x80002a10]:ld t5, 1248(a1)
[0x80002a14]:sub a1, a1, s1
[0x80002a18]:lui s1, 1
[0x80002a1c]:addiw s1, s1, 2048
[0x80002a20]:add a1, a1, s1
[0x80002a24]:ld t4, 1256(a1)
[0x80002a28]:sub a1, a1, s1
[0x80002a2c]:addi s1, zero, 0
[0x80002a30]:csrrw zero, fcsr, s1
[0x80002a34]:fmax.s t6, t5, t4

[0x80002a34]:fmax.s t6, t5, t4
[0x80002a38]:csrrs a2, fcsr, zero
[0x80002a3c]:sd t6, 1200(fp)
[0x80002a40]:sd a2, 1208(fp)
[0x80002a44]:lui s1, 1
[0x80002a48]:addiw s1, s1, 2048
[0x80002a4c]:add a1, a1, s1
[0x80002a50]:ld t5, 1264(a1)
[0x80002a54]:sub a1, a1, s1
[0x80002a58]:lui s1, 1
[0x80002a5c]:addiw s1, s1, 2048
[0x80002a60]:add a1, a1, s1
[0x80002a64]:ld t4, 1272(a1)
[0x80002a68]:sub a1, a1, s1
[0x80002a6c]:addi s1, zero, 0
[0x80002a70]:csrrw zero, fcsr, s1
[0x80002a74]:fmax.s t6, t5, t4

[0x80002a74]:fmax.s t6, t5, t4
[0x80002a78]:csrrs a2, fcsr, zero
[0x80002a7c]:sd t6, 1216(fp)
[0x80002a80]:sd a2, 1224(fp)
[0x80002a84]:lui s1, 1
[0x80002a88]:addiw s1, s1, 2048
[0x80002a8c]:add a1, a1, s1
[0x80002a90]:ld t5, 1280(a1)
[0x80002a94]:sub a1, a1, s1
[0x80002a98]:lui s1, 1
[0x80002a9c]:addiw s1, s1, 2048
[0x80002aa0]:add a1, a1, s1
[0x80002aa4]:ld t4, 1288(a1)
[0x80002aa8]:sub a1, a1, s1
[0x80002aac]:addi s1, zero, 0
[0x80002ab0]:csrrw zero, fcsr, s1
[0x80002ab4]:fmax.s t6, t5, t4

[0x80002ab4]:fmax.s t6, t5, t4
[0x80002ab8]:csrrs a2, fcsr, zero
[0x80002abc]:sd t6, 1232(fp)
[0x80002ac0]:sd a2, 1240(fp)
[0x80002ac4]:lui s1, 1
[0x80002ac8]:addiw s1, s1, 2048
[0x80002acc]:add a1, a1, s1
[0x80002ad0]:ld t5, 1296(a1)
[0x80002ad4]:sub a1, a1, s1
[0x80002ad8]:lui s1, 1
[0x80002adc]:addiw s1, s1, 2048
[0x80002ae0]:add a1, a1, s1
[0x80002ae4]:ld t4, 1304(a1)
[0x80002ae8]:sub a1, a1, s1
[0x80002aec]:addi s1, zero, 0
[0x80002af0]:csrrw zero, fcsr, s1
[0x80002af4]:fmax.s t6, t5, t4

[0x80002af4]:fmax.s t6, t5, t4
[0x80002af8]:csrrs a2, fcsr, zero
[0x80002afc]:sd t6, 1248(fp)
[0x80002b00]:sd a2, 1256(fp)
[0x80002b04]:lui s1, 1
[0x80002b08]:addiw s1, s1, 2048
[0x80002b0c]:add a1, a1, s1
[0x80002b10]:ld t5, 1312(a1)
[0x80002b14]:sub a1, a1, s1
[0x80002b18]:lui s1, 1
[0x80002b1c]:addiw s1, s1, 2048
[0x80002b20]:add a1, a1, s1
[0x80002b24]:ld t4, 1320(a1)
[0x80002b28]:sub a1, a1, s1
[0x80002b2c]:addi s1, zero, 0
[0x80002b30]:csrrw zero, fcsr, s1
[0x80002b34]:fmax.s t6, t5, t4

[0x80002b34]:fmax.s t6, t5, t4
[0x80002b38]:csrrs a2, fcsr, zero
[0x80002b3c]:sd t6, 1264(fp)
[0x80002b40]:sd a2, 1272(fp)
[0x80002b44]:lui s1, 1
[0x80002b48]:addiw s1, s1, 2048
[0x80002b4c]:add a1, a1, s1
[0x80002b50]:ld t5, 1328(a1)
[0x80002b54]:sub a1, a1, s1
[0x80002b58]:lui s1, 1
[0x80002b5c]:addiw s1, s1, 2048
[0x80002b60]:add a1, a1, s1
[0x80002b64]:ld t4, 1336(a1)
[0x80002b68]:sub a1, a1, s1
[0x80002b6c]:addi s1, zero, 0
[0x80002b70]:csrrw zero, fcsr, s1
[0x80002b74]:fmax.s t6, t5, t4

[0x80002b74]:fmax.s t6, t5, t4
[0x80002b78]:csrrs a2, fcsr, zero
[0x80002b7c]:sd t6, 1280(fp)
[0x80002b80]:sd a2, 1288(fp)
[0x80002b84]:lui s1, 1
[0x80002b88]:addiw s1, s1, 2048
[0x80002b8c]:add a1, a1, s1
[0x80002b90]:ld t5, 1344(a1)
[0x80002b94]:sub a1, a1, s1
[0x80002b98]:lui s1, 1
[0x80002b9c]:addiw s1, s1, 2048
[0x80002ba0]:add a1, a1, s1
[0x80002ba4]:ld t4, 1352(a1)
[0x80002ba8]:sub a1, a1, s1
[0x80002bac]:addi s1, zero, 0
[0x80002bb0]:csrrw zero, fcsr, s1
[0x80002bb4]:fmax.s t6, t5, t4

[0x80002bb4]:fmax.s t6, t5, t4
[0x80002bb8]:csrrs a2, fcsr, zero
[0x80002bbc]:sd t6, 1296(fp)
[0x80002bc0]:sd a2, 1304(fp)
[0x80002bc4]:lui s1, 1
[0x80002bc8]:addiw s1, s1, 2048
[0x80002bcc]:add a1, a1, s1
[0x80002bd0]:ld t5, 1360(a1)
[0x80002bd4]:sub a1, a1, s1
[0x80002bd8]:lui s1, 1
[0x80002bdc]:addiw s1, s1, 2048
[0x80002be0]:add a1, a1, s1
[0x80002be4]:ld t4, 1368(a1)
[0x80002be8]:sub a1, a1, s1
[0x80002bec]:addi s1, zero, 0
[0x80002bf0]:csrrw zero, fcsr, s1
[0x80002bf4]:fmax.s t6, t5, t4

[0x80002bf4]:fmax.s t6, t5, t4
[0x80002bf8]:csrrs a2, fcsr, zero
[0x80002bfc]:sd t6, 1312(fp)
[0x80002c00]:sd a2, 1320(fp)
[0x80002c04]:lui s1, 1
[0x80002c08]:addiw s1, s1, 2048
[0x80002c0c]:add a1, a1, s1
[0x80002c10]:ld t5, 1376(a1)
[0x80002c14]:sub a1, a1, s1
[0x80002c18]:lui s1, 1
[0x80002c1c]:addiw s1, s1, 2048
[0x80002c20]:add a1, a1, s1
[0x80002c24]:ld t4, 1384(a1)
[0x80002c28]:sub a1, a1, s1
[0x80002c2c]:addi s1, zero, 0
[0x80002c30]:csrrw zero, fcsr, s1
[0x80002c34]:fmax.s t6, t5, t4

[0x80002c34]:fmax.s t6, t5, t4
[0x80002c38]:csrrs a2, fcsr, zero
[0x80002c3c]:sd t6, 1328(fp)
[0x80002c40]:sd a2, 1336(fp)
[0x80002c44]:lui s1, 1
[0x80002c48]:addiw s1, s1, 2048
[0x80002c4c]:add a1, a1, s1
[0x80002c50]:ld t5, 1392(a1)
[0x80002c54]:sub a1, a1, s1
[0x80002c58]:lui s1, 1
[0x80002c5c]:addiw s1, s1, 2048
[0x80002c60]:add a1, a1, s1
[0x80002c64]:ld t4, 1400(a1)
[0x80002c68]:sub a1, a1, s1
[0x80002c6c]:addi s1, zero, 0
[0x80002c70]:csrrw zero, fcsr, s1
[0x80002c74]:fmax.s t6, t5, t4

[0x80002c74]:fmax.s t6, t5, t4
[0x80002c78]:csrrs a2, fcsr, zero
[0x80002c7c]:sd t6, 1344(fp)
[0x80002c80]:sd a2, 1352(fp)
[0x80002c84]:lui s1, 1
[0x80002c88]:addiw s1, s1, 2048
[0x80002c8c]:add a1, a1, s1
[0x80002c90]:ld t5, 1408(a1)
[0x80002c94]:sub a1, a1, s1
[0x80002c98]:lui s1, 1
[0x80002c9c]:addiw s1, s1, 2048
[0x80002ca0]:add a1, a1, s1
[0x80002ca4]:ld t4, 1416(a1)
[0x80002ca8]:sub a1, a1, s1
[0x80002cac]:addi s1, zero, 0
[0x80002cb0]:csrrw zero, fcsr, s1
[0x80002cb4]:fmax.s t6, t5, t4

[0x80002cb4]:fmax.s t6, t5, t4
[0x80002cb8]:csrrs a2, fcsr, zero
[0x80002cbc]:sd t6, 1360(fp)
[0x80002cc0]:sd a2, 1368(fp)
[0x80002cc4]:lui s1, 1
[0x80002cc8]:addiw s1, s1, 2048
[0x80002ccc]:add a1, a1, s1
[0x80002cd0]:ld t5, 1424(a1)
[0x80002cd4]:sub a1, a1, s1
[0x80002cd8]:lui s1, 1
[0x80002cdc]:addiw s1, s1, 2048
[0x80002ce0]:add a1, a1, s1
[0x80002ce4]:ld t4, 1432(a1)
[0x80002ce8]:sub a1, a1, s1
[0x80002cec]:addi s1, zero, 0
[0x80002cf0]:csrrw zero, fcsr, s1
[0x80002cf4]:fmax.s t6, t5, t4

[0x80002cf4]:fmax.s t6, t5, t4
[0x80002cf8]:csrrs a2, fcsr, zero
[0x80002cfc]:sd t6, 1376(fp)
[0x80002d00]:sd a2, 1384(fp)
[0x80002d04]:lui s1, 1
[0x80002d08]:addiw s1, s1, 2048
[0x80002d0c]:add a1, a1, s1
[0x80002d10]:ld t5, 1440(a1)
[0x80002d14]:sub a1, a1, s1
[0x80002d18]:lui s1, 1
[0x80002d1c]:addiw s1, s1, 2048
[0x80002d20]:add a1, a1, s1
[0x80002d24]:ld t4, 1448(a1)
[0x80002d28]:sub a1, a1, s1
[0x80002d2c]:addi s1, zero, 0
[0x80002d30]:csrrw zero, fcsr, s1
[0x80002d34]:fmax.s t6, t5, t4

[0x80002d34]:fmax.s t6, t5, t4
[0x80002d38]:csrrs a2, fcsr, zero
[0x80002d3c]:sd t6, 1392(fp)
[0x80002d40]:sd a2, 1400(fp)
[0x80002d44]:lui s1, 1
[0x80002d48]:addiw s1, s1, 2048
[0x80002d4c]:add a1, a1, s1
[0x80002d50]:ld t5, 1456(a1)
[0x80002d54]:sub a1, a1, s1
[0x80002d58]:lui s1, 1
[0x80002d5c]:addiw s1, s1, 2048
[0x80002d60]:add a1, a1, s1
[0x80002d64]:ld t4, 1464(a1)
[0x80002d68]:sub a1, a1, s1
[0x80002d6c]:addi s1, zero, 0
[0x80002d70]:csrrw zero, fcsr, s1
[0x80002d74]:fmax.s t6, t5, t4

[0x80002d74]:fmax.s t6, t5, t4
[0x80002d78]:csrrs a2, fcsr, zero
[0x80002d7c]:sd t6, 1408(fp)
[0x80002d80]:sd a2, 1416(fp)
[0x80002d84]:lui s1, 1
[0x80002d88]:addiw s1, s1, 2048
[0x80002d8c]:add a1, a1, s1
[0x80002d90]:ld t5, 1472(a1)
[0x80002d94]:sub a1, a1, s1
[0x80002d98]:lui s1, 1
[0x80002d9c]:addiw s1, s1, 2048
[0x80002da0]:add a1, a1, s1
[0x80002da4]:ld t4, 1480(a1)
[0x80002da8]:sub a1, a1, s1
[0x80002dac]:addi s1, zero, 0
[0x80002db0]:csrrw zero, fcsr, s1
[0x80002db4]:fmax.s t6, t5, t4

[0x80002db4]:fmax.s t6, t5, t4
[0x80002db8]:csrrs a2, fcsr, zero
[0x80002dbc]:sd t6, 1424(fp)
[0x80002dc0]:sd a2, 1432(fp)
[0x80002dc4]:lui s1, 1
[0x80002dc8]:addiw s1, s1, 2048
[0x80002dcc]:add a1, a1, s1
[0x80002dd0]:ld t5, 1488(a1)
[0x80002dd4]:sub a1, a1, s1
[0x80002dd8]:lui s1, 1
[0x80002ddc]:addiw s1, s1, 2048
[0x80002de0]:add a1, a1, s1
[0x80002de4]:ld t4, 1496(a1)
[0x80002de8]:sub a1, a1, s1
[0x80002dec]:addi s1, zero, 0
[0x80002df0]:csrrw zero, fcsr, s1
[0x80002df4]:fmax.s t6, t5, t4

[0x80002df4]:fmax.s t6, t5, t4
[0x80002df8]:csrrs a2, fcsr, zero
[0x80002dfc]:sd t6, 1440(fp)
[0x80002e00]:sd a2, 1448(fp)
[0x80002e04]:lui s1, 1
[0x80002e08]:addiw s1, s1, 2048
[0x80002e0c]:add a1, a1, s1
[0x80002e10]:ld t5, 1504(a1)
[0x80002e14]:sub a1, a1, s1
[0x80002e18]:lui s1, 1
[0x80002e1c]:addiw s1, s1, 2048
[0x80002e20]:add a1, a1, s1
[0x80002e24]:ld t4, 1512(a1)
[0x80002e28]:sub a1, a1, s1
[0x80002e2c]:addi s1, zero, 0
[0x80002e30]:csrrw zero, fcsr, s1
[0x80002e34]:fmax.s t6, t5, t4

[0x80002e34]:fmax.s t6, t5, t4
[0x80002e38]:csrrs a2, fcsr, zero
[0x80002e3c]:sd t6, 1456(fp)
[0x80002e40]:sd a2, 1464(fp)
[0x80002e44]:lui s1, 1
[0x80002e48]:addiw s1, s1, 2048
[0x80002e4c]:add a1, a1, s1
[0x80002e50]:ld t5, 1520(a1)
[0x80002e54]:sub a1, a1, s1
[0x80002e58]:lui s1, 1
[0x80002e5c]:addiw s1, s1, 2048
[0x80002e60]:add a1, a1, s1
[0x80002e64]:ld t4, 1528(a1)
[0x80002e68]:sub a1, a1, s1
[0x80002e6c]:addi s1, zero, 0
[0x80002e70]:csrrw zero, fcsr, s1
[0x80002e74]:fmax.s t6, t5, t4

[0x80002e74]:fmax.s t6, t5, t4
[0x80002e78]:csrrs a2, fcsr, zero
[0x80002e7c]:sd t6, 1472(fp)
[0x80002e80]:sd a2, 1480(fp)
[0x80002e84]:lui s1, 1
[0x80002e88]:addiw s1, s1, 2048
[0x80002e8c]:add a1, a1, s1
[0x80002e90]:ld t5, 1536(a1)
[0x80002e94]:sub a1, a1, s1
[0x80002e98]:lui s1, 1
[0x80002e9c]:addiw s1, s1, 2048
[0x80002ea0]:add a1, a1, s1
[0x80002ea4]:ld t4, 1544(a1)
[0x80002ea8]:sub a1, a1, s1
[0x80002eac]:addi s1, zero, 0
[0x80002eb0]:csrrw zero, fcsr, s1
[0x80002eb4]:fmax.s t6, t5, t4

[0x80002eb4]:fmax.s t6, t5, t4
[0x80002eb8]:csrrs a2, fcsr, zero
[0x80002ebc]:sd t6, 1488(fp)
[0x80002ec0]:sd a2, 1496(fp)
[0x80002ec4]:lui s1, 1
[0x80002ec8]:addiw s1, s1, 2048
[0x80002ecc]:add a1, a1, s1
[0x80002ed0]:ld t5, 1552(a1)
[0x80002ed4]:sub a1, a1, s1
[0x80002ed8]:lui s1, 1
[0x80002edc]:addiw s1, s1, 2048
[0x80002ee0]:add a1, a1, s1
[0x80002ee4]:ld t4, 1560(a1)
[0x80002ee8]:sub a1, a1, s1
[0x80002eec]:addi s1, zero, 0
[0x80002ef0]:csrrw zero, fcsr, s1
[0x80002ef4]:fmax.s t6, t5, t4

[0x80002ef4]:fmax.s t6, t5, t4
[0x80002ef8]:csrrs a2, fcsr, zero
[0x80002efc]:sd t6, 1504(fp)
[0x80002f00]:sd a2, 1512(fp)
[0x80002f04]:lui s1, 1
[0x80002f08]:addiw s1, s1, 2048
[0x80002f0c]:add a1, a1, s1
[0x80002f10]:ld t5, 1568(a1)
[0x80002f14]:sub a1, a1, s1
[0x80002f18]:lui s1, 1
[0x80002f1c]:addiw s1, s1, 2048
[0x80002f20]:add a1, a1, s1
[0x80002f24]:ld t4, 1576(a1)
[0x80002f28]:sub a1, a1, s1
[0x80002f2c]:addi s1, zero, 0
[0x80002f30]:csrrw zero, fcsr, s1
[0x80002f34]:fmax.s t6, t5, t4

[0x80002f34]:fmax.s t6, t5, t4
[0x80002f38]:csrrs a2, fcsr, zero
[0x80002f3c]:sd t6, 1520(fp)
[0x80002f40]:sd a2, 1528(fp)
[0x80002f44]:lui s1, 1
[0x80002f48]:addiw s1, s1, 2048
[0x80002f4c]:add a1, a1, s1
[0x80002f50]:ld t5, 1584(a1)
[0x80002f54]:sub a1, a1, s1
[0x80002f58]:lui s1, 1
[0x80002f5c]:addiw s1, s1, 2048
[0x80002f60]:add a1, a1, s1
[0x80002f64]:ld t4, 1592(a1)
[0x80002f68]:sub a1, a1, s1
[0x80002f6c]:addi s1, zero, 0
[0x80002f70]:csrrw zero, fcsr, s1
[0x80002f74]:fmax.s t6, t5, t4

[0x80002f74]:fmax.s t6, t5, t4
[0x80002f78]:csrrs a2, fcsr, zero
[0x80002f7c]:sd t6, 1536(fp)
[0x80002f80]:sd a2, 1544(fp)
[0x80002f84]:lui s1, 1
[0x80002f88]:addiw s1, s1, 2048
[0x80002f8c]:add a1, a1, s1
[0x80002f90]:ld t5, 1600(a1)
[0x80002f94]:sub a1, a1, s1
[0x80002f98]:lui s1, 1
[0x80002f9c]:addiw s1, s1, 2048
[0x80002fa0]:add a1, a1, s1
[0x80002fa4]:ld t4, 1608(a1)
[0x80002fa8]:sub a1, a1, s1
[0x80002fac]:addi s1, zero, 0
[0x80002fb0]:csrrw zero, fcsr, s1
[0x80002fb4]:fmax.s t6, t5, t4

[0x80002fb4]:fmax.s t6, t5, t4
[0x80002fb8]:csrrs a2, fcsr, zero
[0x80002fbc]:sd t6, 1552(fp)
[0x80002fc0]:sd a2, 1560(fp)
[0x80002fc4]:lui s1, 1
[0x80002fc8]:addiw s1, s1, 2048
[0x80002fcc]:add a1, a1, s1
[0x80002fd0]:ld t5, 1616(a1)
[0x80002fd4]:sub a1, a1, s1
[0x80002fd8]:lui s1, 1
[0x80002fdc]:addiw s1, s1, 2048
[0x80002fe0]:add a1, a1, s1
[0x80002fe4]:ld t4, 1624(a1)
[0x80002fe8]:sub a1, a1, s1
[0x80002fec]:addi s1, zero, 0
[0x80002ff0]:csrrw zero, fcsr, s1
[0x80002ff4]:fmax.s t6, t5, t4

[0x80002ff4]:fmax.s t6, t5, t4
[0x80002ff8]:csrrs a2, fcsr, zero
[0x80002ffc]:sd t6, 1568(fp)
[0x80003000]:sd a2, 1576(fp)
[0x80003004]:lui s1, 1
[0x80003008]:addiw s1, s1, 2048
[0x8000300c]:add a1, a1, s1
[0x80003010]:ld t5, 1632(a1)
[0x80003014]:sub a1, a1, s1
[0x80003018]:lui s1, 1
[0x8000301c]:addiw s1, s1, 2048
[0x80003020]:add a1, a1, s1
[0x80003024]:ld t4, 1640(a1)
[0x80003028]:sub a1, a1, s1
[0x8000302c]:addi s1, zero, 0
[0x80003030]:csrrw zero, fcsr, s1
[0x80003034]:fmax.s t6, t5, t4

[0x80003034]:fmax.s t6, t5, t4
[0x80003038]:csrrs a2, fcsr, zero
[0x8000303c]:sd t6, 1584(fp)
[0x80003040]:sd a2, 1592(fp)
[0x80003044]:lui s1, 1
[0x80003048]:addiw s1, s1, 2048
[0x8000304c]:add a1, a1, s1
[0x80003050]:ld t5, 1648(a1)
[0x80003054]:sub a1, a1, s1
[0x80003058]:lui s1, 1
[0x8000305c]:addiw s1, s1, 2048
[0x80003060]:add a1, a1, s1
[0x80003064]:ld t4, 1656(a1)
[0x80003068]:sub a1, a1, s1
[0x8000306c]:addi s1, zero, 0
[0x80003070]:csrrw zero, fcsr, s1
[0x80003074]:fmax.s t6, t5, t4

[0x80003074]:fmax.s t6, t5, t4
[0x80003078]:csrrs a2, fcsr, zero
[0x8000307c]:sd t6, 1600(fp)
[0x80003080]:sd a2, 1608(fp)
[0x80003084]:lui s1, 1
[0x80003088]:addiw s1, s1, 2048
[0x8000308c]:add a1, a1, s1
[0x80003090]:ld t5, 1664(a1)
[0x80003094]:sub a1, a1, s1
[0x80003098]:lui s1, 1
[0x8000309c]:addiw s1, s1, 2048
[0x800030a0]:add a1, a1, s1
[0x800030a4]:ld t4, 1672(a1)
[0x800030a8]:sub a1, a1, s1
[0x800030ac]:addi s1, zero, 0
[0x800030b0]:csrrw zero, fcsr, s1
[0x800030b4]:fmax.s t6, t5, t4

[0x800030b4]:fmax.s t6, t5, t4
[0x800030b8]:csrrs a2, fcsr, zero
[0x800030bc]:sd t6, 1616(fp)
[0x800030c0]:sd a2, 1624(fp)
[0x800030c4]:lui s1, 1
[0x800030c8]:addiw s1, s1, 2048
[0x800030cc]:add a1, a1, s1
[0x800030d0]:ld t5, 1680(a1)
[0x800030d4]:sub a1, a1, s1
[0x800030d8]:lui s1, 1
[0x800030dc]:addiw s1, s1, 2048
[0x800030e0]:add a1, a1, s1
[0x800030e4]:ld t4, 1688(a1)
[0x800030e8]:sub a1, a1, s1
[0x800030ec]:addi s1, zero, 0
[0x800030f0]:csrrw zero, fcsr, s1
[0x800030f4]:fmax.s t6, t5, t4

[0x800030f4]:fmax.s t6, t5, t4
[0x800030f8]:csrrs a2, fcsr, zero
[0x800030fc]:sd t6, 1632(fp)
[0x80003100]:sd a2, 1640(fp)
[0x80003104]:lui s1, 1
[0x80003108]:addiw s1, s1, 2048
[0x8000310c]:add a1, a1, s1
[0x80003110]:ld t5, 1696(a1)
[0x80003114]:sub a1, a1, s1
[0x80003118]:lui s1, 1
[0x8000311c]:addiw s1, s1, 2048
[0x80003120]:add a1, a1, s1
[0x80003124]:ld t4, 1704(a1)
[0x80003128]:sub a1, a1, s1
[0x8000312c]:addi s1, zero, 0
[0x80003130]:csrrw zero, fcsr, s1
[0x80003134]:fmax.s t6, t5, t4

[0x80003134]:fmax.s t6, t5, t4
[0x80003138]:csrrs a2, fcsr, zero
[0x8000313c]:sd t6, 1648(fp)
[0x80003140]:sd a2, 1656(fp)
[0x80003144]:lui s1, 1
[0x80003148]:addiw s1, s1, 2048
[0x8000314c]:add a1, a1, s1
[0x80003150]:ld t5, 1712(a1)
[0x80003154]:sub a1, a1, s1
[0x80003158]:lui s1, 1
[0x8000315c]:addiw s1, s1, 2048
[0x80003160]:add a1, a1, s1
[0x80003164]:ld t4, 1720(a1)
[0x80003168]:sub a1, a1, s1
[0x8000316c]:addi s1, zero, 0
[0x80003170]:csrrw zero, fcsr, s1
[0x80003174]:fmax.s t6, t5, t4

[0x80003174]:fmax.s t6, t5, t4
[0x80003178]:csrrs a2, fcsr, zero
[0x8000317c]:sd t6, 1664(fp)
[0x80003180]:sd a2, 1672(fp)
[0x80003184]:lui s1, 1
[0x80003188]:addiw s1, s1, 2048
[0x8000318c]:add a1, a1, s1
[0x80003190]:ld t5, 1728(a1)
[0x80003194]:sub a1, a1, s1
[0x80003198]:lui s1, 1
[0x8000319c]:addiw s1, s1, 2048
[0x800031a0]:add a1, a1, s1
[0x800031a4]:ld t4, 1736(a1)
[0x800031a8]:sub a1, a1, s1
[0x800031ac]:addi s1, zero, 0
[0x800031b0]:csrrw zero, fcsr, s1
[0x800031b4]:fmax.s t6, t5, t4

[0x800031b4]:fmax.s t6, t5, t4
[0x800031b8]:csrrs a2, fcsr, zero
[0x800031bc]:sd t6, 1680(fp)
[0x800031c0]:sd a2, 1688(fp)
[0x800031c4]:lui s1, 1
[0x800031c8]:addiw s1, s1, 2048
[0x800031cc]:add a1, a1, s1
[0x800031d0]:ld t5, 1744(a1)
[0x800031d4]:sub a1, a1, s1
[0x800031d8]:lui s1, 1
[0x800031dc]:addiw s1, s1, 2048
[0x800031e0]:add a1, a1, s1
[0x800031e4]:ld t4, 1752(a1)
[0x800031e8]:sub a1, a1, s1
[0x800031ec]:addi s1, zero, 0
[0x800031f0]:csrrw zero, fcsr, s1
[0x800031f4]:fmax.s t6, t5, t4

[0x800031f4]:fmax.s t6, t5, t4
[0x800031f8]:csrrs a2, fcsr, zero
[0x800031fc]:sd t6, 1696(fp)
[0x80003200]:sd a2, 1704(fp)
[0x80003204]:lui s1, 1
[0x80003208]:addiw s1, s1, 2048
[0x8000320c]:add a1, a1, s1
[0x80003210]:ld t5, 1760(a1)
[0x80003214]:sub a1, a1, s1
[0x80003218]:lui s1, 1
[0x8000321c]:addiw s1, s1, 2048
[0x80003220]:add a1, a1, s1
[0x80003224]:ld t4, 1768(a1)
[0x80003228]:sub a1, a1, s1
[0x8000322c]:addi s1, zero, 0
[0x80003230]:csrrw zero, fcsr, s1
[0x80003234]:fmax.s t6, t5, t4

[0x80003234]:fmax.s t6, t5, t4
[0x80003238]:csrrs a2, fcsr, zero
[0x8000323c]:sd t6, 1712(fp)
[0x80003240]:sd a2, 1720(fp)
[0x80003244]:lui s1, 1
[0x80003248]:addiw s1, s1, 2048
[0x8000324c]:add a1, a1, s1
[0x80003250]:ld t5, 1776(a1)
[0x80003254]:sub a1, a1, s1
[0x80003258]:lui s1, 1
[0x8000325c]:addiw s1, s1, 2048
[0x80003260]:add a1, a1, s1
[0x80003264]:ld t4, 1784(a1)
[0x80003268]:sub a1, a1, s1
[0x8000326c]:addi s1, zero, 0
[0x80003270]:csrrw zero, fcsr, s1
[0x80003274]:fmax.s t6, t5, t4

[0x80003274]:fmax.s t6, t5, t4
[0x80003278]:csrrs a2, fcsr, zero
[0x8000327c]:sd t6, 1728(fp)
[0x80003280]:sd a2, 1736(fp)
[0x80003284]:lui s1, 1
[0x80003288]:addiw s1, s1, 2048
[0x8000328c]:add a1, a1, s1
[0x80003290]:ld t5, 1792(a1)
[0x80003294]:sub a1, a1, s1
[0x80003298]:lui s1, 1
[0x8000329c]:addiw s1, s1, 2048
[0x800032a0]:add a1, a1, s1
[0x800032a4]:ld t4, 1800(a1)
[0x800032a8]:sub a1, a1, s1
[0x800032ac]:addi s1, zero, 0
[0x800032b0]:csrrw zero, fcsr, s1
[0x800032b4]:fmax.s t6, t5, t4

[0x800032b4]:fmax.s t6, t5, t4
[0x800032b8]:csrrs a2, fcsr, zero
[0x800032bc]:sd t6, 1744(fp)
[0x800032c0]:sd a2, 1752(fp)
[0x800032c4]:lui s1, 1
[0x800032c8]:addiw s1, s1, 2048
[0x800032cc]:add a1, a1, s1
[0x800032d0]:ld t5, 1808(a1)
[0x800032d4]:sub a1, a1, s1
[0x800032d8]:lui s1, 1
[0x800032dc]:addiw s1, s1, 2048
[0x800032e0]:add a1, a1, s1
[0x800032e4]:ld t4, 1816(a1)
[0x800032e8]:sub a1, a1, s1
[0x800032ec]:addi s1, zero, 0
[0x800032f0]:csrrw zero, fcsr, s1
[0x800032f4]:fmax.s t6, t5, t4

[0x800032f4]:fmax.s t6, t5, t4
[0x800032f8]:csrrs a2, fcsr, zero
[0x800032fc]:sd t6, 1760(fp)
[0x80003300]:sd a2, 1768(fp)
[0x80003304]:lui s1, 1
[0x80003308]:addiw s1, s1, 2048
[0x8000330c]:add a1, a1, s1
[0x80003310]:ld t5, 1824(a1)
[0x80003314]:sub a1, a1, s1
[0x80003318]:lui s1, 1
[0x8000331c]:addiw s1, s1, 2048
[0x80003320]:add a1, a1, s1
[0x80003324]:ld t4, 1832(a1)
[0x80003328]:sub a1, a1, s1
[0x8000332c]:addi s1, zero, 0
[0x80003330]:csrrw zero, fcsr, s1
[0x80003334]:fmax.s t6, t5, t4

[0x80003334]:fmax.s t6, t5, t4
[0x80003338]:csrrs a2, fcsr, zero
[0x8000333c]:sd t6, 1776(fp)
[0x80003340]:sd a2, 1784(fp)
[0x80003344]:lui s1, 1
[0x80003348]:addiw s1, s1, 2048
[0x8000334c]:add a1, a1, s1
[0x80003350]:ld t5, 1840(a1)
[0x80003354]:sub a1, a1, s1
[0x80003358]:lui s1, 1
[0x8000335c]:addiw s1, s1, 2048
[0x80003360]:add a1, a1, s1
[0x80003364]:ld t4, 1848(a1)
[0x80003368]:sub a1, a1, s1
[0x8000336c]:addi s1, zero, 0
[0x80003370]:csrrw zero, fcsr, s1
[0x80003374]:fmax.s t6, t5, t4

[0x80003374]:fmax.s t6, t5, t4
[0x80003378]:csrrs a2, fcsr, zero
[0x8000337c]:sd t6, 1792(fp)
[0x80003380]:sd a2, 1800(fp)
[0x80003384]:lui s1, 1
[0x80003388]:addiw s1, s1, 2048
[0x8000338c]:add a1, a1, s1
[0x80003390]:ld t5, 1856(a1)
[0x80003394]:sub a1, a1, s1
[0x80003398]:lui s1, 1
[0x8000339c]:addiw s1, s1, 2048
[0x800033a0]:add a1, a1, s1
[0x800033a4]:ld t4, 1864(a1)
[0x800033a8]:sub a1, a1, s1
[0x800033ac]:addi s1, zero, 0
[0x800033b0]:csrrw zero, fcsr, s1
[0x800033b4]:fmax.s t6, t5, t4

[0x800033b4]:fmax.s t6, t5, t4
[0x800033b8]:csrrs a2, fcsr, zero
[0x800033bc]:sd t6, 1808(fp)
[0x800033c0]:sd a2, 1816(fp)
[0x800033c4]:lui s1, 1
[0x800033c8]:addiw s1, s1, 2048
[0x800033cc]:add a1, a1, s1
[0x800033d0]:ld t5, 1872(a1)
[0x800033d4]:sub a1, a1, s1
[0x800033d8]:lui s1, 1
[0x800033dc]:addiw s1, s1, 2048
[0x800033e0]:add a1, a1, s1
[0x800033e4]:ld t4, 1880(a1)
[0x800033e8]:sub a1, a1, s1
[0x800033ec]:addi s1, zero, 0
[0x800033f0]:csrrw zero, fcsr, s1
[0x800033f4]:fmax.s t6, t5, t4

[0x800033f4]:fmax.s t6, t5, t4
[0x800033f8]:csrrs a2, fcsr, zero
[0x800033fc]:sd t6, 1824(fp)
[0x80003400]:sd a2, 1832(fp)
[0x80003404]:lui s1, 1
[0x80003408]:addiw s1, s1, 2048
[0x8000340c]:add a1, a1, s1
[0x80003410]:ld t5, 1888(a1)
[0x80003414]:sub a1, a1, s1
[0x80003418]:lui s1, 1
[0x8000341c]:addiw s1, s1, 2048
[0x80003420]:add a1, a1, s1
[0x80003424]:ld t4, 1896(a1)
[0x80003428]:sub a1, a1, s1
[0x8000342c]:addi s1, zero, 0
[0x80003430]:csrrw zero, fcsr, s1
[0x80003434]:fmax.s t6, t5, t4

[0x80003434]:fmax.s t6, t5, t4
[0x80003438]:csrrs a2, fcsr, zero
[0x8000343c]:sd t6, 1840(fp)
[0x80003440]:sd a2, 1848(fp)
[0x80003444]:lui s1, 1
[0x80003448]:addiw s1, s1, 2048
[0x8000344c]:add a1, a1, s1
[0x80003450]:ld t5, 1904(a1)
[0x80003454]:sub a1, a1, s1
[0x80003458]:lui s1, 1
[0x8000345c]:addiw s1, s1, 2048
[0x80003460]:add a1, a1, s1
[0x80003464]:ld t4, 1912(a1)
[0x80003468]:sub a1, a1, s1
[0x8000346c]:addi s1, zero, 0
[0x80003470]:csrrw zero, fcsr, s1
[0x80003474]:fmax.s t6, t5, t4

[0x80003474]:fmax.s t6, t5, t4
[0x80003478]:csrrs a2, fcsr, zero
[0x8000347c]:sd t6, 1856(fp)
[0x80003480]:sd a2, 1864(fp)
[0x80003484]:lui s1, 1
[0x80003488]:addiw s1, s1, 2048
[0x8000348c]:add a1, a1, s1
[0x80003490]:ld t5, 1920(a1)
[0x80003494]:sub a1, a1, s1
[0x80003498]:lui s1, 1
[0x8000349c]:addiw s1, s1, 2048
[0x800034a0]:add a1, a1, s1
[0x800034a4]:ld t4, 1928(a1)
[0x800034a8]:sub a1, a1, s1
[0x800034ac]:addi s1, zero, 0
[0x800034b0]:csrrw zero, fcsr, s1
[0x800034b4]:fmax.s t6, t5, t4

[0x800034b4]:fmax.s t6, t5, t4
[0x800034b8]:csrrs a2, fcsr, zero
[0x800034bc]:sd t6, 1872(fp)
[0x800034c0]:sd a2, 1880(fp)
[0x800034c4]:lui s1, 1
[0x800034c8]:addiw s1, s1, 2048
[0x800034cc]:add a1, a1, s1
[0x800034d0]:ld t5, 1936(a1)
[0x800034d4]:sub a1, a1, s1
[0x800034d8]:lui s1, 1
[0x800034dc]:addiw s1, s1, 2048
[0x800034e0]:add a1, a1, s1
[0x800034e4]:ld t4, 1944(a1)
[0x800034e8]:sub a1, a1, s1
[0x800034ec]:addi s1, zero, 0
[0x800034f0]:csrrw zero, fcsr, s1
[0x800034f4]:fmax.s t6, t5, t4

[0x800034f4]:fmax.s t6, t5, t4
[0x800034f8]:csrrs a2, fcsr, zero
[0x800034fc]:sd t6, 1888(fp)
[0x80003500]:sd a2, 1896(fp)
[0x80003504]:lui s1, 1
[0x80003508]:addiw s1, s1, 2048
[0x8000350c]:add a1, a1, s1
[0x80003510]:ld t5, 1952(a1)
[0x80003514]:sub a1, a1, s1
[0x80003518]:lui s1, 1
[0x8000351c]:addiw s1, s1, 2048
[0x80003520]:add a1, a1, s1
[0x80003524]:ld t4, 1960(a1)
[0x80003528]:sub a1, a1, s1
[0x8000352c]:addi s1, zero, 0
[0x80003530]:csrrw zero, fcsr, s1
[0x80003534]:fmax.s t6, t5, t4

[0x80003534]:fmax.s t6, t5, t4
[0x80003538]:csrrs a2, fcsr, zero
[0x8000353c]:sd t6, 1904(fp)
[0x80003540]:sd a2, 1912(fp)
[0x80003544]:lui s1, 1
[0x80003548]:addiw s1, s1, 2048
[0x8000354c]:add a1, a1, s1
[0x80003550]:ld t5, 1968(a1)
[0x80003554]:sub a1, a1, s1
[0x80003558]:lui s1, 1
[0x8000355c]:addiw s1, s1, 2048
[0x80003560]:add a1, a1, s1
[0x80003564]:ld t4, 1976(a1)
[0x80003568]:sub a1, a1, s1
[0x8000356c]:addi s1, zero, 0
[0x80003570]:csrrw zero, fcsr, s1
[0x80003574]:fmax.s t6, t5, t4

[0x80003574]:fmax.s t6, t5, t4
[0x80003578]:csrrs a2, fcsr, zero
[0x8000357c]:sd t6, 1920(fp)
[0x80003580]:sd a2, 1928(fp)
[0x80003584]:lui s1, 1
[0x80003588]:addiw s1, s1, 2048
[0x8000358c]:add a1, a1, s1
[0x80003590]:ld t5, 1984(a1)
[0x80003594]:sub a1, a1, s1
[0x80003598]:lui s1, 1
[0x8000359c]:addiw s1, s1, 2048
[0x800035a0]:add a1, a1, s1
[0x800035a4]:ld t4, 1992(a1)
[0x800035a8]:sub a1, a1, s1
[0x800035ac]:addi s1, zero, 0
[0x800035b0]:csrrw zero, fcsr, s1
[0x800035b4]:fmax.s t6, t5, t4

[0x800035b4]:fmax.s t6, t5, t4
[0x800035b8]:csrrs a2, fcsr, zero
[0x800035bc]:sd t6, 1936(fp)
[0x800035c0]:sd a2, 1944(fp)
[0x800035c4]:lui s1, 1
[0x800035c8]:addiw s1, s1, 2048
[0x800035cc]:add a1, a1, s1
[0x800035d0]:ld t5, 2000(a1)
[0x800035d4]:sub a1, a1, s1
[0x800035d8]:lui s1, 1
[0x800035dc]:addiw s1, s1, 2048
[0x800035e0]:add a1, a1, s1
[0x800035e4]:ld t4, 2008(a1)
[0x800035e8]:sub a1, a1, s1
[0x800035ec]:addi s1, zero, 0
[0x800035f0]:csrrw zero, fcsr, s1
[0x800035f4]:fmax.s t6, t5, t4

[0x800035f4]:fmax.s t6, t5, t4
[0x800035f8]:csrrs a2, fcsr, zero
[0x800035fc]:sd t6, 1952(fp)
[0x80003600]:sd a2, 1960(fp)
[0x80003604]:lui s1, 1
[0x80003608]:addiw s1, s1, 2048
[0x8000360c]:add a1, a1, s1
[0x80003610]:ld t5, 2016(a1)
[0x80003614]:sub a1, a1, s1
[0x80003618]:lui s1, 1
[0x8000361c]:addiw s1, s1, 2048
[0x80003620]:add a1, a1, s1
[0x80003624]:ld t4, 2024(a1)
[0x80003628]:sub a1, a1, s1
[0x8000362c]:addi s1, zero, 0
[0x80003630]:csrrw zero, fcsr, s1
[0x80003634]:fmax.s t6, t5, t4

[0x80003634]:fmax.s t6, t5, t4
[0x80003638]:csrrs a2, fcsr, zero
[0x8000363c]:sd t6, 1968(fp)
[0x80003640]:sd a2, 1976(fp)
[0x80003644]:lui s1, 1
[0x80003648]:addiw s1, s1, 2048
[0x8000364c]:add a1, a1, s1
[0x80003650]:ld t5, 2032(a1)
[0x80003654]:sub a1, a1, s1
[0x80003658]:lui s1, 1
[0x8000365c]:addiw s1, s1, 2048
[0x80003660]:add a1, a1, s1
[0x80003664]:ld t4, 2040(a1)
[0x80003668]:sub a1, a1, s1
[0x8000366c]:addi s1, zero, 0
[0x80003670]:csrrw zero, fcsr, s1
[0x80003674]:fmax.s t6, t5, t4

[0x80003674]:fmax.s t6, t5, t4
[0x80003678]:csrrs a2, fcsr, zero
[0x8000367c]:sd t6, 1984(fp)
[0x80003680]:sd a2, 1992(fp)
[0x80003684]:lui s1, 1
[0x80003688]:add a1, a1, s1
[0x8000368c]:ld t5, 0(a1)
[0x80003690]:sub a1, a1, s1
[0x80003694]:lui s1, 1
[0x80003698]:add a1, a1, s1
[0x8000369c]:ld t4, 8(a1)
[0x800036a0]:sub a1, a1, s1
[0x800036a4]:addi s1, zero, 0
[0x800036a8]:csrrw zero, fcsr, s1
[0x800036ac]:fmax.s t6, t5, t4

[0x800036ac]:fmax.s t6, t5, t4
[0x800036b0]:csrrs a2, fcsr, zero
[0x800036b4]:sd t6, 2000(fp)
[0x800036b8]:sd a2, 2008(fp)
[0x800036bc]:lui s1, 1
[0x800036c0]:add a1, a1, s1
[0x800036c4]:ld t5, 16(a1)
[0x800036c8]:sub a1, a1, s1
[0x800036cc]:lui s1, 1
[0x800036d0]:add a1, a1, s1
[0x800036d4]:ld t4, 24(a1)
[0x800036d8]:sub a1, a1, s1
[0x800036dc]:addi s1, zero, 0
[0x800036e0]:csrrw zero, fcsr, s1
[0x800036e4]:fmax.s t6, t5, t4

[0x800036e4]:fmax.s t6, t5, t4
[0x800036e8]:csrrs a2, fcsr, zero
[0x800036ec]:sd t6, 2016(fp)
[0x800036f0]:sd a2, 2024(fp)
[0x800036f4]:lui s1, 1
[0x800036f8]:add a1, a1, s1
[0x800036fc]:ld t5, 32(a1)
[0x80003700]:sub a1, a1, s1
[0x80003704]:lui s1, 1
[0x80003708]:add a1, a1, s1
[0x8000370c]:ld t4, 40(a1)
[0x80003710]:sub a1, a1, s1
[0x80003714]:addi s1, zero, 0
[0x80003718]:csrrw zero, fcsr, s1
[0x8000371c]:fmax.s t6, t5, t4

[0x8000371c]:fmax.s t6, t5, t4
[0x80003720]:csrrs a2, fcsr, zero
[0x80003724]:sd t6, 2032(fp)
[0x80003728]:sd a2, 2040(fp)
[0x8000372c]:auipc fp, 17
[0x80003730]:addi fp, fp, 2172
[0x80003734]:lui s1, 1
[0x80003738]:add a1, a1, s1
[0x8000373c]:ld t5, 48(a1)
[0x80003740]:sub a1, a1, s1
[0x80003744]:lui s1, 1
[0x80003748]:add a1, a1, s1
[0x8000374c]:ld t4, 56(a1)
[0x80003750]:sub a1, a1, s1
[0x80003754]:addi s1, zero, 0
[0x80003758]:csrrw zero, fcsr, s1
[0x8000375c]:fmax.s t6, t5, t4

[0x8000375c]:fmax.s t6, t5, t4
[0x80003760]:csrrs a2, fcsr, zero
[0x80003764]:sd t6, 0(fp)
[0x80003768]:sd a2, 8(fp)
[0x8000376c]:lui s1, 1
[0x80003770]:add a1, a1, s1
[0x80003774]:ld t5, 64(a1)
[0x80003778]:sub a1, a1, s1
[0x8000377c]:lui s1, 1
[0x80003780]:add a1, a1, s1
[0x80003784]:ld t4, 72(a1)
[0x80003788]:sub a1, a1, s1
[0x8000378c]:addi s1, zero, 0
[0x80003790]:csrrw zero, fcsr, s1
[0x80003794]:fmax.s t6, t5, t4

[0x80003794]:fmax.s t6, t5, t4
[0x80003798]:csrrs a2, fcsr, zero
[0x8000379c]:sd t6, 16(fp)
[0x800037a0]:sd a2, 24(fp)
[0x800037a4]:lui s1, 1
[0x800037a8]:add a1, a1, s1
[0x800037ac]:ld t5, 80(a1)
[0x800037b0]:sub a1, a1, s1
[0x800037b4]:lui s1, 1
[0x800037b8]:add a1, a1, s1
[0x800037bc]:ld t4, 88(a1)
[0x800037c0]:sub a1, a1, s1
[0x800037c4]:addi s1, zero, 0
[0x800037c8]:csrrw zero, fcsr, s1
[0x800037cc]:fmax.s t6, t5, t4

[0x800037cc]:fmax.s t6, t5, t4
[0x800037d0]:csrrs a2, fcsr, zero
[0x800037d4]:sd t6, 32(fp)
[0x800037d8]:sd a2, 40(fp)
[0x800037dc]:lui s1, 1
[0x800037e0]:add a1, a1, s1
[0x800037e4]:ld t5, 96(a1)
[0x800037e8]:sub a1, a1, s1
[0x800037ec]:lui s1, 1
[0x800037f0]:add a1, a1, s1
[0x800037f4]:ld t4, 104(a1)
[0x800037f8]:sub a1, a1, s1
[0x800037fc]:addi s1, zero, 0
[0x80003800]:csrrw zero, fcsr, s1
[0x80003804]:fmax.s t6, t5, t4

[0x80003804]:fmax.s t6, t5, t4
[0x80003808]:csrrs a2, fcsr, zero
[0x8000380c]:sd t6, 48(fp)
[0x80003810]:sd a2, 56(fp)
[0x80003814]:lui s1, 1
[0x80003818]:add a1, a1, s1
[0x8000381c]:ld t5, 112(a1)
[0x80003820]:sub a1, a1, s1
[0x80003824]:lui s1, 1
[0x80003828]:add a1, a1, s1
[0x8000382c]:ld t4, 120(a1)
[0x80003830]:sub a1, a1, s1
[0x80003834]:addi s1, zero, 0
[0x80003838]:csrrw zero, fcsr, s1
[0x8000383c]:fmax.s t6, t5, t4

[0x8000383c]:fmax.s t6, t5, t4
[0x80003840]:csrrs a2, fcsr, zero
[0x80003844]:sd t6, 64(fp)
[0x80003848]:sd a2, 72(fp)
[0x8000384c]:lui s1, 1
[0x80003850]:add a1, a1, s1
[0x80003854]:ld t5, 128(a1)
[0x80003858]:sub a1, a1, s1
[0x8000385c]:lui s1, 1
[0x80003860]:add a1, a1, s1
[0x80003864]:ld t4, 136(a1)
[0x80003868]:sub a1, a1, s1
[0x8000386c]:addi s1, zero, 0
[0x80003870]:csrrw zero, fcsr, s1
[0x80003874]:fmax.s t6, t5, t4

[0x80003874]:fmax.s t6, t5, t4
[0x80003878]:csrrs a2, fcsr, zero
[0x8000387c]:sd t6, 80(fp)
[0x80003880]:sd a2, 88(fp)
[0x80003884]:lui s1, 1
[0x80003888]:add a1, a1, s1
[0x8000388c]:ld t5, 144(a1)
[0x80003890]:sub a1, a1, s1
[0x80003894]:lui s1, 1
[0x80003898]:add a1, a1, s1
[0x8000389c]:ld t4, 152(a1)
[0x800038a0]:sub a1, a1, s1
[0x800038a4]:addi s1, zero, 0
[0x800038a8]:csrrw zero, fcsr, s1
[0x800038ac]:fmax.s t6, t5, t4

[0x800038ac]:fmax.s t6, t5, t4
[0x800038b0]:csrrs a2, fcsr, zero
[0x800038b4]:sd t6, 96(fp)
[0x800038b8]:sd a2, 104(fp)
[0x800038bc]:lui s1, 1
[0x800038c0]:add a1, a1, s1
[0x800038c4]:ld t5, 160(a1)
[0x800038c8]:sub a1, a1, s1
[0x800038cc]:lui s1, 1
[0x800038d0]:add a1, a1, s1
[0x800038d4]:ld t4, 168(a1)
[0x800038d8]:sub a1, a1, s1
[0x800038dc]:addi s1, zero, 0
[0x800038e0]:csrrw zero, fcsr, s1
[0x800038e4]:fmax.s t6, t5, t4

[0x800038e4]:fmax.s t6, t5, t4
[0x800038e8]:csrrs a2, fcsr, zero
[0x800038ec]:sd t6, 112(fp)
[0x800038f0]:sd a2, 120(fp)
[0x800038f4]:lui s1, 1
[0x800038f8]:add a1, a1, s1
[0x800038fc]:ld t5, 176(a1)
[0x80003900]:sub a1, a1, s1
[0x80003904]:lui s1, 1
[0x80003908]:add a1, a1, s1
[0x8000390c]:ld t4, 184(a1)
[0x80003910]:sub a1, a1, s1
[0x80003914]:addi s1, zero, 0
[0x80003918]:csrrw zero, fcsr, s1
[0x8000391c]:fmax.s t6, t5, t4

[0x8000391c]:fmax.s t6, t5, t4
[0x80003920]:csrrs a2, fcsr, zero
[0x80003924]:sd t6, 128(fp)
[0x80003928]:sd a2, 136(fp)
[0x8000392c]:lui s1, 1
[0x80003930]:add a1, a1, s1
[0x80003934]:ld t5, 192(a1)
[0x80003938]:sub a1, a1, s1
[0x8000393c]:lui s1, 1
[0x80003940]:add a1, a1, s1
[0x80003944]:ld t4, 200(a1)
[0x80003948]:sub a1, a1, s1
[0x8000394c]:addi s1, zero, 0
[0x80003950]:csrrw zero, fcsr, s1
[0x80003954]:fmax.s t6, t5, t4

[0x80003954]:fmax.s t6, t5, t4
[0x80003958]:csrrs a2, fcsr, zero
[0x8000395c]:sd t6, 144(fp)
[0x80003960]:sd a2, 152(fp)
[0x80003964]:lui s1, 1
[0x80003968]:add a1, a1, s1
[0x8000396c]:ld t5, 208(a1)
[0x80003970]:sub a1, a1, s1
[0x80003974]:lui s1, 1
[0x80003978]:add a1, a1, s1
[0x8000397c]:ld t4, 216(a1)
[0x80003980]:sub a1, a1, s1
[0x80003984]:addi s1, zero, 0
[0x80003988]:csrrw zero, fcsr, s1
[0x8000398c]:fmax.s t6, t5, t4

[0x8000398c]:fmax.s t6, t5, t4
[0x80003990]:csrrs a2, fcsr, zero
[0x80003994]:sd t6, 160(fp)
[0x80003998]:sd a2, 168(fp)
[0x8000399c]:lui s1, 1
[0x800039a0]:add a1, a1, s1
[0x800039a4]:ld t5, 224(a1)
[0x800039a8]:sub a1, a1, s1
[0x800039ac]:lui s1, 1
[0x800039b0]:add a1, a1, s1
[0x800039b4]:ld t4, 232(a1)
[0x800039b8]:sub a1, a1, s1
[0x800039bc]:addi s1, zero, 0
[0x800039c0]:csrrw zero, fcsr, s1
[0x800039c4]:fmax.s t6, t5, t4

[0x800039c4]:fmax.s t6, t5, t4
[0x800039c8]:csrrs a2, fcsr, zero
[0x800039cc]:sd t6, 176(fp)
[0x800039d0]:sd a2, 184(fp)
[0x800039d4]:lui s1, 1
[0x800039d8]:add a1, a1, s1
[0x800039dc]:ld t5, 240(a1)
[0x800039e0]:sub a1, a1, s1
[0x800039e4]:lui s1, 1
[0x800039e8]:add a1, a1, s1
[0x800039ec]:ld t4, 248(a1)
[0x800039f0]:sub a1, a1, s1
[0x800039f4]:addi s1, zero, 0
[0x800039f8]:csrrw zero, fcsr, s1
[0x800039fc]:fmax.s t6, t5, t4

[0x800039fc]:fmax.s t6, t5, t4
[0x80003a00]:csrrs a2, fcsr, zero
[0x80003a04]:sd t6, 192(fp)
[0x80003a08]:sd a2, 200(fp)
[0x80003a0c]:lui s1, 1
[0x80003a10]:add a1, a1, s1
[0x80003a14]:ld t5, 256(a1)
[0x80003a18]:sub a1, a1, s1
[0x80003a1c]:lui s1, 1
[0x80003a20]:add a1, a1, s1
[0x80003a24]:ld t4, 264(a1)
[0x80003a28]:sub a1, a1, s1
[0x80003a2c]:addi s1, zero, 0
[0x80003a30]:csrrw zero, fcsr, s1
[0x80003a34]:fmax.s t6, t5, t4

[0x80003a34]:fmax.s t6, t5, t4
[0x80003a38]:csrrs a2, fcsr, zero
[0x80003a3c]:sd t6, 208(fp)
[0x80003a40]:sd a2, 216(fp)
[0x80003a44]:lui s1, 1
[0x80003a48]:add a1, a1, s1
[0x80003a4c]:ld t5, 272(a1)
[0x80003a50]:sub a1, a1, s1
[0x80003a54]:lui s1, 1
[0x80003a58]:add a1, a1, s1
[0x80003a5c]:ld t4, 280(a1)
[0x80003a60]:sub a1, a1, s1
[0x80003a64]:addi s1, zero, 0
[0x80003a68]:csrrw zero, fcsr, s1
[0x80003a6c]:fmax.s t6, t5, t4

[0x80003a6c]:fmax.s t6, t5, t4
[0x80003a70]:csrrs a2, fcsr, zero
[0x80003a74]:sd t6, 224(fp)
[0x80003a78]:sd a2, 232(fp)
[0x80003a7c]:lui s1, 1
[0x80003a80]:add a1, a1, s1
[0x80003a84]:ld t5, 288(a1)
[0x80003a88]:sub a1, a1, s1
[0x80003a8c]:lui s1, 1
[0x80003a90]:add a1, a1, s1
[0x80003a94]:ld t4, 296(a1)
[0x80003a98]:sub a1, a1, s1
[0x80003a9c]:addi s1, zero, 0
[0x80003aa0]:csrrw zero, fcsr, s1
[0x80003aa4]:fmax.s t6, t5, t4

[0x80003aa4]:fmax.s t6, t5, t4
[0x80003aa8]:csrrs a2, fcsr, zero
[0x80003aac]:sd t6, 240(fp)
[0x80003ab0]:sd a2, 248(fp)
[0x80003ab4]:lui s1, 1
[0x80003ab8]:add a1, a1, s1
[0x80003abc]:ld t5, 304(a1)
[0x80003ac0]:sub a1, a1, s1
[0x80003ac4]:lui s1, 1
[0x80003ac8]:add a1, a1, s1
[0x80003acc]:ld t4, 312(a1)
[0x80003ad0]:sub a1, a1, s1
[0x80003ad4]:addi s1, zero, 0
[0x80003ad8]:csrrw zero, fcsr, s1
[0x80003adc]:fmax.s t6, t5, t4

[0x80003adc]:fmax.s t6, t5, t4
[0x80003ae0]:csrrs a2, fcsr, zero
[0x80003ae4]:sd t6, 256(fp)
[0x80003ae8]:sd a2, 264(fp)
[0x80003aec]:lui s1, 1
[0x80003af0]:add a1, a1, s1
[0x80003af4]:ld t5, 320(a1)
[0x80003af8]:sub a1, a1, s1
[0x80003afc]:lui s1, 1
[0x80003b00]:add a1, a1, s1
[0x80003b04]:ld t4, 328(a1)
[0x80003b08]:sub a1, a1, s1
[0x80003b0c]:addi s1, zero, 0
[0x80003b10]:csrrw zero, fcsr, s1
[0x80003b14]:fmax.s t6, t5, t4

[0x80003b14]:fmax.s t6, t5, t4
[0x80003b18]:csrrs a2, fcsr, zero
[0x80003b1c]:sd t6, 272(fp)
[0x80003b20]:sd a2, 280(fp)
[0x80003b24]:lui s1, 1
[0x80003b28]:add a1, a1, s1
[0x80003b2c]:ld t5, 336(a1)
[0x80003b30]:sub a1, a1, s1
[0x80003b34]:lui s1, 1
[0x80003b38]:add a1, a1, s1
[0x80003b3c]:ld t4, 344(a1)
[0x80003b40]:sub a1, a1, s1
[0x80003b44]:addi s1, zero, 0
[0x80003b48]:csrrw zero, fcsr, s1
[0x80003b4c]:fmax.s t6, t5, t4

[0x80003b4c]:fmax.s t6, t5, t4
[0x80003b50]:csrrs a2, fcsr, zero
[0x80003b54]:sd t6, 288(fp)
[0x80003b58]:sd a2, 296(fp)
[0x80003b5c]:lui s1, 1
[0x80003b60]:add a1, a1, s1
[0x80003b64]:ld t5, 352(a1)
[0x80003b68]:sub a1, a1, s1
[0x80003b6c]:lui s1, 1
[0x80003b70]:add a1, a1, s1
[0x80003b74]:ld t4, 360(a1)
[0x80003b78]:sub a1, a1, s1
[0x80003b7c]:addi s1, zero, 0
[0x80003b80]:csrrw zero, fcsr, s1
[0x80003b84]:fmax.s t6, t5, t4

[0x80003b84]:fmax.s t6, t5, t4
[0x80003b88]:csrrs a2, fcsr, zero
[0x80003b8c]:sd t6, 304(fp)
[0x80003b90]:sd a2, 312(fp)
[0x80003b94]:lui s1, 1
[0x80003b98]:add a1, a1, s1
[0x80003b9c]:ld t5, 368(a1)
[0x80003ba0]:sub a1, a1, s1
[0x80003ba4]:lui s1, 1
[0x80003ba8]:add a1, a1, s1
[0x80003bac]:ld t4, 376(a1)
[0x80003bb0]:sub a1, a1, s1
[0x80003bb4]:addi s1, zero, 0
[0x80003bb8]:csrrw zero, fcsr, s1
[0x80003bbc]:fmax.s t6, t5, t4

[0x80003bbc]:fmax.s t6, t5, t4
[0x80003bc0]:csrrs a2, fcsr, zero
[0x80003bc4]:sd t6, 320(fp)
[0x80003bc8]:sd a2, 328(fp)
[0x80003bcc]:lui s1, 1
[0x80003bd0]:add a1, a1, s1
[0x80003bd4]:ld t5, 384(a1)
[0x80003bd8]:sub a1, a1, s1
[0x80003bdc]:lui s1, 1
[0x80003be0]:add a1, a1, s1
[0x80003be4]:ld t4, 392(a1)
[0x80003be8]:sub a1, a1, s1
[0x80003bec]:addi s1, zero, 0
[0x80003bf0]:csrrw zero, fcsr, s1
[0x80003bf4]:fmax.s t6, t5, t4

[0x80003bf4]:fmax.s t6, t5, t4
[0x80003bf8]:csrrs a2, fcsr, zero
[0x80003bfc]:sd t6, 336(fp)
[0x80003c00]:sd a2, 344(fp)
[0x80003c04]:lui s1, 1
[0x80003c08]:add a1, a1, s1
[0x80003c0c]:ld t5, 400(a1)
[0x80003c10]:sub a1, a1, s1
[0x80003c14]:lui s1, 1
[0x80003c18]:add a1, a1, s1
[0x80003c1c]:ld t4, 408(a1)
[0x80003c20]:sub a1, a1, s1
[0x80003c24]:addi s1, zero, 0
[0x80003c28]:csrrw zero, fcsr, s1
[0x80003c2c]:fmax.s t6, t5, t4

[0x80003c2c]:fmax.s t6, t5, t4
[0x80003c30]:csrrs a2, fcsr, zero
[0x80003c34]:sd t6, 352(fp)
[0x80003c38]:sd a2, 360(fp)
[0x80003c3c]:lui s1, 1
[0x80003c40]:add a1, a1, s1
[0x80003c44]:ld t5, 416(a1)
[0x80003c48]:sub a1, a1, s1
[0x80003c4c]:lui s1, 1
[0x80003c50]:add a1, a1, s1
[0x80003c54]:ld t4, 424(a1)
[0x80003c58]:sub a1, a1, s1
[0x80003c5c]:addi s1, zero, 0
[0x80003c60]:csrrw zero, fcsr, s1
[0x80003c64]:fmax.s t6, t5, t4

[0x80003c64]:fmax.s t6, t5, t4
[0x80003c68]:csrrs a2, fcsr, zero
[0x80003c6c]:sd t6, 368(fp)
[0x80003c70]:sd a2, 376(fp)
[0x80003c74]:lui s1, 1
[0x80003c78]:add a1, a1, s1
[0x80003c7c]:ld t5, 432(a1)
[0x80003c80]:sub a1, a1, s1
[0x80003c84]:lui s1, 1
[0x80003c88]:add a1, a1, s1
[0x80003c8c]:ld t4, 440(a1)
[0x80003c90]:sub a1, a1, s1
[0x80003c94]:addi s1, zero, 0
[0x80003c98]:csrrw zero, fcsr, s1
[0x80003c9c]:fmax.s t6, t5, t4

[0x80003c9c]:fmax.s t6, t5, t4
[0x80003ca0]:csrrs a2, fcsr, zero
[0x80003ca4]:sd t6, 384(fp)
[0x80003ca8]:sd a2, 392(fp)
[0x80003cac]:lui s1, 1
[0x80003cb0]:add a1, a1, s1
[0x80003cb4]:ld t5, 448(a1)
[0x80003cb8]:sub a1, a1, s1
[0x80003cbc]:lui s1, 1
[0x80003cc0]:add a1, a1, s1
[0x80003cc4]:ld t4, 456(a1)
[0x80003cc8]:sub a1, a1, s1
[0x80003ccc]:addi s1, zero, 0
[0x80003cd0]:csrrw zero, fcsr, s1
[0x80003cd4]:fmax.s t6, t5, t4

[0x80003cd4]:fmax.s t6, t5, t4
[0x80003cd8]:csrrs a2, fcsr, zero
[0x80003cdc]:sd t6, 400(fp)
[0x80003ce0]:sd a2, 408(fp)
[0x80003ce4]:lui s1, 1
[0x80003ce8]:add a1, a1, s1
[0x80003cec]:ld t5, 464(a1)
[0x80003cf0]:sub a1, a1, s1
[0x80003cf4]:lui s1, 1
[0x80003cf8]:add a1, a1, s1
[0x80003cfc]:ld t4, 472(a1)
[0x80003d00]:sub a1, a1, s1
[0x80003d04]:addi s1, zero, 0
[0x80003d08]:csrrw zero, fcsr, s1
[0x80003d0c]:fmax.s t6, t5, t4

[0x80003d0c]:fmax.s t6, t5, t4
[0x80003d10]:csrrs a2, fcsr, zero
[0x80003d14]:sd t6, 416(fp)
[0x80003d18]:sd a2, 424(fp)
[0x80003d1c]:lui s1, 1
[0x80003d20]:add a1, a1, s1
[0x80003d24]:ld t5, 480(a1)
[0x80003d28]:sub a1, a1, s1
[0x80003d2c]:lui s1, 1
[0x80003d30]:add a1, a1, s1
[0x80003d34]:ld t4, 488(a1)
[0x80003d38]:sub a1, a1, s1
[0x80003d3c]:addi s1, zero, 0
[0x80003d40]:csrrw zero, fcsr, s1
[0x80003d44]:fmax.s t6, t5, t4

[0x80003d44]:fmax.s t6, t5, t4
[0x80003d48]:csrrs a2, fcsr, zero
[0x80003d4c]:sd t6, 432(fp)
[0x80003d50]:sd a2, 440(fp)
[0x80003d54]:lui s1, 1
[0x80003d58]:add a1, a1, s1
[0x80003d5c]:ld t5, 496(a1)
[0x80003d60]:sub a1, a1, s1
[0x80003d64]:lui s1, 1
[0x80003d68]:add a1, a1, s1
[0x80003d6c]:ld t4, 504(a1)
[0x80003d70]:sub a1, a1, s1
[0x80003d74]:addi s1, zero, 0
[0x80003d78]:csrrw zero, fcsr, s1
[0x80003d7c]:fmax.s t6, t5, t4

[0x80003d7c]:fmax.s t6, t5, t4
[0x80003d80]:csrrs a2, fcsr, zero
[0x80003d84]:sd t6, 448(fp)
[0x80003d88]:sd a2, 456(fp)
[0x80003d8c]:lui s1, 1
[0x80003d90]:add a1, a1, s1
[0x80003d94]:ld t5, 512(a1)
[0x80003d98]:sub a1, a1, s1
[0x80003d9c]:lui s1, 1
[0x80003da0]:add a1, a1, s1
[0x80003da4]:ld t4, 520(a1)
[0x80003da8]:sub a1, a1, s1
[0x80003dac]:addi s1, zero, 0
[0x80003db0]:csrrw zero, fcsr, s1
[0x80003db4]:fmax.s t6, t5, t4

[0x80003db4]:fmax.s t6, t5, t4
[0x80003db8]:csrrs a2, fcsr, zero
[0x80003dbc]:sd t6, 464(fp)
[0x80003dc0]:sd a2, 472(fp)
[0x80003dc4]:lui s1, 1
[0x80003dc8]:add a1, a1, s1
[0x80003dcc]:ld t5, 528(a1)
[0x80003dd0]:sub a1, a1, s1
[0x80003dd4]:lui s1, 1
[0x80003dd8]:add a1, a1, s1
[0x80003ddc]:ld t4, 536(a1)
[0x80003de0]:sub a1, a1, s1
[0x80003de4]:addi s1, zero, 0
[0x80003de8]:csrrw zero, fcsr, s1
[0x80003dec]:fmax.s t6, t5, t4

[0x80003dec]:fmax.s t6, t5, t4
[0x80003df0]:csrrs a2, fcsr, zero
[0x80003df4]:sd t6, 480(fp)
[0x80003df8]:sd a2, 488(fp)
[0x80003dfc]:lui s1, 1
[0x80003e00]:add a1, a1, s1
[0x80003e04]:ld t5, 544(a1)
[0x80003e08]:sub a1, a1, s1
[0x80003e0c]:lui s1, 1
[0x80003e10]:add a1, a1, s1
[0x80003e14]:ld t4, 552(a1)
[0x80003e18]:sub a1, a1, s1
[0x80003e1c]:addi s1, zero, 0
[0x80003e20]:csrrw zero, fcsr, s1
[0x80003e24]:fmax.s t6, t5, t4

[0x80003e24]:fmax.s t6, t5, t4
[0x80003e28]:csrrs a2, fcsr, zero
[0x80003e2c]:sd t6, 496(fp)
[0x80003e30]:sd a2, 504(fp)
[0x80003e34]:lui s1, 1
[0x80003e38]:add a1, a1, s1
[0x80003e3c]:ld t5, 560(a1)
[0x80003e40]:sub a1, a1, s1
[0x80003e44]:lui s1, 1
[0x80003e48]:add a1, a1, s1
[0x80003e4c]:ld t4, 568(a1)
[0x80003e50]:sub a1, a1, s1
[0x80003e54]:addi s1, zero, 0
[0x80003e58]:csrrw zero, fcsr, s1
[0x80003e5c]:fmax.s t6, t5, t4

[0x80003e5c]:fmax.s t6, t5, t4
[0x80003e60]:csrrs a2, fcsr, zero
[0x80003e64]:sd t6, 512(fp)
[0x80003e68]:sd a2, 520(fp)
[0x80003e6c]:lui s1, 1
[0x80003e70]:add a1, a1, s1
[0x80003e74]:ld t5, 576(a1)
[0x80003e78]:sub a1, a1, s1
[0x80003e7c]:lui s1, 1
[0x80003e80]:add a1, a1, s1
[0x80003e84]:ld t4, 584(a1)
[0x80003e88]:sub a1, a1, s1
[0x80003e8c]:addi s1, zero, 0
[0x80003e90]:csrrw zero, fcsr, s1
[0x80003e94]:fmax.s t6, t5, t4

[0x80003e94]:fmax.s t6, t5, t4
[0x80003e98]:csrrs a2, fcsr, zero
[0x80003e9c]:sd t6, 528(fp)
[0x80003ea0]:sd a2, 536(fp)
[0x80003ea4]:lui s1, 1
[0x80003ea8]:add a1, a1, s1
[0x80003eac]:ld t5, 592(a1)
[0x80003eb0]:sub a1, a1, s1
[0x80003eb4]:lui s1, 1
[0x80003eb8]:add a1, a1, s1
[0x80003ebc]:ld t4, 600(a1)
[0x80003ec0]:sub a1, a1, s1
[0x80003ec4]:addi s1, zero, 0
[0x80003ec8]:csrrw zero, fcsr, s1
[0x80003ecc]:fmax.s t6, t5, t4

[0x80003ecc]:fmax.s t6, t5, t4
[0x80003ed0]:csrrs a2, fcsr, zero
[0x80003ed4]:sd t6, 544(fp)
[0x80003ed8]:sd a2, 552(fp)
[0x80003edc]:lui s1, 1
[0x80003ee0]:add a1, a1, s1
[0x80003ee4]:ld t5, 608(a1)
[0x80003ee8]:sub a1, a1, s1
[0x80003eec]:lui s1, 1
[0x80003ef0]:add a1, a1, s1
[0x80003ef4]:ld t4, 616(a1)
[0x80003ef8]:sub a1, a1, s1
[0x80003efc]:addi s1, zero, 0
[0x80003f00]:csrrw zero, fcsr, s1
[0x80003f04]:fmax.s t6, t5, t4

[0x80003f04]:fmax.s t6, t5, t4
[0x80003f08]:csrrs a2, fcsr, zero
[0x80003f0c]:sd t6, 560(fp)
[0x80003f10]:sd a2, 568(fp)
[0x80003f14]:lui s1, 1
[0x80003f18]:add a1, a1, s1
[0x80003f1c]:ld t5, 624(a1)
[0x80003f20]:sub a1, a1, s1
[0x80003f24]:lui s1, 1
[0x80003f28]:add a1, a1, s1
[0x80003f2c]:ld t4, 632(a1)
[0x80003f30]:sub a1, a1, s1
[0x80003f34]:addi s1, zero, 0
[0x80003f38]:csrrw zero, fcsr, s1
[0x80003f3c]:fmax.s t6, t5, t4

[0x80003f3c]:fmax.s t6, t5, t4
[0x80003f40]:csrrs a2, fcsr, zero
[0x80003f44]:sd t6, 576(fp)
[0x80003f48]:sd a2, 584(fp)
[0x80003f4c]:lui s1, 1
[0x80003f50]:add a1, a1, s1
[0x80003f54]:ld t5, 640(a1)
[0x80003f58]:sub a1, a1, s1
[0x80003f5c]:lui s1, 1
[0x80003f60]:add a1, a1, s1
[0x80003f64]:ld t4, 648(a1)
[0x80003f68]:sub a1, a1, s1
[0x80003f6c]:addi s1, zero, 0
[0x80003f70]:csrrw zero, fcsr, s1
[0x80003f74]:fmax.s t6, t5, t4

[0x80003f74]:fmax.s t6, t5, t4
[0x80003f78]:csrrs a2, fcsr, zero
[0x80003f7c]:sd t6, 592(fp)
[0x80003f80]:sd a2, 600(fp)
[0x80003f84]:lui s1, 1
[0x80003f88]:add a1, a1, s1
[0x80003f8c]:ld t5, 656(a1)
[0x80003f90]:sub a1, a1, s1
[0x80003f94]:lui s1, 1
[0x80003f98]:add a1, a1, s1
[0x80003f9c]:ld t4, 664(a1)
[0x80003fa0]:sub a1, a1, s1
[0x80003fa4]:addi s1, zero, 0
[0x80003fa8]:csrrw zero, fcsr, s1
[0x80003fac]:fmax.s t6, t5, t4

[0x80003fac]:fmax.s t6, t5, t4
[0x80003fb0]:csrrs a2, fcsr, zero
[0x80003fb4]:sd t6, 608(fp)
[0x80003fb8]:sd a2, 616(fp)
[0x80003fbc]:lui s1, 1
[0x80003fc0]:add a1, a1, s1
[0x80003fc4]:ld t5, 672(a1)
[0x80003fc8]:sub a1, a1, s1
[0x80003fcc]:lui s1, 1
[0x80003fd0]:add a1, a1, s1
[0x80003fd4]:ld t4, 680(a1)
[0x80003fd8]:sub a1, a1, s1
[0x80003fdc]:addi s1, zero, 0
[0x80003fe0]:csrrw zero, fcsr, s1
[0x80003fe4]:fmax.s t6, t5, t4

[0x80003fe4]:fmax.s t6, t5, t4
[0x80003fe8]:csrrs a2, fcsr, zero
[0x80003fec]:sd t6, 624(fp)
[0x80003ff0]:sd a2, 632(fp)
[0x80003ff4]:lui s1, 1
[0x80003ff8]:add a1, a1, s1
[0x80003ffc]:ld t5, 688(a1)
[0x80004000]:sub a1, a1, s1
[0x80004004]:lui s1, 1
[0x80004008]:add a1, a1, s1
[0x8000400c]:ld t4, 696(a1)
[0x80004010]:sub a1, a1, s1
[0x80004014]:addi s1, zero, 0
[0x80004018]:csrrw zero, fcsr, s1
[0x8000401c]:fmax.s t6, t5, t4

[0x8000401c]:fmax.s t6, t5, t4
[0x80004020]:csrrs a2, fcsr, zero
[0x80004024]:sd t6, 640(fp)
[0x80004028]:sd a2, 648(fp)
[0x8000402c]:lui s1, 1
[0x80004030]:add a1, a1, s1
[0x80004034]:ld t5, 704(a1)
[0x80004038]:sub a1, a1, s1
[0x8000403c]:lui s1, 1
[0x80004040]:add a1, a1, s1
[0x80004044]:ld t4, 712(a1)
[0x80004048]:sub a1, a1, s1
[0x8000404c]:addi s1, zero, 0
[0x80004050]:csrrw zero, fcsr, s1
[0x80004054]:fmax.s t6, t5, t4

[0x80004054]:fmax.s t6, t5, t4
[0x80004058]:csrrs a2, fcsr, zero
[0x8000405c]:sd t6, 656(fp)
[0x80004060]:sd a2, 664(fp)
[0x80004064]:lui s1, 1
[0x80004068]:add a1, a1, s1
[0x8000406c]:ld t5, 720(a1)
[0x80004070]:sub a1, a1, s1
[0x80004074]:lui s1, 1
[0x80004078]:add a1, a1, s1
[0x8000407c]:ld t4, 728(a1)
[0x80004080]:sub a1, a1, s1
[0x80004084]:addi s1, zero, 0
[0x80004088]:csrrw zero, fcsr, s1
[0x8000408c]:fmax.s t6, t5, t4

[0x8000408c]:fmax.s t6, t5, t4
[0x80004090]:csrrs a2, fcsr, zero
[0x80004094]:sd t6, 672(fp)
[0x80004098]:sd a2, 680(fp)
[0x8000409c]:lui s1, 1
[0x800040a0]:add a1, a1, s1
[0x800040a4]:ld t5, 736(a1)
[0x800040a8]:sub a1, a1, s1
[0x800040ac]:lui s1, 1
[0x800040b0]:add a1, a1, s1
[0x800040b4]:ld t4, 744(a1)
[0x800040b8]:sub a1, a1, s1
[0x800040bc]:addi s1, zero, 0
[0x800040c0]:csrrw zero, fcsr, s1
[0x800040c4]:fmax.s t6, t5, t4

[0x800040c4]:fmax.s t6, t5, t4
[0x800040c8]:csrrs a2, fcsr, zero
[0x800040cc]:sd t6, 688(fp)
[0x800040d0]:sd a2, 696(fp)
[0x800040d4]:lui s1, 1
[0x800040d8]:add a1, a1, s1
[0x800040dc]:ld t5, 752(a1)
[0x800040e0]:sub a1, a1, s1
[0x800040e4]:lui s1, 1
[0x800040e8]:add a1, a1, s1
[0x800040ec]:ld t4, 760(a1)
[0x800040f0]:sub a1, a1, s1
[0x800040f4]:addi s1, zero, 0
[0x800040f8]:csrrw zero, fcsr, s1
[0x800040fc]:fmax.s t6, t5, t4

[0x800040fc]:fmax.s t6, t5, t4
[0x80004100]:csrrs a2, fcsr, zero
[0x80004104]:sd t6, 704(fp)
[0x80004108]:sd a2, 712(fp)
[0x8000410c]:lui s1, 1
[0x80004110]:add a1, a1, s1
[0x80004114]:ld t5, 768(a1)
[0x80004118]:sub a1, a1, s1
[0x8000411c]:lui s1, 1
[0x80004120]:add a1, a1, s1
[0x80004124]:ld t4, 776(a1)
[0x80004128]:sub a1, a1, s1
[0x8000412c]:addi s1, zero, 0
[0x80004130]:csrrw zero, fcsr, s1
[0x80004134]:fmax.s t6, t5, t4

[0x80004134]:fmax.s t6, t5, t4
[0x80004138]:csrrs a2, fcsr, zero
[0x8000413c]:sd t6, 720(fp)
[0x80004140]:sd a2, 728(fp)
[0x80004144]:lui s1, 1
[0x80004148]:add a1, a1, s1
[0x8000414c]:ld t5, 784(a1)
[0x80004150]:sub a1, a1, s1
[0x80004154]:lui s1, 1
[0x80004158]:add a1, a1, s1
[0x8000415c]:ld t4, 792(a1)
[0x80004160]:sub a1, a1, s1
[0x80004164]:addi s1, zero, 0
[0x80004168]:csrrw zero, fcsr, s1
[0x8000416c]:fmax.s t6, t5, t4

[0x8000416c]:fmax.s t6, t5, t4
[0x80004170]:csrrs a2, fcsr, zero
[0x80004174]:sd t6, 736(fp)
[0x80004178]:sd a2, 744(fp)
[0x8000417c]:lui s1, 1
[0x80004180]:add a1, a1, s1
[0x80004184]:ld t5, 800(a1)
[0x80004188]:sub a1, a1, s1
[0x8000418c]:lui s1, 1
[0x80004190]:add a1, a1, s1
[0x80004194]:ld t4, 808(a1)
[0x80004198]:sub a1, a1, s1
[0x8000419c]:addi s1, zero, 0
[0x800041a0]:csrrw zero, fcsr, s1
[0x800041a4]:fmax.s t6, t5, t4

[0x800041a4]:fmax.s t6, t5, t4
[0x800041a8]:csrrs a2, fcsr, zero
[0x800041ac]:sd t6, 752(fp)
[0x800041b0]:sd a2, 760(fp)
[0x800041b4]:lui s1, 1
[0x800041b8]:add a1, a1, s1
[0x800041bc]:ld t5, 816(a1)
[0x800041c0]:sub a1, a1, s1
[0x800041c4]:lui s1, 1
[0x800041c8]:add a1, a1, s1
[0x800041cc]:ld t4, 824(a1)
[0x800041d0]:sub a1, a1, s1
[0x800041d4]:addi s1, zero, 0
[0x800041d8]:csrrw zero, fcsr, s1
[0x800041dc]:fmax.s t6, t5, t4

[0x800041dc]:fmax.s t6, t5, t4
[0x800041e0]:csrrs a2, fcsr, zero
[0x800041e4]:sd t6, 768(fp)
[0x800041e8]:sd a2, 776(fp)
[0x800041ec]:lui s1, 1
[0x800041f0]:add a1, a1, s1
[0x800041f4]:ld t5, 832(a1)
[0x800041f8]:sub a1, a1, s1
[0x800041fc]:lui s1, 1
[0x80004200]:add a1, a1, s1
[0x80004204]:ld t4, 840(a1)
[0x80004208]:sub a1, a1, s1
[0x8000420c]:addi s1, zero, 0
[0x80004210]:csrrw zero, fcsr, s1
[0x80004214]:fmax.s t6, t5, t4

[0x80004214]:fmax.s t6, t5, t4
[0x80004218]:csrrs a2, fcsr, zero
[0x8000421c]:sd t6, 784(fp)
[0x80004220]:sd a2, 792(fp)
[0x80004224]:lui s1, 1
[0x80004228]:add a1, a1, s1
[0x8000422c]:ld t5, 848(a1)
[0x80004230]:sub a1, a1, s1
[0x80004234]:lui s1, 1
[0x80004238]:add a1, a1, s1
[0x8000423c]:ld t4, 856(a1)
[0x80004240]:sub a1, a1, s1
[0x80004244]:addi s1, zero, 0
[0x80004248]:csrrw zero, fcsr, s1
[0x8000424c]:fmax.s t6, t5, t4

[0x8000424c]:fmax.s t6, t5, t4
[0x80004250]:csrrs a2, fcsr, zero
[0x80004254]:sd t6, 800(fp)
[0x80004258]:sd a2, 808(fp)
[0x8000425c]:lui s1, 1
[0x80004260]:add a1, a1, s1
[0x80004264]:ld t5, 864(a1)
[0x80004268]:sub a1, a1, s1
[0x8000426c]:lui s1, 1
[0x80004270]:add a1, a1, s1
[0x80004274]:ld t4, 872(a1)
[0x80004278]:sub a1, a1, s1
[0x8000427c]:addi s1, zero, 0
[0x80004280]:csrrw zero, fcsr, s1
[0x80004284]:fmax.s t6, t5, t4

[0x80004284]:fmax.s t6, t5, t4
[0x80004288]:csrrs a2, fcsr, zero
[0x8000428c]:sd t6, 816(fp)
[0x80004290]:sd a2, 824(fp)
[0x80004294]:lui s1, 1
[0x80004298]:add a1, a1, s1
[0x8000429c]:ld t5, 880(a1)
[0x800042a0]:sub a1, a1, s1
[0x800042a4]:lui s1, 1
[0x800042a8]:add a1, a1, s1
[0x800042ac]:ld t4, 888(a1)
[0x800042b0]:sub a1, a1, s1
[0x800042b4]:addi s1, zero, 0
[0x800042b8]:csrrw zero, fcsr, s1
[0x800042bc]:fmax.s t6, t5, t4

[0x800042bc]:fmax.s t6, t5, t4
[0x800042c0]:csrrs a2, fcsr, zero
[0x800042c4]:sd t6, 832(fp)
[0x800042c8]:sd a2, 840(fp)
[0x800042cc]:lui s1, 1
[0x800042d0]:add a1, a1, s1
[0x800042d4]:ld t5, 896(a1)
[0x800042d8]:sub a1, a1, s1
[0x800042dc]:lui s1, 1
[0x800042e0]:add a1, a1, s1
[0x800042e4]:ld t4, 904(a1)
[0x800042e8]:sub a1, a1, s1
[0x800042ec]:addi s1, zero, 0
[0x800042f0]:csrrw zero, fcsr, s1
[0x800042f4]:fmax.s t6, t5, t4

[0x800042f4]:fmax.s t6, t5, t4
[0x800042f8]:csrrs a2, fcsr, zero
[0x800042fc]:sd t6, 848(fp)
[0x80004300]:sd a2, 856(fp)
[0x80004304]:lui s1, 1
[0x80004308]:add a1, a1, s1
[0x8000430c]:ld t5, 912(a1)
[0x80004310]:sub a1, a1, s1
[0x80004314]:lui s1, 1
[0x80004318]:add a1, a1, s1
[0x8000431c]:ld t4, 920(a1)
[0x80004320]:sub a1, a1, s1
[0x80004324]:addi s1, zero, 0
[0x80004328]:csrrw zero, fcsr, s1
[0x8000432c]:fmax.s t6, t5, t4

[0x8000432c]:fmax.s t6, t5, t4
[0x80004330]:csrrs a2, fcsr, zero
[0x80004334]:sd t6, 864(fp)
[0x80004338]:sd a2, 872(fp)
[0x8000433c]:lui s1, 1
[0x80004340]:add a1, a1, s1
[0x80004344]:ld t5, 928(a1)
[0x80004348]:sub a1, a1, s1
[0x8000434c]:lui s1, 1
[0x80004350]:add a1, a1, s1
[0x80004354]:ld t4, 936(a1)
[0x80004358]:sub a1, a1, s1
[0x8000435c]:addi s1, zero, 0
[0x80004360]:csrrw zero, fcsr, s1
[0x80004364]:fmax.s t6, t5, t4

[0x80004364]:fmax.s t6, t5, t4
[0x80004368]:csrrs a2, fcsr, zero
[0x8000436c]:sd t6, 880(fp)
[0x80004370]:sd a2, 888(fp)
[0x80004374]:lui s1, 1
[0x80004378]:add a1, a1, s1
[0x8000437c]:ld t5, 944(a1)
[0x80004380]:sub a1, a1, s1
[0x80004384]:lui s1, 1
[0x80004388]:add a1, a1, s1
[0x8000438c]:ld t4, 952(a1)
[0x80004390]:sub a1, a1, s1
[0x80004394]:addi s1, zero, 0
[0x80004398]:csrrw zero, fcsr, s1
[0x8000439c]:fmax.s t6, t5, t4

[0x8000439c]:fmax.s t6, t5, t4
[0x800043a0]:csrrs a2, fcsr, zero
[0x800043a4]:sd t6, 896(fp)
[0x800043a8]:sd a2, 904(fp)
[0x800043ac]:lui s1, 1
[0x800043b0]:add a1, a1, s1
[0x800043b4]:ld t5, 960(a1)
[0x800043b8]:sub a1, a1, s1
[0x800043bc]:lui s1, 1
[0x800043c0]:add a1, a1, s1
[0x800043c4]:ld t4, 968(a1)
[0x800043c8]:sub a1, a1, s1
[0x800043cc]:addi s1, zero, 0
[0x800043d0]:csrrw zero, fcsr, s1
[0x800043d4]:fmax.s t6, t5, t4

[0x800043d4]:fmax.s t6, t5, t4
[0x800043d8]:csrrs a2, fcsr, zero
[0x800043dc]:sd t6, 912(fp)
[0x800043e0]:sd a2, 920(fp)
[0x800043e4]:lui s1, 1
[0x800043e8]:add a1, a1, s1
[0x800043ec]:ld t5, 976(a1)
[0x800043f0]:sub a1, a1, s1
[0x800043f4]:lui s1, 1
[0x800043f8]:add a1, a1, s1
[0x800043fc]:ld t4, 984(a1)
[0x80004400]:sub a1, a1, s1
[0x80004404]:addi s1, zero, 0
[0x80004408]:csrrw zero, fcsr, s1
[0x8000440c]:fmax.s t6, t5, t4

[0x8000440c]:fmax.s t6, t5, t4
[0x80004410]:csrrs a2, fcsr, zero
[0x80004414]:sd t6, 928(fp)
[0x80004418]:sd a2, 936(fp)
[0x8000441c]:lui s1, 1
[0x80004420]:add a1, a1, s1
[0x80004424]:ld t5, 992(a1)
[0x80004428]:sub a1, a1, s1
[0x8000442c]:lui s1, 1
[0x80004430]:add a1, a1, s1
[0x80004434]:ld t4, 1000(a1)
[0x80004438]:sub a1, a1, s1
[0x8000443c]:addi s1, zero, 0
[0x80004440]:csrrw zero, fcsr, s1
[0x80004444]:fmax.s t6, t5, t4

[0x80004444]:fmax.s t6, t5, t4
[0x80004448]:csrrs a2, fcsr, zero
[0x8000444c]:sd t6, 944(fp)
[0x80004450]:sd a2, 952(fp)
[0x80004454]:lui s1, 1
[0x80004458]:add a1, a1, s1
[0x8000445c]:ld t5, 1008(a1)
[0x80004460]:sub a1, a1, s1
[0x80004464]:lui s1, 1
[0x80004468]:add a1, a1, s1
[0x8000446c]:ld t4, 1016(a1)
[0x80004470]:sub a1, a1, s1
[0x80004474]:addi s1, zero, 0
[0x80004478]:csrrw zero, fcsr, s1
[0x8000447c]:fmax.s t6, t5, t4

[0x8000447c]:fmax.s t6, t5, t4
[0x80004480]:csrrs a2, fcsr, zero
[0x80004484]:sd t6, 960(fp)
[0x80004488]:sd a2, 968(fp)
[0x8000448c]:lui s1, 1
[0x80004490]:add a1, a1, s1
[0x80004494]:ld t5, 1024(a1)
[0x80004498]:sub a1, a1, s1
[0x8000449c]:lui s1, 1
[0x800044a0]:add a1, a1, s1
[0x800044a4]:ld t4, 1032(a1)
[0x800044a8]:sub a1, a1, s1
[0x800044ac]:addi s1, zero, 0
[0x800044b0]:csrrw zero, fcsr, s1
[0x800044b4]:fmax.s t6, t5, t4

[0x800044b4]:fmax.s t6, t5, t4
[0x800044b8]:csrrs a2, fcsr, zero
[0x800044bc]:sd t6, 976(fp)
[0x800044c0]:sd a2, 984(fp)
[0x800044c4]:lui s1, 1
[0x800044c8]:add a1, a1, s1
[0x800044cc]:ld t5, 1040(a1)
[0x800044d0]:sub a1, a1, s1
[0x800044d4]:lui s1, 1
[0x800044d8]:add a1, a1, s1
[0x800044dc]:ld t4, 1048(a1)
[0x800044e0]:sub a1, a1, s1
[0x800044e4]:addi s1, zero, 0
[0x800044e8]:csrrw zero, fcsr, s1
[0x800044ec]:fmax.s t6, t5, t4

[0x800044ec]:fmax.s t6, t5, t4
[0x800044f0]:csrrs a2, fcsr, zero
[0x800044f4]:sd t6, 992(fp)
[0x800044f8]:sd a2, 1000(fp)
[0x800044fc]:lui s1, 1
[0x80004500]:add a1, a1, s1
[0x80004504]:ld t5, 1056(a1)
[0x80004508]:sub a1, a1, s1
[0x8000450c]:lui s1, 1
[0x80004510]:add a1, a1, s1
[0x80004514]:ld t4, 1064(a1)
[0x80004518]:sub a1, a1, s1
[0x8000451c]:addi s1, zero, 0
[0x80004520]:csrrw zero, fcsr, s1
[0x80004524]:fmax.s t6, t5, t4

[0x80004524]:fmax.s t6, t5, t4
[0x80004528]:csrrs a2, fcsr, zero
[0x8000452c]:sd t6, 1008(fp)
[0x80004530]:sd a2, 1016(fp)
[0x80004534]:lui s1, 1
[0x80004538]:add a1, a1, s1
[0x8000453c]:ld t5, 1072(a1)
[0x80004540]:sub a1, a1, s1
[0x80004544]:lui s1, 1
[0x80004548]:add a1, a1, s1
[0x8000454c]:ld t4, 1080(a1)
[0x80004550]:sub a1, a1, s1
[0x80004554]:addi s1, zero, 0
[0x80004558]:csrrw zero, fcsr, s1
[0x8000455c]:fmax.s t6, t5, t4

[0x8000455c]:fmax.s t6, t5, t4
[0x80004560]:csrrs a2, fcsr, zero
[0x80004564]:sd t6, 1024(fp)
[0x80004568]:sd a2, 1032(fp)
[0x8000456c]:lui s1, 1
[0x80004570]:add a1, a1, s1
[0x80004574]:ld t5, 1088(a1)
[0x80004578]:sub a1, a1, s1
[0x8000457c]:lui s1, 1
[0x80004580]:add a1, a1, s1
[0x80004584]:ld t4, 1096(a1)
[0x80004588]:sub a1, a1, s1
[0x8000458c]:addi s1, zero, 0
[0x80004590]:csrrw zero, fcsr, s1
[0x80004594]:fmax.s t6, t5, t4

[0x80004594]:fmax.s t6, t5, t4
[0x80004598]:csrrs a2, fcsr, zero
[0x8000459c]:sd t6, 1040(fp)
[0x800045a0]:sd a2, 1048(fp)
[0x800045a4]:lui s1, 1
[0x800045a8]:add a1, a1, s1
[0x800045ac]:ld t5, 1104(a1)
[0x800045b0]:sub a1, a1, s1
[0x800045b4]:lui s1, 1
[0x800045b8]:add a1, a1, s1
[0x800045bc]:ld t4, 1112(a1)
[0x800045c0]:sub a1, a1, s1
[0x800045c4]:addi s1, zero, 0
[0x800045c8]:csrrw zero, fcsr, s1
[0x800045cc]:fmax.s t6, t5, t4

[0x800045cc]:fmax.s t6, t5, t4
[0x800045d0]:csrrs a2, fcsr, zero
[0x800045d4]:sd t6, 1056(fp)
[0x800045d8]:sd a2, 1064(fp)
[0x800045dc]:lui s1, 1
[0x800045e0]:add a1, a1, s1
[0x800045e4]:ld t5, 1120(a1)
[0x800045e8]:sub a1, a1, s1
[0x800045ec]:lui s1, 1
[0x800045f0]:add a1, a1, s1
[0x800045f4]:ld t4, 1128(a1)
[0x800045f8]:sub a1, a1, s1
[0x800045fc]:addi s1, zero, 0
[0x80004600]:csrrw zero, fcsr, s1
[0x80004604]:fmax.s t6, t5, t4

[0x80004604]:fmax.s t6, t5, t4
[0x80004608]:csrrs a2, fcsr, zero
[0x8000460c]:sd t6, 1072(fp)
[0x80004610]:sd a2, 1080(fp)
[0x80004614]:lui s1, 1
[0x80004618]:add a1, a1, s1
[0x8000461c]:ld t5, 1136(a1)
[0x80004620]:sub a1, a1, s1
[0x80004624]:lui s1, 1
[0x80004628]:add a1, a1, s1
[0x8000462c]:ld t4, 1144(a1)
[0x80004630]:sub a1, a1, s1
[0x80004634]:addi s1, zero, 0
[0x80004638]:csrrw zero, fcsr, s1
[0x8000463c]:fmax.s t6, t5, t4

[0x8000463c]:fmax.s t6, t5, t4
[0x80004640]:csrrs a2, fcsr, zero
[0x80004644]:sd t6, 1088(fp)
[0x80004648]:sd a2, 1096(fp)
[0x8000464c]:lui s1, 1
[0x80004650]:add a1, a1, s1
[0x80004654]:ld t5, 1152(a1)
[0x80004658]:sub a1, a1, s1
[0x8000465c]:lui s1, 1
[0x80004660]:add a1, a1, s1
[0x80004664]:ld t4, 1160(a1)
[0x80004668]:sub a1, a1, s1
[0x8000466c]:addi s1, zero, 0
[0x80004670]:csrrw zero, fcsr, s1
[0x80004674]:fmax.s t6, t5, t4

[0x80004674]:fmax.s t6, t5, t4
[0x80004678]:csrrs a2, fcsr, zero
[0x8000467c]:sd t6, 1104(fp)
[0x80004680]:sd a2, 1112(fp)
[0x80004684]:lui s1, 1
[0x80004688]:add a1, a1, s1
[0x8000468c]:ld t5, 1168(a1)
[0x80004690]:sub a1, a1, s1
[0x80004694]:lui s1, 1
[0x80004698]:add a1, a1, s1
[0x8000469c]:ld t4, 1176(a1)
[0x800046a0]:sub a1, a1, s1
[0x800046a4]:addi s1, zero, 0
[0x800046a8]:csrrw zero, fcsr, s1
[0x800046ac]:fmax.s t6, t5, t4

[0x800046ac]:fmax.s t6, t5, t4
[0x800046b0]:csrrs a2, fcsr, zero
[0x800046b4]:sd t6, 1120(fp)
[0x800046b8]:sd a2, 1128(fp)
[0x800046bc]:lui s1, 1
[0x800046c0]:add a1, a1, s1
[0x800046c4]:ld t5, 1184(a1)
[0x800046c8]:sub a1, a1, s1
[0x800046cc]:lui s1, 1
[0x800046d0]:add a1, a1, s1
[0x800046d4]:ld t4, 1192(a1)
[0x800046d8]:sub a1, a1, s1
[0x800046dc]:addi s1, zero, 0
[0x800046e0]:csrrw zero, fcsr, s1
[0x800046e4]:fmax.s t6, t5, t4

[0x800046e4]:fmax.s t6, t5, t4
[0x800046e8]:csrrs a2, fcsr, zero
[0x800046ec]:sd t6, 1136(fp)
[0x800046f0]:sd a2, 1144(fp)
[0x800046f4]:lui s1, 1
[0x800046f8]:add a1, a1, s1
[0x800046fc]:ld t5, 1200(a1)
[0x80004700]:sub a1, a1, s1
[0x80004704]:lui s1, 1
[0x80004708]:add a1, a1, s1
[0x8000470c]:ld t4, 1208(a1)
[0x80004710]:sub a1, a1, s1
[0x80004714]:addi s1, zero, 0
[0x80004718]:csrrw zero, fcsr, s1
[0x8000471c]:fmax.s t6, t5, t4

[0x8000471c]:fmax.s t6, t5, t4
[0x80004720]:csrrs a2, fcsr, zero
[0x80004724]:sd t6, 1152(fp)
[0x80004728]:sd a2, 1160(fp)
[0x8000472c]:lui s1, 1
[0x80004730]:add a1, a1, s1
[0x80004734]:ld t5, 1216(a1)
[0x80004738]:sub a1, a1, s1
[0x8000473c]:lui s1, 1
[0x80004740]:add a1, a1, s1
[0x80004744]:ld t4, 1224(a1)
[0x80004748]:sub a1, a1, s1
[0x8000474c]:addi s1, zero, 0
[0x80004750]:csrrw zero, fcsr, s1
[0x80004754]:fmax.s t6, t5, t4

[0x80004754]:fmax.s t6, t5, t4
[0x80004758]:csrrs a2, fcsr, zero
[0x8000475c]:sd t6, 1168(fp)
[0x80004760]:sd a2, 1176(fp)
[0x80004764]:lui s1, 1
[0x80004768]:add a1, a1, s1
[0x8000476c]:ld t5, 1232(a1)
[0x80004770]:sub a1, a1, s1
[0x80004774]:lui s1, 1
[0x80004778]:add a1, a1, s1
[0x8000477c]:ld t4, 1240(a1)
[0x80004780]:sub a1, a1, s1
[0x80004784]:addi s1, zero, 0
[0x80004788]:csrrw zero, fcsr, s1
[0x8000478c]:fmax.s t6, t5, t4

[0x8000478c]:fmax.s t6, t5, t4
[0x80004790]:csrrs a2, fcsr, zero
[0x80004794]:sd t6, 1184(fp)
[0x80004798]:sd a2, 1192(fp)
[0x8000479c]:lui s1, 1
[0x800047a0]:add a1, a1, s1
[0x800047a4]:ld t5, 1248(a1)
[0x800047a8]:sub a1, a1, s1
[0x800047ac]:lui s1, 1
[0x800047b0]:add a1, a1, s1
[0x800047b4]:ld t4, 1256(a1)
[0x800047b8]:sub a1, a1, s1
[0x800047bc]:addi s1, zero, 0
[0x800047c0]:csrrw zero, fcsr, s1
[0x800047c4]:fmax.s t6, t5, t4

[0x800047c4]:fmax.s t6, t5, t4
[0x800047c8]:csrrs a2, fcsr, zero
[0x800047cc]:sd t6, 1200(fp)
[0x800047d0]:sd a2, 1208(fp)
[0x800047d4]:lui s1, 1
[0x800047d8]:add a1, a1, s1
[0x800047dc]:ld t5, 1264(a1)
[0x800047e0]:sub a1, a1, s1
[0x800047e4]:lui s1, 1
[0x800047e8]:add a1, a1, s1
[0x800047ec]:ld t4, 1272(a1)
[0x800047f0]:sub a1, a1, s1
[0x800047f4]:addi s1, zero, 0
[0x800047f8]:csrrw zero, fcsr, s1
[0x800047fc]:fmax.s t6, t5, t4

[0x800047fc]:fmax.s t6, t5, t4
[0x80004800]:csrrs a2, fcsr, zero
[0x80004804]:sd t6, 1216(fp)
[0x80004808]:sd a2, 1224(fp)
[0x8000480c]:lui s1, 1
[0x80004810]:add a1, a1, s1
[0x80004814]:ld t5, 1280(a1)
[0x80004818]:sub a1, a1, s1
[0x8000481c]:lui s1, 1
[0x80004820]:add a1, a1, s1
[0x80004824]:ld t4, 1288(a1)
[0x80004828]:sub a1, a1, s1
[0x8000482c]:addi s1, zero, 0
[0x80004830]:csrrw zero, fcsr, s1
[0x80004834]:fmax.s t6, t5, t4

[0x80004834]:fmax.s t6, t5, t4
[0x80004838]:csrrs a2, fcsr, zero
[0x8000483c]:sd t6, 1232(fp)
[0x80004840]:sd a2, 1240(fp)
[0x80004844]:lui s1, 1
[0x80004848]:add a1, a1, s1
[0x8000484c]:ld t5, 1296(a1)
[0x80004850]:sub a1, a1, s1
[0x80004854]:lui s1, 1
[0x80004858]:add a1, a1, s1
[0x8000485c]:ld t4, 1304(a1)
[0x80004860]:sub a1, a1, s1
[0x80004864]:addi s1, zero, 0
[0x80004868]:csrrw zero, fcsr, s1
[0x8000486c]:fmax.s t6, t5, t4

[0x8000486c]:fmax.s t6, t5, t4
[0x80004870]:csrrs a2, fcsr, zero
[0x80004874]:sd t6, 1248(fp)
[0x80004878]:sd a2, 1256(fp)
[0x8000487c]:lui s1, 1
[0x80004880]:add a1, a1, s1
[0x80004884]:ld t5, 1312(a1)
[0x80004888]:sub a1, a1, s1
[0x8000488c]:lui s1, 1
[0x80004890]:add a1, a1, s1
[0x80004894]:ld t4, 1320(a1)
[0x80004898]:sub a1, a1, s1
[0x8000489c]:addi s1, zero, 0
[0x800048a0]:csrrw zero, fcsr, s1
[0x800048a4]:fmax.s t6, t5, t4

[0x800048a4]:fmax.s t6, t5, t4
[0x800048a8]:csrrs a2, fcsr, zero
[0x800048ac]:sd t6, 1264(fp)
[0x800048b0]:sd a2, 1272(fp)
[0x800048b4]:lui s1, 1
[0x800048b8]:add a1, a1, s1
[0x800048bc]:ld t5, 1328(a1)
[0x800048c0]:sub a1, a1, s1
[0x800048c4]:lui s1, 1
[0x800048c8]:add a1, a1, s1
[0x800048cc]:ld t4, 1336(a1)
[0x800048d0]:sub a1, a1, s1
[0x800048d4]:addi s1, zero, 0
[0x800048d8]:csrrw zero, fcsr, s1
[0x800048dc]:fmax.s t6, t5, t4

[0x800048dc]:fmax.s t6, t5, t4
[0x800048e0]:csrrs a2, fcsr, zero
[0x800048e4]:sd t6, 1280(fp)
[0x800048e8]:sd a2, 1288(fp)
[0x800048ec]:lui s1, 1
[0x800048f0]:add a1, a1, s1
[0x800048f4]:ld t5, 1344(a1)
[0x800048f8]:sub a1, a1, s1
[0x800048fc]:lui s1, 1
[0x80004900]:add a1, a1, s1
[0x80004904]:ld t4, 1352(a1)
[0x80004908]:sub a1, a1, s1
[0x8000490c]:addi s1, zero, 0
[0x80004910]:csrrw zero, fcsr, s1
[0x80004914]:fmax.s t6, t5, t4

[0x80004914]:fmax.s t6, t5, t4
[0x80004918]:csrrs a2, fcsr, zero
[0x8000491c]:sd t6, 1296(fp)
[0x80004920]:sd a2, 1304(fp)
[0x80004924]:lui s1, 1
[0x80004928]:add a1, a1, s1
[0x8000492c]:ld t5, 1360(a1)
[0x80004930]:sub a1, a1, s1
[0x80004934]:lui s1, 1
[0x80004938]:add a1, a1, s1
[0x8000493c]:ld t4, 1368(a1)
[0x80004940]:sub a1, a1, s1
[0x80004944]:addi s1, zero, 0
[0x80004948]:csrrw zero, fcsr, s1
[0x8000494c]:fmax.s t6, t5, t4

[0x8000494c]:fmax.s t6, t5, t4
[0x80004950]:csrrs a2, fcsr, zero
[0x80004954]:sd t6, 1312(fp)
[0x80004958]:sd a2, 1320(fp)
[0x8000495c]:lui s1, 1
[0x80004960]:add a1, a1, s1
[0x80004964]:ld t5, 1376(a1)
[0x80004968]:sub a1, a1, s1
[0x8000496c]:lui s1, 1
[0x80004970]:add a1, a1, s1
[0x80004974]:ld t4, 1384(a1)
[0x80004978]:sub a1, a1, s1
[0x8000497c]:addi s1, zero, 0
[0x80004980]:csrrw zero, fcsr, s1
[0x80004984]:fmax.s t6, t5, t4

[0x80004984]:fmax.s t6, t5, t4
[0x80004988]:csrrs a2, fcsr, zero
[0x8000498c]:sd t6, 1328(fp)
[0x80004990]:sd a2, 1336(fp)
[0x80004994]:lui s1, 1
[0x80004998]:add a1, a1, s1
[0x8000499c]:ld t5, 1392(a1)
[0x800049a0]:sub a1, a1, s1
[0x800049a4]:lui s1, 1
[0x800049a8]:add a1, a1, s1
[0x800049ac]:ld t4, 1400(a1)
[0x800049b0]:sub a1, a1, s1
[0x800049b4]:addi s1, zero, 0
[0x800049b8]:csrrw zero, fcsr, s1
[0x800049bc]:fmax.s t6, t5, t4

[0x800049bc]:fmax.s t6, t5, t4
[0x800049c0]:csrrs a2, fcsr, zero
[0x800049c4]:sd t6, 1344(fp)
[0x800049c8]:sd a2, 1352(fp)
[0x800049cc]:lui s1, 1
[0x800049d0]:add a1, a1, s1
[0x800049d4]:ld t5, 1408(a1)
[0x800049d8]:sub a1, a1, s1
[0x800049dc]:lui s1, 1
[0x800049e0]:add a1, a1, s1
[0x800049e4]:ld t4, 1416(a1)
[0x800049e8]:sub a1, a1, s1
[0x800049ec]:addi s1, zero, 0
[0x800049f0]:csrrw zero, fcsr, s1
[0x800049f4]:fmax.s t6, t5, t4

[0x800049f4]:fmax.s t6, t5, t4
[0x800049f8]:csrrs a2, fcsr, zero
[0x800049fc]:sd t6, 1360(fp)
[0x80004a00]:sd a2, 1368(fp)
[0x80004a04]:lui s1, 1
[0x80004a08]:add a1, a1, s1
[0x80004a0c]:ld t5, 1424(a1)
[0x80004a10]:sub a1, a1, s1
[0x80004a14]:lui s1, 1
[0x80004a18]:add a1, a1, s1
[0x80004a1c]:ld t4, 1432(a1)
[0x80004a20]:sub a1, a1, s1
[0x80004a24]:addi s1, zero, 0
[0x80004a28]:csrrw zero, fcsr, s1
[0x80004a2c]:fmax.s t6, t5, t4

[0x80004a2c]:fmax.s t6, t5, t4
[0x80004a30]:csrrs a2, fcsr, zero
[0x80004a34]:sd t6, 1376(fp)
[0x80004a38]:sd a2, 1384(fp)
[0x80004a3c]:lui s1, 1
[0x80004a40]:add a1, a1, s1
[0x80004a44]:ld t5, 1440(a1)
[0x80004a48]:sub a1, a1, s1
[0x80004a4c]:lui s1, 1
[0x80004a50]:add a1, a1, s1
[0x80004a54]:ld t4, 1448(a1)
[0x80004a58]:sub a1, a1, s1
[0x80004a5c]:addi s1, zero, 0
[0x80004a60]:csrrw zero, fcsr, s1
[0x80004a64]:fmax.s t6, t5, t4

[0x80004a64]:fmax.s t6, t5, t4
[0x80004a68]:csrrs a2, fcsr, zero
[0x80004a6c]:sd t6, 1392(fp)
[0x80004a70]:sd a2, 1400(fp)
[0x80004a74]:lui s1, 1
[0x80004a78]:add a1, a1, s1
[0x80004a7c]:ld t5, 1456(a1)
[0x80004a80]:sub a1, a1, s1
[0x80004a84]:lui s1, 1
[0x80004a88]:add a1, a1, s1
[0x80004a8c]:ld t4, 1464(a1)
[0x80004a90]:sub a1, a1, s1
[0x80004a94]:addi s1, zero, 0
[0x80004a98]:csrrw zero, fcsr, s1
[0x80004a9c]:fmax.s t6, t5, t4

[0x80004a9c]:fmax.s t6, t5, t4
[0x80004aa0]:csrrs a2, fcsr, zero
[0x80004aa4]:sd t6, 1408(fp)
[0x80004aa8]:sd a2, 1416(fp)
[0x80004aac]:lui s1, 1
[0x80004ab0]:add a1, a1, s1
[0x80004ab4]:ld t5, 1472(a1)
[0x80004ab8]:sub a1, a1, s1
[0x80004abc]:lui s1, 1
[0x80004ac0]:add a1, a1, s1
[0x80004ac4]:ld t4, 1480(a1)
[0x80004ac8]:sub a1, a1, s1
[0x80004acc]:addi s1, zero, 0
[0x80004ad0]:csrrw zero, fcsr, s1
[0x80004ad4]:fmax.s t6, t5, t4

[0x80004ad4]:fmax.s t6, t5, t4
[0x80004ad8]:csrrs a2, fcsr, zero
[0x80004adc]:sd t6, 1424(fp)
[0x80004ae0]:sd a2, 1432(fp)
[0x80004ae4]:lui s1, 1
[0x80004ae8]:add a1, a1, s1
[0x80004aec]:ld t5, 1488(a1)
[0x80004af0]:sub a1, a1, s1
[0x80004af4]:lui s1, 1
[0x80004af8]:add a1, a1, s1
[0x80004afc]:ld t4, 1496(a1)
[0x80004b00]:sub a1, a1, s1
[0x80004b04]:addi s1, zero, 0
[0x80004b08]:csrrw zero, fcsr, s1
[0x80004b0c]:fmax.s t6, t5, t4

[0x80004b0c]:fmax.s t6, t5, t4
[0x80004b10]:csrrs a2, fcsr, zero
[0x80004b14]:sd t6, 1440(fp)
[0x80004b18]:sd a2, 1448(fp)
[0x80004b1c]:lui s1, 1
[0x80004b20]:add a1, a1, s1
[0x80004b24]:ld t5, 1504(a1)
[0x80004b28]:sub a1, a1, s1
[0x80004b2c]:lui s1, 1
[0x80004b30]:add a1, a1, s1
[0x80004b34]:ld t4, 1512(a1)
[0x80004b38]:sub a1, a1, s1
[0x80004b3c]:addi s1, zero, 0
[0x80004b40]:csrrw zero, fcsr, s1
[0x80004b44]:fmax.s t6, t5, t4

[0x80004b44]:fmax.s t6, t5, t4
[0x80004b48]:csrrs a2, fcsr, zero
[0x80004b4c]:sd t6, 1456(fp)
[0x80004b50]:sd a2, 1464(fp)
[0x80004b54]:lui s1, 1
[0x80004b58]:add a1, a1, s1
[0x80004b5c]:ld t5, 1520(a1)
[0x80004b60]:sub a1, a1, s1
[0x80004b64]:lui s1, 1
[0x80004b68]:add a1, a1, s1
[0x80004b6c]:ld t4, 1528(a1)
[0x80004b70]:sub a1, a1, s1
[0x80004b74]:addi s1, zero, 0
[0x80004b78]:csrrw zero, fcsr, s1
[0x80004b7c]:fmax.s t6, t5, t4

[0x80004b7c]:fmax.s t6, t5, t4
[0x80004b80]:csrrs a2, fcsr, zero
[0x80004b84]:sd t6, 1472(fp)
[0x80004b88]:sd a2, 1480(fp)
[0x80004b8c]:lui s1, 1
[0x80004b90]:add a1, a1, s1
[0x80004b94]:ld t5, 1536(a1)
[0x80004b98]:sub a1, a1, s1
[0x80004b9c]:lui s1, 1
[0x80004ba0]:add a1, a1, s1
[0x80004ba4]:ld t4, 1544(a1)
[0x80004ba8]:sub a1, a1, s1
[0x80004bac]:addi s1, zero, 0
[0x80004bb0]:csrrw zero, fcsr, s1
[0x80004bb4]:fmax.s t6, t5, t4

[0x80004bb4]:fmax.s t6, t5, t4
[0x80004bb8]:csrrs a2, fcsr, zero
[0x80004bbc]:sd t6, 1488(fp)
[0x80004bc0]:sd a2, 1496(fp)
[0x80004bc4]:lui s1, 1
[0x80004bc8]:add a1, a1, s1
[0x80004bcc]:ld t5, 1552(a1)
[0x80004bd0]:sub a1, a1, s1
[0x80004bd4]:lui s1, 1
[0x80004bd8]:add a1, a1, s1
[0x80004bdc]:ld t4, 1560(a1)
[0x80004be0]:sub a1, a1, s1
[0x80004be4]:addi s1, zero, 0
[0x80004be8]:csrrw zero, fcsr, s1
[0x80004bec]:fmax.s t6, t5, t4

[0x80004bec]:fmax.s t6, t5, t4
[0x80004bf0]:csrrs a2, fcsr, zero
[0x80004bf4]:sd t6, 1504(fp)
[0x80004bf8]:sd a2, 1512(fp)
[0x80004bfc]:lui s1, 1
[0x80004c00]:add a1, a1, s1
[0x80004c04]:ld t5, 1568(a1)
[0x80004c08]:sub a1, a1, s1
[0x80004c0c]:lui s1, 1
[0x80004c10]:add a1, a1, s1
[0x80004c14]:ld t4, 1576(a1)
[0x80004c18]:sub a1, a1, s1
[0x80004c1c]:addi s1, zero, 0
[0x80004c20]:csrrw zero, fcsr, s1
[0x80004c24]:fmax.s t6, t5, t4

[0x80004c24]:fmax.s t6, t5, t4
[0x80004c28]:csrrs a2, fcsr, zero
[0x80004c2c]:sd t6, 1520(fp)
[0x80004c30]:sd a2, 1528(fp)
[0x80004c34]:lui s1, 1
[0x80004c38]:add a1, a1, s1
[0x80004c3c]:ld t5, 1584(a1)
[0x80004c40]:sub a1, a1, s1
[0x80004c44]:lui s1, 1
[0x80004c48]:add a1, a1, s1
[0x80004c4c]:ld t4, 1592(a1)
[0x80004c50]:sub a1, a1, s1
[0x80004c54]:addi s1, zero, 0
[0x80004c58]:csrrw zero, fcsr, s1
[0x80004c5c]:fmax.s t6, t5, t4

[0x80004c5c]:fmax.s t6, t5, t4
[0x80004c60]:csrrs a2, fcsr, zero
[0x80004c64]:sd t6, 1536(fp)
[0x80004c68]:sd a2, 1544(fp)
[0x80004c6c]:lui s1, 1
[0x80004c70]:add a1, a1, s1
[0x80004c74]:ld t5, 1600(a1)
[0x80004c78]:sub a1, a1, s1
[0x80004c7c]:lui s1, 1
[0x80004c80]:add a1, a1, s1
[0x80004c84]:ld t4, 1608(a1)
[0x80004c88]:sub a1, a1, s1
[0x80004c8c]:addi s1, zero, 0
[0x80004c90]:csrrw zero, fcsr, s1
[0x80004c94]:fmax.s t6, t5, t4

[0x80004c94]:fmax.s t6, t5, t4
[0x80004c98]:csrrs a2, fcsr, zero
[0x80004c9c]:sd t6, 1552(fp)
[0x80004ca0]:sd a2, 1560(fp)
[0x80004ca4]:lui s1, 1
[0x80004ca8]:add a1, a1, s1
[0x80004cac]:ld t5, 1616(a1)
[0x80004cb0]:sub a1, a1, s1
[0x80004cb4]:lui s1, 1
[0x80004cb8]:add a1, a1, s1
[0x80004cbc]:ld t4, 1624(a1)
[0x80004cc0]:sub a1, a1, s1
[0x80004cc4]:addi s1, zero, 0
[0x80004cc8]:csrrw zero, fcsr, s1
[0x80004ccc]:fmax.s t6, t5, t4

[0x80004ccc]:fmax.s t6, t5, t4
[0x80004cd0]:csrrs a2, fcsr, zero
[0x80004cd4]:sd t6, 1568(fp)
[0x80004cd8]:sd a2, 1576(fp)
[0x80004cdc]:lui s1, 1
[0x80004ce0]:add a1, a1, s1
[0x80004ce4]:ld t5, 1632(a1)
[0x80004ce8]:sub a1, a1, s1
[0x80004cec]:lui s1, 1
[0x80004cf0]:add a1, a1, s1
[0x80004cf4]:ld t4, 1640(a1)
[0x80004cf8]:sub a1, a1, s1
[0x80004cfc]:addi s1, zero, 0
[0x80004d00]:csrrw zero, fcsr, s1
[0x80004d04]:fmax.s t6, t5, t4

[0x80004d04]:fmax.s t6, t5, t4
[0x80004d08]:csrrs a2, fcsr, zero
[0x80004d0c]:sd t6, 1584(fp)
[0x80004d10]:sd a2, 1592(fp)
[0x80004d14]:lui s1, 1
[0x80004d18]:add a1, a1, s1
[0x80004d1c]:ld t5, 1648(a1)
[0x80004d20]:sub a1, a1, s1
[0x80004d24]:lui s1, 1
[0x80004d28]:add a1, a1, s1
[0x80004d2c]:ld t4, 1656(a1)
[0x80004d30]:sub a1, a1, s1
[0x80004d34]:addi s1, zero, 0
[0x80004d38]:csrrw zero, fcsr, s1
[0x80004d3c]:fmax.s t6, t5, t4

[0x80004d3c]:fmax.s t6, t5, t4
[0x80004d40]:csrrs a2, fcsr, zero
[0x80004d44]:sd t6, 1600(fp)
[0x80004d48]:sd a2, 1608(fp)
[0x80004d4c]:lui s1, 1
[0x80004d50]:add a1, a1, s1
[0x80004d54]:ld t5, 1664(a1)
[0x80004d58]:sub a1, a1, s1
[0x80004d5c]:lui s1, 1
[0x80004d60]:add a1, a1, s1
[0x80004d64]:ld t4, 1672(a1)
[0x80004d68]:sub a1, a1, s1
[0x80004d6c]:addi s1, zero, 0
[0x80004d70]:csrrw zero, fcsr, s1
[0x80004d74]:fmax.s t6, t5, t4

[0x80004d74]:fmax.s t6, t5, t4
[0x80004d78]:csrrs a2, fcsr, zero
[0x80004d7c]:sd t6, 1616(fp)
[0x80004d80]:sd a2, 1624(fp)
[0x80004d84]:lui s1, 1
[0x80004d88]:add a1, a1, s1
[0x80004d8c]:ld t5, 1680(a1)
[0x80004d90]:sub a1, a1, s1
[0x80004d94]:lui s1, 1
[0x80004d98]:add a1, a1, s1
[0x80004d9c]:ld t4, 1688(a1)
[0x80004da0]:sub a1, a1, s1
[0x80004da4]:addi s1, zero, 0
[0x80004da8]:csrrw zero, fcsr, s1
[0x80004dac]:fmax.s t6, t5, t4

[0x80004dac]:fmax.s t6, t5, t4
[0x80004db0]:csrrs a2, fcsr, zero
[0x80004db4]:sd t6, 1632(fp)
[0x80004db8]:sd a2, 1640(fp)
[0x80004dbc]:lui s1, 1
[0x80004dc0]:add a1, a1, s1
[0x80004dc4]:ld t5, 1696(a1)
[0x80004dc8]:sub a1, a1, s1
[0x80004dcc]:lui s1, 1
[0x80004dd0]:add a1, a1, s1
[0x80004dd4]:ld t4, 1704(a1)
[0x80004dd8]:sub a1, a1, s1
[0x80004ddc]:addi s1, zero, 0
[0x80004de0]:csrrw zero, fcsr, s1
[0x80004de4]:fmax.s t6, t5, t4

[0x80004de4]:fmax.s t6, t5, t4
[0x80004de8]:csrrs a2, fcsr, zero
[0x80004dec]:sd t6, 1648(fp)
[0x80004df0]:sd a2, 1656(fp)
[0x80004df4]:lui s1, 1
[0x80004df8]:add a1, a1, s1
[0x80004dfc]:ld t5, 1712(a1)
[0x80004e00]:sub a1, a1, s1
[0x80004e04]:lui s1, 1
[0x80004e08]:add a1, a1, s1
[0x80004e0c]:ld t4, 1720(a1)
[0x80004e10]:sub a1, a1, s1
[0x80004e14]:addi s1, zero, 0
[0x80004e18]:csrrw zero, fcsr, s1
[0x80004e1c]:fmax.s t6, t5, t4

[0x80004e1c]:fmax.s t6, t5, t4
[0x80004e20]:csrrs a2, fcsr, zero
[0x80004e24]:sd t6, 1664(fp)
[0x80004e28]:sd a2, 1672(fp)
[0x80004e2c]:lui s1, 1
[0x80004e30]:add a1, a1, s1
[0x80004e34]:ld t5, 1728(a1)
[0x80004e38]:sub a1, a1, s1
[0x80004e3c]:lui s1, 1
[0x80004e40]:add a1, a1, s1
[0x80004e44]:ld t4, 1736(a1)
[0x80004e48]:sub a1, a1, s1
[0x80004e4c]:addi s1, zero, 0
[0x80004e50]:csrrw zero, fcsr, s1
[0x80004e54]:fmax.s t6, t5, t4

[0x80004e54]:fmax.s t6, t5, t4
[0x80004e58]:csrrs a2, fcsr, zero
[0x80004e5c]:sd t6, 1680(fp)
[0x80004e60]:sd a2, 1688(fp)
[0x80004e64]:lui s1, 1
[0x80004e68]:add a1, a1, s1
[0x80004e6c]:ld t5, 1744(a1)
[0x80004e70]:sub a1, a1, s1
[0x80004e74]:lui s1, 1
[0x80004e78]:add a1, a1, s1
[0x80004e7c]:ld t4, 1752(a1)
[0x80004e80]:sub a1, a1, s1
[0x80004e84]:addi s1, zero, 0
[0x80004e88]:csrrw zero, fcsr, s1
[0x80004e8c]:fmax.s t6, t5, t4

[0x80004e8c]:fmax.s t6, t5, t4
[0x80004e90]:csrrs a2, fcsr, zero
[0x80004e94]:sd t6, 1696(fp)
[0x80004e98]:sd a2, 1704(fp)
[0x80004e9c]:lui s1, 1
[0x80004ea0]:add a1, a1, s1
[0x80004ea4]:ld t5, 1760(a1)
[0x80004ea8]:sub a1, a1, s1
[0x80004eac]:lui s1, 1
[0x80004eb0]:add a1, a1, s1
[0x80004eb4]:ld t4, 1768(a1)
[0x80004eb8]:sub a1, a1, s1
[0x80004ebc]:addi s1, zero, 0
[0x80004ec0]:csrrw zero, fcsr, s1
[0x80004ec4]:fmax.s t6, t5, t4

[0x80004ec4]:fmax.s t6, t5, t4
[0x80004ec8]:csrrs a2, fcsr, zero
[0x80004ecc]:sd t6, 1712(fp)
[0x80004ed0]:sd a2, 1720(fp)
[0x80004ed4]:lui s1, 1
[0x80004ed8]:add a1, a1, s1
[0x80004edc]:ld t5, 1776(a1)
[0x80004ee0]:sub a1, a1, s1
[0x80004ee4]:lui s1, 1
[0x80004ee8]:add a1, a1, s1
[0x80004eec]:ld t4, 1784(a1)
[0x80004ef0]:sub a1, a1, s1
[0x80004ef4]:addi s1, zero, 0
[0x80004ef8]:csrrw zero, fcsr, s1
[0x80004efc]:fmax.s t6, t5, t4

[0x80004efc]:fmax.s t6, t5, t4
[0x80004f00]:csrrs a2, fcsr, zero
[0x80004f04]:sd t6, 1728(fp)
[0x80004f08]:sd a2, 1736(fp)
[0x80004f0c]:lui s1, 1
[0x80004f10]:add a1, a1, s1
[0x80004f14]:ld t5, 1792(a1)
[0x80004f18]:sub a1, a1, s1
[0x80004f1c]:lui s1, 1
[0x80004f20]:add a1, a1, s1
[0x80004f24]:ld t4, 1800(a1)
[0x80004f28]:sub a1, a1, s1
[0x80004f2c]:addi s1, zero, 0
[0x80004f30]:csrrw zero, fcsr, s1
[0x80004f34]:fmax.s t6, t5, t4

[0x80004f34]:fmax.s t6, t5, t4
[0x80004f38]:csrrs a2, fcsr, zero
[0x80004f3c]:sd t6, 1744(fp)
[0x80004f40]:sd a2, 1752(fp)
[0x80004f44]:lui s1, 1
[0x80004f48]:add a1, a1, s1
[0x80004f4c]:ld t5, 1808(a1)
[0x80004f50]:sub a1, a1, s1
[0x80004f54]:lui s1, 1
[0x80004f58]:add a1, a1, s1
[0x80004f5c]:ld t4, 1816(a1)
[0x80004f60]:sub a1, a1, s1
[0x80004f64]:addi s1, zero, 0
[0x80004f68]:csrrw zero, fcsr, s1
[0x80004f6c]:fmax.s t6, t5, t4

[0x80004f6c]:fmax.s t6, t5, t4
[0x80004f70]:csrrs a2, fcsr, zero
[0x80004f74]:sd t6, 1760(fp)
[0x80004f78]:sd a2, 1768(fp)
[0x80004f7c]:lui s1, 1
[0x80004f80]:add a1, a1, s1
[0x80004f84]:ld t5, 1824(a1)
[0x80004f88]:sub a1, a1, s1
[0x80004f8c]:lui s1, 1
[0x80004f90]:add a1, a1, s1
[0x80004f94]:ld t4, 1832(a1)
[0x80004f98]:sub a1, a1, s1
[0x80004f9c]:addi s1, zero, 0
[0x80004fa0]:csrrw zero, fcsr, s1
[0x80004fa4]:fmax.s t6, t5, t4

[0x80004fa4]:fmax.s t6, t5, t4
[0x80004fa8]:csrrs a2, fcsr, zero
[0x80004fac]:sd t6, 1776(fp)
[0x80004fb0]:sd a2, 1784(fp)
[0x80004fb4]:lui s1, 1
[0x80004fb8]:add a1, a1, s1
[0x80004fbc]:ld t5, 1840(a1)
[0x80004fc0]:sub a1, a1, s1
[0x80004fc4]:lui s1, 1
[0x80004fc8]:add a1, a1, s1
[0x80004fcc]:ld t4, 1848(a1)
[0x80004fd0]:sub a1, a1, s1
[0x80004fd4]:addi s1, zero, 0
[0x80004fd8]:csrrw zero, fcsr, s1
[0x80004fdc]:fmax.s t6, t5, t4

[0x80004fdc]:fmax.s t6, t5, t4
[0x80004fe0]:csrrs a2, fcsr, zero
[0x80004fe4]:sd t6, 1792(fp)
[0x80004fe8]:sd a2, 1800(fp)
[0x80004fec]:lui s1, 1
[0x80004ff0]:add a1, a1, s1
[0x80004ff4]:ld t5, 1856(a1)
[0x80004ff8]:sub a1, a1, s1
[0x80004ffc]:lui s1, 1
[0x80005000]:add a1, a1, s1
[0x80005004]:ld t4, 1864(a1)
[0x80005008]:sub a1, a1, s1
[0x8000500c]:addi s1, zero, 0
[0x80005010]:csrrw zero, fcsr, s1
[0x80005014]:fmax.s t6, t5, t4

[0x80005014]:fmax.s t6, t5, t4
[0x80005018]:csrrs a2, fcsr, zero
[0x8000501c]:sd t6, 1808(fp)
[0x80005020]:sd a2, 1816(fp)
[0x80005024]:lui s1, 1
[0x80005028]:add a1, a1, s1
[0x8000502c]:ld t5, 1872(a1)
[0x80005030]:sub a1, a1, s1
[0x80005034]:lui s1, 1
[0x80005038]:add a1, a1, s1
[0x8000503c]:ld t4, 1880(a1)
[0x80005040]:sub a1, a1, s1
[0x80005044]:addi s1, zero, 0
[0x80005048]:csrrw zero, fcsr, s1
[0x8000504c]:fmax.s t6, t5, t4

[0x8000504c]:fmax.s t6, t5, t4
[0x80005050]:csrrs a2, fcsr, zero
[0x80005054]:sd t6, 1824(fp)
[0x80005058]:sd a2, 1832(fp)
[0x8000505c]:lui s1, 1
[0x80005060]:add a1, a1, s1
[0x80005064]:ld t5, 1888(a1)
[0x80005068]:sub a1, a1, s1
[0x8000506c]:lui s1, 1
[0x80005070]:add a1, a1, s1
[0x80005074]:ld t4, 1896(a1)
[0x80005078]:sub a1, a1, s1
[0x8000507c]:addi s1, zero, 0
[0x80005080]:csrrw zero, fcsr, s1
[0x80005084]:fmax.s t6, t5, t4

[0x80005084]:fmax.s t6, t5, t4
[0x80005088]:csrrs a2, fcsr, zero
[0x8000508c]:sd t6, 1840(fp)
[0x80005090]:sd a2, 1848(fp)
[0x80005094]:lui s1, 1
[0x80005098]:add a1, a1, s1
[0x8000509c]:ld t5, 1904(a1)
[0x800050a0]:sub a1, a1, s1
[0x800050a4]:lui s1, 1
[0x800050a8]:add a1, a1, s1
[0x800050ac]:ld t4, 1912(a1)
[0x800050b0]:sub a1, a1, s1
[0x800050b4]:addi s1, zero, 0
[0x800050b8]:csrrw zero, fcsr, s1
[0x800050bc]:fmax.s t6, t5, t4

[0x800050bc]:fmax.s t6, t5, t4
[0x800050c0]:csrrs a2, fcsr, zero
[0x800050c4]:sd t6, 1856(fp)
[0x800050c8]:sd a2, 1864(fp)
[0x800050cc]:lui s1, 1
[0x800050d0]:add a1, a1, s1
[0x800050d4]:ld t5, 1920(a1)
[0x800050d8]:sub a1, a1, s1
[0x800050dc]:lui s1, 1
[0x800050e0]:add a1, a1, s1
[0x800050e4]:ld t4, 1928(a1)
[0x800050e8]:sub a1, a1, s1
[0x800050ec]:addi s1, zero, 0
[0x800050f0]:csrrw zero, fcsr, s1
[0x800050f4]:fmax.s t6, t5, t4

[0x800050f4]:fmax.s t6, t5, t4
[0x800050f8]:csrrs a2, fcsr, zero
[0x800050fc]:sd t6, 1872(fp)
[0x80005100]:sd a2, 1880(fp)
[0x80005104]:lui s1, 1
[0x80005108]:add a1, a1, s1
[0x8000510c]:ld t5, 1936(a1)
[0x80005110]:sub a1, a1, s1
[0x80005114]:lui s1, 1
[0x80005118]:add a1, a1, s1
[0x8000511c]:ld t4, 1944(a1)
[0x80005120]:sub a1, a1, s1
[0x80005124]:addi s1, zero, 0
[0x80005128]:csrrw zero, fcsr, s1
[0x8000512c]:fmax.s t6, t5, t4

[0x8000512c]:fmax.s t6, t5, t4
[0x80005130]:csrrs a2, fcsr, zero
[0x80005134]:sd t6, 1888(fp)
[0x80005138]:sd a2, 1896(fp)
[0x8000513c]:lui s1, 1
[0x80005140]:add a1, a1, s1
[0x80005144]:ld t5, 1952(a1)
[0x80005148]:sub a1, a1, s1
[0x8000514c]:lui s1, 1
[0x80005150]:add a1, a1, s1
[0x80005154]:ld t4, 1960(a1)
[0x80005158]:sub a1, a1, s1
[0x8000515c]:addi s1, zero, 0
[0x80005160]:csrrw zero, fcsr, s1
[0x80005164]:fmax.s t6, t5, t4

[0x80005164]:fmax.s t6, t5, t4
[0x80005168]:csrrs a2, fcsr, zero
[0x8000516c]:sd t6, 1904(fp)
[0x80005170]:sd a2, 1912(fp)
[0x80005174]:lui s1, 1
[0x80005178]:add a1, a1, s1
[0x8000517c]:ld t5, 1968(a1)
[0x80005180]:sub a1, a1, s1
[0x80005184]:lui s1, 1
[0x80005188]:add a1, a1, s1
[0x8000518c]:ld t4, 1976(a1)
[0x80005190]:sub a1, a1, s1
[0x80005194]:addi s1, zero, 0
[0x80005198]:csrrw zero, fcsr, s1
[0x8000519c]:fmax.s t6, t5, t4

[0x8000519c]:fmax.s t6, t5, t4
[0x800051a0]:csrrs a2, fcsr, zero
[0x800051a4]:sd t6, 1920(fp)
[0x800051a8]:sd a2, 1928(fp)
[0x800051ac]:lui s1, 1
[0x800051b0]:add a1, a1, s1
[0x800051b4]:ld t5, 1984(a1)
[0x800051b8]:sub a1, a1, s1
[0x800051bc]:lui s1, 1
[0x800051c0]:add a1, a1, s1
[0x800051c4]:ld t4, 1992(a1)
[0x800051c8]:sub a1, a1, s1
[0x800051cc]:addi s1, zero, 0
[0x800051d0]:csrrw zero, fcsr, s1
[0x800051d4]:fmax.s t6, t5, t4

[0x800051d4]:fmax.s t6, t5, t4
[0x800051d8]:csrrs a2, fcsr, zero
[0x800051dc]:sd t6, 1936(fp)
[0x800051e0]:sd a2, 1944(fp)
[0x800051e4]:lui s1, 1
[0x800051e8]:add a1, a1, s1
[0x800051ec]:ld t5, 2000(a1)
[0x800051f0]:sub a1, a1, s1
[0x800051f4]:lui s1, 1
[0x800051f8]:add a1, a1, s1
[0x800051fc]:ld t4, 2008(a1)
[0x80005200]:sub a1, a1, s1
[0x80005204]:addi s1, zero, 0
[0x80005208]:csrrw zero, fcsr, s1
[0x8000520c]:fmax.s t6, t5, t4

[0x8000520c]:fmax.s t6, t5, t4
[0x80005210]:csrrs a2, fcsr, zero
[0x80005214]:sd t6, 1952(fp)
[0x80005218]:sd a2, 1960(fp)
[0x8000521c]:lui s1, 1
[0x80005220]:add a1, a1, s1
[0x80005224]:ld t5, 2016(a1)
[0x80005228]:sub a1, a1, s1
[0x8000522c]:lui s1, 1
[0x80005230]:add a1, a1, s1
[0x80005234]:ld t4, 2024(a1)
[0x80005238]:sub a1, a1, s1
[0x8000523c]:addi s1, zero, 0
[0x80005240]:csrrw zero, fcsr, s1
[0x80005244]:fmax.s t6, t5, t4

[0x80005244]:fmax.s t6, t5, t4
[0x80005248]:csrrs a2, fcsr, zero
[0x8000524c]:sd t6, 1968(fp)
[0x80005250]:sd a2, 1976(fp)
[0x80005254]:lui s1, 1
[0x80005258]:add a1, a1, s1
[0x8000525c]:ld t5, 2032(a1)
[0x80005260]:sub a1, a1, s1
[0x80005264]:lui s1, 1
[0x80005268]:add a1, a1, s1
[0x8000526c]:ld t4, 2040(a1)
[0x80005270]:sub a1, a1, s1
[0x80005274]:addi s1, zero, 0
[0x80005278]:csrrw zero, fcsr, s1
[0x8000527c]:fmax.s t6, t5, t4

[0x8000527c]:fmax.s t6, t5, t4
[0x80005280]:csrrs a2, fcsr, zero
[0x80005284]:sd t6, 1984(fp)
[0x80005288]:sd a2, 1992(fp)
[0x8000528c]:lui s1, 2
[0x80005290]:addiw s1, s1, 2048
[0x80005294]:add a1, a1, s1
[0x80005298]:ld t5, 0(a1)
[0x8000529c]:sub a1, a1, s1
[0x800052a0]:lui s1, 2
[0x800052a4]:addiw s1, s1, 2048
[0x800052a8]:add a1, a1, s1
[0x800052ac]:ld t4, 8(a1)
[0x800052b0]:sub a1, a1, s1
[0x800052b4]:addi s1, zero, 0
[0x800052b8]:csrrw zero, fcsr, s1
[0x800052bc]:fmax.s t6, t5, t4

[0x800052bc]:fmax.s t6, t5, t4
[0x800052c0]:csrrs a2, fcsr, zero
[0x800052c4]:sd t6, 2000(fp)
[0x800052c8]:sd a2, 2008(fp)
[0x800052cc]:lui s1, 2
[0x800052d0]:addiw s1, s1, 2048
[0x800052d4]:add a1, a1, s1
[0x800052d8]:ld t5, 16(a1)
[0x800052dc]:sub a1, a1, s1
[0x800052e0]:lui s1, 2
[0x800052e4]:addiw s1, s1, 2048
[0x800052e8]:add a1, a1, s1
[0x800052ec]:ld t4, 24(a1)
[0x800052f0]:sub a1, a1, s1
[0x800052f4]:addi s1, zero, 0
[0x800052f8]:csrrw zero, fcsr, s1
[0x800052fc]:fmax.s t6, t5, t4

[0x800052fc]:fmax.s t6, t5, t4
[0x80005300]:csrrs a2, fcsr, zero
[0x80005304]:sd t6, 2016(fp)
[0x80005308]:sd a2, 2024(fp)
[0x8000530c]:lui s1, 2
[0x80005310]:addiw s1, s1, 2048
[0x80005314]:add a1, a1, s1
[0x80005318]:ld t5, 32(a1)
[0x8000531c]:sub a1, a1, s1
[0x80005320]:lui s1, 2
[0x80005324]:addiw s1, s1, 2048
[0x80005328]:add a1, a1, s1
[0x8000532c]:ld t4, 40(a1)
[0x80005330]:sub a1, a1, s1
[0x80005334]:addi s1, zero, 0
[0x80005338]:csrrw zero, fcsr, s1
[0x8000533c]:fmax.s t6, t5, t4

[0x8000533c]:fmax.s t6, t5, t4
[0x80005340]:csrrs a2, fcsr, zero
[0x80005344]:sd t6, 2032(fp)
[0x80005348]:sd a2, 2040(fp)
[0x8000534c]:auipc fp, 15
[0x80005350]:addi fp, fp, 1116
[0x80005354]:lui s1, 2
[0x80005358]:addiw s1, s1, 2048
[0x8000535c]:add a1, a1, s1
[0x80005360]:ld t5, 48(a1)
[0x80005364]:sub a1, a1, s1
[0x80005368]:lui s1, 2
[0x8000536c]:addiw s1, s1, 2048
[0x80005370]:add a1, a1, s1
[0x80005374]:ld t4, 56(a1)
[0x80005378]:sub a1, a1, s1
[0x8000537c]:addi s1, zero, 0
[0x80005380]:csrrw zero, fcsr, s1
[0x80005384]:fmax.s t6, t5, t4

[0x80005384]:fmax.s t6, t5, t4
[0x80005388]:csrrs a2, fcsr, zero
[0x8000538c]:sd t6, 0(fp)
[0x80005390]:sd a2, 8(fp)
[0x80005394]:lui s1, 2
[0x80005398]:addiw s1, s1, 2048
[0x8000539c]:add a1, a1, s1
[0x800053a0]:ld t5, 64(a1)
[0x800053a4]:sub a1, a1, s1
[0x800053a8]:lui s1, 2
[0x800053ac]:addiw s1, s1, 2048
[0x800053b0]:add a1, a1, s1
[0x800053b4]:ld t4, 72(a1)
[0x800053b8]:sub a1, a1, s1
[0x800053bc]:addi s1, zero, 0
[0x800053c0]:csrrw zero, fcsr, s1
[0x800053c4]:fmax.s t6, t5, t4

[0x800053c4]:fmax.s t6, t5, t4
[0x800053c8]:csrrs a2, fcsr, zero
[0x800053cc]:sd t6, 16(fp)
[0x800053d0]:sd a2, 24(fp)
[0x800053d4]:lui s1, 2
[0x800053d8]:addiw s1, s1, 2048
[0x800053dc]:add a1, a1, s1
[0x800053e0]:ld t5, 80(a1)
[0x800053e4]:sub a1, a1, s1
[0x800053e8]:lui s1, 2
[0x800053ec]:addiw s1, s1, 2048
[0x800053f0]:add a1, a1, s1
[0x800053f4]:ld t4, 88(a1)
[0x800053f8]:sub a1, a1, s1
[0x800053fc]:addi s1, zero, 0
[0x80005400]:csrrw zero, fcsr, s1
[0x80005404]:fmax.s t6, t5, t4

[0x80005404]:fmax.s t6, t5, t4
[0x80005408]:csrrs a2, fcsr, zero
[0x8000540c]:sd t6, 32(fp)
[0x80005410]:sd a2, 40(fp)
[0x80005414]:lui s1, 2
[0x80005418]:addiw s1, s1, 2048
[0x8000541c]:add a1, a1, s1
[0x80005420]:ld t5, 96(a1)
[0x80005424]:sub a1, a1, s1
[0x80005428]:lui s1, 2
[0x8000542c]:addiw s1, s1, 2048
[0x80005430]:add a1, a1, s1
[0x80005434]:ld t4, 104(a1)
[0x80005438]:sub a1, a1, s1
[0x8000543c]:addi s1, zero, 0
[0x80005440]:csrrw zero, fcsr, s1
[0x80005444]:fmax.s t6, t5, t4

[0x80005444]:fmax.s t6, t5, t4
[0x80005448]:csrrs a2, fcsr, zero
[0x8000544c]:sd t6, 48(fp)
[0x80005450]:sd a2, 56(fp)
[0x80005454]:lui s1, 2
[0x80005458]:addiw s1, s1, 2048
[0x8000545c]:add a1, a1, s1
[0x80005460]:ld t5, 112(a1)
[0x80005464]:sub a1, a1, s1
[0x80005468]:lui s1, 2
[0x8000546c]:addiw s1, s1, 2048
[0x80005470]:add a1, a1, s1
[0x80005474]:ld t4, 120(a1)
[0x80005478]:sub a1, a1, s1
[0x8000547c]:addi s1, zero, 0
[0x80005480]:csrrw zero, fcsr, s1
[0x80005484]:fmax.s t6, t5, t4

[0x80005484]:fmax.s t6, t5, t4
[0x80005488]:csrrs a2, fcsr, zero
[0x8000548c]:sd t6, 64(fp)
[0x80005490]:sd a2, 72(fp)
[0x80005494]:lui s1, 2
[0x80005498]:addiw s1, s1, 2048
[0x8000549c]:add a1, a1, s1
[0x800054a0]:ld t5, 128(a1)
[0x800054a4]:sub a1, a1, s1
[0x800054a8]:lui s1, 2
[0x800054ac]:addiw s1, s1, 2048
[0x800054b0]:add a1, a1, s1
[0x800054b4]:ld t4, 136(a1)
[0x800054b8]:sub a1, a1, s1
[0x800054bc]:addi s1, zero, 0
[0x800054c0]:csrrw zero, fcsr, s1
[0x800054c4]:fmax.s t6, t5, t4

[0x800054c4]:fmax.s t6, t5, t4
[0x800054c8]:csrrs a2, fcsr, zero
[0x800054cc]:sd t6, 80(fp)
[0x800054d0]:sd a2, 88(fp)
[0x800054d4]:lui s1, 2
[0x800054d8]:addiw s1, s1, 2048
[0x800054dc]:add a1, a1, s1
[0x800054e0]:ld t5, 144(a1)
[0x800054e4]:sub a1, a1, s1
[0x800054e8]:lui s1, 2
[0x800054ec]:addiw s1, s1, 2048
[0x800054f0]:add a1, a1, s1
[0x800054f4]:ld t4, 152(a1)
[0x800054f8]:sub a1, a1, s1
[0x800054fc]:addi s1, zero, 0
[0x80005500]:csrrw zero, fcsr, s1
[0x80005504]:fmax.s t6, t5, t4

[0x80005504]:fmax.s t6, t5, t4
[0x80005508]:csrrs a2, fcsr, zero
[0x8000550c]:sd t6, 96(fp)
[0x80005510]:sd a2, 104(fp)
[0x80005514]:lui s1, 2
[0x80005518]:addiw s1, s1, 2048
[0x8000551c]:add a1, a1, s1
[0x80005520]:ld t5, 160(a1)
[0x80005524]:sub a1, a1, s1
[0x80005528]:lui s1, 2
[0x8000552c]:addiw s1, s1, 2048
[0x80005530]:add a1, a1, s1
[0x80005534]:ld t4, 168(a1)
[0x80005538]:sub a1, a1, s1
[0x8000553c]:addi s1, zero, 0
[0x80005540]:csrrw zero, fcsr, s1
[0x80005544]:fmax.s t6, t5, t4

[0x80005544]:fmax.s t6, t5, t4
[0x80005548]:csrrs a2, fcsr, zero
[0x8000554c]:sd t6, 112(fp)
[0x80005550]:sd a2, 120(fp)
[0x80005554]:lui s1, 2
[0x80005558]:addiw s1, s1, 2048
[0x8000555c]:add a1, a1, s1
[0x80005560]:ld t5, 176(a1)
[0x80005564]:sub a1, a1, s1
[0x80005568]:lui s1, 2
[0x8000556c]:addiw s1, s1, 2048
[0x80005570]:add a1, a1, s1
[0x80005574]:ld t4, 184(a1)
[0x80005578]:sub a1, a1, s1
[0x8000557c]:addi s1, zero, 0
[0x80005580]:csrrw zero, fcsr, s1
[0x80005584]:fmax.s t6, t5, t4

[0x80005584]:fmax.s t6, t5, t4
[0x80005588]:csrrs a2, fcsr, zero
[0x8000558c]:sd t6, 128(fp)
[0x80005590]:sd a2, 136(fp)
[0x80005594]:lui s1, 2
[0x80005598]:addiw s1, s1, 2048
[0x8000559c]:add a1, a1, s1
[0x800055a0]:ld t5, 192(a1)
[0x800055a4]:sub a1, a1, s1
[0x800055a8]:lui s1, 2
[0x800055ac]:addiw s1, s1, 2048
[0x800055b0]:add a1, a1, s1
[0x800055b4]:ld t4, 200(a1)
[0x800055b8]:sub a1, a1, s1
[0x800055bc]:addi s1, zero, 0
[0x800055c0]:csrrw zero, fcsr, s1
[0x800055c4]:fmax.s t6, t5, t4

[0x800055c4]:fmax.s t6, t5, t4
[0x800055c8]:csrrs a2, fcsr, zero
[0x800055cc]:sd t6, 144(fp)
[0x800055d0]:sd a2, 152(fp)
[0x800055d4]:lui s1, 2
[0x800055d8]:addiw s1, s1, 2048
[0x800055dc]:add a1, a1, s1
[0x800055e0]:ld t5, 208(a1)
[0x800055e4]:sub a1, a1, s1
[0x800055e8]:lui s1, 2
[0x800055ec]:addiw s1, s1, 2048
[0x800055f0]:add a1, a1, s1
[0x800055f4]:ld t4, 216(a1)
[0x800055f8]:sub a1, a1, s1
[0x800055fc]:addi s1, zero, 0
[0x80005600]:csrrw zero, fcsr, s1
[0x80005604]:fmax.s t6, t5, t4

[0x80005604]:fmax.s t6, t5, t4
[0x80005608]:csrrs a2, fcsr, zero
[0x8000560c]:sd t6, 160(fp)
[0x80005610]:sd a2, 168(fp)
[0x80005614]:lui s1, 2
[0x80005618]:addiw s1, s1, 2048
[0x8000561c]:add a1, a1, s1
[0x80005620]:ld t5, 224(a1)
[0x80005624]:sub a1, a1, s1
[0x80005628]:lui s1, 2
[0x8000562c]:addiw s1, s1, 2048
[0x80005630]:add a1, a1, s1
[0x80005634]:ld t4, 232(a1)
[0x80005638]:sub a1, a1, s1
[0x8000563c]:addi s1, zero, 0
[0x80005640]:csrrw zero, fcsr, s1
[0x80005644]:fmax.s t6, t5, t4

[0x80005644]:fmax.s t6, t5, t4
[0x80005648]:csrrs a2, fcsr, zero
[0x8000564c]:sd t6, 176(fp)
[0x80005650]:sd a2, 184(fp)
[0x80005654]:lui s1, 2
[0x80005658]:addiw s1, s1, 2048
[0x8000565c]:add a1, a1, s1
[0x80005660]:ld t5, 240(a1)
[0x80005664]:sub a1, a1, s1
[0x80005668]:lui s1, 2
[0x8000566c]:addiw s1, s1, 2048
[0x80005670]:add a1, a1, s1
[0x80005674]:ld t4, 248(a1)
[0x80005678]:sub a1, a1, s1
[0x8000567c]:addi s1, zero, 0
[0x80005680]:csrrw zero, fcsr, s1
[0x80005684]:fmax.s t6, t5, t4

[0x80005684]:fmax.s t6, t5, t4
[0x80005688]:csrrs a2, fcsr, zero
[0x8000568c]:sd t6, 192(fp)
[0x80005690]:sd a2, 200(fp)
[0x80005694]:lui s1, 2
[0x80005698]:addiw s1, s1, 2048
[0x8000569c]:add a1, a1, s1
[0x800056a0]:ld t5, 256(a1)
[0x800056a4]:sub a1, a1, s1
[0x800056a8]:lui s1, 2
[0x800056ac]:addiw s1, s1, 2048
[0x800056b0]:add a1, a1, s1
[0x800056b4]:ld t4, 264(a1)
[0x800056b8]:sub a1, a1, s1
[0x800056bc]:addi s1, zero, 0
[0x800056c0]:csrrw zero, fcsr, s1
[0x800056c4]:fmax.s t6, t5, t4

[0x800056c4]:fmax.s t6, t5, t4
[0x800056c8]:csrrs a2, fcsr, zero
[0x800056cc]:sd t6, 208(fp)
[0x800056d0]:sd a2, 216(fp)
[0x800056d4]:lui s1, 2
[0x800056d8]:addiw s1, s1, 2048
[0x800056dc]:add a1, a1, s1
[0x800056e0]:ld t5, 272(a1)
[0x800056e4]:sub a1, a1, s1
[0x800056e8]:lui s1, 2
[0x800056ec]:addiw s1, s1, 2048
[0x800056f0]:add a1, a1, s1
[0x800056f4]:ld t4, 280(a1)
[0x800056f8]:sub a1, a1, s1
[0x800056fc]:addi s1, zero, 0
[0x80005700]:csrrw zero, fcsr, s1
[0x80005704]:fmax.s t6, t5, t4

[0x80005704]:fmax.s t6, t5, t4
[0x80005708]:csrrs a2, fcsr, zero
[0x8000570c]:sd t6, 224(fp)
[0x80005710]:sd a2, 232(fp)
[0x80005714]:lui s1, 2
[0x80005718]:addiw s1, s1, 2048
[0x8000571c]:add a1, a1, s1
[0x80005720]:ld t5, 288(a1)
[0x80005724]:sub a1, a1, s1
[0x80005728]:lui s1, 2
[0x8000572c]:addiw s1, s1, 2048
[0x80005730]:add a1, a1, s1
[0x80005734]:ld t4, 296(a1)
[0x80005738]:sub a1, a1, s1
[0x8000573c]:addi s1, zero, 0
[0x80005740]:csrrw zero, fcsr, s1
[0x80005744]:fmax.s t6, t5, t4

[0x80005744]:fmax.s t6, t5, t4
[0x80005748]:csrrs a2, fcsr, zero
[0x8000574c]:sd t6, 240(fp)
[0x80005750]:sd a2, 248(fp)
[0x80005754]:lui s1, 2
[0x80005758]:addiw s1, s1, 2048
[0x8000575c]:add a1, a1, s1
[0x80005760]:ld t5, 304(a1)
[0x80005764]:sub a1, a1, s1
[0x80005768]:lui s1, 2
[0x8000576c]:addiw s1, s1, 2048
[0x80005770]:add a1, a1, s1
[0x80005774]:ld t4, 312(a1)
[0x80005778]:sub a1, a1, s1
[0x8000577c]:addi s1, zero, 0
[0x80005780]:csrrw zero, fcsr, s1
[0x80005784]:fmax.s t6, t5, t4

[0x80005784]:fmax.s t6, t5, t4
[0x80005788]:csrrs a2, fcsr, zero
[0x8000578c]:sd t6, 256(fp)
[0x80005790]:sd a2, 264(fp)
[0x80005794]:lui s1, 2
[0x80005798]:addiw s1, s1, 2048
[0x8000579c]:add a1, a1, s1
[0x800057a0]:ld t5, 320(a1)
[0x800057a4]:sub a1, a1, s1
[0x800057a8]:lui s1, 2
[0x800057ac]:addiw s1, s1, 2048
[0x800057b0]:add a1, a1, s1
[0x800057b4]:ld t4, 328(a1)
[0x800057b8]:sub a1, a1, s1
[0x800057bc]:addi s1, zero, 0
[0x800057c0]:csrrw zero, fcsr, s1
[0x800057c4]:fmax.s t6, t5, t4

[0x800057c4]:fmax.s t6, t5, t4
[0x800057c8]:csrrs a2, fcsr, zero
[0x800057cc]:sd t6, 272(fp)
[0x800057d0]:sd a2, 280(fp)
[0x800057d4]:lui s1, 2
[0x800057d8]:addiw s1, s1, 2048
[0x800057dc]:add a1, a1, s1
[0x800057e0]:ld t5, 336(a1)
[0x800057e4]:sub a1, a1, s1
[0x800057e8]:lui s1, 2
[0x800057ec]:addiw s1, s1, 2048
[0x800057f0]:add a1, a1, s1
[0x800057f4]:ld t4, 344(a1)
[0x800057f8]:sub a1, a1, s1
[0x800057fc]:addi s1, zero, 0
[0x80005800]:csrrw zero, fcsr, s1
[0x80005804]:fmax.s t6, t5, t4

[0x80005804]:fmax.s t6, t5, t4
[0x80005808]:csrrs a2, fcsr, zero
[0x8000580c]:sd t6, 288(fp)
[0x80005810]:sd a2, 296(fp)
[0x80005814]:lui s1, 2
[0x80005818]:addiw s1, s1, 2048
[0x8000581c]:add a1, a1, s1
[0x80005820]:ld t5, 352(a1)
[0x80005824]:sub a1, a1, s1
[0x80005828]:lui s1, 2
[0x8000582c]:addiw s1, s1, 2048
[0x80005830]:add a1, a1, s1
[0x80005834]:ld t4, 360(a1)
[0x80005838]:sub a1, a1, s1
[0x8000583c]:addi s1, zero, 0
[0x80005840]:csrrw zero, fcsr, s1
[0x80005844]:fmax.s t6, t5, t4

[0x80005844]:fmax.s t6, t5, t4
[0x80005848]:csrrs a2, fcsr, zero
[0x8000584c]:sd t6, 304(fp)
[0x80005850]:sd a2, 312(fp)
[0x80005854]:lui s1, 2
[0x80005858]:addiw s1, s1, 2048
[0x8000585c]:add a1, a1, s1
[0x80005860]:ld t5, 368(a1)
[0x80005864]:sub a1, a1, s1
[0x80005868]:lui s1, 2
[0x8000586c]:addiw s1, s1, 2048
[0x80005870]:add a1, a1, s1
[0x80005874]:ld t4, 376(a1)
[0x80005878]:sub a1, a1, s1
[0x8000587c]:addi s1, zero, 0
[0x80005880]:csrrw zero, fcsr, s1
[0x80005884]:fmax.s t6, t5, t4

[0x80005884]:fmax.s t6, t5, t4
[0x80005888]:csrrs a2, fcsr, zero
[0x8000588c]:sd t6, 320(fp)
[0x80005890]:sd a2, 328(fp)
[0x80005894]:lui s1, 2
[0x80005898]:addiw s1, s1, 2048
[0x8000589c]:add a1, a1, s1
[0x800058a0]:ld t5, 384(a1)
[0x800058a4]:sub a1, a1, s1
[0x800058a8]:lui s1, 2
[0x800058ac]:addiw s1, s1, 2048
[0x800058b0]:add a1, a1, s1
[0x800058b4]:ld t4, 392(a1)
[0x800058b8]:sub a1, a1, s1
[0x800058bc]:addi s1, zero, 0
[0x800058c0]:csrrw zero, fcsr, s1
[0x800058c4]:fmax.s t6, t5, t4

[0x800058c4]:fmax.s t6, t5, t4
[0x800058c8]:csrrs a2, fcsr, zero
[0x800058cc]:sd t6, 336(fp)
[0x800058d0]:sd a2, 344(fp)
[0x800058d4]:lui s1, 2
[0x800058d8]:addiw s1, s1, 2048
[0x800058dc]:add a1, a1, s1
[0x800058e0]:ld t5, 400(a1)
[0x800058e4]:sub a1, a1, s1
[0x800058e8]:lui s1, 2
[0x800058ec]:addiw s1, s1, 2048
[0x800058f0]:add a1, a1, s1
[0x800058f4]:ld t4, 408(a1)
[0x800058f8]:sub a1, a1, s1
[0x800058fc]:addi s1, zero, 0
[0x80005900]:csrrw zero, fcsr, s1
[0x80005904]:fmax.s t6, t5, t4

[0x80005904]:fmax.s t6, t5, t4
[0x80005908]:csrrs a2, fcsr, zero
[0x8000590c]:sd t6, 352(fp)
[0x80005910]:sd a2, 360(fp)
[0x80005914]:lui s1, 2
[0x80005918]:addiw s1, s1, 2048
[0x8000591c]:add a1, a1, s1
[0x80005920]:ld t5, 416(a1)
[0x80005924]:sub a1, a1, s1
[0x80005928]:lui s1, 2
[0x8000592c]:addiw s1, s1, 2048
[0x80005930]:add a1, a1, s1
[0x80005934]:ld t4, 424(a1)
[0x80005938]:sub a1, a1, s1
[0x8000593c]:addi s1, zero, 0
[0x80005940]:csrrw zero, fcsr, s1
[0x80005944]:fmax.s t6, t5, t4

[0x80005944]:fmax.s t6, t5, t4
[0x80005948]:csrrs a2, fcsr, zero
[0x8000594c]:sd t6, 368(fp)
[0x80005950]:sd a2, 376(fp)
[0x80005954]:lui s1, 2
[0x80005958]:addiw s1, s1, 2048
[0x8000595c]:add a1, a1, s1
[0x80005960]:ld t5, 432(a1)
[0x80005964]:sub a1, a1, s1
[0x80005968]:lui s1, 2
[0x8000596c]:addiw s1, s1, 2048
[0x80005970]:add a1, a1, s1
[0x80005974]:ld t4, 440(a1)
[0x80005978]:sub a1, a1, s1
[0x8000597c]:addi s1, zero, 0
[0x80005980]:csrrw zero, fcsr, s1
[0x80005984]:fmax.s t6, t5, t4

[0x80005984]:fmax.s t6, t5, t4
[0x80005988]:csrrs a2, fcsr, zero
[0x8000598c]:sd t6, 384(fp)
[0x80005990]:sd a2, 392(fp)
[0x80005994]:lui s1, 2
[0x80005998]:addiw s1, s1, 2048
[0x8000599c]:add a1, a1, s1
[0x800059a0]:ld t5, 448(a1)
[0x800059a4]:sub a1, a1, s1
[0x800059a8]:lui s1, 2
[0x800059ac]:addiw s1, s1, 2048
[0x800059b0]:add a1, a1, s1
[0x800059b4]:ld t4, 456(a1)
[0x800059b8]:sub a1, a1, s1
[0x800059bc]:addi s1, zero, 0
[0x800059c0]:csrrw zero, fcsr, s1
[0x800059c4]:fmax.s t6, t5, t4

[0x800059c4]:fmax.s t6, t5, t4
[0x800059c8]:csrrs a2, fcsr, zero
[0x800059cc]:sd t6, 400(fp)
[0x800059d0]:sd a2, 408(fp)
[0x800059d4]:lui s1, 2
[0x800059d8]:addiw s1, s1, 2048
[0x800059dc]:add a1, a1, s1
[0x800059e0]:ld t5, 464(a1)
[0x800059e4]:sub a1, a1, s1
[0x800059e8]:lui s1, 2
[0x800059ec]:addiw s1, s1, 2048
[0x800059f0]:add a1, a1, s1
[0x800059f4]:ld t4, 472(a1)
[0x800059f8]:sub a1, a1, s1
[0x800059fc]:addi s1, zero, 0
[0x80005a00]:csrrw zero, fcsr, s1
[0x80005a04]:fmax.s t6, t5, t4

[0x80005a04]:fmax.s t6, t5, t4
[0x80005a08]:csrrs a2, fcsr, zero
[0x80005a0c]:sd t6, 416(fp)
[0x80005a10]:sd a2, 424(fp)
[0x80005a14]:lui s1, 2
[0x80005a18]:addiw s1, s1, 2048
[0x80005a1c]:add a1, a1, s1
[0x80005a20]:ld t5, 480(a1)
[0x80005a24]:sub a1, a1, s1
[0x80005a28]:lui s1, 2
[0x80005a2c]:addiw s1, s1, 2048
[0x80005a30]:add a1, a1, s1
[0x80005a34]:ld t4, 488(a1)
[0x80005a38]:sub a1, a1, s1
[0x80005a3c]:addi s1, zero, 0
[0x80005a40]:csrrw zero, fcsr, s1
[0x80005a44]:fmax.s t6, t5, t4

[0x80005a44]:fmax.s t6, t5, t4
[0x80005a48]:csrrs a2, fcsr, zero
[0x80005a4c]:sd t6, 432(fp)
[0x80005a50]:sd a2, 440(fp)
[0x80005a54]:lui s1, 2
[0x80005a58]:addiw s1, s1, 2048
[0x80005a5c]:add a1, a1, s1
[0x80005a60]:ld t5, 496(a1)
[0x80005a64]:sub a1, a1, s1
[0x80005a68]:lui s1, 2
[0x80005a6c]:addiw s1, s1, 2048
[0x80005a70]:add a1, a1, s1
[0x80005a74]:ld t4, 504(a1)
[0x80005a78]:sub a1, a1, s1
[0x80005a7c]:addi s1, zero, 0
[0x80005a80]:csrrw zero, fcsr, s1
[0x80005a84]:fmax.s t6, t5, t4

[0x80005a84]:fmax.s t6, t5, t4
[0x80005a88]:csrrs a2, fcsr, zero
[0x80005a8c]:sd t6, 448(fp)
[0x80005a90]:sd a2, 456(fp)
[0x80005a94]:lui s1, 2
[0x80005a98]:addiw s1, s1, 2048
[0x80005a9c]:add a1, a1, s1
[0x80005aa0]:ld t5, 512(a1)
[0x80005aa4]:sub a1, a1, s1
[0x80005aa8]:lui s1, 2
[0x80005aac]:addiw s1, s1, 2048
[0x80005ab0]:add a1, a1, s1
[0x80005ab4]:ld t4, 520(a1)
[0x80005ab8]:sub a1, a1, s1
[0x80005abc]:addi s1, zero, 0
[0x80005ac0]:csrrw zero, fcsr, s1
[0x80005ac4]:fmax.s t6, t5, t4

[0x80005ac4]:fmax.s t6, t5, t4
[0x80005ac8]:csrrs a2, fcsr, zero
[0x80005acc]:sd t6, 464(fp)
[0x80005ad0]:sd a2, 472(fp)
[0x80005ad4]:lui s1, 2
[0x80005ad8]:addiw s1, s1, 2048
[0x80005adc]:add a1, a1, s1
[0x80005ae0]:ld t5, 528(a1)
[0x80005ae4]:sub a1, a1, s1
[0x80005ae8]:lui s1, 2
[0x80005aec]:addiw s1, s1, 2048
[0x80005af0]:add a1, a1, s1
[0x80005af4]:ld t4, 536(a1)
[0x80005af8]:sub a1, a1, s1
[0x80005afc]:addi s1, zero, 0
[0x80005b00]:csrrw zero, fcsr, s1
[0x80005b04]:fmax.s t6, t5, t4

[0x80005b04]:fmax.s t6, t5, t4
[0x80005b08]:csrrs a2, fcsr, zero
[0x80005b0c]:sd t6, 480(fp)
[0x80005b10]:sd a2, 488(fp)
[0x80005b14]:lui s1, 2
[0x80005b18]:addiw s1, s1, 2048
[0x80005b1c]:add a1, a1, s1
[0x80005b20]:ld t5, 544(a1)
[0x80005b24]:sub a1, a1, s1
[0x80005b28]:lui s1, 2
[0x80005b2c]:addiw s1, s1, 2048
[0x80005b30]:add a1, a1, s1
[0x80005b34]:ld t4, 552(a1)
[0x80005b38]:sub a1, a1, s1
[0x80005b3c]:addi s1, zero, 0
[0x80005b40]:csrrw zero, fcsr, s1
[0x80005b44]:fmax.s t6, t5, t4

[0x80005b44]:fmax.s t6, t5, t4
[0x80005b48]:csrrs a2, fcsr, zero
[0x80005b4c]:sd t6, 496(fp)
[0x80005b50]:sd a2, 504(fp)
[0x80005b54]:lui s1, 2
[0x80005b58]:addiw s1, s1, 2048
[0x80005b5c]:add a1, a1, s1
[0x80005b60]:ld t5, 560(a1)
[0x80005b64]:sub a1, a1, s1
[0x80005b68]:lui s1, 2
[0x80005b6c]:addiw s1, s1, 2048
[0x80005b70]:add a1, a1, s1
[0x80005b74]:ld t4, 568(a1)
[0x80005b78]:sub a1, a1, s1
[0x80005b7c]:addi s1, zero, 0
[0x80005b80]:csrrw zero, fcsr, s1
[0x80005b84]:fmax.s t6, t5, t4

[0x80005b84]:fmax.s t6, t5, t4
[0x80005b88]:csrrs a2, fcsr, zero
[0x80005b8c]:sd t6, 512(fp)
[0x80005b90]:sd a2, 520(fp)
[0x80005b94]:lui s1, 2
[0x80005b98]:addiw s1, s1, 2048
[0x80005b9c]:add a1, a1, s1
[0x80005ba0]:ld t5, 576(a1)
[0x80005ba4]:sub a1, a1, s1
[0x80005ba8]:lui s1, 2
[0x80005bac]:addiw s1, s1, 2048
[0x80005bb0]:add a1, a1, s1
[0x80005bb4]:ld t4, 584(a1)
[0x80005bb8]:sub a1, a1, s1
[0x80005bbc]:addi s1, zero, 0
[0x80005bc0]:csrrw zero, fcsr, s1
[0x80005bc4]:fmax.s t6, t5, t4

[0x80005bc4]:fmax.s t6, t5, t4
[0x80005bc8]:csrrs a2, fcsr, zero
[0x80005bcc]:sd t6, 528(fp)
[0x80005bd0]:sd a2, 536(fp)
[0x80005bd4]:lui s1, 2
[0x80005bd8]:addiw s1, s1, 2048
[0x80005bdc]:add a1, a1, s1
[0x80005be0]:ld t5, 592(a1)
[0x80005be4]:sub a1, a1, s1
[0x80005be8]:lui s1, 2
[0x80005bec]:addiw s1, s1, 2048
[0x80005bf0]:add a1, a1, s1
[0x80005bf4]:ld t4, 600(a1)
[0x80005bf8]:sub a1, a1, s1
[0x80005bfc]:addi s1, zero, 0
[0x80005c00]:csrrw zero, fcsr, s1
[0x80005c04]:fmax.s t6, t5, t4

[0x80005c04]:fmax.s t6, t5, t4
[0x80005c08]:csrrs a2, fcsr, zero
[0x80005c0c]:sd t6, 544(fp)
[0x80005c10]:sd a2, 552(fp)
[0x80005c14]:lui s1, 2
[0x80005c18]:addiw s1, s1, 2048
[0x80005c1c]:add a1, a1, s1
[0x80005c20]:ld t5, 608(a1)
[0x80005c24]:sub a1, a1, s1
[0x80005c28]:lui s1, 2
[0x80005c2c]:addiw s1, s1, 2048
[0x80005c30]:add a1, a1, s1
[0x80005c34]:ld t4, 616(a1)
[0x80005c38]:sub a1, a1, s1
[0x80005c3c]:addi s1, zero, 0
[0x80005c40]:csrrw zero, fcsr, s1
[0x80005c44]:fmax.s t6, t5, t4

[0x80005c44]:fmax.s t6, t5, t4
[0x80005c48]:csrrs a2, fcsr, zero
[0x80005c4c]:sd t6, 560(fp)
[0x80005c50]:sd a2, 568(fp)
[0x80005c54]:lui s1, 2
[0x80005c58]:addiw s1, s1, 2048
[0x80005c5c]:add a1, a1, s1
[0x80005c60]:ld t5, 624(a1)
[0x80005c64]:sub a1, a1, s1
[0x80005c68]:lui s1, 2
[0x80005c6c]:addiw s1, s1, 2048
[0x80005c70]:add a1, a1, s1
[0x80005c74]:ld t4, 632(a1)
[0x80005c78]:sub a1, a1, s1
[0x80005c7c]:addi s1, zero, 0
[0x80005c80]:csrrw zero, fcsr, s1
[0x80005c84]:fmax.s t6, t5, t4

[0x80005c84]:fmax.s t6, t5, t4
[0x80005c88]:csrrs a2, fcsr, zero
[0x80005c8c]:sd t6, 576(fp)
[0x80005c90]:sd a2, 584(fp)
[0x80005c94]:lui s1, 2
[0x80005c98]:addiw s1, s1, 2048
[0x80005c9c]:add a1, a1, s1
[0x80005ca0]:ld t5, 640(a1)
[0x80005ca4]:sub a1, a1, s1
[0x80005ca8]:lui s1, 2
[0x80005cac]:addiw s1, s1, 2048
[0x80005cb0]:add a1, a1, s1
[0x80005cb4]:ld t4, 648(a1)
[0x80005cb8]:sub a1, a1, s1
[0x80005cbc]:addi s1, zero, 0
[0x80005cc0]:csrrw zero, fcsr, s1
[0x80005cc4]:fmax.s t6, t5, t4

[0x80005cc4]:fmax.s t6, t5, t4
[0x80005cc8]:csrrs a2, fcsr, zero
[0x80005ccc]:sd t6, 592(fp)
[0x80005cd0]:sd a2, 600(fp)
[0x80005cd4]:lui s1, 2
[0x80005cd8]:addiw s1, s1, 2048
[0x80005cdc]:add a1, a1, s1
[0x80005ce0]:ld t5, 656(a1)
[0x80005ce4]:sub a1, a1, s1
[0x80005ce8]:lui s1, 2
[0x80005cec]:addiw s1, s1, 2048
[0x80005cf0]:add a1, a1, s1
[0x80005cf4]:ld t4, 664(a1)
[0x80005cf8]:sub a1, a1, s1
[0x80005cfc]:addi s1, zero, 0
[0x80005d00]:csrrw zero, fcsr, s1
[0x80005d04]:fmax.s t6, t5, t4

[0x80005d04]:fmax.s t6, t5, t4
[0x80005d08]:csrrs a2, fcsr, zero
[0x80005d0c]:sd t6, 608(fp)
[0x80005d10]:sd a2, 616(fp)
[0x80005d14]:lui s1, 2
[0x80005d18]:addiw s1, s1, 2048
[0x80005d1c]:add a1, a1, s1
[0x80005d20]:ld t5, 672(a1)
[0x80005d24]:sub a1, a1, s1
[0x80005d28]:lui s1, 2
[0x80005d2c]:addiw s1, s1, 2048
[0x80005d30]:add a1, a1, s1
[0x80005d34]:ld t4, 680(a1)
[0x80005d38]:sub a1, a1, s1
[0x80005d3c]:addi s1, zero, 0
[0x80005d40]:csrrw zero, fcsr, s1
[0x80005d44]:fmax.s t6, t5, t4

[0x80005d44]:fmax.s t6, t5, t4
[0x80005d48]:csrrs a2, fcsr, zero
[0x80005d4c]:sd t6, 624(fp)
[0x80005d50]:sd a2, 632(fp)
[0x80005d54]:lui s1, 2
[0x80005d58]:addiw s1, s1, 2048
[0x80005d5c]:add a1, a1, s1
[0x80005d60]:ld t5, 688(a1)
[0x80005d64]:sub a1, a1, s1
[0x80005d68]:lui s1, 2
[0x80005d6c]:addiw s1, s1, 2048
[0x80005d70]:add a1, a1, s1
[0x80005d74]:ld t4, 696(a1)
[0x80005d78]:sub a1, a1, s1
[0x80005d7c]:addi s1, zero, 0
[0x80005d80]:csrrw zero, fcsr, s1
[0x80005d84]:fmax.s t6, t5, t4

[0x80005d84]:fmax.s t6, t5, t4
[0x80005d88]:csrrs a2, fcsr, zero
[0x80005d8c]:sd t6, 640(fp)
[0x80005d90]:sd a2, 648(fp)
[0x80005d94]:lui s1, 2
[0x80005d98]:addiw s1, s1, 2048
[0x80005d9c]:add a1, a1, s1
[0x80005da0]:ld t5, 704(a1)
[0x80005da4]:sub a1, a1, s1
[0x80005da8]:lui s1, 2
[0x80005dac]:addiw s1, s1, 2048
[0x80005db0]:add a1, a1, s1
[0x80005db4]:ld t4, 712(a1)
[0x80005db8]:sub a1, a1, s1
[0x80005dbc]:addi s1, zero, 0
[0x80005dc0]:csrrw zero, fcsr, s1
[0x80005dc4]:fmax.s t6, t5, t4

[0x80005dc4]:fmax.s t6, t5, t4
[0x80005dc8]:csrrs a2, fcsr, zero
[0x80005dcc]:sd t6, 656(fp)
[0x80005dd0]:sd a2, 664(fp)
[0x80005dd4]:lui s1, 2
[0x80005dd8]:addiw s1, s1, 2048
[0x80005ddc]:add a1, a1, s1
[0x80005de0]:ld t5, 720(a1)
[0x80005de4]:sub a1, a1, s1
[0x80005de8]:lui s1, 2
[0x80005dec]:addiw s1, s1, 2048
[0x80005df0]:add a1, a1, s1
[0x80005df4]:ld t4, 728(a1)
[0x80005df8]:sub a1, a1, s1
[0x80005dfc]:addi s1, zero, 0
[0x80005e00]:csrrw zero, fcsr, s1
[0x80005e04]:fmax.s t6, t5, t4

[0x80005e04]:fmax.s t6, t5, t4
[0x80005e08]:csrrs a2, fcsr, zero
[0x80005e0c]:sd t6, 672(fp)
[0x80005e10]:sd a2, 680(fp)
[0x80005e14]:lui s1, 2
[0x80005e18]:addiw s1, s1, 2048
[0x80005e1c]:add a1, a1, s1
[0x80005e20]:ld t5, 736(a1)
[0x80005e24]:sub a1, a1, s1
[0x80005e28]:lui s1, 2
[0x80005e2c]:addiw s1, s1, 2048
[0x80005e30]:add a1, a1, s1
[0x80005e34]:ld t4, 744(a1)
[0x80005e38]:sub a1, a1, s1
[0x80005e3c]:addi s1, zero, 0
[0x80005e40]:csrrw zero, fcsr, s1
[0x80005e44]:fmax.s t6, t5, t4

[0x80005e44]:fmax.s t6, t5, t4
[0x80005e48]:csrrs a2, fcsr, zero
[0x80005e4c]:sd t6, 688(fp)
[0x80005e50]:sd a2, 696(fp)
[0x80005e54]:lui s1, 2
[0x80005e58]:addiw s1, s1, 2048
[0x80005e5c]:add a1, a1, s1
[0x80005e60]:ld t5, 752(a1)
[0x80005e64]:sub a1, a1, s1
[0x80005e68]:lui s1, 2
[0x80005e6c]:addiw s1, s1, 2048
[0x80005e70]:add a1, a1, s1
[0x80005e74]:ld t4, 760(a1)
[0x80005e78]:sub a1, a1, s1
[0x80005e7c]:addi s1, zero, 0
[0x80005e80]:csrrw zero, fcsr, s1
[0x80005e84]:fmax.s t6, t5, t4

[0x80005e84]:fmax.s t6, t5, t4
[0x80005e88]:csrrs a2, fcsr, zero
[0x80005e8c]:sd t6, 704(fp)
[0x80005e90]:sd a2, 712(fp)
[0x80005e94]:lui s1, 2
[0x80005e98]:addiw s1, s1, 2048
[0x80005e9c]:add a1, a1, s1
[0x80005ea0]:ld t5, 768(a1)
[0x80005ea4]:sub a1, a1, s1
[0x80005ea8]:lui s1, 2
[0x80005eac]:addiw s1, s1, 2048
[0x80005eb0]:add a1, a1, s1
[0x80005eb4]:ld t4, 776(a1)
[0x80005eb8]:sub a1, a1, s1
[0x80005ebc]:addi s1, zero, 0
[0x80005ec0]:csrrw zero, fcsr, s1
[0x80005ec4]:fmax.s t6, t5, t4

[0x80005ec4]:fmax.s t6, t5, t4
[0x80005ec8]:csrrs a2, fcsr, zero
[0x80005ecc]:sd t6, 720(fp)
[0x80005ed0]:sd a2, 728(fp)
[0x80005ed4]:lui s1, 2
[0x80005ed8]:addiw s1, s1, 2048
[0x80005edc]:add a1, a1, s1
[0x80005ee0]:ld t5, 784(a1)
[0x80005ee4]:sub a1, a1, s1
[0x80005ee8]:lui s1, 2
[0x80005eec]:addiw s1, s1, 2048
[0x80005ef0]:add a1, a1, s1
[0x80005ef4]:ld t4, 792(a1)
[0x80005ef8]:sub a1, a1, s1
[0x80005efc]:addi s1, zero, 0
[0x80005f00]:csrrw zero, fcsr, s1
[0x80005f04]:fmax.s t6, t5, t4

[0x80005f04]:fmax.s t6, t5, t4
[0x80005f08]:csrrs a2, fcsr, zero
[0x80005f0c]:sd t6, 736(fp)
[0x80005f10]:sd a2, 744(fp)
[0x80005f14]:lui s1, 2
[0x80005f18]:addiw s1, s1, 2048
[0x80005f1c]:add a1, a1, s1
[0x80005f20]:ld t5, 800(a1)
[0x80005f24]:sub a1, a1, s1
[0x80005f28]:lui s1, 2
[0x80005f2c]:addiw s1, s1, 2048
[0x80005f30]:add a1, a1, s1
[0x80005f34]:ld t4, 808(a1)
[0x80005f38]:sub a1, a1, s1
[0x80005f3c]:addi s1, zero, 0
[0x80005f40]:csrrw zero, fcsr, s1
[0x80005f44]:fmax.s t6, t5, t4

[0x80005f44]:fmax.s t6, t5, t4
[0x80005f48]:csrrs a2, fcsr, zero
[0x80005f4c]:sd t6, 752(fp)
[0x80005f50]:sd a2, 760(fp)
[0x80005f54]:lui s1, 2
[0x80005f58]:addiw s1, s1, 2048
[0x80005f5c]:add a1, a1, s1
[0x80005f60]:ld t5, 816(a1)
[0x80005f64]:sub a1, a1, s1
[0x80005f68]:lui s1, 2
[0x80005f6c]:addiw s1, s1, 2048
[0x80005f70]:add a1, a1, s1
[0x80005f74]:ld t4, 824(a1)
[0x80005f78]:sub a1, a1, s1
[0x80005f7c]:addi s1, zero, 0
[0x80005f80]:csrrw zero, fcsr, s1
[0x80005f84]:fmax.s t6, t5, t4

[0x80005f84]:fmax.s t6, t5, t4
[0x80005f88]:csrrs a2, fcsr, zero
[0x80005f8c]:sd t6, 768(fp)
[0x80005f90]:sd a2, 776(fp)
[0x80005f94]:lui s1, 2
[0x80005f98]:addiw s1, s1, 2048
[0x80005f9c]:add a1, a1, s1
[0x80005fa0]:ld t5, 832(a1)
[0x80005fa4]:sub a1, a1, s1
[0x80005fa8]:lui s1, 2
[0x80005fac]:addiw s1, s1, 2048
[0x80005fb0]:add a1, a1, s1
[0x80005fb4]:ld t4, 840(a1)
[0x80005fb8]:sub a1, a1, s1
[0x80005fbc]:addi s1, zero, 0
[0x80005fc0]:csrrw zero, fcsr, s1
[0x80005fc4]:fmax.s t6, t5, t4

[0x80005fc4]:fmax.s t6, t5, t4
[0x80005fc8]:csrrs a2, fcsr, zero
[0x80005fcc]:sd t6, 784(fp)
[0x80005fd0]:sd a2, 792(fp)
[0x80005fd4]:lui s1, 2
[0x80005fd8]:addiw s1, s1, 2048
[0x80005fdc]:add a1, a1, s1
[0x80005fe0]:ld t5, 848(a1)
[0x80005fe4]:sub a1, a1, s1
[0x80005fe8]:lui s1, 2
[0x80005fec]:addiw s1, s1, 2048
[0x80005ff0]:add a1, a1, s1
[0x80005ff4]:ld t4, 856(a1)
[0x80005ff8]:sub a1, a1, s1
[0x80005ffc]:addi s1, zero, 0
[0x80006000]:csrrw zero, fcsr, s1
[0x80006004]:fmax.s t6, t5, t4

[0x80006004]:fmax.s t6, t5, t4
[0x80006008]:csrrs a2, fcsr, zero
[0x8000600c]:sd t6, 800(fp)
[0x80006010]:sd a2, 808(fp)
[0x80006014]:lui s1, 2
[0x80006018]:addiw s1, s1, 2048
[0x8000601c]:add a1, a1, s1
[0x80006020]:ld t5, 864(a1)
[0x80006024]:sub a1, a1, s1
[0x80006028]:lui s1, 2
[0x8000602c]:addiw s1, s1, 2048
[0x80006030]:add a1, a1, s1
[0x80006034]:ld t4, 872(a1)
[0x80006038]:sub a1, a1, s1
[0x8000603c]:addi s1, zero, 0
[0x80006040]:csrrw zero, fcsr, s1
[0x80006044]:fmax.s t6, t5, t4

[0x80006044]:fmax.s t6, t5, t4
[0x80006048]:csrrs a2, fcsr, zero
[0x8000604c]:sd t6, 816(fp)
[0x80006050]:sd a2, 824(fp)
[0x80006054]:lui s1, 2
[0x80006058]:addiw s1, s1, 2048
[0x8000605c]:add a1, a1, s1
[0x80006060]:ld t5, 880(a1)
[0x80006064]:sub a1, a1, s1
[0x80006068]:lui s1, 2
[0x8000606c]:addiw s1, s1, 2048
[0x80006070]:add a1, a1, s1
[0x80006074]:ld t4, 888(a1)
[0x80006078]:sub a1, a1, s1
[0x8000607c]:addi s1, zero, 0
[0x80006080]:csrrw zero, fcsr, s1
[0x80006084]:fmax.s t6, t5, t4

[0x80006084]:fmax.s t6, t5, t4
[0x80006088]:csrrs a2, fcsr, zero
[0x8000608c]:sd t6, 832(fp)
[0x80006090]:sd a2, 840(fp)
[0x80006094]:lui s1, 2
[0x80006098]:addiw s1, s1, 2048
[0x8000609c]:add a1, a1, s1
[0x800060a0]:ld t5, 896(a1)
[0x800060a4]:sub a1, a1, s1
[0x800060a8]:lui s1, 2
[0x800060ac]:addiw s1, s1, 2048
[0x800060b0]:add a1, a1, s1
[0x800060b4]:ld t4, 904(a1)
[0x800060b8]:sub a1, a1, s1
[0x800060bc]:addi s1, zero, 0
[0x800060c0]:csrrw zero, fcsr, s1
[0x800060c4]:fmax.s t6, t5, t4

[0x800060c4]:fmax.s t6, t5, t4
[0x800060c8]:csrrs a2, fcsr, zero
[0x800060cc]:sd t6, 848(fp)
[0x800060d0]:sd a2, 856(fp)
[0x800060d4]:lui s1, 2
[0x800060d8]:addiw s1, s1, 2048
[0x800060dc]:add a1, a1, s1
[0x800060e0]:ld t5, 912(a1)
[0x800060e4]:sub a1, a1, s1
[0x800060e8]:lui s1, 2
[0x800060ec]:addiw s1, s1, 2048
[0x800060f0]:add a1, a1, s1
[0x800060f4]:ld t4, 920(a1)
[0x800060f8]:sub a1, a1, s1
[0x800060fc]:addi s1, zero, 0
[0x80006100]:csrrw zero, fcsr, s1
[0x80006104]:fmax.s t6, t5, t4

[0x80006104]:fmax.s t6, t5, t4
[0x80006108]:csrrs a2, fcsr, zero
[0x8000610c]:sd t6, 864(fp)
[0x80006110]:sd a2, 872(fp)
[0x80006114]:lui s1, 2
[0x80006118]:addiw s1, s1, 2048
[0x8000611c]:add a1, a1, s1
[0x80006120]:ld t5, 928(a1)
[0x80006124]:sub a1, a1, s1
[0x80006128]:lui s1, 2
[0x8000612c]:addiw s1, s1, 2048
[0x80006130]:add a1, a1, s1
[0x80006134]:ld t4, 936(a1)
[0x80006138]:sub a1, a1, s1
[0x8000613c]:addi s1, zero, 0
[0x80006140]:csrrw zero, fcsr, s1
[0x80006144]:fmax.s t6, t5, t4

[0x80006144]:fmax.s t6, t5, t4
[0x80006148]:csrrs a2, fcsr, zero
[0x8000614c]:sd t6, 880(fp)
[0x80006150]:sd a2, 888(fp)
[0x80006154]:lui s1, 2
[0x80006158]:addiw s1, s1, 2048
[0x8000615c]:add a1, a1, s1
[0x80006160]:ld t5, 944(a1)
[0x80006164]:sub a1, a1, s1
[0x80006168]:lui s1, 2
[0x8000616c]:addiw s1, s1, 2048
[0x80006170]:add a1, a1, s1
[0x80006174]:ld t4, 952(a1)
[0x80006178]:sub a1, a1, s1
[0x8000617c]:addi s1, zero, 0
[0x80006180]:csrrw zero, fcsr, s1
[0x80006184]:fmax.s t6, t5, t4

[0x80006184]:fmax.s t6, t5, t4
[0x80006188]:csrrs a2, fcsr, zero
[0x8000618c]:sd t6, 896(fp)
[0x80006190]:sd a2, 904(fp)
[0x80006194]:lui s1, 2
[0x80006198]:addiw s1, s1, 2048
[0x8000619c]:add a1, a1, s1
[0x800061a0]:ld t5, 960(a1)
[0x800061a4]:sub a1, a1, s1
[0x800061a8]:lui s1, 2
[0x800061ac]:addiw s1, s1, 2048
[0x800061b0]:add a1, a1, s1
[0x800061b4]:ld t4, 968(a1)
[0x800061b8]:sub a1, a1, s1
[0x800061bc]:addi s1, zero, 0
[0x800061c0]:csrrw zero, fcsr, s1
[0x800061c4]:fmax.s t6, t5, t4

[0x800061c4]:fmax.s t6, t5, t4
[0x800061c8]:csrrs a2, fcsr, zero
[0x800061cc]:sd t6, 912(fp)
[0x800061d0]:sd a2, 920(fp)
[0x800061d4]:lui s1, 2
[0x800061d8]:addiw s1, s1, 2048
[0x800061dc]:add a1, a1, s1
[0x800061e0]:ld t5, 976(a1)
[0x800061e4]:sub a1, a1, s1
[0x800061e8]:lui s1, 2
[0x800061ec]:addiw s1, s1, 2048
[0x800061f0]:add a1, a1, s1
[0x800061f4]:ld t4, 984(a1)
[0x800061f8]:sub a1, a1, s1
[0x800061fc]:addi s1, zero, 0
[0x80006200]:csrrw zero, fcsr, s1
[0x80006204]:fmax.s t6, t5, t4

[0x80006204]:fmax.s t6, t5, t4
[0x80006208]:csrrs a2, fcsr, zero
[0x8000620c]:sd t6, 928(fp)
[0x80006210]:sd a2, 936(fp)
[0x80006214]:lui s1, 2
[0x80006218]:addiw s1, s1, 2048
[0x8000621c]:add a1, a1, s1
[0x80006220]:ld t5, 992(a1)
[0x80006224]:sub a1, a1, s1
[0x80006228]:lui s1, 2
[0x8000622c]:addiw s1, s1, 2048
[0x80006230]:add a1, a1, s1
[0x80006234]:ld t4, 1000(a1)
[0x80006238]:sub a1, a1, s1
[0x8000623c]:addi s1, zero, 0
[0x80006240]:csrrw zero, fcsr, s1
[0x80006244]:fmax.s t6, t5, t4

[0x80006244]:fmax.s t6, t5, t4
[0x80006248]:csrrs a2, fcsr, zero
[0x8000624c]:sd t6, 944(fp)
[0x80006250]:sd a2, 952(fp)
[0x80006254]:lui s1, 2
[0x80006258]:addiw s1, s1, 2048
[0x8000625c]:add a1, a1, s1
[0x80006260]:ld t5, 1008(a1)
[0x80006264]:sub a1, a1, s1
[0x80006268]:lui s1, 2
[0x8000626c]:addiw s1, s1, 2048
[0x80006270]:add a1, a1, s1
[0x80006274]:ld t4, 1016(a1)
[0x80006278]:sub a1, a1, s1
[0x8000627c]:addi s1, zero, 0
[0x80006280]:csrrw zero, fcsr, s1
[0x80006284]:fmax.s t6, t5, t4

[0x80006284]:fmax.s t6, t5, t4
[0x80006288]:csrrs a2, fcsr, zero
[0x8000628c]:sd t6, 960(fp)
[0x80006290]:sd a2, 968(fp)
[0x80006294]:lui s1, 2
[0x80006298]:addiw s1, s1, 2048
[0x8000629c]:add a1, a1, s1
[0x800062a0]:ld t5, 1024(a1)
[0x800062a4]:sub a1, a1, s1
[0x800062a8]:lui s1, 2
[0x800062ac]:addiw s1, s1, 2048
[0x800062b0]:add a1, a1, s1
[0x800062b4]:ld t4, 1032(a1)
[0x800062b8]:sub a1, a1, s1
[0x800062bc]:addi s1, zero, 0
[0x800062c0]:csrrw zero, fcsr, s1
[0x800062c4]:fmax.s t6, t5, t4

[0x800062c4]:fmax.s t6, t5, t4
[0x800062c8]:csrrs a2, fcsr, zero
[0x800062cc]:sd t6, 976(fp)
[0x800062d0]:sd a2, 984(fp)
[0x800062d4]:lui s1, 2
[0x800062d8]:addiw s1, s1, 2048
[0x800062dc]:add a1, a1, s1
[0x800062e0]:ld t5, 1040(a1)
[0x800062e4]:sub a1, a1, s1
[0x800062e8]:lui s1, 2
[0x800062ec]:addiw s1, s1, 2048
[0x800062f0]:add a1, a1, s1
[0x800062f4]:ld t4, 1048(a1)
[0x800062f8]:sub a1, a1, s1
[0x800062fc]:addi s1, zero, 0
[0x80006300]:csrrw zero, fcsr, s1
[0x80006304]:fmax.s t6, t5, t4

[0x80006304]:fmax.s t6, t5, t4
[0x80006308]:csrrs a2, fcsr, zero
[0x8000630c]:sd t6, 992(fp)
[0x80006310]:sd a2, 1000(fp)
[0x80006314]:lui s1, 2
[0x80006318]:addiw s1, s1, 2048
[0x8000631c]:add a1, a1, s1
[0x80006320]:ld t5, 1056(a1)
[0x80006324]:sub a1, a1, s1
[0x80006328]:lui s1, 2
[0x8000632c]:addiw s1, s1, 2048
[0x80006330]:add a1, a1, s1
[0x80006334]:ld t4, 1064(a1)
[0x80006338]:sub a1, a1, s1
[0x8000633c]:addi s1, zero, 0
[0x80006340]:csrrw zero, fcsr, s1
[0x80006344]:fmax.s t6, t5, t4

[0x80006344]:fmax.s t6, t5, t4
[0x80006348]:csrrs a2, fcsr, zero
[0x8000634c]:sd t6, 1008(fp)
[0x80006350]:sd a2, 1016(fp)
[0x80006354]:lui s1, 2
[0x80006358]:addiw s1, s1, 2048
[0x8000635c]:add a1, a1, s1
[0x80006360]:ld t5, 1072(a1)
[0x80006364]:sub a1, a1, s1
[0x80006368]:lui s1, 2
[0x8000636c]:addiw s1, s1, 2048
[0x80006370]:add a1, a1, s1
[0x80006374]:ld t4, 1080(a1)
[0x80006378]:sub a1, a1, s1
[0x8000637c]:addi s1, zero, 0
[0x80006380]:csrrw zero, fcsr, s1
[0x80006384]:fmax.s t6, t5, t4

[0x80006384]:fmax.s t6, t5, t4
[0x80006388]:csrrs a2, fcsr, zero
[0x8000638c]:sd t6, 1024(fp)
[0x80006390]:sd a2, 1032(fp)
[0x80006394]:lui s1, 2
[0x80006398]:addiw s1, s1, 2048
[0x8000639c]:add a1, a1, s1
[0x800063a0]:ld t5, 1088(a1)
[0x800063a4]:sub a1, a1, s1
[0x800063a8]:lui s1, 2
[0x800063ac]:addiw s1, s1, 2048
[0x800063b0]:add a1, a1, s1
[0x800063b4]:ld t4, 1096(a1)
[0x800063b8]:sub a1, a1, s1
[0x800063bc]:addi s1, zero, 0
[0x800063c0]:csrrw zero, fcsr, s1
[0x800063c4]:fmax.s t6, t5, t4

[0x800063c4]:fmax.s t6, t5, t4
[0x800063c8]:csrrs a2, fcsr, zero
[0x800063cc]:sd t6, 1040(fp)
[0x800063d0]:sd a2, 1048(fp)
[0x800063d4]:lui s1, 2
[0x800063d8]:addiw s1, s1, 2048
[0x800063dc]:add a1, a1, s1
[0x800063e0]:ld t5, 1104(a1)
[0x800063e4]:sub a1, a1, s1
[0x800063e8]:lui s1, 2
[0x800063ec]:addiw s1, s1, 2048
[0x800063f0]:add a1, a1, s1
[0x800063f4]:ld t4, 1112(a1)
[0x800063f8]:sub a1, a1, s1
[0x800063fc]:addi s1, zero, 0
[0x80006400]:csrrw zero, fcsr, s1
[0x80006404]:fmax.s t6, t5, t4

[0x80006404]:fmax.s t6, t5, t4
[0x80006408]:csrrs a2, fcsr, zero
[0x8000640c]:sd t6, 1056(fp)
[0x80006410]:sd a2, 1064(fp)
[0x80006414]:lui s1, 2
[0x80006418]:addiw s1, s1, 2048
[0x8000641c]:add a1, a1, s1
[0x80006420]:ld t5, 1120(a1)
[0x80006424]:sub a1, a1, s1
[0x80006428]:lui s1, 2
[0x8000642c]:addiw s1, s1, 2048
[0x80006430]:add a1, a1, s1
[0x80006434]:ld t4, 1128(a1)
[0x80006438]:sub a1, a1, s1
[0x8000643c]:addi s1, zero, 0
[0x80006440]:csrrw zero, fcsr, s1
[0x80006444]:fmax.s t6, t5, t4

[0x80006444]:fmax.s t6, t5, t4
[0x80006448]:csrrs a2, fcsr, zero
[0x8000644c]:sd t6, 1072(fp)
[0x80006450]:sd a2, 1080(fp)
[0x80006454]:lui s1, 2
[0x80006458]:addiw s1, s1, 2048
[0x8000645c]:add a1, a1, s1
[0x80006460]:ld t5, 1136(a1)
[0x80006464]:sub a1, a1, s1
[0x80006468]:lui s1, 2
[0x8000646c]:addiw s1, s1, 2048
[0x80006470]:add a1, a1, s1
[0x80006474]:ld t4, 1144(a1)
[0x80006478]:sub a1, a1, s1
[0x8000647c]:addi s1, zero, 0
[0x80006480]:csrrw zero, fcsr, s1
[0x80006484]:fmax.s t6, t5, t4

[0x80006484]:fmax.s t6, t5, t4
[0x80006488]:csrrs a2, fcsr, zero
[0x8000648c]:sd t6, 1088(fp)
[0x80006490]:sd a2, 1096(fp)
[0x80006494]:lui s1, 2
[0x80006498]:addiw s1, s1, 2048
[0x8000649c]:add a1, a1, s1
[0x800064a0]:ld t5, 1152(a1)
[0x800064a4]:sub a1, a1, s1
[0x800064a8]:lui s1, 2
[0x800064ac]:addiw s1, s1, 2048
[0x800064b0]:add a1, a1, s1
[0x800064b4]:ld t4, 1160(a1)
[0x800064b8]:sub a1, a1, s1
[0x800064bc]:addi s1, zero, 0
[0x800064c0]:csrrw zero, fcsr, s1
[0x800064c4]:fmax.s t6, t5, t4

[0x800064c4]:fmax.s t6, t5, t4
[0x800064c8]:csrrs a2, fcsr, zero
[0x800064cc]:sd t6, 1104(fp)
[0x800064d0]:sd a2, 1112(fp)
[0x800064d4]:lui s1, 2
[0x800064d8]:addiw s1, s1, 2048
[0x800064dc]:add a1, a1, s1
[0x800064e0]:ld t5, 1168(a1)
[0x800064e4]:sub a1, a1, s1
[0x800064e8]:lui s1, 2
[0x800064ec]:addiw s1, s1, 2048
[0x800064f0]:add a1, a1, s1
[0x800064f4]:ld t4, 1176(a1)
[0x800064f8]:sub a1, a1, s1
[0x800064fc]:addi s1, zero, 0
[0x80006500]:csrrw zero, fcsr, s1
[0x80006504]:fmax.s t6, t5, t4

[0x80006504]:fmax.s t6, t5, t4
[0x80006508]:csrrs a2, fcsr, zero
[0x8000650c]:sd t6, 1120(fp)
[0x80006510]:sd a2, 1128(fp)
[0x80006514]:lui s1, 2
[0x80006518]:addiw s1, s1, 2048
[0x8000651c]:add a1, a1, s1
[0x80006520]:ld t5, 1184(a1)
[0x80006524]:sub a1, a1, s1
[0x80006528]:lui s1, 2
[0x8000652c]:addiw s1, s1, 2048
[0x80006530]:add a1, a1, s1
[0x80006534]:ld t4, 1192(a1)
[0x80006538]:sub a1, a1, s1
[0x8000653c]:addi s1, zero, 0
[0x80006540]:csrrw zero, fcsr, s1
[0x80006544]:fmax.s t6, t5, t4

[0x80006544]:fmax.s t6, t5, t4
[0x80006548]:csrrs a2, fcsr, zero
[0x8000654c]:sd t6, 1136(fp)
[0x80006550]:sd a2, 1144(fp)
[0x80006554]:lui s1, 2
[0x80006558]:addiw s1, s1, 2048
[0x8000655c]:add a1, a1, s1
[0x80006560]:ld t5, 1200(a1)
[0x80006564]:sub a1, a1, s1
[0x80006568]:lui s1, 2
[0x8000656c]:addiw s1, s1, 2048
[0x80006570]:add a1, a1, s1
[0x80006574]:ld t4, 1208(a1)
[0x80006578]:sub a1, a1, s1
[0x8000657c]:addi s1, zero, 0
[0x80006580]:csrrw zero, fcsr, s1
[0x80006584]:fmax.s t6, t5, t4

[0x80006584]:fmax.s t6, t5, t4
[0x80006588]:csrrs a2, fcsr, zero
[0x8000658c]:sd t6, 1152(fp)
[0x80006590]:sd a2, 1160(fp)
[0x80006594]:lui s1, 2
[0x80006598]:addiw s1, s1, 2048
[0x8000659c]:add a1, a1, s1
[0x800065a0]:ld t5, 1216(a1)
[0x800065a4]:sub a1, a1, s1
[0x800065a8]:lui s1, 2
[0x800065ac]:addiw s1, s1, 2048
[0x800065b0]:add a1, a1, s1
[0x800065b4]:ld t4, 1224(a1)
[0x800065b8]:sub a1, a1, s1
[0x800065bc]:addi s1, zero, 0
[0x800065c0]:csrrw zero, fcsr, s1
[0x800065c4]:fmax.s t6, t5, t4

[0x800065c4]:fmax.s t6, t5, t4
[0x800065c8]:csrrs a2, fcsr, zero
[0x800065cc]:sd t6, 1168(fp)
[0x800065d0]:sd a2, 1176(fp)
[0x800065d4]:lui s1, 2
[0x800065d8]:addiw s1, s1, 2048
[0x800065dc]:add a1, a1, s1
[0x800065e0]:ld t5, 1232(a1)
[0x800065e4]:sub a1, a1, s1
[0x800065e8]:lui s1, 2
[0x800065ec]:addiw s1, s1, 2048
[0x800065f0]:add a1, a1, s1
[0x800065f4]:ld t4, 1240(a1)
[0x800065f8]:sub a1, a1, s1
[0x800065fc]:addi s1, zero, 0
[0x80006600]:csrrw zero, fcsr, s1
[0x80006604]:fmax.s t6, t5, t4

[0x80006604]:fmax.s t6, t5, t4
[0x80006608]:csrrs a2, fcsr, zero
[0x8000660c]:sd t6, 1184(fp)
[0x80006610]:sd a2, 1192(fp)
[0x80006614]:lui s1, 2
[0x80006618]:addiw s1, s1, 2048
[0x8000661c]:add a1, a1, s1
[0x80006620]:ld t5, 1248(a1)
[0x80006624]:sub a1, a1, s1
[0x80006628]:lui s1, 2
[0x8000662c]:addiw s1, s1, 2048
[0x80006630]:add a1, a1, s1
[0x80006634]:ld t4, 1256(a1)
[0x80006638]:sub a1, a1, s1
[0x8000663c]:addi s1, zero, 0
[0x80006640]:csrrw zero, fcsr, s1
[0x80006644]:fmax.s t6, t5, t4

[0x80006644]:fmax.s t6, t5, t4
[0x80006648]:csrrs a2, fcsr, zero
[0x8000664c]:sd t6, 1200(fp)
[0x80006650]:sd a2, 1208(fp)
[0x80006654]:lui s1, 2
[0x80006658]:addiw s1, s1, 2048
[0x8000665c]:add a1, a1, s1
[0x80006660]:ld t5, 1264(a1)
[0x80006664]:sub a1, a1, s1
[0x80006668]:lui s1, 2
[0x8000666c]:addiw s1, s1, 2048
[0x80006670]:add a1, a1, s1
[0x80006674]:ld t4, 1272(a1)
[0x80006678]:sub a1, a1, s1
[0x8000667c]:addi s1, zero, 0
[0x80006680]:csrrw zero, fcsr, s1
[0x80006684]:fmax.s t6, t5, t4

[0x80006684]:fmax.s t6, t5, t4
[0x80006688]:csrrs a2, fcsr, zero
[0x8000668c]:sd t6, 1216(fp)
[0x80006690]:sd a2, 1224(fp)
[0x80006694]:lui s1, 2
[0x80006698]:addiw s1, s1, 2048
[0x8000669c]:add a1, a1, s1
[0x800066a0]:ld t5, 1280(a1)
[0x800066a4]:sub a1, a1, s1
[0x800066a8]:lui s1, 2
[0x800066ac]:addiw s1, s1, 2048
[0x800066b0]:add a1, a1, s1
[0x800066b4]:ld t4, 1288(a1)
[0x800066b8]:sub a1, a1, s1
[0x800066bc]:addi s1, zero, 0
[0x800066c0]:csrrw zero, fcsr, s1
[0x800066c4]:fmax.s t6, t5, t4

[0x800066c4]:fmax.s t6, t5, t4
[0x800066c8]:csrrs a2, fcsr, zero
[0x800066cc]:sd t6, 1232(fp)
[0x800066d0]:sd a2, 1240(fp)
[0x800066d4]:lui s1, 2
[0x800066d8]:addiw s1, s1, 2048
[0x800066dc]:add a1, a1, s1
[0x800066e0]:ld t5, 1296(a1)
[0x800066e4]:sub a1, a1, s1
[0x800066e8]:lui s1, 2
[0x800066ec]:addiw s1, s1, 2048
[0x800066f0]:add a1, a1, s1
[0x800066f4]:ld t4, 1304(a1)
[0x800066f8]:sub a1, a1, s1
[0x800066fc]:addi s1, zero, 0
[0x80006700]:csrrw zero, fcsr, s1
[0x80006704]:fmax.s t6, t5, t4

[0x80006704]:fmax.s t6, t5, t4
[0x80006708]:csrrs a2, fcsr, zero
[0x8000670c]:sd t6, 1248(fp)
[0x80006710]:sd a2, 1256(fp)
[0x80006714]:lui s1, 2
[0x80006718]:addiw s1, s1, 2048
[0x8000671c]:add a1, a1, s1
[0x80006720]:ld t5, 1312(a1)
[0x80006724]:sub a1, a1, s1
[0x80006728]:lui s1, 2
[0x8000672c]:addiw s1, s1, 2048
[0x80006730]:add a1, a1, s1
[0x80006734]:ld t4, 1320(a1)
[0x80006738]:sub a1, a1, s1
[0x8000673c]:addi s1, zero, 0
[0x80006740]:csrrw zero, fcsr, s1
[0x80006744]:fmax.s t6, t5, t4

[0x80006744]:fmax.s t6, t5, t4
[0x80006748]:csrrs a2, fcsr, zero
[0x8000674c]:sd t6, 1264(fp)
[0x80006750]:sd a2, 1272(fp)
[0x80006754]:lui s1, 2
[0x80006758]:addiw s1, s1, 2048
[0x8000675c]:add a1, a1, s1
[0x80006760]:ld t5, 1328(a1)
[0x80006764]:sub a1, a1, s1
[0x80006768]:lui s1, 2
[0x8000676c]:addiw s1, s1, 2048
[0x80006770]:add a1, a1, s1
[0x80006774]:ld t4, 1336(a1)
[0x80006778]:sub a1, a1, s1
[0x8000677c]:addi s1, zero, 0
[0x80006780]:csrrw zero, fcsr, s1
[0x80006784]:fmax.s t6, t5, t4

[0x80006784]:fmax.s t6, t5, t4
[0x80006788]:csrrs a2, fcsr, zero
[0x8000678c]:sd t6, 1280(fp)
[0x80006790]:sd a2, 1288(fp)
[0x80006794]:lui s1, 2
[0x80006798]:addiw s1, s1, 2048
[0x8000679c]:add a1, a1, s1
[0x800067a0]:ld t5, 1344(a1)
[0x800067a4]:sub a1, a1, s1
[0x800067a8]:lui s1, 2
[0x800067ac]:addiw s1, s1, 2048
[0x800067b0]:add a1, a1, s1
[0x800067b4]:ld t4, 1352(a1)
[0x800067b8]:sub a1, a1, s1
[0x800067bc]:addi s1, zero, 0
[0x800067c0]:csrrw zero, fcsr, s1
[0x800067c4]:fmax.s t6, t5, t4

[0x800067c4]:fmax.s t6, t5, t4
[0x800067c8]:csrrs a2, fcsr, zero
[0x800067cc]:sd t6, 1296(fp)
[0x800067d0]:sd a2, 1304(fp)
[0x800067d4]:lui s1, 2
[0x800067d8]:addiw s1, s1, 2048
[0x800067dc]:add a1, a1, s1
[0x800067e0]:ld t5, 1360(a1)
[0x800067e4]:sub a1, a1, s1
[0x800067e8]:lui s1, 2
[0x800067ec]:addiw s1, s1, 2048
[0x800067f0]:add a1, a1, s1
[0x800067f4]:ld t4, 1368(a1)
[0x800067f8]:sub a1, a1, s1
[0x800067fc]:addi s1, zero, 0
[0x80006800]:csrrw zero, fcsr, s1
[0x80006804]:fmax.s t6, t5, t4

[0x80006804]:fmax.s t6, t5, t4
[0x80006808]:csrrs a2, fcsr, zero
[0x8000680c]:sd t6, 1312(fp)
[0x80006810]:sd a2, 1320(fp)
[0x80006814]:lui s1, 2
[0x80006818]:addiw s1, s1, 2048
[0x8000681c]:add a1, a1, s1
[0x80006820]:ld t5, 1376(a1)
[0x80006824]:sub a1, a1, s1
[0x80006828]:lui s1, 2
[0x8000682c]:addiw s1, s1, 2048
[0x80006830]:add a1, a1, s1
[0x80006834]:ld t4, 1384(a1)
[0x80006838]:sub a1, a1, s1
[0x8000683c]:addi s1, zero, 0
[0x80006840]:csrrw zero, fcsr, s1
[0x80006844]:fmax.s t6, t5, t4

[0x80006844]:fmax.s t6, t5, t4
[0x80006848]:csrrs a2, fcsr, zero
[0x8000684c]:sd t6, 1328(fp)
[0x80006850]:sd a2, 1336(fp)
[0x80006854]:lui s1, 2
[0x80006858]:addiw s1, s1, 2048
[0x8000685c]:add a1, a1, s1
[0x80006860]:ld t5, 1392(a1)
[0x80006864]:sub a1, a1, s1
[0x80006868]:lui s1, 2
[0x8000686c]:addiw s1, s1, 2048
[0x80006870]:add a1, a1, s1
[0x80006874]:ld t4, 1400(a1)
[0x80006878]:sub a1, a1, s1
[0x8000687c]:addi s1, zero, 0
[0x80006880]:csrrw zero, fcsr, s1
[0x80006884]:fmax.s t6, t5, t4

[0x80006884]:fmax.s t6, t5, t4
[0x80006888]:csrrs a2, fcsr, zero
[0x8000688c]:sd t6, 1344(fp)
[0x80006890]:sd a2, 1352(fp)
[0x80006894]:lui s1, 2
[0x80006898]:addiw s1, s1, 2048
[0x8000689c]:add a1, a1, s1
[0x800068a0]:ld t5, 1408(a1)
[0x800068a4]:sub a1, a1, s1
[0x800068a8]:lui s1, 2
[0x800068ac]:addiw s1, s1, 2048
[0x800068b0]:add a1, a1, s1
[0x800068b4]:ld t4, 1416(a1)
[0x800068b8]:sub a1, a1, s1
[0x800068bc]:addi s1, zero, 0
[0x800068c0]:csrrw zero, fcsr, s1
[0x800068c4]:fmax.s t6, t5, t4

[0x800068c4]:fmax.s t6, t5, t4
[0x800068c8]:csrrs a2, fcsr, zero
[0x800068cc]:sd t6, 1360(fp)
[0x800068d0]:sd a2, 1368(fp)
[0x800068d4]:lui s1, 2
[0x800068d8]:addiw s1, s1, 2048
[0x800068dc]:add a1, a1, s1
[0x800068e0]:ld t5, 1424(a1)
[0x800068e4]:sub a1, a1, s1
[0x800068e8]:lui s1, 2
[0x800068ec]:addiw s1, s1, 2048
[0x800068f0]:add a1, a1, s1
[0x800068f4]:ld t4, 1432(a1)
[0x800068f8]:sub a1, a1, s1
[0x800068fc]:addi s1, zero, 0
[0x80006900]:csrrw zero, fcsr, s1
[0x80006904]:fmax.s t6, t5, t4

[0x80006904]:fmax.s t6, t5, t4
[0x80006908]:csrrs a2, fcsr, zero
[0x8000690c]:sd t6, 1376(fp)
[0x80006910]:sd a2, 1384(fp)
[0x80006914]:lui s1, 2
[0x80006918]:addiw s1, s1, 2048
[0x8000691c]:add a1, a1, s1
[0x80006920]:ld t5, 1440(a1)
[0x80006924]:sub a1, a1, s1
[0x80006928]:lui s1, 2
[0x8000692c]:addiw s1, s1, 2048
[0x80006930]:add a1, a1, s1
[0x80006934]:ld t4, 1448(a1)
[0x80006938]:sub a1, a1, s1
[0x8000693c]:addi s1, zero, 0
[0x80006940]:csrrw zero, fcsr, s1
[0x80006944]:fmax.s t6, t5, t4

[0x80006944]:fmax.s t6, t5, t4
[0x80006948]:csrrs a2, fcsr, zero
[0x8000694c]:sd t6, 1392(fp)
[0x80006950]:sd a2, 1400(fp)
[0x80006954]:lui s1, 2
[0x80006958]:addiw s1, s1, 2048
[0x8000695c]:add a1, a1, s1
[0x80006960]:ld t5, 1456(a1)
[0x80006964]:sub a1, a1, s1
[0x80006968]:lui s1, 2
[0x8000696c]:addiw s1, s1, 2048
[0x80006970]:add a1, a1, s1
[0x80006974]:ld t4, 1464(a1)
[0x80006978]:sub a1, a1, s1
[0x8000697c]:addi s1, zero, 0
[0x80006980]:csrrw zero, fcsr, s1
[0x80006984]:fmax.s t6, t5, t4

[0x80006984]:fmax.s t6, t5, t4
[0x80006988]:csrrs a2, fcsr, zero
[0x8000698c]:sd t6, 1408(fp)
[0x80006990]:sd a2, 1416(fp)
[0x80006994]:lui s1, 2
[0x80006998]:addiw s1, s1, 2048
[0x8000699c]:add a1, a1, s1
[0x800069a0]:ld t5, 1472(a1)
[0x800069a4]:sub a1, a1, s1
[0x800069a8]:lui s1, 2
[0x800069ac]:addiw s1, s1, 2048
[0x800069b0]:add a1, a1, s1
[0x800069b4]:ld t4, 1480(a1)
[0x800069b8]:sub a1, a1, s1
[0x800069bc]:addi s1, zero, 0
[0x800069c0]:csrrw zero, fcsr, s1
[0x800069c4]:fmax.s t6, t5, t4

[0x800069c4]:fmax.s t6, t5, t4
[0x800069c8]:csrrs a2, fcsr, zero
[0x800069cc]:sd t6, 1424(fp)
[0x800069d0]:sd a2, 1432(fp)
[0x800069d4]:lui s1, 2
[0x800069d8]:addiw s1, s1, 2048
[0x800069dc]:add a1, a1, s1
[0x800069e0]:ld t5, 1488(a1)
[0x800069e4]:sub a1, a1, s1
[0x800069e8]:lui s1, 2
[0x800069ec]:addiw s1, s1, 2048
[0x800069f0]:add a1, a1, s1
[0x800069f4]:ld t4, 1496(a1)
[0x800069f8]:sub a1, a1, s1
[0x800069fc]:addi s1, zero, 0
[0x80006a00]:csrrw zero, fcsr, s1
[0x80006a04]:fmax.s t6, t5, t4

[0x80006a04]:fmax.s t6, t5, t4
[0x80006a08]:csrrs a2, fcsr, zero
[0x80006a0c]:sd t6, 1440(fp)
[0x80006a10]:sd a2, 1448(fp)
[0x80006a14]:lui s1, 2
[0x80006a18]:addiw s1, s1, 2048
[0x80006a1c]:add a1, a1, s1
[0x80006a20]:ld t5, 1504(a1)
[0x80006a24]:sub a1, a1, s1
[0x80006a28]:lui s1, 2
[0x80006a2c]:addiw s1, s1, 2048
[0x80006a30]:add a1, a1, s1
[0x80006a34]:ld t4, 1512(a1)
[0x80006a38]:sub a1, a1, s1
[0x80006a3c]:addi s1, zero, 0
[0x80006a40]:csrrw zero, fcsr, s1
[0x80006a44]:fmax.s t6, t5, t4

[0x80006a44]:fmax.s t6, t5, t4
[0x80006a48]:csrrs a2, fcsr, zero
[0x80006a4c]:sd t6, 1456(fp)
[0x80006a50]:sd a2, 1464(fp)
[0x80006a54]:lui s1, 2
[0x80006a58]:addiw s1, s1, 2048
[0x80006a5c]:add a1, a1, s1
[0x80006a60]:ld t5, 1520(a1)
[0x80006a64]:sub a1, a1, s1
[0x80006a68]:lui s1, 2
[0x80006a6c]:addiw s1, s1, 2048
[0x80006a70]:add a1, a1, s1
[0x80006a74]:ld t4, 1528(a1)
[0x80006a78]:sub a1, a1, s1
[0x80006a7c]:addi s1, zero, 0
[0x80006a80]:csrrw zero, fcsr, s1
[0x80006a84]:fmax.s t6, t5, t4

[0x80006a84]:fmax.s t6, t5, t4
[0x80006a88]:csrrs a2, fcsr, zero
[0x80006a8c]:sd t6, 1472(fp)
[0x80006a90]:sd a2, 1480(fp)
[0x80006a94]:lui s1, 2
[0x80006a98]:addiw s1, s1, 2048
[0x80006a9c]:add a1, a1, s1
[0x80006aa0]:ld t5, 1536(a1)
[0x80006aa4]:sub a1, a1, s1
[0x80006aa8]:lui s1, 2
[0x80006aac]:addiw s1, s1, 2048
[0x80006ab0]:add a1, a1, s1
[0x80006ab4]:ld t4, 1544(a1)
[0x80006ab8]:sub a1, a1, s1
[0x80006abc]:addi s1, zero, 0
[0x80006ac0]:csrrw zero, fcsr, s1
[0x80006ac4]:fmax.s t6, t5, t4

[0x80006ac4]:fmax.s t6, t5, t4
[0x80006ac8]:csrrs a2, fcsr, zero
[0x80006acc]:sd t6, 1488(fp)
[0x80006ad0]:sd a2, 1496(fp)
[0x80006ad4]:lui s1, 2
[0x80006ad8]:addiw s1, s1, 2048
[0x80006adc]:add a1, a1, s1
[0x80006ae0]:ld t5, 1552(a1)
[0x80006ae4]:sub a1, a1, s1
[0x80006ae8]:lui s1, 2
[0x80006aec]:addiw s1, s1, 2048
[0x80006af0]:add a1, a1, s1
[0x80006af4]:ld t4, 1560(a1)
[0x80006af8]:sub a1, a1, s1
[0x80006afc]:addi s1, zero, 0
[0x80006b00]:csrrw zero, fcsr, s1
[0x80006b04]:fmax.s t6, t5, t4

[0x80006b04]:fmax.s t6, t5, t4
[0x80006b08]:csrrs a2, fcsr, zero
[0x80006b0c]:sd t6, 1504(fp)
[0x80006b10]:sd a2, 1512(fp)
[0x80006b14]:lui s1, 2
[0x80006b18]:addiw s1, s1, 2048
[0x80006b1c]:add a1, a1, s1
[0x80006b20]:ld t5, 1568(a1)
[0x80006b24]:sub a1, a1, s1
[0x80006b28]:lui s1, 2
[0x80006b2c]:addiw s1, s1, 2048
[0x80006b30]:add a1, a1, s1
[0x80006b34]:ld t4, 1576(a1)
[0x80006b38]:sub a1, a1, s1
[0x80006b3c]:addi s1, zero, 0
[0x80006b40]:csrrw zero, fcsr, s1
[0x80006b44]:fmax.s t6, t5, t4

[0x80006b44]:fmax.s t6, t5, t4
[0x80006b48]:csrrs a2, fcsr, zero
[0x80006b4c]:sd t6, 1520(fp)
[0x80006b50]:sd a2, 1528(fp)
[0x80006b54]:lui s1, 2
[0x80006b58]:addiw s1, s1, 2048
[0x80006b5c]:add a1, a1, s1
[0x80006b60]:ld t5, 1584(a1)
[0x80006b64]:sub a1, a1, s1
[0x80006b68]:lui s1, 2
[0x80006b6c]:addiw s1, s1, 2048
[0x80006b70]:add a1, a1, s1
[0x80006b74]:ld t4, 1592(a1)
[0x80006b78]:sub a1, a1, s1
[0x80006b7c]:addi s1, zero, 0
[0x80006b80]:csrrw zero, fcsr, s1
[0x80006b84]:fmax.s t6, t5, t4

[0x80006b84]:fmax.s t6, t5, t4
[0x80006b88]:csrrs a2, fcsr, zero
[0x80006b8c]:sd t6, 1536(fp)
[0x80006b90]:sd a2, 1544(fp)
[0x80006b94]:lui s1, 2
[0x80006b98]:addiw s1, s1, 2048
[0x80006b9c]:add a1, a1, s1
[0x80006ba0]:ld t5, 1600(a1)
[0x80006ba4]:sub a1, a1, s1
[0x80006ba8]:lui s1, 2
[0x80006bac]:addiw s1, s1, 2048
[0x80006bb0]:add a1, a1, s1
[0x80006bb4]:ld t4, 1608(a1)
[0x80006bb8]:sub a1, a1, s1
[0x80006bbc]:addi s1, zero, 0
[0x80006bc0]:csrrw zero, fcsr, s1
[0x80006bc4]:fmax.s t6, t5, t4

[0x80006bc4]:fmax.s t6, t5, t4
[0x80006bc8]:csrrs a2, fcsr, zero
[0x80006bcc]:sd t6, 1552(fp)
[0x80006bd0]:sd a2, 1560(fp)
[0x80006bd4]:lui s1, 2
[0x80006bd8]:addiw s1, s1, 2048
[0x80006bdc]:add a1, a1, s1
[0x80006be0]:ld t5, 1616(a1)
[0x80006be4]:sub a1, a1, s1
[0x80006be8]:lui s1, 2
[0x80006bec]:addiw s1, s1, 2048
[0x80006bf0]:add a1, a1, s1
[0x80006bf4]:ld t4, 1624(a1)
[0x80006bf8]:sub a1, a1, s1
[0x80006bfc]:addi s1, zero, 0
[0x80006c00]:csrrw zero, fcsr, s1
[0x80006c04]:fmax.s t6, t5, t4

[0x80006c04]:fmax.s t6, t5, t4
[0x80006c08]:csrrs a2, fcsr, zero
[0x80006c0c]:sd t6, 1568(fp)
[0x80006c10]:sd a2, 1576(fp)
[0x80006c14]:lui s1, 2
[0x80006c18]:addiw s1, s1, 2048
[0x80006c1c]:add a1, a1, s1
[0x80006c20]:ld t5, 1632(a1)
[0x80006c24]:sub a1, a1, s1
[0x80006c28]:lui s1, 2
[0x80006c2c]:addiw s1, s1, 2048
[0x80006c30]:add a1, a1, s1
[0x80006c34]:ld t4, 1640(a1)
[0x80006c38]:sub a1, a1, s1
[0x80006c3c]:addi s1, zero, 0
[0x80006c40]:csrrw zero, fcsr, s1
[0x80006c44]:fmax.s t6, t5, t4

[0x80006c44]:fmax.s t6, t5, t4
[0x80006c48]:csrrs a2, fcsr, zero
[0x80006c4c]:sd t6, 1584(fp)
[0x80006c50]:sd a2, 1592(fp)
[0x80006c54]:lui s1, 2
[0x80006c58]:addiw s1, s1, 2048
[0x80006c5c]:add a1, a1, s1
[0x80006c60]:ld t5, 1648(a1)
[0x80006c64]:sub a1, a1, s1
[0x80006c68]:lui s1, 2
[0x80006c6c]:addiw s1, s1, 2048
[0x80006c70]:add a1, a1, s1
[0x80006c74]:ld t4, 1656(a1)
[0x80006c78]:sub a1, a1, s1
[0x80006c7c]:addi s1, zero, 0
[0x80006c80]:csrrw zero, fcsr, s1
[0x80006c84]:fmax.s t6, t5, t4

[0x80006c84]:fmax.s t6, t5, t4
[0x80006c88]:csrrs a2, fcsr, zero
[0x80006c8c]:sd t6, 1600(fp)
[0x80006c90]:sd a2, 1608(fp)
[0x80006c94]:lui s1, 2
[0x80006c98]:addiw s1, s1, 2048
[0x80006c9c]:add a1, a1, s1
[0x80006ca0]:ld t5, 1664(a1)
[0x80006ca4]:sub a1, a1, s1
[0x80006ca8]:lui s1, 2
[0x80006cac]:addiw s1, s1, 2048
[0x80006cb0]:add a1, a1, s1
[0x80006cb4]:ld t4, 1672(a1)
[0x80006cb8]:sub a1, a1, s1
[0x80006cbc]:addi s1, zero, 0
[0x80006cc0]:csrrw zero, fcsr, s1
[0x80006cc4]:fmax.s t6, t5, t4

[0x80006cc4]:fmax.s t6, t5, t4
[0x80006cc8]:csrrs a2, fcsr, zero
[0x80006ccc]:sd t6, 1616(fp)
[0x80006cd0]:sd a2, 1624(fp)
[0x80006cd4]:lui s1, 2
[0x80006cd8]:addiw s1, s1, 2048
[0x80006cdc]:add a1, a1, s1
[0x80006ce0]:ld t5, 1680(a1)
[0x80006ce4]:sub a1, a1, s1
[0x80006ce8]:lui s1, 2
[0x80006cec]:addiw s1, s1, 2048
[0x80006cf0]:add a1, a1, s1
[0x80006cf4]:ld t4, 1688(a1)
[0x80006cf8]:sub a1, a1, s1
[0x80006cfc]:addi s1, zero, 0
[0x80006d00]:csrrw zero, fcsr, s1
[0x80006d04]:fmax.s t6, t5, t4

[0x80006d04]:fmax.s t6, t5, t4
[0x80006d08]:csrrs a2, fcsr, zero
[0x80006d0c]:sd t6, 1632(fp)
[0x80006d10]:sd a2, 1640(fp)
[0x80006d14]:lui s1, 2
[0x80006d18]:addiw s1, s1, 2048
[0x80006d1c]:add a1, a1, s1
[0x80006d20]:ld t5, 1696(a1)
[0x80006d24]:sub a1, a1, s1
[0x80006d28]:lui s1, 2
[0x80006d2c]:addiw s1, s1, 2048
[0x80006d30]:add a1, a1, s1
[0x80006d34]:ld t4, 1704(a1)
[0x80006d38]:sub a1, a1, s1
[0x80006d3c]:addi s1, zero, 0
[0x80006d40]:csrrw zero, fcsr, s1
[0x80006d44]:fmax.s t6, t5, t4

[0x80006d44]:fmax.s t6, t5, t4
[0x80006d48]:csrrs a2, fcsr, zero
[0x80006d4c]:sd t6, 1648(fp)
[0x80006d50]:sd a2, 1656(fp)
[0x80006d54]:lui s1, 2
[0x80006d58]:addiw s1, s1, 2048
[0x80006d5c]:add a1, a1, s1
[0x80006d60]:ld t5, 1712(a1)
[0x80006d64]:sub a1, a1, s1
[0x80006d68]:lui s1, 2
[0x80006d6c]:addiw s1, s1, 2048
[0x80006d70]:add a1, a1, s1
[0x80006d74]:ld t4, 1720(a1)
[0x80006d78]:sub a1, a1, s1
[0x80006d7c]:addi s1, zero, 0
[0x80006d80]:csrrw zero, fcsr, s1
[0x80006d84]:fmax.s t6, t5, t4

[0x80006d84]:fmax.s t6, t5, t4
[0x80006d88]:csrrs a2, fcsr, zero
[0x80006d8c]:sd t6, 1664(fp)
[0x80006d90]:sd a2, 1672(fp)
[0x80006d94]:lui s1, 2
[0x80006d98]:addiw s1, s1, 2048
[0x80006d9c]:add a1, a1, s1
[0x80006da0]:ld t5, 1728(a1)
[0x80006da4]:sub a1, a1, s1
[0x80006da8]:lui s1, 2
[0x80006dac]:addiw s1, s1, 2048
[0x80006db0]:add a1, a1, s1
[0x80006db4]:ld t4, 1736(a1)
[0x80006db8]:sub a1, a1, s1
[0x80006dbc]:addi s1, zero, 0
[0x80006dc0]:csrrw zero, fcsr, s1
[0x80006dc4]:fmax.s t6, t5, t4

[0x80006dc4]:fmax.s t6, t5, t4
[0x80006dc8]:csrrs a2, fcsr, zero
[0x80006dcc]:sd t6, 1680(fp)
[0x80006dd0]:sd a2, 1688(fp)
[0x80006dd4]:lui s1, 2
[0x80006dd8]:addiw s1, s1, 2048
[0x80006ddc]:add a1, a1, s1
[0x80006de0]:ld t5, 1744(a1)
[0x80006de4]:sub a1, a1, s1
[0x80006de8]:lui s1, 2
[0x80006dec]:addiw s1, s1, 2048
[0x80006df0]:add a1, a1, s1
[0x80006df4]:ld t4, 1752(a1)
[0x80006df8]:sub a1, a1, s1
[0x80006dfc]:addi s1, zero, 0
[0x80006e00]:csrrw zero, fcsr, s1
[0x80006e04]:fmax.s t6, t5, t4

[0x80006e04]:fmax.s t6, t5, t4
[0x80006e08]:csrrs a2, fcsr, zero
[0x80006e0c]:sd t6, 1696(fp)
[0x80006e10]:sd a2, 1704(fp)
[0x80006e14]:lui s1, 2
[0x80006e18]:addiw s1, s1, 2048
[0x80006e1c]:add a1, a1, s1
[0x80006e20]:ld t5, 1760(a1)
[0x80006e24]:sub a1, a1, s1
[0x80006e28]:lui s1, 2
[0x80006e2c]:addiw s1, s1, 2048
[0x80006e30]:add a1, a1, s1
[0x80006e34]:ld t4, 1768(a1)
[0x80006e38]:sub a1, a1, s1
[0x80006e3c]:addi s1, zero, 0
[0x80006e40]:csrrw zero, fcsr, s1
[0x80006e44]:fmax.s t6, t5, t4

[0x80006e44]:fmax.s t6, t5, t4
[0x80006e48]:csrrs a2, fcsr, zero
[0x80006e4c]:sd t6, 1712(fp)
[0x80006e50]:sd a2, 1720(fp)
[0x80006e54]:lui s1, 2
[0x80006e58]:addiw s1, s1, 2048
[0x80006e5c]:add a1, a1, s1
[0x80006e60]:ld t5, 1776(a1)
[0x80006e64]:sub a1, a1, s1
[0x80006e68]:lui s1, 2
[0x80006e6c]:addiw s1, s1, 2048
[0x80006e70]:add a1, a1, s1
[0x80006e74]:ld t4, 1784(a1)
[0x80006e78]:sub a1, a1, s1
[0x80006e7c]:addi s1, zero, 0
[0x80006e80]:csrrw zero, fcsr, s1
[0x80006e84]:fmax.s t6, t5, t4

[0x80006e84]:fmax.s t6, t5, t4
[0x80006e88]:csrrs a2, fcsr, zero
[0x80006e8c]:sd t6, 1728(fp)
[0x80006e90]:sd a2, 1736(fp)
[0x80006e94]:lui s1, 2
[0x80006e98]:addiw s1, s1, 2048
[0x80006e9c]:add a1, a1, s1
[0x80006ea0]:ld t5, 1792(a1)
[0x80006ea4]:sub a1, a1, s1
[0x80006ea8]:lui s1, 2
[0x80006eac]:addiw s1, s1, 2048
[0x80006eb0]:add a1, a1, s1
[0x80006eb4]:ld t4, 1800(a1)
[0x80006eb8]:sub a1, a1, s1
[0x80006ebc]:addi s1, zero, 0
[0x80006ec0]:csrrw zero, fcsr, s1
[0x80006ec4]:fmax.s t6, t5, t4

[0x80006ec4]:fmax.s t6, t5, t4
[0x80006ec8]:csrrs a2, fcsr, zero
[0x80006ecc]:sd t6, 1744(fp)
[0x80006ed0]:sd a2, 1752(fp)
[0x80006ed4]:lui s1, 2
[0x80006ed8]:addiw s1, s1, 2048
[0x80006edc]:add a1, a1, s1
[0x80006ee0]:ld t5, 1808(a1)
[0x80006ee4]:sub a1, a1, s1
[0x80006ee8]:lui s1, 2
[0x80006eec]:addiw s1, s1, 2048
[0x80006ef0]:add a1, a1, s1
[0x80006ef4]:ld t4, 1816(a1)
[0x80006ef8]:sub a1, a1, s1
[0x80006efc]:addi s1, zero, 0
[0x80006f00]:csrrw zero, fcsr, s1
[0x80006f04]:fmax.s t6, t5, t4

[0x80006f04]:fmax.s t6, t5, t4
[0x80006f08]:csrrs a2, fcsr, zero
[0x80006f0c]:sd t6, 1760(fp)
[0x80006f10]:sd a2, 1768(fp)
[0x80006f14]:lui s1, 2
[0x80006f18]:addiw s1, s1, 2048
[0x80006f1c]:add a1, a1, s1
[0x80006f20]:ld t5, 1824(a1)
[0x80006f24]:sub a1, a1, s1
[0x80006f28]:lui s1, 2
[0x80006f2c]:addiw s1, s1, 2048
[0x80006f30]:add a1, a1, s1
[0x80006f34]:ld t4, 1832(a1)
[0x80006f38]:sub a1, a1, s1
[0x80006f3c]:addi s1, zero, 0
[0x80006f40]:csrrw zero, fcsr, s1
[0x80006f44]:fmax.s t6, t5, t4

[0x80006f44]:fmax.s t6, t5, t4
[0x80006f48]:csrrs a2, fcsr, zero
[0x80006f4c]:sd t6, 1776(fp)
[0x80006f50]:sd a2, 1784(fp)
[0x80006f54]:lui s1, 2
[0x80006f58]:addiw s1, s1, 2048
[0x80006f5c]:add a1, a1, s1
[0x80006f60]:ld t5, 1840(a1)
[0x80006f64]:sub a1, a1, s1
[0x80006f68]:lui s1, 2
[0x80006f6c]:addiw s1, s1, 2048
[0x80006f70]:add a1, a1, s1
[0x80006f74]:ld t4, 1848(a1)
[0x80006f78]:sub a1, a1, s1
[0x80006f7c]:addi s1, zero, 0
[0x80006f80]:csrrw zero, fcsr, s1
[0x80006f84]:fmax.s t6, t5, t4

[0x80006f84]:fmax.s t6, t5, t4
[0x80006f88]:csrrs a2, fcsr, zero
[0x80006f8c]:sd t6, 1792(fp)
[0x80006f90]:sd a2, 1800(fp)
[0x80006f94]:lui s1, 2
[0x80006f98]:addiw s1, s1, 2048
[0x80006f9c]:add a1, a1, s1
[0x80006fa0]:ld t5, 1856(a1)
[0x80006fa4]:sub a1, a1, s1
[0x80006fa8]:lui s1, 2
[0x80006fac]:addiw s1, s1, 2048
[0x80006fb0]:add a1, a1, s1
[0x80006fb4]:ld t4, 1864(a1)
[0x80006fb8]:sub a1, a1, s1
[0x80006fbc]:addi s1, zero, 0
[0x80006fc0]:csrrw zero, fcsr, s1
[0x80006fc4]:fmax.s t6, t5, t4

[0x80006fc4]:fmax.s t6, t5, t4
[0x80006fc8]:csrrs a2, fcsr, zero
[0x80006fcc]:sd t6, 1808(fp)
[0x80006fd0]:sd a2, 1816(fp)
[0x80006fd4]:lui s1, 2
[0x80006fd8]:addiw s1, s1, 2048
[0x80006fdc]:add a1, a1, s1
[0x80006fe0]:ld t5, 1872(a1)
[0x80006fe4]:sub a1, a1, s1
[0x80006fe8]:lui s1, 2
[0x80006fec]:addiw s1, s1, 2048
[0x80006ff0]:add a1, a1, s1
[0x80006ff4]:ld t4, 1880(a1)
[0x80006ff8]:sub a1, a1, s1
[0x80006ffc]:addi s1, zero, 0
[0x80007000]:csrrw zero, fcsr, s1
[0x80007004]:fmax.s t6, t5, t4

[0x80007004]:fmax.s t6, t5, t4
[0x80007008]:csrrs a2, fcsr, zero
[0x8000700c]:sd t6, 1824(fp)
[0x80007010]:sd a2, 1832(fp)
[0x80007014]:lui s1, 2
[0x80007018]:addiw s1, s1, 2048
[0x8000701c]:add a1, a1, s1
[0x80007020]:ld t5, 1888(a1)
[0x80007024]:sub a1, a1, s1
[0x80007028]:lui s1, 2
[0x8000702c]:addiw s1, s1, 2048
[0x80007030]:add a1, a1, s1
[0x80007034]:ld t4, 1896(a1)
[0x80007038]:sub a1, a1, s1
[0x8000703c]:addi s1, zero, 0
[0x80007040]:csrrw zero, fcsr, s1
[0x80007044]:fmax.s t6, t5, t4

[0x80007044]:fmax.s t6, t5, t4
[0x80007048]:csrrs a2, fcsr, zero
[0x8000704c]:sd t6, 1840(fp)
[0x80007050]:sd a2, 1848(fp)
[0x80007054]:lui s1, 2
[0x80007058]:addiw s1, s1, 2048
[0x8000705c]:add a1, a1, s1
[0x80007060]:ld t5, 1904(a1)
[0x80007064]:sub a1, a1, s1
[0x80007068]:lui s1, 2
[0x8000706c]:addiw s1, s1, 2048
[0x80007070]:add a1, a1, s1
[0x80007074]:ld t4, 1912(a1)
[0x80007078]:sub a1, a1, s1
[0x8000707c]:addi s1, zero, 0
[0x80007080]:csrrw zero, fcsr, s1
[0x80007084]:fmax.s t6, t5, t4

[0x80007084]:fmax.s t6, t5, t4
[0x80007088]:csrrs a2, fcsr, zero
[0x8000708c]:sd t6, 1856(fp)
[0x80007090]:sd a2, 1864(fp)
[0x80007094]:lui s1, 2
[0x80007098]:addiw s1, s1, 2048
[0x8000709c]:add a1, a1, s1
[0x800070a0]:ld t5, 1920(a1)
[0x800070a4]:sub a1, a1, s1
[0x800070a8]:lui s1, 2
[0x800070ac]:addiw s1, s1, 2048
[0x800070b0]:add a1, a1, s1
[0x800070b4]:ld t4, 1928(a1)
[0x800070b8]:sub a1, a1, s1
[0x800070bc]:addi s1, zero, 0
[0x800070c0]:csrrw zero, fcsr, s1
[0x800070c4]:fmax.s t6, t5, t4

[0x800070c4]:fmax.s t6, t5, t4
[0x800070c8]:csrrs a2, fcsr, zero
[0x800070cc]:sd t6, 1872(fp)
[0x800070d0]:sd a2, 1880(fp)
[0x800070d4]:lui s1, 2
[0x800070d8]:addiw s1, s1, 2048
[0x800070dc]:add a1, a1, s1
[0x800070e0]:ld t5, 1936(a1)
[0x800070e4]:sub a1, a1, s1
[0x800070e8]:lui s1, 2
[0x800070ec]:addiw s1, s1, 2048
[0x800070f0]:add a1, a1, s1
[0x800070f4]:ld t4, 1944(a1)
[0x800070f8]:sub a1, a1, s1
[0x800070fc]:addi s1, zero, 0
[0x80007100]:csrrw zero, fcsr, s1
[0x80007104]:fmax.s t6, t5, t4

[0x80007104]:fmax.s t6, t5, t4
[0x80007108]:csrrs a2, fcsr, zero
[0x8000710c]:sd t6, 1888(fp)
[0x80007110]:sd a2, 1896(fp)
[0x80007114]:lui s1, 2
[0x80007118]:addiw s1, s1, 2048
[0x8000711c]:add a1, a1, s1
[0x80007120]:ld t5, 1952(a1)
[0x80007124]:sub a1, a1, s1
[0x80007128]:lui s1, 2
[0x8000712c]:addiw s1, s1, 2048
[0x80007130]:add a1, a1, s1
[0x80007134]:ld t4, 1960(a1)
[0x80007138]:sub a1, a1, s1
[0x8000713c]:addi s1, zero, 0
[0x80007140]:csrrw zero, fcsr, s1
[0x80007144]:fmax.s t6, t5, t4

[0x80007144]:fmax.s t6, t5, t4
[0x80007148]:csrrs a2, fcsr, zero
[0x8000714c]:sd t6, 1904(fp)
[0x80007150]:sd a2, 1912(fp)
[0x80007154]:lui s1, 2
[0x80007158]:addiw s1, s1, 2048
[0x8000715c]:add a1, a1, s1
[0x80007160]:ld t5, 1968(a1)
[0x80007164]:sub a1, a1, s1
[0x80007168]:lui s1, 2
[0x8000716c]:addiw s1, s1, 2048
[0x80007170]:add a1, a1, s1
[0x80007174]:ld t4, 1976(a1)
[0x80007178]:sub a1, a1, s1
[0x8000717c]:addi s1, zero, 0
[0x80007180]:csrrw zero, fcsr, s1
[0x80007184]:fmax.s t6, t5, t4

[0x80007184]:fmax.s t6, t5, t4
[0x80007188]:csrrs a2, fcsr, zero
[0x8000718c]:sd t6, 1920(fp)
[0x80007190]:sd a2, 1928(fp)
[0x80007194]:lui s1, 2
[0x80007198]:addiw s1, s1, 2048
[0x8000719c]:add a1, a1, s1
[0x800071a0]:ld t5, 1984(a1)
[0x800071a4]:sub a1, a1, s1
[0x800071a8]:lui s1, 2
[0x800071ac]:addiw s1, s1, 2048
[0x800071b0]:add a1, a1, s1
[0x800071b4]:ld t4, 1992(a1)
[0x800071b8]:sub a1, a1, s1
[0x800071bc]:addi s1, zero, 0
[0x800071c0]:csrrw zero, fcsr, s1
[0x800071c4]:fmax.s t6, t5, t4

[0x800071c4]:fmax.s t6, t5, t4
[0x800071c8]:csrrs a2, fcsr, zero
[0x800071cc]:sd t6, 1936(fp)
[0x800071d0]:sd a2, 1944(fp)
[0x800071d4]:lui s1, 2
[0x800071d8]:addiw s1, s1, 2048
[0x800071dc]:add a1, a1, s1
[0x800071e0]:ld t5, 2000(a1)
[0x800071e4]:sub a1, a1, s1
[0x800071e8]:lui s1, 2
[0x800071ec]:addiw s1, s1, 2048
[0x800071f0]:add a1, a1, s1
[0x800071f4]:ld t4, 2008(a1)
[0x800071f8]:sub a1, a1, s1
[0x800071fc]:addi s1, zero, 0
[0x80007200]:csrrw zero, fcsr, s1
[0x80007204]:fmax.s t6, t5, t4

[0x80007204]:fmax.s t6, t5, t4
[0x80007208]:csrrs a2, fcsr, zero
[0x8000720c]:sd t6, 1952(fp)
[0x80007210]:sd a2, 1960(fp)
[0x80007214]:lui s1, 2
[0x80007218]:addiw s1, s1, 2048
[0x8000721c]:add a1, a1, s1
[0x80007220]:ld t5, 2016(a1)
[0x80007224]:sub a1, a1, s1
[0x80007228]:lui s1, 2
[0x8000722c]:addiw s1, s1, 2048
[0x80007230]:add a1, a1, s1
[0x80007234]:ld t4, 2024(a1)
[0x80007238]:sub a1, a1, s1
[0x8000723c]:addi s1, zero, 0
[0x80007240]:csrrw zero, fcsr, s1
[0x80007244]:fmax.s t6, t5, t4

[0x80007244]:fmax.s t6, t5, t4
[0x80007248]:csrrs a2, fcsr, zero
[0x8000724c]:sd t6, 1968(fp)
[0x80007250]:sd a2, 1976(fp)
[0x80007254]:lui s1, 2
[0x80007258]:addiw s1, s1, 2048
[0x8000725c]:add a1, a1, s1
[0x80007260]:ld t5, 2032(a1)
[0x80007264]:sub a1, a1, s1
[0x80007268]:lui s1, 2
[0x8000726c]:addiw s1, s1, 2048
[0x80007270]:add a1, a1, s1
[0x80007274]:ld t4, 2040(a1)
[0x80007278]:sub a1, a1, s1
[0x8000727c]:addi s1, zero, 0
[0x80007280]:csrrw zero, fcsr, s1
[0x80007284]:fmax.s t6, t5, t4

[0x80007284]:fmax.s t6, t5, t4
[0x80007288]:csrrs a2, fcsr, zero
[0x8000728c]:sd t6, 1984(fp)
[0x80007290]:sd a2, 1992(fp)
[0x80007294]:lui s1, 2
[0x80007298]:add a1, a1, s1
[0x8000729c]:ld t5, 0(a1)
[0x800072a0]:sub a1, a1, s1
[0x800072a4]:lui s1, 2
[0x800072a8]:add a1, a1, s1
[0x800072ac]:ld t4, 8(a1)
[0x800072b0]:sub a1, a1, s1
[0x800072b4]:addi s1, zero, 0
[0x800072b8]:csrrw zero, fcsr, s1
[0x800072bc]:fmax.s t6, t5, t4

[0x800072bc]:fmax.s t6, t5, t4
[0x800072c0]:csrrs a2, fcsr, zero
[0x800072c4]:sd t6, 2000(fp)
[0x800072c8]:sd a2, 2008(fp)
[0x800072cc]:lui s1, 2
[0x800072d0]:add a1, a1, s1
[0x800072d4]:ld t5, 16(a1)
[0x800072d8]:sub a1, a1, s1
[0x800072dc]:lui s1, 2
[0x800072e0]:add a1, a1, s1
[0x800072e4]:ld t4, 24(a1)
[0x800072e8]:sub a1, a1, s1
[0x800072ec]:addi s1, zero, 0
[0x800072f0]:csrrw zero, fcsr, s1
[0x800072f4]:fmax.s t6, t5, t4

[0x800072f4]:fmax.s t6, t5, t4
[0x800072f8]:csrrs a2, fcsr, zero
[0x800072fc]:sd t6, 2016(fp)
[0x80007300]:sd a2, 2024(fp)
[0x80007304]:lui s1, 2
[0x80007308]:add a1, a1, s1
[0x8000730c]:ld t5, 32(a1)
[0x80007310]:sub a1, a1, s1
[0x80007314]:lui s1, 2
[0x80007318]:add a1, a1, s1
[0x8000731c]:ld t4, 40(a1)
[0x80007320]:sub a1, a1, s1
[0x80007324]:addi s1, zero, 0
[0x80007328]:csrrw zero, fcsr, s1
[0x8000732c]:fmax.s t6, t5, t4

[0x8000732c]:fmax.s t6, t5, t4
[0x80007330]:csrrs a2, fcsr, zero
[0x80007334]:sd t6, 2032(fp)
[0x80007338]:sd a2, 2040(fp)
[0x8000733c]:auipc fp, 14
[0x80007340]:addi fp, fp, 3180
[0x80007344]:lui s1, 2
[0x80007348]:add a1, a1, s1
[0x8000734c]:ld t5, 48(a1)
[0x80007350]:sub a1, a1, s1
[0x80007354]:lui s1, 2
[0x80007358]:add a1, a1, s1
[0x8000735c]:ld t4, 56(a1)
[0x80007360]:sub a1, a1, s1
[0x80007364]:addi s1, zero, 0
[0x80007368]:csrrw zero, fcsr, s1
[0x8000736c]:fmax.s t6, t5, t4

[0x8000736c]:fmax.s t6, t5, t4
[0x80007370]:csrrs a2, fcsr, zero
[0x80007374]:sd t6, 0(fp)
[0x80007378]:sd a2, 8(fp)
[0x8000737c]:lui s1, 2
[0x80007380]:add a1, a1, s1
[0x80007384]:ld t5, 64(a1)
[0x80007388]:sub a1, a1, s1
[0x8000738c]:lui s1, 2
[0x80007390]:add a1, a1, s1
[0x80007394]:ld t4, 72(a1)
[0x80007398]:sub a1, a1, s1
[0x8000739c]:addi s1, zero, 0
[0x800073a0]:csrrw zero, fcsr, s1
[0x800073a4]:fmax.s t6, t5, t4

[0x800073a4]:fmax.s t6, t5, t4
[0x800073a8]:csrrs a2, fcsr, zero
[0x800073ac]:sd t6, 16(fp)
[0x800073b0]:sd a2, 24(fp)
[0x800073b4]:lui s1, 2
[0x800073b8]:add a1, a1, s1
[0x800073bc]:ld t5, 80(a1)
[0x800073c0]:sub a1, a1, s1
[0x800073c4]:lui s1, 2
[0x800073c8]:add a1, a1, s1
[0x800073cc]:ld t4, 88(a1)
[0x800073d0]:sub a1, a1, s1
[0x800073d4]:addi s1, zero, 0
[0x800073d8]:csrrw zero, fcsr, s1
[0x800073dc]:fmax.s t6, t5, t4

[0x800073dc]:fmax.s t6, t5, t4
[0x800073e0]:csrrs a2, fcsr, zero
[0x800073e4]:sd t6, 32(fp)
[0x800073e8]:sd a2, 40(fp)
[0x800073ec]:lui s1, 2
[0x800073f0]:add a1, a1, s1
[0x800073f4]:ld t5, 96(a1)
[0x800073f8]:sub a1, a1, s1
[0x800073fc]:lui s1, 2
[0x80007400]:add a1, a1, s1
[0x80007404]:ld t4, 104(a1)
[0x80007408]:sub a1, a1, s1
[0x8000740c]:addi s1, zero, 0
[0x80007410]:csrrw zero, fcsr, s1
[0x80007414]:fmax.s t6, t5, t4

[0x80007414]:fmax.s t6, t5, t4
[0x80007418]:csrrs a2, fcsr, zero
[0x8000741c]:sd t6, 48(fp)
[0x80007420]:sd a2, 56(fp)
[0x80007424]:lui s1, 2
[0x80007428]:add a1, a1, s1
[0x8000742c]:ld t5, 112(a1)
[0x80007430]:sub a1, a1, s1
[0x80007434]:lui s1, 2
[0x80007438]:add a1, a1, s1
[0x8000743c]:ld t4, 120(a1)
[0x80007440]:sub a1, a1, s1
[0x80007444]:addi s1, zero, 0
[0x80007448]:csrrw zero, fcsr, s1
[0x8000744c]:fmax.s t6, t5, t4

[0x8000744c]:fmax.s t6, t5, t4
[0x80007450]:csrrs a2, fcsr, zero
[0x80007454]:sd t6, 64(fp)
[0x80007458]:sd a2, 72(fp)
[0x8000745c]:lui s1, 2
[0x80007460]:add a1, a1, s1
[0x80007464]:ld t5, 128(a1)
[0x80007468]:sub a1, a1, s1
[0x8000746c]:lui s1, 2
[0x80007470]:add a1, a1, s1
[0x80007474]:ld t4, 136(a1)
[0x80007478]:sub a1, a1, s1
[0x8000747c]:addi s1, zero, 0
[0x80007480]:csrrw zero, fcsr, s1
[0x80007484]:fmax.s t6, t5, t4

[0x80007484]:fmax.s t6, t5, t4
[0x80007488]:csrrs a2, fcsr, zero
[0x8000748c]:sd t6, 80(fp)
[0x80007490]:sd a2, 88(fp)
[0x80007494]:lui s1, 2
[0x80007498]:add a1, a1, s1
[0x8000749c]:ld t5, 144(a1)
[0x800074a0]:sub a1, a1, s1
[0x800074a4]:lui s1, 2
[0x800074a8]:add a1, a1, s1
[0x800074ac]:ld t4, 152(a1)
[0x800074b0]:sub a1, a1, s1
[0x800074b4]:addi s1, zero, 0
[0x800074b8]:csrrw zero, fcsr, s1
[0x800074bc]:fmax.s t6, t5, t4

[0x800074bc]:fmax.s t6, t5, t4
[0x800074c0]:csrrs a2, fcsr, zero
[0x800074c4]:sd t6, 96(fp)
[0x800074c8]:sd a2, 104(fp)
[0x800074cc]:lui s1, 2
[0x800074d0]:add a1, a1, s1
[0x800074d4]:ld t5, 160(a1)
[0x800074d8]:sub a1, a1, s1
[0x800074dc]:lui s1, 2
[0x800074e0]:add a1, a1, s1
[0x800074e4]:ld t4, 168(a1)
[0x800074e8]:sub a1, a1, s1
[0x800074ec]:addi s1, zero, 0
[0x800074f0]:csrrw zero, fcsr, s1
[0x800074f4]:fmax.s t6, t5, t4

[0x800074f4]:fmax.s t6, t5, t4
[0x800074f8]:csrrs a2, fcsr, zero
[0x800074fc]:sd t6, 112(fp)
[0x80007500]:sd a2, 120(fp)
[0x80007504]:lui s1, 2
[0x80007508]:add a1, a1, s1
[0x8000750c]:ld t5, 176(a1)
[0x80007510]:sub a1, a1, s1
[0x80007514]:lui s1, 2
[0x80007518]:add a1, a1, s1
[0x8000751c]:ld t4, 184(a1)
[0x80007520]:sub a1, a1, s1
[0x80007524]:addi s1, zero, 0
[0x80007528]:csrrw zero, fcsr, s1
[0x8000752c]:fmax.s t6, t5, t4

[0x8000752c]:fmax.s t6, t5, t4
[0x80007530]:csrrs a2, fcsr, zero
[0x80007534]:sd t6, 128(fp)
[0x80007538]:sd a2, 136(fp)
[0x8000753c]:lui s1, 2
[0x80007540]:add a1, a1, s1
[0x80007544]:ld t5, 192(a1)
[0x80007548]:sub a1, a1, s1
[0x8000754c]:lui s1, 2
[0x80007550]:add a1, a1, s1
[0x80007554]:ld t4, 200(a1)
[0x80007558]:sub a1, a1, s1
[0x8000755c]:addi s1, zero, 0
[0x80007560]:csrrw zero, fcsr, s1
[0x80007564]:fmax.s t6, t5, t4

[0x80007564]:fmax.s t6, t5, t4
[0x80007568]:csrrs a2, fcsr, zero
[0x8000756c]:sd t6, 144(fp)
[0x80007570]:sd a2, 152(fp)
[0x80007574]:lui s1, 2
[0x80007578]:add a1, a1, s1
[0x8000757c]:ld t5, 208(a1)
[0x80007580]:sub a1, a1, s1
[0x80007584]:lui s1, 2
[0x80007588]:add a1, a1, s1
[0x8000758c]:ld t4, 216(a1)
[0x80007590]:sub a1, a1, s1
[0x80007594]:addi s1, zero, 0
[0x80007598]:csrrw zero, fcsr, s1
[0x8000759c]:fmax.s t6, t5, t4

[0x8000759c]:fmax.s t6, t5, t4
[0x800075a0]:csrrs a2, fcsr, zero
[0x800075a4]:sd t6, 160(fp)
[0x800075a8]:sd a2, 168(fp)
[0x800075ac]:lui s1, 2
[0x800075b0]:add a1, a1, s1
[0x800075b4]:ld t5, 224(a1)
[0x800075b8]:sub a1, a1, s1
[0x800075bc]:lui s1, 2
[0x800075c0]:add a1, a1, s1
[0x800075c4]:ld t4, 232(a1)
[0x800075c8]:sub a1, a1, s1
[0x800075cc]:addi s1, zero, 0
[0x800075d0]:csrrw zero, fcsr, s1
[0x800075d4]:fmax.s t6, t5, t4

[0x800075d4]:fmax.s t6, t5, t4
[0x800075d8]:csrrs a2, fcsr, zero
[0x800075dc]:sd t6, 176(fp)
[0x800075e0]:sd a2, 184(fp)
[0x800075e4]:lui s1, 2
[0x800075e8]:add a1, a1, s1
[0x800075ec]:ld t5, 240(a1)
[0x800075f0]:sub a1, a1, s1
[0x800075f4]:lui s1, 2
[0x800075f8]:add a1, a1, s1
[0x800075fc]:ld t4, 248(a1)
[0x80007600]:sub a1, a1, s1
[0x80007604]:addi s1, zero, 0
[0x80007608]:csrrw zero, fcsr, s1
[0x8000760c]:fmax.s t6, t5, t4

[0x8000760c]:fmax.s t6, t5, t4
[0x80007610]:csrrs a2, fcsr, zero
[0x80007614]:sd t6, 192(fp)
[0x80007618]:sd a2, 200(fp)
[0x8000761c]:lui s1, 2
[0x80007620]:add a1, a1, s1
[0x80007624]:ld t5, 256(a1)
[0x80007628]:sub a1, a1, s1
[0x8000762c]:lui s1, 2
[0x80007630]:add a1, a1, s1
[0x80007634]:ld t4, 264(a1)
[0x80007638]:sub a1, a1, s1
[0x8000763c]:addi s1, zero, 0
[0x80007640]:csrrw zero, fcsr, s1
[0x80007644]:fmax.s t6, t5, t4

[0x80007644]:fmax.s t6, t5, t4
[0x80007648]:csrrs a2, fcsr, zero
[0x8000764c]:sd t6, 208(fp)
[0x80007650]:sd a2, 216(fp)
[0x80007654]:lui s1, 2
[0x80007658]:add a1, a1, s1
[0x8000765c]:ld t5, 272(a1)
[0x80007660]:sub a1, a1, s1
[0x80007664]:lui s1, 2
[0x80007668]:add a1, a1, s1
[0x8000766c]:ld t4, 280(a1)
[0x80007670]:sub a1, a1, s1
[0x80007674]:addi s1, zero, 0
[0x80007678]:csrrw zero, fcsr, s1
[0x8000767c]:fmax.s t6, t5, t4

[0x8000767c]:fmax.s t6, t5, t4
[0x80007680]:csrrs a2, fcsr, zero
[0x80007684]:sd t6, 224(fp)
[0x80007688]:sd a2, 232(fp)
[0x8000768c]:lui s1, 2
[0x80007690]:add a1, a1, s1
[0x80007694]:ld t5, 288(a1)
[0x80007698]:sub a1, a1, s1
[0x8000769c]:lui s1, 2
[0x800076a0]:add a1, a1, s1
[0x800076a4]:ld t4, 296(a1)
[0x800076a8]:sub a1, a1, s1
[0x800076ac]:addi s1, zero, 0
[0x800076b0]:csrrw zero, fcsr, s1
[0x800076b4]:fmax.s t6, t5, t4

[0x800076b4]:fmax.s t6, t5, t4
[0x800076b8]:csrrs a2, fcsr, zero
[0x800076bc]:sd t6, 240(fp)
[0x800076c0]:sd a2, 248(fp)
[0x800076c4]:lui s1, 2
[0x800076c8]:add a1, a1, s1
[0x800076cc]:ld t5, 304(a1)
[0x800076d0]:sub a1, a1, s1
[0x800076d4]:lui s1, 2
[0x800076d8]:add a1, a1, s1
[0x800076dc]:ld t4, 312(a1)
[0x800076e0]:sub a1, a1, s1
[0x800076e4]:addi s1, zero, 0
[0x800076e8]:csrrw zero, fcsr, s1
[0x800076ec]:fmax.s t6, t5, t4

[0x800076ec]:fmax.s t6, t5, t4
[0x800076f0]:csrrs a2, fcsr, zero
[0x800076f4]:sd t6, 256(fp)
[0x800076f8]:sd a2, 264(fp)
[0x800076fc]:lui s1, 2
[0x80007700]:add a1, a1, s1
[0x80007704]:ld t5, 320(a1)
[0x80007708]:sub a1, a1, s1
[0x8000770c]:lui s1, 2
[0x80007710]:add a1, a1, s1
[0x80007714]:ld t4, 328(a1)
[0x80007718]:sub a1, a1, s1
[0x8000771c]:addi s1, zero, 0
[0x80007720]:csrrw zero, fcsr, s1
[0x80007724]:fmax.s t6, t5, t4

[0x80007724]:fmax.s t6, t5, t4
[0x80007728]:csrrs a2, fcsr, zero
[0x8000772c]:sd t6, 272(fp)
[0x80007730]:sd a2, 280(fp)
[0x80007734]:lui s1, 2
[0x80007738]:add a1, a1, s1
[0x8000773c]:ld t5, 336(a1)
[0x80007740]:sub a1, a1, s1
[0x80007744]:lui s1, 2
[0x80007748]:add a1, a1, s1
[0x8000774c]:ld t4, 344(a1)
[0x80007750]:sub a1, a1, s1
[0x80007754]:addi s1, zero, 0
[0x80007758]:csrrw zero, fcsr, s1
[0x8000775c]:fmax.s t6, t5, t4

[0x8000775c]:fmax.s t6, t5, t4
[0x80007760]:csrrs a2, fcsr, zero
[0x80007764]:sd t6, 288(fp)
[0x80007768]:sd a2, 296(fp)
[0x8000776c]:lui s1, 2
[0x80007770]:add a1, a1, s1
[0x80007774]:ld t5, 352(a1)
[0x80007778]:sub a1, a1, s1
[0x8000777c]:lui s1, 2
[0x80007780]:add a1, a1, s1
[0x80007784]:ld t4, 360(a1)
[0x80007788]:sub a1, a1, s1
[0x8000778c]:addi s1, zero, 0
[0x80007790]:csrrw zero, fcsr, s1
[0x80007794]:fmax.s t6, t5, t4

[0x80007794]:fmax.s t6, t5, t4
[0x80007798]:csrrs a2, fcsr, zero
[0x8000779c]:sd t6, 304(fp)
[0x800077a0]:sd a2, 312(fp)
[0x800077a4]:lui s1, 2
[0x800077a8]:add a1, a1, s1
[0x800077ac]:ld t5, 368(a1)
[0x800077b0]:sub a1, a1, s1
[0x800077b4]:lui s1, 2
[0x800077b8]:add a1, a1, s1
[0x800077bc]:ld t4, 376(a1)
[0x800077c0]:sub a1, a1, s1
[0x800077c4]:addi s1, zero, 0
[0x800077c8]:csrrw zero, fcsr, s1
[0x800077cc]:fmax.s t6, t5, t4

[0x800077cc]:fmax.s t6, t5, t4
[0x800077d0]:csrrs a2, fcsr, zero
[0x800077d4]:sd t6, 320(fp)
[0x800077d8]:sd a2, 328(fp)
[0x800077dc]:lui s1, 2
[0x800077e0]:add a1, a1, s1
[0x800077e4]:ld t5, 384(a1)
[0x800077e8]:sub a1, a1, s1
[0x800077ec]:lui s1, 2
[0x800077f0]:add a1, a1, s1
[0x800077f4]:ld t4, 392(a1)
[0x800077f8]:sub a1, a1, s1
[0x800077fc]:addi s1, zero, 0
[0x80007800]:csrrw zero, fcsr, s1
[0x80007804]:fmax.s t6, t5, t4

[0x80007804]:fmax.s t6, t5, t4
[0x80007808]:csrrs a2, fcsr, zero
[0x8000780c]:sd t6, 336(fp)
[0x80007810]:sd a2, 344(fp)
[0x80007814]:lui s1, 2
[0x80007818]:add a1, a1, s1
[0x8000781c]:ld t5, 400(a1)
[0x80007820]:sub a1, a1, s1
[0x80007824]:lui s1, 2
[0x80007828]:add a1, a1, s1
[0x8000782c]:ld t4, 408(a1)
[0x80007830]:sub a1, a1, s1
[0x80007834]:addi s1, zero, 0
[0x80007838]:csrrw zero, fcsr, s1
[0x8000783c]:fmax.s t6, t5, t4

[0x8000783c]:fmax.s t6, t5, t4
[0x80007840]:csrrs a2, fcsr, zero
[0x80007844]:sd t6, 352(fp)
[0x80007848]:sd a2, 360(fp)
[0x8000784c]:lui s1, 2
[0x80007850]:add a1, a1, s1
[0x80007854]:ld t5, 416(a1)
[0x80007858]:sub a1, a1, s1
[0x8000785c]:lui s1, 2
[0x80007860]:add a1, a1, s1
[0x80007864]:ld t4, 424(a1)
[0x80007868]:sub a1, a1, s1
[0x8000786c]:addi s1, zero, 0
[0x80007870]:csrrw zero, fcsr, s1
[0x80007874]:fmax.s t6, t5, t4

[0x80007874]:fmax.s t6, t5, t4
[0x80007878]:csrrs a2, fcsr, zero
[0x8000787c]:sd t6, 368(fp)
[0x80007880]:sd a2, 376(fp)
[0x80007884]:lui s1, 2
[0x80007888]:add a1, a1, s1
[0x8000788c]:ld t5, 432(a1)
[0x80007890]:sub a1, a1, s1
[0x80007894]:lui s1, 2
[0x80007898]:add a1, a1, s1
[0x8000789c]:ld t4, 440(a1)
[0x800078a0]:sub a1, a1, s1
[0x800078a4]:addi s1, zero, 0
[0x800078a8]:csrrw zero, fcsr, s1
[0x800078ac]:fmax.s t6, t5, t4

[0x800078ac]:fmax.s t6, t5, t4
[0x800078b0]:csrrs a2, fcsr, zero
[0x800078b4]:sd t6, 384(fp)
[0x800078b8]:sd a2, 392(fp)
[0x800078bc]:lui s1, 2
[0x800078c0]:add a1, a1, s1
[0x800078c4]:ld t5, 448(a1)
[0x800078c8]:sub a1, a1, s1
[0x800078cc]:lui s1, 2
[0x800078d0]:add a1, a1, s1
[0x800078d4]:ld t4, 456(a1)
[0x800078d8]:sub a1, a1, s1
[0x800078dc]:addi s1, zero, 0
[0x800078e0]:csrrw zero, fcsr, s1
[0x800078e4]:fmax.s t6, t5, t4

[0x800078e4]:fmax.s t6, t5, t4
[0x800078e8]:csrrs a2, fcsr, zero
[0x800078ec]:sd t6, 400(fp)
[0x800078f0]:sd a2, 408(fp)
[0x800078f4]:lui s1, 2
[0x800078f8]:add a1, a1, s1
[0x800078fc]:ld t5, 464(a1)
[0x80007900]:sub a1, a1, s1
[0x80007904]:lui s1, 2
[0x80007908]:add a1, a1, s1
[0x8000790c]:ld t4, 472(a1)
[0x80007910]:sub a1, a1, s1
[0x80007914]:addi s1, zero, 0
[0x80007918]:csrrw zero, fcsr, s1
[0x8000791c]:fmax.s t6, t5, t4

[0x8000791c]:fmax.s t6, t5, t4
[0x80007920]:csrrs a2, fcsr, zero
[0x80007924]:sd t6, 416(fp)
[0x80007928]:sd a2, 424(fp)
[0x8000792c]:lui s1, 2
[0x80007930]:add a1, a1, s1
[0x80007934]:ld t5, 480(a1)
[0x80007938]:sub a1, a1, s1
[0x8000793c]:lui s1, 2
[0x80007940]:add a1, a1, s1
[0x80007944]:ld t4, 488(a1)
[0x80007948]:sub a1, a1, s1
[0x8000794c]:addi s1, zero, 0
[0x80007950]:csrrw zero, fcsr, s1
[0x80007954]:fmax.s t6, t5, t4

[0x80007954]:fmax.s t6, t5, t4
[0x80007958]:csrrs a2, fcsr, zero
[0x8000795c]:sd t6, 432(fp)
[0x80007960]:sd a2, 440(fp)
[0x80007964]:lui s1, 2
[0x80007968]:add a1, a1, s1
[0x8000796c]:ld t5, 496(a1)
[0x80007970]:sub a1, a1, s1
[0x80007974]:lui s1, 2
[0x80007978]:add a1, a1, s1
[0x8000797c]:ld t4, 504(a1)
[0x80007980]:sub a1, a1, s1
[0x80007984]:addi s1, zero, 0
[0x80007988]:csrrw zero, fcsr, s1
[0x8000798c]:fmax.s t6, t5, t4

[0x8000798c]:fmax.s t6, t5, t4
[0x80007990]:csrrs a2, fcsr, zero
[0x80007994]:sd t6, 448(fp)
[0x80007998]:sd a2, 456(fp)
[0x8000799c]:lui s1, 2
[0x800079a0]:add a1, a1, s1
[0x800079a4]:ld t5, 512(a1)
[0x800079a8]:sub a1, a1, s1
[0x800079ac]:lui s1, 2
[0x800079b0]:add a1, a1, s1
[0x800079b4]:ld t4, 520(a1)
[0x800079b8]:sub a1, a1, s1
[0x800079bc]:addi s1, zero, 0
[0x800079c0]:csrrw zero, fcsr, s1
[0x800079c4]:fmax.s t6, t5, t4

[0x800079c4]:fmax.s t6, t5, t4
[0x800079c8]:csrrs a2, fcsr, zero
[0x800079cc]:sd t6, 464(fp)
[0x800079d0]:sd a2, 472(fp)
[0x800079d4]:lui s1, 2
[0x800079d8]:add a1, a1, s1
[0x800079dc]:ld t5, 528(a1)
[0x800079e0]:sub a1, a1, s1
[0x800079e4]:lui s1, 2
[0x800079e8]:add a1, a1, s1
[0x800079ec]:ld t4, 536(a1)
[0x800079f0]:sub a1, a1, s1
[0x800079f4]:addi s1, zero, 0
[0x800079f8]:csrrw zero, fcsr, s1
[0x800079fc]:fmax.s t6, t5, t4

[0x800079fc]:fmax.s t6, t5, t4
[0x80007a00]:csrrs a2, fcsr, zero
[0x80007a04]:sd t6, 480(fp)
[0x80007a08]:sd a2, 488(fp)
[0x80007a0c]:lui s1, 2
[0x80007a10]:add a1, a1, s1
[0x80007a14]:ld t5, 544(a1)
[0x80007a18]:sub a1, a1, s1
[0x80007a1c]:lui s1, 2
[0x80007a20]:add a1, a1, s1
[0x80007a24]:ld t4, 552(a1)
[0x80007a28]:sub a1, a1, s1
[0x80007a2c]:addi s1, zero, 0
[0x80007a30]:csrrw zero, fcsr, s1
[0x80007a34]:fmax.s t6, t5, t4

[0x80007a34]:fmax.s t6, t5, t4
[0x80007a38]:csrrs a2, fcsr, zero
[0x80007a3c]:sd t6, 496(fp)
[0x80007a40]:sd a2, 504(fp)
[0x80007a44]:lui s1, 2
[0x80007a48]:add a1, a1, s1
[0x80007a4c]:ld t5, 560(a1)
[0x80007a50]:sub a1, a1, s1
[0x80007a54]:lui s1, 2
[0x80007a58]:add a1, a1, s1
[0x80007a5c]:ld t4, 568(a1)
[0x80007a60]:sub a1, a1, s1
[0x80007a64]:addi s1, zero, 0
[0x80007a68]:csrrw zero, fcsr, s1
[0x80007a6c]:fmax.s t6, t5, t4

[0x80007a6c]:fmax.s t6, t5, t4
[0x80007a70]:csrrs a2, fcsr, zero
[0x80007a74]:sd t6, 512(fp)
[0x80007a78]:sd a2, 520(fp)
[0x80007a7c]:lui s1, 2
[0x80007a80]:add a1, a1, s1
[0x80007a84]:ld t5, 576(a1)
[0x80007a88]:sub a1, a1, s1
[0x80007a8c]:lui s1, 2
[0x80007a90]:add a1, a1, s1
[0x80007a94]:ld t4, 584(a1)
[0x80007a98]:sub a1, a1, s1
[0x80007a9c]:addi s1, zero, 0
[0x80007aa0]:csrrw zero, fcsr, s1
[0x80007aa4]:fmax.s t6, t5, t4

[0x80007aa4]:fmax.s t6, t5, t4
[0x80007aa8]:csrrs a2, fcsr, zero
[0x80007aac]:sd t6, 528(fp)
[0x80007ab0]:sd a2, 536(fp)
[0x80007ab4]:lui s1, 2
[0x80007ab8]:add a1, a1, s1
[0x80007abc]:ld t5, 592(a1)
[0x80007ac0]:sub a1, a1, s1
[0x80007ac4]:lui s1, 2
[0x80007ac8]:add a1, a1, s1
[0x80007acc]:ld t4, 600(a1)
[0x80007ad0]:sub a1, a1, s1
[0x80007ad4]:addi s1, zero, 0
[0x80007ad8]:csrrw zero, fcsr, s1
[0x80007adc]:fmax.s t6, t5, t4

[0x80007adc]:fmax.s t6, t5, t4
[0x80007ae0]:csrrs a2, fcsr, zero
[0x80007ae4]:sd t6, 544(fp)
[0x80007ae8]:sd a2, 552(fp)
[0x80007aec]:lui s1, 2
[0x80007af0]:add a1, a1, s1
[0x80007af4]:ld t5, 608(a1)
[0x80007af8]:sub a1, a1, s1
[0x80007afc]:lui s1, 2
[0x80007b00]:add a1, a1, s1
[0x80007b04]:ld t4, 616(a1)
[0x80007b08]:sub a1, a1, s1
[0x80007b0c]:addi s1, zero, 0
[0x80007b10]:csrrw zero, fcsr, s1
[0x80007b14]:fmax.s t6, t5, t4

[0x80007b14]:fmax.s t6, t5, t4
[0x80007b18]:csrrs a2, fcsr, zero
[0x80007b1c]:sd t6, 560(fp)
[0x80007b20]:sd a2, 568(fp)
[0x80007b24]:lui s1, 2
[0x80007b28]:add a1, a1, s1
[0x80007b2c]:ld t5, 624(a1)
[0x80007b30]:sub a1, a1, s1
[0x80007b34]:lui s1, 2
[0x80007b38]:add a1, a1, s1
[0x80007b3c]:ld t4, 632(a1)
[0x80007b40]:sub a1, a1, s1
[0x80007b44]:addi s1, zero, 0
[0x80007b48]:csrrw zero, fcsr, s1
[0x80007b4c]:fmax.s t6, t5, t4

[0x80007b4c]:fmax.s t6, t5, t4
[0x80007b50]:csrrs a2, fcsr, zero
[0x80007b54]:sd t6, 576(fp)
[0x80007b58]:sd a2, 584(fp)
[0x80007b5c]:lui s1, 2
[0x80007b60]:add a1, a1, s1
[0x80007b64]:ld t5, 640(a1)
[0x80007b68]:sub a1, a1, s1
[0x80007b6c]:lui s1, 2
[0x80007b70]:add a1, a1, s1
[0x80007b74]:ld t4, 648(a1)
[0x80007b78]:sub a1, a1, s1
[0x80007b7c]:addi s1, zero, 0
[0x80007b80]:csrrw zero, fcsr, s1
[0x80007b84]:fmax.s t6, t5, t4

[0x80007b84]:fmax.s t6, t5, t4
[0x80007b88]:csrrs a2, fcsr, zero
[0x80007b8c]:sd t6, 592(fp)
[0x80007b90]:sd a2, 600(fp)
[0x80007b94]:lui s1, 2
[0x80007b98]:add a1, a1, s1
[0x80007b9c]:ld t5, 656(a1)
[0x80007ba0]:sub a1, a1, s1
[0x80007ba4]:lui s1, 2
[0x80007ba8]:add a1, a1, s1
[0x80007bac]:ld t4, 664(a1)
[0x80007bb0]:sub a1, a1, s1
[0x80007bb4]:addi s1, zero, 0
[0x80007bb8]:csrrw zero, fcsr, s1
[0x80007bbc]:fmax.s t6, t5, t4

[0x80007bbc]:fmax.s t6, t5, t4
[0x80007bc0]:csrrs a2, fcsr, zero
[0x80007bc4]:sd t6, 608(fp)
[0x80007bc8]:sd a2, 616(fp)
[0x80007bcc]:lui s1, 2
[0x80007bd0]:add a1, a1, s1
[0x80007bd4]:ld t5, 672(a1)
[0x80007bd8]:sub a1, a1, s1
[0x80007bdc]:lui s1, 2
[0x80007be0]:add a1, a1, s1
[0x80007be4]:ld t4, 680(a1)
[0x80007be8]:sub a1, a1, s1
[0x80007bec]:addi s1, zero, 0
[0x80007bf0]:csrrw zero, fcsr, s1
[0x80007bf4]:fmax.s t6, t5, t4

[0x80007bf4]:fmax.s t6, t5, t4
[0x80007bf8]:csrrs a2, fcsr, zero
[0x80007bfc]:sd t6, 624(fp)
[0x80007c00]:sd a2, 632(fp)
[0x80007c04]:lui s1, 2
[0x80007c08]:add a1, a1, s1
[0x80007c0c]:ld t5, 688(a1)
[0x80007c10]:sub a1, a1, s1
[0x80007c14]:lui s1, 2
[0x80007c18]:add a1, a1, s1
[0x80007c1c]:ld t4, 696(a1)
[0x80007c20]:sub a1, a1, s1
[0x80007c24]:addi s1, zero, 0
[0x80007c28]:csrrw zero, fcsr, s1
[0x80007c2c]:fmax.s t6, t5, t4

[0x80007c2c]:fmax.s t6, t5, t4
[0x80007c30]:csrrs a2, fcsr, zero
[0x80007c34]:sd t6, 640(fp)
[0x80007c38]:sd a2, 648(fp)
[0x80007c3c]:lui s1, 2
[0x80007c40]:add a1, a1, s1
[0x80007c44]:ld t5, 704(a1)
[0x80007c48]:sub a1, a1, s1
[0x80007c4c]:lui s1, 2
[0x80007c50]:add a1, a1, s1
[0x80007c54]:ld t4, 712(a1)
[0x80007c58]:sub a1, a1, s1
[0x80007c5c]:addi s1, zero, 0
[0x80007c60]:csrrw zero, fcsr, s1
[0x80007c64]:fmax.s t6, t5, t4

[0x80007c64]:fmax.s t6, t5, t4
[0x80007c68]:csrrs a2, fcsr, zero
[0x80007c6c]:sd t6, 656(fp)
[0x80007c70]:sd a2, 664(fp)
[0x80007c74]:lui s1, 2
[0x80007c78]:add a1, a1, s1
[0x80007c7c]:ld t5, 720(a1)
[0x80007c80]:sub a1, a1, s1
[0x80007c84]:lui s1, 2
[0x80007c88]:add a1, a1, s1
[0x80007c8c]:ld t4, 728(a1)
[0x80007c90]:sub a1, a1, s1
[0x80007c94]:addi s1, zero, 0
[0x80007c98]:csrrw zero, fcsr, s1
[0x80007c9c]:fmax.s t6, t5, t4

[0x80007c9c]:fmax.s t6, t5, t4
[0x80007ca0]:csrrs a2, fcsr, zero
[0x80007ca4]:sd t6, 672(fp)
[0x80007ca8]:sd a2, 680(fp)
[0x80007cac]:lui s1, 2
[0x80007cb0]:add a1, a1, s1
[0x80007cb4]:ld t5, 736(a1)
[0x80007cb8]:sub a1, a1, s1
[0x80007cbc]:lui s1, 2
[0x80007cc0]:add a1, a1, s1
[0x80007cc4]:ld t4, 744(a1)
[0x80007cc8]:sub a1, a1, s1
[0x80007ccc]:addi s1, zero, 0
[0x80007cd0]:csrrw zero, fcsr, s1
[0x80007cd4]:fmax.s t6, t5, t4

[0x80007cd4]:fmax.s t6, t5, t4
[0x80007cd8]:csrrs a2, fcsr, zero
[0x80007cdc]:sd t6, 688(fp)
[0x80007ce0]:sd a2, 696(fp)
[0x80007ce4]:lui s1, 2
[0x80007ce8]:add a1, a1, s1
[0x80007cec]:ld t5, 752(a1)
[0x80007cf0]:sub a1, a1, s1
[0x80007cf4]:lui s1, 2
[0x80007cf8]:add a1, a1, s1
[0x80007cfc]:ld t4, 760(a1)
[0x80007d00]:sub a1, a1, s1
[0x80007d04]:addi s1, zero, 0
[0x80007d08]:csrrw zero, fcsr, s1
[0x80007d0c]:fmax.s t6, t5, t4

[0x80007d0c]:fmax.s t6, t5, t4
[0x80007d10]:csrrs a2, fcsr, zero
[0x80007d14]:sd t6, 704(fp)
[0x80007d18]:sd a2, 712(fp)
[0x80007d1c]:lui s1, 2
[0x80007d20]:add a1, a1, s1
[0x80007d24]:ld t5, 768(a1)
[0x80007d28]:sub a1, a1, s1
[0x80007d2c]:lui s1, 2
[0x80007d30]:add a1, a1, s1
[0x80007d34]:ld t4, 776(a1)
[0x80007d38]:sub a1, a1, s1
[0x80007d3c]:addi s1, zero, 0
[0x80007d40]:csrrw zero, fcsr, s1
[0x80007d44]:fmax.s t6, t5, t4

[0x80007d44]:fmax.s t6, t5, t4
[0x80007d48]:csrrs a2, fcsr, zero
[0x80007d4c]:sd t6, 720(fp)
[0x80007d50]:sd a2, 728(fp)
[0x80007d54]:lui s1, 2
[0x80007d58]:add a1, a1, s1
[0x80007d5c]:ld t5, 784(a1)
[0x80007d60]:sub a1, a1, s1
[0x80007d64]:lui s1, 2
[0x80007d68]:add a1, a1, s1
[0x80007d6c]:ld t4, 792(a1)
[0x80007d70]:sub a1, a1, s1
[0x80007d74]:addi s1, zero, 0
[0x80007d78]:csrrw zero, fcsr, s1
[0x80007d7c]:fmax.s t6, t5, t4

[0x80007d7c]:fmax.s t6, t5, t4
[0x80007d80]:csrrs a2, fcsr, zero
[0x80007d84]:sd t6, 736(fp)
[0x80007d88]:sd a2, 744(fp)
[0x80007d8c]:lui s1, 2
[0x80007d90]:add a1, a1, s1
[0x80007d94]:ld t5, 800(a1)
[0x80007d98]:sub a1, a1, s1
[0x80007d9c]:lui s1, 2
[0x80007da0]:add a1, a1, s1
[0x80007da4]:ld t4, 808(a1)
[0x80007da8]:sub a1, a1, s1
[0x80007dac]:addi s1, zero, 0
[0x80007db0]:csrrw zero, fcsr, s1
[0x80007db4]:fmax.s t6, t5, t4

[0x80007db4]:fmax.s t6, t5, t4
[0x80007db8]:csrrs a2, fcsr, zero
[0x80007dbc]:sd t6, 752(fp)
[0x80007dc0]:sd a2, 760(fp)
[0x80007dc4]:lui s1, 2
[0x80007dc8]:add a1, a1, s1
[0x80007dcc]:ld t5, 816(a1)
[0x80007dd0]:sub a1, a1, s1
[0x80007dd4]:lui s1, 2
[0x80007dd8]:add a1, a1, s1
[0x80007ddc]:ld t4, 824(a1)
[0x80007de0]:sub a1, a1, s1
[0x80007de4]:addi s1, zero, 0
[0x80007de8]:csrrw zero, fcsr, s1
[0x80007dec]:fmax.s t6, t5, t4

[0x80007dec]:fmax.s t6, t5, t4
[0x80007df0]:csrrs a2, fcsr, zero
[0x80007df4]:sd t6, 768(fp)
[0x80007df8]:sd a2, 776(fp)
[0x80007dfc]:lui s1, 2
[0x80007e00]:add a1, a1, s1
[0x80007e04]:ld t5, 832(a1)
[0x80007e08]:sub a1, a1, s1
[0x80007e0c]:lui s1, 2
[0x80007e10]:add a1, a1, s1
[0x80007e14]:ld t4, 840(a1)
[0x80007e18]:sub a1, a1, s1
[0x80007e1c]:addi s1, zero, 0
[0x80007e20]:csrrw zero, fcsr, s1
[0x80007e24]:fmax.s t6, t5, t4

[0x80007e24]:fmax.s t6, t5, t4
[0x80007e28]:csrrs a2, fcsr, zero
[0x80007e2c]:sd t6, 784(fp)
[0x80007e30]:sd a2, 792(fp)
[0x80007e34]:lui s1, 2
[0x80007e38]:add a1, a1, s1
[0x80007e3c]:ld t5, 848(a1)
[0x80007e40]:sub a1, a1, s1
[0x80007e44]:lui s1, 2
[0x80007e48]:add a1, a1, s1
[0x80007e4c]:ld t4, 856(a1)
[0x80007e50]:sub a1, a1, s1
[0x80007e54]:addi s1, zero, 0
[0x80007e58]:csrrw zero, fcsr, s1
[0x80007e5c]:fmax.s t6, t5, t4

[0x80007e5c]:fmax.s t6, t5, t4
[0x80007e60]:csrrs a2, fcsr, zero
[0x80007e64]:sd t6, 800(fp)
[0x80007e68]:sd a2, 808(fp)
[0x80007e6c]:lui s1, 2
[0x80007e70]:add a1, a1, s1
[0x80007e74]:ld t5, 864(a1)
[0x80007e78]:sub a1, a1, s1
[0x80007e7c]:lui s1, 2
[0x80007e80]:add a1, a1, s1
[0x80007e84]:ld t4, 872(a1)
[0x80007e88]:sub a1, a1, s1
[0x80007e8c]:addi s1, zero, 0
[0x80007e90]:csrrw zero, fcsr, s1
[0x80007e94]:fmax.s t6, t5, t4

[0x80007e94]:fmax.s t6, t5, t4
[0x80007e98]:csrrs a2, fcsr, zero
[0x80007e9c]:sd t6, 816(fp)
[0x80007ea0]:sd a2, 824(fp)
[0x80007ea4]:lui s1, 2
[0x80007ea8]:add a1, a1, s1
[0x80007eac]:ld t5, 880(a1)
[0x80007eb0]:sub a1, a1, s1
[0x80007eb4]:lui s1, 2
[0x80007eb8]:add a1, a1, s1
[0x80007ebc]:ld t4, 888(a1)
[0x80007ec0]:sub a1, a1, s1
[0x80007ec4]:addi s1, zero, 0
[0x80007ec8]:csrrw zero, fcsr, s1
[0x80007ecc]:fmax.s t6, t5, t4

[0x80007ecc]:fmax.s t6, t5, t4
[0x80007ed0]:csrrs a2, fcsr, zero
[0x80007ed4]:sd t6, 832(fp)
[0x80007ed8]:sd a2, 840(fp)
[0x80007edc]:lui s1, 2
[0x80007ee0]:add a1, a1, s1
[0x80007ee4]:ld t5, 896(a1)
[0x80007ee8]:sub a1, a1, s1
[0x80007eec]:lui s1, 2
[0x80007ef0]:add a1, a1, s1
[0x80007ef4]:ld t4, 904(a1)
[0x80007ef8]:sub a1, a1, s1
[0x80007efc]:addi s1, zero, 0
[0x80007f00]:csrrw zero, fcsr, s1
[0x80007f04]:fmax.s t6, t5, t4

[0x80007f04]:fmax.s t6, t5, t4
[0x80007f08]:csrrs a2, fcsr, zero
[0x80007f0c]:sd t6, 848(fp)
[0x80007f10]:sd a2, 856(fp)
[0x80007f14]:lui s1, 2
[0x80007f18]:add a1, a1, s1
[0x80007f1c]:ld t5, 912(a1)
[0x80007f20]:sub a1, a1, s1
[0x80007f24]:lui s1, 2
[0x80007f28]:add a1, a1, s1
[0x80007f2c]:ld t4, 920(a1)
[0x80007f30]:sub a1, a1, s1
[0x80007f34]:addi s1, zero, 0
[0x80007f38]:csrrw zero, fcsr, s1
[0x80007f3c]:fmax.s t6, t5, t4

[0x80007f3c]:fmax.s t6, t5, t4
[0x80007f40]:csrrs a2, fcsr, zero
[0x80007f44]:sd t6, 864(fp)
[0x80007f48]:sd a2, 872(fp)
[0x80007f4c]:lui s1, 2
[0x80007f50]:add a1, a1, s1
[0x80007f54]:ld t5, 928(a1)
[0x80007f58]:sub a1, a1, s1
[0x80007f5c]:lui s1, 2
[0x80007f60]:add a1, a1, s1
[0x80007f64]:ld t4, 936(a1)
[0x80007f68]:sub a1, a1, s1
[0x80007f6c]:addi s1, zero, 0
[0x80007f70]:csrrw zero, fcsr, s1
[0x80007f74]:fmax.s t6, t5, t4

[0x80007f74]:fmax.s t6, t5, t4
[0x80007f78]:csrrs a2, fcsr, zero
[0x80007f7c]:sd t6, 880(fp)
[0x80007f80]:sd a2, 888(fp)
[0x80007f84]:lui s1, 2
[0x80007f88]:add a1, a1, s1
[0x80007f8c]:ld t5, 944(a1)
[0x80007f90]:sub a1, a1, s1
[0x80007f94]:lui s1, 2
[0x80007f98]:add a1, a1, s1
[0x80007f9c]:ld t4, 952(a1)
[0x80007fa0]:sub a1, a1, s1
[0x80007fa4]:addi s1, zero, 0
[0x80007fa8]:csrrw zero, fcsr, s1
[0x80007fac]:fmax.s t6, t5, t4

[0x80007fac]:fmax.s t6, t5, t4
[0x80007fb0]:csrrs a2, fcsr, zero
[0x80007fb4]:sd t6, 896(fp)
[0x80007fb8]:sd a2, 904(fp)
[0x80007fbc]:lui s1, 2
[0x80007fc0]:add a1, a1, s1
[0x80007fc4]:ld t5, 960(a1)
[0x80007fc8]:sub a1, a1, s1
[0x80007fcc]:lui s1, 2
[0x80007fd0]:add a1, a1, s1
[0x80007fd4]:ld t4, 968(a1)
[0x80007fd8]:sub a1, a1, s1
[0x80007fdc]:addi s1, zero, 0
[0x80007fe0]:csrrw zero, fcsr, s1
[0x80007fe4]:fmax.s t6, t5, t4

[0x80007fe4]:fmax.s t6, t5, t4
[0x80007fe8]:csrrs a2, fcsr, zero
[0x80007fec]:sd t6, 912(fp)
[0x80007ff0]:sd a2, 920(fp)
[0x80007ff4]:lui s1, 2
[0x80007ff8]:add a1, a1, s1
[0x80007ffc]:ld t5, 976(a1)
[0x80008000]:sub a1, a1, s1
[0x80008004]:lui s1, 2
[0x80008008]:add a1, a1, s1
[0x8000800c]:ld t4, 984(a1)
[0x80008010]:sub a1, a1, s1
[0x80008014]:addi s1, zero, 0
[0x80008018]:csrrw zero, fcsr, s1
[0x8000801c]:fmax.s t6, t5, t4

[0x8000801c]:fmax.s t6, t5, t4
[0x80008020]:csrrs a2, fcsr, zero
[0x80008024]:sd t6, 928(fp)
[0x80008028]:sd a2, 936(fp)
[0x8000802c]:lui s1, 2
[0x80008030]:add a1, a1, s1
[0x80008034]:ld t5, 992(a1)
[0x80008038]:sub a1, a1, s1
[0x8000803c]:lui s1, 2
[0x80008040]:add a1, a1, s1
[0x80008044]:ld t4, 1000(a1)
[0x80008048]:sub a1, a1, s1
[0x8000804c]:addi s1, zero, 0
[0x80008050]:csrrw zero, fcsr, s1
[0x80008054]:fmax.s t6, t5, t4

[0x80008054]:fmax.s t6, t5, t4
[0x80008058]:csrrs a2, fcsr, zero
[0x8000805c]:sd t6, 944(fp)
[0x80008060]:sd a2, 952(fp)
[0x80008064]:lui s1, 2
[0x80008068]:add a1, a1, s1
[0x8000806c]:ld t5, 1008(a1)
[0x80008070]:sub a1, a1, s1
[0x80008074]:lui s1, 2
[0x80008078]:add a1, a1, s1
[0x8000807c]:ld t4, 1016(a1)
[0x80008080]:sub a1, a1, s1
[0x80008084]:addi s1, zero, 0
[0x80008088]:csrrw zero, fcsr, s1
[0x8000808c]:fmax.s t6, t5, t4

[0x8000808c]:fmax.s t6, t5, t4
[0x80008090]:csrrs a2, fcsr, zero
[0x80008094]:sd t6, 960(fp)
[0x80008098]:sd a2, 968(fp)
[0x8000809c]:lui s1, 2
[0x800080a0]:add a1, a1, s1
[0x800080a4]:ld t5, 1024(a1)
[0x800080a8]:sub a1, a1, s1
[0x800080ac]:lui s1, 2
[0x800080b0]:add a1, a1, s1
[0x800080b4]:ld t4, 1032(a1)
[0x800080b8]:sub a1, a1, s1
[0x800080bc]:addi s1, zero, 0
[0x800080c0]:csrrw zero, fcsr, s1
[0x800080c4]:fmax.s t6, t5, t4

[0x800080c4]:fmax.s t6, t5, t4
[0x800080c8]:csrrs a2, fcsr, zero
[0x800080cc]:sd t6, 976(fp)
[0x800080d0]:sd a2, 984(fp)
[0x800080d4]:lui s1, 2
[0x800080d8]:add a1, a1, s1
[0x800080dc]:ld t5, 1040(a1)
[0x800080e0]:sub a1, a1, s1
[0x800080e4]:lui s1, 2
[0x800080e8]:add a1, a1, s1
[0x800080ec]:ld t4, 1048(a1)
[0x800080f0]:sub a1, a1, s1
[0x800080f4]:addi s1, zero, 0
[0x800080f8]:csrrw zero, fcsr, s1
[0x800080fc]:fmax.s t6, t5, t4

[0x800080fc]:fmax.s t6, t5, t4
[0x80008100]:csrrs a2, fcsr, zero
[0x80008104]:sd t6, 992(fp)
[0x80008108]:sd a2, 1000(fp)
[0x8000810c]:lui s1, 2
[0x80008110]:add a1, a1, s1
[0x80008114]:ld t5, 1056(a1)
[0x80008118]:sub a1, a1, s1
[0x8000811c]:lui s1, 2
[0x80008120]:add a1, a1, s1
[0x80008124]:ld t4, 1064(a1)
[0x80008128]:sub a1, a1, s1
[0x8000812c]:addi s1, zero, 0
[0x80008130]:csrrw zero, fcsr, s1
[0x80008134]:fmax.s t6, t5, t4

[0x80008134]:fmax.s t6, t5, t4
[0x80008138]:csrrs a2, fcsr, zero
[0x8000813c]:sd t6, 1008(fp)
[0x80008140]:sd a2, 1016(fp)
[0x80008144]:lui s1, 2
[0x80008148]:add a1, a1, s1
[0x8000814c]:ld t5, 1072(a1)
[0x80008150]:sub a1, a1, s1
[0x80008154]:lui s1, 2
[0x80008158]:add a1, a1, s1
[0x8000815c]:ld t4, 1080(a1)
[0x80008160]:sub a1, a1, s1
[0x80008164]:addi s1, zero, 0
[0x80008168]:csrrw zero, fcsr, s1
[0x8000816c]:fmax.s t6, t5, t4

[0x8000816c]:fmax.s t6, t5, t4
[0x80008170]:csrrs a2, fcsr, zero
[0x80008174]:sd t6, 1024(fp)
[0x80008178]:sd a2, 1032(fp)
[0x8000817c]:lui s1, 2
[0x80008180]:add a1, a1, s1
[0x80008184]:ld t5, 1088(a1)
[0x80008188]:sub a1, a1, s1
[0x8000818c]:lui s1, 2
[0x80008190]:add a1, a1, s1
[0x80008194]:ld t4, 1096(a1)
[0x80008198]:sub a1, a1, s1
[0x8000819c]:addi s1, zero, 0
[0x800081a0]:csrrw zero, fcsr, s1
[0x800081a4]:fmax.s t6, t5, t4

[0x800081a4]:fmax.s t6, t5, t4
[0x800081a8]:csrrs a2, fcsr, zero
[0x800081ac]:sd t6, 1040(fp)
[0x800081b0]:sd a2, 1048(fp)
[0x800081b4]:lui s1, 2
[0x800081b8]:add a1, a1, s1
[0x800081bc]:ld t5, 1104(a1)
[0x800081c0]:sub a1, a1, s1
[0x800081c4]:lui s1, 2
[0x800081c8]:add a1, a1, s1
[0x800081cc]:ld t4, 1112(a1)
[0x800081d0]:sub a1, a1, s1
[0x800081d4]:addi s1, zero, 0
[0x800081d8]:csrrw zero, fcsr, s1
[0x800081dc]:fmax.s t6, t5, t4

[0x800081dc]:fmax.s t6, t5, t4
[0x800081e0]:csrrs a2, fcsr, zero
[0x800081e4]:sd t6, 1056(fp)
[0x800081e8]:sd a2, 1064(fp)
[0x800081ec]:lui s1, 2
[0x800081f0]:add a1, a1, s1
[0x800081f4]:ld t5, 1120(a1)
[0x800081f8]:sub a1, a1, s1
[0x800081fc]:lui s1, 2
[0x80008200]:add a1, a1, s1
[0x80008204]:ld t4, 1128(a1)
[0x80008208]:sub a1, a1, s1
[0x8000820c]:addi s1, zero, 0
[0x80008210]:csrrw zero, fcsr, s1
[0x80008214]:fmax.s t6, t5, t4

[0x80008214]:fmax.s t6, t5, t4
[0x80008218]:csrrs a2, fcsr, zero
[0x8000821c]:sd t6, 1072(fp)
[0x80008220]:sd a2, 1080(fp)
[0x80008224]:lui s1, 2
[0x80008228]:add a1, a1, s1
[0x8000822c]:ld t5, 1136(a1)
[0x80008230]:sub a1, a1, s1
[0x80008234]:lui s1, 2
[0x80008238]:add a1, a1, s1
[0x8000823c]:ld t4, 1144(a1)
[0x80008240]:sub a1, a1, s1
[0x80008244]:addi s1, zero, 0
[0x80008248]:csrrw zero, fcsr, s1
[0x8000824c]:fmax.s t6, t5, t4

[0x8000824c]:fmax.s t6, t5, t4
[0x80008250]:csrrs a2, fcsr, zero
[0x80008254]:sd t6, 1088(fp)
[0x80008258]:sd a2, 1096(fp)
[0x8000825c]:lui s1, 2
[0x80008260]:add a1, a1, s1
[0x80008264]:ld t5, 1152(a1)
[0x80008268]:sub a1, a1, s1
[0x8000826c]:lui s1, 2
[0x80008270]:add a1, a1, s1
[0x80008274]:ld t4, 1160(a1)
[0x80008278]:sub a1, a1, s1
[0x8000827c]:addi s1, zero, 0
[0x80008280]:csrrw zero, fcsr, s1
[0x80008284]:fmax.s t6, t5, t4

[0x80008284]:fmax.s t6, t5, t4
[0x80008288]:csrrs a2, fcsr, zero
[0x8000828c]:sd t6, 1104(fp)
[0x80008290]:sd a2, 1112(fp)
[0x80008294]:lui s1, 2
[0x80008298]:add a1, a1, s1
[0x8000829c]:ld t5, 1168(a1)
[0x800082a0]:sub a1, a1, s1
[0x800082a4]:lui s1, 2
[0x800082a8]:add a1, a1, s1
[0x800082ac]:ld t4, 1176(a1)
[0x800082b0]:sub a1, a1, s1
[0x800082b4]:addi s1, zero, 0
[0x800082b8]:csrrw zero, fcsr, s1
[0x800082bc]:fmax.s t6, t5, t4

[0x800082bc]:fmax.s t6, t5, t4
[0x800082c0]:csrrs a2, fcsr, zero
[0x800082c4]:sd t6, 1120(fp)
[0x800082c8]:sd a2, 1128(fp)
[0x800082cc]:lui s1, 2
[0x800082d0]:add a1, a1, s1
[0x800082d4]:ld t5, 1184(a1)
[0x800082d8]:sub a1, a1, s1
[0x800082dc]:lui s1, 2
[0x800082e0]:add a1, a1, s1
[0x800082e4]:ld t4, 1192(a1)
[0x800082e8]:sub a1, a1, s1
[0x800082ec]:addi s1, zero, 0
[0x800082f0]:csrrw zero, fcsr, s1
[0x800082f4]:fmax.s t6, t5, t4

[0x800082f4]:fmax.s t6, t5, t4
[0x800082f8]:csrrs a2, fcsr, zero
[0x800082fc]:sd t6, 1136(fp)
[0x80008300]:sd a2, 1144(fp)
[0x80008304]:lui s1, 2
[0x80008308]:add a1, a1, s1
[0x8000830c]:ld t5, 1200(a1)
[0x80008310]:sub a1, a1, s1
[0x80008314]:lui s1, 2
[0x80008318]:add a1, a1, s1
[0x8000831c]:ld t4, 1208(a1)
[0x80008320]:sub a1, a1, s1
[0x80008324]:addi s1, zero, 0
[0x80008328]:csrrw zero, fcsr, s1
[0x8000832c]:fmax.s t6, t5, t4

[0x8000832c]:fmax.s t6, t5, t4
[0x80008330]:csrrs a2, fcsr, zero
[0x80008334]:sd t6, 1152(fp)
[0x80008338]:sd a2, 1160(fp)
[0x8000833c]:lui s1, 2
[0x80008340]:add a1, a1, s1
[0x80008344]:ld t5, 1216(a1)
[0x80008348]:sub a1, a1, s1
[0x8000834c]:lui s1, 2
[0x80008350]:add a1, a1, s1
[0x80008354]:ld t4, 1224(a1)
[0x80008358]:sub a1, a1, s1
[0x8000835c]:addi s1, zero, 0
[0x80008360]:csrrw zero, fcsr, s1
[0x80008364]:fmax.s t6, t5, t4

[0x80008364]:fmax.s t6, t5, t4
[0x80008368]:csrrs a2, fcsr, zero
[0x8000836c]:sd t6, 1168(fp)
[0x80008370]:sd a2, 1176(fp)
[0x80008374]:lui s1, 2
[0x80008378]:add a1, a1, s1
[0x8000837c]:ld t5, 1232(a1)
[0x80008380]:sub a1, a1, s1
[0x80008384]:lui s1, 2
[0x80008388]:add a1, a1, s1
[0x8000838c]:ld t4, 1240(a1)
[0x80008390]:sub a1, a1, s1
[0x80008394]:addi s1, zero, 0
[0x80008398]:csrrw zero, fcsr, s1
[0x8000839c]:fmax.s t6, t5, t4

[0x8000839c]:fmax.s t6, t5, t4
[0x800083a0]:csrrs a2, fcsr, zero
[0x800083a4]:sd t6, 1184(fp)
[0x800083a8]:sd a2, 1192(fp)
[0x800083ac]:lui s1, 2
[0x800083b0]:add a1, a1, s1
[0x800083b4]:ld t5, 1248(a1)
[0x800083b8]:sub a1, a1, s1
[0x800083bc]:lui s1, 2
[0x800083c0]:add a1, a1, s1
[0x800083c4]:ld t4, 1256(a1)
[0x800083c8]:sub a1, a1, s1
[0x800083cc]:addi s1, zero, 0
[0x800083d0]:csrrw zero, fcsr, s1
[0x800083d4]:fmax.s t6, t5, t4

[0x800083d4]:fmax.s t6, t5, t4
[0x800083d8]:csrrs a2, fcsr, zero
[0x800083dc]:sd t6, 1200(fp)
[0x800083e0]:sd a2, 1208(fp)
[0x800083e4]:lui s1, 2
[0x800083e8]:add a1, a1, s1
[0x800083ec]:ld t5, 1264(a1)
[0x800083f0]:sub a1, a1, s1
[0x800083f4]:lui s1, 2
[0x800083f8]:add a1, a1, s1
[0x800083fc]:ld t4, 1272(a1)
[0x80008400]:sub a1, a1, s1
[0x80008404]:addi s1, zero, 0
[0x80008408]:csrrw zero, fcsr, s1
[0x8000840c]:fmax.s t6, t5, t4

[0x8000840c]:fmax.s t6, t5, t4
[0x80008410]:csrrs a2, fcsr, zero
[0x80008414]:sd t6, 1216(fp)
[0x80008418]:sd a2, 1224(fp)
[0x8000841c]:lui s1, 2
[0x80008420]:add a1, a1, s1
[0x80008424]:ld t5, 1280(a1)
[0x80008428]:sub a1, a1, s1
[0x8000842c]:lui s1, 2
[0x80008430]:add a1, a1, s1
[0x80008434]:ld t4, 1288(a1)
[0x80008438]:sub a1, a1, s1
[0x8000843c]:addi s1, zero, 0
[0x80008440]:csrrw zero, fcsr, s1
[0x80008444]:fmax.s t6, t5, t4

[0x80008444]:fmax.s t6, t5, t4
[0x80008448]:csrrs a2, fcsr, zero
[0x8000844c]:sd t6, 1232(fp)
[0x80008450]:sd a2, 1240(fp)
[0x80008454]:lui s1, 2
[0x80008458]:add a1, a1, s1
[0x8000845c]:ld t5, 1296(a1)
[0x80008460]:sub a1, a1, s1
[0x80008464]:lui s1, 2
[0x80008468]:add a1, a1, s1
[0x8000846c]:ld t4, 1304(a1)
[0x80008470]:sub a1, a1, s1
[0x80008474]:addi s1, zero, 0
[0x80008478]:csrrw zero, fcsr, s1
[0x8000847c]:fmax.s t6, t5, t4

[0x8000847c]:fmax.s t6, t5, t4
[0x80008480]:csrrs a2, fcsr, zero
[0x80008484]:sd t6, 1248(fp)
[0x80008488]:sd a2, 1256(fp)
[0x8000848c]:lui s1, 2
[0x80008490]:add a1, a1, s1
[0x80008494]:ld t5, 1312(a1)
[0x80008498]:sub a1, a1, s1
[0x8000849c]:lui s1, 2
[0x800084a0]:add a1, a1, s1
[0x800084a4]:ld t4, 1320(a1)
[0x800084a8]:sub a1, a1, s1
[0x800084ac]:addi s1, zero, 0
[0x800084b0]:csrrw zero, fcsr, s1
[0x800084b4]:fmax.s t6, t5, t4

[0x800084b4]:fmax.s t6, t5, t4
[0x800084b8]:csrrs a2, fcsr, zero
[0x800084bc]:sd t6, 1264(fp)
[0x800084c0]:sd a2, 1272(fp)
[0x800084c4]:lui s1, 2
[0x800084c8]:add a1, a1, s1
[0x800084cc]:ld t5, 1328(a1)
[0x800084d0]:sub a1, a1, s1
[0x800084d4]:lui s1, 2
[0x800084d8]:add a1, a1, s1
[0x800084dc]:ld t4, 1336(a1)
[0x800084e0]:sub a1, a1, s1
[0x800084e4]:addi s1, zero, 0
[0x800084e8]:csrrw zero, fcsr, s1
[0x800084ec]:fmax.s t6, t5, t4

[0x800084ec]:fmax.s t6, t5, t4
[0x800084f0]:csrrs a2, fcsr, zero
[0x800084f4]:sd t6, 1280(fp)
[0x800084f8]:sd a2, 1288(fp)
[0x800084fc]:lui s1, 2
[0x80008500]:add a1, a1, s1
[0x80008504]:ld t5, 1344(a1)
[0x80008508]:sub a1, a1, s1
[0x8000850c]:lui s1, 2
[0x80008510]:add a1, a1, s1
[0x80008514]:ld t4, 1352(a1)
[0x80008518]:sub a1, a1, s1
[0x8000851c]:addi s1, zero, 0
[0x80008520]:csrrw zero, fcsr, s1
[0x80008524]:fmax.s t6, t5, t4

[0x80008524]:fmax.s t6, t5, t4
[0x80008528]:csrrs a2, fcsr, zero
[0x8000852c]:sd t6, 1296(fp)
[0x80008530]:sd a2, 1304(fp)
[0x80008534]:lui s1, 2
[0x80008538]:add a1, a1, s1
[0x8000853c]:ld t5, 1360(a1)
[0x80008540]:sub a1, a1, s1
[0x80008544]:lui s1, 2
[0x80008548]:add a1, a1, s1
[0x8000854c]:ld t4, 1368(a1)
[0x80008550]:sub a1, a1, s1
[0x80008554]:addi s1, zero, 0
[0x80008558]:csrrw zero, fcsr, s1
[0x8000855c]:fmax.s t6, t5, t4

[0x8000855c]:fmax.s t6, t5, t4
[0x80008560]:csrrs a2, fcsr, zero
[0x80008564]:sd t6, 1312(fp)
[0x80008568]:sd a2, 1320(fp)
[0x8000856c]:lui s1, 2
[0x80008570]:add a1, a1, s1
[0x80008574]:ld t5, 1376(a1)
[0x80008578]:sub a1, a1, s1
[0x8000857c]:lui s1, 2
[0x80008580]:add a1, a1, s1
[0x80008584]:ld t4, 1384(a1)
[0x80008588]:sub a1, a1, s1
[0x8000858c]:addi s1, zero, 0
[0x80008590]:csrrw zero, fcsr, s1
[0x80008594]:fmax.s t6, t5, t4

[0x80008594]:fmax.s t6, t5, t4
[0x80008598]:csrrs a2, fcsr, zero
[0x8000859c]:sd t6, 1328(fp)
[0x800085a0]:sd a2, 1336(fp)
[0x800085a4]:lui s1, 2
[0x800085a8]:add a1, a1, s1
[0x800085ac]:ld t5, 1392(a1)
[0x800085b0]:sub a1, a1, s1
[0x800085b4]:lui s1, 2
[0x800085b8]:add a1, a1, s1
[0x800085bc]:ld t4, 1400(a1)
[0x800085c0]:sub a1, a1, s1
[0x800085c4]:addi s1, zero, 0
[0x800085c8]:csrrw zero, fcsr, s1
[0x800085cc]:fmax.s t6, t5, t4

[0x800085cc]:fmax.s t6, t5, t4
[0x800085d0]:csrrs a2, fcsr, zero
[0x800085d4]:sd t6, 1344(fp)
[0x800085d8]:sd a2, 1352(fp)
[0x800085dc]:lui s1, 2
[0x800085e0]:add a1, a1, s1
[0x800085e4]:ld t5, 1408(a1)
[0x800085e8]:sub a1, a1, s1
[0x800085ec]:lui s1, 2
[0x800085f0]:add a1, a1, s1
[0x800085f4]:ld t4, 1416(a1)
[0x800085f8]:sub a1, a1, s1
[0x800085fc]:addi s1, zero, 0
[0x80008600]:csrrw zero, fcsr, s1
[0x80008604]:fmax.s t6, t5, t4

[0x80008604]:fmax.s t6, t5, t4
[0x80008608]:csrrs a2, fcsr, zero
[0x8000860c]:sd t6, 1360(fp)
[0x80008610]:sd a2, 1368(fp)
[0x80008614]:lui s1, 2
[0x80008618]:add a1, a1, s1
[0x8000861c]:ld t5, 1424(a1)
[0x80008620]:sub a1, a1, s1
[0x80008624]:lui s1, 2
[0x80008628]:add a1, a1, s1
[0x8000862c]:ld t4, 1432(a1)
[0x80008630]:sub a1, a1, s1
[0x80008634]:addi s1, zero, 0
[0x80008638]:csrrw zero, fcsr, s1
[0x8000863c]:fmax.s t6, t5, t4

[0x8000863c]:fmax.s t6, t5, t4
[0x80008640]:csrrs a2, fcsr, zero
[0x80008644]:sd t6, 1376(fp)
[0x80008648]:sd a2, 1384(fp)
[0x8000864c]:lui s1, 2
[0x80008650]:add a1, a1, s1
[0x80008654]:ld t5, 1440(a1)
[0x80008658]:sub a1, a1, s1
[0x8000865c]:lui s1, 2
[0x80008660]:add a1, a1, s1
[0x80008664]:ld t4, 1448(a1)
[0x80008668]:sub a1, a1, s1
[0x8000866c]:addi s1, zero, 0
[0x80008670]:csrrw zero, fcsr, s1
[0x80008674]:fmax.s t6, t5, t4

[0x80008674]:fmax.s t6, t5, t4
[0x80008678]:csrrs a2, fcsr, zero
[0x8000867c]:sd t6, 1392(fp)
[0x80008680]:sd a2, 1400(fp)
[0x80008684]:lui s1, 2
[0x80008688]:add a1, a1, s1
[0x8000868c]:ld t5, 1456(a1)
[0x80008690]:sub a1, a1, s1
[0x80008694]:lui s1, 2
[0x80008698]:add a1, a1, s1
[0x8000869c]:ld t4, 1464(a1)
[0x800086a0]:sub a1, a1, s1
[0x800086a4]:addi s1, zero, 0
[0x800086a8]:csrrw zero, fcsr, s1
[0x800086ac]:fmax.s t6, t5, t4

[0x800086ac]:fmax.s t6, t5, t4
[0x800086b0]:csrrs a2, fcsr, zero
[0x800086b4]:sd t6, 1408(fp)
[0x800086b8]:sd a2, 1416(fp)
[0x800086bc]:lui s1, 2
[0x800086c0]:add a1, a1, s1
[0x800086c4]:ld t5, 1472(a1)
[0x800086c8]:sub a1, a1, s1
[0x800086cc]:lui s1, 2
[0x800086d0]:add a1, a1, s1
[0x800086d4]:ld t4, 1480(a1)
[0x800086d8]:sub a1, a1, s1
[0x800086dc]:addi s1, zero, 0
[0x800086e0]:csrrw zero, fcsr, s1
[0x800086e4]:fmax.s t6, t5, t4

[0x800086e4]:fmax.s t6, t5, t4
[0x800086e8]:csrrs a2, fcsr, zero
[0x800086ec]:sd t6, 1424(fp)
[0x800086f0]:sd a2, 1432(fp)
[0x800086f4]:lui s1, 2
[0x800086f8]:add a1, a1, s1
[0x800086fc]:ld t5, 1488(a1)
[0x80008700]:sub a1, a1, s1
[0x80008704]:lui s1, 2
[0x80008708]:add a1, a1, s1
[0x8000870c]:ld t4, 1496(a1)
[0x80008710]:sub a1, a1, s1
[0x80008714]:addi s1, zero, 0
[0x80008718]:csrrw zero, fcsr, s1
[0x8000871c]:fmax.s t6, t5, t4

[0x8000871c]:fmax.s t6, t5, t4
[0x80008720]:csrrs a2, fcsr, zero
[0x80008724]:sd t6, 1440(fp)
[0x80008728]:sd a2, 1448(fp)
[0x8000872c]:lui s1, 2
[0x80008730]:add a1, a1, s1
[0x80008734]:ld t5, 1504(a1)
[0x80008738]:sub a1, a1, s1
[0x8000873c]:lui s1, 2
[0x80008740]:add a1, a1, s1
[0x80008744]:ld t4, 1512(a1)
[0x80008748]:sub a1, a1, s1
[0x8000874c]:addi s1, zero, 0
[0x80008750]:csrrw zero, fcsr, s1
[0x80008754]:fmax.s t6, t5, t4

[0x80008754]:fmax.s t6, t5, t4
[0x80008758]:csrrs a2, fcsr, zero
[0x8000875c]:sd t6, 1456(fp)
[0x80008760]:sd a2, 1464(fp)
[0x80008764]:lui s1, 2
[0x80008768]:add a1, a1, s1
[0x8000876c]:ld t5, 1520(a1)
[0x80008770]:sub a1, a1, s1
[0x80008774]:lui s1, 2
[0x80008778]:add a1, a1, s1
[0x8000877c]:ld t4, 1528(a1)
[0x80008780]:sub a1, a1, s1
[0x80008784]:addi s1, zero, 0
[0x80008788]:csrrw zero, fcsr, s1
[0x8000878c]:fmax.s t6, t5, t4

[0x8000878c]:fmax.s t6, t5, t4
[0x80008790]:csrrs a2, fcsr, zero
[0x80008794]:sd t6, 1472(fp)
[0x80008798]:sd a2, 1480(fp)
[0x8000879c]:lui s1, 2
[0x800087a0]:add a1, a1, s1
[0x800087a4]:ld t5, 1536(a1)
[0x800087a8]:sub a1, a1, s1
[0x800087ac]:lui s1, 2
[0x800087b0]:add a1, a1, s1
[0x800087b4]:ld t4, 1544(a1)
[0x800087b8]:sub a1, a1, s1
[0x800087bc]:addi s1, zero, 0
[0x800087c0]:csrrw zero, fcsr, s1
[0x800087c4]:fmax.s t6, t5, t4

[0x800087c4]:fmax.s t6, t5, t4
[0x800087c8]:csrrs a2, fcsr, zero
[0x800087cc]:sd t6, 1488(fp)
[0x800087d0]:sd a2, 1496(fp)
[0x800087d4]:lui s1, 2
[0x800087d8]:add a1, a1, s1
[0x800087dc]:ld t5, 1552(a1)
[0x800087e0]:sub a1, a1, s1
[0x800087e4]:lui s1, 2
[0x800087e8]:add a1, a1, s1
[0x800087ec]:ld t4, 1560(a1)
[0x800087f0]:sub a1, a1, s1
[0x800087f4]:addi s1, zero, 0
[0x800087f8]:csrrw zero, fcsr, s1
[0x800087fc]:fmax.s t6, t5, t4

[0x800087fc]:fmax.s t6, t5, t4
[0x80008800]:csrrs a2, fcsr, zero
[0x80008804]:sd t6, 1504(fp)
[0x80008808]:sd a2, 1512(fp)
[0x8000880c]:lui s1, 2
[0x80008810]:add a1, a1, s1
[0x80008814]:ld t5, 1568(a1)
[0x80008818]:sub a1, a1, s1
[0x8000881c]:lui s1, 2
[0x80008820]:add a1, a1, s1
[0x80008824]:ld t4, 1576(a1)
[0x80008828]:sub a1, a1, s1
[0x8000882c]:addi s1, zero, 0
[0x80008830]:csrrw zero, fcsr, s1
[0x80008834]:fmax.s t6, t5, t4

[0x80008834]:fmax.s t6, t5, t4
[0x80008838]:csrrs a2, fcsr, zero
[0x8000883c]:sd t6, 1520(fp)
[0x80008840]:sd a2, 1528(fp)
[0x80008844]:lui s1, 2
[0x80008848]:add a1, a1, s1
[0x8000884c]:ld t5, 1584(a1)
[0x80008850]:sub a1, a1, s1
[0x80008854]:lui s1, 2
[0x80008858]:add a1, a1, s1
[0x8000885c]:ld t4, 1592(a1)
[0x80008860]:sub a1, a1, s1
[0x80008864]:addi s1, zero, 0
[0x80008868]:csrrw zero, fcsr, s1
[0x8000886c]:fmax.s t6, t5, t4

[0x8000886c]:fmax.s t6, t5, t4
[0x80008870]:csrrs a2, fcsr, zero
[0x80008874]:sd t6, 1536(fp)
[0x80008878]:sd a2, 1544(fp)
[0x8000887c]:lui s1, 2
[0x80008880]:add a1, a1, s1
[0x80008884]:ld t5, 1600(a1)
[0x80008888]:sub a1, a1, s1
[0x8000888c]:lui s1, 2
[0x80008890]:add a1, a1, s1
[0x80008894]:ld t4, 1608(a1)
[0x80008898]:sub a1, a1, s1
[0x8000889c]:addi s1, zero, 0
[0x800088a0]:csrrw zero, fcsr, s1
[0x800088a4]:fmax.s t6, t5, t4

[0x800088a4]:fmax.s t6, t5, t4
[0x800088a8]:csrrs a2, fcsr, zero
[0x800088ac]:sd t6, 1552(fp)
[0x800088b0]:sd a2, 1560(fp)
[0x800088b4]:lui s1, 2
[0x800088b8]:add a1, a1, s1
[0x800088bc]:ld t5, 1616(a1)
[0x800088c0]:sub a1, a1, s1
[0x800088c4]:lui s1, 2
[0x800088c8]:add a1, a1, s1
[0x800088cc]:ld t4, 1624(a1)
[0x800088d0]:sub a1, a1, s1
[0x800088d4]:addi s1, zero, 0
[0x800088d8]:csrrw zero, fcsr, s1
[0x800088dc]:fmax.s t6, t5, t4

[0x800088dc]:fmax.s t6, t5, t4
[0x800088e0]:csrrs a2, fcsr, zero
[0x800088e4]:sd t6, 1568(fp)
[0x800088e8]:sd a2, 1576(fp)
[0x800088ec]:lui s1, 2
[0x800088f0]:add a1, a1, s1
[0x800088f4]:ld t5, 1632(a1)
[0x800088f8]:sub a1, a1, s1
[0x800088fc]:lui s1, 2
[0x80008900]:add a1, a1, s1
[0x80008904]:ld t4, 1640(a1)
[0x80008908]:sub a1, a1, s1
[0x8000890c]:addi s1, zero, 0
[0x80008910]:csrrw zero, fcsr, s1
[0x80008914]:fmax.s t6, t5, t4

[0x80008914]:fmax.s t6, t5, t4
[0x80008918]:csrrs a2, fcsr, zero
[0x8000891c]:sd t6, 1584(fp)
[0x80008920]:sd a2, 1592(fp)
[0x80008924]:lui s1, 2
[0x80008928]:add a1, a1, s1
[0x8000892c]:ld t5, 1648(a1)
[0x80008930]:sub a1, a1, s1
[0x80008934]:lui s1, 2
[0x80008938]:add a1, a1, s1
[0x8000893c]:ld t4, 1656(a1)
[0x80008940]:sub a1, a1, s1
[0x80008944]:addi s1, zero, 0
[0x80008948]:csrrw zero, fcsr, s1
[0x8000894c]:fmax.s t6, t5, t4

[0x8000894c]:fmax.s t6, t5, t4
[0x80008950]:csrrs a2, fcsr, zero
[0x80008954]:sd t6, 1600(fp)
[0x80008958]:sd a2, 1608(fp)
[0x8000895c]:lui s1, 2
[0x80008960]:add a1, a1, s1
[0x80008964]:ld t5, 1664(a1)
[0x80008968]:sub a1, a1, s1
[0x8000896c]:lui s1, 2
[0x80008970]:add a1, a1, s1
[0x80008974]:ld t4, 1672(a1)
[0x80008978]:sub a1, a1, s1
[0x8000897c]:addi s1, zero, 0
[0x80008980]:csrrw zero, fcsr, s1
[0x80008984]:fmax.s t6, t5, t4

[0x80008984]:fmax.s t6, t5, t4
[0x80008988]:csrrs a2, fcsr, zero
[0x8000898c]:sd t6, 1616(fp)
[0x80008990]:sd a2, 1624(fp)
[0x80008994]:lui s1, 2
[0x80008998]:add a1, a1, s1
[0x8000899c]:ld t5, 1680(a1)
[0x800089a0]:sub a1, a1, s1
[0x800089a4]:lui s1, 2
[0x800089a8]:add a1, a1, s1
[0x800089ac]:ld t4, 1688(a1)
[0x800089b0]:sub a1, a1, s1
[0x800089b4]:addi s1, zero, 0
[0x800089b8]:csrrw zero, fcsr, s1
[0x800089bc]:fmax.s t6, t5, t4

[0x800089bc]:fmax.s t6, t5, t4
[0x800089c0]:csrrs a2, fcsr, zero
[0x800089c4]:sd t6, 1632(fp)
[0x800089c8]:sd a2, 1640(fp)
[0x800089cc]:lui s1, 2
[0x800089d0]:add a1, a1, s1
[0x800089d4]:ld t5, 1696(a1)
[0x800089d8]:sub a1, a1, s1
[0x800089dc]:lui s1, 2
[0x800089e0]:add a1, a1, s1
[0x800089e4]:ld t4, 1704(a1)
[0x800089e8]:sub a1, a1, s1
[0x800089ec]:addi s1, zero, 0
[0x800089f0]:csrrw zero, fcsr, s1
[0x800089f4]:fmax.s t6, t5, t4

[0x800089f4]:fmax.s t6, t5, t4
[0x800089f8]:csrrs a2, fcsr, zero
[0x800089fc]:sd t6, 1648(fp)
[0x80008a00]:sd a2, 1656(fp)
[0x80008a04]:lui s1, 2
[0x80008a08]:add a1, a1, s1
[0x80008a0c]:ld t5, 1712(a1)
[0x80008a10]:sub a1, a1, s1
[0x80008a14]:lui s1, 2
[0x80008a18]:add a1, a1, s1
[0x80008a1c]:ld t4, 1720(a1)
[0x80008a20]:sub a1, a1, s1
[0x80008a24]:addi s1, zero, 0
[0x80008a28]:csrrw zero, fcsr, s1
[0x80008a2c]:fmax.s t6, t5, t4

[0x80008a2c]:fmax.s t6, t5, t4
[0x80008a30]:csrrs a2, fcsr, zero
[0x80008a34]:sd t6, 1664(fp)
[0x80008a38]:sd a2, 1672(fp)
[0x80008a3c]:lui s1, 2
[0x80008a40]:add a1, a1, s1
[0x80008a44]:ld t5, 1728(a1)
[0x80008a48]:sub a1, a1, s1
[0x80008a4c]:lui s1, 2
[0x80008a50]:add a1, a1, s1
[0x80008a54]:ld t4, 1736(a1)
[0x80008a58]:sub a1, a1, s1
[0x80008a5c]:addi s1, zero, 0
[0x80008a60]:csrrw zero, fcsr, s1
[0x80008a64]:fmax.s t6, t5, t4

[0x80008a64]:fmax.s t6, t5, t4
[0x80008a68]:csrrs a2, fcsr, zero
[0x80008a6c]:sd t6, 1680(fp)
[0x80008a70]:sd a2, 1688(fp)
[0x80008a74]:lui s1, 2
[0x80008a78]:add a1, a1, s1
[0x80008a7c]:ld t5, 1744(a1)
[0x80008a80]:sub a1, a1, s1
[0x80008a84]:lui s1, 2
[0x80008a88]:add a1, a1, s1
[0x80008a8c]:ld t4, 1752(a1)
[0x80008a90]:sub a1, a1, s1
[0x80008a94]:addi s1, zero, 0
[0x80008a98]:csrrw zero, fcsr, s1
[0x80008a9c]:fmax.s t6, t5, t4

[0x80008a9c]:fmax.s t6, t5, t4
[0x80008aa0]:csrrs a2, fcsr, zero
[0x80008aa4]:sd t6, 1696(fp)
[0x80008aa8]:sd a2, 1704(fp)
[0x80008aac]:lui s1, 2
[0x80008ab0]:add a1, a1, s1
[0x80008ab4]:ld t5, 1760(a1)
[0x80008ab8]:sub a1, a1, s1
[0x80008abc]:lui s1, 2
[0x80008ac0]:add a1, a1, s1
[0x80008ac4]:ld t4, 1768(a1)
[0x80008ac8]:sub a1, a1, s1
[0x80008acc]:addi s1, zero, 0
[0x80008ad0]:csrrw zero, fcsr, s1
[0x80008ad4]:fmax.s t6, t5, t4

[0x80008ad4]:fmax.s t6, t5, t4
[0x80008ad8]:csrrs a2, fcsr, zero
[0x80008adc]:sd t6, 1712(fp)
[0x80008ae0]:sd a2, 1720(fp)
[0x80008ae4]:lui s1, 2
[0x80008ae8]:add a1, a1, s1
[0x80008aec]:ld t5, 1776(a1)
[0x80008af0]:sub a1, a1, s1
[0x80008af4]:lui s1, 2
[0x80008af8]:add a1, a1, s1
[0x80008afc]:ld t4, 1784(a1)
[0x80008b00]:sub a1, a1, s1
[0x80008b04]:addi s1, zero, 0
[0x80008b08]:csrrw zero, fcsr, s1
[0x80008b0c]:fmax.s t6, t5, t4

[0x80008b0c]:fmax.s t6, t5, t4
[0x80008b10]:csrrs a2, fcsr, zero
[0x80008b14]:sd t6, 1728(fp)
[0x80008b18]:sd a2, 1736(fp)
[0x80008b1c]:lui s1, 2
[0x80008b20]:add a1, a1, s1
[0x80008b24]:ld t5, 1792(a1)
[0x80008b28]:sub a1, a1, s1
[0x80008b2c]:lui s1, 2
[0x80008b30]:add a1, a1, s1
[0x80008b34]:ld t4, 1800(a1)
[0x80008b38]:sub a1, a1, s1
[0x80008b3c]:addi s1, zero, 0
[0x80008b40]:csrrw zero, fcsr, s1
[0x80008b44]:fmax.s t6, t5, t4

[0x80008b44]:fmax.s t6, t5, t4
[0x80008b48]:csrrs a2, fcsr, zero
[0x80008b4c]:sd t6, 1744(fp)
[0x80008b50]:sd a2, 1752(fp)
[0x80008b54]:lui s1, 2
[0x80008b58]:add a1, a1, s1
[0x80008b5c]:ld t5, 1808(a1)
[0x80008b60]:sub a1, a1, s1
[0x80008b64]:lui s1, 2
[0x80008b68]:add a1, a1, s1
[0x80008b6c]:ld t4, 1816(a1)
[0x80008b70]:sub a1, a1, s1
[0x80008b74]:addi s1, zero, 0
[0x80008b78]:csrrw zero, fcsr, s1
[0x80008b7c]:fmax.s t6, t5, t4

[0x80008b7c]:fmax.s t6, t5, t4
[0x80008b80]:csrrs a2, fcsr, zero
[0x80008b84]:sd t6, 1760(fp)
[0x80008b88]:sd a2, 1768(fp)
[0x80008b8c]:lui s1, 2
[0x80008b90]:add a1, a1, s1
[0x80008b94]:ld t5, 1824(a1)
[0x80008b98]:sub a1, a1, s1
[0x80008b9c]:lui s1, 2
[0x80008ba0]:add a1, a1, s1
[0x80008ba4]:ld t4, 1832(a1)
[0x80008ba8]:sub a1, a1, s1
[0x80008bac]:addi s1, zero, 0
[0x80008bb0]:csrrw zero, fcsr, s1
[0x80008bb4]:fmax.s t6, t5, t4

[0x80008bb4]:fmax.s t6, t5, t4
[0x80008bb8]:csrrs a2, fcsr, zero
[0x80008bbc]:sd t6, 1776(fp)
[0x80008bc0]:sd a2, 1784(fp)
[0x80008bc4]:lui s1, 2
[0x80008bc8]:add a1, a1, s1
[0x80008bcc]:ld t5, 1840(a1)
[0x80008bd0]:sub a1, a1, s1
[0x80008bd4]:lui s1, 2
[0x80008bd8]:add a1, a1, s1
[0x80008bdc]:ld t4, 1848(a1)
[0x80008be0]:sub a1, a1, s1
[0x80008be4]:addi s1, zero, 0
[0x80008be8]:csrrw zero, fcsr, s1
[0x80008bec]:fmax.s t6, t5, t4

[0x80008bec]:fmax.s t6, t5, t4
[0x80008bf0]:csrrs a2, fcsr, zero
[0x80008bf4]:sd t6, 1792(fp)
[0x80008bf8]:sd a2, 1800(fp)
[0x80008bfc]:lui s1, 2
[0x80008c00]:add a1, a1, s1
[0x80008c04]:ld t5, 1856(a1)
[0x80008c08]:sub a1, a1, s1
[0x80008c0c]:lui s1, 2
[0x80008c10]:add a1, a1, s1
[0x80008c14]:ld t4, 1864(a1)
[0x80008c18]:sub a1, a1, s1
[0x80008c1c]:addi s1, zero, 0
[0x80008c20]:csrrw zero, fcsr, s1
[0x80008c24]:fmax.s t6, t5, t4

[0x80008c24]:fmax.s t6, t5, t4
[0x80008c28]:csrrs a2, fcsr, zero
[0x80008c2c]:sd t6, 1808(fp)
[0x80008c30]:sd a2, 1816(fp)
[0x80008c34]:lui s1, 2
[0x80008c38]:add a1, a1, s1
[0x80008c3c]:ld t5, 1872(a1)
[0x80008c40]:sub a1, a1, s1
[0x80008c44]:lui s1, 2
[0x80008c48]:add a1, a1, s1
[0x80008c4c]:ld t4, 1880(a1)
[0x80008c50]:sub a1, a1, s1
[0x80008c54]:addi s1, zero, 0
[0x80008c58]:csrrw zero, fcsr, s1
[0x80008c5c]:fmax.s t6, t5, t4

[0x80008c5c]:fmax.s t6, t5, t4
[0x80008c60]:csrrs a2, fcsr, zero
[0x80008c64]:sd t6, 1824(fp)
[0x80008c68]:sd a2, 1832(fp)
[0x80008c6c]:lui s1, 2
[0x80008c70]:add a1, a1, s1
[0x80008c74]:ld t5, 1888(a1)
[0x80008c78]:sub a1, a1, s1
[0x80008c7c]:lui s1, 2
[0x80008c80]:add a1, a1, s1
[0x80008c84]:ld t4, 1896(a1)
[0x80008c88]:sub a1, a1, s1
[0x80008c8c]:addi s1, zero, 0
[0x80008c90]:csrrw zero, fcsr, s1
[0x80008c94]:fmax.s t6, t5, t4

[0x80008c94]:fmax.s t6, t5, t4
[0x80008c98]:csrrs a2, fcsr, zero
[0x80008c9c]:sd t6, 1840(fp)
[0x80008ca0]:sd a2, 1848(fp)
[0x80008ca4]:lui s1, 2
[0x80008ca8]:add a1, a1, s1
[0x80008cac]:ld t5, 1904(a1)
[0x80008cb0]:sub a1, a1, s1
[0x80008cb4]:lui s1, 2
[0x80008cb8]:add a1, a1, s1
[0x80008cbc]:ld t4, 1912(a1)
[0x80008cc0]:sub a1, a1, s1
[0x80008cc4]:addi s1, zero, 0
[0x80008cc8]:csrrw zero, fcsr, s1
[0x80008ccc]:fmax.s t6, t5, t4

[0x80008ccc]:fmax.s t6, t5, t4
[0x80008cd0]:csrrs a2, fcsr, zero
[0x80008cd4]:sd t6, 1856(fp)
[0x80008cd8]:sd a2, 1864(fp)
[0x80008cdc]:lui s1, 2
[0x80008ce0]:add a1, a1, s1
[0x80008ce4]:ld t5, 1920(a1)
[0x80008ce8]:sub a1, a1, s1
[0x80008cec]:lui s1, 2
[0x80008cf0]:add a1, a1, s1
[0x80008cf4]:ld t4, 1928(a1)
[0x80008cf8]:sub a1, a1, s1
[0x80008cfc]:addi s1, zero, 0
[0x80008d00]:csrrw zero, fcsr, s1
[0x80008d04]:fmax.s t6, t5, t4

[0x80008d04]:fmax.s t6, t5, t4
[0x80008d08]:csrrs a2, fcsr, zero
[0x80008d0c]:sd t6, 1872(fp)
[0x80008d10]:sd a2, 1880(fp)
[0x80008d14]:lui s1, 2
[0x80008d18]:add a1, a1, s1
[0x80008d1c]:ld t5, 1936(a1)
[0x80008d20]:sub a1, a1, s1
[0x80008d24]:lui s1, 2
[0x80008d28]:add a1, a1, s1
[0x80008d2c]:ld t4, 1944(a1)
[0x80008d30]:sub a1, a1, s1
[0x80008d34]:addi s1, zero, 0
[0x80008d38]:csrrw zero, fcsr, s1
[0x80008d3c]:fmax.s t6, t5, t4

[0x80008d3c]:fmax.s t6, t5, t4
[0x80008d40]:csrrs a2, fcsr, zero
[0x80008d44]:sd t6, 1888(fp)
[0x80008d48]:sd a2, 1896(fp)
[0x80008d4c]:lui s1, 2
[0x80008d50]:add a1, a1, s1
[0x80008d54]:ld t5, 1952(a1)
[0x80008d58]:sub a1, a1, s1
[0x80008d5c]:lui s1, 2
[0x80008d60]:add a1, a1, s1
[0x80008d64]:ld t4, 1960(a1)
[0x80008d68]:sub a1, a1, s1
[0x80008d6c]:addi s1, zero, 0
[0x80008d70]:csrrw zero, fcsr, s1
[0x80008d74]:fmax.s t6, t5, t4

[0x80008d74]:fmax.s t6, t5, t4
[0x80008d78]:csrrs a2, fcsr, zero
[0x80008d7c]:sd t6, 1904(fp)
[0x80008d80]:sd a2, 1912(fp)
[0x80008d84]:lui s1, 2
[0x80008d88]:add a1, a1, s1
[0x80008d8c]:ld t5, 1968(a1)
[0x80008d90]:sub a1, a1, s1
[0x80008d94]:lui s1, 2
[0x80008d98]:add a1, a1, s1
[0x80008d9c]:ld t4, 1976(a1)
[0x80008da0]:sub a1, a1, s1
[0x80008da4]:addi s1, zero, 0
[0x80008da8]:csrrw zero, fcsr, s1
[0x80008dac]:fmax.s t6, t5, t4

[0x80008dac]:fmax.s t6, t5, t4
[0x80008db0]:csrrs a2, fcsr, zero
[0x80008db4]:sd t6, 1920(fp)
[0x80008db8]:sd a2, 1928(fp)
[0x80008dbc]:lui s1, 2
[0x80008dc0]:add a1, a1, s1
[0x80008dc4]:ld t5, 1984(a1)
[0x80008dc8]:sub a1, a1, s1
[0x80008dcc]:lui s1, 2
[0x80008dd0]:add a1, a1, s1
[0x80008dd4]:ld t4, 1992(a1)
[0x80008dd8]:sub a1, a1, s1
[0x80008ddc]:addi s1, zero, 0
[0x80008de0]:csrrw zero, fcsr, s1
[0x80008de4]:fmax.s t6, t5, t4

[0x80008de4]:fmax.s t6, t5, t4
[0x80008de8]:csrrs a2, fcsr, zero
[0x80008dec]:sd t6, 1936(fp)
[0x80008df0]:sd a2, 1944(fp)
[0x80008df4]:lui s1, 2
[0x80008df8]:add a1, a1, s1
[0x80008dfc]:ld t5, 2000(a1)
[0x80008e00]:sub a1, a1, s1
[0x80008e04]:lui s1, 2
[0x80008e08]:add a1, a1, s1
[0x80008e0c]:ld t4, 2008(a1)
[0x80008e10]:sub a1, a1, s1
[0x80008e14]:addi s1, zero, 0
[0x80008e18]:csrrw zero, fcsr, s1
[0x80008e1c]:fmax.s t6, t5, t4

[0x80008e1c]:fmax.s t6, t5, t4
[0x80008e20]:csrrs a2, fcsr, zero
[0x80008e24]:sd t6, 1952(fp)
[0x80008e28]:sd a2, 1960(fp)
[0x80008e2c]:lui s1, 2
[0x80008e30]:add a1, a1, s1
[0x80008e34]:ld t5, 2016(a1)
[0x80008e38]:sub a1, a1, s1
[0x80008e3c]:lui s1, 2
[0x80008e40]:add a1, a1, s1
[0x80008e44]:ld t4, 2024(a1)
[0x80008e48]:sub a1, a1, s1
[0x80008e4c]:addi s1, zero, 0
[0x80008e50]:csrrw zero, fcsr, s1
[0x80008e54]:fmax.s t6, t5, t4

[0x80008e54]:fmax.s t6, t5, t4
[0x80008e58]:csrrs a2, fcsr, zero
[0x80008e5c]:sd t6, 1968(fp)
[0x80008e60]:sd a2, 1976(fp)
[0x80008e64]:lui s1, 2
[0x80008e68]:add a1, a1, s1
[0x80008e6c]:ld t5, 2032(a1)
[0x80008e70]:sub a1, a1, s1
[0x80008e74]:lui s1, 2
[0x80008e78]:add a1, a1, s1
[0x80008e7c]:ld t4, 2040(a1)
[0x80008e80]:sub a1, a1, s1
[0x80008e84]:addi s1, zero, 0
[0x80008e88]:csrrw zero, fcsr, s1
[0x80008e8c]:fmax.s t6, t5, t4

[0x80008e8c]:fmax.s t6, t5, t4
[0x80008e90]:csrrs a2, fcsr, zero
[0x80008e94]:sd t6, 1984(fp)
[0x80008e98]:sd a2, 1992(fp)
[0x80008e9c]:lui s1, 3
[0x80008ea0]:addiw s1, s1, 2048
[0x80008ea4]:add a1, a1, s1
[0x80008ea8]:ld t5, 0(a1)
[0x80008eac]:sub a1, a1, s1
[0x80008eb0]:lui s1, 3
[0x80008eb4]:addiw s1, s1, 2048
[0x80008eb8]:add a1, a1, s1
[0x80008ebc]:ld t4, 8(a1)
[0x80008ec0]:sub a1, a1, s1
[0x80008ec4]:addi s1, zero, 0
[0x80008ec8]:csrrw zero, fcsr, s1
[0x80008ecc]:fmax.s t6, t5, t4

[0x80008ecc]:fmax.s t6, t5, t4
[0x80008ed0]:csrrs a2, fcsr, zero
[0x80008ed4]:sd t6, 2000(fp)
[0x80008ed8]:sd a2, 2008(fp)
[0x80008edc]:lui s1, 3
[0x80008ee0]:addiw s1, s1, 2048
[0x80008ee4]:add a1, a1, s1
[0x80008ee8]:ld t5, 16(a1)
[0x80008eec]:sub a1, a1, s1
[0x80008ef0]:lui s1, 3
[0x80008ef4]:addiw s1, s1, 2048
[0x80008ef8]:add a1, a1, s1
[0x80008efc]:ld t4, 24(a1)
[0x80008f00]:sub a1, a1, s1
[0x80008f04]:addi s1, zero, 0
[0x80008f08]:csrrw zero, fcsr, s1
[0x80008f0c]:fmax.s t6, t5, t4

[0x80008f0c]:fmax.s t6, t5, t4
[0x80008f10]:csrrs a2, fcsr, zero
[0x80008f14]:sd t6, 2016(fp)
[0x80008f18]:sd a2, 2024(fp)
[0x80008f1c]:lui s1, 3
[0x80008f20]:addiw s1, s1, 2048
[0x80008f24]:add a1, a1, s1
[0x80008f28]:ld t5, 32(a1)
[0x80008f2c]:sub a1, a1, s1
[0x80008f30]:lui s1, 3
[0x80008f34]:addiw s1, s1, 2048
[0x80008f38]:add a1, a1, s1
[0x80008f3c]:ld t4, 40(a1)
[0x80008f40]:sub a1, a1, s1
[0x80008f44]:addi s1, zero, 0
[0x80008f48]:csrrw zero, fcsr, s1
[0x80008f4c]:fmax.s t6, t5, t4

[0x80008f4c]:fmax.s t6, t5, t4
[0x80008f50]:csrrs a2, fcsr, zero
[0x80008f54]:sd t6, 2032(fp)
[0x80008f58]:sd a2, 2040(fp)
[0x80008f5c]:auipc fp, 13
[0x80008f60]:addi fp, fp, 2124
[0x80008f64]:lui s1, 3
[0x80008f68]:addiw s1, s1, 2048
[0x80008f6c]:add a1, a1, s1
[0x80008f70]:ld t5, 48(a1)
[0x80008f74]:sub a1, a1, s1
[0x80008f78]:lui s1, 3
[0x80008f7c]:addiw s1, s1, 2048
[0x80008f80]:add a1, a1, s1
[0x80008f84]:ld t4, 56(a1)
[0x80008f88]:sub a1, a1, s1
[0x80008f8c]:addi s1, zero, 0
[0x80008f90]:csrrw zero, fcsr, s1
[0x80008f94]:fmax.s t6, t5, t4

[0x80008f94]:fmax.s t6, t5, t4
[0x80008f98]:csrrs a2, fcsr, zero
[0x80008f9c]:sd t6, 0(fp)
[0x80008fa0]:sd a2, 8(fp)
[0x80008fa4]:lui s1, 3
[0x80008fa8]:addiw s1, s1, 2048
[0x80008fac]:add a1, a1, s1
[0x80008fb0]:ld t5, 64(a1)
[0x80008fb4]:sub a1, a1, s1
[0x80008fb8]:lui s1, 3
[0x80008fbc]:addiw s1, s1, 2048
[0x80008fc0]:add a1, a1, s1
[0x80008fc4]:ld t4, 72(a1)
[0x80008fc8]:sub a1, a1, s1
[0x80008fcc]:addi s1, zero, 0
[0x80008fd0]:csrrw zero, fcsr, s1
[0x80008fd4]:fmax.s t6, t5, t4

[0x80008fd4]:fmax.s t6, t5, t4
[0x80008fd8]:csrrs a2, fcsr, zero
[0x80008fdc]:sd t6, 16(fp)
[0x80008fe0]:sd a2, 24(fp)
[0x80008fe4]:lui s1, 3
[0x80008fe8]:addiw s1, s1, 2048
[0x80008fec]:add a1, a1, s1
[0x80008ff0]:ld t5, 80(a1)
[0x80008ff4]:sub a1, a1, s1
[0x80008ff8]:lui s1, 3
[0x80008ffc]:addiw s1, s1, 2048
[0x80009000]:add a1, a1, s1
[0x80009004]:ld t4, 88(a1)
[0x80009008]:sub a1, a1, s1
[0x8000900c]:addi s1, zero, 0
[0x80009010]:csrrw zero, fcsr, s1
[0x80009014]:fmax.s t6, t5, t4

[0x80009014]:fmax.s t6, t5, t4
[0x80009018]:csrrs a2, fcsr, zero
[0x8000901c]:sd t6, 32(fp)
[0x80009020]:sd a2, 40(fp)
[0x80009024]:lui s1, 3
[0x80009028]:addiw s1, s1, 2048
[0x8000902c]:add a1, a1, s1
[0x80009030]:ld t5, 96(a1)
[0x80009034]:sub a1, a1, s1
[0x80009038]:lui s1, 3
[0x8000903c]:addiw s1, s1, 2048
[0x80009040]:add a1, a1, s1
[0x80009044]:ld t4, 104(a1)
[0x80009048]:sub a1, a1, s1
[0x8000904c]:addi s1, zero, 0
[0x80009050]:csrrw zero, fcsr, s1
[0x80009054]:fmax.s t6, t5, t4

[0x80009054]:fmax.s t6, t5, t4
[0x80009058]:csrrs a2, fcsr, zero
[0x8000905c]:sd t6, 48(fp)
[0x80009060]:sd a2, 56(fp)
[0x80009064]:lui s1, 3
[0x80009068]:addiw s1, s1, 2048
[0x8000906c]:add a1, a1, s1
[0x80009070]:ld t5, 112(a1)
[0x80009074]:sub a1, a1, s1
[0x80009078]:lui s1, 3
[0x8000907c]:addiw s1, s1, 2048
[0x80009080]:add a1, a1, s1
[0x80009084]:ld t4, 120(a1)
[0x80009088]:sub a1, a1, s1
[0x8000908c]:addi s1, zero, 0
[0x80009090]:csrrw zero, fcsr, s1
[0x80009094]:fmax.s t6, t5, t4

[0x80009094]:fmax.s t6, t5, t4
[0x80009098]:csrrs a2, fcsr, zero
[0x8000909c]:sd t6, 64(fp)
[0x800090a0]:sd a2, 72(fp)
[0x800090a4]:lui s1, 3
[0x800090a8]:addiw s1, s1, 2048
[0x800090ac]:add a1, a1, s1
[0x800090b0]:ld t5, 128(a1)
[0x800090b4]:sub a1, a1, s1
[0x800090b8]:lui s1, 3
[0x800090bc]:addiw s1, s1, 2048
[0x800090c0]:add a1, a1, s1
[0x800090c4]:ld t4, 136(a1)
[0x800090c8]:sub a1, a1, s1
[0x800090cc]:addi s1, zero, 0
[0x800090d0]:csrrw zero, fcsr, s1
[0x800090d4]:fmax.s t6, t5, t4

[0x800090d4]:fmax.s t6, t5, t4
[0x800090d8]:csrrs a2, fcsr, zero
[0x800090dc]:sd t6, 80(fp)
[0x800090e0]:sd a2, 88(fp)
[0x800090e4]:lui s1, 3
[0x800090e8]:addiw s1, s1, 2048
[0x800090ec]:add a1, a1, s1
[0x800090f0]:ld t5, 144(a1)
[0x800090f4]:sub a1, a1, s1
[0x800090f8]:lui s1, 3
[0x800090fc]:addiw s1, s1, 2048
[0x80009100]:add a1, a1, s1
[0x80009104]:ld t4, 152(a1)
[0x80009108]:sub a1, a1, s1
[0x8000910c]:addi s1, zero, 0
[0x80009110]:csrrw zero, fcsr, s1
[0x80009114]:fmax.s t6, t5, t4

[0x80009114]:fmax.s t6, t5, t4
[0x80009118]:csrrs a2, fcsr, zero
[0x8000911c]:sd t6, 96(fp)
[0x80009120]:sd a2, 104(fp)
[0x80009124]:lui s1, 3
[0x80009128]:addiw s1, s1, 2048
[0x8000912c]:add a1, a1, s1
[0x80009130]:ld t5, 160(a1)
[0x80009134]:sub a1, a1, s1
[0x80009138]:lui s1, 3
[0x8000913c]:addiw s1, s1, 2048
[0x80009140]:add a1, a1, s1
[0x80009144]:ld t4, 168(a1)
[0x80009148]:sub a1, a1, s1
[0x8000914c]:addi s1, zero, 0
[0x80009150]:csrrw zero, fcsr, s1
[0x80009154]:fmax.s t6, t5, t4

[0x80009154]:fmax.s t6, t5, t4
[0x80009158]:csrrs a2, fcsr, zero
[0x8000915c]:sd t6, 112(fp)
[0x80009160]:sd a2, 120(fp)
[0x80009164]:lui s1, 3
[0x80009168]:addiw s1, s1, 2048
[0x8000916c]:add a1, a1, s1
[0x80009170]:ld t5, 176(a1)
[0x80009174]:sub a1, a1, s1
[0x80009178]:lui s1, 3
[0x8000917c]:addiw s1, s1, 2048
[0x80009180]:add a1, a1, s1
[0x80009184]:ld t4, 184(a1)
[0x80009188]:sub a1, a1, s1
[0x8000918c]:addi s1, zero, 0
[0x80009190]:csrrw zero, fcsr, s1
[0x80009194]:fmax.s t6, t5, t4

[0x80009194]:fmax.s t6, t5, t4
[0x80009198]:csrrs a2, fcsr, zero
[0x8000919c]:sd t6, 128(fp)
[0x800091a0]:sd a2, 136(fp)
[0x800091a4]:lui s1, 3
[0x800091a8]:addiw s1, s1, 2048
[0x800091ac]:add a1, a1, s1
[0x800091b0]:ld t5, 192(a1)
[0x800091b4]:sub a1, a1, s1
[0x800091b8]:lui s1, 3
[0x800091bc]:addiw s1, s1, 2048
[0x800091c0]:add a1, a1, s1
[0x800091c4]:ld t4, 200(a1)
[0x800091c8]:sub a1, a1, s1
[0x800091cc]:addi s1, zero, 0
[0x800091d0]:csrrw zero, fcsr, s1
[0x800091d4]:fmax.s t6, t5, t4

[0x800091d4]:fmax.s t6, t5, t4
[0x800091d8]:csrrs a2, fcsr, zero
[0x800091dc]:sd t6, 144(fp)
[0x800091e0]:sd a2, 152(fp)
[0x800091e4]:lui s1, 3
[0x800091e8]:addiw s1, s1, 2048
[0x800091ec]:add a1, a1, s1
[0x800091f0]:ld t5, 208(a1)
[0x800091f4]:sub a1, a1, s1
[0x800091f8]:lui s1, 3
[0x800091fc]:addiw s1, s1, 2048
[0x80009200]:add a1, a1, s1
[0x80009204]:ld t4, 216(a1)
[0x80009208]:sub a1, a1, s1
[0x8000920c]:addi s1, zero, 0
[0x80009210]:csrrw zero, fcsr, s1
[0x80009214]:fmax.s t6, t5, t4

[0x80009214]:fmax.s t6, t5, t4
[0x80009218]:csrrs a2, fcsr, zero
[0x8000921c]:sd t6, 160(fp)
[0x80009220]:sd a2, 168(fp)
[0x80009224]:lui s1, 3
[0x80009228]:addiw s1, s1, 2048
[0x8000922c]:add a1, a1, s1
[0x80009230]:ld t5, 224(a1)
[0x80009234]:sub a1, a1, s1
[0x80009238]:lui s1, 3
[0x8000923c]:addiw s1, s1, 2048
[0x80009240]:add a1, a1, s1
[0x80009244]:ld t4, 232(a1)
[0x80009248]:sub a1, a1, s1
[0x8000924c]:addi s1, zero, 0
[0x80009250]:csrrw zero, fcsr, s1
[0x80009254]:fmax.s t6, t5, t4

[0x80009254]:fmax.s t6, t5, t4
[0x80009258]:csrrs a2, fcsr, zero
[0x8000925c]:sd t6, 176(fp)
[0x80009260]:sd a2, 184(fp)
[0x80009264]:lui s1, 3
[0x80009268]:addiw s1, s1, 2048
[0x8000926c]:add a1, a1, s1
[0x80009270]:ld t5, 240(a1)
[0x80009274]:sub a1, a1, s1
[0x80009278]:lui s1, 3
[0x8000927c]:addiw s1, s1, 2048
[0x80009280]:add a1, a1, s1
[0x80009284]:ld t4, 248(a1)
[0x80009288]:sub a1, a1, s1
[0x8000928c]:addi s1, zero, 0
[0x80009290]:csrrw zero, fcsr, s1
[0x80009294]:fmax.s t6, t5, t4

[0x80009294]:fmax.s t6, t5, t4
[0x80009298]:csrrs a2, fcsr, zero
[0x8000929c]:sd t6, 192(fp)
[0x800092a0]:sd a2, 200(fp)
[0x800092a4]:lui s1, 3
[0x800092a8]:addiw s1, s1, 2048
[0x800092ac]:add a1, a1, s1
[0x800092b0]:ld t5, 256(a1)
[0x800092b4]:sub a1, a1, s1
[0x800092b8]:lui s1, 3
[0x800092bc]:addiw s1, s1, 2048
[0x800092c0]:add a1, a1, s1
[0x800092c4]:ld t4, 264(a1)
[0x800092c8]:sub a1, a1, s1
[0x800092cc]:addi s1, zero, 0
[0x800092d0]:csrrw zero, fcsr, s1
[0x800092d4]:fmax.s t6, t5, t4

[0x800092d4]:fmax.s t6, t5, t4
[0x800092d8]:csrrs a2, fcsr, zero
[0x800092dc]:sd t6, 208(fp)
[0x800092e0]:sd a2, 216(fp)
[0x800092e4]:lui s1, 3
[0x800092e8]:addiw s1, s1, 2048
[0x800092ec]:add a1, a1, s1
[0x800092f0]:ld t5, 272(a1)
[0x800092f4]:sub a1, a1, s1
[0x800092f8]:lui s1, 3
[0x800092fc]:addiw s1, s1, 2048
[0x80009300]:add a1, a1, s1
[0x80009304]:ld t4, 280(a1)
[0x80009308]:sub a1, a1, s1
[0x8000930c]:addi s1, zero, 0
[0x80009310]:csrrw zero, fcsr, s1
[0x80009314]:fmax.s t6, t5, t4

[0x80009314]:fmax.s t6, t5, t4
[0x80009318]:csrrs a2, fcsr, zero
[0x8000931c]:sd t6, 224(fp)
[0x80009320]:sd a2, 232(fp)
[0x80009324]:lui s1, 3
[0x80009328]:addiw s1, s1, 2048
[0x8000932c]:add a1, a1, s1
[0x80009330]:ld t5, 288(a1)
[0x80009334]:sub a1, a1, s1
[0x80009338]:lui s1, 3
[0x8000933c]:addiw s1, s1, 2048
[0x80009340]:add a1, a1, s1
[0x80009344]:ld t4, 296(a1)
[0x80009348]:sub a1, a1, s1
[0x8000934c]:addi s1, zero, 0
[0x80009350]:csrrw zero, fcsr, s1
[0x80009354]:fmax.s t6, t5, t4

[0x80009354]:fmax.s t6, t5, t4
[0x80009358]:csrrs a2, fcsr, zero
[0x8000935c]:sd t6, 240(fp)
[0x80009360]:sd a2, 248(fp)
[0x80009364]:lui s1, 3
[0x80009368]:addiw s1, s1, 2048
[0x8000936c]:add a1, a1, s1
[0x80009370]:ld t5, 304(a1)
[0x80009374]:sub a1, a1, s1
[0x80009378]:lui s1, 3
[0x8000937c]:addiw s1, s1, 2048
[0x80009380]:add a1, a1, s1
[0x80009384]:ld t4, 312(a1)
[0x80009388]:sub a1, a1, s1
[0x8000938c]:addi s1, zero, 0
[0x80009390]:csrrw zero, fcsr, s1
[0x80009394]:fmax.s t6, t5, t4

[0x80009394]:fmax.s t6, t5, t4
[0x80009398]:csrrs a2, fcsr, zero
[0x8000939c]:sd t6, 256(fp)
[0x800093a0]:sd a2, 264(fp)
[0x800093a4]:lui s1, 3
[0x800093a8]:addiw s1, s1, 2048
[0x800093ac]:add a1, a1, s1
[0x800093b0]:ld t5, 320(a1)
[0x800093b4]:sub a1, a1, s1
[0x800093b8]:lui s1, 3
[0x800093bc]:addiw s1, s1, 2048
[0x800093c0]:add a1, a1, s1
[0x800093c4]:ld t4, 328(a1)
[0x800093c8]:sub a1, a1, s1
[0x800093cc]:addi s1, zero, 0
[0x800093d0]:csrrw zero, fcsr, s1
[0x800093d4]:fmax.s t6, t5, t4

[0x800093d4]:fmax.s t6, t5, t4
[0x800093d8]:csrrs a2, fcsr, zero
[0x800093dc]:sd t6, 272(fp)
[0x800093e0]:sd a2, 280(fp)
[0x800093e4]:lui s1, 3
[0x800093e8]:addiw s1, s1, 2048
[0x800093ec]:add a1, a1, s1
[0x800093f0]:ld t5, 336(a1)
[0x800093f4]:sub a1, a1, s1
[0x800093f8]:lui s1, 3
[0x800093fc]:addiw s1, s1, 2048
[0x80009400]:add a1, a1, s1
[0x80009404]:ld t4, 344(a1)
[0x80009408]:sub a1, a1, s1
[0x8000940c]:addi s1, zero, 0
[0x80009410]:csrrw zero, fcsr, s1
[0x80009414]:fmax.s t6, t5, t4

[0x80009414]:fmax.s t6, t5, t4
[0x80009418]:csrrs a2, fcsr, zero
[0x8000941c]:sd t6, 288(fp)
[0x80009420]:sd a2, 296(fp)
[0x80009424]:lui s1, 3
[0x80009428]:addiw s1, s1, 2048
[0x8000942c]:add a1, a1, s1
[0x80009430]:ld t5, 352(a1)
[0x80009434]:sub a1, a1, s1
[0x80009438]:lui s1, 3
[0x8000943c]:addiw s1, s1, 2048
[0x80009440]:add a1, a1, s1
[0x80009444]:ld t4, 360(a1)
[0x80009448]:sub a1, a1, s1
[0x8000944c]:addi s1, zero, 0
[0x80009450]:csrrw zero, fcsr, s1
[0x80009454]:fmax.s t6, t5, t4

[0x80009454]:fmax.s t6, t5, t4
[0x80009458]:csrrs a2, fcsr, zero
[0x8000945c]:sd t6, 304(fp)
[0x80009460]:sd a2, 312(fp)
[0x80009464]:lui s1, 3
[0x80009468]:addiw s1, s1, 2048
[0x8000946c]:add a1, a1, s1
[0x80009470]:ld t5, 368(a1)
[0x80009474]:sub a1, a1, s1
[0x80009478]:lui s1, 3
[0x8000947c]:addiw s1, s1, 2048
[0x80009480]:add a1, a1, s1
[0x80009484]:ld t4, 376(a1)
[0x80009488]:sub a1, a1, s1
[0x8000948c]:addi s1, zero, 0
[0x80009490]:csrrw zero, fcsr, s1
[0x80009494]:fmax.s t6, t5, t4

[0x80009494]:fmax.s t6, t5, t4
[0x80009498]:csrrs a2, fcsr, zero
[0x8000949c]:sd t6, 320(fp)
[0x800094a0]:sd a2, 328(fp)
[0x800094a4]:lui s1, 3
[0x800094a8]:addiw s1, s1, 2048
[0x800094ac]:add a1, a1, s1
[0x800094b0]:ld t5, 384(a1)
[0x800094b4]:sub a1, a1, s1
[0x800094b8]:lui s1, 3
[0x800094bc]:addiw s1, s1, 2048
[0x800094c0]:add a1, a1, s1
[0x800094c4]:ld t4, 392(a1)
[0x800094c8]:sub a1, a1, s1
[0x800094cc]:addi s1, zero, 0
[0x800094d0]:csrrw zero, fcsr, s1
[0x800094d4]:fmax.s t6, t5, t4

[0x800094d4]:fmax.s t6, t5, t4
[0x800094d8]:csrrs a2, fcsr, zero
[0x800094dc]:sd t6, 336(fp)
[0x800094e0]:sd a2, 344(fp)
[0x800094e4]:lui s1, 3
[0x800094e8]:addiw s1, s1, 2048
[0x800094ec]:add a1, a1, s1
[0x800094f0]:ld t5, 400(a1)
[0x800094f4]:sub a1, a1, s1
[0x800094f8]:lui s1, 3
[0x800094fc]:addiw s1, s1, 2048
[0x80009500]:add a1, a1, s1
[0x80009504]:ld t4, 408(a1)
[0x80009508]:sub a1, a1, s1
[0x8000950c]:addi s1, zero, 0
[0x80009510]:csrrw zero, fcsr, s1
[0x80009514]:fmax.s t6, t5, t4

[0x80009514]:fmax.s t6, t5, t4
[0x80009518]:csrrs a2, fcsr, zero
[0x8000951c]:sd t6, 352(fp)
[0x80009520]:sd a2, 360(fp)
[0x80009524]:lui s1, 3
[0x80009528]:addiw s1, s1, 2048
[0x8000952c]:add a1, a1, s1
[0x80009530]:ld t5, 416(a1)
[0x80009534]:sub a1, a1, s1
[0x80009538]:lui s1, 3
[0x8000953c]:addiw s1, s1, 2048
[0x80009540]:add a1, a1, s1
[0x80009544]:ld t4, 424(a1)
[0x80009548]:sub a1, a1, s1
[0x8000954c]:addi s1, zero, 0
[0x80009550]:csrrw zero, fcsr, s1
[0x80009554]:fmax.s t6, t5, t4

[0x80009554]:fmax.s t6, t5, t4
[0x80009558]:csrrs a2, fcsr, zero
[0x8000955c]:sd t6, 368(fp)
[0x80009560]:sd a2, 376(fp)
[0x80009564]:lui s1, 3
[0x80009568]:addiw s1, s1, 2048
[0x8000956c]:add a1, a1, s1
[0x80009570]:ld t5, 432(a1)
[0x80009574]:sub a1, a1, s1
[0x80009578]:lui s1, 3
[0x8000957c]:addiw s1, s1, 2048
[0x80009580]:add a1, a1, s1
[0x80009584]:ld t4, 440(a1)
[0x80009588]:sub a1, a1, s1
[0x8000958c]:addi s1, zero, 0
[0x80009590]:csrrw zero, fcsr, s1
[0x80009594]:fmax.s t6, t5, t4

[0x80009594]:fmax.s t6, t5, t4
[0x80009598]:csrrs a2, fcsr, zero
[0x8000959c]:sd t6, 384(fp)
[0x800095a0]:sd a2, 392(fp)
[0x800095a4]:lui s1, 3
[0x800095a8]:addiw s1, s1, 2048
[0x800095ac]:add a1, a1, s1
[0x800095b0]:ld t5, 448(a1)
[0x800095b4]:sub a1, a1, s1
[0x800095b8]:lui s1, 3
[0x800095bc]:addiw s1, s1, 2048
[0x800095c0]:add a1, a1, s1
[0x800095c4]:ld t4, 456(a1)
[0x800095c8]:sub a1, a1, s1
[0x800095cc]:addi s1, zero, 0
[0x800095d0]:csrrw zero, fcsr, s1
[0x800095d4]:fmax.s t6, t5, t4

[0x800095d4]:fmax.s t6, t5, t4
[0x800095d8]:csrrs a2, fcsr, zero
[0x800095dc]:sd t6, 400(fp)
[0x800095e0]:sd a2, 408(fp)
[0x800095e4]:lui s1, 3
[0x800095e8]:addiw s1, s1, 2048
[0x800095ec]:add a1, a1, s1
[0x800095f0]:ld t5, 464(a1)
[0x800095f4]:sub a1, a1, s1
[0x800095f8]:lui s1, 3
[0x800095fc]:addiw s1, s1, 2048
[0x80009600]:add a1, a1, s1
[0x80009604]:ld t4, 472(a1)
[0x80009608]:sub a1, a1, s1
[0x8000960c]:addi s1, zero, 0
[0x80009610]:csrrw zero, fcsr, s1
[0x80009614]:fmax.s t6, t5, t4

[0x80009614]:fmax.s t6, t5, t4
[0x80009618]:csrrs a2, fcsr, zero
[0x8000961c]:sd t6, 416(fp)
[0x80009620]:sd a2, 424(fp)
[0x80009624]:lui s1, 3
[0x80009628]:addiw s1, s1, 2048
[0x8000962c]:add a1, a1, s1
[0x80009630]:ld t5, 480(a1)
[0x80009634]:sub a1, a1, s1
[0x80009638]:lui s1, 3
[0x8000963c]:addiw s1, s1, 2048
[0x80009640]:add a1, a1, s1
[0x80009644]:ld t4, 488(a1)
[0x80009648]:sub a1, a1, s1
[0x8000964c]:addi s1, zero, 0
[0x80009650]:csrrw zero, fcsr, s1
[0x80009654]:fmax.s t6, t5, t4

[0x80009654]:fmax.s t6, t5, t4
[0x80009658]:csrrs a2, fcsr, zero
[0x8000965c]:sd t6, 432(fp)
[0x80009660]:sd a2, 440(fp)
[0x80009664]:lui s1, 3
[0x80009668]:addiw s1, s1, 2048
[0x8000966c]:add a1, a1, s1
[0x80009670]:ld t5, 496(a1)
[0x80009674]:sub a1, a1, s1
[0x80009678]:lui s1, 3
[0x8000967c]:addiw s1, s1, 2048
[0x80009680]:add a1, a1, s1
[0x80009684]:ld t4, 504(a1)
[0x80009688]:sub a1, a1, s1
[0x8000968c]:addi s1, zero, 0
[0x80009690]:csrrw zero, fcsr, s1
[0x80009694]:fmax.s t6, t5, t4

[0x80009694]:fmax.s t6, t5, t4
[0x80009698]:csrrs a2, fcsr, zero
[0x8000969c]:sd t6, 448(fp)
[0x800096a0]:sd a2, 456(fp)
[0x800096a4]:lui s1, 3
[0x800096a8]:addiw s1, s1, 2048
[0x800096ac]:add a1, a1, s1
[0x800096b0]:ld t5, 512(a1)
[0x800096b4]:sub a1, a1, s1
[0x800096b8]:lui s1, 3
[0x800096bc]:addiw s1, s1, 2048
[0x800096c0]:add a1, a1, s1
[0x800096c4]:ld t4, 520(a1)
[0x800096c8]:sub a1, a1, s1
[0x800096cc]:addi s1, zero, 0
[0x800096d0]:csrrw zero, fcsr, s1
[0x800096d4]:fmax.s t6, t5, t4

[0x800096d4]:fmax.s t6, t5, t4
[0x800096d8]:csrrs a2, fcsr, zero
[0x800096dc]:sd t6, 464(fp)
[0x800096e0]:sd a2, 472(fp)
[0x800096e4]:lui s1, 3
[0x800096e8]:addiw s1, s1, 2048
[0x800096ec]:add a1, a1, s1
[0x800096f0]:ld t5, 528(a1)
[0x800096f4]:sub a1, a1, s1
[0x800096f8]:lui s1, 3
[0x800096fc]:addiw s1, s1, 2048
[0x80009700]:add a1, a1, s1
[0x80009704]:ld t4, 536(a1)
[0x80009708]:sub a1, a1, s1
[0x8000970c]:addi s1, zero, 0
[0x80009710]:csrrw zero, fcsr, s1
[0x80009714]:fmax.s t6, t5, t4

[0x80009714]:fmax.s t6, t5, t4
[0x80009718]:csrrs a2, fcsr, zero
[0x8000971c]:sd t6, 480(fp)
[0x80009720]:sd a2, 488(fp)
[0x80009724]:lui s1, 3
[0x80009728]:addiw s1, s1, 2048
[0x8000972c]:add a1, a1, s1
[0x80009730]:ld t5, 544(a1)
[0x80009734]:sub a1, a1, s1
[0x80009738]:lui s1, 3
[0x8000973c]:addiw s1, s1, 2048
[0x80009740]:add a1, a1, s1
[0x80009744]:ld t4, 552(a1)
[0x80009748]:sub a1, a1, s1
[0x8000974c]:addi s1, zero, 0
[0x80009750]:csrrw zero, fcsr, s1
[0x80009754]:fmax.s t6, t5, t4

[0x80009754]:fmax.s t6, t5, t4
[0x80009758]:csrrs a2, fcsr, zero
[0x8000975c]:sd t6, 496(fp)
[0x80009760]:sd a2, 504(fp)
[0x80009764]:lui s1, 3
[0x80009768]:addiw s1, s1, 2048
[0x8000976c]:add a1, a1, s1
[0x80009770]:ld t5, 560(a1)
[0x80009774]:sub a1, a1, s1
[0x80009778]:lui s1, 3
[0x8000977c]:addiw s1, s1, 2048
[0x80009780]:add a1, a1, s1
[0x80009784]:ld t4, 568(a1)
[0x80009788]:sub a1, a1, s1
[0x8000978c]:addi s1, zero, 0
[0x80009790]:csrrw zero, fcsr, s1
[0x80009794]:fmax.s t6, t5, t4

[0x80009794]:fmax.s t6, t5, t4
[0x80009798]:csrrs a2, fcsr, zero
[0x8000979c]:sd t6, 512(fp)
[0x800097a0]:sd a2, 520(fp)
[0x800097a4]:lui s1, 3
[0x800097a8]:addiw s1, s1, 2048
[0x800097ac]:add a1, a1, s1
[0x800097b0]:ld t5, 576(a1)
[0x800097b4]:sub a1, a1, s1
[0x800097b8]:lui s1, 3
[0x800097bc]:addiw s1, s1, 2048
[0x800097c0]:add a1, a1, s1
[0x800097c4]:ld t4, 584(a1)
[0x800097c8]:sub a1, a1, s1
[0x800097cc]:addi s1, zero, 0
[0x800097d0]:csrrw zero, fcsr, s1
[0x800097d4]:fmax.s t6, t5, t4

[0x800097d4]:fmax.s t6, t5, t4
[0x800097d8]:csrrs a2, fcsr, zero
[0x800097dc]:sd t6, 528(fp)
[0x800097e0]:sd a2, 536(fp)
[0x800097e4]:lui s1, 3
[0x800097e8]:addiw s1, s1, 2048
[0x800097ec]:add a1, a1, s1
[0x800097f0]:ld t5, 592(a1)
[0x800097f4]:sub a1, a1, s1
[0x800097f8]:lui s1, 3
[0x800097fc]:addiw s1, s1, 2048
[0x80009800]:add a1, a1, s1
[0x80009804]:ld t4, 600(a1)
[0x80009808]:sub a1, a1, s1
[0x8000980c]:addi s1, zero, 0
[0x80009810]:csrrw zero, fcsr, s1
[0x80009814]:fmax.s t6, t5, t4

[0x80009814]:fmax.s t6, t5, t4
[0x80009818]:csrrs a2, fcsr, zero
[0x8000981c]:sd t6, 544(fp)
[0x80009820]:sd a2, 552(fp)
[0x80009824]:lui s1, 3
[0x80009828]:addiw s1, s1, 2048
[0x8000982c]:add a1, a1, s1
[0x80009830]:ld t5, 608(a1)
[0x80009834]:sub a1, a1, s1
[0x80009838]:lui s1, 3
[0x8000983c]:addiw s1, s1, 2048
[0x80009840]:add a1, a1, s1
[0x80009844]:ld t4, 616(a1)
[0x80009848]:sub a1, a1, s1
[0x8000984c]:addi s1, zero, 0
[0x80009850]:csrrw zero, fcsr, s1
[0x80009854]:fmax.s t6, t5, t4

[0x80009854]:fmax.s t6, t5, t4
[0x80009858]:csrrs a2, fcsr, zero
[0x8000985c]:sd t6, 560(fp)
[0x80009860]:sd a2, 568(fp)
[0x80009864]:lui s1, 3
[0x80009868]:addiw s1, s1, 2048
[0x8000986c]:add a1, a1, s1
[0x80009870]:ld t5, 624(a1)
[0x80009874]:sub a1, a1, s1
[0x80009878]:lui s1, 3
[0x8000987c]:addiw s1, s1, 2048
[0x80009880]:add a1, a1, s1
[0x80009884]:ld t4, 632(a1)
[0x80009888]:sub a1, a1, s1
[0x8000988c]:addi s1, zero, 0
[0x80009890]:csrrw zero, fcsr, s1
[0x80009894]:fmax.s t6, t5, t4

[0x80009894]:fmax.s t6, t5, t4
[0x80009898]:csrrs a2, fcsr, zero
[0x8000989c]:sd t6, 576(fp)
[0x800098a0]:sd a2, 584(fp)
[0x800098a4]:lui s1, 3
[0x800098a8]:addiw s1, s1, 2048
[0x800098ac]:add a1, a1, s1
[0x800098b0]:ld t5, 640(a1)
[0x800098b4]:sub a1, a1, s1
[0x800098b8]:lui s1, 3
[0x800098bc]:addiw s1, s1, 2048
[0x800098c0]:add a1, a1, s1
[0x800098c4]:ld t4, 648(a1)
[0x800098c8]:sub a1, a1, s1
[0x800098cc]:addi s1, zero, 0
[0x800098d0]:csrrw zero, fcsr, s1
[0x800098d4]:fmax.s t6, t5, t4

[0x800098d4]:fmax.s t6, t5, t4
[0x800098d8]:csrrs a2, fcsr, zero
[0x800098dc]:sd t6, 592(fp)
[0x800098e0]:sd a2, 600(fp)
[0x800098e4]:lui s1, 3
[0x800098e8]:addiw s1, s1, 2048
[0x800098ec]:add a1, a1, s1
[0x800098f0]:ld t5, 656(a1)
[0x800098f4]:sub a1, a1, s1
[0x800098f8]:lui s1, 3
[0x800098fc]:addiw s1, s1, 2048
[0x80009900]:add a1, a1, s1
[0x80009904]:ld t4, 664(a1)
[0x80009908]:sub a1, a1, s1
[0x8000990c]:addi s1, zero, 0
[0x80009910]:csrrw zero, fcsr, s1
[0x80009914]:fmax.s t6, t5, t4

[0x80009914]:fmax.s t6, t5, t4
[0x80009918]:csrrs a2, fcsr, zero
[0x8000991c]:sd t6, 608(fp)
[0x80009920]:sd a2, 616(fp)
[0x80009924]:lui s1, 3
[0x80009928]:addiw s1, s1, 2048
[0x8000992c]:add a1, a1, s1
[0x80009930]:ld t5, 672(a1)
[0x80009934]:sub a1, a1, s1
[0x80009938]:lui s1, 3
[0x8000993c]:addiw s1, s1, 2048
[0x80009940]:add a1, a1, s1
[0x80009944]:ld t4, 680(a1)
[0x80009948]:sub a1, a1, s1
[0x8000994c]:addi s1, zero, 0
[0x80009950]:csrrw zero, fcsr, s1
[0x80009954]:fmax.s t6, t5, t4

[0x80009954]:fmax.s t6, t5, t4
[0x80009958]:csrrs a2, fcsr, zero
[0x8000995c]:sd t6, 624(fp)
[0x80009960]:sd a2, 632(fp)
[0x80009964]:lui s1, 3
[0x80009968]:addiw s1, s1, 2048
[0x8000996c]:add a1, a1, s1
[0x80009970]:ld t5, 688(a1)
[0x80009974]:sub a1, a1, s1
[0x80009978]:lui s1, 3
[0x8000997c]:addiw s1, s1, 2048
[0x80009980]:add a1, a1, s1
[0x80009984]:ld t4, 696(a1)
[0x80009988]:sub a1, a1, s1
[0x8000998c]:addi s1, zero, 0
[0x80009990]:csrrw zero, fcsr, s1
[0x80009994]:fmax.s t6, t5, t4

[0x80009994]:fmax.s t6, t5, t4
[0x80009998]:csrrs a2, fcsr, zero
[0x8000999c]:sd t6, 640(fp)
[0x800099a0]:sd a2, 648(fp)
[0x800099a4]:lui s1, 3
[0x800099a8]:addiw s1, s1, 2048
[0x800099ac]:add a1, a1, s1
[0x800099b0]:ld t5, 704(a1)
[0x800099b4]:sub a1, a1, s1
[0x800099b8]:lui s1, 3
[0x800099bc]:addiw s1, s1, 2048
[0x800099c0]:add a1, a1, s1
[0x800099c4]:ld t4, 712(a1)
[0x800099c8]:sub a1, a1, s1
[0x800099cc]:addi s1, zero, 0
[0x800099d0]:csrrw zero, fcsr, s1
[0x800099d4]:fmax.s t6, t5, t4

[0x800099d4]:fmax.s t6, t5, t4
[0x800099d8]:csrrs a2, fcsr, zero
[0x800099dc]:sd t6, 656(fp)
[0x800099e0]:sd a2, 664(fp)
[0x800099e4]:lui s1, 3
[0x800099e8]:addiw s1, s1, 2048
[0x800099ec]:add a1, a1, s1
[0x800099f0]:ld t5, 720(a1)
[0x800099f4]:sub a1, a1, s1
[0x800099f8]:lui s1, 3
[0x800099fc]:addiw s1, s1, 2048
[0x80009a00]:add a1, a1, s1
[0x80009a04]:ld t4, 728(a1)
[0x80009a08]:sub a1, a1, s1
[0x80009a0c]:addi s1, zero, 0
[0x80009a10]:csrrw zero, fcsr, s1
[0x80009a14]:fmax.s t6, t5, t4

[0x80009a14]:fmax.s t6, t5, t4
[0x80009a18]:csrrs a2, fcsr, zero
[0x80009a1c]:sd t6, 672(fp)
[0x80009a20]:sd a2, 680(fp)
[0x80009a24]:lui s1, 3
[0x80009a28]:addiw s1, s1, 2048
[0x80009a2c]:add a1, a1, s1
[0x80009a30]:ld t5, 736(a1)
[0x80009a34]:sub a1, a1, s1
[0x80009a38]:lui s1, 3
[0x80009a3c]:addiw s1, s1, 2048
[0x80009a40]:add a1, a1, s1
[0x80009a44]:ld t4, 744(a1)
[0x80009a48]:sub a1, a1, s1
[0x80009a4c]:addi s1, zero, 0
[0x80009a50]:csrrw zero, fcsr, s1
[0x80009a54]:fmax.s t6, t5, t4

[0x80009a54]:fmax.s t6, t5, t4
[0x80009a58]:csrrs a2, fcsr, zero
[0x80009a5c]:sd t6, 688(fp)
[0x80009a60]:sd a2, 696(fp)
[0x80009a64]:lui s1, 3
[0x80009a68]:addiw s1, s1, 2048
[0x80009a6c]:add a1, a1, s1
[0x80009a70]:ld t5, 752(a1)
[0x80009a74]:sub a1, a1, s1
[0x80009a78]:lui s1, 3
[0x80009a7c]:addiw s1, s1, 2048
[0x80009a80]:add a1, a1, s1
[0x80009a84]:ld t4, 760(a1)
[0x80009a88]:sub a1, a1, s1
[0x80009a8c]:addi s1, zero, 0
[0x80009a90]:csrrw zero, fcsr, s1
[0x80009a94]:fmax.s t6, t5, t4

[0x80009a94]:fmax.s t6, t5, t4
[0x80009a98]:csrrs a2, fcsr, zero
[0x80009a9c]:sd t6, 704(fp)
[0x80009aa0]:sd a2, 712(fp)
[0x80009aa4]:lui s1, 3
[0x80009aa8]:addiw s1, s1, 2048
[0x80009aac]:add a1, a1, s1
[0x80009ab0]:ld t5, 768(a1)
[0x80009ab4]:sub a1, a1, s1
[0x80009ab8]:lui s1, 3
[0x80009abc]:addiw s1, s1, 2048
[0x80009ac0]:add a1, a1, s1
[0x80009ac4]:ld t4, 776(a1)
[0x80009ac8]:sub a1, a1, s1
[0x80009acc]:addi s1, zero, 0
[0x80009ad0]:csrrw zero, fcsr, s1
[0x80009ad4]:fmax.s t6, t5, t4

[0x80009ad4]:fmax.s t6, t5, t4
[0x80009ad8]:csrrs a2, fcsr, zero
[0x80009adc]:sd t6, 720(fp)
[0x80009ae0]:sd a2, 728(fp)
[0x80009ae4]:lui s1, 3
[0x80009ae8]:addiw s1, s1, 2048
[0x80009aec]:add a1, a1, s1
[0x80009af0]:ld t5, 784(a1)
[0x80009af4]:sub a1, a1, s1
[0x80009af8]:lui s1, 3
[0x80009afc]:addiw s1, s1, 2048
[0x80009b00]:add a1, a1, s1
[0x80009b04]:ld t4, 792(a1)
[0x80009b08]:sub a1, a1, s1
[0x80009b0c]:addi s1, zero, 0
[0x80009b10]:csrrw zero, fcsr, s1
[0x80009b14]:fmax.s t6, t5, t4

[0x80009b14]:fmax.s t6, t5, t4
[0x80009b18]:csrrs a2, fcsr, zero
[0x80009b1c]:sd t6, 736(fp)
[0x80009b20]:sd a2, 744(fp)
[0x80009b24]:lui s1, 3
[0x80009b28]:addiw s1, s1, 2048
[0x80009b2c]:add a1, a1, s1
[0x80009b30]:ld t5, 800(a1)
[0x80009b34]:sub a1, a1, s1
[0x80009b38]:lui s1, 3
[0x80009b3c]:addiw s1, s1, 2048
[0x80009b40]:add a1, a1, s1
[0x80009b44]:ld t4, 808(a1)
[0x80009b48]:sub a1, a1, s1
[0x80009b4c]:addi s1, zero, 0
[0x80009b50]:csrrw zero, fcsr, s1
[0x80009b54]:fmax.s t6, t5, t4

[0x80009b54]:fmax.s t6, t5, t4
[0x80009b58]:csrrs a2, fcsr, zero
[0x80009b5c]:sd t6, 752(fp)
[0x80009b60]:sd a2, 760(fp)
[0x80009b64]:lui s1, 3
[0x80009b68]:addiw s1, s1, 2048
[0x80009b6c]:add a1, a1, s1
[0x80009b70]:ld t5, 816(a1)
[0x80009b74]:sub a1, a1, s1
[0x80009b78]:lui s1, 3
[0x80009b7c]:addiw s1, s1, 2048
[0x80009b80]:add a1, a1, s1
[0x80009b84]:ld t4, 824(a1)
[0x80009b88]:sub a1, a1, s1
[0x80009b8c]:addi s1, zero, 0
[0x80009b90]:csrrw zero, fcsr, s1
[0x80009b94]:fmax.s t6, t5, t4

[0x80009b94]:fmax.s t6, t5, t4
[0x80009b98]:csrrs a2, fcsr, zero
[0x80009b9c]:sd t6, 768(fp)
[0x80009ba0]:sd a2, 776(fp)
[0x80009ba4]:lui s1, 3
[0x80009ba8]:addiw s1, s1, 2048
[0x80009bac]:add a1, a1, s1
[0x80009bb0]:ld t5, 832(a1)
[0x80009bb4]:sub a1, a1, s1
[0x80009bb8]:lui s1, 3
[0x80009bbc]:addiw s1, s1, 2048
[0x80009bc0]:add a1, a1, s1
[0x80009bc4]:ld t4, 840(a1)
[0x80009bc8]:sub a1, a1, s1
[0x80009bcc]:addi s1, zero, 0
[0x80009bd0]:csrrw zero, fcsr, s1
[0x80009bd4]:fmax.s t6, t5, t4

[0x80009bd4]:fmax.s t6, t5, t4
[0x80009bd8]:csrrs a2, fcsr, zero
[0x80009bdc]:sd t6, 784(fp)
[0x80009be0]:sd a2, 792(fp)
[0x80009be4]:lui s1, 3
[0x80009be8]:addiw s1, s1, 2048
[0x80009bec]:add a1, a1, s1
[0x80009bf0]:ld t5, 848(a1)
[0x80009bf4]:sub a1, a1, s1
[0x80009bf8]:lui s1, 3
[0x80009bfc]:addiw s1, s1, 2048
[0x80009c00]:add a1, a1, s1
[0x80009c04]:ld t4, 856(a1)
[0x80009c08]:sub a1, a1, s1
[0x80009c0c]:addi s1, zero, 0
[0x80009c10]:csrrw zero, fcsr, s1
[0x80009c14]:fmax.s t6, t5, t4

[0x80009c14]:fmax.s t6, t5, t4
[0x80009c18]:csrrs a2, fcsr, zero
[0x80009c1c]:sd t6, 800(fp)
[0x80009c20]:sd a2, 808(fp)
[0x80009c24]:lui s1, 3
[0x80009c28]:addiw s1, s1, 2048
[0x80009c2c]:add a1, a1, s1
[0x80009c30]:ld t5, 864(a1)
[0x80009c34]:sub a1, a1, s1
[0x80009c38]:lui s1, 3
[0x80009c3c]:addiw s1, s1, 2048
[0x80009c40]:add a1, a1, s1
[0x80009c44]:ld t4, 872(a1)
[0x80009c48]:sub a1, a1, s1
[0x80009c4c]:addi s1, zero, 0
[0x80009c50]:csrrw zero, fcsr, s1
[0x80009c54]:fmax.s t6, t5, t4

[0x80009c54]:fmax.s t6, t5, t4
[0x80009c58]:csrrs a2, fcsr, zero
[0x80009c5c]:sd t6, 816(fp)
[0x80009c60]:sd a2, 824(fp)
[0x80009c64]:lui s1, 3
[0x80009c68]:addiw s1, s1, 2048
[0x80009c6c]:add a1, a1, s1
[0x80009c70]:ld t5, 880(a1)
[0x80009c74]:sub a1, a1, s1
[0x80009c78]:lui s1, 3
[0x80009c7c]:addiw s1, s1, 2048
[0x80009c80]:add a1, a1, s1
[0x80009c84]:ld t4, 888(a1)
[0x80009c88]:sub a1, a1, s1
[0x80009c8c]:addi s1, zero, 0
[0x80009c90]:csrrw zero, fcsr, s1
[0x80009c94]:fmax.s t6, t5, t4

[0x80009c94]:fmax.s t6, t5, t4
[0x80009c98]:csrrs a2, fcsr, zero
[0x80009c9c]:sd t6, 832(fp)
[0x80009ca0]:sd a2, 840(fp)
[0x80009ca4]:lui s1, 3
[0x80009ca8]:addiw s1, s1, 2048
[0x80009cac]:add a1, a1, s1
[0x80009cb0]:ld t5, 896(a1)
[0x80009cb4]:sub a1, a1, s1
[0x80009cb8]:lui s1, 3
[0x80009cbc]:addiw s1, s1, 2048
[0x80009cc0]:add a1, a1, s1
[0x80009cc4]:ld t4, 904(a1)
[0x80009cc8]:sub a1, a1, s1
[0x80009ccc]:addi s1, zero, 0
[0x80009cd0]:csrrw zero, fcsr, s1
[0x80009cd4]:fmax.s t6, t5, t4

[0x80009cd4]:fmax.s t6, t5, t4
[0x80009cd8]:csrrs a2, fcsr, zero
[0x80009cdc]:sd t6, 848(fp)
[0x80009ce0]:sd a2, 856(fp)
[0x80009ce4]:lui s1, 3
[0x80009ce8]:addiw s1, s1, 2048
[0x80009cec]:add a1, a1, s1
[0x80009cf0]:ld t5, 912(a1)
[0x80009cf4]:sub a1, a1, s1
[0x80009cf8]:lui s1, 3
[0x80009cfc]:addiw s1, s1, 2048
[0x80009d00]:add a1, a1, s1
[0x80009d04]:ld t4, 920(a1)
[0x80009d08]:sub a1, a1, s1
[0x80009d0c]:addi s1, zero, 0
[0x80009d10]:csrrw zero, fcsr, s1
[0x80009d14]:fmax.s t6, t5, t4

[0x80009d14]:fmax.s t6, t5, t4
[0x80009d18]:csrrs a2, fcsr, zero
[0x80009d1c]:sd t6, 864(fp)
[0x80009d20]:sd a2, 872(fp)
[0x80009d24]:lui s1, 3
[0x80009d28]:addiw s1, s1, 2048
[0x80009d2c]:add a1, a1, s1
[0x80009d30]:ld t5, 928(a1)
[0x80009d34]:sub a1, a1, s1
[0x80009d38]:lui s1, 3
[0x80009d3c]:addiw s1, s1, 2048
[0x80009d40]:add a1, a1, s1
[0x80009d44]:ld t4, 936(a1)
[0x80009d48]:sub a1, a1, s1
[0x80009d4c]:addi s1, zero, 0
[0x80009d50]:csrrw zero, fcsr, s1
[0x80009d54]:fmax.s t6, t5, t4

[0x80009d54]:fmax.s t6, t5, t4
[0x80009d58]:csrrs a2, fcsr, zero
[0x80009d5c]:sd t6, 880(fp)
[0x80009d60]:sd a2, 888(fp)
[0x80009d64]:lui s1, 3
[0x80009d68]:addiw s1, s1, 2048
[0x80009d6c]:add a1, a1, s1
[0x80009d70]:ld t5, 944(a1)
[0x80009d74]:sub a1, a1, s1
[0x80009d78]:lui s1, 3
[0x80009d7c]:addiw s1, s1, 2048
[0x80009d80]:add a1, a1, s1
[0x80009d84]:ld t4, 952(a1)
[0x80009d88]:sub a1, a1, s1
[0x80009d8c]:addi s1, zero, 0
[0x80009d90]:csrrw zero, fcsr, s1
[0x80009d94]:fmax.s t6, t5, t4

[0x80009d94]:fmax.s t6, t5, t4
[0x80009d98]:csrrs a2, fcsr, zero
[0x80009d9c]:sd t6, 896(fp)
[0x80009da0]:sd a2, 904(fp)
[0x80009da4]:lui s1, 3
[0x80009da8]:addiw s1, s1, 2048
[0x80009dac]:add a1, a1, s1
[0x80009db0]:ld t5, 960(a1)
[0x80009db4]:sub a1, a1, s1
[0x80009db8]:lui s1, 3
[0x80009dbc]:addiw s1, s1, 2048
[0x80009dc0]:add a1, a1, s1
[0x80009dc4]:ld t4, 968(a1)
[0x80009dc8]:sub a1, a1, s1
[0x80009dcc]:addi s1, zero, 0
[0x80009dd0]:csrrw zero, fcsr, s1
[0x80009dd4]:fmax.s t6, t5, t4

[0x80009dd4]:fmax.s t6, t5, t4
[0x80009dd8]:csrrs a2, fcsr, zero
[0x80009ddc]:sd t6, 912(fp)
[0x80009de0]:sd a2, 920(fp)
[0x80009de4]:lui s1, 3
[0x80009de8]:addiw s1, s1, 2048
[0x80009dec]:add a1, a1, s1
[0x80009df0]:ld t5, 976(a1)
[0x80009df4]:sub a1, a1, s1
[0x80009df8]:lui s1, 3
[0x80009dfc]:addiw s1, s1, 2048
[0x80009e00]:add a1, a1, s1
[0x80009e04]:ld t4, 984(a1)
[0x80009e08]:sub a1, a1, s1
[0x80009e0c]:addi s1, zero, 0
[0x80009e10]:csrrw zero, fcsr, s1
[0x80009e14]:fmax.s t6, t5, t4

[0x80009e14]:fmax.s t6, t5, t4
[0x80009e18]:csrrs a2, fcsr, zero
[0x80009e1c]:sd t6, 928(fp)
[0x80009e20]:sd a2, 936(fp)
[0x80009e24]:lui s1, 3
[0x80009e28]:addiw s1, s1, 2048
[0x80009e2c]:add a1, a1, s1
[0x80009e30]:ld t5, 992(a1)
[0x80009e34]:sub a1, a1, s1
[0x80009e38]:lui s1, 3
[0x80009e3c]:addiw s1, s1, 2048
[0x80009e40]:add a1, a1, s1
[0x80009e44]:ld t4, 1000(a1)
[0x80009e48]:sub a1, a1, s1
[0x80009e4c]:addi s1, zero, 0
[0x80009e50]:csrrw zero, fcsr, s1
[0x80009e54]:fmax.s t6, t5, t4

[0x80009e54]:fmax.s t6, t5, t4
[0x80009e58]:csrrs a2, fcsr, zero
[0x80009e5c]:sd t6, 944(fp)
[0x80009e60]:sd a2, 952(fp)
[0x80009e64]:lui s1, 3
[0x80009e68]:addiw s1, s1, 2048
[0x80009e6c]:add a1, a1, s1
[0x80009e70]:ld t5, 1008(a1)
[0x80009e74]:sub a1, a1, s1
[0x80009e78]:lui s1, 3
[0x80009e7c]:addiw s1, s1, 2048
[0x80009e80]:add a1, a1, s1
[0x80009e84]:ld t4, 1016(a1)
[0x80009e88]:sub a1, a1, s1
[0x80009e8c]:addi s1, zero, 0
[0x80009e90]:csrrw zero, fcsr, s1
[0x80009e94]:fmax.s t6, t5, t4

[0x80009e94]:fmax.s t6, t5, t4
[0x80009e98]:csrrs a2, fcsr, zero
[0x80009e9c]:sd t6, 960(fp)
[0x80009ea0]:sd a2, 968(fp)
[0x80009ea4]:lui s1, 3
[0x80009ea8]:addiw s1, s1, 2048
[0x80009eac]:add a1, a1, s1
[0x80009eb0]:ld t5, 1024(a1)
[0x80009eb4]:sub a1, a1, s1
[0x80009eb8]:lui s1, 3
[0x80009ebc]:addiw s1, s1, 2048
[0x80009ec0]:add a1, a1, s1
[0x80009ec4]:ld t4, 1032(a1)
[0x80009ec8]:sub a1, a1, s1
[0x80009ecc]:addi s1, zero, 0
[0x80009ed0]:csrrw zero, fcsr, s1
[0x80009ed4]:fmax.s t6, t5, t4

[0x80009ed4]:fmax.s t6, t5, t4
[0x80009ed8]:csrrs a2, fcsr, zero
[0x80009edc]:sd t6, 976(fp)
[0x80009ee0]:sd a2, 984(fp)
[0x80009ee4]:lui s1, 3
[0x80009ee8]:addiw s1, s1, 2048
[0x80009eec]:add a1, a1, s1
[0x80009ef0]:ld t5, 1040(a1)
[0x80009ef4]:sub a1, a1, s1
[0x80009ef8]:lui s1, 3
[0x80009efc]:addiw s1, s1, 2048
[0x80009f00]:add a1, a1, s1
[0x80009f04]:ld t4, 1048(a1)
[0x80009f08]:sub a1, a1, s1
[0x80009f0c]:addi s1, zero, 0
[0x80009f10]:csrrw zero, fcsr, s1
[0x80009f14]:fmax.s t6, t5, t4

[0x80009f14]:fmax.s t6, t5, t4
[0x80009f18]:csrrs a2, fcsr, zero
[0x80009f1c]:sd t6, 992(fp)
[0x80009f20]:sd a2, 1000(fp)
[0x80009f24]:lui s1, 3
[0x80009f28]:addiw s1, s1, 2048
[0x80009f2c]:add a1, a1, s1
[0x80009f30]:ld t5, 1056(a1)
[0x80009f34]:sub a1, a1, s1
[0x80009f38]:lui s1, 3
[0x80009f3c]:addiw s1, s1, 2048
[0x80009f40]:add a1, a1, s1
[0x80009f44]:ld t4, 1064(a1)
[0x80009f48]:sub a1, a1, s1
[0x80009f4c]:addi s1, zero, 0
[0x80009f50]:csrrw zero, fcsr, s1
[0x80009f54]:fmax.s t6, t5, t4

[0x80009f54]:fmax.s t6, t5, t4
[0x80009f58]:csrrs a2, fcsr, zero
[0x80009f5c]:sd t6, 1008(fp)
[0x80009f60]:sd a2, 1016(fp)
[0x80009f64]:lui s1, 3
[0x80009f68]:addiw s1, s1, 2048
[0x80009f6c]:add a1, a1, s1
[0x80009f70]:ld t5, 1072(a1)
[0x80009f74]:sub a1, a1, s1
[0x80009f78]:lui s1, 3
[0x80009f7c]:addiw s1, s1, 2048
[0x80009f80]:add a1, a1, s1
[0x80009f84]:ld t4, 1080(a1)
[0x80009f88]:sub a1, a1, s1
[0x80009f8c]:addi s1, zero, 0
[0x80009f90]:csrrw zero, fcsr, s1
[0x80009f94]:fmax.s t6, t5, t4

[0x80009f94]:fmax.s t6, t5, t4
[0x80009f98]:csrrs a2, fcsr, zero
[0x80009f9c]:sd t6, 1024(fp)
[0x80009fa0]:sd a2, 1032(fp)
[0x80009fa4]:lui s1, 3
[0x80009fa8]:addiw s1, s1, 2048
[0x80009fac]:add a1, a1, s1
[0x80009fb0]:ld t5, 1088(a1)
[0x80009fb4]:sub a1, a1, s1
[0x80009fb8]:lui s1, 3
[0x80009fbc]:addiw s1, s1, 2048
[0x80009fc0]:add a1, a1, s1
[0x80009fc4]:ld t4, 1096(a1)
[0x80009fc8]:sub a1, a1, s1
[0x80009fcc]:addi s1, zero, 0
[0x80009fd0]:csrrw zero, fcsr, s1
[0x80009fd4]:fmax.s t6, t5, t4

[0x80009fd4]:fmax.s t6, t5, t4
[0x80009fd8]:csrrs a2, fcsr, zero
[0x80009fdc]:sd t6, 1040(fp)
[0x80009fe0]:sd a2, 1048(fp)
[0x80009fe4]:lui s1, 3
[0x80009fe8]:addiw s1, s1, 2048
[0x80009fec]:add a1, a1, s1
[0x80009ff0]:ld t5, 1104(a1)
[0x80009ff4]:sub a1, a1, s1
[0x80009ff8]:lui s1, 3
[0x80009ffc]:addiw s1, s1, 2048
[0x8000a000]:add a1, a1, s1
[0x8000a004]:ld t4, 1112(a1)
[0x8000a008]:sub a1, a1, s1
[0x8000a00c]:addi s1, zero, 0
[0x8000a010]:csrrw zero, fcsr, s1
[0x8000a014]:fmax.s t6, t5, t4

[0x8000a014]:fmax.s t6, t5, t4
[0x8000a018]:csrrs a2, fcsr, zero
[0x8000a01c]:sd t6, 1056(fp)
[0x8000a020]:sd a2, 1064(fp)
[0x8000a024]:lui s1, 3
[0x8000a028]:addiw s1, s1, 2048
[0x8000a02c]:add a1, a1, s1
[0x8000a030]:ld t5, 1120(a1)
[0x8000a034]:sub a1, a1, s1
[0x8000a038]:lui s1, 3
[0x8000a03c]:addiw s1, s1, 2048
[0x8000a040]:add a1, a1, s1
[0x8000a044]:ld t4, 1128(a1)
[0x8000a048]:sub a1, a1, s1
[0x8000a04c]:addi s1, zero, 0
[0x8000a050]:csrrw zero, fcsr, s1
[0x8000a054]:fmax.s t6, t5, t4

[0x8000a054]:fmax.s t6, t5, t4
[0x8000a058]:csrrs a2, fcsr, zero
[0x8000a05c]:sd t6, 1072(fp)
[0x8000a060]:sd a2, 1080(fp)
[0x8000a064]:lui s1, 3
[0x8000a068]:addiw s1, s1, 2048
[0x8000a06c]:add a1, a1, s1
[0x8000a070]:ld t5, 1136(a1)
[0x8000a074]:sub a1, a1, s1
[0x8000a078]:lui s1, 3
[0x8000a07c]:addiw s1, s1, 2048
[0x8000a080]:add a1, a1, s1
[0x8000a084]:ld t4, 1144(a1)
[0x8000a088]:sub a1, a1, s1
[0x8000a08c]:addi s1, zero, 0
[0x8000a090]:csrrw zero, fcsr, s1
[0x8000a094]:fmax.s t6, t5, t4

[0x8000a094]:fmax.s t6, t5, t4
[0x8000a098]:csrrs a2, fcsr, zero
[0x8000a09c]:sd t6, 1088(fp)
[0x8000a0a0]:sd a2, 1096(fp)
[0x8000a0a4]:lui s1, 3
[0x8000a0a8]:addiw s1, s1, 2048
[0x8000a0ac]:add a1, a1, s1
[0x8000a0b0]:ld t5, 1152(a1)
[0x8000a0b4]:sub a1, a1, s1
[0x8000a0b8]:lui s1, 3
[0x8000a0bc]:addiw s1, s1, 2048
[0x8000a0c0]:add a1, a1, s1
[0x8000a0c4]:ld t4, 1160(a1)
[0x8000a0c8]:sub a1, a1, s1
[0x8000a0cc]:addi s1, zero, 0
[0x8000a0d0]:csrrw zero, fcsr, s1
[0x8000a0d4]:fmax.s t6, t5, t4

[0x8000a0d4]:fmax.s t6, t5, t4
[0x8000a0d8]:csrrs a2, fcsr, zero
[0x8000a0dc]:sd t6, 1104(fp)
[0x8000a0e0]:sd a2, 1112(fp)
[0x8000a0e4]:lui s1, 3
[0x8000a0e8]:addiw s1, s1, 2048
[0x8000a0ec]:add a1, a1, s1
[0x8000a0f0]:ld t5, 1168(a1)
[0x8000a0f4]:sub a1, a1, s1
[0x8000a0f8]:lui s1, 3
[0x8000a0fc]:addiw s1, s1, 2048
[0x8000a100]:add a1, a1, s1
[0x8000a104]:ld t4, 1176(a1)
[0x8000a108]:sub a1, a1, s1
[0x8000a10c]:addi s1, zero, 0
[0x8000a110]:csrrw zero, fcsr, s1
[0x8000a114]:fmax.s t6, t5, t4

[0x8000a114]:fmax.s t6, t5, t4
[0x8000a118]:csrrs a2, fcsr, zero
[0x8000a11c]:sd t6, 1120(fp)
[0x8000a120]:sd a2, 1128(fp)
[0x8000a124]:lui s1, 3
[0x8000a128]:addiw s1, s1, 2048
[0x8000a12c]:add a1, a1, s1
[0x8000a130]:ld t5, 1184(a1)
[0x8000a134]:sub a1, a1, s1
[0x8000a138]:lui s1, 3
[0x8000a13c]:addiw s1, s1, 2048
[0x8000a140]:add a1, a1, s1
[0x8000a144]:ld t4, 1192(a1)
[0x8000a148]:sub a1, a1, s1
[0x8000a14c]:addi s1, zero, 0
[0x8000a150]:csrrw zero, fcsr, s1
[0x8000a154]:fmax.s t6, t5, t4

[0x8000a154]:fmax.s t6, t5, t4
[0x8000a158]:csrrs a2, fcsr, zero
[0x8000a15c]:sd t6, 1136(fp)
[0x8000a160]:sd a2, 1144(fp)
[0x8000a164]:lui s1, 3
[0x8000a168]:addiw s1, s1, 2048
[0x8000a16c]:add a1, a1, s1
[0x8000a170]:ld t5, 1200(a1)
[0x8000a174]:sub a1, a1, s1
[0x8000a178]:lui s1, 3
[0x8000a17c]:addiw s1, s1, 2048
[0x8000a180]:add a1, a1, s1
[0x8000a184]:ld t4, 1208(a1)
[0x8000a188]:sub a1, a1, s1
[0x8000a18c]:addi s1, zero, 0
[0x8000a190]:csrrw zero, fcsr, s1
[0x8000a194]:fmax.s t6, t5, t4

[0x8000a194]:fmax.s t6, t5, t4
[0x8000a198]:csrrs a2, fcsr, zero
[0x8000a19c]:sd t6, 1152(fp)
[0x8000a1a0]:sd a2, 1160(fp)
[0x8000a1a4]:lui s1, 3
[0x8000a1a8]:addiw s1, s1, 2048
[0x8000a1ac]:add a1, a1, s1
[0x8000a1b0]:ld t5, 1216(a1)
[0x8000a1b4]:sub a1, a1, s1
[0x8000a1b8]:lui s1, 3
[0x8000a1bc]:addiw s1, s1, 2048
[0x8000a1c0]:add a1, a1, s1
[0x8000a1c4]:ld t4, 1224(a1)
[0x8000a1c8]:sub a1, a1, s1
[0x8000a1cc]:addi s1, zero, 0
[0x8000a1d0]:csrrw zero, fcsr, s1
[0x8000a1d4]:fmax.s t6, t5, t4

[0x8000a1d4]:fmax.s t6, t5, t4
[0x8000a1d8]:csrrs a2, fcsr, zero
[0x8000a1dc]:sd t6, 1168(fp)
[0x8000a1e0]:sd a2, 1176(fp)
[0x8000a1e4]:lui s1, 3
[0x8000a1e8]:addiw s1, s1, 2048
[0x8000a1ec]:add a1, a1, s1
[0x8000a1f0]:ld t5, 1232(a1)
[0x8000a1f4]:sub a1, a1, s1
[0x8000a1f8]:lui s1, 3
[0x8000a1fc]:addiw s1, s1, 2048
[0x8000a200]:add a1, a1, s1
[0x8000a204]:ld t4, 1240(a1)
[0x8000a208]:sub a1, a1, s1
[0x8000a20c]:addi s1, zero, 0
[0x8000a210]:csrrw zero, fcsr, s1
[0x8000a214]:fmax.s t6, t5, t4

[0x8000a214]:fmax.s t6, t5, t4
[0x8000a218]:csrrs a2, fcsr, zero
[0x8000a21c]:sd t6, 1184(fp)
[0x8000a220]:sd a2, 1192(fp)
[0x8000a224]:lui s1, 3
[0x8000a228]:addiw s1, s1, 2048
[0x8000a22c]:add a1, a1, s1
[0x8000a230]:ld t5, 1248(a1)
[0x8000a234]:sub a1, a1, s1
[0x8000a238]:lui s1, 3
[0x8000a23c]:addiw s1, s1, 2048
[0x8000a240]:add a1, a1, s1
[0x8000a244]:ld t4, 1256(a1)
[0x8000a248]:sub a1, a1, s1
[0x8000a24c]:addi s1, zero, 0
[0x8000a250]:csrrw zero, fcsr, s1
[0x8000a254]:fmax.s t6, t5, t4

[0x8000a254]:fmax.s t6, t5, t4
[0x8000a258]:csrrs a2, fcsr, zero
[0x8000a25c]:sd t6, 1200(fp)
[0x8000a260]:sd a2, 1208(fp)
[0x8000a264]:lui s1, 3
[0x8000a268]:addiw s1, s1, 2048
[0x8000a26c]:add a1, a1, s1
[0x8000a270]:ld t5, 1264(a1)
[0x8000a274]:sub a1, a1, s1
[0x8000a278]:lui s1, 3
[0x8000a27c]:addiw s1, s1, 2048
[0x8000a280]:add a1, a1, s1
[0x8000a284]:ld t4, 1272(a1)
[0x8000a288]:sub a1, a1, s1
[0x8000a28c]:addi s1, zero, 0
[0x8000a290]:csrrw zero, fcsr, s1
[0x8000a294]:fmax.s t6, t5, t4

[0x8000a294]:fmax.s t6, t5, t4
[0x8000a298]:csrrs a2, fcsr, zero
[0x8000a29c]:sd t6, 1216(fp)
[0x8000a2a0]:sd a2, 1224(fp)
[0x8000a2a4]:lui s1, 3
[0x8000a2a8]:addiw s1, s1, 2048
[0x8000a2ac]:add a1, a1, s1
[0x8000a2b0]:ld t5, 1280(a1)
[0x8000a2b4]:sub a1, a1, s1
[0x8000a2b8]:lui s1, 3
[0x8000a2bc]:addiw s1, s1, 2048
[0x8000a2c0]:add a1, a1, s1
[0x8000a2c4]:ld t4, 1288(a1)
[0x8000a2c8]:sub a1, a1, s1
[0x8000a2cc]:addi s1, zero, 0
[0x8000a2d0]:csrrw zero, fcsr, s1
[0x8000a2d4]:fmax.s t6, t5, t4

[0x8000a2d4]:fmax.s t6, t5, t4
[0x8000a2d8]:csrrs a2, fcsr, zero
[0x8000a2dc]:sd t6, 1232(fp)
[0x8000a2e0]:sd a2, 1240(fp)
[0x8000a2e4]:lui s1, 3
[0x8000a2e8]:addiw s1, s1, 2048
[0x8000a2ec]:add a1, a1, s1
[0x8000a2f0]:ld t5, 1296(a1)
[0x8000a2f4]:sub a1, a1, s1
[0x8000a2f8]:lui s1, 3
[0x8000a2fc]:addiw s1, s1, 2048
[0x8000a300]:add a1, a1, s1
[0x8000a304]:ld t4, 1304(a1)
[0x8000a308]:sub a1, a1, s1
[0x8000a30c]:addi s1, zero, 0
[0x8000a310]:csrrw zero, fcsr, s1
[0x8000a314]:fmax.s t6, t5, t4

[0x8000a314]:fmax.s t6, t5, t4
[0x8000a318]:csrrs a2, fcsr, zero
[0x8000a31c]:sd t6, 1248(fp)
[0x8000a320]:sd a2, 1256(fp)
[0x8000a324]:lui s1, 3
[0x8000a328]:addiw s1, s1, 2048
[0x8000a32c]:add a1, a1, s1
[0x8000a330]:ld t5, 1312(a1)
[0x8000a334]:sub a1, a1, s1
[0x8000a338]:lui s1, 3
[0x8000a33c]:addiw s1, s1, 2048
[0x8000a340]:add a1, a1, s1
[0x8000a344]:ld t4, 1320(a1)
[0x8000a348]:sub a1, a1, s1
[0x8000a34c]:addi s1, zero, 0
[0x8000a350]:csrrw zero, fcsr, s1
[0x8000a354]:fmax.s t6, t5, t4

[0x8000a354]:fmax.s t6, t5, t4
[0x8000a358]:csrrs a2, fcsr, zero
[0x8000a35c]:sd t6, 1264(fp)
[0x8000a360]:sd a2, 1272(fp)
[0x8000a364]:lui s1, 3
[0x8000a368]:addiw s1, s1, 2048
[0x8000a36c]:add a1, a1, s1
[0x8000a370]:ld t5, 1328(a1)
[0x8000a374]:sub a1, a1, s1
[0x8000a378]:lui s1, 3
[0x8000a37c]:addiw s1, s1, 2048
[0x8000a380]:add a1, a1, s1
[0x8000a384]:ld t4, 1336(a1)
[0x8000a388]:sub a1, a1, s1
[0x8000a38c]:addi s1, zero, 0
[0x8000a390]:csrrw zero, fcsr, s1
[0x8000a394]:fmax.s t6, t5, t4

[0x8000a394]:fmax.s t6, t5, t4
[0x8000a398]:csrrs a2, fcsr, zero
[0x8000a39c]:sd t6, 1280(fp)
[0x8000a3a0]:sd a2, 1288(fp)
[0x8000a3a4]:lui s1, 3
[0x8000a3a8]:addiw s1, s1, 2048
[0x8000a3ac]:add a1, a1, s1
[0x8000a3b0]:ld t5, 1344(a1)
[0x8000a3b4]:sub a1, a1, s1
[0x8000a3b8]:lui s1, 3
[0x8000a3bc]:addiw s1, s1, 2048
[0x8000a3c0]:add a1, a1, s1
[0x8000a3c4]:ld t4, 1352(a1)
[0x8000a3c8]:sub a1, a1, s1
[0x8000a3cc]:addi s1, zero, 0
[0x8000a3d0]:csrrw zero, fcsr, s1
[0x8000a3d4]:fmax.s t6, t5, t4

[0x8000a3d4]:fmax.s t6, t5, t4
[0x8000a3d8]:csrrs a2, fcsr, zero
[0x8000a3dc]:sd t6, 1296(fp)
[0x8000a3e0]:sd a2, 1304(fp)
[0x8000a3e4]:lui s1, 3
[0x8000a3e8]:addiw s1, s1, 2048
[0x8000a3ec]:add a1, a1, s1
[0x8000a3f0]:ld t5, 1360(a1)
[0x8000a3f4]:sub a1, a1, s1
[0x8000a3f8]:lui s1, 3
[0x8000a3fc]:addiw s1, s1, 2048
[0x8000a400]:add a1, a1, s1
[0x8000a404]:ld t4, 1368(a1)
[0x8000a408]:sub a1, a1, s1
[0x8000a40c]:addi s1, zero, 0
[0x8000a410]:csrrw zero, fcsr, s1
[0x8000a414]:fmax.s t6, t5, t4

[0x8000a414]:fmax.s t6, t5, t4
[0x8000a418]:csrrs a2, fcsr, zero
[0x8000a41c]:sd t6, 1312(fp)
[0x8000a420]:sd a2, 1320(fp)
[0x8000a424]:lui s1, 3
[0x8000a428]:addiw s1, s1, 2048
[0x8000a42c]:add a1, a1, s1
[0x8000a430]:ld t5, 1376(a1)
[0x8000a434]:sub a1, a1, s1
[0x8000a438]:lui s1, 3
[0x8000a43c]:addiw s1, s1, 2048
[0x8000a440]:add a1, a1, s1
[0x8000a444]:ld t4, 1384(a1)
[0x8000a448]:sub a1, a1, s1
[0x8000a44c]:addi s1, zero, 0
[0x8000a450]:csrrw zero, fcsr, s1
[0x8000a454]:fmax.s t6, t5, t4

[0x8000a454]:fmax.s t6, t5, t4
[0x8000a458]:csrrs a2, fcsr, zero
[0x8000a45c]:sd t6, 1328(fp)
[0x8000a460]:sd a2, 1336(fp)
[0x8000a464]:lui s1, 3
[0x8000a468]:addiw s1, s1, 2048
[0x8000a46c]:add a1, a1, s1
[0x8000a470]:ld t5, 1392(a1)
[0x8000a474]:sub a1, a1, s1
[0x8000a478]:lui s1, 3
[0x8000a47c]:addiw s1, s1, 2048
[0x8000a480]:add a1, a1, s1
[0x8000a484]:ld t4, 1400(a1)
[0x8000a488]:sub a1, a1, s1
[0x8000a48c]:addi s1, zero, 0
[0x8000a490]:csrrw zero, fcsr, s1
[0x8000a494]:fmax.s t6, t5, t4

[0x8000a494]:fmax.s t6, t5, t4
[0x8000a498]:csrrs a2, fcsr, zero
[0x8000a49c]:sd t6, 1344(fp)
[0x8000a4a0]:sd a2, 1352(fp)
[0x8000a4a4]:lui s1, 3
[0x8000a4a8]:addiw s1, s1, 2048
[0x8000a4ac]:add a1, a1, s1
[0x8000a4b0]:ld t5, 1408(a1)
[0x8000a4b4]:sub a1, a1, s1
[0x8000a4b8]:lui s1, 3
[0x8000a4bc]:addiw s1, s1, 2048
[0x8000a4c0]:add a1, a1, s1
[0x8000a4c4]:ld t4, 1416(a1)
[0x8000a4c8]:sub a1, a1, s1
[0x8000a4cc]:addi s1, zero, 0
[0x8000a4d0]:csrrw zero, fcsr, s1
[0x8000a4d4]:fmax.s t6, t5, t4

[0x8000a4d4]:fmax.s t6, t5, t4
[0x8000a4d8]:csrrs a2, fcsr, zero
[0x8000a4dc]:sd t6, 1360(fp)
[0x8000a4e0]:sd a2, 1368(fp)
[0x8000a4e4]:lui s1, 3
[0x8000a4e8]:addiw s1, s1, 2048
[0x8000a4ec]:add a1, a1, s1
[0x8000a4f0]:ld t5, 1424(a1)
[0x8000a4f4]:sub a1, a1, s1
[0x8000a4f8]:lui s1, 3
[0x8000a4fc]:addiw s1, s1, 2048
[0x8000a500]:add a1, a1, s1
[0x8000a504]:ld t4, 1432(a1)
[0x8000a508]:sub a1, a1, s1
[0x8000a50c]:addi s1, zero, 0
[0x8000a510]:csrrw zero, fcsr, s1
[0x8000a514]:fmax.s t6, t5, t4

[0x8000a514]:fmax.s t6, t5, t4
[0x8000a518]:csrrs a2, fcsr, zero
[0x8000a51c]:sd t6, 1376(fp)
[0x8000a520]:sd a2, 1384(fp)
[0x8000a524]:lui s1, 3
[0x8000a528]:addiw s1, s1, 2048
[0x8000a52c]:add a1, a1, s1
[0x8000a530]:ld t5, 1440(a1)
[0x8000a534]:sub a1, a1, s1
[0x8000a538]:lui s1, 3
[0x8000a53c]:addiw s1, s1, 2048
[0x8000a540]:add a1, a1, s1
[0x8000a544]:ld t4, 1448(a1)
[0x8000a548]:sub a1, a1, s1
[0x8000a54c]:addi s1, zero, 0
[0x8000a550]:csrrw zero, fcsr, s1
[0x8000a554]:fmax.s t6, t5, t4

[0x8000a554]:fmax.s t6, t5, t4
[0x8000a558]:csrrs a2, fcsr, zero
[0x8000a55c]:sd t6, 1392(fp)
[0x8000a560]:sd a2, 1400(fp)
[0x8000a564]:lui s1, 3
[0x8000a568]:addiw s1, s1, 2048
[0x8000a56c]:add a1, a1, s1
[0x8000a570]:ld t5, 1456(a1)
[0x8000a574]:sub a1, a1, s1
[0x8000a578]:lui s1, 3
[0x8000a57c]:addiw s1, s1, 2048
[0x8000a580]:add a1, a1, s1
[0x8000a584]:ld t4, 1464(a1)
[0x8000a588]:sub a1, a1, s1
[0x8000a58c]:addi s1, zero, 0
[0x8000a590]:csrrw zero, fcsr, s1
[0x8000a594]:fmax.s t6, t5, t4

[0x8000a594]:fmax.s t6, t5, t4
[0x8000a598]:csrrs a2, fcsr, zero
[0x8000a59c]:sd t6, 1408(fp)
[0x8000a5a0]:sd a2, 1416(fp)
[0x8000a5a4]:lui s1, 3
[0x8000a5a8]:addiw s1, s1, 2048
[0x8000a5ac]:add a1, a1, s1
[0x8000a5b0]:ld t5, 1472(a1)
[0x8000a5b4]:sub a1, a1, s1
[0x8000a5b8]:lui s1, 3
[0x8000a5bc]:addiw s1, s1, 2048
[0x8000a5c0]:add a1, a1, s1
[0x8000a5c4]:ld t4, 1480(a1)
[0x8000a5c8]:sub a1, a1, s1
[0x8000a5cc]:addi s1, zero, 0
[0x8000a5d0]:csrrw zero, fcsr, s1
[0x8000a5d4]:fmax.s t6, t5, t4

[0x8000a5d4]:fmax.s t6, t5, t4
[0x8000a5d8]:csrrs a2, fcsr, zero
[0x8000a5dc]:sd t6, 1424(fp)
[0x8000a5e0]:sd a2, 1432(fp)
[0x8000a5e4]:lui s1, 3
[0x8000a5e8]:addiw s1, s1, 2048
[0x8000a5ec]:add a1, a1, s1
[0x8000a5f0]:ld t5, 1488(a1)
[0x8000a5f4]:sub a1, a1, s1
[0x8000a5f8]:lui s1, 3
[0x8000a5fc]:addiw s1, s1, 2048
[0x8000a600]:add a1, a1, s1
[0x8000a604]:ld t4, 1496(a1)
[0x8000a608]:sub a1, a1, s1
[0x8000a60c]:addi s1, zero, 0
[0x8000a610]:csrrw zero, fcsr, s1
[0x8000a614]:fmax.s t6, t5, t4

[0x8000a614]:fmax.s t6, t5, t4
[0x8000a618]:csrrs a2, fcsr, zero
[0x8000a61c]:sd t6, 1440(fp)
[0x8000a620]:sd a2, 1448(fp)
[0x8000a624]:lui s1, 3
[0x8000a628]:addiw s1, s1, 2048
[0x8000a62c]:add a1, a1, s1
[0x8000a630]:ld t5, 1504(a1)
[0x8000a634]:sub a1, a1, s1
[0x8000a638]:lui s1, 3
[0x8000a63c]:addiw s1, s1, 2048
[0x8000a640]:add a1, a1, s1
[0x8000a644]:ld t4, 1512(a1)
[0x8000a648]:sub a1, a1, s1
[0x8000a64c]:addi s1, zero, 0
[0x8000a650]:csrrw zero, fcsr, s1
[0x8000a654]:fmax.s t6, t5, t4

[0x8000a654]:fmax.s t6, t5, t4
[0x8000a658]:csrrs a2, fcsr, zero
[0x8000a65c]:sd t6, 1456(fp)
[0x8000a660]:sd a2, 1464(fp)
[0x8000a664]:lui s1, 3
[0x8000a668]:addiw s1, s1, 2048
[0x8000a66c]:add a1, a1, s1
[0x8000a670]:ld t5, 1520(a1)
[0x8000a674]:sub a1, a1, s1
[0x8000a678]:lui s1, 3
[0x8000a67c]:addiw s1, s1, 2048
[0x8000a680]:add a1, a1, s1
[0x8000a684]:ld t4, 1528(a1)
[0x8000a688]:sub a1, a1, s1
[0x8000a68c]:addi s1, zero, 0
[0x8000a690]:csrrw zero, fcsr, s1
[0x8000a694]:fmax.s t6, t5, t4

[0x8000a694]:fmax.s t6, t5, t4
[0x8000a698]:csrrs a2, fcsr, zero
[0x8000a69c]:sd t6, 1472(fp)
[0x8000a6a0]:sd a2, 1480(fp)
[0x8000a6a4]:lui s1, 3
[0x8000a6a8]:addiw s1, s1, 2048
[0x8000a6ac]:add a1, a1, s1
[0x8000a6b0]:ld t5, 1536(a1)
[0x8000a6b4]:sub a1, a1, s1
[0x8000a6b8]:lui s1, 3
[0x8000a6bc]:addiw s1, s1, 2048
[0x8000a6c0]:add a1, a1, s1
[0x8000a6c4]:ld t4, 1544(a1)
[0x8000a6c8]:sub a1, a1, s1
[0x8000a6cc]:addi s1, zero, 0
[0x8000a6d0]:csrrw zero, fcsr, s1
[0x8000a6d4]:fmax.s t6, t5, t4

[0x8000a6d4]:fmax.s t6, t5, t4
[0x8000a6d8]:csrrs a2, fcsr, zero
[0x8000a6dc]:sd t6, 1488(fp)
[0x8000a6e0]:sd a2, 1496(fp)
[0x8000a6e4]:lui s1, 3
[0x8000a6e8]:addiw s1, s1, 2048
[0x8000a6ec]:add a1, a1, s1
[0x8000a6f0]:ld t5, 1552(a1)
[0x8000a6f4]:sub a1, a1, s1
[0x8000a6f8]:lui s1, 3
[0x8000a6fc]:addiw s1, s1, 2048
[0x8000a700]:add a1, a1, s1
[0x8000a704]:ld t4, 1560(a1)
[0x8000a708]:sub a1, a1, s1
[0x8000a70c]:addi s1, zero, 0
[0x8000a710]:csrrw zero, fcsr, s1
[0x8000a714]:fmax.s t6, t5, t4

[0x8000a714]:fmax.s t6, t5, t4
[0x8000a718]:csrrs a2, fcsr, zero
[0x8000a71c]:sd t6, 1504(fp)
[0x8000a720]:sd a2, 1512(fp)
[0x8000a724]:lui s1, 3
[0x8000a728]:addiw s1, s1, 2048
[0x8000a72c]:add a1, a1, s1
[0x8000a730]:ld t5, 1568(a1)
[0x8000a734]:sub a1, a1, s1
[0x8000a738]:lui s1, 3
[0x8000a73c]:addiw s1, s1, 2048
[0x8000a740]:add a1, a1, s1
[0x8000a744]:ld t4, 1576(a1)
[0x8000a748]:sub a1, a1, s1
[0x8000a74c]:addi s1, zero, 0
[0x8000a750]:csrrw zero, fcsr, s1
[0x8000a754]:fmax.s t6, t5, t4

[0x8000a754]:fmax.s t6, t5, t4
[0x8000a758]:csrrs a2, fcsr, zero
[0x8000a75c]:sd t6, 1520(fp)
[0x8000a760]:sd a2, 1528(fp)
[0x8000a764]:lui s1, 3
[0x8000a768]:addiw s1, s1, 2048
[0x8000a76c]:add a1, a1, s1
[0x8000a770]:ld t5, 1584(a1)
[0x8000a774]:sub a1, a1, s1
[0x8000a778]:lui s1, 3
[0x8000a77c]:addiw s1, s1, 2048
[0x8000a780]:add a1, a1, s1
[0x8000a784]:ld t4, 1592(a1)
[0x8000a788]:sub a1, a1, s1
[0x8000a78c]:addi s1, zero, 0
[0x8000a790]:csrrw zero, fcsr, s1
[0x8000a794]:fmax.s t6, t5, t4

[0x8000a794]:fmax.s t6, t5, t4
[0x8000a798]:csrrs a2, fcsr, zero
[0x8000a79c]:sd t6, 1536(fp)
[0x8000a7a0]:sd a2, 1544(fp)
[0x8000a7a4]:lui s1, 3
[0x8000a7a8]:addiw s1, s1, 2048
[0x8000a7ac]:add a1, a1, s1
[0x8000a7b0]:ld t5, 1600(a1)
[0x8000a7b4]:sub a1, a1, s1
[0x8000a7b8]:lui s1, 3
[0x8000a7bc]:addiw s1, s1, 2048
[0x8000a7c0]:add a1, a1, s1
[0x8000a7c4]:ld t4, 1608(a1)
[0x8000a7c8]:sub a1, a1, s1
[0x8000a7cc]:addi s1, zero, 0
[0x8000a7d0]:csrrw zero, fcsr, s1
[0x8000a7d4]:fmax.s t6, t5, t4

[0x8000a7d4]:fmax.s t6, t5, t4
[0x8000a7d8]:csrrs a2, fcsr, zero
[0x8000a7dc]:sd t6, 1552(fp)
[0x8000a7e0]:sd a2, 1560(fp)
[0x8000a7e4]:lui s1, 3
[0x8000a7e8]:addiw s1, s1, 2048
[0x8000a7ec]:add a1, a1, s1
[0x8000a7f0]:ld t5, 1616(a1)
[0x8000a7f4]:sub a1, a1, s1
[0x8000a7f8]:lui s1, 3
[0x8000a7fc]:addiw s1, s1, 2048
[0x8000a800]:add a1, a1, s1
[0x8000a804]:ld t4, 1624(a1)
[0x8000a808]:sub a1, a1, s1
[0x8000a80c]:addi s1, zero, 0
[0x8000a810]:csrrw zero, fcsr, s1
[0x8000a814]:fmax.s t6, t5, t4

[0x8000a814]:fmax.s t6, t5, t4
[0x8000a818]:csrrs a2, fcsr, zero
[0x8000a81c]:sd t6, 1568(fp)
[0x8000a820]:sd a2, 1576(fp)
[0x8000a824]:lui s1, 3
[0x8000a828]:addiw s1, s1, 2048
[0x8000a82c]:add a1, a1, s1
[0x8000a830]:ld t5, 1632(a1)
[0x8000a834]:sub a1, a1, s1
[0x8000a838]:lui s1, 3
[0x8000a83c]:addiw s1, s1, 2048
[0x8000a840]:add a1, a1, s1
[0x8000a844]:ld t4, 1640(a1)
[0x8000a848]:sub a1, a1, s1
[0x8000a84c]:addi s1, zero, 0
[0x8000a850]:csrrw zero, fcsr, s1
[0x8000a854]:fmax.s t6, t5, t4

[0x8000a854]:fmax.s t6, t5, t4
[0x8000a858]:csrrs a2, fcsr, zero
[0x8000a85c]:sd t6, 1584(fp)
[0x8000a860]:sd a2, 1592(fp)
[0x8000a864]:lui s1, 3
[0x8000a868]:addiw s1, s1, 2048
[0x8000a86c]:add a1, a1, s1
[0x8000a870]:ld t5, 1648(a1)
[0x8000a874]:sub a1, a1, s1
[0x8000a878]:lui s1, 3
[0x8000a87c]:addiw s1, s1, 2048
[0x8000a880]:add a1, a1, s1
[0x8000a884]:ld t4, 1656(a1)
[0x8000a888]:sub a1, a1, s1
[0x8000a88c]:addi s1, zero, 0
[0x8000a890]:csrrw zero, fcsr, s1
[0x8000a894]:fmax.s t6, t5, t4

[0x8000a894]:fmax.s t6, t5, t4
[0x8000a898]:csrrs a2, fcsr, zero
[0x8000a89c]:sd t6, 1600(fp)
[0x8000a8a0]:sd a2, 1608(fp)
[0x8000a8a4]:lui s1, 3
[0x8000a8a8]:addiw s1, s1, 2048
[0x8000a8ac]:add a1, a1, s1
[0x8000a8b0]:ld t5, 1664(a1)
[0x8000a8b4]:sub a1, a1, s1
[0x8000a8b8]:lui s1, 3
[0x8000a8bc]:addiw s1, s1, 2048
[0x8000a8c0]:add a1, a1, s1
[0x8000a8c4]:ld t4, 1672(a1)
[0x8000a8c8]:sub a1, a1, s1
[0x8000a8cc]:addi s1, zero, 0
[0x8000a8d0]:csrrw zero, fcsr, s1
[0x8000a8d4]:fmax.s t6, t5, t4

[0x8000a8d4]:fmax.s t6, t5, t4
[0x8000a8d8]:csrrs a2, fcsr, zero
[0x8000a8dc]:sd t6, 1616(fp)
[0x8000a8e0]:sd a2, 1624(fp)
[0x8000a8e4]:lui s1, 3
[0x8000a8e8]:addiw s1, s1, 2048
[0x8000a8ec]:add a1, a1, s1
[0x8000a8f0]:ld t5, 1680(a1)
[0x8000a8f4]:sub a1, a1, s1
[0x8000a8f8]:lui s1, 3
[0x8000a8fc]:addiw s1, s1, 2048
[0x8000a900]:add a1, a1, s1
[0x8000a904]:ld t4, 1688(a1)
[0x8000a908]:sub a1, a1, s1
[0x8000a90c]:addi s1, zero, 0
[0x8000a910]:csrrw zero, fcsr, s1
[0x8000a914]:fmax.s t6, t5, t4

[0x8000a914]:fmax.s t6, t5, t4
[0x8000a918]:csrrs a2, fcsr, zero
[0x8000a91c]:sd t6, 1632(fp)
[0x8000a920]:sd a2, 1640(fp)
[0x8000a924]:lui s1, 3
[0x8000a928]:addiw s1, s1, 2048
[0x8000a92c]:add a1, a1, s1
[0x8000a930]:ld t5, 1696(a1)
[0x8000a934]:sub a1, a1, s1
[0x8000a938]:lui s1, 3
[0x8000a93c]:addiw s1, s1, 2048
[0x8000a940]:add a1, a1, s1
[0x8000a944]:ld t4, 1704(a1)
[0x8000a948]:sub a1, a1, s1
[0x8000a94c]:addi s1, zero, 0
[0x8000a950]:csrrw zero, fcsr, s1
[0x8000a954]:fmax.s t6, t5, t4

[0x8000a954]:fmax.s t6, t5, t4
[0x8000a958]:csrrs a2, fcsr, zero
[0x8000a95c]:sd t6, 1648(fp)
[0x8000a960]:sd a2, 1656(fp)
[0x8000a964]:lui s1, 3
[0x8000a968]:addiw s1, s1, 2048
[0x8000a96c]:add a1, a1, s1
[0x8000a970]:ld t5, 1712(a1)
[0x8000a974]:sub a1, a1, s1
[0x8000a978]:lui s1, 3
[0x8000a97c]:addiw s1, s1, 2048
[0x8000a980]:add a1, a1, s1
[0x8000a984]:ld t4, 1720(a1)
[0x8000a988]:sub a1, a1, s1
[0x8000a98c]:addi s1, zero, 0
[0x8000a990]:csrrw zero, fcsr, s1
[0x8000a994]:fmax.s t6, t5, t4

[0x8000a994]:fmax.s t6, t5, t4
[0x8000a998]:csrrs a2, fcsr, zero
[0x8000a99c]:sd t6, 1664(fp)
[0x8000a9a0]:sd a2, 1672(fp)
[0x8000a9a4]:lui s1, 3
[0x8000a9a8]:addiw s1, s1, 2048
[0x8000a9ac]:add a1, a1, s1
[0x8000a9b0]:ld t5, 1728(a1)
[0x8000a9b4]:sub a1, a1, s1
[0x8000a9b8]:lui s1, 3
[0x8000a9bc]:addiw s1, s1, 2048
[0x8000a9c0]:add a1, a1, s1
[0x8000a9c4]:ld t4, 1736(a1)
[0x8000a9c8]:sub a1, a1, s1
[0x8000a9cc]:addi s1, zero, 0
[0x8000a9d0]:csrrw zero, fcsr, s1
[0x8000a9d4]:fmax.s t6, t5, t4

[0x8000a9d4]:fmax.s t6, t5, t4
[0x8000a9d8]:csrrs a2, fcsr, zero
[0x8000a9dc]:sd t6, 1680(fp)
[0x8000a9e0]:sd a2, 1688(fp)
[0x8000a9e4]:lui s1, 3
[0x8000a9e8]:addiw s1, s1, 2048
[0x8000a9ec]:add a1, a1, s1
[0x8000a9f0]:ld t5, 1744(a1)
[0x8000a9f4]:sub a1, a1, s1
[0x8000a9f8]:lui s1, 3
[0x8000a9fc]:addiw s1, s1, 2048
[0x8000aa00]:add a1, a1, s1
[0x8000aa04]:ld t4, 1752(a1)
[0x8000aa08]:sub a1, a1, s1
[0x8000aa0c]:addi s1, zero, 0
[0x8000aa10]:csrrw zero, fcsr, s1
[0x8000aa14]:fmax.s t6, t5, t4

[0x8000aa14]:fmax.s t6, t5, t4
[0x8000aa18]:csrrs a2, fcsr, zero
[0x8000aa1c]:sd t6, 1696(fp)
[0x8000aa20]:sd a2, 1704(fp)
[0x8000aa24]:lui s1, 3
[0x8000aa28]:addiw s1, s1, 2048
[0x8000aa2c]:add a1, a1, s1
[0x8000aa30]:ld t5, 1760(a1)
[0x8000aa34]:sub a1, a1, s1
[0x8000aa38]:lui s1, 3
[0x8000aa3c]:addiw s1, s1, 2048
[0x8000aa40]:add a1, a1, s1
[0x8000aa44]:ld t4, 1768(a1)
[0x8000aa48]:sub a1, a1, s1
[0x8000aa4c]:addi s1, zero, 0
[0x8000aa50]:csrrw zero, fcsr, s1
[0x8000aa54]:fmax.s t6, t5, t4

[0x8000aa54]:fmax.s t6, t5, t4
[0x8000aa58]:csrrs a2, fcsr, zero
[0x8000aa5c]:sd t6, 1712(fp)
[0x8000aa60]:sd a2, 1720(fp)
[0x8000aa64]:lui s1, 3
[0x8000aa68]:addiw s1, s1, 2048
[0x8000aa6c]:add a1, a1, s1
[0x8000aa70]:ld t5, 1776(a1)
[0x8000aa74]:sub a1, a1, s1
[0x8000aa78]:lui s1, 3
[0x8000aa7c]:addiw s1, s1, 2048
[0x8000aa80]:add a1, a1, s1
[0x8000aa84]:ld t4, 1784(a1)
[0x8000aa88]:sub a1, a1, s1
[0x8000aa8c]:addi s1, zero, 0
[0x8000aa90]:csrrw zero, fcsr, s1
[0x8000aa94]:fmax.s t6, t5, t4

[0x8000aa94]:fmax.s t6, t5, t4
[0x8000aa98]:csrrs a2, fcsr, zero
[0x8000aa9c]:sd t6, 1728(fp)
[0x8000aaa0]:sd a2, 1736(fp)
[0x8000aaa4]:lui s1, 3
[0x8000aaa8]:addiw s1, s1, 2048
[0x8000aaac]:add a1, a1, s1
[0x8000aab0]:ld t5, 1792(a1)
[0x8000aab4]:sub a1, a1, s1
[0x8000aab8]:lui s1, 3
[0x8000aabc]:addiw s1, s1, 2048
[0x8000aac0]:add a1, a1, s1
[0x8000aac4]:ld t4, 1800(a1)
[0x8000aac8]:sub a1, a1, s1
[0x8000aacc]:addi s1, zero, 0
[0x8000aad0]:csrrw zero, fcsr, s1
[0x8000aad4]:fmax.s t6, t5, t4

[0x8000aad4]:fmax.s t6, t5, t4
[0x8000aad8]:csrrs a2, fcsr, zero
[0x8000aadc]:sd t6, 1744(fp)
[0x8000aae0]:sd a2, 1752(fp)
[0x8000aae4]:lui s1, 3
[0x8000aae8]:addiw s1, s1, 2048
[0x8000aaec]:add a1, a1, s1
[0x8000aaf0]:ld t5, 1808(a1)
[0x8000aaf4]:sub a1, a1, s1
[0x8000aaf8]:lui s1, 3
[0x8000aafc]:addiw s1, s1, 2048
[0x8000ab00]:add a1, a1, s1
[0x8000ab04]:ld t4, 1816(a1)
[0x8000ab08]:sub a1, a1, s1
[0x8000ab0c]:addi s1, zero, 0
[0x8000ab10]:csrrw zero, fcsr, s1
[0x8000ab14]:fmax.s t6, t5, t4

[0x8000ab14]:fmax.s t6, t5, t4
[0x8000ab18]:csrrs a2, fcsr, zero
[0x8000ab1c]:sd t6, 1760(fp)
[0x8000ab20]:sd a2, 1768(fp)
[0x8000ab24]:lui s1, 3
[0x8000ab28]:addiw s1, s1, 2048
[0x8000ab2c]:add a1, a1, s1
[0x8000ab30]:ld t5, 1824(a1)
[0x8000ab34]:sub a1, a1, s1
[0x8000ab38]:lui s1, 3
[0x8000ab3c]:addiw s1, s1, 2048
[0x8000ab40]:add a1, a1, s1
[0x8000ab44]:ld t4, 1832(a1)
[0x8000ab48]:sub a1, a1, s1
[0x8000ab4c]:addi s1, zero, 0
[0x8000ab50]:csrrw zero, fcsr, s1
[0x8000ab54]:fmax.s t6, t5, t4

[0x8000ab54]:fmax.s t6, t5, t4
[0x8000ab58]:csrrs a2, fcsr, zero
[0x8000ab5c]:sd t6, 1776(fp)
[0x8000ab60]:sd a2, 1784(fp)
[0x8000ab64]:lui s1, 3
[0x8000ab68]:addiw s1, s1, 2048
[0x8000ab6c]:add a1, a1, s1
[0x8000ab70]:ld t5, 1840(a1)
[0x8000ab74]:sub a1, a1, s1
[0x8000ab78]:lui s1, 3
[0x8000ab7c]:addiw s1, s1, 2048
[0x8000ab80]:add a1, a1, s1
[0x8000ab84]:ld t4, 1848(a1)
[0x8000ab88]:sub a1, a1, s1
[0x8000ab8c]:addi s1, zero, 0
[0x8000ab90]:csrrw zero, fcsr, s1
[0x8000ab94]:fmax.s t6, t5, t4

[0x8000ab94]:fmax.s t6, t5, t4
[0x8000ab98]:csrrs a2, fcsr, zero
[0x8000ab9c]:sd t6, 1792(fp)
[0x8000aba0]:sd a2, 1800(fp)
[0x8000aba4]:lui s1, 3
[0x8000aba8]:addiw s1, s1, 2048
[0x8000abac]:add a1, a1, s1
[0x8000abb0]:ld t5, 1856(a1)
[0x8000abb4]:sub a1, a1, s1
[0x8000abb8]:lui s1, 3
[0x8000abbc]:addiw s1, s1, 2048
[0x8000abc0]:add a1, a1, s1
[0x8000abc4]:ld t4, 1864(a1)
[0x8000abc8]:sub a1, a1, s1
[0x8000abcc]:addi s1, zero, 0
[0x8000abd0]:csrrw zero, fcsr, s1
[0x8000abd4]:fmax.s t6, t5, t4

[0x8000abd4]:fmax.s t6, t5, t4
[0x8000abd8]:csrrs a2, fcsr, zero
[0x8000abdc]:sd t6, 1808(fp)
[0x8000abe0]:sd a2, 1816(fp)
[0x8000abe4]:lui s1, 3
[0x8000abe8]:addiw s1, s1, 2048
[0x8000abec]:add a1, a1, s1
[0x8000abf0]:ld t5, 1872(a1)
[0x8000abf4]:sub a1, a1, s1
[0x8000abf8]:lui s1, 3
[0x8000abfc]:addiw s1, s1, 2048
[0x8000ac00]:add a1, a1, s1
[0x8000ac04]:ld t4, 1880(a1)
[0x8000ac08]:sub a1, a1, s1
[0x8000ac0c]:addi s1, zero, 0
[0x8000ac10]:csrrw zero, fcsr, s1
[0x8000ac14]:fmax.s t6, t5, t4

[0x8000ac14]:fmax.s t6, t5, t4
[0x8000ac18]:csrrs a2, fcsr, zero
[0x8000ac1c]:sd t6, 1824(fp)
[0x8000ac20]:sd a2, 1832(fp)
[0x8000ac24]:lui s1, 3
[0x8000ac28]:addiw s1, s1, 2048
[0x8000ac2c]:add a1, a1, s1
[0x8000ac30]:ld t5, 1888(a1)
[0x8000ac34]:sub a1, a1, s1
[0x8000ac38]:lui s1, 3
[0x8000ac3c]:addiw s1, s1, 2048
[0x8000ac40]:add a1, a1, s1
[0x8000ac44]:ld t4, 1896(a1)
[0x8000ac48]:sub a1, a1, s1
[0x8000ac4c]:addi s1, zero, 0
[0x8000ac50]:csrrw zero, fcsr, s1
[0x8000ac54]:fmax.s t6, t5, t4

[0x8000ac54]:fmax.s t6, t5, t4
[0x8000ac58]:csrrs a2, fcsr, zero
[0x8000ac5c]:sd t6, 1840(fp)
[0x8000ac60]:sd a2, 1848(fp)
[0x8000ac64]:lui s1, 3
[0x8000ac68]:addiw s1, s1, 2048
[0x8000ac6c]:add a1, a1, s1
[0x8000ac70]:ld t5, 1904(a1)
[0x8000ac74]:sub a1, a1, s1
[0x8000ac78]:lui s1, 3
[0x8000ac7c]:addiw s1, s1, 2048
[0x8000ac80]:add a1, a1, s1
[0x8000ac84]:ld t4, 1912(a1)
[0x8000ac88]:sub a1, a1, s1
[0x8000ac8c]:addi s1, zero, 0
[0x8000ac90]:csrrw zero, fcsr, s1
[0x8000ac94]:fmax.s t6, t5, t4

[0x8000ac94]:fmax.s t6, t5, t4
[0x8000ac98]:csrrs a2, fcsr, zero
[0x8000ac9c]:sd t6, 1856(fp)
[0x8000aca0]:sd a2, 1864(fp)
[0x8000aca4]:lui s1, 3
[0x8000aca8]:addiw s1, s1, 2048
[0x8000acac]:add a1, a1, s1
[0x8000acb0]:ld t5, 1920(a1)
[0x8000acb4]:sub a1, a1, s1
[0x8000acb8]:lui s1, 3
[0x8000acbc]:addiw s1, s1, 2048
[0x8000acc0]:add a1, a1, s1
[0x8000acc4]:ld t4, 1928(a1)
[0x8000acc8]:sub a1, a1, s1
[0x8000accc]:addi s1, zero, 0
[0x8000acd0]:csrrw zero, fcsr, s1
[0x8000acd4]:fmax.s t6, t5, t4

[0x8000acd4]:fmax.s t6, t5, t4
[0x8000acd8]:csrrs a2, fcsr, zero
[0x8000acdc]:sd t6, 1872(fp)
[0x8000ace0]:sd a2, 1880(fp)
[0x8000ace4]:lui s1, 3
[0x8000ace8]:addiw s1, s1, 2048
[0x8000acec]:add a1, a1, s1
[0x8000acf0]:ld t5, 1936(a1)
[0x8000acf4]:sub a1, a1, s1
[0x8000acf8]:lui s1, 3
[0x8000acfc]:addiw s1, s1, 2048
[0x8000ad00]:add a1, a1, s1
[0x8000ad04]:ld t4, 1944(a1)
[0x8000ad08]:sub a1, a1, s1
[0x8000ad0c]:addi s1, zero, 0
[0x8000ad10]:csrrw zero, fcsr, s1
[0x8000ad14]:fmax.s t6, t5, t4

[0x8000ad14]:fmax.s t6, t5, t4
[0x8000ad18]:csrrs a2, fcsr, zero
[0x8000ad1c]:sd t6, 1888(fp)
[0x8000ad20]:sd a2, 1896(fp)
[0x8000ad24]:lui s1, 3
[0x8000ad28]:addiw s1, s1, 2048
[0x8000ad2c]:add a1, a1, s1
[0x8000ad30]:ld t5, 1952(a1)
[0x8000ad34]:sub a1, a1, s1
[0x8000ad38]:lui s1, 3
[0x8000ad3c]:addiw s1, s1, 2048
[0x8000ad40]:add a1, a1, s1
[0x8000ad44]:ld t4, 1960(a1)
[0x8000ad48]:sub a1, a1, s1
[0x8000ad4c]:addi s1, zero, 0
[0x8000ad50]:csrrw zero, fcsr, s1
[0x8000ad54]:fmax.s t6, t5, t4

[0x8000ad54]:fmax.s t6, t5, t4
[0x8000ad58]:csrrs a2, fcsr, zero
[0x8000ad5c]:sd t6, 1904(fp)
[0x8000ad60]:sd a2, 1912(fp)
[0x8000ad64]:lui s1, 3
[0x8000ad68]:addiw s1, s1, 2048
[0x8000ad6c]:add a1, a1, s1
[0x8000ad70]:ld t5, 1968(a1)
[0x8000ad74]:sub a1, a1, s1
[0x8000ad78]:lui s1, 3
[0x8000ad7c]:addiw s1, s1, 2048
[0x8000ad80]:add a1, a1, s1
[0x8000ad84]:ld t4, 1976(a1)
[0x8000ad88]:sub a1, a1, s1
[0x8000ad8c]:addi s1, zero, 0
[0x8000ad90]:csrrw zero, fcsr, s1
[0x8000ad94]:fmax.s t6, t5, t4

[0x8000ad94]:fmax.s t6, t5, t4
[0x8000ad98]:csrrs a2, fcsr, zero
[0x8000ad9c]:sd t6, 1920(fp)
[0x8000ada0]:sd a2, 1928(fp)
[0x8000ada4]:lui s1, 3
[0x8000ada8]:addiw s1, s1, 2048
[0x8000adac]:add a1, a1, s1
[0x8000adb0]:ld t5, 1984(a1)
[0x8000adb4]:sub a1, a1, s1
[0x8000adb8]:lui s1, 3
[0x8000adbc]:addiw s1, s1, 2048
[0x8000adc0]:add a1, a1, s1
[0x8000adc4]:ld t4, 1992(a1)
[0x8000adc8]:sub a1, a1, s1
[0x8000adcc]:addi s1, zero, 0
[0x8000add0]:csrrw zero, fcsr, s1
[0x8000add4]:fmax.s t6, t5, t4

[0x8000add4]:fmax.s t6, t5, t4
[0x8000add8]:csrrs a2, fcsr, zero
[0x8000addc]:sd t6, 1936(fp)
[0x8000ade0]:sd a2, 1944(fp)
[0x8000ade4]:lui s1, 3
[0x8000ade8]:addiw s1, s1, 2048
[0x8000adec]:add a1, a1, s1
[0x8000adf0]:ld t5, 2000(a1)
[0x8000adf4]:sub a1, a1, s1
[0x8000adf8]:lui s1, 3
[0x8000adfc]:addiw s1, s1, 2048
[0x8000ae00]:add a1, a1, s1
[0x8000ae04]:ld t4, 2008(a1)
[0x8000ae08]:sub a1, a1, s1
[0x8000ae0c]:addi s1, zero, 0
[0x8000ae10]:csrrw zero, fcsr, s1
[0x8000ae14]:fmax.s t6, t5, t4

[0x8000ae14]:fmax.s t6, t5, t4
[0x8000ae18]:csrrs a2, fcsr, zero
[0x8000ae1c]:sd t6, 1952(fp)
[0x8000ae20]:sd a2, 1960(fp)
[0x8000ae24]:lui s1, 3
[0x8000ae28]:addiw s1, s1, 2048
[0x8000ae2c]:add a1, a1, s1
[0x8000ae30]:ld t5, 2016(a1)
[0x8000ae34]:sub a1, a1, s1
[0x8000ae38]:lui s1, 3
[0x8000ae3c]:addiw s1, s1, 2048
[0x8000ae40]:add a1, a1, s1
[0x8000ae44]:ld t4, 2024(a1)
[0x8000ae48]:sub a1, a1, s1
[0x8000ae4c]:addi s1, zero, 0
[0x8000ae50]:csrrw zero, fcsr, s1
[0x8000ae54]:fmax.s t6, t5, t4

[0x8000ae54]:fmax.s t6, t5, t4
[0x8000ae58]:csrrs a2, fcsr, zero
[0x8000ae5c]:sd t6, 1968(fp)
[0x8000ae60]:sd a2, 1976(fp)
[0x8000ae64]:lui s1, 3
[0x8000ae68]:addiw s1, s1, 2048
[0x8000ae6c]:add a1, a1, s1
[0x8000ae70]:ld t5, 2032(a1)
[0x8000ae74]:sub a1, a1, s1
[0x8000ae78]:lui s1, 3
[0x8000ae7c]:addiw s1, s1, 2048
[0x8000ae80]:add a1, a1, s1
[0x8000ae84]:ld t4, 2040(a1)
[0x8000ae88]:sub a1, a1, s1
[0x8000ae8c]:addi s1, zero, 0
[0x8000ae90]:csrrw zero, fcsr, s1
[0x8000ae94]:fmax.s t6, t5, t4

[0x8000ae94]:fmax.s t6, t5, t4
[0x8000ae98]:csrrs a2, fcsr, zero
[0x8000ae9c]:sd t6, 1984(fp)
[0x8000aea0]:sd a2, 1992(fp)
[0x8000aea4]:lui s1, 3
[0x8000aea8]:add a1, a1, s1
[0x8000aeac]:ld t5, 0(a1)
[0x8000aeb0]:sub a1, a1, s1
[0x8000aeb4]:lui s1, 3
[0x8000aeb8]:add a1, a1, s1
[0x8000aebc]:ld t4, 8(a1)
[0x8000aec0]:sub a1, a1, s1
[0x8000aec4]:addi s1, zero, 0
[0x8000aec8]:csrrw zero, fcsr, s1
[0x8000aecc]:fmax.s t6, t5, t4

[0x8000aecc]:fmax.s t6, t5, t4
[0x8000aed0]:csrrs a2, fcsr, zero
[0x8000aed4]:sd t6, 2000(fp)
[0x8000aed8]:sd a2, 2008(fp)
[0x8000aedc]:lui s1, 3
[0x8000aee0]:add a1, a1, s1
[0x8000aee4]:ld t5, 16(a1)
[0x8000aee8]:sub a1, a1, s1
[0x8000aeec]:lui s1, 3
[0x8000aef0]:add a1, a1, s1
[0x8000aef4]:ld t4, 24(a1)
[0x8000aef8]:sub a1, a1, s1
[0x8000aefc]:addi s1, zero, 0
[0x8000af00]:csrrw zero, fcsr, s1
[0x8000af04]:fmax.s t6, t5, t4

[0x8000af04]:fmax.s t6, t5, t4
[0x8000af08]:csrrs a2, fcsr, zero
[0x8000af0c]:sd t6, 2016(fp)
[0x8000af10]:sd a2, 2024(fp)
[0x8000af14]:lui s1, 3
[0x8000af18]:add a1, a1, s1
[0x8000af1c]:ld t5, 32(a1)
[0x8000af20]:sub a1, a1, s1
[0x8000af24]:lui s1, 3
[0x8000af28]:add a1, a1, s1
[0x8000af2c]:ld t4, 40(a1)
[0x8000af30]:sub a1, a1, s1
[0x8000af34]:addi s1, zero, 0
[0x8000af38]:csrrw zero, fcsr, s1
[0x8000af3c]:fmax.s t6, t5, t4

[0x8000af3c]:fmax.s t6, t5, t4
[0x8000af40]:csrrs a2, fcsr, zero
[0x8000af44]:sd t6, 2032(fp)
[0x8000af48]:sd a2, 2040(fp)
[0x8000af4c]:auipc fp, 11
[0x8000af50]:addi fp, fp, 92
[0x8000af54]:lui s1, 3
[0x8000af58]:add a1, a1, s1
[0x8000af5c]:ld t5, 48(a1)
[0x8000af60]:sub a1, a1, s1
[0x8000af64]:lui s1, 3
[0x8000af68]:add a1, a1, s1
[0x8000af6c]:ld t4, 56(a1)
[0x8000af70]:sub a1, a1, s1
[0x8000af74]:addi s1, zero, 0
[0x8000af78]:csrrw zero, fcsr, s1
[0x8000af7c]:fmax.s t6, t5, t4

[0x8000af7c]:fmax.s t6, t5, t4
[0x8000af80]:csrrs a2, fcsr, zero
[0x8000af84]:sd t6, 0(fp)
[0x8000af88]:sd a2, 8(fp)
[0x8000af8c]:lui s1, 3
[0x8000af90]:add a1, a1, s1
[0x8000af94]:ld t5, 64(a1)
[0x8000af98]:sub a1, a1, s1
[0x8000af9c]:lui s1, 3
[0x8000afa0]:add a1, a1, s1
[0x8000afa4]:ld t4, 72(a1)
[0x8000afa8]:sub a1, a1, s1
[0x8000afac]:addi s1, zero, 0
[0x8000afb0]:csrrw zero, fcsr, s1
[0x8000afb4]:fmax.s t6, t5, t4

[0x8000afb4]:fmax.s t6, t5, t4
[0x8000afb8]:csrrs a2, fcsr, zero
[0x8000afbc]:sd t6, 16(fp)
[0x8000afc0]:sd a2, 24(fp)
[0x8000afc4]:lui s1, 3
[0x8000afc8]:add a1, a1, s1
[0x8000afcc]:ld t5, 80(a1)
[0x8000afd0]:sub a1, a1, s1
[0x8000afd4]:lui s1, 3
[0x8000afd8]:add a1, a1, s1
[0x8000afdc]:ld t4, 88(a1)
[0x8000afe0]:sub a1, a1, s1
[0x8000afe4]:addi s1, zero, 0
[0x8000afe8]:csrrw zero, fcsr, s1
[0x8000afec]:fmax.s t6, t5, t4

[0x8000afec]:fmax.s t6, t5, t4
[0x8000aff0]:csrrs a2, fcsr, zero
[0x8000aff4]:sd t6, 32(fp)
[0x8000aff8]:sd a2, 40(fp)
[0x8000affc]:lui s1, 3
[0x8000b000]:add a1, a1, s1
[0x8000b004]:ld t5, 96(a1)
[0x8000b008]:sub a1, a1, s1
[0x8000b00c]:lui s1, 3
[0x8000b010]:add a1, a1, s1
[0x8000b014]:ld t4, 104(a1)
[0x8000b018]:sub a1, a1, s1
[0x8000b01c]:addi s1, zero, 0
[0x8000b020]:csrrw zero, fcsr, s1
[0x8000b024]:fmax.s t6, t5, t4

[0x8000b024]:fmax.s t6, t5, t4
[0x8000b028]:csrrs a2, fcsr, zero
[0x8000b02c]:sd t6, 48(fp)
[0x8000b030]:sd a2, 56(fp)
[0x8000b034]:lui s1, 3
[0x8000b038]:add a1, a1, s1
[0x8000b03c]:ld t5, 112(a1)
[0x8000b040]:sub a1, a1, s1
[0x8000b044]:lui s1, 3
[0x8000b048]:add a1, a1, s1
[0x8000b04c]:ld t4, 120(a1)
[0x8000b050]:sub a1, a1, s1
[0x8000b054]:addi s1, zero, 0
[0x8000b058]:csrrw zero, fcsr, s1
[0x8000b05c]:fmax.s t6, t5, t4

[0x8000b05c]:fmax.s t6, t5, t4
[0x8000b060]:csrrs a2, fcsr, zero
[0x8000b064]:sd t6, 64(fp)
[0x8000b068]:sd a2, 72(fp)
[0x8000b06c]:lui s1, 3
[0x8000b070]:add a1, a1, s1
[0x8000b074]:ld t5, 128(a1)
[0x8000b078]:sub a1, a1, s1
[0x8000b07c]:lui s1, 3
[0x8000b080]:add a1, a1, s1
[0x8000b084]:ld t4, 136(a1)
[0x8000b088]:sub a1, a1, s1
[0x8000b08c]:addi s1, zero, 0
[0x8000b090]:csrrw zero, fcsr, s1
[0x8000b094]:fmax.s t6, t5, t4

[0x8000b094]:fmax.s t6, t5, t4
[0x8000b098]:csrrs a2, fcsr, zero
[0x8000b09c]:sd t6, 80(fp)
[0x8000b0a0]:sd a2, 88(fp)
[0x8000b0a4]:lui s1, 3
[0x8000b0a8]:add a1, a1, s1
[0x8000b0ac]:ld t5, 144(a1)
[0x8000b0b0]:sub a1, a1, s1
[0x8000b0b4]:lui s1, 3
[0x8000b0b8]:add a1, a1, s1
[0x8000b0bc]:ld t4, 152(a1)
[0x8000b0c0]:sub a1, a1, s1
[0x8000b0c4]:addi s1, zero, 0
[0x8000b0c8]:csrrw zero, fcsr, s1
[0x8000b0cc]:fmax.s t6, t5, t4

[0x8000b0cc]:fmax.s t6, t5, t4
[0x8000b0d0]:csrrs a2, fcsr, zero
[0x8000b0d4]:sd t6, 96(fp)
[0x8000b0d8]:sd a2, 104(fp)
[0x8000b0dc]:lui s1, 3
[0x8000b0e0]:add a1, a1, s1
[0x8000b0e4]:ld t5, 160(a1)
[0x8000b0e8]:sub a1, a1, s1
[0x8000b0ec]:lui s1, 3
[0x8000b0f0]:add a1, a1, s1
[0x8000b0f4]:ld t4, 168(a1)
[0x8000b0f8]:sub a1, a1, s1
[0x8000b0fc]:addi s1, zero, 0
[0x8000b100]:csrrw zero, fcsr, s1
[0x8000b104]:fmax.s t6, t5, t4

[0x8000b104]:fmax.s t6, t5, t4
[0x8000b108]:csrrs a2, fcsr, zero
[0x8000b10c]:sd t6, 112(fp)
[0x8000b110]:sd a2, 120(fp)
[0x8000b114]:lui s1, 3
[0x8000b118]:add a1, a1, s1
[0x8000b11c]:ld t5, 176(a1)
[0x8000b120]:sub a1, a1, s1
[0x8000b124]:lui s1, 3
[0x8000b128]:add a1, a1, s1
[0x8000b12c]:ld t4, 184(a1)
[0x8000b130]:sub a1, a1, s1
[0x8000b134]:addi s1, zero, 0
[0x8000b138]:csrrw zero, fcsr, s1
[0x8000b13c]:fmax.s t6, t5, t4

[0x8000b13c]:fmax.s t6, t5, t4
[0x8000b140]:csrrs a2, fcsr, zero
[0x8000b144]:sd t6, 128(fp)
[0x8000b148]:sd a2, 136(fp)
[0x8000b14c]:lui s1, 3
[0x8000b150]:add a1, a1, s1
[0x8000b154]:ld t5, 192(a1)
[0x8000b158]:sub a1, a1, s1
[0x8000b15c]:lui s1, 3
[0x8000b160]:add a1, a1, s1
[0x8000b164]:ld t4, 200(a1)
[0x8000b168]:sub a1, a1, s1
[0x8000b16c]:addi s1, zero, 0
[0x8000b170]:csrrw zero, fcsr, s1
[0x8000b174]:fmax.s t6, t5, t4

[0x8000b174]:fmax.s t6, t5, t4
[0x8000b178]:csrrs a2, fcsr, zero
[0x8000b17c]:sd t6, 144(fp)
[0x8000b180]:sd a2, 152(fp)
[0x8000b184]:lui s1, 3
[0x8000b188]:add a1, a1, s1
[0x8000b18c]:ld t5, 208(a1)
[0x8000b190]:sub a1, a1, s1
[0x8000b194]:lui s1, 3
[0x8000b198]:add a1, a1, s1
[0x8000b19c]:ld t4, 216(a1)
[0x8000b1a0]:sub a1, a1, s1
[0x8000b1a4]:addi s1, zero, 0
[0x8000b1a8]:csrrw zero, fcsr, s1
[0x8000b1ac]:fmax.s t6, t5, t4

[0x8000b1ac]:fmax.s t6, t5, t4
[0x8000b1b0]:csrrs a2, fcsr, zero
[0x8000b1b4]:sd t6, 160(fp)
[0x8000b1b8]:sd a2, 168(fp)
[0x8000b1bc]:lui s1, 3
[0x8000b1c0]:add a1, a1, s1
[0x8000b1c4]:ld t5, 224(a1)
[0x8000b1c8]:sub a1, a1, s1
[0x8000b1cc]:lui s1, 3
[0x8000b1d0]:add a1, a1, s1
[0x8000b1d4]:ld t4, 232(a1)
[0x8000b1d8]:sub a1, a1, s1
[0x8000b1dc]:addi s1, zero, 0
[0x8000b1e0]:csrrw zero, fcsr, s1
[0x8000b1e4]:fmax.s t6, t5, t4

[0x8000b1e4]:fmax.s t6, t5, t4
[0x8000b1e8]:csrrs a2, fcsr, zero
[0x8000b1ec]:sd t6, 176(fp)
[0x8000b1f0]:sd a2, 184(fp)
[0x8000b1f4]:lui s1, 3
[0x8000b1f8]:add a1, a1, s1
[0x8000b1fc]:ld t5, 240(a1)
[0x8000b200]:sub a1, a1, s1
[0x8000b204]:lui s1, 3
[0x8000b208]:add a1, a1, s1
[0x8000b20c]:ld t4, 248(a1)
[0x8000b210]:sub a1, a1, s1
[0x8000b214]:addi s1, zero, 0
[0x8000b218]:csrrw zero, fcsr, s1
[0x8000b21c]:fmax.s t6, t5, t4

[0x8000b21c]:fmax.s t6, t5, t4
[0x8000b220]:csrrs a2, fcsr, zero
[0x8000b224]:sd t6, 192(fp)
[0x8000b228]:sd a2, 200(fp)
[0x8000b22c]:lui s1, 3
[0x8000b230]:add a1, a1, s1
[0x8000b234]:ld t5, 256(a1)
[0x8000b238]:sub a1, a1, s1
[0x8000b23c]:lui s1, 3
[0x8000b240]:add a1, a1, s1
[0x8000b244]:ld t4, 264(a1)
[0x8000b248]:sub a1, a1, s1
[0x8000b24c]:addi s1, zero, 0
[0x8000b250]:csrrw zero, fcsr, s1
[0x8000b254]:fmax.s t6, t5, t4

[0x8000b254]:fmax.s t6, t5, t4
[0x8000b258]:csrrs a2, fcsr, zero
[0x8000b25c]:sd t6, 208(fp)
[0x8000b260]:sd a2, 216(fp)
[0x8000b264]:lui s1, 3
[0x8000b268]:add a1, a1, s1
[0x8000b26c]:ld t5, 272(a1)
[0x8000b270]:sub a1, a1, s1
[0x8000b274]:lui s1, 3
[0x8000b278]:add a1, a1, s1
[0x8000b27c]:ld t4, 280(a1)
[0x8000b280]:sub a1, a1, s1
[0x8000b284]:addi s1, zero, 0
[0x8000b288]:csrrw zero, fcsr, s1
[0x8000b28c]:fmax.s t6, t5, t4

[0x8000b28c]:fmax.s t6, t5, t4
[0x8000b290]:csrrs a2, fcsr, zero
[0x8000b294]:sd t6, 224(fp)
[0x8000b298]:sd a2, 232(fp)
[0x8000b29c]:lui s1, 3
[0x8000b2a0]:add a1, a1, s1
[0x8000b2a4]:ld t5, 288(a1)
[0x8000b2a8]:sub a1, a1, s1
[0x8000b2ac]:lui s1, 3
[0x8000b2b0]:add a1, a1, s1
[0x8000b2b4]:ld t4, 296(a1)
[0x8000b2b8]:sub a1, a1, s1
[0x8000b2bc]:addi s1, zero, 0
[0x8000b2c0]:csrrw zero, fcsr, s1
[0x8000b2c4]:fmax.s t6, t5, t4

[0x8000b2c4]:fmax.s t6, t5, t4
[0x8000b2c8]:csrrs a2, fcsr, zero
[0x8000b2cc]:sd t6, 240(fp)
[0x8000b2d0]:sd a2, 248(fp)
[0x8000b2d4]:lui s1, 3
[0x8000b2d8]:add a1, a1, s1
[0x8000b2dc]:ld t5, 304(a1)
[0x8000b2e0]:sub a1, a1, s1
[0x8000b2e4]:lui s1, 3
[0x8000b2e8]:add a1, a1, s1
[0x8000b2ec]:ld t4, 312(a1)
[0x8000b2f0]:sub a1, a1, s1
[0x8000b2f4]:addi s1, zero, 0
[0x8000b2f8]:csrrw zero, fcsr, s1
[0x8000b2fc]:fmax.s t6, t5, t4

[0x8000b2fc]:fmax.s t6, t5, t4
[0x8000b300]:csrrs a2, fcsr, zero
[0x8000b304]:sd t6, 256(fp)
[0x8000b308]:sd a2, 264(fp)
[0x8000b30c]:lui s1, 3
[0x8000b310]:add a1, a1, s1
[0x8000b314]:ld t5, 320(a1)
[0x8000b318]:sub a1, a1, s1
[0x8000b31c]:lui s1, 3
[0x8000b320]:add a1, a1, s1
[0x8000b324]:ld t4, 328(a1)
[0x8000b328]:sub a1, a1, s1
[0x8000b32c]:addi s1, zero, 0
[0x8000b330]:csrrw zero, fcsr, s1
[0x8000b334]:fmax.s t6, t5, t4

[0x8000b334]:fmax.s t6, t5, t4
[0x8000b338]:csrrs a2, fcsr, zero
[0x8000b33c]:sd t6, 272(fp)
[0x8000b340]:sd a2, 280(fp)
[0x8000b344]:lui s1, 3
[0x8000b348]:add a1, a1, s1
[0x8000b34c]:ld t5, 336(a1)
[0x8000b350]:sub a1, a1, s1
[0x8000b354]:lui s1, 3
[0x8000b358]:add a1, a1, s1
[0x8000b35c]:ld t4, 344(a1)
[0x8000b360]:sub a1, a1, s1
[0x8000b364]:addi s1, zero, 0
[0x8000b368]:csrrw zero, fcsr, s1
[0x8000b36c]:fmax.s t6, t5, t4

[0x8000b36c]:fmax.s t6, t5, t4
[0x8000b370]:csrrs a2, fcsr, zero
[0x8000b374]:sd t6, 288(fp)
[0x8000b378]:sd a2, 296(fp)
[0x8000b37c]:lui s1, 3
[0x8000b380]:add a1, a1, s1
[0x8000b384]:ld t5, 352(a1)
[0x8000b388]:sub a1, a1, s1
[0x8000b38c]:lui s1, 3
[0x8000b390]:add a1, a1, s1
[0x8000b394]:ld t4, 360(a1)
[0x8000b398]:sub a1, a1, s1
[0x8000b39c]:addi s1, zero, 0
[0x8000b3a0]:csrrw zero, fcsr, s1
[0x8000b3a4]:fmax.s t6, t5, t4

[0x8000b3a4]:fmax.s t6, t5, t4
[0x8000b3a8]:csrrs a2, fcsr, zero
[0x8000b3ac]:sd t6, 304(fp)
[0x8000b3b0]:sd a2, 312(fp)
[0x8000b3b4]:lui s1, 3
[0x8000b3b8]:add a1, a1, s1
[0x8000b3bc]:ld t5, 368(a1)
[0x8000b3c0]:sub a1, a1, s1
[0x8000b3c4]:lui s1, 3
[0x8000b3c8]:add a1, a1, s1
[0x8000b3cc]:ld t4, 376(a1)
[0x8000b3d0]:sub a1, a1, s1
[0x8000b3d4]:addi s1, zero, 0
[0x8000b3d8]:csrrw zero, fcsr, s1
[0x8000b3dc]:fmax.s t6, t5, t4

[0x8000b3dc]:fmax.s t6, t5, t4
[0x8000b3e0]:csrrs a2, fcsr, zero
[0x8000b3e4]:sd t6, 320(fp)
[0x8000b3e8]:sd a2, 328(fp)
[0x8000b3ec]:lui s1, 3
[0x8000b3f0]:add a1, a1, s1
[0x8000b3f4]:ld t5, 384(a1)
[0x8000b3f8]:sub a1, a1, s1
[0x8000b3fc]:lui s1, 3
[0x8000b400]:add a1, a1, s1
[0x8000b404]:ld t4, 392(a1)
[0x8000b408]:sub a1, a1, s1
[0x8000b40c]:addi s1, zero, 0
[0x8000b410]:csrrw zero, fcsr, s1
[0x8000b414]:fmax.s t6, t5, t4

[0x8000b414]:fmax.s t6, t5, t4
[0x8000b418]:csrrs a2, fcsr, zero
[0x8000b41c]:sd t6, 336(fp)
[0x8000b420]:sd a2, 344(fp)
[0x8000b424]:lui s1, 3
[0x8000b428]:add a1, a1, s1
[0x8000b42c]:ld t5, 400(a1)
[0x8000b430]:sub a1, a1, s1
[0x8000b434]:lui s1, 3
[0x8000b438]:add a1, a1, s1
[0x8000b43c]:ld t4, 408(a1)
[0x8000b440]:sub a1, a1, s1
[0x8000b444]:addi s1, zero, 0
[0x8000b448]:csrrw zero, fcsr, s1
[0x8000b44c]:fmax.s t6, t5, t4

[0x8000b44c]:fmax.s t6, t5, t4
[0x8000b450]:csrrs a2, fcsr, zero
[0x8000b454]:sd t6, 352(fp)
[0x8000b458]:sd a2, 360(fp)
[0x8000b45c]:lui s1, 3
[0x8000b460]:add a1, a1, s1
[0x8000b464]:ld t5, 416(a1)
[0x8000b468]:sub a1, a1, s1
[0x8000b46c]:lui s1, 3
[0x8000b470]:add a1, a1, s1
[0x8000b474]:ld t4, 424(a1)
[0x8000b478]:sub a1, a1, s1
[0x8000b47c]:addi s1, zero, 0
[0x8000b480]:csrrw zero, fcsr, s1
[0x8000b484]:fmax.s t6, t5, t4

[0x8000b484]:fmax.s t6, t5, t4
[0x8000b488]:csrrs a2, fcsr, zero
[0x8000b48c]:sd t6, 368(fp)
[0x8000b490]:sd a2, 376(fp)
[0x8000b494]:lui s1, 3
[0x8000b498]:add a1, a1, s1
[0x8000b49c]:ld t5, 432(a1)
[0x8000b4a0]:sub a1, a1, s1
[0x8000b4a4]:lui s1, 3
[0x8000b4a8]:add a1, a1, s1
[0x8000b4ac]:ld t4, 440(a1)
[0x8000b4b0]:sub a1, a1, s1
[0x8000b4b4]:addi s1, zero, 0
[0x8000b4b8]:csrrw zero, fcsr, s1
[0x8000b4bc]:fmax.s t6, t5, t4

[0x8000b4bc]:fmax.s t6, t5, t4
[0x8000b4c0]:csrrs a2, fcsr, zero
[0x8000b4c4]:sd t6, 384(fp)
[0x8000b4c8]:sd a2, 392(fp)
[0x8000b4cc]:lui s1, 3
[0x8000b4d0]:add a1, a1, s1
[0x8000b4d4]:ld t5, 448(a1)
[0x8000b4d8]:sub a1, a1, s1
[0x8000b4dc]:lui s1, 3
[0x8000b4e0]:add a1, a1, s1
[0x8000b4e4]:ld t4, 456(a1)
[0x8000b4e8]:sub a1, a1, s1
[0x8000b4ec]:addi s1, zero, 0
[0x8000b4f0]:csrrw zero, fcsr, s1
[0x8000b4f4]:fmax.s t6, t5, t4

[0x8000b4f4]:fmax.s t6, t5, t4
[0x8000b4f8]:csrrs a2, fcsr, zero
[0x8000b4fc]:sd t6, 400(fp)
[0x8000b500]:sd a2, 408(fp)
[0x8000b504]:lui s1, 3
[0x8000b508]:add a1, a1, s1
[0x8000b50c]:ld t5, 464(a1)
[0x8000b510]:sub a1, a1, s1
[0x8000b514]:lui s1, 3
[0x8000b518]:add a1, a1, s1
[0x8000b51c]:ld t4, 472(a1)
[0x8000b520]:sub a1, a1, s1
[0x8000b524]:addi s1, zero, 0
[0x8000b528]:csrrw zero, fcsr, s1
[0x8000b52c]:fmax.s t6, t5, t4

[0x8000b52c]:fmax.s t6, t5, t4
[0x8000b530]:csrrs a2, fcsr, zero
[0x8000b534]:sd t6, 416(fp)
[0x8000b538]:sd a2, 424(fp)
[0x8000b53c]:lui s1, 3
[0x8000b540]:add a1, a1, s1
[0x8000b544]:ld t5, 480(a1)
[0x8000b548]:sub a1, a1, s1
[0x8000b54c]:lui s1, 3
[0x8000b550]:add a1, a1, s1
[0x8000b554]:ld t4, 488(a1)
[0x8000b558]:sub a1, a1, s1
[0x8000b55c]:addi s1, zero, 0
[0x8000b560]:csrrw zero, fcsr, s1
[0x8000b564]:fmax.s t6, t5, t4

[0x8000b564]:fmax.s t6, t5, t4
[0x8000b568]:csrrs a2, fcsr, zero
[0x8000b56c]:sd t6, 432(fp)
[0x8000b570]:sd a2, 440(fp)
[0x8000b574]:lui s1, 3
[0x8000b578]:add a1, a1, s1
[0x8000b57c]:ld t5, 496(a1)
[0x8000b580]:sub a1, a1, s1
[0x8000b584]:lui s1, 3
[0x8000b588]:add a1, a1, s1
[0x8000b58c]:ld t4, 504(a1)
[0x8000b590]:sub a1, a1, s1
[0x8000b594]:addi s1, zero, 0
[0x8000b598]:csrrw zero, fcsr, s1
[0x8000b59c]:fmax.s t6, t5, t4

[0x8000b59c]:fmax.s t6, t5, t4
[0x8000b5a0]:csrrs a2, fcsr, zero
[0x8000b5a4]:sd t6, 448(fp)
[0x8000b5a8]:sd a2, 456(fp)
[0x8000b5ac]:lui s1, 3
[0x8000b5b0]:add a1, a1, s1
[0x8000b5b4]:ld t5, 512(a1)
[0x8000b5b8]:sub a1, a1, s1
[0x8000b5bc]:lui s1, 3
[0x8000b5c0]:add a1, a1, s1
[0x8000b5c4]:ld t4, 520(a1)
[0x8000b5c8]:sub a1, a1, s1
[0x8000b5cc]:addi s1, zero, 0
[0x8000b5d0]:csrrw zero, fcsr, s1
[0x8000b5d4]:fmax.s t6, t5, t4

[0x8000b5d4]:fmax.s t6, t5, t4
[0x8000b5d8]:csrrs a2, fcsr, zero
[0x8000b5dc]:sd t6, 464(fp)
[0x8000b5e0]:sd a2, 472(fp)
[0x8000b5e4]:lui s1, 3
[0x8000b5e8]:add a1, a1, s1
[0x8000b5ec]:ld t5, 528(a1)
[0x8000b5f0]:sub a1, a1, s1
[0x8000b5f4]:lui s1, 3
[0x8000b5f8]:add a1, a1, s1
[0x8000b5fc]:ld t4, 536(a1)
[0x8000b600]:sub a1, a1, s1
[0x8000b604]:addi s1, zero, 0
[0x8000b608]:csrrw zero, fcsr, s1
[0x8000b60c]:fmax.s t6, t5, t4

[0x8000b60c]:fmax.s t6, t5, t4
[0x8000b610]:csrrs a2, fcsr, zero
[0x8000b614]:sd t6, 480(fp)
[0x8000b618]:sd a2, 488(fp)
[0x8000b61c]:lui s1, 3
[0x8000b620]:add a1, a1, s1
[0x8000b624]:ld t5, 544(a1)
[0x8000b628]:sub a1, a1, s1
[0x8000b62c]:lui s1, 3
[0x8000b630]:add a1, a1, s1
[0x8000b634]:ld t4, 552(a1)
[0x8000b638]:sub a1, a1, s1
[0x8000b63c]:addi s1, zero, 0
[0x8000b640]:csrrw zero, fcsr, s1
[0x8000b644]:fmax.s t6, t5, t4

[0x8000b644]:fmax.s t6, t5, t4
[0x8000b648]:csrrs a2, fcsr, zero
[0x8000b64c]:sd t6, 496(fp)
[0x8000b650]:sd a2, 504(fp)
[0x8000b654]:lui s1, 3
[0x8000b658]:add a1, a1, s1
[0x8000b65c]:ld t5, 560(a1)
[0x8000b660]:sub a1, a1, s1
[0x8000b664]:lui s1, 3
[0x8000b668]:add a1, a1, s1
[0x8000b66c]:ld t4, 568(a1)
[0x8000b670]:sub a1, a1, s1
[0x8000b674]:addi s1, zero, 0
[0x8000b678]:csrrw zero, fcsr, s1
[0x8000b67c]:fmax.s t6, t5, t4

[0x8000b67c]:fmax.s t6, t5, t4
[0x8000b680]:csrrs a2, fcsr, zero
[0x8000b684]:sd t6, 512(fp)
[0x8000b688]:sd a2, 520(fp)
[0x8000b68c]:lui s1, 3
[0x8000b690]:add a1, a1, s1
[0x8000b694]:ld t5, 576(a1)
[0x8000b698]:sub a1, a1, s1
[0x8000b69c]:lui s1, 3
[0x8000b6a0]:add a1, a1, s1
[0x8000b6a4]:ld t4, 584(a1)
[0x8000b6a8]:sub a1, a1, s1
[0x8000b6ac]:addi s1, zero, 0
[0x8000b6b0]:csrrw zero, fcsr, s1
[0x8000b6b4]:fmax.s t6, t5, t4

[0x8000b6b4]:fmax.s t6, t5, t4
[0x8000b6b8]:csrrs a2, fcsr, zero
[0x8000b6bc]:sd t6, 528(fp)
[0x8000b6c0]:sd a2, 536(fp)
[0x8000b6c4]:lui s1, 3
[0x8000b6c8]:add a1, a1, s1
[0x8000b6cc]:ld t5, 592(a1)
[0x8000b6d0]:sub a1, a1, s1
[0x8000b6d4]:lui s1, 3
[0x8000b6d8]:add a1, a1, s1
[0x8000b6dc]:ld t4, 600(a1)
[0x8000b6e0]:sub a1, a1, s1
[0x8000b6e4]:addi s1, zero, 0
[0x8000b6e8]:csrrw zero, fcsr, s1
[0x8000b6ec]:fmax.s t6, t5, t4

[0x8000b6ec]:fmax.s t6, t5, t4
[0x8000b6f0]:csrrs a2, fcsr, zero
[0x8000b6f4]:sd t6, 544(fp)
[0x8000b6f8]:sd a2, 552(fp)
[0x8000b6fc]:lui s1, 3
[0x8000b700]:add a1, a1, s1
[0x8000b704]:ld t5, 608(a1)
[0x8000b708]:sub a1, a1, s1
[0x8000b70c]:lui s1, 3
[0x8000b710]:add a1, a1, s1
[0x8000b714]:ld t4, 616(a1)
[0x8000b718]:sub a1, a1, s1
[0x8000b71c]:addi s1, zero, 0
[0x8000b720]:csrrw zero, fcsr, s1
[0x8000b724]:fmax.s t6, t5, t4

[0x8000b724]:fmax.s t6, t5, t4
[0x8000b728]:csrrs a2, fcsr, zero
[0x8000b72c]:sd t6, 560(fp)
[0x8000b730]:sd a2, 568(fp)
[0x8000b734]:lui s1, 3
[0x8000b738]:add a1, a1, s1
[0x8000b73c]:ld t5, 624(a1)
[0x8000b740]:sub a1, a1, s1
[0x8000b744]:lui s1, 3
[0x8000b748]:add a1, a1, s1
[0x8000b74c]:ld t4, 632(a1)
[0x8000b750]:sub a1, a1, s1
[0x8000b754]:addi s1, zero, 0
[0x8000b758]:csrrw zero, fcsr, s1
[0x8000b75c]:fmax.s t6, t5, t4

[0x8000b75c]:fmax.s t6, t5, t4
[0x8000b760]:csrrs a2, fcsr, zero
[0x8000b764]:sd t6, 576(fp)
[0x8000b768]:sd a2, 584(fp)
[0x8000b76c]:lui s1, 3
[0x8000b770]:add a1, a1, s1
[0x8000b774]:ld t5, 640(a1)
[0x8000b778]:sub a1, a1, s1
[0x8000b77c]:lui s1, 3
[0x8000b780]:add a1, a1, s1
[0x8000b784]:ld t4, 648(a1)
[0x8000b788]:sub a1, a1, s1
[0x8000b78c]:addi s1, zero, 0
[0x8000b790]:csrrw zero, fcsr, s1
[0x8000b794]:fmax.s t6, t5, t4

[0x8000b794]:fmax.s t6, t5, t4
[0x8000b798]:csrrs a2, fcsr, zero
[0x8000b79c]:sd t6, 592(fp)
[0x8000b7a0]:sd a2, 600(fp)
[0x8000b7a4]:lui s1, 3
[0x8000b7a8]:add a1, a1, s1
[0x8000b7ac]:ld t5, 656(a1)
[0x8000b7b0]:sub a1, a1, s1
[0x8000b7b4]:lui s1, 3
[0x8000b7b8]:add a1, a1, s1
[0x8000b7bc]:ld t4, 664(a1)
[0x8000b7c0]:sub a1, a1, s1
[0x8000b7c4]:addi s1, zero, 0
[0x8000b7c8]:csrrw zero, fcsr, s1
[0x8000b7cc]:fmax.s t6, t5, t4

[0x8000b7cc]:fmax.s t6, t5, t4
[0x8000b7d0]:csrrs a2, fcsr, zero
[0x8000b7d4]:sd t6, 608(fp)
[0x8000b7d8]:sd a2, 616(fp)
[0x8000b7dc]:lui s1, 3
[0x8000b7e0]:add a1, a1, s1
[0x8000b7e4]:ld t5, 672(a1)
[0x8000b7e8]:sub a1, a1, s1
[0x8000b7ec]:lui s1, 3
[0x8000b7f0]:add a1, a1, s1
[0x8000b7f4]:ld t4, 680(a1)
[0x8000b7f8]:sub a1, a1, s1
[0x8000b7fc]:addi s1, zero, 0
[0x8000b800]:csrrw zero, fcsr, s1
[0x8000b804]:fmax.s t6, t5, t4

[0x8000b804]:fmax.s t6, t5, t4
[0x8000b808]:csrrs a2, fcsr, zero
[0x8000b80c]:sd t6, 624(fp)
[0x8000b810]:sd a2, 632(fp)
[0x8000b814]:lui s1, 3
[0x8000b818]:add a1, a1, s1
[0x8000b81c]:ld t5, 688(a1)
[0x8000b820]:sub a1, a1, s1
[0x8000b824]:lui s1, 3
[0x8000b828]:add a1, a1, s1
[0x8000b82c]:ld t4, 696(a1)
[0x8000b830]:sub a1, a1, s1
[0x8000b834]:addi s1, zero, 0
[0x8000b838]:csrrw zero, fcsr, s1
[0x8000b83c]:fmax.s t6, t5, t4

[0x8000b83c]:fmax.s t6, t5, t4
[0x8000b840]:csrrs a2, fcsr, zero
[0x8000b844]:sd t6, 640(fp)
[0x8000b848]:sd a2, 648(fp)
[0x8000b84c]:lui s1, 3
[0x8000b850]:add a1, a1, s1
[0x8000b854]:ld t5, 704(a1)
[0x8000b858]:sub a1, a1, s1
[0x8000b85c]:lui s1, 3
[0x8000b860]:add a1, a1, s1
[0x8000b864]:ld t4, 712(a1)
[0x8000b868]:sub a1, a1, s1
[0x8000b86c]:addi s1, zero, 0
[0x8000b870]:csrrw zero, fcsr, s1
[0x8000b874]:fmax.s t6, t5, t4

[0x8000b874]:fmax.s t6, t5, t4
[0x8000b878]:csrrs a2, fcsr, zero
[0x8000b87c]:sd t6, 656(fp)
[0x8000b880]:sd a2, 664(fp)
[0x8000b884]:lui s1, 3
[0x8000b888]:add a1, a1, s1
[0x8000b88c]:ld t5, 720(a1)
[0x8000b890]:sub a1, a1, s1
[0x8000b894]:lui s1, 3
[0x8000b898]:add a1, a1, s1
[0x8000b89c]:ld t4, 728(a1)
[0x8000b8a0]:sub a1, a1, s1
[0x8000b8a4]:addi s1, zero, 0
[0x8000b8a8]:csrrw zero, fcsr, s1
[0x8000b8ac]:fmax.s t6, t5, t4

[0x8000b8ac]:fmax.s t6, t5, t4
[0x8000b8b0]:csrrs a2, fcsr, zero
[0x8000b8b4]:sd t6, 672(fp)
[0x8000b8b8]:sd a2, 680(fp)
[0x8000b8bc]:lui s1, 3
[0x8000b8c0]:add a1, a1, s1
[0x8000b8c4]:ld t5, 736(a1)
[0x8000b8c8]:sub a1, a1, s1
[0x8000b8cc]:lui s1, 3
[0x8000b8d0]:add a1, a1, s1
[0x8000b8d4]:ld t4, 744(a1)
[0x8000b8d8]:sub a1, a1, s1
[0x8000b8dc]:addi s1, zero, 0
[0x8000b8e0]:csrrw zero, fcsr, s1
[0x8000b8e4]:fmax.s t6, t5, t4

[0x8000b8e4]:fmax.s t6, t5, t4
[0x8000b8e8]:csrrs a2, fcsr, zero
[0x8000b8ec]:sd t6, 688(fp)
[0x8000b8f0]:sd a2, 696(fp)
[0x8000b8f4]:lui s1, 3
[0x8000b8f8]:add a1, a1, s1
[0x8000b8fc]:ld t5, 752(a1)
[0x8000b900]:sub a1, a1, s1
[0x8000b904]:lui s1, 3
[0x8000b908]:add a1, a1, s1
[0x8000b90c]:ld t4, 760(a1)
[0x8000b910]:sub a1, a1, s1
[0x8000b914]:addi s1, zero, 0
[0x8000b918]:csrrw zero, fcsr, s1
[0x8000b91c]:fmax.s t6, t5, t4

[0x8000b91c]:fmax.s t6, t5, t4
[0x8000b920]:csrrs a2, fcsr, zero
[0x8000b924]:sd t6, 704(fp)
[0x8000b928]:sd a2, 712(fp)
[0x8000b92c]:lui s1, 3
[0x8000b930]:add a1, a1, s1
[0x8000b934]:ld t5, 768(a1)
[0x8000b938]:sub a1, a1, s1
[0x8000b93c]:lui s1, 3
[0x8000b940]:add a1, a1, s1
[0x8000b944]:ld t4, 776(a1)
[0x8000b948]:sub a1, a1, s1
[0x8000b94c]:addi s1, zero, 0
[0x8000b950]:csrrw zero, fcsr, s1
[0x8000b954]:fmax.s t6, t5, t4

[0x8000b954]:fmax.s t6, t5, t4
[0x8000b958]:csrrs a2, fcsr, zero
[0x8000b95c]:sd t6, 720(fp)
[0x8000b960]:sd a2, 728(fp)
[0x8000b964]:lui s1, 3
[0x8000b968]:add a1, a1, s1
[0x8000b96c]:ld t5, 784(a1)
[0x8000b970]:sub a1, a1, s1
[0x8000b974]:lui s1, 3
[0x8000b978]:add a1, a1, s1
[0x8000b97c]:ld t4, 792(a1)
[0x8000b980]:sub a1, a1, s1
[0x8000b984]:addi s1, zero, 0
[0x8000b988]:csrrw zero, fcsr, s1
[0x8000b98c]:fmax.s t6, t5, t4

[0x8000b98c]:fmax.s t6, t5, t4
[0x8000b990]:csrrs a2, fcsr, zero
[0x8000b994]:sd t6, 736(fp)
[0x8000b998]:sd a2, 744(fp)
[0x8000b99c]:lui s1, 3
[0x8000b9a0]:add a1, a1, s1
[0x8000b9a4]:ld t5, 800(a1)
[0x8000b9a8]:sub a1, a1, s1
[0x8000b9ac]:lui s1, 3
[0x8000b9b0]:add a1, a1, s1
[0x8000b9b4]:ld t4, 808(a1)
[0x8000b9b8]:sub a1, a1, s1
[0x8000b9bc]:addi s1, zero, 0
[0x8000b9c0]:csrrw zero, fcsr, s1
[0x8000b9c4]:fmax.s t6, t5, t4

[0x8000b9c4]:fmax.s t6, t5, t4
[0x8000b9c8]:csrrs a2, fcsr, zero
[0x8000b9cc]:sd t6, 752(fp)
[0x8000b9d0]:sd a2, 760(fp)
[0x8000b9d4]:lui s1, 3
[0x8000b9d8]:add a1, a1, s1
[0x8000b9dc]:ld t5, 816(a1)
[0x8000b9e0]:sub a1, a1, s1
[0x8000b9e4]:lui s1, 3
[0x8000b9e8]:add a1, a1, s1
[0x8000b9ec]:ld t4, 824(a1)
[0x8000b9f0]:sub a1, a1, s1
[0x8000b9f4]:addi s1, zero, 0
[0x8000b9f8]:csrrw zero, fcsr, s1
[0x8000b9fc]:fmax.s t6, t5, t4

[0x8000b9fc]:fmax.s t6, t5, t4
[0x8000ba00]:csrrs a2, fcsr, zero
[0x8000ba04]:sd t6, 768(fp)
[0x8000ba08]:sd a2, 776(fp)
[0x8000ba0c]:lui s1, 3
[0x8000ba10]:add a1, a1, s1
[0x8000ba14]:ld t5, 832(a1)
[0x8000ba18]:sub a1, a1, s1
[0x8000ba1c]:lui s1, 3
[0x8000ba20]:add a1, a1, s1
[0x8000ba24]:ld t4, 840(a1)
[0x8000ba28]:sub a1, a1, s1
[0x8000ba2c]:addi s1, zero, 0
[0x8000ba30]:csrrw zero, fcsr, s1
[0x8000ba34]:fmax.s t6, t5, t4

[0x8000ba34]:fmax.s t6, t5, t4
[0x8000ba38]:csrrs a2, fcsr, zero
[0x8000ba3c]:sd t6, 784(fp)
[0x8000ba40]:sd a2, 792(fp)
[0x8000ba44]:lui s1, 3
[0x8000ba48]:add a1, a1, s1
[0x8000ba4c]:ld t5, 848(a1)
[0x8000ba50]:sub a1, a1, s1
[0x8000ba54]:lui s1, 3
[0x8000ba58]:add a1, a1, s1
[0x8000ba5c]:ld t4, 856(a1)
[0x8000ba60]:sub a1, a1, s1
[0x8000ba64]:addi s1, zero, 0
[0x8000ba68]:csrrw zero, fcsr, s1
[0x8000ba6c]:fmax.s t6, t5, t4

[0x8000ba6c]:fmax.s t6, t5, t4
[0x8000ba70]:csrrs a2, fcsr, zero
[0x8000ba74]:sd t6, 800(fp)
[0x8000ba78]:sd a2, 808(fp)
[0x8000ba7c]:lui s1, 3
[0x8000ba80]:add a1, a1, s1
[0x8000ba84]:ld t5, 864(a1)
[0x8000ba88]:sub a1, a1, s1
[0x8000ba8c]:lui s1, 3
[0x8000ba90]:add a1, a1, s1
[0x8000ba94]:ld t4, 872(a1)
[0x8000ba98]:sub a1, a1, s1
[0x8000ba9c]:addi s1, zero, 0
[0x8000baa0]:csrrw zero, fcsr, s1
[0x8000baa4]:fmax.s t6, t5, t4

[0x8000baa4]:fmax.s t6, t5, t4
[0x8000baa8]:csrrs a2, fcsr, zero
[0x8000baac]:sd t6, 816(fp)
[0x8000bab0]:sd a2, 824(fp)
[0x8000bab4]:lui s1, 3
[0x8000bab8]:add a1, a1, s1
[0x8000babc]:ld t5, 880(a1)
[0x8000bac0]:sub a1, a1, s1
[0x8000bac4]:lui s1, 3
[0x8000bac8]:add a1, a1, s1
[0x8000bacc]:ld t4, 888(a1)
[0x8000bad0]:sub a1, a1, s1
[0x8000bad4]:addi s1, zero, 0
[0x8000bad8]:csrrw zero, fcsr, s1
[0x8000badc]:fmax.s t6, t5, t4

[0x8000badc]:fmax.s t6, t5, t4
[0x8000bae0]:csrrs a2, fcsr, zero
[0x8000bae4]:sd t6, 832(fp)
[0x8000bae8]:sd a2, 840(fp)
[0x8000baec]:lui s1, 3
[0x8000baf0]:add a1, a1, s1
[0x8000baf4]:ld t5, 896(a1)
[0x8000baf8]:sub a1, a1, s1
[0x8000bafc]:lui s1, 3
[0x8000bb00]:add a1, a1, s1
[0x8000bb04]:ld t4, 904(a1)
[0x8000bb08]:sub a1, a1, s1
[0x8000bb0c]:addi s1, zero, 0
[0x8000bb10]:csrrw zero, fcsr, s1
[0x8000bb14]:fmax.s t6, t5, t4

[0x8000bb14]:fmax.s t6, t5, t4
[0x8000bb18]:csrrs a2, fcsr, zero
[0x8000bb1c]:sd t6, 848(fp)
[0x8000bb20]:sd a2, 856(fp)
[0x8000bb24]:lui s1, 3
[0x8000bb28]:add a1, a1, s1
[0x8000bb2c]:ld t5, 912(a1)
[0x8000bb30]:sub a1, a1, s1
[0x8000bb34]:lui s1, 3
[0x8000bb38]:add a1, a1, s1
[0x8000bb3c]:ld t4, 920(a1)
[0x8000bb40]:sub a1, a1, s1
[0x8000bb44]:addi s1, zero, 0
[0x8000bb48]:csrrw zero, fcsr, s1
[0x8000bb4c]:fmax.s t6, t5, t4

[0x8000bb4c]:fmax.s t6, t5, t4
[0x8000bb50]:csrrs a2, fcsr, zero
[0x8000bb54]:sd t6, 864(fp)
[0x8000bb58]:sd a2, 872(fp)
[0x8000bb5c]:lui s1, 3
[0x8000bb60]:add a1, a1, s1
[0x8000bb64]:ld t5, 928(a1)
[0x8000bb68]:sub a1, a1, s1
[0x8000bb6c]:lui s1, 3
[0x8000bb70]:add a1, a1, s1
[0x8000bb74]:ld t4, 936(a1)
[0x8000bb78]:sub a1, a1, s1
[0x8000bb7c]:addi s1, zero, 0
[0x8000bb80]:csrrw zero, fcsr, s1
[0x8000bb84]:fmax.s t6, t5, t4

[0x8000bb84]:fmax.s t6, t5, t4
[0x8000bb88]:csrrs a2, fcsr, zero
[0x8000bb8c]:sd t6, 880(fp)
[0x8000bb90]:sd a2, 888(fp)
[0x8000bb94]:lui s1, 3
[0x8000bb98]:add a1, a1, s1
[0x8000bb9c]:ld t5, 944(a1)
[0x8000bba0]:sub a1, a1, s1
[0x8000bba4]:lui s1, 3
[0x8000bba8]:add a1, a1, s1
[0x8000bbac]:ld t4, 952(a1)
[0x8000bbb0]:sub a1, a1, s1
[0x8000bbb4]:addi s1, zero, 0
[0x8000bbb8]:csrrw zero, fcsr, s1
[0x8000bbbc]:fmax.s t6, t5, t4

[0x8000bbbc]:fmax.s t6, t5, t4
[0x8000bbc0]:csrrs a2, fcsr, zero
[0x8000bbc4]:sd t6, 896(fp)
[0x8000bbc8]:sd a2, 904(fp)
[0x8000bbcc]:lui s1, 3
[0x8000bbd0]:add a1, a1, s1
[0x8000bbd4]:ld t5, 960(a1)
[0x8000bbd8]:sub a1, a1, s1
[0x8000bbdc]:lui s1, 3
[0x8000bbe0]:add a1, a1, s1
[0x8000bbe4]:ld t4, 968(a1)
[0x8000bbe8]:sub a1, a1, s1
[0x8000bbec]:addi s1, zero, 0
[0x8000bbf0]:csrrw zero, fcsr, s1
[0x8000bbf4]:fmax.s t6, t5, t4

[0x8000bbf4]:fmax.s t6, t5, t4
[0x8000bbf8]:csrrs a2, fcsr, zero
[0x8000bbfc]:sd t6, 912(fp)
[0x8000bc00]:sd a2, 920(fp)
[0x8000bc04]:lui s1, 3
[0x8000bc08]:add a1, a1, s1
[0x8000bc0c]:ld t5, 976(a1)
[0x8000bc10]:sub a1, a1, s1
[0x8000bc14]:lui s1, 3
[0x8000bc18]:add a1, a1, s1
[0x8000bc1c]:ld t4, 984(a1)
[0x8000bc20]:sub a1, a1, s1
[0x8000bc24]:addi s1, zero, 0
[0x8000bc28]:csrrw zero, fcsr, s1
[0x8000bc2c]:fmax.s t6, t5, t4

[0x8000bc2c]:fmax.s t6, t5, t4
[0x8000bc30]:csrrs a2, fcsr, zero
[0x8000bc34]:sd t6, 928(fp)
[0x8000bc38]:sd a2, 936(fp)
[0x8000bc3c]:lui s1, 3
[0x8000bc40]:add a1, a1, s1
[0x8000bc44]:ld t5, 992(a1)
[0x8000bc48]:sub a1, a1, s1
[0x8000bc4c]:lui s1, 3
[0x8000bc50]:add a1, a1, s1
[0x8000bc54]:ld t4, 1000(a1)
[0x8000bc58]:sub a1, a1, s1
[0x8000bc5c]:addi s1, zero, 0
[0x8000bc60]:csrrw zero, fcsr, s1
[0x8000bc64]:fmax.s t6, t5, t4

[0x8000bc64]:fmax.s t6, t5, t4
[0x8000bc68]:csrrs a2, fcsr, zero
[0x8000bc6c]:sd t6, 944(fp)
[0x8000bc70]:sd a2, 952(fp)
[0x8000bc74]:lui s1, 3
[0x8000bc78]:add a1, a1, s1
[0x8000bc7c]:ld t5, 1008(a1)
[0x8000bc80]:sub a1, a1, s1
[0x8000bc84]:lui s1, 3
[0x8000bc88]:add a1, a1, s1
[0x8000bc8c]:ld t4, 1016(a1)
[0x8000bc90]:sub a1, a1, s1
[0x8000bc94]:addi s1, zero, 0
[0x8000bc98]:csrrw zero, fcsr, s1
[0x8000bc9c]:fmax.s t6, t5, t4

[0x8000bc9c]:fmax.s t6, t5, t4
[0x8000bca0]:csrrs a2, fcsr, zero
[0x8000bca4]:sd t6, 960(fp)
[0x8000bca8]:sd a2, 968(fp)
[0x8000bcac]:lui s1, 3
[0x8000bcb0]:add a1, a1, s1
[0x8000bcb4]:ld t5, 1024(a1)
[0x8000bcb8]:sub a1, a1, s1
[0x8000bcbc]:lui s1, 3
[0x8000bcc0]:add a1, a1, s1
[0x8000bcc4]:ld t4, 1032(a1)
[0x8000bcc8]:sub a1, a1, s1
[0x8000bccc]:addi s1, zero, 0
[0x8000bcd0]:csrrw zero, fcsr, s1
[0x8000bcd4]:fmax.s t6, t5, t4

[0x8000bcd4]:fmax.s t6, t5, t4
[0x8000bcd8]:csrrs a2, fcsr, zero
[0x8000bcdc]:sd t6, 976(fp)
[0x8000bce0]:sd a2, 984(fp)
[0x8000bce4]:lui s1, 3
[0x8000bce8]:add a1, a1, s1
[0x8000bcec]:ld t5, 1040(a1)
[0x8000bcf0]:sub a1, a1, s1
[0x8000bcf4]:lui s1, 3
[0x8000bcf8]:add a1, a1, s1
[0x8000bcfc]:ld t4, 1048(a1)
[0x8000bd00]:sub a1, a1, s1
[0x8000bd04]:addi s1, zero, 0
[0x8000bd08]:csrrw zero, fcsr, s1
[0x8000bd0c]:fmax.s t6, t5, t4

[0x8000bd0c]:fmax.s t6, t5, t4
[0x8000bd10]:csrrs a2, fcsr, zero
[0x8000bd14]:sd t6, 992(fp)
[0x8000bd18]:sd a2, 1000(fp)
[0x8000bd1c]:lui s1, 3
[0x8000bd20]:add a1, a1, s1
[0x8000bd24]:ld t5, 1056(a1)
[0x8000bd28]:sub a1, a1, s1
[0x8000bd2c]:lui s1, 3
[0x8000bd30]:add a1, a1, s1
[0x8000bd34]:ld t4, 1064(a1)
[0x8000bd38]:sub a1, a1, s1
[0x8000bd3c]:addi s1, zero, 0
[0x8000bd40]:csrrw zero, fcsr, s1
[0x8000bd44]:fmax.s t6, t5, t4

[0x8000bd44]:fmax.s t6, t5, t4
[0x8000bd48]:csrrs a2, fcsr, zero
[0x8000bd4c]:sd t6, 1008(fp)
[0x8000bd50]:sd a2, 1016(fp)
[0x8000bd54]:lui s1, 3
[0x8000bd58]:add a1, a1, s1
[0x8000bd5c]:ld t5, 1072(a1)
[0x8000bd60]:sub a1, a1, s1
[0x8000bd64]:lui s1, 3
[0x8000bd68]:add a1, a1, s1
[0x8000bd6c]:ld t4, 1080(a1)
[0x8000bd70]:sub a1, a1, s1
[0x8000bd74]:addi s1, zero, 0
[0x8000bd78]:csrrw zero, fcsr, s1
[0x8000bd7c]:fmax.s t6, t5, t4

[0x8000bd7c]:fmax.s t6, t5, t4
[0x8000bd80]:csrrs a2, fcsr, zero
[0x8000bd84]:sd t6, 1024(fp)
[0x8000bd88]:sd a2, 1032(fp)
[0x8000bd8c]:lui s1, 3
[0x8000bd90]:add a1, a1, s1
[0x8000bd94]:ld t5, 1088(a1)
[0x8000bd98]:sub a1, a1, s1
[0x8000bd9c]:lui s1, 3
[0x8000bda0]:add a1, a1, s1
[0x8000bda4]:ld t4, 1096(a1)
[0x8000bda8]:sub a1, a1, s1
[0x8000bdac]:addi s1, zero, 0
[0x8000bdb0]:csrrw zero, fcsr, s1
[0x8000bdb4]:fmax.s t6, t5, t4

[0x8000bdb4]:fmax.s t6, t5, t4
[0x8000bdb8]:csrrs a2, fcsr, zero
[0x8000bdbc]:sd t6, 1040(fp)
[0x8000bdc0]:sd a2, 1048(fp)
[0x8000bdc4]:lui s1, 3
[0x8000bdc8]:add a1, a1, s1
[0x8000bdcc]:ld t5, 1104(a1)
[0x8000bdd0]:sub a1, a1, s1
[0x8000bdd4]:lui s1, 3
[0x8000bdd8]:add a1, a1, s1
[0x8000bddc]:ld t4, 1112(a1)
[0x8000bde0]:sub a1, a1, s1
[0x8000bde4]:addi s1, zero, 0
[0x8000bde8]:csrrw zero, fcsr, s1
[0x8000bdec]:fmax.s t6, t5, t4

[0x8000bdec]:fmax.s t6, t5, t4
[0x8000bdf0]:csrrs a2, fcsr, zero
[0x8000bdf4]:sd t6, 1056(fp)
[0x8000bdf8]:sd a2, 1064(fp)
[0x8000bdfc]:lui s1, 3
[0x8000be00]:add a1, a1, s1
[0x8000be04]:ld t5, 1120(a1)
[0x8000be08]:sub a1, a1, s1
[0x8000be0c]:lui s1, 3
[0x8000be10]:add a1, a1, s1
[0x8000be14]:ld t4, 1128(a1)
[0x8000be18]:sub a1, a1, s1
[0x8000be1c]:addi s1, zero, 0
[0x8000be20]:csrrw zero, fcsr, s1
[0x8000be24]:fmax.s t6, t5, t4

[0x8000be24]:fmax.s t6, t5, t4
[0x8000be28]:csrrs a2, fcsr, zero
[0x8000be2c]:sd t6, 1072(fp)
[0x8000be30]:sd a2, 1080(fp)
[0x8000be34]:lui s1, 3
[0x8000be38]:add a1, a1, s1
[0x8000be3c]:ld t5, 1136(a1)
[0x8000be40]:sub a1, a1, s1
[0x8000be44]:lui s1, 3
[0x8000be48]:add a1, a1, s1
[0x8000be4c]:ld t4, 1144(a1)
[0x8000be50]:sub a1, a1, s1
[0x8000be54]:addi s1, zero, 0
[0x8000be58]:csrrw zero, fcsr, s1
[0x8000be5c]:fmax.s t6, t5, t4

[0x8000be5c]:fmax.s t6, t5, t4
[0x8000be60]:csrrs a2, fcsr, zero
[0x8000be64]:sd t6, 1088(fp)
[0x8000be68]:sd a2, 1096(fp)
[0x8000be6c]:lui s1, 3
[0x8000be70]:add a1, a1, s1
[0x8000be74]:ld t5, 1152(a1)
[0x8000be78]:sub a1, a1, s1
[0x8000be7c]:lui s1, 3
[0x8000be80]:add a1, a1, s1
[0x8000be84]:ld t4, 1160(a1)
[0x8000be88]:sub a1, a1, s1
[0x8000be8c]:addi s1, zero, 0
[0x8000be90]:csrrw zero, fcsr, s1
[0x8000be94]:fmax.s t6, t5, t4

[0x8000be94]:fmax.s t6, t5, t4
[0x8000be98]:csrrs a2, fcsr, zero
[0x8000be9c]:sd t6, 1104(fp)
[0x8000bea0]:sd a2, 1112(fp)
[0x8000bea4]:lui s1, 3
[0x8000bea8]:add a1, a1, s1
[0x8000beac]:ld t5, 1168(a1)
[0x8000beb0]:sub a1, a1, s1
[0x8000beb4]:lui s1, 3
[0x8000beb8]:add a1, a1, s1
[0x8000bebc]:ld t4, 1176(a1)
[0x8000bec0]:sub a1, a1, s1
[0x8000bec4]:addi s1, zero, 0
[0x8000bec8]:csrrw zero, fcsr, s1
[0x8000becc]:fmax.s t6, t5, t4

[0x8000becc]:fmax.s t6, t5, t4
[0x8000bed0]:csrrs a2, fcsr, zero
[0x8000bed4]:sd t6, 1120(fp)
[0x8000bed8]:sd a2, 1128(fp)
[0x8000bedc]:lui s1, 3
[0x8000bee0]:add a1, a1, s1
[0x8000bee4]:ld t5, 1184(a1)
[0x8000bee8]:sub a1, a1, s1
[0x8000beec]:lui s1, 3
[0x8000bef0]:add a1, a1, s1
[0x8000bef4]:ld t4, 1192(a1)
[0x8000bef8]:sub a1, a1, s1
[0x8000befc]:addi s1, zero, 0
[0x8000bf00]:csrrw zero, fcsr, s1
[0x8000bf04]:fmax.s t6, t5, t4

[0x8000bf04]:fmax.s t6, t5, t4
[0x8000bf08]:csrrs a2, fcsr, zero
[0x8000bf0c]:sd t6, 1136(fp)
[0x8000bf10]:sd a2, 1144(fp)
[0x8000bf14]:lui s1, 3
[0x8000bf18]:add a1, a1, s1
[0x8000bf1c]:ld t5, 1200(a1)
[0x8000bf20]:sub a1, a1, s1
[0x8000bf24]:lui s1, 3
[0x8000bf28]:add a1, a1, s1
[0x8000bf2c]:ld t4, 1208(a1)
[0x8000bf30]:sub a1, a1, s1
[0x8000bf34]:addi s1, zero, 0
[0x8000bf38]:csrrw zero, fcsr, s1
[0x8000bf3c]:fmax.s t6, t5, t4

[0x8000bf3c]:fmax.s t6, t5, t4
[0x8000bf40]:csrrs a2, fcsr, zero
[0x8000bf44]:sd t6, 1152(fp)
[0x8000bf48]:sd a2, 1160(fp)
[0x8000bf4c]:lui s1, 3
[0x8000bf50]:add a1, a1, s1
[0x8000bf54]:ld t5, 1216(a1)
[0x8000bf58]:sub a1, a1, s1
[0x8000bf5c]:lui s1, 3
[0x8000bf60]:add a1, a1, s1
[0x8000bf64]:ld t4, 1224(a1)
[0x8000bf68]:sub a1, a1, s1
[0x8000bf6c]:addi s1, zero, 0
[0x8000bf70]:csrrw zero, fcsr, s1
[0x8000bf74]:fmax.s t6, t5, t4

[0x8000bf74]:fmax.s t6, t5, t4
[0x8000bf78]:csrrs a2, fcsr, zero
[0x8000bf7c]:sd t6, 1168(fp)
[0x8000bf80]:sd a2, 1176(fp)
[0x8000bf84]:lui s1, 3
[0x8000bf88]:add a1, a1, s1
[0x8000bf8c]:ld t5, 1232(a1)
[0x8000bf90]:sub a1, a1, s1
[0x8000bf94]:lui s1, 3
[0x8000bf98]:add a1, a1, s1
[0x8000bf9c]:ld t4, 1240(a1)
[0x8000bfa0]:sub a1, a1, s1
[0x8000bfa4]:addi s1, zero, 0
[0x8000bfa8]:csrrw zero, fcsr, s1
[0x8000bfac]:fmax.s t6, t5, t4

[0x8000bfac]:fmax.s t6, t5, t4
[0x8000bfb0]:csrrs a2, fcsr, zero
[0x8000bfb4]:sd t6, 1184(fp)
[0x8000bfb8]:sd a2, 1192(fp)
[0x8000bfbc]:lui s1, 3
[0x8000bfc0]:add a1, a1, s1
[0x8000bfc4]:ld t5, 1248(a1)
[0x8000bfc8]:sub a1, a1, s1
[0x8000bfcc]:lui s1, 3
[0x8000bfd0]:add a1, a1, s1
[0x8000bfd4]:ld t4, 1256(a1)
[0x8000bfd8]:sub a1, a1, s1
[0x8000bfdc]:addi s1, zero, 0
[0x8000bfe0]:csrrw zero, fcsr, s1
[0x8000bfe4]:fmax.s t6, t5, t4

[0x8000bfe4]:fmax.s t6, t5, t4
[0x8000bfe8]:csrrs a2, fcsr, zero
[0x8000bfec]:sd t6, 1200(fp)
[0x8000bff0]:sd a2, 1208(fp)
[0x8000bff4]:lui s1, 3
[0x8000bff8]:add a1, a1, s1
[0x8000bffc]:ld t5, 1264(a1)
[0x8000c000]:sub a1, a1, s1
[0x8000c004]:lui s1, 3
[0x8000c008]:add a1, a1, s1
[0x8000c00c]:ld t4, 1272(a1)
[0x8000c010]:sub a1, a1, s1
[0x8000c014]:addi s1, zero, 0
[0x8000c018]:csrrw zero, fcsr, s1
[0x8000c01c]:fmax.s t6, t5, t4

[0x8000c01c]:fmax.s t6, t5, t4
[0x8000c020]:csrrs a2, fcsr, zero
[0x8000c024]:sd t6, 1216(fp)
[0x8000c028]:sd a2, 1224(fp)
[0x8000c02c]:lui s1, 3
[0x8000c030]:add a1, a1, s1
[0x8000c034]:ld t5, 1280(a1)
[0x8000c038]:sub a1, a1, s1
[0x8000c03c]:lui s1, 3
[0x8000c040]:add a1, a1, s1
[0x8000c044]:ld t4, 1288(a1)
[0x8000c048]:sub a1, a1, s1
[0x8000c04c]:addi s1, zero, 0
[0x8000c050]:csrrw zero, fcsr, s1
[0x8000c054]:fmax.s t6, t5, t4

[0x8000c054]:fmax.s t6, t5, t4
[0x8000c058]:csrrs a2, fcsr, zero
[0x8000c05c]:sd t6, 1232(fp)
[0x8000c060]:sd a2, 1240(fp)
[0x8000c064]:lui s1, 3
[0x8000c068]:add a1, a1, s1
[0x8000c06c]:ld t5, 1296(a1)
[0x8000c070]:sub a1, a1, s1
[0x8000c074]:lui s1, 3
[0x8000c078]:add a1, a1, s1
[0x8000c07c]:ld t4, 1304(a1)
[0x8000c080]:sub a1, a1, s1
[0x8000c084]:addi s1, zero, 0
[0x8000c088]:csrrw zero, fcsr, s1
[0x8000c08c]:fmax.s t6, t5, t4

[0x8000c08c]:fmax.s t6, t5, t4
[0x8000c090]:csrrs a2, fcsr, zero
[0x8000c094]:sd t6, 1248(fp)
[0x8000c098]:sd a2, 1256(fp)
[0x8000c09c]:lui s1, 3
[0x8000c0a0]:add a1, a1, s1
[0x8000c0a4]:ld t5, 1312(a1)
[0x8000c0a8]:sub a1, a1, s1
[0x8000c0ac]:lui s1, 3
[0x8000c0b0]:add a1, a1, s1
[0x8000c0b4]:ld t4, 1320(a1)
[0x8000c0b8]:sub a1, a1, s1
[0x8000c0bc]:addi s1, zero, 0
[0x8000c0c0]:csrrw zero, fcsr, s1
[0x8000c0c4]:fmax.s t6, t5, t4

[0x8000c0c4]:fmax.s t6, t5, t4
[0x8000c0c8]:csrrs a2, fcsr, zero
[0x8000c0cc]:sd t6, 1264(fp)
[0x8000c0d0]:sd a2, 1272(fp)
[0x8000c0d4]:lui s1, 3
[0x8000c0d8]:add a1, a1, s1
[0x8000c0dc]:ld t5, 1328(a1)
[0x8000c0e0]:sub a1, a1, s1
[0x8000c0e4]:lui s1, 3
[0x8000c0e8]:add a1, a1, s1
[0x8000c0ec]:ld t4, 1336(a1)
[0x8000c0f0]:sub a1, a1, s1
[0x8000c0f4]:addi s1, zero, 0
[0x8000c0f8]:csrrw zero, fcsr, s1
[0x8000c0fc]:fmax.s t6, t5, t4

[0x8000c0fc]:fmax.s t6, t5, t4
[0x8000c100]:csrrs a2, fcsr, zero
[0x8000c104]:sd t6, 1280(fp)
[0x8000c108]:sd a2, 1288(fp)
[0x8000c10c]:lui s1, 3
[0x8000c110]:add a1, a1, s1
[0x8000c114]:ld t5, 1344(a1)
[0x8000c118]:sub a1, a1, s1
[0x8000c11c]:lui s1, 3
[0x8000c120]:add a1, a1, s1
[0x8000c124]:ld t4, 1352(a1)
[0x8000c128]:sub a1, a1, s1
[0x8000c12c]:addi s1, zero, 0
[0x8000c130]:csrrw zero, fcsr, s1
[0x8000c134]:fmax.s t6, t5, t4

[0x8000c134]:fmax.s t6, t5, t4
[0x8000c138]:csrrs a2, fcsr, zero
[0x8000c13c]:sd t6, 1296(fp)
[0x8000c140]:sd a2, 1304(fp)
[0x8000c144]:lui s1, 3
[0x8000c148]:add a1, a1, s1
[0x8000c14c]:ld t5, 1360(a1)
[0x8000c150]:sub a1, a1, s1
[0x8000c154]:lui s1, 3
[0x8000c158]:add a1, a1, s1
[0x8000c15c]:ld t4, 1368(a1)
[0x8000c160]:sub a1, a1, s1
[0x8000c164]:addi s1, zero, 0
[0x8000c168]:csrrw zero, fcsr, s1
[0x8000c16c]:fmax.s t6, t5, t4

[0x8000c16c]:fmax.s t6, t5, t4
[0x8000c170]:csrrs a2, fcsr, zero
[0x8000c174]:sd t6, 1312(fp)
[0x8000c178]:sd a2, 1320(fp)
[0x8000c17c]:lui s1, 3
[0x8000c180]:add a1, a1, s1
[0x8000c184]:ld t5, 1376(a1)
[0x8000c188]:sub a1, a1, s1
[0x8000c18c]:lui s1, 3
[0x8000c190]:add a1, a1, s1
[0x8000c194]:ld t4, 1384(a1)
[0x8000c198]:sub a1, a1, s1
[0x8000c19c]:addi s1, zero, 0
[0x8000c1a0]:csrrw zero, fcsr, s1
[0x8000c1a4]:fmax.s t6, t5, t4

[0x8000c1a4]:fmax.s t6, t5, t4
[0x8000c1a8]:csrrs a2, fcsr, zero
[0x8000c1ac]:sd t6, 1328(fp)
[0x8000c1b0]:sd a2, 1336(fp)
[0x8000c1b4]:lui s1, 3
[0x8000c1b8]:add a1, a1, s1
[0x8000c1bc]:ld t5, 1392(a1)
[0x8000c1c0]:sub a1, a1, s1
[0x8000c1c4]:lui s1, 3
[0x8000c1c8]:add a1, a1, s1
[0x8000c1cc]:ld t4, 1400(a1)
[0x8000c1d0]:sub a1, a1, s1
[0x8000c1d4]:addi s1, zero, 0
[0x8000c1d8]:csrrw zero, fcsr, s1
[0x8000c1dc]:fmax.s t6, t5, t4

[0x8000c1dc]:fmax.s t6, t5, t4
[0x8000c1e0]:csrrs a2, fcsr, zero
[0x8000c1e4]:sd t6, 1344(fp)
[0x8000c1e8]:sd a2, 1352(fp)
[0x8000c1ec]:lui s1, 3
[0x8000c1f0]:add a1, a1, s1
[0x8000c1f4]:ld t5, 1408(a1)
[0x8000c1f8]:sub a1, a1, s1
[0x8000c1fc]:lui s1, 3
[0x8000c200]:add a1, a1, s1
[0x8000c204]:ld t4, 1416(a1)
[0x8000c208]:sub a1, a1, s1
[0x8000c20c]:addi s1, zero, 0
[0x8000c210]:csrrw zero, fcsr, s1
[0x8000c214]:fmax.s t6, t5, t4

[0x8000c214]:fmax.s t6, t5, t4
[0x8000c218]:csrrs a2, fcsr, zero
[0x8000c21c]:sd t6, 1360(fp)
[0x8000c220]:sd a2, 1368(fp)
[0x8000c224]:lui s1, 3
[0x8000c228]:add a1, a1, s1
[0x8000c22c]:ld t5, 1424(a1)
[0x8000c230]:sub a1, a1, s1
[0x8000c234]:lui s1, 3
[0x8000c238]:add a1, a1, s1
[0x8000c23c]:ld t4, 1432(a1)
[0x8000c240]:sub a1, a1, s1
[0x8000c244]:addi s1, zero, 0
[0x8000c248]:csrrw zero, fcsr, s1
[0x8000c24c]:fmax.s t6, t5, t4

[0x8000c24c]:fmax.s t6, t5, t4
[0x8000c250]:csrrs a2, fcsr, zero
[0x8000c254]:sd t6, 1376(fp)
[0x8000c258]:sd a2, 1384(fp)
[0x8000c25c]:lui s1, 3
[0x8000c260]:add a1, a1, s1
[0x8000c264]:ld t5, 1440(a1)
[0x8000c268]:sub a1, a1, s1
[0x8000c26c]:lui s1, 3
[0x8000c270]:add a1, a1, s1
[0x8000c274]:ld t4, 1448(a1)
[0x8000c278]:sub a1, a1, s1
[0x8000c27c]:addi s1, zero, 0
[0x8000c280]:csrrw zero, fcsr, s1
[0x8000c284]:fmax.s t6, t5, t4

[0x8000c284]:fmax.s t6, t5, t4
[0x8000c288]:csrrs a2, fcsr, zero
[0x8000c28c]:sd t6, 1392(fp)
[0x8000c290]:sd a2, 1400(fp)
[0x8000c294]:lui s1, 3
[0x8000c298]:add a1, a1, s1
[0x8000c29c]:ld t5, 1456(a1)
[0x8000c2a0]:sub a1, a1, s1
[0x8000c2a4]:lui s1, 3
[0x8000c2a8]:add a1, a1, s1
[0x8000c2ac]:ld t4, 1464(a1)
[0x8000c2b0]:sub a1, a1, s1
[0x8000c2b4]:addi s1, zero, 0
[0x8000c2b8]:csrrw zero, fcsr, s1
[0x8000c2bc]:fmax.s t6, t5, t4

[0x8000c2bc]:fmax.s t6, t5, t4
[0x8000c2c0]:csrrs a2, fcsr, zero
[0x8000c2c4]:sd t6, 1408(fp)
[0x8000c2c8]:sd a2, 1416(fp)
[0x8000c2cc]:lui s1, 3
[0x8000c2d0]:add a1, a1, s1
[0x8000c2d4]:ld t5, 1472(a1)
[0x8000c2d8]:sub a1, a1, s1
[0x8000c2dc]:lui s1, 3
[0x8000c2e0]:add a1, a1, s1
[0x8000c2e4]:ld t4, 1480(a1)
[0x8000c2e8]:sub a1, a1, s1
[0x8000c2ec]:addi s1, zero, 0
[0x8000c2f0]:csrrw zero, fcsr, s1
[0x8000c2f4]:fmax.s t6, t5, t4

[0x8000c2f4]:fmax.s t6, t5, t4
[0x8000c2f8]:csrrs a2, fcsr, zero
[0x8000c2fc]:sd t6, 1424(fp)
[0x8000c300]:sd a2, 1432(fp)
[0x8000c304]:lui s1, 3
[0x8000c308]:add a1, a1, s1
[0x8000c30c]:ld t5, 1488(a1)
[0x8000c310]:sub a1, a1, s1
[0x8000c314]:lui s1, 3
[0x8000c318]:add a1, a1, s1
[0x8000c31c]:ld t4, 1496(a1)
[0x8000c320]:sub a1, a1, s1
[0x8000c324]:addi s1, zero, 0
[0x8000c328]:csrrw zero, fcsr, s1
[0x8000c32c]:fmax.s t6, t5, t4

[0x8000c32c]:fmax.s t6, t5, t4
[0x8000c330]:csrrs a2, fcsr, zero
[0x8000c334]:sd t6, 1440(fp)
[0x8000c338]:sd a2, 1448(fp)
[0x8000c33c]:lui s1, 3
[0x8000c340]:add a1, a1, s1
[0x8000c344]:ld t5, 1504(a1)
[0x8000c348]:sub a1, a1, s1
[0x8000c34c]:lui s1, 3
[0x8000c350]:add a1, a1, s1
[0x8000c354]:ld t4, 1512(a1)
[0x8000c358]:sub a1, a1, s1
[0x8000c35c]:addi s1, zero, 0
[0x8000c360]:csrrw zero, fcsr, s1
[0x8000c364]:fmax.s t6, t5, t4

[0x8000c364]:fmax.s t6, t5, t4
[0x8000c368]:csrrs a2, fcsr, zero
[0x8000c36c]:sd t6, 1456(fp)
[0x8000c370]:sd a2, 1464(fp)
[0x8000c374]:lui s1, 3
[0x8000c378]:add a1, a1, s1
[0x8000c37c]:ld t5, 1520(a1)
[0x8000c380]:sub a1, a1, s1
[0x8000c384]:lui s1, 3
[0x8000c388]:add a1, a1, s1
[0x8000c38c]:ld t4, 1528(a1)
[0x8000c390]:sub a1, a1, s1
[0x8000c394]:addi s1, zero, 0
[0x8000c398]:csrrw zero, fcsr, s1
[0x8000c39c]:fmax.s t6, t5, t4

[0x8000c39c]:fmax.s t6, t5, t4
[0x8000c3a0]:csrrs a2, fcsr, zero
[0x8000c3a4]:sd t6, 1472(fp)
[0x8000c3a8]:sd a2, 1480(fp)
[0x8000c3ac]:lui s1, 3
[0x8000c3b0]:add a1, a1, s1
[0x8000c3b4]:ld t5, 1536(a1)
[0x8000c3b8]:sub a1, a1, s1
[0x8000c3bc]:lui s1, 3
[0x8000c3c0]:add a1, a1, s1
[0x8000c3c4]:ld t4, 1544(a1)
[0x8000c3c8]:sub a1, a1, s1
[0x8000c3cc]:addi s1, zero, 0
[0x8000c3d0]:csrrw zero, fcsr, s1
[0x8000c3d4]:fmax.s t6, t5, t4

[0x8000c3d4]:fmax.s t6, t5, t4
[0x8000c3d8]:csrrs a2, fcsr, zero
[0x8000c3dc]:sd t6, 1488(fp)
[0x8000c3e0]:sd a2, 1496(fp)
[0x8000c3e4]:lui s1, 3
[0x8000c3e8]:add a1, a1, s1
[0x8000c3ec]:ld t5, 1552(a1)
[0x8000c3f0]:sub a1, a1, s1
[0x8000c3f4]:lui s1, 3
[0x8000c3f8]:add a1, a1, s1
[0x8000c3fc]:ld t4, 1560(a1)
[0x8000c400]:sub a1, a1, s1
[0x8000c404]:addi s1, zero, 0
[0x8000c408]:csrrw zero, fcsr, s1
[0x8000c40c]:fmax.s t6, t5, t4

[0x8000c40c]:fmax.s t6, t5, t4
[0x8000c410]:csrrs a2, fcsr, zero
[0x8000c414]:sd t6, 1504(fp)
[0x8000c418]:sd a2, 1512(fp)
[0x8000c41c]:lui s1, 3
[0x8000c420]:add a1, a1, s1
[0x8000c424]:ld t5, 1568(a1)
[0x8000c428]:sub a1, a1, s1
[0x8000c42c]:lui s1, 3
[0x8000c430]:add a1, a1, s1
[0x8000c434]:ld t4, 1576(a1)
[0x8000c438]:sub a1, a1, s1
[0x8000c43c]:addi s1, zero, 0
[0x8000c440]:csrrw zero, fcsr, s1
[0x8000c444]:fmax.s t6, t5, t4

[0x8000c444]:fmax.s t6, t5, t4
[0x8000c448]:csrrs a2, fcsr, zero
[0x8000c44c]:sd t6, 1520(fp)
[0x8000c450]:sd a2, 1528(fp)
[0x8000c454]:lui s1, 3
[0x8000c458]:add a1, a1, s1
[0x8000c45c]:ld t5, 1584(a1)
[0x8000c460]:sub a1, a1, s1
[0x8000c464]:lui s1, 3
[0x8000c468]:add a1, a1, s1
[0x8000c46c]:ld t4, 1592(a1)
[0x8000c470]:sub a1, a1, s1
[0x8000c474]:addi s1, zero, 0
[0x8000c478]:csrrw zero, fcsr, s1
[0x8000c47c]:fmax.s t6, t5, t4

[0x8000c47c]:fmax.s t6, t5, t4
[0x8000c480]:csrrs a2, fcsr, zero
[0x8000c484]:sd t6, 1536(fp)
[0x8000c488]:sd a2, 1544(fp)
[0x8000c48c]:lui s1, 3
[0x8000c490]:add a1, a1, s1
[0x8000c494]:ld t5, 1600(a1)
[0x8000c498]:sub a1, a1, s1
[0x8000c49c]:lui s1, 3
[0x8000c4a0]:add a1, a1, s1
[0x8000c4a4]:ld t4, 1608(a1)
[0x8000c4a8]:sub a1, a1, s1
[0x8000c4ac]:addi s1, zero, 0
[0x8000c4b0]:csrrw zero, fcsr, s1
[0x8000c4b4]:fmax.s t6, t5, t4

[0x8000c4b4]:fmax.s t6, t5, t4
[0x8000c4b8]:csrrs a2, fcsr, zero
[0x8000c4bc]:sd t6, 1552(fp)
[0x8000c4c0]:sd a2, 1560(fp)
[0x8000c4c4]:lui s1, 3
[0x8000c4c8]:add a1, a1, s1
[0x8000c4cc]:ld t5, 1616(a1)
[0x8000c4d0]:sub a1, a1, s1
[0x8000c4d4]:lui s1, 3
[0x8000c4d8]:add a1, a1, s1
[0x8000c4dc]:ld t4, 1624(a1)
[0x8000c4e0]:sub a1, a1, s1
[0x8000c4e4]:addi s1, zero, 0
[0x8000c4e8]:csrrw zero, fcsr, s1
[0x8000c4ec]:fmax.s t6, t5, t4

[0x8000c4ec]:fmax.s t6, t5, t4
[0x8000c4f0]:csrrs a2, fcsr, zero
[0x8000c4f4]:sd t6, 1568(fp)
[0x8000c4f8]:sd a2, 1576(fp)
[0x8000c4fc]:lui s1, 3
[0x8000c500]:add a1, a1, s1
[0x8000c504]:ld t5, 1632(a1)
[0x8000c508]:sub a1, a1, s1
[0x8000c50c]:lui s1, 3
[0x8000c510]:add a1, a1, s1
[0x8000c514]:ld t4, 1640(a1)
[0x8000c518]:sub a1, a1, s1
[0x8000c51c]:addi s1, zero, 0
[0x8000c520]:csrrw zero, fcsr, s1
[0x8000c524]:fmax.s t6, t5, t4

[0x8000c524]:fmax.s t6, t5, t4
[0x8000c528]:csrrs a2, fcsr, zero
[0x8000c52c]:sd t6, 1584(fp)
[0x8000c530]:sd a2, 1592(fp)
[0x8000c534]:lui s1, 3
[0x8000c538]:add a1, a1, s1
[0x8000c53c]:ld t5, 1648(a1)
[0x8000c540]:sub a1, a1, s1
[0x8000c544]:lui s1, 3
[0x8000c548]:add a1, a1, s1
[0x8000c54c]:ld t4, 1656(a1)
[0x8000c550]:sub a1, a1, s1
[0x8000c554]:addi s1, zero, 0
[0x8000c558]:csrrw zero, fcsr, s1
[0x8000c55c]:fmax.s t6, t5, t4

[0x8000c55c]:fmax.s t6, t5, t4
[0x8000c560]:csrrs a2, fcsr, zero
[0x8000c564]:sd t6, 1600(fp)
[0x8000c568]:sd a2, 1608(fp)
[0x8000c56c]:lui s1, 3
[0x8000c570]:add a1, a1, s1
[0x8000c574]:ld t5, 1664(a1)
[0x8000c578]:sub a1, a1, s1
[0x8000c57c]:lui s1, 3
[0x8000c580]:add a1, a1, s1
[0x8000c584]:ld t4, 1672(a1)
[0x8000c588]:sub a1, a1, s1
[0x8000c58c]:addi s1, zero, 0
[0x8000c590]:csrrw zero, fcsr, s1
[0x8000c594]:fmax.s t6, t5, t4

[0x8000c594]:fmax.s t6, t5, t4
[0x8000c598]:csrrs a2, fcsr, zero
[0x8000c59c]:sd t6, 1616(fp)
[0x8000c5a0]:sd a2, 1624(fp)
[0x8000c5a4]:lui s1, 3
[0x8000c5a8]:add a1, a1, s1
[0x8000c5ac]:ld t5, 1680(a1)
[0x8000c5b0]:sub a1, a1, s1
[0x8000c5b4]:lui s1, 3
[0x8000c5b8]:add a1, a1, s1
[0x8000c5bc]:ld t4, 1688(a1)
[0x8000c5c0]:sub a1, a1, s1
[0x8000c5c4]:addi s1, zero, 0
[0x8000c5c8]:csrrw zero, fcsr, s1
[0x8000c5cc]:fmax.s t6, t5, t4

[0x8000c5cc]:fmax.s t6, t5, t4
[0x8000c5d0]:csrrs a2, fcsr, zero
[0x8000c5d4]:sd t6, 1632(fp)
[0x8000c5d8]:sd a2, 1640(fp)
[0x8000c5dc]:lui s1, 3
[0x8000c5e0]:add a1, a1, s1
[0x8000c5e4]:ld t5, 1696(a1)
[0x8000c5e8]:sub a1, a1, s1
[0x8000c5ec]:lui s1, 3
[0x8000c5f0]:add a1, a1, s1
[0x8000c5f4]:ld t4, 1704(a1)
[0x8000c5f8]:sub a1, a1, s1
[0x8000c5fc]:addi s1, zero, 0
[0x8000c600]:csrrw zero, fcsr, s1
[0x8000c604]:fmax.s t6, t5, t4

[0x8000c604]:fmax.s t6, t5, t4
[0x8000c608]:csrrs a2, fcsr, zero
[0x8000c60c]:sd t6, 1648(fp)
[0x8000c610]:sd a2, 1656(fp)
[0x8000c614]:lui s1, 3
[0x8000c618]:add a1, a1, s1
[0x8000c61c]:ld t5, 1712(a1)
[0x8000c620]:sub a1, a1, s1
[0x8000c624]:lui s1, 3
[0x8000c628]:add a1, a1, s1
[0x8000c62c]:ld t4, 1720(a1)
[0x8000c630]:sub a1, a1, s1
[0x8000c634]:addi s1, zero, 0
[0x8000c638]:csrrw zero, fcsr, s1
[0x8000c63c]:fmax.s t6, t5, t4

[0x8000c63c]:fmax.s t6, t5, t4
[0x8000c640]:csrrs a2, fcsr, zero
[0x8000c644]:sd t6, 1664(fp)
[0x8000c648]:sd a2, 1672(fp)
[0x8000c64c]:lui s1, 3
[0x8000c650]:add a1, a1, s1
[0x8000c654]:ld t5, 1728(a1)
[0x8000c658]:sub a1, a1, s1
[0x8000c65c]:lui s1, 3
[0x8000c660]:add a1, a1, s1
[0x8000c664]:ld t4, 1736(a1)
[0x8000c668]:sub a1, a1, s1
[0x8000c66c]:addi s1, zero, 0
[0x8000c670]:csrrw zero, fcsr, s1
[0x8000c674]:fmax.s t6, t5, t4

[0x8000c674]:fmax.s t6, t5, t4
[0x8000c678]:csrrs a2, fcsr, zero
[0x8000c67c]:sd t6, 1680(fp)
[0x8000c680]:sd a2, 1688(fp)
[0x8000c684]:lui s1, 3
[0x8000c688]:add a1, a1, s1
[0x8000c68c]:ld t5, 1744(a1)
[0x8000c690]:sub a1, a1, s1
[0x8000c694]:lui s1, 3
[0x8000c698]:add a1, a1, s1
[0x8000c69c]:ld t4, 1752(a1)
[0x8000c6a0]:sub a1, a1, s1
[0x8000c6a4]:addi s1, zero, 0
[0x8000c6a8]:csrrw zero, fcsr, s1
[0x8000c6ac]:fmax.s t6, t5, t4

[0x8000c6ac]:fmax.s t6, t5, t4
[0x8000c6b0]:csrrs a2, fcsr, zero
[0x8000c6b4]:sd t6, 1696(fp)
[0x8000c6b8]:sd a2, 1704(fp)
[0x8000c6bc]:lui s1, 3
[0x8000c6c0]:add a1, a1, s1
[0x8000c6c4]:ld t5, 1760(a1)
[0x8000c6c8]:sub a1, a1, s1
[0x8000c6cc]:lui s1, 3
[0x8000c6d0]:add a1, a1, s1
[0x8000c6d4]:ld t4, 1768(a1)
[0x8000c6d8]:sub a1, a1, s1
[0x8000c6dc]:addi s1, zero, 0
[0x8000c6e0]:csrrw zero, fcsr, s1
[0x8000c6e4]:fmax.s t6, t5, t4

[0x8000c6e4]:fmax.s t6, t5, t4
[0x8000c6e8]:csrrs a2, fcsr, zero
[0x8000c6ec]:sd t6, 1712(fp)
[0x8000c6f0]:sd a2, 1720(fp)
[0x8000c6f4]:lui s1, 3
[0x8000c6f8]:add a1, a1, s1
[0x8000c6fc]:ld t5, 1776(a1)
[0x8000c700]:sub a1, a1, s1
[0x8000c704]:lui s1, 3
[0x8000c708]:add a1, a1, s1
[0x8000c70c]:ld t4, 1784(a1)
[0x8000c710]:sub a1, a1, s1
[0x8000c714]:addi s1, zero, 0
[0x8000c718]:csrrw zero, fcsr, s1
[0x8000c71c]:fmax.s t6, t5, t4

[0x8000c71c]:fmax.s t6, t5, t4
[0x8000c720]:csrrs a2, fcsr, zero
[0x8000c724]:sd t6, 1728(fp)
[0x8000c728]:sd a2, 1736(fp)
[0x8000c72c]:lui s1, 3
[0x8000c730]:add a1, a1, s1
[0x8000c734]:ld t5, 1792(a1)
[0x8000c738]:sub a1, a1, s1
[0x8000c73c]:lui s1, 3
[0x8000c740]:add a1, a1, s1
[0x8000c744]:ld t4, 1800(a1)
[0x8000c748]:sub a1, a1, s1
[0x8000c74c]:addi s1, zero, 0
[0x8000c750]:csrrw zero, fcsr, s1
[0x8000c754]:fmax.s t6, t5, t4

[0x8000c754]:fmax.s t6, t5, t4
[0x8000c758]:csrrs a2, fcsr, zero
[0x8000c75c]:sd t6, 1744(fp)
[0x8000c760]:sd a2, 1752(fp)
[0x8000c764]:lui s1, 3
[0x8000c768]:add a1, a1, s1
[0x8000c76c]:ld t5, 1808(a1)
[0x8000c770]:sub a1, a1, s1
[0x8000c774]:lui s1, 3
[0x8000c778]:add a1, a1, s1
[0x8000c77c]:ld t4, 1816(a1)
[0x8000c780]:sub a1, a1, s1
[0x8000c784]:addi s1, zero, 0
[0x8000c788]:csrrw zero, fcsr, s1
[0x8000c78c]:fmax.s t6, t5, t4

[0x8000c78c]:fmax.s t6, t5, t4
[0x8000c790]:csrrs a2, fcsr, zero
[0x8000c794]:sd t6, 1760(fp)
[0x8000c798]:sd a2, 1768(fp)
[0x8000c79c]:lui s1, 3
[0x8000c7a0]:add a1, a1, s1
[0x8000c7a4]:ld t5, 1824(a1)
[0x8000c7a8]:sub a1, a1, s1
[0x8000c7ac]:lui s1, 3
[0x8000c7b0]:add a1, a1, s1
[0x8000c7b4]:ld t4, 1832(a1)
[0x8000c7b8]:sub a1, a1, s1
[0x8000c7bc]:addi s1, zero, 0
[0x8000c7c0]:csrrw zero, fcsr, s1
[0x8000c7c4]:fmax.s t6, t5, t4

[0x8000c7c4]:fmax.s t6, t5, t4
[0x8000c7c8]:csrrs a2, fcsr, zero
[0x8000c7cc]:sd t6, 1776(fp)
[0x8000c7d0]:sd a2, 1784(fp)
[0x8000c7d4]:lui s1, 3
[0x8000c7d8]:add a1, a1, s1
[0x8000c7dc]:ld t5, 1840(a1)
[0x8000c7e0]:sub a1, a1, s1
[0x8000c7e4]:lui s1, 3
[0x8000c7e8]:add a1, a1, s1
[0x8000c7ec]:ld t4, 1848(a1)
[0x8000c7f0]:sub a1, a1, s1
[0x8000c7f4]:addi s1, zero, 0
[0x8000c7f8]:csrrw zero, fcsr, s1
[0x8000c7fc]:fmax.s t6, t5, t4

[0x8000c7fc]:fmax.s t6, t5, t4
[0x8000c800]:csrrs a2, fcsr, zero
[0x8000c804]:sd t6, 1792(fp)
[0x8000c808]:sd a2, 1800(fp)
[0x8000c80c]:lui s1, 3
[0x8000c810]:add a1, a1, s1
[0x8000c814]:ld t5, 1856(a1)
[0x8000c818]:sub a1, a1, s1
[0x8000c81c]:lui s1, 3
[0x8000c820]:add a1, a1, s1
[0x8000c824]:ld t4, 1864(a1)
[0x8000c828]:sub a1, a1, s1
[0x8000c82c]:addi s1, zero, 0
[0x8000c830]:csrrw zero, fcsr, s1
[0x8000c834]:fmax.s t6, t5, t4

[0x8000c834]:fmax.s t6, t5, t4
[0x8000c838]:csrrs a2, fcsr, zero
[0x8000c83c]:sd t6, 1808(fp)
[0x8000c840]:sd a2, 1816(fp)
[0x8000c844]:lui s1, 3
[0x8000c848]:add a1, a1, s1
[0x8000c84c]:ld t5, 1872(a1)
[0x8000c850]:sub a1, a1, s1
[0x8000c854]:lui s1, 3
[0x8000c858]:add a1, a1, s1
[0x8000c85c]:ld t4, 1880(a1)
[0x8000c860]:sub a1, a1, s1
[0x8000c864]:addi s1, zero, 0
[0x8000c868]:csrrw zero, fcsr, s1
[0x8000c86c]:fmax.s t6, t5, t4

[0x8000c86c]:fmax.s t6, t5, t4
[0x8000c870]:csrrs a2, fcsr, zero
[0x8000c874]:sd t6, 1824(fp)
[0x8000c878]:sd a2, 1832(fp)
[0x8000c87c]:lui s1, 3
[0x8000c880]:add a1, a1, s1
[0x8000c884]:ld t5, 1888(a1)
[0x8000c888]:sub a1, a1, s1
[0x8000c88c]:lui s1, 3
[0x8000c890]:add a1, a1, s1
[0x8000c894]:ld t4, 1896(a1)
[0x8000c898]:sub a1, a1, s1
[0x8000c89c]:addi s1, zero, 0
[0x8000c8a0]:csrrw zero, fcsr, s1
[0x8000c8a4]:fmax.s t6, t5, t4

[0x8000c8a4]:fmax.s t6, t5, t4
[0x8000c8a8]:csrrs a2, fcsr, zero
[0x8000c8ac]:sd t6, 1840(fp)
[0x8000c8b0]:sd a2, 1848(fp)
[0x8000c8b4]:lui s1, 3
[0x8000c8b8]:add a1, a1, s1
[0x8000c8bc]:ld t5, 1904(a1)
[0x8000c8c0]:sub a1, a1, s1
[0x8000c8c4]:lui s1, 3
[0x8000c8c8]:add a1, a1, s1
[0x8000c8cc]:ld t4, 1912(a1)
[0x8000c8d0]:sub a1, a1, s1
[0x8000c8d4]:addi s1, zero, 0
[0x8000c8d8]:csrrw zero, fcsr, s1
[0x8000c8dc]:fmax.s t6, t5, t4

[0x8000c8dc]:fmax.s t6, t5, t4
[0x8000c8e0]:csrrs a2, fcsr, zero
[0x8000c8e4]:sd t6, 1856(fp)
[0x8000c8e8]:sd a2, 1864(fp)
[0x8000c8ec]:lui s1, 3
[0x8000c8f0]:add a1, a1, s1
[0x8000c8f4]:ld t5, 1920(a1)
[0x8000c8f8]:sub a1, a1, s1
[0x8000c8fc]:lui s1, 3
[0x8000c900]:add a1, a1, s1
[0x8000c904]:ld t4, 1928(a1)
[0x8000c908]:sub a1, a1, s1
[0x8000c90c]:addi s1, zero, 0
[0x8000c910]:csrrw zero, fcsr, s1
[0x8000c914]:fmax.s t6, t5, t4

[0x8000c914]:fmax.s t6, t5, t4
[0x8000c918]:csrrs a2, fcsr, zero
[0x8000c91c]:sd t6, 1872(fp)
[0x8000c920]:sd a2, 1880(fp)
[0x8000c924]:lui s1, 3
[0x8000c928]:add a1, a1, s1
[0x8000c92c]:ld t5, 1936(a1)
[0x8000c930]:sub a1, a1, s1
[0x8000c934]:lui s1, 3
[0x8000c938]:add a1, a1, s1
[0x8000c93c]:ld t4, 1944(a1)
[0x8000c940]:sub a1, a1, s1
[0x8000c944]:addi s1, zero, 0
[0x8000c948]:csrrw zero, fcsr, s1
[0x8000c94c]:fmax.s t6, t5, t4

[0x8000c94c]:fmax.s t6, t5, t4
[0x8000c950]:csrrs a2, fcsr, zero
[0x8000c954]:sd t6, 1888(fp)
[0x8000c958]:sd a2, 1896(fp)
[0x8000c95c]:lui s1, 3
[0x8000c960]:add a1, a1, s1
[0x8000c964]:ld t5, 1952(a1)
[0x8000c968]:sub a1, a1, s1
[0x8000c96c]:lui s1, 3
[0x8000c970]:add a1, a1, s1
[0x8000c974]:ld t4, 1960(a1)
[0x8000c978]:sub a1, a1, s1
[0x8000c97c]:addi s1, zero, 0
[0x8000c980]:csrrw zero, fcsr, s1
[0x8000c984]:fmax.s t6, t5, t4

[0x8000c984]:fmax.s t6, t5, t4
[0x8000c988]:csrrs a2, fcsr, zero
[0x8000c98c]:sd t6, 1904(fp)
[0x8000c990]:sd a2, 1912(fp)
[0x8000c994]:lui s1, 3
[0x8000c998]:add a1, a1, s1
[0x8000c99c]:ld t5, 1968(a1)
[0x8000c9a0]:sub a1, a1, s1
[0x8000c9a4]:lui s1, 3
[0x8000c9a8]:add a1, a1, s1
[0x8000c9ac]:ld t4, 1976(a1)
[0x8000c9b0]:sub a1, a1, s1
[0x8000c9b4]:addi s1, zero, 0
[0x8000c9b8]:csrrw zero, fcsr, s1
[0x8000c9bc]:fmax.s t6, t5, t4

[0x8000c9bc]:fmax.s t6, t5, t4
[0x8000c9c0]:csrrs a2, fcsr, zero
[0x8000c9c4]:sd t6, 1920(fp)
[0x8000c9c8]:sd a2, 1928(fp)
[0x8000c9cc]:lui s1, 3
[0x8000c9d0]:add a1, a1, s1
[0x8000c9d4]:ld t5, 1984(a1)
[0x8000c9d8]:sub a1, a1, s1
[0x8000c9dc]:lui s1, 3
[0x8000c9e0]:add a1, a1, s1
[0x8000c9e4]:ld t4, 1992(a1)
[0x8000c9e8]:sub a1, a1, s1
[0x8000c9ec]:addi s1, zero, 0
[0x8000c9f0]:csrrw zero, fcsr, s1
[0x8000c9f4]:fmax.s t6, t5, t4

[0x8000c9f4]:fmax.s t6, t5, t4
[0x8000c9f8]:csrrs a2, fcsr, zero
[0x8000c9fc]:sd t6, 1936(fp)
[0x8000ca00]:sd a2, 1944(fp)
[0x8000ca04]:lui s1, 3
[0x8000ca08]:add a1, a1, s1
[0x8000ca0c]:ld t5, 2000(a1)
[0x8000ca10]:sub a1, a1, s1
[0x8000ca14]:lui s1, 3
[0x8000ca18]:add a1, a1, s1
[0x8000ca1c]:ld t4, 2008(a1)
[0x8000ca20]:sub a1, a1, s1
[0x8000ca24]:addi s1, zero, 0
[0x8000ca28]:csrrw zero, fcsr, s1
[0x8000ca2c]:fmax.s t6, t5, t4

[0x8000ca2c]:fmax.s t6, t5, t4
[0x8000ca30]:csrrs a2, fcsr, zero
[0x8000ca34]:sd t6, 1952(fp)
[0x8000ca38]:sd a2, 1960(fp)
[0x8000ca3c]:lui s1, 3
[0x8000ca40]:add a1, a1, s1
[0x8000ca44]:ld t5, 2016(a1)
[0x8000ca48]:sub a1, a1, s1
[0x8000ca4c]:lui s1, 3
[0x8000ca50]:add a1, a1, s1
[0x8000ca54]:ld t4, 2024(a1)
[0x8000ca58]:sub a1, a1, s1
[0x8000ca5c]:addi s1, zero, 0
[0x8000ca60]:csrrw zero, fcsr, s1
[0x8000ca64]:fmax.s t6, t5, t4

[0x8000ca64]:fmax.s t6, t5, t4
[0x8000ca68]:csrrs a2, fcsr, zero
[0x8000ca6c]:sd t6, 1968(fp)
[0x8000ca70]:sd a2, 1976(fp)
[0x8000ca74]:lui s1, 3
[0x8000ca78]:add a1, a1, s1
[0x8000ca7c]:ld t5, 2032(a1)
[0x8000ca80]:sub a1, a1, s1
[0x8000ca84]:lui s1, 3
[0x8000ca88]:add a1, a1, s1
[0x8000ca8c]:ld t4, 2040(a1)
[0x8000ca90]:sub a1, a1, s1
[0x8000ca94]:addi s1, zero, 0
[0x8000ca98]:csrrw zero, fcsr, s1
[0x8000ca9c]:fmax.s t6, t5, t4

[0x8000ca9c]:fmax.s t6, t5, t4
[0x8000caa0]:csrrs a2, fcsr, zero
[0x8000caa4]:sd t6, 1984(fp)
[0x8000caa8]:sd a2, 1992(fp)
[0x8000caac]:lui s1, 4
[0x8000cab0]:addiw s1, s1, 2048
[0x8000cab4]:add a1, a1, s1
[0x8000cab8]:ld t5, 0(a1)
[0x8000cabc]:sub a1, a1, s1
[0x8000cac0]:lui s1, 4
[0x8000cac4]:addiw s1, s1, 2048
[0x8000cac8]:add a1, a1, s1
[0x8000cacc]:ld t4, 8(a1)
[0x8000cad0]:sub a1, a1, s1
[0x8000cad4]:addi s1, zero, 0
[0x8000cad8]:csrrw zero, fcsr, s1
[0x8000cadc]:fmax.s t6, t5, t4

[0x8000cadc]:fmax.s t6, t5, t4
[0x8000cae0]:csrrs a2, fcsr, zero
[0x8000cae4]:sd t6, 2000(fp)
[0x8000cae8]:sd a2, 2008(fp)
[0x8000caec]:lui s1, 4
[0x8000caf0]:addiw s1, s1, 2048
[0x8000caf4]:add a1, a1, s1
[0x8000caf8]:ld t5, 16(a1)
[0x8000cafc]:sub a1, a1, s1
[0x8000cb00]:lui s1, 4
[0x8000cb04]:addiw s1, s1, 2048
[0x8000cb08]:add a1, a1, s1
[0x8000cb0c]:ld t4, 24(a1)
[0x8000cb10]:sub a1, a1, s1
[0x8000cb14]:addi s1, zero, 0
[0x8000cb18]:csrrw zero, fcsr, s1
[0x8000cb1c]:fmax.s t6, t5, t4

[0x8000cb1c]:fmax.s t6, t5, t4
[0x8000cb20]:csrrs a2, fcsr, zero
[0x8000cb24]:sd t6, 2016(fp)
[0x8000cb28]:sd a2, 2024(fp)
[0x8000cb2c]:lui s1, 4
[0x8000cb30]:addiw s1, s1, 2048
[0x8000cb34]:add a1, a1, s1
[0x8000cb38]:ld t5, 32(a1)
[0x8000cb3c]:sub a1, a1, s1
[0x8000cb40]:lui s1, 4
[0x8000cb44]:addiw s1, s1, 2048
[0x8000cb48]:add a1, a1, s1
[0x8000cb4c]:ld t4, 40(a1)
[0x8000cb50]:sub a1, a1, s1
[0x8000cb54]:addi s1, zero, 0
[0x8000cb58]:csrrw zero, fcsr, s1
[0x8000cb5c]:fmax.s t6, t5, t4

[0x8000cb5c]:fmax.s t6, t5, t4
[0x8000cb60]:csrrs a2, fcsr, zero
[0x8000cb64]:sd t6, 2032(fp)
[0x8000cb68]:sd a2, 2040(fp)
[0x8000cb6c]:auipc fp, 10
[0x8000cb70]:addi fp, fp, 3132
[0x8000cb74]:lui s1, 4
[0x8000cb78]:addiw s1, s1, 2048
[0x8000cb7c]:add a1, a1, s1
[0x8000cb80]:ld t5, 48(a1)
[0x8000cb84]:sub a1, a1, s1
[0x8000cb88]:lui s1, 4
[0x8000cb8c]:addiw s1, s1, 2048
[0x8000cb90]:add a1, a1, s1
[0x8000cb94]:ld t4, 56(a1)
[0x8000cb98]:sub a1, a1, s1
[0x8000cb9c]:addi s1, zero, 0
[0x8000cba0]:csrrw zero, fcsr, s1
[0x8000cba4]:fmax.s t6, t5, t4

[0x8000cba4]:fmax.s t6, t5, t4
[0x8000cba8]:csrrs a2, fcsr, zero
[0x8000cbac]:sd t6, 0(fp)
[0x8000cbb0]:sd a2, 8(fp)
[0x8000cbb4]:lui s1, 4
[0x8000cbb8]:addiw s1, s1, 2048
[0x8000cbbc]:add a1, a1, s1
[0x8000cbc0]:ld t5, 64(a1)
[0x8000cbc4]:sub a1, a1, s1
[0x8000cbc8]:lui s1, 4
[0x8000cbcc]:addiw s1, s1, 2048
[0x8000cbd0]:add a1, a1, s1
[0x8000cbd4]:ld t4, 72(a1)
[0x8000cbd8]:sub a1, a1, s1
[0x8000cbdc]:addi s1, zero, 0
[0x8000cbe0]:csrrw zero, fcsr, s1
[0x8000cbe4]:fmax.s t6, t5, t4

[0x8000cbe4]:fmax.s t6, t5, t4
[0x8000cbe8]:csrrs a2, fcsr, zero
[0x8000cbec]:sd t6, 16(fp)
[0x8000cbf0]:sd a2, 24(fp)
[0x8000cbf4]:lui s1, 4
[0x8000cbf8]:addiw s1, s1, 2048
[0x8000cbfc]:add a1, a1, s1
[0x8000cc00]:ld t5, 80(a1)
[0x8000cc04]:sub a1, a1, s1
[0x8000cc08]:lui s1, 4
[0x8000cc0c]:addiw s1, s1, 2048
[0x8000cc10]:add a1, a1, s1
[0x8000cc14]:ld t4, 88(a1)
[0x8000cc18]:sub a1, a1, s1
[0x8000cc1c]:addi s1, zero, 0
[0x8000cc20]:csrrw zero, fcsr, s1
[0x8000cc24]:fmax.s t6, t5, t4

[0x8000cc24]:fmax.s t6, t5, t4
[0x8000cc28]:csrrs a2, fcsr, zero
[0x8000cc2c]:sd t6, 32(fp)
[0x8000cc30]:sd a2, 40(fp)
[0x8000cc34]:lui s1, 4
[0x8000cc38]:addiw s1, s1, 2048
[0x8000cc3c]:add a1, a1, s1
[0x8000cc40]:ld t5, 96(a1)
[0x8000cc44]:sub a1, a1, s1
[0x8000cc48]:lui s1, 4
[0x8000cc4c]:addiw s1, s1, 2048
[0x8000cc50]:add a1, a1, s1
[0x8000cc54]:ld t4, 104(a1)
[0x8000cc58]:sub a1, a1, s1
[0x8000cc5c]:addi s1, zero, 0
[0x8000cc60]:csrrw zero, fcsr, s1
[0x8000cc64]:fmax.s t6, t5, t4

[0x8000cc64]:fmax.s t6, t5, t4
[0x8000cc68]:csrrs a2, fcsr, zero
[0x8000cc6c]:sd t6, 48(fp)
[0x8000cc70]:sd a2, 56(fp)
[0x8000cc74]:lui s1, 4
[0x8000cc78]:addiw s1, s1, 2048
[0x8000cc7c]:add a1, a1, s1
[0x8000cc80]:ld t5, 112(a1)
[0x8000cc84]:sub a1, a1, s1
[0x8000cc88]:lui s1, 4
[0x8000cc8c]:addiw s1, s1, 2048
[0x8000cc90]:add a1, a1, s1
[0x8000cc94]:ld t4, 120(a1)
[0x8000cc98]:sub a1, a1, s1
[0x8000cc9c]:addi s1, zero, 0
[0x8000cca0]:csrrw zero, fcsr, s1
[0x8000cca4]:fmax.s t6, t5, t4

[0x8000cca4]:fmax.s t6, t5, t4
[0x8000cca8]:csrrs a2, fcsr, zero
[0x8000ccac]:sd t6, 64(fp)
[0x8000ccb0]:sd a2, 72(fp)
[0x8000ccb4]:lui s1, 4
[0x8000ccb8]:addiw s1, s1, 2048
[0x8000ccbc]:add a1, a1, s1
[0x8000ccc0]:ld t5, 128(a1)
[0x8000ccc4]:sub a1, a1, s1
[0x8000ccc8]:lui s1, 4
[0x8000cccc]:addiw s1, s1, 2048
[0x8000ccd0]:add a1, a1, s1
[0x8000ccd4]:ld t4, 136(a1)
[0x8000ccd8]:sub a1, a1, s1
[0x8000ccdc]:addi s1, zero, 0
[0x8000cce0]:csrrw zero, fcsr, s1
[0x8000cce4]:fmax.s t6, t5, t4

[0x8000cce4]:fmax.s t6, t5, t4
[0x8000cce8]:csrrs a2, fcsr, zero
[0x8000ccec]:sd t6, 80(fp)
[0x8000ccf0]:sd a2, 88(fp)
[0x8000ccf4]:lui s1, 4
[0x8000ccf8]:addiw s1, s1, 2048
[0x8000ccfc]:add a1, a1, s1
[0x8000cd00]:ld t5, 144(a1)
[0x8000cd04]:sub a1, a1, s1
[0x8000cd08]:lui s1, 4
[0x8000cd0c]:addiw s1, s1, 2048
[0x8000cd10]:add a1, a1, s1
[0x8000cd14]:ld t4, 152(a1)
[0x8000cd18]:sub a1, a1, s1
[0x8000cd1c]:addi s1, zero, 0
[0x8000cd20]:csrrw zero, fcsr, s1
[0x8000cd24]:fmax.s t6, t5, t4

[0x8000cd24]:fmax.s t6, t5, t4
[0x8000cd28]:csrrs a2, fcsr, zero
[0x8000cd2c]:sd t6, 96(fp)
[0x8000cd30]:sd a2, 104(fp)
[0x8000cd34]:lui s1, 4
[0x8000cd38]:addiw s1, s1, 2048
[0x8000cd3c]:add a1, a1, s1
[0x8000cd40]:ld t5, 160(a1)
[0x8000cd44]:sub a1, a1, s1
[0x8000cd48]:lui s1, 4
[0x8000cd4c]:addiw s1, s1, 2048
[0x8000cd50]:add a1, a1, s1
[0x8000cd54]:ld t4, 168(a1)
[0x8000cd58]:sub a1, a1, s1
[0x8000cd5c]:addi s1, zero, 0
[0x8000cd60]:csrrw zero, fcsr, s1
[0x8000cd64]:fmax.s t6, t5, t4

[0x8000cd64]:fmax.s t6, t5, t4
[0x8000cd68]:csrrs a2, fcsr, zero
[0x8000cd6c]:sd t6, 112(fp)
[0x8000cd70]:sd a2, 120(fp)
[0x8000cd74]:lui s1, 4
[0x8000cd78]:addiw s1, s1, 2048
[0x8000cd7c]:add a1, a1, s1
[0x8000cd80]:ld t5, 176(a1)
[0x8000cd84]:sub a1, a1, s1
[0x8000cd88]:lui s1, 4
[0x8000cd8c]:addiw s1, s1, 2048
[0x8000cd90]:add a1, a1, s1
[0x8000cd94]:ld t4, 184(a1)
[0x8000cd98]:sub a1, a1, s1
[0x8000cd9c]:addi s1, zero, 0
[0x8000cda0]:csrrw zero, fcsr, s1
[0x8000cda4]:fmax.s t6, t5, t4

[0x8000cda4]:fmax.s t6, t5, t4
[0x8000cda8]:csrrs a2, fcsr, zero
[0x8000cdac]:sd t6, 128(fp)
[0x8000cdb0]:sd a2, 136(fp)
[0x8000cdb4]:lui s1, 4
[0x8000cdb8]:addiw s1, s1, 2048
[0x8000cdbc]:add a1, a1, s1
[0x8000cdc0]:ld t5, 192(a1)
[0x8000cdc4]:sub a1, a1, s1
[0x8000cdc8]:lui s1, 4
[0x8000cdcc]:addiw s1, s1, 2048
[0x8000cdd0]:add a1, a1, s1
[0x8000cdd4]:ld t4, 200(a1)
[0x8000cdd8]:sub a1, a1, s1
[0x8000cddc]:addi s1, zero, 0
[0x8000cde0]:csrrw zero, fcsr, s1
[0x8000cde4]:fmax.s t6, t5, t4

[0x8000cde4]:fmax.s t6, t5, t4
[0x8000cde8]:csrrs a2, fcsr, zero
[0x8000cdec]:sd t6, 144(fp)
[0x8000cdf0]:sd a2, 152(fp)
[0x8000cdf4]:lui s1, 4
[0x8000cdf8]:addiw s1, s1, 2048
[0x8000cdfc]:add a1, a1, s1
[0x8000ce00]:ld t5, 208(a1)
[0x8000ce04]:sub a1, a1, s1
[0x8000ce08]:lui s1, 4
[0x8000ce0c]:addiw s1, s1, 2048
[0x8000ce10]:add a1, a1, s1
[0x8000ce14]:ld t4, 216(a1)
[0x8000ce18]:sub a1, a1, s1
[0x8000ce1c]:addi s1, zero, 0
[0x8000ce20]:csrrw zero, fcsr, s1
[0x8000ce24]:fmax.s t6, t5, t4

[0x8000ce24]:fmax.s t6, t5, t4
[0x8000ce28]:csrrs a2, fcsr, zero
[0x8000ce2c]:sd t6, 160(fp)
[0x8000ce30]:sd a2, 168(fp)
[0x8000ce34]:lui s1, 4
[0x8000ce38]:addiw s1, s1, 2048
[0x8000ce3c]:add a1, a1, s1
[0x8000ce40]:ld t5, 224(a1)
[0x8000ce44]:sub a1, a1, s1
[0x8000ce48]:lui s1, 4
[0x8000ce4c]:addiw s1, s1, 2048
[0x8000ce50]:add a1, a1, s1
[0x8000ce54]:ld t4, 232(a1)
[0x8000ce58]:sub a1, a1, s1
[0x8000ce5c]:addi s1, zero, 0
[0x8000ce60]:csrrw zero, fcsr, s1
[0x8000ce64]:fmax.s t6, t5, t4

[0x8000ce64]:fmax.s t6, t5, t4
[0x8000ce68]:csrrs a2, fcsr, zero
[0x8000ce6c]:sd t6, 176(fp)
[0x8000ce70]:sd a2, 184(fp)
[0x8000ce74]:lui s1, 4
[0x8000ce78]:addiw s1, s1, 2048
[0x8000ce7c]:add a1, a1, s1
[0x8000ce80]:ld t5, 240(a1)
[0x8000ce84]:sub a1, a1, s1
[0x8000ce88]:lui s1, 4
[0x8000ce8c]:addiw s1, s1, 2048
[0x8000ce90]:add a1, a1, s1
[0x8000ce94]:ld t4, 248(a1)
[0x8000ce98]:sub a1, a1, s1
[0x8000ce9c]:addi s1, zero, 0
[0x8000cea0]:csrrw zero, fcsr, s1
[0x8000cea4]:fmax.s t6, t5, t4

[0x8000cea4]:fmax.s t6, t5, t4
[0x8000cea8]:csrrs a2, fcsr, zero
[0x8000ceac]:sd t6, 192(fp)
[0x8000ceb0]:sd a2, 200(fp)
[0x8000ceb4]:lui s1, 4
[0x8000ceb8]:addiw s1, s1, 2048
[0x8000cebc]:add a1, a1, s1
[0x8000cec0]:ld t5, 256(a1)
[0x8000cec4]:sub a1, a1, s1
[0x8000cec8]:lui s1, 4
[0x8000cecc]:addiw s1, s1, 2048
[0x8000ced0]:add a1, a1, s1
[0x8000ced4]:ld t4, 264(a1)
[0x8000ced8]:sub a1, a1, s1
[0x8000cedc]:addi s1, zero, 0
[0x8000cee0]:csrrw zero, fcsr, s1
[0x8000cee4]:fmax.s t6, t5, t4

[0x8000cee4]:fmax.s t6, t5, t4
[0x8000cee8]:csrrs a2, fcsr, zero
[0x8000ceec]:sd t6, 208(fp)
[0x8000cef0]:sd a2, 216(fp)
[0x8000cef4]:lui s1, 4
[0x8000cef8]:addiw s1, s1, 2048
[0x8000cefc]:add a1, a1, s1
[0x8000cf00]:ld t5, 272(a1)
[0x8000cf04]:sub a1, a1, s1
[0x8000cf08]:lui s1, 4
[0x8000cf0c]:addiw s1, s1, 2048
[0x8000cf10]:add a1, a1, s1
[0x8000cf14]:ld t4, 280(a1)
[0x8000cf18]:sub a1, a1, s1
[0x8000cf1c]:addi s1, zero, 0
[0x8000cf20]:csrrw zero, fcsr, s1
[0x8000cf24]:fmax.s t6, t5, t4

[0x8000cf24]:fmax.s t6, t5, t4
[0x8000cf28]:csrrs a2, fcsr, zero
[0x8000cf2c]:sd t6, 224(fp)
[0x8000cf30]:sd a2, 232(fp)
[0x8000cf34]:lui s1, 4
[0x8000cf38]:addiw s1, s1, 2048
[0x8000cf3c]:add a1, a1, s1
[0x8000cf40]:ld t5, 288(a1)
[0x8000cf44]:sub a1, a1, s1
[0x8000cf48]:lui s1, 4
[0x8000cf4c]:addiw s1, s1, 2048
[0x8000cf50]:add a1, a1, s1
[0x8000cf54]:ld t4, 296(a1)
[0x8000cf58]:sub a1, a1, s1
[0x8000cf5c]:addi s1, zero, 0
[0x8000cf60]:csrrw zero, fcsr, s1
[0x8000cf64]:fmax.s t6, t5, t4

[0x8000cf64]:fmax.s t6, t5, t4
[0x8000cf68]:csrrs a2, fcsr, zero
[0x8000cf6c]:sd t6, 240(fp)
[0x8000cf70]:sd a2, 248(fp)
[0x8000cf74]:lui s1, 4
[0x8000cf78]:addiw s1, s1, 2048
[0x8000cf7c]:add a1, a1, s1
[0x8000cf80]:ld t5, 304(a1)
[0x8000cf84]:sub a1, a1, s1
[0x8000cf88]:lui s1, 4
[0x8000cf8c]:addiw s1, s1, 2048
[0x8000cf90]:add a1, a1, s1
[0x8000cf94]:ld t4, 312(a1)
[0x8000cf98]:sub a1, a1, s1
[0x8000cf9c]:addi s1, zero, 0
[0x8000cfa0]:csrrw zero, fcsr, s1
[0x8000cfa4]:fmax.s t6, t5, t4

[0x8000cfa4]:fmax.s t6, t5, t4
[0x8000cfa8]:csrrs a2, fcsr, zero
[0x8000cfac]:sd t6, 256(fp)
[0x8000cfb0]:sd a2, 264(fp)
[0x8000cfb4]:lui s1, 4
[0x8000cfb8]:addiw s1, s1, 2048
[0x8000cfbc]:add a1, a1, s1
[0x8000cfc0]:ld t5, 320(a1)
[0x8000cfc4]:sub a1, a1, s1
[0x8000cfc8]:lui s1, 4
[0x8000cfcc]:addiw s1, s1, 2048
[0x8000cfd0]:add a1, a1, s1
[0x8000cfd4]:ld t4, 328(a1)
[0x8000cfd8]:sub a1, a1, s1
[0x8000cfdc]:addi s1, zero, 0
[0x8000cfe0]:csrrw zero, fcsr, s1
[0x8000cfe4]:fmax.s t6, t5, t4

[0x8000cfe4]:fmax.s t6, t5, t4
[0x8000cfe8]:csrrs a2, fcsr, zero
[0x8000cfec]:sd t6, 272(fp)
[0x8000cff0]:sd a2, 280(fp)
[0x8000cff4]:lui s1, 4
[0x8000cff8]:addiw s1, s1, 2048
[0x8000cffc]:add a1, a1, s1
[0x8000d000]:ld t5, 336(a1)
[0x8000d004]:sub a1, a1, s1
[0x8000d008]:lui s1, 4
[0x8000d00c]:addiw s1, s1, 2048
[0x8000d010]:add a1, a1, s1
[0x8000d014]:ld t4, 344(a1)
[0x8000d018]:sub a1, a1, s1
[0x8000d01c]:addi s1, zero, 0
[0x8000d020]:csrrw zero, fcsr, s1
[0x8000d024]:fmax.s t6, t5, t4

[0x8000d024]:fmax.s t6, t5, t4
[0x8000d028]:csrrs a2, fcsr, zero
[0x8000d02c]:sd t6, 288(fp)
[0x8000d030]:sd a2, 296(fp)
[0x8000d034]:lui s1, 4
[0x8000d038]:addiw s1, s1, 2048
[0x8000d03c]:add a1, a1, s1
[0x8000d040]:ld t5, 352(a1)
[0x8000d044]:sub a1, a1, s1
[0x8000d048]:lui s1, 4
[0x8000d04c]:addiw s1, s1, 2048
[0x8000d050]:add a1, a1, s1
[0x8000d054]:ld t4, 360(a1)
[0x8000d058]:sub a1, a1, s1
[0x8000d05c]:addi s1, zero, 0
[0x8000d060]:csrrw zero, fcsr, s1
[0x8000d064]:fmax.s t6, t5, t4

[0x8000d064]:fmax.s t6, t5, t4
[0x8000d068]:csrrs a2, fcsr, zero
[0x8000d06c]:sd t6, 304(fp)
[0x8000d070]:sd a2, 312(fp)
[0x8000d074]:lui s1, 4
[0x8000d078]:addiw s1, s1, 2048
[0x8000d07c]:add a1, a1, s1
[0x8000d080]:ld t5, 368(a1)
[0x8000d084]:sub a1, a1, s1
[0x8000d088]:lui s1, 4
[0x8000d08c]:addiw s1, s1, 2048
[0x8000d090]:add a1, a1, s1
[0x8000d094]:ld t4, 376(a1)
[0x8000d098]:sub a1, a1, s1
[0x8000d09c]:addi s1, zero, 0
[0x8000d0a0]:csrrw zero, fcsr, s1
[0x8000d0a4]:fmax.s t6, t5, t4

[0x8000d0a4]:fmax.s t6, t5, t4
[0x8000d0a8]:csrrs a2, fcsr, zero
[0x8000d0ac]:sd t6, 320(fp)
[0x8000d0b0]:sd a2, 328(fp)
[0x8000d0b4]:lui s1, 4
[0x8000d0b8]:addiw s1, s1, 2048
[0x8000d0bc]:add a1, a1, s1
[0x8000d0c0]:ld t5, 384(a1)
[0x8000d0c4]:sub a1, a1, s1
[0x8000d0c8]:lui s1, 4
[0x8000d0cc]:addiw s1, s1, 2048
[0x8000d0d0]:add a1, a1, s1
[0x8000d0d4]:ld t4, 392(a1)
[0x8000d0d8]:sub a1, a1, s1
[0x8000d0dc]:addi s1, zero, 0
[0x8000d0e0]:csrrw zero, fcsr, s1
[0x8000d0e4]:fmax.s t6, t5, t4

[0x8000d0e4]:fmax.s t6, t5, t4
[0x8000d0e8]:csrrs a2, fcsr, zero
[0x8000d0ec]:sd t6, 336(fp)
[0x8000d0f0]:sd a2, 344(fp)
[0x8000d0f4]:lui s1, 4
[0x8000d0f8]:addiw s1, s1, 2048
[0x8000d0fc]:add a1, a1, s1
[0x8000d100]:ld t5, 400(a1)
[0x8000d104]:sub a1, a1, s1
[0x8000d108]:lui s1, 4
[0x8000d10c]:addiw s1, s1, 2048
[0x8000d110]:add a1, a1, s1
[0x8000d114]:ld t4, 408(a1)
[0x8000d118]:sub a1, a1, s1
[0x8000d11c]:addi s1, zero, 0
[0x8000d120]:csrrw zero, fcsr, s1
[0x8000d124]:fmax.s t6, t5, t4

[0x8000d124]:fmax.s t6, t5, t4
[0x8000d128]:csrrs a2, fcsr, zero
[0x8000d12c]:sd t6, 352(fp)
[0x8000d130]:sd a2, 360(fp)
[0x8000d134]:lui s1, 4
[0x8000d138]:addiw s1, s1, 2048
[0x8000d13c]:add a1, a1, s1
[0x8000d140]:ld t5, 416(a1)
[0x8000d144]:sub a1, a1, s1
[0x8000d148]:lui s1, 4
[0x8000d14c]:addiw s1, s1, 2048
[0x8000d150]:add a1, a1, s1
[0x8000d154]:ld t4, 424(a1)
[0x8000d158]:sub a1, a1, s1
[0x8000d15c]:addi s1, zero, 0
[0x8000d160]:csrrw zero, fcsr, s1
[0x8000d164]:fmax.s t6, t5, t4

[0x8000d164]:fmax.s t6, t5, t4
[0x8000d168]:csrrs a2, fcsr, zero
[0x8000d16c]:sd t6, 368(fp)
[0x8000d170]:sd a2, 376(fp)
[0x8000d174]:lui s1, 4
[0x8000d178]:addiw s1, s1, 2048
[0x8000d17c]:add a1, a1, s1
[0x8000d180]:ld t5, 432(a1)
[0x8000d184]:sub a1, a1, s1
[0x8000d188]:lui s1, 4
[0x8000d18c]:addiw s1, s1, 2048
[0x8000d190]:add a1, a1, s1
[0x8000d194]:ld t4, 440(a1)
[0x8000d198]:sub a1, a1, s1
[0x8000d19c]:addi s1, zero, 0
[0x8000d1a0]:csrrw zero, fcsr, s1
[0x8000d1a4]:fmax.s t6, t5, t4

[0x8000d1a4]:fmax.s t6, t5, t4
[0x8000d1a8]:csrrs a2, fcsr, zero
[0x8000d1ac]:sd t6, 384(fp)
[0x8000d1b0]:sd a2, 392(fp)
[0x8000d1b4]:lui s1, 4
[0x8000d1b8]:addiw s1, s1, 2048
[0x8000d1bc]:add a1, a1, s1
[0x8000d1c0]:ld t5, 448(a1)
[0x8000d1c4]:sub a1, a1, s1
[0x8000d1c8]:lui s1, 4
[0x8000d1cc]:addiw s1, s1, 2048
[0x8000d1d0]:add a1, a1, s1
[0x8000d1d4]:ld t4, 456(a1)
[0x8000d1d8]:sub a1, a1, s1
[0x8000d1dc]:addi s1, zero, 0
[0x8000d1e0]:csrrw zero, fcsr, s1
[0x8000d1e4]:fmax.s t6, t5, t4

[0x8000d1e4]:fmax.s t6, t5, t4
[0x8000d1e8]:csrrs a2, fcsr, zero
[0x8000d1ec]:sd t6, 400(fp)
[0x8000d1f0]:sd a2, 408(fp)
[0x8000d1f4]:lui s1, 4
[0x8000d1f8]:addiw s1, s1, 2048
[0x8000d1fc]:add a1, a1, s1
[0x8000d200]:ld t5, 464(a1)
[0x8000d204]:sub a1, a1, s1
[0x8000d208]:lui s1, 4
[0x8000d20c]:addiw s1, s1, 2048
[0x8000d210]:add a1, a1, s1
[0x8000d214]:ld t4, 472(a1)
[0x8000d218]:sub a1, a1, s1
[0x8000d21c]:addi s1, zero, 0
[0x8000d220]:csrrw zero, fcsr, s1
[0x8000d224]:fmax.s t6, t5, t4

[0x8000d224]:fmax.s t6, t5, t4
[0x8000d228]:csrrs a2, fcsr, zero
[0x8000d22c]:sd t6, 416(fp)
[0x8000d230]:sd a2, 424(fp)
[0x8000d234]:lui s1, 4
[0x8000d238]:addiw s1, s1, 2048
[0x8000d23c]:add a1, a1, s1
[0x8000d240]:ld t5, 480(a1)
[0x8000d244]:sub a1, a1, s1
[0x8000d248]:lui s1, 4
[0x8000d24c]:addiw s1, s1, 2048
[0x8000d250]:add a1, a1, s1
[0x8000d254]:ld t4, 488(a1)
[0x8000d258]:sub a1, a1, s1
[0x8000d25c]:addi s1, zero, 0
[0x8000d260]:csrrw zero, fcsr, s1
[0x8000d264]:fmax.s t6, t5, t4

[0x8000d264]:fmax.s t6, t5, t4
[0x8000d268]:csrrs a2, fcsr, zero
[0x8000d26c]:sd t6, 432(fp)
[0x8000d270]:sd a2, 440(fp)
[0x8000d274]:lui s1, 4
[0x8000d278]:addiw s1, s1, 2048
[0x8000d27c]:add a1, a1, s1
[0x8000d280]:ld t5, 496(a1)
[0x8000d284]:sub a1, a1, s1
[0x8000d288]:lui s1, 4
[0x8000d28c]:addiw s1, s1, 2048
[0x8000d290]:add a1, a1, s1
[0x8000d294]:ld t4, 504(a1)
[0x8000d298]:sub a1, a1, s1
[0x8000d29c]:addi s1, zero, 0
[0x8000d2a0]:csrrw zero, fcsr, s1
[0x8000d2a4]:fmax.s t6, t5, t4

[0x8000d2a4]:fmax.s t6, t5, t4
[0x8000d2a8]:csrrs a2, fcsr, zero
[0x8000d2ac]:sd t6, 448(fp)
[0x8000d2b0]:sd a2, 456(fp)
[0x8000d2b4]:lui s1, 4
[0x8000d2b8]:addiw s1, s1, 2048
[0x8000d2bc]:add a1, a1, s1
[0x8000d2c0]:ld t5, 512(a1)
[0x8000d2c4]:sub a1, a1, s1
[0x8000d2c8]:lui s1, 4
[0x8000d2cc]:addiw s1, s1, 2048
[0x8000d2d0]:add a1, a1, s1
[0x8000d2d4]:ld t4, 520(a1)
[0x8000d2d8]:sub a1, a1, s1
[0x8000d2dc]:addi s1, zero, 0
[0x8000d2e0]:csrrw zero, fcsr, s1
[0x8000d2e4]:fmax.s t6, t5, t4

[0x8000d2e4]:fmax.s t6, t5, t4
[0x8000d2e8]:csrrs a2, fcsr, zero
[0x8000d2ec]:sd t6, 464(fp)
[0x8000d2f0]:sd a2, 472(fp)
[0x8000d2f4]:lui s1, 4
[0x8000d2f8]:addiw s1, s1, 2048
[0x8000d2fc]:add a1, a1, s1
[0x8000d300]:ld t5, 528(a1)
[0x8000d304]:sub a1, a1, s1
[0x8000d308]:lui s1, 4
[0x8000d30c]:addiw s1, s1, 2048
[0x8000d310]:add a1, a1, s1
[0x8000d314]:ld t4, 536(a1)
[0x8000d318]:sub a1, a1, s1
[0x8000d31c]:addi s1, zero, 0
[0x8000d320]:csrrw zero, fcsr, s1
[0x8000d324]:fmax.s t6, t5, t4

[0x8000d324]:fmax.s t6, t5, t4
[0x8000d328]:csrrs a2, fcsr, zero
[0x8000d32c]:sd t6, 480(fp)
[0x8000d330]:sd a2, 488(fp)
[0x8000d334]:lui s1, 4
[0x8000d338]:addiw s1, s1, 2048
[0x8000d33c]:add a1, a1, s1
[0x8000d340]:ld t5, 544(a1)
[0x8000d344]:sub a1, a1, s1
[0x8000d348]:lui s1, 4
[0x8000d34c]:addiw s1, s1, 2048
[0x8000d350]:add a1, a1, s1
[0x8000d354]:ld t4, 552(a1)
[0x8000d358]:sub a1, a1, s1
[0x8000d35c]:addi s1, zero, 0
[0x8000d360]:csrrw zero, fcsr, s1
[0x8000d364]:fmax.s t6, t5, t4

[0x8000d364]:fmax.s t6, t5, t4
[0x8000d368]:csrrs a2, fcsr, zero
[0x8000d36c]:sd t6, 496(fp)
[0x8000d370]:sd a2, 504(fp)
[0x8000d374]:lui s1, 4
[0x8000d378]:addiw s1, s1, 2048
[0x8000d37c]:add a1, a1, s1
[0x8000d380]:ld t5, 560(a1)
[0x8000d384]:sub a1, a1, s1
[0x8000d388]:lui s1, 4
[0x8000d38c]:addiw s1, s1, 2048
[0x8000d390]:add a1, a1, s1
[0x8000d394]:ld t4, 568(a1)
[0x8000d398]:sub a1, a1, s1
[0x8000d39c]:addi s1, zero, 0
[0x8000d3a0]:csrrw zero, fcsr, s1
[0x8000d3a4]:fmax.s t6, t5, t4

[0x8000d3a4]:fmax.s t6, t5, t4
[0x8000d3a8]:csrrs a2, fcsr, zero
[0x8000d3ac]:sd t6, 512(fp)
[0x8000d3b0]:sd a2, 520(fp)
[0x8000d3b4]:lui s1, 4
[0x8000d3b8]:addiw s1, s1, 2048
[0x8000d3bc]:add a1, a1, s1
[0x8000d3c0]:ld t5, 576(a1)
[0x8000d3c4]:sub a1, a1, s1
[0x8000d3c8]:lui s1, 4
[0x8000d3cc]:addiw s1, s1, 2048
[0x8000d3d0]:add a1, a1, s1
[0x8000d3d4]:ld t4, 584(a1)
[0x8000d3d8]:sub a1, a1, s1
[0x8000d3dc]:addi s1, zero, 0
[0x8000d3e0]:csrrw zero, fcsr, s1
[0x8000d3e4]:fmax.s t6, t5, t4

[0x8000d3e4]:fmax.s t6, t5, t4
[0x8000d3e8]:csrrs a2, fcsr, zero
[0x8000d3ec]:sd t6, 528(fp)
[0x8000d3f0]:sd a2, 536(fp)
[0x8000d3f4]:lui s1, 4
[0x8000d3f8]:addiw s1, s1, 2048
[0x8000d3fc]:add a1, a1, s1
[0x8000d400]:ld t5, 592(a1)
[0x8000d404]:sub a1, a1, s1
[0x8000d408]:lui s1, 4
[0x8000d40c]:addiw s1, s1, 2048
[0x8000d410]:add a1, a1, s1
[0x8000d414]:ld t4, 600(a1)
[0x8000d418]:sub a1, a1, s1
[0x8000d41c]:addi s1, zero, 0
[0x8000d420]:csrrw zero, fcsr, s1
[0x8000d424]:fmax.s t6, t5, t4

[0x8000d424]:fmax.s t6, t5, t4
[0x8000d428]:csrrs a2, fcsr, zero
[0x8000d42c]:sd t6, 544(fp)
[0x8000d430]:sd a2, 552(fp)
[0x8000d434]:lui s1, 4
[0x8000d438]:addiw s1, s1, 2048
[0x8000d43c]:add a1, a1, s1
[0x8000d440]:ld t5, 608(a1)
[0x8000d444]:sub a1, a1, s1
[0x8000d448]:lui s1, 4
[0x8000d44c]:addiw s1, s1, 2048
[0x8000d450]:add a1, a1, s1
[0x8000d454]:ld t4, 616(a1)
[0x8000d458]:sub a1, a1, s1
[0x8000d45c]:addi s1, zero, 0
[0x8000d460]:csrrw zero, fcsr, s1
[0x8000d464]:fmax.s t6, t5, t4

[0x8000d464]:fmax.s t6, t5, t4
[0x8000d468]:csrrs a2, fcsr, zero
[0x8000d46c]:sd t6, 560(fp)
[0x8000d470]:sd a2, 568(fp)
[0x8000d474]:lui s1, 4
[0x8000d478]:addiw s1, s1, 2048
[0x8000d47c]:add a1, a1, s1
[0x8000d480]:ld t5, 624(a1)
[0x8000d484]:sub a1, a1, s1
[0x8000d488]:lui s1, 4
[0x8000d48c]:addiw s1, s1, 2048
[0x8000d490]:add a1, a1, s1
[0x8000d494]:ld t4, 632(a1)
[0x8000d498]:sub a1, a1, s1
[0x8000d49c]:addi s1, zero, 0
[0x8000d4a0]:csrrw zero, fcsr, s1
[0x8000d4a4]:fmax.s t6, t5, t4

[0x8000d4a4]:fmax.s t6, t5, t4
[0x8000d4a8]:csrrs a2, fcsr, zero
[0x8000d4ac]:sd t6, 576(fp)
[0x8000d4b0]:sd a2, 584(fp)
[0x8000d4b4]:lui s1, 4
[0x8000d4b8]:addiw s1, s1, 2048
[0x8000d4bc]:add a1, a1, s1
[0x8000d4c0]:ld t5, 640(a1)
[0x8000d4c4]:sub a1, a1, s1
[0x8000d4c8]:lui s1, 4
[0x8000d4cc]:addiw s1, s1, 2048
[0x8000d4d0]:add a1, a1, s1
[0x8000d4d4]:ld t4, 648(a1)
[0x8000d4d8]:sub a1, a1, s1
[0x8000d4dc]:addi s1, zero, 0
[0x8000d4e0]:csrrw zero, fcsr, s1
[0x8000d4e4]:fmax.s t6, t5, t4

[0x8000d4e4]:fmax.s t6, t5, t4
[0x8000d4e8]:csrrs a2, fcsr, zero
[0x8000d4ec]:sd t6, 592(fp)
[0x8000d4f0]:sd a2, 600(fp)
[0x8000d4f4]:lui s1, 4
[0x8000d4f8]:addiw s1, s1, 2048
[0x8000d4fc]:add a1, a1, s1
[0x8000d500]:ld t5, 656(a1)
[0x8000d504]:sub a1, a1, s1
[0x8000d508]:lui s1, 4
[0x8000d50c]:addiw s1, s1, 2048
[0x8000d510]:add a1, a1, s1
[0x8000d514]:ld t4, 664(a1)
[0x8000d518]:sub a1, a1, s1
[0x8000d51c]:addi s1, zero, 0
[0x8000d520]:csrrw zero, fcsr, s1
[0x8000d524]:fmax.s t6, t5, t4

[0x8000d524]:fmax.s t6, t5, t4
[0x8000d528]:csrrs a2, fcsr, zero
[0x8000d52c]:sd t6, 608(fp)
[0x8000d530]:sd a2, 616(fp)
[0x8000d534]:lui s1, 4
[0x8000d538]:addiw s1, s1, 2048
[0x8000d53c]:add a1, a1, s1
[0x8000d540]:ld t5, 672(a1)
[0x8000d544]:sub a1, a1, s1
[0x8000d548]:lui s1, 4
[0x8000d54c]:addiw s1, s1, 2048
[0x8000d550]:add a1, a1, s1
[0x8000d554]:ld t4, 680(a1)
[0x8000d558]:sub a1, a1, s1
[0x8000d55c]:addi s1, zero, 0
[0x8000d560]:csrrw zero, fcsr, s1
[0x8000d564]:fmax.s t6, t5, t4



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fmax.s', 'rs1 : x30', 'rs2 : x31', 'rd : x31', 'rs2 == rd != rs1', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003bc]:fmax.s t6, t5, t6
	-[0x800003c0]:csrrs tp, fcsr, zero
	-[0x800003c4]:sd t6, 0(ra)
	-[0x800003c8]:sd tp, 8(ra)
Current Store : [0x800003c8] : sd tp, 8(ra) -- Store: [0x80012e20]:0x0000000000000000




Last Coverpoint : ['rs1 : x29', 'rs2 : x29', 'rd : x29', 'rs1 == rs2 == rd']
Last Code Sequence : 
	-[0x800003dc]:fmax.s t4, t4, t4
	-[0x800003e0]:csrrs tp, fcsr, zero
	-[0x800003e4]:sd t4, 16(ra)
	-[0x800003e8]:sd tp, 24(ra)
Current Store : [0x800003e8] : sd tp, 24(ra) -- Store: [0x80012e30]:0x0000000000000000




Last Coverpoint : ['rs1 : x31', 'rs2 : x28', 'rd : x30', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfa and fm2 == 0x183299 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800003fc]:fmax.s t5, t6, t3
	-[0x80000400]:csrrs tp, fcsr, zero
	-[0x80000404]:sd t5, 32(ra)
	-[0x80000408]:sd tp, 40(ra)
Current Store : [0x80000408] : sd tp, 40(ra) -- Store: [0x80012e40]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rs2 : x30', 'rd : x28', 'rs1 == rd != rs2', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000041c]:fmax.s t3, t3, t5
	-[0x80000420]:csrrs tp, fcsr, zero
	-[0x80000424]:sd t3, 48(ra)
	-[0x80000428]:sd tp, 56(ra)
Current Store : [0x80000428] : sd tp, 56(ra) -- Store: [0x80012e50]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rs2 : x26', 'rd : x27', 'rs1 == rs2 != rd']
Last Code Sequence : 
	-[0x8000043c]:fmax.s s11, s10, s10
	-[0x80000440]:csrrs tp, fcsr, zero
	-[0x80000444]:sd s11, 64(ra)
	-[0x80000448]:sd tp, 72(ra)
Current Store : [0x80000448] : sd tp, 72(ra) -- Store: [0x80012e60]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rs2 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000045c]:fmax.s s10, s11, s9
	-[0x80000460]:csrrs tp, fcsr, zero
	-[0x80000464]:sd s10, 80(ra)
	-[0x80000468]:sd tp, 88(ra)
Current Store : [0x80000468] : sd tp, 88(ra) -- Store: [0x80012e70]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rs2 : x27', 'rd : x25', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000047c]:fmax.s s9, s8, s11
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s9, 96(ra)
	-[0x80000488]:sd tp, 104(ra)
Current Store : [0x80000488] : sd tp, 104(ra) -- Store: [0x80012e80]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rs2 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000049c]:fmax.s s8, s9, s7
	-[0x800004a0]:csrrs tp, fcsr, zero
	-[0x800004a4]:sd s8, 112(ra)
	-[0x800004a8]:sd tp, 120(ra)
Current Store : [0x800004a8] : sd tp, 120(ra) -- Store: [0x80012e90]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rs2 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004bc]:fmax.s s7, s6, s8
	-[0x800004c0]:csrrs tp, fcsr, zero
	-[0x800004c4]:sd s7, 128(ra)
	-[0x800004c8]:sd tp, 136(ra)
Current Store : [0x800004c8] : sd tp, 136(ra) -- Store: [0x80012ea0]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rs2 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfa and fm2 == 0x6a2c24 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004dc]:fmax.s s6, s7, s5
	-[0x800004e0]:csrrs tp, fcsr, zero
	-[0x800004e4]:sd s6, 144(ra)
	-[0x800004e8]:sd tp, 152(ra)
Current Store : [0x800004e8] : sd tp, 152(ra) -- Store: [0x80012eb0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rs2 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800004fc]:fmax.s s5, s4, s6
	-[0x80000500]:csrrs tp, fcsr, zero
	-[0x80000504]:sd s5, 160(ra)
	-[0x80000508]:sd tp, 168(ra)
Current Store : [0x80000508] : sd tp, 168(ra) -- Store: [0x80012ec0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rs2 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000051c]:fmax.s s4, s5, s3
	-[0x80000520]:csrrs tp, fcsr, zero
	-[0x80000524]:sd s4, 176(ra)
	-[0x80000528]:sd tp, 184(ra)
Current Store : [0x80000528] : sd tp, 184(ra) -- Store: [0x80012ed0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rs2 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000053c]:fmax.s s3, s2, s4
	-[0x80000540]:csrrs tp, fcsr, zero
	-[0x80000544]:sd s3, 192(ra)
	-[0x80000548]:sd tp, 200(ra)
Current Store : [0x80000548] : sd tp, 200(ra) -- Store: [0x80012ee0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rs2 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000055c]:fmax.s s2, s3, a7
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd s2, 208(ra)
	-[0x80000568]:sd tp, 216(ra)
Current Store : [0x80000568] : sd tp, 216(ra) -- Store: [0x80012ef0]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rs2 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000057c]:fmax.s a7, a6, s2
	-[0x80000580]:csrrs tp, fcsr, zero
	-[0x80000584]:sd a7, 224(ra)
	-[0x80000588]:sd tp, 232(ra)
Current Store : [0x80000588] : sd tp, 232(ra) -- Store: [0x80012f00]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rs2 : x15', 'rd : x16', 'fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000059c]:fmax.s a6, a7, a5
	-[0x800005a0]:csrrs tp, fcsr, zero
	-[0x800005a4]:sd a6, 240(ra)
	-[0x800005a8]:sd tp, 248(ra)
Current Store : [0x800005a8] : sd tp, 248(ra) -- Store: [0x80012f10]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rs2 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005bc]:fmax.s a5, a4, a6
	-[0x800005c0]:csrrs tp, fcsr, zero
	-[0x800005c4]:sd a5, 256(ra)
	-[0x800005c8]:sd tp, 264(ra)
Current Store : [0x800005c8] : sd tp, 264(ra) -- Store: [0x80012f20]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rs2 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfa and fm2 == 0x291dc8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800005dc]:fmax.s a4, a5, a3
	-[0x800005e0]:csrrs tp, fcsr, zero
	-[0x800005e4]:sd a4, 272(ra)
	-[0x800005e8]:sd tp, 280(ra)
Current Store : [0x800005e8] : sd tp, 280(ra) -- Store: [0x80012f30]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rs2 : x14', 'rd : x13', 'fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800005fc]:fmax.s a3, a2, a4
	-[0x80000600]:csrrs tp, fcsr, zero
	-[0x80000604]:sd a3, 288(ra)
	-[0x80000608]:sd tp, 296(ra)
Current Store : [0x80000608] : sd tp, 296(ra) -- Store: [0x80012f40]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rs2 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000061c]:fmax.s a2, a3, a1
	-[0x80000620]:csrrs tp, fcsr, zero
	-[0x80000624]:sd a2, 304(ra)
	-[0x80000628]:sd tp, 312(ra)
Current Store : [0x80000628] : sd tp, 312(ra) -- Store: [0x80012f50]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rs2 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000063c]:fmax.s a1, a0, a2
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd a1, 320(ra)
	-[0x80000648]:sd tp, 328(ra)
Current Store : [0x80000648] : sd tp, 328(ra) -- Store: [0x80012f60]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rs2 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfb and fm2 == 0x153eee and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000065c]:fmax.s a0, a1, s1
	-[0x80000660]:csrrs tp, fcsr, zero
	-[0x80000664]:sd a0, 336(ra)
	-[0x80000668]:sd tp, 344(ra)
Current Store : [0x80000668] : sd tp, 344(ra) -- Store: [0x80012f70]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rs2 : x10', 'rd : x9', 'fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000684]:fmax.s s1, fp, a0
	-[0x80000688]:csrrs a2, fcsr, zero
	-[0x8000068c]:sd s1, 352(ra)
	-[0x80000690]:sd a2, 360(ra)
Current Store : [0x80000690] : sd a2, 360(ra) -- Store: [0x80012f80]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rs2 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006a4]:fmax.s fp, s1, t2
	-[0x800006a8]:csrrs a2, fcsr, zero
	-[0x800006ac]:sd fp, 368(ra)
	-[0x800006b0]:sd a2, 376(ra)
Current Store : [0x800006b0] : sd a2, 376(ra) -- Store: [0x80012f90]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rs2 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006c4]:fmax.s t2, t1, fp
	-[0x800006c8]:csrrs a2, fcsr, zero
	-[0x800006cc]:sd t2, 384(ra)
	-[0x800006d0]:sd a2, 392(ra)
Current Store : [0x800006d0] : sd a2, 392(ra) -- Store: [0x80012fa0]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rs2 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfb and fm2 == 0x1946c8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800006ec]:fmax.s t1, t2, t0
	-[0x800006f0]:csrrs a2, fcsr, zero
	-[0x800006f4]:sd t1, 0(fp)
	-[0x800006f8]:sd a2, 8(fp)
Current Store : [0x800006f8] : sd a2, 8(fp) -- Store: [0x80012fb0]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rs2 : x6', 'rd : x5', 'fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000070c]:fmax.s t0, tp, t1
	-[0x80000710]:csrrs a2, fcsr, zero
	-[0x80000714]:sd t0, 16(fp)
	-[0x80000718]:sd a2, 24(fp)
Current Store : [0x80000718] : sd a2, 24(fp) -- Store: [0x80012fc0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rs2 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000072c]:fmax.s tp, t0, gp
	-[0x80000730]:csrrs a2, fcsr, zero
	-[0x80000734]:sd tp, 32(fp)
	-[0x80000738]:sd a2, 40(fp)
Current Store : [0x80000738] : sd a2, 40(fp) -- Store: [0x80012fd0]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rs2 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000074c]:fmax.s gp, sp, tp
	-[0x80000750]:csrrs a2, fcsr, zero
	-[0x80000754]:sd gp, 48(fp)
	-[0x80000758]:sd a2, 56(fp)
Current Store : [0x80000758] : sd a2, 56(fp) -- Store: [0x80012fe0]:0x0000000000000000




Last Coverpoint : ['rs1 : x3', 'rs2 : x1', 'rd : x2', 'fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000076c]:fmax.s sp, gp, ra
	-[0x80000770]:csrrs a2, fcsr, zero
	-[0x80000774]:sd sp, 64(fp)
	-[0x80000778]:sd a2, 72(fp)
Current Store : [0x80000778] : sd a2, 72(fp) -- Store: [0x80012ff0]:0x0000000000000000




Last Coverpoint : ['rs1 : x0', 'rs2 : x2', 'rd : x1']
Last Code Sequence : 
	-[0x8000078c]:fmax.s ra, zero, sp
	-[0x80000790]:csrrs a2, fcsr, zero
	-[0x80000794]:sd ra, 80(fp)
	-[0x80000798]:sd a2, 88(fp)
Current Store : [0x80000798] : sd a2, 88(fp) -- Store: [0x80013000]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ac]:fmax.s t6, ra, t5
	-[0x800007b0]:csrrs a2, fcsr, zero
	-[0x800007b4]:sd t6, 96(fp)
	-[0x800007b8]:sd a2, 104(fp)
Current Store : [0x800007b8] : sd a2, 104(fp) -- Store: [0x80013010]:0x0000000000000000




Last Coverpoint : ['rs2 : x0']
Last Code Sequence : 
	-[0x800007cc]:fmax.s t6, t5, zero
	-[0x800007d0]:csrrs a2, fcsr, zero
	-[0x800007d4]:sd t6, 112(fp)
	-[0x800007d8]:sd a2, 120(fp)
Current Store : [0x800007d8] : sd a2, 120(fp) -- Store: [0x80013020]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800007ec]:fmax.s zero, t6, t5
	-[0x800007f0]:csrrs a2, fcsr, zero
	-[0x800007f4]:sd zero, 128(fp)
	-[0x800007f8]:sd a2, 136(fp)
Current Store : [0x800007f8] : sd a2, 136(fp) -- Store: [0x80013030]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000080c]:fmax.s t6, t5, t4
	-[0x80000810]:csrrs a2, fcsr, zero
	-[0x80000814]:sd t6, 144(fp)
	-[0x80000818]:sd a2, 152(fp)
Current Store : [0x80000818] : sd a2, 152(fp) -- Store: [0x80013040]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000082c]:fmax.s t6, t5, t4
	-[0x80000830]:csrrs a2, fcsr, zero
	-[0x80000834]:sd t6, 160(fp)
	-[0x80000838]:sd a2, 168(fp)
Current Store : [0x80000838] : sd a2, 168(fp) -- Store: [0x80013050]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000084c]:fmax.s t6, t5, t4
	-[0x80000850]:csrrs a2, fcsr, zero
	-[0x80000854]:sd t6, 176(fp)
	-[0x80000858]:sd a2, 184(fp)
Current Store : [0x80000858] : sd a2, 184(fp) -- Store: [0x80013060]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000086c]:fmax.s t6, t5, t4
	-[0x80000870]:csrrs a2, fcsr, zero
	-[0x80000874]:sd t6, 192(fp)
	-[0x80000878]:sd a2, 200(fp)
Current Store : [0x80000878] : sd a2, 200(fp) -- Store: [0x80013070]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000088c]:fmax.s t6, t5, t4
	-[0x80000890]:csrrs a2, fcsr, zero
	-[0x80000894]:sd t6, 208(fp)
	-[0x80000898]:sd a2, 216(fp)
Current Store : [0x80000898] : sd a2, 216(fp) -- Store: [0x80013080]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ac]:fmax.s t6, t5, t4
	-[0x800008b0]:csrrs a2, fcsr, zero
	-[0x800008b4]:sd t6, 224(fp)
	-[0x800008b8]:sd a2, 232(fp)
Current Store : [0x800008b8] : sd a2, 232(fp) -- Store: [0x80013090]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008cc]:fmax.s t6, t5, t4
	-[0x800008d0]:csrrs a2, fcsr, zero
	-[0x800008d4]:sd t6, 240(fp)
	-[0x800008d8]:sd a2, 248(fp)
Current Store : [0x800008d8] : sd a2, 248(fp) -- Store: [0x800130a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800008ec]:fmax.s t6, t5, t4
	-[0x800008f0]:csrrs a2, fcsr, zero
	-[0x800008f4]:sd t6, 256(fp)
	-[0x800008f8]:sd a2, 264(fp)
Current Store : [0x800008f8] : sd a2, 264(fp) -- Store: [0x800130b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000090c]:fmax.s t6, t5, t4
	-[0x80000910]:csrrs a2, fcsr, zero
	-[0x80000914]:sd t6, 272(fp)
	-[0x80000918]:sd a2, 280(fp)
Current Store : [0x80000918] : sd a2, 280(fp) -- Store: [0x800130c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000092c]:fmax.s t6, t5, t4
	-[0x80000930]:csrrs a2, fcsr, zero
	-[0x80000934]:sd t6, 288(fp)
	-[0x80000938]:sd a2, 296(fp)
Current Store : [0x80000938] : sd a2, 296(fp) -- Store: [0x800130d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000094c]:fmax.s t6, t5, t4
	-[0x80000950]:csrrs a2, fcsr, zero
	-[0x80000954]:sd t6, 304(fp)
	-[0x80000958]:sd a2, 312(fp)
Current Store : [0x80000958] : sd a2, 312(fp) -- Store: [0x800130e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000096c]:fmax.s t6, t5, t4
	-[0x80000970]:csrrs a2, fcsr, zero
	-[0x80000974]:sd t6, 320(fp)
	-[0x80000978]:sd a2, 328(fp)
Current Store : [0x80000978] : sd a2, 328(fp) -- Store: [0x800130f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000098c]:fmax.s t6, t5, t4
	-[0x80000990]:csrrs a2, fcsr, zero
	-[0x80000994]:sd t6, 336(fp)
	-[0x80000998]:sd a2, 344(fp)
Current Store : [0x80000998] : sd a2, 344(fp) -- Store: [0x80013100]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ac]:fmax.s t6, t5, t4
	-[0x800009b0]:csrrs a2, fcsr, zero
	-[0x800009b4]:sd t6, 352(fp)
	-[0x800009b8]:sd a2, 360(fp)
Current Store : [0x800009b8] : sd a2, 360(fp) -- Store: [0x80013110]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009cc]:fmax.s t6, t5, t4
	-[0x800009d0]:csrrs a2, fcsr, zero
	-[0x800009d4]:sd t6, 368(fp)
	-[0x800009d8]:sd a2, 376(fp)
Current Store : [0x800009d8] : sd a2, 376(fp) -- Store: [0x80013120]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800009ec]:fmax.s t6, t5, t4
	-[0x800009f0]:csrrs a2, fcsr, zero
	-[0x800009f4]:sd t6, 384(fp)
	-[0x800009f8]:sd a2, 392(fp)
Current Store : [0x800009f8] : sd a2, 392(fp) -- Store: [0x80013130]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a0c]:fmax.s t6, t5, t4
	-[0x80000a10]:csrrs a2, fcsr, zero
	-[0x80000a14]:sd t6, 400(fp)
	-[0x80000a18]:sd a2, 408(fp)
Current Store : [0x80000a18] : sd a2, 408(fp) -- Store: [0x80013140]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a2c]:fmax.s t6, t5, t4
	-[0x80000a30]:csrrs a2, fcsr, zero
	-[0x80000a34]:sd t6, 416(fp)
	-[0x80000a38]:sd a2, 424(fp)
Current Store : [0x80000a38] : sd a2, 424(fp) -- Store: [0x80013150]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a4c]:fmax.s t6, t5, t4
	-[0x80000a50]:csrrs a2, fcsr, zero
	-[0x80000a54]:sd t6, 432(fp)
	-[0x80000a58]:sd a2, 440(fp)
Current Store : [0x80000a58] : sd a2, 440(fp) -- Store: [0x80013160]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000a6c]:fmax.s t6, t5, t4
	-[0x80000a70]:csrrs a2, fcsr, zero
	-[0x80000a74]:sd t6, 448(fp)
	-[0x80000a78]:sd a2, 456(fp)
Current Store : [0x80000a78] : sd a2, 456(fp) -- Store: [0x80013170]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000a8c]:fmax.s t6, t5, t4
	-[0x80000a90]:csrrs a2, fcsr, zero
	-[0x80000a94]:sd t6, 464(fp)
	-[0x80000a98]:sd a2, 472(fp)
Current Store : [0x80000a98] : sd a2, 472(fp) -- Store: [0x80013180]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000aac]:fmax.s t6, t5, t4
	-[0x80000ab0]:csrrs a2, fcsr, zero
	-[0x80000ab4]:sd t6, 480(fp)
	-[0x80000ab8]:sd a2, 488(fp)
Current Store : [0x80000ab8] : sd a2, 488(fp) -- Store: [0x80013190]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000acc]:fmax.s t6, t5, t4
	-[0x80000ad0]:csrrs a2, fcsr, zero
	-[0x80000ad4]:sd t6, 496(fp)
	-[0x80000ad8]:sd a2, 504(fp)
Current Store : [0x80000ad8] : sd a2, 504(fp) -- Store: [0x800131a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000aec]:fmax.s t6, t5, t4
	-[0x80000af0]:csrrs a2, fcsr, zero
	-[0x80000af4]:sd t6, 512(fp)
	-[0x80000af8]:sd a2, 520(fp)
Current Store : [0x80000af8] : sd a2, 520(fp) -- Store: [0x800131b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b0c]:fmax.s t6, t5, t4
	-[0x80000b10]:csrrs a2, fcsr, zero
	-[0x80000b14]:sd t6, 528(fp)
	-[0x80000b18]:sd a2, 536(fp)
Current Store : [0x80000b18] : sd a2, 536(fp) -- Store: [0x800131c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b2c]:fmax.s t6, t5, t4
	-[0x80000b30]:csrrs a2, fcsr, zero
	-[0x80000b34]:sd t6, 544(fp)
	-[0x80000b38]:sd a2, 552(fp)
Current Store : [0x80000b38] : sd a2, 552(fp) -- Store: [0x800131d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b4c]:fmax.s t6, t5, t4
	-[0x80000b50]:csrrs a2, fcsr, zero
	-[0x80000b54]:sd t6, 560(fp)
	-[0x80000b58]:sd a2, 568(fp)
Current Store : [0x80000b58] : sd a2, 568(fp) -- Store: [0x800131e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000b6c]:fmax.s t6, t5, t4
	-[0x80000b70]:csrrs a2, fcsr, zero
	-[0x80000b74]:sd t6, 576(fp)
	-[0x80000b78]:sd a2, 584(fp)
Current Store : [0x80000b78] : sd a2, 584(fp) -- Store: [0x800131f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000b8c]:fmax.s t6, t5, t4
	-[0x80000b90]:csrrs a2, fcsr, zero
	-[0x80000b94]:sd t6, 592(fp)
	-[0x80000b98]:sd a2, 600(fp)
Current Store : [0x80000b98] : sd a2, 600(fp) -- Store: [0x80013200]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bac]:fmax.s t6, t5, t4
	-[0x80000bb0]:csrrs a2, fcsr, zero
	-[0x80000bb4]:sd t6, 608(fp)
	-[0x80000bb8]:sd a2, 616(fp)
Current Store : [0x80000bb8] : sd a2, 616(fp) -- Store: [0x80013210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000bcc]:fmax.s t6, t5, t4
	-[0x80000bd0]:csrrs a2, fcsr, zero
	-[0x80000bd4]:sd t6, 624(fp)
	-[0x80000bd8]:sd a2, 632(fp)
Current Store : [0x80000bd8] : sd a2, 632(fp) -- Store: [0x80013220]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000bec]:fmax.s t6, t5, t4
	-[0x80000bf0]:csrrs a2, fcsr, zero
	-[0x80000bf4]:sd t6, 640(fp)
	-[0x80000bf8]:sd a2, 648(fp)
Current Store : [0x80000bf8] : sd a2, 648(fp) -- Store: [0x80013230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c0c]:fmax.s t6, t5, t4
	-[0x80000c10]:csrrs a2, fcsr, zero
	-[0x80000c14]:sd t6, 656(fp)
	-[0x80000c18]:sd a2, 664(fp)
Current Store : [0x80000c18] : sd a2, 664(fp) -- Store: [0x80013240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c2c]:fmax.s t6, t5, t4
	-[0x80000c30]:csrrs a2, fcsr, zero
	-[0x80000c34]:sd t6, 672(fp)
	-[0x80000c38]:sd a2, 680(fp)
Current Store : [0x80000c38] : sd a2, 680(fp) -- Store: [0x80013250]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c4c]:fmax.s t6, t5, t4
	-[0x80000c50]:csrrs a2, fcsr, zero
	-[0x80000c54]:sd t6, 688(fp)
	-[0x80000c58]:sd a2, 696(fp)
Current Store : [0x80000c58] : sd a2, 696(fp) -- Store: [0x80013260]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000c6c]:fmax.s t6, t5, t4
	-[0x80000c70]:csrrs a2, fcsr, zero
	-[0x80000c74]:sd t6, 704(fp)
	-[0x80000c78]:sd a2, 712(fp)
Current Store : [0x80000c78] : sd a2, 712(fp) -- Store: [0x80013270]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000c8c]:fmax.s t6, t5, t4
	-[0x80000c90]:csrrs a2, fcsr, zero
	-[0x80000c94]:sd t6, 720(fp)
	-[0x80000c98]:sd a2, 728(fp)
Current Store : [0x80000c98] : sd a2, 728(fp) -- Store: [0x80013280]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cac]:fmax.s t6, t5, t4
	-[0x80000cb0]:csrrs a2, fcsr, zero
	-[0x80000cb4]:sd t6, 736(fp)
	-[0x80000cb8]:sd a2, 744(fp)
Current Store : [0x80000cb8] : sd a2, 744(fp) -- Store: [0x80013290]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x5b76ec and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000ccc]:fmax.s t6, t5, t4
	-[0x80000cd0]:csrrs a2, fcsr, zero
	-[0x80000cd4]:sd t6, 752(fp)
	-[0x80000cd8]:sd a2, 760(fp)
Current Store : [0x80000cd8] : sd a2, 760(fp) -- Store: [0x800132a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x5b76ec and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000cec]:fmax.s t6, t5, t4
	-[0x80000cf0]:csrrs a2, fcsr, zero
	-[0x80000cf4]:sd t6, 768(fp)
	-[0x80000cf8]:sd a2, 776(fp)
Current Store : [0x80000cf8] : sd a2, 776(fp) -- Store: [0x800132b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x80 and fm2 == 0x5b76ec and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d0c]:fmax.s t6, t5, t4
	-[0x80000d10]:csrrs a2, fcsr, zero
	-[0x80000d14]:sd t6, 784(fp)
	-[0x80000d18]:sd a2, 792(fp)
Current Store : [0x80000d18] : sd a2, 792(fp) -- Store: [0x800132c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d2c]:fmax.s t6, t5, t4
	-[0x80000d30]:csrrs a2, fcsr, zero
	-[0x80000d34]:sd t6, 800(fp)
	-[0x80000d38]:sd a2, 808(fp)
Current Store : [0x80000d38] : sd a2, 808(fp) -- Store: [0x800132d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d4c]:fmax.s t6, t5, t4
	-[0x80000d50]:csrrs a2, fcsr, zero
	-[0x80000d54]:sd t6, 816(fp)
	-[0x80000d58]:sd a2, 824(fp)
Current Store : [0x80000d58] : sd a2, 824(fp) -- Store: [0x800132e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfa and fm2 == 0x183299 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d6c]:fmax.s t6, t5, t4
	-[0x80000d70]:csrrs a2, fcsr, zero
	-[0x80000d74]:sd t6, 832(fp)
	-[0x80000d78]:sd a2, 840(fp)
Current Store : [0x80000d78] : sd a2, 840(fp) -- Store: [0x800132f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000d8c]:fmax.s t6, t5, t4
	-[0x80000d90]:csrrs a2, fcsr, zero
	-[0x80000d94]:sd t6, 848(fp)
	-[0x80000d98]:sd a2, 856(fp)
Current Store : [0x80000d98] : sd a2, 856(fp) -- Store: [0x80013300]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dac]:fmax.s t6, t5, t4
	-[0x80000db0]:csrrs a2, fcsr, zero
	-[0x80000db4]:sd t6, 864(fp)
	-[0x80000db8]:sd a2, 872(fp)
Current Store : [0x80000db8] : sd a2, 872(fp) -- Store: [0x80013310]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dcc]:fmax.s t6, t5, t4
	-[0x80000dd0]:csrrs a2, fcsr, zero
	-[0x80000dd4]:sd t6, 880(fp)
	-[0x80000dd8]:sd a2, 888(fp)
Current Store : [0x80000dd8] : sd a2, 888(fp) -- Store: [0x80013320]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 0 and fe2 == 0xfa and fm2 == 0x183299 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000dec]:fmax.s t6, t5, t4
	-[0x80000df0]:csrrs a2, fcsr, zero
	-[0x80000df4]:sd t6, 896(fp)
	-[0x80000df8]:sd a2, 904(fp)
Current Store : [0x80000df8] : sd a2, 904(fp) -- Store: [0x80013330]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e0c]:fmax.s t6, t5, t4
	-[0x80000e10]:csrrs a2, fcsr, zero
	-[0x80000e14]:sd t6, 912(fp)
	-[0x80000e18]:sd a2, 920(fp)
Current Store : [0x80000e18] : sd a2, 920(fp) -- Store: [0x80013340]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e2c]:fmax.s t6, t5, t4
	-[0x80000e30]:csrrs a2, fcsr, zero
	-[0x80000e34]:sd t6, 928(fp)
	-[0x80000e38]:sd a2, 936(fp)
Current Store : [0x80000e38] : sd a2, 936(fp) -- Store: [0x80013350]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e4c]:fmax.s t6, t5, t4
	-[0x80000e50]:csrrs a2, fcsr, zero
	-[0x80000e54]:sd t6, 944(fp)
	-[0x80000e58]:sd a2, 952(fp)
Current Store : [0x80000e58] : sd a2, 952(fp) -- Store: [0x80013360]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e6c]:fmax.s t6, t5, t4
	-[0x80000e70]:csrrs a2, fcsr, zero
	-[0x80000e74]:sd t6, 960(fp)
	-[0x80000e78]:sd a2, 968(fp)
Current Store : [0x80000e78] : sd a2, 968(fp) -- Store: [0x80013370]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000e8c]:fmax.s t6, t5, t4
	-[0x80000e90]:csrrs a2, fcsr, zero
	-[0x80000e94]:sd t6, 976(fp)
	-[0x80000e98]:sd a2, 984(fp)
Current Store : [0x80000e98] : sd a2, 984(fp) -- Store: [0x80013380]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000eac]:fmax.s t6, t5, t4
	-[0x80000eb0]:csrrs a2, fcsr, zero
	-[0x80000eb4]:sd t6, 992(fp)
	-[0x80000eb8]:sd a2, 1000(fp)
Current Store : [0x80000eb8] : sd a2, 1000(fp) -- Store: [0x80013390]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000ecc]:fmax.s t6, t5, t4
	-[0x80000ed0]:csrrs a2, fcsr, zero
	-[0x80000ed4]:sd t6, 1008(fp)
	-[0x80000ed8]:sd a2, 1016(fp)
Current Store : [0x80000ed8] : sd a2, 1016(fp) -- Store: [0x800133a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfa and fm2 == 0x183299 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000eec]:fmax.s t6, t5, t4
	-[0x80000ef0]:csrrs a2, fcsr, zero
	-[0x80000ef4]:sd t6, 1024(fp)
	-[0x80000ef8]:sd a2, 1032(fp)
Current Store : [0x80000ef8] : sd a2, 1032(fp) -- Store: [0x800133b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f0c]:fmax.s t6, t5, t4
	-[0x80000f10]:csrrs a2, fcsr, zero
	-[0x80000f14]:sd t6, 1040(fp)
	-[0x80000f18]:sd a2, 1048(fp)
Current Store : [0x80000f18] : sd a2, 1048(fp) -- Store: [0x800133c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f2c]:fmax.s t6, t5, t4
	-[0x80000f30]:csrrs a2, fcsr, zero
	-[0x80000f34]:sd t6, 1056(fp)
	-[0x80000f38]:sd a2, 1064(fp)
Current Store : [0x80000f38] : sd a2, 1064(fp) -- Store: [0x800133d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f4c]:fmax.s t6, t5, t4
	-[0x80000f50]:csrrs a2, fcsr, zero
	-[0x80000f54]:sd t6, 1072(fp)
	-[0x80000f58]:sd a2, 1080(fp)
Current Store : [0x80000f58] : sd a2, 1080(fp) -- Store: [0x800133e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000f6c]:fmax.s t6, t5, t4
	-[0x80000f70]:csrrs a2, fcsr, zero
	-[0x80000f74]:sd t6, 1088(fp)
	-[0x80000f78]:sd a2, 1096(fp)
Current Store : [0x80000f78] : sd a2, 1096(fp) -- Store: [0x800133f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000f8c]:fmax.s t6, t5, t4
	-[0x80000f90]:csrrs a2, fcsr, zero
	-[0x80000f94]:sd t6, 1104(fp)
	-[0x80000f98]:sd a2, 1112(fp)
Current Store : [0x80000f98] : sd a2, 1112(fp) -- Store: [0x80013400]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fac]:fmax.s t6, t5, t4
	-[0x80000fb0]:csrrs a2, fcsr, zero
	-[0x80000fb4]:sd t6, 1120(fp)
	-[0x80000fb8]:sd a2, 1128(fp)
Current Store : [0x80000fb8] : sd a2, 1128(fp) -- Store: [0x80013410]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80000fcc]:fmax.s t6, t5, t4
	-[0x80000fd0]:csrrs a2, fcsr, zero
	-[0x80000fd4]:sd t6, 1136(fp)
	-[0x80000fd8]:sd a2, 1144(fp)
Current Store : [0x80000fd8] : sd a2, 1144(fp) -- Store: [0x80013420]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80000fec]:fmax.s t6, t5, t4
	-[0x80000ff0]:csrrs a2, fcsr, zero
	-[0x80000ff4]:sd t6, 1152(fp)
	-[0x80000ff8]:sd a2, 1160(fp)
Current Store : [0x80000ff8] : sd a2, 1160(fp) -- Store: [0x80013430]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000100c]:fmax.s t6, t5, t4
	-[0x80001010]:csrrs a2, fcsr, zero
	-[0x80001014]:sd t6, 1168(fp)
	-[0x80001018]:sd a2, 1176(fp)
Current Store : [0x80001018] : sd a2, 1176(fp) -- Store: [0x80013440]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 0 and fe2 == 0xfa and fm2 == 0x183299 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000102c]:fmax.s t6, t5, t4
	-[0x80001030]:csrrs a2, fcsr, zero
	-[0x80001034]:sd t6, 1184(fp)
	-[0x80001038]:sd a2, 1192(fp)
Current Store : [0x80001038] : sd a2, 1192(fp) -- Store: [0x80013450]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x183299 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000104c]:fmax.s t6, t5, t4
	-[0x80001050]:csrrs a2, fcsr, zero
	-[0x80001054]:sd t6, 1200(fp)
	-[0x80001058]:sd a2, 1208(fp)
Current Store : [0x80001058] : sd a2, 1208(fp) -- Store: [0x80013460]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000106c]:fmax.s t6, t5, t4
	-[0x80001070]:csrrs a2, fcsr, zero
	-[0x80001074]:sd t6, 1216(fp)
	-[0x80001078]:sd a2, 1224(fp)
Current Store : [0x80001078] : sd a2, 1224(fp) -- Store: [0x80013470]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000108c]:fmax.s t6, t5, t4
	-[0x80001090]:csrrs a2, fcsr, zero
	-[0x80001094]:sd t6, 1232(fp)
	-[0x80001098]:sd a2, 1240(fp)
Current Store : [0x80001098] : sd a2, 1240(fp) -- Store: [0x80013480]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010ac]:fmax.s t6, t5, t4
	-[0x800010b0]:csrrs a2, fcsr, zero
	-[0x800010b4]:sd t6, 1248(fp)
	-[0x800010b8]:sd a2, 1256(fp)
Current Store : [0x800010b8] : sd a2, 1256(fp) -- Store: [0x80013490]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010cc]:fmax.s t6, t5, t4
	-[0x800010d0]:csrrs a2, fcsr, zero
	-[0x800010d4]:sd t6, 1264(fp)
	-[0x800010d8]:sd a2, 1272(fp)
Current Store : [0x800010d8] : sd a2, 1272(fp) -- Store: [0x800134a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800010ec]:fmax.s t6, t5, t4
	-[0x800010f0]:csrrs a2, fcsr, zero
	-[0x800010f4]:sd t6, 1280(fp)
	-[0x800010f8]:sd a2, 1288(fp)
Current Store : [0x800010f8] : sd a2, 1288(fp) -- Store: [0x800134b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000110c]:fmax.s t6, t5, t4
	-[0x80001110]:csrrs a2, fcsr, zero
	-[0x80001114]:sd t6, 1296(fp)
	-[0x80001118]:sd a2, 1304(fp)
Current Store : [0x80001118] : sd a2, 1304(fp) -- Store: [0x800134c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000112c]:fmax.s t6, t5, t4
	-[0x80001130]:csrrs a2, fcsr, zero
	-[0x80001134]:sd t6, 1312(fp)
	-[0x80001138]:sd a2, 1320(fp)
Current Store : [0x80001138] : sd a2, 1320(fp) -- Store: [0x800134d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000114c]:fmax.s t6, t5, t4
	-[0x80001150]:csrrs a2, fcsr, zero
	-[0x80001154]:sd t6, 1328(fp)
	-[0x80001158]:sd a2, 1336(fp)
Current Store : [0x80001158] : sd a2, 1336(fp) -- Store: [0x800134e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000116c]:fmax.s t6, t5, t4
	-[0x80001170]:csrrs a2, fcsr, zero
	-[0x80001174]:sd t6, 1344(fp)
	-[0x80001178]:sd a2, 1352(fp)
Current Store : [0x80001178] : sd a2, 1352(fp) -- Store: [0x800134f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000118c]:fmax.s t6, t5, t4
	-[0x80001190]:csrrs a2, fcsr, zero
	-[0x80001194]:sd t6, 1360(fp)
	-[0x80001198]:sd a2, 1368(fp)
Current Store : [0x80001198] : sd a2, 1368(fp) -- Store: [0x80013500]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011ac]:fmax.s t6, t5, t4
	-[0x800011b0]:csrrs a2, fcsr, zero
	-[0x800011b4]:sd t6, 1376(fp)
	-[0x800011b8]:sd a2, 1384(fp)
Current Store : [0x800011b8] : sd a2, 1384(fp) -- Store: [0x80013510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011cc]:fmax.s t6, t5, t4
	-[0x800011d0]:csrrs a2, fcsr, zero
	-[0x800011d4]:sd t6, 1392(fp)
	-[0x800011d8]:sd a2, 1400(fp)
Current Store : [0x800011d8] : sd a2, 1400(fp) -- Store: [0x80013520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800011ec]:fmax.s t6, t5, t4
	-[0x800011f0]:csrrs a2, fcsr, zero
	-[0x800011f4]:sd t6, 1408(fp)
	-[0x800011f8]:sd a2, 1416(fp)
Current Store : [0x800011f8] : sd a2, 1416(fp) -- Store: [0x80013530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000120c]:fmax.s t6, t5, t4
	-[0x80001210]:csrrs a2, fcsr, zero
	-[0x80001214]:sd t6, 1424(fp)
	-[0x80001218]:sd a2, 1432(fp)
Current Store : [0x80001218] : sd a2, 1432(fp) -- Store: [0x80013540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000122c]:fmax.s t6, t5, t4
	-[0x80001230]:csrrs a2, fcsr, zero
	-[0x80001234]:sd t6, 1440(fp)
	-[0x80001238]:sd a2, 1448(fp)
Current Store : [0x80001238] : sd a2, 1448(fp) -- Store: [0x80013550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000124c]:fmax.s t6, t5, t4
	-[0x80001250]:csrrs a2, fcsr, zero
	-[0x80001254]:sd t6, 1456(fp)
	-[0x80001258]:sd a2, 1464(fp)
Current Store : [0x80001258] : sd a2, 1464(fp) -- Store: [0x80013560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000126c]:fmax.s t6, t5, t4
	-[0x80001270]:csrrs a2, fcsr, zero
	-[0x80001274]:sd t6, 1472(fp)
	-[0x80001278]:sd a2, 1480(fp)
Current Store : [0x80001278] : sd a2, 1480(fp) -- Store: [0x80013570]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000128c]:fmax.s t6, t5, t4
	-[0x80001290]:csrrs a2, fcsr, zero
	-[0x80001294]:sd t6, 1488(fp)
	-[0x80001298]:sd a2, 1496(fp)
Current Store : [0x80001298] : sd a2, 1496(fp) -- Store: [0x80013580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ac]:fmax.s t6, t5, t4
	-[0x800012b0]:csrrs a2, fcsr, zero
	-[0x800012b4]:sd t6, 1504(fp)
	-[0x800012b8]:sd a2, 1512(fp)
Current Store : [0x800012b8] : sd a2, 1512(fp) -- Store: [0x80013590]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012cc]:fmax.s t6, t5, t4
	-[0x800012d0]:csrrs a2, fcsr, zero
	-[0x800012d4]:sd t6, 1520(fp)
	-[0x800012d8]:sd a2, 1528(fp)
Current Store : [0x800012d8] : sd a2, 1528(fp) -- Store: [0x800135a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800012ec]:fmax.s t6, t5, t4
	-[0x800012f0]:csrrs a2, fcsr, zero
	-[0x800012f4]:sd t6, 1536(fp)
	-[0x800012f8]:sd a2, 1544(fp)
Current Store : [0x800012f8] : sd a2, 1544(fp) -- Store: [0x800135b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000130c]:fmax.s t6, t5, t4
	-[0x80001310]:csrrs a2, fcsr, zero
	-[0x80001314]:sd t6, 1552(fp)
	-[0x80001318]:sd a2, 1560(fp)
Current Store : [0x80001318] : sd a2, 1560(fp) -- Store: [0x800135c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000132c]:fmax.s t6, t5, t4
	-[0x80001330]:csrrs a2, fcsr, zero
	-[0x80001334]:sd t6, 1568(fp)
	-[0x80001338]:sd a2, 1576(fp)
Current Store : [0x80001338] : sd a2, 1576(fp) -- Store: [0x800135d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000134c]:fmax.s t6, t5, t4
	-[0x80001350]:csrrs a2, fcsr, zero
	-[0x80001354]:sd t6, 1584(fp)
	-[0x80001358]:sd a2, 1592(fp)
Current Store : [0x80001358] : sd a2, 1592(fp) -- Store: [0x800135e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000136c]:fmax.s t6, t5, t4
	-[0x80001370]:csrrs a2, fcsr, zero
	-[0x80001374]:sd t6, 1600(fp)
	-[0x80001378]:sd a2, 1608(fp)
Current Store : [0x80001378] : sd a2, 1608(fp) -- Store: [0x800135f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000138c]:fmax.s t6, t5, t4
	-[0x80001390]:csrrs a2, fcsr, zero
	-[0x80001394]:sd t6, 1616(fp)
	-[0x80001398]:sd a2, 1624(fp)
Current Store : [0x80001398] : sd a2, 1624(fp) -- Store: [0x80013600]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ac]:fmax.s t6, t5, t4
	-[0x800013b0]:csrrs a2, fcsr, zero
	-[0x800013b4]:sd t6, 1632(fp)
	-[0x800013b8]:sd a2, 1640(fp)
Current Store : [0x800013b8] : sd a2, 1640(fp) -- Store: [0x80013610]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013cc]:fmax.s t6, t5, t4
	-[0x800013d0]:csrrs a2, fcsr, zero
	-[0x800013d4]:sd t6, 1648(fp)
	-[0x800013d8]:sd a2, 1656(fp)
Current Store : [0x800013d8] : sd a2, 1656(fp) -- Store: [0x80013620]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800013ec]:fmax.s t6, t5, t4
	-[0x800013f0]:csrrs a2, fcsr, zero
	-[0x800013f4]:sd t6, 1664(fp)
	-[0x800013f8]:sd a2, 1672(fp)
Current Store : [0x800013f8] : sd a2, 1672(fp) -- Store: [0x80013630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0dc4a8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000140c]:fmax.s t6, t5, t4
	-[0x80001410]:csrrs a2, fcsr, zero
	-[0x80001414]:sd t6, 1680(fp)
	-[0x80001418]:sd a2, 1688(fp)
Current Store : [0x80001418] : sd a2, 1688(fp) -- Store: [0x80013640]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000142c]:fmax.s t6, t5, t4
	-[0x80001430]:csrrs a2, fcsr, zero
	-[0x80001434]:sd t6, 1696(fp)
	-[0x80001438]:sd a2, 1704(fp)
Current Store : [0x80001438] : sd a2, 1704(fp) -- Store: [0x80013650]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000144c]:fmax.s t6, t5, t4
	-[0x80001450]:csrrs a2, fcsr, zero
	-[0x80001454]:sd t6, 1712(fp)
	-[0x80001458]:sd a2, 1720(fp)
Current Store : [0x80001458] : sd a2, 1720(fp) -- Store: [0x80013660]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0dc4a8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000146c]:fmax.s t6, t5, t4
	-[0x80001470]:csrrs a2, fcsr, zero
	-[0x80001474]:sd t6, 1728(fp)
	-[0x80001478]:sd a2, 1736(fp)
Current Store : [0x80001478] : sd a2, 1736(fp) -- Store: [0x80013670]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000148c]:fmax.s t6, t5, t4
	-[0x80001490]:csrrs a2, fcsr, zero
	-[0x80001494]:sd t6, 1744(fp)
	-[0x80001498]:sd a2, 1752(fp)
Current Store : [0x80001498] : sd a2, 1752(fp) -- Store: [0x80013680]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x21d824 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800014ac]:fmax.s t6, t5, t4
	-[0x800014b0]:csrrs a2, fcsr, zero
	-[0x800014b4]:sd t6, 1760(fp)
	-[0x800014b8]:sd a2, 1768(fp)
Current Store : [0x800014b8] : sd a2, 1768(fp) -- Store: [0x80013690]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x21d824 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800014cc]:fmax.s t6, t5, t4
	-[0x800014d0]:csrrs a2, fcsr, zero
	-[0x800014d4]:sd t6, 1776(fp)
	-[0x800014d8]:sd a2, 1784(fp)
Current Store : [0x800014d8] : sd a2, 1784(fp) -- Store: [0x800136a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfd and fm1 == 0x3e3f3f and fs2 == 0 and fe2 == 0x7f and fm2 == 0x21d824 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800014ec]:fmax.s t6, t5, t4
	-[0x800014f0]:csrrs a2, fcsr, zero
	-[0x800014f4]:sd t6, 1792(fp)
	-[0x800014f8]:sd a2, 1800(fp)
Current Store : [0x800014f8] : sd a2, 1800(fp) -- Store: [0x800136b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000150c]:fmax.s t6, t5, t4
	-[0x80001510]:csrrs a2, fcsr, zero
	-[0x80001514]:sd t6, 1808(fp)
	-[0x80001518]:sd a2, 1816(fp)
Current Store : [0x80001518] : sd a2, 1816(fp) -- Store: [0x800136c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000152c]:fmax.s t6, t5, t4
	-[0x80001530]:csrrs a2, fcsr, zero
	-[0x80001534]:sd t6, 1824(fp)
	-[0x80001538]:sd a2, 1832(fp)
Current Store : [0x80001538] : sd a2, 1832(fp) -- Store: [0x800136d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000154c]:fmax.s t6, t5, t4
	-[0x80001550]:csrrs a2, fcsr, zero
	-[0x80001554]:sd t6, 1840(fp)
	-[0x80001558]:sd a2, 1848(fp)
Current Store : [0x80001558] : sd a2, 1848(fp) -- Store: [0x800136e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000156c]:fmax.s t6, t5, t4
	-[0x80001570]:csrrs a2, fcsr, zero
	-[0x80001574]:sd t6, 1856(fp)
	-[0x80001578]:sd a2, 1864(fp)
Current Store : [0x80001578] : sd a2, 1864(fp) -- Store: [0x800136f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000158c]:fmax.s t6, t5, t4
	-[0x80001590]:csrrs a2, fcsr, zero
	-[0x80001594]:sd t6, 1872(fp)
	-[0x80001598]:sd a2, 1880(fp)
Current Store : [0x80001598] : sd a2, 1880(fp) -- Store: [0x80013700]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 0 and fe2 == 0xfa and fm2 == 0x6a2c24 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015ac]:fmax.s t6, t5, t4
	-[0x800015b0]:csrrs a2, fcsr, zero
	-[0x800015b4]:sd t6, 1888(fp)
	-[0x800015b8]:sd a2, 1896(fp)
Current Store : [0x800015b8] : sd a2, 1896(fp) -- Store: [0x80013710]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015cc]:fmax.s t6, t5, t4
	-[0x800015d0]:csrrs a2, fcsr, zero
	-[0x800015d4]:sd t6, 1904(fp)
	-[0x800015d8]:sd a2, 1912(fp)
Current Store : [0x800015d8] : sd a2, 1912(fp) -- Store: [0x80013720]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800015ec]:fmax.s t6, t5, t4
	-[0x800015f0]:csrrs a2, fcsr, zero
	-[0x800015f4]:sd t6, 1920(fp)
	-[0x800015f8]:sd a2, 1928(fp)
Current Store : [0x800015f8] : sd a2, 1928(fp) -- Store: [0x80013730]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000160c]:fmax.s t6, t5, t4
	-[0x80001610]:csrrs a2, fcsr, zero
	-[0x80001614]:sd t6, 1936(fp)
	-[0x80001618]:sd a2, 1944(fp)
Current Store : [0x80001618] : sd a2, 1944(fp) -- Store: [0x80013740]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000162c]:fmax.s t6, t5, t4
	-[0x80001630]:csrrs a2, fcsr, zero
	-[0x80001634]:sd t6, 1952(fp)
	-[0x80001638]:sd a2, 1960(fp)
Current Store : [0x80001638] : sd a2, 1960(fp) -- Store: [0x80013750]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000164c]:fmax.s t6, t5, t4
	-[0x80001650]:csrrs a2, fcsr, zero
	-[0x80001654]:sd t6, 1968(fp)
	-[0x80001658]:sd a2, 1976(fp)
Current Store : [0x80001658] : sd a2, 1976(fp) -- Store: [0x80013760]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000166c]:fmax.s t6, t5, t4
	-[0x80001670]:csrrs a2, fcsr, zero
	-[0x80001674]:sd t6, 1984(fp)
	-[0x80001678]:sd a2, 1992(fp)
Current Store : [0x80001678] : sd a2, 1992(fp) -- Store: [0x80013770]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800016ac]:fmax.s t6, t5, t4
	-[0x800016b0]:csrrs a2, fcsr, zero
	-[0x800016b4]:sd t6, 2000(fp)
	-[0x800016b8]:sd a2, 2008(fp)
Current Store : [0x800016b8] : sd a2, 2008(fp) -- Store: [0x80013780]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfa and fm2 == 0x291dc8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800016ec]:fmax.s t6, t5, t4
	-[0x800016f0]:csrrs a2, fcsr, zero
	-[0x800016f4]:sd t6, 2016(fp)
	-[0x800016f8]:sd a2, 2024(fp)
Current Store : [0x800016f8] : sd a2, 2024(fp) -- Store: [0x80013790]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000172c]:fmax.s t6, t5, t4
	-[0x80001730]:csrrs a2, fcsr, zero
	-[0x80001734]:sd t6, 2032(fp)
	-[0x80001738]:sd a2, 2040(fp)
Current Store : [0x80001738] : sd a2, 2040(fp) -- Store: [0x800137a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001774]:fmax.s t6, t5, t4
	-[0x80001778]:csrrs a2, fcsr, zero
	-[0x8000177c]:sd t6, 0(fp)
	-[0x80001780]:sd a2, 8(fp)
Current Store : [0x80001780] : sd a2, 8(fp) -- Store: [0x800137b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800017b4]:fmax.s t6, t5, t4
	-[0x800017b8]:csrrs a2, fcsr, zero
	-[0x800017bc]:sd t6, 16(fp)
	-[0x800017c0]:sd a2, 24(fp)
Current Store : [0x800017c0] : sd a2, 24(fp) -- Store: [0x800137c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfb and fm2 == 0x153eee and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800017f4]:fmax.s t6, t5, t4
	-[0x800017f8]:csrrs a2, fcsr, zero
	-[0x800017fc]:sd t6, 32(fp)
	-[0x80001800]:sd a2, 40(fp)
Current Store : [0x80001800] : sd a2, 40(fp) -- Store: [0x800137d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001834]:fmax.s t6, t5, t4
	-[0x80001838]:csrrs a2, fcsr, zero
	-[0x8000183c]:sd t6, 48(fp)
	-[0x80001840]:sd a2, 56(fp)
Current Store : [0x80001840] : sd a2, 56(fp) -- Store: [0x800137e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001874]:fmax.s t6, t5, t4
	-[0x80001878]:csrrs a2, fcsr, zero
	-[0x8000187c]:sd t6, 64(fp)
	-[0x80001880]:sd a2, 72(fp)
Current Store : [0x80001880] : sd a2, 72(fp) -- Store: [0x800137f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800018b4]:fmax.s t6, t5, t4
	-[0x800018b8]:csrrs a2, fcsr, zero
	-[0x800018bc]:sd t6, 80(fp)
	-[0x800018c0]:sd a2, 88(fp)
Current Store : [0x800018c0] : sd a2, 88(fp) -- Store: [0x80013800]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfb and fm2 == 0x1946c8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800018f4]:fmax.s t6, t5, t4
	-[0x800018f8]:csrrs a2, fcsr, zero
	-[0x800018fc]:sd t6, 96(fp)
	-[0x80001900]:sd a2, 104(fp)
Current Store : [0x80001900] : sd a2, 104(fp) -- Store: [0x80013810]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x3435dc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001934]:fmax.s t6, t5, t4
	-[0x80001938]:csrrs a2, fcsr, zero
	-[0x8000193c]:sd t6, 112(fp)
	-[0x80001940]:sd a2, 120(fp)
Current Store : [0x80001940] : sd a2, 120(fp) -- Store: [0x80013820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x3435dc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001974]:fmax.s t6, t5, t4
	-[0x80001978]:csrrs a2, fcsr, zero
	-[0x8000197c]:sd t6, 128(fp)
	-[0x80001980]:sd a2, 136(fp)
Current Store : [0x80001980] : sd a2, 136(fp) -- Store: [0x80013830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800019b4]:fmax.s t6, t5, t4
	-[0x800019b8]:csrrs a2, fcsr, zero
	-[0x800019bc]:sd t6, 144(fp)
	-[0x800019c0]:sd a2, 152(fp)
Current Store : [0x800019c0] : sd a2, 152(fp) -- Store: [0x80013840]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800019f4]:fmax.s t6, t5, t4
	-[0x800019f8]:csrrs a2, fcsr, zero
	-[0x800019fc]:sd t6, 160(fp)
	-[0x80001a00]:sd a2, 168(fp)
Current Store : [0x80001a00] : sd a2, 168(fp) -- Store: [0x80013850]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001a34]:fmax.s t6, t5, t4
	-[0x80001a38]:csrrs a2, fcsr, zero
	-[0x80001a3c]:sd t6, 176(fp)
	-[0x80001a40]:sd a2, 184(fp)
Current Store : [0x80001a40] : sd a2, 184(fp) -- Store: [0x80013860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001a74]:fmax.s t6, t5, t4
	-[0x80001a78]:csrrs a2, fcsr, zero
	-[0x80001a7c]:sd t6, 192(fp)
	-[0x80001a80]:sd a2, 200(fp)
Current Store : [0x80001a80] : sd a2, 200(fp) -- Store: [0x80013870]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ab4]:fmax.s t6, t5, t4
	-[0x80001ab8]:csrrs a2, fcsr, zero
	-[0x80001abc]:sd t6, 208(fp)
	-[0x80001ac0]:sd a2, 216(fp)
Current Store : [0x80001ac0] : sd a2, 216(fp) -- Store: [0x80013880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001af4]:fmax.s t6, t5, t4
	-[0x80001af8]:csrrs a2, fcsr, zero
	-[0x80001afc]:sd t6, 224(fp)
	-[0x80001b00]:sd a2, 232(fp)
Current Store : [0x80001b00] : sd a2, 232(fp) -- Store: [0x80013890]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001b34]:fmax.s t6, t5, t4
	-[0x80001b38]:csrrs a2, fcsr, zero
	-[0x80001b3c]:sd t6, 240(fp)
	-[0x80001b40]:sd a2, 248(fp)
Current Store : [0x80001b40] : sd a2, 248(fp) -- Store: [0x800138a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001b74]:fmax.s t6, t5, t4
	-[0x80001b78]:csrrs a2, fcsr, zero
	-[0x80001b7c]:sd t6, 256(fp)
	-[0x80001b80]:sd a2, 264(fp)
Current Store : [0x80001b80] : sd a2, 264(fp) -- Store: [0x800138b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001bb4]:fmax.s t6, t5, t4
	-[0x80001bb8]:csrrs a2, fcsr, zero
	-[0x80001bbc]:sd t6, 272(fp)
	-[0x80001bc0]:sd a2, 280(fp)
Current Store : [0x80001bc0] : sd a2, 280(fp) -- Store: [0x800138c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001bf4]:fmax.s t6, t5, t4
	-[0x80001bf8]:csrrs a2, fcsr, zero
	-[0x80001bfc]:sd t6, 288(fp)
	-[0x80001c00]:sd a2, 296(fp)
Current Store : [0x80001c00] : sd a2, 296(fp) -- Store: [0x800138d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001c34]:fmax.s t6, t5, t4
	-[0x80001c38]:csrrs a2, fcsr, zero
	-[0x80001c3c]:sd t6, 304(fp)
	-[0x80001c40]:sd a2, 312(fp)
Current Store : [0x80001c40] : sd a2, 312(fp) -- Store: [0x800138e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001c74]:fmax.s t6, t5, t4
	-[0x80001c78]:csrrs a2, fcsr, zero
	-[0x80001c7c]:sd t6, 320(fp)
	-[0x80001c80]:sd a2, 328(fp)
Current Store : [0x80001c80] : sd a2, 328(fp) -- Store: [0x800138f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001cb4]:fmax.s t6, t5, t4
	-[0x80001cb8]:csrrs a2, fcsr, zero
	-[0x80001cbc]:sd t6, 336(fp)
	-[0x80001cc0]:sd a2, 344(fp)
Current Store : [0x80001cc0] : sd a2, 344(fp) -- Store: [0x80013900]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001cf4]:fmax.s t6, t5, t4
	-[0x80001cf8]:csrrs a2, fcsr, zero
	-[0x80001cfc]:sd t6, 352(fp)
	-[0x80001d00]:sd a2, 360(fp)
Current Store : [0x80001d00] : sd a2, 360(fp) -- Store: [0x80013910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001d34]:fmax.s t6, t5, t4
	-[0x80001d38]:csrrs a2, fcsr, zero
	-[0x80001d3c]:sd t6, 368(fp)
	-[0x80001d40]:sd a2, 376(fp)
Current Store : [0x80001d40] : sd a2, 376(fp) -- Store: [0x80013920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001d74]:fmax.s t6, t5, t4
	-[0x80001d78]:csrrs a2, fcsr, zero
	-[0x80001d7c]:sd t6, 384(fp)
	-[0x80001d80]:sd a2, 392(fp)
Current Store : [0x80001d80] : sd a2, 392(fp) -- Store: [0x80013930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001db4]:fmax.s t6, t5, t4
	-[0x80001db8]:csrrs a2, fcsr, zero
	-[0x80001dbc]:sd t6, 400(fp)
	-[0x80001dc0]:sd a2, 408(fp)
Current Store : [0x80001dc0] : sd a2, 408(fp) -- Store: [0x80013940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001df4]:fmax.s t6, t5, t4
	-[0x80001df8]:csrrs a2, fcsr, zero
	-[0x80001dfc]:sd t6, 416(fp)
	-[0x80001e00]:sd a2, 424(fp)
Current Store : [0x80001e00] : sd a2, 424(fp) -- Store: [0x80013950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001e34]:fmax.s t6, t5, t4
	-[0x80001e38]:csrrs a2, fcsr, zero
	-[0x80001e3c]:sd t6, 432(fp)
	-[0x80001e40]:sd a2, 440(fp)
Current Store : [0x80001e40] : sd a2, 440(fp) -- Store: [0x80013960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001e74]:fmax.s t6, t5, t4
	-[0x80001e78]:csrrs a2, fcsr, zero
	-[0x80001e7c]:sd t6, 448(fp)
	-[0x80001e80]:sd a2, 456(fp)
Current Store : [0x80001e80] : sd a2, 456(fp) -- Store: [0x80013970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001eb4]:fmax.s t6, t5, t4
	-[0x80001eb8]:csrrs a2, fcsr, zero
	-[0x80001ebc]:sd t6, 464(fp)
	-[0x80001ec0]:sd a2, 472(fp)
Current Store : [0x80001ec0] : sd a2, 472(fp) -- Store: [0x80013980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ef4]:fmax.s t6, t5, t4
	-[0x80001ef8]:csrrs a2, fcsr, zero
	-[0x80001efc]:sd t6, 480(fp)
	-[0x80001f00]:sd a2, 488(fp)
Current Store : [0x80001f00] : sd a2, 488(fp) -- Store: [0x80013990]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001f34]:fmax.s t6, t5, t4
	-[0x80001f38]:csrrs a2, fcsr, zero
	-[0x80001f3c]:sd t6, 496(fp)
	-[0x80001f40]:sd a2, 504(fp)
Current Store : [0x80001f40] : sd a2, 504(fp) -- Store: [0x800139a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001f74]:fmax.s t6, t5, t4
	-[0x80001f78]:csrrs a2, fcsr, zero
	-[0x80001f7c]:sd t6, 512(fp)
	-[0x80001f80]:sd a2, 520(fp)
Current Store : [0x80001f80] : sd a2, 520(fp) -- Store: [0x800139b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80001fb4]:fmax.s t6, t5, t4
	-[0x80001fb8]:csrrs a2, fcsr, zero
	-[0x80001fbc]:sd t6, 528(fp)
	-[0x80001fc0]:sd a2, 536(fp)
Current Store : [0x80001fc0] : sd a2, 536(fp) -- Store: [0x800139c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80001ff4]:fmax.s t6, t5, t4
	-[0x80001ff8]:csrrs a2, fcsr, zero
	-[0x80001ffc]:sd t6, 544(fp)
	-[0x80002000]:sd a2, 552(fp)
Current Store : [0x80002000] : sd a2, 552(fp) -- Store: [0x800139d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002034]:fmax.s t6, t5, t4
	-[0x80002038]:csrrs a2, fcsr, zero
	-[0x8000203c]:sd t6, 560(fp)
	-[0x80002040]:sd a2, 568(fp)
Current Store : [0x80002040] : sd a2, 568(fp) -- Store: [0x800139e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002074]:fmax.s t6, t5, t4
	-[0x80002078]:csrrs a2, fcsr, zero
	-[0x8000207c]:sd t6, 576(fp)
	-[0x80002080]:sd a2, 584(fp)
Current Store : [0x80002080] : sd a2, 584(fp) -- Store: [0x800139f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800020b4]:fmax.s t6, t5, t4
	-[0x800020b8]:csrrs a2, fcsr, zero
	-[0x800020bc]:sd t6, 592(fp)
	-[0x800020c0]:sd a2, 600(fp)
Current Store : [0x800020c0] : sd a2, 600(fp) -- Store: [0x80013a00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800020f4]:fmax.s t6, t5, t4
	-[0x800020f8]:csrrs a2, fcsr, zero
	-[0x800020fc]:sd t6, 608(fp)
	-[0x80002100]:sd a2, 616(fp)
Current Store : [0x80002100] : sd a2, 616(fp) -- Store: [0x80013a10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002134]:fmax.s t6, t5, t4
	-[0x80002138]:csrrs a2, fcsr, zero
	-[0x8000213c]:sd t6, 624(fp)
	-[0x80002140]:sd a2, 632(fp)
Current Store : [0x80002140] : sd a2, 632(fp) -- Store: [0x80013a20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002174]:fmax.s t6, t5, t4
	-[0x80002178]:csrrs a2, fcsr, zero
	-[0x8000217c]:sd t6, 640(fp)
	-[0x80002180]:sd a2, 648(fp)
Current Store : [0x80002180] : sd a2, 648(fp) -- Store: [0x80013a30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800021b4]:fmax.s t6, t5, t4
	-[0x800021b8]:csrrs a2, fcsr, zero
	-[0x800021bc]:sd t6, 656(fp)
	-[0x800021c0]:sd a2, 664(fp)
Current Store : [0x800021c0] : sd a2, 664(fp) -- Store: [0x80013a40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800021f4]:fmax.s t6, t5, t4
	-[0x800021f8]:csrrs a2, fcsr, zero
	-[0x800021fc]:sd t6, 672(fp)
	-[0x80002200]:sd a2, 680(fp)
Current Store : [0x80002200] : sd a2, 680(fp) -- Store: [0x80013a50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002234]:fmax.s t6, t5, t4
	-[0x80002238]:csrrs a2, fcsr, zero
	-[0x8000223c]:sd t6, 688(fp)
	-[0x80002240]:sd a2, 696(fp)
Current Store : [0x80002240] : sd a2, 696(fp) -- Store: [0x80013a60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002274]:fmax.s t6, t5, t4
	-[0x80002278]:csrrs a2, fcsr, zero
	-[0x8000227c]:sd t6, 704(fp)
	-[0x80002280]:sd a2, 712(fp)
Current Store : [0x80002280] : sd a2, 712(fp) -- Store: [0x80013a70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800022b4]:fmax.s t6, t5, t4
	-[0x800022b8]:csrrs a2, fcsr, zero
	-[0x800022bc]:sd t6, 720(fp)
	-[0x800022c0]:sd a2, 728(fp)
Current Store : [0x800022c0] : sd a2, 728(fp) -- Store: [0x80013a80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800022f4]:fmax.s t6, t5, t4
	-[0x800022f8]:csrrs a2, fcsr, zero
	-[0x800022fc]:sd t6, 736(fp)
	-[0x80002300]:sd a2, 744(fp)
Current Store : [0x80002300] : sd a2, 744(fp) -- Store: [0x80013a90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002334]:fmax.s t6, t5, t4
	-[0x80002338]:csrrs a2, fcsr, zero
	-[0x8000233c]:sd t6, 752(fp)
	-[0x80002340]:sd a2, 760(fp)
Current Store : [0x80002340] : sd a2, 760(fp) -- Store: [0x80013aa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002374]:fmax.s t6, t5, t4
	-[0x80002378]:csrrs a2, fcsr, zero
	-[0x8000237c]:sd t6, 768(fp)
	-[0x80002380]:sd a2, 776(fp)
Current Store : [0x80002380] : sd a2, 776(fp) -- Store: [0x80013ab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800023b4]:fmax.s t6, t5, t4
	-[0x800023b8]:csrrs a2, fcsr, zero
	-[0x800023bc]:sd t6, 784(fp)
	-[0x800023c0]:sd a2, 792(fp)
Current Store : [0x800023c0] : sd a2, 792(fp) -- Store: [0x80013ac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1a156b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800023f4]:fmax.s t6, t5, t4
	-[0x800023f8]:csrrs a2, fcsr, zero
	-[0x800023fc]:sd t6, 800(fp)
	-[0x80002400]:sd a2, 808(fp)
Current Store : [0x80002400] : sd a2, 808(fp) -- Store: [0x80013ad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1a156b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002434]:fmax.s t6, t5, t4
	-[0x80002438]:csrrs a2, fcsr, zero
	-[0x8000243c]:sd t6, 816(fp)
	-[0x80002440]:sd a2, 824(fp)
Current Store : [0x80002440] : sd a2, 824(fp) -- Store: [0x80013ae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002474]:fmax.s t6, t5, t4
	-[0x80002478]:csrrs a2, fcsr, zero
	-[0x8000247c]:sd t6, 832(fp)
	-[0x80002480]:sd a2, 840(fp)
Current Store : [0x80002480] : sd a2, 840(fp) -- Store: [0x80013af0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x194e59 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800024b4]:fmax.s t6, t5, t4
	-[0x800024b8]:csrrs a2, fcsr, zero
	-[0x800024bc]:sd t6, 848(fp)
	-[0x800024c0]:sd a2, 856(fp)
Current Store : [0x800024c0] : sd a2, 856(fp) -- Store: [0x80013b00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x194e59 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800024f4]:fmax.s t6, t5, t4
	-[0x800024f8]:csrrs a2, fcsr, zero
	-[0x800024fc]:sd t6, 864(fp)
	-[0x80002500]:sd a2, 872(fp)
Current Store : [0x80002500] : sd a2, 872(fp) -- Store: [0x80013b10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x102b16 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x194e59 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002534]:fmax.s t6, t5, t4
	-[0x80002538]:csrrs a2, fcsr, zero
	-[0x8000253c]:sd t6, 880(fp)
	-[0x80002540]:sd a2, 888(fp)
Current Store : [0x80002540] : sd a2, 888(fp) -- Store: [0x80013b20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002574]:fmax.s t6, t5, t4
	-[0x80002578]:csrrs a2, fcsr, zero
	-[0x8000257c]:sd t6, 896(fp)
	-[0x80002580]:sd a2, 904(fp)
Current Store : [0x80002580] : sd a2, 904(fp) -- Store: [0x80013b30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800025b4]:fmax.s t6, t5, t4
	-[0x800025b8]:csrrs a2, fcsr, zero
	-[0x800025bc]:sd t6, 912(fp)
	-[0x800025c0]:sd a2, 920(fp)
Current Store : [0x800025c0] : sd a2, 920(fp) -- Store: [0x80013b40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfa and fm2 == 0x6a2c24 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800025f4]:fmax.s t6, t5, t4
	-[0x800025f8]:csrrs a2, fcsr, zero
	-[0x800025fc]:sd t6, 928(fp)
	-[0x80002600]:sd a2, 936(fp)
Current Store : [0x80002600] : sd a2, 936(fp) -- Store: [0x80013b50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002634]:fmax.s t6, t5, t4
	-[0x80002638]:csrrs a2, fcsr, zero
	-[0x8000263c]:sd t6, 944(fp)
	-[0x80002640]:sd a2, 952(fp)
Current Store : [0x80002640] : sd a2, 952(fp) -- Store: [0x80013b60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002674]:fmax.s t6, t5, t4
	-[0x80002678]:csrrs a2, fcsr, zero
	-[0x8000267c]:sd t6, 960(fp)
	-[0x80002680]:sd a2, 968(fp)
Current Store : [0x80002680] : sd a2, 968(fp) -- Store: [0x80013b70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800026b4]:fmax.s t6, t5, t4
	-[0x800026b8]:csrrs a2, fcsr, zero
	-[0x800026bc]:sd t6, 976(fp)
	-[0x800026c0]:sd a2, 984(fp)
Current Store : [0x800026c0] : sd a2, 984(fp) -- Store: [0x80013b80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800026f4]:fmax.s t6, t5, t4
	-[0x800026f8]:csrrs a2, fcsr, zero
	-[0x800026fc]:sd t6, 992(fp)
	-[0x80002700]:sd a2, 1000(fp)
Current Store : [0x80002700] : sd a2, 1000(fp) -- Store: [0x80013b90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002734]:fmax.s t6, t5, t4
	-[0x80002738]:csrrs a2, fcsr, zero
	-[0x8000273c]:sd t6, 1008(fp)
	-[0x80002740]:sd a2, 1016(fp)
Current Store : [0x80002740] : sd a2, 1016(fp) -- Store: [0x80013ba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002774]:fmax.s t6, t5, t4
	-[0x80002778]:csrrs a2, fcsr, zero
	-[0x8000277c]:sd t6, 1024(fp)
	-[0x80002780]:sd a2, 1032(fp)
Current Store : [0x80002780] : sd a2, 1032(fp) -- Store: [0x80013bb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800027b4]:fmax.s t6, t5, t4
	-[0x800027b8]:csrrs a2, fcsr, zero
	-[0x800027bc]:sd t6, 1040(fp)
	-[0x800027c0]:sd a2, 1048(fp)
Current Store : [0x800027c0] : sd a2, 1048(fp) -- Store: [0x80013bc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfa and fm2 == 0x6a2c24 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800027f4]:fmax.s t6, t5, t4
	-[0x800027f8]:csrrs a2, fcsr, zero
	-[0x800027fc]:sd t6, 1056(fp)
	-[0x80002800]:sd a2, 1064(fp)
Current Store : [0x80002800] : sd a2, 1064(fp) -- Store: [0x80013bd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002834]:fmax.s t6, t5, t4
	-[0x80002838]:csrrs a2, fcsr, zero
	-[0x8000283c]:sd t6, 1072(fp)
	-[0x80002840]:sd a2, 1080(fp)
Current Store : [0x80002840] : sd a2, 1080(fp) -- Store: [0x80013be0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002874]:fmax.s t6, t5, t4
	-[0x80002878]:csrrs a2, fcsr, zero
	-[0x8000287c]:sd t6, 1088(fp)
	-[0x80002880]:sd a2, 1096(fp)
Current Store : [0x80002880] : sd a2, 1096(fp) -- Store: [0x80013bf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800028b4]:fmax.s t6, t5, t4
	-[0x800028b8]:csrrs a2, fcsr, zero
	-[0x800028bc]:sd t6, 1104(fp)
	-[0x800028c0]:sd a2, 1112(fp)
Current Store : [0x800028c0] : sd a2, 1112(fp) -- Store: [0x80013c00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800028f4]:fmax.s t6, t5, t4
	-[0x800028f8]:csrrs a2, fcsr, zero
	-[0x800028fc]:sd t6, 1120(fp)
	-[0x80002900]:sd a2, 1128(fp)
Current Store : [0x80002900] : sd a2, 1128(fp) -- Store: [0x80013c10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002934]:fmax.s t6, t5, t4
	-[0x80002938]:csrrs a2, fcsr, zero
	-[0x8000293c]:sd t6, 1136(fp)
	-[0x80002940]:sd a2, 1144(fp)
Current Store : [0x80002940] : sd a2, 1144(fp) -- Store: [0x80013c20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002974]:fmax.s t6, t5, t4
	-[0x80002978]:csrrs a2, fcsr, zero
	-[0x8000297c]:sd t6, 1152(fp)
	-[0x80002980]:sd a2, 1160(fp)
Current Store : [0x80002980] : sd a2, 1160(fp) -- Store: [0x80013c30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800029b4]:fmax.s t6, t5, t4
	-[0x800029b8]:csrrs a2, fcsr, zero
	-[0x800029bc]:sd t6, 1168(fp)
	-[0x800029c0]:sd a2, 1176(fp)
Current Store : [0x800029c0] : sd a2, 1176(fp) -- Store: [0x80013c40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800029f4]:fmax.s t6, t5, t4
	-[0x800029f8]:csrrs a2, fcsr, zero
	-[0x800029fc]:sd t6, 1184(fp)
	-[0x80002a00]:sd a2, 1192(fp)
Current Store : [0x80002a00] : sd a2, 1192(fp) -- Store: [0x80013c50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002a34]:fmax.s t6, t5, t4
	-[0x80002a38]:csrrs a2, fcsr, zero
	-[0x80002a3c]:sd t6, 1200(fp)
	-[0x80002a40]:sd a2, 1208(fp)
Current Store : [0x80002a40] : sd a2, 1208(fp) -- Store: [0x80013c60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 0 and fe2 == 0xfa and fm2 == 0x6a2c24 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002a74]:fmax.s t6, t5, t4
	-[0x80002a78]:csrrs a2, fcsr, zero
	-[0x80002a7c]:sd t6, 1216(fp)
	-[0x80002a80]:sd a2, 1224(fp)
Current Store : [0x80002a80] : sd a2, 1224(fp) -- Store: [0x80013c70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfa and fm1 == 0x6a2c24 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002ab4]:fmax.s t6, t5, t4
	-[0x80002ab8]:csrrs a2, fcsr, zero
	-[0x80002abc]:sd t6, 1232(fp)
	-[0x80002ac0]:sd a2, 1240(fp)
Current Store : [0x80002ac0] : sd a2, 1240(fp) -- Store: [0x80013c80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002af4]:fmax.s t6, t5, t4
	-[0x80002af8]:csrrs a2, fcsr, zero
	-[0x80002afc]:sd t6, 1248(fp)
	-[0x80002b00]:sd a2, 1256(fp)
Current Store : [0x80002b00] : sd a2, 1256(fp) -- Store: [0x80013c90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002b34]:fmax.s t6, t5, t4
	-[0x80002b38]:csrrs a2, fcsr, zero
	-[0x80002b3c]:sd t6, 1264(fp)
	-[0x80002b40]:sd a2, 1272(fp)
Current Store : [0x80002b40] : sd a2, 1272(fp) -- Store: [0x80013ca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002b74]:fmax.s t6, t5, t4
	-[0x80002b78]:csrrs a2, fcsr, zero
	-[0x80002b7c]:sd t6, 1280(fp)
	-[0x80002b80]:sd a2, 1288(fp)
Current Store : [0x80002b80] : sd a2, 1288(fp) -- Store: [0x80013cb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002bb4]:fmax.s t6, t5, t4
	-[0x80002bb8]:csrrs a2, fcsr, zero
	-[0x80002bbc]:sd t6, 1296(fp)
	-[0x80002bc0]:sd a2, 1304(fp)
Current Store : [0x80002bc0] : sd a2, 1304(fp) -- Store: [0x80013cc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002bf4]:fmax.s t6, t5, t4
	-[0x80002bf8]:csrrs a2, fcsr, zero
	-[0x80002bfc]:sd t6, 1312(fp)
	-[0x80002c00]:sd a2, 1320(fp)
Current Store : [0x80002c00] : sd a2, 1320(fp) -- Store: [0x80013cd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002c34]:fmax.s t6, t5, t4
	-[0x80002c38]:csrrs a2, fcsr, zero
	-[0x80002c3c]:sd t6, 1328(fp)
	-[0x80002c40]:sd a2, 1336(fp)
Current Store : [0x80002c40] : sd a2, 1336(fp) -- Store: [0x80013ce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002c74]:fmax.s t6, t5, t4
	-[0x80002c78]:csrrs a2, fcsr, zero
	-[0x80002c7c]:sd t6, 1344(fp)
	-[0x80002c80]:sd a2, 1352(fp)
Current Store : [0x80002c80] : sd a2, 1352(fp) -- Store: [0x80013cf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002cb4]:fmax.s t6, t5, t4
	-[0x80002cb8]:csrrs a2, fcsr, zero
	-[0x80002cbc]:sd t6, 1360(fp)
	-[0x80002cc0]:sd a2, 1368(fp)
Current Store : [0x80002cc0] : sd a2, 1368(fp) -- Store: [0x80013d00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002cf4]:fmax.s t6, t5, t4
	-[0x80002cf8]:csrrs a2, fcsr, zero
	-[0x80002cfc]:sd t6, 1376(fp)
	-[0x80002d00]:sd a2, 1384(fp)
Current Store : [0x80002d00] : sd a2, 1384(fp) -- Store: [0x80013d10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002d34]:fmax.s t6, t5, t4
	-[0x80002d38]:csrrs a2, fcsr, zero
	-[0x80002d3c]:sd t6, 1392(fp)
	-[0x80002d40]:sd a2, 1400(fp)
Current Store : [0x80002d40] : sd a2, 1400(fp) -- Store: [0x80013d20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002d74]:fmax.s t6, t5, t4
	-[0x80002d78]:csrrs a2, fcsr, zero
	-[0x80002d7c]:sd t6, 1408(fp)
	-[0x80002d80]:sd a2, 1416(fp)
Current Store : [0x80002d80] : sd a2, 1416(fp) -- Store: [0x80013d30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002db4]:fmax.s t6, t5, t4
	-[0x80002db8]:csrrs a2, fcsr, zero
	-[0x80002dbc]:sd t6, 1424(fp)
	-[0x80002dc0]:sd a2, 1432(fp)
Current Store : [0x80002dc0] : sd a2, 1432(fp) -- Store: [0x80013d40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002df4]:fmax.s t6, t5, t4
	-[0x80002df8]:csrrs a2, fcsr, zero
	-[0x80002dfc]:sd t6, 1440(fp)
	-[0x80002e00]:sd a2, 1448(fp)
Current Store : [0x80002e00] : sd a2, 1448(fp) -- Store: [0x80013d50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002e34]:fmax.s t6, t5, t4
	-[0x80002e38]:csrrs a2, fcsr, zero
	-[0x80002e3c]:sd t6, 1456(fp)
	-[0x80002e40]:sd a2, 1464(fp)
Current Store : [0x80002e40] : sd a2, 1464(fp) -- Store: [0x80013d60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002e74]:fmax.s t6, t5, t4
	-[0x80002e78]:csrrs a2, fcsr, zero
	-[0x80002e7c]:sd t6, 1472(fp)
	-[0x80002e80]:sd a2, 1480(fp)
Current Store : [0x80002e80] : sd a2, 1480(fp) -- Store: [0x80013d70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002eb4]:fmax.s t6, t5, t4
	-[0x80002eb8]:csrrs a2, fcsr, zero
	-[0x80002ebc]:sd t6, 1488(fp)
	-[0x80002ec0]:sd a2, 1496(fp)
Current Store : [0x80002ec0] : sd a2, 1496(fp) -- Store: [0x80013d80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002ef4]:fmax.s t6, t5, t4
	-[0x80002ef8]:csrrs a2, fcsr, zero
	-[0x80002efc]:sd t6, 1504(fp)
	-[0x80002f00]:sd a2, 1512(fp)
Current Store : [0x80002f00] : sd a2, 1512(fp) -- Store: [0x80013d90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80002f34]:fmax.s t6, t5, t4
	-[0x80002f38]:csrrs a2, fcsr, zero
	-[0x80002f3c]:sd t6, 1520(fp)
	-[0x80002f40]:sd a2, 1528(fp)
Current Store : [0x80002f40] : sd a2, 1528(fp) -- Store: [0x80013da0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002f74]:fmax.s t6, t5, t4
	-[0x80002f78]:csrrs a2, fcsr, zero
	-[0x80002f7c]:sd t6, 1536(fp)
	-[0x80002f80]:sd a2, 1544(fp)
Current Store : [0x80002f80] : sd a2, 1544(fp) -- Store: [0x80013db0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002fb4]:fmax.s t6, t5, t4
	-[0x80002fb8]:csrrs a2, fcsr, zero
	-[0x80002fbc]:sd t6, 1552(fp)
	-[0x80002fc0]:sd a2, 1560(fp)
Current Store : [0x80002fc0] : sd a2, 1560(fp) -- Store: [0x80013dc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80002ff4]:fmax.s t6, t5, t4
	-[0x80002ff8]:csrrs a2, fcsr, zero
	-[0x80002ffc]:sd t6, 1568(fp)
	-[0x80003000]:sd a2, 1576(fp)
Current Store : [0x80003000] : sd a2, 1576(fp) -- Store: [0x80013dd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003034]:fmax.s t6, t5, t4
	-[0x80003038]:csrrs a2, fcsr, zero
	-[0x8000303c]:sd t6, 1584(fp)
	-[0x80003040]:sd a2, 1592(fp)
Current Store : [0x80003040] : sd a2, 1592(fp) -- Store: [0x80013de0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003074]:fmax.s t6, t5, t4
	-[0x80003078]:csrrs a2, fcsr, zero
	-[0x8000307c]:sd t6, 1600(fp)
	-[0x80003080]:sd a2, 1608(fp)
Current Store : [0x80003080] : sd a2, 1608(fp) -- Store: [0x80013df0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800030b4]:fmax.s t6, t5, t4
	-[0x800030b8]:csrrs a2, fcsr, zero
	-[0x800030bc]:sd t6, 1616(fp)
	-[0x800030c0]:sd a2, 1624(fp)
Current Store : [0x800030c0] : sd a2, 1624(fp) -- Store: [0x80013e00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800030f4]:fmax.s t6, t5, t4
	-[0x800030f8]:csrrs a2, fcsr, zero
	-[0x800030fc]:sd t6, 1632(fp)
	-[0x80003100]:sd a2, 1640(fp)
Current Store : [0x80003100] : sd a2, 1640(fp) -- Store: [0x80013e10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003134]:fmax.s t6, t5, t4
	-[0x80003138]:csrrs a2, fcsr, zero
	-[0x8000313c]:sd t6, 1648(fp)
	-[0x80003140]:sd a2, 1656(fp)
Current Store : [0x80003140] : sd a2, 1656(fp) -- Store: [0x80013e20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003174]:fmax.s t6, t5, t4
	-[0x80003178]:csrrs a2, fcsr, zero
	-[0x8000317c]:sd t6, 1664(fp)
	-[0x80003180]:sd a2, 1672(fp)
Current Store : [0x80003180] : sd a2, 1672(fp) -- Store: [0x80013e30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800031b4]:fmax.s t6, t5, t4
	-[0x800031b8]:csrrs a2, fcsr, zero
	-[0x800031bc]:sd t6, 1680(fp)
	-[0x800031c0]:sd a2, 1688(fp)
Current Store : [0x800031c0] : sd a2, 1688(fp) -- Store: [0x80013e40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800031f4]:fmax.s t6, t5, t4
	-[0x800031f8]:csrrs a2, fcsr, zero
	-[0x800031fc]:sd t6, 1696(fp)
	-[0x80003200]:sd a2, 1704(fp)
Current Store : [0x80003200] : sd a2, 1704(fp) -- Store: [0x80013e50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 0 and fe2 == 0x00 and fm2 == 0x152f10 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003234]:fmax.s t6, t5, t4
	-[0x80003238]:csrrs a2, fcsr, zero
	-[0x8000323c]:sd t6, 1712(fp)
	-[0x80003240]:sd a2, 1720(fp)
Current Store : [0x80003240] : sd a2, 1720(fp) -- Store: [0x80013e60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003274]:fmax.s t6, t5, t4
	-[0x80003278]:csrrs a2, fcsr, zero
	-[0x8000327c]:sd t6, 1728(fp)
	-[0x80003280]:sd a2, 1736(fp)
Current Store : [0x80003280] : sd a2, 1736(fp) -- Store: [0x80013e70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800032b4]:fmax.s t6, t5, t4
	-[0x800032b8]:csrrs a2, fcsr, zero
	-[0x800032bc]:sd t6, 1744(fp)
	-[0x800032c0]:sd a2, 1752(fp)
Current Store : [0x800032c0] : sd a2, 1752(fp) -- Store: [0x80013e80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x152f10 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800032f4]:fmax.s t6, t5, t4
	-[0x800032f8]:csrrs a2, fcsr, zero
	-[0x800032fc]:sd t6, 1760(fp)
	-[0x80003300]:sd a2, 1768(fp)
Current Store : [0x80003300] : sd a2, 1768(fp) -- Store: [0x80013e90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003334]:fmax.s t6, t5, t4
	-[0x80003338]:csrrs a2, fcsr, zero
	-[0x8000333c]:sd t6, 1776(fp)
	-[0x80003340]:sd a2, 1784(fp)
Current Store : [0x80003340] : sd a2, 1784(fp) -- Store: [0x80013ea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x7f and fm1 == 0x7903cc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003374]:fmax.s t6, t5, t4
	-[0x80003378]:csrrs a2, fcsr, zero
	-[0x8000337c]:sd t6, 1792(fp)
	-[0x80003380]:sd a2, 1800(fp)
Current Store : [0x80003380] : sd a2, 1800(fp) -- Store: [0x80013eb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x7903cc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800033b4]:fmax.s t6, t5, t4
	-[0x800033b8]:csrrs a2, fcsr, zero
	-[0x800033bc]:sd t6, 1808(fp)
	-[0x800033c0]:sd a2, 1816(fp)
Current Store : [0x800033c0] : sd a2, 1816(fp) -- Store: [0x80013ec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x125b96 and fs2 == 0 and fe2 == 0x7f and fm2 == 0x7903cc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800033f4]:fmax.s t6, t5, t4
	-[0x800033f8]:csrrs a2, fcsr, zero
	-[0x800033fc]:sd t6, 1824(fp)
	-[0x80003400]:sd a2, 1832(fp)
Current Store : [0x80003400] : sd a2, 1832(fp) -- Store: [0x80013ed0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003434]:fmax.s t6, t5, t4
	-[0x80003438]:csrrs a2, fcsr, zero
	-[0x8000343c]:sd t6, 1840(fp)
	-[0x80003440]:sd a2, 1848(fp)
Current Store : [0x80003440] : sd a2, 1848(fp) -- Store: [0x80013ee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003474]:fmax.s t6, t5, t4
	-[0x80003478]:csrrs a2, fcsr, zero
	-[0x8000347c]:sd t6, 1856(fp)
	-[0x80003480]:sd a2, 1864(fp)
Current Store : [0x80003480] : sd a2, 1864(fp) -- Store: [0x80013ef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800034b4]:fmax.s t6, t5, t4
	-[0x800034b8]:csrrs a2, fcsr, zero
	-[0x800034bc]:sd t6, 1872(fp)
	-[0x800034c0]:sd a2, 1880(fp)
Current Store : [0x800034c0] : sd a2, 1880(fp) -- Store: [0x80013f00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800034f4]:fmax.s t6, t5, t4
	-[0x800034f8]:csrrs a2, fcsr, zero
	-[0x800034fc]:sd t6, 1888(fp)
	-[0x80003500]:sd a2, 1896(fp)
Current Store : [0x80003500] : sd a2, 1896(fp) -- Store: [0x80013f10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003534]:fmax.s t6, t5, t4
	-[0x80003538]:csrrs a2, fcsr, zero
	-[0x8000353c]:sd t6, 1904(fp)
	-[0x80003540]:sd a2, 1912(fp)
Current Store : [0x80003540] : sd a2, 1912(fp) -- Store: [0x80013f20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003574]:fmax.s t6, t5, t4
	-[0x80003578]:csrrs a2, fcsr, zero
	-[0x8000357c]:sd t6, 1920(fp)
	-[0x80003580]:sd a2, 1928(fp)
Current Store : [0x80003580] : sd a2, 1928(fp) -- Store: [0x80013f30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800035b4]:fmax.s t6, t5, t4
	-[0x800035b8]:csrrs a2, fcsr, zero
	-[0x800035bc]:sd t6, 1936(fp)
	-[0x800035c0]:sd a2, 1944(fp)
Current Store : [0x800035c0] : sd a2, 1944(fp) -- Store: [0x80013f40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800035f4]:fmax.s t6, t5, t4
	-[0x800035f8]:csrrs a2, fcsr, zero
	-[0x800035fc]:sd t6, 1952(fp)
	-[0x80003600]:sd a2, 1960(fp)
Current Store : [0x80003600] : sd a2, 1960(fp) -- Store: [0x80013f50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003634]:fmax.s t6, t5, t4
	-[0x80003638]:csrrs a2, fcsr, zero
	-[0x8000363c]:sd t6, 1968(fp)
	-[0x80003640]:sd a2, 1976(fp)
Current Store : [0x80003640] : sd a2, 1976(fp) -- Store: [0x80013f60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003674]:fmax.s t6, t5, t4
	-[0x80003678]:csrrs a2, fcsr, zero
	-[0x8000367c]:sd t6, 1984(fp)
	-[0x80003680]:sd a2, 1992(fp)
Current Store : [0x80003680] : sd a2, 1992(fp) -- Store: [0x80013f70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800036ac]:fmax.s t6, t5, t4
	-[0x800036b0]:csrrs a2, fcsr, zero
	-[0x800036b4]:sd t6, 2000(fp)
	-[0x800036b8]:sd a2, 2008(fp)
Current Store : [0x800036b8] : sd a2, 2008(fp) -- Store: [0x80013f80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800036e4]:fmax.s t6, t5, t4
	-[0x800036e8]:csrrs a2, fcsr, zero
	-[0x800036ec]:sd t6, 2016(fp)
	-[0x800036f0]:sd a2, 2024(fp)
Current Store : [0x800036f0] : sd a2, 2024(fp) -- Store: [0x80013f90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000371c]:fmax.s t6, t5, t4
	-[0x80003720]:csrrs a2, fcsr, zero
	-[0x80003724]:sd t6, 2032(fp)
	-[0x80003728]:sd a2, 2040(fp)
Current Store : [0x80003728] : sd a2, 2040(fp) -- Store: [0x80013fa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000375c]:fmax.s t6, t5, t4
	-[0x80003760]:csrrs a2, fcsr, zero
	-[0x80003764]:sd t6, 0(fp)
	-[0x80003768]:sd a2, 8(fp)
Current Store : [0x80003768] : sd a2, 8(fp) -- Store: [0x80013fb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003794]:fmax.s t6, t5, t4
	-[0x80003798]:csrrs a2, fcsr, zero
	-[0x8000379c]:sd t6, 16(fp)
	-[0x800037a0]:sd a2, 24(fp)
Current Store : [0x800037a0] : sd a2, 24(fp) -- Store: [0x80013fc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800037cc]:fmax.s t6, t5, t4
	-[0x800037d0]:csrrs a2, fcsr, zero
	-[0x800037d4]:sd t6, 32(fp)
	-[0x800037d8]:sd a2, 40(fp)
Current Store : [0x800037d8] : sd a2, 40(fp) -- Store: [0x80013fd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003804]:fmax.s t6, t5, t4
	-[0x80003808]:csrrs a2, fcsr, zero
	-[0x8000380c]:sd t6, 48(fp)
	-[0x80003810]:sd a2, 56(fp)
Current Store : [0x80003810] : sd a2, 56(fp) -- Store: [0x80013fe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000383c]:fmax.s t6, t5, t4
	-[0x80003840]:csrrs a2, fcsr, zero
	-[0x80003844]:sd t6, 64(fp)
	-[0x80003848]:sd a2, 72(fp)
Current Store : [0x80003848] : sd a2, 72(fp) -- Store: [0x80013ff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003874]:fmax.s t6, t5, t4
	-[0x80003878]:csrrs a2, fcsr, zero
	-[0x8000387c]:sd t6, 80(fp)
	-[0x80003880]:sd a2, 88(fp)
Current Store : [0x80003880] : sd a2, 88(fp) -- Store: [0x80014000]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800038ac]:fmax.s t6, t5, t4
	-[0x800038b0]:csrrs a2, fcsr, zero
	-[0x800038b4]:sd t6, 96(fp)
	-[0x800038b8]:sd a2, 104(fp)
Current Store : [0x800038b8] : sd a2, 104(fp) -- Store: [0x80014010]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800038e4]:fmax.s t6, t5, t4
	-[0x800038e8]:csrrs a2, fcsr, zero
	-[0x800038ec]:sd t6, 112(fp)
	-[0x800038f0]:sd a2, 120(fp)
Current Store : [0x800038f0] : sd a2, 120(fp) -- Store: [0x80014020]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000391c]:fmax.s t6, t5, t4
	-[0x80003920]:csrrs a2, fcsr, zero
	-[0x80003924]:sd t6, 128(fp)
	-[0x80003928]:sd a2, 136(fp)
Current Store : [0x80003928] : sd a2, 136(fp) -- Store: [0x80014030]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003954]:fmax.s t6, t5, t4
	-[0x80003958]:csrrs a2, fcsr, zero
	-[0x8000395c]:sd t6, 144(fp)
	-[0x80003960]:sd a2, 152(fp)
Current Store : [0x80003960] : sd a2, 152(fp) -- Store: [0x80014040]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000398c]:fmax.s t6, t5, t4
	-[0x80003990]:csrrs a2, fcsr, zero
	-[0x80003994]:sd t6, 160(fp)
	-[0x80003998]:sd a2, 168(fp)
Current Store : [0x80003998] : sd a2, 168(fp) -- Store: [0x80014050]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800039c4]:fmax.s t6, t5, t4
	-[0x800039c8]:csrrs a2, fcsr, zero
	-[0x800039cc]:sd t6, 176(fp)
	-[0x800039d0]:sd a2, 184(fp)
Current Store : [0x800039d0] : sd a2, 184(fp) -- Store: [0x80014060]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800039fc]:fmax.s t6, t5, t4
	-[0x80003a00]:csrrs a2, fcsr, zero
	-[0x80003a04]:sd t6, 192(fp)
	-[0x80003a08]:sd a2, 200(fp)
Current Store : [0x80003a08] : sd a2, 200(fp) -- Store: [0x80014070]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003a34]:fmax.s t6, t5, t4
	-[0x80003a38]:csrrs a2, fcsr, zero
	-[0x80003a3c]:sd t6, 208(fp)
	-[0x80003a40]:sd a2, 216(fp)
Current Store : [0x80003a40] : sd a2, 216(fp) -- Store: [0x80014080]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003a6c]:fmax.s t6, t5, t4
	-[0x80003a70]:csrrs a2, fcsr, zero
	-[0x80003a74]:sd t6, 224(fp)
	-[0x80003a78]:sd a2, 232(fp)
Current Store : [0x80003a78] : sd a2, 232(fp) -- Store: [0x80014090]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003aa4]:fmax.s t6, t5, t4
	-[0x80003aa8]:csrrs a2, fcsr, zero
	-[0x80003aac]:sd t6, 240(fp)
	-[0x80003ab0]:sd a2, 248(fp)
Current Store : [0x80003ab0] : sd a2, 248(fp) -- Store: [0x800140a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003adc]:fmax.s t6, t5, t4
	-[0x80003ae0]:csrrs a2, fcsr, zero
	-[0x80003ae4]:sd t6, 256(fp)
	-[0x80003ae8]:sd a2, 264(fp)
Current Store : [0x80003ae8] : sd a2, 264(fp) -- Store: [0x800140b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b14]:fmax.s t6, t5, t4
	-[0x80003b18]:csrrs a2, fcsr, zero
	-[0x80003b1c]:sd t6, 272(fp)
	-[0x80003b20]:sd a2, 280(fp)
Current Store : [0x80003b20] : sd a2, 280(fp) -- Store: [0x800140c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003b4c]:fmax.s t6, t5, t4
	-[0x80003b50]:csrrs a2, fcsr, zero
	-[0x80003b54]:sd t6, 288(fp)
	-[0x80003b58]:sd a2, 296(fp)
Current Store : [0x80003b58] : sd a2, 296(fp) -- Store: [0x800140d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003b84]:fmax.s t6, t5, t4
	-[0x80003b88]:csrrs a2, fcsr, zero
	-[0x80003b8c]:sd t6, 304(fp)
	-[0x80003b90]:sd a2, 312(fp)
Current Store : [0x80003b90] : sd a2, 312(fp) -- Store: [0x800140e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003bbc]:fmax.s t6, t5, t4
	-[0x80003bc0]:csrrs a2, fcsr, zero
	-[0x80003bc4]:sd t6, 320(fp)
	-[0x80003bc8]:sd a2, 328(fp)
Current Store : [0x80003bc8] : sd a2, 328(fp) -- Store: [0x800140f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003bf4]:fmax.s t6, t5, t4
	-[0x80003bf8]:csrrs a2, fcsr, zero
	-[0x80003bfc]:sd t6, 336(fp)
	-[0x80003c00]:sd a2, 344(fp)
Current Store : [0x80003c00] : sd a2, 344(fp) -- Store: [0x80014100]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c2c]:fmax.s t6, t5, t4
	-[0x80003c30]:csrrs a2, fcsr, zero
	-[0x80003c34]:sd t6, 352(fp)
	-[0x80003c38]:sd a2, 360(fp)
Current Store : [0x80003c38] : sd a2, 360(fp) -- Store: [0x80014110]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c64]:fmax.s t6, t5, t4
	-[0x80003c68]:csrrs a2, fcsr, zero
	-[0x80003c6c]:sd t6, 368(fp)
	-[0x80003c70]:sd a2, 376(fp)
Current Store : [0x80003c70] : sd a2, 376(fp) -- Store: [0x80014120]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003c9c]:fmax.s t6, t5, t4
	-[0x80003ca0]:csrrs a2, fcsr, zero
	-[0x80003ca4]:sd t6, 384(fp)
	-[0x80003ca8]:sd a2, 392(fp)
Current Store : [0x80003ca8] : sd a2, 392(fp) -- Store: [0x80014130]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003cd4]:fmax.s t6, t5, t4
	-[0x80003cd8]:csrrs a2, fcsr, zero
	-[0x80003cdc]:sd t6, 400(fp)
	-[0x80003ce0]:sd a2, 408(fp)
Current Store : [0x80003ce0] : sd a2, 408(fp) -- Store: [0x80014140]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d0c]:fmax.s t6, t5, t4
	-[0x80003d10]:csrrs a2, fcsr, zero
	-[0x80003d14]:sd t6, 416(fp)
	-[0x80003d18]:sd a2, 424(fp)
Current Store : [0x80003d18] : sd a2, 424(fp) -- Store: [0x80014150]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d44]:fmax.s t6, t5, t4
	-[0x80003d48]:csrrs a2, fcsr, zero
	-[0x80003d4c]:sd t6, 432(fp)
	-[0x80003d50]:sd a2, 440(fp)
Current Store : [0x80003d50] : sd a2, 440(fp) -- Store: [0x80014160]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003d7c]:fmax.s t6, t5, t4
	-[0x80003d80]:csrrs a2, fcsr, zero
	-[0x80003d84]:sd t6, 448(fp)
	-[0x80003d88]:sd a2, 456(fp)
Current Store : [0x80003d88] : sd a2, 456(fp) -- Store: [0x80014170]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003db4]:fmax.s t6, t5, t4
	-[0x80003db8]:csrrs a2, fcsr, zero
	-[0x80003dbc]:sd t6, 464(fp)
	-[0x80003dc0]:sd a2, 472(fp)
Current Store : [0x80003dc0] : sd a2, 472(fp) -- Store: [0x80014180]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003dec]:fmax.s t6, t5, t4
	-[0x80003df0]:csrrs a2, fcsr, zero
	-[0x80003df4]:sd t6, 480(fp)
	-[0x80003df8]:sd a2, 488(fp)
Current Store : [0x80003df8] : sd a2, 488(fp) -- Store: [0x80014190]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003e24]:fmax.s t6, t5, t4
	-[0x80003e28]:csrrs a2, fcsr, zero
	-[0x80003e2c]:sd t6, 496(fp)
	-[0x80003e30]:sd a2, 504(fp)
Current Store : [0x80003e30] : sd a2, 504(fp) -- Store: [0x800141a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003e5c]:fmax.s t6, t5, t4
	-[0x80003e60]:csrrs a2, fcsr, zero
	-[0x80003e64]:sd t6, 512(fp)
	-[0x80003e68]:sd a2, 520(fp)
Current Store : [0x80003e68] : sd a2, 520(fp) -- Store: [0x800141b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003e94]:fmax.s t6, t5, t4
	-[0x80003e98]:csrrs a2, fcsr, zero
	-[0x80003e9c]:sd t6, 528(fp)
	-[0x80003ea0]:sd a2, 536(fp)
Current Store : [0x80003ea0] : sd a2, 536(fp) -- Store: [0x800141c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003ecc]:fmax.s t6, t5, t4
	-[0x80003ed0]:csrrs a2, fcsr, zero
	-[0x80003ed4]:sd t6, 544(fp)
	-[0x80003ed8]:sd a2, 552(fp)
Current Store : [0x80003ed8] : sd a2, 552(fp) -- Store: [0x800141d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003f04]:fmax.s t6, t5, t4
	-[0x80003f08]:csrrs a2, fcsr, zero
	-[0x80003f0c]:sd t6, 560(fp)
	-[0x80003f10]:sd a2, 568(fp)
Current Store : [0x80003f10] : sd a2, 568(fp) -- Store: [0x800141e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003f3c]:fmax.s t6, t5, t4
	-[0x80003f40]:csrrs a2, fcsr, zero
	-[0x80003f44]:sd t6, 576(fp)
	-[0x80003f48]:sd a2, 584(fp)
Current Store : [0x80003f48] : sd a2, 584(fp) -- Store: [0x800141f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x3f92c0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003f74]:fmax.s t6, t5, t4
	-[0x80003f78]:csrrs a2, fcsr, zero
	-[0x80003f7c]:sd t6, 592(fp)
	-[0x80003f80]:sd a2, 600(fp)
Current Store : [0x80003f80] : sd a2, 600(fp) -- Store: [0x80014200]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x3f92c0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80003fac]:fmax.s t6, t5, t4
	-[0x80003fb0]:csrrs a2, fcsr, zero
	-[0x80003fb4]:sd t6, 608(fp)
	-[0x80003fb8]:sd a2, 616(fp)
Current Store : [0x80003fb8] : sd a2, 616(fp) -- Store: [0x80014210]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80003fe4]:fmax.s t6, t5, t4
	-[0x80003fe8]:csrrs a2, fcsr, zero
	-[0x80003fec]:sd t6, 624(fp)
	-[0x80003ff0]:sd a2, 632(fp)
Current Store : [0x80003ff0] : sd a2, 632(fp) -- Store: [0x80014220]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x3ad332 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000401c]:fmax.s t6, t5, t4
	-[0x80004020]:csrrs a2, fcsr, zero
	-[0x80004024]:sd t6, 640(fp)
	-[0x80004028]:sd a2, 648(fp)
Current Store : [0x80004028] : sd a2, 648(fp) -- Store: [0x80014230]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3ad332 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004054]:fmax.s t6, t5, t4
	-[0x80004058]:csrrs a2, fcsr, zero
	-[0x8000405c]:sd t6, 656(fp)
	-[0x80004060]:sd a2, 664(fp)
Current Store : [0x80004060] : sd a2, 664(fp) -- Store: [0x80014240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x2fb07b and fs2 == 0 and fe2 == 0x81 and fm2 == 0x3ad332 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000408c]:fmax.s t6, t5, t4
	-[0x80004090]:csrrs a2, fcsr, zero
	-[0x80004094]:sd t6, 672(fp)
	-[0x80004098]:sd a2, 680(fp)
Current Store : [0x80004098] : sd a2, 680(fp) -- Store: [0x80014250]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800040c4]:fmax.s t6, t5, t4
	-[0x800040c8]:csrrs a2, fcsr, zero
	-[0x800040cc]:sd t6, 688(fp)
	-[0x800040d0]:sd a2, 696(fp)
Current Store : [0x800040d0] : sd a2, 696(fp) -- Store: [0x80014260]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800040fc]:fmax.s t6, t5, t4
	-[0x80004100]:csrrs a2, fcsr, zero
	-[0x80004104]:sd t6, 704(fp)
	-[0x80004108]:sd a2, 712(fp)
Current Store : [0x80004108] : sd a2, 712(fp) -- Store: [0x80014270]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004134]:fmax.s t6, t5, t4
	-[0x80004138]:csrrs a2, fcsr, zero
	-[0x8000413c]:sd t6, 720(fp)
	-[0x80004140]:sd a2, 728(fp)
Current Store : [0x80004140] : sd a2, 728(fp) -- Store: [0x80014280]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000416c]:fmax.s t6, t5, t4
	-[0x80004170]:csrrs a2, fcsr, zero
	-[0x80004174]:sd t6, 736(fp)
	-[0x80004178]:sd a2, 744(fp)
Current Store : [0x80004178] : sd a2, 744(fp) -- Store: [0x80014290]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800041a4]:fmax.s t6, t5, t4
	-[0x800041a8]:csrrs a2, fcsr, zero
	-[0x800041ac]:sd t6, 752(fp)
	-[0x800041b0]:sd a2, 760(fp)
Current Store : [0x800041b0] : sd a2, 760(fp) -- Store: [0x800142a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800041dc]:fmax.s t6, t5, t4
	-[0x800041e0]:csrrs a2, fcsr, zero
	-[0x800041e4]:sd t6, 768(fp)
	-[0x800041e8]:sd a2, 776(fp)
Current Store : [0x800041e8] : sd a2, 776(fp) -- Store: [0x800142b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004214]:fmax.s t6, t5, t4
	-[0x80004218]:csrrs a2, fcsr, zero
	-[0x8000421c]:sd t6, 784(fp)
	-[0x80004220]:sd a2, 792(fp)
Current Store : [0x80004220] : sd a2, 792(fp) -- Store: [0x800142c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfa and fm2 == 0x291dc8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000424c]:fmax.s t6, t5, t4
	-[0x80004250]:csrrs a2, fcsr, zero
	-[0x80004254]:sd t6, 800(fp)
	-[0x80004258]:sd a2, 808(fp)
Current Store : [0x80004258] : sd a2, 808(fp) -- Store: [0x800142d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004284]:fmax.s t6, t5, t4
	-[0x80004288]:csrrs a2, fcsr, zero
	-[0x8000428c]:sd t6, 816(fp)
	-[0x80004290]:sd a2, 824(fp)
Current Store : [0x80004290] : sd a2, 824(fp) -- Store: [0x800142e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800042bc]:fmax.s t6, t5, t4
	-[0x800042c0]:csrrs a2, fcsr, zero
	-[0x800042c4]:sd t6, 832(fp)
	-[0x800042c8]:sd a2, 840(fp)
Current Store : [0x800042c8] : sd a2, 840(fp) -- Store: [0x800142f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800042f4]:fmax.s t6, t5, t4
	-[0x800042f8]:csrrs a2, fcsr, zero
	-[0x800042fc]:sd t6, 848(fp)
	-[0x80004300]:sd a2, 856(fp)
Current Store : [0x80004300] : sd a2, 856(fp) -- Store: [0x80014300]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfb and fm2 == 0x153eee and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000432c]:fmax.s t6, t5, t4
	-[0x80004330]:csrrs a2, fcsr, zero
	-[0x80004334]:sd t6, 864(fp)
	-[0x80004338]:sd a2, 872(fp)
Current Store : [0x80004338] : sd a2, 872(fp) -- Store: [0x80014310]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004364]:fmax.s t6, t5, t4
	-[0x80004368]:csrrs a2, fcsr, zero
	-[0x8000436c]:sd t6, 880(fp)
	-[0x80004370]:sd a2, 888(fp)
Current Store : [0x80004370] : sd a2, 888(fp) -- Store: [0x80014320]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000439c]:fmax.s t6, t5, t4
	-[0x800043a0]:csrrs a2, fcsr, zero
	-[0x800043a4]:sd t6, 896(fp)
	-[0x800043a8]:sd a2, 904(fp)
Current Store : [0x800043a8] : sd a2, 904(fp) -- Store: [0x80014330]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800043d4]:fmax.s t6, t5, t4
	-[0x800043d8]:csrrs a2, fcsr, zero
	-[0x800043dc]:sd t6, 912(fp)
	-[0x800043e0]:sd a2, 920(fp)
Current Store : [0x800043e0] : sd a2, 920(fp) -- Store: [0x80014340]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfb and fm2 == 0x1946c8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000440c]:fmax.s t6, t5, t4
	-[0x80004410]:csrrs a2, fcsr, zero
	-[0x80004414]:sd t6, 928(fp)
	-[0x80004418]:sd a2, 936(fp)
Current Store : [0x80004418] : sd a2, 936(fp) -- Store: [0x80014350]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004444]:fmax.s t6, t5, t4
	-[0x80004448]:csrrs a2, fcsr, zero
	-[0x8000444c]:sd t6, 944(fp)
	-[0x80004450]:sd a2, 952(fp)
Current Store : [0x80004450] : sd a2, 952(fp) -- Store: [0x80014360]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000447c]:fmax.s t6, t5, t4
	-[0x80004480]:csrrs a2, fcsr, zero
	-[0x80004484]:sd t6, 960(fp)
	-[0x80004488]:sd a2, 968(fp)
Current Store : [0x80004488] : sd a2, 968(fp) -- Store: [0x80014370]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800044b4]:fmax.s t6, t5, t4
	-[0x800044b8]:csrrs a2, fcsr, zero
	-[0x800044bc]:sd t6, 976(fp)
	-[0x800044c0]:sd a2, 984(fp)
Current Store : [0x800044c0] : sd a2, 984(fp) -- Store: [0x80014380]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800044ec]:fmax.s t6, t5, t4
	-[0x800044f0]:csrrs a2, fcsr, zero
	-[0x800044f4]:sd t6, 992(fp)
	-[0x800044f8]:sd a2, 1000(fp)
Current Store : [0x800044f8] : sd a2, 1000(fp) -- Store: [0x80014390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004524]:fmax.s t6, t5, t4
	-[0x80004528]:csrrs a2, fcsr, zero
	-[0x8000452c]:sd t6, 1008(fp)
	-[0x80004530]:sd a2, 1016(fp)
Current Store : [0x80004530] : sd a2, 1016(fp) -- Store: [0x800143a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000455c]:fmax.s t6, t5, t4
	-[0x80004560]:csrrs a2, fcsr, zero
	-[0x80004564]:sd t6, 1024(fp)
	-[0x80004568]:sd a2, 1032(fp)
Current Store : [0x80004568] : sd a2, 1032(fp) -- Store: [0x800143b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004594]:fmax.s t6, t5, t4
	-[0x80004598]:csrrs a2, fcsr, zero
	-[0x8000459c]:sd t6, 1040(fp)
	-[0x800045a0]:sd a2, 1048(fp)
Current Store : [0x800045a0] : sd a2, 1048(fp) -- Store: [0x800143c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800045cc]:fmax.s t6, t5, t4
	-[0x800045d0]:csrrs a2, fcsr, zero
	-[0x800045d4]:sd t6, 1056(fp)
	-[0x800045d8]:sd a2, 1064(fp)
Current Store : [0x800045d8] : sd a2, 1064(fp) -- Store: [0x800143d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004604]:fmax.s t6, t5, t4
	-[0x80004608]:csrrs a2, fcsr, zero
	-[0x8000460c]:sd t6, 1072(fp)
	-[0x80004610]:sd a2, 1080(fp)
Current Store : [0x80004610] : sd a2, 1080(fp) -- Store: [0x800143e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000463c]:fmax.s t6, t5, t4
	-[0x80004640]:csrrs a2, fcsr, zero
	-[0x80004644]:sd t6, 1088(fp)
	-[0x80004648]:sd a2, 1096(fp)
Current Store : [0x80004648] : sd a2, 1096(fp) -- Store: [0x800143f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004674]:fmax.s t6, t5, t4
	-[0x80004678]:csrrs a2, fcsr, zero
	-[0x8000467c]:sd t6, 1104(fp)
	-[0x80004680]:sd a2, 1112(fp)
Current Store : [0x80004680] : sd a2, 1112(fp) -- Store: [0x80014400]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800046ac]:fmax.s t6, t5, t4
	-[0x800046b0]:csrrs a2, fcsr, zero
	-[0x800046b4]:sd t6, 1120(fp)
	-[0x800046b8]:sd a2, 1128(fp)
Current Store : [0x800046b8] : sd a2, 1128(fp) -- Store: [0x80014410]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800046e4]:fmax.s t6, t5, t4
	-[0x800046e8]:csrrs a2, fcsr, zero
	-[0x800046ec]:sd t6, 1136(fp)
	-[0x800046f0]:sd a2, 1144(fp)
Current Store : [0x800046f0] : sd a2, 1144(fp) -- Store: [0x80014420]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000471c]:fmax.s t6, t5, t4
	-[0x80004720]:csrrs a2, fcsr, zero
	-[0x80004724]:sd t6, 1152(fp)
	-[0x80004728]:sd a2, 1160(fp)
Current Store : [0x80004728] : sd a2, 1160(fp) -- Store: [0x80014430]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004754]:fmax.s t6, t5, t4
	-[0x80004758]:csrrs a2, fcsr, zero
	-[0x8000475c]:sd t6, 1168(fp)
	-[0x80004760]:sd a2, 1176(fp)
Current Store : [0x80004760] : sd a2, 1176(fp) -- Store: [0x80014440]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000478c]:fmax.s t6, t5, t4
	-[0x80004790]:csrrs a2, fcsr, zero
	-[0x80004794]:sd t6, 1184(fp)
	-[0x80004798]:sd a2, 1192(fp)
Current Store : [0x80004798] : sd a2, 1192(fp) -- Store: [0x80014450]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800047c4]:fmax.s t6, t5, t4
	-[0x800047c8]:csrrs a2, fcsr, zero
	-[0x800047cc]:sd t6, 1200(fp)
	-[0x800047d0]:sd a2, 1208(fp)
Current Store : [0x800047d0] : sd a2, 1208(fp) -- Store: [0x80014460]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800047fc]:fmax.s t6, t5, t4
	-[0x80004800]:csrrs a2, fcsr, zero
	-[0x80004804]:sd t6, 1216(fp)
	-[0x80004808]:sd a2, 1224(fp)
Current Store : [0x80004808] : sd a2, 1224(fp) -- Store: [0x80014470]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004834]:fmax.s t6, t5, t4
	-[0x80004838]:csrrs a2, fcsr, zero
	-[0x8000483c]:sd t6, 1232(fp)
	-[0x80004840]:sd a2, 1240(fp)
Current Store : [0x80004840] : sd a2, 1240(fp) -- Store: [0x80014480]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000486c]:fmax.s t6, t5, t4
	-[0x80004870]:csrrs a2, fcsr, zero
	-[0x80004874]:sd t6, 1248(fp)
	-[0x80004878]:sd a2, 1256(fp)
Current Store : [0x80004878] : sd a2, 1256(fp) -- Store: [0x80014490]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800048a4]:fmax.s t6, t5, t4
	-[0x800048a8]:csrrs a2, fcsr, zero
	-[0x800048ac]:sd t6, 1264(fp)
	-[0x800048b0]:sd a2, 1272(fp)
Current Store : [0x800048b0] : sd a2, 1272(fp) -- Store: [0x800144a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800048dc]:fmax.s t6, t5, t4
	-[0x800048e0]:csrrs a2, fcsr, zero
	-[0x800048e4]:sd t6, 1280(fp)
	-[0x800048e8]:sd a2, 1288(fp)
Current Store : [0x800048e8] : sd a2, 1288(fp) -- Store: [0x800144b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004914]:fmax.s t6, t5, t4
	-[0x80004918]:csrrs a2, fcsr, zero
	-[0x8000491c]:sd t6, 1296(fp)
	-[0x80004920]:sd a2, 1304(fp)
Current Store : [0x80004920] : sd a2, 1304(fp) -- Store: [0x800144c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000494c]:fmax.s t6, t5, t4
	-[0x80004950]:csrrs a2, fcsr, zero
	-[0x80004954]:sd t6, 1312(fp)
	-[0x80004958]:sd a2, 1320(fp)
Current Store : [0x80004958] : sd a2, 1320(fp) -- Store: [0x800144d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004984]:fmax.s t6, t5, t4
	-[0x80004988]:csrrs a2, fcsr, zero
	-[0x8000498c]:sd t6, 1328(fp)
	-[0x80004990]:sd a2, 1336(fp)
Current Store : [0x80004990] : sd a2, 1336(fp) -- Store: [0x800144e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800049bc]:fmax.s t6, t5, t4
	-[0x800049c0]:csrrs a2, fcsr, zero
	-[0x800049c4]:sd t6, 1344(fp)
	-[0x800049c8]:sd a2, 1352(fp)
Current Store : [0x800049c8] : sd a2, 1352(fp) -- Store: [0x800144f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800049f4]:fmax.s t6, t5, t4
	-[0x800049f8]:csrrs a2, fcsr, zero
	-[0x800049fc]:sd t6, 1360(fp)
	-[0x80004a00]:sd a2, 1368(fp)
Current Store : [0x80004a00] : sd a2, 1368(fp) -- Store: [0x80014500]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a2c]:fmax.s t6, t5, t4
	-[0x80004a30]:csrrs a2, fcsr, zero
	-[0x80004a34]:sd t6, 1376(fp)
	-[0x80004a38]:sd a2, 1384(fp)
Current Store : [0x80004a38] : sd a2, 1384(fp) -- Store: [0x80014510]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a64]:fmax.s t6, t5, t4
	-[0x80004a68]:csrrs a2, fcsr, zero
	-[0x80004a6c]:sd t6, 1392(fp)
	-[0x80004a70]:sd a2, 1400(fp)
Current Store : [0x80004a70] : sd a2, 1400(fp) -- Store: [0x80014520]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004a9c]:fmax.s t6, t5, t4
	-[0x80004aa0]:csrrs a2, fcsr, zero
	-[0x80004aa4]:sd t6, 1408(fp)
	-[0x80004aa8]:sd a2, 1416(fp)
Current Store : [0x80004aa8] : sd a2, 1416(fp) -- Store: [0x80014530]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ad4]:fmax.s t6, t5, t4
	-[0x80004ad8]:csrrs a2, fcsr, zero
	-[0x80004adc]:sd t6, 1424(fp)
	-[0x80004ae0]:sd a2, 1432(fp)
Current Store : [0x80004ae0] : sd a2, 1432(fp) -- Store: [0x80014540]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b0c]:fmax.s t6, t5, t4
	-[0x80004b10]:csrrs a2, fcsr, zero
	-[0x80004b14]:sd t6, 1440(fp)
	-[0x80004b18]:sd a2, 1448(fp)
Current Store : [0x80004b18] : sd a2, 1448(fp) -- Store: [0x80014550]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b44]:fmax.s t6, t5, t4
	-[0x80004b48]:csrrs a2, fcsr, zero
	-[0x80004b4c]:sd t6, 1456(fp)
	-[0x80004b50]:sd a2, 1464(fp)
Current Store : [0x80004b50] : sd a2, 1464(fp) -- Store: [0x80014560]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004b7c]:fmax.s t6, t5, t4
	-[0x80004b80]:csrrs a2, fcsr, zero
	-[0x80004b84]:sd t6, 1472(fp)
	-[0x80004b88]:sd a2, 1480(fp)
Current Store : [0x80004b88] : sd a2, 1480(fp) -- Store: [0x80014570]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004bb4]:fmax.s t6, t5, t4
	-[0x80004bb8]:csrrs a2, fcsr, zero
	-[0x80004bbc]:sd t6, 1488(fp)
	-[0x80004bc0]:sd a2, 1496(fp)
Current Store : [0x80004bc0] : sd a2, 1496(fp) -- Store: [0x80014580]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004bec]:fmax.s t6, t5, t4
	-[0x80004bf0]:csrrs a2, fcsr, zero
	-[0x80004bf4]:sd t6, 1504(fp)
	-[0x80004bf8]:sd a2, 1512(fp)
Current Store : [0x80004bf8] : sd a2, 1512(fp) -- Store: [0x80014590]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c24]:fmax.s t6, t5, t4
	-[0x80004c28]:csrrs a2, fcsr, zero
	-[0x80004c2c]:sd t6, 1520(fp)
	-[0x80004c30]:sd a2, 1528(fp)
Current Store : [0x80004c30] : sd a2, 1528(fp) -- Store: [0x800145a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c5c]:fmax.s t6, t5, t4
	-[0x80004c60]:csrrs a2, fcsr, zero
	-[0x80004c64]:sd t6, 1536(fp)
	-[0x80004c68]:sd a2, 1544(fp)
Current Store : [0x80004c68] : sd a2, 1544(fp) -- Store: [0x800145b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004c94]:fmax.s t6, t5, t4
	-[0x80004c98]:csrrs a2, fcsr, zero
	-[0x80004c9c]:sd t6, 1552(fp)
	-[0x80004ca0]:sd a2, 1560(fp)
Current Store : [0x80004ca0] : sd a2, 1560(fp) -- Store: [0x800145c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ccc]:fmax.s t6, t5, t4
	-[0x80004cd0]:csrrs a2, fcsr, zero
	-[0x80004cd4]:sd t6, 1568(fp)
	-[0x80004cd8]:sd a2, 1576(fp)
Current Store : [0x80004cd8] : sd a2, 1576(fp) -- Store: [0x800145d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004d04]:fmax.s t6, t5, t4
	-[0x80004d08]:csrrs a2, fcsr, zero
	-[0x80004d0c]:sd t6, 1584(fp)
	-[0x80004d10]:sd a2, 1592(fp)
Current Store : [0x80004d10] : sd a2, 1592(fp) -- Store: [0x800145e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004d3c]:fmax.s t6, t5, t4
	-[0x80004d40]:csrrs a2, fcsr, zero
	-[0x80004d44]:sd t6, 1600(fp)
	-[0x80004d48]:sd a2, 1608(fp)
Current Store : [0x80004d48] : sd a2, 1608(fp) -- Store: [0x800145f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004d74]:fmax.s t6, t5, t4
	-[0x80004d78]:csrrs a2, fcsr, zero
	-[0x80004d7c]:sd t6, 1616(fp)
	-[0x80004d80]:sd a2, 1624(fp)
Current Store : [0x80004d80] : sd a2, 1624(fp) -- Store: [0x80014600]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2a65f8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004dac]:fmax.s t6, t5, t4
	-[0x80004db0]:csrrs a2, fcsr, zero
	-[0x80004db4]:sd t6, 1632(fp)
	-[0x80004db8]:sd a2, 1640(fp)
Current Store : [0x80004db8] : sd a2, 1640(fp) -- Store: [0x80014610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2a65f8 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004de4]:fmax.s t6, t5, t4
	-[0x80004de8]:csrrs a2, fcsr, zero
	-[0x80004dec]:sd t6, 1648(fp)
	-[0x80004df0]:sd a2, 1656(fp)
Current Store : [0x80004df0] : sd a2, 1656(fp) -- Store: [0x80014620]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004e1c]:fmax.s t6, t5, t4
	-[0x80004e20]:csrrs a2, fcsr, zero
	-[0x80004e24]:sd t6, 1664(fp)
	-[0x80004e28]:sd a2, 1672(fp)
Current Store : [0x80004e28] : sd a2, 1672(fp) -- Store: [0x80014630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x80 and fm1 == 0x7931e5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004e54]:fmax.s t6, t5, t4
	-[0x80004e58]:csrrs a2, fcsr, zero
	-[0x80004e5c]:sd t6, 1680(fp)
	-[0x80004e60]:sd a2, 1688(fp)
Current Store : [0x80004e60] : sd a2, 1688(fp) -- Store: [0x80014640]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x7931e5 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004e8c]:fmax.s t6, t5, t4
	-[0x80004e90]:csrrs a2, fcsr, zero
	-[0x80004e94]:sd t6, 1696(fp)
	-[0x80004e98]:sd a2, 1704(fp)
Current Store : [0x80004e98] : sd a2, 1704(fp) -- Store: [0x80014650]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x6a577e and fs2 == 1 and fe2 == 0x80 and fm2 == 0x7931e5 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004ec4]:fmax.s t6, t5, t4
	-[0x80004ec8]:csrrs a2, fcsr, zero
	-[0x80004ecc]:sd t6, 1712(fp)
	-[0x80004ed0]:sd a2, 1720(fp)
Current Store : [0x80004ed0] : sd a2, 1720(fp) -- Store: [0x80014660]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004efc]:fmax.s t6, t5, t4
	-[0x80004f00]:csrrs a2, fcsr, zero
	-[0x80004f04]:sd t6, 1728(fp)
	-[0x80004f08]:sd a2, 1736(fp)
Current Store : [0x80004f08] : sd a2, 1736(fp) -- Store: [0x80014670]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004f34]:fmax.s t6, t5, t4
	-[0x80004f38]:csrrs a2, fcsr, zero
	-[0x80004f3c]:sd t6, 1744(fp)
	-[0x80004f40]:sd a2, 1752(fp)
Current Store : [0x80004f40] : sd a2, 1752(fp) -- Store: [0x80014680]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfa and fm2 == 0x291dc8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004f6c]:fmax.s t6, t5, t4
	-[0x80004f70]:csrrs a2, fcsr, zero
	-[0x80004f74]:sd t6, 1760(fp)
	-[0x80004f78]:sd a2, 1768(fp)
Current Store : [0x80004f78] : sd a2, 1768(fp) -- Store: [0x80014690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80004fa4]:fmax.s t6, t5, t4
	-[0x80004fa8]:csrrs a2, fcsr, zero
	-[0x80004fac]:sd t6, 1776(fp)
	-[0x80004fb0]:sd a2, 1784(fp)
Current Store : [0x80004fb0] : sd a2, 1784(fp) -- Store: [0x800146a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80004fdc]:fmax.s t6, t5, t4
	-[0x80004fe0]:csrrs a2, fcsr, zero
	-[0x80004fe4]:sd t6, 1792(fp)
	-[0x80004fe8]:sd a2, 1800(fp)
Current Store : [0x80004fe8] : sd a2, 1800(fp) -- Store: [0x800146b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005014]:fmax.s t6, t5, t4
	-[0x80005018]:csrrs a2, fcsr, zero
	-[0x8000501c]:sd t6, 1808(fp)
	-[0x80005020]:sd a2, 1816(fp)
Current Store : [0x80005020] : sd a2, 1816(fp) -- Store: [0x800146c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000504c]:fmax.s t6, t5, t4
	-[0x80005050]:csrrs a2, fcsr, zero
	-[0x80005054]:sd t6, 1824(fp)
	-[0x80005058]:sd a2, 1832(fp)
Current Store : [0x80005058] : sd a2, 1832(fp) -- Store: [0x800146d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005084]:fmax.s t6, t5, t4
	-[0x80005088]:csrrs a2, fcsr, zero
	-[0x8000508c]:sd t6, 1840(fp)
	-[0x80005090]:sd a2, 1848(fp)
Current Store : [0x80005090] : sd a2, 1848(fp) -- Store: [0x800146e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800050bc]:fmax.s t6, t5, t4
	-[0x800050c0]:csrrs a2, fcsr, zero
	-[0x800050c4]:sd t6, 1856(fp)
	-[0x800050c8]:sd a2, 1864(fp)
Current Store : [0x800050c8] : sd a2, 1864(fp) -- Store: [0x800146f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800050f4]:fmax.s t6, t5, t4
	-[0x800050f8]:csrrs a2, fcsr, zero
	-[0x800050fc]:sd t6, 1872(fp)
	-[0x80005100]:sd a2, 1880(fp)
Current Store : [0x80005100] : sd a2, 1880(fp) -- Store: [0x80014700]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000512c]:fmax.s t6, t5, t4
	-[0x80005130]:csrrs a2, fcsr, zero
	-[0x80005134]:sd t6, 1888(fp)
	-[0x80005138]:sd a2, 1896(fp)
Current Store : [0x80005138] : sd a2, 1896(fp) -- Store: [0x80014710]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005164]:fmax.s t6, t5, t4
	-[0x80005168]:csrrs a2, fcsr, zero
	-[0x8000516c]:sd t6, 1904(fp)
	-[0x80005170]:sd a2, 1912(fp)
Current Store : [0x80005170] : sd a2, 1912(fp) -- Store: [0x80014720]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000519c]:fmax.s t6, t5, t4
	-[0x800051a0]:csrrs a2, fcsr, zero
	-[0x800051a4]:sd t6, 1920(fp)
	-[0x800051a8]:sd a2, 1928(fp)
Current Store : [0x800051a8] : sd a2, 1928(fp) -- Store: [0x80014730]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800051d4]:fmax.s t6, t5, t4
	-[0x800051d8]:csrrs a2, fcsr, zero
	-[0x800051dc]:sd t6, 1936(fp)
	-[0x800051e0]:sd a2, 1944(fp)
Current Store : [0x800051e0] : sd a2, 1944(fp) -- Store: [0x80014740]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000520c]:fmax.s t6, t5, t4
	-[0x80005210]:csrrs a2, fcsr, zero
	-[0x80005214]:sd t6, 1952(fp)
	-[0x80005218]:sd a2, 1960(fp)
Current Store : [0x80005218] : sd a2, 1960(fp) -- Store: [0x80014750]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005244]:fmax.s t6, t5, t4
	-[0x80005248]:csrrs a2, fcsr, zero
	-[0x8000524c]:sd t6, 1968(fp)
	-[0x80005250]:sd a2, 1976(fp)
Current Store : [0x80005250] : sd a2, 1976(fp) -- Store: [0x80014760]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x291dc8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000527c]:fmax.s t6, t5, t4
	-[0x80005280]:csrrs a2, fcsr, zero
	-[0x80005284]:sd t6, 1984(fp)
	-[0x80005288]:sd a2, 1992(fp)
Current Store : [0x80005288] : sd a2, 1992(fp) -- Store: [0x80014770]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x291dc8 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800052bc]:fmax.s t6, t5, t4
	-[0x800052c0]:csrrs a2, fcsr, zero
	-[0x800052c4]:sd t6, 2000(fp)
	-[0x800052c8]:sd a2, 2008(fp)
Current Store : [0x800052c8] : sd a2, 2008(fp) -- Store: [0x80014780]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800052fc]:fmax.s t6, t5, t4
	-[0x80005300]:csrrs a2, fcsr, zero
	-[0x80005304]:sd t6, 2016(fp)
	-[0x80005308]:sd a2, 2024(fp)
Current Store : [0x80005308] : sd a2, 2024(fp) -- Store: [0x80014790]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000533c]:fmax.s t6, t5, t4
	-[0x80005340]:csrrs a2, fcsr, zero
	-[0x80005344]:sd t6, 2032(fp)
	-[0x80005348]:sd a2, 2040(fp)
Current Store : [0x80005348] : sd a2, 2040(fp) -- Store: [0x800147a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005384]:fmax.s t6, t5, t4
	-[0x80005388]:csrrs a2, fcsr, zero
	-[0x8000538c]:sd t6, 0(fp)
	-[0x80005390]:sd a2, 8(fp)
Current Store : [0x80005390] : sd a2, 8(fp) -- Store: [0x800147b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800053c4]:fmax.s t6, t5, t4
	-[0x800053c8]:csrrs a2, fcsr, zero
	-[0x800053cc]:sd t6, 16(fp)
	-[0x800053d0]:sd a2, 24(fp)
Current Store : [0x800053d0] : sd a2, 24(fp) -- Store: [0x800147c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005404]:fmax.s t6, t5, t4
	-[0x80005408]:csrrs a2, fcsr, zero
	-[0x8000540c]:sd t6, 32(fp)
	-[0x80005410]:sd a2, 40(fp)
Current Store : [0x80005410] : sd a2, 40(fp) -- Store: [0x800147d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005444]:fmax.s t6, t5, t4
	-[0x80005448]:csrrs a2, fcsr, zero
	-[0x8000544c]:sd t6, 48(fp)
	-[0x80005450]:sd a2, 56(fp)
Current Store : [0x80005450] : sd a2, 56(fp) -- Store: [0x800147e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005484]:fmax.s t6, t5, t4
	-[0x80005488]:csrrs a2, fcsr, zero
	-[0x8000548c]:sd t6, 64(fp)
	-[0x80005490]:sd a2, 72(fp)
Current Store : [0x80005490] : sd a2, 72(fp) -- Store: [0x800147f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800054c4]:fmax.s t6, t5, t4
	-[0x800054c8]:csrrs a2, fcsr, zero
	-[0x800054cc]:sd t6, 80(fp)
	-[0x800054d0]:sd a2, 88(fp)
Current Store : [0x800054d0] : sd a2, 88(fp) -- Store: [0x80014800]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005504]:fmax.s t6, t5, t4
	-[0x80005508]:csrrs a2, fcsr, zero
	-[0x8000550c]:sd t6, 96(fp)
	-[0x80005510]:sd a2, 104(fp)
Current Store : [0x80005510] : sd a2, 104(fp) -- Store: [0x80014810]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005544]:fmax.s t6, t5, t4
	-[0x80005548]:csrrs a2, fcsr, zero
	-[0x8000554c]:sd t6, 112(fp)
	-[0x80005550]:sd a2, 120(fp)
Current Store : [0x80005550] : sd a2, 120(fp) -- Store: [0x80014820]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005584]:fmax.s t6, t5, t4
	-[0x80005588]:csrrs a2, fcsr, zero
	-[0x8000558c]:sd t6, 128(fp)
	-[0x80005590]:sd a2, 136(fp)
Current Store : [0x80005590] : sd a2, 136(fp) -- Store: [0x80014830]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800055c4]:fmax.s t6, t5, t4
	-[0x800055c8]:csrrs a2, fcsr, zero
	-[0x800055cc]:sd t6, 144(fp)
	-[0x800055d0]:sd a2, 152(fp)
Current Store : [0x800055d0] : sd a2, 152(fp) -- Store: [0x80014840]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005604]:fmax.s t6, t5, t4
	-[0x80005608]:csrrs a2, fcsr, zero
	-[0x8000560c]:sd t6, 160(fp)
	-[0x80005610]:sd a2, 168(fp)
Current Store : [0x80005610] : sd a2, 168(fp) -- Store: [0x80014850]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005644]:fmax.s t6, t5, t4
	-[0x80005648]:csrrs a2, fcsr, zero
	-[0x8000564c]:sd t6, 176(fp)
	-[0x80005650]:sd a2, 184(fp)
Current Store : [0x80005650] : sd a2, 184(fp) -- Store: [0x80014860]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005684]:fmax.s t6, t5, t4
	-[0x80005688]:csrrs a2, fcsr, zero
	-[0x8000568c]:sd t6, 192(fp)
	-[0x80005690]:sd a2, 200(fp)
Current Store : [0x80005690] : sd a2, 200(fp) -- Store: [0x80014870]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800056c4]:fmax.s t6, t5, t4
	-[0x800056c8]:csrrs a2, fcsr, zero
	-[0x800056cc]:sd t6, 208(fp)
	-[0x800056d0]:sd a2, 216(fp)
Current Store : [0x800056d0] : sd a2, 216(fp) -- Store: [0x80014880]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005704]:fmax.s t6, t5, t4
	-[0x80005708]:csrrs a2, fcsr, zero
	-[0x8000570c]:sd t6, 224(fp)
	-[0x80005710]:sd a2, 232(fp)
Current Store : [0x80005710] : sd a2, 232(fp) -- Store: [0x80014890]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005744]:fmax.s t6, t5, t4
	-[0x80005748]:csrrs a2, fcsr, zero
	-[0x8000574c]:sd t6, 240(fp)
	-[0x80005750]:sd a2, 248(fp)
Current Store : [0x80005750] : sd a2, 248(fp) -- Store: [0x800148a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005784]:fmax.s t6, t5, t4
	-[0x80005788]:csrrs a2, fcsr, zero
	-[0x8000578c]:sd t6, 256(fp)
	-[0x80005790]:sd a2, 264(fp)
Current Store : [0x80005790] : sd a2, 264(fp) -- Store: [0x800148b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800057c4]:fmax.s t6, t5, t4
	-[0x800057c8]:csrrs a2, fcsr, zero
	-[0x800057cc]:sd t6, 272(fp)
	-[0x800057d0]:sd a2, 280(fp)
Current Store : [0x800057d0] : sd a2, 280(fp) -- Store: [0x800148c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005804]:fmax.s t6, t5, t4
	-[0x80005808]:csrrs a2, fcsr, zero
	-[0x8000580c]:sd t6, 288(fp)
	-[0x80005810]:sd a2, 296(fp)
Current Store : [0x80005810] : sd a2, 296(fp) -- Store: [0x800148d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005844]:fmax.s t6, t5, t4
	-[0x80005848]:csrrs a2, fcsr, zero
	-[0x8000584c]:sd t6, 304(fp)
	-[0x80005850]:sd a2, 312(fp)
Current Store : [0x80005850] : sd a2, 312(fp) -- Store: [0x800148e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005884]:fmax.s t6, t5, t4
	-[0x80005888]:csrrs a2, fcsr, zero
	-[0x8000588c]:sd t6, 320(fp)
	-[0x80005890]:sd a2, 328(fp)
Current Store : [0x80005890] : sd a2, 328(fp) -- Store: [0x800148f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800058c4]:fmax.s t6, t5, t4
	-[0x800058c8]:csrrs a2, fcsr, zero
	-[0x800058cc]:sd t6, 336(fp)
	-[0x800058d0]:sd a2, 344(fp)
Current Store : [0x800058d0] : sd a2, 344(fp) -- Store: [0x80014900]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005904]:fmax.s t6, t5, t4
	-[0x80005908]:csrrs a2, fcsr, zero
	-[0x8000590c]:sd t6, 352(fp)
	-[0x80005910]:sd a2, 360(fp)
Current Store : [0x80005910] : sd a2, 360(fp) -- Store: [0x80014910]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005944]:fmax.s t6, t5, t4
	-[0x80005948]:csrrs a2, fcsr, zero
	-[0x8000594c]:sd t6, 368(fp)
	-[0x80005950]:sd a2, 376(fp)
Current Store : [0x80005950] : sd a2, 376(fp) -- Store: [0x80014920]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005984]:fmax.s t6, t5, t4
	-[0x80005988]:csrrs a2, fcsr, zero
	-[0x8000598c]:sd t6, 384(fp)
	-[0x80005990]:sd a2, 392(fp)
Current Store : [0x80005990] : sd a2, 392(fp) -- Store: [0x80014930]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800059c4]:fmax.s t6, t5, t4
	-[0x800059c8]:csrrs a2, fcsr, zero
	-[0x800059cc]:sd t6, 400(fp)
	-[0x800059d0]:sd a2, 408(fp)
Current Store : [0x800059d0] : sd a2, 408(fp) -- Store: [0x80014940]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a04]:fmax.s t6, t5, t4
	-[0x80005a08]:csrrs a2, fcsr, zero
	-[0x80005a0c]:sd t6, 416(fp)
	-[0x80005a10]:sd a2, 424(fp)
Current Store : [0x80005a10] : sd a2, 424(fp) -- Store: [0x80014950]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f4c77 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a44]:fmax.s t6, t5, t4
	-[0x80005a48]:csrrs a2, fcsr, zero
	-[0x80005a4c]:sd t6, 432(fp)
	-[0x80005a50]:sd a2, 440(fp)
Current Store : [0x80005a50] : sd a2, 440(fp) -- Store: [0x80014960]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005a84]:fmax.s t6, t5, t4
	-[0x80005a88]:csrrs a2, fcsr, zero
	-[0x80005a8c]:sd t6, 448(fp)
	-[0x80005a90]:sd a2, 456(fp)
Current Store : [0x80005a90] : sd a2, 456(fp) -- Store: [0x80014970]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005ac4]:fmax.s t6, t5, t4
	-[0x80005ac8]:csrrs a2, fcsr, zero
	-[0x80005acc]:sd t6, 464(fp)
	-[0x80005ad0]:sd a2, 472(fp)
Current Store : [0x80005ad0] : sd a2, 472(fp) -- Store: [0x80014980]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f4c77 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005b04]:fmax.s t6, t5, t4
	-[0x80005b08]:csrrs a2, fcsr, zero
	-[0x80005b0c]:sd t6, 480(fp)
	-[0x80005b10]:sd a2, 488(fp)
Current Store : [0x80005b10] : sd a2, 488(fp) -- Store: [0x80014990]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005b44]:fmax.s t6, t5, t4
	-[0x80005b48]:csrrs a2, fcsr, zero
	-[0x80005b4c]:sd t6, 496(fp)
	-[0x80005b50]:sd a2, 504(fp)
Current Store : [0x80005b50] : sd a2, 504(fp) -- Store: [0x800149a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x33d5d8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005b84]:fmax.s t6, t5, t4
	-[0x80005b88]:csrrs a2, fcsr, zero
	-[0x80005b8c]:sd t6, 512(fp)
	-[0x80005b90]:sd a2, 520(fp)
Current Store : [0x80005b90] : sd a2, 520(fp) -- Store: [0x800149b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x33d5d8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005bc4]:fmax.s t6, t5, t4
	-[0x80005bc8]:csrrs a2, fcsr, zero
	-[0x80005bcc]:sd t6, 528(fp)
	-[0x80005bd0]:sd a2, 536(fp)
Current Store : [0x80005bd0] : sd a2, 536(fp) -- Store: [0x800149c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x53653a and fs2 == 1 and fe2 == 0x7f and fm2 == 0x33d5d8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005c04]:fmax.s t6, t5, t4
	-[0x80005c08]:csrrs a2, fcsr, zero
	-[0x80005c0c]:sd t6, 544(fp)
	-[0x80005c10]:sd a2, 552(fp)
Current Store : [0x80005c10] : sd a2, 552(fp) -- Store: [0x800149d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005c44]:fmax.s t6, t5, t4
	-[0x80005c48]:csrrs a2, fcsr, zero
	-[0x80005c4c]:sd t6, 560(fp)
	-[0x80005c50]:sd a2, 568(fp)
Current Store : [0x80005c50] : sd a2, 568(fp) -- Store: [0x800149e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005c84]:fmax.s t6, t5, t4
	-[0x80005c88]:csrrs a2, fcsr, zero
	-[0x80005c8c]:sd t6, 576(fp)
	-[0x80005c90]:sd a2, 584(fp)
Current Store : [0x80005c90] : sd a2, 584(fp) -- Store: [0x800149f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x153eee and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005cc4]:fmax.s t6, t5, t4
	-[0x80005cc8]:csrrs a2, fcsr, zero
	-[0x80005ccc]:sd t6, 592(fp)
	-[0x80005cd0]:sd a2, 600(fp)
Current Store : [0x80005cd0] : sd a2, 600(fp) -- Store: [0x80014a00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005d04]:fmax.s t6, t5, t4
	-[0x80005d08]:csrrs a2, fcsr, zero
	-[0x80005d0c]:sd t6, 608(fp)
	-[0x80005d10]:sd a2, 616(fp)
Current Store : [0x80005d10] : sd a2, 616(fp) -- Store: [0x80014a10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005d44]:fmax.s t6, t5, t4
	-[0x80005d48]:csrrs a2, fcsr, zero
	-[0x80005d4c]:sd t6, 624(fp)
	-[0x80005d50]:sd a2, 632(fp)
Current Store : [0x80005d50] : sd a2, 632(fp) -- Store: [0x80014a20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005d84]:fmax.s t6, t5, t4
	-[0x80005d88]:csrrs a2, fcsr, zero
	-[0x80005d8c]:sd t6, 640(fp)
	-[0x80005d90]:sd a2, 648(fp)
Current Store : [0x80005d90] : sd a2, 648(fp) -- Store: [0x80014a30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005dc4]:fmax.s t6, t5, t4
	-[0x80005dc8]:csrrs a2, fcsr, zero
	-[0x80005dcc]:sd t6, 656(fp)
	-[0x80005dd0]:sd a2, 664(fp)
Current Store : [0x80005dd0] : sd a2, 664(fp) -- Store: [0x80014a40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80005e04]:fmax.s t6, t5, t4
	-[0x80005e08]:csrrs a2, fcsr, zero
	-[0x80005e0c]:sd t6, 672(fp)
	-[0x80005e10]:sd a2, 680(fp)
Current Store : [0x80005e10] : sd a2, 680(fp) -- Store: [0x80014a50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005e44]:fmax.s t6, t5, t4
	-[0x80005e48]:csrrs a2, fcsr, zero
	-[0x80005e4c]:sd t6, 688(fp)
	-[0x80005e50]:sd a2, 696(fp)
Current Store : [0x80005e50] : sd a2, 696(fp) -- Store: [0x80014a60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005e84]:fmax.s t6, t5, t4
	-[0x80005e88]:csrrs a2, fcsr, zero
	-[0x80005e8c]:sd t6, 704(fp)
	-[0x80005e90]:sd a2, 712(fp)
Current Store : [0x80005e90] : sd a2, 712(fp) -- Store: [0x80014a70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005ec4]:fmax.s t6, t5, t4
	-[0x80005ec8]:csrrs a2, fcsr, zero
	-[0x80005ecc]:sd t6, 720(fp)
	-[0x80005ed0]:sd a2, 728(fp)
Current Store : [0x80005ed0] : sd a2, 728(fp) -- Store: [0x80014a80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f04]:fmax.s t6, t5, t4
	-[0x80005f08]:csrrs a2, fcsr, zero
	-[0x80005f0c]:sd t6, 736(fp)
	-[0x80005f10]:sd a2, 744(fp)
Current Store : [0x80005f10] : sd a2, 744(fp) -- Store: [0x80014a90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f44]:fmax.s t6, t5, t4
	-[0x80005f48]:csrrs a2, fcsr, zero
	-[0x80005f4c]:sd t6, 752(fp)
	-[0x80005f50]:sd a2, 760(fp)
Current Store : [0x80005f50] : sd a2, 760(fp) -- Store: [0x80014aa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005f84]:fmax.s t6, t5, t4
	-[0x80005f88]:csrrs a2, fcsr, zero
	-[0x80005f8c]:sd t6, 768(fp)
	-[0x80005f90]:sd a2, 776(fp)
Current Store : [0x80005f90] : sd a2, 776(fp) -- Store: [0x80014ab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x153eee and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80005fc4]:fmax.s t6, t5, t4
	-[0x80005fc8]:csrrs a2, fcsr, zero
	-[0x80005fcc]:sd t6, 784(fp)
	-[0x80005fd0]:sd a2, 792(fp)
Current Store : [0x80005fd0] : sd a2, 792(fp) -- Store: [0x80014ac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x153eee and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006004]:fmax.s t6, t5, t4
	-[0x80006008]:csrrs a2, fcsr, zero
	-[0x8000600c]:sd t6, 800(fp)
	-[0x80006010]:sd a2, 808(fp)
Current Store : [0x80006010] : sd a2, 808(fp) -- Store: [0x80014ad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006044]:fmax.s t6, t5, t4
	-[0x80006048]:csrrs a2, fcsr, zero
	-[0x8000604c]:sd t6, 816(fp)
	-[0x80006050]:sd a2, 824(fp)
Current Store : [0x80006050] : sd a2, 824(fp) -- Store: [0x80014ae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006084]:fmax.s t6, t5, t4
	-[0x80006088]:csrrs a2, fcsr, zero
	-[0x8000608c]:sd t6, 832(fp)
	-[0x80006090]:sd a2, 840(fp)
Current Store : [0x80006090] : sd a2, 840(fp) -- Store: [0x80014af0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800060c4]:fmax.s t6, t5, t4
	-[0x800060c8]:csrrs a2, fcsr, zero
	-[0x800060cc]:sd t6, 848(fp)
	-[0x800060d0]:sd a2, 856(fp)
Current Store : [0x800060d0] : sd a2, 856(fp) -- Store: [0x80014b00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006104]:fmax.s t6, t5, t4
	-[0x80006108]:csrrs a2, fcsr, zero
	-[0x8000610c]:sd t6, 864(fp)
	-[0x80006110]:sd a2, 872(fp)
Current Store : [0x80006110] : sd a2, 872(fp) -- Store: [0x80014b10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006144]:fmax.s t6, t5, t4
	-[0x80006148]:csrrs a2, fcsr, zero
	-[0x8000614c]:sd t6, 880(fp)
	-[0x80006150]:sd a2, 888(fp)
Current Store : [0x80006150] : sd a2, 888(fp) -- Store: [0x80014b20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006184]:fmax.s t6, t5, t4
	-[0x80006188]:csrrs a2, fcsr, zero
	-[0x8000618c]:sd t6, 896(fp)
	-[0x80006190]:sd a2, 904(fp)
Current Store : [0x80006190] : sd a2, 904(fp) -- Store: [0x80014b30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800061c4]:fmax.s t6, t5, t4
	-[0x800061c8]:csrrs a2, fcsr, zero
	-[0x800061cc]:sd t6, 912(fp)
	-[0x800061d0]:sd a2, 920(fp)
Current Store : [0x800061d0] : sd a2, 920(fp) -- Store: [0x80014b40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006204]:fmax.s t6, t5, t4
	-[0x80006208]:csrrs a2, fcsr, zero
	-[0x8000620c]:sd t6, 928(fp)
	-[0x80006210]:sd a2, 936(fp)
Current Store : [0x80006210] : sd a2, 936(fp) -- Store: [0x80014b50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006244]:fmax.s t6, t5, t4
	-[0x80006248]:csrrs a2, fcsr, zero
	-[0x8000624c]:sd t6, 944(fp)
	-[0x80006250]:sd a2, 952(fp)
Current Store : [0x80006250] : sd a2, 952(fp) -- Store: [0x80014b60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006284]:fmax.s t6, t5, t4
	-[0x80006288]:csrrs a2, fcsr, zero
	-[0x8000628c]:sd t6, 960(fp)
	-[0x80006290]:sd a2, 968(fp)
Current Store : [0x80006290] : sd a2, 968(fp) -- Store: [0x80014b70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800062c4]:fmax.s t6, t5, t4
	-[0x800062c8]:csrrs a2, fcsr, zero
	-[0x800062cc]:sd t6, 976(fp)
	-[0x800062d0]:sd a2, 984(fp)
Current Store : [0x800062d0] : sd a2, 984(fp) -- Store: [0x80014b80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006304]:fmax.s t6, t5, t4
	-[0x80006308]:csrrs a2, fcsr, zero
	-[0x8000630c]:sd t6, 992(fp)
	-[0x80006310]:sd a2, 1000(fp)
Current Store : [0x80006310] : sd a2, 1000(fp) -- Store: [0x80014b90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006344]:fmax.s t6, t5, t4
	-[0x80006348]:csrrs a2, fcsr, zero
	-[0x8000634c]:sd t6, 1008(fp)
	-[0x80006350]:sd a2, 1016(fp)
Current Store : [0x80006350] : sd a2, 1016(fp) -- Store: [0x80014ba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006384]:fmax.s t6, t5, t4
	-[0x80006388]:csrrs a2, fcsr, zero
	-[0x8000638c]:sd t6, 1024(fp)
	-[0x80006390]:sd a2, 1032(fp)
Current Store : [0x80006390] : sd a2, 1032(fp) -- Store: [0x80014bb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800063c4]:fmax.s t6, t5, t4
	-[0x800063c8]:csrrs a2, fcsr, zero
	-[0x800063cc]:sd t6, 1040(fp)
	-[0x800063d0]:sd a2, 1048(fp)
Current Store : [0x800063d0] : sd a2, 1048(fp) -- Store: [0x80014bc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006404]:fmax.s t6, t5, t4
	-[0x80006408]:csrrs a2, fcsr, zero
	-[0x8000640c]:sd t6, 1056(fp)
	-[0x80006410]:sd a2, 1064(fp)
Current Store : [0x80006410] : sd a2, 1064(fp) -- Store: [0x80014bd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006444]:fmax.s t6, t5, t4
	-[0x80006448]:csrrs a2, fcsr, zero
	-[0x8000644c]:sd t6, 1072(fp)
	-[0x80006450]:sd a2, 1080(fp)
Current Store : [0x80006450] : sd a2, 1080(fp) -- Store: [0x80014be0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006484]:fmax.s t6, t5, t4
	-[0x80006488]:csrrs a2, fcsr, zero
	-[0x8000648c]:sd t6, 1088(fp)
	-[0x80006490]:sd a2, 1096(fp)
Current Store : [0x80006490] : sd a2, 1096(fp) -- Store: [0x80014bf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800064c4]:fmax.s t6, t5, t4
	-[0x800064c8]:csrrs a2, fcsr, zero
	-[0x800064cc]:sd t6, 1104(fp)
	-[0x800064d0]:sd a2, 1112(fp)
Current Store : [0x800064d0] : sd a2, 1112(fp) -- Store: [0x80014c00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006504]:fmax.s t6, t5, t4
	-[0x80006508]:csrrs a2, fcsr, zero
	-[0x8000650c]:sd t6, 1120(fp)
	-[0x80006510]:sd a2, 1128(fp)
Current Store : [0x80006510] : sd a2, 1128(fp) -- Store: [0x80014c10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006544]:fmax.s t6, t5, t4
	-[0x80006548]:csrrs a2, fcsr, zero
	-[0x8000654c]:sd t6, 1136(fp)
	-[0x80006550]:sd a2, 1144(fp)
Current Store : [0x80006550] : sd a2, 1144(fp) -- Store: [0x80014c20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006584]:fmax.s t6, t5, t4
	-[0x80006588]:csrrs a2, fcsr, zero
	-[0x8000658c]:sd t6, 1152(fp)
	-[0x80006590]:sd a2, 1160(fp)
Current Store : [0x80006590] : sd a2, 1160(fp) -- Store: [0x80014c30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800065c4]:fmax.s t6, t5, t4
	-[0x800065c8]:csrrs a2, fcsr, zero
	-[0x800065cc]:sd t6, 1168(fp)
	-[0x800065d0]:sd a2, 1176(fp)
Current Store : [0x800065d0] : sd a2, 1176(fp) -- Store: [0x80014c40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006604]:fmax.s t6, t5, t4
	-[0x80006608]:csrrs a2, fcsr, zero
	-[0x8000660c]:sd t6, 1184(fp)
	-[0x80006610]:sd a2, 1192(fp)
Current Store : [0x80006610] : sd a2, 1192(fp) -- Store: [0x80014c50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006644]:fmax.s t6, t5, t4
	-[0x80006648]:csrrs a2, fcsr, zero
	-[0x8000664c]:sd t6, 1200(fp)
	-[0x80006650]:sd a2, 1208(fp)
Current Store : [0x80006650] : sd a2, 1208(fp) -- Store: [0x80014c60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006684]:fmax.s t6, t5, t4
	-[0x80006688]:csrrs a2, fcsr, zero
	-[0x8000668c]:sd t6, 1216(fp)
	-[0x80006690]:sd a2, 1224(fp)
Current Store : [0x80006690] : sd a2, 1224(fp) -- Store: [0x80014c70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800066c4]:fmax.s t6, t5, t4
	-[0x800066c8]:csrrs a2, fcsr, zero
	-[0x800066cc]:sd t6, 1232(fp)
	-[0x800066d0]:sd a2, 1240(fp)
Current Store : [0x800066d0] : sd a2, 1240(fp) -- Store: [0x80014c80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006704]:fmax.s t6, t5, t4
	-[0x80006708]:csrrs a2, fcsr, zero
	-[0x8000670c]:sd t6, 1248(fp)
	-[0x80006710]:sd a2, 1256(fp)
Current Store : [0x80006710] : sd a2, 1256(fp) -- Store: [0x80014c90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006744]:fmax.s t6, t5, t4
	-[0x80006748]:csrrs a2, fcsr, zero
	-[0x8000674c]:sd t6, 1264(fp)
	-[0x80006750]:sd a2, 1272(fp)
Current Store : [0x80006750] : sd a2, 1272(fp) -- Store: [0x80014ca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1b0098 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006784]:fmax.s t6, t5, t4
	-[0x80006788]:csrrs a2, fcsr, zero
	-[0x8000678c]:sd t6, 1280(fp)
	-[0x80006790]:sd a2, 1288(fp)
Current Store : [0x80006790] : sd a2, 1288(fp) -- Store: [0x80014cb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800067c4]:fmax.s t6, t5, t4
	-[0x800067c8]:csrrs a2, fcsr, zero
	-[0x800067cc]:sd t6, 1296(fp)
	-[0x800067d0]:sd a2, 1304(fp)
Current Store : [0x800067d0] : sd a2, 1304(fp) -- Store: [0x80014cc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006804]:fmax.s t6, t5, t4
	-[0x80006808]:csrrs a2, fcsr, zero
	-[0x8000680c]:sd t6, 1312(fp)
	-[0x80006810]:sd a2, 1320(fp)
Current Store : [0x80006810] : sd a2, 1320(fp) -- Store: [0x80014cd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1b0098 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006844]:fmax.s t6, t5, t4
	-[0x80006848]:csrrs a2, fcsr, zero
	-[0x8000684c]:sd t6, 1328(fp)
	-[0x80006850]:sd a2, 1336(fp)
Current Store : [0x80006850] : sd a2, 1336(fp) -- Store: [0x80014ce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006884]:fmax.s t6, t5, t4
	-[0x80006888]:csrrs a2, fcsr, zero
	-[0x8000688c]:sd t6, 1344(fp)
	-[0x80006890]:sd a2, 1352(fp)
Current Store : [0x80006890] : sd a2, 1352(fp) -- Store: [0x80014cf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x80 and fm1 == 0x1eb493 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800068c4]:fmax.s t6, t5, t4
	-[0x800068c8]:csrrs a2, fcsr, zero
	-[0x800068cc]:sd t6, 1360(fp)
	-[0x800068d0]:sd a2, 1368(fp)
Current Store : [0x800068d0] : sd a2, 1368(fp) -- Store: [0x80014d00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1eb493 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006904]:fmax.s t6, t5, t4
	-[0x80006908]:csrrs a2, fcsr, zero
	-[0x8000690c]:sd t6, 1376(fp)
	-[0x80006910]:sd a2, 1384(fp)
Current Store : [0x80006910] : sd a2, 1384(fp) -- Store: [0x80014d10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3a8ea9 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x1eb493 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006944]:fmax.s t6, t5, t4
	-[0x80006948]:csrrs a2, fcsr, zero
	-[0x8000694c]:sd t6, 1392(fp)
	-[0x80006950]:sd a2, 1400(fp)
Current Store : [0x80006950] : sd a2, 1400(fp) -- Store: [0x80014d20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006984]:fmax.s t6, t5, t4
	-[0x80006988]:csrrs a2, fcsr, zero
	-[0x8000698c]:sd t6, 1408(fp)
	-[0x80006990]:sd a2, 1416(fp)
Current Store : [0x80006990] : sd a2, 1416(fp) -- Store: [0x80014d30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800069c4]:fmax.s t6, t5, t4
	-[0x800069c8]:csrrs a2, fcsr, zero
	-[0x800069cc]:sd t6, 1424(fp)
	-[0x800069d0]:sd a2, 1432(fp)
Current Store : [0x800069d0] : sd a2, 1432(fp) -- Store: [0x80014d40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x1946c8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006a04]:fmax.s t6, t5, t4
	-[0x80006a08]:csrrs a2, fcsr, zero
	-[0x80006a0c]:sd t6, 1440(fp)
	-[0x80006a10]:sd a2, 1448(fp)
Current Store : [0x80006a10] : sd a2, 1448(fp) -- Store: [0x80014d50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006a44]:fmax.s t6, t5, t4
	-[0x80006a48]:csrrs a2, fcsr, zero
	-[0x80006a4c]:sd t6, 1456(fp)
	-[0x80006a50]:sd a2, 1464(fp)
Current Store : [0x80006a50] : sd a2, 1464(fp) -- Store: [0x80014d60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006a84]:fmax.s t6, t5, t4
	-[0x80006a88]:csrrs a2, fcsr, zero
	-[0x80006a8c]:sd t6, 1472(fp)
	-[0x80006a90]:sd a2, 1480(fp)
Current Store : [0x80006a90] : sd a2, 1480(fp) -- Store: [0x80014d70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ac4]:fmax.s t6, t5, t4
	-[0x80006ac8]:csrrs a2, fcsr, zero
	-[0x80006acc]:sd t6, 1488(fp)
	-[0x80006ad0]:sd a2, 1496(fp)
Current Store : [0x80006ad0] : sd a2, 1496(fp) -- Store: [0x80014d80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006b04]:fmax.s t6, t5, t4
	-[0x80006b08]:csrrs a2, fcsr, zero
	-[0x80006b0c]:sd t6, 1504(fp)
	-[0x80006b10]:sd a2, 1512(fp)
Current Store : [0x80006b10] : sd a2, 1512(fp) -- Store: [0x80014d90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006b44]:fmax.s t6, t5, t4
	-[0x80006b48]:csrrs a2, fcsr, zero
	-[0x80006b4c]:sd t6, 1520(fp)
	-[0x80006b50]:sd a2, 1528(fp)
Current Store : [0x80006b50] : sd a2, 1528(fp) -- Store: [0x80014da0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006b84]:fmax.s t6, t5, t4
	-[0x80006b88]:csrrs a2, fcsr, zero
	-[0x80006b8c]:sd t6, 1536(fp)
	-[0x80006b90]:sd a2, 1544(fp)
Current Store : [0x80006b90] : sd a2, 1544(fp) -- Store: [0x80014db0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006bc4]:fmax.s t6, t5, t4
	-[0x80006bc8]:csrrs a2, fcsr, zero
	-[0x80006bcc]:sd t6, 1552(fp)
	-[0x80006bd0]:sd a2, 1560(fp)
Current Store : [0x80006bd0] : sd a2, 1560(fp) -- Store: [0x80014dc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c04]:fmax.s t6, t5, t4
	-[0x80006c08]:csrrs a2, fcsr, zero
	-[0x80006c0c]:sd t6, 1568(fp)
	-[0x80006c10]:sd a2, 1576(fp)
Current Store : [0x80006c10] : sd a2, 1576(fp) -- Store: [0x80014dd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c44]:fmax.s t6, t5, t4
	-[0x80006c48]:csrrs a2, fcsr, zero
	-[0x80006c4c]:sd t6, 1584(fp)
	-[0x80006c50]:sd a2, 1592(fp)
Current Store : [0x80006c50] : sd a2, 1592(fp) -- Store: [0x80014de0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x1946c8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006c84]:fmax.s t6, t5, t4
	-[0x80006c88]:csrrs a2, fcsr, zero
	-[0x80006c8c]:sd t6, 1600(fp)
	-[0x80006c90]:sd a2, 1608(fp)
Current Store : [0x80006c90] : sd a2, 1608(fp) -- Store: [0x80014df0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x1946c8 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006cc4]:fmax.s t6, t5, t4
	-[0x80006cc8]:csrrs a2, fcsr, zero
	-[0x80006ccc]:sd t6, 1616(fp)
	-[0x80006cd0]:sd a2, 1624(fp)
Current Store : [0x80006cd0] : sd a2, 1624(fp) -- Store: [0x80014e00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006d04]:fmax.s t6, t5, t4
	-[0x80006d08]:csrrs a2, fcsr, zero
	-[0x80006d0c]:sd t6, 1632(fp)
	-[0x80006d10]:sd a2, 1640(fp)
Current Store : [0x80006d10] : sd a2, 1640(fp) -- Store: [0x80014e10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006d44]:fmax.s t6, t5, t4
	-[0x80006d48]:csrrs a2, fcsr, zero
	-[0x80006d4c]:sd t6, 1648(fp)
	-[0x80006d50]:sd a2, 1656(fp)
Current Store : [0x80006d50] : sd a2, 1656(fp) -- Store: [0x80014e20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006d84]:fmax.s t6, t5, t4
	-[0x80006d88]:csrrs a2, fcsr, zero
	-[0x80006d8c]:sd t6, 1664(fp)
	-[0x80006d90]:sd a2, 1672(fp)
Current Store : [0x80006d90] : sd a2, 1672(fp) -- Store: [0x80014e30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006dc4]:fmax.s t6, t5, t4
	-[0x80006dc8]:csrrs a2, fcsr, zero
	-[0x80006dcc]:sd t6, 1680(fp)
	-[0x80006dd0]:sd a2, 1688(fp)
Current Store : [0x80006dd0] : sd a2, 1688(fp) -- Store: [0x80014e40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006e04]:fmax.s t6, t5, t4
	-[0x80006e08]:csrrs a2, fcsr, zero
	-[0x80006e0c]:sd t6, 1696(fp)
	-[0x80006e10]:sd a2, 1704(fp)
Current Store : [0x80006e10] : sd a2, 1704(fp) -- Store: [0x80014e50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006e44]:fmax.s t6, t5, t4
	-[0x80006e48]:csrrs a2, fcsr, zero
	-[0x80006e4c]:sd t6, 1712(fp)
	-[0x80006e50]:sd a2, 1720(fp)
Current Store : [0x80006e50] : sd a2, 1720(fp) -- Store: [0x80014e60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006e84]:fmax.s t6, t5, t4
	-[0x80006e88]:csrrs a2, fcsr, zero
	-[0x80006e8c]:sd t6, 1728(fp)
	-[0x80006e90]:sd a2, 1736(fp)
Current Store : [0x80006e90] : sd a2, 1736(fp) -- Store: [0x80014e70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006ec4]:fmax.s t6, t5, t4
	-[0x80006ec8]:csrrs a2, fcsr, zero
	-[0x80006ecc]:sd t6, 1744(fp)
	-[0x80006ed0]:sd a2, 1752(fp)
Current Store : [0x80006ed0] : sd a2, 1752(fp) -- Store: [0x80014e80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006f04]:fmax.s t6, t5, t4
	-[0x80006f08]:csrrs a2, fcsr, zero
	-[0x80006f0c]:sd t6, 1760(fp)
	-[0x80006f10]:sd a2, 1768(fp)
Current Store : [0x80006f10] : sd a2, 1768(fp) -- Store: [0x80014e90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006f44]:fmax.s t6, t5, t4
	-[0x80006f48]:csrrs a2, fcsr, zero
	-[0x80006f4c]:sd t6, 1776(fp)
	-[0x80006f50]:sd a2, 1784(fp)
Current Store : [0x80006f50] : sd a2, 1784(fp) -- Store: [0x80014ea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80006f84]:fmax.s t6, t5, t4
	-[0x80006f88]:csrrs a2, fcsr, zero
	-[0x80006f8c]:sd t6, 1792(fp)
	-[0x80006f90]:sd a2, 1800(fp)
Current Store : [0x80006f90] : sd a2, 1800(fp) -- Store: [0x80014eb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80006fc4]:fmax.s t6, t5, t4
	-[0x80006fc8]:csrrs a2, fcsr, zero
	-[0x80006fcc]:sd t6, 1808(fp)
	-[0x80006fd0]:sd a2, 1816(fp)
Current Store : [0x80006fd0] : sd a2, 1816(fp) -- Store: [0x80014ec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007004]:fmax.s t6, t5, t4
	-[0x80007008]:csrrs a2, fcsr, zero
	-[0x8000700c]:sd t6, 1824(fp)
	-[0x80007010]:sd a2, 1832(fp)
Current Store : [0x80007010] : sd a2, 1832(fp) -- Store: [0x80014ed0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007044]:fmax.s t6, t5, t4
	-[0x80007048]:csrrs a2, fcsr, zero
	-[0x8000704c]:sd t6, 1840(fp)
	-[0x80007050]:sd a2, 1848(fp)
Current Store : [0x80007050] : sd a2, 1848(fp) -- Store: [0x80014ee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007084]:fmax.s t6, t5, t4
	-[0x80007088]:csrrs a2, fcsr, zero
	-[0x8000708c]:sd t6, 1856(fp)
	-[0x80007090]:sd a2, 1864(fp)
Current Store : [0x80007090] : sd a2, 1864(fp) -- Store: [0x80014ef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800070c4]:fmax.s t6, t5, t4
	-[0x800070c8]:csrrs a2, fcsr, zero
	-[0x800070cc]:sd t6, 1872(fp)
	-[0x800070d0]:sd a2, 1880(fp)
Current Store : [0x800070d0] : sd a2, 1880(fp) -- Store: [0x80014f00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007104]:fmax.s t6, t5, t4
	-[0x80007108]:csrrs a2, fcsr, zero
	-[0x8000710c]:sd t6, 1888(fp)
	-[0x80007110]:sd a2, 1896(fp)
Current Store : [0x80007110] : sd a2, 1896(fp) -- Store: [0x80014f10]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007144]:fmax.s t6, t5, t4
	-[0x80007148]:csrrs a2, fcsr, zero
	-[0x8000714c]:sd t6, 1904(fp)
	-[0x80007150]:sd a2, 1912(fp)
Current Store : [0x80007150] : sd a2, 1912(fp) -- Store: [0x80014f20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007184]:fmax.s t6, t5, t4
	-[0x80007188]:csrrs a2, fcsr, zero
	-[0x8000718c]:sd t6, 1920(fp)
	-[0x80007190]:sd a2, 1928(fp)
Current Store : [0x80007190] : sd a2, 1928(fp) -- Store: [0x80014f30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800071c4]:fmax.s t6, t5, t4
	-[0x800071c8]:csrrs a2, fcsr, zero
	-[0x800071cc]:sd t6, 1936(fp)
	-[0x800071d0]:sd a2, 1944(fp)
Current Store : [0x800071d0] : sd a2, 1944(fp) -- Store: [0x80014f40]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007204]:fmax.s t6, t5, t4
	-[0x80007208]:csrrs a2, fcsr, zero
	-[0x8000720c]:sd t6, 1952(fp)
	-[0x80007210]:sd a2, 1960(fp)
Current Store : [0x80007210] : sd a2, 1960(fp) -- Store: [0x80014f50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007244]:fmax.s t6, t5, t4
	-[0x80007248]:csrrs a2, fcsr, zero
	-[0x8000724c]:sd t6, 1968(fp)
	-[0x80007250]:sd a2, 1976(fp)
Current Store : [0x80007250] : sd a2, 1976(fp) -- Store: [0x80014f60]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007284]:fmax.s t6, t5, t4
	-[0x80007288]:csrrs a2, fcsr, zero
	-[0x8000728c]:sd t6, 1984(fp)
	-[0x80007290]:sd a2, 1992(fp)
Current Store : [0x80007290] : sd a2, 1992(fp) -- Store: [0x80014f70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800072bc]:fmax.s t6, t5, t4
	-[0x800072c0]:csrrs a2, fcsr, zero
	-[0x800072c4]:sd t6, 2000(fp)
	-[0x800072c8]:sd a2, 2008(fp)
Current Store : [0x800072c8] : sd a2, 2008(fp) -- Store: [0x80014f80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800072f4]:fmax.s t6, t5, t4
	-[0x800072f8]:csrrs a2, fcsr, zero
	-[0x800072fc]:sd t6, 2016(fp)
	-[0x80007300]:sd a2, 2024(fp)
Current Store : [0x80007300] : sd a2, 2024(fp) -- Store: [0x80014f90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000732c]:fmax.s t6, t5, t4
	-[0x80007330]:csrrs a2, fcsr, zero
	-[0x80007334]:sd t6, 2032(fp)
	-[0x80007338]:sd a2, 2040(fp)
Current Store : [0x80007338] : sd a2, 2040(fp) -- Store: [0x80014fa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000736c]:fmax.s t6, t5, t4
	-[0x80007370]:csrrs a2, fcsr, zero
	-[0x80007374]:sd t6, 0(fp)
	-[0x80007378]:sd a2, 8(fp)
Current Store : [0x80007378] : sd a2, 8(fp) -- Store: [0x80014fb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800073a4]:fmax.s t6, t5, t4
	-[0x800073a8]:csrrs a2, fcsr, zero
	-[0x800073ac]:sd t6, 16(fp)
	-[0x800073b0]:sd a2, 24(fp)
Current Store : [0x800073b0] : sd a2, 24(fp) -- Store: [0x80014fc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800073dc]:fmax.s t6, t5, t4
	-[0x800073e0]:csrrs a2, fcsr, zero
	-[0x800073e4]:sd t6, 32(fp)
	-[0x800073e8]:sd a2, 40(fp)
Current Store : [0x800073e8] : sd a2, 40(fp) -- Store: [0x80014fd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0x00 and fm2 == 0x1bbb48 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007414]:fmax.s t6, t5, t4
	-[0x80007418]:csrrs a2, fcsr, zero
	-[0x8000741c]:sd t6, 48(fp)
	-[0x80007420]:sd a2, 56(fp)
Current Store : [0x80007420] : sd a2, 56(fp) -- Store: [0x80014fe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000744c]:fmax.s t6, t5, t4
	-[0x80007450]:csrrs a2, fcsr, zero
	-[0x80007454]:sd t6, 64(fp)
	-[0x80007458]:sd a2, 72(fp)
Current Store : [0x80007458] : sd a2, 72(fp) -- Store: [0x80014ff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007484]:fmax.s t6, t5, t4
	-[0x80007488]:csrrs a2, fcsr, zero
	-[0x8000748c]:sd t6, 80(fp)
	-[0x80007490]:sd a2, 88(fp)
Current Store : [0x80007490] : sd a2, 88(fp) -- Store: [0x80015000]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x1bbb48 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800074bc]:fmax.s t6, t5, t4
	-[0x800074c0]:csrrs a2, fcsr, zero
	-[0x800074c4]:sd t6, 96(fp)
	-[0x800074c8]:sd a2, 104(fp)
Current Store : [0x800074c8] : sd a2, 104(fp) -- Store: [0x80015010]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800074f4]:fmax.s t6, t5, t4
	-[0x800074f8]:csrrs a2, fcsr, zero
	-[0x800074fc]:sd t6, 112(fp)
	-[0x80007500]:sd a2, 120(fp)
Current Store : [0x80007500] : sd a2, 120(fp) -- Store: [0x80015020]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x80 and fm1 == 0x22fdd5 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000752c]:fmax.s t6, t5, t4
	-[0x80007530]:csrrs a2, fcsr, zero
	-[0x80007534]:sd t6, 128(fp)
	-[0x80007538]:sd a2, 136(fp)
Current Store : [0x80007538] : sd a2, 136(fp) -- Store: [0x80015030]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x22fdd5 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007564]:fmax.s t6, t5, t4
	-[0x80007568]:csrrs a2, fcsr, zero
	-[0x8000756c]:sd t6, 144(fp)
	-[0x80007570]:sd a2, 152(fp)
Current Store : [0x80007570] : sd a2, 152(fp) -- Store: [0x80015040]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x3f987b and fs2 == 1 and fe2 == 0x80 and fm2 == 0x22fdd5 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000759c]:fmax.s t6, t5, t4
	-[0x800075a0]:csrrs a2, fcsr, zero
	-[0x800075a4]:sd t6, 160(fp)
	-[0x800075a8]:sd a2, 168(fp)
Current Store : [0x800075a8] : sd a2, 168(fp) -- Store: [0x80015050]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800075d4]:fmax.s t6, t5, t4
	-[0x800075d8]:csrrs a2, fcsr, zero
	-[0x800075dc]:sd t6, 176(fp)
	-[0x800075e0]:sd a2, 184(fp)
Current Store : [0x800075e0] : sd a2, 184(fp) -- Store: [0x80015060]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000760c]:fmax.s t6, t5, t4
	-[0x80007610]:csrrs a2, fcsr, zero
	-[0x80007614]:sd t6, 192(fp)
	-[0x80007618]:sd a2, 200(fp)
Current Store : [0x80007618] : sd a2, 200(fp) -- Store: [0x80015070]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007644]:fmax.s t6, t5, t4
	-[0x80007648]:csrrs a2, fcsr, zero
	-[0x8000764c]:sd t6, 208(fp)
	-[0x80007650]:sd a2, 216(fp)
Current Store : [0x80007650] : sd a2, 216(fp) -- Store: [0x80015080]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfd and fm2 == 0x522917 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000767c]:fmax.s t6, t5, t4
	-[0x80007680]:csrrs a2, fcsr, zero
	-[0x80007684]:sd t6, 224(fp)
	-[0x80007688]:sd a2, 232(fp)
Current Store : [0x80007688] : sd a2, 232(fp) -- Store: [0x80015090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800076b4]:fmax.s t6, t5, t4
	-[0x800076b8]:csrrs a2, fcsr, zero
	-[0x800076bc]:sd t6, 240(fp)
	-[0x800076c0]:sd a2, 248(fp)
Current Store : [0x800076c0] : sd a2, 248(fp) -- Store: [0x800150a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800076ec]:fmax.s t6, t5, t4
	-[0x800076f0]:csrrs a2, fcsr, zero
	-[0x800076f4]:sd t6, 256(fp)
	-[0x800076f8]:sd a2, 264(fp)
Current Store : [0x800076f8] : sd a2, 264(fp) -- Store: [0x800150b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007724]:fmax.s t6, t5, t4
	-[0x80007728]:csrrs a2, fcsr, zero
	-[0x8000772c]:sd t6, 272(fp)
	-[0x80007730]:sd a2, 280(fp)
Current Store : [0x80007730] : sd a2, 280(fp) -- Store: [0x800150c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000775c]:fmax.s t6, t5, t4
	-[0x80007760]:csrrs a2, fcsr, zero
	-[0x80007764]:sd t6, 288(fp)
	-[0x80007768]:sd a2, 296(fp)
Current Store : [0x80007768] : sd a2, 296(fp) -- Store: [0x800150d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007794]:fmax.s t6, t5, t4
	-[0x80007798]:csrrs a2, fcsr, zero
	-[0x8000779c]:sd t6, 304(fp)
	-[0x800077a0]:sd a2, 312(fp)
Current Store : [0x800077a0] : sd a2, 312(fp) -- Store: [0x800150e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800077cc]:fmax.s t6, t5, t4
	-[0x800077d0]:csrrs a2, fcsr, zero
	-[0x800077d4]:sd t6, 320(fp)
	-[0x800077d8]:sd a2, 328(fp)
Current Store : [0x800077d8] : sd a2, 328(fp) -- Store: [0x800150f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007804]:fmax.s t6, t5, t4
	-[0x80007808]:csrrs a2, fcsr, zero
	-[0x8000780c]:sd t6, 336(fp)
	-[0x80007810]:sd a2, 344(fp)
Current Store : [0x80007810] : sd a2, 344(fp) -- Store: [0x80015100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x522917 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000783c]:fmax.s t6, t5, t4
	-[0x80007840]:csrrs a2, fcsr, zero
	-[0x80007844]:sd t6, 352(fp)
	-[0x80007848]:sd a2, 360(fp)
Current Store : [0x80007848] : sd a2, 360(fp) -- Store: [0x80015110]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007874]:fmax.s t6, t5, t4
	-[0x80007878]:csrrs a2, fcsr, zero
	-[0x8000787c]:sd t6, 368(fp)
	-[0x80007880]:sd a2, 376(fp)
Current Store : [0x80007880] : sd a2, 376(fp) -- Store: [0x80015120]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800078ac]:fmax.s t6, t5, t4
	-[0x800078b0]:csrrs a2, fcsr, zero
	-[0x800078b4]:sd t6, 384(fp)
	-[0x800078b8]:sd a2, 392(fp)
Current Store : [0x800078b8] : sd a2, 392(fp) -- Store: [0x80015130]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800078e4]:fmax.s t6, t5, t4
	-[0x800078e8]:csrrs a2, fcsr, zero
	-[0x800078ec]:sd t6, 400(fp)
	-[0x800078f0]:sd a2, 408(fp)
Current Store : [0x800078f0] : sd a2, 408(fp) -- Store: [0x80015140]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000791c]:fmax.s t6, t5, t4
	-[0x80007920]:csrrs a2, fcsr, zero
	-[0x80007924]:sd t6, 416(fp)
	-[0x80007928]:sd a2, 424(fp)
Current Store : [0x80007928] : sd a2, 424(fp) -- Store: [0x80015150]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007954]:fmax.s t6, t5, t4
	-[0x80007958]:csrrs a2, fcsr, zero
	-[0x8000795c]:sd t6, 432(fp)
	-[0x80007960]:sd a2, 440(fp)
Current Store : [0x80007960] : sd a2, 440(fp) -- Store: [0x80015160]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000798c]:fmax.s t6, t5, t4
	-[0x80007990]:csrrs a2, fcsr, zero
	-[0x80007994]:sd t6, 448(fp)
	-[0x80007998]:sd a2, 456(fp)
Current Store : [0x80007998] : sd a2, 456(fp) -- Store: [0x80015170]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800079c4]:fmax.s t6, t5, t4
	-[0x800079c8]:csrrs a2, fcsr, zero
	-[0x800079cc]:sd t6, 464(fp)
	-[0x800079d0]:sd a2, 472(fp)
Current Store : [0x800079d0] : sd a2, 472(fp) -- Store: [0x80015180]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800079fc]:fmax.s t6, t5, t4
	-[0x80007a00]:csrrs a2, fcsr, zero
	-[0x80007a04]:sd t6, 480(fp)
	-[0x80007a08]:sd a2, 488(fp)
Current Store : [0x80007a08] : sd a2, 488(fp) -- Store: [0x80015190]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007a34]:fmax.s t6, t5, t4
	-[0x80007a38]:csrrs a2, fcsr, zero
	-[0x80007a3c]:sd t6, 496(fp)
	-[0x80007a40]:sd a2, 504(fp)
Current Store : [0x80007a40] : sd a2, 504(fp) -- Store: [0x800151a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007a6c]:fmax.s t6, t5, t4
	-[0x80007a70]:csrrs a2, fcsr, zero
	-[0x80007a74]:sd t6, 512(fp)
	-[0x80007a78]:sd a2, 520(fp)
Current Store : [0x80007a78] : sd a2, 520(fp) -- Store: [0x800151b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007aa4]:fmax.s t6, t5, t4
	-[0x80007aa8]:csrrs a2, fcsr, zero
	-[0x80007aac]:sd t6, 528(fp)
	-[0x80007ab0]:sd a2, 536(fp)
Current Store : [0x80007ab0] : sd a2, 536(fp) -- Store: [0x800151c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007adc]:fmax.s t6, t5, t4
	-[0x80007ae0]:csrrs a2, fcsr, zero
	-[0x80007ae4]:sd t6, 544(fp)
	-[0x80007ae8]:sd a2, 552(fp)
Current Store : [0x80007ae8] : sd a2, 552(fp) -- Store: [0x800151d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b14]:fmax.s t6, t5, t4
	-[0x80007b18]:csrrs a2, fcsr, zero
	-[0x80007b1c]:sd t6, 560(fp)
	-[0x80007b20]:sd a2, 568(fp)
Current Store : [0x80007b20] : sd a2, 568(fp) -- Store: [0x800151e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b4c]:fmax.s t6, t5, t4
	-[0x80007b50]:csrrs a2, fcsr, zero
	-[0x80007b54]:sd t6, 576(fp)
	-[0x80007b58]:sd a2, 584(fp)
Current Store : [0x80007b58] : sd a2, 584(fp) -- Store: [0x800151f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007b84]:fmax.s t6, t5, t4
	-[0x80007b88]:csrrs a2, fcsr, zero
	-[0x80007b8c]:sd t6, 592(fp)
	-[0x80007b90]:sd a2, 600(fp)
Current Store : [0x80007b90] : sd a2, 600(fp) -- Store: [0x80015200]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007bbc]:fmax.s t6, t5, t4
	-[0x80007bc0]:csrrs a2, fcsr, zero
	-[0x80007bc4]:sd t6, 608(fp)
	-[0x80007bc8]:sd a2, 616(fp)
Current Store : [0x80007bc8] : sd a2, 616(fp) -- Store: [0x80015210]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007bf4]:fmax.s t6, t5, t4
	-[0x80007bf8]:csrrs a2, fcsr, zero
	-[0x80007bfc]:sd t6, 624(fp)
	-[0x80007c00]:sd a2, 632(fp)
Current Store : [0x80007c00] : sd a2, 632(fp) -- Store: [0x80015220]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c2c]:fmax.s t6, t5, t4
	-[0x80007c30]:csrrs a2, fcsr, zero
	-[0x80007c34]:sd t6, 640(fp)
	-[0x80007c38]:sd a2, 648(fp)
Current Store : [0x80007c38] : sd a2, 648(fp) -- Store: [0x80015230]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007c64]:fmax.s t6, t5, t4
	-[0x80007c68]:csrrs a2, fcsr, zero
	-[0x80007c6c]:sd t6, 656(fp)
	-[0x80007c70]:sd a2, 664(fp)
Current Store : [0x80007c70] : sd a2, 664(fp) -- Store: [0x80015240]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007c9c]:fmax.s t6, t5, t4
	-[0x80007ca0]:csrrs a2, fcsr, zero
	-[0x80007ca4]:sd t6, 672(fp)
	-[0x80007ca8]:sd a2, 680(fp)
Current Store : [0x80007ca8] : sd a2, 680(fp) -- Store: [0x80015250]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80007cd4]:fmax.s t6, t5, t4
	-[0x80007cd8]:csrrs a2, fcsr, zero
	-[0x80007cdc]:sd t6, 688(fp)
	-[0x80007ce0]:sd a2, 696(fp)
Current Store : [0x80007ce0] : sd a2, 696(fp) -- Store: [0x80015260]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007d0c]:fmax.s t6, t5, t4
	-[0x80007d10]:csrrs a2, fcsr, zero
	-[0x80007d14]:sd t6, 704(fp)
	-[0x80007d18]:sd a2, 712(fp)
Current Store : [0x80007d18] : sd a2, 712(fp) -- Store: [0x80015270]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007d44]:fmax.s t6, t5, t4
	-[0x80007d48]:csrrs a2, fcsr, zero
	-[0x80007d4c]:sd t6, 720(fp)
	-[0x80007d50]:sd a2, 728(fp)
Current Store : [0x80007d50] : sd a2, 728(fp) -- Store: [0x80015280]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007d7c]:fmax.s t6, t5, t4
	-[0x80007d80]:csrrs a2, fcsr, zero
	-[0x80007d84]:sd t6, 736(fp)
	-[0x80007d88]:sd a2, 744(fp)
Current Store : [0x80007d88] : sd a2, 744(fp) -- Store: [0x80015290]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007db4]:fmax.s t6, t5, t4
	-[0x80007db8]:csrrs a2, fcsr, zero
	-[0x80007dbc]:sd t6, 752(fp)
	-[0x80007dc0]:sd a2, 760(fp)
Current Store : [0x80007dc0] : sd a2, 760(fp) -- Store: [0x800152a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007dec]:fmax.s t6, t5, t4
	-[0x80007df0]:csrrs a2, fcsr, zero
	-[0x80007df4]:sd t6, 768(fp)
	-[0x80007df8]:sd a2, 776(fp)
Current Store : [0x80007df8] : sd a2, 776(fp) -- Store: [0x800152b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007e24]:fmax.s t6, t5, t4
	-[0x80007e28]:csrrs a2, fcsr, zero
	-[0x80007e2c]:sd t6, 784(fp)
	-[0x80007e30]:sd a2, 792(fp)
Current Store : [0x80007e30] : sd a2, 792(fp) -- Store: [0x800152c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007e5c]:fmax.s t6, t5, t4
	-[0x80007e60]:csrrs a2, fcsr, zero
	-[0x80007e64]:sd t6, 800(fp)
	-[0x80007e68]:sd a2, 808(fp)
Current Store : [0x80007e68] : sd a2, 808(fp) -- Store: [0x800152d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007e94]:fmax.s t6, t5, t4
	-[0x80007e98]:csrrs a2, fcsr, zero
	-[0x80007e9c]:sd t6, 816(fp)
	-[0x80007ea0]:sd a2, 824(fp)
Current Store : [0x80007ea0] : sd a2, 824(fp) -- Store: [0x800152e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007ecc]:fmax.s t6, t5, t4
	-[0x80007ed0]:csrrs a2, fcsr, zero
	-[0x80007ed4]:sd t6, 832(fp)
	-[0x80007ed8]:sd a2, 840(fp)
Current Store : [0x80007ed8] : sd a2, 840(fp) -- Store: [0x800152f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007f04]:fmax.s t6, t5, t4
	-[0x80007f08]:csrrs a2, fcsr, zero
	-[0x80007f0c]:sd t6, 848(fp)
	-[0x80007f10]:sd a2, 856(fp)
Current Store : [0x80007f10] : sd a2, 856(fp) -- Store: [0x80015300]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007f3c]:fmax.s t6, t5, t4
	-[0x80007f40]:csrrs a2, fcsr, zero
	-[0x80007f44]:sd t6, 864(fp)
	-[0x80007f48]:sd a2, 872(fp)
Current Store : [0x80007f48] : sd a2, 872(fp) -- Store: [0x80015310]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007f74]:fmax.s t6, t5, t4
	-[0x80007f78]:csrrs a2, fcsr, zero
	-[0x80007f7c]:sd t6, 880(fp)
	-[0x80007f80]:sd a2, 888(fp)
Current Store : [0x80007f80] : sd a2, 888(fp) -- Store: [0x80015320]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007fac]:fmax.s t6, t5, t4
	-[0x80007fb0]:csrrs a2, fcsr, zero
	-[0x80007fb4]:sd t6, 896(fp)
	-[0x80007fb8]:sd a2, 904(fp)
Current Store : [0x80007fb8] : sd a2, 904(fp) -- Store: [0x80015330]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80007fe4]:fmax.s t6, t5, t4
	-[0x80007fe8]:csrrs a2, fcsr, zero
	-[0x80007fec]:sd t6, 912(fp)
	-[0x80007ff0]:sd a2, 920(fp)
Current Store : [0x80007ff0] : sd a2, 920(fp) -- Store: [0x80015340]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000801c]:fmax.s t6, t5, t4
	-[0x80008020]:csrrs a2, fcsr, zero
	-[0x80008024]:sd t6, 928(fp)
	-[0x80008028]:sd a2, 936(fp)
Current Store : [0x80008028] : sd a2, 936(fp) -- Store: [0x80015350]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008054]:fmax.s t6, t5, t4
	-[0x80008058]:csrrs a2, fcsr, zero
	-[0x8000805c]:sd t6, 944(fp)
	-[0x80008060]:sd a2, 952(fp)
Current Store : [0x80008060] : sd a2, 952(fp) -- Store: [0x80015360]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000808c]:fmax.s t6, t5, t4
	-[0x80008090]:csrrs a2, fcsr, zero
	-[0x80008094]:sd t6, 960(fp)
	-[0x80008098]:sd a2, 968(fp)
Current Store : [0x80008098] : sd a2, 968(fp) -- Store: [0x80015370]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800080c4]:fmax.s t6, t5, t4
	-[0x800080c8]:csrrs a2, fcsr, zero
	-[0x800080cc]:sd t6, 976(fp)
	-[0x800080d0]:sd a2, 984(fp)
Current Store : [0x800080d0] : sd a2, 984(fp) -- Store: [0x80015380]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0f3596 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800080fc]:fmax.s t6, t5, t4
	-[0x80008100]:csrrs a2, fcsr, zero
	-[0x80008104]:sd t6, 992(fp)
	-[0x80008108]:sd a2, 1000(fp)
Current Store : [0x80008108] : sd a2, 1000(fp) -- Store: [0x80015390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0f3596 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008134]:fmax.s t6, t5, t4
	-[0x80008138]:csrrs a2, fcsr, zero
	-[0x8000813c]:sd t6, 1008(fp)
	-[0x80008140]:sd a2, 1016(fp)
Current Store : [0x80008140] : sd a2, 1016(fp) -- Store: [0x800153a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000816c]:fmax.s t6, t5, t4
	-[0x80008170]:csrrs a2, fcsr, zero
	-[0x80008174]:sd t6, 1024(fp)
	-[0x80008178]:sd a2, 1032(fp)
Current Store : [0x80008178] : sd a2, 1032(fp) -- Store: [0x800153b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x32c8e8 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800081a4]:fmax.s t6, t5, t4
	-[0x800081a8]:csrrs a2, fcsr, zero
	-[0x800081ac]:sd t6, 1040(fp)
	-[0x800081b0]:sd a2, 1048(fp)
Current Store : [0x800081b0] : sd a2, 1048(fp) -- Store: [0x800153c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x32c8e8 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800081dc]:fmax.s t6, t5, t4
	-[0x800081e0]:csrrs a2, fcsr, zero
	-[0x800081e4]:sd t6, 1056(fp)
	-[0x800081e8]:sd a2, 1064(fp)
Current Store : [0x800081e8] : sd a2, 1064(fp) -- Store: [0x800153d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x2820df and fs2 == 1 and fe2 == 0x7f and fm2 == 0x32c8e8 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008214]:fmax.s t6, t5, t4
	-[0x80008218]:csrrs a2, fcsr, zero
	-[0x8000821c]:sd t6, 1072(fp)
	-[0x80008220]:sd a2, 1080(fp)
Current Store : [0x80008220] : sd a2, 1080(fp) -- Store: [0x800153e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000824c]:fmax.s t6, t5, t4
	-[0x80008250]:csrrs a2, fcsr, zero
	-[0x80008254]:sd t6, 1088(fp)
	-[0x80008258]:sd a2, 1096(fp)
Current Store : [0x80008258] : sd a2, 1096(fp) -- Store: [0x800153f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008284]:fmax.s t6, t5, t4
	-[0x80008288]:csrrs a2, fcsr, zero
	-[0x8000828c]:sd t6, 1104(fp)
	-[0x80008290]:sd a2, 1112(fp)
Current Store : [0x80008290] : sd a2, 1112(fp) -- Store: [0x80015400]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfc and fm2 == 0x07167c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800082bc]:fmax.s t6, t5, t4
	-[0x800082c0]:csrrs a2, fcsr, zero
	-[0x800082c4]:sd t6, 1120(fp)
	-[0x800082c8]:sd a2, 1128(fp)
Current Store : [0x800082c8] : sd a2, 1128(fp) -- Store: [0x80015410]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800082f4]:fmax.s t6, t5, t4
	-[0x800082f8]:csrrs a2, fcsr, zero
	-[0x800082fc]:sd t6, 1136(fp)
	-[0x80008300]:sd a2, 1144(fp)
Current Store : [0x80008300] : sd a2, 1144(fp) -- Store: [0x80015420]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000832c]:fmax.s t6, t5, t4
	-[0x80008330]:csrrs a2, fcsr, zero
	-[0x80008334]:sd t6, 1152(fp)
	-[0x80008338]:sd a2, 1160(fp)
Current Store : [0x80008338] : sd a2, 1160(fp) -- Store: [0x80015430]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008364]:fmax.s t6, t5, t4
	-[0x80008368]:csrrs a2, fcsr, zero
	-[0x8000836c]:sd t6, 1168(fp)
	-[0x80008370]:sd a2, 1176(fp)
Current Store : [0x80008370] : sd a2, 1176(fp) -- Store: [0x80015440]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000839c]:fmax.s t6, t5, t4
	-[0x800083a0]:csrrs a2, fcsr, zero
	-[0x800083a4]:sd t6, 1184(fp)
	-[0x800083a8]:sd a2, 1192(fp)
Current Store : [0x800083a8] : sd a2, 1192(fp) -- Store: [0x80015450]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800083d4]:fmax.s t6, t5, t4
	-[0x800083d8]:csrrs a2, fcsr, zero
	-[0x800083dc]:sd t6, 1200(fp)
	-[0x800083e0]:sd a2, 1208(fp)
Current Store : [0x800083e0] : sd a2, 1208(fp) -- Store: [0x80015460]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000840c]:fmax.s t6, t5, t4
	-[0x80008410]:csrrs a2, fcsr, zero
	-[0x80008414]:sd t6, 1216(fp)
	-[0x80008418]:sd a2, 1224(fp)
Current Store : [0x80008418] : sd a2, 1224(fp) -- Store: [0x80015470]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008444]:fmax.s t6, t5, t4
	-[0x80008448]:csrrs a2, fcsr, zero
	-[0x8000844c]:sd t6, 1232(fp)
	-[0x80008450]:sd a2, 1240(fp)
Current Store : [0x80008450] : sd a2, 1240(fp) -- Store: [0x80015480]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000847c]:fmax.s t6, t5, t4
	-[0x80008480]:csrrs a2, fcsr, zero
	-[0x80008484]:sd t6, 1248(fp)
	-[0x80008488]:sd a2, 1256(fp)
Current Store : [0x80008488] : sd a2, 1256(fp) -- Store: [0x80015490]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800084b4]:fmax.s t6, t5, t4
	-[0x800084b8]:csrrs a2, fcsr, zero
	-[0x800084bc]:sd t6, 1264(fp)
	-[0x800084c0]:sd a2, 1272(fp)
Current Store : [0x800084c0] : sd a2, 1272(fp) -- Store: [0x800154a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800084ec]:fmax.s t6, t5, t4
	-[0x800084f0]:csrrs a2, fcsr, zero
	-[0x800084f4]:sd t6, 1280(fp)
	-[0x800084f8]:sd a2, 1288(fp)
Current Store : [0x800084f8] : sd a2, 1288(fp) -- Store: [0x800154b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008524]:fmax.s t6, t5, t4
	-[0x80008528]:csrrs a2, fcsr, zero
	-[0x8000852c]:sd t6, 1296(fp)
	-[0x80008530]:sd a2, 1304(fp)
Current Store : [0x80008530] : sd a2, 1304(fp) -- Store: [0x800154c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000855c]:fmax.s t6, t5, t4
	-[0x80008560]:csrrs a2, fcsr, zero
	-[0x80008564]:sd t6, 1312(fp)
	-[0x80008568]:sd a2, 1320(fp)
Current Store : [0x80008568] : sd a2, 1320(fp) -- Store: [0x800154d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008594]:fmax.s t6, t5, t4
	-[0x80008598]:csrrs a2, fcsr, zero
	-[0x8000859c]:sd t6, 1328(fp)
	-[0x800085a0]:sd a2, 1336(fp)
Current Store : [0x800085a0] : sd a2, 1336(fp) -- Store: [0x800154e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800085cc]:fmax.s t6, t5, t4
	-[0x800085d0]:csrrs a2, fcsr, zero
	-[0x800085d4]:sd t6, 1344(fp)
	-[0x800085d8]:sd a2, 1352(fp)
Current Store : [0x800085d8] : sd a2, 1352(fp) -- Store: [0x800154f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008604]:fmax.s t6, t5, t4
	-[0x80008608]:csrrs a2, fcsr, zero
	-[0x8000860c]:sd t6, 1360(fp)
	-[0x80008610]:sd a2, 1368(fp)
Current Store : [0x80008610] : sd a2, 1368(fp) -- Store: [0x80015500]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000863c]:fmax.s t6, t5, t4
	-[0x80008640]:csrrs a2, fcsr, zero
	-[0x80008644]:sd t6, 1376(fp)
	-[0x80008648]:sd a2, 1384(fp)
Current Store : [0x80008648] : sd a2, 1384(fp) -- Store: [0x80015510]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008674]:fmax.s t6, t5, t4
	-[0x80008678]:csrrs a2, fcsr, zero
	-[0x8000867c]:sd t6, 1392(fp)
	-[0x80008680]:sd a2, 1400(fp)
Current Store : [0x80008680] : sd a2, 1400(fp) -- Store: [0x80015520]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800086ac]:fmax.s t6, t5, t4
	-[0x800086b0]:csrrs a2, fcsr, zero
	-[0x800086b4]:sd t6, 1408(fp)
	-[0x800086b8]:sd a2, 1416(fp)
Current Store : [0x800086b8] : sd a2, 1416(fp) -- Store: [0x80015530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800086e4]:fmax.s t6, t5, t4
	-[0x800086e8]:csrrs a2, fcsr, zero
	-[0x800086ec]:sd t6, 1424(fp)
	-[0x800086f0]:sd a2, 1432(fp)
Current Store : [0x800086f0] : sd a2, 1432(fp) -- Store: [0x80015540]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000871c]:fmax.s t6, t5, t4
	-[0x80008720]:csrrs a2, fcsr, zero
	-[0x80008724]:sd t6, 1440(fp)
	-[0x80008728]:sd a2, 1448(fp)
Current Store : [0x80008728] : sd a2, 1448(fp) -- Store: [0x80015550]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008754]:fmax.s t6, t5, t4
	-[0x80008758]:csrrs a2, fcsr, zero
	-[0x8000875c]:sd t6, 1456(fp)
	-[0x80008760]:sd a2, 1464(fp)
Current Store : [0x80008760] : sd a2, 1464(fp) -- Store: [0x80015560]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000878c]:fmax.s t6, t5, t4
	-[0x80008790]:csrrs a2, fcsr, zero
	-[0x80008794]:sd t6, 1472(fp)
	-[0x80008798]:sd a2, 1480(fp)
Current Store : [0x80008798] : sd a2, 1480(fp) -- Store: [0x80015570]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800087c4]:fmax.s t6, t5, t4
	-[0x800087c8]:csrrs a2, fcsr, zero
	-[0x800087cc]:sd t6, 1488(fp)
	-[0x800087d0]:sd a2, 1496(fp)
Current Store : [0x800087d0] : sd a2, 1496(fp) -- Store: [0x80015580]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800087fc]:fmax.s t6, t5, t4
	-[0x80008800]:csrrs a2, fcsr, zero
	-[0x80008804]:sd t6, 1504(fp)
	-[0x80008808]:sd a2, 1512(fp)
Current Store : [0x80008808] : sd a2, 1512(fp) -- Store: [0x80015590]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008834]:fmax.s t6, t5, t4
	-[0x80008838]:csrrs a2, fcsr, zero
	-[0x8000883c]:sd t6, 1520(fp)
	-[0x80008840]:sd a2, 1528(fp)
Current Store : [0x80008840] : sd a2, 1528(fp) -- Store: [0x800155a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000886c]:fmax.s t6, t5, t4
	-[0x80008870]:csrrs a2, fcsr, zero
	-[0x80008874]:sd t6, 1536(fp)
	-[0x80008878]:sd a2, 1544(fp)
Current Store : [0x80008878] : sd a2, 1544(fp) -- Store: [0x800155b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800088a4]:fmax.s t6, t5, t4
	-[0x800088a8]:csrrs a2, fcsr, zero
	-[0x800088ac]:sd t6, 1552(fp)
	-[0x800088b0]:sd a2, 1560(fp)
Current Store : [0x800088b0] : sd a2, 1560(fp) -- Store: [0x800155c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800088dc]:fmax.s t6, t5, t4
	-[0x800088e0]:csrrs a2, fcsr, zero
	-[0x800088e4]:sd t6, 1568(fp)
	-[0x800088e8]:sd a2, 1576(fp)
Current Store : [0x800088e8] : sd a2, 1576(fp) -- Store: [0x800155d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008914]:fmax.s t6, t5, t4
	-[0x80008918]:csrrs a2, fcsr, zero
	-[0x8000891c]:sd t6, 1584(fp)
	-[0x80008920]:sd a2, 1592(fp)
Current Store : [0x80008920] : sd a2, 1592(fp) -- Store: [0x800155e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000894c]:fmax.s t6, t5, t4
	-[0x80008950]:csrrs a2, fcsr, zero
	-[0x80008954]:sd t6, 1600(fp)
	-[0x80008958]:sd a2, 1608(fp)
Current Store : [0x80008958] : sd a2, 1608(fp) -- Store: [0x800155f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008984]:fmax.s t6, t5, t4
	-[0x80008988]:csrrs a2, fcsr, zero
	-[0x8000898c]:sd t6, 1616(fp)
	-[0x80008990]:sd a2, 1624(fp)
Current Store : [0x80008990] : sd a2, 1624(fp) -- Store: [0x80015600]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800089bc]:fmax.s t6, t5, t4
	-[0x800089c0]:csrrs a2, fcsr, zero
	-[0x800089c4]:sd t6, 1632(fp)
	-[0x800089c8]:sd a2, 1640(fp)
Current Store : [0x800089c8] : sd a2, 1640(fp) -- Store: [0x80015610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800089f4]:fmax.s t6, t5, t4
	-[0x800089f8]:csrrs a2, fcsr, zero
	-[0x800089fc]:sd t6, 1648(fp)
	-[0x80008a00]:sd a2, 1656(fp)
Current Store : [0x80008a00] : sd a2, 1656(fp) -- Store: [0x80015620]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008a2c]:fmax.s t6, t5, t4
	-[0x80008a30]:csrrs a2, fcsr, zero
	-[0x80008a34]:sd t6, 1664(fp)
	-[0x80008a38]:sd a2, 1672(fp)
Current Store : [0x80008a38] : sd a2, 1672(fp) -- Store: [0x80015630]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x0fa668 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008a64]:fmax.s t6, t5, t4
	-[0x80008a68]:csrrs a2, fcsr, zero
	-[0x80008a6c]:sd t6, 1680(fp)
	-[0x80008a70]:sd a2, 1688(fp)
Current Store : [0x80008a70] : sd a2, 1688(fp) -- Store: [0x80015640]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x0fa668 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008a9c]:fmax.s t6, t5, t4
	-[0x80008aa0]:csrrs a2, fcsr, zero
	-[0x80008aa4]:sd t6, 1696(fp)
	-[0x80008aa8]:sd a2, 1704(fp)
Current Store : [0x80008aa8] : sd a2, 1704(fp) -- Store: [0x80015650]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x30e1ae and fs2 == 0 and fe2 == 0x81 and fm2 == 0x0fa668 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008ad4]:fmax.s t6, t5, t4
	-[0x80008ad8]:csrrs a2, fcsr, zero
	-[0x80008adc]:sd t6, 1712(fp)
	-[0x80008ae0]:sd a2, 1720(fp)
Current Store : [0x80008ae0] : sd a2, 1720(fp) -- Store: [0x80015660]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008b0c]:fmax.s t6, t5, t4
	-[0x80008b10]:csrrs a2, fcsr, zero
	-[0x80008b14]:sd t6, 1728(fp)
	-[0x80008b18]:sd a2, 1736(fp)
Current Store : [0x80008b18] : sd a2, 1736(fp) -- Store: [0x80015670]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008b44]:fmax.s t6, t5, t4
	-[0x80008b48]:csrrs a2, fcsr, zero
	-[0x80008b4c]:sd t6, 1744(fp)
	-[0x80008b50]:sd a2, 1752(fp)
Current Store : [0x80008b50] : sd a2, 1752(fp) -- Store: [0x80015680]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x667e2a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008b7c]:fmax.s t6, t5, t4
	-[0x80008b80]:csrrs a2, fcsr, zero
	-[0x80008b84]:sd t6, 1760(fp)
	-[0x80008b88]:sd a2, 1768(fp)
Current Store : [0x80008b88] : sd a2, 1768(fp) -- Store: [0x80015690]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008bb4]:fmax.s t6, t5, t4
	-[0x80008bb8]:csrrs a2, fcsr, zero
	-[0x80008bbc]:sd t6, 1776(fp)
	-[0x80008bc0]:sd a2, 1784(fp)
Current Store : [0x80008bc0] : sd a2, 1784(fp) -- Store: [0x800156a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008bec]:fmax.s t6, t5, t4
	-[0x80008bf0]:csrrs a2, fcsr, zero
	-[0x80008bf4]:sd t6, 1792(fp)
	-[0x80008bf8]:sd a2, 1800(fp)
Current Store : [0x80008bf8] : sd a2, 1800(fp) -- Store: [0x800156b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008c24]:fmax.s t6, t5, t4
	-[0x80008c28]:csrrs a2, fcsr, zero
	-[0x80008c2c]:sd t6, 1808(fp)
	-[0x80008c30]:sd a2, 1816(fp)
Current Store : [0x80008c30] : sd a2, 1816(fp) -- Store: [0x800156c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008c5c]:fmax.s t6, t5, t4
	-[0x80008c60]:csrrs a2, fcsr, zero
	-[0x80008c64]:sd t6, 1824(fp)
	-[0x80008c68]:sd a2, 1832(fp)
Current Store : [0x80008c68] : sd a2, 1832(fp) -- Store: [0x800156d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008c94]:fmax.s t6, t5, t4
	-[0x80008c98]:csrrs a2, fcsr, zero
	-[0x80008c9c]:sd t6, 1840(fp)
	-[0x80008ca0]:sd a2, 1848(fp)
Current Store : [0x80008ca0] : sd a2, 1848(fp) -- Store: [0x800156e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008ccc]:fmax.s t6, t5, t4
	-[0x80008cd0]:csrrs a2, fcsr, zero
	-[0x80008cd4]:sd t6, 1856(fp)
	-[0x80008cd8]:sd a2, 1864(fp)
Current Store : [0x80008cd8] : sd a2, 1864(fp) -- Store: [0x800156f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008d04]:fmax.s t6, t5, t4
	-[0x80008d08]:csrrs a2, fcsr, zero
	-[0x80008d0c]:sd t6, 1872(fp)
	-[0x80008d10]:sd a2, 1880(fp)
Current Store : [0x80008d10] : sd a2, 1880(fp) -- Store: [0x80015700]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008d3c]:fmax.s t6, t5, t4
	-[0x80008d40]:csrrs a2, fcsr, zero
	-[0x80008d44]:sd t6, 1888(fp)
	-[0x80008d48]:sd a2, 1896(fp)
Current Store : [0x80008d48] : sd a2, 1896(fp) -- Store: [0x80015710]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008d74]:fmax.s t6, t5, t4
	-[0x80008d78]:csrrs a2, fcsr, zero
	-[0x80008d7c]:sd t6, 1904(fp)
	-[0x80008d80]:sd a2, 1912(fp)
Current Store : [0x80008d80] : sd a2, 1912(fp) -- Store: [0x80015720]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008dac]:fmax.s t6, t5, t4
	-[0x80008db0]:csrrs a2, fcsr, zero
	-[0x80008db4]:sd t6, 1920(fp)
	-[0x80008db8]:sd a2, 1928(fp)
Current Store : [0x80008db8] : sd a2, 1928(fp) -- Store: [0x80015730]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008de4]:fmax.s t6, t5, t4
	-[0x80008de8]:csrrs a2, fcsr, zero
	-[0x80008dec]:sd t6, 1936(fp)
	-[0x80008df0]:sd a2, 1944(fp)
Current Store : [0x80008df0] : sd a2, 1944(fp) -- Store: [0x80015740]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008e1c]:fmax.s t6, t5, t4
	-[0x80008e20]:csrrs a2, fcsr, zero
	-[0x80008e24]:sd t6, 1952(fp)
	-[0x80008e28]:sd a2, 1960(fp)
Current Store : [0x80008e28] : sd a2, 1960(fp) -- Store: [0x80015750]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008e54]:fmax.s t6, t5, t4
	-[0x80008e58]:csrrs a2, fcsr, zero
	-[0x80008e5c]:sd t6, 1968(fp)
	-[0x80008e60]:sd a2, 1976(fp)
Current Store : [0x80008e60] : sd a2, 1976(fp) -- Store: [0x80015760]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008e8c]:fmax.s t6, t5, t4
	-[0x80008e90]:csrrs a2, fcsr, zero
	-[0x80008e94]:sd t6, 1984(fp)
	-[0x80008e98]:sd a2, 1992(fp)
Current Store : [0x80008e98] : sd a2, 1992(fp) -- Store: [0x80015770]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x667e2a and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80008ecc]:fmax.s t6, t5, t4
	-[0x80008ed0]:csrrs a2, fcsr, zero
	-[0x80008ed4]:sd t6, 2000(fp)
	-[0x80008ed8]:sd a2, 2008(fp)
Current Store : [0x80008ed8] : sd a2, 2008(fp) -- Store: [0x80015780]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008f0c]:fmax.s t6, t5, t4
	-[0x80008f10]:csrrs a2, fcsr, zero
	-[0x80008f14]:sd t6, 2016(fp)
	-[0x80008f18]:sd a2, 2024(fp)
Current Store : [0x80008f18] : sd a2, 2024(fp) -- Store: [0x80015790]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008f4c]:fmax.s t6, t5, t4
	-[0x80008f50]:csrrs a2, fcsr, zero
	-[0x80008f54]:sd t6, 2032(fp)
	-[0x80008f58]:sd a2, 2040(fp)
Current Store : [0x80008f58] : sd a2, 2040(fp) -- Store: [0x800157a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008f94]:fmax.s t6, t5, t4
	-[0x80008f98]:csrrs a2, fcsr, zero
	-[0x80008f9c]:sd t6, 0(fp)
	-[0x80008fa0]:sd a2, 8(fp)
Current Store : [0x80008fa0] : sd a2, 8(fp) -- Store: [0x800157b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80008fd4]:fmax.s t6, t5, t4
	-[0x80008fd8]:csrrs a2, fcsr, zero
	-[0x80008fdc]:sd t6, 16(fp)
	-[0x80008fe0]:sd a2, 24(fp)
Current Store : [0x80008fe0] : sd a2, 24(fp) -- Store: [0x800157c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009014]:fmax.s t6, t5, t4
	-[0x80009018]:csrrs a2, fcsr, zero
	-[0x8000901c]:sd t6, 32(fp)
	-[0x80009020]:sd a2, 40(fp)
Current Store : [0x80009020] : sd a2, 40(fp) -- Store: [0x800157d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009054]:fmax.s t6, t5, t4
	-[0x80009058]:csrrs a2, fcsr, zero
	-[0x8000905c]:sd t6, 48(fp)
	-[0x80009060]:sd a2, 56(fp)
Current Store : [0x80009060] : sd a2, 56(fp) -- Store: [0x800157e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009094]:fmax.s t6, t5, t4
	-[0x80009098]:csrrs a2, fcsr, zero
	-[0x8000909c]:sd t6, 64(fp)
	-[0x800090a0]:sd a2, 72(fp)
Current Store : [0x800090a0] : sd a2, 72(fp) -- Store: [0x800157f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800090d4]:fmax.s t6, t5, t4
	-[0x800090d8]:csrrs a2, fcsr, zero
	-[0x800090dc]:sd t6, 80(fp)
	-[0x800090e0]:sd a2, 88(fp)
Current Store : [0x800090e0] : sd a2, 88(fp) -- Store: [0x80015800]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009114]:fmax.s t6, t5, t4
	-[0x80009118]:csrrs a2, fcsr, zero
	-[0x8000911c]:sd t6, 96(fp)
	-[0x80009120]:sd a2, 104(fp)
Current Store : [0x80009120] : sd a2, 104(fp) -- Store: [0x80015810]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009154]:fmax.s t6, t5, t4
	-[0x80009158]:csrrs a2, fcsr, zero
	-[0x8000915c]:sd t6, 112(fp)
	-[0x80009160]:sd a2, 120(fp)
Current Store : [0x80009160] : sd a2, 120(fp) -- Store: [0x80015820]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009194]:fmax.s t6, t5, t4
	-[0x80009198]:csrrs a2, fcsr, zero
	-[0x8000919c]:sd t6, 128(fp)
	-[0x800091a0]:sd a2, 136(fp)
Current Store : [0x800091a0] : sd a2, 136(fp) -- Store: [0x80015830]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800091d4]:fmax.s t6, t5, t4
	-[0x800091d8]:csrrs a2, fcsr, zero
	-[0x800091dc]:sd t6, 144(fp)
	-[0x800091e0]:sd a2, 152(fp)
Current Store : [0x800091e0] : sd a2, 152(fp) -- Store: [0x80015840]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009214]:fmax.s t6, t5, t4
	-[0x80009218]:csrrs a2, fcsr, zero
	-[0x8000921c]:sd t6, 160(fp)
	-[0x80009220]:sd a2, 168(fp)
Current Store : [0x80009220] : sd a2, 168(fp) -- Store: [0x80015850]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009254]:fmax.s t6, t5, t4
	-[0x80009258]:csrrs a2, fcsr, zero
	-[0x8000925c]:sd t6, 176(fp)
	-[0x80009260]:sd a2, 184(fp)
Current Store : [0x80009260] : sd a2, 184(fp) -- Store: [0x80015860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009294]:fmax.s t6, t5, t4
	-[0x80009298]:csrrs a2, fcsr, zero
	-[0x8000929c]:sd t6, 192(fp)
	-[0x800092a0]:sd a2, 200(fp)
Current Store : [0x800092a0] : sd a2, 200(fp) -- Store: [0x80015870]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800092d4]:fmax.s t6, t5, t4
	-[0x800092d8]:csrrs a2, fcsr, zero
	-[0x800092dc]:sd t6, 208(fp)
	-[0x800092e0]:sd a2, 216(fp)
Current Store : [0x800092e0] : sd a2, 216(fp) -- Store: [0x80015880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009314]:fmax.s t6, t5, t4
	-[0x80009318]:csrrs a2, fcsr, zero
	-[0x8000931c]:sd t6, 224(fp)
	-[0x80009320]:sd a2, 232(fp)
Current Store : [0x80009320] : sd a2, 232(fp) -- Store: [0x80015890]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x751a1e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009354]:fmax.s t6, t5, t4
	-[0x80009358]:csrrs a2, fcsr, zero
	-[0x8000935c]:sd t6, 240(fp)
	-[0x80009360]:sd a2, 248(fp)
Current Store : [0x80009360] : sd a2, 248(fp) -- Store: [0x800158a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x751a1e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009394]:fmax.s t6, t5, t4
	-[0x80009398]:csrrs a2, fcsr, zero
	-[0x8000939c]:sd t6, 256(fp)
	-[0x800093a0]:sd a2, 264(fp)
Current Store : [0x800093a0] : sd a2, 264(fp) -- Store: [0x800158b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x29b3b2 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x751a1e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800093d4]:fmax.s t6, t5, t4
	-[0x800093d8]:csrrs a2, fcsr, zero
	-[0x800093dc]:sd t6, 272(fp)
	-[0x800093e0]:sd a2, 280(fp)
Current Store : [0x800093e0] : sd a2, 280(fp) -- Store: [0x800158c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009414]:fmax.s t6, t5, t4
	-[0x80009418]:csrrs a2, fcsr, zero
	-[0x8000941c]:sd t6, 288(fp)
	-[0x80009420]:sd a2, 296(fp)
Current Store : [0x80009420] : sd a2, 296(fp) -- Store: [0x800158d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009454]:fmax.s t6, t5, t4
	-[0x80009458]:csrrs a2, fcsr, zero
	-[0x8000945c]:sd t6, 304(fp)
	-[0x80009460]:sd a2, 312(fp)
Current Store : [0x80009460] : sd a2, 312(fp) -- Store: [0x800158e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfc and fm2 == 0x13d219 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009494]:fmax.s t6, t5, t4
	-[0x80009498]:csrrs a2, fcsr, zero
	-[0x8000949c]:sd t6, 320(fp)
	-[0x800094a0]:sd a2, 328(fp)
Current Store : [0x800094a0] : sd a2, 328(fp) -- Store: [0x800158f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800094d4]:fmax.s t6, t5, t4
	-[0x800094d8]:csrrs a2, fcsr, zero
	-[0x800094dc]:sd t6, 336(fp)
	-[0x800094e0]:sd a2, 344(fp)
Current Store : [0x800094e0] : sd a2, 344(fp) -- Store: [0x80015900]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009514]:fmax.s t6, t5, t4
	-[0x80009518]:csrrs a2, fcsr, zero
	-[0x8000951c]:sd t6, 352(fp)
	-[0x80009520]:sd a2, 360(fp)
Current Store : [0x80009520] : sd a2, 360(fp) -- Store: [0x80015910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009554]:fmax.s t6, t5, t4
	-[0x80009558]:csrrs a2, fcsr, zero
	-[0x8000955c]:sd t6, 368(fp)
	-[0x80009560]:sd a2, 376(fp)
Current Store : [0x80009560] : sd a2, 376(fp) -- Store: [0x80015920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009594]:fmax.s t6, t5, t4
	-[0x80009598]:csrrs a2, fcsr, zero
	-[0x8000959c]:sd t6, 384(fp)
	-[0x800095a0]:sd a2, 392(fp)
Current Store : [0x800095a0] : sd a2, 392(fp) -- Store: [0x80015930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800095d4]:fmax.s t6, t5, t4
	-[0x800095d8]:csrrs a2, fcsr, zero
	-[0x800095dc]:sd t6, 400(fp)
	-[0x800095e0]:sd a2, 408(fp)
Current Store : [0x800095e0] : sd a2, 408(fp) -- Store: [0x80015940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009614]:fmax.s t6, t5, t4
	-[0x80009618]:csrrs a2, fcsr, zero
	-[0x8000961c]:sd t6, 416(fp)
	-[0x80009620]:sd a2, 424(fp)
Current Store : [0x80009620] : sd a2, 424(fp) -- Store: [0x80015950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009654]:fmax.s t6, t5, t4
	-[0x80009658]:csrrs a2, fcsr, zero
	-[0x8000965c]:sd t6, 432(fp)
	-[0x80009660]:sd a2, 440(fp)
Current Store : [0x80009660] : sd a2, 440(fp) -- Store: [0x80015960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009694]:fmax.s t6, t5, t4
	-[0x80009698]:csrrs a2, fcsr, zero
	-[0x8000969c]:sd t6, 448(fp)
	-[0x800096a0]:sd a2, 456(fp)
Current Store : [0x800096a0] : sd a2, 456(fp) -- Store: [0x80015970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800096d4]:fmax.s t6, t5, t4
	-[0x800096d8]:csrrs a2, fcsr, zero
	-[0x800096dc]:sd t6, 464(fp)
	-[0x800096e0]:sd a2, 472(fp)
Current Store : [0x800096e0] : sd a2, 472(fp) -- Store: [0x80015980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009714]:fmax.s t6, t5, t4
	-[0x80009718]:csrrs a2, fcsr, zero
	-[0x8000971c]:sd t6, 480(fp)
	-[0x80009720]:sd a2, 488(fp)
Current Store : [0x80009720] : sd a2, 488(fp) -- Store: [0x80015990]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009754]:fmax.s t6, t5, t4
	-[0x80009758]:csrrs a2, fcsr, zero
	-[0x8000975c]:sd t6, 496(fp)
	-[0x80009760]:sd a2, 504(fp)
Current Store : [0x80009760] : sd a2, 504(fp) -- Store: [0x800159a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009794]:fmax.s t6, t5, t4
	-[0x80009798]:csrrs a2, fcsr, zero
	-[0x8000979c]:sd t6, 512(fp)
	-[0x800097a0]:sd a2, 520(fp)
Current Store : [0x800097a0] : sd a2, 520(fp) -- Store: [0x800159b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x800097d4]:fmax.s t6, t5, t4
	-[0x800097d8]:csrrs a2, fcsr, zero
	-[0x800097dc]:sd t6, 528(fp)
	-[0x800097e0]:sd a2, 536(fp)
Current Store : [0x800097e0] : sd a2, 536(fp) -- Store: [0x800159c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009814]:fmax.s t6, t5, t4
	-[0x80009818]:csrrs a2, fcsr, zero
	-[0x8000981c]:sd t6, 544(fp)
	-[0x80009820]:sd a2, 552(fp)
Current Store : [0x80009820] : sd a2, 552(fp) -- Store: [0x800159d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x13d219 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009854]:fmax.s t6, t5, t4
	-[0x80009858]:csrrs a2, fcsr, zero
	-[0x8000985c]:sd t6, 560(fp)
	-[0x80009860]:sd a2, 568(fp)
Current Store : [0x80009860] : sd a2, 568(fp) -- Store: [0x800159e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009894]:fmax.s t6, t5, t4
	-[0x80009898]:csrrs a2, fcsr, zero
	-[0x8000989c]:sd t6, 576(fp)
	-[0x800098a0]:sd a2, 584(fp)
Current Store : [0x800098a0] : sd a2, 584(fp) -- Store: [0x800159f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800098d4]:fmax.s t6, t5, t4
	-[0x800098d8]:csrrs a2, fcsr, zero
	-[0x800098dc]:sd t6, 592(fp)
	-[0x800098e0]:sd a2, 600(fp)
Current Store : [0x800098e0] : sd a2, 600(fp) -- Store: [0x80015a00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009914]:fmax.s t6, t5, t4
	-[0x80009918]:csrrs a2, fcsr, zero
	-[0x8000991c]:sd t6, 608(fp)
	-[0x80009920]:sd a2, 616(fp)
Current Store : [0x80009920] : sd a2, 616(fp) -- Store: [0x80015a10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009954]:fmax.s t6, t5, t4
	-[0x80009958]:csrrs a2, fcsr, zero
	-[0x8000995c]:sd t6, 624(fp)
	-[0x80009960]:sd a2, 632(fp)
Current Store : [0x80009960] : sd a2, 632(fp) -- Store: [0x80015a20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009994]:fmax.s t6, t5, t4
	-[0x80009998]:csrrs a2, fcsr, zero
	-[0x8000999c]:sd t6, 640(fp)
	-[0x800099a0]:sd a2, 648(fp)
Current Store : [0x800099a0] : sd a2, 648(fp) -- Store: [0x80015a30]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x800099d4]:fmax.s t6, t5, t4
	-[0x800099d8]:csrrs a2, fcsr, zero
	-[0x800099dc]:sd t6, 656(fp)
	-[0x800099e0]:sd a2, 664(fp)
Current Store : [0x800099e0] : sd a2, 664(fp) -- Store: [0x80015a40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009a14]:fmax.s t6, t5, t4
	-[0x80009a18]:csrrs a2, fcsr, zero
	-[0x80009a1c]:sd t6, 672(fp)
	-[0x80009a20]:sd a2, 680(fp)
Current Store : [0x80009a20] : sd a2, 680(fp) -- Store: [0x80015a50]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009a54]:fmax.s t6, t5, t4
	-[0x80009a58]:csrrs a2, fcsr, zero
	-[0x80009a5c]:sd t6, 688(fp)
	-[0x80009a60]:sd a2, 696(fp)
Current Store : [0x80009a60] : sd a2, 696(fp) -- Store: [0x80015a60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009a94]:fmax.s t6, t5, t4
	-[0x80009a98]:csrrs a2, fcsr, zero
	-[0x80009a9c]:sd t6, 704(fp)
	-[0x80009aa0]:sd a2, 712(fp)
Current Store : [0x80009aa0] : sd a2, 712(fp) -- Store: [0x80015a70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009ad4]:fmax.s t6, t5, t4
	-[0x80009ad8]:csrrs a2, fcsr, zero
	-[0x80009adc]:sd t6, 720(fp)
	-[0x80009ae0]:sd a2, 728(fp)
Current Store : [0x80009ae0] : sd a2, 728(fp) -- Store: [0x80015a80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009b14]:fmax.s t6, t5, t4
	-[0x80009b18]:csrrs a2, fcsr, zero
	-[0x80009b1c]:sd t6, 736(fp)
	-[0x80009b20]:sd a2, 744(fp)
Current Store : [0x80009b20] : sd a2, 744(fp) -- Store: [0x80015a90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009b54]:fmax.s t6, t5, t4
	-[0x80009b58]:csrrs a2, fcsr, zero
	-[0x80009b5c]:sd t6, 752(fp)
	-[0x80009b60]:sd a2, 760(fp)
Current Store : [0x80009b60] : sd a2, 760(fp) -- Store: [0x80015aa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x80009b94]:fmax.s t6, t5, t4
	-[0x80009b98]:csrrs a2, fcsr, zero
	-[0x80009b9c]:sd t6, 768(fp)
	-[0x80009ba0]:sd a2, 776(fp)
Current Store : [0x80009ba0] : sd a2, 776(fp) -- Store: [0x80015ab0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009bd4]:fmax.s t6, t5, t4
	-[0x80009bd8]:csrrs a2, fcsr, zero
	-[0x80009bdc]:sd t6, 784(fp)
	-[0x80009be0]:sd a2, 792(fp)
Current Store : [0x80009be0] : sd a2, 792(fp) -- Store: [0x80015ac0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009c14]:fmax.s t6, t5, t4
	-[0x80009c18]:csrrs a2, fcsr, zero
	-[0x80009c1c]:sd t6, 800(fp)
	-[0x80009c20]:sd a2, 808(fp)
Current Store : [0x80009c20] : sd a2, 808(fp) -- Store: [0x80015ad0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x1d309f and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009c54]:fmax.s t6, t5, t4
	-[0x80009c58]:csrrs a2, fcsr, zero
	-[0x80009c5c]:sd t6, 816(fp)
	-[0x80009c60]:sd a2, 824(fp)
Current Store : [0x80009c60] : sd a2, 824(fp) -- Store: [0x80015ae0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x1d309f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009c94]:fmax.s t6, t5, t4
	-[0x80009c98]:csrrs a2, fcsr, zero
	-[0x80009c9c]:sd t6, 832(fp)
	-[0x80009ca0]:sd a2, 840(fp)
Current Store : [0x80009ca0] : sd a2, 840(fp) -- Store: [0x80015af0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x357d2c and fs2 == 0 and fe2 == 0x81 and fm2 == 0x1d309f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009cd4]:fmax.s t6, t5, t4
	-[0x80009cd8]:csrrs a2, fcsr, zero
	-[0x80009cdc]:sd t6, 848(fp)
	-[0x80009ce0]:sd a2, 856(fp)
Current Store : [0x80009ce0] : sd a2, 856(fp) -- Store: [0x80015b00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009d14]:fmax.s t6, t5, t4
	-[0x80009d18]:csrrs a2, fcsr, zero
	-[0x80009d1c]:sd t6, 864(fp)
	-[0x80009d20]:sd a2, 872(fp)
Current Store : [0x80009d20] : sd a2, 872(fp) -- Store: [0x80015b10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009d54]:fmax.s t6, t5, t4
	-[0x80009d58]:csrrs a2, fcsr, zero
	-[0x80009d5c]:sd t6, 880(fp)
	-[0x80009d60]:sd a2, 888(fp)
Current Store : [0x80009d60] : sd a2, 888(fp) -- Store: [0x80015b20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x1d8cd6 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009d94]:fmax.s t6, t5, t4
	-[0x80009d98]:csrrs a2, fcsr, zero
	-[0x80009d9c]:sd t6, 896(fp)
	-[0x80009da0]:sd a2, 904(fp)
Current Store : [0x80009da0] : sd a2, 904(fp) -- Store: [0x80015b30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009dd4]:fmax.s t6, t5, t4
	-[0x80009dd8]:csrrs a2, fcsr, zero
	-[0x80009ddc]:sd t6, 912(fp)
	-[0x80009de0]:sd a2, 920(fp)
Current Store : [0x80009de0] : sd a2, 920(fp) -- Store: [0x80015b40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009e14]:fmax.s t6, t5, t4
	-[0x80009e18]:csrrs a2, fcsr, zero
	-[0x80009e1c]:sd t6, 928(fp)
	-[0x80009e20]:sd a2, 936(fp)
Current Store : [0x80009e20] : sd a2, 936(fp) -- Store: [0x80015b50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009e54]:fmax.s t6, t5, t4
	-[0x80009e58]:csrrs a2, fcsr, zero
	-[0x80009e5c]:sd t6, 944(fp)
	-[0x80009e60]:sd a2, 952(fp)
Current Store : [0x80009e60] : sd a2, 952(fp) -- Store: [0x80015b60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x44f00b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009e94]:fmax.s t6, t5, t4
	-[0x80009e98]:csrrs a2, fcsr, zero
	-[0x80009e9c]:sd t6, 960(fp)
	-[0x80009ea0]:sd a2, 968(fp)
Current Store : [0x80009ea0] : sd a2, 968(fp) -- Store: [0x80015b70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009ed4]:fmax.s t6, t5, t4
	-[0x80009ed8]:csrrs a2, fcsr, zero
	-[0x80009edc]:sd t6, 976(fp)
	-[0x80009ee0]:sd a2, 984(fp)
Current Store : [0x80009ee0] : sd a2, 984(fp) -- Store: [0x80015b80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009f14]:fmax.s t6, t5, t4
	-[0x80009f18]:csrrs a2, fcsr, zero
	-[0x80009f1c]:sd t6, 992(fp)
	-[0x80009f20]:sd a2, 1000(fp)
Current Store : [0x80009f20] : sd a2, 1000(fp) -- Store: [0x80015b90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009f54]:fmax.s t6, t5, t4
	-[0x80009f58]:csrrs a2, fcsr, zero
	-[0x80009f5c]:sd t6, 1008(fp)
	-[0x80009f60]:sd a2, 1016(fp)
Current Store : [0x80009f60] : sd a2, 1016(fp) -- Store: [0x80015ba0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009f94]:fmax.s t6, t5, t4
	-[0x80009f98]:csrrs a2, fcsr, zero
	-[0x80009f9c]:sd t6, 1024(fp)
	-[0x80009fa0]:sd a2, 1032(fp)
Current Store : [0x80009fa0] : sd a2, 1032(fp) -- Store: [0x80015bb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x80009fd4]:fmax.s t6, t5, t4
	-[0x80009fd8]:csrrs a2, fcsr, zero
	-[0x80009fdc]:sd t6, 1040(fp)
	-[0x80009fe0]:sd a2, 1048(fp)
Current Store : [0x80009fe0] : sd a2, 1048(fp) -- Store: [0x80015bc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a014]:fmax.s t6, t5, t4
	-[0x8000a018]:csrrs a2, fcsr, zero
	-[0x8000a01c]:sd t6, 1056(fp)
	-[0x8000a020]:sd a2, 1064(fp)
Current Store : [0x8000a020] : sd a2, 1064(fp) -- Store: [0x80015bd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a054]:fmax.s t6, t5, t4
	-[0x8000a058]:csrrs a2, fcsr, zero
	-[0x8000a05c]:sd t6, 1072(fp)
	-[0x8000a060]:sd a2, 1080(fp)
Current Store : [0x8000a060] : sd a2, 1080(fp) -- Store: [0x80015be0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a094]:fmax.s t6, t5, t4
	-[0x8000a098]:csrrs a2, fcsr, zero
	-[0x8000a09c]:sd t6, 1088(fp)
	-[0x8000a0a0]:sd a2, 1096(fp)
Current Store : [0x8000a0a0] : sd a2, 1096(fp) -- Store: [0x80015bf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a0d4]:fmax.s t6, t5, t4
	-[0x8000a0d8]:csrrs a2, fcsr, zero
	-[0x8000a0dc]:sd t6, 1104(fp)
	-[0x8000a0e0]:sd a2, 1112(fp)
Current Store : [0x8000a0e0] : sd a2, 1112(fp) -- Store: [0x80015c00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a114]:fmax.s t6, t5, t4
	-[0x8000a118]:csrrs a2, fcsr, zero
	-[0x8000a11c]:sd t6, 1120(fp)
	-[0x8000a120]:sd a2, 1128(fp)
Current Store : [0x8000a120] : sd a2, 1128(fp) -- Store: [0x80015c10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a154]:fmax.s t6, t5, t4
	-[0x8000a158]:csrrs a2, fcsr, zero
	-[0x8000a15c]:sd t6, 1136(fp)
	-[0x8000a160]:sd a2, 1144(fp)
Current Store : [0x8000a160] : sd a2, 1144(fp) -- Store: [0x80015c20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a194]:fmax.s t6, t5, t4
	-[0x8000a198]:csrrs a2, fcsr, zero
	-[0x8000a19c]:sd t6, 1152(fp)
	-[0x8000a1a0]:sd a2, 1160(fp)
Current Store : [0x8000a1a0] : sd a2, 1160(fp) -- Store: [0x80015c30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a1d4]:fmax.s t6, t5, t4
	-[0x8000a1d8]:csrrs a2, fcsr, zero
	-[0x8000a1dc]:sd t6, 1168(fp)
	-[0x8000a1e0]:sd a2, 1176(fp)
Current Store : [0x8000a1e0] : sd a2, 1176(fp) -- Store: [0x80015c40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x44f00b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a214]:fmax.s t6, t5, t4
	-[0x8000a218]:csrrs a2, fcsr, zero
	-[0x8000a21c]:sd t6, 1184(fp)
	-[0x8000a220]:sd a2, 1192(fp)
Current Store : [0x8000a220] : sd a2, 1192(fp) -- Store: [0x80015c50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a254]:fmax.s t6, t5, t4
	-[0x8000a258]:csrrs a2, fcsr, zero
	-[0x8000a25c]:sd t6, 1200(fp)
	-[0x8000a260]:sd a2, 1208(fp)
Current Store : [0x8000a260] : sd a2, 1208(fp) -- Store: [0x80015c60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x1d8cd6 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a294]:fmax.s t6, t5, t4
	-[0x8000a298]:csrrs a2, fcsr, zero
	-[0x8000a29c]:sd t6, 1216(fp)
	-[0x8000a2a0]:sd a2, 1224(fp)
Current Store : [0x8000a2a0] : sd a2, 1224(fp) -- Store: [0x80015c70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a2d4]:fmax.s t6, t5, t4
	-[0x8000a2d8]:csrrs a2, fcsr, zero
	-[0x8000a2dc]:sd t6, 1232(fp)
	-[0x8000a2e0]:sd a2, 1240(fp)
Current Store : [0x8000a2e0] : sd a2, 1240(fp) -- Store: [0x80015c80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a314]:fmax.s t6, t5, t4
	-[0x8000a318]:csrrs a2, fcsr, zero
	-[0x8000a31c]:sd t6, 1248(fp)
	-[0x8000a320]:sd a2, 1256(fp)
Current Store : [0x8000a320] : sd a2, 1256(fp) -- Store: [0x80015c90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a354]:fmax.s t6, t5, t4
	-[0x8000a358]:csrrs a2, fcsr, zero
	-[0x8000a35c]:sd t6, 1264(fp)
	-[0x8000a360]:sd a2, 1272(fp)
Current Store : [0x8000a360] : sd a2, 1272(fp) -- Store: [0x80015ca0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a394]:fmax.s t6, t5, t4
	-[0x8000a398]:csrrs a2, fcsr, zero
	-[0x8000a39c]:sd t6, 1280(fp)
	-[0x8000a3a0]:sd a2, 1288(fp)
Current Store : [0x8000a3a0] : sd a2, 1288(fp) -- Store: [0x80015cb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a3d4]:fmax.s t6, t5, t4
	-[0x8000a3d8]:csrrs a2, fcsr, zero
	-[0x8000a3dc]:sd t6, 1296(fp)
	-[0x8000a3e0]:sd a2, 1304(fp)
Current Store : [0x8000a3e0] : sd a2, 1304(fp) -- Store: [0x80015cc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a414]:fmax.s t6, t5, t4
	-[0x8000a418]:csrrs a2, fcsr, zero
	-[0x8000a41c]:sd t6, 1312(fp)
	-[0x8000a420]:sd a2, 1320(fp)
Current Store : [0x8000a420] : sd a2, 1320(fp) -- Store: [0x80015cd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a454]:fmax.s t6, t5, t4
	-[0x8000a458]:csrrs a2, fcsr, zero
	-[0x8000a45c]:sd t6, 1328(fp)
	-[0x8000a460]:sd a2, 1336(fp)
Current Store : [0x8000a460] : sd a2, 1336(fp) -- Store: [0x80015ce0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a494]:fmax.s t6, t5, t4
	-[0x8000a498]:csrrs a2, fcsr, zero
	-[0x8000a49c]:sd t6, 1344(fp)
	-[0x8000a4a0]:sd a2, 1352(fp)
Current Store : [0x8000a4a0] : sd a2, 1352(fp) -- Store: [0x80015cf0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a4d4]:fmax.s t6, t5, t4
	-[0x8000a4d8]:csrrs a2, fcsr, zero
	-[0x8000a4dc]:sd t6, 1360(fp)
	-[0x8000a4e0]:sd a2, 1368(fp)
Current Store : [0x8000a4e0] : sd a2, 1368(fp) -- Store: [0x80015d00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a514]:fmax.s t6, t5, t4
	-[0x8000a518]:csrrs a2, fcsr, zero
	-[0x8000a51c]:sd t6, 1376(fp)
	-[0x8000a520]:sd a2, 1384(fp)
Current Store : [0x8000a520] : sd a2, 1384(fp) -- Store: [0x80015d10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a554]:fmax.s t6, t5, t4
	-[0x8000a558]:csrrs a2, fcsr, zero
	-[0x8000a55c]:sd t6, 1392(fp)
	-[0x8000a560]:sd a2, 1400(fp)
Current Store : [0x8000a560] : sd a2, 1400(fp) -- Store: [0x80015d20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a594]:fmax.s t6, t5, t4
	-[0x8000a598]:csrrs a2, fcsr, zero
	-[0x8000a59c]:sd t6, 1408(fp)
	-[0x8000a5a0]:sd a2, 1416(fp)
Current Store : [0x8000a5a0] : sd a2, 1416(fp) -- Store: [0x80015d30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a5d4]:fmax.s t6, t5, t4
	-[0x8000a5d8]:csrrs a2, fcsr, zero
	-[0x8000a5dc]:sd t6, 1424(fp)
	-[0x8000a5e0]:sd a2, 1432(fp)
Current Store : [0x8000a5e0] : sd a2, 1432(fp) -- Store: [0x80015d40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x80 and fm1 == 0x27893a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a614]:fmax.s t6, t5, t4
	-[0x8000a618]:csrrs a2, fcsr, zero
	-[0x8000a61c]:sd t6, 1440(fp)
	-[0x8000a620]:sd a2, 1448(fp)
Current Store : [0x8000a620] : sd a2, 1448(fp) -- Store: [0x80015d50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x27893a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a654]:fmax.s t6, t5, t4
	-[0x8000a658]:csrrs a2, fcsr, zero
	-[0x8000a65c]:sd t6, 1456(fp)
	-[0x8000a660]:sd a2, 1464(fp)
Current Store : [0x8000a660] : sd a2, 1464(fp) -- Store: [0x80015d60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x1c8139 and fs2 == 0 and fe2 == 0x80 and fm2 == 0x27893a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a694]:fmax.s t6, t5, t4
	-[0x8000a698]:csrrs a2, fcsr, zero
	-[0x8000a69c]:sd t6, 1472(fp)
	-[0x8000a6a0]:sd a2, 1480(fp)
Current Store : [0x8000a6a0] : sd a2, 1480(fp) -- Store: [0x80015d70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a6d4]:fmax.s t6, t5, t4
	-[0x8000a6d8]:csrrs a2, fcsr, zero
	-[0x8000a6dc]:sd t6, 1488(fp)
	-[0x8000a6e0]:sd a2, 1496(fp)
Current Store : [0x8000a6e0] : sd a2, 1496(fp) -- Store: [0x80015d80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a714]:fmax.s t6, t5, t4
	-[0x8000a718]:csrrs a2, fcsr, zero
	-[0x8000a71c]:sd t6, 1504(fp)
	-[0x8000a720]:sd a2, 1512(fp)
Current Store : [0x8000a720] : sd a2, 1512(fp) -- Store: [0x80015d90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfc and fm2 == 0x1f6f2f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a754]:fmax.s t6, t5, t4
	-[0x8000a758]:csrrs a2, fcsr, zero
	-[0x8000a75c]:sd t6, 1520(fp)
	-[0x8000a760]:sd a2, 1528(fp)
Current Store : [0x8000a760] : sd a2, 1528(fp) -- Store: [0x80015da0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a794]:fmax.s t6, t5, t4
	-[0x8000a798]:csrrs a2, fcsr, zero
	-[0x8000a79c]:sd t6, 1536(fp)
	-[0x8000a7a0]:sd a2, 1544(fp)
Current Store : [0x8000a7a0] : sd a2, 1544(fp) -- Store: [0x80015db0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a7d4]:fmax.s t6, t5, t4
	-[0x8000a7d8]:csrrs a2, fcsr, zero
	-[0x8000a7dc]:sd t6, 1552(fp)
	-[0x8000a7e0]:sd a2, 1560(fp)
Current Store : [0x8000a7e0] : sd a2, 1560(fp) -- Store: [0x80015dc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a814]:fmax.s t6, t5, t4
	-[0x8000a818]:csrrs a2, fcsr, zero
	-[0x8000a81c]:sd t6, 1568(fp)
	-[0x8000a820]:sd a2, 1576(fp)
Current Store : [0x8000a820] : sd a2, 1576(fp) -- Store: [0x80015dd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a854]:fmax.s t6, t5, t4
	-[0x8000a858]:csrrs a2, fcsr, zero
	-[0x8000a85c]:sd t6, 1584(fp)
	-[0x8000a860]:sd a2, 1592(fp)
Current Store : [0x8000a860] : sd a2, 1592(fp) -- Store: [0x80015de0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a894]:fmax.s t6, t5, t4
	-[0x8000a898]:csrrs a2, fcsr, zero
	-[0x8000a89c]:sd t6, 1600(fp)
	-[0x8000a8a0]:sd a2, 1608(fp)
Current Store : [0x8000a8a0] : sd a2, 1608(fp) -- Store: [0x80015df0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a8d4]:fmax.s t6, t5, t4
	-[0x8000a8d8]:csrrs a2, fcsr, zero
	-[0x8000a8dc]:sd t6, 1616(fp)
	-[0x8000a8e0]:sd a2, 1624(fp)
Current Store : [0x8000a8e0] : sd a2, 1624(fp) -- Store: [0x80015e00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a914]:fmax.s t6, t5, t4
	-[0x8000a918]:csrrs a2, fcsr, zero
	-[0x8000a91c]:sd t6, 1632(fp)
	-[0x8000a920]:sd a2, 1640(fp)
Current Store : [0x8000a920] : sd a2, 1640(fp) -- Store: [0x80015e10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000a954]:fmax.s t6, t5, t4
	-[0x8000a958]:csrrs a2, fcsr, zero
	-[0x8000a95c]:sd t6, 1648(fp)
	-[0x8000a960]:sd a2, 1656(fp)
Current Store : [0x8000a960] : sd a2, 1656(fp) -- Store: [0x80015e20]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a994]:fmax.s t6, t5, t4
	-[0x8000a998]:csrrs a2, fcsr, zero
	-[0x8000a99c]:sd t6, 1664(fp)
	-[0x8000a9a0]:sd a2, 1672(fp)
Current Store : [0x8000a9a0] : sd a2, 1672(fp) -- Store: [0x80015e30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000a9d4]:fmax.s t6, t5, t4
	-[0x8000a9d8]:csrrs a2, fcsr, zero
	-[0x8000a9dc]:sd t6, 1680(fp)
	-[0x8000a9e0]:sd a2, 1688(fp)
Current Store : [0x8000a9e0] : sd a2, 1688(fp) -- Store: [0x80015e40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000aa14]:fmax.s t6, t5, t4
	-[0x8000aa18]:csrrs a2, fcsr, zero
	-[0x8000aa1c]:sd t6, 1696(fp)
	-[0x8000aa20]:sd a2, 1704(fp)
Current Store : [0x8000aa20] : sd a2, 1704(fp) -- Store: [0x80015e50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000aa54]:fmax.s t6, t5, t4
	-[0x8000aa58]:csrrs a2, fcsr, zero
	-[0x8000aa5c]:sd t6, 1712(fp)
	-[0x8000aa60]:sd a2, 1720(fp)
Current Store : [0x8000aa60] : sd a2, 1720(fp) -- Store: [0x80015e60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000aa94]:fmax.s t6, t5, t4
	-[0x8000aa98]:csrrs a2, fcsr, zero
	-[0x8000aa9c]:sd t6, 1728(fp)
	-[0x8000aaa0]:sd a2, 1736(fp)
Current Store : [0x8000aaa0] : sd a2, 1736(fp) -- Store: [0x80015e70]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000aad4]:fmax.s t6, t5, t4
	-[0x8000aad8]:csrrs a2, fcsr, zero
	-[0x8000aadc]:sd t6, 1744(fp)
	-[0x8000aae0]:sd a2, 1752(fp)
Current Store : [0x8000aae0] : sd a2, 1752(fp) -- Store: [0x80015e80]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x1f6f2f and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ab14]:fmax.s t6, t5, t4
	-[0x8000ab18]:csrrs a2, fcsr, zero
	-[0x8000ab1c]:sd t6, 1760(fp)
	-[0x8000ab20]:sd a2, 1768(fp)
Current Store : [0x8000ab20] : sd a2, 1768(fp) -- Store: [0x80015e90]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ab54]:fmax.s t6, t5, t4
	-[0x8000ab58]:csrrs a2, fcsr, zero
	-[0x8000ab5c]:sd t6, 1776(fp)
	-[0x8000ab60]:sd a2, 1784(fp)
Current Store : [0x8000ab60] : sd a2, 1784(fp) -- Store: [0x80015ea0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ab94]:fmax.s t6, t5, t4
	-[0x8000ab98]:csrrs a2, fcsr, zero
	-[0x8000ab9c]:sd t6, 1792(fp)
	-[0x8000aba0]:sd a2, 1800(fp)
Current Store : [0x8000aba0] : sd a2, 1800(fp) -- Store: [0x80015eb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000abd4]:fmax.s t6, t5, t4
	-[0x8000abd8]:csrrs a2, fcsr, zero
	-[0x8000abdc]:sd t6, 1808(fp)
	-[0x8000abe0]:sd a2, 1816(fp)
Current Store : [0x8000abe0] : sd a2, 1816(fp) -- Store: [0x80015ec0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ac14]:fmax.s t6, t5, t4
	-[0x8000ac18]:csrrs a2, fcsr, zero
	-[0x8000ac1c]:sd t6, 1824(fp)
	-[0x8000ac20]:sd a2, 1832(fp)
Current Store : [0x8000ac20] : sd a2, 1832(fp) -- Store: [0x80015ed0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ac54]:fmax.s t6, t5, t4
	-[0x8000ac58]:csrrs a2, fcsr, zero
	-[0x8000ac5c]:sd t6, 1840(fp)
	-[0x8000ac60]:sd a2, 1848(fp)
Current Store : [0x8000ac60] : sd a2, 1848(fp) -- Store: [0x80015ee0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ac94]:fmax.s t6, t5, t4
	-[0x8000ac98]:csrrs a2, fcsr, zero
	-[0x8000ac9c]:sd t6, 1856(fp)
	-[0x8000aca0]:sd a2, 1864(fp)
Current Store : [0x8000aca0] : sd a2, 1864(fp) -- Store: [0x80015ef0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000acd4]:fmax.s t6, t5, t4
	-[0x8000acd8]:csrrs a2, fcsr, zero
	-[0x8000acdc]:sd t6, 1872(fp)
	-[0x8000ace0]:sd a2, 1880(fp)
Current Store : [0x8000ace0] : sd a2, 1880(fp) -- Store: [0x80015f00]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ad14]:fmax.s t6, t5, t4
	-[0x8000ad18]:csrrs a2, fcsr, zero
	-[0x8000ad1c]:sd t6, 1888(fp)
	-[0x8000ad20]:sd a2, 1896(fp)
Current Store : [0x8000ad20] : sd a2, 1896(fp) -- Store: [0x80015f10]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ad54]:fmax.s t6, t5, t4
	-[0x8000ad58]:csrrs a2, fcsr, zero
	-[0x8000ad5c]:sd t6, 1904(fp)
	-[0x8000ad60]:sd a2, 1912(fp)
Current Store : [0x8000ad60] : sd a2, 1912(fp) -- Store: [0x80015f20]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ad94]:fmax.s t6, t5, t4
	-[0x8000ad98]:csrrs a2, fcsr, zero
	-[0x8000ad9c]:sd t6, 1920(fp)
	-[0x8000ada0]:sd a2, 1928(fp)
Current Store : [0x8000ada0] : sd a2, 1928(fp) -- Store: [0x80015f30]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000add4]:fmax.s t6, t5, t4
	-[0x8000add8]:csrrs a2, fcsr, zero
	-[0x8000addc]:sd t6, 1936(fp)
	-[0x8000ade0]:sd a2, 1944(fp)
Current Store : [0x8000ade0] : sd a2, 1944(fp) -- Store: [0x80015f40]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x81 and fm1 == 0x298a26 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ae14]:fmax.s t6, t5, t4
	-[0x8000ae18]:csrrs a2, fcsr, zero
	-[0x8000ae1c]:sd t6, 1952(fp)
	-[0x8000ae20]:sd a2, 1960(fp)
Current Store : [0x8000ae20] : sd a2, 1960(fp) -- Store: [0x80015f50]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x81 and fm2 == 0x298a26 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ae54]:fmax.s t6, t5, t4
	-[0x8000ae58]:csrrs a2, fcsr, zero
	-[0x8000ae5c]:sd t6, 1968(fp)
	-[0x8000ae60]:sd a2, 1976(fp)
Current Store : [0x8000ae60] : sd a2, 1976(fp) -- Store: [0x80015f60]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x39b0fc and fs2 == 0 and fe2 == 0x81 and fm2 == 0x298a26 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ae94]:fmax.s t6, t5, t4
	-[0x8000ae98]:csrrs a2, fcsr, zero
	-[0x8000ae9c]:sd t6, 1984(fp)
	-[0x8000aea0]:sd a2, 1992(fp)
Current Store : [0x8000aea0] : sd a2, 1992(fp) -- Store: [0x80015f70]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000aecc]:fmax.s t6, t5, t4
	-[0x8000aed0]:csrrs a2, fcsr, zero
	-[0x8000aed4]:sd t6, 2000(fp)
	-[0x8000aed8]:sd a2, 2008(fp)
Current Store : [0x8000aed8] : sd a2, 2008(fp) -- Store: [0x80015f80]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000af04]:fmax.s t6, t5, t4
	-[0x8000af08]:csrrs a2, fcsr, zero
	-[0x8000af0c]:sd t6, 2016(fp)
	-[0x8000af10]:sd a2, 2024(fp)
Current Store : [0x8000af10] : sd a2, 2024(fp) -- Store: [0x80015f90]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfc and fm2 == 0x03c146 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000af3c]:fmax.s t6, t5, t4
	-[0x8000af40]:csrrs a2, fcsr, zero
	-[0x8000af44]:sd t6, 2032(fp)
	-[0x8000af48]:sd a2, 2040(fp)
Current Store : [0x8000af48] : sd a2, 2040(fp) -- Store: [0x80015fa0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000af7c]:fmax.s t6, t5, t4
	-[0x8000af80]:csrrs a2, fcsr, zero
	-[0x8000af84]:sd t6, 0(fp)
	-[0x8000af88]:sd a2, 8(fp)
Current Store : [0x8000af88] : sd a2, 8(fp) -- Store: [0x80015fb0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000afb4]:fmax.s t6, t5, t4
	-[0x8000afb8]:csrrs a2, fcsr, zero
	-[0x8000afbc]:sd t6, 16(fp)
	-[0x8000afc0]:sd a2, 24(fp)
Current Store : [0x8000afc0] : sd a2, 24(fp) -- Store: [0x80015fc0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000afec]:fmax.s t6, t5, t4
	-[0x8000aff0]:csrrs a2, fcsr, zero
	-[0x8000aff4]:sd t6, 32(fp)
	-[0x8000aff8]:sd a2, 40(fp)
Current Store : [0x8000aff8] : sd a2, 40(fp) -- Store: [0x80015fd0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b024]:fmax.s t6, t5, t4
	-[0x8000b028]:csrrs a2, fcsr, zero
	-[0x8000b02c]:sd t6, 48(fp)
	-[0x8000b030]:sd a2, 56(fp)
Current Store : [0x8000b030] : sd a2, 56(fp) -- Store: [0x80015fe0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b05c]:fmax.s t6, t5, t4
	-[0x8000b060]:csrrs a2, fcsr, zero
	-[0x8000b064]:sd t6, 64(fp)
	-[0x8000b068]:sd a2, 72(fp)
Current Store : [0x8000b068] : sd a2, 72(fp) -- Store: [0x80015ff0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b094]:fmax.s t6, t5, t4
	-[0x8000b098]:csrrs a2, fcsr, zero
	-[0x8000b09c]:sd t6, 80(fp)
	-[0x8000b0a0]:sd a2, 88(fp)
Current Store : [0x8000b0a0] : sd a2, 88(fp) -- Store: [0x80016000]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b0cc]:fmax.s t6, t5, t4
	-[0x8000b0d0]:csrrs a2, fcsr, zero
	-[0x8000b0d4]:sd t6, 96(fp)
	-[0x8000b0d8]:sd a2, 104(fp)
Current Store : [0x8000b0d8] : sd a2, 104(fp) -- Store: [0x80016010]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b104]:fmax.s t6, t5, t4
	-[0x8000b108]:csrrs a2, fcsr, zero
	-[0x8000b10c]:sd t6, 112(fp)
	-[0x8000b110]:sd a2, 120(fp)
Current Store : [0x8000b110] : sd a2, 120(fp) -- Store: [0x80016020]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b13c]:fmax.s t6, t5, t4
	-[0x8000b140]:csrrs a2, fcsr, zero
	-[0x8000b144]:sd t6, 128(fp)
	-[0x8000b148]:sd a2, 136(fp)
Current Store : [0x8000b148] : sd a2, 136(fp) -- Store: [0x80016030]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b174]:fmax.s t6, t5, t4
	-[0x8000b178]:csrrs a2, fcsr, zero
	-[0x8000b17c]:sd t6, 144(fp)
	-[0x8000b180]:sd a2, 152(fp)
Current Store : [0x8000b180] : sd a2, 152(fp) -- Store: [0x80016040]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b1ac]:fmax.s t6, t5, t4
	-[0x8000b1b0]:csrrs a2, fcsr, zero
	-[0x8000b1b4]:sd t6, 160(fp)
	-[0x8000b1b8]:sd a2, 168(fp)
Current Store : [0x8000b1b8] : sd a2, 168(fp) -- Store: [0x80016050]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b1e4]:fmax.s t6, t5, t4
	-[0x8000b1e8]:csrrs a2, fcsr, zero
	-[0x8000b1ec]:sd t6, 176(fp)
	-[0x8000b1f0]:sd a2, 184(fp)
Current Store : [0x8000b1f0] : sd a2, 184(fp) -- Store: [0x80016060]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b21c]:fmax.s t6, t5, t4
	-[0x8000b220]:csrrs a2, fcsr, zero
	-[0x8000b224]:sd t6, 192(fp)
	-[0x8000b228]:sd a2, 200(fp)
Current Store : [0x8000b228] : sd a2, 200(fp) -- Store: [0x80016070]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b254]:fmax.s t6, t5, t4
	-[0x8000b258]:csrrs a2, fcsr, zero
	-[0x8000b25c]:sd t6, 208(fp)
	-[0x8000b260]:sd a2, 216(fp)
Current Store : [0x8000b260] : sd a2, 216(fp) -- Store: [0x80016080]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x03c146 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b28c]:fmax.s t6, t5, t4
	-[0x8000b290]:csrrs a2, fcsr, zero
	-[0x8000b294]:sd t6, 224(fp)
	-[0x8000b298]:sd a2, 232(fp)
Current Store : [0x8000b298] : sd a2, 232(fp) -- Store: [0x80016090]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b2c4]:fmax.s t6, t5, t4
	-[0x8000b2c8]:csrrs a2, fcsr, zero
	-[0x8000b2cc]:sd t6, 240(fp)
	-[0x8000b2d0]:sd a2, 248(fp)
Current Store : [0x8000b2d0] : sd a2, 248(fp) -- Store: [0x800160a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b2fc]:fmax.s t6, t5, t4
	-[0x8000b300]:csrrs a2, fcsr, zero
	-[0x8000b304]:sd t6, 256(fp)
	-[0x8000b308]:sd a2, 264(fp)
Current Store : [0x8000b308] : sd a2, 264(fp) -- Store: [0x800160b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b334]:fmax.s t6, t5, t4
	-[0x8000b338]:csrrs a2, fcsr, zero
	-[0x8000b33c]:sd t6, 272(fp)
	-[0x8000b340]:sd a2, 280(fp)
Current Store : [0x8000b340] : sd a2, 280(fp) -- Store: [0x800160c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b36c]:fmax.s t6, t5, t4
	-[0x8000b370]:csrrs a2, fcsr, zero
	-[0x8000b374]:sd t6, 288(fp)
	-[0x8000b378]:sd a2, 296(fp)
Current Store : [0x8000b378] : sd a2, 296(fp) -- Store: [0x800160d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b3a4]:fmax.s t6, t5, t4
	-[0x8000b3a8]:csrrs a2, fcsr, zero
	-[0x8000b3ac]:sd t6, 304(fp)
	-[0x8000b3b0]:sd a2, 312(fp)
Current Store : [0x8000b3b0] : sd a2, 312(fp) -- Store: [0x800160e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b3dc]:fmax.s t6, t5, t4
	-[0x8000b3e0]:csrrs a2, fcsr, zero
	-[0x8000b3e4]:sd t6, 320(fp)
	-[0x8000b3e8]:sd a2, 328(fp)
Current Store : [0x8000b3e8] : sd a2, 328(fp) -- Store: [0x800160f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b414]:fmax.s t6, t5, t4
	-[0x8000b418]:csrrs a2, fcsr, zero
	-[0x8000b41c]:sd t6, 336(fp)
	-[0x8000b420]:sd a2, 344(fp)
Current Store : [0x8000b420] : sd a2, 344(fp) -- Store: [0x80016100]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b44c]:fmax.s t6, t5, t4
	-[0x8000b450]:csrrs a2, fcsr, zero
	-[0x8000b454]:sd t6, 352(fp)
	-[0x8000b458]:sd a2, 360(fp)
Current Store : [0x8000b458] : sd a2, 360(fp) -- Store: [0x80016110]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b484]:fmax.s t6, t5, t4
	-[0x8000b488]:csrrs a2, fcsr, zero
	-[0x8000b48c]:sd t6, 368(fp)
	-[0x8000b490]:sd a2, 376(fp)
Current Store : [0x8000b490] : sd a2, 376(fp) -- Store: [0x80016120]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x81 and fm1 == 0x0c1b1e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b4bc]:fmax.s t6, t5, t4
	-[0x8000b4c0]:csrrs a2, fcsr, zero
	-[0x8000b4c4]:sd t6, 384(fp)
	-[0x8000b4c8]:sd a2, 392(fp)
Current Store : [0x8000b4c8] : sd a2, 392(fp) -- Store: [0x80016130]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x0c1b1e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b4f4]:fmax.s t6, t5, t4
	-[0x8000b4f8]:csrrs a2, fcsr, zero
	-[0x8000b4fc]:sd t6, 400(fp)
	-[0x8000b500]:sd a2, 408(fp)
Current Store : [0x8000b500] : sd a2, 408(fp) -- Store: [0x80016140]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x2facf2 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x0c1b1e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b52c]:fmax.s t6, t5, t4
	-[0x8000b530]:csrrs a2, fcsr, zero
	-[0x8000b534]:sd t6, 416(fp)
	-[0x8000b538]:sd a2, 424(fp)
Current Store : [0x8000b538] : sd a2, 424(fp) -- Store: [0x80016150]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b564]:fmax.s t6, t5, t4
	-[0x8000b568]:csrrs a2, fcsr, zero
	-[0x8000b56c]:sd t6, 432(fp)
	-[0x8000b570]:sd a2, 440(fp)
Current Store : [0x8000b570] : sd a2, 440(fp) -- Store: [0x80016160]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b59c]:fmax.s t6, t5, t4
	-[0x8000b5a0]:csrrs a2, fcsr, zero
	-[0x8000b5a4]:sd t6, 448(fp)
	-[0x8000b5a8]:sd a2, 456(fp)
Current Store : [0x8000b5a8] : sd a2, 456(fp) -- Store: [0x80016170]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x157915 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b5d4]:fmax.s t6, t5, t4
	-[0x8000b5d8]:csrrs a2, fcsr, zero
	-[0x8000b5dc]:sd t6, 464(fp)
	-[0x8000b5e0]:sd a2, 472(fp)
Current Store : [0x8000b5e0] : sd a2, 472(fp) -- Store: [0x80016180]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b60c]:fmax.s t6, t5, t4
	-[0x8000b610]:csrrs a2, fcsr, zero
	-[0x8000b614]:sd t6, 480(fp)
	-[0x8000b618]:sd a2, 488(fp)
Current Store : [0x8000b618] : sd a2, 488(fp) -- Store: [0x80016190]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b644]:fmax.s t6, t5, t4
	-[0x8000b648]:csrrs a2, fcsr, zero
	-[0x8000b64c]:sd t6, 496(fp)
	-[0x8000b650]:sd a2, 504(fp)
Current Store : [0x8000b650] : sd a2, 504(fp) -- Store: [0x800161a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b67c]:fmax.s t6, t5, t4
	-[0x8000b680]:csrrs a2, fcsr, zero
	-[0x8000b684]:sd t6, 512(fp)
	-[0x8000b688]:sd a2, 520(fp)
Current Store : [0x8000b688] : sd a2, 520(fp) -- Store: [0x800161b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfd and fm2 == 0x3ad75a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b6b4]:fmax.s t6, t5, t4
	-[0x8000b6b8]:csrrs a2, fcsr, zero
	-[0x8000b6bc]:sd t6, 528(fp)
	-[0x8000b6c0]:sd a2, 536(fp)
Current Store : [0x8000b6c0] : sd a2, 536(fp) -- Store: [0x800161c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b6ec]:fmax.s t6, t5, t4
	-[0x8000b6f0]:csrrs a2, fcsr, zero
	-[0x8000b6f4]:sd t6, 544(fp)
	-[0x8000b6f8]:sd a2, 552(fp)
Current Store : [0x8000b6f8] : sd a2, 552(fp) -- Store: [0x800161d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b724]:fmax.s t6, t5, t4
	-[0x8000b728]:csrrs a2, fcsr, zero
	-[0x8000b72c]:sd t6, 560(fp)
	-[0x8000b730]:sd a2, 568(fp)
Current Store : [0x8000b730] : sd a2, 568(fp) -- Store: [0x800161e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b75c]:fmax.s t6, t5, t4
	-[0x8000b760]:csrrs a2, fcsr, zero
	-[0x8000b764]:sd t6, 576(fp)
	-[0x8000b768]:sd a2, 584(fp)
Current Store : [0x8000b768] : sd a2, 584(fp) -- Store: [0x800161f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b794]:fmax.s t6, t5, t4
	-[0x8000b798]:csrrs a2, fcsr, zero
	-[0x8000b79c]:sd t6, 592(fp)
	-[0x8000b7a0]:sd a2, 600(fp)
Current Store : [0x8000b7a0] : sd a2, 600(fp) -- Store: [0x80016200]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b7cc]:fmax.s t6, t5, t4
	-[0x8000b7d0]:csrrs a2, fcsr, zero
	-[0x8000b7d4]:sd t6, 608(fp)
	-[0x8000b7d8]:sd a2, 616(fp)
Current Store : [0x8000b7d8] : sd a2, 616(fp) -- Store: [0x80016210]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000b804]:fmax.s t6, t5, t4
	-[0x8000b808]:csrrs a2, fcsr, zero
	-[0x8000b80c]:sd t6, 624(fp)
	-[0x8000b810]:sd a2, 632(fp)
Current Store : [0x8000b810] : sd a2, 632(fp) -- Store: [0x80016220]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b83c]:fmax.s t6, t5, t4
	-[0x8000b840]:csrrs a2, fcsr, zero
	-[0x8000b844]:sd t6, 640(fp)
	-[0x8000b848]:sd a2, 648(fp)
Current Store : [0x8000b848] : sd a2, 648(fp) -- Store: [0x80016230]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b874]:fmax.s t6, t5, t4
	-[0x8000b878]:csrrs a2, fcsr, zero
	-[0x8000b87c]:sd t6, 656(fp)
	-[0x8000b880]:sd a2, 664(fp)
Current Store : [0x8000b880] : sd a2, 664(fp) -- Store: [0x80016240]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b8ac]:fmax.s t6, t5, t4
	-[0x8000b8b0]:csrrs a2, fcsr, zero
	-[0x8000b8b4]:sd t6, 672(fp)
	-[0x8000b8b8]:sd a2, 680(fp)
Current Store : [0x8000b8b8] : sd a2, 680(fp) -- Store: [0x80016250]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b8e4]:fmax.s t6, t5, t4
	-[0x8000b8e8]:csrrs a2, fcsr, zero
	-[0x8000b8ec]:sd t6, 688(fp)
	-[0x8000b8f0]:sd a2, 696(fp)
Current Store : [0x8000b8f0] : sd a2, 696(fp) -- Store: [0x80016260]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b91c]:fmax.s t6, t5, t4
	-[0x8000b920]:csrrs a2, fcsr, zero
	-[0x8000b924]:sd t6, 704(fp)
	-[0x8000b928]:sd a2, 712(fp)
Current Store : [0x8000b928] : sd a2, 712(fp) -- Store: [0x80016270]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b954]:fmax.s t6, t5, t4
	-[0x8000b958]:csrrs a2, fcsr, zero
	-[0x8000b95c]:sd t6, 720(fp)
	-[0x8000b960]:sd a2, 728(fp)
Current Store : [0x8000b960] : sd a2, 728(fp) -- Store: [0x80016280]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b98c]:fmax.s t6, t5, t4
	-[0x8000b990]:csrrs a2, fcsr, zero
	-[0x8000b994]:sd t6, 736(fp)
	-[0x8000b998]:sd a2, 744(fp)
Current Store : [0x8000b998] : sd a2, 744(fp) -- Store: [0x80016290]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x3ad75a and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b9c4]:fmax.s t6, t5, t4
	-[0x8000b9c8]:csrrs a2, fcsr, zero
	-[0x8000b9cc]:sd t6, 752(fp)
	-[0x8000b9d0]:sd a2, 760(fp)
Current Store : [0x8000b9d0] : sd a2, 760(fp) -- Store: [0x800162a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000b9fc]:fmax.s t6, t5, t4
	-[0x8000ba00]:csrrs a2, fcsr, zero
	-[0x8000ba04]:sd t6, 768(fp)
	-[0x8000ba08]:sd a2, 776(fp)
Current Store : [0x8000ba08] : sd a2, 776(fp) -- Store: [0x800162b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x157915 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ba34]:fmax.s t6, t5, t4
	-[0x8000ba38]:csrrs a2, fcsr, zero
	-[0x8000ba3c]:sd t6, 784(fp)
	-[0x8000ba40]:sd a2, 792(fp)
Current Store : [0x8000ba40] : sd a2, 792(fp) -- Store: [0x800162c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ba6c]:fmax.s t6, t5, t4
	-[0x8000ba70]:csrrs a2, fcsr, zero
	-[0x8000ba74]:sd t6, 800(fp)
	-[0x8000ba78]:sd a2, 808(fp)
Current Store : [0x8000ba78] : sd a2, 808(fp) -- Store: [0x800162d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000baa4]:fmax.s t6, t5, t4
	-[0x8000baa8]:csrrs a2, fcsr, zero
	-[0x8000baac]:sd t6, 816(fp)
	-[0x8000bab0]:sd a2, 824(fp)
Current Store : [0x8000bab0] : sd a2, 824(fp) -- Store: [0x800162e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000badc]:fmax.s t6, t5, t4
	-[0x8000bae0]:csrrs a2, fcsr, zero
	-[0x8000bae4]:sd t6, 832(fp)
	-[0x8000bae8]:sd a2, 840(fp)
Current Store : [0x8000bae8] : sd a2, 840(fp) -- Store: [0x800162f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bb14]:fmax.s t6, t5, t4
	-[0x8000bb18]:csrrs a2, fcsr, zero
	-[0x8000bb1c]:sd t6, 848(fp)
	-[0x8000bb20]:sd a2, 856(fp)
Current Store : [0x8000bb20] : sd a2, 856(fp) -- Store: [0x80016300]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bb4c]:fmax.s t6, t5, t4
	-[0x8000bb50]:csrrs a2, fcsr, zero
	-[0x8000bb54]:sd t6, 864(fp)
	-[0x8000bb58]:sd a2, 872(fp)
Current Store : [0x8000bb58] : sd a2, 872(fp) -- Store: [0x80016310]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bb84]:fmax.s t6, t5, t4
	-[0x8000bb88]:csrrs a2, fcsr, zero
	-[0x8000bb8c]:sd t6, 880(fp)
	-[0x8000bb90]:sd a2, 888(fp)
Current Store : [0x8000bb90] : sd a2, 888(fp) -- Store: [0x80016320]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bbbc]:fmax.s t6, t5, t4
	-[0x8000bbc0]:csrrs a2, fcsr, zero
	-[0x8000bbc4]:sd t6, 896(fp)
	-[0x8000bbc8]:sd a2, 904(fp)
Current Store : [0x8000bbc8] : sd a2, 904(fp) -- Store: [0x80016330]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x1ef26a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bbf4]:fmax.s t6, t5, t4
	-[0x8000bbf8]:csrrs a2, fcsr, zero
	-[0x8000bbfc]:sd t6, 912(fp)
	-[0x8000bc00]:sd a2, 920(fp)
Current Store : [0x8000bc00] : sd a2, 920(fp) -- Store: [0x80016340]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x1ef26a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bc2c]:fmax.s t6, t5, t4
	-[0x8000bc30]:csrrs a2, fcsr, zero
	-[0x8000bc34]:sd t6, 928(fp)
	-[0x8000bc38]:sd a2, 936(fp)
Current Store : [0x8000bc38] : sd a2, 936(fp) -- Store: [0x80016350]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x0d858e and fs2 == 1 and fe2 == 0x7f and fm2 == 0x1ef26a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bc64]:fmax.s t6, t5, t4
	-[0x8000bc68]:csrrs a2, fcsr, zero
	-[0x8000bc6c]:sd t6, 944(fp)
	-[0x8000bc70]:sd a2, 952(fp)
Current Store : [0x8000bc70] : sd a2, 952(fp) -- Store: [0x80016360]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bc9c]:fmax.s t6, t5, t4
	-[0x8000bca0]:csrrs a2, fcsr, zero
	-[0x8000bca4]:sd t6, 960(fp)
	-[0x8000bca8]:sd a2, 968(fp)
Current Store : [0x8000bca8] : sd a2, 968(fp) -- Store: [0x80016370]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bcd4]:fmax.s t6, t5, t4
	-[0x8000bcd8]:csrrs a2, fcsr, zero
	-[0x8000bcdc]:sd t6, 976(fp)
	-[0x8000bce0]:sd a2, 984(fp)
Current Store : [0x8000bce0] : sd a2, 984(fp) -- Store: [0x80016380]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x48a6ca and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bd0c]:fmax.s t6, t5, t4
	-[0x8000bd10]:csrrs a2, fcsr, zero
	-[0x8000bd14]:sd t6, 992(fp)
	-[0x8000bd18]:sd a2, 1000(fp)
Current Store : [0x8000bd18] : sd a2, 1000(fp) -- Store: [0x80016390]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bd44]:fmax.s t6, t5, t4
	-[0x8000bd48]:csrrs a2, fcsr, zero
	-[0x8000bd4c]:sd t6, 1008(fp)
	-[0x8000bd50]:sd a2, 1016(fp)
Current Store : [0x8000bd50] : sd a2, 1016(fp) -- Store: [0x800163a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bd7c]:fmax.s t6, t5, t4
	-[0x8000bd80]:csrrs a2, fcsr, zero
	-[0x8000bd84]:sd t6, 1024(fp)
	-[0x8000bd88]:sd a2, 1032(fp)
Current Store : [0x8000bd88] : sd a2, 1032(fp) -- Store: [0x800163b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bdb4]:fmax.s t6, t5, t4
	-[0x8000bdb8]:csrrs a2, fcsr, zero
	-[0x8000bdbc]:sd t6, 1040(fp)
	-[0x8000bdc0]:sd a2, 1048(fp)
Current Store : [0x8000bdc0] : sd a2, 1048(fp) -- Store: [0x800163c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7ad07d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bdec]:fmax.s t6, t5, t4
	-[0x8000bdf0]:csrrs a2, fcsr, zero
	-[0x8000bdf4]:sd t6, 1056(fp)
	-[0x8000bdf8]:sd a2, 1064(fp)
Current Store : [0x8000bdf8] : sd a2, 1064(fp) -- Store: [0x800163d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000be24]:fmax.s t6, t5, t4
	-[0x8000be28]:csrrs a2, fcsr, zero
	-[0x8000be2c]:sd t6, 1072(fp)
	-[0x8000be30]:sd a2, 1080(fp)
Current Store : [0x8000be30] : sd a2, 1080(fp) -- Store: [0x800163e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000be5c]:fmax.s t6, t5, t4
	-[0x8000be60]:csrrs a2, fcsr, zero
	-[0x8000be64]:sd t6, 1088(fp)
	-[0x8000be68]:sd a2, 1096(fp)
Current Store : [0x8000be68] : sd a2, 1096(fp) -- Store: [0x800163f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000be94]:fmax.s t6, t5, t4
	-[0x8000be98]:csrrs a2, fcsr, zero
	-[0x8000be9c]:sd t6, 1104(fp)
	-[0x8000bea0]:sd a2, 1112(fp)
Current Store : [0x8000bea0] : sd a2, 1112(fp) -- Store: [0x80016400]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000becc]:fmax.s t6, t5, t4
	-[0x8000bed0]:csrrs a2, fcsr, zero
	-[0x8000bed4]:sd t6, 1120(fp)
	-[0x8000bed8]:sd a2, 1128(fp)
Current Store : [0x8000bed8] : sd a2, 1128(fp) -- Store: [0x80016410]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bf04]:fmax.s t6, t5, t4
	-[0x8000bf08]:csrrs a2, fcsr, zero
	-[0x8000bf0c]:sd t6, 1136(fp)
	-[0x8000bf10]:sd a2, 1144(fp)
Current Store : [0x8000bf10] : sd a2, 1144(fp) -- Store: [0x80016420]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000bf3c]:fmax.s t6, t5, t4
	-[0x8000bf40]:csrrs a2, fcsr, zero
	-[0x8000bf44]:sd t6, 1152(fp)
	-[0x8000bf48]:sd a2, 1160(fp)
Current Store : [0x8000bf48] : sd a2, 1160(fp) -- Store: [0x80016430]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bf74]:fmax.s t6, t5, t4
	-[0x8000bf78]:csrrs a2, fcsr, zero
	-[0x8000bf7c]:sd t6, 1168(fp)
	-[0x8000bf80]:sd a2, 1176(fp)
Current Store : [0x8000bf80] : sd a2, 1176(fp) -- Store: [0x80016440]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bfac]:fmax.s t6, t5, t4
	-[0x8000bfb0]:csrrs a2, fcsr, zero
	-[0x8000bfb4]:sd t6, 1184(fp)
	-[0x8000bfb8]:sd a2, 1192(fp)
Current Store : [0x8000bfb8] : sd a2, 1192(fp) -- Store: [0x80016450]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000bfe4]:fmax.s t6, t5, t4
	-[0x8000bfe8]:csrrs a2, fcsr, zero
	-[0x8000bfec]:sd t6, 1200(fp)
	-[0x8000bff0]:sd a2, 1208(fp)
Current Store : [0x8000bff0] : sd a2, 1208(fp) -- Store: [0x80016460]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c01c]:fmax.s t6, t5, t4
	-[0x8000c020]:csrrs a2, fcsr, zero
	-[0x8000c024]:sd t6, 1216(fp)
	-[0x8000c028]:sd a2, 1224(fp)
Current Store : [0x8000c028] : sd a2, 1224(fp) -- Store: [0x80016470]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c054]:fmax.s t6, t5, t4
	-[0x8000c058]:csrrs a2, fcsr, zero
	-[0x8000c05c]:sd t6, 1232(fp)
	-[0x8000c060]:sd a2, 1240(fp)
Current Store : [0x8000c060] : sd a2, 1240(fp) -- Store: [0x80016480]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c08c]:fmax.s t6, t5, t4
	-[0x8000c090]:csrrs a2, fcsr, zero
	-[0x8000c094]:sd t6, 1248(fp)
	-[0x8000c098]:sd a2, 1256(fp)
Current Store : [0x8000c098] : sd a2, 1256(fp) -- Store: [0x80016490]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c0c4]:fmax.s t6, t5, t4
	-[0x8000c0c8]:csrrs a2, fcsr, zero
	-[0x8000c0cc]:sd t6, 1264(fp)
	-[0x8000c0d0]:sd a2, 1272(fp)
Current Store : [0x8000c0d0] : sd a2, 1272(fp) -- Store: [0x800164a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfe and fm1 == 0x7ad07d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c0fc]:fmax.s t6, t5, t4
	-[0x8000c100]:csrrs a2, fcsr, zero
	-[0x8000c104]:sd t6, 1280(fp)
	-[0x8000c108]:sd a2, 1288(fp)
Current Store : [0x8000c108] : sd a2, 1288(fp) -- Store: [0x800164b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c134]:fmax.s t6, t5, t4
	-[0x8000c138]:csrrs a2, fcsr, zero
	-[0x8000c13c]:sd t6, 1296(fp)
	-[0x8000c140]:sd a2, 1304(fp)
Current Store : [0x8000c140] : sd a2, 1304(fp) -- Store: [0x800164c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfb and fm1 == 0x48a6ca and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c16c]:fmax.s t6, t5, t4
	-[0x8000c170]:csrrs a2, fcsr, zero
	-[0x8000c174]:sd t6, 1312(fp)
	-[0x8000c178]:sd a2, 1320(fp)
Current Store : [0x8000c178] : sd a2, 1320(fp) -- Store: [0x800164d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c1a4]:fmax.s t6, t5, t4
	-[0x8000c1a8]:csrrs a2, fcsr, zero
	-[0x8000c1ac]:sd t6, 1328(fp)
	-[0x8000c1b0]:sd a2, 1336(fp)
Current Store : [0x8000c1b0] : sd a2, 1336(fp) -- Store: [0x800164e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c1dc]:fmax.s t6, t5, t4
	-[0x8000c1e0]:csrrs a2, fcsr, zero
	-[0x8000c1e4]:sd t6, 1344(fp)
	-[0x8000c1e8]:sd a2, 1352(fp)
Current Store : [0x8000c1e8] : sd a2, 1352(fp) -- Store: [0x800164f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c214]:fmax.s t6, t5, t4
	-[0x8000c218]:csrrs a2, fcsr, zero
	-[0x8000c21c]:sd t6, 1360(fp)
	-[0x8000c220]:sd a2, 1368(fp)
Current Store : [0x8000c220] : sd a2, 1368(fp) -- Store: [0x80016500]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c24c]:fmax.s t6, t5, t4
	-[0x8000c250]:csrrs a2, fcsr, zero
	-[0x8000c254]:sd t6, 1376(fp)
	-[0x8000c258]:sd a2, 1384(fp)
Current Store : [0x8000c258] : sd a2, 1384(fp) -- Store: [0x80016510]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c284]:fmax.s t6, t5, t4
	-[0x8000c288]:csrrs a2, fcsr, zero
	-[0x8000c28c]:sd t6, 1392(fp)
	-[0x8000c290]:sd a2, 1400(fp)
Current Store : [0x8000c290] : sd a2, 1400(fp) -- Store: [0x80016520]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x80 and fm1 == 0x555e8a and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c2bc]:fmax.s t6, t5, t4
	-[0x8000c2c0]:csrrs a2, fcsr, zero
	-[0x8000c2c4]:sd t6, 1408(fp)
	-[0x8000c2c8]:sd a2, 1416(fp)
Current Store : [0x8000c2c8] : sd a2, 1416(fp) -- Store: [0x80016530]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x80 and fm2 == 0x555e8a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c2f4]:fmax.s t6, t5, t4
	-[0x8000c2f8]:csrrs a2, fcsr, zero
	-[0x8000c2fc]:sd t6, 1424(fp)
	-[0x8000c300]:sd a2, 1432(fp)
Current Store : [0x8000c300] : sd a2, 1432(fp) -- Store: [0x80016540]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x244d8b and fs2 == 1 and fe2 == 0x80 and fm2 == 0x555e8a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c32c]:fmax.s t6, t5, t4
	-[0x8000c330]:csrrs a2, fcsr, zero
	-[0x8000c334]:sd t6, 1440(fp)
	-[0x8000c338]:sd a2, 1448(fp)
Current Store : [0x8000c338] : sd a2, 1448(fp) -- Store: [0x80016550]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c364]:fmax.s t6, t5, t4
	-[0x8000c368]:csrrs a2, fcsr, zero
	-[0x8000c36c]:sd t6, 1456(fp)
	-[0x8000c370]:sd a2, 1464(fp)
Current Store : [0x8000c370] : sd a2, 1464(fp) -- Store: [0x80016560]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c39c]:fmax.s t6, t5, t4
	-[0x8000c3a0]:csrrs a2, fcsr, zero
	-[0x8000c3a4]:sd t6, 1472(fp)
	-[0x8000c3a8]:sd a2, 1480(fp)
Current Store : [0x8000c3a8] : sd a2, 1480(fp) -- Store: [0x80016570]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x4500e4 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c3d4]:fmax.s t6, t5, t4
	-[0x8000c3d8]:csrrs a2, fcsr, zero
	-[0x8000c3dc]:sd t6, 1488(fp)
	-[0x8000c3e0]:sd a2, 1496(fp)
Current Store : [0x8000c3e0] : sd a2, 1496(fp) -- Store: [0x80016580]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c40c]:fmax.s t6, t5, t4
	-[0x8000c410]:csrrs a2, fcsr, zero
	-[0x8000c414]:sd t6, 1504(fp)
	-[0x8000c418]:sd a2, 1512(fp)
Current Store : [0x8000c418] : sd a2, 1512(fp) -- Store: [0x80016590]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c444]:fmax.s t6, t5, t4
	-[0x8000c448]:csrrs a2, fcsr, zero
	-[0x8000c44c]:sd t6, 1520(fp)
	-[0x8000c450]:sd a2, 1528(fp)
Current Store : [0x8000c450] : sd a2, 1528(fp) -- Store: [0x800165a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c47c]:fmax.s t6, t5, t4
	-[0x8000c480]:csrrs a2, fcsr, zero
	-[0x8000c484]:sd t6, 1536(fp)
	-[0x8000c488]:sd a2, 1544(fp)
Current Store : [0x8000c488] : sd a2, 1544(fp) -- Store: [0x800165b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x76411d and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c4b4]:fmax.s t6, t5, t4
	-[0x8000c4b8]:csrrs a2, fcsr, zero
	-[0x8000c4bc]:sd t6, 1552(fp)
	-[0x8000c4c0]:sd a2, 1560(fp)
Current Store : [0x8000c4c0] : sd a2, 1560(fp) -- Store: [0x800165c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c4ec]:fmax.s t6, t5, t4
	-[0x8000c4f0]:csrrs a2, fcsr, zero
	-[0x8000c4f4]:sd t6, 1568(fp)
	-[0x8000c4f8]:sd a2, 1576(fp)
Current Store : [0x8000c4f8] : sd a2, 1576(fp) -- Store: [0x800165d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c524]:fmax.s t6, t5, t4
	-[0x8000c528]:csrrs a2, fcsr, zero
	-[0x8000c52c]:sd t6, 1584(fp)
	-[0x8000c530]:sd a2, 1592(fp)
Current Store : [0x8000c530] : sd a2, 1592(fp) -- Store: [0x800165e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c55c]:fmax.s t6, t5, t4
	-[0x8000c560]:csrrs a2, fcsr, zero
	-[0x8000c564]:sd t6, 1600(fp)
	-[0x8000c568]:sd a2, 1608(fp)
Current Store : [0x8000c568] : sd a2, 1608(fp) -- Store: [0x800165f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c594]:fmax.s t6, t5, t4
	-[0x8000c598]:csrrs a2, fcsr, zero
	-[0x8000c59c]:sd t6, 1616(fp)
	-[0x8000c5a0]:sd a2, 1624(fp)
Current Store : [0x8000c5a0] : sd a2, 1624(fp) -- Store: [0x80016600]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c5cc]:fmax.s t6, t5, t4
	-[0x8000c5d0]:csrrs a2, fcsr, zero
	-[0x8000c5d4]:sd t6, 1632(fp)
	-[0x8000c5d8]:sd a2, 1640(fp)
Current Store : [0x8000c5d8] : sd a2, 1640(fp) -- Store: [0x80016610]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c604]:fmax.s t6, t5, t4
	-[0x8000c608]:csrrs a2, fcsr, zero
	-[0x8000c60c]:sd t6, 1648(fp)
	-[0x8000c610]:sd a2, 1656(fp)
Current Store : [0x8000c610] : sd a2, 1656(fp) -- Store: [0x80016620]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c63c]:fmax.s t6, t5, t4
	-[0x8000c640]:csrrs a2, fcsr, zero
	-[0x8000c644]:sd t6, 1664(fp)
	-[0x8000c648]:sd a2, 1672(fp)
Current Store : [0x8000c648] : sd a2, 1672(fp) -- Store: [0x80016630]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c674]:fmax.s t6, t5, t4
	-[0x8000c678]:csrrs a2, fcsr, zero
	-[0x8000c67c]:sd t6, 1680(fp)
	-[0x8000c680]:sd a2, 1688(fp)
Current Store : [0x8000c680] : sd a2, 1688(fp) -- Store: [0x80016640]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c6ac]:fmax.s t6, t5, t4
	-[0x8000c6b0]:csrrs a2, fcsr, zero
	-[0x8000c6b4]:sd t6, 1696(fp)
	-[0x8000c6b8]:sd a2, 1704(fp)
Current Store : [0x8000c6b8] : sd a2, 1704(fp) -- Store: [0x80016650]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c6e4]:fmax.s t6, t5, t4
	-[0x8000c6e8]:csrrs a2, fcsr, zero
	-[0x8000c6ec]:sd t6, 1712(fp)
	-[0x8000c6f0]:sd a2, 1720(fp)
Current Store : [0x8000c6f0] : sd a2, 1720(fp) -- Store: [0x80016660]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c71c]:fmax.s t6, t5, t4
	-[0x8000c720]:csrrs a2, fcsr, zero
	-[0x8000c724]:sd t6, 1728(fp)
	-[0x8000c728]:sd a2, 1736(fp)
Current Store : [0x8000c728] : sd a2, 1736(fp) -- Store: [0x80016670]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c754]:fmax.s t6, t5, t4
	-[0x8000c758]:csrrs a2, fcsr, zero
	-[0x8000c75c]:sd t6, 1744(fp)
	-[0x8000c760]:sd a2, 1752(fp)
Current Store : [0x8000c760] : sd a2, 1752(fp) -- Store: [0x80016680]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c78c]:fmax.s t6, t5, t4
	-[0x8000c790]:csrrs a2, fcsr, zero
	-[0x8000c794]:sd t6, 1760(fp)
	-[0x8000c798]:sd a2, 1768(fp)
Current Store : [0x8000c798] : sd a2, 1768(fp) -- Store: [0x80016690]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfd and fm1 == 0x76411d and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c7c4]:fmax.s t6, t5, t4
	-[0x8000c7c8]:csrrs a2, fcsr, zero
	-[0x8000c7cc]:sd t6, 1776(fp)
	-[0x8000c7d0]:sd a2, 1784(fp)
Current Store : [0x8000c7d0] : sd a2, 1784(fp) -- Store: [0x800166a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c7fc]:fmax.s t6, t5, t4
	-[0x8000c800]:csrrs a2, fcsr, zero
	-[0x8000c804]:sd t6, 1792(fp)
	-[0x8000c808]:sd a2, 1800(fp)
Current Store : [0x8000c808] : sd a2, 1800(fp) -- Store: [0x800166b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfa and fm1 == 0x4500e4 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c834]:fmax.s t6, t5, t4
	-[0x8000c838]:csrrs a2, fcsr, zero
	-[0x8000c83c]:sd t6, 1808(fp)
	-[0x8000c840]:sd a2, 1816(fp)
Current Store : [0x8000c840] : sd a2, 1816(fp) -- Store: [0x800166c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c86c]:fmax.s t6, t5, t4
	-[0x8000c870]:csrrs a2, fcsr, zero
	-[0x8000c874]:sd t6, 1824(fp)
	-[0x8000c878]:sd a2, 1832(fp)
Current Store : [0x8000c878] : sd a2, 1832(fp) -- Store: [0x800166d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c8a4]:fmax.s t6, t5, t4
	-[0x8000c8a8]:csrrs a2, fcsr, zero
	-[0x8000c8ac]:sd t6, 1840(fp)
	-[0x8000c8b0]:sd a2, 1848(fp)
Current Store : [0x8000c8b0] : sd a2, 1848(fp) -- Store: [0x800166e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c8dc]:fmax.s t6, t5, t4
	-[0x8000c8e0]:csrrs a2, fcsr, zero
	-[0x8000c8e4]:sd t6, 1856(fp)
	-[0x8000c8e8]:sd a2, 1864(fp)
Current Store : [0x8000c8e8] : sd a2, 1864(fp) -- Store: [0x800166f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x7f and fm1 == 0x517d72 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c914]:fmax.s t6, t5, t4
	-[0x8000c918]:csrrs a2, fcsr, zero
	-[0x8000c91c]:sd t6, 1872(fp)
	-[0x8000c920]:sd a2, 1880(fp)
Current Store : [0x8000c920] : sd a2, 1880(fp) -- Store: [0x80016700]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x517d72 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c94c]:fmax.s t6, t5, t4
	-[0x8000c950]:csrrs a2, fcsr, zero
	-[0x8000c954]:sd t6, 1888(fp)
	-[0x8000c958]:sd a2, 1896(fp)
Current Store : [0x8000c958] : sd a2, 1896(fp) -- Store: [0x80016710]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x11d249 and fs2 == 1 and fe2 == 0x7f and fm2 == 0x517d72 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000c984]:fmax.s t6, t5, t4
	-[0x8000c988]:csrrs a2, fcsr, zero
	-[0x8000c98c]:sd t6, 1904(fp)
	-[0x8000c990]:sd a2, 1912(fp)
Current Store : [0x8000c990] : sd a2, 1912(fp) -- Store: [0x80016720]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c9bc]:fmax.s t6, t5, t4
	-[0x8000c9c0]:csrrs a2, fcsr, zero
	-[0x8000c9c4]:sd t6, 1920(fp)
	-[0x8000c9c8]:sd a2, 1928(fp)
Current Store : [0x8000c9c8] : sd a2, 1928(fp) -- Store: [0x80016730]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000c9f4]:fmax.s t6, t5, t4
	-[0x8000c9f8]:csrrs a2, fcsr, zero
	-[0x8000c9fc]:sd t6, 1936(fp)
	-[0x8000ca00]:sd a2, 1944(fp)
Current Store : [0x8000ca00] : sd a2, 1944(fp) -- Store: [0x80016740]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfc and fm2 == 0x2b7553 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ca2c]:fmax.s t6, t5, t4
	-[0x8000ca30]:csrrs a2, fcsr, zero
	-[0x8000ca34]:sd t6, 1952(fp)
	-[0x8000ca38]:sd a2, 1960(fp)
Current Store : [0x8000ca38] : sd a2, 1960(fp) -- Store: [0x80016750]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000ca64]:fmax.s t6, t5, t4
	-[0x8000ca68]:csrrs a2, fcsr, zero
	-[0x8000ca6c]:sd t6, 1968(fp)
	-[0x8000ca70]:sd a2, 1976(fp)
Current Store : [0x8000ca70] : sd a2, 1976(fp) -- Store: [0x80016760]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ca9c]:fmax.s t6, t5, t4
	-[0x8000caa0]:csrrs a2, fcsr, zero
	-[0x8000caa4]:sd t6, 1984(fp)
	-[0x8000caa8]:sd a2, 1992(fp)
Current Store : [0x8000caa8] : sd a2, 1992(fp) -- Store: [0x80016770]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfe and fm2 == 0x7fffff and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cadc]:fmax.s t6, t5, t4
	-[0x8000cae0]:csrrs a2, fcsr, zero
	-[0x8000cae4]:sd t6, 2000(fp)
	-[0x8000cae8]:sd a2, 2008(fp)
Current Store : [0x8000cae8] : sd a2, 2008(fp) -- Store: [0x80016780]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cb1c]:fmax.s t6, t5, t4
	-[0x8000cb20]:csrrs a2, fcsr, zero
	-[0x8000cb24]:sd t6, 2016(fp)
	-[0x8000cb28]:sd a2, 2024(fp)
Current Store : [0x8000cb28] : sd a2, 2024(fp) -- Store: [0x80016790]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cb5c]:fmax.s t6, t5, t4
	-[0x8000cb60]:csrrs a2, fcsr, zero
	-[0x8000cb64]:sd t6, 2032(fp)
	-[0x8000cb68]:sd a2, 2040(fp)
Current Store : [0x8000cb68] : sd a2, 2040(fp) -- Store: [0x800167a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cba4]:fmax.s t6, t5, t4
	-[0x8000cba8]:csrrs a2, fcsr, zero
	-[0x8000cbac]:sd t6, 0(fp)
	-[0x8000cbb0]:sd a2, 8(fp)
Current Store : [0x8000cbb0] : sd a2, 8(fp) -- Store: [0x800167b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cbe4]:fmax.s t6, t5, t4
	-[0x8000cbe8]:csrrs a2, fcsr, zero
	-[0x8000cbec]:sd t6, 16(fp)
	-[0x8000cbf0]:sd a2, 24(fp)
Current Store : [0x8000cbf0] : sd a2, 24(fp) -- Store: [0x800167c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cc24]:fmax.s t6, t5, t4
	-[0x8000cc28]:csrrs a2, fcsr, zero
	-[0x8000cc2c]:sd t6, 32(fp)
	-[0x8000cc30]:sd a2, 40(fp)
Current Store : [0x8000cc30] : sd a2, 40(fp) -- Store: [0x800167d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cc64]:fmax.s t6, t5, t4
	-[0x8000cc68]:csrrs a2, fcsr, zero
	-[0x8000cc6c]:sd t6, 48(fp)
	-[0x8000cc70]:sd a2, 56(fp)
Current Store : [0x8000cc70] : sd a2, 56(fp) -- Store: [0x800167e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cca4]:fmax.s t6, t5, t4
	-[0x8000cca8]:csrrs a2, fcsr, zero
	-[0x8000ccac]:sd t6, 64(fp)
	-[0x8000ccb0]:sd a2, 72(fp)
Current Store : [0x8000ccb0] : sd a2, 72(fp) -- Store: [0x800167f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cce4]:fmax.s t6, t5, t4
	-[0x8000cce8]:csrrs a2, fcsr, zero
	-[0x8000ccec]:sd t6, 80(fp)
	-[0x8000ccf0]:sd a2, 88(fp)
Current Store : [0x8000ccf0] : sd a2, 88(fp) -- Store: [0x80016800]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cd24]:fmax.s t6, t5, t4
	-[0x8000cd28]:csrrs a2, fcsr, zero
	-[0x8000cd2c]:sd t6, 96(fp)
	-[0x8000cd30]:sd a2, 104(fp)
Current Store : [0x8000cd30] : sd a2, 104(fp) -- Store: [0x80016810]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cd64]:fmax.s t6, t5, t4
	-[0x8000cd68]:csrrs a2, fcsr, zero
	-[0x8000cd6c]:sd t6, 112(fp)
	-[0x8000cd70]:sd a2, 120(fp)
Current Store : [0x8000cd70] : sd a2, 120(fp) -- Store: [0x80016820]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cda4]:fmax.s t6, t5, t4
	-[0x8000cda8]:csrrs a2, fcsr, zero
	-[0x8000cdac]:sd t6, 128(fp)
	-[0x8000cdb0]:sd a2, 136(fp)
Current Store : [0x8000cdb0] : sd a2, 136(fp) -- Store: [0x80016830]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0xfc and fm1 == 0x2b7553 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cde4]:fmax.s t6, t5, t4
	-[0x8000cde8]:csrrs a2, fcsr, zero
	-[0x8000cdec]:sd t6, 144(fp)
	-[0x8000cdf0]:sd a2, 152(fp)
Current Store : [0x8000cdf0] : sd a2, 152(fp) -- Store: [0x80016840]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ce24]:fmax.s t6, t5, t4
	-[0x8000ce28]:csrrs a2, fcsr, zero
	-[0x8000ce2c]:sd t6, 160(fp)
	-[0x8000ce30]:sd a2, 168(fp)
Current Store : [0x8000ce30] : sd a2, 168(fp) -- Store: [0x80016850]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x81 and fm1 == 0x365363 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000ce64]:fmax.s t6, t5, t4
	-[0x8000ce68]:csrrs a2, fcsr, zero
	-[0x8000ce6c]:sd t6, 176(fp)
	-[0x8000ce70]:sd a2, 184(fp)
Current Store : [0x8000ce70] : sd a2, 184(fp) -- Store: [0x80016860]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x81 and fm2 == 0x365363 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cea4]:fmax.s t6, t5, t4
	-[0x8000cea8]:csrrs a2, fcsr, zero
	-[0x8000ceac]:sd t6, 192(fp)
	-[0x8000ceb0]:sd a2, 200(fp)
Current Store : [0x8000ceb0] : sd a2, 200(fp) -- Store: [0x80016870]:0x0000000000000000




Last Coverpoint : ['fs1 == 1 and fe1 == 0x00 and fm1 == 0x3e0acf and fs2 == 1 and fe2 == 0x81 and fm2 == 0x365363 and  fcsr == 0 and rs1_sgn_prefix == 0xffffffff and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000cee4]:fmax.s t6, t5, t4
	-[0x8000cee8]:csrrs a2, fcsr, zero
	-[0x8000ceec]:sd t6, 208(fp)
	-[0x8000cef0]:sd a2, 216(fp)
Current Store : [0x8000cef0] : sd a2, 216(fp) -- Store: [0x80016880]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x4e622b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cf24]:fmax.s t6, t5, t4
	-[0x8000cf28]:csrrs a2, fcsr, zero
	-[0x8000cf2c]:sd t6, 224(fp)
	-[0x8000cf30]:sd a2, 232(fp)
Current Store : [0x8000cf30] : sd a2, 232(fp) -- Store: [0x80016890]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x0007f0 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cf64]:fmax.s t6, t5, t4
	-[0x8000cf68]:csrrs a2, fcsr, zero
	-[0x8000cf6c]:sd t6, 240(fp)
	-[0x8000cf70]:sd a2, 248(fp)
Current Store : [0x8000cf70] : sd a2, 248(fp) -- Store: [0x800168a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cfa4]:fmax.s t6, t5, t4
	-[0x8000cfa8]:csrrs a2, fcsr, zero
	-[0x8000cfac]:sd t6, 256(fp)
	-[0x8000cfb0]:sd a2, 264(fp)
Current Store : [0x8000cfb0] : sd a2, 264(fp) -- Store: [0x800168b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0xfb and fm2 == 0x102b16 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000cfe4]:fmax.s t6, t5, t4
	-[0x8000cfe8]:csrrs a2, fcsr, zero
	-[0x8000cfec]:sd t6, 272(fp)
	-[0x8000cff0]:sd a2, 280(fp)
Current Store : [0x8000cff0] : sd a2, 280(fp) -- Store: [0x800168c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0xfe and fm2 == 0x125b96 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d024]:fmax.s t6, t5, t4
	-[0x8000d028]:csrrs a2, fcsr, zero
	-[0x8000d02c]:sd t6, 288(fp)
	-[0x8000d030]:sd a2, 296(fp)
Current Store : [0x8000d030] : sd a2, 296(fp) -- Store: [0x800168d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0xfc and fm2 == 0x2fb07b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d064]:fmax.s t6, t5, t4
	-[0x8000d068]:csrrs a2, fcsr, zero
	-[0x8000d06c]:sd t6, 304(fp)
	-[0x8000d070]:sd a2, 312(fp)
Current Store : [0x8000d070] : sd a2, 312(fp) -- Store: [0x800168e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0xfb and fm2 == 0x6a577e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d0a4]:fmax.s t6, t5, t4
	-[0x8000d0a8]:csrrs a2, fcsr, zero
	-[0x8000d0ac]:sd t6, 320(fp)
	-[0x8000d0b0]:sd a2, 328(fp)
Current Store : [0x8000d0b0] : sd a2, 328(fp) -- Store: [0x800168f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0xfd and fm2 == 0x53653a and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d0e4]:fmax.s t6, t5, t4
	-[0x8000d0e8]:csrrs a2, fcsr, zero
	-[0x8000d0ec]:sd t6, 336(fp)
	-[0x8000d0f0]:sd a2, 344(fp)
Current Store : [0x8000d0f0] : sd a2, 344(fp) -- Store: [0x80016900]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3a8ea9 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d124]:fmax.s t6, t5, t4
	-[0x8000d128]:csrrs a2, fcsr, zero
	-[0x8000d12c]:sd t6, 352(fp)
	-[0x8000d130]:sd a2, 360(fp)
Current Store : [0x8000d130] : sd a2, 360(fp) -- Store: [0x80016910]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0xfe and fm2 == 0x3f987b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d164]:fmax.s t6, t5, t4
	-[0x8000d168]:csrrs a2, fcsr, zero
	-[0x8000d16c]:sd t6, 368(fp)
	-[0x8000d170]:sd a2, 376(fp)
Current Store : [0x8000d170] : sd a2, 376(fp) -- Store: [0x80016920]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0xfa and fm2 == 0x2820df and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d1a4]:fmax.s t6, t5, t4
	-[0x8000d1a8]:csrrs a2, fcsr, zero
	-[0x8000d1ac]:sd t6, 384(fp)
	-[0x8000d1b0]:sd a2, 392(fp)
Current Store : [0x8000d1b0] : sd a2, 392(fp) -- Store: [0x80016930]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d1e4]:fmax.s t6, t5, t4
	-[0x8000d1e8]:csrrs a2, fcsr, zero
	-[0x8000d1ec]:sd t6, 400(fp)
	-[0x8000d1f0]:sd a2, 408(fp)
Current Store : [0x8000d1f0] : sd a2, 408(fp) -- Store: [0x80016940]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x29b3b2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d224]:fmax.s t6, t5, t4
	-[0x8000d228]:csrrs a2, fcsr, zero
	-[0x8000d22c]:sd t6, 416(fp)
	-[0x8000d230]:sd a2, 424(fp)
Current Store : [0x8000d230] : sd a2, 424(fp) -- Store: [0x80016950]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x357d2c and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d264]:fmax.s t6, t5, t4
	-[0x8000d268]:csrrs a2, fcsr, zero
	-[0x8000d26c]:sd t6, 432(fp)
	-[0x8000d270]:sd a2, 440(fp)
Current Store : [0x8000d270] : sd a2, 440(fp) -- Store: [0x80016960]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x1c8139 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d2a4]:fmax.s t6, t5, t4
	-[0x8000d2a8]:csrrs a2, fcsr, zero
	-[0x8000d2ac]:sd t6, 448(fp)
	-[0x8000d2b0]:sd a2, 456(fp)
Current Store : [0x8000d2b0] : sd a2, 456(fp) -- Store: [0x80016970]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x39b0fc and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d2e4]:fmax.s t6, t5, t4
	-[0x8000d2e8]:csrrs a2, fcsr, zero
	-[0x8000d2ec]:sd t6, 464(fp)
	-[0x8000d2f0]:sd a2, 472(fp)
Current Store : [0x8000d2f0] : sd a2, 472(fp) -- Store: [0x80016980]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x2facf2 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d324]:fmax.s t6, t5, t4
	-[0x8000d328]:csrrs a2, fcsr, zero
	-[0x8000d32c]:sd t6, 480(fp)
	-[0x8000d330]:sd a2, 488(fp)
Current Store : [0x8000d330] : sd a2, 488(fp) -- Store: [0x80016990]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x0d858e and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d364]:fmax.s t6, t5, t4
	-[0x8000d368]:csrrs a2, fcsr, zero
	-[0x8000d36c]:sd t6, 496(fp)
	-[0x8000d370]:sd a2, 504(fp)
Current Store : [0x8000d370] : sd a2, 504(fp) -- Store: [0x800169a0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x244d8b and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d3a4]:fmax.s t6, t5, t4
	-[0x8000d3a8]:csrrs a2, fcsr, zero
	-[0x8000d3ac]:sd t6, 512(fp)
	-[0x8000d3b0]:sd a2, 520(fp)
Current Store : [0x8000d3b0] : sd a2, 520(fp) -- Store: [0x800169b0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x11d249 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d3e4]:fmax.s t6, t5, t4
	-[0x8000d3e8]:csrrs a2, fcsr, zero
	-[0x8000d3ec]:sd t6, 528(fp)
	-[0x8000d3f0]:sd a2, 536(fp)
Current Store : [0x8000d3f0] : sd a2, 536(fp) -- Store: [0x800169c0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0007f0 and fs2 == 1 and fe2 == 0x00 and fm2 == 0x3e0acf and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0xffffffff  #nosat']
Last Code Sequence : 
	-[0x8000d424]:fmax.s t6, t5, t4
	-[0x8000d428]:csrrs a2, fcsr, zero
	-[0x8000d42c]:sd t6, 544(fp)
	-[0x8000d430]:sd a2, 552(fp)
Current Store : [0x8000d430] : sd a2, 552(fp) -- Store: [0x800169d0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d464]:fmax.s t6, t5, t4
	-[0x8000d468]:csrrs a2, fcsr, zero
	-[0x8000d46c]:sd t6, 560(fp)
	-[0x8000d470]:sd a2, 568(fp)
Current Store : [0x8000d470] : sd a2, 568(fp) -- Store: [0x800169e0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and fs2 == 0 and fe2 == 0xfd and fm2 == 0x3e3f3f and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d4a4]:fmax.s t6, t5, t4
	-[0x8000d4a8]:csrrs a2, fcsr, zero
	-[0x8000d4ac]:sd t6, 576(fp)
	-[0x8000d4b0]:sd a2, 584(fp)
Current Store : [0x8000d4b0] : sd a2, 584(fp) -- Store: [0x800169f0]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfb and fm1 == 0x4e622b and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d4e4]:fmax.s t6, t5, t4
	-[0x8000d4e8]:csrrs a2, fcsr, zero
	-[0x8000d4ec]:sd t6, 592(fp)
	-[0x8000d4f0]:sd a2, 600(fp)
Current Store : [0x8000d4f0] : sd a2, 600(fp) -- Store: [0x80016a00]:0x0000000000000000




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfc and fm1 == 0x07167c and fs2 == 0 and fe2 == 0x00 and fm2 == 0x255707 and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d524]:fmax.s t6, t5, t4
	-[0x8000d528]:csrrs a2, fcsr, zero
	-[0x8000d52c]:sd t6, 608(fp)
	-[0x8000d530]:sd a2, 616(fp)
Current Store : [0x8000d530] : sd a2, 616(fp) -- Store: [0x80016a10]:0x0000000000000000




Last Coverpoint : ['mnemonic : fmax.s', 'rs1 : x30', 'rs2 : x29', 'rd : x31', 'rs1 != rs2  and rs1 != rd and rs2 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x255707 and fs2 == 0 and fe2 == 0x00 and fm2 == 0x30e1ae and  fcsr == 0 and rs1_sgn_prefix == 0x00000000 and rs2_sgn_prefix == 0x00000000  #nosat']
Last Code Sequence : 
	-[0x8000d564]:fmax.s t6, t5, t4
	-[0x8000d568]:csrrs a2, fcsr, zero
	-[0x8000d56c]:sd t6, 624(fp)
	-[0x8000d570]:sd a2, 632(fp)
Current Store : [0x8000d570] : sd a2, 632(fp) -- Store: [0x80016a20]:0x0000000000000000





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
