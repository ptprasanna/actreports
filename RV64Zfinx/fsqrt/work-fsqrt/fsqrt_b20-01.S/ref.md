
# Data Propagation Report

- **STAT1** : Number of instructions that hit unique coverpoints and update the signature
- **STAT2** : Number of instructions that hit covepoints which are not unique but still update the signature (completely or partially)
- **STAT3** : Number of instructions that hit a unique coverpoint but do not update the signature completely
- **STAT4** : Number of multiple signature updates for the same coverpoint
- **STAT5** : Number of times the signature was overwritten

| Param                     | Value    |
|---------------------------|----------|
| XLEN                      | 64      |
| TEST_REGION               | [('0x80000390', '0x80000b10')]      |
| SIG_REGION                | [('0x80002410', '0x80002640', '70 dwords')]      |
| COV_LABELS                | fsqrt_b20      |
| TEST_NAME                 | /home/riscv/riscv-ctg/RV64Zfinx-rvopcodesdecoder/work-fsqrt/fsqrt_b20-01.S/ref.S    |
| Total Number of coverpoints| 132     |
| Total Coverpoints Hit     | 132      |
| Total Signature Updates   | 96      |
| STAT1                     | 0      |
| STAT2                     | 0      |
| STAT3                     | 66     |
| STAT4                     | 48     |
| STAT5                     | 0     |

## Details for STAT2:

```


```

## Details of STAT3

```
[0x800003b8]:fsqrt.s t6, t6, dyn
[0x800003bc]:csrrs tp, fcsr, zero
[0x800003c0]:sd t6, 0(ra)
[0x800003c4]:sd tp, 8(ra)
[0x800003c8]:ld t4, 8(gp)
[0x800003cc]:addi sp, zero, 0
[0x800003d0]:csrrw zero, fcsr, sp
[0x800003d4]:fsqrt.s t5, t4, dyn

[0x800003d4]:fsqrt.s t5, t4, dyn
[0x800003d8]:csrrs tp, fcsr, zero
[0x800003dc]:sd t5, 16(ra)
[0x800003e0]:sd tp, 24(ra)
[0x800003e4]:ld t5, 16(gp)
[0x800003e8]:addi sp, zero, 0
[0x800003ec]:csrrw zero, fcsr, sp
[0x800003f0]:fsqrt.s t4, t5, dyn

[0x800003f0]:fsqrt.s t4, t5, dyn
[0x800003f4]:csrrs tp, fcsr, zero
[0x800003f8]:sd t4, 32(ra)
[0x800003fc]:sd tp, 40(ra)
[0x80000400]:ld s11, 24(gp)
[0x80000404]:addi sp, zero, 0
[0x80000408]:csrrw zero, fcsr, sp
[0x8000040c]:fsqrt.s t3, s11, dyn

[0x8000040c]:fsqrt.s t3, s11, dyn
[0x80000410]:csrrs tp, fcsr, zero
[0x80000414]:sd t3, 48(ra)
[0x80000418]:sd tp, 56(ra)
[0x8000041c]:ld t3, 32(gp)
[0x80000420]:addi sp, zero, 0
[0x80000424]:csrrw zero, fcsr, sp
[0x80000428]:fsqrt.s s11, t3, dyn

[0x80000428]:fsqrt.s s11, t3, dyn
[0x8000042c]:csrrs tp, fcsr, zero
[0x80000430]:sd s11, 64(ra)
[0x80000434]:sd tp, 72(ra)
[0x80000438]:ld s9, 40(gp)
[0x8000043c]:addi sp, zero, 0
[0x80000440]:csrrw zero, fcsr, sp
[0x80000444]:fsqrt.s s10, s9, dyn

[0x80000444]:fsqrt.s s10, s9, dyn
[0x80000448]:csrrs tp, fcsr, zero
[0x8000044c]:sd s10, 80(ra)
[0x80000450]:sd tp, 88(ra)
[0x80000454]:ld s10, 48(gp)
[0x80000458]:addi sp, zero, 0
[0x8000045c]:csrrw zero, fcsr, sp
[0x80000460]:fsqrt.s s9, s10, dyn

[0x80000460]:fsqrt.s s9, s10, dyn
[0x80000464]:csrrs tp, fcsr, zero
[0x80000468]:sd s9, 96(ra)
[0x8000046c]:sd tp, 104(ra)
[0x80000470]:ld s7, 56(gp)
[0x80000474]:addi sp, zero, 0
[0x80000478]:csrrw zero, fcsr, sp
[0x8000047c]:fsqrt.s s8, s7, dyn

[0x8000047c]:fsqrt.s s8, s7, dyn
[0x80000480]:csrrs tp, fcsr, zero
[0x80000484]:sd s8, 112(ra)
[0x80000488]:sd tp, 120(ra)
[0x8000048c]:ld s8, 64(gp)
[0x80000490]:addi sp, zero, 0
[0x80000494]:csrrw zero, fcsr, sp
[0x80000498]:fsqrt.s s7, s8, dyn

[0x80000498]:fsqrt.s s7, s8, dyn
[0x8000049c]:csrrs tp, fcsr, zero
[0x800004a0]:sd s7, 128(ra)
[0x800004a4]:sd tp, 136(ra)
[0x800004a8]:ld s5, 72(gp)
[0x800004ac]:addi sp, zero, 0
[0x800004b0]:csrrw zero, fcsr, sp
[0x800004b4]:fsqrt.s s6, s5, dyn

[0x800004b4]:fsqrt.s s6, s5, dyn
[0x800004b8]:csrrs tp, fcsr, zero
[0x800004bc]:sd s6, 144(ra)
[0x800004c0]:sd tp, 152(ra)
[0x800004c4]:ld s6, 80(gp)
[0x800004c8]:addi sp, zero, 0
[0x800004cc]:csrrw zero, fcsr, sp
[0x800004d0]:fsqrt.s s5, s6, dyn

[0x800004d0]:fsqrt.s s5, s6, dyn
[0x800004d4]:csrrs tp, fcsr, zero
[0x800004d8]:sd s5, 160(ra)
[0x800004dc]:sd tp, 168(ra)
[0x800004e0]:ld s3, 88(gp)
[0x800004e4]:addi sp, zero, 0
[0x800004e8]:csrrw zero, fcsr, sp
[0x800004ec]:fsqrt.s s4, s3, dyn

[0x800004ec]:fsqrt.s s4, s3, dyn
[0x800004f0]:csrrs tp, fcsr, zero
[0x800004f4]:sd s4, 176(ra)
[0x800004f8]:sd tp, 184(ra)
[0x800004fc]:ld s4, 96(gp)
[0x80000500]:addi sp, zero, 0
[0x80000504]:csrrw zero, fcsr, sp
[0x80000508]:fsqrt.s s3, s4, dyn

[0x80000508]:fsqrt.s s3, s4, dyn
[0x8000050c]:csrrs tp, fcsr, zero
[0x80000510]:sd s3, 192(ra)
[0x80000514]:sd tp, 200(ra)
[0x80000518]:ld a7, 104(gp)
[0x8000051c]:addi sp, zero, 0
[0x80000520]:csrrw zero, fcsr, sp
[0x80000524]:fsqrt.s s2, a7, dyn

[0x80000524]:fsqrt.s s2, a7, dyn
[0x80000528]:csrrs tp, fcsr, zero
[0x8000052c]:sd s2, 208(ra)
[0x80000530]:sd tp, 216(ra)
[0x80000534]:ld s2, 112(gp)
[0x80000538]:addi sp, zero, 0
[0x8000053c]:csrrw zero, fcsr, sp
[0x80000540]:fsqrt.s a7, s2, dyn

[0x80000540]:fsqrt.s a7, s2, dyn
[0x80000544]:csrrs tp, fcsr, zero
[0x80000548]:sd a7, 224(ra)
[0x8000054c]:sd tp, 232(ra)
[0x80000550]:ld a5, 120(gp)
[0x80000554]:addi sp, zero, 0
[0x80000558]:csrrw zero, fcsr, sp
[0x8000055c]:fsqrt.s a6, a5, dyn

[0x8000055c]:fsqrt.s a6, a5, dyn
[0x80000560]:csrrs tp, fcsr, zero
[0x80000564]:sd a6, 240(ra)
[0x80000568]:sd tp, 248(ra)
[0x8000056c]:ld a6, 128(gp)
[0x80000570]:addi sp, zero, 0
[0x80000574]:csrrw zero, fcsr, sp
[0x80000578]:fsqrt.s a5, a6, dyn

[0x80000578]:fsqrt.s a5, a6, dyn
[0x8000057c]:csrrs tp, fcsr, zero
[0x80000580]:sd a5, 256(ra)
[0x80000584]:sd tp, 264(ra)
[0x80000588]:ld a3, 136(gp)
[0x8000058c]:addi sp, zero, 0
[0x80000590]:csrrw zero, fcsr, sp
[0x80000594]:fsqrt.s a4, a3, dyn

[0x80000594]:fsqrt.s a4, a3, dyn
[0x80000598]:csrrs tp, fcsr, zero
[0x8000059c]:sd a4, 272(ra)
[0x800005a0]:sd tp, 280(ra)
[0x800005a4]:ld a4, 144(gp)
[0x800005a8]:addi sp, zero, 0
[0x800005ac]:csrrw zero, fcsr, sp
[0x800005b0]:fsqrt.s a3, a4, dyn

[0x800005b0]:fsqrt.s a3, a4, dyn
[0x800005b4]:csrrs tp, fcsr, zero
[0x800005b8]:sd a3, 288(ra)
[0x800005bc]:sd tp, 296(ra)
[0x800005c0]:ld a1, 152(gp)
[0x800005c4]:addi sp, zero, 0
[0x800005c8]:csrrw zero, fcsr, sp
[0x800005cc]:fsqrt.s a2, a1, dyn

[0x800005cc]:fsqrt.s a2, a1, dyn
[0x800005d0]:csrrs tp, fcsr, zero
[0x800005d4]:sd a2, 304(ra)
[0x800005d8]:sd tp, 312(ra)
[0x800005dc]:ld a2, 160(gp)
[0x800005e0]:addi sp, zero, 0
[0x800005e4]:csrrw zero, fcsr, sp
[0x800005e8]:fsqrt.s a1, a2, dyn

[0x800005e8]:fsqrt.s a1, a2, dyn
[0x800005ec]:csrrs tp, fcsr, zero
[0x800005f0]:sd a1, 320(ra)
[0x800005f4]:sd tp, 328(ra)
[0x800005f8]:ld s1, 168(gp)
[0x800005fc]:addi sp, zero, 0
[0x80000600]:csrrw zero, fcsr, sp
[0x80000604]:fsqrt.s a0, s1, dyn

[0x80000604]:fsqrt.s a0, s1, dyn
[0x80000608]:csrrs tp, fcsr, zero
[0x8000060c]:sd a0, 336(ra)
[0x80000610]:sd tp, 344(ra)
[0x80000614]:ld a0, 176(gp)
[0x80000618]:addi sp, zero, 0
[0x8000061c]:csrrw zero, fcsr, sp
[0x80000620]:fsqrt.s s1, a0, dyn

[0x80000620]:fsqrt.s s1, a0, dyn
[0x80000624]:csrrs tp, fcsr, zero
[0x80000628]:sd s1, 352(ra)
[0x8000062c]:sd tp, 360(ra)
[0x80000630]:ld t2, 184(gp)
[0x80000634]:addi sp, zero, 0
[0x80000638]:csrrw zero, fcsr, sp
[0x8000063c]:fsqrt.s fp, t2, dyn

[0x8000063c]:fsqrt.s fp, t2, dyn
[0x80000640]:csrrs tp, fcsr, zero
[0x80000644]:sd fp, 368(ra)
[0x80000648]:sd tp, 376(ra)
[0x8000064c]:auipc s1, 2
[0x80000650]:addi s1, s1, 2692
[0x80000654]:ld fp, 0(s1)
[0x80000658]:addi sp, zero, 0
[0x8000065c]:csrrw zero, fcsr, sp
[0x80000660]:fsqrt.s t2, fp, dyn

[0x80000660]:fsqrt.s t2, fp, dyn
[0x80000664]:csrrs a0, fcsr, zero
[0x80000668]:sd t2, 384(ra)
[0x8000066c]:sd a0, 392(ra)
[0x80000670]:ld t0, 8(s1)
[0x80000674]:addi sp, zero, 0
[0x80000678]:csrrw zero, fcsr, sp
[0x8000067c]:fsqrt.s t1, t0, dyn

[0x8000067c]:fsqrt.s t1, t0, dyn
[0x80000680]:csrrs a0, fcsr, zero
[0x80000684]:sd t1, 400(ra)
[0x80000688]:sd a0, 408(ra)
[0x8000068c]:auipc t2, 2
[0x80000690]:addi t2, t2, 3676
[0x80000694]:ld t1, 16(s1)
[0x80000698]:addi fp, zero, 0
[0x8000069c]:csrrw zero, fcsr, fp
[0x800006a0]:fsqrt.s t0, t1, dyn

[0x800006a0]:fsqrt.s t0, t1, dyn
[0x800006a4]:csrrs a0, fcsr, zero
[0x800006a8]:sd t0, 0(t2)
[0x800006ac]:sd a0, 8(t2)
[0x800006b0]:ld gp, 24(s1)
[0x800006b4]:addi fp, zero, 0
[0x800006b8]:csrrw zero, fcsr, fp
[0x800006bc]:fsqrt.s tp, gp, dyn

[0x800006bc]:fsqrt.s tp, gp, dyn
[0x800006c0]:csrrs a0, fcsr, zero
[0x800006c4]:sd tp, 16(t2)
[0x800006c8]:sd a0, 24(t2)
[0x800006cc]:ld tp, 32(s1)
[0x800006d0]:addi fp, zero, 0
[0x800006d4]:csrrw zero, fcsr, fp
[0x800006d8]:fsqrt.s gp, tp, dyn

[0x800006d8]:fsqrt.s gp, tp, dyn
[0x800006dc]:csrrs a0, fcsr, zero
[0x800006e0]:sd gp, 32(t2)
[0x800006e4]:sd a0, 40(t2)
[0x800006e8]:ld ra, 40(s1)
[0x800006ec]:addi fp, zero, 0
[0x800006f0]:csrrw zero, fcsr, fp
[0x800006f4]:fsqrt.s sp, ra, dyn

[0x800006f4]:fsqrt.s sp, ra, dyn
[0x800006f8]:csrrs a0, fcsr, zero
[0x800006fc]:sd sp, 48(t2)
[0x80000700]:sd a0, 56(t2)
[0x80000704]:ld sp, 48(s1)
[0x80000708]:addi fp, zero, 0
[0x8000070c]:csrrw zero, fcsr, fp
[0x80000710]:fsqrt.s ra, sp, dyn

[0x80000710]:fsqrt.s ra, sp, dyn
[0x80000714]:csrrs a0, fcsr, zero
[0x80000718]:sd ra, 64(t2)
[0x8000071c]:sd a0, 72(t2)
[0x80000720]:ld zero, 56(s1)
[0x80000724]:addi fp, zero, 0
[0x80000728]:csrrw zero, fcsr, fp
[0x8000072c]:fsqrt.s t6, zero, dyn

[0x8000072c]:fsqrt.s t6, zero, dyn
[0x80000730]:csrrs a0, fcsr, zero
[0x80000734]:sd t6, 80(t2)
[0x80000738]:sd a0, 88(t2)
[0x8000073c]:ld t6, 64(s1)
[0x80000740]:addi fp, zero, 0
[0x80000744]:csrrw zero, fcsr, fp
[0x80000748]:fsqrt.s zero, t6, dyn

[0x80000748]:fsqrt.s zero, t6, dyn
[0x8000074c]:csrrs a0, fcsr, zero
[0x80000750]:sd zero, 96(t2)
[0x80000754]:sd a0, 104(t2)
[0x80000758]:ld t5, 72(s1)
[0x8000075c]:addi fp, zero, 0
[0x80000760]:csrrw zero, fcsr, fp
[0x80000764]:fsqrt.s t6, t5, dyn

[0x80000764]:fsqrt.s t6, t5, dyn
[0x80000768]:csrrs a0, fcsr, zero
[0x8000076c]:sd t6, 112(t2)
[0x80000770]:sd a0, 120(t2)
[0x80000774]:ld t5, 80(s1)
[0x80000778]:addi fp, zero, 0
[0x8000077c]:csrrw zero, fcsr, fp
[0x80000780]:fsqrt.s t6, t5, dyn

[0x80000780]:fsqrt.s t6, t5, dyn
[0x80000784]:csrrs a0, fcsr, zero
[0x80000788]:sd t6, 128(t2)
[0x8000078c]:sd a0, 136(t2)
[0x80000790]:ld t5, 88(s1)
[0x80000794]:addi fp, zero, 0
[0x80000798]:csrrw zero, fcsr, fp
[0x8000079c]:fsqrt.s t6, t5, dyn

[0x8000079c]:fsqrt.s t6, t5, dyn
[0x800007a0]:csrrs a0, fcsr, zero
[0x800007a4]:sd t6, 144(t2)
[0x800007a8]:sd a0, 152(t2)
[0x800007ac]:ld t5, 96(s1)
[0x800007b0]:addi fp, zero, 0
[0x800007b4]:csrrw zero, fcsr, fp
[0x800007b8]:fsqrt.s t6, t5, dyn

[0x800007b8]:fsqrt.s t6, t5, dyn
[0x800007bc]:csrrs a0, fcsr, zero
[0x800007c0]:sd t6, 160(t2)
[0x800007c4]:sd a0, 168(t2)
[0x800007c8]:ld t5, 104(s1)
[0x800007cc]:addi fp, zero, 0
[0x800007d0]:csrrw zero, fcsr, fp
[0x800007d4]:fsqrt.s t6, t5, dyn

[0x800007d4]:fsqrt.s t6, t5, dyn
[0x800007d8]:csrrs a0, fcsr, zero
[0x800007dc]:sd t6, 176(t2)
[0x800007e0]:sd a0, 184(t2)
[0x800007e4]:ld t5, 112(s1)
[0x800007e8]:addi fp, zero, 0
[0x800007ec]:csrrw zero, fcsr, fp
[0x800007f0]:fsqrt.s t6, t5, dyn

[0x800007f0]:fsqrt.s t6, t5, dyn
[0x800007f4]:csrrs a0, fcsr, zero
[0x800007f8]:sd t6, 192(t2)
[0x800007fc]:sd a0, 200(t2)
[0x80000800]:ld t5, 120(s1)
[0x80000804]:addi fp, zero, 0
[0x80000808]:csrrw zero, fcsr, fp
[0x8000080c]:fsqrt.s t6, t5, dyn

[0x8000080c]:fsqrt.s t6, t5, dyn
[0x80000810]:csrrs a0, fcsr, zero
[0x80000814]:sd t6, 208(t2)
[0x80000818]:sd a0, 216(t2)
[0x8000081c]:ld t5, 128(s1)
[0x80000820]:addi fp, zero, 0
[0x80000824]:csrrw zero, fcsr, fp
[0x80000828]:fsqrt.s t6, t5, dyn

[0x80000828]:fsqrt.s t6, t5, dyn
[0x8000082c]:csrrs a0, fcsr, zero
[0x80000830]:sd t6, 224(t2)
[0x80000834]:sd a0, 232(t2)
[0x80000838]:ld t5, 136(s1)
[0x8000083c]:addi fp, zero, 0
[0x80000840]:csrrw zero, fcsr, fp
[0x80000844]:fsqrt.s t6, t5, dyn

[0x80000844]:fsqrt.s t6, t5, dyn
[0x80000848]:csrrs a0, fcsr, zero
[0x8000084c]:sd t6, 240(t2)
[0x80000850]:sd a0, 248(t2)
[0x80000854]:ld t5, 144(s1)
[0x80000858]:addi fp, zero, 0
[0x8000085c]:csrrw zero, fcsr, fp
[0x80000860]:fsqrt.s t6, t5, dyn

[0x80000860]:fsqrt.s t6, t5, dyn
[0x80000864]:csrrs a0, fcsr, zero
[0x80000868]:sd t6, 256(t2)
[0x8000086c]:sd a0, 264(t2)
[0x80000870]:ld t5, 152(s1)
[0x80000874]:addi fp, zero, 0
[0x80000878]:csrrw zero, fcsr, fp
[0x8000087c]:fsqrt.s t6, t5, dyn

[0x8000087c]:fsqrt.s t6, t5, dyn
[0x80000880]:csrrs a0, fcsr, zero
[0x80000884]:sd t6, 272(t2)
[0x80000888]:sd a0, 280(t2)
[0x8000088c]:ld t5, 160(s1)
[0x80000890]:addi fp, zero, 0
[0x80000894]:csrrw zero, fcsr, fp
[0x80000898]:fsqrt.s t6, t5, dyn

[0x80000898]:fsqrt.s t6, t5, dyn
[0x8000089c]:csrrs a0, fcsr, zero
[0x800008a0]:sd t6, 288(t2)
[0x800008a4]:sd a0, 296(t2)
[0x800008a8]:ld t5, 168(s1)
[0x800008ac]:addi fp, zero, 0
[0x800008b0]:csrrw zero, fcsr, fp
[0x800008b4]:fsqrt.s t6, t5, dyn

[0x800008b4]:fsqrt.s t6, t5, dyn
[0x800008b8]:csrrs a0, fcsr, zero
[0x800008bc]:sd t6, 304(t2)
[0x800008c0]:sd a0, 312(t2)
[0x800008c4]:ld t5, 176(s1)
[0x800008c8]:addi fp, zero, 0
[0x800008cc]:csrrw zero, fcsr, fp
[0x800008d0]:fsqrt.s t6, t5, dyn

[0x800008d0]:fsqrt.s t6, t5, dyn
[0x800008d4]:csrrs a0, fcsr, zero
[0x800008d8]:sd t6, 320(t2)
[0x800008dc]:sd a0, 328(t2)
[0x800008e0]:ld t5, 184(s1)
[0x800008e4]:addi fp, zero, 0
[0x800008e8]:csrrw zero, fcsr, fp
[0x800008ec]:fsqrt.s t6, t5, dyn

[0x800008ec]:fsqrt.s t6, t5, dyn
[0x800008f0]:csrrs a0, fcsr, zero
[0x800008f4]:sd t6, 336(t2)
[0x800008f8]:sd a0, 344(t2)
[0x800008fc]:ld t5, 192(s1)
[0x80000900]:addi fp, zero, 0
[0x80000904]:csrrw zero, fcsr, fp
[0x80000908]:fsqrt.s t6, t5, dyn

[0x80000908]:fsqrt.s t6, t5, dyn
[0x8000090c]:csrrs a0, fcsr, zero
[0x80000910]:sd t6, 352(t2)
[0x80000914]:sd a0, 360(t2)
[0x80000918]:ld t5, 200(s1)
[0x8000091c]:addi fp, zero, 0
[0x80000920]:csrrw zero, fcsr, fp
[0x80000924]:fsqrt.s t6, t5, dyn

[0x80000924]:fsqrt.s t6, t5, dyn
[0x80000928]:csrrs a0, fcsr, zero
[0x8000092c]:sd t6, 368(t2)
[0x80000930]:sd a0, 376(t2)
[0x80000934]:ld t5, 208(s1)
[0x80000938]:addi fp, zero, 0
[0x8000093c]:csrrw zero, fcsr, fp
[0x80000940]:fsqrt.s t6, t5, dyn

[0x80000940]:fsqrt.s t6, t5, dyn
[0x80000944]:csrrs a0, fcsr, zero
[0x80000948]:sd t6, 384(t2)
[0x8000094c]:sd a0, 392(t2)
[0x80000950]:ld t5, 216(s1)
[0x80000954]:addi fp, zero, 0
[0x80000958]:csrrw zero, fcsr, fp
[0x8000095c]:fsqrt.s t6, t5, dyn

[0x8000095c]:fsqrt.s t6, t5, dyn
[0x80000960]:csrrs a0, fcsr, zero
[0x80000964]:sd t6, 400(t2)
[0x80000968]:sd a0, 408(t2)
[0x8000096c]:ld t5, 224(s1)
[0x80000970]:addi fp, zero, 0
[0x80000974]:csrrw zero, fcsr, fp
[0x80000978]:fsqrt.s t6, t5, dyn

[0x80000978]:fsqrt.s t6, t5, dyn
[0x8000097c]:csrrs a0, fcsr, zero
[0x80000980]:sd t6, 416(t2)
[0x80000984]:sd a0, 424(t2)
[0x80000988]:ld t5, 232(s1)
[0x8000098c]:addi fp, zero, 0
[0x80000990]:csrrw zero, fcsr, fp
[0x80000994]:fsqrt.s t6, t5, dyn

[0x80000994]:fsqrt.s t6, t5, dyn
[0x80000998]:csrrs a0, fcsr, zero
[0x8000099c]:sd t6, 432(t2)
[0x800009a0]:sd a0, 440(t2)
[0x800009a4]:ld t5, 240(s1)
[0x800009a8]:addi fp, zero, 0
[0x800009ac]:csrrw zero, fcsr, fp
[0x800009b0]:fsqrt.s t6, t5, dyn

[0x800009b0]:fsqrt.s t6, t5, dyn
[0x800009b4]:csrrs a0, fcsr, zero
[0x800009b8]:sd t6, 448(t2)
[0x800009bc]:sd a0, 456(t2)
[0x800009c0]:ld t5, 248(s1)
[0x800009c4]:addi fp, zero, 0
[0x800009c8]:csrrw zero, fcsr, fp
[0x800009cc]:fsqrt.s t6, t5, dyn

[0x800009cc]:fsqrt.s t6, t5, dyn
[0x800009d0]:csrrs a0, fcsr, zero
[0x800009d4]:sd t6, 464(t2)
[0x800009d8]:sd a0, 472(t2)
[0x800009dc]:ld t5, 256(s1)
[0x800009e0]:addi fp, zero, 0
[0x800009e4]:csrrw zero, fcsr, fp
[0x800009e8]:fsqrt.s t6, t5, dyn

[0x800009e8]:fsqrt.s t6, t5, dyn
[0x800009ec]:csrrs a0, fcsr, zero
[0x800009f0]:sd t6, 480(t2)
[0x800009f4]:sd a0, 488(t2)
[0x800009f8]:ld t5, 264(s1)
[0x800009fc]:addi fp, zero, 0
[0x80000a00]:csrrw zero, fcsr, fp
[0x80000a04]:fsqrt.s t6, t5, dyn

[0x80000a04]:fsqrt.s t6, t5, dyn
[0x80000a08]:csrrs a0, fcsr, zero
[0x80000a0c]:sd t6, 496(t2)
[0x80000a10]:sd a0, 504(t2)
[0x80000a14]:ld t5, 272(s1)
[0x80000a18]:addi fp, zero, 0
[0x80000a1c]:csrrw zero, fcsr, fp
[0x80000a20]:fsqrt.s t6, t5, dyn

[0x80000a20]:fsqrt.s t6, t5, dyn
[0x80000a24]:csrrs a0, fcsr, zero
[0x80000a28]:sd t6, 512(t2)
[0x80000a2c]:sd a0, 520(t2)
[0x80000a30]:ld t5, 280(s1)
[0x80000a34]:addi fp, zero, 0
[0x80000a38]:csrrw zero, fcsr, fp
[0x80000a3c]:fsqrt.s t6, t5, dyn

[0x80000a3c]:fsqrt.s t6, t5, dyn
[0x80000a40]:csrrs a0, fcsr, zero
[0x80000a44]:sd t6, 528(t2)
[0x80000a48]:sd a0, 536(t2)
[0x80000a4c]:ld t5, 288(s1)
[0x80000a50]:addi fp, zero, 0
[0x80000a54]:csrrw zero, fcsr, fp
[0x80000a58]:fsqrt.s t6, t5, dyn

[0x80000a58]:fsqrt.s t6, t5, dyn
[0x80000a5c]:csrrs a0, fcsr, zero
[0x80000a60]:sd t6, 544(t2)
[0x80000a64]:sd a0, 552(t2)
[0x80000a68]:ld t5, 296(s1)
[0x80000a6c]:addi fp, zero, 0
[0x80000a70]:csrrw zero, fcsr, fp
[0x80000a74]:fsqrt.s t6, t5, dyn

[0x80000a74]:fsqrt.s t6, t5, dyn
[0x80000a78]:csrrs a0, fcsr, zero
[0x80000a7c]:sd t6, 560(t2)
[0x80000a80]:sd a0, 568(t2)
[0x80000a84]:ld t5, 304(s1)
[0x80000a88]:addi fp, zero, 0
[0x80000a8c]:csrrw zero, fcsr, fp
[0x80000a90]:fsqrt.s t6, t5, dyn

[0x80000a90]:fsqrt.s t6, t5, dyn
[0x80000a94]:csrrs a0, fcsr, zero
[0x80000a98]:sd t6, 576(t2)
[0x80000a9c]:sd a0, 584(t2)
[0x80000aa0]:ld t5, 312(s1)
[0x80000aa4]:addi fp, zero, 0
[0x80000aa8]:csrrw zero, fcsr, fp
[0x80000aac]:fsqrt.s t6, t5, dyn

[0x80000aac]:fsqrt.s t6, t5, dyn
[0x80000ab0]:csrrs a0, fcsr, zero
[0x80000ab4]:sd t6, 592(t2)
[0x80000ab8]:sd a0, 600(t2)
[0x80000abc]:ld t5, 320(s1)
[0x80000ac0]:addi fp, zero, 0
[0x80000ac4]:csrrw zero, fcsr, fp
[0x80000ac8]:fsqrt.s t6, t5, dyn

[0x80000ac8]:fsqrt.s t6, t5, dyn
[0x80000acc]:csrrs a0, fcsr, zero
[0x80000ad0]:sd t6, 608(t2)
[0x80000ad4]:sd a0, 616(t2)
[0x80000ad8]:ld t5, 328(s1)
[0x80000adc]:addi fp, zero, 0
[0x80000ae0]:csrrw zero, fcsr, fp
[0x80000ae4]:fsqrt.s t6, t5, dyn

[0x80000ae4]:fsqrt.s t6, t5, dyn
[0x80000ae8]:csrrs a0, fcsr, zero
[0x80000aec]:sd t6, 624(t2)
[0x80000af0]:sd a0, 632(t2)
[0x80000af4]:ld t5, 336(s1)
[0x80000af8]:addi fp, zero, 0
[0x80000afc]:csrrw zero, fcsr, fp
[0x80000b00]:fsqrt.s t6, t5, dyn



```

## Details of STAT4:

```
Last Coverpoint : ['mnemonic : fsqrt.s', 'rs1 : x31', 'rd : x31', 'rs1 == rd', 'fs1 == 0 and fe1 == 0xfe and fm1 == 0x7fffff and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003b8]:fsqrt.s t6, t6, dyn
	-[0x800003bc]:csrrs tp, fcsr, zero
	-[0x800003c0]:sd t6, 0(ra)
	-[0x800003c4]:sd tp, 8(ra)
Current Store : [0x800003c4] : sd tp, 8(ra) -- Store: [0x80002420]:0x0000000000000001




Last Coverpoint : ['rs1 : x29', 'rd : x30', 'rs1 != rd', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003d4]:fsqrt.s t5, t4, dyn
	-[0x800003d8]:csrrs tp, fcsr, zero
	-[0x800003dc]:sd t5, 16(ra)
	-[0x800003e0]:sd tp, 24(ra)
Current Store : [0x800003e0] : sd tp, 24(ra) -- Store: [0x80002430]:0x0000000000000000




Last Coverpoint : ['rs1 : x30', 'rd : x29', 'fs1 == 0 and fe1 == 0x39 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800003f0]:fsqrt.s t4, t5, dyn
	-[0x800003f4]:csrrs tp, fcsr, zero
	-[0x800003f8]:sd t4, 32(ra)
	-[0x800003fc]:sd tp, 40(ra)
Current Store : [0x800003fc] : sd tp, 40(ra) -- Store: [0x80002440]:0x0000000000000000




Last Coverpoint : ['rs1 : x27', 'rd : x28', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x480000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000040c]:fsqrt.s t3, s11, dyn
	-[0x80000410]:csrrs tp, fcsr, zero
	-[0x80000414]:sd t3, 48(ra)
	-[0x80000418]:sd tp, 56(ra)
Current Store : [0x80000418] : sd tp, 56(ra) -- Store: [0x80002450]:0x0000000000000000




Last Coverpoint : ['rs1 : x28', 'rd : x27', 'fs1 == 0 and fe1 == 0xb7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000428]:fsqrt.s s11, t3, dyn
	-[0x8000042c]:csrrs tp, fcsr, zero
	-[0x80000430]:sd s11, 64(ra)
	-[0x80000434]:sd tp, 72(ra)
Current Store : [0x80000434] : sd tp, 72(ra) -- Store: [0x80002460]:0x0000000000000000




Last Coverpoint : ['rs1 : x25', 'rd : x26', 'fs1 == 0 and fe1 == 0xc7 and fm1 == 0x720000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000444]:fsqrt.s s10, s9, dyn
	-[0x80000448]:csrrs tp, fcsr, zero
	-[0x8000044c]:sd s10, 80(ra)
	-[0x80000450]:sd tp, 88(ra)
Current Store : [0x80000450] : sd tp, 88(ra) -- Store: [0x80002470]:0x0000000000000000




Last Coverpoint : ['rs1 : x26', 'rd : x25', 'fs1 == 0 and fe1 == 0xf4 and fm1 == 0x610000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000460]:fsqrt.s s9, s10, dyn
	-[0x80000464]:csrrs tp, fcsr, zero
	-[0x80000468]:sd s9, 96(ra)
	-[0x8000046c]:sd tp, 104(ra)
Current Store : [0x8000046c] : sd tp, 104(ra) -- Store: [0x80002480]:0x0000000000000000




Last Coverpoint : ['rs1 : x23', 'rd : x24', 'fs1 == 0 and fe1 == 0x86 and fm1 == 0x704000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000047c]:fsqrt.s s8, s7, dyn
	-[0x80000480]:csrrs tp, fcsr, zero
	-[0x80000484]:sd s8, 112(ra)
	-[0x80000488]:sd tp, 120(ra)
Current Store : [0x80000488] : sd tp, 120(ra) -- Store: [0x80002490]:0x0000000000000000




Last Coverpoint : ['rs1 : x24', 'rd : x23', 'fs1 == 0 and fe1 == 0x97 and fm1 == 0x5c8000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000498]:fsqrt.s s7, s8, dyn
	-[0x8000049c]:csrrs tp, fcsr, zero
	-[0x800004a0]:sd s7, 128(ra)
	-[0x800004a4]:sd tp, 136(ra)
Current Store : [0x800004a4] : sd tp, 136(ra) -- Store: [0x800024a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x21', 'rd : x22', 'fs1 == 0 and fe1 == 0x82 and fm1 == 0x044000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004b4]:fsqrt.s s6, s5, dyn
	-[0x800004b8]:csrrs tp, fcsr, zero
	-[0x800004bc]:sd s6, 144(ra)
	-[0x800004c0]:sd tp, 152(ra)
Current Store : [0x800004c0] : sd tp, 152(ra) -- Store: [0x800024b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x22', 'rd : x21', 'fs1 == 0 and fe1 == 0x3b and fm1 == 0x108000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004d0]:fsqrt.s s5, s6, dyn
	-[0x800004d4]:csrrs tp, fcsr, zero
	-[0x800004d8]:sd s5, 160(ra)
	-[0x800004dc]:sd tp, 168(ra)
Current Store : [0x800004dc] : sd tp, 168(ra) -- Store: [0x800024c0]:0x0000000000000000




Last Coverpoint : ['rs1 : x19', 'rd : x20', 'fs1 == 0 and fe1 == 0x24 and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800004ec]:fsqrt.s s4, s3, dyn
	-[0x800004f0]:csrrs tp, fcsr, zero
	-[0x800004f4]:sd s4, 176(ra)
	-[0x800004f8]:sd tp, 184(ra)
Current Store : [0x800004f8] : sd tp, 184(ra) -- Store: [0x800024d0]:0x0000000000000000




Last Coverpoint : ['rs1 : x20', 'rd : x19', 'fs1 == 0 and fe1 == 0x59 and fm1 == 0x7d2000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000508]:fsqrt.s s3, s4, dyn
	-[0x8000050c]:csrrs tp, fcsr, zero
	-[0x80000510]:sd s3, 192(ra)
	-[0x80000514]:sd tp, 200(ra)
Current Store : [0x80000514] : sd tp, 200(ra) -- Store: [0x800024e0]:0x0000000000000000




Last Coverpoint : ['rs1 : x17', 'rd : x18', 'fs1 == 0 and fe1 == 0x8e and fm1 == 0x689000 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000524]:fsqrt.s s2, a7, dyn
	-[0x80000528]:csrrs tp, fcsr, zero
	-[0x8000052c]:sd s2, 208(ra)
	-[0x80000530]:sd tp, 216(ra)
Current Store : [0x80000530] : sd tp, 216(ra) -- Store: [0x800024f0]:0x0000000000000000




Last Coverpoint : ['rs1 : x18', 'rd : x17', 'fs1 == 0 and fe1 == 0x86 and fm1 == 0x130400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000540]:fsqrt.s a7, s2, dyn
	-[0x80000544]:csrrs tp, fcsr, zero
	-[0x80000548]:sd a7, 224(ra)
	-[0x8000054c]:sd tp, 232(ra)
Current Store : [0x8000054c] : sd tp, 232(ra) -- Store: [0x80002500]:0x0000000000000000




Last Coverpoint : ['rs1 : x15', 'rd : x16', 'fs1 == 0 and fe1 == 0x88 and fm1 == 0x7c0400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000055c]:fsqrt.s a6, a5, dyn
	-[0x80000560]:csrrs tp, fcsr, zero
	-[0x80000564]:sd a6, 240(ra)
	-[0x80000568]:sd tp, 248(ra)
Current Store : [0x80000568] : sd tp, 248(ra) -- Store: [0x80002510]:0x0000000000000000




Last Coverpoint : ['rs1 : x16', 'rd : x15', 'fs1 == 0 and fe1 == 0x66 and fm1 == 0x64c400 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000578]:fsqrt.s a5, a6, dyn
	-[0x8000057c]:csrrs tp, fcsr, zero
	-[0x80000580]:sd a5, 256(ra)
	-[0x80000584]:sd tp, 264(ra)
Current Store : [0x80000584] : sd tp, 264(ra) -- Store: [0x80002520]:0x0000000000000000




Last Coverpoint : ['rs1 : x13', 'rd : x14', 'fs1 == 0 and fe1 == 0xf9 and fm1 == 0x7ff200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000594]:fsqrt.s a4, a3, dyn
	-[0x80000598]:csrrs tp, fcsr, zero
	-[0x8000059c]:sd a4, 272(ra)
	-[0x800005a0]:sd tp, 280(ra)
Current Store : [0x800005a0] : sd tp, 280(ra) -- Store: [0x80002530]:0x0000000000000000




Last Coverpoint : ['rs1 : x14', 'rd : x13', 'fs1 == 0 and fe1 == 0x39 and fm1 == 0x69d200 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005b0]:fsqrt.s a3, a4, dyn
	-[0x800005b4]:csrrs tp, fcsr, zero
	-[0x800005b8]:sd a3, 288(ra)
	-[0x800005bc]:sd tp, 296(ra)
Current Store : [0x800005bc] : sd tp, 296(ra) -- Store: [0x80002540]:0x0000000000000000




Last Coverpoint : ['rs1 : x11', 'rd : x12', 'fs1 == 0 and fe1 == 0xef and fm1 == 0x7bb880 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005cc]:fsqrt.s a2, a1, dyn
	-[0x800005d0]:csrrs tp, fcsr, zero
	-[0x800005d4]:sd a2, 304(ra)
	-[0x800005d8]:sd tp, 312(ra)
Current Store : [0x800005d8] : sd tp, 312(ra) -- Store: [0x80002550]:0x0000000000000000




Last Coverpoint : ['rs1 : x12', 'rd : x11', 'fs1 == 0 and fe1 == 0xd0 and fm1 == 0x095440 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800005e8]:fsqrt.s a1, a2, dyn
	-[0x800005ec]:csrrs tp, fcsr, zero
	-[0x800005f0]:sd a1, 320(ra)
	-[0x800005f4]:sd tp, 328(ra)
Current Store : [0x800005f4] : sd tp, 328(ra) -- Store: [0x80002560]:0x0000000000000000




Last Coverpoint : ['rs1 : x9', 'rd : x10', 'fs1 == 0 and fe1 == 0x7b and fm1 == 0x46c080 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000604]:fsqrt.s a0, s1, dyn
	-[0x80000608]:csrrs tp, fcsr, zero
	-[0x8000060c]:sd a0, 336(ra)
	-[0x80000610]:sd tp, 344(ra)
Current Store : [0x80000610] : sd tp, 344(ra) -- Store: [0x80002570]:0x0000000000000000




Last Coverpoint : ['rs1 : x10', 'rd : x9', 'fs1 == 0 and fe1 == 0x80 and fm1 == 0x6d5a40 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000620]:fsqrt.s s1, a0, dyn
	-[0x80000624]:csrrs tp, fcsr, zero
	-[0x80000628]:sd s1, 352(ra)
	-[0x8000062c]:sd tp, 360(ra)
Current Store : [0x8000062c] : sd tp, 360(ra) -- Store: [0x80002580]:0x0000000000000000




Last Coverpoint : ['rs1 : x7', 'rd : x8', 'fs1 == 0 and fe1 == 0xd3 and fm1 == 0x6a7f20 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000063c]:fsqrt.s fp, t2, dyn
	-[0x80000640]:csrrs tp, fcsr, zero
	-[0x80000644]:sd fp, 368(ra)
	-[0x80000648]:sd tp, 376(ra)
Current Store : [0x80000648] : sd tp, 376(ra) -- Store: [0x80002590]:0x0000000000000000




Last Coverpoint : ['rs1 : x8', 'rd : x7', 'fs1 == 0 and fe1 == 0x34 and fm1 == 0x08f690 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000660]:fsqrt.s t2, fp, dyn
	-[0x80000664]:csrrs a0, fcsr, zero
	-[0x80000668]:sd t2, 384(ra)
	-[0x8000066c]:sd a0, 392(ra)
Current Store : [0x8000066c] : sd a0, 392(ra) -- Store: [0x800025a0]:0x0000000000000000




Last Coverpoint : ['rs1 : x5', 'rd : x6', 'fs1 == 0 and fe1 == 0x6a and fm1 == 0x3e2364 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000067c]:fsqrt.s t1, t0, dyn
	-[0x80000680]:csrrs a0, fcsr, zero
	-[0x80000684]:sd t1, 400(ra)
	-[0x80000688]:sd a0, 408(ra)
Current Store : [0x80000688] : sd a0, 408(ra) -- Store: [0x800025b0]:0x0000000000000000




Last Coverpoint : ['rs1 : x6', 'rd : x5', 'fs1 == 0 and fe1 == 0x00 and fm1 == 0x000160 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006a0]:fsqrt.s t0, t1, dyn
	-[0x800006a4]:csrrs a0, fcsr, zero
	-[0x800006a8]:sd t0, 0(t2)
	-[0x800006ac]:sd a0, 8(t2)
Current Store : [0x800006ac] : sd a0, 8(t2) -- Store: [0x800024f0]:0x0000000000000001




Last Coverpoint : ['rs1 : x3', 'rd : x4', 'fs1 == 0 and fe1 == 0xbc and fm1 == 0x68cd04 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006bc]:fsqrt.s tp, gp, dyn
	-[0x800006c0]:csrrs a0, fcsr, zero
	-[0x800006c4]:sd tp, 16(t2)
	-[0x800006c8]:sd a0, 24(t2)
Current Store : [0x800006c8] : sd a0, 24(t2) -- Store: [0x80002500]:0x0000000000000000




Last Coverpoint : ['rs1 : x4', 'rd : x3', 'fs1 == 0 and fe1 == 0x1b and fm1 == 0x5b5a62 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006d8]:fsqrt.s gp, tp, dyn
	-[0x800006dc]:csrrs a0, fcsr, zero
	-[0x800006e0]:sd gp, 32(t2)
	-[0x800006e4]:sd a0, 40(t2)
Current Store : [0x800006e4] : sd a0, 40(t2) -- Store: [0x80002510]:0x0000000000000000




Last Coverpoint : ['rs1 : x1', 'rd : x2', 'fs1 == 0 and fe1 == 0xea and fm1 == 0x4f33d9 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800006f4]:fsqrt.s sp, ra, dyn
	-[0x800006f8]:csrrs a0, fcsr, zero
	-[0x800006fc]:sd sp, 48(t2)
	-[0x80000700]:sd a0, 56(t2)
Current Store : [0x80000700] : sd a0, 56(t2) -- Store: [0x80002520]:0x0000000000000000




Last Coverpoint : ['rs1 : x2', 'rd : x1', 'fs1 == 0 and fe1 == 0xa0 and fm1 == 0x10d851 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000710]:fsqrt.s ra, sp, dyn
	-[0x80000714]:csrrs a0, fcsr, zero
	-[0x80000718]:sd ra, 64(t2)
	-[0x8000071c]:sd a0, 72(t2)
Current Store : [0x8000071c] : sd a0, 72(t2) -- Store: [0x80002530]:0x0000000000000000




Last Coverpoint : ['rs1 : x0']
Last Code Sequence : 
	-[0x8000072c]:fsqrt.s t6, zero, dyn
	-[0x80000730]:csrrs a0, fcsr, zero
	-[0x80000734]:sd t6, 80(t2)
	-[0x80000738]:sd a0, 88(t2)
Current Store : [0x80000738] : sd a0, 88(t2) -- Store: [0x80002540]:0x0000000000000000




Last Coverpoint : ['rd : x0', 'fs1 == 0 and fe1 == 0x52 and fm1 == 0x216b44 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000748]:fsqrt.s zero, t6, dyn
	-[0x8000074c]:csrrs a0, fcsr, zero
	-[0x80000750]:sd zero, 96(t2)
	-[0x80000754]:sd a0, 104(t2)
Current Store : [0x80000754] : sd a0, 104(t2) -- Store: [0x80002550]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x000005 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000764]:fsqrt.s t6, t5, dyn
	-[0x80000768]:csrrs a0, fcsr, zero
	-[0x8000076c]:sd t6, 112(t2)
	-[0x80000770]:sd a0, 120(t2)
Current Store : [0x80000770] : sd a0, 120(t2) -- Store: [0x80002560]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3c and fm1 == 0x124e58 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000780]:fsqrt.s t6, t5, dyn
	-[0x80000784]:csrrs a0, fcsr, zero
	-[0x80000788]:sd t6, 128(t2)
	-[0x8000078c]:sd a0, 136(t2)
Current Store : [0x8000078c] : sd a0, 136(t2) -- Store: [0x80002570]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xc0 and fm1 == 0x3590aa and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000079c]:fsqrt.s t6, t5, dyn
	-[0x800007a0]:csrrs a0, fcsr, zero
	-[0x800007a4]:sd t6, 144(t2)
	-[0x800007a8]:sd a0, 152(t2)
Current Store : [0x800007a8] : sd a0, 152(t2) -- Store: [0x80002580]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf3 and fm1 == 0x6653ed and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007b8]:fsqrt.s t6, t5, dyn
	-[0x800007bc]:csrrs a0, fcsr, zero
	-[0x800007c0]:sd t6, 160(t2)
	-[0x800007c4]:sd a0, 168(t2)
Current Store : [0x800007c4] : sd a0, 168(t2) -- Store: [0x80002590]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xfe and fm1 == 0x7f3827 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007d4]:fsqrt.s t6, t5, dyn
	-[0x800007d8]:csrrs a0, fcsr, zero
	-[0x800007dc]:sd t6, 176(t2)
	-[0x800007e0]:sd a0, 184(t2)
Current Store : [0x800007e0] : sd a0, 184(t2) -- Store: [0x800025a0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xa7 and fm1 == 0x0f78f8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800007f0]:fsqrt.s t6, t5, dyn
	-[0x800007f4]:csrrs a0, fcsr, zero
	-[0x800007f8]:sd t6, 192(t2)
	-[0x800007fc]:sd a0, 200(t2)
Current Store : [0x800007fc] : sd a0, 200(t2) -- Store: [0x800025b0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x0b and fm1 == 0x0cd684 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000080c]:fsqrt.s t6, t5, dyn
	-[0x80000810]:csrrs a0, fcsr, zero
	-[0x80000814]:sd t6, 208(t2)
	-[0x80000818]:sd a0, 216(t2)
Current Store : [0x80000818] : sd a0, 216(t2) -- Store: [0x800025c0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xdd and fm1 == 0x4096e8 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000828]:fsqrt.s t6, t5, dyn
	-[0x8000082c]:csrrs a0, fcsr, zero
	-[0x80000830]:sd t6, 224(t2)
	-[0x80000834]:sd a0, 232(t2)
Current Store : [0x80000834] : sd a0, 232(t2) -- Store: [0x800025d0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x0cd173 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000844]:fsqrt.s t6, t5, dyn
	-[0x80000848]:csrrs a0, fcsr, zero
	-[0x8000084c]:sd t6, 240(t2)
	-[0x80000850]:sd a0, 248(t2)
Current Store : [0x80000850] : sd a0, 248(t2) -- Store: [0x800025e0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x59 and fm1 == 0x0fed85 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000860]:fsqrt.s t6, t5, dyn
	-[0x80000864]:csrrs a0, fcsr, zero
	-[0x80000868]:sd t6, 256(t2)
	-[0x8000086c]:sd a0, 264(t2)
Current Store : [0x8000086c] : sd a0, 264(t2) -- Store: [0x800025f0]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x39 and fm1 == 0x0ef3b1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x8000087c]:fsqrt.s t6, t5, dyn
	-[0x80000880]:csrrs a0, fcsr, zero
	-[0x80000884]:sd t6, 272(t2)
	-[0x80000888]:sd a0, 280(t2)
Current Store : [0x80000888] : sd a0, 280(t2) -- Store: [0x80002600]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x00 and fm1 == 0x2bf296 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x80000898]:fsqrt.s t6, t5, dyn
	-[0x8000089c]:csrrs a0, fcsr, zero
	-[0x800008a0]:sd t6, 288(t2)
	-[0x800008a4]:sd a0, 296(t2)
Current Store : [0x800008a4] : sd a0, 296(t2) -- Store: [0x80002610]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf5 and fm1 == 0x0aadc1 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008b4]:fsqrt.s t6, t5, dyn
	-[0x800008b8]:csrrs a0, fcsr, zero
	-[0x800008bc]:sd t6, 304(t2)
	-[0x800008c0]:sd a0, 312(t2)
Current Store : [0x800008c0] : sd a0, 312(t2) -- Store: [0x80002620]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0x3f and fm1 == 0x0577a2 and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008d0]:fsqrt.s t6, t5, dyn
	-[0x800008d4]:csrrs a0, fcsr, zero
	-[0x800008d8]:sd t6, 320(t2)
	-[0x800008dc]:sd a0, 328(t2)
Current Store : [0x800008dc] : sd a0, 328(t2) -- Store: [0x80002630]:0x0000000000000001




Last Coverpoint : ['fs1 == 0 and fe1 == 0xf9 and fm1 == 0x2b61ee and  fcsr == 0x0 and rm_val == 7   #nosat']
Last Code Sequence : 
	-[0x800008ec]:fsqrt.s t6, t5, dyn
	-[0x800008f0]:csrrs a0, fcsr, zero
	-[0x800008f4]:sd t6, 336(t2)
	-[0x800008f8]:sd a0, 344(t2)
Current Store : [0x800008f8] : sd a0, 344(t2) -- Store: [0x80002640]:0x0000000000000001





```

## Details of STAT5:



## Details of STAT1:

- The first column indicates the signature address(es) and the data at that location in hexadecimal in the following format:
  ```
  [Address1]
  Data1

  [Address2]
  Data2

  ...
  ```

- The second column captures all the coverpoints which have been captured by that particular signature location

- The third column captures all the insrtuctions since the time a coverpoint was
  hit to the point when a store to the signature was performed. Each line has
  the following format:
  ```
  [PC of instruction] : mnemonic
  ```
- The order in the table is based on the order of signatures occuring in the
  test. These need not necessarily be in increasing or decreasing order of the
  address in the signature region.

|s.no|signature|coverpoints|code|
|----|---------|-----------|----|
